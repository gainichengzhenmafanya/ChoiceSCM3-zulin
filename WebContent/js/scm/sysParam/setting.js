
//初始化遍历多选只有一个选项
function checkboxInitial(id,isSelect){
	  if(isSelect == 'N'||isSelect == '0'){		  
		  $("#"+id).removeClass('ant-checkbox-checked');
	  }else{
        $("#"+id).addClass('ant-checkbox-checked');
      }
}

//初始化遍历多选的多个选项
function checkboxInitialAll(id, name) {
//console.log("-----------id",id,name)
  var checkboxArry = id.split(',');
   //console.log("多选", !!checkboxArry);
  if (!!checkboxArry) {

	  for (var i=0;i<checkboxArry.length;i++){
		  $("span[name='" + name + "'][value='" + checkboxArry[i] + "']").addClass('ant-checkbox-checked');
	  }
  } else {
    //  $(".checkboxArry[name='+"name"+']").removeClass('ant-checkbox-checked');
    $("span[name='" + name + "']").each(function() {
      $(this).removeClass('ant-checkbox-checked');
    })
  }
}

//初始值遍历单选数据
function radioInitial(name,value){
  $("label[name='"+name+"']").removeClass('ant-radio-wrapper-checked');
  $("label[name='"+name+"']").children("span.ant-radio").removeClass('ant-radio-checked');
  $("label[name='"+name+"']").each(function(){
		  if($(this).find('input').attr('value')==value){
		    $(this).addClass('ant-radio-wrapper-checked');
		    $(this).children("span.ant-radio").addClass('ant-radio-checked');
		  };
	  }
  )
}
