//表格数据部分
var onTableResized = function(e){
	//js计算表头列宽
	var trnum = $(e.currentTarget).find("tr").length;
	var array = new Array();
	if(trnum==1){
		$(e.currentTarget).find("tr").find('td').each(function(index){
			array[index]=$(this).width();
		});
	}else if(trnum==2){//双表头
		var crnum=-1;
		$(e.currentTarget).find("tr:first").find('td').each(function(index){
			var colspan = $(this).attr('colspan');
			var rowspan = $(this).attr('rowspan');
			if(colspan==undefined||colspan==''||colspan=='1'){
				if(rowspan==undefined||rowspan==''||rowspan=='1'){
					crnum++;
				}
				array[array.length]=$(this).width();
			}else{
				var numcolspan = Number(colspan);
				var arraylen = array.length;
				for(var i=arraylen;i<arraylen+numcolspan;i++){
					crnum++;
					$(e.currentTarget).find("tr:last").find('td');
					array[i] = $(e.currentTarget).find("tr:last").find('td:eq('+crnum+')').width();
				}
			}
		});
	}else{//其它不做
	}
	//alert(array);
	//把数据表格的宽度进行调整
	for(var i=0,len=array.length;i<len;i++){
		$('#'+thisPageTableDataId).find('tr td:eq('+i+')').width(array[i]);
	}
};
//保存数据表的id
var thisPageTableDataId = '';
//使表列宽可调整
function tableCanResize(table_head,table_data){
	thisPageTableDataId = table_data;
	//表格头部
	$("#"+table_head).colResizable({
		liveDrag:true,
		gripInnerHtml:"<div class='grip'></div>",
		draggingClass:"dragging",
		onResize:onTableResized
	});
}