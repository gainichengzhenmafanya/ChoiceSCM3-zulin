//改变table的宽度2012-05-09  
var lineMove = false;
var currTh = null;
function changeTh(){
	 $("body").append("<div id=\"line\" style=\"width:1px;height:200px;border-left:1px solid #00000000; position:absolute;display:none\" ></div> ");
     $("body").bind("mousemove", function(event) {
         if (lineMove == true) {
             $("#line").css({ "left": event.clientX }).show();
         }
     });

     $(".table-head").find('td').bind("mousemove", function(event) {
         var th = $(this);
         //不给第1、2列添加效果  
         if (th.prevAll().length <= 2 || th.nextAll().length < 0) {
             return;
         }
         var left = th.offset().left;
         //距离表头边框线左右4像素才触发效果,鼠标变型
         if (event.clientX - left < 4 || (th.width() - (event.clientX - left)) < 4) {
             th.css({ 'cursor': 'col-resize' });
         }
         else {
             th.css({ 'cursor': 'default' });
         }
     });
     //在Td的mousemove事件中处理：
     $(".table-head").find('td').bind("mousedown", function(event) {
         var th = $(this);
         if (th.prevAll().length <= 2 | th.nextAll().length < 0) {
             return;
         }
         var pos = th.offset();
         if (event.clientX - pos.left < 4 || (th.width() - (event.clientX - pos.left)) < 4) {
             var height = th.parent().parent().height();
             var top = pos.top;
             $("#line").css({ "height": height, "top": top,"left":event .clientX,"display":"" });
             lineMove = true;
             if (event.clientX - pos.left < th.width() / 2) {
                 currTh = th.prev();
             }
             else {
                 currTh = th;
             }
         }
     });
     //最后鼠标弹起时，最后的调整列宽效果。
     $("body").bind("mouseup", function(event) {
         if (lineMove == true) {
             $("#line").hide();
             lineMove = false;
             var pos = currTh.offset();
             var index = currTh.prevAll().length;
             var currThWidth=currTh.width();
             currTh.width(event.clientX - pos.left);
             currTh.find('span').width(event.clientX - pos.left-10);
             $(".table-body").find("tr").each(function() {
            	 var width=$(".table-head").find('td').eq(index).width();
            	 
            	 if((width-2)>(event.clientX - pos.left)){
            		 $(this).children().eq(index).find('span').width(width-10);
            		 $(this).find('td').eq(index).width(width);
            	 }else{
            		 $(this).children().eq(index).find('span').width(event.clientX - pos.left-10);
            		 $(this).find("td").eq(index).width(event.clientX - pos.left);
            	 }
             }); //.children().eq(index).width(event.clientX - pos.left);
             //调整滚动条的长度，避免在列宽变大后再减小时导致的横向滚动条只缩短不增长的bug
              //if(event.clientX - pos.left-currThWidth>0){
        	  var width3=$(".table-body").width();
        	  $(".table-head").width(width3+event.clientX - pos.left-currThWidth);
        	  $(".table-body").width(width3+event.clientX - pos.left-currThWidth);
              //}
           	loadGrid();//  自动计算滚动条的js方法
         }
     });
 };