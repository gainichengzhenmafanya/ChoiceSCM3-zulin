var queryParams = {};
var tableHeight = 0;//充满div时，表格高度
var bodyHeight = 0;//iframe body高度
var firstLoad = true;//标记是否为第一次加载
var colChooseWindow;
var fieldMap = {};
//方法延时执行
var delay = function(t,func){
	var self = this;
	if(self.curTime)
		clearTimeout(self.curTime);
	self.curTime = setTimeout(function(){
		func.apply(self);
		},t*1000);
};
//生成工具栏
function builtToolBar(params){
	var search_tele ;
	try{
		search_tele = report_search_perm_tele;
	}catch(exception){
		search_tele = true;
	}
	var excel_tele ;
	try{
		excel_tele = report_export_perm_tele;
	}catch(exception){
		excel_tele = true;
	}
	var print_tele ;
	try{
		print_tele = report_print_perm_tele;
	}catch(exception){
		print_tele = true;
	}
	var form = $('#'+params.formId);//页面formid
	var grid = params.gridId ? $('#'+params.gridId) : params.grid;//表格所在div
	var basePath = params.basePath;
	var curtoolbar = params.toolbar;//需要的工具按钮,可能的值search,excel,print,option,exit
	var searchFun = params.searchFun; //自定义查询方法
	var verifyFun = params.verifyFun;
	var exportTyp = params.exportTyp;//excel导出时获取表头的方式，默认为从数据库查询。设置为true时从页面获取
	var items = [];
	if(grid)grid.data("verifyFun",verifyFun);
	var toolbar = {search:{
		text: $.messager.defaults.search,
		title: $.messager.defaults.search,
		useable:search_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['0px','-40px']
		},
		handler: function(){
			delay(0.5,function(){
				if(!(verifyFun ? verifyFun() : true))return;
				searchFun ? searchFun(grid,form) :
				grid.datagrid("load",getParam(form));
			});
		}
	},
	excel:{
		text: $.messager.defaults.excel,
		title: $.messager.defaults.excel,
		useable:excel_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-40px','-20px']
		},
		handler: function(){
			if(!(verifyFun ? verifyFun() : true))return;
			var headers = [];
			if(exportTyp){
				var panel = grid.datagrid('getPanel');
				var content = panel.panel('body');
				function clearHead(head){
					head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
					head.find('td').each(function(){
						if($(this).css('display') == 'none'){
							$(this).remove();
						}else{
							$(this).removeAttr('class');
							$(this).children('div').html($.trim($(this).text()));
							$(this).children('div').removeAttr('class');
						}
					});
					return head.html();
				}
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));
				headers.push("<fieldMap>"+$.toJSON(fieldMap)+"</fieldMap>");
			}
			headers = headers.join("");
			var rs = headers.match(/\w+\s*=\w+/g);
			for(var s in rs){
				var string = String(rs[s]);
				string.match(/(\w+)$/g);
				headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));
			}
			var head = $("<input type='hidden' name='headers'/>");
			form.find("input[name='headers']").remove();
			head.val(headers.replace(/\r\n/g,""));
			head.appendTo(form);
			form.attr('action',params.excelUrl);
			form.submit();
		}
	},
	print:{
		text: $.messager.defaults.print,
		title: $.messager.defaults.print,
		useable:print_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-140px','-100px']
		},
		handler: function(){
			if(!(verifyFun ? verifyFun() : true))return;
			form.attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
			var action=params.printUrl;
			form.attr('action',action);
			form.submit();
		}
	},
	option:{
		text: $.messager.defaults.option,
		title: $.messager.defaults.option,
		useable:true,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-100px','-60px']
		},
		handler: function(){
			toColsChoose(params.colsChooseUrl);
		}
	},
	exit:{
		text: $.messager.defaults.exit,
		title: $.messager.defaults.exit,
		useable:true,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-160px','-100px']
		},
		handler: function(){
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
	}
	};
	
	for(var i in curtoolbar){
		if(typeof(curtoolbar[i]) == 'string')
			items.push(toolbar[curtoolbar[i]]);
		else
			items.push(curtoolbar[i]);
	}
	$('#'+params.toolbarId).html('');
	$('#'+params.toolbarId).toolbar({
		items:items
	});
	bodyHeight = $(".layout-panel-center",top.document).children('div[region="center"]').height() - $(".tab-control",top.document).height();
	tableHeight = bodyHeight - $("#tool").height() - $("#queryForm").height() - $(".tabs-header").height();
	$('body').height(bodyHeight);
}

//解析获取表单数据

function getParam(form){
	form = form.find("*[name]").filter(function(){
		return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val() 
		&& !$(this).attr('disabled');
	});
	var mul = ['radio','checkbox'];
	var temp = {};
	var param = {};
	form.each(function(){
		this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();
	});
	for(var i in temp){
		$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()
				: param[i] = form.filter('input[name="'+i+'"]:checked').val();
	}
	queryParams = param;
	return param;
}

//跳转到列选择页面
function toColsChoose(url){
	colChooseWindow = $('body').window({
		title: '列选择',
		content: '<iframe frameborder="0" src='+url+'></iframe>',
		width: '460px',
		height: '430px',
		draggable: true,
		isModal: true,
		confirmClose: false
	});
}

function closeColChooseWin(){
	if(colChooseWindow)
		colChooseWindow.close();
}
//生成表格
function builtTable(params){
	var headUrl = params.headUrl;//获取表头的url
	var width = params.width ? params.width : '100%';
	var contentUrl = params.dataUrl;//获取表格内容的url
	var remoteSort = String(params.remoteSort) != 'undefined' ? params.pagination : true;
	var title = params.title;//表格title
	var grid = params.id ? $('#'+params.id) : params.grid;//表格所在div
	var dateCols = params.dateCols ? params.dateCols.join(',').toLowerCase().split(',') : [];//需要按日期格式化的数据
	var timeCols = params.timeCols ? params.timeCols.join(',').toLowerCase().split(',') : [];//需要按时间格式化的数据
	var numCols = params.numCols ? params.numCols.join(',').toLowerCase().split(',') : [];//需要按数字格式化的数据
	var alignCols = params.alignCols ? params.alignCols.join(',').toLowerCase().split(',') : [];//需要右对齐的列
	var filter = typeof(params.filter) == 'function' ? params.filter : function(data){return data;};//对获取的数据进行格式化的方法
	var singleSelect = String(params.singleSelect) != 'undefined' ? params.singleSelect : true;//是否单选
	var pagination = String(params.pagination) != 'undefined' ? params.pagination : true;//是否显示分页工具条
	var showFooter = String(params.showFooter) != 'undefined' ? params.showFooter : true;//是否显示页脚栏
	var pageList = params.pageList ? params.pageList : [20,30,40,50];//定义分页数目
	var gridHeight = params.height ? params.height : tableHeight;//表格高度
	var onClickRow = params.onClickRow ? params.onClickRow : function(a,b){return;};//表格行单击事件，第一参数为行号，第二参数为改行数据json格式，{field:data}
	var onDblClickRow = params.onDblClickRow ? params.onDblClickRow : function(a,b){return;};
	var decimalDigitR = params.decimalDigitR ? Number(params.decimalDigitR) : 2;
	var decimalDigitF = params.decimalDigitF ? Number(params.decimalDigitF) : 2;
	var createHeader = params.createHeader ? params.createHeader : undefined; 
	var hiddenCols = params.hiddenCols ? params.hiddenCols : undefined;
	//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
	var tableContent = {};
	//表头行（单行）
	var columns = [];
	//表头（多行），其中元素为columns
	var head = [];
	//需要固定在左侧的列的表头（单行）
	var frozenHead = [];
	//需要固定在左侧的列的表头（多行），元素为frozenHead
	var frozenColumns = [];
	//ajax获取报表表头
	if(headUrl)
		$.ajax({url:headUrl,
				async:false,
				data:queryParams,
				type:'POST',
				success:function(data){
					tableContent = data;
				}
			});
	//解析获取的数据
	if(!createHeader){
		alignCols = alignCols.concat(numCols);
		var frozenIndex = tableContent.frozenColumns ? tableContent.frozenColumns.split(',') : [];
			var Cols = [];
			var colsSecond = [];
		var prev = '';
		var temp;
			for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
			var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
			if(t && !t.length){
				for(var i in tableContent.columns){
					if(!tableContent.columns[i].properties)continue;
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 
		 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
		 			else
		 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
		 		}
			head.push(columns);
		 		frozenHead.push(frozenColumns);
			}else{
				for(var i in tableContent.columns){
					if(!tableContent.columns[i].properties)continue;
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 
					if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
					else{
						var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
						if(cur && cur.length){
							var cur = tableContent.columns[i].zhColumnName;
							if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
								temp.colspan ++;
							}else{
								temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
								columns.push(temp);
								prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
							}
							colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
						}else{
							if(tableContent.columns[i].columnName)
								columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
						}
					}
				}
				head.push(columns);
				head.push(colsSecond);
				if(hiddenCols){
					for(var obj in hiddenCols)
						head[0].push({field:hiddenCols[obj].field,rowspan:head.length,hidden:true});
				}
				frozenHead.push(frozenColumns);
			}
	}else {
		createHeader(tableContent,head,frozenHead);
	}
	
 		//生成报表数据表格
		grid.datagrid({
	 			title:title,
	 			width:width,
	 			height:gridHeight,
	 			nowrap: true,
				striped: true,
				singleSelect:singleSelect,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
				dataFilter:function(data,type){
					if(createHeader)return data;
					var rs = eval("("+data+")");
					var modifyRows = [];
					var modifyFooter = [];
					var footer = rs.footer;
					var rows = rs.rows;
					if(!rows || rows.length <= 0)grid.datagrid('loadData',{total:0,rows:[],footer:[]});
					for(var i in rows){
						var cols = tableContent.columns;
						var curRow = {};
						for(var j in cols){
							try{
								var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								//-------------------------
								if($.inArray(cols[j].properties.toLowerCase(),numCols) >=0){
									value = value ? Number($.trim(String(value))).toFixed(decimalDigitR) : '0.00';
								}else if(String(value).match(/0|(?:.+)/)){
									if($.inArray(cols[j].properties.toLowerCase(),dateCols) >= 0){
										value = convertDate(value,false);
									}else if($.inArray(cols[j].properties.toLowerCase(),timeCols) >= 0){
										value = convertDate(value,true);
									}
								}else{
									value='';
								}
								//-----------------------------
								curRow[cols[j].columnName.toUpperCase()] = value;
								fieldMap[cols[j].columnName.toUpperCase()] = cols[j].properties.toUpperCase();
							}catch(e){
								console.warn('Exception   '+"rows["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);
							}
						}
						if(hiddenCols){
							for(var obj in hiddenCols)
								curRow[hiddenCols[obj].field] = eval("rows["+i+"]."+hiddenCols[obj].field);
						}
						modifyRows.push(curRow);
					}
					rs.rows = modifyRows;
					for(var i in footer){
						var cols = tableContent.columns;
						var foot = {};
						for(var j in cols){
							try{
								var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? Number($.trim(String(value))).toFixed(decimalDigitF) : '0.00') : (String(value).match(/0|(?:.+)/) ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								foot[cols[j].columnName.toUpperCase()] = value;
							}catch(e){
								console.warn('Exception   '+"footer["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);
							}
						}
						modifyFooter.push(foot);
					}
					rs.footer = modifyFooter;
					rs = filter(rs);
					return $.toJSON(rs);
					function convertDate(time,flag){
						if(isNaN(time)){
							return;
						}
						var date=new Date(time); 
						var str="";     
						str+=date.getFullYear()+"-";     
						str+=((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-";     
						str+=date.getDate()>9?date.getDate():"0"+date.getDate();
						if(flag){
							str += " "+((date.getHours()>9)?date.getHours():"0"+date.getHours())+":";
							str += ((date.getMinutes()>9)?date.getMinutes():"0"+date.getMinutes())+":";
							str += ((date.getSeconds()>9)?date.getSeconds():"0"+date.getSeconds());
						}
						return str;
					}
				},
				url:contentUrl,
				remoteSort: remoteSort,
				//页码选择项
				pageList:pageList,
				frozenColumns:frozenHead,
				columns:head,
				queryParams:queryParams,
				showFooter:showFooter,
				pagination:pagination,
				rownumbers:true,
				fitColumns:false,
				onClickRow:onClickRow,
				onDblClickRow:onDblClickRow,
				rowStyler:function(){
					return 'line-height:11px';
				},
				onBeforeLoad:function(){
					if(firstLoad){
						firstLoad = false;
						return false;
					}
					if(grid.data("verifyFun") && typeof(grid.data("verifyFun")) == "function"){
						return grid.data("verifyFun")();
					}
				}
	 	});
	 	$(".panel-tool").remove();
}
//新建图表(免费版)
function buildChart(params){
	var swf = params.swf;//展示图表的swf文件路径
	var url = params.url;//生成图表是获取的xml数据的url
	var id = params.id ? params.id : 'myChartId';//生成图表的id
	var width = params.width ? params.width : $('body').width();//生成图表的宽度
	var height = params.height ? params.height : tableHeight;//生成图表的高度
	var form = params.form;//从form中获取需要提交的参数，指定form
	var div = params.div;//生成的图表所在的div
	var para = getParam(form);//提交url时，需附加的参数
	var myChart = new FusionCharts(swf,id,width,height);
	//myChart.addParam("wmode","Opaque");//控制flash显示，防止chart显示在最上层
	$.ajax({
		url:url,
		data:para,
		type:'POST',
		beforeSend:function(){
			$('#wait2,#wait').css("visibility","visible");
		},
		success:function(data){
 			myChart.setDataXML(data);
 			myChart.render(div);
		},
		complete:function(){
			$('#wait2,#wait').css("visibility","hidden");
		}
	});
}
//选择分店
function selectFirm(params){
	var basePath = params.basePath;
	var url = basePath+"/firm/toChooseFirm.do";
	var callBack = params.callBack ? params.callBack : "setFirm";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择分店',
		content: '<iframe id="listFrimFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//会员选择分店
function crmSelectFirm(params){
	var basePath = params.basePath;
	var url = basePath+"/crmFirm/toChooseFirm.do";
	var callBack = params.callBack ? params.callBack : "setFirm";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择分店',
		content: '<iframe id="listFrimFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择类别
function selectPubGrp(params){
	var basePath = params.basePath;
	var url = basePath + "/pubGrp/toChoosePubGrp.do";
	var callBack = params.callBack ? params.callBack : "setPubGrp";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePubGrp',
		title: '选择类别',
		content: '<iframe id="listPubGrpFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 400,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择菜谱明细
function selectPubItem(params){
	var basePath = params.basePath;
	var url = basePath+"/pubItem/toChoosePubItem.do";
	var callBack = params.callBack ? params.callBack : "setPubItem";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePubItem',
		title: '选择菜谱明细',
		content: '<iframe id="listPubItemFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 450,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择菜品
function selectCaiPin(params){
	var basePath = params.basePath;
	var url = basePath+"/gift/toChoosePubitem.do";
	var callBack = params.callBack ? params.callBack : "setPubitem";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePubitem',
		title: '选择菜品',
		content: '<iframe id="listPubitemFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 600,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择套餐类型明细
function selectPackages(params){
	var basePath = params.basePath;
	var url = basePath+"/packAges/toChoosePackages.do";
	var callBack = params.callBack ? params.callBack : "setPackages";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePubItem',
		title: '选择菜谱信息',
		content: '<iframe id="listPubItemFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 450,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择(参数字典)
function selectCodDes(params){
	var basePath = params.basePath;
	var url = basePath+"/codDes/toChooseCodDes.do";
	var title = params.title ? params.title : '选择参数字典';
	var callBack = params.callBack ? params.callBack : "setCodDes";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePubItem',
		title: title,
		content: '<iframe id="listPubItemFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 450,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择事件
function selectEvets(params){
	var basePath = params.basePath;
	var url = basePath + "/operate/toChooseOperate.do";
	var callBack = params.callBack ? params.callBack : "setEvets";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseEvets',
		title: '选择事件',
		content: '<iframe id="listEvetsFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 270,
		height: params.height ? params.height : 370,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//限制输入框字数 
$.fn.limitLength = function(inputLength){
	var value = $(this).val();
	var startStr = value.replace(/[^\x00-\xff]/g, "**"); 
	var length = startStr.length; 
	//当填写的字节数小于设置的字节数 
	if (length * 1 <= inputLength * 1){
		return false; 
	}
	var limitStr = startStr.substr(0, inputLength);
	var count = 0; 
	var finalStr = ""; 
	for (var i = 0; i < limitStr.length; i++) { 
		 var flat = limitStr.substr(i, 1); 
		if (flat == "*") { 
			  count++; 
		} 
	} 
	var size = 0; 
	//var istar = startStr.substr(inputLength * 1 - 1, 1);//校验点是否为“×” 
	//if 基点是×; 判断在基点内有×为偶数还是奇数   
	if(count % 2 == 0){ 
		//当为偶数时 
		size = count / 2 + (inputLength * 1 - count); 
	}else{ 
		//当为奇数时 
		size = (count - 1) / 2 + (inputLength * 1 - count); 
	} 
	finalStr = value.substr(0, size); 
	this.val(finalStr); 
	return;			
};

$.getDateFromStr = function(str){
	var date = new Date();
	var reg = /(\d+)\D(\d+)\D(\d+)/;
	str.match(reg);
	date.setFullYear(RegExp.$1,Number(RegExp.$2)-1,RegExp.$3);
	return date;
};