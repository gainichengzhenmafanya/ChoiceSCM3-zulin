Date.prototype.prevYear = function(){
	var temp = new Date(this.getTime());
	temp.setFullYear(temp.getFullYear() - 1, temp.getMonth(), temp.getDate());
	return temp;
};
Date.prototype.prevMonth = function(){
	var temp = new Date(this.getTime());
	temp.setDate(1);
	return new Date(temp.getTime() - 24*60*60*1000);
};
Date.prototype.format = function(str){
	str = str ? str : "yyyy年mm月";
	var yy = String(this.getFullYear());
	var mm = String(this.getMonth() + 1 < 10 ? '0'+String(this.getMonth() + 1) : this.getMonth() + 1);
	var dd = String(this.getDate() < 10 ? '0'+String(this.getDate()) : this.getDate());
	var y = str.match(/y+/);
	var m = str.match(/m+/);
	var d = str.match(/d+/);
	str = y ? str.replace(y,yy.match(eval("/\\d{1,"+y[0].length+"}$/"))) : str;
	str = m ? str.replace(m,mm.match(eval("/\\d{1,"+m[0].length+"}$/"))) : str;
	str = d ? str.replace(d,dd.match(eval("/\\d{1,"+d[0].length+"}$/"))) : str;
	return str;
};