
function customSupply(titl,path,sp_code_,sp_name_,unit_,unit1_,unit_2,unit1_2,handler){
		customWindow = $('body').window({
			id: 'window_customWindow',
			title: titl,
			content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
			width: 1000,
			height: 550,
			confirmClose: false,
			draggable: true,
			isModal: true,
			afterCloseHandler: function(param){
				if(param=='Y'){
					var	sp_code = $('#selectFrame',top.window.document).contents().find('#sp_code').val();//编码
					var	sp_name = $('#selectFrame',top.window.document).contents().find('#sp_name').val();//名称
					var	unit = $('#selectFrame',top.window.document).contents().find('#unit').val();//单位
					var	unit1 = $('#selectFrame',top.window.document).contents().find('#unit1').val();//单位1
					sp_code_.val(sp_code);
					if(sp_name_ != undefined && unit_ != undefined && unit1_ != undefined){
						sp_name_.val(sp_name ? sp_name : sp_name_.val());
						unit_.val(unit ? unit : unit_.val());
						unit1_.val(unit1 ? unit1 : unit1_.val());
					}
					if(unit_2 != undefined && unit1_2 != undefined){
						unit_2.html(unit ? unit : unit_2.html());
						unit1_2.html(unit1 ? unit1 : unit1_2.html());
					}
					if(handler != undefined){
						handler(sp_code);
					}
				}else{
					customWindow = null;
				}
			}
		});
		return customWindow;
	}
//菜品Bom 产品bom添加返回成本单位数据
function customSupplyUnit2(titl,path,sp_code_,sp_name_,unit_,unit1_,unit_2,unit1_2,unit2_2,handler){
		
		customWindow = $('body').window({
			id: 'window_customWindow',
			title: titl,
			content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
			width: 1000,
			height: 550,
			confirmClose: false,
			draggable: true,
			isModal: true,
			afterCloseHandler: function(param){
				if(param=='Y'){
					var	sp_code = $('#selectFrame',top.window.document).contents().find('#sp_code').val();//编码
					var	sp_name = $('#selectFrame',top.window.document).contents().find('#sp_name').val();//名称
					var	unit = $('#selectFrame',top.window.document).contents().find('#unit').val();//单位
					var	unit1 = $('#selectFrame',top.window.document).contents().find('#unit1').val();//单位1
					var	unit2 = $('#selectFrame',top.window.document).contents().find('#unit2').val();//成本单位
					sp_code_.val(sp_code);
					if(sp_name_ != undefined && unit_ != undefined && unit1_ != undefined){
						sp_name_.val(sp_name ? sp_name : sp_name_.val());
						unit_.val(unit ? unit : unit_.val());
						unit1_.val(unit1 ? unit1 : unit1_.val());
					}
					if(unit_2 != undefined && unit1_2 != undefined){
						unit_2.html(unit ? unit : unit_2.html());
						unit1_2.html(unit1 ? unit1 : unit1_2.html());
					}
					if(unit2_2 != undefined && unit2_2 != null){
						unit2_2.html(unit2);
					}
					if(handler != undefined){
						handler(sp_code);
					}
				}else{
					customWindow = null;
				}
			}
		});
		return customWindow;
	}
function custom(titl,path,offset,nameInput,idInput,code,proInput,facInput,width,height,isNUll){
	var width_=200,height_=280;
	if(width!= undefined && height!= undefined){
		width_=width;height_=height;
	}
	customWindow = $('body').window({
		id: 'window_customWindow',
		title: titl,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
		width: width_,
		height: height_,
		confirmClose: false,
		isModal: true,
		afterCloseHandler: function(){
			var deptName = $('#selectFrame',top.window.document)
				.contents()
				.find('#parentName')
				.val();
			var	deptId = $('#selectFrame',top.window.document)
				.contents()
				.find('#parentId')
				.val();
			var proName = $('#selectFrame',top.window.document)
				.contents()
				.find('#proName')
				.val();
			var facName = $('#selectFrame',top.window.document)
				.contents()
				.find('#facName')
				.val();
			if(isNUll!=undefined){
				nameInput.val(deptName);
				idInput.val(deptId);
			}else{
				nameInput.val(deptName ? deptName : nameInput.val());
				idInput.val(deptId ? deptId : idInput.val());
			};
			if(proInput != undefined && facInput != undefined){
				proInput.val(proName ? proName : proInput.val());
				facInput.val(facName ? facName : facInput.val());
			}
			customWindow = null;
		}
	});
	
	return customWindow;
}
//选择菜品
function custCaiPin(titl,path,offset,itCodeInput,desInput,initInput,unitInput,minFenInput,arr,width,height,isNUll){
	var width_=200,height_=280;
	if(width!= undefined && height!= undefined){
		width_=width;height_=height;
	}
	customWindow = $('body').window({
		id: 'window_customWindow',
		title: titl,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
		width: width_,
		height: height_,
		confirmClose: false,
		isModal: true,
		afterCloseHandler: function(param){
			if(param=='Y'){
				var itCode = $('#selectFrame',top.window.document)
					.contents()
					.find('#PITCODE')
					.val();
				var	des = $('#selectFrame',top.window.document)
					.contents()
					.find('#PDES')
					.val();
				var init = $('#selectFrame',top.window.document)
					.contents()
					.find('#PINIT')
					.val();
				var unit = $('#selectFrame',top.window.document)
					.contents()
					.find('#PUNIT')
					.val();
				var minFen = $('#selectFrame',top.window.document)
					.contents()
					.find('#PRICE')
					.val();
				if(isNUll!=undefined){
					itCodeInput.val(itCode);
					desInput.val(des);
					initInput.val(init);
					minFenInput.val(minFen);
					if(inArray(unit,arr)){
						unitInput.val(unit);
					}else{
						unitInput.append("<option value="+unit+" selected='selected'>"+unit+"</option>");
					}
				}else{
					itCodeInput.val(itCode ? itCode : itCodeInput.val());
					desInput.val(des ? des : desInput.val());
					initInput.val(init ? init : initInput.val());
					unitInput.val(unit ? unit : unitInput.val());
					minFenInput.val(minFen ? minFen : minFenInput.val());
				};
			}else{
				customWindow = null;	
			}
		}
	});
	return customWindow;
}
function cust(titl,path,offset,nameInput,idInput,width,height,isNUll,handler){
	var width_=200,height_=280;
	if(width!= undefined && height!= undefined){
		width_=width;height_=height;
	}
	customWindow = $('body').window({
		id: 'window_customWindow',
		title: titl,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
		width: width_,
		height: height_,
		confirmClose: false,
		draggable: true,
		isModal: true,
		afterCloseHandler: function(param){
			var deptName = $('#selectFrame',top.window.document)
			.contents()
			.find('#parentName')
			.val();
			var	deptId = $('#selectFrame',top.window.document)
			.contents()
			.find('#parentId')
			.val();
			if(param=='Y'){
				if(isNUll!=undefined){
					nameInput.val(deptName);
					idInput.val(deptId);
				}else{
					nameInput.val(deptName ? deptName : nameInput.val());
					idInput.val(deptId ? deptId : idInput.val());
				};
				if(handler != undefined){
					handler(deptId);
				}
			}else{
				customWindow = null;
			}
			
		}
	});
	
	return customWindow;
}

function simpleCust(titl,path,offset,name,id,width,height,handler){
	var width_=200,height_=280;
	if(width!= undefined && height!= undefined){
		width_=width;height_=height;
	}
	customWindow = $('body').window({
		id: 'window_customWindow',
		title: titl,
		content: '<iframe id="selectFrame" name="selectFrame" frameborder="0" src="'+path+'"></iframe>',
		width: width_,
		height: height_,
		position: {
			type: 'absolute',
			top: offset.top,	
			left: offset.left
		},
		confirmClose: false,
		draggable: false,
		isModal: true,
		afterCloseHandler: function(){
			var deptName = $('#selectFrame').contents().find('#name').val();
			var	deptId = $('#selectFrame').contents().find('#id').val();
			var obj = {};
			obj['id']=deptId;
			obj['name']=deptName;
			if(handler != undefined){
				handler(obj);
			}
		}
	});
	
	return customWindow;
}


function closeCustom(){
	if(customWindow){
		customWindow.close_enter();
		customWindow = null;
	}
}

function inArray(param,array){
	for(var i=0; i<array.length;i++){
		if(param == array[i]){
			return true;
		}
	}
	return false;
}
