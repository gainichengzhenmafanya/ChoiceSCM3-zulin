function ajaxAddr(path){
	$.ajax({
		type: "POST",
		url: path+"/orderMeal/findTop.do",
		data: "name="+$("#addrName").val(),
		dataType: "json",
		success:function(addrList){
			if(addrList.length!=0){
				$("#mMenu").remove();
				$('#addrName').after("<ul id='mMenu'></ul>");
					$('#mMenu').append('<li class="mMenuSelected" style="width:100px;">'+addrList[0].name+'</li>');
				for(var i = 1 ; i < addrList.length ; i++){
					$('#mMenu').append('<li>'+addrList[i].name+'</li>');
		  		};
				$("#mMenu li").each(function(i) {
					$(this).bind('click',function(){
						$("#addrId").val(addrList[i].id);
						$("#addrName").val(addrList[i].name);
						$("#firmId").val(addrList[i].firmid);
						$('#mMenu').hide();
					});
				});
				var j=0;
				$(document).keydown(function (event) { 
					if (event.keyCode == 40) { //下
						j++;
					}
					if (event.keyCode == 38) { //上
						j--;
					}
					if(j>=addrList.length){
						j=0;
					}
					if(j<=-1){
						j=addrList.length-1;
					}
					if (event.keyCode == 13) { //回车
						$("#addrId").val(addrList[j].id);
						$("#addrName").val(addrList[j].name);
						$("#firmId").val(addrList[j].firmid);
						$('#mMenu').remove();
					}
					$('#mMenu li').removeClass("mMenuSelected");
					$('#mMenu li:eq('+j+')').addClass("mMenuSelected");
				}); 
			}else{
				$("#mMenu").remove();
				$('#addrName').after("<ul id='mMenu'></ul>");
				$('#mMenu').append('<li class="mMenuSelected">查无结果</li>');
				$("#addrId").val("");
				$("#firmId").val("");
			}
		},
		error: function(){
			alert("服务器没有返回数据，可能服务器忙，请重试");
		}
	});
}