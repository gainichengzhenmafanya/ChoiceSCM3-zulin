﻿(function () {
var URL;
var tmp = window.location.pathname;
URL = path+"/js/ueditor/"||window.UEDITOR_HOME_URL||tmp.substr(0,tmp.lastIndexOf("\/")+1).replace("_examples/","").replace("website/","");//这里你可以配置成ueditor目录在您网站的相对路径或者绝对路径（指以http开头的绝对路径）

window.UEDITOR_CONFIG = {
//为编辑器实例添加一个路径，这个不能被注释
UEDITOR_HOME_URL : URL
,lang:"zh-cn"
,langPath:URL +"lang/"
//图片上传配置区
,imageUrl:URL+"jsp/imageUp.jsp"           	//图片上传提交地址
,imagePath: path+"/"		                //图片修正地址，引用了fixedImagePath,如有特殊需求，可自行配置
//,imageFieldName:"upload"                  //图片数据的key,若此处修改，需要在后台对应文件修改对应参数
//,compressSide:0                           //等比压缩的基准，确定maxImageSideLength参数的参照对象。0为按照最长边，1为按照宽度，2为按照高度
,maxImageSideLength:900                     //上传图片最大允许的边长，超过会自动等比缩放,不缩放就设置一个比较大的值，更多设置在image.html中
//图片在线管理配置区
,imageManagerUrl:URL + "jsp/imageManager.jsp"       //图片在线管理的处理地址
,imageManagerPath:path                      //图片修正地址，同imagePath
//远程抓取配置区
//,catchRemoteImageEnable:true               //是否开启远程图片抓取,默认开启
,catcherUrl:URL +"jsp/getRemoteImage.jsp"   //处理远程图片抓取的地址
,catcherPath:URL + "jsp/"                  //图片修正地址，同imagePath
//,catchFieldName:"upfile"                   //提交到后台远程图片uri合集，若此处修改，需要在后台对应文件修改对应参数
//,separater:'ue_separate_ue'               //提交至后台的远程图片地址字符串分隔符
//,localDomain:[]                            //本地顶级域名，当开启远程图片抓取时，除此之外的所有其它域名下的图片都将被抓取到本地
//工具栏上的所有的功能按钮和下拉框，可以在new编辑器的实例时选择自己需要的从新定义
,toolbars:[["FontFamily","FontSize","Bold","Italic","Underline","ForeColor","BackColor","JustifyLeft","JustifyCenter","LineHeight","Undo","Redo","Link","Unlink","InsertImage","InsertTable","DeleteTable"]]
//当鼠标放在工具栏上时显示的tooltip提示
,labelMap:{'fontfamily':'','fontsize':'','bold':'','italic':'','underline':'','forecolor':'','backcolor':'','justifyleft':'','justifycenter':'','justifyright':'','justifyjustify':'','lineheight':'','undo':'','redo':'','link':'','unlink':'','inserttable':'','deletetable':'','splittocells':'','mergecells':'','insertcol':'','insertrow':'','deletecol':'','deleterow':''}
//常用配置项目
//,iframeUrlMap:{
// 'anchor':'~/dialogs/anchor/anchor.html',
// }
,isShow : true    //默认显示编辑器
,initialContent:""    //初始化编辑器的内容,也可以通过textarea/script给值，看官网例子
,initialFrameWidth:100  //初始化编辑器宽度,默认1000
,initialFrameHeight:300  //初始化编辑器高度,默认320
,autoClearinitialContent: false //是否自动清除编辑器初始内容，注意：如果focus属性设置为true,这个也为真，那么编辑器一上来就会触发导致初始化的内容看不到了
,textarea:"content"  // 提交表单时，服务器获取编辑器提交内容的所用的参数，多实例时可以给容器name属性，会将name给定的值最为每个实例的键值，不用每次实例化的时候都设置这个值
,focus:true //初始化时，是否让编辑器获得焦点true或false
,minFrameWidth:550    //编辑器最小宽度,默认800，拖动时以这个为准
//,minFrameHeight:220  //编辑器最小高度,默认220,拖动时以这个为准
,autoClearEmptyNode : true    //getContent时，是否删除空的inlineElement节点（包括嵌套的情况）
,fullscreen : false //是否开启初始化时即全屏，默认关闭
,readonly : false    //编辑器初始化结束后,编辑区域是否是只读的，默认是false
,zIndex : 900     //编辑器层级的基数,默认是900
,imagePopup:true      //图片操作的浮层开关，默认打开
//,initialStyle:'body{font-size:18px}'   //编辑器内部样式,可以用来改变字体等
,pasteplain:false  //是否纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
//字体设置 label留空支持多语言自动切换，若配置，则以配置值为准
,'fontfamily':[{"label":"","name":"songti","val":"宋体,SimSun"},{"label":"","name":"kaiti","val":"楷体,楷体_GB2312, SimKai"},{"label":"","name":"heiti","val":"黑体, SimHei"},{"label":"","name":"lishu","val":"隶书, SimLi"},{"label":"","name":"andaleMono","val":"andale mono"},{"label":"","name":"arial","val":"arial, helvetica,sans-serif"},{"label":"","name":"arialBlack","val":"arial black,avant garde"},{"label":"","name":"comicSansMs","val":"comic sans ms"},{"label":"","name":"impact","val":"impact,chicago"},{"label":"","name":"timesNewRoman","val":"times new roman"}]
//字号
,'fontsize':[10,11,12,14,16,18,20,24,36]
//行内间距 值和显示的名字相同
,'lineheight':["1","1.5","1.75","2","3","4","5"]
//customstyle
//自定义样式，不支持国际化，此处配置值即可最后显示值
//block的元素是依据设置段落的逻辑设置的，inline的元素依据BIU的逻辑设置
//尽量使用一些常用的标签
//参数说明
//tag 使用的标签名字
//label 显示的名字也是用来标识不同类型的标识符，注意这个值每个要不同，
//style 添加的样式
//每一个对象就是一个自定义的样式
,'customstyle':[
            {tag:'h1', name:'tc', label:'', style:'border-bottom:#ccc 2px solid;padding:0 4px 0 0;text-align:center;margin:0 0 20px 0;'},
            {tag:'h1', name:'tl',label:'', style:'border-bottom:#ccc 2px solid;padding:0 4px 0 0;margin:0 0 10px 0;'},
            {tag:'span',name:'im', label:'', style:'font-style:italic;font-weight:bold;color:#000'},
            {tag:'span',name:'hi', label:'', style:'font-style:italic;font-weight:bold;color:rgb(51, 153, 204)'}
        ]
//右键菜单的内容
,contextMenu:[{"label":"","cmdName":"delete"},{"label":"","cmdName":"selectall"},{"label":"","cmdName":"highlightcode","icon":"deletehighlightcode"},{"label":"","cmdName":"cleardoc","exec":function(){if(confirm("确定清空文档吗？")){this.execCommand("cleardoc");}}},"-",{"label":"","cmdName":"unlink"},"-",{"group":"","icon":"justifyjustify","subMenu":[{"label":"","cmdName":"justify","value":"left"},{"label":"","cmdName":"justify","value":"right"},{"label":"","cmdName":"justify","value":"center"},{"label":"","cmdName":"justify","value":"justify"}]},"-",{"label":"","cmdName":"edittable","exec":function(){this.ui._dialogs["inserttableDialog"].open();}},{"label":"","cmdName":"edittd","exec":function(){if(UE.ui["edittd"]){new UE.ui["edittd"](this);}this.ui._dialogs["edittdDialog"].open();}},{"group":"","icon":"table","subMenu":[{"label":"","cmdName":"inserttable"},{"label":"","cmdName":"deletetable"},{"label":"","cmdName":"insertparagraphbeforetable"},{"label":"","cmdName":"deleterow"},{"label":"","cmdName":"deletecol"},{"label":"","cmdName":"insertrow"},{"label":"","cmdName":"insertcol"},{"label":"","cmdName":"mergeright"},{"label":"","cmdName":"mergedown"},{"label":"","cmdName":"splittorows"},{"label":"","cmdName":"splittocols"},{"label":"","cmdName":"mergecells"},{"label":"","cmdName":"splittocells"}]},{"label":"","cmdName":"copy","exec":function(){alert("请使用ctrl+c进行复制");},"query":function(){return 0;}},{"label":"","cmdName":"paste","exec":function(){alert("请使用ctrl+v进行粘贴");},"query":function(){return 0;}},{"label":"","cmdName":"highlightcode","exec":function () {if ( UE.ui["highlightcode"] ) {new UE.ui["highlightcode"]( this );} this.ui._dialogs["highlightcodeDialog"].open();}}]
,wordCount:true          //是否开启字数统计
,maximumWords:2000       //允许的最大字符数
//字数统计提示，{#count}代表当前字数，{#leave}代表还可以输入多少字符数,留空支持多语言自动切换，否则按此配置显示
//,wordCountMsg:''   //当前已输入 {#count} 个字符，您还可以输入{#leave} 个字符
//超出字数限制提示  留空支持多语言自动切换，否则按此配置显示
//,wordOverFlowMsg:''    //<span style="color:red;">你输入的字符个数已经超出最大允许值，服务器可能会拒绝保存！</span>
//点击tab键时移动的距离,tabSize倍数，tabNode什么字符做为单位
,tabSize:"4"
,tabNode:"&nbsp;"
//是否启用元素路径，默认是显示
,elementPathEnabled : false
//可以最多回退的次数,默认20
,maxUndoCount:"20"
//当输入的字符数超过该值时，保存一次现场
,maxInputCount:"20"
// 是否自动长高,默认true
,autoHeightEnabled:false
//是否可以拉伸长高,默认true(当开启时，自动长高失效)
,scaleEnabled:true
//是否保持toolbar的位置不动,默认true
,autoFloatEnabled:true
//浮动时工具栏距离浏览器顶部的高度，用于某些具有固定头部的页面
//,topOffset:30
//首行缩进距离,默认是2em
//,indentValue:'2em'
};
})();