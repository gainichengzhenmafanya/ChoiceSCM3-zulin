function alterProfessional(path,chaValue,professionalId,professionalName,studentTypeId,sfzh){
	$('body').window({
		id: 'window_alter',
		title: '专业调整',
		content: '<iframe id="professinalAlter" frameborder="0" src="'+path+'/studentInfo/initProfessional.do?id='+chaValue+'&professionId.id='+professionalId+'&professionId.zymc='+professionalName+'&studentType.id='+studentTypeId+'&identityId='+sfzh+'"></iframe>',
		width: 497,
		height: 180,
		isModal: true
	});
}

function affirmReport(path,chaVlaue,sfzh){
	$('body').window({
		id: 'window_affirm',
		title: '确认报到',
		content: '<iframe id="window_affirmFrame" frameborder="0" src="'+path+'/studentInfo/affirmReport.do?id='+chaVlaue+'&identityId='+sfzh+'"></iframe>',
		width: 497,
		height: 180,
		isModal: true
	});
}