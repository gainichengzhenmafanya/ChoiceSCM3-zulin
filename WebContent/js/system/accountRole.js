
function addAccountRole(path,roleIds,accountId,show,context){
	context = context || $(document.body);	
	$.ajax({
		url: path+'/accountRole/ajaxAddAccountRole.do',
		type: 'POST',
		data: 'roleIds='+roleIds+'&accountId='+accountId,
		dataType: 'html',
		success: function(info){
			if(info && info === 'T'){
				
				//弹出提示信息
				showMessage({
					handler: function(){
						parent.relateRoleWin.close();
					}
				});					
				
				//修改选中角色的标识
				var idArray = roleIds.split(',');
				$.each(idArray,function(i){
					var id = idArray[i];
					$('#state_'+id,context)
						.children('span')
						.removeClass()
						.addClass('relate_1')
						.html(show+"&nbsp;");
					$('#chk_'+id,context).removeAttr('checked');
				});
				
			}
		}
	});
}

function deleteAccountRole(path,roleIds,accountId,show,context){
	context = context || $(document.body);	
	$.ajax({
		url: path+'/accountRole/ajaxDeleteAccountRole.do',
		type: 'POST',
		data: 'roleIds='+roleIds+'&accountId='+accountId,
		dataType: 'html',
		success: function(info){
			if(info && info === 'T'){
				
				//弹出提示信息
				showMessage({
					handler: function(){
						parent.relateRoleWin.close();
					}
				});					
				
				//修改选中角色的标识
				var idArray = roleIds.split(',');
				$.each(idArray,function(i){
					var id = idArray[i];
					$('#state_'+id,context)
						.children('span')
						.removeClass()
						.addClass('relate_0')
						.html(show+"&nbsp;");
					$('#chk_'+id,context).removeAttr('checked');
				});
				
			}
		}
	});
}

