﻿var queryParams = {};
var tableHeight = 0;//充满div时，表格高度
var bodyHeight = 0;//iframe body高度
var firstLoad = true;//标记是否为第一次加载
var colChooseWindow;
var fieldMap = {};
//方法延时执行
var delay = function(t,func){
	var self = this;
	if(self.curTime)
		clearTimeout(self.curTime);
	self.curTime = setTimeout(function(){
		func.apply(self);
		},t*1000);
};
var clearQueryForm = function(){
	$('.search-div input[type!=button]').val('');
};
//生成工具栏
function builtToolBar(params){
	var form = $('#'+params.formId);//页面formid
	var grid = params.gridId ? $('#'+params.gridId) : params.grid;//表格所在div
	var basePath = params.basePath;
	var curtoolbar = params.toolbar;//需要的工具按钮,可能的值search,excel,print,option,exit
	var searchFun = params.searchFun; //自定义查询方法
	var verifyFun = params.verifyFun;
	var exportTyp = params.exportTyp;//excel导出时获取表头的方式，默认为从数据库查询。设置为true时从页面获取
	var items = [];
	if(grid)grid.data("verifyFun",verifyFun);
	var toolbar = {search:{
		text: $.messager.defaults.search,
		title: $.messager.defaults.search,
		useable:report_search_perm_tele === undefined ? true : report_search_perm_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['0px','-40px']
		},
		handler: function(){
			delay(0.5,function(){
				if(!(verifyFun ? verifyFun() : true))return;
				searchFun ? searchFun(grid,form) :
				grid.datagrid("load",getParam(form));
			});
		}
	},
	excel:{
		text: $.messager.defaults.excel,
		title: $.messager.defaults.excel,
		useable:report_export_perm_tele === undefined ? true : report_export_perm_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-40px','-20px']
		},
		handler: function(){
			if(!(verifyFun ? verifyFun() : true))return;
			$("#wait2").css("visibility", "visible");
			$("#wait").css("visibility", "visible");
			var headers = [];
			if(exportTyp){
				var panel = grid.datagrid('getPanel');
				var content = panel.panel('body');
				function clearHead(head){
					head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
					head.find('td').each(function(){
						if($(this).css('display') == 'none'){
							$(this).remove();
						}else{
							$(this).removeAttr('class');
							$(this).children('div').html($.trim($(this).text()));
							$(this).children('div').removeAttr('class');
						}
					});
					return head.html();
				}
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));
				headers.push("<fieldMap>"+$.toJSON(fieldMap)+"</fieldMap>");
			}
			headers = headers.join("");
			var rs = headers.match(/\w+\s*=\w+/g);
			for(var s in rs){
				var string = String(rs[s]);
				string.match(/(\w+)$/g);
				headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));
			}
			var head = $("<input type='hidden' name='headers'/>");
			form.find("input[name='headers']").remove();
			head.val(headers.replace(/\r\n/g,""));
			head.appendTo(form);
			form.attr('action',params.excelUrl);
			form.submit();
			delay(1,function(){
				$("#wait2").css("visibility","hidden");
	 			$("#wait").css("visibility","hidden");
			});
		}
	},
	print:{
		text: $.messager.defaults.print,
		title: $.messager.defaults.print,
		useable:report_print_perm_tele === undefined ? true : report_print_perm_tele,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-140px','-100px']
		},
		handler: function(){
			if(!(verifyFun ? verifyFun() : true))return;
			form.attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
			var action=params.printUrl;
			form.attr('action',action);
			form.submit();
		}
	},
	option:{
		text: $.messager.defaults.option,
		title: $.messager.defaults.option,
		useable:true,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-100px','-60px']
		},
		handler: function(){
			toColsChoose(params.colsChooseUrl);
		}
	},
	exit:{
		text: $.messager.defaults.exit,
		title: $.messager.defaults.exit,
		useable:true,
		icon: {
			url: basePath+'/image/Button/op_owner.gif',
			position: ['-160px','-100px']
		},
		handler: function(){
			invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
	}
	};
	
	for(var i in curtoolbar){
		if(typeof(curtoolbar[i]) == 'string')
			items.push(toolbar[curtoolbar[i]]);
		else
			items.push(curtoolbar[i]);
	}
	$('#'+params.toolbarId).html('');
	$('#'+params.toolbarId).toolbar({
		items:items
	});
	bodyHeight = $(".layout-panel-center",top.document).children('div[region="center"]').height() - $(".tab-control",top.document).height();
	tableHeight = bodyHeight - $("#tool").height() - $("#queryForm").height() - $(".tabs-header").height();
	$('body').height(bodyHeight);
}

//解析获取表单数据

function getParam(form){
	form = form.find("*[name]").filter(function(){
		return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val() 
		&& !$(this).attr('disabled');
	});
	var mul = ['radio','checkbox'];
	var temp = {};
	var param = {};
	form.each(function(){
		this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();
	});
	for(var i in temp){
		$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()
				: param[i] = form.filter('input[name="'+i+'"]:checked').val();
	}
	queryParams = param;
	return param;
}

//跳转到列选择页面
function toColsChoose(url){
	colChooseWindow = $('body').window({
		title: '列选择',
		content: '<iframe frameborder="0" src='+url+'></iframe>',
		width: '460px',
		height: '430px',
		draggable: true,
		isModal: true,
		confirmClose: false
	});
}

function closeColChooseWin(){
	if(colChooseWindow)
		colChooseWindow.close();
}
//生成表格
function builtTable(params){
	var headUrl = params.headUrl;//获取表头的url
	var width = params.width ? params.width : '100%';
	var contentUrl = params.dataUrl;//获取表格内容的url
	var remoteSort = String(params.remoteSort) != 'undefined' ? params.pagination : true;
	var title = params.title;//表格title
	var grid = params.id ? $('#'+params.id) : params.grid;//表格所在div
	var dateCols = params.dateCols ? params.dateCols.join(',').toLowerCase().split(',') : [];//需要按日期格式化的数据
	var timeCols = params.timeCols ? params.timeCols.join(',').toLowerCase().split(',') : [];//需要按时间格式化的数据
	var numCols = params.numCols ? params.numCols.join(',').toLowerCase().split(',') : [];//需要按数字格式化的数据
	var alignCols = params.alignCols ? params.alignCols.join(',').toLowerCase().split(',') : [];//需要右对齐的列
	var filter = typeof(params.filter) == 'function' ? params.filter : function(data){return data;};//对获取的数据进行格式化的方法
	var singleSelect = String(params.singleSelect) != 'undefined' ? params.singleSelect : true;//是否单选
	var pagination = String(params.pagination) != 'undefined' ? params.pagination : true;//是否显示分页工具条
	var showFooter = String(params.showFooter) != 'undefined' ? params.showFooter : true;//是否显示页脚栏
	var pageList = params.pageList ? params.pageList : [20,30,40,50];//定义分页数目
	var gridHeight = params.height ? params.height : tableHeight;//表格高度
	var onClickRow = params.onClickRow ? params.onClickRow : function(a,b){return;};//表格行单击事件，第一参数为行号，第二参数为改行数据json格式，{field:data}
	var onDblClickRow = params.onDblClickRow ? params.onDblClickRow : function(a,b){return;};
	var decimalDigitR = params.decimalDigitR ? Number(params.decimalDigitR) : 2;
	var decimalDigitF = params.decimalDigitF ? Number(params.decimalDigitF) : 2;
	var createHeader = params.createHeader ? params.createHeader : undefined; 
	var hiddenCols = params.hiddenCols ? params.hiddenCols : undefined;
	//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
	var tableContent = {};
	//表头行（单行）
	var columns = [];
	//表头（多行），其中元素为columns
	var head = [];
	//需要固定在左侧的列的表头（单行）
	var frozenHead = [];
	//需要固定在左侧的列的表头（多行），元素为frozenHead
	var frozenColumns = [];
	//ajax获取报表表头
	if(headUrl!=null && headUrl!='')
		$.ajax({url:headUrl,
				async:false,
//				data:queryParams,
				type:'POST',
				success:function(data){
					tableContent = data;
				},
				error:function(data){
					alerterror(data);
				}
			});
	//解析获取的数据
	if(!createHeader){
		alignCols = alignCols.concat(numCols);
		var frozenIndex = tableContent.frozenColumns ? tableContent.frozenColumns.split(',') : [];
			var Cols = [];
			var colsSecond = [];
		var prev = '';
		var temp;
			for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
			var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
			if(t && !t.length){
				for(var i in tableContent.columns){
					if(!tableContent.columns[i].properties)continue;
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 
		 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
		 			else
		 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
		 		}
			head.push(columns);
		 		frozenHead.push(frozenColumns);
			}else{
				for(var i in tableContent.columns){
					if(!tableContent.columns[i].properties)continue;
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 
					if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
					else{
						var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
						if(cur && cur.length){
							var cur = tableContent.columns[i].zhColumnName;
							if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
								temp.colspan ++;
							}else{
								temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
								columns.push(temp);
								prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
							}
							colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
						}else{
							if(tableContent.columns[i].columnName)
								columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
						}
					}
				}
				head.push(columns);
				head.push(colsSecond);
				if(hiddenCols){
					for(var obj in hiddenCols)
						head[0].push({field:hiddenCols[obj].field,rowspan:head.length,hidden:true});
				}
				frozenHead.push(frozenColumns);
			}
	}else {
		createHeader(tableContent,head,frozenHead);
	}
	
 		//生成报表数据表格
		grid.datagrid({
	 			title:title,
	 			width:width,
	 			height:gridHeight,
	 			nowrap: true,
				striped: true,
				singleSelect:singleSelect,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化selectPrintset
				dataFilter:function(data,type){
					var rs = eval("("+data+")");
					if(createHeader)return $.toJSON(filter(rs,head));
					var modifyRows = [];
					var modifyFooter = [];
					var footer = rs.footer;
					var rows = rs.rows;
					if(!rows || rows.length <= 0)grid.datagrid('loadData',{total:0,rows:[],footer:[]});
					for(var i in rows){
						var cols = tableContent.columns;
						var curRow = {};
						for(var j in cols){
							try{
								var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								//-------------------------
								if($.inArray(cols[j].properties.toLowerCase(),numCols) >=0){
									value = value ? Number($.trim(String(value))).toFixed(decimalDigitR) : '0.00';
								}else if(String(value).match(/0|(?:.+)/)){
									if($.inArray(cols[j].properties.toLowerCase(),dateCols) >= 0){
										value = convertDate(value,false);
									}else if($.inArray(cols[j].properties.toLowerCase(),timeCols) >= 0){
										value = convertDate(value,true);
									}
								}else{
									value='';
								}
								//-----------------------------
								curRow[cols[j].columnName.toUpperCase()] = value;
								fieldMap[cols[j].columnName.toUpperCase()] = cols[j].properties.toUpperCase();
							}catch(e){
								console.warn('Exception   '+"rows["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);
							}
						}
						if(hiddenCols){
							for(var obj in hiddenCols)
								curRow[hiddenCols[obj].field] = eval("rows["+i+"]."+hiddenCols[obj].field);
						}
						modifyRows.push(curRow);
					}
					rs.rows = modifyRows;
					for(var i in footer){
						var cols = tableContent.columns;
						var foot = {};
						for(var j in cols){
							try{
								var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? Number($.trim(String(value))).toFixed(decimalDigitF) : '0.00') : (String(value).match(/0|(?:.+)/) ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
								foot[cols[j].columnName.toUpperCase()] = value;
							}catch(e){
								console.warn('Exception   '+"footer["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);
							}
						}
						modifyFooter.push(foot);
					}
					rs.footer = modifyFooter;
					rs = filter(rs);
					return $.toJSON(rs);
					function convertDate(time,flag){
						if(isNaN(time)){
							return;
						}
						var date=new Date(time); 
						var str="";     
						str+=date.getFullYear()+"-";     
						str+=((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-";     
						str+=date.getDate()>9?date.getDate():"0"+date.getDate();
						if(flag){
							str += " "+((date.getHours()>9)?date.getHours():"0"+date.getHours())+":";
							str += ((date.getMinutes()>9)?date.getMinutes():"0"+date.getMinutes())+":";
							str += ((date.getSeconds()>9)?date.getSeconds():"0"+date.getSeconds());
						}
						return str;
					}
				},
				url:contentUrl,
				remoteSort: remoteSort,
				//页码选择项
				pageList:pageList,
				frozenColumns:frozenHead,
				columns:head,
				queryParams:queryParams,
				showFooter:showFooter,
				pagination:pagination,
				rownumbers:true,
				fitColumns:false,
				onClickRow:onClickRow,
				onDblClickRow:onDblClickRow,
				rowStyler:function(){
					return 'line-height:11px';
				},
				onBeforeLoad:function(){
					if(firstLoad){
						firstLoad = false;
						return false;
					}
					if(grid.data("verifyFun") && typeof(grid.data("verifyFun")) == "function"){
						return grid.data("verifyFun")();
					}
				},
				onLoadSuccess:function(data){
					if(data.total==0){
						initone(data);
					}
				}
	 	});
	 	$(".panel-tool").remove();
}

function initone(data){
	if($("tr[datagrid-row-index='0']").length<=0){//首次进入报表页面，说明没有数据，就进行添加空行
		$('#datagrid').datagrid('insertRow',{
			row: {
			}
		});
		$("tr[datagrid-row-index='0']").css({"visibility":"hidden"});
	}else{
		//否则进行查询操作
		if($('.datagrid-body tbody tr').length==0){ //如果返回0条数据，就添加一行空数据
			$('#datagrid').datagrid('insertRow',{
				row: {
				}
			});
			$("tr[datagrid-row-index='0']").css({"visibility":"hidden"});
		}
	}
}
//新建图表(免费版)
function buildChart(params){
	var swf = params.swf;//展示图表的swf文件路径
	var url = params.url;//生成图表是获取的xml数据的url
	var id = params.id ? params.id : 'myChartId';//生成图表的id
	var width = params.width ? params.width : $('body').width();//生成图表的宽度
	var height = params.height ? params.height : tableHeight;//生成图表的高度
	var form = params.form;//从form中获取需要提交的参数，指定form
	var div = params.div;//生成的图表所在的div
	var para = getParam(form);//提交url时，需附加的参数
	var myChart = new FusionCharts(swf,id,width,height);
	//myChart.addParam("wmode","Opaque");//控制flash显示，防止chart显示在最上层
	$.ajax({
		url:url,
		data:para,
		type:'POST',
		beforeSend:function(){
//			$('#wait2,#wait').css("visibility","visible");
		},
		success:function(data){
 			myChart.setDataXML(data);
 			myChart.render(div);
		},
		complete:function(){
			$('#wait2,#wait').css("visibility","hidden");
		}
	});
}
//选择分店
function selectFirm(params){
	var basePath = params.basePath;
	var url = basePath+"/firm/toChooseFirm.do";
	var callBack = params.callBack ? params.callBack : "setFirm";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择分店',
		content: '<iframe id="listFrimFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
	
}
//选择物资类别
function selectMaterialType(params){
	var basePath = params.basePath;
	var url = basePath+"/materialType/selectMaterialType.do";
	var callBack = params.callBack ? params.callBack : "setMaterialType";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择物资类别',
		content: '<iframe id="listFrimFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
	
}
//选择物资
function selectMaterial(params){
	var basePath = params.basePath;
	var url = basePath+"/material/materiallist.do";
	var callBack = params.callBack ? params.callBack : "setMaterial";
	url += ("?callBack="+callBack);
	url += ("&pk_supplier="+$.trim(params.pk_supplier));
	url += ("&pk_org="+$.trim(params.pk_org));
	var irateflag = params.irateflag ? params.irateflag : "0";
	url += ("&irateflag="+irateflag);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseMaterial',
		title: '选择物资',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		maximizable : false,
		draggable: true,
		isModal: true
	});
}
//选择物资====来自商城的物资
function selectJmuMaterial(params){
	var basePath = params.basePath;
	var url = basePath+"/material/jmumateriallist.do";
	var callBack = params.callBack ? params.callBack : "setMaterial";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseMaterial',
		title: '选择物资',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择采购清单
function selectDetailedList(params){
	var basePath = params.basePath;
	var url = basePath+"/purtemplet/selectAllPurtempletm.do";
	var callBack = params.callBack ? params.callBack : "setDetailedList";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePurtemplet',
		title: '选择采购清单',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择采购订单
function selectPuprOrder(params){
	var basePath = params.basePath;
	var url = basePath+"/puprorder/selectAllPuprOrders.do";
	var callBack = params.callBack ? params.callBack : "setPuprOrder";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	if(params.bdate) url += '&bdate='+params.bdate;
	if(params.edate) url += '&edate='+params.edate;
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	url += '&delivercode='+params.delivercode;
	url += '&istate='+params.istate;
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_choosePuprorder',
		title:  params.title ? params.title :'选择采购订单',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择验货单
function selectInspect(params){
	var basePath = params.basePath;
	var url = basePath+"/inspect/selectAllInspectm.do";
	var callBack = params.callBack ? params.callBack : "setInspect";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	url += '&delivercode='+params.delivercode;
	if(params.istate) url += '&istate='+params.istate;
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseInspect',
		title: '选择验货单',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择入库单
function selectInOrder(params){
	var basePath = params.basePath;
	var url = basePath+"/otherpayables/selectInOrder.do";
	var callBack = params.callBack ? params.callBack : "setInspect";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	url += '&delivercode='+params.delivercode;
	if(params.istate) url += '&istate='+params.istate;
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseInspect',
		title: '选择入库单',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择供应商
function selectSupplier(params){
	var basePath = params.basePath;
	var url = basePath+"/supplier/selectOneDeliver.do";
	var callBack = params.callBack ? params.callBack : "setSupplier";
	url += ("?callBack="+callBack);
	url += ("&sp_code="+(params.sp_code?params.sp_code:''));
	if(params.single) url += '&single=true';
	url += params.defaultCode ? '&defaultCode='+params.defaultCode : '';
	url += params.defaultName ? '&defaultName='+params.defaultName : '';
	url += params.isReal ? '&isReal='+params.isReal : '';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseSupplier',
		title: '选择供应商',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择供应商
function selectdisFenBo(params){
	var basePath = params.basePath;
	var url = basePath+"/puprorder/selectDisfenbo.do";
	/*var callBack = params.callBack ? params.callBack : "setSupplier";*/
	/*url += ("?callBack="+callBack);
	url += ("&sp_code="+(params.sp_code?params.sp_code:''));*/
	/*if(params.single) url += '&single=true';
	url += params.defaultCode ? '&defaultCode='+params.defaultCode : '';
	url += params.defaultName ? '&defaultName='+params.defaultName : '';
	url += params.isReal ? '&isReal='+params.isReal : '';
	var data = params.param;*/
	return $('body').window({
		id: 'window_chooseSupplier',
		title: '来自报货分拨',
		content: '<iframe id="disFenbo" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 700,
		height: params.height ? params.height : 700,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择供应商====来自商城的供应商
function selectjmuSupplier(params){
	var basePath = params.basePath;
	var url = basePath+"/supplier/supplierJmuList.do";
	var callBack = params.callBack ? params.callBack : "setJmuSupplier";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	url += params.supplierName ? '&vname='+encodeURI(encodeURI(params.supplierName)) : '&vname=';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseJmuSupplier',
		title: '选择供应商',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择组织-授权/单选
function chooseDepartMent(params){
	var basePath = params.basePath;
	var url = basePath+"/department/chooseDepartment.do?single=false";//默认多选
	if(params.single) {
		url = basePath+"/department/chooseDepartmentSingle.do";//如果传过来的single参数为true，自动切换为单选界面
		url += '?single=true';
		url += ("&id="+params.id);
		url += ("&name="+params.name);
		url += ("&code="+params.code);
	}
	var callBack = params.callBack ? params.callBack : "setDepartment";
	url += ("&callBack="+callBack);
	url += ("&pk_check="+params.pk_check);
	url += ("&onlyFirm="+params.onlyFirm);//为"T"时只显示末级节点类型为门店的组织
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择组织',
		content: '<iframe id="listDepartmentFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择组织列表展示
function chooseDepartMentList(params){
	var basePath = params.basePath;
	var url = basePath+"/positn/chooseDepartmentList.do";
	var callBack = params.callBack ? params.callBack : "setDepartmentList";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	if(params.pk_orgs) url += '&pk_orgs='+params.pk_orgs;
	if(params.sp_code) url += '&sp_code='+params.sp_code;
	if(params.delivercode) url += '&delivercode='+params.delivercode;
	if(params.typn) url += '&typn='+params.typn;
	url += "&type="+params.type;
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择组织',
		content: '<iframe id="listDepartmentFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择组织列表展示
function chooseDepartMentList_new(params){
	var basePath = params.basePath;
	var url = basePath+"/positn/chooseDepartmentList.do";
	var callBack = params.callBack ? params.callBack : "setDepartmentList";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	if(params.pk_orgs) url += '&pk_orgs='+params.pk_orgs;
	if(params.sp_code) url += '&sp_code='+params.sp_code;
	if(params.delivercode) url += '&delivercode='+params.delivercode;
	if(params.typn) url += '&typn='+params.typn;
	url += "&type="+params.type;
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseFirm',
		title: '选择组织',
		content: '<iframe id="listDepartmentFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}


//选择合同条款类型
function chooseContractterms(params){
	var basePath = params.basePath;
	var url = basePath+"/contractterms/chooseContractterms.do";
	var callBack = params.callBack ? params.callBack : "setContractterms";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseContractterms',
		title: '合同条款类型',
		content: '<iframe id="listDepartmentFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择询价类型
function chooseInquiry(params){
	var basePath = params.basePath;
	var url = basePath+"/inquiry/chooseInquiry.do";
	var callBack = params.callBack ? params.callBack : "setInquiry";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseContractterms',
		title: '询价类型',
		content: '<iframe id="listDepartmentFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择验货类型
function chooseInspection(params){
	var basePath = params.basePath;
	var url = basePath+"/inspection/chooseInspection.do";
	var callBack = params.callBack ? params.callBack : "setInspection";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseInspection',
		title: '验货类型',
		content: '<iframe id="listInspection" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择组织结构类型
function chooseOrgStructure(params){
	var basePath = params.basePath;
	var url = basePath+"/orgstructure/chooseOrgStructure.do";
	var callBack = params.callBack ? params.callBack : "setOrgStructure";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseOrgStructure',
		title: '组织结构类型',
		content: '<iframe id="listOrgStructure" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择其他应付款
function chooseOtherpay(params){
	var basePath = params.basePath;
	var url = basePath+"/otherpay/chooseOtherpay.do";
	var callBack = params.callBack ? params.callBack : "setOtherpay";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseOtherpay',
		title: '其他应付款',
		content: '<iframe id="listOtherpay" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择税率设置
function chooseTax(params){
	var basePath = params.basePath;
	var url = basePath+"/taxRate/chooseTax.do";
	var callBack = params.callBack ? params.callBack : "setTax";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseTaxRate',
		title: '税率设置',
		content: '<iframe id="listTaxRate" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择招标类型
function chooseTender(params){
	var basePath = params.basePath;
	var url = basePath+"/tender/chooseTender.do";
	var callBack = params.callBack ? params.callBack : "setTender";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseTender',
		title: '招标类型',
		content: '<iframe id="listTender" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择单位类型
function chooseUnitCjmu(params){
	var basePath = params.basePath;
	var url = basePath+"/unitCjmu/chooseUnitCjmu.do";
	var callBack = params.callBack ? params.callBack : "setUnitCjmu";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseUnitCjmu',
		title: '单位类型',
		content: '<iframe id="listUnitCjmu" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//选择物资类别-授权
function chooseMaterialTypeSq(params){
	var basePath = params.basePath;
	var url = basePath+"/materialType/chooseMaterialTypeSq.do";
	var callBack = params.callBack ? params.callBack : "setMaterialTypeSq";
	url += ("?callBack="+callBack);
	url += ("&pk_check="+params.pk_check);
	url += ("&onlyFirm="+params.onlyFirm);//为"T"时只显示末级节点类型为门店的组织
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseMaterialType',
		title: params.title ? params.title : '物资类别授权',
		content: '<iframe id="listMaterialTypeSqFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择物资====来自商城的物资V2
function selectJmuMaterialV2(params){
	var basePath = params.basePath;
	var url = basePath+"/search/toMallMaterial.do";
	var callBack = params.callBack ? params.callBack : "setMaterial";
	url += ("?callBack="+callBack);
	url += ("&vname="+encodeURI(encodeURI(params.vname)));
	url += ("&vsuppliername="+encodeURI(encodeURI(params.vsuppliername)));
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseMallMaterial',
		title: '选择商城物资',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//选择角色
function chooseRole(params){
	var basePath = params.basePath;
	var url = basePath+"/role/chooseRole.do";
	var callBack = params.callBack ? params.callBack : "setRole";
	url += ("?callBack="+callBack);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseRole',
		title: '选择角色',
		content: '<iframe id="listUnitCjmu" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}

//翻页选择物资
function selectMaterialByPage(params){
	var basePath = params.basePath;
	var url = basePath+"/material/showSelectMaterialByPage.do";
	var callBack = params.callBack ? params.callBack : "setMaterialByPage";
	url += ("?callBack="+callBack);
	var irateflag = params.irateflag ? params.irateflag : "0";
	url += ("&irateflag="+irateflag);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';//父页面存储已选择的数据元素
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseMaterialPage',
		title: '选择物资',
		content: '<iframe id="chooseMaterialPage" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true,
        topBar: {
            items: [{
                text: '保存',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    position: ['-80px','-0px']
                },
                handler: function(){
                    getFrame('chooseMaterialPage').saveSelectMaterial();
                }
            },{
                text: '取消 ',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    position: ['-160px','-100px']
                },
                handler: function(){
                    $('.close').click();
                }
            }
            ]
        }
	});
}

//选择供应商类别
function chooseSupplierTypeTree(params){
	var basePath = params.basePath;
	var url = basePath+"/supplierType/chooseSupplierTypeTree.do";
	var callBack = params.callBack ? params.callBack : "setSupplierType";
	url += ("?callBack="+callBack);
	url += ("&pk_check="+params.pk_check);
	if(params.single) url += '&single=true';
	url += params.domId ? '&domId='+params.domId : '&domId=selected';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_chooseSupplierType',
		title: params.title ? params.title : '供应商类别',
		content: '<iframe id="listSupplierTypeTreeFrame" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
		height: params.height ? params.height : 500,
		confirmClose: false,
		draggable: true,
		isModal: true
	});
}
//===================================供应链采购助手
//选择物资-分页选择
function selectSupplyBatchAsst(params){
	var basePath = params.basePath;
	var url = basePath+"/material/addSupplyBatchAsst.do";
	var callBack = params.callBack ? params.callBack : "setMaterial";
	url += ("?callBack="+callBack);
	url += "&defaultCode="+(params.defaultCode ? params.defaultCode : "");
	url += "&type="+(params.type ? params.type : "");
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_addSupplyBatchAsst',
		title: '选择物资',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
				height: params.height ? params.height : 500,
						confirmClose: false,
						maximizable : false,
						draggable: true,
						isModal: true
	});
}
//选择物资-普通
function selectSupplyLeftAsst(params){
	var basePath = params.basePath;
	var url = basePath+"/material/selectSupplyLeftAsst.do";
	var callBack = params.callBack ? params.callBack : "setMaterial";
	url += ("?callBack="+callBack);
	url += "&defaultCode="+(params.defaultCode ? params.defaultCode : "");
	url += "&is_supply_x="+(params.is_supply_x ? params.is_supply_x : "");
	url += "&ex1="+(params.ex1 ? params.ex1 : "");
	url += "&deliver="+(params.code ? params.code : "");
	var irateflag = params.irateflag ? params.irateflag : "0";
	if(params.single) url += '&single=true';
	var data = params.param;
	for(var i in data){
		url += ('&'+i + '=' + data[i]);
	}
	return $('body').window({
		id: 'window_addSupplyBatchAsst',
		title: '选择物资',
		content: '<iframe id="listForm" frameborder="0" src='+url+'></iframe>',
		width: params.width ? params.width : 500,
				height: params.height ? params.height : 500,
						confirmClose: false,
						maximizable : false,
						draggable: true,
						isModal: true
	});
}
