var queryParams = {};		
var tableHeight = 0;//充满div时，表格高度		
var bodyHeight = 0;//iframe body高度		
var firstLoad = true;//标记是否为第一次加载		
var colChooseWindow;		
var fieldMap = {};		
//方法延时执行		
var delay = function(t,func){		
	var self = this;		
	if(self.curTime)		
		clearTimeout(self.curTime);		
	self.curTime = setTimeout(function(){		
		func.apply(self);		
		},t*1000);		
};		
//生成工具栏		
function builtToolBar(params){		
	var form = $('#'+params.formId);//页面formid		
	var grid = params.gridId ? $('#'+params.gridId) : params.grid;//表格所在div		
	var basePath = params.basePath;		
	var curtoolbar = params.toolbar;//需要的工具按钮,可能的值search,excel,print,option,exit		
	var searchFun = params.searchFun; //自定义查询方法		
	var verifyFun = params.verifyFun;		
	var exportTyp = params.exportTyp;//excel导出时获取表头的方式，默认为从数据库查询。设置为true时从页面获取		
	var items = [];		
	if(grid)grid.data("verifyFun",verifyFun);		
	var toolbar = {search:{		
		text: $.messager.defaults.search,		
		title: $.messager.defaults.search,		
		useable:report_search_perm_tele === undefined ? true : report_search_perm_tele,		
		icon: {		
			url: basePath+'/image/Button/op_owner.gif',		
			position: ['0px','-40px']		
		},		
		handler: function(){		
			delay(0.5,function(){		
				if(!(verifyFun ? verifyFun() : true))return;		
				searchFun ? searchFun(grid,form) :		
				grid.datagrid("load",getParam(form));		
			});		
		}		
	},		
	excel:{		
		text: $.messager.defaults.excel,		
		title: $.messager.defaults.excel,		
		useable:report_export_perm_tele === undefined ? true : report_export_perm_tele,		
		icon: {		
			url: basePath+'/image/Button/op_owner.gif',		
			position: ['-40px','-20px']		
		},		
		handler: function(){		
			if(!(verifyFun ? verifyFun() : true))return;	
			$("#wait2").css("visibility", "visible");
			$("#wait").css("visibility", "visible");
			var headers = [];		
			if(exportTyp){		
				var panel = grid.datagrid('getPanel');		
				var content = panel.panel('body');		
				function clearHead(head){		
					head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');		
					head.find('td').each(function(){		
						if($(this).css('display') == 'none'){		
							$(this).remove();		
						}else{		
							$(this).removeAttr('class');		
							$(this).children('div').html($.trim($(this).text()));		
							$(this).children('div').removeAttr('class');		
						}		
					});		
					return head.html();		
				}		
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));		
				headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));		
				headers.push("<fieldMap>"+$.toJSON(fieldMap)+"</fieldMap>");		
			}		
			headers = headers.join("");		
			var rs = headers.match(/\w+\s*=\w+/g);		
			for(var s in rs){		
				var string = String(rs[s]);		
				string.match(/(\w+)$/g);		
				headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));		
			}		
			var head = $("<input type='hidden' name='headers'/>");		
			form.find("input[name='headers']").remove();		
			head.val(headers.replace(/\r\n/g,""));		
			head.appendTo(form);		
			form.attr('action',params.excelUrl);		
			form.submit();		
			delay(1,function(){		
				$("#wait2").css("visibility","hidden");		
	 			$("#wait").css("visibility","hidden");		
			});		
		}		
	},		
	print:{		
		text: $.messager.defaults.print,		
		title: $.messager.defaults.print,		
		useable:report_print_perm_tele === undefined ? true : report_print_perm_tele,		
		icon: {		
			url: basePath+'/image/Button/op_owner.gif',		
			position: ['-140px','-100px']		
		},		
		handler: function(){		
			if(!(verifyFun ? verifyFun() : true))return;		
			form.attr('target','report');		
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');		
			var action=params.printUrl;		
			form.attr('action',action);		
			form.submit();		
		}		
	},		
	option:{		
		text: $.messager.defaults.option,		
		title: $.messager.defaults.option,		
		useable:report_option_perm_tele === undefined ? true : report_option_perm_tele,		
		icon: {		
			url: basePath+'/image/Button/op_owner.gif',		
			position: ['-100px','-60px']		
		},		
		handler: function(){		
			toColsChoose(params.colsChooseUrl);		
		}		
	},		
	exit:{		
		text: $.messager.defaults.exit,		
		title: $.messager.defaults.exit,		
		useable:true,		
		icon: {		
			url: basePath+'/image/Button/op_owner.gif',		
			position: ['-160px','-100px']		
		},		
		handler: function(){		
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));		
		}		
	}		
	};		
			
	for(var i in curtoolbar){		
		if(typeof(curtoolbar[i]) == 'string')		
			items.push(toolbar[curtoolbar[i]]);		
		else		
			items.push(curtoolbar[i]);		
	}		
	$('#'+params.toolbarId).html('');		
	$('#'+params.toolbarId).toolbar({		
		items:items		
	});		
	bodyHeight = $(".layout-panel-center",top.document).children('div[region="center"]').height() - $(".tab-control",top.document).height();		
	tableHeight = bodyHeight - $("#tool").height() - $("#queryForm").height() - $(".tabs-header").height();		
	$('body').height(bodyHeight);		
}		
		
//解析获取表单数据		
		
function getParam(form){		
	form = form.find("*[name]").filter(function(){		
		return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val() 		
		&& !$(this).attr('disabled');		
	});		
	var mul = ['radio','checkbox'];		
	var temp = {};		
	var param = {};		
	form.each(function(){		
		this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();		
	});		
	for(var i in temp){		
		$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()		
				: param[i] = form.filter('input[name="'+i+'"]:checked').val();		
	}		
	queryParams = param;		
	return param;		
}		
		
//跳转到<fmt:message key="column_selection" />页面		
function toColsChoose(url){		
	colChooseWindow = $('body').window({		
		title: 'Column Selection',		
		content: '<iframe frameborder="0" src='+url+'></iframe>',		
		width: '460px',		
		height: '430px',		
		draggable: true,		
		isModal: true,		
		confirmClose: false		
	});		
}		
		
function closeColChooseWin(){		
	if(colChooseWindow)		
		colChooseWindow.close();		
}		
//生成表格		
function builtTable(params){		
	var headUrl = params.headUrl;//获取表头的url		
	var width = params.width ? params.width : '100%';		
	var contentUrl = params.dataUrl;//获取表格内容的url		
	var remoteSort = String(params.remoteSort) != 'undefined' ? params.pagination : true;		
	var title = params.title;//表格title		
	var grid = params.id ? $('#'+params.id) : params.grid;//表格所在div		
	var dateCols = params.dateCols ? params.dateCols.join(',').toLowerCase().split(',') : [];//需要按日期格式化的数据		
	var timeCols = params.timeCols ? params.timeCols.join(',').toLowerCase().split(',') : [];//需要按时间格式化的数据		
	var numCols = params.numCols ? params.numCols.join(',').toLowerCase().split(',') : [];//需要按数字格式化的数据		
	var alignCols = params.alignCols ? params.alignCols.join(',').toLowerCase().split(',') : [];//需要右对齐的列		
	var filter = typeof(params.filter) == 'function' ? params.filter : function(data){return data;};//对获取的数据进行格式化的方法		
	var singleSelect = String(params.singleSelect) != 'undefined' ? params.singleSelect : true;//是否单选		
	var pagination = String(params.pagination) != 'undefined' ? params.pagination : true;//是否显示分页工具条		
	var showFooter = String(params.showFooter) != 'undefined' ? params.showFooter : true;//是否显示页脚栏		
	var pageList = params.pageList ? params.pageList : [20,30,40,50];//定义分页数目		
	var gridHeight = params.height ? params.height : tableHeight;//表格高度		
	var onClickRow = params.onClickRow ? params.onClickRow : function(a,b){return;};//表格行单击事件，第一参数为行号，第二参数为改行数据json格式，{field:data}		
	var onDblClickRow = params.onDblClickRow ? params.onDblClickRow : function(a,b){return;};		
	var decimalDigitR = params.decimalDigitR ? Number(params.decimalDigitR) : 2;		
	var decimalDigitF = params.decimalDigitF ? Number(params.decimalDigitF) : 2;		
	var createHeader = params.createHeader ? params.createHeader : undefined; 		
	var hiddenCols = params.hiddenCols ? params.hiddenCols : undefined;		
	//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）		
	var tableContent = {};		
	//表头行（单行）		
	var columns = [];		
	//表头（多行），其中元素为columns		
	var head = [];		
	//需要固定在左侧的列的表头（单行）		
	var frozenHead = [];		
	//需要固定在左侧的列的表头（多行），元素为frozenHead		
	var frozenColumns = [];		
	//ajax获取报表表头		
	if(headUrl)		
		$.ajax({url:headUrl,		
				async:false,		
//				data:queryParams,		
				type:'POST',		
				success:function(data){		
					tableContent = data;		
				}		
			});		
	//解析获取的数据		
	if(!createHeader){		
		alignCols = alignCols.concat(numCols);		
		var frozenIndex = tableContent.frozenColumns ? tableContent.frozenColumns.split(',') : [];		
			var Cols = [];		
			var colsSecond = [];		
		var prev = '';		
		var temp;		
			for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);		
			var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);		
			if(t && !t.length){		
				for(var i in tableContent.columns){		
					if(!tableContent.columns[i].properties)continue;		
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 		
		 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)		
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});		
		 			else		
		 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});		
		 		}		
			head.push(columns);		
		 		frozenHead.push(frozenColumns);		
			}else{		
				for(var i in tableContent.columns){		
					if(!tableContent.columns[i].properties)continue;		
					var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),alignCols) >= 0 ? "right" : "left"; 		
					if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)		
		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});		
					else{		
						var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);		
						if(cur && cur.length){		
							var cur = tableContent.columns[i].zhColumnName;		
							if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){		
								temp.colspan ++;		
							}else{		
								temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};		
								columns.push(temp);		
								prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");		
							}		
							colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});		
						}else{		
							if(tableContent.columns[i].columnName)		
								columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});		
						}		
					}		
				}		
				head.push(columns);		
				head.push(colsSecond);		
				if(hiddenCols){		
					for(var obj in hiddenCols)		
						head[0].push({field:hiddenCols[obj].field,rowspan:head.length,hidden:true});		
				}		
				frozenHead.push(frozenColumns);		
			}		
	}else {		
		createHeader(tableContent,head,frozenHead);		
	}		
			
 		//生成报表数据表格		
		grid.datagrid({		
	 			title:title,		
	 			width:width,		
	 			height:gridHeight,		
	 			nowrap: true,		
				striped: true,		
				singleSelect:singleSelect,		
				collapsible:true,		
				//对从服务器获取的数据进行解析格式化selectPrintset		
				dataFilter:function(data,type){		
					var rs = eval("("+data+")");		
					if(createHeader)return $.toJSON(filter(rs,head));		
					var modifyRows = [];		
					var modifyFooter = [];		
					var footer = rs.footer;		
					var rows = rs.rows;		
					if(!rows || rows.length <= 0)grid.datagrid('loadData',{total:0,rows:[],footer:[]});		
					for(var i in rows){		
						var cols = tableContent.columns;		
						var curRow = {};		
						for(var j in cols){		
							try{		
								var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());		
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');		
								//-------------------------		
								if($.inArray(cols[j].properties.toLowerCase(),numCols) >=0){		
									value = value ? Number($.trim(String(value))).toFixed(decimalDigitR) : '0.00';		
								}else if(String(value).match(/0|(?:.+)/)){		
									if($.inArray(cols[j].properties.toLowerCase(),dateCols) >= 0){		
										value = convertDate(value,false);		
									}else if($.inArray(cols[j].properties.toLowerCase(),timeCols) >= 0){		
										value = convertDate(value,true);		
									}		
								}else{		
									value='';		
								}		
								//-----------------------------		
								curRow[cols[j].columnName.toUpperCase()] = value;		
								fieldMap[cols[j].columnName.toUpperCase()] = cols[j].properties.toUpperCase();		
							}catch(e){		
								console.warn('Exception   '+"rows["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);		
							}		
						}		
						if(hiddenCols){		
							for(var obj in hiddenCols)		
								curRow[hiddenCols[obj].field] = eval("rows["+i+"]."+hiddenCols[obj].field);		
						}		
						modifyRows.push(curRow);		
					}		
					rs.rows = modifyRows;		
					for(var i in footer){		
						var cols = tableContent.columns;		
						var foot = {};		
						for(var j in cols){		
							try{		
								var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;		
								//value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');		
								value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? Number($.trim(String(value))).toFixed(decimalDigitF) : '0.00') : (String(value).match(/0|(?:.+)/) ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');		
								foot[cols[j].columnName.toUpperCase()] = value;		
							}catch(e){		
								console.warn('Exception   '+"footer["+i+"]."+cols[j].properties+"====>"+cols[i].zhColumnName);		
							}		
						}		
						modifyFooter.push(foot);		
					}		
					rs.footer = modifyFooter;		
					rs = filter(rs);		
					return $.toJSON(rs);		
					function convertDate(time,flag){		
						if(isNaN(time)){		
							return;		
						}		
						var date=new Date(time); 		
						var str="";     		
						str+=date.getFullYear()+"-";     		
						str+=((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-";     		
						str+=date.getDate()>9?date.getDate():"0"+date.getDate();		
						if(flag){		
							str += " "+((date.getHours()>9)?date.getHours():"0"+date.getHours())+":";		
							str += ((date.getMinutes()>9)?date.getMinutes():"0"+date.getMinutes())+":";		
							str += ((date.getSeconds()>9)?date.getSeconds():"0"+date.getSeconds());		
						}		
						return str;		
					}		
				},		
				url:contentUrl,		
				remoteSort: remoteSort,		
				//页码选择项		
				pageList:pageList,		
				frozenColumns:frozenHead,		
				columns:head,		
				queryParams:queryParams,		
				showFooter:showFooter,		
				pagination:pagination,		
				rownumbers:true,		
				fitColumns:false,		
				onClickRow:onClickRow,		
				onDblClickRow:onDblClickRow,		
				rowStyler:function(){		
					return 'line-height:11px';		
				},		
				onBeforeLoad:function(){		
					if(firstLoad){		
						firstLoad = false;		
						return false;		
					}		
					if(grid.data("verifyFun") && typeof(grid.data("verifyFun")) == "function"){		
						return grid.data("verifyFun")();		
					}		
				},		
				onLoadSuccess:function(data){		
					if(data.total==0){		
						initone(data);		
					}		
				}		
	 	});		
	 	$(".panel-tool").remove();		
}		
		
function initone(data){		
	if($("tr[datagrid-row-index='0']").length<=0){//首次进入报表页面，说明没有数据，就进行添加空行		
		$('#datagrid').datagrid('insertRow',{		
			row: {		
			}		
		});		
		$("tr[datagrid-row-index='0']").css({"visibility":"hidden"});		
	}else{		
		//否则进行查询操作		
		if($('.datagrid-body tbody tr').length==0){ //如果返回0条数据，就添加一行空数据		
			$('#datagrid').datagrid('insertRow',{		
				row: {		
				}		
			});		
			$("tr[datagrid-row-index='0']").css({"visibility":"hidden"});		
		}		
	}		
}		
//新建图表(免费版)		
function buildChart(params){		
	var swf = params.swf;//展示图表的swf文件路径		
	var url = params.url;//生成图表是获取的xml数据的url		
	var id = params.id ? params.id : 'myChartId';//生成图表的id		
	var width = params.width ? params.width : $('body').width();//生成图表的宽度		
	var height = params.height ? params.height : tableHeight;//生成图表的高度		
	var form = params.form;//从form中获取需要提交的参数，指定form		
	var div = params.div;//生成的图表所在的div		
	var para = getParam(form);//提交url时，需附加的参数		
	var myChart = new FusionCharts(swf,id,width,height);		
	//myChart.addParam("wmode","Opaque");//控制flash显示，防止chart显示在最上层		
	$.ajax({		
		url:url,		
		data:para,		
		type:'POST',		
		beforeSend:function(){		
//			$('#wait2,#wait').css("visibility","visible");		
		},		
		success:function(data){		
 			myChart.setDataXML(data);		
 			myChart.render(div);		
		},		
		complete:function(){		
			$('#wait2,#wait').css("visibility","hidden");		
		}		
	});		
}		
//select stores		
function selectFirm(params){		
	var basePath = params.basePath;		
	var url = basePath+"/firm/toChooseFirm.do";		
	var callBack = params.callBack ? params.callBack : "setFirm";		
	url += ("?callBack="+callBack);		
	if(params.single) url += '&single=true';		
	url += params.domId ? '&domId='+params.domId : '&domId=selected';		
	var data = params.param;		
	for(var i in data){		
		url += ('&'+i + '=' + data[i]);		
	}		
	return $('body').window({		
		id: 'window_chooseFirm',		
		title: 'select stores',		
		content: '<iframe id="listFrimFrame" frameborder="0" src='+url+'></iframe>',		
		width: params.width ? params.width : 500,		
		height: params.height ? params.height : 500,		
		confirmClose: false,		
		draggable: true,		
		isModal: true		
	});		
}		
