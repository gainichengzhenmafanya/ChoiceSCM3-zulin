/**
 * Created by mc on 14-11-9.
 */
var valerrorbox=function(id,msg){
    valObj = $("#"+id);
    var msgDiv = $('<div class="validateMsg"></div>');
    msgDiv.css({
        'top': valObj.position().top + 3,
        'left': valObj.position().left,
        'height': valObj.height() - 2,
        'width': valObj.width() - 4,
        'line-height': valObj.height() + 'px'
    }).html(msg)
    .insertAfter(valObj)
    .bind('click.validate', function () {
        $(this).prev().focus();
        $(this).siblings('.validateMsg').remove();
        $(this).remove();
    });
};
var errorbox=function(valObj,msg){
    var msgDiv = $('<div class="validateMsg"></div>');
    msgDiv.css({
        'top': valObj.position().top + 3,
        'left': valObj.position().left,
        'height': valObj.height() - 2,
        'width': valObj.width() - 4,
        'line-height': valObj.height() + 'px'
    }).html(msg)
        .insertAfter(valObj)
        .bind('click.validate', function () {
            $(this).prev().focus();
            $(this).siblings('.validateMsg').remove();
            $(this).remove();
        });
};
var codeval=function(params,codeMark,code,parent,lvl){
    var action=params.action+"/coderule/format.do";
    var data={};
    data.codeMark=$.trim(codeMark);
    data.code=$.trim(code);
    data.parent=$.trim(parent);
    data.lvl=$.trim(lvl);
    $.ajax({
        url:action,
        async:false,
        data:data,
        success:function(msg){
            window[params.callback](msg);
        }
    });
};
var orderval=function(params,codeMark,code){
    var action=params.action+"/coderule/codeformat.do";
    var data={};
    data.codeMark=$.trim(codeMark);
    data.code=$.trim(code);
    $.ajax({
        url:action,
        data:data,
        success:function(msg){
            window[params.callback](msg);
        }
    });
};

var getCode=function(params,codeMark,typecode,parentcode,lvl){
    var action=params.action+"/coderule/getCode.do";
    var data={};
    data.pk_coderule=$.trim(codeMark);
    data.typecode= $.trim(typecode);
    data.parentcode=$.trim(parentcode);
    data.lvl=$.trim(lvl);
    $.ajaxSetup({ cache: false });
    $.ajax({
        url:action,
        async:false,
        data:data,
        success:function(msg){
            window[params.callback](msg);
        }
    });
};