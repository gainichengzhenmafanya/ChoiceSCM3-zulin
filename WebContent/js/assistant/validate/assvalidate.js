(function($){
	$.fn.CommonMethod = function(){
		return this.each(function() {
			var commonMethod = new CommonMethod();
		});
	};
	
	CommonMethod= function(options,container){
		//初始化按钮
		this._init();
	};
	
	CommonMethod.prototype = {
		_init:function(){
			this.vcode,
			this.vcodename,
			this.vtablename,
			this.isortno;
			this.enablestate;
			this.ids;
			this.pk_id;
			this.pk_store;
			this.modulename,
			this.params,
			this.parent,
			this.parentVal;
		}
	};
})(jQuery);

var savelock = false;
//编码、名称等的重复验证
function validateVcode(vpath,common){
	if(savelock){
		return;
	}else{
		savelock = true;
	}
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 0;
	
	var data = {};
	data["vcode"] = common.vcode;
	data["vcodename"] = common.vcodename;
	data["vtablename"] = common.vtablename;
	data["pk_store"] = common.pk_store;
	data["pk_id"] = common.pk_id;
	data["params"] = common.params;
	data["ids"] = common.ids;
	data["parent"] = common.parent;
	data["parentVal"] = common.parentVal;
	$.post(vpath + "/commonMethod/validateVcode.do",data,function(returndata){
		if (returndata != null && returndata > 0) {
			returnvalue = 1;
		} else if (returndata != null && returndata == "error") {
			returnvalue = -1;
		} else {
			returnvalue = 0;
		}
		savelock = false;
	});
	return returnvalue;
}

//获取排序号
function getSortNo(vpath,common){
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 1;
	
	var data = {};
	data["isortno"] = common.isortno;
	data["vtablename"] = common.vtablename;
	data["params"] = common.params;
	$.post(vpath + "/commonMethod/getSortNo.do",data,function(returndata){
		if (returndata!=null && returndata!="error" && returndata!="") {
			returnvalue = returndata;
		}
	});
	return returnvalue;
}


//排序号重复验证
function validateSortNo(vpath,common){
	$.ajaxSetup({ 
		  async: false 
	});
	 
	var returnvalue ;
	var data = {};
	data["isortno"] = common.isortno;
	data["vtablename"] = common.vtablename;
	data["visortnoName"] = common.visortnoName;
	data["pk_id"]=common.pk_id;
	data["pk_id_name"]=common.pk_id_name;
	$.post(vpath + "/commonMethod/validateSortNo.do",data,function(returndata){
		returnvalue=returndata;
	});
	return returnvalue;
}

//设置是否启用
function IsEnableFiled(vpath,common){
	$.ajaxSetup({ 
		  async: false 
  	});
	
	var returnvalue = 0;
	
	var data = {};
	data["enablestate"] = common.enablestate;
	data["vtablename"] = common.vtablename;
	data["pk_id"]=common.pk_id;
	data["ids"]=common.ids;
	data['modulename']=common.modulename;
	data['vcode']=common.vcode;
	
	$.post(vpath + "/commonMethod/IsEnable.do",data,function(returndata){
		if (returndata!=null && returndata!="error") {
			returnvalue = returndata;
		}
	});
	return returnvalue;
}

//特殊字符验证
function checkSpecilWord(objvalue){
	var regArray=new Array("`","~","!","@","#","$","^","&","*","=","|","{","}","'","%",":","；",
			"'",",","\\","\\\\","[","\\","\\\\","]",".","<",">","?","！","@","#","￥","…","…",
			"&","*","&",";","【","】","‘","’","；","：","”","“","。","，","、","？");
	var len = regArray.length;
	for(var iu=0;iu<len;iu++){
        if (objvalue.indexOf(regArray[iu])!=-1){
			return false;
        }
	}
	return true;
}

//验证该数据是否被引用,tablename:引用的表，field:引用的字段，value:被引用的值
function checkQuote(vpath,tablename,field,value,enablestate){
	$.ajaxSetup({ 
		  async: false 
  	});
	var returnvalue = 0;
	var data = {};
	data["vcode"] = value;
	data["vcodename"] = field;
	data["vtablename"] = tablename;
	data["enablestate"] = enablestate;
	$.post(vpath + "/commonMethod/checkQuote.do",data,function(returndata){
		if (returndata != null && returndata > 0) {
			returnvalue = 1;
		} else if (returndata != null && returndata == "error") {
			returnvalue = -1;
		} else {
			returnvalue = 0;
		}
	});
	return returnvalue;
}

function checkMultiCodeQuote(vpath,tablename,field,value,enablestate){
	$.ajaxSetup({ 
		  async: false 
  	});
	var returnvalue = 0;
	var data = {};
	data["vcode"] = value;
	data["vcodename"] = field;
	data["vtablename"] = tablename;
	data["enablestate"] = enablestate;
	$.post(vpath + "/commonMethod/checkMultiCodeQuote.do",data,function(returndata){
		if (returndata != null && returndata > 0) {
			returnvalue = 1;
		} else if (returndata != null && returndata == "error") {
			returnvalue = -1;
		} else {
			returnvalue = 0;
		}
	});
	return returnvalue;
}
