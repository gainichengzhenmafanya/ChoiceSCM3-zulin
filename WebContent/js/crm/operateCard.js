var cardExist = true;
var stateCode = 0;
/**
 * 打开读卡器串口
 */
function openCard(){
	MWRFATL.OpenReader();
	if(MWRFATL.LastRet!=0){
		//alert('USB串口初始化错误！');
		stateCode = 1;
		cardExist = false;
		return;
	}else{
		stateCode = 0;
		cardExist = true;
	}
	MWRFATL.OpenCard(1);//寻卡，读出卡号
	if(MWRFATL.LastRet!=0){
		stateCode = 2;
		cardExist = false;
	}else{
		stateCode = 0;
		cardExist = true;
	}
}

/**
 * 装载验证密码
 * @param key 密码
 */
function loadKey(key){
	MWRFATL.RF_LoadKey(0,13,key);//装载密码
	MWRFATL.RF_Authentication(0,13);//验证密码
	if(MWRFATL.LastRet!=0){
		alert('请确认是本公司会员卡！');
		cardExist = false;
		try{
			clearInterval(timer);
		}catch(e){
			
		}
		closeCard();
		invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
	}else{
		try{
			clearInterval(timer);
		}catch(e){
			
		}
		cardExist = true;
	}
}

/**
 * 读卡
 */
function readCard(){
	return MWRFATL.MF_Read(52)+'@' + MWRFATL.MF_Read(53)+'@' + MWRFATL.MF_Read(54);
}

/**
 * 写卡
 * @param sector 要写的块
 * @param info 内容
 */
function writeCard(sector,info){
	MWRFATL.MF_Write(sector, info);
}

/**
 * 控制读卡器蜂鸣
 * @param time 蜂鸣时间ms
 */
function beepCard(time){
	MWRFATL.RF_Beep(time);//蜂鸣时间
}

/**
 * 关闭卡并关闭读卡器串口
 */
function closeCard(){
	MWRFATL.CloseCard();
	MWRFATL.CloseReader();
}