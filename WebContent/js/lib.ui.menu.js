
function MenuView(config){
	this.items = config.items || [];
	this.ids = '';
	
	//根节点编号
	this.rootId = '00000000000000000000000000000000';
	
	//存放菜单的容器(即：菜单的父元素)
	this.container = typeof config.container === 'string' ? $('#' + config.container) : config.container;
	
	this.create();
}

MenuView.prototype = {
	create: function(){
		var self = this;
		
		//组建菜单
		var _menuHTML = [];
		_menuHTML.push('<div class="menubar" id="menubar">');
		_menuHTML.push('	<ul class="menubar-main-ul"></ul>');
		_menuHTML.push('</div>');
		
		var _doc = self._doc = $(_menuHTML.join(''));
		self._doc.appendTo(self.container);

		var _menubar = self._menubar = $('#menubar');
		self._mbar_main_ul = $('.menubar-main-ul',_doc);

		//将数组节点转换成字符串，使用“,”隔开
		//转换后的字符串样式:		parentId_selfId,parentId_selfId
		var idArr = [];
		for(var i in self.items){
			idArr.push(i);
		}
		self.ids = idArr.join(',');
		
		var info = self._getEachMenuId(self.rootId);
		
		if($.isArray(info) && info.length > 0){
			for(var i = 0, len = info.length; i < len; i++){
				var item = info[i],
					parentId = item.split('_')[1],
					_li = $('<li class="menubar-main-li"></li>').appendTo(self._mbar_main_ul);
	
				//加载菜单的内容。
				self._addItem(self.items[item],_li);					
	
				//加载子菜单的内容
				self._loadChildItems(self._getEachMenuId(parentId),_li,1);
			}
		}
		
		self._bindEvent();
	},
	
	/**
	 * 获取所有以parentId开始，32位字母或者数字组成的字符串结尾，中间使用下划线分割的字符串。
	 * 即：匹配【parentId_selfId】
	 * @param parentId
	 * @returns
	 */
	_getEachMenuId: function(parentId){
		var self = this;
		var reg = new RegExp(parentId+"[_][a-zA-Z0-9]{32}","g");
		return self.ids.match(reg);
	},
	
	/**
	 *	private
	 *
	 *	加载子菜单的内容
	 *
	 */
	_loadChildItems: function(items,currentLi,step){
		var self = this;
		step++;
		if(items && $.isArray(items) && items.length > 0){
			var _child_ul;
			if(step==3){
				 _child_ul = $('<ul class="menubar-child-ul  ul2"></ul>').appendTo(currentLi);
			}else{
				 _child_ul = $('<ul class="menubar-child-ul"></ul>').appendTo(currentLi);
			}

			for(var i = 0, len = items.length; i < len; i++){

				var item = items[i],
					parentId = item.split('_')[1],
					_li = $('<li class="menubar-child-li"></li>').appendTo(_child_ul),
					childItems = self._getEachMenuId(parentId),
					hasHandler = true;
				
				if($.isArray(childItems) && childItems.length > 0)
					hasHandler = false;
				
				//将item添加到菜单中。
				self._addItem(self.items[item],_li,hasHandler);
				
				//加载item的子菜单内容
				self._loadChildItems(childItems,_li,step);
			}
		}
	},

	/**
	 *	private
	 *
	 *	增加工具条的内容
	 *
	 *	@param	item {array} 需要添加的内容
	 *			container {jQuery Object} 内容所属的容器
	 *
	 */
	_addItem: function(item,container,hasHandler){
		var self = this,
			_btn = null;
		$.extend(item,{'scale':'medium'});
		if(!hasHandler){
			$.extend(item,{'handler':$.noop});
		}
		if(container.hasClass('menubar-main-li')){
		//	$.extend(item,{'icon':{'url':'../image/collapse_arrow.png'}});
		//子主菜单的内容添加样式
		}else if(!hasHandler && container.hasClass('menubar-child-li')){
			container.addClass('li2');
		//	$.extend(item,{'icon':{'url':'../image/L2.gif'}});
		}
		
		if($.isPlainObject(item) && !$.isEmptyObject(item)){
//			_btn = container.button(item);
			if(item.handler.toString().substr(0,4)=='show'){
				_btn = '<div href="javascript:void(0)"  onclick='+item.handler.replace(/[ ]/g, "_")+' title='+item.text+' class="button button_now" style="position: relative; top: 0px; left: 0px; height: 26px; width: 189px; border: 0px;"><a>'+item.text+'</a></div>';			
			}else{
				_btn = '<div title='+item.text+' class="button button_now" style="position: relative; top: 0px; left: 0px; height: 26px; width: 189px; border: 0px;"><a>'+item.text+'</a></div>';						
			}
			container.append(_btn);
		}
		
//		_btn._button.css({
//			'border': '0px'
//		});
		
//		//为主菜单的内容添加样式
//		if(container.hasClass('menubar-main-li')){
//			_btn.setPosition({
//				type: 'relative'
//			}).setWidth(container.width())
//			.setToggle(true);
//		//子主菜单的内容添加样式
//		}else if(container.hasClass('menubar-child-li')){
//			_btn.setPosition({
//				type: 'relative'
//			}).setWidth(container.width());
//		}

	},
	/**
	 *	private
	 *
	 *	为工具条绑定事件
	 *
	 */
	_bindEvent: function(){
		var self = this,
			ul = null;

		//设置子菜单的display，初始状态为隐藏
		self._mbar_main_ul
			.find('.menubar-child-ul')
			.css('display','none');
		
		//给子菜单绑定mouseover,mouseout事件
		self._mbar_main_ul
		.find('.menubar-main-li')
		.bind('click',function(){
			var _self = $(this),
			_$child_ul = _self.children('.menubar-child-ul');
			
			if(_$child_ul.length === 0){
				return false;
			}
			
			_$child_ul.toggle();
			
			return false;
			
		});
		self._mbar_main_ul
			.find('.menubar-child-li')
			.bind('click',function(){
				var _self = $(this),
					_$child_ul = _self.children('.menubar-child-ul');
				_$child_ul.find('.button-icon medium left').css('background','../image/L233.gif');
				if(_$child_ul.length === 0){
					return false;
				}
				
				_$child_ul.toggle();
				
				return false;
				
			});
		self._mbar_main_ul.find('.li2').toggle(
		  function () {
		    $(this).css('background','transparent url(../image/tree/M3.gif) no-repeat 0 5px');
		  },
		  function () {
		    $(this).css('background','transparent url(../image/tree/P3.gif) no-repeat 0 5px');
		  }
		);
	}
};
