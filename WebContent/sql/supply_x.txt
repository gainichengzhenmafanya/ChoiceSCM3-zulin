
--虚拟物料
alter table supply add sp_code_x varchar2(10);
alter table supply add sp_name_x varchar2(50);
alter table supply add (YN_X char(1) default 'N' not null);

INSERT INTO dict_columns_bs (ID,TABLE_NAME,COLUMN_NAME,ZH_COLUMN_NAME,PROPERTIES,
COLUMN_WIDTH,COLUMN_TYPE) VALUES ('1047','supply','sp_code_x','虚拟物资编码','sp_code_x','100','text');

INSERT INTO dict_columns_bs (ID,TABLE_NAME,COLUMN_NAME,ZH_COLUMN_NAME,PROPERTIES,
COLUMN_WIDTH,COLUMN_TYPE) VALUES ('1048','supply','sp_name_x','虚拟物资名称','sp_name_x','100','text');

update  supply  set sp_code_x = sp_code ,  sp_name_x = sp_name ;

create table CHKSTOM_x
(
  acct     VARCHAR2(4) not null,
  yearr    VARCHAR2(4) not null,
  chkstono INTEGER not null,
  maded    DATE,
  madet    VARCHAR2(20),
  checd    DATE,
  chect    DATE,
  madeby   VARCHAR2(10),
  checby   VARCHAR2(10),
  vouno    VARCHAR2(20),
  dept     VARCHAR2(20),
  firm     VARCHAR2(10),
  positn   VARCHAR2(10),
  totalamt FLOAT default 0,
  status   VARCHAR2(10),
  build    CHAR(1) default 'N',
  bak1     VARCHAR2(10),
  bak2     INTEGER default 0,
  chectim  VARCHAR2(20)
);

create table CHKSTOD_x
(
  acct       VARCHAR2(4) not null,
  yearr      VARCHAR2(4) not null,
  id         INTEGER not null,
  chkstono   INTEGER default 0,
  sp_code    VARCHAR2(10),
  price      FLOAT default 0,
  amount     FLOAT default 0,
  hoped      VARCHAR2(40),
  status     VARCHAR2(10),
  memo       VARCHAR2(50),
  bak1       VARCHAR2(10),
  deliver    VARCHAR2(10),
  bak2       INTEGER,
  amountin   FLOAT default 0,
  ind        DATE,
  pricein    FLOAT default 0,
  chkin      CHAR(1) default 'N',
  dued       DATE,
  chkout     CHAR(1) default 'N',
  chkinout   CHAR(1) default 'N',
  chksend    CHAR(1) default 'N',
  inout      VARCHAR2(4) default 'N',
  pound      FLOAT default 0,
  sp_id      INTEGER default 0,
  sta        CHAR(1) default 'N',
  memo1      VARCHAR2(20),
  totalamt   FLOAT default 0,
  amount1    FLOAT default 0,
  amount1in  FLOAT default 0,
  chksta     CHAR(1) default 'N',
  empnam     VARCHAR2(20) default '',
  amountbla  FLOAT default 0,
  positn     VARCHAR2(10),
  chk1emp    VARCHAR2(20),
  chk1tim    VARCHAR2(20),
  chk1       CHAR(1) default 'N',
  chk2emp    VARCHAR2(20),
  chk2tim    VARCHAR2(20),
  prncnt     INTEGER default 0,
  amountsto  FLOAT default 0,
  amount1sto FLOAT default 0,
  pricesto   FLOAT default 0,
  empfirm    VARCHAR2(20),
  chkyh      CHAR(1) default 'N',
  psorder    INTEGER default 1,
  ynpsorder  VARCHAR2(1) default 'N',
  pricesale  FLOAT default 0
);

ADD_CHKSTOD_X   ADD_CHKSTOM_X    EDIT_CHKSTOM_X 
