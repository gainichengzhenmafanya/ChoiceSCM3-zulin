<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>我的工作台</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/view/tableMain.css" />
			
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			
			<style type="text/css">
				.table-body{
					height:190px;
    					overflow-x: hidden;
    					overflow-y: auto;
				}
				.grid {
					position : static;
				}
				.refresh{
/* 					visibility: hidden; */
					color: blue;cursor: pointer;
					margin:5px auto -3px 20px;
				}
				body{
					height: 95%;
				}
			</style>
		</head>
		
	<body>
		<div class="refresh" style="width:50%;float:left;"><fmt:message key="refresh" /><fmt:message key="my-desktop" />>>></div>
		<div id="message" style="width:40%;float:right;text-align:right;padding-right:30px;"><span style="cursor: pointer;text-decoration: underline;" onmousemove="$(this).css('color','blue')" onmouseout="$(this).css('color','black')" onclick="showInfo('62d1a702b589499abd370d98a9643caa','<fmt:message key="my_desktop"/>','/mainInfo/open.do')"><fmt:message key="scm_set_up_the" /><fmt:message key="my-desktop" />>>></span></div>
		<div id="m_1_1" class="module" style="height: 150px;display: none;">
			<h4 class="moduleHeader">
			    <span class="title" ><fmt:message key="my_remind"/></span>
			</h4>
			<div>
				<ul style="height:100px;">
					<c:choose>
						<c:when test="${unCheckedList== null || fn:length(unCheckedList) == 0}">
							<li><fmt:message key="you_have_no_audit_documents"/></li>
						</c:when>
						<c:otherwise>
							<c:forEach var="map" items="${unCheckedList}" varStatus="status">
								<li><fmt:message key="nearly_three_days_you_have"/>
								 <span style="cursor: pointer;" onclick="openUnChecked('<c:out value='${map["nickname"]}'></c:out>',null)">
								<span style="color: red;"><c:out value="${map['count'] }"></c:out></span></span> <fmt:message key="article"/> 
								<c:if test="${map['nickname']=='in' }"><fmt:message key="storage_bills"/></c:if>
								<c:if test="${map['nickname']=='out' }"><fmt:message key="storehouse_bills"/></c:if>
								<c:if test="${map['nickname']=='sto' }"><fmt:message key="reported_bills"/></c:if> <fmt:message key="unchecked"/></li>
							</c:forEach>
							<li><fmt:message key="yesterday_there_was"/>
							<span style="cursor: pointer;" onclick="openUnChecked('unUpload','<c:out value='${positnMapYesterday["nickname"]}'></c:out>')">
								<span style="color: red;"><c:out value="${yesterday}"></c:out></span> </span>
								<fmt:message key="branches_donot_uploaded"/>,
							
							<fmt:message key="today_there_is"/> 
							<span style="cursor: pointer;" onclick="openUnChecked('unUpload','<c:out value='${positnMapToday["nickname"]}'></c:out>')">
								<span style="color: red;"><c:out value="${today}"></c:out></span></span>
							 <fmt:message key="branches_donot_uploaded"/></li>
							
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
		<div id="m_1_2" class="module" style="height: 150px;display: none;">
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="announcement_notification"/></span>
		  </h4>
			<ul>
				<marquee  direction=up height=120px id=m onmouseout=m.start() onMouseOver=m.stop() scrollamount=2 >
					<c:forEach var="announce" items="${announcementList}" varStatus="status">
						<li onclick="showAnnounce(<c:out value="${announce.id}" />)" style="cursor: pointer;"><c:out value="${announce.title}" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${announce.create_time}" /></li>
					</c:forEach>
				</marquee>
			</ul>
		</div>
		
		<div id="m_2_1" class="module" style="display: none;text-align: center;height: 235px;">
		  <h4 class="moduleHeader">
		    <span class="title" style="text-align: left;">物资超上限TOP10</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_1_more')" onmouseout="hide('m_2_1_more')"><span id="m_2_1_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="openUnChecked('overLimit',0)"><fmt:message key="more"/>>>></span></span>
		  </h4>
		  <div id="m_2_1_div"></div>
		</div>
		<div id="m_2_2" class="module" style="display: none;text-align: center;height: 235px;" >
		   <h4 class="moduleHeader">
		    <span class="title" style="text-align: left;">物资不足下限TOP10</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_2_more')" onmouseout="hide('m_2_2_more')"><span id="m_2_2_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="openUnChecked('lessLimit',0)"><fmt:message key="more"/>>>></span></span>
		  </h4>
		  <div id="m_2_2_div"></div>
		</div>
		
		<div id="m_2_3" class="module" style="display: none;text-align: center;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">进货单据汇总&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;汇总最近
		    <select id="num_m_2_3" style="height: 20px;" onchange="changeChart('m_2_3')">
			    <option value=3 >3</option>
			    <option value=7 >7</option>
			    <option value=30 >30</option>
		    </select>天的数据</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_3_more')" onmouseout="hide('m_2_3_more')"><span id="m_2_3_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_3')"><fmt:message key="more"/>>>></span></span>
		  </h4>
		  <div id="m_2_3_div"></div>
			<%-- <div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <td><span style="width:60px;">供应商编码</span></td> -->
								<td><span style="width:150px;"><fmt:message key="suppliers_name"/></span></td>
								<td><span style="width:50px;"><fmt:message key="total_amount1"/></span></td>
								<td><span style="width:50px;"><fmt:message key="storage_amount"/></span></td>
								<!-- <td><span style="width:90px;">入库凭证号</span></td> -->
								<td><span style="width:70px;"><fmt:message key="document_types"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="stackBillSum" items="${stackBillSumList}" varStatus="status">
								<tr>
									<td><span style="width:60px;"><c:out value="${stackBillSum['DELIVER']}" />&nbsp;</span></td>
									<td><span style="width:150px;"><c:out value="${stackBillSum['DES']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${stackBillSum['TOTALAMT']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${stackBillSum['AMT']}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${stackBillSum['VOUNO']}" />&nbsp;</span></td>
									<td><span style="width:70px;"><c:out value="${stackBillSum['TYP']}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div> --%>
		</div>	
		
		<div id="m_2_4" class="module" style="display: none;text-align: center;height: 235px;">
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">出库单据汇总&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;汇总最近
		    <select id="num_m_2_4" style="height: 20px;" onchange="changeChart('m_2_4')">
			    <option value=3 >3</option>
			    <option value=7 >7</option>
			    <option value=30 >30</option>
		    </select>天的数据</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_4_more')" onmouseout="hide('m_2_4_more')"><span id="m_2_4_more" style="visibility: hidden;color: blue;cursor:pointer;" onclick="showMore('m_2_4')"><fmt:message key="more"/>>>></span></span>
		  </h4>
		  <div id="m_2_4_div"></div>
			<%-- <div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:60px;"><fmt:message key="positions"/></span></td>
								<td><span style="width:60px;"><fmt:message key="positions_amount"/></span></td>
								<td><span style="width:60px;"><fmt:message key="requisitioned_positions"/></span></td>
								<!-- <td><span style="width:50px;">分店金额</span></td> -->
								<td><span style="width:50px;"><fmt:message key="the_amount_of_the_single"/></span></td>
								<td><span style="width:100px;"><fmt:message key="orders_num"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="outBillSum" items="${outBillSumList}" varStatus="status">
								<tr>
									<td><span style="width:60px;"><c:out value="${outBillSum['POSITNDES']}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${outBillSum['POSITNAMT']}" />&nbsp;</span></td>
									<td><span style="width:60px;"><c:out value="${outBillSum['FIRMDES']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${outBillSum['FIRMAMT']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${outBillSum['AMT']}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${outBillSum['VOUNO']}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div> --%>
		</div>
		 <div id="m_2_5" class="module" style="display: none;" >
		<h4 class="moduleHeader">
		    <span class="title" ><fmt:message key="will_expire_supplies_offer"/></span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_5_more')" onmouseout="hide('m_2_5_more')"><a id="m_2_5_more" href="#" style="visibility: hidden;color: blue;" onclick="showMore('m_2_5')"><fmt:message key="more"/>>>></a></span>
		  </h4>
			<div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:80px;">物资编码</span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:100px;"><fmt:message key="branche"/></span></td>
								<td><span style="width:60px;"><fmt:message key="offer"/></span></td>
<!-- 								<td><span style="width:80px;">开始日期</span></td> -->
								<td><span style="width:80px;"><fmt:message key="enddate"/></span></td>
								<!-- wangjie 2014年11月10日 15:24:23 增加剩余天数列 -->
								<td><span style="width:80px;">剩余天数</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="spprice" items="${sppriceList}" varStatus="status">
								<tr>
									<td><span style="width:80px;"><c:out value="${spprice.sp_code}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${spprice.sp_name}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${spprice.areaDes}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${spprice.price}" />&nbsp;</span></td>
<%-- 									<td><span style="width:80px;"><fmt:formatDate value="${spprice.bdat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td> --%>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.edat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td align="center"><span style="width:80px;">
										<c:if test="${spprice.deltaT == 1 }">不足</c:if>${spprice.deltaT }
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div> 
		<div id="m_2_6" class="module" style="display: none;text-align: center;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">本月出库物资 TOP10</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_6_more')" onmouseout="hide('m_2_6_more')"><span id="m_2_6_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_6')"><fmt:message key="more"/>>>></span></span>
		  </h4>
			<div id="m_2_6_div" ></div>
		</div> 
<!-- 		<div id="m_2_7" class="module" style="display: none;text-align: center;height: 235px;" > -->
<!-- 		<h4 class="moduleHeader"> -->
<!-- 		    <span style="float: left;font-size: 90%;font-weight: bold;">门店营业额</span> -->
<%-- 		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_7_more')" onmouseout="hide('m_2_7_more')"><span id="m_2_7_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_7')"><fmt:message key="more"/>>>></span></span> --%>
<!-- 		  </h4> -->
<!-- 			<div id="" class="grid" style="height: 220px;" > -->
<!-- 				<div class="table-head" > -->
<!-- 					<table cellspacing="0" cellpadding="0"> -->
<!-- 						<thead> -->
<!-- 							<tr> -->
<!-- 								<td><span style="width:35px;">编码</span></td> -->
<!-- 								<td><span style="width:120px;">分店名称</span></td> -->
<!-- 								<td><span style="width:85px;">所属事业群</span></td> -->
<!-- 								<td><span style="width:53px;">营业额</span></td> -->
<!-- 								<td><span style="width:50px;">交易次数</span></td> -->
<!-- 								<td><span style="width:53px;">每单金额</span></td> -->
<!-- 								<td><span style="width:53px;">翻台率</span></td> -->
<!-- 							</tr> -->
<!-- 						</thead> -->
<!-- 					</table> -->
<!-- 				</div> -->
				
<!-- 				<div class="table-body"> -->
<!-- 					<table cellspacing="0" cellpadding="0"> -->
<!-- 						<tbody> -->
<%-- 							<c:forEach var="firmAmt" items="${firmAmtList}" varStatus="status"> --%>
<!-- 								<tr> -->
<%-- 									<td><span style="width:35px;"><c:out value="${firmAmt['FIRMID']}" /></span></td> --%>
<%-- 									<td><span style="width:120px;text-align: left;"><c:out value="${firmAmt['FIRMDES']}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:85px;text-align: left;"><c:out value="${firmAmt['MODNAM']}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:53px;text-align: right;"><c:out value="${firmAmt['AMT']}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:50px;text-align: right;"><c:out value="${firmAmt['CNT']}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:53px;text-align: right;"><c:out value="${firmAmt['AVGTAB']}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:53px;text-align: right;"><c:out value="${firmAmt['TBLSRATE']}" />&nbsp;</span></td> --%>
<!-- 								</tr> -->
<%-- 							</c:forEach> --%>
<!-- 						</tbody> -->
<!-- 					</table> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div>  -->
<!-- 		<div id="m_2_8" class="module" style="display: none;text-align: center;height: 235px;" > -->
<!-- 		<h4 class="moduleHeader"> -->
<!-- 		    <span style="float: left;font-size: 90%;font-weight: bold;">事业群总营业额</span> -->
<%-- 		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_8_more')" onmouseout="hide('m_2_8_more')"><span id="m_2_8_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_8')"><fmt:message key="more"/>>>></span></span> --%>
<!-- 		  </h4> -->
<!-- 			<div id="m_2_8_div" ></div> -->
<!-- 		</div>  -->
<!-- 		<div id="m_2_9" class="module" style="display: none;text-align: center;height: 235px;" > -->
<!-- 		<h4 class="moduleHeader"> -->
<!-- 		    <span style="float: left;font-size: 90%;font-weight: bold;">本月营业额走势图</span> -->
<%-- 		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_9_more')" onmouseout="hide('m_2_9_more')"><span id="m_2_9_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_9')"><fmt:message key="more"/>>>></span></span> --%>
<!-- 		  </h4> -->
<!-- 			<div id="m_2_9_div" ></div> -->
<!-- 		</div>  -->
<!-- 		<div id="m_2_10" class="module" style="display: none;text-align: center;height: 235px;" > -->
<!-- 		<h4 class="moduleHeader"> -->
<!-- 		    <span style="float: left;font-size: 90%;font-weight: bold;">菜品类别排行</span> -->
<%-- 		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_10_more')" onmouseout="hide('m_2_10_more')"><span id="m_2_10_more" style="visibility: hidden;color: blue;cursor: pointer;" onclick="showMore('m_2_10')"><fmt:message key="more"/>>>></span></span> --%>
<!-- 		  </h4> -->
<!-- 			<div id="m_2_10_div" ></div> -->
<!-- 		</div>  -->
		
		<div id="m_2_20" class="module" style="display: none;text-align: center;height: 150px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;"><fmt:message key="My_usual_Report"/></span>
		    <span style="margin-right: 0px;float: right;" <%--onmouseover="show('m_2_20_more')" onmouseout="hide('m_2_20_more')"--%>><span id="m_2_20_more" style="color: blue;cursor: pointer;" onclick="showMore('m_2_20')"><fmt:message key="scm_set_up_the"/>>></span></span>
		  </h4>
			<div id="m_2_20_div" style="text-align:left;font-size:14px;padding-top:10px;">
				<table>
					<tr>
						<c:forEach items="${reportModuleList }" var="rm" varStatus="status">
						<td><a href="<%=path %>${rm.url}" class="easyui-linkbutton reportBtn" plain="true">${rm.name }</a></td>
						<c:if test="${(status.index + 1) % 5 == 0 }"></tr><tr></c:if>
					</c:forEach>
				</table>
			</div>
		</div> 
		<div id="m_76_1" class="module" style="display: none;text-align: center;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">
				<fmt:message key="turnover"/><fmt:message key="The_growth_rate_of"/></span>
		  </h4>
				<div id="kg1" style="float: left;"></div> 
				<div id="kg2" style="float: left;"></div> 
		</div> 
		<div id="m_76_2" class="module" style="display: none;text-align: center;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">
				<fmt:message key="per_capita"/>、<fmt:message key="Single_guest"/>
		    	<fmt:message key="The_growth_rate_of"/></span>
		  </h4>
				<div id="kg3" style="float: left;"></div> 
				<div id="kg4" style="float: left;"></div> 
		</div>
		<div id="m_76_4" class="module" style="display: none;text-align: center;height: 235px; " >
		<h4 class="moduleHeader">
		     <span style="float: left;font-size: 90%;font-weight: bold;"><fmt:message key="This_month_sales_charts"/></span>
		  </h4>
			<div id="m_76_4_div" ></div>
		</div> 
		<div id="m_76_5" class="module" style="display: none;text-align: center;height: 235px; " >
		<h4 class="moduleHeader">
		   <fmt:message key="food_category"/><fmt:message key="accounted_for"/></span>
		  </h4>
			<div id="m_76_5_div" ></div>
		</div>
		<div id="m_33_1" class="module" style="display: none;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">当前门店今日需要报货</span>
		  </h4>
			<div id="" class="grid" style="height: 220px;" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:100px;">报货分类</span></td>
								<td><span style="width:80px;">报货日期</span></td>
								<td><span style="width:80px;">报货时间</span></td>
								<td><span style="width:80px;">送货日期</span></td>
								<td><span style="width:80px;">送货时间</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="schedule" items="${scheduleList}" varStatus="status">
								<tr>
									<td><span style="width:100px;">${schedule.category_Code }</span></td>
									<td><span style="width:80px;">${schedule.orderDate }</span></td>
									<td><span style="width:80px;">${schedule.orderTime }</span></td>
									<td><span style="width:80px;">${schedule.receiveDate }</span></td>
									<td><span style="width:80px;">${schedule.receiveTime }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div> 
		<div id="m_33_2" class="module" style="display: none;height: 235px;" >
		<h4 class="moduleHeader">
		    <span style="float: left;font-size: 90%;font-weight: bold;">当前门店今日需要验货</span>
		  </h4>
		  <jsp:include page="updateInspection.jsp" />
		</div> 
<!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
		
		<script type="text/javascript" src="<%=path%>/js/justgage.1.0.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/raphael.2.1.0.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				
				//显示菜单
				var menu = '${menu}';
				if(menu==''){
					$("#message").find("span").html('<fmt:message key="you_have_not_set_my_desktop_remind"/>！');
				}else{
					var menuArray = menu.split(',');
					var chartSwf = "Column3D.swf";
					for(var i in menuArray){
						$("#"+menuArray[i]).show();
						if(menuArray[i]!="m_1_1" && menuArray[i]!="m_1_2" && menuArray[i]!="m_2_5" && menuArray[i]!="m_2_20"){
							var w = $("#"+menuArray[i]).width();
							var h = $("#"+menuArray[i]).height()-20;
							var num = 3;
							if(menuArray[i]=="m_2_1" ||menuArray[i]=="m_2_2"){
								w=w-10;
							}
							if(menuArray[i]=="m_2_3" ||menuArray[i]=="m_2_4"){
								num = $("#num_"+menuArray[i]).val();
								chartSwf = "ScrollColumn2D.swf";
							}
							if(menuArray[i]=="m_2_6"){
								chartSwf = "Area2D.swf";
							}
// 							if(menuArray[i]=="m_2_8"){
// 								chartSwf = "Pie3D.swf";
// 							}
// 							if(menuArray[i]=="m_2_9"){
// 								chartSwf = "Line.swf";
// 							}
// 							if(menuArray[i]=="m_2_10"){
// 								chartSwf = "Column3D.swf";
// 							}
							if(menuArray[i]=="m_76_4"){
								chartSwf = "Line.swf";
							}
							if(menuArray[i]=="m_76_5"){
								chartSwf = "Pie3D.swf";
							}
							if(menuArray[i]=="m_76_1"){
								var y1 = '${ky1}';
								var y2 = '${ky2}';
							    var maxy1=100;var maxy2=100;
							    if(y1>100){
							    	maxy1=y1;
							    }
							    if(y2>100){
							    	maxy2=y2;
							    }
						        $('#kg1').width( $('#m_76_1').width()/2-5);
							    $('#kg1').height( $('#m_76_1').height()-50);		
							    $('#kg2').width( $('#m_76_1').width()/2-5);
							    $('#kg2').height( $('#m_76_1').height()-50);		
									
						        var g1 = new JustGage({
							          id: "kg1", 
							          value: getRandomInt(0, 30), 
							          min: -100,
							          max: maxy1,
							          title: "<fmt:message key='Fold_the_former'/><fmt:message key='The_growth_rate_of'/>",
							          label: "<fmt:message key='The_percentage'/>",
										levelColors: [
										  "#FE3C00",
										  "#FE3C00",
										  "#008000"
										]    
							        });
							    g1.refresh(y1);
						        var g2 = new JustGage({
						          id: "kg2", 
						          value: getRandomInt(0, 30), 
						          min: -100,
						          max: maxy2,
						          title: "<fmt:message key='After_the_discount'/><fmt:message key='The_growth_rate_of'/>",
						          label: "<fmt:message key='The_percentage'/>",
									levelColors: [
									  "#EECA00",
									  "#EECA00",
									  "#008000"
									]    
						        });
						        g2.refresh(y2);					        
							}
							if(menuArray[i]=="m_76_2"){
								var y3 = '${ky3}';
								var y4 = '${ky4}';
								var maxy3=100;var maxy4=100;
								
								if(y3>100){
							    	maxy3=y3; 
							    }
							    if(y4>100){
							    	maxy4=y4;
							    }
							    $('#kg3').width( $('#m_76_2').width()/2-5);
							    $('#kg3').height( $('#m_76_2').height()-50);		
							    $('#kg4').width( $('#m_76_2').width()/2-5);
							    $('#kg4').height( $('#m_76_2').height()-50);
								   
								var g3 = new JustGage({
							          id: "kg3", 
							          value: getRandomInt(0, 30), 
							          min: -100,
							          max: maxy3,
							          title: "<fmt:message key='passenger_flow'/><fmt:message key='The_growth_rate_of'/>",
							          label: "<fmt:message key='The_percentage'/>",
										levelColors: [
										  "#EECA00",
										  "#EECA00",
										  "#008000"
										]    
							        });
							        g3.refresh(y3);
							    
							        var g4 = new JustGage({
							          id: "kg4", 
							          value: getRandomInt(0, 30), 
							          min: -100,
							          max: maxy4,
							          title: "<fmt:message key='List_all'/><fmt:message key='The_growth_rate_of'/>",
							          label: "<fmt:message key='The_percentage'/>",
										levelColors: [
										  "#FE3C00",
										  "#FE3C00",
										  "#008000"
										]    
							        });
							        g4.refresh(y4);
							}
							//图表
							var chart = new FusionCharts("<%=path%>/Charts/"+chartSwf+"?ChartNoDataText=无数据显示", "myChart"+menuArray[i], w, h);
							chart.setDataURL(escape("<%=path%>/mainInfo/getXml.do?id="+menuArray[i]+"&days="+num));
							chart.render(menuArray[i]+"_div");
						}
					}
				}
				//table行加斑马色
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
				);
				$('.refresh').bind('click',function refresh(){//刷新
					location.href = location.href;
				});
				
				//跳转我的常用报表
				$('.reportBtn').click(function(event){
		  			var self = $(this);
		  			if(!self.attr("tabid")){
		  				self.attr("tabid",self.text()+new Date().getTime());
		  			}
		  			event.preventDefault();
		  			showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
		  		});
				
			});
			
			function show(id){
				$("#"+id)[0].style.visibility='visible';
			}
			
			function hide(id){
				$("#"+id)[0].style.visibility='hidden';
			}
			
			//查看公告
			function showAnnounce(announcementId){
				<%-- var iTop=(window.screen.height-350)/2;           
				var iLeft=(window.screen.width-500)/2;
				
				window.open("<%=path%>/announcement/show.do?id="+announcementId,"查看公告详情","height=350, width=500, top="+iTop+",left="+iLeft+",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
				 --%>
				 $('body').window({
					id: 'window_addAnnouncement',
					//title: '查看公告',
					content: '<iframe id="showAnnouncementFrame" frameborder="0" src="<%=path%>/announcement/show.do?id='+announcementId+'"></iframe>',
					width: 520,
					height: '380px',
					confirmClose:false,
					draggable: true,
					isModal: true
				});
				
			};
			//打开提醒里边的信息
			function openUnChecked(chkstono,type){
				var startDate = '${startDate}';
				var currDate = '${currDate}';
				var yesterDay = '${yesterDay}';
				if(chkstono=='in'){
					showInfo('b114a52b856449b6b3dbd46a3fe4130c','入库单填制审核','/chkinm/add.do?action=init&tableFrom=table');
				}else if(chkstono=='out'){
					showInfo('c388ec4d46094e229e9df86a6b394f10','出库单填制审核','/chkout/addChkout.do?action=init&tableFrom=table');
				}else if(chkstono=='sto'){
					showInfo('0d4fdeca60c948ec8d65c9ab21ee3279','报货单填制提交','/chkstom/table.do?tableFrom=table');
				}else if(chkstono=='unUpload'){
					var datetime;
					if(type=='unUploadYesterday'){
						datetime = yesterDay;
					}else if(type=='unUploadToday'){
						datetime = currDate;
					}
					showInfo('0d4fdeca60c948ec8d65c9ab21ee3279','报货单填制提交','/chkstom/searchUpload.do?tableFrom=main');
				}else if(chkstono=='overLimit'){
					showInfo('00000000000000000000000overlimit','物资超上限','/mainInfo/overLimit.do');
				}else if(chkstono=='lessLimit'){
					showInfo('00000000000000000000000lesslimit','物资不足下限','/mainInfo/lessLimit.do');
				}
			};
			//showInfo('b114a52b856449b6b3dbd46a3fe4130c','入库单填制审核','/chkinm/add.do?action=init')
			function showMore(id){
				if(id=='m_2_1'){
					
				}else if(id=='m_2_2'){
					
				}else if(id=='m_2_3'){
					showInfo('a934b1d602ff4f6b830a55593c0be3e2','<fmt:message key="purchase_invoices_summary"/>','/JhDanjuHuizong/toStockBillSum.do');
				}else if(id=='m_2_4'){
					showInfo('a934b1d602ff4f6b830a55593c0be3e3','<fmt:message key="storehouse_data_summary"/>','/CkDanjuHuizong/toChkoutOrderSum.do');
				}else if(id=='m_2_5'){
					showInfo('b2905d7ced0d4b9eb20d55832ac7abac','<fmt:message key="will_expire_supplies_offer"/>','/spprice/findSppriceByEdat.do');
				}else if(id=='m_2_6'){
					showInfo('b2905d7ced0d4b9eb20d55832ac7abad','出库汇总查询','/CkHuizongChaxun/toChkoutSumQuery.do');
				}else if(id='m_2_20'){
					$('body').window({
						id: 'setReportModule',
						title: '<fmt:message key="scm_set_up_the"/><fmt:message key="My_usual_Report"/>',
						content: '<iframe id="setReportModuleFrame" frameborder="0" src="<%=path%>/mainInfo/addReportModule.do"></iframe>',
						width: '320px',
						height: '460px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('setReportModuleFrame')){
											window.document.getElementById("setReportModuleFrame").contentWindow.saveUsedModel();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});
				}
				
			}
	  	 	function showInfo(moduleId,moduleName,moduleUrl){
	  	 		window.parent.tabMain.addItem([{
		  				id: 'tab_'+moduleId,
		  				text: moduleName,
		  				title: moduleName,
		  				closable: true,
		  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
		  			}
		  		]);
	  	 		window.parent.tabMain.show('tab_'+moduleId);
	  	 	}
	  	 	
	  	 	function changeChart(chartId){
	  	 		var w = $("#"+chartId).width();
				var h = $("#"+chartId).height()-20;
	  	 		var num = $("#num_"+chartId).val();
				var chart = new FusionCharts("<%=path%>/Charts/ScrollColumn2D.swf?ChartNoDataText=无数据显示", "myChart"+chartId, w, h);
				chart.setDataURL(escape("<%=path%>/mainInfo/getXml.do?id="+chartId+"&days="+num));
				chart.render(chartId+"_div");
	  	 	}
	  	 	
		</script>
	</body>
</html>