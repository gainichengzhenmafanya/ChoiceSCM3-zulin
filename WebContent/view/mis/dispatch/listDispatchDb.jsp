<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>调拨验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">		
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:35px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>
		<input type="hidden" id="firmCode" value="${firmCode }"/>
		<input type="hidden" id="ynUseDept" value="${ynUseDept }"/>
		<form id="listForm" action="<%=path%>/firmMis/dispatchDb.do?mold=select" method="post">
		<!-- <input type="hidden" id="mold" name="mold"/> -->	
			<input type="hidden" id="sta" name="sta"/>		
			<div class="form-line">	
				<div class="form-label"><fmt:message key="arrival_date"/></div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:90px;" id="bdate" name="bdate" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;" id="edate" name="edate" value="<fmt:formatDate value="${edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
				<div class="form-label"></div>
				<div class="form-input" style="width:170px;">[
					<input type="radio" id="all" name="check" value="0" 
						<c:if test="${check=='0'}"> checked="checked"</c:if> /><fmt:message key="all"/>
					<input type="radio" id="chk" name="check" value="1"
						<c:if test="${check=='1'}"> checked="checked"</c:if> /><fmt:message key="checked"/>
					<input type="radio" id="unchk" name="check" value="2" 
						<c:if test="${check=='2'}"> checked="checked"</c:if> /><fmt:message key="unchecked"/>]
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="orders_num"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="standard_unit"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td colspan="2"><span><fmt:message key="distribution"/></span></td>
								<td colspan="2"><span><fmt:message key="inspection"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td colspan="4"><span><fmt:message key="inspection_difference"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:60px;">验货人</span></td>
							</tr>
							<tr>
 								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="quantity"/>1</span></td>
 								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="quantity"/>1</span></td>
 								<td><span style="width:70px;"><fmt:message key="inspection_ratio"/><br/>上限</span></td>
 								<td><span style="width:70px;"><fmt:message key="inspection_ratio"/><br/>下限</span></td>
 								<td><span style="width:50px;"><fmt:message key="branch_loss"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="center_loss"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr data-chkyh="${dis.chkyh }" data-unitper="${dis.supply.unitper }" data-chkstono="${dis.chkstoNo }" data-isdept="${dis.isDept }">
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.chkno}" style="width:40px;">${dis.chkno}</span></td>
									<td><span title="${dis.sp_code }" style="width:60px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:80px;">${dis.sp_name }</span></td>
									<td><span title="${dis.spdesc }" style="width:60px;">${dis.spdesc }</span></td>
									<td><span title="${dis.unit }" style="width:50px;">${dis.unit }</span></td>
									<td><span title="${dis.unit1 }" style="width:50px;">${dis.unit1 }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.pricesale }" type="currency" pattern="0.00"/>" style="width:40px;text-align:right;"><fmt:formatNumber value="${dis.pricesale }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.cntout}" style="width:60px;text-align:right;"><fmt:formatNumber value="${dis.cntout }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.cntuout }" style="width:60px;text-align:right;"><fmt:formatNumber value="${dis.cntuout }" type="currency" pattern="0.00"/></span></td>
									<c:choose>
										<c:when test="${dis.chkyh == 'Y' }">
											<td>
												<span title="${dis.cntfirm}" style="width:60px;text-align:right;"><fmt:formatNumber value="${dis.cntfirm}" type="currency" pattern="0.00"/></span>
											</td>
											<td>
												<span title="${dis.cntfirm1}" style="width:60px;text-align:right;"><fmt:formatNumber value="${dis.cntfirm1}" type="currency" pattern="0.00"/></span>
											</td>
										</c:when>
										<c:otherwise>
											<td class="textInput">
												<span title="${dis.cntfirm}" style="width:60px;">
													<input type="text" id="cnt_${dis.id}" value="<fmt:formatNumber value="${dis.cntfirm}" type="currency" pattern="0.00"/>" readonly="readonly" style="width:60px;text-align: right;" onfocus="onEdit(this);" onblur="update(this,'${dis.cntfirm}');outEdit(this);"/>
												</span>
											</td>
											<td class="textInput">
												<span title="${dis.cntfirm1}" style="width:60px;">
													<input type="text" id="cnt1_${dis.id}" value="<fmt:formatNumber value="${dis.cntfirm1}" type="currency" pattern="0.00"/>" readonly="readonly" style="width:60px;text-align: right;"  onfocus="onEdit(this);" onblur="update1(this,'${dis.cntfirm1}');outEdit(this);"/>
												</span>
											</td>
										</c:otherwise>
									</c:choose>
									<td><span title="${dis.amount}" style="width:60px;text-align:right;">
										<fmt:formatNumber value="${dis.amount}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
									</span></td>
									<td>
										<span title="${dis.supply.accprate}" style="width:70px;text-align:right;">
											<fmt:formatNumber value="${dis.supply.accprate}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
									<td>
										<span title="${dis.supply.accpratemin}" style="width:70px;text-align:right;">
											<fmt:formatNumber value="${dis.supply.accpratemin}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
									<td>
										<span title="${dis.cntfirmks}" style="width:50px;text-align:right;">
											<fmt:formatNumber value="${dis.cntfirmks}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
									<td>
										<span title="${dis.cntlogistks}" style="width:50px;text-align:right;">
											<fmt:formatNumber value="${dis.cntlogistks}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;
										</span>
									</td>
									<td><span title="${dis.des}" style="width:60px;">${dis.des }</span></td>
									<td><span title="${dis.empfirm}" style="width:60px;">${dis.empfirm }</span></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td><span style="width:40px;"><fmt:message key="total"/>:</span></td>
								<td colspan="10"></td>
								<td><span style="width:60px;text-align:right;"></span></td>
							</tr>							
						</tfoot>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		var ids = new Array();
		$(document).ready(function(){
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),105);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			if($('input:radio[name="check"]:checked').val()=='2'){
				loadToolBar([true,true]);
			}else{//查询已审核或者全部的时候 才能使用直发档口按钮
				loadToolBar([false,false]);
			}
			//页面获得焦点
			focus() ;
			//按钮快捷键
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});

			// 筛选条件为未审时，数量和数量2变为可编辑状态。
			if($('input:radio[name="check"]:checked').val()=='2') {
				$('.grid input').removeAttr("readonly");
			}
			var totalAmount=0;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.each(function(){
// 				自动计算金额
// 				var price=$(this).parents('tr').find('td:eq(8)').find('span').attr('title');
// 				var amount=$(this).parents('tr').find('td:eq(11)').find('input').val();
// 				$(this).parents('tr').find('td:eq(13)').find('span').find('input').val(Math.round((price*amount)*10)/10);
// 				根据第二单位对第一单位的转化率自动计算报货数量2
// 				var value=$(this).parents('tr').find('td:eq(9)').find('span').attr('title');
// 				var unitper=$(this).parents('tr').find('td:eq(19)').find('input').val();
// 				$(this).parents('tr').find('td:eq(10)').find('span').find('input').val(Math.round((unitper*value)*10)/10);
				$(this).parents('tr').find('td:eq(13)').find('span').text();
				totalAmount+=Number($(this).parents('tr').find('td:eq(13)').find('span').text());
			});
			 if(0==totalAmount) {
				$('tfoot').hide();
			} 
			$('tfoot').find('tr').find('td:eq(4)').find('span').text(Number(totalAmount).toFixed(2)).attr('title',Number(totalAmount).toFixed(2));
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
		    
			new tabTableInput(".table-body","text");
		});	
		
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="scm_ps_yh"/>',
						title: '<fmt:message key="scm_ps_yh"/>',
						useable: use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							peiSongToyanHuo();
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable:use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateDis();
						}
					},{
						text: '<fmt:message key="check" />(<u>C</u>)',
						title: '<fmt:message key="check"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-58px','-240px']
						},
						handler: function(){
							//判断修改了多少，没保存
							var str = []; 
							for(var i = 0;i < ids.length;i++){ 
								!RegExp(ids[i],"g").test(str.join(",")) && (str.push(ids[i])); 
							} 
							if(str.length !=0){
								alert("<fmt:message key='update'/> "+str.length+" <fmt:message key='article'/><fmt:message key='data'/>，<fmt:message key='save'/><fmt:message key='after'/><fmt:message key='check'/>！");
							}else{
								check();
							}

						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#sta').val("print");
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/firmMis/printDispatchDb.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/firmMis/dispatchDb.do?mold=select');
							$("#wait2").val('');//等待加载还原
						}
					},<%-- {
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/inspection/exportDispatch.do');
							$('#listForm').submit();
							$("#wait2").val('');//等待加载还原
						}
					}, --%>{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		}
		//配送数量等于验货数量
		function peiSongToyanHuo(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					$(this).parents('tr').find('td:eq(11)').find('span').find('input').val($(this).parents('tr').find('td:eq(9)').find('span').attr('title'));//验货数量
					$(this).parents('tr').find('td:eq(12)').find('span').find('input').val($(this).parents('tr').find('td:eq(10)').find('span').attr('title'));
					var price=$(this).parents('tr').find('td:eq(8)').find('span').attr('title');//价格
					var amount=$(this).parents('tr').find('td:eq(11)').find('input').val();
					$(this).parents('tr').find('td:eq(13)').find('span').text(Number(price*amount).toFixed(2));
// 					$('#ifedite').val($('#ifedite').val()+1);
					ids.push($(this).val());//放进id，用来判断修改了那些
				});
			}else{
				alert("<fmt:message key='please_select'/><fmt:message key='update'/><fmt:message key='data'/>！");
			}
		}
		// 点击保存以后保存验货数量和验货差异
		function updateDis() {
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
					checkboxList.filter(':checked').each(function(i){
						var cntfirm=$(this).parents('tr').find('td:eq(11)').find('span').find('input').val();
						selected['supplyAcctList['+i+'].id'] =  $(this).parents('tr').find('td:eq(1)').find('input').val();
						selected['supplyAcctList['+i+'].cntfirm'] =  cntfirm;
						selected['supplyAcctList['+i+'].cntfirm1'] = $(this).parents('tr').find('td:eq(12)').find('span').find('input').val();
						selected['supplyAcctList['+i+'].cntfirmks'] =  $(this).parents('tr').find('td:eq(16)').find('span').attr('title');
						selected['supplyAcctList['+i+'].cntlogistks'] = $(this).parents('tr').find('td:eq(17)').find('span').attr('title');
						if(Number(cntfirm)==0){
							count++;
						}
						//单元格编辑状态 0表示值未变  1表示值改变
// 						$(this).parents("tr").find("input[name=ifedite]").val("0");
					});
					if(count>0){
						if(confirm("<fmt:message key='the_number_of_inspection'/><fmt:message key='have'/>"+count+"<fmt:message key='article'/>0<fmt:message key='data'/>，<fmt:message key='wether'/><fmt:message key='continue'/>？")){
							savePost(selected);
							}
						}else{
							savePost(selected);
						}
					}
					ids = [];//将编辑的单据置为空
				}else{
					alert('<fmt:message key="please_select_options_you_need_save" />！');
					return ;
				}
		}
		function savePost(selected){
			$.post('<%=path%>/firmMis/updateAcctDb.do',selected,function(data){
				showMessage({//弹出提示信息
					type: 'success',
					msg: '<fmt:message key="operation_successful" />！',
					speed: 1000
					});	
				});
		}
		
		function checkDeptSave(chkValue,chkstonoVaue){
			var ids=chkValue.join(",");
			var chkstono=chkstonoVaue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '直发档口',
				content: '<iframe id="chkinmZBFrame" name="chkinmZBFrame" frameborder="0" src="<%=path%>/firmMis/toChkinmDept.do?id='+ids+'&chkstoNos='+chkstono+'"></iframe>',
				width: '99%',
				height: '98%',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '直发档口',
							title: '直发档口',
						//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								window.frames["chkinmZBFrame"].saveChkinmDept();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		//审核数据
		function check(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				var flag = true;//用来判断如果转换率是0的物资，如果改了标准数量，参考数量不能为0;或者反过来 wjf
				var sp_code = "";
				var chkValue = [];
				var flag1 = false;//用来判断是否有档口报货来的
				var chkValue1 = [];//存放档口报货来的
				var chkstonoValue = [];
				checkboxList.filter(':checked').each(function(i){
					if(Number($(this).parents('tr').data('unitper')) == 0){
						var cnttrival = $(this).parents('tr').find('td:eq(11)').find('input').val();
						var cntutrival = $(this).parents('tr').find('td:eq(12)').find('input').val();
						if((Number(cnttrival) == 0 && Number(cntutrival) != 0) || (Number(cntutrival) == 0 && Number(cnttrival) != 0) ){
							flag = false;
							sp_code = $(this).parents('tr').find('td:eq(4)').find('span').text();
							return;
						}
					}
					if('Y' == $(this).parents('tr').data('isdept')){//是档口报货来的
						flag1 = true;
						chkValue1.push($(this).val());
						chkstonoValue.push($(this).parents('tr').data('chkstono'));//申购单号
					}else{
						chkValue.push($(this).val());
					}
					if(Number($(this).parents('tr').find('td:eq(11)').find('span').find('input').val())==0){
						count++;
					}
				});
				if(!flag){
// 					alert('物资：['+sp_code+']入库的标准数量和参考数量不能有一个为0。');
					alert('<fmt:message key="supplies"/>：['+sp_code+']<fmt:message key="storage"/><fmt:message key="standard_number_or_reference_cannot_have_a"/>0。');
					return;
				}
				if(count>0){
					//有验货是0 的数据
					if(confirm("<fmt:message key='the_number_of_inspection'/><fmt:message key='have'/>"+count+"<fmt:message key='article'/>0<fmt:message key='data'/>，<fmt:message key='whether'/><fmt:message key='continue'/>？")){
						if(flag1){
							//有档口报货的走档口报货
							if(confirm('<fmt:message key="there_are_stalls_offer_goods_supplies"/>，<fmt:message key="whether_the_first_file_export_inspection"/>？')){
								checkDeptSave(chkValue1,chkstonoValue);
							}else if(chkValue.length > 0){//门店报货的，分启不启用档口 两种情况
								if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面，还未实现。。。。。。。。。
									checkSaveGroup(chkValue);
								}else{
									if(confirm('<fmt:message key="determine_audit_and_generate_store_storage"/>?'))
										checkSave(chkValue);
								}
							}
						}else if(chkValue.length > 0){
							if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面，还未实现。。。。。。。。。
								checkSaveGroup(chkValue);
							}else{
								if(confirm('<fmt:message key="determine_audit_and_generate_store_storage"/>?'))
									checkSave(chkValue);
							}
						}
					}
				}else{
					if(flag1){
						if(confirm('<fmt:message key="there_are_stalls_offer_goods_supplies"/>，<fmt:message key="whether_the_first_file_export_inspection"/>？')){
							checkDeptSave(chkValue1,chkstonoValue);
						}else if(chkValue.length > 0){
							if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面，还未实现。。。。。。。。。
								checkSaveGroup(chkValue);
							}else{
								if(confirm('<fmt:message key="determine_audit_and_generate_store_storage"/>?'))
									checkSave(chkValue);
							}
						}
					}else if(chkValue.length > 0){
						if("Y" == $('#ynUseDept').val()){//分多档口的,弹出多档口的验货界面，还未实现。。。。。。。。。
							checkSaveGroup(chkValue);
						}else{
							if(confirm('<fmt:message key="determine_audit_and_generate_store_storage"/>?'))
								checkSave(chkValue);
						}
					}
				}
			}else{
				alert('<fmt:message key="please_select_check_information"/>！！');
				return ;
			}
		}
		function checkSave(chkValue){
			//先判断仓库有没有期初
			$.ajax({
				url:"<%=path%>/positn/checkQC.do?code="+$('#firmCode').val(),
				type:"post",
				success:function(data){
					if(data){
						var action = '<%=path%>/firmMis/checkDb.do?ids='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="audit_arrival_information"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}else{
// 						alert($('#firmCode').val()+'该门店还未做期初！不能入库！');
						alert($('#firmCode').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
						return;
					}
				}
			});
		}
		//门店分组验货 有档口的情况下
		function checkSaveGroup(chkValue){
			var ids=chkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '门店验货',
				content: '<iframe id="chkinmGroupFrame" name="chkinmGroupFrame" frameborder="0" src="<%=path%>/firmMis/toChkinmGroupDb.do?ids='+ids+'"></iframe>',
				width: '99%',
				height: '98%',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								if(confirm('提示：验货到门店将生成门店入库单,验货到档口将生成档口直发单,是否继续?'))
									window.frames["chkinmGroupFrame"].saveChkinmGroup();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		//修改验货数量时，验货数量2、金额自动计算
		function update(e,f){				
			if(($(e).attr("readonly")=="readonly")==true){
				return;
			}else if(Number(e.value)<0){
				alert('<fmt:message key="number_cannot_be_negative"/>');
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			if(!checkYhbl(e)){
				alert('<fmt:message key="does_not_conform_to_the_inspection_rate"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			}
			var value=e.value;
			var unitper=$(e).closest('tr').data('unitper');
			$(e).closest('tr').find('td:eq(12)').find('span').find('input').val(Number(value*unitper).toFixed(2));//数量1
			
			var price=$(e).closest('tr').find('td:eq(8)').find('span').attr('title');
			$(e).closest('tr').find('td:eq(13)').find('span').text(Number(value*price).toFixed(2));//总金额
			
			//计算分店亏和中心亏  wjf 2014.11.22
			var oamount = Number($(e).closest('tr').find('td:eq(9)').find('span').attr('title'));
			var namount = Number($(e).closest('tr').find('td:eq(11)').find('input').val());
			if(oamount > namount){//如果发货数量大于验货数量 则分店亏，反之 则总部亏
				$(e).closest('tr').find('td:eq(16)').find('span').text((oamount-namount).toFixed(2)).attr('title',(oamount-namount).toFixed(2));
				$(e).closest('tr').find('td:eq(17)').find('span').text(0.00).attr('title',0.00);
			}else{
				$(e).closest('tr').find('td:eq(16)').find('span').text(0.00).attr('title',0.00);
				$(e).closest('tr').find('td:eq(17)').find('span').text((namount-oamount).toFixed(2)).attr('title',(namount-oamount).toFixed(2));
			}
			
			if(e.value != f){//判断该单元格是否被修改
// 				$('#ifedite').val($('#ifedite').val()+1);
				ids.push($(e).closest('tr').find('td:eq(1)').find('input').val());//放进id，用来判断修改了那些
			}
		}
		//修改验货数量2时，验货数量、金额自动计算
		function update1(e,f){
			if(($(e).attr("readonly")=="readonly")==true){
				return;
			}else if(Number(e.value)<0){
				alert('<fmt:message key="number_cannot_be_negative"/>');
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			
			var flag = true;
			var value=e.value;
			var unitper=$(e).closest('tr').data('unitper');
			if(Number(unitper) != 0){
				var val = $(e).closest('tr').find('td:eq(11)').find('span').find('input').val();
				$(e).closest('tr').find('td:eq(11)').find('span').find('input').val(Number(value/unitper).toFixed(2));
				if(!checkYhbl($(e).closest('tr').find('td:eq(11)').find('span').find('input').get(0))){
					alert('<fmt:message key="does_not_conform_to_the_inspection_rate"/>！');
					e.value=e.defaultValue;
					$(e).closest('tr').find('td:eq(11)').find('span').find('input').val(val);
					e.focus();
					flag = false;
				}else{
					var price=$(e).closest('tr').find('td:eq(8)').find('span').attr('title');
					var amount=$(e).closest('tr').find('td:eq(11)').find('input').val();
					$(e).closest('tr').find('td:eq(13)').find('span').find('input').val(Number(value*price).toFixed(2));
				}
			}
			if(!flag){
				return;
			}
			
			//计算分店亏和中心亏  wjf 2014.11.22
			var oamount = Number($(e).closest('tr').find('td:eq(9)').find('span').attr('title'));
			var namount = Number($(e).closest('tr').find('td:eq(11)').find('input').val());
			if(oamount > namount){//如果发货数量大于验货数量 则分店亏，反之 则总部亏
				$(e).closest('tr').find('td:eq(16)').find('span').text((oamount-namount).toFixed(2)).attr('title',(oamount-namount).toFixed(2));
				$(e).closest('tr').find('td:eq(17)').find('span').text(0.00).attr('title',0.00);
			}else{
				$(e).closest('tr').find('td:eq(16)').find('span').text(0.00).attr('title',0.00);
				$(e).closest('tr').find('td:eq(17)').find('span').text((namount-oamount).toFixed(2)).attr('title',(namount-oamount).toFixed(2));
			}
			
			if(e.value != f){//判断该单元格是否被修改
// 				$(e).parent().parent().parent().find("input[name=ifedite]").val("1");
				ids.push($(e).closest('tr').find('td:eq(1)').find('input').val());//放进id，用来判断修改了那些
			}
		}
		
		//可编辑表格的样式，获得焦点时候	
		function onEdit(obj){
			if(($(obj).attr("readonly")=="readonly")==true){
				return;
			}else{
				obj.select();
				$(obj).removeClass("input");
				$(obj).addClass("onEdit");
			}
		}
		//可编辑表格的样式，焦点离开时候
		function outEdit(obj){
			if(($(obj).attr("readonly")=="readonly")==true){
				return;
			}else{
				$(obj).removeClass("onEdit");
				$(obj).addClass("input");
			}
		}
		function pageReload(){
			$('#listForm').submit();
		}
		
		//计算验货比率
		function checkYhbl(e){
			var value = Number(e.value);
			var cnt = Number($(e).closest('tr').find('td:eq(9)').find('span').attr('title'));//原值
			var blsx = Number($(e).closest('tr').find('td:eq(14)').find('span').attr('title'));//上限
			var blxx = Number($(e).closest('tr').find('td:eq(15)').find('span').attr('title'));//下限
			if(blsx == 0 && blxx == 0){//都为0 随便验
				return true;
			}else if(blsx == 1 && blxx == 1){// 都为1的情况下，按发货数量验
				if(cnt == value){
					return true;
				}else{
					return false;
				}
			}else{
				if(value <= (cnt*blsx) && value >= (cnt*blxx)){//如果在验货比率内
					return true;
				}else{
					return false;
				}
			}
		}
		</script>
	</body>
</html>