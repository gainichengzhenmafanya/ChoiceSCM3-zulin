<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--配送差异处理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">		
			.tool {
				position: relative;
				height: 27px;
			}
			.page{
				margin-bottom: 25px;
			}
			.grid td span{
				padding:0px;
			}
			.table-head td span{
				white-space: normal;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkout/listDiff.do" method="post">
		<!-- <input type="hidden" id="mold" name="mold"/> -->	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top;" value="${dis.positn}"/>
						<select name="positn" id="positn" class="select" style="width:320px;">
							<option value="">--</option>
							<c:forEach items="${positnOut }" var="positn">
								<option value="${positn.code }" title="${positn.des}"
								<c:if test="${positn.code == dis.positn }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label">出库日期：</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:100px;" id="dat" name="dat" value="<fmt:formatDate value="${dis.dat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker();"/>
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="use_positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" id="firm_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${dis.firm}"/>
						<select name="firm" id="firm" class="select" style="width:320px;">
							<option value="">--</option>
							<c:forEach items="${positnIn }" var="positn">
								<option value="${positn.code }" title="${positn.des}"
								<c:if test="${positn.code == dis.firm }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-input" style="width:220px;text-align:right;">
					[<input type="radio" id="iscy1" name="iscy" value="1"
						<c:if test="${dis.iscy=='1'}"> checked="checked"</c:if> />分店盈
					<input type="radio" id="iscy2" name="iscy" value="2" 
						<c:if test="${dis.iscy=='2'}"> checked="checked"</c:if> />分店亏]
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:15px;">&nbsp;</span></td>
								<td style="width:20px;text-align: center;"><input type="checkbox" id="chkAll"/></td>
								<td><span style="width:120px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:100px;">出库仓位</span></td>
								<td><span style="width:100px;">分店名称</span></td>
								<td><span style="width:70px;">物资<fmt:message key="coding"/></span></td>
								<td><span style="width:100px;">物资<fmt:message key="name"/></span></td>
								<td><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:60px;" title="<fmt:message key="standard_unit"/>"><fmt:message key="standard_unit"/></span></td>
								<td><span style="width:60px;" title="<fmt:message key="reference_unit"/>"><fmt:message key="reference_unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:80px;"><fmt:message key="distribution"/>数量</span></td>
								<td><span style="width:80px;"><fmt:message key="inspection"/>数量</span></td>
								<td><span style="width:80px;">差异数量</span></td>
								<td><span style="width:80px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></td>
									<td><span title="${dis.vouno}" style="width:120px;text-align: center;">${dis.vouno}</span></td>
									<td><span title="${dis.positn }" style="width:100px;">${dis.positn }</span></td>
									<td><span title="${dis.firm }" style="width:100px;">${dis.firm }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
									<td><span title="${dis.spdesc }" style="width:60px;">${dis.spdesc }</span></td>
									<td><span title="${dis.unit }" style="width:60px;">${dis.unit }</span></td>
									<td><span title="${dis.unit1 }" style="width:60px;">${dis.unit1 }</span></td>
									<td><span title="${dis.pricesale }" style="width:60px;text-align: right;">${dis.pricesale }</span></td>
									<td><span title="${dis.cntout}" style="width:80px;text-align: right;">${dis.cntout }</span></td>
									<td><span title="${dis.cntfirm}" style="width:80px;text-align: right;">${dis.cntfirm}</span></td>
									<td><span title="${dis.cntout-dis.cntfirm}" style="width:80px;text-align: right;"><fmt:formatNumber value="${dis.cntout-dis.cntfirm}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td>
										<span title="${dis.des}" style="width:80px;text-align: center;">${dis.des}
<%-- 											<input type="text" value="${dis.des}" style="width:60px;height:13px;"/> --%>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			loadToolBar();
			$("#positn_select").val($("#positn").val());
			$("#firm_select").val($("#firm").val());
			/*过滤*/
			$('#positn_select').bind('keyup',function(){
	          $("#positn").find('option')
	                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
	                    .attr('selected','selected');
	       });
			$('#firm_select').bind('keyup',function(){
	          $("#firm").find('option')
	                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
	                    .attr('selected','selected');
	       });
			//页面获得焦点
			focus() ;
			//按钮快捷键
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			});
		    
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();			
		});	
		
		//控制按钮显示
		function loadToolBar(){
			$('.tool').html('');
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
// 						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '冲消并报损',
						title: '冲消并报损',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							cxbs();
						}
					},{
						text: '报盈处理',
						title: '报盈处理',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-58px','-240px']
						},
						handler: function(){
							by();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		}
		//查询出库和入库仓位
		$('#seachOnePositn').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#positn_select').val();
				var defaultName = $('#positn').val();
				var offset = getOffset('maded');
				top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positn_select'),'760','520','isNull',handler);
			}
		});
	 	function handler(a){
	 		 $("#positn").find("option[value='"+a+"']").attr('selected','selected');
	 	}
	 	$('#seachOnePositn1').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#firm_select').val();
				var defaultName = $('#firm').val();
				var offset = getOffset('maded');
				top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#firm_select'),'760','520','isNull',handler1);
			}
		});
	 	function handler1(a){
	 		 $("#firm").find("option[value='"+a+"']").attr('selected','selected');
	 	}
		
	 	//冲消并报损
	 	function cxbs(){
	 		if(confirm('系统将生成相应的冲消单，并将所选冲消物资报损，同时生成报损出库单，确认继续？')){
	 			if($('.grid').find('.table-body').find('tr').size() == 0){
	 				alert('当前没有数据！');
	 				return;
	 			}
	 			if($('#positn').val() == '' || $('#firm').val() == ''){
	 				alert('请选择出库仓位和领用门店！');
	 				return;
	 			}
	 			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(i){
						chkValue.push($(this).val());
					});
					$.ajax({
		 				url:'<%=path%>/chkout/dealDiff.do?dat='+$('#dat').val()+'&positn='+$('#positn').val()+'&firm='+$('#firm').val()+'&iscy='+$('input[name="iscy"]:checked').val()+'&ids='+chkValue.join(","),
		 				type:'post',
		 				success:function(data){
		 					var rs = eval("("+data+")");
		 					if(rs.pr == 1){
		 						alert('操作成功！');
		 						$('#listForm').submit();
							}else if(!rs.msg.match(/^\{\d|\D*\}$/)){
								showMessage({
									type: 'error',
									msg: rs.msg,
									speed: 3000
									});
							}
		 				}
		 			});	 	
				}else{
					alert("请选择要操作的数据！");
				}
	 		}
	 	}
	 	
	 	//报盈处理
	 	function by(){
			if(confirm('本操作将生成一个报盈补入的到门店的直拨单，确认继续？')){
	 			alert('此功能暂未开发！');
	 		}
	 	}
		
		</script>
	</body>
</html>