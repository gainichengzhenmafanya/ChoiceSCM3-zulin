<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>价格预警与比重</title>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/image/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/image/themes/icon.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.button.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.toolbar.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.window.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path %>/css/lib.ui.core.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path %>/css/lib.ui.form.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/scm/ajaxSearch.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
<style type="text/css">
.text {
	font-size: 12px, border:0px, line-height:20px, height:20px, padding:0px,*height:18px,*line-height:18px,
		_height:18px, _line-height:18px;
}

input[type=radio],input[type=checkbox] {
	height: 13px;
	vertical-align: text-top;
	margin-top: 1px;
	margin-right: 3px;
}

.search {
	margin-top: 3px;
	cursor: pointer;
}

form .form-line .form-label {
	width: 6.5%;
}

form .form-line .form-input {
	width: 16%;
}

form .form-line .form-input input[type=text] {
	width: 90%;
}

form .form-line .form-input select {
	width: 92%;
}
</style>
</head>
<body>
	<div class="tool"></div>
	<input id="firstLoad" type="hidden" />
	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index: 10;">
			<div class="form-label"
				<c:if test='${type!=null}'>style="display:none;"</c:if>>
				<fmt:message key="positions" />
			</div>
			<div class="form-input"
				<c:if test='${type!=null}'>style="display:none;"</c:if>>
<!-- 				<select name="positn" id="positn" -->
<%-- 					url="<%=path %>/positn/findAllPositnOut.do" class="select"></select> --%>
				<%-- <input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
				<input type="hidden" id="positn" name="positn" value=""/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
				<input type="text" name="positn_name" id="positn_name" style="margin-top: -3px;" autocomplete="off" class="text" value=""/>
				<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
				<input type="hidden" id="positn" name="positn" value="" />
				<input type="hidden" id="positn1" name="positn1" value="" />
<%-- 				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label">
				<fmt:message key="coding" />
			</div>
			<div class="form-input" style="margin-top: -3px;">
				<input type="text" id="sp_code" name="sp_code" value="${sp_code}"
					class="text" /> <img id="seachSupply" class="search"
					src="<%=path%>/image/themes/icons/search.png"
					alt='<fmt:message key="query_supplies"/>' />
			</div>
			<div class="form-label">
				<fmt:message key="months" />
			</div>
			<div class="form-input" style="margin-top: -2px;">
				<select id="month" name="month" class="select">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
			<div class="form-input"
				<c:if test='${type!=null}'>style="margin-left:80px;"</c:if>>
				<input type="checkbox" id="ups" name="ups" value="1" />连续三个月价格上升
			</div>
		</div>
		<div class="form-line" style="z-index: 9;">
			<div class="form-label">
				<fmt:message key="bigClass" />
			</div>
			<div class="form-input">
				<select name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"
					class="select"></select>
			</div>
			<div class="form-label">
				<fmt:message key="middleClass" />
			</div>
			<div class="form-input">
				<select name="grp" url="<%=path %>/grpTyp/findAllGrp.do"
					class="select"></select>
			</div>
			<div class="form-label">
				<fmt:message key="smallClass" />
			</div>
			<div class="form-input">
				<select name="typ" url="<%=path %>/grpTyp/findAllTyp.do"
					class="select"></select>
			</div>
			<div class="form-label">
				波动幅度大于：<input type="text" id="fluctuations" name="fluctuations"
					value="0.00"
					onkeypress="if((event.keyCode <48||event.keyCode> 57) && event.keyCode!=46){return false;}"
					style="width: 50%; ime-mode: disabled;" />&nbsp; 比重大于：<input
					type="text" id="proportion" name="proportion" value="0.00"
					onkeypress="if((event.keyCode <48||event.keyCode> 57) && event.keyCode!=46){return false;}"
					style="width: 50%; ime-mode: disabled;" />
			</div>
		</div>
	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var str = validate();
							if(str=='f') return;
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							var str = validate();
							if(str=='f') return;
							$('#queryForm').attr('action',"<%=path%>/JgYujingBizhongMis/exportPriceWarning.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							var str = validate();
							if(str=='f') return;
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.availWidth+',height='+window.screen.availHeight+',top=0,left=0');
							var action="<%=path%>/JgYujingBizhongMis/printPriceWarning.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
			var month = new Date().getMonth()+1;
			$("#month > option[value="+month+"]").attr("selected","selected");
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").not("#month,#positn1").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['priceavgex','priceavgex2','priceavg','pricemax','pricemin','inabal','ina','inc','incbal','incex','incex2','inaex','inaex2'];
  	 		var numColsAlign = ['priceavgex','priceavgex2','priceavg','pricemax','pricemin','pricerate','inabal','ljbz','ina','inc','incbal','incex','incex2','inaex','inaex2'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/JgYujingBizhongMis/findPriceWarningHeader.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numColsAlign) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numColsAlign) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'采购价格预警与物资比重分析',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				 var rs = eval("("+data+")");
		 				var modifyRows = [];
		 				var modifyFoot = [];
		 				var rows = rs.rows;
		 				var footer = rs.footer;
		 				if(!rows || rows.length <= 0){
		 					
		 					//wangjie 2014年10月27日 14:01:55
		 					var item = $('#datagrid').datagrid('getRows');
		 		            if (item) {
		 		                for (var i = item.length - 1; i >= 0; i--) {
		 		                    var index = $('#datagrid').datagrid('getRowIndex', item[i]);
		 		                    $('#datagrid').datagrid('deleteRow', index);//遍历并删除结果集中的数据
		 		                }
		 		            }
		 					$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
		 				}
		 				for(var i in rows){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
		 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');		 	
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"rows["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyRows.push(curRow);
		 				}
		 				rs.rows = modifyRows;
		 				for(var i in footer){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
		 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"footer["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyFoot.push(curRow);
		 				}
		 				rs.footer = modifyFoot;
		 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/JgYujingBizhongMis/findPriceWarning.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:false,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$(".panel-tool").remove();

  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/JgYujingBizhongMis/toColChoosePriceWarning.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 	function validate(){
  	 		var flag = 't';  	 		
			if(isNaN($('#fluctuations').val())){
				alert('<fmt:message key="only_digits" />！');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="only_digits" />！',
					speed: 1500
				}); */
				flag = 'f';
			}
			if(isNaN($('#proportion').val())){
				alert('<fmt:message key="only_digits" />！');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="only_digits" />！',
					speed: 1500
				}); */
				flag = 'f';
			}
			return flag;
  	 	}
  	 </script>
</body>
</html>