<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/image/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/image/themes/icon.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.button.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.toolbar.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/widget/lib.ui.window.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path %>/css/lib.ui.core.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path %>/css/lib.ui.form.css" />
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/scm/ajaxSearch.css" />
<style type="text/css">
.text {
	font-size: 12px, border:0px, line-height:20px, height:20px, padding:0px,*height:18px,*line-height:18px,
		_height:18px, _line-height:18px;
}

.search {
	margin-top: 3px;
	cursor: pointer;
}

form .form-line .form-label {
	width: 5%;
}

form .form-line .form-input {
	width: 18%;
}
</style>
</head>
<body>
	<div class="tool"></div>
	<input id="firstLoad" type="hidden" />
	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index: 10;">
			<div class="form-label"
				<c:if test='${type!=null}'>style="display:none;"</c:if>>
				<fmt:message key="positions" />
			</div>
			<div class="form-input"
				<c:if test='${type!=null}'>style="display:none;"</c:if> style="margin-top: -3px;">
				<input type="text" id="positn_name" name="positn_name"
					readonly="readonly" value="" class="text" style="width: 136px;"/> <input type="hidden" id="positn"
					name="positn" value="" /> <img id="seachPositn" class="search"
					src="<%=path%>/image/themes/icons/searchmul1.png"
					alt='<fmt:message key="query_position"/>' />
			</div>
			<div class="form-label">
				<fmt:message key="coding" />
			</div>
			<div class="form-input" style="margin-top: -3px;">
				<input type="text" name="sp_code" id="sp_code" style="width: 136px;"
					autocomplete="off" class="text"
					value="<c:out value="${chkoutm.chkoutno}" />" /> <img
					id="seachSupply" class="search"
					src="<%=path%>/image/themes/icons/search.png"
					alt='<fmt:message key="query_supplies"/>' />
			</div>
		</div>
		<div class="form-line" style="z-index: 9;">
			<div class="form-label">
				<fmt:message key="bigClass" />
			</div>
			<div class="form-input">
				<select name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"
					class="select"></select>
			</div>
			<div class="form-label">
				<fmt:message key="middleClass" />
			</div>
			<div class="form-input">
				<select name="grp" url="<%=path %>/grpTyp/findAllGrp.do"
					class="select"></select>
			</div>
		</div>
	</form>
	<div id="datagrid"></div>
	<div id="dataDetail"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript"
		src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
 		var firstLoad = true;//标记是否为第一次加载
 		//收集form表单数据的对象
 		var params = {};
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load",params);
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/KlZongheFenxiMis/exportInventoryAgingSum.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/KlZongheFenxiMis/printInventoryAgingSum.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[]);
  	 		});
  	 		
  	 		
  	 		builtTable({
  	 			headUrl:"<%=path%>/KlZongheFenxiMis/findInventoryAgingSumHeaders.do?typ=sum",
  	 			dataUrl:"<%=path%>/KlZongheFenxiMis/findInventoryAgingSum.do",
  	 			title:'库龄综合分析',
  	 			id:"datagrid",
  	 			height:tableHeight/2,
  	 			onClickRow:function(rowIndex, rowData){
  	 				var p = {};
  	 				p['positn']=rowData.POSITN;
  	 				p['grptyp']=rowData.GRPTYP;
  	 				$("#dataDetail").datagrid("load",p);
  	 			}
  	 		});
  	 		
  	 		builtTable({
  	 			headUrl:"<%=path%>/KlZongheFenxiMis/findInventoryAgingSumHeaders.do?typ=detail",
  	 			dataUrl:"<%=path%>/KlZongheFenxiMis/findInventoryAgingSum.do?typ=detail",
  	 			title:'库龄综合分析-物资明细',
  	 			id:"dataDetail",
  	 			height:tableHeight/2
  	 		});
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/KlZongheFenxiMis/toColChooseInventoryAgingSum.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  		//生成表格
  	 	function builtTable(params){
  	 		firstLoad = true;
  	 		var headUrl = params.headUrl;
  	 		var contentUrl = params.dataUrl;
  	 		var title = params.title;
  	 		var grid = $('#'+params.id);
  	 		var dateCols = params.dateCols ? params.dateCols : [];
  	 		var numCols = params.numCols ? params.numCols : [];
  	 		var filter = typeof(params.filter) == 'function' ? params.filter : function(data){return data;};
  	 		var singleSelect = params.singleSelect ? params.singleSelect : true;
  	 		var pagination = params.pagination ? params.pagination : true;
  	 		var showFooter = params.showFooter ? params.showFooter : true;
  	 		var pageList = params.pageList ? params.pageList : [10,20,30,40,50];
  	 		var gridHeight = params.height ? params.height : tableHeight;
  	 		var onClickRow = params.onClickRow ? params.onClickRow : function(a,b){return;};
  	 		
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({type:"POST",
  	 				url:headUrl,
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 			var Cols = [];
  	 			var colsSecond = [];
  	 		var prev = '';
  	 		var temp;
  	 			for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 			var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 			if(t && !t.length){
  	 				for(var i in tableContent.columns){
  	 					var align = $.inArray(tableContent.columns[i].columnName.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 		 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 		 			else
  	 		 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 		 		}
  	 			head.push(columns);
  	 		 		frozenHead.push(frozenColumns);
  	 			}else{
  	 				for(var i in tableContent.columns){
  	 					var align = $.inArray(tableContent.columns[i].columnName.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 					if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					else{
  	 						var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 						if(cur && cur.length){
  	 							var cur = tableContent.columns[i].zhColumnName;
  	 							if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 								temp.colspan ++;
  	 							}else{
  	 								temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 								columns.push(temp);
  	 								prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 							}
  	 							colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 						}else{
  	 							if(tableContent.columns[i].columnName)
  	 								columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 						}
  	 					}
  	 				}
  	 				head.push(columns);
  	 				colsSecond.push({field:'POSITN',title:'positn',rowspan:2,hidden:true});
  	 				head.push(colsSecond);
  	 				frozenHead.push(frozenColumns);
  	 			}
  	 	 		//生成报表数据表格
  	 			grid.datagrid({
  	 		 			title:title,
  	 		 			width:'100%',
  	 		 			height:gridHeight,
  	 		 			nowrap: true,
  	 					striped: true,
  	 					singleSelect:singleSelect,
  	 					collapsible:true,
  	 					loadFilter:function(data){
  	 						if(dateCols.length){
  	 							var row = data.rows;
  	 							for(var i in row){
  	 								for(var j in dateCols){
  	 									row[i][j] = convertDate(row[i][j] ? Number(row[i][j]) : 0);
  	 								}
  	 							}
  	 						}
  	 		 				return filter(data);
  	 					},
  	 					url:contentUrl,
  	 					remoteSort: true,
  	 					//页码选择项
  	 					pageList:pageList,
  	 					frozenColumns:frozenHead,
  	 					columns:head,
  	 					queryParams:params,
  	 					showFooter:showFooter,
  	 					pagination:pagination,
  	 					rownumbers:true,
  	 					fitColumns:false,
  	 					onClickRow:onClickRow,
  	 					rowStyler:function(){
  	 						return 'line-height:11px';
  	 					},
  	 					onBeforeLoad:function(){
  	 						if(firstLoad){
  	 							firstLoad = false;
  	 							return false;
  	 						}	
  	 					}
  	 		 	});
  	 		 	$(".panel-tool").remove();
  	 	}
  	 </script>
</body>
</html>
