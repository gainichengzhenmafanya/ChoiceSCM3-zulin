<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MIS-查看分店上传</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.bgBlue{
			background-color:lightblue;
		}
		</style>					
	</head>
	<body>
	<div class="tool"></div>
	<div class="form">
		<form id="SearchForm" action="<%=path%>/firmMis/searchUploadByDate.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="date"/></div>
				<div class="form-input" style="width:300px;">
						<input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${curDate}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker()"/>
				</div>
			</div>
		</form>
		<form id="listForm" action="<%=path%>/chkstom/list.do" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width:25px;text-align:center;">&nbsp;</span></td>
								<td style="width:120px;"><fmt:message key="branch_name"/></td>
								<td style="width:80px;"><fmt:message key="whether_upload"/></td>
								<td style="width:80px;"><fmt:message key="initial_time"/></td>
								<td style="width:100px;"><fmt:message key="last_time"/></td>
								<td style="width:80px;"><fmt:message key="documents_num"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${positnList }" varStatus="status">
								<c:forEach var="chkstom" items="${chkstomList }">
									<c:if test="${positn.code == chkstom.firm }">
										<tr>
											<td ><span class="num" style="width:25px;text-align:center;"></span></td>
											<td ><span style="width:110px;">${positn.des }</span></td>
											<td title=""><span style="width:70px;"><fmt:message key="uploaded"/></span></td>
											<td title=""><span style="width:70px;">${chkstom.bMad}</span></td>
											<td title=""><span style="width:90px;">${chkstom.eMad}</span></td>
											<td title=""><span style="width:70px;">${chkstom.chkCount }</span></td>
										</tr>
									</c:if>
								</c:forEach>
							</c:forEach>
							<c:forEach var="positn" items="${noUpList }">
								<tr>
									<td ><span class="num" style="width:25px;text-align:center;"></span></td>
									<td ><span style="width:110px;"><font color="red">${positn.des }</font></span></td>
									<td title=""><span style="width:70px;"><font color="red"><fmt:message key="unuploaded"/></font></span></td>
									<td title="" ><span style="width:70px;"></span></td>
									<td title="" ><span style="width:90px;"></span></td>
									<td title="" ><span style="width:70px;"><font color="red">0</font></span></td>
								</tr>
							</c:forEach>							
						</tbody>
					</table>
				</div>
			</div>		
		</form>
	</div>
	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: parent.$('.<fmt:message key="view_branch_upload_data"/>').click(); break;
	            }
			});  
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法	
		});
		var tool = $('.tool').toolbar({
			items: [{
				text: '<fmt:message key="view"/>',
				title: '<fmt:message key="view_upload"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					$("#SearchForm").submit();
				}
			},{
				text: '<fmt:message key="quit"/>',
				title: '<fmt:message key="quit"/>',
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					parent.$('.close').click();
				}
			}]
		});
		var num=0;
		$('.grid').find('.table-body').find('.num').each(function (){
			$(this).text(Number(num)+1);
			num++;
		});
		</script>				
	</body>
</html>