<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_messages_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 90px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			} 
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/firmMis/findSafeChkstom.do" method="post">
			<div class="form-line">
 				<div class="form-label" style="width:80px;margin-left:30px;"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
				<div class="form-input" style="width:205px;">
					<input type="text" value="${positn.des }" id="firmDes" name="firmDes"/>
					<input type="hidden" value="${positn.code }" id="firm" name="firm"/>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 46px;"><span >&nbsp;</span></td>
								<td colspan="4"><fmt:message key="supplies"/></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="minimum_inventory" /></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="current_inventory" /></span></td>
								<td rowspan="2"><span style="width:70px;">在途</span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="computation" /></span></td>
								<td colspan="2"><fmt:message key="procurement_unit" /></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="adjustment_amount" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr data-mincnt="${chkstod.supply.mincnt}" data-unitper3="${chkstod.supply.unitper3 }"
								data-sp_price="${chkstod.supply.sp_price }">
								<td align="center" style="width:46px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:100px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:50px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.cntmin}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.cntnow}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.cntway}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.cntmin - chkstod.cntnow - chkstod.cntway}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:50px;">${chkstod.supply.unit3 }</span></td>
								<td class="textInput">
									<span style="width:70px;text-align: right;">
										<input type="text" onfocus="this.select()" onkeyup="validate(this)" onblur="validate2(this)" name='input' style='border:0;width:69px;text-align:right;' value="<fmt:formatNumber value="${chkstod.cntuse}" type="currency" pattern="0.00"/>"/>
									</span>
								</td>
							</tr>
						</c:forEach>
					</table>					
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
			loadToolBar(true);
			
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			
// 			new tabTableInput("table-body","text"); //input  上下左右移动
			//监听键盘点击事件
			$('input[type="text"]').keydown(function(e) {               
		 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
		 		var event = $.event.fix(e);
		 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
		 		if (event.keyCode == 13) {
		 			$(event.target).parents('tr').next().find('input').focus();
		 		}
			}); 
		});
		
		function loadToolBar(use){
			$('.tool').html('');
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						if(""==$('#firm').val()){
							alert('<fmt:message key="please_select_positions" />！');
							return;
						}
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="generated_newspaper_manifest" />',
					title: '<fmt:message key="generated_newspaper_manifest" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')} && use,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if(""==$('#firm').val()){
							alert('<fmt:message key="please_select_positions" />！');
							return;
						}
						save();
					}
				},{
//						text: 'excel',
//						title: 'excel',
//						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
//						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
//							position: ['-140px','-100px']
//						},
//						handler: function(){
//							excel();
//						}
//					},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		}
		
		// 鼠标离开时检查数量输入格式是否合法
		function validate(e){
	    	if(Number(e.value)<0){
	    		alert('<fmt:message key="number_be_not_valid_number"/>！');//数量不能是负数
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			}
		}
		
		// 鼠标离开时检查数量输入格式是否合法
		function validate2(e){
	    	if(Number(e.value)<0){
	    		alert('<fmt:message key="number_be_not_valid_number"/>！');//数量不能是负数
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(Number($(e).parents('tr').data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(e.value)/Number($(e).parents('tr').data("mincnt")))) && Number(e.value) != 0){
// 				alert('该物质最低申购数量为'+$(e).parents('tr').data("mincnt")+"，申购量必须是最小申购量的整数倍!");
				alert('<fmt:message key="the_minimum_purchase_quantity_of_material"/>'+$(e).parents('tr').data("mincnt")+'<fmt:message key="subscriptions_must_be_an_integer_multiple_of_the_minimum_purchase_amount"/>!')
				e.value=e.defaultValue;
				e.focus();
				return;
			}
		}
		
		function save(){
			var numNull=0;//默认0代表成功
			var sp_name = '';
			var data = {};
			var i = 0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(10) input').val();
					if(Number(amount)==0){
						return true;
					}
					if(amount=="" || amount==null || isNaN(amount)){
						numNull=2;//2数量为空
						sp_name = $(this).find('td:eq(2)').text();
						return false;
					}
					if(Number(amount) != 0){
						data["chkstodList["+i+"].supply.sp_code"] = $(this).find('td:eq(1)').text();
						data["chkstodList["+i+"].amount1"] = amount;
						var unitper3 = Number($(this).data('unitper3'));
						if(unitper3 == 0){
							data["chkstodList["+i+"].amount"] = 0;
						}else{
							data["chkstodList["+i+"].amount"] = Number(amount)/unitper3;
						}
						data["chkstodList["+i+"].price"] = Number($(this).data('sp_price'));
						data["chkstodList["+i+"].hoped"] = getInd();
						i++;
					}
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key="not_add_empty_document"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('物资：['+sp_name +']<fmt:message key="number_be_not_valid_number"/>！');
				return;
			}
			
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			$("#wait2").show();
			$("#wait").show();
			//提交并返回值，判断执行状态
			$.post("<%=path%>/firmMis/saveBySafeChkstom.do",data,function(data){
				$("#wait2").hide();//等待加载还原
				$("#wait").hide();
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					break;
				case 1:
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 1500
								});
					loadToolBar(false);
					break;
				}
			});	
		}
		
		function excel(){
			
		}
		
		//获取系统时间+1天
		function getInd(){
			var myDate=new Date(new Date().getTime() + 24*60*60*1000);  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		</script>				
	</body>
</html>