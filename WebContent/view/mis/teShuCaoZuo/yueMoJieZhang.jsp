<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>月末结账</title>
	</head>
	<body>
	<div style="text-align:center;color:red;font-size:30px;margin-top:50px;">
		<fmt:message key="month_end_closing_relates_to_data_is_more_important_please_be_careful_of"/>！！！
	</div>
	<input type="hidden" id="action" name="action" value="${action }"/>
	<input type="hidden" id="num" name="num" value="${num }"></input>
	<input type="hidden" id="monthh" name="monthh" value="${monthh }"></input>
	<form id="listForm" action="<%=path%>/firmMis/toYueMoJieZhang.do" method="post">
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			if("init"==$('#action').val()){
				if(confirm("<fmt:message key='to_determine_the_month_end_closing_it'/>？")){
 					$('#listForm').submit();
 				}else{
 					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
 				}
			}else{
				if($('#num').val()>0){
					alert('<fmt:message key="the_system_is_the_inventory"/>....');
				}else if(0==$('#num').val() && 12>=$('#monthh').val()){
					alert('【'+$('#monthh').val()+'月】月末结转完成！');	
				}else{
					alert('<fmt:message key="carryover_month_cannot_be_greater_than"/>！');
				}				
				invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			}
		});
	</script>
	</body>
</html>