<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>年终结转</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		p span input{
			width: 90px;
		}
		a.l-btn-plain{
			border:1px solid #7eabcd; 
			text-align: center;
		}
		#loading{width:295px; height:22px;border-style:groove;text-align:center;}
		#loading div{height:22px;text-align:center;}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3">
					<fieldset style="margin-left:10px;margin-top:5px;">
						<p><span>
							<font color="blue" size="3">【物流年终结转】&nbsp;</font><font color="red" size="3">会计期由【${main.yearr }】年--->【${main.yearr+1 }】年</font>&nbsp;
							<a id="jieZhuan" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:jieZhuan();"><font color="blue">开始年终结转</font></a>
						</span></p>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td rowspan="2">
					<fieldset style="width:300px;height:350px;margin-left:10px;text-align:center;">
						<legend style="text-align:left;">&lt;结转前年份会计期[<font color="blue">${main.yearr }</font>]--可调整&gt;</legend>
						<p><span>1月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat1" name="bdat1" class="Wdate text" value="<fmt:formatDate value="${main.bdat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat1\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat1" name="edat1" class="Wdate text" value="<fmt:formatDate value="${main.edat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat1\')}'});"/>
						</span></p>
						<p><span>2月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat2" name="bdat2" class="Wdate text" value="<fmt:formatDate value="${main.bdat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat2\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat2" name="edat2" class="Wdate text" value="<fmt:formatDate value="${main.edat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat2\')}'});"/>
						</span></p>
						<p><span>3月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat3" name="bdat3" class="Wdate text" value="<fmt:formatDate value="${main.bdat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat3\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat3" name="edat3" class="Wdate text" value="<fmt:formatDate value="${main.edat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat3\')}'});"/>
						</span></p>
						<p><span>4月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat4" name="bdat4" class="Wdate text" value="<fmt:formatDate value="${main.bdat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat4\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat4" name="edat4" class="Wdate text" value="<fmt:formatDate value="${main.edat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat4\')}'});"/>
						</span></p>
						<p><span>5月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat5" name="bdat5" class="Wdate text" value="<fmt:formatDate value="${main.bdat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat5\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat5" name="edat5" class="Wdate text" value="<fmt:formatDate value="${main.edat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat5\')}'});"/>
						</span></p>
						<p><span>6月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat6" name="bdat6" class="Wdate text" value="<fmt:formatDate value="${main.bdat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat6\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat7" name="edat7" class="Wdate text" value="<fmt:formatDate value="${main.edat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat6}\')}'});"/>
						</span></p>
						<p><span>7月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat8" name="bdat8" class="Wdate text" value="<fmt:formatDate value="${main.bdat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat7\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat8" name="edat8" class="Wdate text" value="<fmt:formatDate value="${main.edat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat7}\')}'});"/>
						</span></p>
						<p><span>8月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat9" name="bdat9" class="Wdate text" value="<fmt:formatDate value="${main.bdat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat8\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat9" name="edat9" class="Wdate text" value="<fmt:formatDate value="${main.edat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat8\')}'});"/>
						</span></p>
						<p><span>9月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat10" name="bdat10" class="Wdate text" value="<fmt:formatDate value="${main.bdat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat9\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat10" name="edat10" class="Wdate text" value="<fmt:formatDate value="${main.edat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat9\')}'});"/>
						</span></p>
						<p><span>10月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat11" name="bdat11" class="Wdate text" value="<fmt:formatDate value="${main.bdat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat10\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat11" name="edat11" class="Wdate text" value="<fmt:formatDate value="${main.edat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat10\')}'});"/>
						</span></p>
						<p><span>11月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat12" name="bdat12" class="Wdate text" value="<fmt:formatDate value="${main.bdat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat11}\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat12" name="edat12" class="Wdate text" value="<fmt:formatDate value="${main.edat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat11}\')}'});"/>
						</span></p>	
						<p><span>12月：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat1" name="bdat1" class="Wdate text" value="<fmt:formatDate value="${main.bdat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat12\')}'});"/>
							--
							<input type="text"  autocomplete="off" id="edat1" name="edat1" class="Wdate text" value="<fmt:formatDate value="${main.edat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat12\')}'});"/>
						</span></p>											
					</fieldset>
				</td>
				<td rowspan="2">
					<fieldset style="width:300px;height:350px;text-align:center;">
						<legend style="text-align:left;">&lt;结转后年份会计期[<font color="blue">${main.yearr+1 }</font>]--可调整&gt;</legend>
							<p><span>1月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat1" name="bdat1" class="Wdate text" value="<fmt:formatDate value="${bdat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat1\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat1" name="edat1" class="Wdate text" value="<fmt:formatDate value="${edat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat1\')}'});"/>
							 </span></p>
							<p><span>2月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat2" name="bdat2" class="Wdate text" value="<fmt:formatDate value="${bdat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat2\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat2" name="edat2" class="Wdate text" value="<fmt:formatDate value="${edat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat3\')}'});"/>
							 </span></p>
							<p><span>3月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat3" name="bdat3" class="Wdate text" value="<fmt:formatDate value="${bdat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat3\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat3" name="edat3" class="Wdate text" value="<fmt:formatDate value="${edat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat3\')}'});"/>
							 </span></p>
							<p><span>4月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat4" name="bdat4" class="Wdate text" value="<fmt:formatDate value="${bdat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat4\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat4" name="edat4" class="Wdate text" value="<fmt:formatDate value="${edat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat4\')}'});"/>
							 </span></p>
							<p><span>5月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat5" name="bdat5" class="Wdate text" value="<fmt:formatDate value="${bdat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat5\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat5" name="edat5" class="Wdate text" value="<fmt:formatDate value="${edat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat5\')}'});"/>
							 </span></p>
							<p><span>6月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat6" name="bdat6" class="Wdate text" value="<fmt:formatDate value="${bdat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat6\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat6" name="edat6" class="Wdate text" value="<fmt:formatDate value="${edat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat6\')}'});"/>
							 </span></p>
							<p><span>7月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat7" name="bdat7" class="Wdate text" value="<fmt:formatDate value="${bdat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat7\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat7" name="edat7" class="Wdate text" value="<fmt:formatDate value="${edat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat7\')}'});"/>
							 </span></p>
							<p><span>8月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat8" name="bdat8" class="Wdate text" value="<fmt:formatDate value="${bdat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat8\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat8" name="edat8" class="Wdate text" value="<fmt:formatDate value="${edat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat8\')}'});"/>
							 </span></p>
							<p><span>9月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat9" name="bdat9" class="Wdate text" value="<fmt:formatDate value="${bdat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat9\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat9" name="edat9" class="Wdate text" value="<fmt:formatDate value="${edat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat9\')}'});"/>
							 </span></p>
							<p><span>10月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat10" name="bdat10" class="Wdate text" value="<fmt:formatDate value="${bdat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat10\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat10" name="edat10" class="Wdate text" value="<fmt:formatDate value="${edat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat10\')}'});"/>
							 </span></p>
							<p><span>11月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat11" name="bdat11" class="Wdate text" value="<fmt:formatDate value="${bdat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat11\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat11" name="edat11" class="Wdate text" value="<fmt:formatDate value="${edat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat11\')}'});"/>
							 </span></p>
							<p><span>12月：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat12" name="bdat12" class="Wdate text" value="<fmt:formatDate value="${bdat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat12\')}'});"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat12" name="edat12" class="Wdate text" value="<fmt:formatDate value="${edat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat12\')}'});"/>
							 </span></p>							 
					</fieldset>
				</td>
				<td>
					<fieldset style="width:300px;height:270px;text-align:center;">
						<legend style="text-align:left;">&lt;结转监视窗口&gt;</legend>
						<textarea id="listInfo" rows="15" cols="32" style="resize:none;" readonly="readonly"></textarea>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset style="width:300px;height:75px;text-align:center;">
						<label style="color:red;font-size:20px;">请谨慎年终结转！！！</label>
						<div id="message"><font size="3">0%</font></div>
						<div id="loading"><div id="percent"></div></div>
					</fieldset>
				</td>
			</tr>
		</table>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		var progress_id = "loading";
		var str="监视信息\n";
		function SetProgress(progress) {
			if (progress) {
				$("#" + progress_id + " > div").css("width",String(progress) + "%"); //控制#loading div宽度
  				$("#" + progress_id + " > div").css("background-color","royalblue");
// 				$("#" + progress_id + " > div").html(String(progress) + "%"); //显示百分比
  				$("#message").html("<font size='3'>"+String(progress) + "%</font>");
			}
		}
		var i = 0;
		function doProgress() {
			if (i > 100) {
				$("#message").html("<font size='3' color='blue'>年终结转完成！</font>").fadeIn("slow");//加载完毕提示
				return;
			}
			if (i <= 100) {
				$('#listInfo').val(i);
				str+=$('#listInfo').val()+"\n";
				$('#listInfo').val(str);
				document.getElementById("listInfo").scrollTop=document.getElementById("listInfo").scrollHeight;
				setTimeout("doProgress()",100);
				SetProgress(i);
				i++;
			}
		}
		//工具栏
		$(document).ready(function(){
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		function jieZhuan(){
			$("#jieZhuan").attr("disabled","disabled");
			doProgress();
		}
		</script>			
	</body>
</html>