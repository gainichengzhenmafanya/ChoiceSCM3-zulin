<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="generated_newspaper_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css"> 	
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
		</style>
	</head>
	<body>  
		<form id="listForm" method="post" action="<%=path %>/inspection/saveChkstomZP.do">
			<input type="hidden" class="text" id="ids" name="ids" value="${ids}" />
 			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" style="width: 25px;">&nbsp;</td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="storage_positions"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="storage"/><fmt:message key="price"/></span></td>
								<td colspan="4"><span><fmt:message key="receive_goods"/></span></td>
								<td colspan="4"><span><fmt:message key="storage"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span  style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span  style="width:40px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								
								<td><span  style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span  style="width:40px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td><span  style="width:50px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="product" items="${disList}" varStatus="status">
								<tr data-unitper="${product.unitper}" data-sp_code="${product.sp_code }" data-id="${product.id }" data-chkstono="${product.chkstoNo }"
									data-dued="<fmt:formatDate value="${product.dued }" pattern="yyyy-MM-dd" type="date"/>" data-pcno="${product.pcno }"  data-delivercode="${product.deliverCode }">
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td><span title="${product.sp_code}" style="width:80px;text-align: left;">${product.sp_code}&nbsp;</span></td>
									<td><span title="${product.sp_name}" style="width:100px;text-align: left;">${product.sp_name}&nbsp;</span></td>
									<td><span title="${product.sp_desc}" style="width:60px;text-align: right;">${product.sp_desc}&nbsp;</span></td>
									<td><span title="${product.firmCode}" style="width:100px;text-align: left;">${product.firmDes}&nbsp;</span></td>
									<td><span title="${product.pricein}" style="width:60px;text-align: right;">
										<fmt:formatNumber value="${product.pricein}" pattern="##.##" minFractionDigits="2" />
									&nbsp;</span></td>
									<td><span title="${product.unit}" style="width:40px;text-align: left;">${product.unit}&nbsp;</span></td>
									<td><span title="${product.amountin}" style="width:50px;text-align: right;"><c:if test="${product.amountin != 0 }"><fmt:formatNumber value="${product.amountin}" pattern="##.##" minFractionDigits="2" /></c:if>&nbsp;</span></td>
									<td><span title="${product.unit1}" style="width:40px;text-align: left;">${product.unit1}&nbsp;</span></td>
									<td><span title="${product.amount1in}" style="width:50px;text-align: right;"><c:if test="${product.amount1in != 0 }"><fmt:formatNumber value="${product.amount1in}" pattern="##.##" minFractionDigits="2" /></c:if>&nbsp;</span></td>
									
									<td><span title="${product.unit}" style="width:40px;text-align: left;">${product.unit}&nbsp;</span></td>
									<td class="textInput">
										<span style="width:60px;padding: 0px;text-align:center;">
											<input title="<fmt:formatNumber value="${product.amountin}" type="currency" pattern="0.00"/>" type="text" class="nextclass" style="text-align: right;width: 55px;" 
												value="<fmt:formatNumber value="${product.amountin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onkeyup="validate(this);"/>
										</span>
									</td>
									<td><span title="${product.unit1}" style="width:40px;text-align: left;">${product.unit1}&nbsp;</span></td>
									<td class="textInput">
										<span title="" style="width:60px;padding: 0px;text-align:center;">
											<input type="text" value="<fmt:formatNumber value="${product.amount1in}" pattern="##.##" minFractionDigits="2" />" onfocus="this.select()" onkeyup="validate(this);" style="width:55px;text-align:right;"/>
										</span>
									</td>
									<td class="textInput">
										<span title="" style="width:80px;padding: 0px;">
											<input type="text" value="" onfocus="this.select()"/>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			new tabTableInput("table-body","text"); //input  上下左右移动
		});
		
		function validate(e){
	    	if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			if($(e).parents("td").index()==13){
				if(Number($(e).parents('tr').data("unitper")) != 0){//转换率不为0的才能这么转换wjf
					$(e).parents('tr').find('td:eq(11)').find('input').val((Number($(e).parents('tr').find('td:eq(13)').find('input').val())/Number($(e).parents('tr').data("unitper"))).toFixed(2));
					$(e).parents('tr').find('td:eq(11)').find('span').attr('title',(Number($(e).parents('tr').find('td:eq(13)').find('input').val())/Number($(e).parents('tr').data("unitper"))).toFixed(2));
				}
			}else if($(e).parents("td").index()==11){
				 $(e).parents('tr').find('td:eq(13)').find('input').val((Number($(e).parents('tr').find('td:eq(11)').find('input').val())*Number($(e).parents('tr').data("unitper"))).toFixed(2));
				 $(e).parents('tr').find('td:eq(13)').find('span').attr('title',(Number($(e).parents('tr').find('td:eq(11)').find('input').val())*Number($(e).parents('tr').data("unitper"))).toFixed(2));
			 }
		}
		
		//确定修改并生成报货单
		function saveChkinmGroup(){ 
			var checkboxList = $('.grid').find('.table-body').find('tr');
			if(checkboxList && checkboxList.size() > 0){
				//0.判断档口是不是做了期初
				var flag1 = true;
				var dept = "";
				var goQc = true;//是否查期初
				//2.判断验货数和到货数是不是相符
				var first_id = 0;//第一个物资编码
				var first_dh = 0;//用来验证验货数和到货数是不是相符
				var first_yh = 0;
				var flag2 = true;
				var sp_name = '';
				checkboxList.each(function(i){
					var id = $(this).data('sp_code');//物资单号
					var dh = $(this).find('td:eq(7)').find('span').attr('title');//到货
					var yh = $(this).find('td:eq(11)').find('span').find('input').val();//验货
					if(first_id == id || id == ''){
						first_yh = Number(first_yh) + Number(yh);
					}
					if((first_id != id && id != '') || i == checkboxList.length-1){//如果同一个物资循环完了 或者最后一个了
						if(Number(first_dh).toFixed(2) != Number(first_yh).toFixed(2)){
							flag2 = false;
							return false;
						}
						if(first_id != 0){
							goQc = false;
						}
						first_id = id;
						first_dh = dh;
						first_yh = yh;
						sp_name = $(this).find('td:eq(2)').find('span').text()?$(this).find('td:eq(2)').find('span').text():"";
					}
					//判断有没有做期初
					if(goQc){
						dept = $(this).find('td:eq(4)').find('span').attr('title');
						$.ajax({
							url:"<%=path%>/positn/checkQC.do?code="+dept,
							type:"post",
							success:function(data){
								if(!data){
									flag1 = false;
									return false;
								}
							}
						});
					}
				});
				if(!flag1){
					alert(dept+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>!<fmt:message key="can_not"/><fmt:message key="storage"/>!');
					return;
				}
				if(!flag2){
					alert('<fmt:message key="supplies"/>：['+sp_name+']<fmt:message key="receive_goods"/><fmt:message key="quantity"/>和<fmt:message key="storage"/><fmt:message key="quantity"/>不相符！\n<fmt:message key="receive_goods"/>：'+Number(first_dh).toFixed(2)+',<fmt:message key="storage"/>：'+Number(first_yh).toFixed(2));
					return;
				}
				var selected = {};
				var sp_code = '';
				var price = 0;
				checkboxList.each(function(i){
					if($(this).data('sp_code')){
						sp_code = $(this).data('sp_code');
						price = $(this).find('td:eq(5)').find('span').attr('title');
					}
					selected['chkindList['+i+'].supply.sp_code'] = sp_code;
					selected['chkindList['+i+'].supply.sp_name'] = $(this).find('td:eq(2)').find('span').attr('title');//用来判断走档口直发还是走门店入库
					selected['chkindList['+i+'].indept'] = $(this).find('td:eq(4)').find('span').attr('title');
					selected['chkindList['+i+'].price'] = price;
					selected['chkindList['+i+'].amount'] = $(this).find('td:eq(11)').find('input').val();
					selected['chkindList['+i+'].memo'] = $(this).find('td:eq(14)').find('input').val();//备注
					selected['chkindList['+i+'].amount1'] = $(this).find('td:eq(13)').find('input').val();
					selected['chkindList['+i+'].dued'] = $(this).data('dued');
					selected['chkindList['+i+'].pcno'] =  $(this).data('pcno');
					selected['chkindList['+i+'].chkstono'] = $(this).data('chkstono')?$(this).data('chkstono'):0;
					selected['chkindList['+i+'].deliver'] =  $(this).data('delivercode');
				});
				var result='parent';
				selected['ids'] = $('#ids').val();
				$.post('<%=path%>/firmMis/saveChkinmGroupZP.do',selected,function(data){
				 	if (1!=data) {//弹出提示信息
				 		alert('<fmt:message key="operation_failed"/>！');
				 	}else{
			    		alert('<fmt:message key="operation_successful"/>！');
			    	}
					parent.pageReload(result);
				});
			}else{
				alert('<fmt:message key="please_select_materials"/>！');
				return ;
			}
		}
		</script>
	</body>
</html>