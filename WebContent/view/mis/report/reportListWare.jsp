<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width: 110px;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
  	</style>
  </head>	
  <body>
  	<div style="width: 630px;">
  		<a href="<%=path %>/RkMingxiChaxunMis/toChkinDetailS.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_query"/></a>
  		<a href="<%=path %>/RkMingxiHuizongMis/toChkinDetailSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_summary"/></a>
  		<a href="<%=path %>/RkHuizongChaxunMis/toChkinSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_aggregate_query"/></a>
  		<a href="<%=path %>/RkLeibieHuizongMis/toChkinCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_category_summary"/></a>
  		<a href="<%=path %>/RkZongheChaxunMis/toChkinmSynQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_comprehensive_inquiry"/></a>
  	</div>
  	<div style="width: 760px;">
  		<a href="<%=path %>/CkMingxiChaxunMis/toChkoutDetailQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="details_of_the_library_inquiry"/></a>
  		<a href="<%=path %>/CkHuizongChaxunMis/toChkoutSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="aggregate_query_library"/></a>
  		<a href="<%=path %>/CkLeibieHuizongMis/toChkoutCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">出库类别汇总</a>
  	</div>
  	<div style="width: 500px;">
<%--   		<a href="<%=path %>/GysFukuanQingkuangMis/toDeliverPayment.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="vendor_payments_situation"/></a> --%>
  		<a href="<%=path %>/GysJinhuoHuizongMis/toDeliverStockSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="suppliers_purchase_summary"/></a>
  		<a href="<%=path %>/LeibieGysHuizongMis/toCategoryDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/></a>
  		<a href="<%=path %>/LeibieGysHuizongMis/toCategoryDeliverSum1.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/>1</a>
  	</div>
  	<div style="width: 890px;">
  		<a href="<%=path %>/GysLeibieHuizongMis/toDeliverCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="supplier_category_summary"/></a>
  		<a href="<%=path %>/GysHuizongLiebiaoMis/toDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="summary_list_of_suppliers"/></a>
<%--   		<a href="<%=path %>/JhDanjuHuizongMis/toStockBillSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="purchase_invoices_summary"/></a> --%>
  	</div>
  	<div style="width: 890px;">
  		<a href="<%=path %>/WzMingxiZhangMis/toSupplyDetailsInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_ledger"/></a>
  		<a href="<%=path %>/WzYueChaxunMis/toSupplyBalance.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_balance_inquiry"/></a>
  		<a href="<%=path %>/WzMingxiJinchuMis/toSupplyInOutInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_detail_out_of_the_table"/></a>
  		<a href="<%=path %>/WzLeibieJinchubiaoMis/toSupplyTypInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_category_out_of_the_table"/></a>
  	</div>
  	<div style="width: 890px;">
  		<%-- <a href="<%=path %>/firmMis/toDeptSupplyInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">部门物资进出表</a>
  		<a href="<%=path %>/prdPrcCostManage/toCostVariance.do?checkMis=mis" class="easyui-linkbutton" plain="true" iconCls="icon-redo">部门成本差异表</a>
  		<a href="<%=path %>/SupplyAcct/toGrossProfitList.do?checkMis=mis" class="easyui-linkbutton" plain="true" iconCls="icon-redo">分店销售利润表</a>
  		<a href="<%=path %>/firmMis/toFirmFoodProfit.do?checkMis=mis" class="easyui-linkbutton" plain="true" iconCls="icon-redo">分店菜品利润表</a> --%>
  		<a href="<%=path %>/WzZongheJinchubiaoMis/toSupplySumInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_integrated_out_of_the_table"/></a>
  		<a href="<%=path %>/WzCangkuJinchubiaoMis/toGoodsStoreInout.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="materials_warehouse_and_out_of_form"/></a>
  	</div>
  	
  	
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  		/* function show(url){
  			window.location.href = url;
  		} */
  		$('a').click(function(event){
  			var self = $(this);
  			if(!self.attr("tabid")){
  				self.attr("tabid",self.text()+new Date().getTime());
  			}
  			event.preventDefault();
  			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
  		});
  	</script>
  </body>
</html>
