<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
						<input type="text" name="positn_name" id="positn_name" style="width: 126px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${supplyAcct.positn}" />"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
						<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn }"/>
						<input type="hidden" id="positn1" name="positn1" value="${supplyAcct.positn }"/>
					</div>
					
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
					</div>
					<div class="form-label" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>><fmt:message key="document_types"/></div>
					<div class="form-input" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>><select id="chktyp" name="chktyp" class="select"></select></div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
<%-- 					<div class="form-label" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>><fmt:message key="positions"/></div> --%>
<%-- 					<div class="form-input" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>> --%>
<!-- 					<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/> -->
<!-- 					<input type="hidden" id="positn" name="positn" value=""/> -->
<%-- 					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>'  /> --%>
<%-- 						<select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
						<input type="text"  id="deliver_name"  name="deliver_name" readonly="readonly" value="" class="text"/>
						<input type="hidden" id="delivercode" name="delivercode" value=""/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
						<%-- <select  name="delivercode" url="<%=path %>/deliver/findAllDeliver.do"   class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input"><select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
  	 		$('.tool').toolbar({
				items: [{
						id: 'select',
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/RkLeibieHuizongMis/exportChkinCategorySum.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[
  	 			  	 			            {key:'<fmt:message key="normal_storage"/>',value:'<fmt:message key="normal_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="overage_storage"/>',value:'<fmt:message key="overage_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="gifts_warehousing"/>',value:'<fmt:message key="gifts_warehousing"/>'},
  	 			  	 			            {key:'<fmt:message key="recycling_storage"/>',value:'<fmt:message key="recycling_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="consignment_warehousing"/>',value:'<fmt:message key="consignment_warehousing"/>'},
  	 			  	 			           	{key:'<fmt:message key="purchase_storage"/>',value:'<fmt:message key="purchase_storage"/>'},
  	 			  	 			        	{key:'<fmt:message key="normal_shipments"/>',value:'<fmt:message key="normal_shipments"/>'},
  	 			  	 			     		{key:'<fmt:message key="gifts_shipments"/>',value:'<fmt:message key="gifts_shipments"/>'},
  	 			  	 			  			{key:'<fmt:message key="consignment_shipments"/>',value:'<fmt:message key="consignment_shipments"/>'},
  	 			  	 						{key:'<fmt:message key="returns"/>',value:'<fmt:message key="returns"/>'},
  	 			  	 						{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
  	 			  	 			         ]);
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/RkLeibieHuizongMis/findChkinCategorySumHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data.columns;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 			columns.push({field:'TOTAL',title:'<fmt:message key="total"/>',width:100,align:'right'});
				for(var i in tableContent){
	  	 			columns.push({field:tableContent[i].code,title:tableContent[i].des,width:100,align:'right'});
	  	 		}
	  	 		frozenColumns.push({field:'DELIVER',title:'<fmt:message key="project"/>',width:200,align:'left'});
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="warehousing_category_summary"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				var curdeliver = rows[0].DELIVERDES;
					var curRow = {};
					var rowTotal = 0;
	 				for(var i in rows){
	 					if(rows[i].DELIVERDES != curdeliver){
	 						curRow['DELIVER'] = curdeliver;
	 						curRow['TOTAL'] = rowTotal.toFixed(2);
	 						modifyRows.push(curRow);
	 						curRow = {};
	 						rowTotal = 0;
	 						curdeliver = rows[i].DELIVERDES;
	 					}
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].code;
		 						if(rows[i].SP_TYPE == currentCol){
		 							curRow[currentCol] = rows[i].AMT.toFixed(2);
		 							rowTotal += Number(rows[i].AMT);
		 						} 
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 				}
	 				curRow['DELIVER'] = curdeliver;
					curRow['TOTAL'] = rowTotal.toFixed(2);
					modifyRows.push(curRow);
	 				rs.rows = modifyRows;
	 				
	 				var modifyFoot = [];
	 				var totalAll = 0;
	 				var foot = rs.footer;
	 				var ft = {};
	 				ft['DELIVER'] = '<fmt:message key="total"/>';
	 				for(var i in foot){
	 					ft[foot[i].SP_TYPE] = foot[i].AMT.toFixed(2);
	 					totalAll += foot[i].AMT;
	 				}
	 				ft['TOTAL'] = totalAll.toFixed(2);
	 				modifyFoot.push(ft);
	 				rs.footer = modifyFoot;
	 				rs.total = rs.rows.length;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/RkLeibieHuizongMis/findChkinCategorySum.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
				onDblClickRow:function(index,data){
					var grptypname = typeof($("#grptyp").data("checkedName"))!="undefined"?$("#grptyp").data("checkedName"):"";
  	 				var grpname = typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
					var params = {"deliverdes":data['DELIVER'],"bdat":$("#bdat").val(),"edat":$("#edat").val(),
  	 						"grptyp":$("#grptyp").data("checkedVal"),"grptypdes":grptypname,
  	 						"positn":$("#positn").val(),"positndes":$("#positn_name").val(),
  	 						"grp":$("#grp").data("checkedVal"),"grpdes":grpname,
  	 						"chktyp":chktypname};
					openTag("chkindetailS","入库明细查询","<%=path%>/RkMingxiChaxunMis/toChkinDetailS.do",params);
  	 			}	
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
			var t;
			$("#sp_code").keyup(function(event){
				if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
					return; //回车 ，上下 时不执行
				}
				   window.clearTimeout(t); 
				   t=window.setTimeout("ajaxSupply(\'sp_code\',\'<%=path%>\')",200);//延迟0.2秒
			});
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var defaultName = $("#deliver_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();
					var defaultName = $('#positn_name').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('positn');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/SupplyAcct/toColumnsChoose.do?reportName=${reportName}"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
