<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>inventory Info 盘点</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.search{
					margin-top:3px;
					cursor: pointer;
				}
				.page{
					margin-bottom: 25px;
				}
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
/* 				form .form-line .form-label{ */
/* 					width: 10%; */
/* 				} */
/* 				form .form-line .form-input{ */
/* 					width: 20%; */
/* 				} */
			</style>						
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" name="listForm" action="<%=path%>/misInventory/list.do"method="post">
		
		<input type="hidden" id="type" name="type" value="<c:out value="${type}" default="init"/>" />
		<input type="hidden" id="status" name="status" value="<c:out value="${inventory.positn.pd}" default="init"/>" /><!--status是否已盘点 y  n -->
		<input type="hidden" id="yearr" name="yearr" value="${inventory.yearr}" /><!-- 会计年 -->
		<input type="hidden" id="typ" value="${inventory.positn.typ}"/>
		<input type="hidden" id="ynusedept" value="${inventory.positn.ynUseDept}"/><!-- 是否有档口 有档口和总部盘点一样 没档口采用倒挤盘点 -->
		<div class="form-line">
			<div class="form-label"><span style="color:red;">*</span><fmt:message key="positions"/>：</div>
			<div class="form-input">
				<input type="text" class="text" id="positn_name" name="positn.des" readonly="readonly" value="${inventory.positn.des}"/>
				<input type="hidden" id="positn" name="positn.code" value="${inventory.positn.code}"/>
			</div>
			<div class="form-label" style="width: 50px;margin-left: 26px;"><fmt:message key="date"/>：</div>
			<div class="form-input">
				<input type="text" id="date" name="date" class="Wdate text" 
					<c:if test="${action != 'init' }">readonly="readonly"</c:if> <c:if test="${action == 'init' }">onfocus="WdatePicker();"</c:if>
				value="<fmt:formatDate value="${inventory.date}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-input" style="margin-left: 50px;margin-top:6px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" >
				<div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div>
			</div><div id="per" class="form-input" style="width:50px;">0</div>
		</div>
		<!-- 修改ie8下条件一行显示不全，换行显示 wjf-->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 25px;"></span></td>
								<td style="width:22px;" rowspan="2">
									<span><input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="shelves"/></span></td>
								<td rowspan="2"><span style="width:75px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="supplies_specifications"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="category"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="standard_unit"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="the_reference_unit"/></span></td>
								<td colspan="3"><span style="width:180px;"><fmt:message key="balances"/></span></td>
								<td colspan="2"><span style="width:120px;"><fmt:message key="scm_pandian"/></span></td>
								<td colspan="2"><span style="width:120px;"><fmt:message key="scm_yingkui"/></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="scm_standard_quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_the_reference_number"/></span></td>
								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_standard_quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_the_reference_number"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_standard_quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_the_reference_number"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="inventory" items="${inventoryList}" varStatus="status">
								<tr data-unitper="${inventory.supply.unitper }">
									<td class="num" ><span style="width:25px;">${status.index+1}</span></td>
									<td style="width:22px; text-align: center;">
										<span><input type="checkbox" name="idList" id="chk_${inventory.supply.sp_code}" value="${inventory.supply.sp_code}"/></span>
									</td>
									<td><span title="${inventory.supply.positn1}" style="width:60px;">${inventory.supply.positn1}&nbsp;</span></td>
									<td><span title="${inventory.supply.sp_code}" style="width:75px;">${inventory.supply.sp_code}&nbsp;</span></td>
									<td><span title="${inventory.supply.sp_name}" style="width:80px;">${inventory.supply.sp_name}&nbsp;</span></td>
									<td><span title="${inventory.supply.sp_desc}" style="width:60px;">${inventory.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${inventory.supply.typothdes}" style="width:60px;">${inventory.supply.typothdes}&nbsp;</span></td>
									<td><span title="${inventory.supply.unit}" style="width:60px;">${inventory.supply.unit}&nbsp;</span></td>
									<td><span title="${inventory.supply.unit1}" style="width:60px;">${inventory.supply.unit1}&nbsp;</span></td>
									<td><span title="${inventory.cntbla}" style="width:60px;text-align:right;"><fmt:formatNumber value="${inventory.cntbla}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${inventory.cntubla}" style="width:60px;text-align:right;"><fmt:formatNumber value="${inventory.cntubla}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${inventory.amtbla}" style="width:60px;text-align:right;"><fmt:formatNumber value="${inventory.amtbla}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span title="${inventory.cnttrival}" style="width:70px;padding: 0px;">
											<input type="text" value="${inventory.cnttrival}" onfocus="this.select()" onkeyup="validate(this)" disabled="disabled" style="width:60px;padding: 0px;text-align:right;"/>
										</span>
									</td>
									<td class="textInput">
										<span title="${inventory.cntutrival}" style="width:70px;padding: 0px;">
											<input type="text" value="${inventory.cntutrival}" onfocus="this.select()" onkeyup="validate(this)" disabled="disabled" style="width:60px;padding: 0px;text-align:right;"/>
										</span>
									</td>
									<td><span title="" style="width:60px;text-align:right;"><fmt:formatNumber value="${inventory.cnttrival-inventory.cntbla}" type="currency" pattern="0.00"/>&nbsp;</span></td>
									<td><span title="" style="width:60px;text-align:right;"><fmt:formatNumber value="${inventory.cntutrival-inventory.cntubla}" type="currency" pattern="0.00"/>&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="smallClass"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" id="typDes" class="text"  name="supply.typdes" readonly="readonly" value="${inventory.supply.typdes}"/>
					<input type="hidden" id="sp_type" name="supply.sp_type" value="${inventory.supply.sp_type}"/>
					<img id="seachTyp" style="margin-top:1px;" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			    </div>
				<div class="form-label"><fmt:message key="coding"/>：</div>
				<div class="form-input" style="margin-top:-4px;">
					<input type="text" class="text" id="sp_code" name="supply.sp_code" value="${inventory.supply.sp_code}" />
	                <img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="查询物资" />
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 	});
			 	var status = $("#status").val();
			 	if(status == 'Y'){
					loadToolBar([false,true]);
			 	}else if(status == 'N'){
			 		loadToolBar([true,false]);
			 	}else{
			 		loadToolBar([false,false]);
			 	}
				function loadToolBar(use){
					$('.tool').html('');
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="select"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								
								handler: function(){
									var positnCode=$('#positn').val();
									if(positnCode==''){
										alert('<fmt:message key="please_select_positions"/>！');
										return;
									}else{
										$('#listForm').submit();
									}
								}
							},{
								text: '<fmt:message key="scm_change"/>',
								title: '<fmt:message key="scm_change"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','0px']
								},
								handler: function(){
									//change();
									var test=$('#autoId-button-102').find('.button-main').attr('title');
									if(test=='<fmt:message key="scm_change"/>'){
										 $('#autoId-button-102').find('.button-content').html('<fmt:message key="scm_lock"/>');
										 $('#autoId-button-102').find('.button-main').attr('title','<fmt:message key="scm_lock"/>');
										 $('.textInput').find('input').attr('disabled',false);
										 new tabTableInput("table-body","text"); //input  上下左右移动	
									}
									if(test=='<fmt:message key="scm_lock"/>'){
										 $('#autoId-button-102').find('.button-content').html('<fmt:message key="scm_change"/>');
										 $('#autoId-button-102').find('.button-main').attr('title','<fmt:message key="scm_change"/>');
										 $('.textInput').find('input').attr('disabled',true);
									}
								}
							},{
								text: '<fmt:message key="save"/>',
								title: '<fmt:message key="save"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-80px']
								},
								handler: function(){ 
									resolveInventory();
								}
							},{
								text: '<fmt:message key="end"/>',
								title: '<fmt:message key="end"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									//结束盘点谨慎操作，防止网络延迟，造成重复点击产生垃圾数据 wangjie 1.30
									$("body").find(".button:eq(3)").unbind();
 									endInventory();
								}
							},{
								text: '<fmt:message key="scm_start_pandian"/>',
								title: '<fmt:message key="scm_start_pandian"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')}&&use[1],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									$('#status').val('start');//开始盘点
									$('#listForm').submit();
								}
							},{
								text: '<fmt:message key="before_the_end_of_the_inventory_position"/>',
								title: '<fmt:message key="before_the_end_of_the_inventory_position"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									noInventoryPostin();
								}
							},{
								text: '<fmt:message key="all_inventory_is_equal_to_the_balance"/>',
								title: '<fmt:message key="all_inventory_is_equal_to_the_balance"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									allInventorycntbla();
								}
							},{
								text: '<fmt:message key="export" />',
								title: '<fmt:message key="export" />',
// 								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									exportInventory();
								}
							},{
								text: '<fmt:message key="blank_check_sheet" /><fmt:message key="print" />',
								title: '<fmt:message key="blank_check_sheet" /><fmt:message key="print" />',
// 								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									printInventory();
								}
							},{
								text: '<fmt:message key="quit" />',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}
						]
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				new tabTableInput("table-body","text"); //input  上下左右移动
// 				$('.table-body').width($('.table-head').find('table').width());
// 				$('.table-head').width($('.table-head').find('table').width());
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.textInput').find('input').live('click',function(event){
					var self = this;
					setTimeout(function(){
						self.select();
					},10);
				});
				$("select").each(function(){
	  	 			$(this).htmlUtils("select");
	  	 		});
				
				/*弹出树*/
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_type').val();
						var offset = getOffset('positn');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#sp_type'),'650','500','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
					}
				});

				$('.edit').bind('click', function(){
					var tdObj = $(this);
					$(this).html('<input id="editText" text="text" size="10" value="'+$(this).text()+'" name="name" onblur="onBlurMethod(this)"/>');
			    	$('#editText').focus();
				});
				
				/*********************延迟加载相关***************************/
				if('${action}'!='init' && '${currState}'!='1'){
					addTr('first');	
				}else if('${action}'!='init' && '${currState==1}'){
					$("#per").text('100%');
					$("#currState").width(200);
				}
				pageSize = '${pageSize}';
				var ndivHeight = $(".table-body").height();
				$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		        });
			});
			
			//变更 锁定
			function change(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var test=$('#autoId-button-102').find('.button-main').attr('title');
					if(test=='<fmt:message key="scm_change"/>'){
						 $('#autoId-button-102').find('.button-content').html('<fmt:message key="scm_lock"/>');
						 $('#autoId-button-102').find('.button-main').attr('title','<fmt:message key="scm_lock"/>');
						 checkboxList.filter(':checked').each(function(i){
							$(this).parents('tr').find('td:eq(12)').find('input').attr('disabled',false);
							$(this).parents('tr').find('td:eq(13)').find('input').attr('disabled',false);
						 });
						 new tabTableInput("table-body","text"); //input  上下左右移动	
					}
					if(test=='<fmt:message key="scm_lock"/>'){
						 $('#autoId-button-102').find('.button-content').html('<fmt:message key="scm_change"/>');
						 $('#autoId-button-102').find('.button-main').attr('title','<fmt:message key="scm_change"/>');
						 checkboxList.filter(':checked').each(function(i){
							$(this).parents('tr').find('td:eq(12)').find('input').attr('disabled',false);
							$(this).parents('tr').find('td:eq(13)').find('input').attr('disabled',false);
						 });
					}
				}else{
					alert('<fmt:message key="please_select"/><fmt:message key="supplies"/>！');
					return ;
				}
			}
			
		    function onBlurMethod(inputObj){
		    	$(inputObj).parent('span').text($(inputObj).val());
	    	}
			    
		    // 查找添加盘点模板
			function searchInvendemo(){
		    	var action = "<%=path%>/misInventory/addInvendemo.do?firm="+$('#positn').val();
				$('body').window({
					id: 'window_addInvendemo',
					title: '仓库盘点模板',
					content: '<iframe id="addInvendemoFrame" frameborder="0" src='+action+'></iframe>',
					width: '1000px',
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key ="enter" />',
								title: '<fmt:message key ="enter" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('addInvendemoFrame')){
										window.frames["addInvendemoFrame"].enterUpdate();
									}
								}
							},{
								text: '<fmt:message key ="cancel" />',
								title: '<fmt:message key ="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}]
					}
				});
			}
		  
			//保存盘点
			function resolveInventory(){
				var selected = {};
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="only_checked_saved_whether_continue"/>!')){
						selected['positn.code'] = $('#positn').val();
						selected['date'] = $('#date').val();
						var flag = true;//用来判断如果转换率是0的物资，如果改了标准数量，参考数量不能为0;或者反过来 wjf
						var flag1 = true;//判断如果转换率是0的物资 盘亏的标准和参考符号必须一致
						var sp_code = "";
						checkboxList.filter(':checked').each(function(i){
							if(Number($(this).parents('tr').data('unitper')) == 0){
								var cnttrival = $(this).parents('tr').find('td:eq(12)').find('input').val();
								var cntutrival = $(this).parents('tr').find('td:eq(13)').find('input').val();
								var cntcy = $(this).parents('tr').find('td:eq(14)').find('span').text();
								var cntucy = $(this).parents('tr').find('td:eq(15)').find('span').text();
								if((Number(cntcy) >= 0 && Number(cntucy) < 0) || (Number(cntcy) <= 0 && Number(cntucy) > 0)||(Number(cntcy) > 0 && Number(cntucy) <= 0) || (Number(cntcy) < 0 && Number(cntucy) >= 0)){//判断盘亏的标准单位和参考单位必须一致
									flag1 = false;
									sp_code = $(this).val();
									return;
								}
								if((Number(cnttrival) == 0 && Number(cntutrival) != 0) || (Number(cntutrival) == 0 && Number(cnttrival) != 0) ){
									flag = false;
									sp_code = $(this).val();
									return;
								}
							}
							selected['inventoryList['+i+'].supply.sp_code'] = $(this).val();
							selected['inventoryList['+i+'].cntbla'] = $(this).parents('tr').find('td:eq(9)').text();//结存标准数量
							selected['inventoryList['+i+'].cntubla'] = $(this).parents('tr').find('td:eq(10)').text();//结存标准数量
							selected['inventoryList['+i+'].cnttrival'] = $(this).parents('tr').find('td:eq(12)').find('input').val();
							selected['inventoryList['+i+'].cntutrival'] = $(this).parents('tr').find('td:eq(13)').find('input').val();
						});
						if(!flag1){
							alert('<fmt:message key="supplies"/>：['+sp_code+']<fmt:message key="generation_loss_standard_quantity_and_reference_number_must_be_consistent"/>！');
							return;
						}
						if(!flag){
							alert('<fmt:message key="supplies"/>：['+sp_code+']<fmt:message key="the_standard_inventory_quantity_and_reference_cannot_have_a"/>0。');
							return;
						}
						$.post('<%=path%>/misInventory/updateInventory.do',selected,function(data){
						 	if(data=="success"){
								showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 1000
								});	
								$('#listForm').submit();
						 	}else{
						 		showMessage({
									type: 'error',
									msg: '<fmt:message key="save_fail"/>！',
									speed: 1000
								});	
						 	}
						});
					}
				}else{
					alert('<fmt:message key="please_select_options_you_need_save"/>！');
					return ;
				}
			}
			//结束盘点
			function endInventory(){
				var msg = '<fmt:message key="prudent_operation"/>!';
				if('Y' == $('#ynusedept').val()){
					msg = '<fmt:message key="prudent_operation1"/>!';
				}
				if(confirm(msg)){
					var selected = {};
					selected['yearr'] = $('#yearr').val();
					selected['positn.code'] = $('#positn').val();
					selected['date'] = $('#date').val();
					selected['supply.code'] = '';
					selected['positn.ynUseDept'] = $('#ynusedept').val();//是否启用多档口
					$.ajax({url:'<%=path%>/misInventory/endInventory.do',type:'POST',
						data:selected,async:false,success:function(data){
						if (data.indexOf("-4") >= 0) {
							alert(data);
						}else{
							var msg_ ="<fmt:message key='the_positions_corresponding_loss_storehouse_list_has_been_generated'/>！";
							if('Y' == $('#ynusedept').val()){
								msg_ = "<fmt:message key='the_positions_corresponding_loss_storehouse_list_has_been_generated1'/>！";
							}
							alert(msg_);
							$('.<fmt:message key="quit"/>').click();
						}
					}});
				}
			}
			//未结束盘点仓位
			function noInventoryPostin(){
				$('body').window({
					id: 'window_noInventoryPostin',
					title: '未结束盘点仓位',
					content: '<iframe id="noInventoryPostin" frameborder="0" src="<%=path%>/misInventory/noInventoryPostin.do"></iframe>',
					width: '280px',
					height: '390px',
					draggable: true,
					confirmClose : false,
					isModal: true,
					topBar: {
						items: [{
								text: '返回',
								title: '返回',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}	
			    function pageReload(par){
			    	if('parent'==par){
			    		alert('<fmt:message key="operation_successful"/>！');
			    	}
			    	$('#listForm').submit();
			    }
			    
			    function  allInventorycntbla(){
			    	$('.grid').find('.table-body').find('tr').each(function(i){
						$(this).find('td:eq(12)').find('input').val($(this).find('td:eq(9)').find('span').attr('title'));
						$(this).find('td:eq(13)').find('input').val($(this).find('td:eq(10)').find('span').attr('title'));
						$(this).find('td:eq(14)').find('span').html('0');
						$(this).find('td:eq(15)').find('span').html('0');
					});
			    }
			    // 鼠标离开时检查数量输入格式是否合法
				function validate(e){
			    	if(isNaN(e.value)){
						alert('<fmt:message key="number_be_not_valid_number"/>！');
						e.value=e.defaultValue;
						e.focus();
						return;
					};

					if($(e).parents("td").index()==13){
						if(Number($(e).parents('tr').data("unitper")) != 0){//转换率不为0的才能这么转换wjf
							$(e).parents('tr').find('td:eq(12)').find('input').val((Number($(e).parents('tr').find('td:eq(13)').find('input').val())/Number($(e).parents('tr').data("unitper"))).toFixed(2));
							$(e).parents('tr').find('td:eq(12)').find('span').attr('title',(Number($(e).parents('tr').find('td:eq(13)').find('input').val())/Number($(e).parents('tr').data("unitper"))).toFixed(2));
						}
					}else if($(e).parents("td").index()==12){
						 $(e).parents('tr').find('td:eq(13)').find('input').val((Number($(e).parents('tr').find('td:eq(12)').find('input').val())*Number($(e).parents('tr').data("unitper"))).toFixed(2));
						 $(e).parents('tr').find('td:eq(13)').find('span').attr('title',(Number($(e).parents('tr').find('td:eq(12)').find('input').val())*Number($(e).parents('tr').data("unitper"))).toFixed(2));
					 }
					 $(e).parents('tr').find('td:eq(14)').find('span').html($(e).parents('tr').find('td:eq(12)').find('input').val()-$(e).parents('tr').find('td:eq(9)').find('span').attr('title'));//数量1
					 $(e).parents('tr').find('td:eq(14)').find('span').attr('title',($(e).parents('tr').find('td:eq(12)').find('input').val()-$(e).parents('tr').find('td:eq(9)').find('span').attr('title')));
					 $(e).parents('tr').find('td:eq(15)').find('span').html($(e).parents('tr').find('td:eq(13)').find('input').val()-$(e).parents('tr').find('td:eq(10)').find('span').attr('title'));
					 $(e).parents('tr').find('td:eq(15)').find('span').attr('title',($(e).parents('tr').find('td:eq(13)').find('input').val()-$(e).parents('tr').find('td:eq(10)').find('span').attr('title')));
					 $(e).parents('tr').find('td:eq(12)').find('span').attr('title',($(e).parents('tr').find('td:eq(12)').find('input').val()));
					 
// 					 $(e).parents('tr').find('td:eq(14)').find('span').html($(e).parents('tr').find('td:eq(12)').find('input').val()-$(e).parents('tr').find('td:eq(9)').find('span').attr('title'));
// 					 $(e).parents('tr').find('td:eq(14)').find('span').attr('title',($(e).parents('tr').find('td:eq(12)').find('input').val()-$(e).parents('tr').find('td:eq(9)').find('span').attr('title')));
// 					 $(e).parents('tr').find('td:eq(15)').find('span').html($(e).parents('tr').find('td:eq(13)').find('input').val()-$(e).parents('tr').find('td:eq(10)').find('span').attr('title'));
// 					 $(e).parents('tr').find('td:eq(15)').find('span').attr('title',($(e).parents('tr').find('td:eq(13)').find('input').val()-$(e).parents('tr').find('td:eq(10)').find('span').attr('title')));
// 					 $(e).parents('tr').find('td:eq(12)').find('span').attr('title',($(e).parents('tr').find('td:eq(12)').find('input').val()));
// 					 $(e).parents('tr').find('td:eq(13)').find('span').attr('title',($(e).parents('tr').find('td:eq(13)').find('input').val()));
// 					 alert($(e).parents('tr').find('td:eq(12)').find('input').val());
// 					 alert($(e).parents('tr').find('td:eq(9)').find('span').attr('title'));
// 					 alert($(e).parents('tr').find('td:eq(12)').find('input').val()-$(e).parents('tr').find('td:eq(9)').find('span').attr('title'));
			    }
			    
			    //导出excel
			    function exportInventory(){
			    	var positnCode=$('#positn').val();
					if(positnCode==''){
						alert('<fmt:message key="please_select_positions"/>！');
						return;
					}else{
						$("#wait2").val('NO');//不用等待加载
						$("#listForm").attr("action","<%=path%>/misInventory/exportInventory.do");
						$('#listForm').submit();
						$("#wait2 span").html("数据导出中，请稍后...");
						$("#listForm").attr("action","<%=path%>/misInventory/list.do");
						$("#wait2").val('');//等待加载还原
					}
			    }
			    
			    //打印盘点表
			    function printInventory(){
			    	var positnCode=$('#positn').val();
					if(positnCode==''){
						alert('<fmt:message key="please_select_positions"/>！');
						return;
					}else{
						$("#wait2").val('NO');//不用等待加载
						$('#listForm').attr('target','report');
						window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
						var action="<%=path%>/misInventory/printInventory.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
						$("#listForm").attr("action","<%=path%>/misInventory/list.do");
						$('#listForm').attr('target','');
						$("#wait2").val('');//等待加载还原
					}
			    }
			    
			    
			    var totalCount;
				var condition = {};
				var currPage;
				function addTr(check){
					if(check=='first'){
						totalCount = '${totalCount}';
						condition['yearr'] = $('#yearr').val();
						condition['date'] = $('#date').val();
						condition['positn.code'] = $('#positn').val();
						condition['supply.sp_code'] = $('#sp_code').val();
						condition['supply.sp_type'] = $('#sp_type').val();
						currPage= 1;
						condition['totalCount'] = totalCount;
						condition['currPage']=1;
						$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
						$("#currState").width(Number('${currState}')*200);
						return;
					}
					$.post("<%=path%>/misInventory/listAjax.do",condition,function(data){
						var rs = eval('('+data+')');
						$("#per").text((rs.currState*100).toFixed(0)+'%');
						$("#currState").width(""+rs.currState*200+"px");
						//不是最后一页
						var num = rs.currPage*pageSize;
						var inventoryList = rs.inventoryList;
						for(var i in inventoryList){
							try{
								var inventory = inventoryList[i];
								var tr = '<tr data-unitper="'+inventory.supply.unitper+'">';
								tr = tr + '<td class="num"><span style="width:25px;">'+ ++num +'</span></td>';
								tr = tr + '<td><span style="width:20px;"><input type="checkbox" name="idList" id="chk_'+inventory.supply.sp_code+'" value="'+inventory.supply.sp_code+'"/></span></td>';
								tr = tr + '<td><span title="'+inventory.supply.positn1+'" style="width:60px;">'+inventory.supply.positn1+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.sp_code+'" style="width:75px;">'+inventory.supply.sp_code+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.sp_name+'" style="width:80px;">'+inventory.supply.sp_name+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.sp_desc+'" style="width:60px;">'+inventory.supply.sp_desc+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.typothdes+'" style="width:60px;">'+inventory.supply.typothdes+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.unit+'" style="width:60px;">'+inventory.supply.unit+'</span></td>';
								tr = tr + '<td><span title="'+inventory.supply.unit1+'" style="width:60px;">'+inventory.supply.unit1+'</span></td>';
								tr = tr + '<td><span title="'+inventory.cntbla+'" style="width:60px;text-align:right;">'+inventory.cntbla.toFixed(2)+'</span></td>';
								tr = tr + '<td><span title="'+inventory.cntubla+'" style="width:60px;text-align:right;">'+inventory.cntubla.toFixed(2)+'</span></td>';
								tr = tr + '<td><span title="'+inventory.amtbla+'" style="width:60px;text-align:right;">'+inventory.amtbla.toFixed(2)+'</span></td>';
								tr = tr + '<td class="textInput"><span title="'+inventory.cnttrival.toFixed(2)+'" style="width:70px;padding: 0px;"><input type="text" value="'+inventory.cnttrival.toFixed(2)+'" onfocus="this.select()" onkeyup="validate(this);" disabled="disabled" style="width:60px;padding: 0px;text-align:right;"/></span></td>';
								tr = tr + '<td class="textInput"><span title="'+inventory.cntutrival.toFixed(2)+'" style="width:70px;padding: 0px;"><input type="text" value="'+inventory.cntutrival.toFixed(2)+'" onfocus="this.select()" onkeyup="validate(this);" disabled="disabled" style="width:60px;padding: 0px;text-align:right;"/></span></td>';
								tr = tr + '<td><span title="" style="width:60px;text-align:right;">'+(inventory.cnttrival-inventory.cntbla).toFixed(2)+'</span></td>';
								tr = tr + '<td><span title="" style="width:60px;text-align:right;">'+(inventory.cntutrival-inventory.cntubla).toFixed(2)+'</span></td>';
								tr = tr + '</tr>';
								$(".grid .table-body tbody").append($(tr));
	 						}catch(e){
	 							alert('Exception:'+inventoryList[i].supply.sp_name);
	 						}
						}
						if(rs.over!='over'){
							condition['currPage']=++currPage;
							returnInfo = true;
						}
// 						new tabTableInput("table-body","text"); //input  上下左右移动	
					});
				}
			    
		</script>
	</body>
</html>