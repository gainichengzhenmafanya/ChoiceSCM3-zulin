<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>每日差异对比</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
				.form-line .form-label{
					width:70px;
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/firmMis/findDayDifCompare.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
<!-- 				<div class="form-label"><fmt:message key ="branche" /></div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text"  id="firmDes"  name="firmDes" value="${firmDes }" readonly="readonly"/> --%>
<%-- 					<input type="hidden" id="firmCode" name="firmCode" value="${firmCode }"/> --%>
<%-- 					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 				</div> -->
				<div class="form-label"><fmt:message key="bigClass"/></div>
				<div class="form-input">
					<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"   class="select"></select>
				</div>				
				<div class="form-label"><fmt:message key="supplies_code"/></div>
				<div class="form-input">
					<input type="text" style="margin-bottom:4px;vertical-align:middle;width: 137px;" class="text" id="sp_code" name="sp_code" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label">
					<input style="margin-left:15px;vertical-align:middle;" id="dayCheckBox" name="dayCheckBox" <c:if test="${dayCheckBox=='Y' }">checked="checked"</c:if> value="Y" type="checkbox"/><label style="color:red;vertical-align:middle;cursor:pointer;" id="dayCheckBoxLabel">仅查日盘物资</label>
				</div>				
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label"><fmt:message key="middleClass"/></div>
				<div class="form-input">
					<select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select>
				</div>
				<div class="form-label"><fmt:message key="smallClass"/></div>
				<div class="form-input">
					<select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"   class="select"></select>
				</div>				
			</div>
			<table id="test" style="width:'100%';" title="每日差异对比" singleSelect="true" rownumbers="true" remoteSort="true"
				idField="itemid" >
				<thead>
					<tr>
					<th field="SP_CODE" width="100px"><span id="SP_CODE">物资编码</span></th>
					<th field="SP_NAME" width="100px"><span id="SP_NAME">物资名称</span></th>
					<th field="SP_DESC" width="100px"><span id="SP_DESC">物资规格</span></th>
					<th field="UNIT" width="100px"><span id="UNIT">单位</span></th>
					<c:forEach var="obj" items="${columns}">
						<th field="F_${obj}" width="100px;">${obj}</th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#grptyp").htmlUtils("select",[]);
			$("#grp").htmlUtils("select",[]);
			$("#typ").htmlUtils("select",[]);
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}else if($('#bdat').val().substr(0,4)!=$('#edat').val().substr(0,4)){
								alert('<fmt:message key="cannot_span_years_query"/>！');
								return;
							}
							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#listForm').attr('action',"<%=path%>/firmMis/exportDayDifCompare.do?'checkMis='+checkMis");
							$('#listForm').submit();
							$('#wait').hide();
							$('#wait2').hide();
							$('#listForm').attr('action',"<%=path%>/firmMis/findDayDifCompare.do");
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
								$('#listForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								var action="<%=path%>/firmMis/printGrossProfit.do?'checkMis='+checkMis";
								$('#listForm').attr('action',action);
								$('#listForm').submit();								
							}else{
								alert('<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！');
								/* showMessage({
									type: 'error',
									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
									speed: 1000
									}); */
								return ;
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),100);
  	 		$('#test').datagrid({});
  	 		var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'));	
				}
			});		  	 		
  	 	});
  	 	//仅查日盘物资
		$('#dayCheckBoxLabel').toggle(
			function(){
				$("#dayCheckBox").attr("checked",true);
			},
			function(){
				$("#dayCheckBox").attr("checked",false);
			}
		);
  	 </script>
  </body>
</html>
