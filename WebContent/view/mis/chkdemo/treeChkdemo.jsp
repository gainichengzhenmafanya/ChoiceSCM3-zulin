<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>盘点模板</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
			<style type="text/css">
			.moduleInfo {
				background-color: #E1E1E1;
				position: relative;
				border-right: 1px solid #999;
				width: 100% !important;
			}		
			a.l-btn-plain{
				border:1px solid #7eabcd; 
			}	
			.grid{
				width:100% !important;
			}
			.table-head{
				width:100% !important;
			}
			.table-body{
				width:100% !important;
			}
			.tr-select{
				background-color: #D2E9FF;
				color:blue;
				font-weight:bold;
				font-style:inherit;
			}
 			.page{ 
 				margin-bottom: 0px; 
 			} 
			.changePageSize{
				display:none;
			}
			.pgSearchInfo{
				display:none;
			}
			.turnPage{
				display:none;
			}			
			.separator {
				margin:0 0 0 0;
				background:none;
			}
			</style>
			<script type="text/javascript">
			var path="<%=path%>";
			</script>
		</head>
	<body>
	<div class="leftFrame" style="width:20%;">
      	<form id="conditionForm" action="<%=path%>/chkdemo/table.do" method="post">
		<div class="moduleInfo">
		<br/>
			<table cellpadding="0" cellspacing="0">
				<tr>
			    	<td>
			    		<span>
				    		<fmt:message key="title"/>&nbsp;<input type="text" value="${chkdemo.title}" id="title" name="title" style="width:80px;" onkeydown="if(event.keyCode==32){return false}"/>
				        	<a style="width:40px;text-align: center;" href="#" id="titleKey" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:searchChk();"><fmt:message key="select"/></a>
			        	</span>
			        </td>
			    </tr>
			</table>
		</div>
		<div class="grid" id="grid">		
			<div class="table-head">
				<table width="100%" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" ><span style="width: 20px;">&nbsp;</span></td>
							<td ><span style="width:150px;"><fmt:message key="title"/></span></td>
                        </tr>
					</thead>
				</table>
			</div>				
			<div class="table-body" style="height:100%;">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkdemo" items="${chkdemoList}" varStatus="status">
							<tr>
								<td class="num" ><span style="width: 20px;">${status.index+1}</span></td>
								<td title="${chkdemo.title}" ><span style="width:150px;">${chkdemo.title}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<page:page form="conditionForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />							
		</div>
	</form>		
    </div>
    <div class="mainFrame" style="width:80%;">
      <iframe src="<%=path%>/chkdemo/findChkdemo.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>   
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		 	//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
				 $('.grid').find('.table-body').find('tr').removeClass("tr-select");
				 $(this).addClass("tr-select");
				 var title=$(this).find('td:eq(1)').find('span').text();
				 $('#title').val(title);
				 window.mainFrame.textSubmit(title);
			});
		 	//奇偶行变色
			//$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),70);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
		});
		//查询
		function searchChk(){
			var action="<%=path%>/chkdemo/table.do";
			$('#conditionForm').attr('action',action);
			$('#conditionForm').submit();
		}
	</script>
	</body>
</html>