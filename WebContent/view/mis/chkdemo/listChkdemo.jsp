<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>盘点模板</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<style type="text/css">
				.FColor{
					width: 100px;
				}
				.form-line .form-label{
					width: 100px;
				}
				.form-line .form-input{
					width: 100px;
				}
				.search{
					margin-top:-1px;
					cursor: pointer;
				}
				.page{margin-bottom: 25px;}
			</style>
		</head>
	<body>
		<div class="tool"></div>
			<form id="addchkdemoForm" name="addchkdemoForm" method="post" action="<%=path%>/chkdemo/addNewChkdemo.do">
				<div class="form-line">
					<div class="form-label" ><fmt:message key="title" /><font style="color: red">*</font></div>
					<div class="form-input">
						<input type="text" class="FColor" name="title2" id="title2" value="${title}" onfocus="this.select()"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="supplies_code" /><font style="color: red">*</font></div>
					<div class="form-input">
						<input type="text" class="FColor"  name="sp_name" id="sp_name" onfocus="this.select()" style="width:200px"/>
						<input type="hidden" name="sp_code" id="sp_code"/>
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询物资' />
					</div>
				</div>
			</form>
		<form id="listForm" action="<%=path%>/chkdemo/findChkByPage.do" method="post">
		<div class="grid" id="grid">		
			<input type="hidden" name="title" id="title" value="${title}"/>
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width:25px;">&nbsp;</span></td>
							<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
							<td><span style="width:150px;"><fmt:message key="title" /></span></td>
							<td><span style="width:50px;"><fmt:message key="abbreviation_code" /></span></td>
							<td><span style="width:100px;"><fmt:message key="supplies_name" /></span></td>
							<td><span style="width:60px;"><fmt:message key="supplies_code" /></span></td>
							<td><span style="width:70px;"><fmt:message key="specification" /></span></td>
							<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
							<td><span style="width:50px;"><fmt:message key="reference_unit" /></span></td>
                           				</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkdemo" items="${chkdemoList}" varStatus="status">
							<tr>
								<td class="num"><span style="width:25px;">${status.index+1}</span></td>
								<td title="${chkdemo.id}" ><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${chkdemo.id}" value="${chkdemo.id}"/></span></td>
								<td title="${chkdemo.title}" ><span style="width:150px;text-align: center;">${chkdemo.title}</span></td>
								<td title="${chkdemo.supply.sp_init}" ><span style="width:50px;text-align: center;">${chkdemo.supply.sp_init}</span></td>
								<td title="${chkdemo.supply.sp_name}" ><span style="width:100px;text-align: center;">${chkdemo.supply.sp_name}</span></td>
								<td title="${chkdemo.supply.sp_code}" ><span style="width:60px;text-align: center;">${chkdemo.supply.sp_code}</span></td>
								<td title="${chkdemo.supply.sp_desc}" ><span style="width:70px;text-align: center;">${chkdemo.supply.sp_desc}</span></td>
								<td title="${chkdemo.supply.unit}" ><span style="width:50px;text-align: center;">${chkdemo.supply.unit}</span></td>
								<td title="${chkdemo.supply.unit}" ><span style="width:50px;text-align: center;">${chkdemo.supply.unit}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>			
		<page:page form="listForm" page="${pageobj}"></page:page>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
	</form>		
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒  
		}		
		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
		   //光标移动
		   new tabTableInput("tblGrid","text"); 
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			}); 	
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.cust('<fmt:message key="please_select_category" />','<%=path%>/supply/addSupplyBatch.do',null,$('#sp_name'),$('#sp_code'),'800','600');
				}
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'title2',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="title" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'title2',
					validateType:['maxLength'],
					param:[20],
					error:['<fmt:message key="title" /><fmt:message key="length_too_long" />！']
				},{
					type:'text',
					validateObj:'sp_name',
					validateType:['canNull','maxLength'],
					param:['F','1000'],
					error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
				}]
			});						
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if(validate._submitValidate()){
								window.document.getElementById("addchkdemoForm").submit();//saveByAdd();
							}
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deletechkdemo();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}]
			});
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),102);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法								
		});				
		//弹出物资树回调函数
		function handler(sp_code){
			$('#sp_code').val(sp_code); 
			if(sp_code==undefined || ''==sp_code){
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					 $('#sp_init').val(supply.sp_init);
				}
			});
		}
		//删除申购信息
		function deletechkdemo(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				alertconfirm('<fmt:message key="delete_data_confirm" />？',function(){
					var selected = {};
					checkboxList.filter(':checked').each(function(i){
						selected['chkdemoList['+i+'].title'] = $(this).parents('tr').find('td:eq(2)').text();
						selected['chkdemoList['+i+'].id'] = $(this).val();
					});
					$.post('<%=path%>/chkdemo/delete.do',selected,function(data){
					 	//弹出提示信息
						showMessage({
							type: 'success',
							msg: '操作成功！',
							speed: 3000
						});	
					 	$('#listForm').submit();
					});
				});
// 				if(confirm('<fmt:message key="delete_data_confirm" />？')){
// 					var selected = {};
// 					checkboxList.filter(':checked').each(function(i){
// 						selected['chkdemoList['+i+'].title'] = $(this).parents('tr').find('td:eq(2)').text();
// 						selected['chkdemoList['+i+'].id'] = $(this).val();
// 					});
<%-- 					$.post('<%=path%>/chkdemo/delete.do',selected,function(data){ --%>
// 					 	弹出提示信息
// 						showMessage({
// 							type: 'success',
// 							msg: '操作成功！',
// 							speed: 3000
// 						});	
// 					 	$('#listForm').submit();
// 					});
					
// 				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
				return ;
			}
		}
		//左侧列表点击事件
		function textSubmit(title){
			$('#listForm').attr('action','<%=path%>/chkdemo/findChkdemo.do');
			$('#title').val(title);
			$('#listForm').submit();
		}
		</script>
	</body>
</html>