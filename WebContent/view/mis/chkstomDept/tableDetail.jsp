<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>报货单详情</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<div style="height:250px;">
			<form id="listForm" action="<%=path%>/chkstomDept/findDetail.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;">单价</span></td>
								<td><span style="width:70px;">金额</span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
							</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
									<tr data-mincnt="${chkstod.supply.mincnt}" data-unitper="${chkstod.supply.unitper }">
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;">${chkstod.amount }</span></td>
								<td><span style="width:70px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:70px;text-align: right;" price="${chkstod.supply.sp_price}"><fmt:formatNumber value="${chkstod.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${chkstod.supply.sp_price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;">${chkstod.amount1 }</span></td>
								<td><span style="width:55px;">${chkstod.supply.unit1 }</span></td>
								<td><span style="width:90px;">${chkstod.hoped}</span></td>
								<td><span style="width:70px;">${chkstod.memo}</span></td>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstod.price }"/></span></td>								
							</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}
				]
			});
		});
		</script>
	</body>
</html>