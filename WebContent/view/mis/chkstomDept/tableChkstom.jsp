<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="audit_filled_reported_manifest"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
<!-- 		<div class="tool"> -->
<!-- 		</div> -->
		<%--存放一个状态 判断是何种操作类型 --%>
		<form id="listForm" action="<%=path%>/chkstomDept/toChkstom.do" method="post">
			<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
			<input type="hidden" id="yntg" name="yntg" value="N"/>
			<input type="hidden" name="idList" value="${idList}"/>
			<input type="hidden" id="positn" name="positn.code" value="${chkstom.positn.code}"/>
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" style="width:200px;" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker();"/>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
<%-- 						<input type="text" value="${chkstom.chkstoNo }" class="text" disabled="disabled"/> --%>
					</div>
					
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="purchase_positions"/>：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" disabled="disabled" class="text" id="firmDes" onfocus="this.select()" onblur="selectFirstFirm(this)" name="firmDes" style="width:40px;margin-top:4px;vertical-align:top;" value="${chkstom.firm}"/>
						<select disabled="disabled" class="select" id="firm" name="firm" style="width:155px" onchange="findByDes(this);" readonly="readonly">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.firm}"> 
									   	selected="selected"
								</c:if>
								id="des" value="${positn.code}" >${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>					
					<div class="form-label"><fmt:message key="make_orders"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madeby}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
					</div>		
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
<!-- 								<td rowspan="2"><span style="width:70px;">赠送数量</span></td> -->
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;">单价</span></td>
								<td><span style="width:70px;">金额</span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr data-mincnt="${chkstod.supply.mincnt}" data-unitper="${chkstod.supply.unitper }">
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;">${chkstod.amount }</span></td>
								<td><span style="width:70px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:70px;text-align: center;" price="${chkstod.price}"><fmt:formatNumber value="${chkstod.price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: center;"><fmt:formatNumber value="${chkstod.price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;">${chkstod.amount1 }</span></td>
								<td><span style="width:55px;">${chkstod.supply.unit1 }</span></td>
								<td><span style="width:90px;">${chkstod.hoped}</span></td>
								<td><span style="width:70px;">${chkstod.memo}</span></td>
<%-- 								<td><span style="width:70px;text-align: right;">${chkstod.iszs}</span></td> --%>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstod.price }"/></span></td>								
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + chkstod.amount}"/>   
							<c:set var="sum_price" value="${sum_price + chkstod.supply.sp_price*chkstod.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
		//工具栏
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
	 		
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),62);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//控制按钮显示
// 		var tool = $('.tool').toolbar({
// 				items: [{
// 						text: '<fmt:message key="save" />(<u>S</u>)',
// 						title: '<fmt:message key="save"/>',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-80px','-0px']
// 						},
// 						handler: function(){
// 							saveChkstom();
// 						}
// 					},{
// 						text: '<fmt:message key="quit" />',
// 						title: '<fmt:message key="quit"/>',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-160px','-100px']
// 						},
// 						handler: function(){
// 							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
// 						}
// 					}
// 				]
// 			});
		
		//保存添加
		function saveChkstom()
		{
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					var amount1=$(this).find('td:eq(8)').text()?$(this).find('td:eq(8)').text():$(this).find('td:eq(8) input').val();
					var ind=$(this).find('td:eq(10)').text()?$(this).find('td:eq(10)').text():$(this).find('td:eq(10) input').val();
					if(Number(amount)==0||Number(amount1)==0){
						numNull=1;//1数量为0
					}
					if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
						numNull=2;//2数量为空
					}
					if(ind==''||ind==null){
						indNull=1;//1日期为空
					}
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key ="not_add_empty_document" />！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('数量不是有效数字！');
				return;
			}
			if(Number(indNull)==1){//日期为空
				alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
				return;
			}
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount","supply.unit",
			            "price","totalAmt","amount1","supply.unit1","hoped","memo"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				var status=$(rows[i]).find('td:eq(0)').text();
				data["chkstodList["+i+"]."+"price"] = $(rows[i]).data('price') ? $(rows[i]).data('price'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
					if(value)
						data["chkstodList["+i+"]."+keys[j-1]] = value;
				}
			}
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkstom/saveByAddOrUpdateDept.do",data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					parent.pageReload();
				case 1:
					//豪享来——BS供应链，分店MIS—采购验货—档口报货单上传，生成上传后，能否提示上传成功。  wangjiE 2014年11月27日 15:49:43
					alert('上传成功！');
// 					showMessage({
// 						type: 'success',
// 						msg: '<fmt:message key="save_successful"/>！',
// 						speed: 3000
// 						});
					parent.pageReload();
				}
			});			 
		}
				
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		</script>			
	</body>
</html>