<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查询档口报货单上传</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkstomDept/listCheckedChkstom.do" method="post">
				<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>			
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo}"/></div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input"><input type="text" id="sp_code" name="sp_code" class="text" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /></div>
				</div>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/></div>
					<div class="form-label"><fmt:message key="scm_export_dept"/>：</div>
					<div class="form-input">
						<select class="select" id="dept" name="dept" style="width:200px">
							<option value=""></option>							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.dept}"> 
									   	selected="selected"
								</c:if>
								value="${positn.code}" title="${positn.des}">${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td style="width:80px;"><fmt:message key="reported_num"/></td>
								<td style="width:100px;"><fmt:message key="date"/></td>
								<td style="width:130px;"><fmt:message key="time"/></td>
								<td style="width:80px;"><fmt:message key="scm_dept"/><fmt:message key="coding"/></td>
								<td style="width:100px;"><fmt:message key="scm_dept"/><fmt:message key="name"/></td>
								<td style="width:80px;"><fmt:message key="total_amount1"/></td>
								<td style="width:100px;"><fmt:message key="orders_maker"/></td>
								<td style="width:80px;display: none;">是否上传</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:70px;" title="${chkstom.chkstoNo }">${chkstom.chkstoNo }</span></td>
									<td><span style="width:90px;" title="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:120px;" title="${chkstom.madet}">${chkstom.madet}</span></td>
									<td><span style="width:70px;" title="${chkstom.dept}">${chkstom.dept }</span></td>
									<td><span style="width:90px;" title="${chkstom.positn.des}">${chkstom.positn.des }</span></td>
									<td><span style="width:70px;text-align:right;" title="${chkstom.totalAmt }"><fmt:formatNumber value="${chkstom.totalAmt }" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:90px;" title="${chkstom.madeby}">${chkstom.madeby }</span></td>
									<td><span style="width:70px;display: none;" title="${chkstom.bak1}">${chkstom.bak1 }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="SearchForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			//双击事件
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				toSelectDetailed();
			});
			
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();;
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});

			var detailWin;
			function toSelectDetailed(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkoutmDetail',
						title: '查询档口报货单明细',
						content: '<iframe id="checkedChksoutDetailFrame" frameborder="0" src="<%=path%>/chkstomDept/findDetail.do?chkstoNo='+chkValue+'"></iframe>',
						width: '1000px',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','0px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else{
					alert('<fmt:message key="please_select_data_to_view"/>！');
					return ;
				}
			}
		});
		
		var tool = $('.tool').toolbar({
			items: [{
					text: '<fmt:message key ="select" />',
					title: '<fmt:message key="select" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '生成门店报货单',
					title: '生成门店报货单',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()<1) {
							alert("<fmt:message key='please_select_the_stalls_newspaper_order_for_operation'/>！");
						}else{
							toChkstom();
						}
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
					}
				}
			]
		});
		
		//生成门店报货单
		function toChkstom(){
			var idChkValue = [];
			var checkboxList = $('.grid').find('.table-body').find('tr').find(':checkbox');
			checkboxList.filter(':checked').each(function(i){
				idChkValue.push($(this).val());
			});
			var ids=idChkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '生成门店报货单',
				content: '<iframe id="chkstomFrame" name="chkstomFrame" frameborder="0" src="<%=path%>/chkstomDept/toChkstom.do?idList='+ids+'"></iframe>',
				width: '1100px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '生成门店报货单',
							title: '生成门店报货单',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								window.frames["chkstomFrame"].saveChkstom();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		function pageReload(){
	        window.location.href = '<%=path%>/chkstomDept/listCheckedChkstom.do';
	    }
		</script>
	</body>
</html>