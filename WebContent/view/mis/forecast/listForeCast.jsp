<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--营业预估</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/castList.do" method="post">	
		<input type="hidden" id="mis" name="mis" value="${mis}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:300px;">
					<font style="color:blue;"><fmt:message key="startdate"/>:</font>
					<input type="text" style="width:90px;" id="startdate" name="startdate" value="<fmt:formatDate value="${startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key="enddate"/>:</font>
					<input type="text" style="width:90px;" id="enddate" name="enddate" value="<fmt:formatDate value="${enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
				<div class="form-label">仓位</div>
				<div class="form-input">
					<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${posSalePlan.deptdes}" />"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
					<input type="hidden" id="dept" name="dept" value=""/>
				</div>
			</div>
			<div class="form-line">	

				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:300px;">
					<font style="color:blue;"><fmt:message key="startdate"/>:</font>
					<input type="text" style="width:90px;" id="bdate" name="bdate" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="enddate"/>:</font>
					<input type="text" style="width:90px;" id="edate" name="edate" value="<fmt:formatDate value="${edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
				<div class="form-label" style="margin-left:50px;"></div>
				<div class="form-input">
					<input type="button" style="width:60px" id="calculate" name="calculate" value='<fmt:message key="calculate"/>'/>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_weeks"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="scm_holidays"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="special_events"/></span></td>
								
								<td colspan="2"><span>一班</span></td>
 								<td colspan="2"><span>二班</span></td>
 								<td colspan="2"><span>三班</span></td>
 								<td colspan="2"><span>四班</span></td>
 								<td colspan="2"><span><fmt:message key="total"/></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_estimated_date"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="adjustment"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posSalePlan" items="${posSalePlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${posSalePlan.dat}" value="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>"/>
										</span>
									</td>
									<td><span title="<fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${posSalePlan.dat}" pattern="yyyy-MM-dd"/></span></td>
									<td><span title="${posSalePlan.week }" style="width:40px;">${posSalePlan.week }</span></td>
									<%-- <td><span title="${posSalePlan.week }" style="width:40px;">${posSalePlan.week}</span></td> --%>
									<td class="textInput"><span title="" style="width:40px;">
										<input type="text" onfocus="this.blur()"  readonly="readonly" style="width:40px;text-align: middle;padding: 0px;" /></span></td>
									<td><span title="${posSalePlan.memo }" style="width:50px;">${posSalePlan.memo }</span></td>
									<td><span title="${posSalePlan.pax1old }" style="width:60px;text-align: right;">${posSalePlan.pax1old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax1 }" style="width:60px;">
										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax1 }" onchange="changeNum(7,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${posSalePlan.pax2old }" style="width:60px;text-align: right;">${posSalePlan.pax2old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax2 }" style="width:60px;">
										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax2 }" onchange="changeNum(9,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${posSalePlan.pax3old }" style="width:60px;text-align: right;">${posSalePlan.pax3old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax3 }" style="width:60px;">
										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax3 }" onchange="changeNum(11,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${posSalePlan.pax4old }" style="width:60px;text-align: right;">${posSalePlan.pax4old }</span></td>
									<td class="textInput"><span title="${posSalePlan.pax4 }" style="width:60px;">
										<input tiaozheng=tiaozheng type="text" value="${posSalePlan.pax4 }" onchange="changeNum(13,${status.index})"; style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${posSalePlan.totalold }" style="width:60px;text-align: right;">${posSalePlan.totalold }</span></td>
									<td><span span=span title="${posSalePlan.total }" style="width:60px;text-align: right;">${posSalePlan.total }</span></td>
									<td><input type="hidden" id="firm" name="firm" value="${posSalePlan.firm }"/>	</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td><span style="width:15px;">&nbsp;</span></td>
								<td><span style="width:20px;">&nbsp;</span></td>
								<td style="width:90px;"><fmt:message key="total"/></td>
								<td colspan="3"><span style="width:152px;">&nbsp;</span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input tous=tous type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input tous=tous type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input tous=tous type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input tous=tous type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
								<td class="textInput"><span title="" style="width:60px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:60px;text-align: right;padding: 0px;" /></span></td>
							</tr>							
						</tfoot>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			focus() ;//页面获得焦点
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false]);
			}
			
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	var a = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];//存放合计值
		 	var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.each(function(){
				// 自动计算是否假期
				var week=$(this).parents('tr').find('td:eq(3)').find('span').attr('title');
				switch (week) {
					case '星期一' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('否');
						break;
					case '星期二' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('否');
						break;
					case '星期三' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('否');
						break;
					case '星期四' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('否');
						break;
					case '星期五' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('否');
						break;
					case '星期六' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('是');
						break;
					case '星期日' :
						$(this).parents('tr').find('td:eq(4)').find('span').find('input').val('是');
						break;
				}
				// 自动计算合计
				a[0]+=$(this).parents('tr').find('td:eq(6)').find('span').attr('title')*1;
				a[1]+=$(this).parents('tr').find('td:eq(7)').find('input').val()*1;
				a[2]+=$(this).parents('tr').find('td:eq(8)').find('span').attr('title')*1;
				a[3]+=$(this).parents('tr').find('td:eq(9)').find('input').val()*1;
				a[4]+=$(this).parents('tr').find('td:eq(10)').find('span').attr('title')*1;
				a[5]+=$(this).parents('tr').find('td:eq(11)').find('input').val()*1;
				a[6]+=$(this).parents('tr').find('td:eq(12)').find('span').attr('title')*1;
				a[7]+=$(this).parents('tr').find('td:eq(13)').find('input').val()*1;
				a[8]+=$(this).parents('tr').find('td:eq(14)').find('span').attr('title')*1;
				a[9]+=$(this).parents('tr').find('td:eq(15)').find('span').attr('title')*1;
			});
			 if(0==checkboxList.size() || !checkboxList.size()) {
				$('tfoot').hide();
			} 
			$('tfoot').find('tr').find('td:eq(4)').find('span').find('input').val(a[0]);
			$('tfoot').find('tr').find('td:eq(5)').find('span').find('input').val(a[1]);
			$('tfoot').find('tr').find('td:eq(6)').find('span').find('input').val(a[2]);
			$('tfoot').find('tr').find('td:eq(7)').find('span').find('input').val(a[3]);
			$('tfoot').find('tr').find('td:eq(8)').find('span').find('input').val(a[4]);
			$('tfoot').find('tr').find('td:eq(9)').find('span').find('input').val(a[5]);
			$('tfoot').find('tr').find('td:eq(10)').find('span').find('input').val(a[6]);
			$('tfoot').find('tr').find('td:eq(11)').find('span').find('input').val(a[7]);
			$('tfoot').find('tr').find('td:eq(12)').find('span').find('input').val(a[8]);
			$('tfoot').find('tr').find('td:eq(13)').find('span').find('input').val(a[9]);

			$('#bdate').bind('click',function(){
			new WdatePicker();
			});
			$('#edate').bind('click',function(){
				new WdatePicker();
			});
			$('#startdate').bind('click',function(){
				new WdatePicker();
			});
			$('#enddate').bind('click',function(){
				new WdatePicker();
			});
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
						
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/printCast.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/castList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/exportCast.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/castList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),78);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
			
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
		});	
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
					checkboxList.filter(':checked').each(function(i){
						selected['posSalePlanList['+i+'].firm'] = $(this).parents('tr').find('td:eq(16)').find('input').val();
						selected['posSalePlanList['+i+'].dat'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
						selected['posSalePlanList['+i+'].pax1'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
						selected['posSalePlanList['+i+'].pax2'] = $(this).parents('tr').find('td:eq(9)').find('input').val();
						selected['posSalePlanList['+i+'].pax3'] = $(this).parents('tr').find('td:eq(11)').find('input').val();
						selected['posSalePlanList['+i+'].pax4'] = $(this).parents('tr').find('td:eq(13)').find('input').val();
					});
					$.post('<%=path%>/forecast/updateSalePlan.do',selected,function(data){
						showMessage({//弹出提示信息
							type: 'success',
							msg: '<fmt:message key="operation_successful" />！',
							speed: 1000
						});	
					});
				}else{
					alert('<fmt:message key="please_select_options_you_need_save" />！');
					return ;
				}
			}
		}
		function DateDiff(sDate1, sDate2)
		{ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		$('#calculate').bind("click",function search(){
			var r = confirm('<fmt:message key="hours_period_values_were_recalculated_it"/>？');
			if (r==true) {
				var a =  new Date($('#edate').val().replace(/-/g,"/")).getTime()- new Date($('#bdate').val().replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)<6) {
					alert('<fmt:message key="please_select"/><fmt:message key="Is_greater_than"/><fmt:message key="please_select"/>一周的<fmt:message key="date"/>');
					return;
				}				if(!$('#bdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="reference"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="reference"/><fmt:message key="enddate"/>！');
					return;
				}
				if(!$('#startdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#enddate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="scm_estimated_date"/><fmt:message key="enddate"/>！');
					return;
				}
				if(DateDiff($("#edate").val(),$("#bdate").val())>31){
					alert('<fmt:message key="reference"/><fmt:message key="date"/><fmt:message key="can_not_be_greater_than"/>1<fmt:message key="month"/>！');
					return;
				}
				var action = "<%=path%>/forecast/calPosSalePlan.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
				}
			});
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
		}
		//计算列与行合计
		function changeNum(n,b){
				var numSum = 0;
				var tousNum = 0;
			//列合计
			$("#tblGrid").find("tbody").find("tr").each(function (){
				var num1 = $(this).find("td:eq("+n+")").find('input').val();
				numSum += parseInt(num1);
			})
			$("#tblGrid").find('tfoot').find('tr').find('td:eq('+(n-2)+')').find('input').val(numSum);
			//行合计
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('td').find('[tiaozheng=tiaozheng]').each(function (){
				tousNum+=parseInt($(this).val());
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').html(tousNum);
			$("#tblGrid").find("tbody").find("tr:eq("+b+")").find('[span=span]').attr('title',tousNum);
			})
			var tousSum = 0;
			$("#tblGrid").find('tfoot').find('tr').find('[tous=tous]').each(function(){
				tousSum+=parseInt($(this).val());
			})
			$("#tblGrid").find('tfoot').find('tr').find('td:eq(13)').find('input').val(tousSum);
		}
		</script>
	</body>
</html>