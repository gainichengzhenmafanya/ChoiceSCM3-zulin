<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--菜品销售计划</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
			<style type="text/css">	
			.page{margin-bottom: 25px;}		
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:60px;
			}
			</style>
	</head>
	<body> 
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/planList.do" method="post">	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>	
		<input type="hidden" id="mis" name="mis" value="${mis}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="scm_estimated_date"/><fmt:message key="date"/>:</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:120px;" id="bdate" name="bdate" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker();"/>
				</div>
			 	<div class="form-label">仓位</div>
				<div class="form-input">
					<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${posItemPlan.deptdes}" />"/>
					<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
					<input type="hidden" id="dept" name="dept" value=""/>
				</div>
				<div class="form-label"></div>
				<div class="form-input">
					<input type="button" style="width:60px" id="calculate" name="calculate" value='<fmt:message key="calculate"/>'/>
				</div>
				</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								
								<td colspan="2"><span>一班</span></td>
 								<td colspan="2"><span>二班</span></td>
 								<td colspan="2"><span>三班</span></td>
 								<td colspan="2"><span>四班</span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="after_the_reform_of"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posItemPlan" items="${posItemPlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${posItemPlan.item}" value="${posItemPlan.item}"/>
										</span>
									</td>
									<td><span title="${posItemPlan.itcode}" style="width:50px;">${posItemPlan.itcode}</span></td>
									<td><span title="${posItemPlan.itdes }" style="width:80px;">${posItemPlan.itdes }</span></td>
									<td><span title="${posItemPlan.itunit }" style="width:25px;">${posItemPlan.itunit }</span></td>
									<td><span title="${posItemPlan.cnt1old }" style="width:60px;text-align: right;">${posItemPlan.cnt1old }</span></td>
									<td class="textInput"><span title="${posItemPlan.cnt1 }" style="width:60px;">
										<input type="text" value="${posItemPlan.cnt1 }" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${posItemPlan.cnt2old }" style="width:60px;text-align: right;">${posItemPlan.cnt2old }</span></td>
									<td class="textInput"><span title="${posItemPlan.cnt2 }" style="width:60px;">
										<input type="text" value="${posItemPlan.cnt2 }" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${posItemPlan.cnt3old }" style="width:60px;text-align: right;">${posItemPlan.cnt3old }</span></td>
									<td class="textInput"><span title="${posItemPlan.cnt3 }" style="width:60px;">
										<input type="text" value="${posItemPlan.cnt3 }" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${posItemPlan.cnt4old }" style="width:60px;text-align: right;">${posItemPlan.cnt4old }</span></td>
									<td class="textInput"><span title="${posItemPlan.cnt4 }" style="width:60px;">
										<input type="text" value="${posItemPlan.cnt4 }" style="width:60px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false,false]);
			}
			
			if ("a"!=$("#msg").val()) {
				alert($("#msg").val());
			}
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
			new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		 	//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key ="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key ="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if ($('.grid').find('.table-body').find(':checkbox').size()==0) {
								alert("请先查询，或者该日销售计划为空，请确认！");
							}else {
								if(confirm('确定删除该日销售计划吗？')){
									var action = "<%=path%>/forecast/deletePlan.do";
									$('#listForm').attr('action',action);
									$('#listForm').submit();
								}
							}
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/printPlan.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/planList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/exportPlan.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/planList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		 	}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),49);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();		
  	 		/*弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn='+'7&iffirm=1&mold='+'oneTmany&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
		
		});	
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
				checkboxList.filter(':checked').each(function(i){
					selected['posItemPlanList['+i+'].item'] = $(this).val();
					selected['posItemPlanList['+i+'].dat'] = $('#bdate').val();
					selected['posItemPlanList['+i+'].cnt1'] = $(this).parents('tr').find('td:eq(6)').find('input').val();
					selected['posItemPlanList['+i+'].cnt2'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
					selected['posItemPlanList['+i+'].cnt3'] = $(this).parents('tr').find('td:eq(10)').find('input').val();
					selected['posItemPlanList['+i+'].cnt4'] = $(this).parents('tr').find('td:eq(12)').find('input').val();
				});
				$.post('<%=path%>/forecast/updatePlan.do',selected,function(data){
					showMessage({//弹出提示信息
						type: 'success',
						msg: '<fmt:message key="operation_successful" />！',
						speed: 1000
					});	
				});
			}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return ;
			}
		}
		
		$('#calculate').bind("click",function search(){
			var r = confirm('确认重新计算菜品销售计划吗？');
			if (r==true) {
				var action = "<%=path%>/forecast/calPlan.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		});
		
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
		}
		</script>
	</body>
</html>