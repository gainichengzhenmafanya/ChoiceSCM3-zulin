<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--菜品点击率</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/list.do" method="post">	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>	
		<input type="hidden" id="mis" name="mis" value="${mis}"/>
		<input type="hidden" id="dataNull" name="dataNull" value="${dataNull}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:90px;" id="bdate" name="bdate" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;" id="edate" name="edate" value="<fmt:formatDate value="${edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
				<div class="form-label" style="margin-left:50px;"></div>
				<div class="form-input">
					<input type="button" style="width:60px" id="calculate" name="calculate" value='<fmt:message key="calculate"/>'/><label id="wait3" style="visibility:hidden;color:red;font-size:15px;">正在计算... ...请稍候...</label>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="3" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="3"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td rowspan="3"><span style="width:50px;"><fmt:message key="scm_pubitem_code"/></span></td>
								<td rowspan="3"><span style="width:100px;"><fmt:message key="scm_pubitem_name"/></span></td>
								<td rowspan="3"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								
								<td colspan="8"><span><fmt:message key="working_day"/></span></td>
								<td colspan="8"><span><fmt:message key="scm_holidays"/></span></td>
							</tr>
							<tr>
								<td colspan="2"><span>一班</span></td>
 								<td colspan="2"><span>二班</span></td>
 								<td colspan="2"><span>三班</span></td>
 								<td colspan="2"><span>四班</span></td>
 								
 								<td colspan="2"><span>一班</span></td>
 								<td colspan="2"><span>二班</span></td>
 								<td colspan="2"><span>三班</span></td>
 								<td colspan="2"><span>四班</span></td>
							</tr>
							<tr>
								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="calculate"/></span></td>
 								<td><span style="width:45px;"><fmt:message key="adjustment"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="itemUse" items="${itemUseList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${itemUse.item}" value="${itemUse.item}"/>
										</span>
									</td>
									<td><span title="${itemUse.itcode}" style="width:50px;">${itemUse.itcode}</span></td>
									<td><span title="${itemUse.itdes }" style="width:100px;">${itemUse.itdes }</span></td>
									<td><span title="${itemUse.itunit }" style="width:25px;">${itemUse.itunit }</span></td>
									<td><span title="${itemUse.cnt1old }" style="width:45px;text-align: right;">${itemUse.cnt1old }</span></td>
									<td class="textInput"><span title="${itemUse.cnt1 }" style="width:45px;">
										<input type="text" value="${itemUse.cnt1 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${itemUse.cnt2old }" style="width:45px;text-align: right;">${itemUse.cnt2old }</span></td>
									<td class="textInput"><span title="${itemUse.cnt2 }" style="width:45px;">
										<input type="text" value="${itemUse.cnt2 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.cnt3old }" style="width:45px;text-align: right;">${itemUse.cnt3old }</span></td>
									<td class="textInput"><span title="${itemUse.cnt3 }" style="width:45px;">
										<input type="text" value="${itemUse.cnt3 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${itemUse.cnt4old }" style="width:45px;text-align: right;">${itemUse.cnt4old }</span></td>
									<td class="textInput"><span title="${itemUse.cnt4 }" style="width:45px;">
										<input type="text" value="${itemUse.cnt4 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									
									<td><span title="${itemUse.hcnt1old }" style="width:45px;text-align: right;">${itemUse.hcnt1old }</span></td>
									<td class="textInput"><span title="${itemUse.hcnt1 }" style="width:45px;">
										<input type="text" value="${itemUse.hcnt1 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${itemUse.hcnt2old }" style="width:45px;text-align: right;">${itemUse.hcnt2old }</span></td>
									<td class="textInput"><span title="${itemUse.hcnt2 }" style="width:45px;">
										<input type="text" value="${itemUse.hcnt2 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>

									<td><span title="${itemUse.hcnt3old }" style="width:45px;text-align: right;">${itemUse.hcnt3old }</span></td>
									<td class="textInput"><span title="${itemUse.hcnt3 }" style="width:45px;">
										<input type="text" value="${itemUse.hcnt3 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td><span title="${itemUse.hcnt4old }" style="width:45px;text-align: right;">${itemUse.hcnt4old }</span></td>
									<td class="textInput"><span title="${itemUse.hcnt4 }" style="width:45px;">
										<input type="text" value="${itemUse.hcnt4 }" style="width:45px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true,true,true]);
			}else {
				loadToolBar([true,false,false,false,false]);
			}
			if ("calculated"==$("#msg").val()) {
				$("#wait3").css("visibility","visible");
			}
			if ("dataNull"==$("#dataNull").val()) {
				alert("<fmt:message key='lack_of_food_sales_data_click_calculated_failure_rate'/>!");
			}
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
			new WdatePicker();
			});
			$('#edate').bind('click',function(){
				new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="sure_to_delete_dishes_click_rate"/>？')){
								var action = "<%=path%>/forecast/delete.do";
								$('#listForm').attr('action',action);
								$('#listForm').submit();
							}
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/print.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/list.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/export.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/list.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();			
		});	
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
				checkboxList.filter(':checked').each(function(i){
					selected['itemUseList['+i+'].item'] = $(this).val();
					selected['itemUseList['+i+'].cnt1'] = $(this).parents('tr').find('td:eq(6)').find('input').val();
					selected['itemUseList['+i+'].cnt2'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
					selected['itemUseList['+i+'].cnt3'] = $(this).parents('tr').find('td:eq(10)').find('input').val();
					selected['itemUseList['+i+'].cnt4'] = $(this).parents('tr').find('td:eq(12)').find('input').val();
					
					selected['itemUseList['+i+'].hcnt1'] = $(this).parents('tr').find('td:eq(14)').find('input').val();
					selected['itemUseList['+i+'].hcnt2'] = $(this).parents('tr').find('td:eq(16)').find('input').val();
					selected['itemUseList['+i+'].hcnt3'] = $(this).parents('tr').find('td:eq(18)').find('input').val();
					selected['itemUseList['+i+'].hcnt4'] = $(this).parents('tr').find('td:eq(20)').find('input').val();
				});
				$.post('<%=path%>/forecast/update.do',selected,function(data){
					showMessage({//弹出提示信息
						type: 'success',
						msg: '<fmt:message key="operation_successful" />！',
						speed: 1000
					});	
				});
			}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save" />！');
				return ;
			}
		}
		function DateDiff(sDate1, sDate2)
		{ 
		    var aDate, oDate1, oDate2, iDays;
		    aDate = sDate1.split("-");
		    oDate1 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]); //转换为12-18-2002格式
		    aDate = sDate2.split("-");
		    oDate2 = new Date(aDate[1] + '/' + aDate[2] + '/' + aDate[0]);
		    iDays = parseInt((oDate1 - oDate2) / 1000 / 60 / 60 /24); //把相差的毫秒数转换为天数
		    return iDays;
		}
		$('#calculate').bind("click",function search(){
			var r = confirm('<fmt:message key="to_confirm_the_re_calculated"/>？');
			if (r==true) {
				if(!$('#bdate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edate').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="enddate"/>！');
					return;
				}
				if($("#bdate").val() > $("#edate").val()) {
					alert('<fmt:message key="startdate"/><fmt:message key="can_not_be_greater_than"/><fmt:message key="enddate"/>！');
					return;
				}
				if(DateDiff($("#edate").val(),$("#bdate").val())>31){
					alert('<fmt:message key="reference"/><fmt:message key="date"/><fmt:message key="can_not_be_greater_than"/>1<fmt:message key="month"/>！');
					return;
				}
				$("#wait3").css("visibility","visible");
				$("#msg").val("calculated");
				$(".button").attr("disabled",true);
				var action = "<%=path%>/forecast/calculate.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		});
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
		}
		</script>
	</body>
</html>