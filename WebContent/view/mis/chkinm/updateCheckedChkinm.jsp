<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Chkinm Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			</style>
	</head>
	<body>

		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/misChkinm/saveChkinm.do" method="post">
		<!-- 加入隐藏内容 -->
		<%-- curStatus 0:init;1:edit;2:add;3:show --%>
		<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		
		<div class="form-line">
			<div class="form-label"><fmt:message key="document_types"/>:</div>
			<div class="form-input" style="width:220px;">
				<select id="typ" name="typ" value="${chkinm.typ}" class="select" style="width:226px;" disabled="disabled">
					<c:forEach items="${billType }" var="codedes">
						<option value="${codedes.code }" <c:if test="${codedes.code == chkinm.typ }">selected="selected"</c:if>>${codedes.des }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="document_number"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.vouno!=null}"><c:out value="${chkinm.vouno}"></c:out></c:if>
				<input type="hidden" name="vouno" id="vouno" value="${chkinm.vouno }"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/>:</div>
			<div class="form-input" style="width:220px;">
			<input type="text" id="maded" name="maded" style="width:224px;" class="Wdate text" value="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker({onpicked:function(){pickedFunc()}})" readonly="readonly" disabled="disabled"/>
			</div>
			<div class="form-label"><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.chkinno!=null}"><c:out value="${chkinm.chkinno}"></c:out></c:if>
				<input type="hidden" name="chkinno" id="chkinno" value="${chkinm.chkinno }"/>				
			</div>												
		</div>
		<div class="form-line">	
			<div class="form-label"><fmt:message key="storage_positions"/>:</div>
			<div class="form-input" style="width:220px;">
				<input type="text"  id="positn"  name="positn.des" style="width:222px;" readonly="readonly" value="${chkinm.positn.des}" disabled="disabled"/>
				<input type="hidden" id="positn_select" name="positn.code" value="${chkinm.positn.code}"/>
<%-- 				<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label"><fmt:message key="orders_maker"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.madeby!=null}"><c:out value="${chkinm.madeby}"></c:out></c:if>
				<input type="hidden" name="madeby" id="madeby" class="text" value="${chkinm.madeby}" />			
			</div>								
		</div>
		<div class="form-line">	
			<div class="form-label"><fmt:message key="suppliers"/>:</div>
			<div class="form-input" style="width:220px;">
				<input type="text"  id="deliver"  style="width:222px;" name="deliver.des" readonly="readonly" value="${chkinm.deliver.des}" disabled="disabled"/>
				<input type="hidden" id="deliver_select" name="deliver.code" value="${chkinm.deliver.code}"/>
<%-- 				<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
			</div>
			<div class="form-label"><fmt:message key="accounting"/>:</div>
				<input type="hidden" name="checby" id="checby" class="text" value="${chkinm.checby}" />			
				<div class="form-input"><c:if test="${chkinm.checby!=null}"><c:out value="${chkinm.checby}"></c:out></c:if>
			</div>		
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="summary"/>:</div>
			<div class="form-input" style="width:220px;">
				<input type="text" name="memo" id="memo" class="text" value="${chkinm.memo}"  style="width:224px" readonly="readonly" disabled="disabled"/>
			</div>
			<div class="form-label"><fmt:message key="audit_remarks"/>:</div>
			<div class="form-input"><c:if test="${chkinm.checby!=null}"><c:out value="${chkinm.chk1memo}"></c:out></c:if>
				<input type="hidden" name="chk1memo" id="chk1memo" class="text" value="${chkinm.chk1memo}" />				
			</div>		
		</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="1">&nbsp;</td>
 								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td colspan="3"><fmt:message key="scm_taxes_pre"/></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="pc_no"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td class="num"><span style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:190px;"><fmt:message key="name"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:80px;"><fmt:message key="quantity"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="tax_rate"/></span></td>
								<td><span style="width:60px;"><fmt:message key="scm_taxes_pre"/><fmt:message key="price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="scm_taxes_pre"/><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<c:forEach var="chkind"  items="${chkinm.chkindList}" varStatus="status" >
								<tr>
									<td class="num"><span style="width: 16px;">${status.index+1}</span></td>
									<td><span style="width:70px;">${chkind.supply.sp_code}</span></td>
									<td><span style="width:190px;">${chkind.supply.sp_name}</span></td>
	 								<td><span style="width:60px;">${chkind.supply.sp_desc}</span></td>
									<td><span style="width:40px;">${chkind.supply.unit}</span></td>
	 								<td><span style="width:60px;text-align:right;"><fmt:formatNumber type="number" value="${chkind.amount}" pattern="#0.00"/></span></td>
									<td><span style="width:60px;text-align:right;"><fmt:formatNumber value="${chkind.price*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align:right;"><fmt:formatNumber value="${chkind.totalamt*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:40px;">${chkind.supply.unit1}</span></td>
	 								<td><span style="width:80px;text-align: right;"><fmt:formatNumber type="number" value="${chkind.amount1}" pattern="#0.00"/></span></td>
	 								<td><span style="width:60px;">${chkind.supply.taxdes}</span></td>
	 								<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${chkind.price}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${chkind.price*chkind.amount}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:70px;"><fmt:formatDate value="${chkind.dued}" pattern="yyyy-MM-dd"/></span></td>
	 								<td><span style="width:60px;">${chkind.pcno}</span></td>
	 								<td><span style="width:90px;">${chkind.memo}</span></td>
								</tr>
				<c:set var="sum_num" value="${status.index+1}"/>  
				<c:set var="sum_amount" value="${sum_amount + chkind.amount}"/>  
				<c:set var="sum_totalamt" value="${sum_totalamt + chkind.totalamt}"/>  
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 25px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
								<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern=".00"></fmt:formatNumber></label>&nbsp;&nbsp;</u></td>
								<td style="width:240px;"><fmt:message key="total_amount"/>:
									<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
								</td>
						</tr>
						
					</thead>
				</table>
		   </div>	
		</form>
		<form id="reportForm" method="post">
		<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/Chkin.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			var selected=[];//存储已经申购过的物资
			$(document).ready(function(){
			 	$(document).bind('keyup',function(e){
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 80: $('#autoId-button-102').click(); break;
		            }
				}); 
				//判断按钮的显示与隐藏
				loadToolBar([true,true]);
				$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						//selectPositn();
						var offset = getOffset('typ');
						top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#positn'),$('#positn_select'),'760','520','isNull');
					}
				});
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('typ');
						top.cust('<fmt:message key="please_select_suppliers"/>','<%=path%>/deliver/selectOneDeliver.do',offset,$('#deliver'),$('#deliver_select'),'900','500','isNull');
					}
				});
				$("select[name='typ'] option[value='<c:out value="${chkinm.typ}"/>']").attr("selected","selected");
			});
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print_storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkinm();
							}
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								window.parent.detailWin.close();
							}
						}
					]
				});
			}	
			function searchChkinm(){
				$('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="query_storage_lists"/>',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/misChkinm/list.do?checkOrNot=unCheck&action=init"></iframe>',
					width: '1024',
					height: '390px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="query_storage_lists"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('searchChkinmFrame')&&window.document.getElementById("searchChkinmFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('searchChkinmFrame','listForm');
									}
								}
							},{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter"/>',
								useable: true,
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									window.frames["searchChkinmFrame"].enterChkinm();
								}
							},{
								text: '<fmt:message key="quit" />',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}	
			//打印
			function printChkinm(){
				$('#reportForm').find('#chkinno').attr('value',$('#listForm').find('#chkinno').val());
				$('#reportForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/misChkinm/viewChkinm.do';	
				$('#reportForm').attr('action',action);
				$('#reportForm').submit();
				$('#reportForm').attr('target','');
				//window.showModalDialog("<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val());
				//location.href="<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val();
			//	window.open ("<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		</script>
	</body>
</html>