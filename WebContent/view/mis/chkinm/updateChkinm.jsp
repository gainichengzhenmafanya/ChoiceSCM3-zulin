<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>mis--入库单</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>		
	</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="importFlag" name="importFlag" value="${importFlag}"/><!-- 导入判断 wjf -->
		<form id="listForm" action="<%=path%>/misChkinm/saveChkinm.do" method="post">
		<div class="bj_head">			 	
		<!-- 加入隐藏内容 -->
		<%-- curStatus 0:init;1:edit;2:add;3:show --%>
		<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		<div class="form-line" style="z-index:13">
			<div class="form-label"><fmt:message key="document_types"/>:</div>
			<div class="form-input" style="width:370px;">
				<select name="typ" id="typ" class="select" style="width:369px;">
					<c:forEach items="${billType }" var="codedes">
						<option value="${codedes.code }">${codedes.des }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="document_number"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.vouno!=null}"><c:out value="${chkinm.vouno}"></c:out></c:if>
				<input type="hidden" name="vouno" id="vouno" value="${chkinm.vouno }"/>
<%-- 				<input type="text" disabled="disabled" class="text" name="vouno" id="vouno" value="${chkinm.vouno }"/> --%>
			</div>
<!-- 			<div class="form-label" style="width:100px;">条形码:</div> -->
<!-- 			<div class="form-input" style="width:150px;"> -->
<!-- 				<input type="text" name="barCode" id="barCode" class="text" onkeyup="javascript: if(event.keyCode==16){searchSupply(this);} "/>  -->
<!-- 			</div> -->
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/>:</div>
			<div class="form-input" style="width:370px;">
				<input type="text" id="maded" name="maded" style="width:367px;" class="Wdate text" value="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker({onpicked:function(){pickedFunc()}})" />
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.chkinno!=null}"><c:out value="${chkinm.chkinno}"></c:out></c:if>
				<input type="hidden" name="chkinno" id="chkinno" value="${chkinm.chkinno }"/>				
			</div>												
		</div>
		<div class="form-line" style="z-index:12">	
			<div class="form-label"><fmt:message key="storage_positions"/>:</div>
			<div class="form-input" style="width:370px;">
				<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${chkinm.positn.code}"/>
				<select class="select" id="positn" name="positn" style="width:323px;" onchange="findPositnByDes(this);">
					<c:forEach var="positn" items="${positnList}" varStatus="status">
						<option 
							<c:if test="${positn.code==chkinm.positn.code}"> 
									   	selected="selected"
							</c:if> 
							id="${positn.code}" value="${positn.code}" title="${positn.des }">${positn.code}-${positn.init}-${positn.des}
						</option>
					</c:forEach>
				</select>
<%-- 				<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="orders_maker"/>:</div>
			<div class="form-input">
				<c:if test="${chkinm.madeby!=null}"><c:out value="${chkinm.madeby}"></c:out></c:if>
				<input type="hidden" name="madeby" id="madeby" class="text" value="${chkinm.madeby}" />			
			</div>								
		</div>
		<div class="form-line" style="z-index:11">	
			<div class="form-label"><fmt:message key="suppliers"/>:</div>
			<div class="form-input" style="width:370px;">
				<input type="text" class="text" id="deliver_select"  onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${chkinm.deliver.code}"/>
				<select class="select" id="deliver" name="deliver" style="width:323px;" onchange="findDeliverByDes(this);">
					<c:forEach var="deliver" items="${deliverList}" varStatus="status">
						<option 
							<c:if test="${deliver.code==chkinm.deliver.code}"> 
									   	selected="selected"
							</c:if> 
							id="${deliver.code}" value="${deliver.code}" title="${deliver.des }">${deliver.code}-${deliver.init}-${deliver.des}
						</option>
					</c:forEach>
				</select>
<%-- 				<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="accounting"/>:</div>
			<input type="hidden" name="checby" id="checby" class="text" value="${chkinm.checby}" />			
			<div class="form-input" id="showChecby"><c:if test="${chkinm.checby!=null}"><c:out value="${chkinm.checby}"></c:out></c:if>
<%-- 				<input type="text" class="text" value="${chkinm.checby}" disabled="disabled"/> --%>
			</div>		
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="summary"/>:</div>
			<div class="form-input" style="width:370px;">
				<input type="text" name="memo" id="memo" class="text" value="${chkinm.memo}"  style="width:367px"/>
			</div>
			<div class="form-label" style="width:100px;"><fmt:message key="audit_remarks"/>:</div>
			<div class="form-input">
				<input type="hidden" name="chk1memo" id="chk1memo" class="text" value="${chkinm.chk1memo}" />				
				<input type="text" name="chk1memo2" id="chk1memo2" class="text" disabled="disabled" value="${chkinm.chk1memo}" />
			</div>	
			<div class="form-label" style="width:100px;"><fmt:message key="please_select_materials"/>:</div>
			<div class="form-input" style="width:220px;">
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
			</div>	
		</div>
		</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="1">&nbsp;</td>
 								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td colspan="3"><fmt:message key="scm_taxes_pre"/></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="pc_no"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td class="num"><span style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:100px;"><fmt:message key="name"/></span></td>
 								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
 								<td><span style="width:40px;"><fmt:message key="tax_rate"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<c:forEach var="chkind"  items="${chkinm.chkindList}" varStatus="status" >
								<tr data-tax="${chkind.supply.tax}" data-unitper="${chkind.supply.unitper}"
									data-sp_per1="${chkind.supply.sp_per1}" data-ynbatch="${chkind.supply.ynbatch }"
									data-sp_id="${chkind.sp_id }">
									<td class="num"><span style="width: 16px;">${status.index+1}</span></td>
									<td><span style="width:70px;">${chkind.supply.sp_code}</span></td>
									<td><span style="width:100px;">${chkind.supply.sp_name}</span></td>
	 								<td><span style="width:70px;">${chkind.supply.sp_desc}</span></td>
									<td><span style="width:50px;">${chkind.supply.unit}</span></td>
	 								<td><span style="width:50px;text-align:right;">${chkind.amount}</span></td>
									<td><span style="width:50px;text-align:right;" a="${chkind.price*(1+chkind.supply.tax)}"><fmt:formatNumber value="${chkind.price*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align:right;"><fmt:formatNumber value="${chkind.totalamt*(1+chkind.supply.tax)}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:50px;">${chkind.supply.unit1}</span></td>
	 								<td><span style="width:50px;text-align:right;">${chkind.amount1}</span></td>
	 								<td><span style="width:40px;">${chkind.supply.taxdes}</span></td>
	 								<td><span style="width:50px;text-align:right;"><fmt:formatNumber value="${chkind.price}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:60px;text-align:right;"><fmt:formatNumber value="${chkind.price*chkind.amount}" type="currency" pattern="0.00"/></span></td>
	 								<td><span style="width:80px;"><fmt:formatDate value="${chkind.dued}" pattern="yyyy-MM-dd"/></span></td>
	 								<td><span style="width:90px;">${chkind.pcno}</span></td>
	 								<td><span style="width:90px;">${chkind.memo}</span></td>
	 								<td><input type="hidden" name="unitper" value="${chkind.supply.unitper}"></input></td>
	 								<td><input type="hidden" name="tax" value="${chkind.supply.tax}"></input></td>
	 								<td><input type="hidden" name="sp_id" value="${chkind.sp_id }"></input></td>
								</tr>
								<c:set var="sum_num" value="${status.index+1}"/>
								<c:set var="sum_amount" value="${sum_amount + chkind.amount}"/>  
								<c:set var="sum_totalamt" value="${sum_totalamt + chkind.totalamt}"/>  
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 25px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount">${sum_amount}</label>&nbsp;&nbsp;</u></td>
							<td style="width:240px;"><fmt:message key="total_amount"/>:
								<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
							</td>
<!-- 							<td><font color="blue">扫码后按SHIFT-L键进行录入</font></td> -->
						</tr>
					</thead>
				</table>
		   </div>	
		</form>
		<form id="reportForm" method="post">
		<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/Chkin.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var selected=[];//存储已经申购过的物资
			//如果导入入库单报错了。。。wjf
			var importError = '<c:out value="${importError}"/>';
			if(importError != null && importError != '' && importError != 'undefined'){
				alert("<fmt:message key='import'/><fmt:message key='failure'/>！<fmt:message key='reason'/>：\n"+importError);
			}
			var validate;
			var first=false;
			var odj = {};
			var YNChinmSpprice = '<c:out value='${YNChinmSpprice}'/>';
			function searchSupply(e){
				var selected = {};
				selected['barCode'] = e.value;
				$.post('<%=path%>/supply/findTop1.do',selected,function(supplyList){
					if (supplyList.length!=1){
						alert("系统中未录入此条形码或者此码对应多条物资！");
					}else{
						if (supplyList[0].sp_code) {
							var trList = $(".table-body").find("tr");
							var flag = false;
							for(var j = 0; j < trList.length; j++){
								if(supplyList[0].sp_code == $(trList[j]).find("td:eq(1)").find('span').text()){
									$(trList[j]).find("td:eq(5)").find('span').text($(trList[j]).find("td:eq(5)").find('span').text()*1+1);
									$(trList[j]).find("td:eq(7)").find('span').text($(trList[j]).find("td:eq(7)").find('span').text()*1+$(trList[j]).find("td:eq(6)").find('span').text()*1);
									$(trList[j]).find("td:eq(9)").find('span').text($(trList[j]).find("td:eq(9)").find('span').text()*1+$(trList[j]).data("unitper")*1);
									flag = true;
									return;
								}
							}
							if(!flag){
								if(first){
									$(".table-body").autoGrid.addRow();
								}
								first = true;
								$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supplyList[0].sp_code);
								$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supplyList[0].sp_name);
								$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supplyList[0].sp_desc);
								$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supplyList[0].unit);
								$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(1).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(supplyList[0].sp_price).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supplyList[0].sp_price).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(supplyList[0].unit1);
								$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(supplyList[0].unitper).css("text-align","right");
								$(".table-body").find("tr:last").data("unitper",supplyList[0].unitper);
//		 						$(".table-body").find("tr:eq(num)").data("sp_id",supplyList[i].sp_code);
							}
						}
					}
				});
				$('#barCode').focus();
			}
			$(document).ready(function(){
				$("#positn_select").val($("#positn").val());
				$("#deliver_select").val($("#deliver").val());
				/*过滤*/
				$('#deliver_select').bind('keyup',function(){
			          $("#deliver").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				
				//按钮快捷键
				focus() ;//页面获得焦点
				$(document).bind('keydown',function(e){
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			 			if((odj[$(e.srcElement).closest('tr').find("td:eq(1) span").text()] > 0 && YNChinmSpprice == 'N')||(Number($(e.srcElement).closest('tr').find("td:eq(6) span").attr('a')) > 0 && YNChinmSpprice == 'N')){
			 				if(index == "6" || index == "7"){
			 					$.fn.autoGrid.setCellEditable($(e.srcElement).closest('tr'),13);
			 					return;
			 				}
			 			}
			 		}
				});
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			    		if(index=="5"||index=="6"||index=="7"){
			    			$(e.srcElement).unbind('blur').blur(function(e){
				 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
				 			});
				    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    		}
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 65: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 69: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-105').click(); break;
		                case 83: $('#autoId-button-106').click(); break;
		                case 67: $('#autoId-button-107').click(); break;
		            }
				}); 
			 	$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positn_select').val();
						var defaultName = $('#positn').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positn_select'),'760','520','isNull',handler);
					}
				});
			 	function handler(a){
			 		 $("#positn").find("option[value='"+a+"']").attr('selected','selected');
			 	}
			 	$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliver_select').val();
						var defaultName = $('#deliver').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliver'),$('#deliver_select'),'900','500','isNull',handler1);
						$("#deliver").find('option').filter(":contains('"+($("#deliver").val())+"')").attr('selected','selected');
					}
				});
			 	function handler1(a){
			 		 $("#deliver").find("option[value='"+a+"']").attr('selected','selected');
			 	}
			 	$('#seachSupply').bind('click.custom',function(e){
			 		if($("#curStatus").val() != "edit" && $("#curStatus").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
			 			alert('<fmt:message key="must_be_dded_or_edited_in_order_to_select_materials"/>！');
			 			return;
			 		}
			 		if($('#positn_select').val() == '' | $('#maded').val() == ''){//必须选仓位和制单日期  2014.12.9wjf
			 			alert('<fmt:message key="please_select"/><fmt:message key="storage_positions"/>和<fmt:message key="fill_time"/>！');
			 			return;
			 		}
			 		if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectSupplyLeft.do?single=false&sp_position='+$('#positn_select').val()),$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
					}
				});
			 	function handler2(sp_codes){
			 		var codes = '';//存放页面上的编码，用来判断是否存在
			 		$('.grid').find('.table-body').find('tr').each(function (){
			 			codes += $(this).find('td:eq(1)').text()+",";
					});
			 		var price;
			 		var sp_code;
					var sp_code_arry = new Array();
			 		sp_code_arry = sp_codes.split(",");
			 		//wangjie  支持物资多选   遍历所选择的物资编码 sp_code 2014年11月21日 14:04:11
					for(var i=0; i<sp_code_arry.length; i++){
						sp_code = sp_code_arry[i];
						if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
							continue;
						}
						var url ="<%=path%>/supply/findBprice.do";
						var data1 = "sp_code="+sp_code+"&area="+$('#positn_select').val()+"&madet="+$('#maded').val();
						$.ajax({
							type: "POST",
							url: url,
							data: data1,
							dataType: "json",
							success:function(spprice){
								price = (spprice.price).toFixed(2);//保留两位小数吧，防止有些小数显示不了问题  wjf
								$.ajax({
									type: "POST",
									url: "<%=path%>/supply/findById.do",
									data: "sp_code="+sp_code,
									dataType: "json",
									success:function(supply){		
										if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
											$.fn.autoGrid.addRow();
										}
										$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code);
										$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
										$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
										$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supply.unit);
										$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(1).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(price).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(price).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(supply.unit1);
										$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(supply.unitper).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(supply.taxdes);
										$(".table-body").find("tr:last").find("td:eq(11)").find('span').text((Number(price)/(1+Number(supply.tax))).toFixed(2)).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(12)").find('span').text((Number(price)/(1+Number(supply.tax))).toFixed(2)).css("text-align","right");
										$(".table-body").find("tr:last").find("td:eq(13)").find('span').text($('#maded').val());
										$(".table-body").find("tr:last").data("unitper",supply.unitper);
										$(".table-body").find("tr:last").data("tax",supply.tax);
										$(".table-body").find("tr:last").data("sp_per1",supply.sp_per1);
										$(".table-body").find("tr:last").data("ynbatch",supply.ynbatch);
									}
								});
							}
						});
					}
			 	}
			 	//回车换焦点start
				    var array = new Array();        
			 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
			 	    array = ['typ','maded','positn_select', 'positn', 'deliver_select','deliver','memo','chk1memo2'];        
			 		//定义加载后定位在第一个输入框上          
			 		$('#'+array[2]).focus();            
			 		$('select,input[type="text"]').not($("#barCode")).keydown(function(e) {                  
				 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
				 		var event = $.event.fix(e);                
				 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
				 		if (event.keyCode == 13) {                
				 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
				 				if(index==6 && $('#'+array[index+1]).attr('disabled')=='disabled'){
				 					++index;
				 				}
				 				$('#'+array[++index]).focus();
				 				if(index==5){
				 					$('#deliver_select').val($('#deliver').val());
				 				} 
				 				if(index==3){
				 					$('#positn_select').val($('#positn').val());
				 				} 
				 				if(index==8){
				 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
				 				} 
				 		}
			 		});    
			 		document.getElementById("typ").onfocus=function(){
			 			this.size = this.length;
			 			$('#typ').css('height','155px');
			 			$('#typ').parent('.form-input').css('z-index','3');
			 			};
		 			document.getElementById("typ").onblur = function(){
			 			this.size = 1;
			 			$('#typ').css('height','20px');
		 			};
		 			$('#typ').bind('dblclick', function() {
	 					$('#typ').blur();
	 				});
		 			$('#typ').change(function(){//onchange方法  2014.10.20wjf
		 				if($(this).val() == '9907' || $(this).val() == '9906'){
		 					$('.table-body').find('tr').remove();
		 					$('#sum_num').text('0');
		 				}else{
		 					setEditable();
		 				}
		 			});
			 		document.getElementById("positn").onfocus=function(){
			 			this.size = this.length;
			 			$('#positn').css('height','100px');
			 			$('#positn').parent('.form-input').css('z-index','2');
			 			};
		 			document.getElementById("positn").onblur = function(){
			 			this.size = 1;
			 			$('#positn').css('height','20px');
		 			};
			 		$('#positn').bind('dblclick', function() {
	 					$('#positn_select').val($(this).val());
	 					$('#positn').blur();
	 				});
			 		document.getElementById("deliver").onfocus=function(){
			 			this.size = this.length;
			 			$('#deliver').css('height','330px');
			 			$('#deliver').parent('.form-input').css('z-index','1');
			 			};
		 			document.getElementById("deliver").onblur = function(){
			 			this.size = 1;
			 			$('#deliver').css('height','20px');
		 			};
		 			$('#deliver').bind('dblclick', function() {
	 					$('#deliver_select').val($(this).val());
	 					$('#deliver').blur();
	 				});
		 		//回车换焦点end
				//新增
				var status = $("#curStatus").val();
				if(status == 'add')setEditable();
				
				<%-- curStatus 0:init;1:edit;2:add;3:show --%>
				//判断按钮的显示与隐藏
				if(status == 'add'){
					loadToolBar([true,true,false,false,false,true,false]);
					$('#chk1memo2').addClass('memoClass');
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
				
					$('#positn').attr("disabled","disabled");
					$('#deliver').attr("disabled","disabled");
					$('#positn_select').attr("disabled","disabled");
					$('#deliver_select').attr("disabled","disabled");
					$('#typ').attr("disabled","disabled");
					$('#maded').attr("disabled","disabled");
				
					$('#chk1memo2').attr('disabled',false);
				}else{//init
					loadToolBar([true,true,false,false,false,false,false]);
					$('#positn').attr("disabled","disabled");
					$('#deliver').attr("disabled","disabled");
					$('#positn_select').attr("disabled","disabled");
					$('#deliver_select').attr("disabled","disabled");
					$('#typ').attr("disabled","disabled");
					$('#maded').attr("disabled","disabled");
					$('#chk1memo2').addClass('memoClass');
				}
			
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_types"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'vouno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="document_number"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'maded',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="date_of_the_system_alone"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'chkinno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'positn',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="purchase_positions"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'deliver',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="suppliers"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				$("select[name='typ'] option[value='<c:out value="${chkinm.typ}"/>']").attr("selected","selected");
				$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),180);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				<c:if test="${tableFrom=='table'}">
					$('#autoId-button-101').click();
				</c:if>
			});
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="query_storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								searchChkinm();
							}
						},{
							text: '<fmt:message key="insert" />(<u>A</u>)',
							title: '<fmt:message key="insert"/><fmt:message key="storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								window.location.replace("<%=path%>/misChkinm/add.do");
							}
						},{
							text: '<fmt:message key="delete" />(<u>D</u>)',
							title: '<fmt:message key="delete"/><fmt:message key="storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteChkinm();
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="update"/><fmt:message key="storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'init')return;
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								$("#curStatus").val("edit");
								loadToolBar([true,true,true,false,true,true,false]);
								setEditable();
								if($('#typ').val() == '9907' || $('#typ').val() == '9906'){
									return;
								}
								$('#positn').attr("disabled",false);
								$('#deliver').attr("disabled",false);
								$('#positn_select').attr("disabled",false);
								$('#deliver_select').attr("disabled",false);
								$('#typ').attr("disabled",false);
								$('#maded').attr("disabled",false);
								
								$('#positn_select').focus();    
							}
						},{
							text: '<fmt:message key="select"/><fmt:message key="reversal"/>',
							title: '<fmt:message key="select"/><fmt:message key="reversal"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'writeoff')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if ($('#typ').val()=='9907'||$('#typ').val()=='9906') {
									var positn = $('#positn_select').val();
									var deliver = $('#deliver_select').val();
									var action="<%=path%>/misChkinm/addChkinmByCx.do?positn="+positn+"&deliver="+deliver;
									$('body').window({
										id: 'window_a',
										title: '<fmt:message key="select"/><fmt:message key="reversal"/>',
										content: '<iframe id="chkstomFrame" frameborder="0" src='+action+'></iframe>',
										width: '90%',
										height: '90%',
										draggable: true,
										isModal: false
									});
								}else {
									alert('<fmt:message key="please_select"/><fmt:message key="document_types"/><fmt:message key="reversal"/><fmt:message key="or"/><fmt:message key="returns"/>！');
								}
							}
						},{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/><fmt:message key="storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkinm();
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/>',
							useable: use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								//先判断仓库有没有期初
								$.ajax({
									url:"<%=path%>/positn/checkQC.do?code="+$('#positn').val(),
									type:"post",
									success:function(data){
										if(data){
											if($("#curStatus").val()=='add' || $("#curStatus").val()=='edit'){
												if(validate._submitValidate()){
													if ($('#typ').val()=='9907'||$('#typ').val()=='9906') {
														saveChkinmByCx();
													}else {
														saveOrUpdateChkinm();
													}
												}
											}else{
												alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
											}
										}else{
											alert($('#positn').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="save"/>！');
											return;
										}
									}
								});
							}
						},{
							text: '<fmt:message key="check" />(<u>C</u>)',
							title: '<fmt:message key="check"/><fmt:message key="storage_message"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[6],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-58px','-240px']
							},
							handler: function(){
								checkChkinm();
								//add wangjie 2014年10月31日 14:19:52 (解决审核后 打印按钮 不可点bug)
								loadToolBar([true,true,false,false,true,false,false]);
							}
						},{
// 							text: '<fmt:message key="import" />',
// 							title: '<fmt:message key="import" />',
// 							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/excel.bmp', --%>
// 								position: ['2px','2px']
// 							},
// 							handler: function(){
// 								importChkinm();
// 							}
// 						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								var status = $("#curStatus").val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))return;
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}	
			function findPositnByDes(inputObj){
				$('#positn_select').val($(inputObj).val());
	        }
			function findDeliverByDes(inputObj){
				$('#deliver_select').val($(inputObj).val());
	        }
			function pickedFunc(){
				var cur=$('#maded').val();
				var uri = "<%=path%>/misChkinm/getVouno.do?maded=" + cur;
				$.get(uri,function(data){
					$("input[name='vouno']").val(data);
				});
			}
			function openChkinm(chkinno){
				window.location.replace("<%=path%>/misChkinm/update.do?chkinno="+chkinno);
			}
			function setEditable(){
				if($('#curStatus').val()=='add'){
					//如果导入成功，则不加1 wjf
					var importFlag = $('#importFlag').val();
					if(importFlag != 'OK'){
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:16,
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					widths:[26,80,110,80,60,60,60,70,60,60,50,60,70,90,100,100],
					colStyle:['','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"},'','','','',''],
					editable:[2,5,6,7,9,13,14,15],
					onLastClick:function(row){
// 						$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
// 						$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(5)').text());//总数量
// 						$('#sum_totalamt').text(Number(($('#sum_totalamt').text())-row.find('td:eq(7)').text()).toFixed(2));//总金额
						getTotalSum();
					},
					onEnter:function(data){
						if(data.curobj.closest('tr').find('td').index(data.curobj.closest('td')) == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							}
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
						function getName(){
							var name;
							$.ajaxSetup({ 
								  async: false
								  });
							$.get("<%=path %>/supply/findById.do",
									{sp_code:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										name =  data.sp_name;
									});
							return name;
						};
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							$(".table-body").scrollLeft(0);
							var sp_position = $("#positn_select").val();
							data['url'] = '<%=path%>/supply/findTop1.do';
							if(!isNaN(data.value))
								data['key'] = 'sp_code';
							else
								data['key'] = 'sp_init';
// 							if(data.value.length>10) {
// 								data['key'] = 'barCode';
// 								data.barCode = this.val();
// 							}
							data.sp_position = $('#positn_select').val();
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var desc = "";
							if(data.sp_desc!=null){
								desc = "-"+data.sp_desc;
							}
							return data.sp_code+'-'+data.sp_init+'-'+data.sp_name + desc;
						},
						afterEnter:function(data,row){
// 							var num=0;
// 							$('.grid').find('.table-body').find('tr').each(function (){
// 								if($(this).find("td:eq(1)").text()==data.sp_code){
// 									num=1;
// 								}
// 							});
// 							if(num==1){
// 								showMessage({
// 	 								type: 'error',
// 	 								msg: '<fmt:message key="added_supplies_remind"/>！',
// 	 								speed: 1000
// 	 							});
// 							}
							if($.inArray(data.sp_code,selected)>=0){
								showMessage({
									type: 'error',
									msg: '<fmt:message key="added_supplies_remind"/>！',
									speed: 1000
								});
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}else{
								selected.push(data.sp_code);
							}
							//查询bao价
							$.ajax({
								type: "POST",
								url: "<%=path%>/supply/findBprice.do",
								data: "sp_code="+data.sp_code+"&area="+$('#positn_select').val()+"&madet="+$('#maded').val(),
								dataType: "json",
								success:function(spprice){
									odj[data.sp_code] = (spprice.price).toFixed(2);
									row.find("td:eq(1) span").text(data.sp_code);
									row.find("td:eq(2) span input").val(data.sp_name).focus();
									row.find("td:eq(3)").find('span').text(data.sp_desc==null?"":data.sp_desc);//修复规格显示问题
									row.find("td:eq(4)").find('span').text(data.unit);
									row.find("td:eq(5)").find('span').text(0).css("text-align","right");
								    row.find("td:eq(6)").find('span').text(Number(spprice.price).toFixed(2)).css("text-align","right");
									row.find("td:eq(7)").find('span').text(0).css("text-align","right");
									row.find("td:eq(8)").find('span').text(data.unit1);
									row.find("td:eq(9)").find('span').text(0).css("text-align","right");
									row.find("td:eq(10)").find('span').text(data.taxdes);
									row.find("td:eq(11)").find('span').text((Number(spprice.price)/(1+Number(data.tax))).toFixed(2)).css("text-align","right");
									row.find("td:eq(12)").find('span').text(0).css("text-align","right");
									row.data("unitper",data.unitper);
									row.data("tax",data.tax);
									row.data("sp_per1",data.sp_per1);
									row.data("ynbatch",data.ynbatch);
									row.data("realPrice",(spprice.price).toFixed(2));
								}
							});
							//查询售价结束
						}
					},{
						index:5,
						action:function(row,data2){
							if(isNaN(data2.value)||Number(data2.value) <= 0){
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								alert('<fmt:message key="please_enter"/>正数！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								//$('#sum_amount').text(Number($('#sum_amount').text())+Number(row.find("td:eq(5)").text())-data2.ovalue);//总数量
// 								$('#sum_totalamt').text((Number($('#sum_totalamt').text())+(Number(data2.value)*Number(row.find("td:eq(6)").text()))-Number(row.find("td:eq(7)").text())).toFixed(2));//总金额
								if(row.data("realPrice")!=null && row.data("realPrice")!=''){
									row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.data("realPrice"))).toFixed(2));
								}else{
									row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.find("td:eq(6)").text())).toFixed(2));
								}
// 								var unitper = row.find("td:eq(14)").find("input").val();//编辑时data中无tax数据，先添加进data
// 								row.data("unitper",unitper);
// 								var tax = row.find("td:eq(15)").find("input").val();//编辑时data中无tax数据，先添加进data
// 								row.data("tax",tax);
								row.find("td:eq(12)").find('span').text((Number(row.find("td:eq(7)").text())/(1+Number(row.data("tax")))).toFixed(2));
								row.find("td:eq(9) span").text(Number(row.find("td:eq(5)").text())*row.data("unitper"));
								/****/
								if(Number(row.find("td:eq(6)").text()) > 0 && YNChinmSpprice == 'N'){
// 									if(row.data("sp_per1")!='' && row.data("sp_per1")!='0.0' && row.data("sp_per1")!='0'){
// 										$.fn.autoGrid.setCellEditable(row,13);
// 									}else{
// 										是否批次判断
// 										if(row.data("ynbatch")!='Y'){
// 											$.fn.autoGrid.setCellEditable(row,15);
// 										}else{
// 											$.fn.autoGrid.setCellEditable(row,14);
// 										}
// 									};
									$.fn.autoGrid.setCellEditable(row,9);
								}else{
									$.fn.autoGrid.setCellEditable(row,6);
								}
								getTotalSum();
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(isNaN(data2.value)||Number(data2.value) < 0){
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								alert('<fmt:message key="please_enter"/>正数！');
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
// 								$('#sum_totalamt').text((Number($('#sum_totalamt').text())+(Number(data2.value)*Number(row.find("td:eq(5)").text()))-Number(row.find("td:eq(7)").text())).toFixed(2));//总金额
								row.data("realPrice",Number(row.find("td:eq(6)").text()));
// 								var tax = row.find("td:eq(15)").find("input").val();//编辑时data中无tax数据，先添加进data
// 								row.data("tax",tax);
								if(row.data("realPrice")!=null && row.data("realPrice")!=''){
									row.find("td:eq(7)").find('span').text(Number(row.data("realPrice")*Number(row.find("td:eq(5)").text())).toFixed(2));
									row.find("td:eq(11)").find('span').text(Number(Number(data2.value)/(1+ Number(row.data("tax")))).toFixed(2));
									row.find("td:eq(12)").find('span').text((((Number(data2.value)*Number(row.find("td:eq(5)").text())).toFixed(2))/(1+Number(row.data("tax")))).toFixed(2));
								}else{
									row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.find("td:eq(5)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(row,7);
								getTotalSum();
							}
						}
					},{
						index:7,
						action:function(row,data2){
							if(isNaN(data2.value)||Number(data2.value) < 0){
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								alert('<fmt:message key="please_enter"/>正数！');
								$.fn.autoGrid.setCellEditable(row,7);
							}else{
								row.find("td:eq(6)").find('span').text((Number(data2.value)/Number(row.find("td:eq(5)").text())).toFixed(2));
								//把真实价格放入缓存中
								row.data("realPrice",(Number(data2.value)/Number(row.find("td:eq(5)").text())));
// 								var tax = row.find("td:eq(15)").find("input").val();//编辑时data中无tax数据，先添加进data
// 								row.data("tax",tax);
								row.find("td:eq(11)").find('span').text(Number(Number(row.find("td:eq(6)").find('span').text())/(1+ Number(row.data("tax")))).toFixed(2));
								row.find("td:eq(12)").find('span').text(Number(Number(row.find("td:eq(7)").find('span').text())/(1+ Number(row.data("tax")))).toFixed(2));
								//$('#sum_totalamt').text((Number($('#sum_totalamt').text())+Number(row.find("td:eq(7)").text())-data2.ovalue).toFixed(2));//总金额
								getTotalSum();
								$.fn.autoGrid.setCellEditable(row,9);
							}
						}
					},{
						index:9,
						action:function(row,data){
							if(isNaN(data.value)||Number(data.value) < 0){
								alert('<fmt:message key="please_enter"/>正数<fmt:message key="or"/>0！');
								$.fn.autoGrid.setCellEditable(row,9);
							}else{
								$.fn.autoGrid.setCellEditable(row,13);
								if(Number(row.data("unitper")) != 0){//如果转换率不为0的  才修改  否则会报错wjf
									row.find("td:eq(5) span").text((Number(row.find("td:eq(9)").text())/(Number(row.data("unitper")))).toFixed(2));//参考数量改变时改变标准数量
									row.find("td:eq(7)").find('span').text((Number(row.find("td:eq(5)").text())*row.data("realPrice")).toFixed(2));//标准数量改变时改变金额
									row.find("td:eq(12)").find('span').text((Number(row.find("td:eq(7)").text())/(1+Number(row.data("tax")))).toFixed(2));
									getTotalSum();
								}
								if(row.data("sp_per1")!='' && row.data("sp_per1")!='0.0' && row.data("sp_per1")!='0'){
									$.fn.autoGrid.setCellEditable(row,13);
								}else if(row.data("ynbatch")=='Y'){
									$.fn.autoGrid.setCellEditable(row,14);
								}else{
									$.fn.autoGrid.setCellEditable(row,15);
								}
							}
						}
					},{
						index:11,
						action:function(row,data){
							$.fn.autoGrid.setCellEditable(row,13);
							row.find("td:eq(11) span").text((Number(row.find("td:eq(6)").text())/data.tax));//参考数量改变时改变标准数量
						}
					},{
						index:13,
						action:function(row,data){
							$.fn.autoGrid.setCellEditable(row,14);
						},CustomAction:function(event,data){
							var input = data.curobj.find('span').find('input');
							input.addClass("Wdate text");
							input.bind('click',function(){
								input.unbind('keyup');
								new WdatePicker({
									startDate:'%y-%M-{%d+1}',
									el:'input',
									onpicked:function(dp){
										data.curobj.find('span').html(dp.cal.getNewDateStr());
										$.fn.autoGrid.setCellEditable(data.row,14);
									},
									oncleared:function(dp){ 
										if(row.data("sp_per1")!='' && row.data("sp_per1")!='0.0' && row.data("sp_per1")!='0'){
											alert('<fmt:message key="supplies"/><fmt:message key="production_date"/><fmt:message key="cannot_be_empty"/>!');
											
											 
										}else{
											data.curobj.find('span').html("");
											if(row.data("ynbatch")=='Y'){
												$.fn.autoGrid.setCellEditable(row,14);
											}else{
												$.fn.autoGrid.setCellEditable(row,15);
											}
										};
									}
								});
							});
							if(Number(data.row.data("sp_per1"))!=0){
								input.trigger('click'); 
							}
						}
					},{
						index:14,
						action:function(row,data){
							if(row.data('ynbatch') == 'Y' && data.value == ''){
								alert('<fmt:message key="supplies"/><fmt:message key="pc_no"/><fmt:message key="cannot_be_empty"/>!');
								$.fn.autoGrid.setCellEditable(row,14);
							}else{
								$.fn.autoGrid.setCellEditable(row,15);
							}
						}
					},{
						index:15,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
// 							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
							$('#sum_num').text($('.table-body').find('tr').size());//总行数
						}
					}]
				});
			}
			function searchChkinm(){
				var curwindow = $('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="query_storage_lists"/>',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/misChkinm/list.do?checkOrNot=unCheck&action=init&inout=rk"></iframe>',
					width: 780,
					height: 480,
					draggable: true,
					isModal: true
				});
				curwindow.max();
			}
			
			//保存冲消
			function saveChkinmByCx(){
				var selected = {};
				selected['typ'] = $('#typ').val();
				selected['vouno'] = $('#vouno').val();
				selected['maded'] = $('#maded').val();
				selected['madeby'] = $('#madeby').val();
				selected['chkinno'] = $('#chkinno').val();
				selected['positn.code'] = $('#positn').val();
				selected['deliver.code'] = $('#deliver').val();
				selected['checby'] = $('#checby').val();
				selected['memo'] = $('#memo').val();
				//table 数组
				var numNull=0;//默认0代表成功，1代表0值，2代表非数字
				var isNull=0;//空单据				
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1)').find('span').html()!=''){
						isNull=1;
						var amount=$(this).find('td:eq(5) span input').val() ? $(this).find('td:eq(5) span input').val() : $(this).find('td:eq(5)').text();
						var amount1=$(this).find('td:eq(9)').text()?$(this).find('td:eq(9)').text():$(this).find('td:eq(9) input').val();
						var price=$(this).find('td:eq(6)').text()?$(this).find('td:eq(6)').text():$(this).find('td:eq(6) input').val();
						var amt=$(this).find('td:eq(7)').text()?$(this).find('td:eq(7)').text():$(this).find('td:eq(7) input').val();
						if(amount===0 || amount==0.0 ||amount==0.00 ){
							numNull=1;
						}
						if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
							numNull=2;//2代表数量为空或者非数字
						}
						if(price=="" || price==null || isNaN(price)){
							numNull=3;//3代表单价为空或者非数字
						}
						if(amt=="" || amt==null || isNaN(amt)){
							numNull=4;//4代表金额为空或者非数字
						}
						selected['chkindList['+i+'].supply.sp_code'] = $(this).find('td:eq(1) span input').val() ? $(this).find('td:eq(1) span input').val() : $(this).find('td:eq(1)').text();
						selected['chkindList['+i+'].supply.sp_name'] = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						selected['chkindList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3) span input').val() ? $(this).find('td:eq(3) span input').val() : $(this).find('td:eq(3)').text();
						selected['chkindList['+i+'].supply.unit'] = $(this).find('td:eq(4) span input').text() ? $(this).find('td:eq(4) span input').text() : $(this).find('td:eq(4)').text();//单位
						selected['chkindList['+i+'].sp_id'] = $(this).data("sp_id");
						selected['chkindList['+i+'].amount'] = amount;//数量
						selected['chkindList['+i+'].price'] = $.trim($(this).data("realPrice")) ? $.trim($(this).data("realPrice")) : ($.trim($(this).find("td:eq(6) span input").val()) ? $.trim($(this).find("td:eq(6) span input").val()):$(this).find('td:eq(6)').text());
						selected['chkindList['+i+'].totalamt'] = $(this).find('td:eq(7) span input').val() ? $(this).find('td:eq(7) span input').val() : $(this).find('td:eq(7)').text();//金额
						selected['chkindList['+i+'].amount1'] = $(this).find('td:eq(9) span input').val() ? $(this).find('td:eq(9) span input').val() : $(this).find('td:eq(9)').text();//数量1
						selected['chkindList['+i+'].dued'] = $(this).find('td:eq(13) span input').val() ? $(this).find('td:eq(13) span input').val() : $(this).find('td:eq(13)').text();// 
						selected['chkindList['+i+'].pcno'] = $(this).find('td:eq(14) span input').val() ? $(this).find('td:eq(14) span input').val() : $(this).find('td:eq(14)').text();// 
						selected['chkindList['+i+'].memo'] = $(this).find('td:eq(15) span input').val() ? $(this).find('td:eq(15) span input').val() : $(this).find('td:eq(15)').text();//备注
					}
				});
				if(Number(isNull)==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(Number(numNull)==1){//数量不为0
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				if(Number(numNull)==2){//数量为空或字母
					alert('<fmt:message key="quantity"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==3){//单价为空或字母
					alert('<fmt:message key="price"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==4){//金额为空或字母
					alert('<fmt:message key="amount"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				$.post('<%=path%>/misChkinm/updateChkinm.do',selected,function(data){
					var rs = eval('('+data+')');
					//判断状态
					var status = $("#curStatus").val();
					var state='<fmt:message key="modify_storage_lists"/>';
					if(status=="add"){
						state='<fmt:message key="add_storage_lists"/>';
						loadToolBar([true,true,true,true,true,false,true]);//点保存后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
						$('#chk1memo2').removeClass('memoClass');
					}
					if(rs=="1"){
						showMessage({
							type: 'success',
							msg: state+'<fmt:message key="successful"/>！',
							speed: 3000
						});	
						loadToolBar([true,true,true,true,true,false,true]);//点保存成功后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
						$('#chk1memo2').removeClass('memoClass');
					}else{
						showMessage({
							type: 'error',
							msg: state+'<fmt:message key="failure"/>！',
							speed: 1000
						});	
					}
				});
			}
			
			//增加 或者 修改
			function saveOrUpdateChkinm(){
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkinno'] = $("#chkinno").val();
				dataS['active'] = 'edit';
				if($("#curStatus").val()=='edit')
					$.ajax({url:'<%=path%>/misChkinm/chkChect.do',type:'POST',
						data:dataS,async:false,success:function(data){
						if ("YES"!=data) {
							alert(data);
							showM='no';
						}
					}});
				if('no' == showM){
					return false;
				}
				//检测是否设置了默认仓位
				var select = {};
				var chkValue = [];
				select['positn'] = $('#positn').val();
				$('.table-body').find('tr').each(function(i){
					var spcode=$(this).find('td:eq(1)').text();
					if(''==spcode){
						
					}else{
						chkValue.push(spcode);
					};
					
				});
				var msg = 'ok';
				
				select['sp_code'] = chkValue.join(",");
				$.ajax({url:'<%=path%>/misChkinm/chkPositn.do',type:'POST',
						data:select,async:false,success:function(data){
					if ("OK"!=data) {
						alert(data);
						msg='no';
					}
				}});
				if('no'==msg){
					return false;
				}
				var selected = {};
				selected['typ'] = $('#typ').val();
				selected['vouno'] = $('#vouno').val();
				selected['maded'] = $('#maded').val();
				selected['madeby'] = $('#madeby').val();
				selected['chkinno'] = $('#chkinno').val();
				selected['positn.code'] = $('#positn').val();
				selected['deliver.code'] = $('#deliver').val();
				selected['checby'] = $('#checby').val();
				selected['memo'] = $('#memo').val();//摘要
				//table 数组
				var numNull=0;//默认0代表成功，1代表0值，2代表非数字
				var isNull=0;//空单据
				var sp_name = "";//用来判断有问题的物资
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1)').find('span').html()!=''){
						isNull=1;
						sp_name = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						var sp_per1 = $(this).data("sp_per1");//判断是否需要生成日期
						var ynbatch = $(this).data('ynbatch');//判断是否需要批次号
						var amount=$(this).find('td:eq(5) span input').val() ? $(this).find('td:eq(5) span input').val() : $(this).find('td:eq(5)').text();
						var amount1=$(this).find('td:eq(9) span input').val() ? $(this).find('td:eq(9) span input').val() : $(this).find('td:eq(9)').text();//数量1
						var price=$(this).find('td:eq(6) span input').val() ? $(this).find('td:eq(6) span input').val() : $(this).find('td:eq(6)').text();
						var totalamt=$(this).find('td:eq(7) span input').val() ? $(this).find('td:eq(7) span input').val() : $(this).find('td:eq(7)').text();
						var dued = $(this).find('td:eq(13) span input').val() ? $(this).find('td:eq(13) span input').val() : $(this).find('td:eq(13)').text();
						var pcno = $(this).find('td:eq(14) span input').val() ? $(this).find('td:eq(14) span input').val() : $(this).find('td:eq(14)').text();
						if(amount==0 || amount==0.0 ||amount==0.00 || amount1 == 0 || amount1==0.0 ||amount1==0.00){//参考数量也不能为0  2015.1.5wjf
							numNull=1;
							return false; 
						}
						if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
							numNull=2;//2代表数量为空或者非数字
							return false; 
						}
						if(price=="" || price==null || isNaN(price)){
							numNull=3;//3代表单价为空或者非数字
							return false; 
						}
						if(totalamt=="" || totalamt==null || isNaN(totalamt)){
							numNull=4;//4代表金额为空或者非数字
							return false; 
						}
						if(Number(sp_per1) != 0 && dued == ''){//生产<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>
							isNull=6;
							return false; 
						}
						if('Y' == ynbatch && pcno == ''){//批次号不能为空
							isNull=7;
							return false; 
						}
						selected['chkindList['+i+'].supply.sp_code'] = $(this).find('td:eq(1)').text();
						selected['chkindList['+i+'].supply.sp_name'] = sp_name;
						selected['chkindList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3)').text();
						selected['chkindList['+i+'].supply.unit'] = $(this).find('td:eq(4)').text();//单位
						selected['chkindList['+i+'].amount'] = Number(amount);//数量
// 						selected['chkindList['+i+'].price'] = $(this).find('td:eq(6)').text();//单价
						selected['chkindList['+i+'].price'] = Number($(this).find('td:eq(7)').text())/Number($(this).find('td:eq(5)').text());//单价=金额/数量
						selected['chkindList['+i+'].totalamt'] = Number(totalamt);//金额
						selected['chkindList['+i+'].amount1'] = Number(amount1);//数量1
						selected['chkindList['+i+'].dued'] = dued;//生产日期
						selected['chkindList['+i+'].pcno'] = pcno;//批次
						var value = $.trim($(this).find("td:eq(15)").text()) ? $.trim($(this).find("td:eq(15)").text()) : $.trim($(this).find("td:eq(15) input").val());
						selected['chkindList['+i+'].memo'] = value;//备注  2014.11.19 wjf
						selected['chkindList['+i+'].sp_id'] = $(this).data("sp_id");
					}
				});
				if(isNull==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(Number(numNull)==1){//数量不为0
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="number_of_standards"/><fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				if(Number(numNull)==2){//数量为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="quantity"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==3){//单价为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="price"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(Number(numNull)==4){//金额为空或字母
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="amount"/><fmt:message key="not_a_valid_number"/>！');
					return;
				}
				if(isNull==6){
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="production_date"/><fmt:message key="cannot_be_empty"/>！');
					return;
				}
				if(isNull==7){
					alert('<fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="pc_no"/><fmt:message key="cannot_be_empty"/>！');
					return;
				}
				$.post('<%=path%>/misChkinm/updateChkinm.do',selected,function(data){
					var rs = eval('('+data+')');
					//判断状态
					var status = $("#curStatus").val();
					var state='<fmt:message key="modify_storage_lists"/>';
					if(status=="add"){
						state='<fmt:message key="add_storage_lists"/>';
						loadToolBar([true,true,true,true,true,false,true]);//点保存后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
						$('#chk1memo2').removeClass('memoClass');
					}
					if(rs=="1"){
// 						showMessage({
// 							type: 'success',
// 							msg: state+'<fmt:message key="successful"/>！',
// 							speed: 3000
// 						});	
			     		loadToolBar([true,true,true,true,true,false,true]);//点保存成功后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						alert('<fmt:message key="save_successful"/>！');
						var  chkinno=$('#chkinno').val();
						window.location.replace("<%=path%>/misChkinm/update.do?chkinno="+chkinno);
// 						$('#chk1memo2').attr('disabled',false);//审核备注可用  wjf
// 						$('#chk1memo2').removeClass('memoClass');
					}else{
						showMessage({
							type: 'error',
							msg: state+'<fmt:message key="failure"/>！',
							speed: 1000
						});	
					}
				});
			}
			//审核
			function checkChkinm(){
				var selected = {};
				selected['typ'] = $('#typ').val();
				selected['vouno'] = $('#vouno').val();
				selected['maded'] = $('#maded').val();
				selected['madeby'] = $('#madeby').val();
				selected['chkinno'] = $('#chkinno').val();
				selected['positn.code'] = $('#positn').val();
				selected['deliver.code'] = $('#deliver').val();
				selected['checby'] = $('#checby').val();
				selected['memo'] = $('#memo').val();
				selected['chk1memo'] = $('#chk1memo2').val();//审核备注
				//table 数组
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1)').find('span').html()!=''){
						selected['chkindList['+i+'].supply.sp_code'] = $(this).find('td:eq(1) span input').val() ? $(this).find('td:eq(1) span input').val() : $(this).find('td:eq(1)').text();
						selected['chkindList['+i+'].supply.sp_name'] = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						selected['chkindList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3) span input').val() ? $(this).find('td:eq(3) span input').val() : $(this).find('td:eq(3)').text();
						selected['chkindList['+i+'].supply.unit'] = $(this).find('td:eq(4) span input').text() ? $(this).find('td:eq(4) span input').text() : $(this).find('td:eq(4)').text();//单位
						selected['chkindList['+i+'].amount'] = $(this).find('td:eq(5) span input').val() ? $(this).find('td:eq(5) span input').val() : $(this).find('td:eq(5)').text();
						selected['chkindList['+i+'].price'] = Number($(this).find('td:eq(6) span input').val() ? $(this).find('td:eq(6) span input').val() : $(this).find('td:eq(6)').text());//单价
						selected['chkindList['+i+'].totalamt'] = $(this).find('td:eq(7) span input').val() ? $(this).find('td:eq(7) span input').val() : $(this).find('td:eq(7)').text();//金额
						selected['chkindList['+i+'].amount1'] = $(this).find('td:eq(9) span input').val() ? $(this).find('td:eq(9) span input').val() : $(this).find('td:eq(9)').text();//数量1
						selected['chkindList['+i+'].dued'] = $(this).find('td:eq(13) span input').val() ? $(this).find('td:eq(13) span input').val() : $(this).find('td:eq(13)').text();// 
						selected['chkindList['+i+'].pcno'] = $(this).find('td:eq(14) span input').val() ? $(this).find('td:eq(14) span input').val() : $(this).find('td:eq(14)').text();// 
						selected['chkindList['+i+'].memo'] = $(this).find('td:eq(15) span input').val() ? $(this).find('td:eq(15) span input').val() : $(this).find('td:eq(15)').text();//备注
					}
				});
				$.post('<%=path%>/misChkinm/checkChkinm.do',selected,function(data){
					var rs = eval('('+data+')');
					if(rs=="-3"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="can_not_find_the_corresponding_materials"/>!<fmt:message key="repeat_return_record"/>',
							speed: 3000
						});	
					}else if(rs=="-1"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="data_in_material_code_is_not_correct"/>,<fmt:message key="please_check"/>?',
							speed: 3000
						});	
					}else if(rs=="0"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="te_data_have_been_audited"/>,<fmt:message key="no_need_to_re_submit"/>!',
							speed: 3000
						});	
					}else if(rs=="-2"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="without_this_material_warehouse"/>,<fmt:message key="can_not_return"/>?',
							speed: 3000
						});	
					}else{
						$('#showChecby').text(rs.checby);
						//弹出提示信息
						showMessage({
							type: 'success',
							msg: '<fmt:message key="operation_successful"/>！',
							speed: 3000
						});	
					};

				});
				loadToolBar([true,true,false,false,false,false,false]);//点保存后把按钮状态改为 init
				$("#curStatus").val('init');    //修改状态
			}
			function deleteChkinm(){
				if(!confirm('<fmt:message key="sure_to_delete_storage"/>？'))return;
				//检测要保存的单据是否已被审核
				var dataS = {},showM = "yes";
				dataS['vouno'] = $("#vouno").val();
				dataS['chkinno'] = $("#chkinno").val();
				dataS['active'] = 'delete';
				$.ajax({url:'<%=path%>/misChkinm/chkChect.do',type:'POST',
					data:dataS,async:false,success:function(data){
					if ("YES"!=data) {
						alert(data);
						showM='no';
					}
				}});
				if('no' == showM){
					return;
				}
				var action = '<%=path%>/misChkinm/delete.do?action=init&&chkinnoids='+$('#chkinno').val();
				$('#deliver').attr('name','deliver.code');
				$('#positn').attr('name','positn.code');
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			//打印
			function printChkinm(){
				$('#reportForm').find('#chkinno').attr('value',$('#listForm').find('#chkinno').val());
				$('#reportForm').attr('target','reportnochkeck');
				window.open("about:blank","reportnochkeck",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/misChkinm/viewChkinm.do';	
				$('#reportForm').attr('action',action);
				$('#reportForm').submit();
				$('#reportForm').attr('target','');
				//window.showModalDialog("<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val());
				//location.href="<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val();
			//	window.open ("<%=path%>/misChkinm/viewChkinm.do?chkinno="+$('#chkinno').val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
			function pageReload(par){
		    	$('#listForm').submit();
	    	}
			function validateByMincnt(index,row,data2){//最小申购量判断
				if(index=="5"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						alert('<fmt:message key="please_enter_positive_integer"/>！');
						row.find("input").focus();
					}
				}else if(index=="6"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						alert('<fmt:message key="please_enter_positive_integer"/>！');
						row.find("input").focus();
					}
				}else if(index=="7"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						alert('<fmt:message key="please_enter_positive_integer"/>！');
						row.find("input").focus();
					}
				}
			}
			function validator(index,row,data2){//输入框验证
				if(index=="5"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						data2.value=0;
					}
// 					$('#sum_amount').text((Number($('#sum_amount').text())+Number(data2.value)-data2.ovalue).toFixed(2));//总数量
					row.find("input").data("ovalue",data2.value);
// 					$('#sum_totalamt').text((Number($('#sum_totalamt').text())+(Number(data2.value)*Number(row.find("td:eq(6)").text()))-Number(row.find("td:eq(7)").text())).toFixed(2));//总金额
					if(row.data("realPrice")!=null && row.data("realPrice")!=''){
						row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.data("realPrice"))).toFixed(2));
					}else{
						row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.find("td:eq(6)").text())).toFixed(2));
					}
					var unitper = row.find("td:eq(14)").find("input").val();//编辑时data中无tax数据，先添加进data
					row.data("unitper",unitper);
					row.find("td:eq(9) span").text((Number(data2.value)*row.data("unitper")).toFixed(2));
					var tax = row.find("td:eq(15)").find("input").val();//编辑时data中无tax数据，先添加进data
					row.data("tax",tax);
					row.find("td:eq(12)").find('span').text((Number(row.find("td:eq(7)").text())/(1+Number(row.data("tax")))).toFixed(2));
					getTotalSum();
				}else if(index=="6"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						data2.value=0;
					}
// 					$('#sum_totalamt').text((Number($('#sum_totalamt').text())+(Number(data2.value)*Number(row.find("td:eq(5)").text()))-Number(row.find("td:eq(7)").text())).toFixed(2));//总金额
// 					row.data("realPrice",Number(data2.value));
					if(row.data("realPrice")!=null && row.data("realPrice")!=''){
						row.find("td:eq(7)").find('span').text(Number(row.data("realPrice")*Number(row.find("td:eq(5)").text())).toFixed(2));
					}else{
						row.find("td:eq(7)").find('span').text((Number(data2.value)*Number(row.find("td:eq(5)").text())).toFixed(2));
					}
					getTotalSum();
				}else if(index=="7"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						data2.value=0;
					}
					row.find("td:eq(6)").find('span').text((Number(data2.value)/Number(row.find("td:eq(5)").text())).toFixed(2));
					//把真实价格放入缓存中
					row.data("realPrice",(Number(data2.value)/Number(row.find("td:eq(5)").text())));
// 					$('#sum_totalamt').text((Number($('#sum_totalamt').text())+Number(data2.value)-data2.ovalue).toFixed(2));//总金额
					row.find("input").data("ovalue",data2.value);
					getTotalSum();
				}
			}
			function getTotalSum(){//计算统计数据
				var sum_amount = 0; 
				var sum_totalamt = 0;
				$('.table-body').find('tr').each(function (){
					if($(this).find('td:eq(1)').text()!=''){//非空行
						var amount = $(this).find('td:eq(5)').text();
						if(!amount){
							amount = $(this).find('td:eq(5)').find("input:eq(0)").val();
						}
						sum_amount += Number(amount);
						var price  = $(this).find('td:eq(7)').text();
						if(!price){//正在编辑该数据
							price = $(this).find('td:eq(7)').find("input:eq(0)").val();
						}
						sum_totalamt = parseFloat(sum_totalamt) + parseFloat(price);
					}
				});
				$('#sum_num').text($(".table-body").find('tr').length);//总行数
				$('#sum_amount').text(sum_amount);//总数量
				$('#sum_totalamt').text(Number(sum_totalamt).toFixed(2));//总金额
			}
			
			//导入excel入库单
			function importChkinm(){
		    	$('body').window({
					id: 'window_importSupply',
					title: '<fmt:message key="import" /><fmt:message key="storage" />单',
					content: '<iframe id="importChkinm" frameborder="0" src="<%=path%>/misChkinm/importChkinm.do"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		    }
		</script>
	</body>
</html>
