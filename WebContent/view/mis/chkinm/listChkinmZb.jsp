<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>chkinm Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 60px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkinmZb/list.do" method="post">
			<input  type="hidden" name="checkOrNot" id="checkOrNot" value="${checkOrNot}"/>
			<input  type="hidden" name="inout" id="inout" value="${chkinm.inout}"/>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${chkinm.orderBy}" default="chkinno"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${chkinm.orderDes}" default="0000000000"/>" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input" ><input autocomplete="off" style="width:85px;" type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" /></div>			
					<div class="form-label" ><fmt:message key="orders_num"/></div>
					<div class="form-input" ><input type="text" id="chkinno" name="chkinno" class="text" value="${chkinm.chkinno}"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkinm.madeby}"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
						<input type="text"  id="deliver" name="deliver.des" readonly="readonly" value="${chkinm.deliver.des}"/>
						<input type="hidden" id="deliverCode" name="deliver.code" value="${chkinm.deliver.code}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="<c:out value="${chkinm.vouno}" />"/></div>
					<div class="form-label"><fmt:message key="orders_audit"/></div>
					<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="<c:out value="${chkinm.checby}" />"/></div>
					<div class="form-label"><fmt:message key="purchase_positions"/></div>
					<div class="form-input">
						<input type="text"  id="positn"  name="positn.des" readonly="readonly" value="${chkinm.positn.des}"/>
						<input type="hidden" id="positnCode" name="positn.code" value="${chkinm.positn.code}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>
		
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td>
									<span style="width:20px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:60px;"><fmt:message key="warehousing_single_number"/></span></td>
								<td><span style="width:90px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:60px;"><fmt:message key="document_types"/></span></td>
								<td><span style="width:70px;"><fmt:message key="date_of_the_system_alone"/></span></td>
								<td><span style="width:120px;"><fmt:message key="time_of_the_system_alone"/></span></td>
								<td><span style="width:80px;"><fmt:message key="positions"/></span></td>
								<td><span style="width:180px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:60px;"><fmt:message key="total_amount"/></span></td>
								<td><span style="width:60px;"><fmt:message key="orders_maker"/></span></td>
<%-- 								<td><span style="width:60px;"><fmt:message key="orders_audit"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div> 
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkinm" varStatus="step" items="${chkinmList}">
								<tr>
									<td class="num" ><span style="width:16px;">${step.count}</span></td>
									<td style="width:25px; text-align: center;">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_${chkinm.chkinno}" value="${chkinm.chkinno}"/></span>
									</td>
									<td><span title="${chkinm.chkinno}" style="width:60px;text-align: right;">${chkinm.chkinno}&nbsp;</span></td>
									<td><span title="${chkinm.vouno}" style="width:90px;">${chkinm.vouno}&nbsp;</span></td>
									<td><span title="${chkinm.typ}" style="width:60px;">${chkinm.typ}&nbsp;</span></td>
									<td><span title="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" style="width:70px;"><fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${chkinm.madet}" style="width:120px;">${chkinm.madet}&nbsp;</span></td>
									<td><span title="${chkinm.positn.des}" style="width:80px;">${chkinm.positn.des}&nbsp;</span></td>
									<td><span title="${chkinm.deliver.des}" style="width:180px;">${chkinm.deliver.des}&nbsp;</span></td>
									<td><span title="${chkinm.totalamt}" style="width:60px;text-align: right;"><fmt:formatNumber type="number" value="${chkinm.totalamt} " pattern="#0.00"/>&nbsp;</span></td>
									<td><span title="${chkinm.madeby}" style="width:60px;">${chkinm.madeby}&nbsp;</span></td>
<%-- 									<td><span title="${chkinm.checby}" style="width:60px;">${chkinm.checby}&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<form id="reportForm" method="post">
			<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//排序start
				var array = new Array();      
				array = ['chkinno','vouno','typ', 'maded', 'madet', 'positn','deliver','totalamt','madeby','checby'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'chkinno',
						validateType:['canNull','intege'],
						/* handler:function(){
							return $("#code").val().replace(/[^\x00-\xff]/g, "**").length<=10; 
						}, */
						param:['T','T','F'],
						error:['','<fmt:message key="single_number_is_digital_or_empty"/>','<fmt:message key="name" /><fmt:message key="length_too_long" />！']
					}]
				});
				//页面过滤供应商和仓位
				$('#deliverCode').bind('keyup',function(){
			          $("#deliver").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#positn').bind('keyup',function(){
			          $("#positnCode").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			    });
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: parent.$('.<fmt:message key="query_storage_lists"/>').click(); break;
		                case 67: parent.$('.<fmt:message key="audit_storage_lists"/>').click(); break;
		                case 68: parent.$('.<fmt:message key="delete_storage_storage"/>').click(); break;
		            }
				});  
				$('#maded').bind('click',function(){
					new WdatePicker();
				});
				$('#madedEnd').bind('click',function(){
					new WdatePicker();
				});
				//双击事件
				$('.grid .table-body tr').live('dblclick',function(){
					var status = $(window.parent.document).find("#curStatus").val();
					if(status == 'add' || status == 'edit'){
						if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？')){
							return;
						}
					}
					var chkinno = $(this).find("input:checkbox").val();
					chkinno ? window.parent.openChkinm(chkinno) && window.close() : alert("error!");
					
				});
				setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positnCode').val();
						var defaultName = $('#positn').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn'),$('#positnCode'),'600','400','isNull');
					}
				});
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliver').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliver'),$('#deliverCode'),'600','400','isNull');
					}
				});
			});	
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/><fmt:message key="direct_dial_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var maded = $('#listForm').find("#maded").val().toString();
						var madedEnd= $('#listForm').find("#madedEnd").val().toString(); 
						if(validate._submitValidate()){
							if(maded<=madedEnd)
							$('#listForm').submit();
							else
								alert("<fmt:message key='starttime'/><fmt:message key='can_not'/><fmt:message key='not_less_than'/><fmt:message key='endtime'/>");
						}		
					}
				},{
					text: '<fmt:message key="enter" />(Enter)',
					title: '<fmt:message key="enter"/>',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-20px']
					},
					handler: function(){
						enterChkinm();
					}
				},{
					text: '<fmt:message key="check" />(<u>C</u>)',
					title: '<fmt:message key="check"/><fmt:message key="direct_dial_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','-240px']
					},
					handler: function(){
						checkChkinm();
					}
				},{
					text: '<fmt:message key="delete" />(<u>D</u>)',
					title: '<fmt:message key="delete"/><fmt:message key="direct_dial_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						deleteChkinm();
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});								
				function findPositnByDes(inputObj){
					$('#positn').val($(inputObj).val());
		        }
				function findDeliverByDes(inputObj){
					$('#deliverCode').val($(inputObj).val());
		        }
				function enterChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
							var chkinno = checkboxList.filter(':checked').val();
							if(status == 'add' || status == 'edit'){
								if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？')){
									return;
								}
							}
							chkinno ? window.parent.openChkinm(chkinno) && window.close() : alert("error!");
					}else{
						alert('<fmt:message key="please_select_single_message"/>！');
						return ;
					}
				}
				function checkChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="whether_to_audit_these_storage_lists"/>？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/chkinmZb/checkAll.do?chkinnoids='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="audit_storage_lists"/>',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_reviewed_information"/>！');
						return ;
					}
				}
				function deleteChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/chkinmZb/delete.do?chkinnoids='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete_storage_storage"/>',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
				function pageReload(){
				    	$('#listForm').submit();
				}
				//打印
				function printChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							$('#reportForm').find('#chkinno').attr('value',chkValue.join(","));
							$('#reportForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
							var action = '<%=path%>/chkinmZb/viewChkinm.do';	
							$('#reportForm').attr('action',action);
							$('#reportForm').submit();
					}else{
						alert('<fmt:message key="please_select_single_message_to_print"/>！');
						return ;
					}
				}
		</script>
	</body>
</html>