<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--物资千人、万元用量</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/spCodeUse/list.do" method="post">	
			<input type="hidden" id="firm" name="firm" value="${spCodeUse.firm}"/>
			<input type="hidden" id="yysj" value="${yysj}"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="the_reference_date"/>:</div>
				<div class="form-input" style="width:250px;">
					<font style="color:blue;">从:</font>
					<input type="text" style="width:90px;" id="bdat" name="bdat" value="${bdat}" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/>:</font>
					<input type="text" style="width:90px;" id="edat" name="edat" value="${edat}" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
				</div>
				<div class="form-label"></div>
				<div class="form-input">
					<input type="button" style="width:60px" id="calculate" name="calculate" value='<fmt:message key="calculate"/>'/>
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:15px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:170px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_specifications"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="scm_calculate"/></span></td>
								<td><span style="width:70px;"><fmt:message key="scm_after_adjustment"/></span></td>
								<td><span style="width:200px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="spCodeUse" items="${spCodeUseList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${spCodeUse.sp_code}" value="${spCodeUse.sp_code}"/>
										</span>
									</td>
									<td><span title="${spCodeUse.sp_code}" style="width:100px;">${spCodeUse.sp_code}</span></td>
									<td><span title="${spCodeUse.sp_name}" style="width:170px;">${spCodeUse.sp_name}</span></td>
									<td><span title="${spCodeUse.sp_desc }" style="width:90px;">${spCodeUse.sp_desc }</span></td>
									<td><span title="${spCodeUse.unit }" style="width:50px;">${spCodeUse.unit }</span></td>
									<td><span title='<fmt:formatNumber type="currency" pattern="#0.00" value="${spCodeUse.cnt }"></fmt:formatNumber>' style="width:50px;"><fmt:formatNumber type="currency" pattern="#0.00" value="${spCodeUse.cnt }"></fmt:formatNumber></span></td>
									<c:if test="${spCodeUse.cnt%1 >0 }">
										<c:set value="${spCodeUse.cnt-spCodeUse.cnt%1+1 }" var="cntNum"></c:set>
									</c:if>
									<c:if test="${spCodeUse.cnt%1 ==0.0 }">
										<c:set value="${spCodeUse.cnt}" var="cntNum"></c:set>
									</c:if>
									<td class="textInput"><span title='<fmt:formatNumber type="currency" pattern="#0" value="${cntNum }"/>' style="width:70px;">
										<input type="text" value='<fmt:formatNumber type="currency" pattern="#0" value="${cntNum }"/>' style="width:70px;text-align: right;padding: 0px;" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td class="textInput"><span title="${spCodeUse.memo }" style="width:200px;">
										<input type="text" value="${spCodeUse.memo }" style="width:200px;text-align: right;padding: 0px;" onfocus="this.select()"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			focus() ;//页面获得焦点
			if ($('#yysj').val()=='0'){
				alert("<fmt:message key='lack_of_business_data'/>！");
			}
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true]);
			}else {
				loadToolBar([true,false]);
			}
			
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});

			$('#bdat').bind('click',function(){
			new WdatePicker();
			});
			$('#edat').bind('click',function(){
				new WdatePicker();
			});
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveUpdate();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();			
		});	
		
		//保存登记
		function saveUpdate(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.each(function(i){
				selected['spCodeUseList['+i+'].firm'] = $('#firm').val();
				selected['spCodeUseList['+i+'].sp_code'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
				selected['spCodeUseList['+i+'].cnt'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
				selected['spCodeUseList['+i+'].cntold'] = $(this).parents('tr').find('td:eq(6)').find('span').attr('title');
				selected['spCodeUseList['+i+'].memo'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
			});
			$.post('<%=path%>/spCodeUse/update.do',selected,function(data){
				showMessage({//弹出提示信息
					type: 'success',
					msg: '<fmt:message key="operation_successful" />！',
					speed: 1000
				});	
			});
		}
		
		$('#calculate').bind("click",function search(){
			var r = confirm('<fmt:message key="to_confirm_the_computation_estimation_Mody"/>？');
			if (r==true) {
				var a =  new Date($('#edat').val().replace(/-/g,"/")).getTime()- new Date($('#bdat').val().replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)<6) {
					alert('<fmt:message key="please_select"/><fmt:message key="Is_greater_than"/>一周的<fmt:message key="date"/>');
					return;
				}
				if(!$('#bdat').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="reference"/><fmt:message key="startdate"/>！');
					return;
				}
				if(!$('#edat').val()) {
					alert('<fmt:message key="please_enter"/><fmt:message key="reference"/><fmt:message key="enddate"/>！');
					return;
				}
				var action = "<%=path%>/spCodeUse/calculate.do";
				//?bdat="+$('#bdat').val()+"&edat="+$('#edat').val();
				$('#listForm').attr('action',action);
				$('#listForm').submit(); 
			}
		});
		// 检查最高最低库存是否有效数字
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert("<fmt:message key='invalid_number'/>！");
				inputObj.focus();
				return false;
			}
			$(inputObj).val(Number($(inputObj).val()).toFixed(2));//保留两位小数
		}
		</script>
	</body>
</html>