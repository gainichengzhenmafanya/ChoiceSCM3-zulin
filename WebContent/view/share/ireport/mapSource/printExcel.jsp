<%@page import="net.sf.jasperreports.engine.base.JRBaseReport"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperExportManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="net.sf.jasperreports.engine.data.JRBeanCollectionDataSource"%>
<%@page import="com.choice.orientationSys.util.IreportMapDataSource"%>
<%@page import="java.util.List"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.util.JRLoader"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.j2ee.servlets.ImageServlet"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>物资编码打印</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
				body{overflow: auto;}
			</style>
		</head>
	<body>
		   
      <%
    //use reflect to set the private field of JRBaseReport
    

      
      
	      //接收传入参数
	      List  list=(List)request.getAttribute("List");
	      HashMap parameters =(HashMap)request.getAttribute("parameters");
	      String reportUrl=(String)request.getAttribute("reportUrl");
	      // HTML Export：
	      File reportFile = new File(getServletContext().getRealPath(reportUrl));
	      JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
	      //或直接使用JasperReport的XML文件
	      //  JasperReport jasperReport2 = JasperCompileManager.compileReport(getServletContext().getRealPath("/reports/WebappReport.jrxml"));
	      IreportMapDataSource jdt=new IreportMapDataSource(list);
	      //填充报表
	      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,jdt);
	      // XLS Export：
	      
          java.lang.reflect.Field margin = JRBaseReport.class.getDeclaredField("leftMargin");
	      margin.setAccessible(true);
	      margin.setInt(jasperReport, 0);                 
	      margin = JRBaseReport.class.getDeclaredField("topMargin");
	      margin.setAccessible(true);
	      margin.setInt(jasperReport, 0);                 
	      margin = JRBaseReport.class.getDeclaredField("bottomMargin");
	      margin.setAccessible(true);
	      margin.setInt(jasperReport, 0);
			      
	      
	      String tmpFilename = System.currentTimeMillis() + ".xls"; 
	    //  response.setHeader("Content-Disposition","attachment;filename="+tmpFilename);
	      response.setContentType("application/vnd.ms-excel");
	      String xlsFileName="JasperXLS";
	      ServletOutputStream outs = response.getOutputStream();
		  JRXlsExporter exporter = new JRXlsExporter();
	   	  exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		  exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,reportFile + xlsFileName);// --输出到文件
		  exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);//--输出到流
		  exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);//--每页使用一个SHEET
		  exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);//--移除空白
		  exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);//--背景色
		  exporter.exportReport();
		  out.clear();
	      out = pageContext.pushBody();
      %>
	</body>
</html>