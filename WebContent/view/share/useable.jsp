<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<body>
		<script type="text/javascript">
				var pageUseable={
						search:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'search')},
						update:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						insert:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						'delete':${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						'import':${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
						'excel':${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						print:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						build:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'build')},
						creat:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'creat')},
						mark:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'mark')},
						supper:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'supper')},
						alone:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'alone')},
						check:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}
				}
		</script>
	</body>
</html>