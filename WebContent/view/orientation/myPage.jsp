<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <title>餐饮管理系统</title>
    <style type="text/css">
    	.mainDiv{
    		width: 794px;
    		height: 1123px;
    		margin-left: auto;
    		margin-right: auto;
    		background: url('<%=path%>/image/flow.jpg');
    	}
    </style>
  </head>	
  <body style="overflow-y:scroll;">
  	 <div class="mainDiv">
  	 	
  	 </div>
  	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  </body>
</html>
