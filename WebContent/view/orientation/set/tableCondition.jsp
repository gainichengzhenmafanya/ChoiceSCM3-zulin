<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>系统参数设置</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<style type="text/css">
				#toolbar{
					position:relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<div id="toolbar"></div>
		<div class="form-group">系统参数设置</div>
		<form id="setForm" action="<%=path %>/studentInfo/saveSetting.do" method="post">
		<div class="form-line">
			<div class="form-label" style="width: 120px;">要启用的查询条件</div>
			<div class="form-input">
				<select id="condition">
					<c:forEach var="column" items="${listColumns}">
						<option value="${column.columnName }"
							<c:if test="${condition.columnName eq column.columnName }">
								selected = "selected"
							</c:if>
						>${column.zhColumnName }</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" style="width: 120px;">调整专业的次数限制</div>
			<div class="form-input">
				<input type="text" class="text" id="zycs" value="${professionalTimes }"/>
			</div>
		</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							save();
						}
					}
				]
			});//end toolbar
			
			//设置表格的整体高度
			setElementHeight('.grid',['#toolbar']);
			
			//设置表格数据展示部分的高度
			setElementHeight('.table-body',['.table-head'],'.grid');
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		});	//end $(document).ready();
		
		function save(){
			var consition = $('#condition').val();
			var professionalTimes = $('#zycs').val(); 
			$('body').window({
					id: 'window_saveCondition',
					title: '<fmt:message key ="save" />',
					content: '<iframe id="window_saveCondition" frameborder="0" src="<%=path%>/studentInfo/saveSetting.do?condition='+consition+'&professionalTimes='+professionalTimes+'"></iframe>',
					width: 497,
					height: 180,
					isModal: true
				});
		}
		

		</script>
	</body>
</html>