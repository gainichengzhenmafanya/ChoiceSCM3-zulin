<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				body{
					background-color: #C9C9C9;
				}
				
				.tool {
					position: relative;
					height: 27px;
				} 
				
				.lineWrap{
					position: relative;
					height: 30px;
					line-height: 30px;
					margin-top: 5px;
					padding: 0px 5px;
					font-weight: bold;
				}
				
				.lineWrap a{
					display: block;
					position: absolute;
				}
				
				.lineWrap span{
					color: red;
				}
				
				.fileButton {
					position: relative;
					height: 24px;
					width: 300px;
					line-height: 24px;
					top: -26px;
					left: 105px;
				}
				
				.fileButton .selectButton{
					position: relative;
					float: left;
					height: 24px;
					width: 75px;
					line-height: 24px;
					cursor: pointer;
				}
				
				.fileButton .showInput {
					position: relative;
					float: left;
					height: 20px;
					line-height: 20px;
					width: 200px;
					margin-left: 5px;
					padding-left: 3px;
					border: 1px solid #999999;
					overflow: hidden;
					text-overflow: ellipsis;
					white-space: nowrap;
					background-color: #FFFFFF;
				}
				
				.file {
					cursor: pointer;
					position: absolute;
					top: 0px;
					right: 0px;
					opacity: 0;
					filter: alpha(opacity=0);
					-ms-filter: "alpha(opacity=0)";
				}
				
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="" id="import" method="post" enctype="multipart/form-data">
			<div class="lineWrap">
				<a href="javascript:download('<%=path %>');">模板下载</a>
			</div>
			<div class="lineWrap">
				<span>*</span>选择导入的文件：
				<div class="fileButton">
					<div class="selectButton"></div>
					<div class="showInput"></div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			function download(path){
				var select=$('#templetId').val();
				window.location = path + '/studentInfo/downloadTemplate.do?select='+select;
			}
			function upload(){
				var file = $("#file").val();
				var fileArr=file.split("\\"); 
				var fileTArr=fileArr[fileArr.length-1].toLowerCase().split("."); 
				var filetype=fileTArr[fileTArr.length-1]; 
				if(file == ''){
					alert("请选择要导入的文件");
					return false;
				}else if(filetype!="xls"){
					alert("请选择Excel格式的文件");
					return false;
				}else{
					$("#import").attr('action','<%=path%>/studentInfo/upload.do').submit();
			  	}
			}
			
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="import" />',
						title: '导入报考人员信息',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							upload();
						}
					}]
				});
				
				$('.selectButton').button({
					text: '浏览文件',
					title: '浏览文件',
					position: {
						top: '0px',
						left: '0px'
					},
					icon: {
						url: '<%=path%>/image/Button/set.png'
					}
				});
				
				$('<input type="file" name="file" id="file" class="file">')
					.appendTo('.selectButton')
					.bind('mouseout',function(){
						var fileVal = $(this).val();
						if(fileVal){
							var file = $("#file").val();
							var fileArr=file.split("\\"); 
							$('.showInput').text(fileArr[fileArr.length-1]);
						}
					});
				
			});
		</script>
	</body>
</html>