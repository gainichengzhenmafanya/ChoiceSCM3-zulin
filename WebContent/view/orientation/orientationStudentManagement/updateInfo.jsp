<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Book Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css"/>
	</head>
	<body>
		<div class="tool"></div>
		<div class="form">
			<form id="promoForm" method="post" action="<%=path %>/studentInfo/update.do">
				<div class="form-line">
					<div class="form-input">报到状态：</div>
					<div class="form-label">更改为：</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="enterState" id="enterState">
						<option value="">--请选择报到状态--</option>
							<option value="0">未报到</option>
							<option value="1">已报到</option>
					</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-input">性别：</div>
					<div class="form-label">更改为：</div>
					<div class="form-input">
						<select name="sex.id" id="sexId">
							<option value="">--请选择性别--</option>
							<c:forEach var="sex" items="${sexList}">
								<option value="${sex.id}">${sex.enum_meaning}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-input">民族：</div>
					<div class="form-label">更改为：</div>
					<div class="form-input">
						<select  name="nationality.id" id="nationalityId">
							<option value="">--请选择民族--</option>
							<c:forEach var="nationality" items="${nationalityList}">
								<option value="${nationality.id}">${nationality.enum_meaning}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-input">学制：</div>
					<div class="form-label">更改为：</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="educationSys.id" id="educationSysId">
						<option value="">--请选择学制--</option>
						<c:forEach var="education" items="${educationSys}">
							<option value="${education.id}">${education.enum_meaning}</option>
						</c:forEach>
					</select>
					</div>
				</div>
					<div class="form-line">
					<div class="form-input">政治面貌：</div>
					<div class="form-label">更改为：</div>
					<div class="form-input">
						<select style="WIDTH: 100pt" name="politicsFace.id" id="politicsFaceId" class="select">
						<option value="">--请选择政治面貌--</option>
						<c:forEach var="politicsFace" items="${featureList}">
							<option value="${politicsFace.id}">${politicsFace.enum_meaning}</option>
						</c:forEach>
					</select>
					</div>
				</div>
				<input type="hidden" id="idList" name="idList" value="${ids}"/>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jQuery.Hz2Py-min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			var tool = $('.tool').toolbar({
				id:'toolbar',
				items: [{
					id:'add',
					text: '<fmt:message key="save" />',
					title: '保存新增宣传信息',
					icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position : ['-80px','-0px']
					},
					handler : function() {
							$("#promoForm").submit();
					}
				}]
			});
		
		</script>
	</body>
</html>