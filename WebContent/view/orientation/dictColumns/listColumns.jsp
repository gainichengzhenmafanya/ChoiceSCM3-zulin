<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:forEach var="column" items="${listColumns}" varStatus="status">
	<tr>
		<td class="num" style="width: 25px;">${status.index+1}</td>
		<td style="width:30px; text-align: center;">
			<input type="checkbox"  name="idList" value="${column.id}"/>
		</td>
		<td><span style="width:40px;" title="${column.id}"><c:out value="${column.id}" />&nbsp;</span></td>
		<td><span style="width:150px;" title="${column.columnName}"><c:out value="${column.columnName}" />&nbsp;</span></td>
		<td><span style="width:150px;" title="${column.zhColumnName}"><c:out value="${column.zhColumnName}" />&nbsp;</span></td>
		<td><span style="width:100px;" title="${column.properties}"><c:out value="${column.properties}" />&nbsp;</span></td>
		<td><span style="width:50px;" title="${column.columnWidth}"><c:out value="${column.columnWidth}" />&nbsp;</span></td>
		<td><span style="width:100px;" title="${column.columnType}"><c:out value="${column.columnType}" />&nbsp;</span></td>
	</tr>
</c:forEach>