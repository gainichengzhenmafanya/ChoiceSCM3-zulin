<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Classes Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		
	</head>
	<body>
		<div class="form">
			<form id="DictForm" method="post" action="<%=path %>/dict/update.do">
					<input  type="hidden" name="id" id="id" value="${dict.id}"/>
					<input  type="hidden" name="parent_id" id="parent_id" value="${dict.parent_id}"/>
				<div class="form-line">
					<div class="form-label">参数名</div>
					<div class="form-input"><input type="text" id="enum_meaning" name="enum_meaning" class="text" value="${dict.enum_meaning}" /></div>
					<div class="form-label">参数值</div>
					<div class="form-input"><input type="text" id="enum_value" name="enum_value" class="text" value="${dict.enum_value}"/></div>
				</div>
				
				<div class="form-line">
					<div class="form-label merge-normal">备注</div>
					<div class="form-input merge-large">
						<textarea class="textarea textarea-merge-large" id="remarks" name="remarks"  >${dict.remarks}</textarea>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'enum_meaning',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['枚举含义不能为空！','枚举含义输入超长']
					},{
						type:'text',
						validateObj:'enum_value',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['枚举值不能为空！','枚举值输入超长']
					}]
				});

			});
			
		</script>
	</body>
</html>