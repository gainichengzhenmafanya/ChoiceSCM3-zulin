<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="system_encoding_message" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">			
			</style>
		</head>
	<body>
	<div class="leftFrame">
		<div id="toolbar"></div>
	    <div class="treePanel">
			<script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
			<script type="text/javascript">
				var tree = new MzTreeView("tree");
				tree.nodes['0_00000000000000000000000000000000'] = 'text:字典分类;method:changeUrl("","root")';
				<c:forEach var="dict" items="${dictList}" varStatus="status">
	      			tree.nodes['00000000000000000000000000000000_${dict.id}'] = 'text:${dict.enum_meaning}; method:changeUrl("${dict.id}","${dict.id}")';
	         	</c:forEach>
	        	tree.setIconPath("<%=path%>/image/tree/none/");
				document.write(tree.toString());
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>   
    <input type="hidden" id="id" name="id" />
    <input type="hidden" id="typ" name="typ" />
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript">
		function changeUrl(id,typ){
			$('#id').val(id);
			$('#typ').val(typ);
		    window.mainFrame.location = "<%=path%>/dict/table.do?id="+id+"&typ="+typ;
	    }			
	    function refreshTree(){
			window.location.href = '<%=path%>/dict/list.do';
	    }
	  //按钮快捷键
		focus() ;//页面获得焦点
	 	$(document).bind('keydown',function(e){
	 		if(e.keyCode==27){
	 			$('.<fmt:message key="quit" />').click();
	 		}
	 	});
		$(document).ready(function(){		
			var toolbar = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="expandAll" />',
						title: '<fmt:message key="expandAll" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					},{
						text: '<fmt:message key="refresh" />',
						title: '<fmt:message key="refresh" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							refreshTree();
						}
					}
				]
			});
			setElementHeight('.treePanel',['#toolbar']);
			window.mainFrame.location="<%=path%>/dict/table.do?typ=root";
		});// end $(document).ready();
	</script>
	</body>
</html>