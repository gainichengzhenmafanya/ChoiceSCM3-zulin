<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>系统注册</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body> 
<!-- 		<div id="toolbar"></div> -->
		<div class="form">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:100px;">
				<form id="registForm" method="post" action="<%=path %>/regist/saveRegist.do">
					<div class="form-line"></div>
						<div class="form-label">
							<span class="green">使用方法： 复制机器码，发给相关技术人员得到注册码后，输入到注册码框中，点击左下角的注册按钮，注册完成后部分中间件需要重新启动，才能生效</span>
					</div>
					<div class="form-line"></div>
						<div class="form-label">
							<span class="red">${info}</span>
					</div>
					<div class="form-line">
						<div class="form-label">
							<span class="red">*</span>机器码
						</div>
						<div class="form-input">
							<textarea style="width:500px;height:180px"  rows="" cols="" id="regist" name="regist" class="text"  ><c:out value="${regist}"></c:out></textarea> 
						</div>
					</div>

					<div class="form-line">
						<div class="form-label"><span class="red">*</span>注册码：</div>
						<div class="form-input">
							<textarea style="width:500px;height:180px" id="registCode" name="registCode" ></textarea> 
						</div>
					</div>
					 

					<div class="form-line" style="margin-top: 200px">
						<div class="form-label"></div>
						<div class="form-label">
						<input  type="button" value="复制机器码" onclick="copy()"/>  <input  type="button" value="注册" onclick="res()"/>
						</div>
					</div>
				</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>				
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				var toolbar = $('#toolbar').toolbar({
					items: [
						{
							text: '注册',
							title: '注册',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								res();
							}
						}
					]
				});
				setElementHeight('.treePanel',['#toolbar']);
			 
			});
			//屏蔽空格
			$(document).keydown(function(event){
				if(event.keyCode==32){
					return false;
				}
			});
			
			function copy(){
				window.clipboardData.setData('text', $("#regist").val());
				alert("已复制！");
			}
			function res(){
				var registCode= $("#registCode").val();
				if(''==registCode){
					alert("注册码为空，请输入！");
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/login/saveRegist.do",{registCode:$("#registCode").val()},function(data){
					if($.trim(data).indexOf("【") >= 0){
						alert($.trim(data)+":重启中间件生效！！！");
					}else if($.trim(data)=="-1"){
						alert("注册码不正确！");
					}else if($.trim(data)=="-2"){
						alert("注册码已过期！");
					}else{
						alert("注册失败！");
					} 
				});
			}
		</script>
	</body>
</html>