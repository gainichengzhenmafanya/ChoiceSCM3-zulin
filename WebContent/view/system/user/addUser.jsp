<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>组信息</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />		
	</head>
	<body>
		<input type="hidden" id="str" name="str" value="${str}"/>
		<div class="form">
			<form id="userForm" method="post" action="<%=path %>/user/saveByAdd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" />
					</div>
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text"/>
					</div>
				</div>
				<div class="form-line">
<%-- 					<div class="form-label"><fmt:message key="sex"/></div> --%>
<!-- 					<div class="form-input"> -->
						<input name="sex" value="0" type="hidden" checked="checked"/>
<%-- 						<input type="radio" id="sex_1" name="sex" value="1"/><fmt:message key="female"/> --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="sector"/></div>
					<div class="form-input">
						<input type="text" id="departmentName" name="department.name" class="selectDepartment text"/>
						<input type="hidden" id="departmentId" name="department.id" />
					</div>
				</div>
				<div class="form-line">
<%-- 					<div class="form-label"><fmt:message key="birthday"/></div> --%>
<!-- 					<div class="form-input"> -->
						<input type="hidden" id="birthday" name="birthday" value="2017-05-06" class="Wdate text" />
<!-- 					</div> -->
					<input name="state" value="1" type="hidden" />
<%-- 					<div class="form-label"><fmt:message key="status"/></div> --%>
<!-- 					<div class="form-input"> -->
<!-- 						<select id="state" name="state" class="select"> -->
<%-- 							<c:forEach var="user_state" items="${USER_STATE}"> --%>
<%-- 								<option value="${user_state.key}">${user_state.value}</option> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>		
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				if("userError"==$("#str").val()){
					alert("编号不能重复!");
				}
		
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="number"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="number_input_extended"/>']
					},{
						type:'text',
						validateObj:'name',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="name_enter_the_ultra_long"/>']
					},{
						type:'text',
						validateObj:'birthday',
						validateType:['canNull','date'],
						param:['F'],
						error:['<fmt:message key="birthday"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="birthday"/><fmt:message key="incorrect_format_input"/>']
					}]
				});
				
				if($('#departmentId',$(parent.document)) && $('#departmentId',$(parent.document)).val())
					$('#departmentId').val($('#departmentId',$(parent.document)).val());
				
				if($('#departmentName',$(parent.document)) && $('#departmentName',$(parent.document)).val())
					$('#departmentName').val($('#departmentName',$(parent.document)).val());
				
				$('#departmentName').bind('focus.selectDepartment',function(e){
					if(!!!top.departmentWindow){
						var offset = getOffset('departmentName');
						top.selectDepartment('<%=path%>',offset,$(this),$('#departmentId'),$('#departmentId').val());
					}
				});
				
				$('#birthday').bind('click',function(){
					new WdatePicker();
				});
				
				$('#code').bind( 'blur',function(){
					var code = $('#code').val();
					if(!/^\w+$/.test(code)){
						alert("编号只能由字母数字下划线组成！");
						return;
					}
					
				});
			});
		</script>
	</body>
</html>
