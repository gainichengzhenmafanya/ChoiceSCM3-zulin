<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>适用区域</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
</head>
<body>

<div class="tool"></div>
	<form id="listForm" action="<%=path%>/comInt/listComInt.do" method="post">
		<div class="grid">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" style="width: 25px;"><span style="width:25px;">&nbsp;</span></td>
							<td><span style="width:150px;">适用区域</span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="firm" items="${firmList }" varStatus="status">
							<tr>
								<td class="num" style="width: 25px;"><span style="width:25px;">${status.index+1}</span></td>
								<td><span style="width:150px;">${firm.firmdes}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>				
		</div>
		<page:page form="listForm" page="${pageobj}"></page:page>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	focus() ;//页面获得焦点
 	$(document).bind('keydown',function(e){
 		if(e.keyCode==27){//按钮快捷键
 			$('.<fmt:message key="quit" />').click();
 		}
 	});
	//自动实现滚动条
	setElementHeight('.grid',['.tool'],$(document.body),70);	//计算.grid的高度
	setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
	loadGrid();//  自动计算滚动条的js方法
	changeTh();//拖动 改变table 中的td宽度
   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
   $('.grid').find('.table-body').find('tr').hover(
		function(){
			$(this).addClass('tr-over');
		},
		function(){
			$(this).removeClass('tr-over');
		}
	);
	setElementHeight('.table-body',['.table-head'],'.grid');
	getFirmIds();
});
	//刷新
	function pageReload(){
    	$('#listForm').submit();
	}
	function getFirmIds(){
		var firmids = '';
		<c:forEach var="comInt" items="${comIntList }" varStatus="status">
			firmids = firmids+'${comInt.firm.firmid},';
		</c:forEach>
 		window.parent.document.getElementById('firmid').value = firmids;
	}
</script>

</body>
</html>