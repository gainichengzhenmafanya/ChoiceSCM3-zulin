<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>log Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.form-line .form-label{
					width: 50px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="queryForm" action="<%=path%>/logs/list.do" name="queryForm"  method="post">
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${logs.orderBy}" />" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${logs.orderDes}" />" />
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label"><fmt:message key="user"/></div>
					<div class="form-input"><input type="text" id="accountname" name="accountname" class="text" onkeyup="ajaxSearch()" value="<c:out value="${queryLogs.accountname}" />"/></div>
					<div class="form-label" style="width:60px;">事件</div>
					<div class="form-input">
						<input type="text"  id="eventsDes"  name="eventsShow" readonly="readonly" class="text" style="margin-top: 0px;" value=""/>
						<input type="hidden" id="events" name="events" value=""/>
						<img id="searchEvents" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" style="margin-top: 0px;" />
					</div>	
					<div class="form-label">IP</div>
					<div class="form-input"><input type="text" id="ips" name="ips" class="text" onkeyup="ajaxSearch()" value="<c:out value="${queryLogs.ips}" />"/></div>
					<div class="form-label"><fmt:message key="date"/></div>
					<div class="form-input" style="width:100px;">
						<input  style="width:100px;" autocomplete="off" type="text" id="logindate" name="logindate" class="Wdate text" value="<fmt:formatDate value="${queryLogs.logindate}" pattern="yyyy-MM-dd"/>" /> 
					</div>
					<div class="form-label" style="width:23px;">到</div>
					<div class="form-input" style="width:55px;">
						<input  style="width:100px;" type="text" id="logindateEnd" name="logindateEnd"  class="Wdate text" value="<fmt:formatDate value="${queryLogs.logindateEnd}" pattern="yyyy-MM-dd"/>" />
					</div>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select"/>'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty"/>'/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:154px;"><span><fmt:message key="time_of_occurrence"/></span></td>
								<td style="width:100px;"><span><fmt:message key="user"/></span></td>
								<td style="width:100px;"><span><fmt:message key="event"/></span></td>
								<td style="width:300px;"><span><fmt:message key="content"/></span></td>
								<td style="width:100px;"><span>IP</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="log" items="${logsList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${log.id}' />" value="<c:out value='${log.id}' />"/>
									</td>
									<td style="width:154px;"><fmt:formatDate value="${log.logindate}" pattern="yyyy-MM-dd HH:mm:ss"/>&nbsp;</td>
									<td style="width:100px;"><c:out value="${log.accountname}" />&nbsp;</td>
									<td style="width:100px;"><c:out value="${log.events}" />&nbsp;</td>
									<td style="width:300px;"><c:out value="${log.contents}" />&nbsp;</td>
									<td style="width:100px;"><c:out value="${log.ips}" />&nbsp;</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			function clearqueryForm(){
				$('#queryForm input[type=text]').val('');
			}
			$(document).ready(function(){
				//选择事件
				$("#searchEvents").click(function(){
					selectEvets({
	  	 				basePath:'<%=path%>'
	   	 			});
				});
				
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearqueryForm();
				});
				$('#logindate').bind('click',function(){
					new WdatePicker();
				});
				$('#logindateEnd').bind('click',function(){
					new WdatePicker();
				});
				
				//排序start
				var array = new Array();      
				array = ['logindate','a.name','contents', 'events', 'ips'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#queryForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="query_log_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(200);
							var t = $('#accountname').val();
							$('#accountname').focus().val(t);
						}
					},"-",{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete_the_log_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deletelogs();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
				});

				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),51);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				function viewlogs(){
					 pageReload();
				}
				
				function deletelogs(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/logs/delete.do?id='+chkValue.join(","); 
							
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
			
			//供选择事件页面调用
	  	 	function setEvets(data){
	  	 		$("#events").val(data.code);
	  	 		$("#eventsDes").val(data.show);
	  	 	}
		</script>
	</body>
</html>