<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="startdate"/>账号-inout</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<style type="text/css"> 
	 	.page{
			margin-bottom: 25px;
		}
		</style>
	</head>
	<body>
		<form action="<%=path%>/account/inoutByAccount.do" id="listForm" name="listForm" method="post">
			<input type="hidden" name="accountId" value="${accountId}" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:50px;"><fmt:message key="coding"/></td>
								<td style="width:180px;"><fmt:message key="name"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="inout" varStatus="step" items="${listInout}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${inout.code}" value="${inout.code}"/>
									</td>
									<td><span title="${inout.code}" style="width:40px;text-align: left;"><c:out value="${inout.code}" />&nbsp;</span></td>
									<td><span title="${inout.des}" style="width:170px;text-align: left;"><c:out value="${inout.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),22);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				<c:forEach var="inoutAccount" varStatus="step" items="${inoutAccountList}">
					$('#chk_${inoutAccount.code}').attr('checked',"true");
				</c:forEach>
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")){
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				}); 
			});
			
			//保存
			function saveInoutAccount(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action = '<%=path%>/account/saveInoutAccount.do?accountId=${accountId}&inoutIds='+chkValue.join(",");
				$('body').window({
					title: '分配账号申购方向范围',
					content: '<iframe frameborder="0" src='+action+'></iframe>',
					width: 500,
					height: '245px',
					draggable: true,
					isModal: true
				});
			}
			
		    function pageReload(){
		    	parent.$('.close').click();
		    }
		</script>
	</body>
</html>