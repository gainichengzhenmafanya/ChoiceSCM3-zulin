<%@ page import="com.choice.framework.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="my_work_desktop_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<style type="text/css">
				#toolbar {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<form id="listForm" method="post" action="<%=path %>/role/saveByUpdateMainTable.do">
		<input type="hidden" id="id" name="id" value="<c:out value="${role.id}" />"/>
		<input type="hidden" id="name" name="name" value="<c:out value="${role.name}" />"/>
		<input type="hidden" id="deleteFlag" name="deleteFlag" value="<c:out value="${role.deleteFlag}" />"/>
		<input type="hidden" id="menu" name="menu" value="<c:out value="${role.menu}" />"/>
	    <div id="toolbar"></div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
						   <tr><td colspan="4"><fmt:message key="role_name"/>：<c:out value="${role.name}" /></td></tr>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td> 
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:180px;"><fmt:message key="module_name"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
					</table>
				</div>
			</div>
		</form> 
    	<form id="roleOperateForm" action="<%=path%>/roleOperate/list.do">
    		<input type="hidden" id="id" name="id" value="${role.id}"/>
    	</form>
    	<form id="roleOperateRangeForm" action="<%=path%>/roleOperateRange/list.do">
			<input type="hidden" id="id" name="id" value="<c:out value="${role.id}" />"/>
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">
		var result = '${result}';
		$(document).ready(function(){
			var name=['<fmt:message key="my_remind"/>','<fmt:message key="announcement_notification"/>',
			          '<fmt:message key="stock_is_lower_than_the_lower_limit"/>','<fmt:message key="stock_exceeds_the_upper_limit"/>',
			          '<fmt:message key="purchase_invoices_summary"/>','<fmt:message key="storehouse_data_summary"/>',
			          '<fmt:message key="will_expire_supplies_offer"/>','本月出库TOP10',"各门店昨日营业数据","昨日未上传分店","各事业群昨日营业数据","本月营业额走势图","菜品类别排行"];
			var id=["m_1_1","m_1_2","m_2_1","m_2_2","m_2_3","m_2_4","m_2_5","m_2_6","m_2_7","m_2_11","m_2_8","m_2_9","m_2_10"];
			for ( var i = 0; i < name.length; i++) {
				var colnum='<tr><td class="num" style="width: 25px;">'+(i+1)+'</td>';
				var colcheckbox='<td style="width:30px; text-align: center;"><input type="checkbox" name="idList" id='+id[i]+' value='+id[i]+' /></td>';
				var colmoduleName='<td style="width:180px;">'+name[i]+'&nbsp;</td>';
				$('.table-body').find('table').append(colnum+colcheckbox+colmoduleName);
			}
			<c:forTokens var="str" items="${role.menu}" delims="," varStatus="status">   
						$('#${str}').attr('checked',"true");
			</c:forTokens>  
			var tool = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_permissions_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveRoleMainTable();
						}
					},{
						text: '<fmt:message key="back_roles_page" />',
						title: '<fmt:message key="back_roles_page"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							 window.location.href="<%=path%>/role/list.do";
						}
					}
				 ]
			});
			
			if(result=='success'){
				alert('<fmt:message key="save_successful" />！');
			}
		});	
		function saveRoleMainTable(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');	
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="do_you_want_to_submit_data"/>？')){
					var chkValue = [];
					
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$('#menu').val(chkValue.join(","));
					$('#listForm').submit();
				}
			}else{
				alert('<fmt:message key="please_select_the_need_to_save_my_desktop"/>！');
				return ;
			}
		}
		
		function pageReload(){
			$('#roleOperateForm').submit();
		}
		</script>

	</body>
</html>