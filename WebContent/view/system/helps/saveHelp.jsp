<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.userInfo,.accountInfo {
				position: relative;
				top: 1px;
				background-color: #E1E1E1;
			}
			
			.userInfo {
				height: 91px;
				line-height: 91px;
			}
			
			.accountInfo {
				height: 91px;
				line-height: 91px;
			}
			
			.accountInfo .form-label{
				width: 80%;
			}
			
		</style>
	</head>
	<body>
	<div class="form">
		<div  style="height:300px; width:300px;left:50%;top:50%;margin:0px auto;margin-left:20px;margin-top:40px;">
			<form id="positnForm" method="post" action="<%=path %>/helps/saveHelps.do">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>项目名称：</div>
					<div class="form-input">
						<select name="projectName" id="projectName" onchange="projectToId(this)" class="select" style="width:134px;">
							<option value=""></option>
							<option value="物流系统" id="SCM">物流</option>
							<option value="决策系统" id="TELE">决策</option>
							<option value="会员系统" id="CRM">会员</option>
						</select>
						<input type="hidden" value="" name="projectId" id="projectId"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>模块上级名称：</div>
					<div class="form-input">
						<input type="text" id="parentName" name="parentName" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>模块名称：</div>
					<div class="form-input">
						<input type="text" id="childName" name="childName" class="text" onkeypress="checkKey()" onblur="getSpInit(this);suoxieYZ()"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>模块缩写：</div>
					<div class="form-input">
						<input type="text" id="init" name="id" class="text" onkeyup="suoxieYZ()"/><span id="sx"></span>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input">
						<input type="submit" value="保存"/>&nbsp;&nbsp;&nbsp;<input type="reset"/>
					</div>
				</div>
				
			</form>
		</div>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//提交
				$("form").submit(function(){
					if(validate._submitValidate()){
						return $("#init").data("result");
					}
					return false;
				})
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'projectName',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'parentName',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'childName',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'init',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					}]
				});
			});
			function projectToId(dat){
				$("input[name=projectId]").val($("option[value="+dat.value+"]").attr("id"));
			}
			function suoxieYZ(){
				$.ajax({
					type : 'POST',
					url : '<%=path%>/helps/validate.do',
					data : {id:$("#init").val(),projectId:$("#projectId").val()},
					dataType : 'json',
					success : function(dat) {
						if (dat=="success") {
							$("#init").data("result",true);
							$("#sx").css("color","green").text("缩写可用");
						}else{
							$("#init").data("result",false);
							$("#sx").css("color","red").text("缩写不可用");
						}
					}
				});
			}
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
			}
		</script>
	</body>
</html>