<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="" id="import" method="post" enctype="multipart/form-data">
			<div align="center" id="divImport">
			<br>
			<br>
						<FONT color=red>* </FONT>选择导入的文件：
								<input type="file" style="WIDTH: 250px" maxLength=5 name="file" id="file"/>
			</div> 
		</form>
		<br>							
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			var tool;
			$(document).ready(function(){
				tool	= _tool();
				});
			
			var _tool=function(){
				return $('.tool').toolbar({
					items: [
					 {
						text: '<fmt:message key="import" />',
						title: '导入物资信息',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							handle();
						}
					}
				]
				});
				}
			function handle(){
				var file = $("#file").val();
				if(file == ''){
					alert("请选择要导入的文件");
					return false;
				}
				 var pos = file.lastIndexOf(".");
				 var lastname = file.substring(pos,file.length) 
				
			   if (!(lastname.toLowerCase()==".dmp")){
				     alert("您上传的文件类型为"+lastname+"，文件必须为.dmp");
				     return false;
			   }else{
					$("#import").attr('action','<%=path%>/database/loadDmp.do').submit();
			  	}
			}
			
		</script>
	</body>
</html>