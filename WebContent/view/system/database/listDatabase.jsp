<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>log Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="" id="listForm" name="listForm" method="post">
		<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:180px;">表名</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="tables" items="${tablesList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${tables.name}' />" value="<c:out value='${tables.name}' />"/>
									</td>
									<td style="width:180px;"><c:out value="${tables.name}" />&nbsp;</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
		
			$(document).ready(function(){
				
				var tool = $('.tool').toolbar({
				items: [{
							text: '导出数据库',
							title: '导出数据库数据',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){

								var r=confirm("确认导出数据库吗？");
								  if (r==true)
								    {
									  exportDmp();
								    }
							}
						},{
							text: '导出以下数据库表',
							title: '导出数据库表',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								exporttables();	    
							}
						},{
							text: '导入数据库',
							title: '导入数据库',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['2px','2px']
							},
							handler: function(){
								//$('.search-div').slideToggle(200);
								importDmp();
							}
						}]
				});

			    function exportDmp(){
			    	$('body').window({
						id: 'window_exportDmp',
						title: '导出数据库',
						content: '<iframe id="exportDmp" frameborder="0" src="<%=path%>/database/exportDmp.do"></iframe>',
						width: '400px',
						height: '200px',
						draggable: true,
						isModal: true
					});
			    }
			    
			    function importDmp(){
			    	$('body').window({
						id: 'window_importSupply',
						title: '导入数据库',
						content: '<iframe id="importDmp" frameborder="0" src="<%=path%>/database/importDmp.do"></iframe>',
						width: '400px',
						height: '200px',
						draggable: true,
						isModal: true
					});
			    }
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				function exporttables(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm("是否要导出数据表？")){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							$('body').window({
								id: 'window_exportSupply',
								title: '导出数据库',
								content: '<iframe id="exportDmp" frameborder="0" src="<%=path%>/database/exportDmp.do?name=chkValue.join(",")"></iframe>',
								width: '400px',
								height: '200px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('请先选择需要导出的数据表！');
						return ;
					}
				}
				function viewlogs(){
					 pageReload();

				}
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
			});
		</script>
	</body>
</html>