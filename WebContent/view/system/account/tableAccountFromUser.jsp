<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.accountState_0 {
					background:url(<%=path %>/image/off.gif) no-repeat 7px center;
				}
				
				.accountState_1 {
					background:url(<%=path %>/image/on.gif) no-repeat 7px center;
				}
			</style>
		</head>
	<body>
		<input type="hidden" id="userId" name="userId" value="${userId}"/>
		<div id="toolbar"></div>
		<div class="grid">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"></td>
							<td class="chk">
								<span style="width:20px;"><input type="checkbox" id="chkAll"/></span>
							</td>
							<td style="width:150px;"><fmt:message key="name"/></td>
							<td style="width:150px;">名字</td>
							<td style="width:80px;"><fmt:message key="status"/></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="account" items="${accountList}" varStatus="status">
							<tr>
								<td class="num">${status.index+1}</td>
								<td class="chk">
									<input type="checkbox" name="idList" id="chk_${account.id}" value="<c:out value='${account.id}' />"/>
								</td>
								<td>
									<span style="width:140px;"><c:out value="${account.name}" />&nbsp;</span>
</td>
								<td>
									<span style="width:140px;"><c:out value="${account.names}" />&nbsp;</span>
								</td>
								<td class="center" id="state_${account.id}">
									<span style="width:70px;" class="accountState_${account.state}"><c:out value='${ACCOUNT_STATE[account.state]}' />&nbsp;</span>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/account.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			
			$('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="new_account_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							addAccountFromUser("<%=path%>",$('#userId').val());
						}
					},{
						text: '<fmt:message key="update" />',
						title: '<fmt:message key="modify_the_account_password"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if(getFirstID()){
								updateAccountFromUser("<%=path%>",getFirstID());
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete_account_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							  if(!confirm('<fmt:message key="delete_data_confirm"/>？'))return;
							if(getAllID()){
								deleteAccount('<%=path%>',getAllID());
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
								return ;
							}
						}
					},'-',{
						text: '<fmt:message key="enable"/>',
						title: '<fmt:message key="enable_account"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','-40px']
						},
						handler: function(){
							if(getAllID()){
								changeState("<%=path%>",getAllID(),"1","<c:out value='${ACCOUNT_STATE["1"]}' />");
							}else{
								alert('<fmt:message key="please_select_the_account_information_needs_to_be_enabled"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="disabled"/>',
						title: '<fmt:message key="disabled_account"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-40px']
						},
						handler: function(){
							if(getAllID()){
								changeState("<%=path%>",getAllID(),"0","<c:out value='${ACCOUNT_STATE["0"]}' />");
							}else{
								alert('<fmt:message key="please_select_need_to_disable_the_account_information"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key ="cancel" />',
						title: '<fmt:message key ="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$('.close',parent.document).click();
						}
					}
				]
			});//end toolbar
			
			//设置表格的整体高度
			setElementHeight('.grid',['#toolbar']);
			
			//设置表格数据展示部分的高度
			setElementHeight('.table-body',['.table-head'],'.grid');
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		});	//end $(document).ready();
		
		

		</script>
	</body>
</html>