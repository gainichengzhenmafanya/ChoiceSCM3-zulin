<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div class="leftFrame">
      <div id="toolbar"></div>
	    <div class="treePanel">
	      <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
          var tree = new MzTreeView("tree");
          
          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="sector"/>;method:changeUrl()';
          <c:forEach var="department" items="${departmentList}" varStatus="status">
	          	tree.nodes['${department.parentDepartment.id}_${department.id}'] 
	          		= 'text:${department.name}; method:changeUrl("${department.id}","${department.name}","${department.viewCode}")';
          </c:forEach>
          tree.setIconPath("<%=path%>/image/tree/none/");
          document.write(tree.toString());
        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    
    <input type="hidden" id="departmentId" name="departmentId" />
    <input type="hidden" id="departmentName" name="departmentName" />
    <input type="hidden" id="departmentCode" name="departmentCode" />
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.tab.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<%-- 		<script type="text/javascript" src="<%=path%>/js/system/account.js"></script> --%>
		<script type="text/javascript" src="<%=path%>/js/crm/crmAccount.js"></script>
		<script type="text/javascript">
			function changeUrl(departmentId,departmentName,departmentCode){
					$('#departmentId').val(departmentId);
					$('#departmentName').val(departmentName);
					$('#departmentCode').val(departmentCode);
				
				if(arguments.length === 3){
		      window.mainFrame.location = "<%=path%>/account/table.do?departmentCode="+departmentCode;
				}
	    }
			
	    function refreshTree(){
				window.location.href = '<%=path%>/account/list.do';
	    }
	    
	    function pageReload(){
	    	window.location.href = '<%=path%>/account/list.do?departmentId='+$("#departmentId").val();
	    }
	    
			$(document).ready(function(){
				
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						}
					]
				});//end toolbar
				
				setElementHeight('.treePanel',['#toolbar']);
				
				tree.focus('${department.id}');
				changeUrl('${department.id}','${department.name}','${department.code}');
				
			});// end $(document).ready();
			
			
			function changeState(path,ids,state,show,context){
				context = context || $(document.body);	
				$.ajax({
					url: path+'/account/ajaxChangeState.do',
					type: 'POST',
					data: 'state='+state+'&ids='+ids,
					dataType: 'html',
					success: function(info){
						if(info && info === 'T'){
							
							//弹出提示信息
							showMessage();					
							
							//修改选中账号的标识
							var idArray = ids.split(',');
							$.each(idArray,function(i){
								var id = idArray[i];
								$('#state_'+id,context)
									.children('span')
									.removeClass()
									.addClass('accountState_'+state)
									.html(show+"&nbsp;");
								$('#chk_'+id,context).removeAttr('checked');
							});
							
						}
					}
				});
			}

			function updateAccount(path,accountId){
				updateAccountWin = $('body').window({
					id: 'window_updateAccount',
					title: '<fmt:message key="modify_the_account_password" />',
					content: '<iframe id="updateAccountFrame" frameborder="0" src="'+path+'/account/update.do?id='+accountId+'"></iframe>',
					width: '650px',
					height: '500px',
					isModal: true,
					draggable: true,
					confirmClose: false
				});
			}

			function addAccount(path,userId){
				addAccountWin = $('body').window({
					id: 'window_addAccount',
					title: '<fmt:message key="new_account_information" />',
					content: '<iframe id="addAccountFrame" frameborder="0" src="'+path+'/account/add.do?userId='+userId+'"></iframe>',
					width: '650px',
					height: '500px',
					isModal: true,
					draggable: true,
					confirmClose: false
				});
			}

			function deleteAccount(path,param){
				$.ajax({
					type: 'POST',
					url: path+'/account/ajaxDelete.do',
					data: 'ids='+param,
					dataType: 'html',
					success: function(info){
						if(info && info === 'T'){
							//弹出提示信息
							showMessage({
								handler: function(){
									var accountTab = parent.window.accountTab;
									if(accountTab){
										var $tableFrame = accountTab.getItem('tab_tableAccount').div.find('#tableAccountFrame');
								    	$tableFrame.attr('src',$tableFrame.attr('src'));
									}else{
										window.pageReload();
									}
								}
							});					
						}
					}
				});
			}

			function relateRole(path,accountId){
				relateRoleWin = $('body').window({
					id: 'window_relateRole',
					title: '<fmt:message key="associated_with_the_role_of_information" />',
					content: '<iframe id="relateRoleFrame" frameborder="0" src="'+path+'/accountRole/table.do?accountId='+accountId+'"></iframe>',
					width: '650px',
					height: '500px',
					isModal: true,
					draggable: true,
					confirmClose: false
				});
			}
			
		</script>

	</body>
</html>