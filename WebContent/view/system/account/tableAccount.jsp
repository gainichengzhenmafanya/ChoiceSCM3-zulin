<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<style type="text/css">
				fieldset{
					padding: 5px;
					margin: 10px 6px;
					border: 1px solid #868686;
				}
				
				legend{
					font-size: 13px;
					font-weight: bold;
					font-style: italic;
					cursor: pointer;
					border: 1px solid #868686;
					padding-left: 20px;
				}
				
				.border_hover{
					border: 2px solid #4F98F6;
				}
				
				.color_hover{
					background-color: #4F98F6;
					color: #FFFFFF;
				}
				
				#info{
					position: relative;
					overflow: auto;
				}
				
				#info .userInfo, #info .accountInfo{
					position: relative;
					float: left;
				}
				
				.form-line,
				.form-line .form-label,
				.form-line .form-input {
					height: 26px;
				}
				
				.accountState_0 {
					background:url(<%=path %>/image/off.gif) no-repeat 7px center;
				}
				
				.accountState_1 {
					background:url(<%=path %>/image/on.gif) no-repeat 7px center;
				}
				
			</style>
	</head>
	<body>
		<input type="hidden" id="userId" name="userId" />
		<input type="hidden" id="ZHGLTSQX" value="${ZHGLTSQX }"/><!-- 用来判断是否显示 其他权限  wjf-->
		<div id="toolbar"></div>
		<div id="info">
			<c:forEach var="user" items="${userList}">
				<fieldset>
					<legend class="sex_${user.sex}" id="${user.id}">${user.name}【${user.code}】</legend>
					<div class="userInfo">
						<div class="form-line">
							<div class="form-label"><fmt:message key="sector"/></div>
							<div class="form-input">
								${user.department.name}
							</div>
						</div>
					</div>
					<div class="accountInfo">
						<div class="grid">
							<div class="table-head">
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td class="num"><span style="width:20px;">&nbsp;</span></td>
											<td class="chk">
												<span style="width:20px;"><input type="checkbox" id="chkAll_${user.id}" class="${user.id}"/></span>
											</td>
											<td><span style="width:80px;"><fmt:message key="account_name"/></span></td>
											<td><span style="width:80px;"><fmt:message key="account_names"/></span></td>
											<td><span style="width:60px;"><fmt:message key="status"/></span></td>
											<td><span style="width:120px;"><fmt:message key="Belong_to_role"/></span></td>
											<td><span style="width:80px;"><fmt:message key="Belong_to_store"/></span></td>
											<td><span style="width:80px;"><fmt:message key="Belong_to_supplier"/></span></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="account" items="${user.accountList}" varStatus="status">
											<tr>
												<td class="num"><span style="width:20px;">${status.index+1}</span></td>
												<td class="chk">
													<span style="width:20px;">
													<input type="checkbox" class="${user.id}"  id="chk_<c:out value='${account.id}' />" value="<c:out value='${account.id}' />"/>
													</span>
												</td>
												<td>
													<span style="width:80px;"><c:out value="${account.name}" />&nbsp;</span>
												</td>
												<td>
													<span style="width:80px;"><c:out value="${account.names}" />&nbsp;</span>
												</td>
												<td class="center" id="state_${account.id}">
													<span style="width:60px;" class="accountState_${account.state}"><c:out value='${ACCOUNT_STATE[account.state]}' />&nbsp;</span>
												</td>
												<td class="left">
													<span style="width:120px;" 
													title="<c:forEach var="role" items="${account.roleList}">
																${role.name}&nbsp;
															</c:forEach>">
														<c:forEach var="role" items="${account.roleList}">
															${role.name}&nbsp;
														</c:forEach>
													</span>
												</td>
												<td class="left">
													<span style="width:80px;" title="${account.positn.code}">${account.positn.des}&nbsp;</span>
												</td>
												<td class="left">
													<span style="width:80px;" title="${account.positn.pcode}">${account.positn.pname}&nbsp;</span>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</c:forEach>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/custom.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function(){
			//判断是否显示其他权限
			var zhgltsqx = $('#ZHGLTSQX').val();
			var use = false;
			if(zhgltsqx == 1){//1是启用
				use = true;
			}
			$('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="new_account_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($('#userId').val()){
								parent.addAccount("<%=path%>",$('#userId').val());
							}else{
								alert('<fmt:message key="please_select_personnel_need_to_add_account"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="update" />',
						title: '<fmt:message key="modify_the_account_password"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if(getFirstID()){
								parent.updateAccount("<%=path%>",getFirstID());
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="reset" /><fmt:message key="password" />',
						title: '<fmt:message key="reset" /><fmt:message key="password"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','0px']
						},
						handler: function(){
							if(getFirstID()){
								if(confirm('<fmt:message key="resetPassWordConfirm" />'))
									resetAccount(getFirstID());
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete_account_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if(getAllID()){
								parent.deleteAccount('<%=path%>',getAllID());
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
								return ;
							}
						}
					},'-',{
						text: '<fmt:message key="enable" />',
						title: '<fmt:message key="enable_account"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','-40px']
						},
						handler: function(){
							if(getAllID()){
								parent.changeState("<%=path%>",getAllID(),"1","<c:out value='${ACCOUNT_STATE["1"]}' />",
										$('#mainFrame',parent.window.document).contents());
							}else{
								alert('<fmt:message key="please_select_the_account_information_needs_to_be_enabled" />！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="disabled" />',
						title: '<fmt:message key="disabled_account"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-40px']
						},
						handler: function(){
							if(getAllID()){
								parent.changeState("<%=path%>",getAllID(),"0","<c:out value='${ACCOUNT_STATE["0"]}' />",
										$('#mainFrame',parent.window.document).contents());
							}else{
								alert('<fmt:message key="please_select_need_to_disable_the_account_information"/>！');
								return ;
							}
						}
					},'-',{
						text: '<fmt:message key="related_roles" />',
						title: '<fmt:message key="associated_with_the_role_of_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','-60px']
						},
						handler: function(){
							if(getFirstID()){
								parent.relateRole('<%=path%>',getFirstID());
							}else{
								alert('<fmt:message key="please_select_a_single_account_information_needs_associated_roles"/>！');
								return ;
							}
						}
					},{
						text: '<fmt:message key="Belong_to_store" />',
						title: '<fmt:message key="Belong_to_store" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','-60px']
						},
						handler: function(){
							toAccountFirm();
						}
					},{
						text: '<fmt:message key="Belong_to_supplier" />',
						title: '<fmt:message key="Belong_to_supplier" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','-60px']
						},
						handler: function(){
							toAccountSupplier();
						}
					},'-',{
						text: '<fmt:message key="positions" />',
						title: '<fmt:message key="assigned_positions_range"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
							toAccountPositn();
						}
 					},{
						text: '<fmt:message key="supplies" />',
						title: '<fmt:message key="assigned_supplies_range"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
							toAccountSupply();
						}
 					},{
						text: '<fmt:message key="suppliers" />',
						title: '<fmt:message key="assigned_suppliers_range"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-40px']
						},
						handler: function(){
							toAccountDeliver();
						}
 					},{
 						text: '<fmt:message key="branche" />',
 						title: '<fmt:message key="assigned_stores_permissions"/>',
 						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
 							position: ['-18px','-60px']
 						},
 						handler: function(){
 							toAssignAccountFirm();
 						}
					},{
 						text: '<fmt:message key="Other_permissions" />',
 						title: '<fmt:message key="Other_permissions" />',
 						useable:use,
 						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
 							position: ['-18px','-60px']
 						},
 						handler: function(){
 							otherPermission();
 						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});//end toolbar
			
			//设置列表区域的整体高度
			setElementHeight('#info',['#toolbar'],"",26);
			
			$('.userInfo').width($('.form-line').width());
			
			//设置账号列表信息的宽度
		  $('.accountInfo').width($('fieldset').width() - $('.userInfo').width() - 2);

			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//展开或者折叠账号列表
			$('legend').bind('dblclick',function(){
				var $self = $(this);
				
				if($self.siblings().is(':hidden')){
					$self.siblings().slideDown('normal');
				}else if($self.siblings().is(':visible')){
					$self.siblings().slideUp('normal');
				}
			});
			
			$('legend').bind('click',function(){
				var $self = $(this);
				
				$('#userId').val($self.attr('id'));
				
				//设置选中的样式
				$('fieldset').removeClass('border_hover');
				$('legend').removeClass('border_hover color_hover');
				$self.parent().addClass('border_hover');
				$self.addClass('border_hover color_hover');
			});
			
			//全选多选框事件
			$(':checkbox[id^="chkAll_"]').click(function(){
				//选择右侧所有的checkbox
				$('.'+$(this).attr('class'),$('.table-body')).attr('checked',$(this).attr('checked'));
			});
			//全选同步
			$(':checkbox[id^="chk_"]').click(function(){
				var $tmp=$('.'+$(this).attr('class')+':gt(0)');
				$('#chkAll_'+$(this).attr('class')).attr('checked',$tmp.length==$tmp.filter(':checked').length);
			});
		});	//end $(document).ready();
		//所属分店
		function toAccountFirm() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() < 1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var firmCode;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				firmCode=checkboxList.filter(':checked').parents('tr').find('td:eq(6)').find('span').attr("title");
			}
			var chkValue = [];
			checkboxList.filter(':checked').each(function(){
				chkValue.push($(this).val());
			});
			path="<%=path%>/positnRole/firmByAccount.do?accountId="+chkValue.join(',')+"&firmCode="+firmCode+"&typ=1203";
			customWindow = $('body').window({
				id: 'window_selectFirm',
				title: '<fmt:message key="Belong_to_store" />',
				content: '<iframe id="accountFirmFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="modify_supplier" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('accountFirmFrame')&&window.document.getElementById("accountFirmFrame").contentWindow.select_Positn()){
									submitFrameForm('accountFirmFrame','listForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		
		//所属供应商
		function toAccountSupplier() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() < 1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var defaultCode;
			if(checkboxList && checkboxList.filter(':checked').size() == 1){
				defaultCode=checkboxList.filter(':checked').parents('tr').find('td:eq(7)').find('span').attr("title");
			}
			if(!!!top.customWindow){
				var offset = getOffset('userId');
				top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull',
				function(deliverCode){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$.ajax({
						url:'<%=path%>/deliverPlat/saveAccountSupplier.do?accountId='+chkValue.join(',')+'&deliverCode='+deliverCode,
						type:'post',
						success:function(msg){
							alert('<fmt:message key="operation_successful" />！');
							parent.pageReload();
						}
					});
				});
			}
		}
		
		//分配仓位范围
		function toAccountPositn() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() !=1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var chkValue = [];
			checkboxList.filter(':checked').each(function(){
				chkValue.push($(this).val());
			});
			path="<%=path%>/positnRole/positnByAccount.do?accountId="+chkValue.join(',');
			customWindow = $('body').window({
				id: 'window_selectPositn',
				title: '<fmt:message key="assigned_positions_range"/>',
				content: '<iframe id="accountPositnFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="modify_supplier" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('accountPositnFrame')&&window.document.getElementById("accountPositnFrame").contentWindow.savePositnAccount()){
									submitFrameForm('accountPositnFrame','listForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		
		//分配账户可使用的物资
		function toAccountSupply() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() !=1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var accountId = $(checkboxList.filter(':checked')[0]).val();
			path="<%=path%>/accountSupply/addSupplyBatch.do?type=account&accountId="+accountId;
			var wzzhqx = "<c:out value='${WZZHQX}'/>";//判断具体到物资还是具体到小类
			if(wzzhqx == '1'){
				path = "<%=path%>/accountSupplyTyp/addSupplyTypBatch.do?accountId="+accountId;
			}
			customWindow = $('body').window({
				id: 'window_selectSupply',
				title: '<fmt:message key="assigned_supplies_range"/>', 
				content: '<iframe id="accountSupplyFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '510px',
				draggable: true,
				isModal: true
			});
		}
		
		//分配账户具有的供应商范围
		function toAccountDeliver() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() !=1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var accountId = $(checkboxList.filter(':checked')[0]).val();
			path="<%=path%>/accountDeliver/addDeliverBatch.do?accountId="+accountId;
			top.customWindow = $('body').window({
				id: 'window_selectDeliver',
				title: '<fmt:message key="assigned_suppliers_range"/>',
				content: '<iframe id="accountDeliverFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '500px',
				draggable: true,
				isModal: true
			});
		}
		
		//分配账户具有的分店范围
		function toAssignAccountFirm() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList&& checkboxList.filter(':checked').size() !=1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var accountId = $(checkboxList.filter(':checked')[0]).val();
			var action="<%=path %>/store/selectStores.do?accountId="+accountId;
			$('body').window({
				id: 'window_selectAccountFirm',
				title: '<fmt:message key ="Distribution_branch_range" />',
				content: '<iframe id="selectAccountFirmFrame" frameborder="0" src="'+action+'"></iframe>',
				width: 550,
				height: '500px',
				draggable: true,
				isModal: true
			});
		}
		
		//重置用户密码
		function resetAccount(userId){
			var url = '<%=path%>/accountPositn/saveByUpdate.do?action=resetPwd&id='+userId;
			$('body').window({
				id: 'window_resetPwd',
				title: '<fmt:message key="reset" /><fmt:message key="password" />',
				content: '<iframe id="resetPwdFrame" frameborder="0" src="'+url+'"></iframe>',
				width: 400,
				height: '300px',
				draggable: true,
				isModal: true
			});
		}
		
		//其他权限
		function otherPermission(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() !=1){
				alert('<fmt:message key="please_select_single_message"/>！');
				return;
			}
			var accountId = $(checkboxList.filter(':checked')[0]).val();
			var url = '<%=path%>/accountDis/otherPermission.do?accountId='+accountId;
			$('body').window({
				id: 'window_otherPermission',
				title: '<fmt:message key="Other_permissions"/>',
				content: '<iframe id="otherPermissionFrame" frameborder="0" src="'+url+'"></iframe>',
				width: 400,
				height: '300px',
				draggable: true,
				isModal: true
			});
		}
		</script>
	</body>
</html>