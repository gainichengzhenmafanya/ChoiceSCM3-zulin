<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					//background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
		<div id="toolbar"></div>
		<div class="form-group"><fmt:message key="personnel_information"/></div>
		<div class="userInfo">
			<div class="form-line">
				<div class="form-label"><fmt:message key="number"/></div>
				<div class="form-input">${user.code}</div>
				<div class="form-label"><fmt:message key="name"/></div>
				<div class="form-input">${user.name}</div>
			</div>
<!-- 			<div class="form-line"> -->
<%-- 				<div class="form-label"><fmt:message key="sex"/></div> --%>
<%-- 				<div class="form-input">${user.sex == "0" ? '<fmt:message key="male"/>' : '<fmt:message key="female"/>' }</div> --%>
<%-- 				<div class="form-label"><fmt:message key="sector"/></div> --%>
<%-- 				<div class="form-input">${user.department.name}</div> --%>
<!-- 			</div> -->
<!-- 			<div class="form-line"> -->
<%-- 				<div class="form-label"><fmt:message key="birthday"/></div> --%>
<%-- 				<div class="form-input"><fmt:formatDate value="${user.birthday}" type="date" /></div> --%>
<%-- 				<div class="form-label"><fmt:message key="status"/></div> --%>
<%-- 				<div class="form-input">${user.state == "0" ? '<fmt:message key="departure"/>' : '<fmt:message key="work"/>'}</div> --%>
<!-- 			</div> -->
		</div>
		<div class="form-group"><fmt:message key="account_information"/></div>
		<div class="accountInfo">
			<form id="accountForm" name="accountForm" method="post" action="<%=path %>/account/saveByAdd.do">
				<input type="hidden" id="userId" name="user.id" value="${user.id}" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="account_names"/></div>
					<div class="form-input">
						<input type="text" id="names" name="names" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="password"/></div>
					<div class="form-input">
						<input type="password" id="password" name="password" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="confirm_password"/></div>
					<div class="form-input">
						<input type="password" id="confimPassword" name="confimPassword" class="text" />
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		
		<script type="text/javascript">

		var validate;
			$(document).ready(function(){
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'name',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'names',
						validateType:['canNull'],
						param:['F'],
						error:['名字<fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'password',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="password"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'confimPassword',
						validateType:['canNull','accordance'],
						param:['F',$('#password')],
						error:['<fmt:message key="confirm_password"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="enter_two_passwords_dont_match"/>！']
					}]
				});
			
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_account_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){

								if(validate._submitValidate()){
									if ($('#password').val()!=$('#confimPassword').val()) {
										$('#confimPassword').focus();
									}else{
										saveAccount();
									}
								}
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close',parent.document).click();
							}
						}
					]
				});//end toolbar
				
			});//end $(document).ready()
			
			function saveAccount(){
				var name = $('#name').val();
// 				if(!/^\w+$/.test(name)){
// 					alert("用户名只能由字母数字下划线组成！");
// 					return;
// 				}
				$.ajax({
					type: 'POST',
					url: '<%=path %>/account/ajaxSaveByAdd.do',
					data: 'userId='+$('#userId').val()+'&name='+$('#name').val()+'&password='+$('#password').val()+'&names='+$('#names').val(),
					dataType: 'html',
					success: function(info){
						if(info && info == 'T'){
							
							//弹出提示信息
							showMessage({
								handler: function(){
									var accountTab = parent.window.accountTab,
										addAccountWin = parent.window.addAccountWin;
									if(accountTab){
							    	var $tableFrame = accountTab.getItem('tab_tableAccount').div.find('#tableAccountFrame');
							    	$tableFrame.attr('src',$tableFrame.attr('src'));
							    	
							    	accountTab.close('tab_addAccount');
							    	accountTab.show('tab_tableAccount');
									}else if(addAccountWin){
										parent.window.pageReload();
									}
								}
							});					
						}else{
							$('#name').addClass('warn').focus();
							
							//弹出提示信息
							showMessage({
								type: 'error',
								msg: info,
								speed: 1000
							});	
						}
					}
				});
				//checkPassword();
				//checkPasswordConfig();
				
			}

		</script>
	</body>
</html>