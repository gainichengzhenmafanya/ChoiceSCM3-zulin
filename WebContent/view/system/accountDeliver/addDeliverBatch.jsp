<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应商信息</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			.topFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.bottomFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.check{
				float: left;
				vertical-align: middle;
				margin-right: 0px;
				height: 30px;
				line-height: 30px;
				text-align: right;
				font-weight: bold;
				padding-right: 5px;
				background-color: #E1E1E1;
			}
			.formInput {
				height: 30px;
				vertical-align: middle;
				line-height: 30px;
				text-align: left;
				padding-left: 5px;
				background-color: #E1E1E1;
				white-space: normal;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<div class="topFrame">
		<form id="deliverBatchForm" method="post" action="<%=path%>/accountDeliver/updateAccounDeliver.do">
			<input type="hidden" id="accountId" name="accountId" class="text" readonly="readonly" value="${accountId}"/>
			<div class="form-line" style="background-color: #E1E1E1;height: 80px;;margin-right:0px;" id="deliverForm">
				<fmt:message key="suppliers"/>:<br/>
				<!-- <div style="overflow: auto;height: 60px;background-color: #E1E1E1;" id="addPoEle"></div> -->
				<input type="hidden" id="parentId" name="deliverId" class="text" readonly="readonly" value="${defaultCode}"/>
				<textarea style="width:780px; height:50px; boder:0px;background-color: #E1E1E1;" id="parentName" name="name" class="text" readonly="readonly" value="${defaultName}">
				</textarea>
			</div>
		</form>
	</div>
	<div class="bottomFrame" style="height:85%">
		<iframe src="<%=path%>/accountDeliver/selectNDeliver.do" frameborder="0" name="choiceFrame" id="choiceFrame"></iframe>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="determine_materials_selection"/>',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Deliver();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						},{
							text: '<fmt:message key ="empty" />',
							title: '<fmt:message key ="empty" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-20px']
							},
							handler: function(){
								$('#parentId').val('');
								$('#parentName').val('');//解决谷歌下清空不了 wjf
								chkCode = [];
								chkName = [];
								window.frames["choiceFrame"].clearValue();
							}
						}]
				});
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
			});
			function select_Deliver(){
				var code = $('#parentId').val();
				var accountId = $('#accountId').val();
				if(code != null && code != ''){
					//改为ajax 提交  wjf  解决谷歌下提交不了
					$.ajax({
						url:'<%=path%>/accountDeliver/updateAccounDeliver.do?deliverId='+code+'&accountId='+accountId,
						type:'post',
						success:function(data){
							alert('<fmt:message key ="update_successful" />!');
						}
					});
// 					top.customWindow.afterCloseHandler('Y');
// 					$('#deliverBatchForm').submit();
// 					top.closeCustom();
				}else{
					alert('<fmt:message key ="please_select_at_least_one_data" />！');
				}
			}
			
			var defaultCode = '${defaultCode}';
			var defaultName = '${defaultName}';
			if(defaultCode!='' && defaultName!=''){
				$('#deliverBatchForm').find('div').find('input').val(defaultCode);
				$('#deliverBatchForm').find('div').find('textarea').val(defaultName);
			}
			var chkCode = defaultCode==''?[]:defaultCode.split(',');
			var chkName = defaultName==''?[]:defaultName.split(',');
			function selectDeliver(id, name){
				var m = jQuery.inArray(id, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					chkCode.push(id);
					chkName.push(name);
				}
				$('#deliverBatchForm').find('div').find('input').val(chkCode);
				$('#deliverBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectAllDeliver(id, name){
				var m = jQuery.inArray(id, chkCode);
				if (m>=0) {
					return;
				}else {
					chkCode.push(id);
					chkName.push(name);
				}
				$('#deliverBatchForm').find('div').find('input').val(chkCode);
				$('#deliverBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectZeroDeliver(id, name){
				var m = jQuery.inArray(id, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					return;
				}
				$('#deliverBatchForm').find('div').find('input').val(chkCode);
				$('#deliverBatchForm').find('div').find('textarea').val(chkName);
			}
			
		</script>
	</body>
</html>