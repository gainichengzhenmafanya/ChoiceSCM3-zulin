<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<style type="text/css">
				.leftFrame{
					float:left;
					 width:25%;
				}
				.mainFrame{
					float:left;
					padding-left:1%;
				 	width:72%;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="leftFrame">
	      	<form id="listForm" action="<%=path%>/accountDeliver/selectNDeliver.do" method="post">
				<div class="grid" style="overflow-y: auto;">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span class="num" style="width: 50px;">&nbsp;</span></td>
									<td><span style="width:182px;"><fmt:message key="name" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="deliver" items="${delivers}" varStatus="status">
									<tr>
										<td class="num"><span style="width:50px;">${status.index+1}</span></td>
										<td><span style="width:182px;">${deliver.typ}</span></td>
										<td><input type="hidden" id="typCode" name="typCode" value="${deliver.typCode}"/>	</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
			</form>
	    </div>
	    <div class="mainFrame">
	        <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
		<input type="hidden" id="id" name="id" />
	    <input type="hidden" id="des" name="des" />
	    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				var typCode = $(this).find('td:eq(2)').find('input').val();
				$("#mainFrame").attr("src","<%=path%>/accountDeliver/selectTableNDeliver.do?typCode="+typCode);
			});
			
			$('.grid').find('.table-body').find('tr:eq(0)').click();
		});// end $(document).ready();
		//清空页面
		function clearValue(){
			window.frames["mainFrame"].clearValue();
		}
	</script>

	</body>
</html>