<%@ page import="com.choice.framework.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_operation_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<style type="text/css">
				#toolbar {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
	<div class="form-group"><fmt:message key="the_operating_range_of_the_role_permissions"/></div>
	<div class="leftFrame">
	<div id="treeToolbar"></div>
	    <div class="treePanel">
	        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<c:out value="${role.name}" /><fmt:message key="permission_directory"/>';
	          <c:forEach var="module" items="${roleOperateList}" varStatus="status">
		          		tree.nodes['00000000000000000000000000000000_${module.id}'] = 'text:${lo:show(module.name)}';
		          		<c:forEach var="operate" items="${module.childOperate}" varStatus="status">
			          		tree.nodes['${module.id}_${operate.id}'] = 'text:${lo:show(operate.name)};dbmethod:selectItem(\'${lo:show(operate.name)}\',\'${operate.id}\')';
		          		</c:forEach>
		      </c:forEach>
	          tree.setIconPath("<%=path%>/js/tree/");
	          document.write(tree.toString());
	          tree.expandAll();
	        </script>
	    </div>
	    </div>
	    <div class="mainFrame" >
	    <div id="toolbar"></div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="3">
								<fmt:message key="role_name"/>：<c:out value="${role.name}" />
								</td>
							</tr>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td> 
								<td style="width:30px;">
									<input type="checkbox" id="chkAll" />
								</td>
								<td id="typeList" style="width:690px;text-align:left;">
									<span id="operateName"></span>
									<input id="operateId" name="operateId" type="hidden" />
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="display: none;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="department" items="${departmentUserList}" varStatus="num">
							<tr id="col_dept_<c:out value="${department.id}" />"  style="background-color: #677FB2;color: #fff">
								<td  style="width:25px; text-align: center;">
									<input id="chk_dept_<c:out value="${department.id}" />" type="checkbox"  value="<c:out value="${department.id}" />" />
								</td>
								<td style="width: 720px" >
									&nbsp;&nbsp;<c:out value="${department.name}" />&nbsp;
								</td>
							</tr>
							<tr id="col_user_<c:out value="${department.id}" />">
							<td class="num" style="width: 25px;"></td>
							<td style="text-align: left;">
								<c:forEach var="user" items="${department.userList}" varStatus="operateNum">
									<span style="width: 90px;float: left;">
										&nbsp;&nbsp;
										<input class="chk_user_<c:out value="${user.id}" />" type="checkbox" value="<c:out value="${user.id}" />" />
										<c:out value="${user.name}" />&nbsp;&nbsp;
									</span>
									<c:if test="${operateNum.index > 0 && operateNum.index%6 == 0 }">
											<br/>
									</c:if>
								</c:forEach>
							</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
    	</div>
    	<form id="roleOperateForm" action="<%=path%>/roleOperate/list.do">
    		<input type="hidden" id="id" name="id" value="${role.id}"/>
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		var colNum=1;
		var bodyWindow;
		$(document).ready(function(){
			(function(){
			    // remove layerX and layerY
			    var all = $.event.props,
		        len = all.length,
		        res = [];
			    while (len--) {
			    var el = all[len];
			    if (el != 'layerX' && el != 'layerY') res.push(el);
			    }
			    $.event.props = res;
		    }());
			var tooltree = $('#treeToolbar').toolbar({
				items: [{
						text: '<fmt:message key="expandAll" />',
						title: '<fmt:message key="expandAll"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					}
				]
			});
			var tool = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_permissions_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveRoleOperateRangeByAdd();
						}
					},"-",{
						text: '<fmt:message key="returns_the_current_role_permissions_assigned_page" />',
						title: '<fmt:message key="returns_the_current_role_permissions_assigned_page"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							toNowRoleOperate();
						}
					},{
						text: '<fmt:message key="back_roles_page" />',
						title: '<fmt:message key="back_roles_page"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							 window.location.href="<%=path%>/role/list.do";
						}
					}
				 ]
			});
			
			//全选多选框事件
			$('#chkAll').bind('change',function(){
				$(':checkbox').attr('checked',$(this).attr('checked'));
			});
			
			//行全选多选框事件
			$(':checkbox[id^="chk_"]').live('change',function(){
				//选择右侧所有的checkbox
				$(':checkbox[class^="chk_"]',$(this).closest('tr').next('tr[id^="col_user"]')).attr('checked',$(this).attr('checked'));
				checkAll();
			});
			
			//选项多选框事件
			$(':checkbox[class^="chk_"]',$('.table-body')).live('change',function(){
				checkLine();
				checkAll();
			});
			
			//种类多选框事件
			$(':checkbox','#typeList').bind('change',function(){
				$('.'+$(this).attr('class'),$('.table-body')).attr('checked',$(this).attr('checked'));
				checkLine();
				checkAll();
			});

			setElementHeight('.treePanel',['#toolbar']);
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		 	 //loadGrid();//  自动计算滚动条的js方法
			var $grid = $('.grid'), 
			$thead = $('.table-head',$grid),
			$tbody = $('.table-body',$grid),
			docWidth = $(document.body).width()-180,
			headWidth = $thead.find('table').outerWidth()-180,
			bodyWidth = $tbody.find('table').outerWidth()-180,
			headWidth = (headWidth > docWidth || headWidth + 17 > docWidth)
				? headWidth + 17 
				: docWidth,
			bodyWidth = (bodyWidth > docWidth || bodyWidth + 17 > docWidth)
				? bodyWidth + 17 
				: docWidth;

			$thead.width(headWidth);
			$tbody.width(bodyWidth);
			
			if(headWidth > docWidth){
				$grid.width(docWidth).css('overflow-x','auto');
				$tbody.height($tbody.height() - 17);
			}else{
				$grid.width(docWidth).css('overflow-x','hidden');		
			}
		});
		
		
		function toNowRoleOperate(){
			$('#roleOperateForm').submit();
		}
		
		//按照类别检查操作列表多选框是否需要选中
		function checkType(){
			$(':checkbox','#typeList').attr('checked','checked');
			$(':checkbox','#typeList').each(function(){
				$('.'+$(this).attr('class'),$('.table-body')).each(function (){
					if(!$(this).is(':checked')){
						$('.'+$(this).attr('class'),'#typeList').removeAttr('checked');
						return false;
					}else{
						$('.'+$(this).attr('class'),'#typeList').attr('checked','checked');
					}
				});
			});
		}
		
		//按照行计算每行的多选框是否选中
		function checkLine(){
			$('tr[id^="col_user"]',$('.table-body')).each(function (){
				$(this).find(':checkbox[class^="chk_"]').each(function (){
					if(!$(this).is(':checked')){
						$(this).closest('tr').prev('tr[id^="col_dept"]').find(':checkbox[id^="chk_"]').removeAttr('checked');
						return false;
					}else{
						$(this).closest('tr').prev('tr[id^="col_dept"]').find(':checkbox[id^="chk_"]').attr('checked','checked');
					}
				});
				
			});
		}
		
		//按照所有选项计算全选多选框是否选中
		function checkAll(){
			$(':checkbox[id^="chk_"]',$('.table-body')).each(function(){
				if(!$(this).is(':checked')){
					$('#chkAll').removeAttr('checked');
					return false;
				}
				else
					$('#chkAll').attr('checked','checked');
			});
		}
		

		//点击模块树通过AJAX方式在后台获取权限列表并加载到页面上
		function selectItem(name,id){
			jQuery.ajax({
				type : 'POST',
				contentType : 'application/json',
				url : '<%=path%>/roleOperateRange/add.do',
				data : '{"role_operateId":"'+id+'"}',
				dataType : 'json',
				success : function(data) {

					$('.table-body').css('display','block');
					$('#typeList #operateName').text(' [ '+name+'<fmt:message key="competence"/> ] <fmt:message key="operating_range"/>');
					$('#typeList #operateId').val(id);
					$(':checkbox').removeAttr('checked');
					var deptIds=(data.departmentIds).split(",");
					var userIds=(data.userIds).split(",");
					for(var i=0;i<deptIds.length;i++){
						$('#chk_dept_'+deptIds[i]).attr('checked','checked');
					}
					for(var i=0;i<userIds.length;i++){
						$('.chk_user_'+userIds[i]).attr('checked','checked');
					}
					checkAll();
   				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert(XMLHttpRequest+"|"+textStatus+"|"+errorThrown);
				}
			});
		}
		
		function saveRoleOperateRangeByAdd(){
			var checkUserList=$(':checkbox[class^="chk_user"]',$('.table-body'));
			var checkDeptList=$(':checkbox[id^="chk_dept"]',$('.table-body'));
			if(checkUserList 
					&& checkUserList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="do_you_want_to_submit_data"/>？')){
					var chkUserValue = [];
					var chkDeptValue = [];
					checkUserList.filter(':checked').each(function(){
						chkUserValue.push($(this).val());
					});
					checkDeptList.filter(':checked').each(function(){
						chkDeptValue.push($(this).val());
					});
					bodyWindow=$('body').window({
						title: '<fmt:message key="tips"/>',
						content: '<iframe id="updateRoleOperateFrame" frameborder="0" src="<%=path%>/roleOperateRange/saveByAdd.do?role_operateId='+$('#typeList #operateId').val()+'&departmentIds='+chkDeptValue.join(",")+'&userIds='+chkUserValue.join(",")+'"></iframe>',
						width: 538,
						height: '185px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}
			}else{
				alert('<fmt:message key="please_select_the_operating_range_needs_to_be_saved"/>！');
				return ;
			}
		}
		
		function pageReload(){
			bodyWindow.close();
		}
		</script>

	</body>
</html>