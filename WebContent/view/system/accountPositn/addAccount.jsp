<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="personnel_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />		
	</head>
	<body>
		<div class="form">
			<form id="accountForm" method="post" action="<%=path %>/accountPositn/saveByAdd.do">
				<div class="form-line">
					<div class="form-label">用户名</div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text"/>
					</div>
					<div class="form-label"><fmt:message key="password"/></div>
					<div class="form-input">
						<input type="password" id="password" name="password" class="text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="confirm_password"/></div>
					<div class="form-input">
						<input type="password" id="confimPassword" name="confimPassword" class="text" />
					</div>
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">姓名</div>
					<div class="form-input">
						<input type="text" id="names" name="names" class="text"/>
					</div>
					<div class="form-label"><fmt:message key="sex"/></div>
					<div class="form-input">
						<input type="radio" id="sex_0" name="sex" value="0" checked="checked"/><fmt:message key="male"/>
						<input type="radio" id="sex_1" name="sex" value="1"/><fmt:message key="female"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">分店-仓位</div>
					<div class="form-input">
						<input type="text" id="positnDes" name="positn.des" class="selectDepartment text"/>
						<input type="hidden" id="positnCode" name="positn.code" value="${positnId}"/>
					</div>
					<div class="form-label"><fmt:message key="birthday"/></div>
					<div class="form-input">
						<input type="text" id="birthday" name="birthday" class="Wdate text" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="status"/></div>
					<div class="form-input">
						<select id="state" name="state" class="select">
							<c:forEach var="account_state" items="${ACCOUNT_STATE}">
								<option value="${account_state.key}">${account_state.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>		
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				$('#positnDes').val($(window.parent.parent.document).find("input[name='positnDes']").val());

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'name',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/accountPositn/nameIsExist.do",{name:$("#name").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F'],
						error:['用户名<fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'name',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="user_name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="user_name_input_extended"/>']
					},{
						type:'text',
						validateObj:'password',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="password"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="password_input_extended"/>']
					},{
						type:'text',
						validateObj:'confimPassword',
						validateType:['canNull','accordance','maxLength'],
						param:['F',$('#password'),'100'],
						error:['<fmt:message key="confirm_password"/><fmt:message key="cannot_be_empty"/>！','两次输入的密码不一致','<fmt:message key="password_input_extended"/>']
					},{
						type:'text',
						validateObj:'code',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="number"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="number_input_extended"/>']
					},{
						type:'text',
						validateObj:'names',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="name_enter_the_ultra_long"/>']
					},{
						type:'text',
						validateObj:'birthday',
						validateType:['canNull','date'],
						param:['F'],
						error:['<fmt:message key="birthday"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="birthday"/><fmt:message key="incorrect_format_input"/>']
					}]
				});
				$('#positnDes').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('name');
						top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
					}
				});
				if($('#departmentId',$(parent.document)) && $('#departmentId',$(parent.document)).val())
					$('#departmentId').val($('#departmentId',$(parent.document)).val());
				
				if($('#departmentName',$(parent.document)) && $('#departmentName',$(parent.document)).val())
					$('#departmentName').val($('#departmentName',$(parent.document)).val());
				
				$('#departmentName').bind('focus.selectDepartment',function(e){
					if(!!!top.departmentWindow){
						var offset = getOffset('departmentName');
						top.selectDepartment('<%=path%>',offset,$(this),$('#departmentId'),$('#departmentId').val());
					}
				});
				
				$('#birthday').bind('click',function(){
					new WdatePicker();
				});
			});
		</script>
	</body>
</html>