<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="personnel_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			
			<style type="text/css">
				.accountState_0 {
					background:url(<%=path%>/image/off.gif) no-repeat 7px center;
				}
				
				.accountState_1 {
					background:url(<%=path%>/image/on.gif) no-repeat 7px center;
				}
				#tool {
					position: relative;
					height: 27px;
				}
				
			</style>
		</head>
	<body>
		<div id="tool"></div>
		<div class="grid">
			<form id="listForm" action="<%=path%>/accountPositn/tableFromPositn.do" method="post">
				<input type="hidden" id="positnId" name="positnId" value="${positnId}"/>
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td class="chk">
									<span style="width:20px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td style="width:100px;"><fmt:message key="personnel_number"/></td>
								<td style="width:100px;"><fmt:message key="personnel_name"/></td>
								<td style="width:100px;"><fmt:message key="user_name"/></td>
								<td style="width:95px;"><fmt:message key="birthday"/></td>
								<td style="width:40px;"><fmt:message key="sex"/></td>
								<td style="width:70px;"><fmt:message key="status"/></td>
								<td style="width:140px;"><fmt:message key="branches_positions" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="accountPositn" items="${listAccount}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td class="chk">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_<c:out value='${accountPositn.id}' />" value="<c:out value='${accountPositn.id}' />"/></span>
									</td>
									<td>
										<span style="width:90px;"><c:out value="${accountPositn.code}" />&nbsp;</span>
									</td>
									<td>
										<span style="width:90px;"><c:out value="${accountPositn.names}" />&nbsp;</span>
									</td>
									<td>
										<span style="width:90px;"><c:out value="${accountPositn.name}" />&nbsp;</span>
									</td>
									<td class="center">
										<span style="width:85px;"><fmt:formatDate value="${accountPositn.birthday}" type="date" />&nbsp;</span>
									</td>
									<td class="center">
										<span style="width:30px;"><c:out value='${SEX_RADIO[accountPositn.sex]}' />&nbsp;</span>
									</td>
									<td class="center" id="state_${accountPositn.id}">
										<span style="width:60px;" class="accountState_${accountPositn.state}">&nbsp;&nbsp;<c:out value='${ACCOUNT_STATE[accountPositn.state]}' />&nbsp;</span>
									</td>
									<td class="center">
										<span style="width:130px;"><c:out value="${accountPositn.positn.des}" />&nbsp;</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
				<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>

		<script type="text/javascript">
		$(document).ready(function(){
			
			var tool = $('#tool').toolbar({
				items: [{
					text: '<fmt:message key="insert" />',
					title: '<fmt:message key="new_account_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						addAccount();
					}
				},{
					text: '<fmt:message key="modify_the_account_password" />',
					title: '<fmt:message key="modify_the_account_password"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						updateAccountPassWord();//修改密码
					}
				},{
					text: '<fmt:message key="update" />基本信息',
					title: '<fmt:message key="modify_the_account_password"/>基本信息',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						updateAccount();//修改账号基本信息
					}
				},{
					text: '<fmt:message key="delete" />',
					title: '<fmt:message key="delete_account_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						deleteAccount();
					}
				},{
					text: '<fmt:message key="enable" />',
					title: '<fmt:message key="enable_account"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','-40px']
					},
					handler: function(){
						if(getAllID()){
							parent.changeState("<%=path%>",getAllID(),"1","<c:out value='${ACCOUNT_STATE["1"]}' />",
									$('#mainFrame',parent.window.document).contents());
						}else{
							alert('<fmt:message key="please_select_the_account_information_needs_to_be_enabled" />！');
							return ;
						}
					}
				},{
					text: '<fmt:message key="disabled" />',
					title: '<fmt:message key="disabled_account"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-40px']
					},
					handler: function(){
						if(getAllID()){
							parent.changeState("<%=path%>",getAllID(),"0","<c:out value='${ACCOUNT_STATE["0"]}' />",
									$('#mainFrame',parent.window.document).contents());
						}else{
							alert('<fmt:message key="please_select_need_to_disable_the_account_information"/>！');
							return ;
						}
					}
				},'-',{
					text: '<fmt:message key="related_roles" />',
					title: '<fmt:message key="associated_with_the_role_of_information"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','-60px']
					},
					handler: function(){
						if(getFirstID()){
							parent.relateRole('<%=path%>',getFirstID());
						}else{
							alert('<fmt:message key="please_select_a_single_account_information_needs_associated_roles"/>！');
							return ;
						}
					}
				},'-',{
					text: '重置密码',
					title: '重置密码',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						resetAccountPassWord();//重置密码
					}
				},'-',{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){								
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
					}
				}
			]
			});	//end $('#tool').toolbar()

			//设置表格的总体高度
			setElementHeight('.grid',['.tool'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
		});	//end $(document).ready();
		function addAccount(){
			$('body').window({
				id: 'window_saveAccount',
				title: '新增账号信息',
				content: '<iframe id="saveAccountFrame" frameborder="0" src="<%=path%>/accountPositn/add.do?positnId='+$('#positnId').val()+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true,
				confirmClose: true,
				topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('saveAccountFrame')&&window.document.getElementById("saveAccountFrame").contentWindow.validate._submitValidate()){
									submitFrameForm('saveAccountFrame','accountForm');
								}
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		//修改账号密码
		function updateAccountPassWord(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() ==1){
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				$('body').window({
					title: '<fmt:message key="update" />',
					content: '<iframe id="updateAccountFrame" frameborder="0" src="<%=path%>/accountPositn/updatePasssWord.do?id='+chkValue+'"></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true
					
				});
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
				alert('<fmt:message key="please_select_data" />！');
				return ;
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
				return ;
			}
		}
		//修改账号基本信息
		function updateAccount(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() ==1){
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				$('body').window({
					title: '<fmt:message key="update" />',
					content: '<iframe id="updateAccountFrame" frameborder="0" src="<%=path%>/accountPositn/update.do?id='+chkValue+'"></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="update" />',
								title: '<fmt:message key="update" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('updateAccountFrame')&&window.document.getElementById("updateAccountFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('updateAccountFrame','accountForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="modify_cancelled" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
				alert('<fmt:message key="please_select_data" />！');
				return ;
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
				return ;
			}
		}
		//删除
		function deleteAccount(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm"/>？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					var action = '<%=path%>/accountPositn/delete.do?ids='+chkValue.join(",");
					$('body').window({
						title: '<fmt:message key="delete_account_information"/>',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
				return ;
			}
		}
		//重置密码
		function resetAccountPassWord(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				if(confirm('确定要重置密码？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					var action = '<%=path%>/accountPositn/saveByUpdate.do?action=resetPwd&id='+chkValue.join(",");
					$('body').window({
						title: '重置密码',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
				alert('请选择一条数据！');
				return ;
			}else{
				alert('请先选择需要重置的数据！');
				return ;
			}
		}
		
		</script>
	</body>
</html>