<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
		<div class="form">
			<form id="moduleForm" method="post" action="<%=path %>/module/saveByAdd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key="name_simplified"/></div>
					<div class="form-input"><input type="text" id="name" name="name" class="text"/></div>
					<div class="form-label"><fmt:message key="name_traditional"/></div>
					<div class="form-input"><input type="text" id="name" name="name" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name_english"/></div>
					<div class="form-input"><input type="text" id="name" name="name" class="text"/></div>
					<div class="form-label">URL</div>
					<div class="form-input"><input type="text" id="url" name="url" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="superior_module"/></div>
					<div class="form-input">
						<input type="text" id="parentModuleName" name="parentModule.name" class="text" 
							value="<c:out value='${module.parentModule.name}' default='模块' />" />
						<input type="hidden" id="parentModuleId" name="parentModule.id" class="text" 
							value="<c:out value='${module.parentModule.id}' default='00000000000000000000000000000000'/>" />
					</div>
					<div class="form-label"><fmt:message key="appear_order"/></div>
					<div class="form-input"><input type="text" id="sequence" name="sequence" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="whether_to_enable"/></div>
					<div class="form-input">
						<select id="useable" name="useable" class="select">
							<option value="T"><fmt:message key="enable"/></option>
							<option value="F"><fmt:message key="disable1"/></option>
						</select>
					</div>
					<div class="form-label"><fmt:message key="whether_to_display"/></div>
					<div class="form-input">
						<select id="showable" name="showable" class="select">
							<option value="T"><fmt:message key="display"/></option>
							<option value="F"><fmt:message key="not_displayed"/></option>
						</select>
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		
		<script type="text/javascript">

		var validate;
			$(document).ready(function(){
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'name',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'sequence',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key="appear_order"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="display_order_must_be_numeric"/>']
					}]
				});
				
				var $parentModuleId = $('treeform > #id',$(parent.document)),
					$parentModuleName = $('#treeform > #name',$(parent.document));
			
				if($parentModuleId && $parentModuleId.val())
					$('#parentModuleId').val($parentModuleId.val());
				
				if($parentModuleName && $parentModuleName.val())
					$('#parentModuleName').val($parentModuleName.val());
				
				$('#parentModuleName').bind('focus.select',function(){
					if(!!!top.selectWindow){
						top.selectWin({
							src: '<%=path%>/module/select.do?code='+$("#code").val(),
							title: '<fmt:message key="select_the_module"/>',
							offset: getOffset('parentModuleName'),
							items:[{
								input: $(this),
								selectInput: '#moduleName'
							},{
								input: $('#parentModuleId'),
								selectInput: '#moduleId'
							}]
						});
					}
				});
			});
		</script>
	</body>
</html>