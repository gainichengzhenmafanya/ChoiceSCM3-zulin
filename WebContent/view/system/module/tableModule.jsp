<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>module Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			
			<style type="text/css">
				#tool,#toolModule {
					position: relative;
					height: 27px;
				}
				
				.condition {
					position: relative;
					top: 1px;
					line-height: 81px;
				}
				.moduleInfo {
					position: relative;
					top: 1px;
					height: 90px;
					line-height: 90px;
					background-color: #E1E1E1;
				}
				.form-line .form-label{
					width: 70px;
				}
				
				.form-line .form-input , .form-line .form-input input[type=text],.form-line .form-input select{
					width: 120px;
				}
				.form-line:first-child{
					margin-left: 0px;
				}
			</style>
		</head>
	<body>
	<c:if test="${moduleAll.id!='00000000000000000000000000000000'}">
<!-- 	<div class="form-group">模块基本信息</div> -->
		<div id="toolModule"></div>
		<div class="moduleInfo">
			<form id="pageUpdateForm" name="pageUpdateForm" method="post" action="<%=path %>/module/saveByUpdate.do">
				<input type="hidden" id="id" name="id" value="<c:out value='${moduleAll.id}' />" />
				<input type="hidden" id="code" name="code" value="<c:out value='${moduleAll.code}' />" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="name_simplified"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" value="<c:out value='${lo:sift(moduleAll.name,0)}' />" />
					</div>
					<div class="form-label"><fmt:message key="name_traditional"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" value="<c:out value='${lo:sift(moduleAll.name,1)}' />" />
					</div>
					<div class="form-label"><fmt:message key="name_english"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" value="<c:out value='${lo:sift(moduleAll.name,2)}' />" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="no"/></div>
					<div class="form-input">
						<input type="text" id="sequence" name="sequence" class="text" value="<c:out value='${moduleAll.sequence}' />" />
					</div>
					<div class="form-label">URL</div>
					<div class="form-input">
						<input type="text" id="url" name="url" class="text" value="<c:out value='${moduleAll.url}' />" />
					</div>
					<div class="form-label"><fmt:message key="superior_module"/></div>
					<div class="form-input">
						<input type="text" id="parentModuleName" name="parentModule.name" class="text" 
							value="<c:out value='${lo:show(moduleAll.parentModule.name)}' default='模块' />" />
						<input type="hidden" id="parentModuleId" name="parentModule.id" class="text" 
							value="<c:out value='${moduleAll.parentModule.id}' default='00000000000000000000000000000000'/>" />
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="whether_to_enable"/></div>
					<div class="form-input">
						<select id="useable" name="useable" class="select">
							<option value="T"
								<c:if test="${moduleAll.useable=='T'}">
								selected="selected"
								</c:if>
							 ><fmt:message key="enable"/></option>
							<option value="F" 
								<c:if test="${moduleAll.useable=='F'}">
								selected="selected"
								</c:if>
							><fmt:message key="disable1"/></option>
						</select>
					</div>
					<div class="form-label"><fmt:message key="whether_to_display"/></div>
					<div class="form-input">
						<select id="showable" name="showable" class="select">
							<option value="T"
								<c:if test="${moduleAll.showable=='T'}">
								selected="selected"
								</c:if>
							 ><fmt:message key="display"/></option>
							<option value="F" 
								<c:if test="${moduleAll.showable=='F'}">
								selected="selected"
								</c:if>
							><fmt:message key="not_displayed"/></option>
						</select>
					</div>
				</div>
			</form>
		</div>
		</c:if>
<%-- 		<div class="form-group">
		<c:if test="${moduleAll.name !=''}">
		<c:out value='${moduleAll.name}' /> - 
		</c:if>
		下级模块信息</div> --%>
		<div id="tool"></div>
		<div class="condition">
		</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num">&nbsp;</td>
								<td class="chk">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:140px;"><fmt:message key="module_name"/></td>
								<td style="width:140px;"><fmt:message key="superior_module"/></td>
								<td style="width:80px;"><fmt:message key="whether_to_enable"/></td>
								<td style="width:80px;"><fmt:message key="whether_to_display"/></td>
								<td style="width:80px;"><fmt:message key="no"/></td>
								<td style="width:350px;">URL</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="module" items="${moduleAll.childModule}" varStatus="status">
								<tr>
									<td class="num">${status.index+1}</td>
									<td  class="chk">
										<input type="checkbox" name="idList" id="chk_<c:out value='${module.id}' />" value="<c:out value='${module.id}' />"/>
									</td>
									<td style="width:140px;">
										<c:out value="${lo:show(module.name)}" />&nbsp;
									</td>
									<td style="width:140px;">
										<c:if test="${moduleAll.name!=''}">
											<c:out value="${lo:show(moduleAll.name)}" />
										</c:if>
										<c:if test="${moduleAll.name==''}">
											<fmt:message key="root_module"/>
										</c:if>
									&nbsp;</td>
									<td style="width:80px;">
										<c:if test="${module.useable=='T'}">
											<fmt:message key="enable"/>
										</c:if>
										<c:if test="${module.useable=='F'}">
											<fmt:message key="disable1"/>
										</c:if>
									&nbsp;</td>
									<td style="width:80px;">
										<c:if test="${module.showable=='T'}">
											<fmt:message key="display"/>
										</c:if>
										<c:if test="${module.showable=='F'}">
											<fmt:message key="not_displayed"/>
										</c:if>
									&nbsp;</td>
									<td style="width:80px;">
										<c:out value="${module.sequence}" />&nbsp;
									</td>
									<td style="width:350px;">
										<c:out value="${module.url}" />&nbsp;
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>

		<script type="text/javascript">
		
			//页面上修改模块信息
			function updateModuleOnPage(){
				$('#pageUpdateForm').submit();
				 //window.parent.updateModuleFromPage($('#pageUpdateForm').serialize());
			}
			//打开新增模块信息页面
			function savemodule(){
				window.parent.savemodule('${moduleAll.id}');
			}
			//打开修改模块信息页面
			function updatemodule(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					
					window.parent.updatemodule(chkValue);
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
				
			}
			
			//删除模块信息
			function deletemodule(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm"/>？')){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						
						var action = '<%=path%>/module/delete.do';
						window.parent.deletemodule(action,chkValue.join(","));
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
					return ;
				}
			}
			
			$(document).ready(function(){
				
				//加载toolbar（修改模块信息）
				var toolModule = $('#toolModule').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_sector_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateModuleOnPage();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
				
				//加载toolbar（子模块操作）
				var tool = $('#tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								savemodule();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updatemodule();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="remove_the_module_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deletemodule();
							}
						}
					]
				});

				//grid隔行变色
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.moduleInfo'],$(document.body),80);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				$('#parentModuleName').bind('focus.select',function(){
					if(!!!top.selectWindow){
						top.selectWin({
							src: '<%=path%>/module/select.do?code='+$("#code").val(),
							title: '<fmt:message key="select_the_module"/>',
							offset: getOffset('parentModuleName'),
							items:[{
								input: $(this),
								selectInput: '#moduleName'
							},{
								input: $('#parentModuleId'),
								selectInput: '#moduleId'
							}]
						});
					}
				});
				
			});
		</script>
	</body>
</html>