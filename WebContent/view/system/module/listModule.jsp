<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>	
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div class="leftFrame">
      <div id="toolbar"></div>
	    <div class="treePanel">
	        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="module_management"/>; method:changeUrl("00000000000000000000000000000000","","模块","")';
	          <c:forEach var="module" items="${treeList}" varStatus="status">
		          		tree.nodes['${module.parentModule.id}_${module.id}'] = 'text:${lo:show(module.name)}; method:changeUrl(\'${module.id}\',\'${module.code}\',\'${lo:show(module.name)}\',\'${module.parentModule.id}\')';
	          </c:forEach>
	          tree.setIconPath("<%=path%>/js/tree/");
	          document.write(tree.toString());
	          tree.expandAll();
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
		<form action="<%=path%>/module/list.do" id="treeform">
			<input type="hidden" id="id" name="id" value=""/>
			<input type="hidden" id="code" name="code" value=""/>
			<input type="hidden" id="name" name="name" value=""/>
			<input type="hidden" id="parentid" name="parentModule.id" value=""/>
			<input type="hidden" id="moduleId" name="moduleId" value=""/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		//改变mainFrame的内容
		function changeUrl(moduleId,moduleCode,moduleName,parentid)
	    {
			$('#treeform > #id').val(moduleId);
			$('#treeform > #code').val(moduleCode);
			$('#treeform > #name').val(moduleName);
			$('#treeform > #parentid').val(parentid);
		 window.mainFrame.location = "<%=path%>/module/table.do?id="+moduleId+"&code="+moduleCode+"&parentModule.id="+parentid;
	    }
	    //弹出页面的回调函数
		function pageReload(){
			$('#treeform').submit();
		}
        //刷新整个页面
	    function refreshTree()
	    {
	      window.location.reload();
	    }
	    //新增模块弹出的窗口
	    function savemodule(pid){
			$('body').window({
				id: 'window_savemodule',
				title: '<fmt:message key="new_module_information"/>',
				content: '<iframe id="savemoduleFrame" frameborder="0" src="<%=path%>/module/add.do?parentModule.id='+pid+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_the_module_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('savemoduleFrame')&&window.document.getElementById("savemoduleFrame").contentWindow.validate._submitValidate()){
									submitFrameForm('savemoduleFrame','moduleForm');
								}
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
	    //修改页面弹出的窗口
	    function updatemodule(chkValue){
	    	
			$('body').window({
				title: '<fmt:message key="modify_the_module_information"/>',
				content: '<iframe id="updatemoduleFrame" frameborder="0" src="<%=path%>/module/update.do?id='+chkValue+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_the_module_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('updatemoduleFrame')&&window.document.getElementById("updatemoduleFrame").contentWindow.validate._submitValidate()){
									submitFrameForm('updatemoduleFrame','moduleForm');
								}
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
	    }
	    
	    //页面上删除子模块的操作
		function deletemodule(action,moduleId){
			$('body').window({
				title: '<fmt:message key="tips"/>',
				content: '<iframe id="updatemoduleFrame" frameborder="0" src="'+action+'?moduleId='+moduleId+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true
			});
		}
	    
	    //页面上修改模块信息的操作（非弹出）
		function updateModuleFromPage(param){
	    var src = '<%=path %>/module/saveByUpdate.do?'+param;
			$('body').window({
				title: '<fmt:message key="tips"/>',
				content: '<iframe id="updatemoduleFrame" frameborder="0" src="'+src+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true
			});
		}
		
	    //页面初始化方法
			$(document).ready(function(){
				//加载toolbar
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						}
					]
				});//end toolbar
				
				setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
				
				changeUrl('${tableState.id}','${tableState.code}');
				
			});// end $(document).ready();
		</script>

	</body>
</html>
