<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0,user-scalable=no" />
  <link rel="stylesheet" href="<%=path %>/css/scm/sysParam/setting.css">
  <style media="screen"></style>
  <style type="text/css">
  	.store::before {
	  content: '';
	  display: inline-block;
	  background: url("<%=path%>/image/scm/sysParam/store.png") center center no-repeat;
	  background-size: contain;
	  width: 14px;
	  height: 18px;
	  margin: 0 5px -4px 0;
	}
	.headquarters::before {
	  content: '';
	  display: inline-block;
	  background: url("<%=path%>/image/scm/sysParam/headquarters.png") center center no-repeat;
	  background-size: contain;
	  width: 14px;
	  height: 18px;
	  margin: 0 5px -4px 0;
	}
  </style>
  <title>系统参数</title>
</head>
<body>
	<div id="main">
	    <div class="top">
	      <a href="#">  <span class="headquarters">总部设置</span></a>
	      <a href="#">  <span class="store">门店设置</span></a>
	    </div>
	    <div class="content">
	      <div class="left">
	        <div id="menu1" class="menu">
	          <a href="#demo1">基本设置</a>
	          <a href="#demo2">权限设置</a>
	          <a href="#demo3">仓库设置</a>
	        </div>
	      </div>
	      <div class="right">
	        <div class="test" id="test1">
	          <section id="demo1" class="section1">
	            <span class="title">基本设置：</span>
	            <div class="title_input">
	              <div class="setting_update_all">
	                <span class="input_text">页面显示条数:</span>
	                <input type="text" name="PAGESIZE" class="ant-input" value="" id="PAGESIZE" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
	              </div>
	              
	              <div class="setting_update_all">
	                <span class="input_text">页面显示最大条数:</span>
	                <input type="text" name="MAXSIZE" class="ant-input" value="" id="MAXSIZE" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">	
	              </div>
	              
	              <div class="setting_update_all">
	                <span class="input_text">日志保留天数:</span>
	                <input type="text" name="saveDays" class="ant-input" value="" id="saveDays" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">	
	              </div>
	              
	              <div class="setting_update_all">
	                <span class="input_text">报货附加项长度:</span>
	                <input type="text" name="BH_FJX_LEN" class="ant-input" value="" id="BH_FJX_LEN" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">	
	              </div>
	              
	               <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
		              <span id="BH_DATE"  class="ant-checkbox">
			              <input type="checkbox" name="BH_DATE" class="ant-checkbox-input" value="N">
			              <span class="ant-checkbox-inner"></span>
		              </span>
	              	  <span>报货日期是否可选</span>
	            	</label>
	              </div>
	            </div>
	          </section>
	          
	          <section id="demo2" class="section1 demo2">	          
	            <span class="title">权限设置：</span>
	            <div>
	              <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
		              <span id="ZHGLTSQX"  class="ant-checkbox">
			              <input type="checkbox" name="ZHGLTSQX" class="ant-checkbox-input" value="0">
			              <span class="ant-checkbox-inner"></span>
		              </span>
	              	  <span>启用账号管理特殊权限</span>
	            	</label>
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>物资权限方式:</span>
	                <label name="WZQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			            <span class="ant-radio ant-radio-checked">
			                <input type="radio" name="WZQX" class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>无筛选</span>
	           		 </label>
	                <label name="WZQX" class="ant-radio-wrapper">
			            <span  class="ant-radio">
			                <input type="radio" name="WZQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			            </span>
	              		<span>按分店物资属性</span>
	            	</label>
	                <label name="WZQX" class="ant-radio-wrapper">
			            <span  class="ant-radio">
			                <input type="radio" name="WZQX"  class="ant-radio-input" value="2">
			                <span class="ant-radio-inner"></span>
			            </span>
	              		<span>按照账号物资权限</span>
	           		 </label>	
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>物资账号权限:</span>
	                <label name="WZZHQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			            <span  class="ant-radio ant-radio-checked">
			                <input type="radio" name="WZZHQX"  class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>具体到物资</span>
	           		 </label>
	                <label name="WZZHQX" class="ant-radio-wrapper">
			            <span  class="ant-radio">
			                <input type="radio" name="WZZHQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>具体到小类</span>
	          		  </label>
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>供应商权限方式:</span>
	                <label name="GYSQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			            <span  class="ant-radio ant-radio-checked">
			                <input type="radio" name="GYSQX"  class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>无筛选</span>
	           		 </label>
	                <label name="GYSQX" class="ant-radio-wrapper" >
			            <span  class="ant-radio">
			                <input type="radio" name="GYSQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>按账号供应商权限</span>
	            	</label>	
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>分店仓位权限方式:</span>
		            <label name="CWQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			            <span  class="ant-radio ant-radio-checked">
			                <input type="radio" name="CWQX"  class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>无筛选</span>
	           		 </label>
	                <label name="CWQX" class="ant-radio-wrapper">
			            <span  class="ant-radio">
			                <input type="radio" name="CWQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span> 按账号仓位权限</span>
	           		 </label>	
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>报货分拨方向权限方式:</span>
		            <label name="FXQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			            <span  class="ant-radio ant-radio-checked">
			                <input type="radio" name="FXQX" class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			            </span>
			            <span>无筛选</span>
	            	</label>
	                <label name="FXQX" class="ant-radio-wrapper">
			              <span  class="ant-radio">
			                <input type="radio" name="FXQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			              </span>
			              <span>按账号申购方向权限</span>
	           		 </label>
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>报货分拨查询筛:</span>
	                <label name="FBCXQX" class="ant-radio-wrapper ant-radio-wrapper-checked">
			              <span  class="ant-radio ant-radio-checked">
			                <input type="radio" name="FBCXQX"  class="ant-radio-input" value="0">
			                <span class="ant-radio-inner"></span>
			              </span>
			              <span>无筛选</span>
	           		 </label>
	                <label name="FBCXQX" class="ant-radio-wrapper">
			              <span  class="ant-radio">
			                <input type="radio" name="FBCXQX"  class="ant-radio-input" value="1">
			                <span class="ant-radio-inner"></span>
			              </span>
			              <span>按物资筛选</span>
	          		  </label>
	              </div>	
	            </div>	            
	          </section>
	          
	          <section id="demo3" class="section1 index_demo3">
	            <span class="title">仓库设置：</span>
	            <div class="title_input">
	              <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
			            <span id="chkout_can_updatesppricesale"  class="ant-checkbox">
				              <input type="checkbox" name="chkout_can_updatesppricesale" class="ant-checkbox-input" value="N">
				              <span class="ant-checkbox-inner"></span>
			           	</span>
	              		<span>出库单是否可修改售价</span>
	           		 </label>
	              </div>
	              <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
			            <span id="INOUT_IS_NOT"  class="ant-checkbox">
			              <input type="checkbox" name="INOUT_IS_NOT" class="ant-checkbox-input" value="0">
			              <span class="ant-checkbox-inner"></span>
			            </span>
			            <span>入库存放至默认仓位</span>
	          	  </label>
	              </div>
	              <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
			            <span id="CHKINM_SPPRICE_IS_YN"  class="ant-checkbox">
			              <input type="checkbox" name="CHKINM_SPPRICE_IS_YN" class="ant-checkbox-input" value="N">
			              <span class="ant-checkbox-inner"></span>
			            </span>
			            <span>入库价格是否允许修改</span>
	           		 </label>
	              </div>
	            </div>
	          </section>
	        </div>
	      </div>
	    </div>
	    
	    <div class="content1">
	      <div class="left">	
	        <div id="menu" class="menu">
	          <a href="#demo4">基本设置</a>
	          <a href="#demo5">月结盘点</a>
	          <a href="#demo6">仓库设置</a>
	        </div>
	      </div>
	      <div class="right">
	        <div class="test" id="test">
	          <section id="demo4" class="section2">
	            <span class="title">基本设置：</span>
	            <div class="title_input">
	              <div class="setting_update_all">
	                <span class="input_text">菜品点击率小数位数:</span>
	                <input type="text" name="BusClicksRatioBit" class="ant-input" value="4" id="BusClicksRatioBit" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">	
	              </div>
	              
	              <div class="setting_update_all">
	                <span class="input_text">向导生成单据次数:</span>
	                <input type="text" name="DailyGoodsCnt" class="ant-input" value="0" id="DailyGoodsCnt" maxlength="3" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">	
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>报货向导订货日期限制:</span>
	                <label name="OrderDayControl" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="OrderDayControl" class="ant-radio-input" value="0">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 不限制  </span>
	             	 </label>
	                <label name="OrderDayControl" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="OrderDayControl" class="ant-radio-input" value="1">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>限制不能选择日期</span>
	             	 </label>
	              </div>	
	            </div>	
	          </section>
	          
	          <section id="demo5" class="section2 demo2">
	            <span class="title">月结盘点：</span>
	            <div class="title_input">	
	              <div class="setting_update_all ant-radio-group">
	                <span>月结限制:</span>
	                <label name="isControPdYj" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="isControPdYj" class="ant-radio-input" value="N">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 无限制 </span>
	            	</label>
	                <label name="isControPdYj" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="isControPdYj" class="ant-radio-input" value="Y">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>验货盘点后月结</span>
	              	</label>
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>月结方式 :</span>
	                <label name="ynCenterMonthEnd" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="ynCenterMonthEnd" class="ant-radio-input" value="N">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>分店月结(默认)</span>
	              	</label>
	                <label name="ynCenterMonthEnd" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="ynCenterMonthEnd" class="ant-radio-input" value="Y">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>总部月结</span>
	              	</label>
	              </div>
	              
	              <div class="setting_update_all ant-radio-group">
	                <span>盘点日期:</span>
	                <label name="ynInventoryNowDate" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="ynInventoryNowDate" class="ant-radio-input" value="N">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 前一天(默认)</span>
	             	 </label>
	                <label name="ynInventoryNowDate" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="ynInventoryNowDate" class="ant-radio-input" value="Y">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>当天</span>
	             	 </label>
	              </div>
	            </div>
	          </section>
	          
	          <section id="demo6" class="section2 demo3">
	            <span class="title">仓库设置：</span>
	            <div class="title_input">
	              <div class="setting_update_all">
	                <label class="ant-checkbox-wrapper">
		              <span id="isControAmount"  class="ant-checkbox">
		                <input type="checkbox" name="isControAmount" class="ant-checkbox-input" value="N">
		                <span class="ant-checkbox-inner"></span>
		              </span>
		              <span>员工餐|报损库数量控制</span>
	              </label>
	              </div>
	              <div class="setting_update_all ant-radio-group">
	                <span>出入库限制:</span>
	                <label name="ynInout" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="ynInout"  class="ant-radio-input" value="0">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 已盘点 给出提示(默认)  </span>
	              	</label>
	                <label name="ynInout" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="ynInout" class="ant-radio-input" value="1">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 不限制 </span>
	              	</label>
	                <label name="ynInout" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="ynInout" class="ant-radio-input" value="2">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 已盘点 不能出入库 </span>
	              	</label>
	              </div>
	              <div class="setting_update_all ant-radio-group">
	                <span>出库方式 :</span>
	                <label name="firmOutWay" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="firmOutWay" class="ant-radio-input" value="0">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 移动平均(默认)  </span>
	              	</label>
	                <label name="firmOutWay" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="firmOutWay" class="ant-radio-input" value="1">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 先进先出</span>
	              	</label>
	                <label name="firmOutWay" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="firmOutWay" class="ant-radio-input" value="1">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 档口店先进先出</span>
	              	</label>
	              </div>
	
	              <div class="setting_update_all ant-radio-group">
	                <span>入库单价格 :</span>
	                <label name="ynChkinByLastPrice" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="ynChkinByLastPrice" class="ant-radio-input" value="N">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>  取报价(平常用法)</span>
	              	</label>
	                <label name="ynChkinByLastPrice" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="ynChkinByLastPrice" class="ant-radio-input" value="Y">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span>  报价没有取最后进价</span>
	              	</label>	
	              </div>
	
	              <div class="setting_update_all ant-radio-group">
	                <span>配送验货设置  :</span>
	                <label name="isInsByBill" class="ant-radio-wrapper ant-radio-wrapper-checked">
		                <span  class="ant-radio ant-radio-checked">
		                  <input type="radio" name="isInsByBill" class="ant-radio-input" value="N">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 无限制  </span>
	              	</label>
	                <label name="isInsByBill" class="ant-radio-wrapper">
		                <span  class="ant-radio">
		                  <input type="radio" name="isInsByBill" class="ant-radio-input" value="Y">
		                  <span class="ant-radio-inner"></span>
		                </span>
		                <span> 根据单据</span>
	                </label>	
	              </div>

	               <div class="setting_update_all">
	                  <span>门店耗用计算类型 :</span>
		                <label class="ant-checkbox-wrapper checkboxMain">
				              <span name="isChkouttyp"  class="ant-checkbox checkboxArry"  value="9910">
					              <input type="checkbox" class="ant-checkbox-input" name="isChkouttyp" value="9910">
					              <span class="ant-checkbox-inner"></span>
				              </span>
				              <span>盘亏出库</span>
		              	</label>
	                	<label class="ant-checkbox-wrapper checkboxMain">
				              <span name="isChkouttyp"  class="ant-checkbox checkboxArry"  value="9915">
					              <input type="checkbox" class="ant-checkbox-input " name="isChkouttyp" value="9915">
					              <span class="ant-checkbox-inner"></span>
				              </span>
				              <span>出库报废</span>
	              		</label>
	                	<label  class="ant-checkbox-wrapper checkboxMain">
				              <span name="isChkouttyp"  class="ant-checkbox checkboxArry"  value="9951">
					              <input type="checkbox" class="ant-checkbox-input" name="isChkouttyp" value="9951">
					              <span class="ant-checkbox-inner"></span>
				              </span>
				              <span>员工餐出库</span>
			            </label>
              		</div>
	            </div>
	          </section>
	        </div>
	      </div>
	    </div>
  </div>
  <script type="text/javascript" src="<%=path %>/js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
  <script type="text/javascript" src="<%=path %>/js/scm/sysParam/setting.js"></script>
  <script type="text/javascript">
    $(function(){ 	    	
    	//初始加载所有参数
        listParamAll();  	
        //修改参数值
        updateParams();
    })
    
    //修改多选框属性值
    function updateParams(){
    	//初始定义的值
    	var timerid;
		var section_deom=1;
		//点击事件
        $('.headquarters').on('click',function(event){
            event.preventDefault();
              $('.content1').hide();
              $('.content').show();
              section_deom=1;
              $("body").scrollTop(0);
              $(".menu a").removeClass('active');
              $(".menu a[href=#demo1]").addClass('active');
        });
        $('.store').on('click',function(event){
            event.preventDefault();
             $('.content').hide();
             $('.content1').show();
             section_deom=2;
             $("body").scrollTop(0);
             $(".menu a").removeClass('active');
             $(".menu a[href=#demo4]").addClass('active');
        });        
        
        //多选点击事件多选项
        $(".checkboxArry").on('click', function(event) {
          	event.preventDefault();

          	var get_name = $(this).attr('name');
          	var get_desc = $(this).next("span").text();
          	var isSelect = true;
          	if ($(this).hasClass('ant-checkbox-checked')) {
            	 $(this).removeClass('ant-checkbox-checked');
             	isSelect = false;
          	} else {
           	 	$(this).addClass('ant-checkbox-checked');
            	isSelect = true;
          	}
          var checkboxArryAll = [];
          $("span[name='" + get_name + "']").each(function() {
             if ($(this).hasClass('ant-checkbox-checked')) {
              	checkboxArryAll.push($(this).attr('value'));
              };
          });
          	var checkValue = checkboxArryAll.join();
          	console.log("提交id", get_name, "是否选中", checkValue, "代表总部还是门店", section_deom);
          	$.ajax({
	      		url: "<%=path %>/param/saveOne.do",
	      		data: {"paramCode": get_name, "paramValue": checkValue, "type": section_deom},
	      		type: "post",
	      		dataType: "JSON",
	      		success: function(res){
	      			var result = $.parseJSON(res);
	      			if('' == result || null == result || result.result==false){      
		        		alert("属性："+get_desc +" 更新失败！");
	      			}	      				
	      		}
      		});  
        });
        
        //单选点击事件
		 $(".ant-radio-wrapper").on('click',function(event){
		      event.preventDefault();
		    var setname=  $(this).attr('name');
		    if($(this).hasClass('ant-radio-wrapper-checked')){
		    }else {
		        $("label[name='"+setname+"']").removeClass('ant-radio-wrapper-checked');
		    	$("label[name='"+setname+"']").children("span.ant-radio").removeClass('ant-radio-checked');		
		    	$(this).addClass('ant-radio-wrapper-checked');
		    	$(this).children("span.ant-radio").addClass('ant-radio-checked');
		    	var change_value=$(this).find('input').attr('value');
		    	var change_name=$(this).find('input').attr('name');
		    	var get_desc = $(this).children("span:eq(1)").text();
		    	console.log('我是value',change_value,"我是name",change_name,"代表总部还是门店",section_deom);
		    	$.ajax({
	        		url: "<%=path %>/param/saveOne.do",
	        		data: {"paramCode": change_name, "paramValue": change_value, "type": section_deom},
	        		type: "post",
	        		dataType: "JSON",
	        		success: function(res){
	        			var result = $.parseJSON(res);
	        			if('' == result || null == result || result.result == false){        				
	        				alert("属性："+get_desc +" 更新失败！");
	        			}
	        		}
	        	});   
		   	}
		});
        
      	//多选点击事件单选项
        $(".ant-checkbox:not(.checkboxArry)").on('click',function(event){
            event.preventDefault(); //取消事件的默认动作            
          	var get_id=$(this).attr('id');
            var get_val=$(this).find("input").attr('value');
            var get_desc = $(this).next("span").text();
          	var isSelect ='';
          	if($(this).hasClass('ant-checkbox-checked')){
            	$(this).removeClass('ant-checkbox-checked');
            	if (get_val==='N'||get_val==='Y'){
            		isSelect='N';
                }else{
                	isSelect=0;
                }            	
          	}else {
            	$(this).addClass('ant-checkbox-checked');              	
              	if (get_val==='N'||get_val==='Y'){
            		isSelect='Y';
                }else{
                	isSelect=1;
                }
          	}
        	//console.log("提交id",get_id,"是否选中",isSelect,"代表总部还是门店",section_deom);
        	//console.log("ID:", get_id, "----val:",isSelect);
        	$.ajax({
        		url: "<%=path %>/param/saveOne.do",
        		data: {"paramCode": get_id, "paramValue": isSelect, "type": section_deom},
        		type: "post",
        		dataType: "JSON",
        		success: function(res){
        			var result = $.parseJSON(res);
        			if('' == result || null == result || result.result == false){        				
        				alert("属性："+get_desc +" 更新失败！");
        			}
        		}
        	});       		
        });  
      	
        
        $(".menu a[href=#demo1]").addClass('active');
        $(".menu a").click(function(e) {
          e.preventDefault();
          $(".menu a").removeClass('active');
          var id = $(this).attr("href").substring(1);
          // console.log($(this),id);
          // $(".menu a[href=#"+id+"]").addClass('active');
          $("body").animate({
            'scrollTop': ($("section#" + id).offset().top - 70)
          });
        });
        $("body").scrollTop(0); //forcing window scroll to execute on page load
        $(window).scroll(function() {
          clearTimeout(timerid);
          timerid = setTimeout(checkactivelink, 20);
        });
        function checkactivelink() {
            var id = "";
            if(!section_deom){
                $(".section1").each(function() {
        	    if (($("body").scrollTop() + 70) >= ($(this).offset().top)) {
        	        id = $(this).attr("id");
        	      }
                });
            }else {
            	$(".section2").each(function() {
        	    if (($("body").scrollTop() + 70) >= ($(this).offset().top)) {
        	        id = $(this).attr("id");
        	      }
            	});
            }
            var hasActive = $(".menu a[href=#" + id + "]").hasClass('active');
            if (!hasActive) {
              $(".menu a").removeClass('active');
              $(".menu a[href=#" + id + "]").addClass('active');
            }
          }  
    }
    //修改文本值
    function change_input_text(id, name) {
    	
   	  	var oldmessage = "";
   	  	$(id).bind("focus", function(event) {
	  	    event.preventDefault();
	  	    oldmessage = $(this).val();
	  	 });
    	$(id).bind("blur", function(event) {
    	    event.preventDefault();
    	    var message = $(this).val();
    	    if (oldmessage == message) {
    	      //	console.log("我是没有变化")
    	    } else {
    	    	var input_type=1;
    	    	//console.log("display",$(".content1").css("display")=='none');
    	    	if($(".content1").css("display")=='none'){
    	    		input_type=1;
    	    	}else{
    	    		input_type=2;
    	    	}
    	        var inputVal= $(this).val();
    	        var inputName=$(this).attr('name');
    	        var get_desc = $(this).prev().text();
    	       // console.log("我是input写ajax地方",inputVal,inputName,input_type);
    	       $.ajax({
	          		url: "<%=path %>/param/saveOne.do",
	          		data: {"paramCode": inputName, "paramValue": inputVal, "type": input_type},
	          		type: "post",
	          		dataType: "JSON",
	          		success: function(res){
	          			var result = $.parseJSON(res);
	          			if('' == result || null == result || result.result == false){        				
	        				alert("属性："+get_desc +" 更新失败！");
	        			}
	          		}
          		});   
    	   	}
    	});
    }
    
    //初始化设置value
    function setValue(id,value){
      $("#"+id).val(value);
      change_input_text('#'+id,id);
    }
    
    //获取所有参数
    function listParamAll(){
    	$.ajax({
    		url: "<%=path %>/param/getAll.do",
    		type: "post",
    		dataType: "JSON",
    		success: function(resultMap){
    			if(null == resultMap || '' == resultMap){    				
	    			console.log("没有获取到参数！");    				
    			}else{
    				var paramList = $.parseJSON(resultMap);   // 将 json 字符串转为json 对象
    				var dataList = paramList.dataList;
    				for(var i=0; i<dataList.length; i++){
    					var inputType;
    					var inputName;    					
    					$("#main").find("input").each(function(){
    			    		inputType = $(this).attr("type");
    			    		inputName = $(this).attr("name");
    			    		// 为所有文本框 数字类型 的属性赋值
    			    		if(inputType == "text" && inputName == dataList[i].paramCode){  
        						setValue(dataList[i].paramCode, dataList[i].paramValue);
        					} 
    			    		// 为所有类型为 单选按钮 的属性赋值
        					if(inputType == "radio" && inputName == dataList[i].paramCode){
        						radioInitial(dataList[i].paramCode, dataList[i].paramValue);
        					}
    			    		// 为所有类型为 多选按钮 的属性赋值(单个)
        					if(inputType == "checkbox" && inputName == dataList[i].paramCode && inputName !='isChkouttyp'){
        						checkboxInitial(dataList[i].paramCode, dataList[i].paramValue);
        					}
    			    		// 为所有类型为 多选按钮 的属性赋值(多个)
        					if(inputType == "checkbox" && inputName == dataList[i].paramCode && inputName =='isChkouttyp'){
        						checkboxInitialAll(dataList[i].paramValue,"isChkouttyp");
        					}
    			    	});
    					
    				}
    				
    			}
    		}
    	});
    }
    
  </script>
</body>
</html>
















