<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>role Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		
		</div>
		<div class="condition">
			<form action="<%=path%>/role/list.do" id="queryForm" name="queryForm" method="post">
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input"><input type="text" id="name" name="name" class="text" value="<c:out value="${queryRole.name}" />"/></div>
					<div class="form-label"><fmt:message key="whether_in_use"/></div>
					<div class="form-input">
					<select id="deleteFlag" name="deleteFlag" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
						<option value=""><fmt:message key="all"/></option>
						<option value="T"
						<c:if test="${queryRole.deleteFlag=='T'}">
						selected="selected"
						</c:if>
						><fmt:message key="enable"/></option>
						<option value="F" 
						<c:if test="${queryRole.deleteFlag=='F'}">
						selected="selected"
						</c:if>
						><fmt:message key="disable1"/></option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<form id="listForm" action="" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:180px;"><fmt:message key="role_name"/></td>
								<td style="width:180px;"><fmt:message key="whether_to_use"/></td>
								<td style="width:150px;">可查询时间范围(多少天前)</td>
								<td style="width:150px;">可查询时间范围(多少天后)</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="role" items="${roleList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${role.id}' />" value="<c:out value='${role.id}' />"/>
									</td>
									<td style="width:180px;"><c:out value="${role.name}" />&nbsp;</td>
									<td style="width:180px;">
									<c:if test="${role.deleteFlag=='T'}"><fmt:message key="enable"/></c:if>
									<c:if test="${role.deleteFlag=='F'}"><fmt:message key="disable1"/></c:if>
									&nbsp;</td>
									<td style="width:150px; text-align: center;"><c:out value="${role.roleTime.beforeDay}" />&nbsp;</td>
									<td style="width:150px; text-align: center;"><c:out value="${role.roleTime.afterDay}" />&nbsp;</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<form id="roleOperateForm" action="<%=path%>/roleOperate/list.do">
		<input type="hidden" name="id"/>
    	</form>
		<form id="roleMainTableForm" action="<%=path%>/roleOperate/listMainTable.do">
		<input type="hidden" name="id"/>
    	</form>
    	<form id="roleOperateRangeForm" action="<%=path%>/roleOperateRange/list.do">
		<input type="hidden" name="id"/>
    	</form>
    	<form id="rolePositnForm" action="<%=path%>/positnRole/list.do">
		<input type="hidden" name="id" value=""/>
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>

		<script type="text/javascript">
		function clearQueryForm(){
			$('#queryForm input[type=text]').val('');
			$('#queryForm option:nth-child(1)').attr('selected','selected');
		}
			function pageReload(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
				
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saverole();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updaterole();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete_role_of_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleterole();
							}
						},"-",{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="query_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								viewrole();
							}
						}
<%-- 					,{
 							text: '<fmt:message key="empty_query" />', 
							title: '<fmt:message key="empty_query"/>',
							icon: {
 								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								clearQueryForm();
							}
 						}--%>
						,{
							text: '<fmt:message key="assign_permissions" />',
							title: '<fmt:message key="assign_permissions"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								toRoleOperate();
							}
						},{
							text: '<fmt:message key="allocate_my_desktop"/>',
							title: '<fmt:message key="allocate_my_desktop"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								toRoleMainTable();
							}
						}
//						,{
// 							text: '<fmt:message key="assigned_positions_range"/>',
// 							title: '<fmt:message key="assigned_positions_range"/>',
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['-100px','-40px']
// 							},
// 							handler: function(){
// 								toRolePositn();
// 							}
// 						},{
// 							text: '<fmt:message key="assign_permissions_range" />',
// 							title: '<fmt:message key="assign_permissions_range"/>',
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['-100px','-40px']
// 							},
// 							handler: function(){
// 								toRoleOperateRange();
// 							}
//						}
						,{
							text: '赋权时间',
							title: '赋权时间',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								toRoleTime();
							}
						}
						,{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});

				//$('.grid').height($(document.body).height() - $('.tool').outerHeight() - $('.condition').outerHeight());
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				function toRoleOperate(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('#roleOperateForm input').val(chkValue);//有问题，一个页面不要用重名的id，id都是唯一的，就一个input 已修改  wjf
					$('#roleOperateForm').submit();
					}else {
						alert('<fmt:message key="please_select_a_single_role"/>！');
					}
				}
				
				function toRoleMainTable(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('#roleMainTableForm input').val(chkValue);//有问题，一个页面不要用重名的id，id都是唯一的，就一个input 已修改  wjf
					$('#roleMainTableForm').submit();
					}else {
						alert('<fmt:message key="please_select_a_single_role"/>！');
					}
				}
				
				function toRolePositn(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('#rolePositnForm').find('input').val(chkValue);
					$('#rolePositnForm').submit();
					}else {
						alert('<fmt:message key="please_select_a_single_role"/>！');
					}
				}
				
				function toRoleOperateRange(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('#roleOperateRangeForm input').val(chkValue);
					$('#roleOperateRangeForm').submit();
					}
				}
				
				function saverole(){
					$('body').window({
						id: 'window_saverole',
						title: '<fmt:message key="new_role_information"/>',
						content: '<iframe id="saveroleFrame" frameborder="0" src="<%=path%>/role/add.do"></iframe>',
						width: '650px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save_role_of_information"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveroleFrame')&&window.document.getElementById("saveroleFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('saveroleFrame','roleForm');
										}
									}
								},{
									text: '<fmt:message key ="cancel" />',
									title: '<fmt:message key ="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				//赋权时间权限
				function toRoleTime(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						$('body').window({
							id: 'window_saveRoleTime',
							title: '赋权时间',
							content: '<iframe id="saveRoleTimeFrame" frameborder="0" src="<%=path%>/roleTime/savePage.do?roleId='+chkValue+'"></iframe>',
							width: '650px',
							height: '500px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save_role_of_information"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('saveRoleTimeFrame')&&window.document.getElementById("saveRoleTimeFrame").contentWindow.validate._submitValidate()){
												submitFrameForm('saveRoleTimeFrame','roleTimeForm');
											}
										}
									},{
										text: '<fmt:message key ="cancel" />',
										title: '<fmt:message key ="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
				}
				function updaterole(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						
						$('body').window({
							title: '<fmt:message key="modify_the_role_information"/>',
							content: '<iframe id="updateroleFrame" frameborder="0" src="<%=path%>/role/update.do?id='+chkValue+'"></iframe>',
							width: '650px',
							height: '500px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save_role_of_information"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updateroleFrame')){
												submitFrameForm('updateroleFrame','roleForm');
											}
										}
									},{
										text: '<fmt:message key ="cancel" />',
										title: '<fmt:message key ="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
					
				}
				
				function viewrole(){
					 pageReload();
				}
				
				function deleterole(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm_role"/>？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/role/delete.do?roleId='+chkValue.join(",");
							
							$('#listForm').attr('action',action);
							$('#listForm').submit();
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
		</script>
	</body>
</html>