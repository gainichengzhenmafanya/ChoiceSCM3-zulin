<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="sector_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>	
			
			<style type="text/css">
				#toolDepartment,#toolChild {
					position: relative;
					height: 27px;
				}
				
				.departmentInfo {
					position: relative;
					top: 1px;
					height: 61px;
					line-height: 61px;
					background-color: #E1E1E1;
				}
				
				.form-line .form-label{
					width: 90px;
				}
				
				.form-line .form-input{
					width: 160px;
				}
				
			</style>
		</head>
	<body>
		
		<div id="toolDepartment"></div>
		<div class="departmentInfo">
			<form id="departmentForm" name="departmentForm" method="post" action="">
				<input type="hidden" id="id" name="id" value="<c:out value='${department.id}' />" />
				<input type="hidden" id="code" name="code" value="<c:out value='${department.code}' />" />
				<input type="hidden" id="deleteFlag" name="deleteFlag" value="<c:out value='${department.deleteFlag}' />" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="viewCode" name="viewCode" class="text" 
							value="<c:out value='${department.viewCode}' />" />
					</div>
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="name" name="name" class="text" 
							value="<c:out value='${department.name}' />" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="higher_authorities"/></div>
					<div class="form-input">
						<input type="text" id="parentDepartmentName" name="parentDepartment.name" class="text" 
							value="<c:out value='${department.parentDepartment.name}' default='部门' />" />
						<input type="hidden" id="parentDepartmenId" name="parentDepartment.id" class="text" 
							value="<c:out value='${department.parentDepartment.id}' default='00000000000000000000000000000000'/>" />
					</div>
					<div class="form-label"><fmt:message key="no"/></div>
					<div class="form-input">
						<input type="text" id="sequence" name="sequence" class="text" 
							value="<c:out value='${department.sequence}' />" />
					</div>
				</div>
			</form>
		</div>
		
		<div id="toolChild"></div>
		<div class="grid" >
			<form id="listForm" action="<%=path%>/department/list.do" method="post">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 26px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:150px;"><fmt:message key="department_number"/></span></td>
								<td><span style="width:200px;"><fmt:message key="department_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="departments_serial_number"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" >
						<tbody>
							<c:forEach var="childDepartment" items="${department.childDepartmentList}" varStatus="status">
								<tr>
									<td class="num" style="width: 36px;">${status.index+1}</td>
									<td style="width:40px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_<c:out value='${childDepartment.id}' />" 
											value="<c:out value='${childDepartment.id}' />"/>
									</td>
									<td>
										<span style="width:150px;"><c:out value="${childDepartment.viewCode}" />&nbsp;</span>
									</td>
									<td>
										<span style="width:200px;"><c:out value="${childDepartment.name}" />&nbsp;</span>
									</td>
									<td class="center">
										<span style="width:80px;"><c:out value="${childDepartment.sequence}" />&nbsp;</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">

		$(document).ready(function(){
			
			if(!$('#id').val()){
				$('#departmentForm').find('.form-label').addClass('disable');
				$('#departmentForm').find(':input').attr('disabled','disabled');
				$('#parentDepartmentName').removeClass('selectDepartment');
				
				$('#toolDepartment').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_sector_information"/>',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}]
				});
			}else{
				$('#parentDepartmentName').addClass('selectDepartment');
				$('#toolDepartment').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_sector_information"/>',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							var param = $('#departmentForm').serialize();
							param = decodeURIComponent(param,true);
							param = encodeURI(encodeURI(param));
							parent.saveDepartment('<%=path%>',param);
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}]
				});
			}
			
			var toolChild = $('#toolChild').toolbar({
				items: [{
					text: '<fmt:message key="insert" />',
					title: '<fmt:message key="new_department_information"/>',
		//			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						parent.addDepartment('<%=path%>');
					}
				},{
					text: '<fmt:message key="update" />',
					title: '<fmt:message key="modifying_sector_information"/>',
					useable: false,
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						if(getFirstID()){
							parent.updateDepartment('<%=path%>',getFirstID());
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
							return ;
						}
					}
				},{
					text: '<fmt:message key="delete" />',
					title: '<fmt:message key="delete_department_information"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						if(getAllID()){
							 if(!confirm('<fmt:message key="delete_data_confirm"/>？'))
								 return;
							parent.deleteDepartment('<%=path%>',getAllID());
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
							return ;
						}
					}
				}]
			});	//end $('#toolChild').toolbar()

			//设置表格的总体高度
			setElementHeight('.grid',['.form-group','#toolDepartment','.departmentInfo','.form-group','#toolChild']);
			
			//设置表格数据展示部分的高度
			setElementHeight('.table-body',['.table-head'],'.grid',30);
			loadGrid();//  自动计算滚动条的js方法
			
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			$('#parentDepartmentName').bind('focus.selectDepartment',function(){
				if(!!!top.departmentWindow){
					var offset = getOffset('parentDepartmentName');
					top.selectDepartment('<%=path%>',offset,$(this),$('#parentDepartmenId'),$('#code').val());
				}
				
			});
			
		});	//end $(document).ready();

		</script>
	</body>
</html>