<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>盘点模板主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body>
		<div style="height:30%;">
			<form id="listForm" action="" method="post">
<%-- 				<input type="hidden" id="positn" name="positn.code" value="${inventory.positn.code}"/> --%>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td><span style="width:100px;"><fmt:message key ="scm_year" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="Filling_in_the_date" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="time" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="orders_maker" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="document_number" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="total_amount1" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="positn_state" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="inventoryDemom" items="${listInventoryDemom}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${inventoryDemom.chkstodemono}' />" value="${inventoryDemom.chkstodemono}"/>
										</td>
										<td><span style="width:100px;">${inventoryDemom.yearr}</span></td>
										<td><span style="width:100px;"><fmt:formatDate value="${inventoryDemom.maded}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:100px;">${inventoryDemom.madet}</span></td>
										<td><span style="width:100px;">${inventoryDemom.madeby}</span></td>
										<td><span style="width:100px;">${inventoryDemom.vouno}</span></td>
										<td><span style="width:100px;">${inventoryDemom.totalamt}</span></td>
										<td><span style="width:100px;">${inventoryDemom.status}</span></td>
										<td><span style="width:100px;">${inventoryDemom.memo}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div class="mainFrame" style="height:60%;width:100%">
		    <iframe frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',0,$(document.body),460);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法

			//单击每行选中前面的checkbox
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if($(this).find(':checkbox')[0].checked){
					$(":checkbox").attr("checked", false);
				}else{
					$(":checkbox").attr("checked", false);
					$(this).find(':checkbox').attr("checked", true);
				}
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
				chkstodemono=$(this).find(':checkbox').val();
		        window.mainFrame.location = "<%=path%>/inventoryDemom/listInventoryDemod.do?chkstodemono="+chkstodemono;
			 });
			
			//禁用checkbox本身的事件
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				event.stopPropagation();
				if(this.checked){
					$(this).attr("checked",false);	
				}else{
					$(this).attr("checked",true);
				}
				$(this).closest('tr').click();
			});
		});

		//确认修改
		function enterUpdate() {
			if($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()<1){
				alert('<fmt:message key="Select_a_template" />！');
				return;
			}else{
// 				alert($('#positn').val());
				var chkstodemono = $('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
<%-- 				parent.$('#listForm').attr("action","<%=path%>/inventory/list.do?chkstodemono="+chkstodemono); --%>
				parent.$('#chkstodemono').val(chkstodemono);
<%-- 				parent.$('#listForm').attr("action","<%=path%>/inventory/list.do?chkstodemono="+chkstodemono+"&positn.code="+$('#positn').val()); --%>
				parent.$('#listForm').submit();
			}
		}
		
		function pageReload(){
	    	window.location.href = '<%=path%>/inventory/addInvendemo.do';
	    }
		</script>
	</body>
</html>