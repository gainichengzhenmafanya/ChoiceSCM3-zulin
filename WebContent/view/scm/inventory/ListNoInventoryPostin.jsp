<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>
		</head>
	<body>
		<form action="<%=path%>/positn/list.do" id="queryForm" name="queryForm" method="post">
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label"><fmt:message key="business_group" /></div>
					<div class="form-input">
						<select name="mod2" class="select">
							<option value=""><fmt:message key="please_select" /><fmt:message key="business_group" /></option>
							<c:forEach items="${listMod }" var="curMod">
								<option value="<c:out value="${curMod.des }"/>" 
									<c:if test="${curMod.des == queryPositn.mod2 }">
										selected="selected"
									</c:if>
									><c:out value="${curMod.des }"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="area" /></div>
					<div class="form-input">
						<select name="area" class="select">
							<option value=""><fmt:message key="please_select" /><fmt:message key="area" /></option>
							<c:forEach items="${listArea }" var="curArea">
									<option value="<c:out value="${curArea.des }"/>" 
									<c:if test="${curArea.des == queryPositn.area }">
										selected="selected"
									</c:if>
									><c:out value="${curArea.des }"/></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 26px;">&nbsp;</span></td>
								<td><span style="width:50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${ListPositn}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;"><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:50px;"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span style="width:180px;"><c:out value="${positn.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			function clearQueryForm(){
				$('#queryForm').find(".select").find('option:first').attr('selected','selected');
			}
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			  invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								savePositn();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updatePositn();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deletePositn();
							}
						},"-",{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }
					     else
					     {
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
				function savePositn(){
					$('body').window({
						id: 'window_savePositn',
						title: '<fmt:message key="insert" /><fmt:message key="branches_and_positions_information" />',
						content: '<iframe id="savePositnFrame" frameborder="0" src="<%=path%>/positn/add.do"></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('savePositnFrame') && getFrame('savePositnFrame').validate._submitValidate()){
											submitFrameForm('savePositnFrame','positnForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				
				function updatePositn(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var aim = checkboxList.filter(':checked').eq(0);
						var chkValue = aim.val();
						chkValue += '&code=' + $.trim($(aim).parent().next().text());
						
						$('body').window({
							title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
							content: '<iframe id="updatePositnFrame" frameborder="0" src="<%=path%>/positn/update.do?acct='+chkValue+'"></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updatePositnFrame') && getFrame('updatePositnFrame').validate._submitValidate()){
												submitFrameForm('updatePositnFrame','positnForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else if(checkboxList 
							&& checkboxList.filter(':checked').size()>1){
						alert('<fmt:message key="please_select_data" />!');
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						return ;
					}
					
				}
				
				function viewPositn(){
					 pageReload();
				}
				
				function deletePositn(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var flag = false;
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />?')){
							var acctValue = 1;
							var codeValue=[];
							checkboxList.filter(':checked').each(function(){
								acctValue = $(this).val();
								codeValue.push($.trim($(this).parent().next().text()));
								if($.trim($(this).closest('tr').find('td:eq(7)').text())=='Y'){
									flag = true;
								}
							});
							if(flag){
								alert('<fmt:message key="Can_not_delete_the_use_of_the_position" />！');
								return;
							}
							var action = '<%=path%>/positn/delete.do?acct='+acctValue+'&code='+codeValue.join(",");
							$('body').window({
								title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
			});
		</script>
	</body>
</html>