<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<form id="positnForm" method="post" action="<%=path %>/firmSupply/saveByUpdate.do">
				<input type="hidden" id="positn" name="positn" value="${positnSpcode.positn}" />
				<input type="hidden" id="sp_code" name="sp_code" value="${positnSpcode.sp_code}" />
				<div class="form-line">
					<div class="form-label"><fmt:message key ="average_daily_consumption" />：</div>
					<div class="form-input">
						<input type="text" id="cntuse" name="cntuse" class="text" value="<c:out value="${positnSpcode.cntuse}" default="0"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="procurement_cycle" />：</div>
					<div class="form-input">
						<input type="text" id="datsto" name="datsto" class="text" value="<c:out value="${positnSpcode.datsto}" default="0"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="the_lowest_ratio_of_inspection" />：</div>
					<div class="form-input">
						<input type="text" id="sp_min1" name="sp_min1" class="text" value="<c:out value="${positnSpcode.sp_min1}" default="0"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="the_highest_ratio_of_inspection" />：</div>
					<div class="form-input">
						<input type="text" id="sp_max1" name="sp_max1" class="text" value="<c:out value="${positnSpcode.sp_max1}" default="0"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="whether_to_purchase" />：</div>
					<div class="form-input">
						<select name="ynsto" id="ynsto" class="select" style="width:134px;">
							<option value="Y" <c:if test="${positnSpcode.ynsto=='Y'}"> 
									  	 	selected="selected"
										</c:if>><fmt:message key ="be" /></option>
							<option value="N" <c:if test="${positnSpcode.ynsto=='N'}"> 
									  	 	selected="selected"
										</c:if>><fmt:message key ="no1" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="whether_to_count" />：</div>
					<div class="form-input">
						<select name="ynpd" id="ynpd" class="select" style="width:134px;">
							<option value="Y" <c:if test="${positnSpcode.ynpd=='Y'}"> 
									  	 	selected="selected"
										</c:if>><fmt:message key ="be" /></option>
							<option value="N" <c:if test="${positnSpcode.ynpd=='N'}"> 
									  	 	selected="selected"
										</c:if>><fmt:message key ="no1" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="minimum_amount_of_purchase" />：</div>
					<div class="form-input">
						<input type="text" id="stomin" name="stomin" class="text" value="<c:out value="${positnSpcode.stomin}" default="0"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Purchase_multiple"/>：</div>
					<div class="form-input">
						<input type="text" id="stocnt" name="stocnt" class="text" value="<c:out value="${positnSpcode.stocnt}" default="0"/>"/>
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'datsto',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="procurement_cycle" /><fmt:message key="cannot_be_empty" />！','<fmt:message key ="procurement_cycle" />必须是数字！']
					},{
						type:'text',
						validateObj:'cntuse',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="average_daily_consumption" /><fmt:message key="cannot_be_empty" />！','<fmt:message key ="average_daily_consumption" />必须是数字！']
					},{
						type:'text',
						validateObj:'sp_min1',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="the_lowest_ratio_of_inspection" /><fmt:message key="cannot_be_empty" />！','<fmt:message key ="the_lowest_ratio_of_inspection" />必须是数字！']
					},{
						type:'text',
						validateObj:'sp_max1',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="the_highest_ratio_of_inspection" /><fmt:message key="cannot_be_empty" />！','<fmt:message key ="the_highest_ratio_of_inspection" />必须是数字！']
					},{
						type:'text',
						validateObj:'stomin',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key="cannot_be_empty" />！','必须是数字！']
					},{
						type:'text',
						validateObj:'stocnt',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key="cannot_be_empty" />！','必须是数字！']
					}]
				});
			});
			//最高验货比和最低验货比做控制。最高的大于最低的。
			function competorSp(){
				var sp_min1 = $("#sp_min1").val();//最低验货比
				var sp_max1 = $("#sp_max1").val();//最高验货比
				if(Number(sp_min1)>Number(sp_max1)){
					return false;
				}else{
					return true;
				}
			}
		</script>
	</body>
</html>