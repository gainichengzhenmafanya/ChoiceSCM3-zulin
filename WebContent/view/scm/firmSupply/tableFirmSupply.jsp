<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店物资</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			
			<style type="text/css">
				.line{
					BORDER-LEFT-STYLE: none;
					BORDER-RIGHT-STYLE: none;
					BORDER-TOP-STYLE: none;
					width: 70px;
				}
				.tool {
					position: relative;
					height: 27px;
				}
				.table-head td span{
					white-space: normal;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<form action="<%=path%>/firmSupply/table.do" id="listForm" name="listForm" method="post">
			<input type="hidden" name="positn" id="positnCode" value="${positnCode}"></input>
			
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:80px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:150px;"><fmt:message key="name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="can_purchase"/></span></td>
								<td><span style="width:50px;"><fmt:message key="can_count"/></span></td>
<!-- 								<td><span style="width:50px;">当前售价</span></td> -->
								<td><span style="width:50px;"><fmt:message key="the_lowest_ratio_of_inspection"/></span></td>
								<td><span style="width:50px;"><fmt:message key="the_highest_ratio_of_inspection"/></span></td>
								<td><span style="width:50px;"><fmt:message key="minimum_inventory"/></span></td>
								<td><span style="width:50px;"><fmt:message key="highest_inventory"/></span></td>
								<td><span style="width:50px;"><fmt:message key="average_daily_consumption"/></span></td>
								<td><span style="width:50px;"><fmt:message key="procurement_cycle"/></span></td>
								<td><span style="width:50px;"><fmt:message key="minimum_amount_of_purchase"/></span></td>
								<td><span style="width:50px;"><fmt:message key="Purchase_multiple"/></span></td>
								<td><span style="width:60px;"><fmt:message key="Distribution_Unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody id="sp_data1">
							<c:forEach var="firmSupply" items="${listFirmSupply}" varStatus="status">
								<tr data-positn="${firmSupply.positn }" data-sp_code="${firmSupply.sp_code }">
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${firmSupply.sp_code}' />" value="${firmSupply.sp_code}"/></span>
									</td>
									<td><span style="width:80px;" title="${firmSupply.sp_code}">${firmSupply.sp_code}&nbsp;</span></td>
									<td><span style="width:150px;" title="${firmSupply.sp_name}">${firmSupply.sp_name}&nbsp;</span></td>
									<td><span style="width:70px;" title="${firmSupply.sp_desc}">${firmSupply.sp_desc}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.unit}">${firmSupply.unit}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.ynsto}">${firmSupply.ynsto}&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmSupply.ynpd}">${firmSupply.ynpd}&nbsp;</span></td>
<%-- 									<td><span style="width:50px;" title="${firmSupply.sp_price}">${firmSupply.sp_price}&nbsp;</span></td> --%>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.yhrate1}">${firmSupply.yhrate1}&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.yhrate2}">${firmSupply.yhrate2}&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.sp_min1}">${firmSupply.sp_min1}&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.sp_max1}">${firmSupply.sp_max1}&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="<fmt:formatNumber value="${firmSupply.cntuse}" pattern="##.##"/>">
										<fmt:formatNumber value="${firmSupply.cntuse}" pattern="##.##"/>
										&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.datsto}">${firmSupply.datsto}&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.stomin }"><fmt:formatNumber value="${firmSupply.stomin}" pattern="##.##"/>&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;" title="${firmSupply.stocnt }"><fmt:formatNumber value="${firmSupply.stocnt}" pattern="##.##"/>&nbsp;</span></td>
									<td><span style="width:60px;text-align:center;" title="${firmSupply.disunit}">
										<select class="disunit" style="width:60px;" disabled="disabled">
											<option value="">&nbsp;&nbsp;&nbsp;&nbsp;</option>
											<c:forEach items="${firmSupply.suList }" var="su">
												<option value="${su.unit }" data-disunitper="${su.unitper }" data-dismincnt="${su.dismincnt }" 
													<c:if test="${firmSupply.disunit == su.unit }">selected="selected"</c:if> >${su.unit }</option>
											</c:forEach>
										</select>
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
						<tbody id="sp_data" style="overflow: scroll;">
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			<div class="search-div">
					<div class="search-condition">
						<table class="search-table" cellspacing="0" cellpadding="0">
							<tr>
								<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
								<td><input type="text" id="sp_init" name="sp_init" class="text" style="text-transform:uppercase;" onkeyup="ajaxSearch('sp_init')" value="${positnSpcode.sp_init}" /></td>
<%-- 								<td class="c-left"><fmt:message key="supplies_no" />：</td> --%>
<%-- 								<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${positnSpcode.sp_code}" /></td> --%>
								<td class="c-left"><fmt:message key="supplies_name" />：</td>
								<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${positnSpcode.sp_name}" /></td>
							</tr>
						</table>
					</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							 $('.search-div').slideToggle(100);
							 var t = $('#sp_init').val();
							 $('#sp_init').focus().val(t);
						}
					},"-",{
						text: '<fmt:message key="insert"/>',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							//var trArray = window.parent.$(".grid").find(".table-body").find('tr');
							var chkboxArray = window.parent.$(".treePanel1").find(":checked");
							if(chkboxArray.length>0){
								addSupply();
							}else{
								alert('<fmt:message key="please_select_positions"/>！');
							}
						}
					},{
						text: '<fmt:message key="update"/>',
						title: '<fmt:message key="update"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							var chkboxArray = window.parent.$(".treePanel1").find(":checked");
							if(chkboxArray.length==3){
								if($(".grid").find(".table-body").find('tr').find('input:checked').length==1){
									updateSupply();
								}else{
									alert("<fmt:message key ="please_select_a_data" />！");
								}
							}else{
								alert("<fmt:message key ="Please_select_a_store" />");
							}
						}
					},
					//增加 批量修改 功能 wangjie 2014年12月16日 10:33:12
					{
						text: '<fmt:message key="scm_bulk_update"/>',
						title: '<fmt:message key="scm_bulk_update"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							var chkboxArray = window.parent.$(".treePanel1").find(":checked");
							if(chkboxArray.length>0){
								if($(".grid").find(".table-body").find('tr').find('input:checked').length>0){
									updateSupply();
								}else{
									alert("<fmt:message key="Select_multiple_data" />！");
								}
							}else{
								alert("<fmt:message key="Please_choose_at_least_1_stores" />！");
							}
						}
					},{
							text: '<fmt:message key="removal_of_gravity"/>',
							title: '<fmt:message key="removal_of_gravity"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								//获取选择的分店
								var firm = [];
								window.parent.$(".treePanel1").find(":checked").map(function(){
									if($(this).attr("name")=='firm'){
										firm.push($(this).val());
									}
								});
								if(firm.length>0){
									seachSupply();
								}else{
									alert("<fmt:message key="please_select_branche"/>！");
								}
							}
						},{
							text: '<fmt:message key="supplies"/><fmt:message key="scm_copy"/>',
							title: '<fmt:message key="supplies"/><fmt:message key="scm_copy"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var flag = false;
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								if(checkboxList && checkboxList.filter(':checked').size() > 0){
									flag = true;
								} else {
									flag = false;
								}
								if(flag){
									copySupply();
								}else{
									alert("<fmt:message key ="please_select_materials" />！");
								}
								
								
							}
						},{
							text: '<fmt:message key="branche"/><fmt:message key="scm_copy"/>',
							title: '<fmt:message key="branche"/><fmt:message key="scm_copy"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								//获取选择的分店
								var firm = [];
								window.parent.$(".treePanel1").find(":checked").map(function(){
									if($(this).attr("name")=='firm'){
										firm.push($(this).val());
									}
								});
								if(firm.length==1){
									copyStoreSupply();
								}else{
									alert("<fmt:message key ="Please_select_a_store" />！");
								}
								
								
							}
						},
						//wangjie 2014年11月21日 16:43:18 分店物资属性 导出
						{
							text: '<fmt:message key="export"/>EXCEL',
							title: '<fmt:message key="export"/>EXCEL',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								//获取选择的分店
								var firm = [];
								window.parent.$(".treePanel1").find(":checked").map(function(){
									if($(this).attr("name")=='firm'){
										firm.push($(this).val());
									}
								});
								if(firm.length>0){
									$("#positnCode").val(firm.join(","));
									$("#wait2").val('NO');//不用等待加载
									$("#listForm").attr("action","<%=path%>/firmSupply/exportFirmSupply.do");
									$("#listForm").submit();
									$("#wait2 span").html("loading...");
									$("#listForm").attr("action","<%=path %>/firmSupply/table.do");
									$("#wait2").val('');
								}else{
									alert("<fmt:message key='please_select_branche'/>");
								}
							}
						},{
							text: '<fmt:message key="delete"/>',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
// 								获取选择的分店
								var firm = [];
								window.parent.$(".treePanel1").find(":checked").map(function(){
									if($(this).attr("name")=='firm'){
										firm.push($(this).val());
									}
								});
								if(firm.length>0){
									deleteFirmSupply();
								}else{
									alert("<fmt:message key="please_select_branche"/>！");
								}
							}
						},'-',{
							text: '<fmt:message key="Distribution_Unit"/>',
							title: '<fmt:message key="Distribution_Unit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							items:[{
								text: '<fmt:message key="update"/>',
								title: '<fmt:message key="update"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-40px','-20px']
								},
								handler: function(){
									updateDisunit();
								}
							},{
								text: '<fmt:message key="save"/>',
								title: '<fmt:message key="save"/>',
								useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-140px','-100px']
								},
								handler: function(){
									saveDisunit();
								}
							},{
								text: '<fmt:message key="Bulk_copy"/>',
								title: '<fmt:message key="Bulk_copy"/>',
								useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-140px','-100px']
								},
								handler: function(){
									bulkCopy();
								}
							}]
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
						
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				
				//物资复制  
				function copySupply() {
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var codeValue=[];
					checkboxList.filter(':checked').each(function(){
						codeValue.push($(this).val());
					});
					var action = '<%=path%>/firmSupply/copySupply.do?ids='+codeValue.join(",");
					$('body').window({
						title: '<fmt:message key ="Copy_the_data_from_the_store_to_the_following" />！',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 850,
						height: '500px',
						draggable: true,
						isModal: true
					});
				}
				
				function copyStoreSupply() {
					//var positnCode = $("#positnCode").val();
					//获取选择的分店
					var firm = [];
					window.parent.$(".treePanel1").find(":checked").map(function(){
						if($(this).attr("name")=='firm'){
							firm.push($(this).val());
						}
					});
					var action = '<%=path%>/firmSupply/copy.do?positn='+firm.join(",");
					$('body').window({
						title: '<fmt:message key ="Copy_the_data_from_the_store_to_the_following" />！',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 650,
						height: '500px',
						draggable: true,
						isModal: true
					});
				}
				
				function deleteFirmSupply(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					//获取选择的分店
					var firm = [];
					window.parent.$(".treePanel1").find(":checked").map(function(){
						if($(this).attr("name")=='firm'){
							firm.push($(this).val());
						}
					});
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key ="determine_the_deletion_of_material_or_modify_the_material_for_the_purchase_Please_delete" />！')){
							//总部分店物资属性表删除物资时增加二次确认  wangjie 2014年11月21日 17:08:40
							if(confirm("<fmt:message key ="determined_to_remove_the_material_from_the_store_Please_delete" />！")){
								var codeValue=[];
								checkboxList.filter(':checked').each(function(){
									codeValue.push($(this).val());
								});
								var action = '<%=path%>/firmSupply/delete.do?positn='+firm.join(",")+'&sp_code='+codeValue.join(",");
								$('body').window({
									title: '<fmt:message key="Delete_material" />',
									content: '<iframe frameborder="0" src='+action+'></iframe>',
									width: 500,
									height: '245px',
									draggable: true,
									isModal: true
								});
								window.setTimeout("pageReload()",1500);
							}
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
				
				function seachSupply(){
					//获取选择的分店
					//获取选择的分店
					var firm = [];
					window.parent.$(".treePanel1").find(":checked").map(function(){
						if($(this).attr("name")=='firm'){
							firm.push($(this).val());
						}
					});
					$("#positnCode").val(firm.join(","));
					$('body').window({
						id: 'window_saveSupply',
						title: '<fmt:message key="removal_of_gravity"/><fmt:message key="supplies"/>',
						content: '<iframe id="saveSupplyFrame" frameborder="0" src="<%=path%>/supply/addSupplyBatch.do?type=firm"></iframe>',
						width: ($(document.body).width()-50+'px')/* '820px' */,
						height:($(document.body).height()-110+'px')/* '550px' */,
						draggable: true,
						isModal: true
					});
				}
				function addSupply(){
					
					//alert(firm.join(","));
					$('body').window({
						id: 'window_saveSupply',
						title: '<fmt:message key="insert"/><fmt:message key="supplies"/>',
						content: '<iframe id="saveSupplyFrame" frameborder="0" src="<%=path%>/firmSupply/addSupplyBatch.do?type=firm"></iframe>',
						width: ($(document.body).width()-50+'px')/* '820px' */,
						height:($(document.body).height()-110+'px')/* '550px' */,
						draggable: true,
						isModal: true
					});
				}	
				function updateSupply(){
					//获取选择的分店
					var firm = [];
					window.parent.$(".treePanel1").find(":checked").map(function(){
						if($(this).attr("name")=='firm'){
							firm.push($(this).val());
						}
					});
					
					//获取选择的物资
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var codeValue=[];
					checkboxList.filter(':checked').each(function(){
						codeValue.push($(this).val());
					});
					//var sp_code = $('.grid').find('.table-body').find(':checked').val();
					$('body').window({
						id: 'window_saveSupply',
						title: '<fmt:message key ="modify_store_material_properties" />',
						content: '<iframe id="saveSupplyFrame" name="saveSupplyFrame" frameborder="0" src="<%=path%>/firmSupply/updateSupply.do?positn='+firm.join(",")+'&sp_code='+codeValue.join(",")+'"></iframe>',
						width: '400px',
						height: '300px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveSupplyFrame') && getFrame('saveSupplyFrame').validate._submitValidate()){
											//wangjie 2014年11月12日 09:01:43 控制最高验货比 不能 低于 最低验货比
											var flag = window.frames["saveSupplyFrame"].competorSp();
											if(flag == false){
												alert('<fmt:message key="The_highest_inspection_ratio_can_not_be_lower_than_the_lowest" />!');
												window.frames["saveSupplyFrame"].document.getElementById('sp_max1').focus();
											}else{
												submitFrameForm('saveSupplyFrame','positnForm');
											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				function saveSupply(){
					$('body').window({
						id: 'window_saveSupply',
						title: '<fmt:message key="New_material" />',
						content: '<iframe id="saveSupplyFrame" frameborder="0" src="<%=path%>/firmSupply/addSupplyBatch.do?type=firm"></iframe>',
						width: '820px',
						height: '550px',
						draggable: true,
						isModal: true
					});
				}	
				//向Table内添加数据
				addSp=function(id){
					var positnCode = $("#positnCode").val();
					var positnSpcode = {};
					positnSpcode['sp_code'] = id;
					positnSpcode['positn'] = positnCode;
					$.ajax({
						type: 'POST',
						url: '<%=path %>/firmSupply/save.do',
						data:positnSpcode,
						success: function(info){
							if(info && info === 'T'){
								//弹出提示信息
								showMessage({
									type: 'success',
									msg: "<fmt:message key ="save_successful" />！",
									speed: 1000
								});				
								window.setTimeout("pageReload()",1500);
								$('.close',parent.document).click();	
							}else{
								alert(info);
							}
						}
					});
					 
				};
				
				NewInput=function(){
					tabTableInput("sp_data","text"); //input  上下左右移动
					loadGrid();//  自动计算滚动条的js方法
					changeTh();//拖动 改变table 中的td宽度 
					$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
					$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					//点击checkbox改变
					$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
					     if ($(this)[0].checked) {
					    	 $(this).attr("checked", false);
					     }else{
					    	 $(this).attr("checked", true);
					     }
					 });
					// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').bind("click", function () {
					     if ($(this).find(':checkbox')[0].checked) {
					    	 $(this).find(':checkbox').attr("checked", false);
					     }else{
					    	 $(this).find(':checkbox').attr("checked", true);
					     }
					 });
				};				
			});
			function select_Supply(positnCode,code){
				
				//获取选择的分店
				var firm = [];
				window.parent.$(".treePanel1").find(":checked").map(function(){
					if($(this).attr("name")=='firm'){
						firm.push($(this).val());
					}
				});
				var positnSpcode = {};
				positnSpcode['sp_code'] = code;
				
				positnSpcode['positn'] = firm.join(",");
				
// 				alert($.toJSON(positnSpcode));
				
				if(code != null && code != ''){
					$.ajax({
						type: 'POST',
						url: '<%=path %>/firmSupply/saveByAddOnly.do',
						data:positnSpcode,
						success: function(info){
							if(info && info === 'T'){
								//弹出提示信息
								showMessage({
									type: 'success',
									msg: "<fmt:message key ="save_successful" />！",
									speed: 1000
								});				
								window.setTimeout("pageReload()",1500);
								$('.close',parent.document).click();	
							}else{
								alert(info);
							}
						}
					});
				}else{
					alert('<fmt:message key="please_select_materials"/>！');
				}
			}
			//左侧列表点击事件
			function textSubmit(positnCode){
				$('#listForm').attr('action','<%=path%>/firmSupply/table.do');
				$('#positnCode').val(positnCode);
				$('#listForm').submit();
			}
			
			function pageReload(){
				$('#listForm').submit();
			}
			//物资复制
			function selectOnePositn_d(idss,code){
				var positnCode = $("#positnCode").val();
				$.ajax({
					type: 'POST',
					url: '<%=path%>/firmSupply/saveByCopySupply.do?ids='+idss+'&code='+code+'&positnCode='+positnCode,
					success: function(info){
						if(info && info === 'T'){
							//弹出提示信息
							showMessage({
								type: 'success',
								msg: "<fmt:message key ="save_successful" />！",
								speed: 1000
							});				
							window.setTimeout("pageReload()",1500);
							$('.close',parent.document).click();	
						}else{
							alert(info);
						}
					}
				});
			}
			
			//修改配送单位
			function updateDisunit(){
				$('.disunit').attr('disabled',false);
			}
			
			//保存配送单位
			function saveDisunit(){
				var tr = $('.grid').find('.table-body').find('tr');
				var data = {};
				var i = 0;
				tr.each(function(){
					var positn = $(this).attr('data-positn');
					var sp_code = $(this).attr('data-sp_code');
					var disunit = $(this).find('td:eq(15)').find('select').val();
					var disunitper = $(this).find('td:eq(15)').find('select').find('option:selected').attr('data-disunitper');
					var dismincnt = $(this).find('td:eq(15)').find('select').find('option:selected').attr('data-dismincnt');
					data['positnSpcodeList['+i+'].positn'] = positn;
					data['positnSpcodeList['+i+'].sp_code'] = sp_code;
					data['positnSpcodeList['+i+'].disunit'] = disunit;
					data['positnSpcodeList['+i+'].disunitper'] = disunitper;
					data['positnSpcodeList['+i+'].dismincnt'] = dismincnt;
					i++;
				});
				if(i == 0){
					return;
				}
				$.ajax({
					url:'<%=path%>/firmSupply/updateDisunit.do',
					type:'post',
					data:data,
					success:function(msg){
						if(msg == 1){
							alert('<fmt:message key ="save_successful" />！');
							$('#listForm').submit();
						}
					}
				});
			}
			
			//批量复制
			function bulkCopy(){
				if(!confirm('是否将所选物资的配送单位复制到左侧选中的仓位？')){
					return;
				}
				//1.选择物资
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var codeValue=[];
				checkboxList.filter(':checked').each(function(){
					codeValue.push($(this).val());
				});
				if(codeValue.length <= 0){
					alert('请选择要复制的物资！');
					return;
				}
				//获取选择的分店
				var firm = [];
				window.parent.$(".treePanel1").find(":checked").map(function(){
					if($(this).attr("name")=='firm' && $("#positnCode").val() != $(this).val()){
						firm.push($(this).val());
					}
				});
				if(firm.length <= 0){
					alert('请在左侧选择要复制到的仓位！');
					return;
				}
				var data = {};
				data['positnCode'] = $("#positnCode").val();
				data['positn'] = firm.join(",");
				data['sp_code'] = codeValue.join(",");
				$.ajax({
					url:'<%=path%>/firmSupply/updateDisunitBatch.do',
					type:'post',
					data:data,
					success:function(msg){
						if(msg == 1){
							alert('<fmt:message key ="save_successful" />！');
							$('#listForm').submit();
						}
					}
				});
			}
		</script>
	</body>
</html>