<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			A.MzTreeview /* TreeView 链接的基本样式 */
		        {
		            cursor: hand;
		            color: black;
		            margin-top: 5px;
		            padding: 2 1 0 2;
		            text-decoration: none;
		        }
			</style>
		</head>
	<body>
		<div class="leftFrame">
      	<div id="toolbar"></div>
      	<form id="listForm" action="<%=path%>/firmSupply/list.do" method="post">
				<div class="form-line" style="margin-top: 2px;">
					<div class="form-label" style="width: 45px;"><fmt:message key ="branches_name" />：</div>
					<div class="form-input" style="width: 80px;">
						<input type="text" name="des" id="key" autocomplete="off" style="text-transform:uppercase;width:70px;" value="${positnSpcode.des}" style="width: 80px;" onkeydown="javascript: if(event.keyCode==13){$('#listForm').submit();} "/>
					</div>
					<div class="tool"></div>
				</div>
		</form>
	    <div class="treePanel1" style="overflow-x:auto;height: 89%;">
	    <script src="<%=path%>/js/tree/MzTreeView11.js" type="text/javascript"></script>
        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          tree.nodes['-1_00000000000000000000000000000002'] = 'ctrl:false;ctrlName:sc;text:<fmt:message key="main_direct_dial_library"/>;';
	          tree.nodes['-1_00000000000000000000000000000001'] = 'ctrl:false;ctrlName:sc;text:<fmt:message key="Base_depot" />;';
	          tree.nodes['-1_00000000000000000000000000000000'] = 'ctrl:false;ctrlName:sc;text:<fmt:message key="branche"/>;';
	          
	          //遍历所有主直拨库、基地仓库
	          <c:forEach var="area" items="${areaList}">
			      tree.nodes['00000000000000000000000000000001_001${area.code}'] 
		      		= 'ctrl:false;ctrlName:area;text:${area.code},${area.des};';
		      	  tree.nodes['00000000000000000000000000000002_002${area.code}'] 
		      		= 'ctrl:false;ctrlName:area;text:${area.code},${area.des};';
	          	<c:forEach var="positn1" items="${listPositn1}">
	          		<c:if test="${area.des == positn1.area}">
			          	<c:if test="${positn1.pcode == '1201'}">
			          		tree.nodes['002${area.code}_${positn1.code}'] 
		      					= 'ctrl:false;ctrlName:firm;text:${positn1.code},${positn1.des};method:changeURl(id,"${positn1.code}")';
		          		</c:if>
		          		<c:if test="${positn1.pcode == '1202'}">
			          		tree.nodes['001${area.code}_${positn1.code}'] 
		      					= 'ctrl:false;ctrlName:firm;text:${positn1.code},${positn1.des};method:changeURl(id,"${positn1.code}")';
		          		</c:if>
	          		</c:if>
	          	</c:forEach>
	          </c:forEach>
	          //遍历所有区域
	          <c:forEach var="area" items="${areaList}">
		          tree.nodes['00000000000000000000000000000000_f${area.code}'] 
	        		= 'ctrl:false;ctrlName:area;text:${area.code},${area.des};';
	        		//遍历分店
	        		<c:forEach var="positn" items="${listPositn}" varStatus="status">
	        			<c:if test="${area.code == positn.area}">
		        			tree.nodes['f${area.code}_${positn.code}'] 
		          				= 'ctrl:false;ctrlName:firm;text:${positn.code},${positn.des};method:changeURl(id,"${positn.code}")';
	        			</c:if>
			        </c:forEach>
	          </c:forEach>
	          
	          tree.setIconPath("<%=path%>/image/tree/none/");
	          document.write(tree.toString());
	          //tree.expandAll();//默认展开全部节点
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="<%=path%>/firmSupply/table.do?positn=105" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//实现滚动条
			//alert($("input[name=firm]").length);
		    var brow=$.browser;
			//alert($("input[type=checkbox]:eq(0)").css("margin-top"));
			//alert(brow.version);
			if(brow.version=="8.0" && brow.msie){
				tree.expandAll();
				$("input[type=checkbox]").css("margin-top","-12px");
				//$("input[name=firm]").css("margin-top","-12px");
			}
			setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
			//setElementWidth('.treePanel',['#toolbar'],$(document.body),30);
// 			$(".treePanel").width("75px");
// 			$(".treePanel").attr("overflow","scroll");
			
			var offset = $(".tool").prev("div").offset();
			offset.left = $(".tool").prev("div").width() + offset.left + 10;
			$(".tool").button({
				text:'<fmt:message key="select" />',
				container:$(".tool"),
				position: {
					type: 'absolute',
					top: offset.top,	
					left: offset.left
				},
				handler:function(){
					$("#listForm").submit();
				}
			});
			//自动实现滚动条
		//	setElementHeight('.grid',['.tool'],$(document.body),40);	//计算.grid的高度
			//setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
		//	loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				// var positnType=$(this).find('td:eq(1)').find('span').text();
				 var descode = $(this).find('td:eq(2)').find('input').val();
				 var descodeName = $(this).find('td:eq(1)').text();
				$("#mainFrame").attr("src","<%=path%>/positn/listDetail.do?descode="+descode+"&descodeName="+descodeName);
			});
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			<c:forEach items="${listPositn}" var="positn" varStatus="status">
				<c:if test="${status.count == 1}">
					$("#mainFrame").attr("src","<%=path%>/firmSupply/table.do?positn="+'${positn.code}');
				</c:if>
			</c:forEach>
		});// end $(document).ready();
		
		function changeURl(id,positn){
			$('#tree_checbox_'+id).attr('checked',true);
			$("#mainFrame").attr("src","<%=path%>/firmSupply/table.do?positn="+positn);
		}
		
	</script>

	</body>
</html>