<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送线路管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<form action="<%=path%>/linesFirm/list.do" id="leftForm" method="post"></form>
<div class="leftFrame" style="width: 50%;">
<div class="tool"> </div>
		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td style="width:30px;text-align: center;">
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="coding"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="name"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="scm_line"/>
									</span>
								</td>
								<td style="width:90px;text-align: center;">
									<span style="width: 80px;">
										<fmt:message key="scm_mileage"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="scm_standard_fuel"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="remark"/>
									</span>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="linesFirm" varStatus="step" items="${linesFirmList }">
								<tr>
									<td class="num"><span  style="width:16px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="code" id="lines_${linesFirm.code }" value="${linesFirm.code }" <c:if test="${step.count == 1 }">checked="checked"</c:if>/>
										<input type="hidden" name="acct" id="lines_${linesFirm.acct }" value="${linesFirm.acct }">
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">${linesFirm.code}</span>
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">${linesFirm.des }</span>
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">${linesFirm.des1 }</span>
									</td>
									<td style="width:90px; text-align: left;">
										<span style="width: 80px;">
											<c:if test="${linesFirm.mileage == 0}">
												0km
											</c:if>
											<c:if test="${linesFirm.mileage != 0}">
											<fmt:formatNumber type="Number" value="${linesFirm.mileage }" pattern="##.##km" minFractionDigits="2"/>
											</c:if>
										</span>
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">
											<c:if test="${linesFirm.staoil == 0}">
												0L
											</c:if>
											<c:if test="${linesFirm.staoil != 0}">
											<fmt:formatNumber type="Number" value="${linesFirm.staoil }" pattern="##.##L" minFractionDigits="2"/>
											</c:if>
										</span>
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">${linesFirm.memo }</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div style="display: inline-block;float: left;width: 49%;" class="mainFrame">
    		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.moduleInfo'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			var code1 = $(".grid").find(".table-body").find("tr:eq(0)").find(":checkbox").val();
			var acct1 = $(".grid").find(".table-body").find("tr:eq(0)").find("input[name=acct]").val();
			window.mainFrame.location = "<%=path%>/linesFirm/findFirmByLinesFirmId.do?code="+code1+"&acct="+acct1;
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							saveLiensFirm();
						}
					},{
						text: '<fmt:message key="update" />',
						title: '<fmt:message key="update" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							
							position: ['-18px','0px']
						},
						handler: function(){
							updateLiensFirm();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteLiensFirm();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
			
 			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$('.grid').find('.table-body').find(":checkbox").removeAttr("checked");
		        $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
		        var code=$(this).removeClass("bgBlue").find(":checkbox").val();
				var acct=$(this).removeClass("bgBlue").find(":checkbox").next("input").val();
		        window.mainFrame.location = "<%=path%>/linesFirm/findFirmByLinesFirmId.do?code="+code+"&acct="+acct;
			 });
// 			$('.grid').find('.table-body').find('tr:eq(0)').click();
	 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//-添加、修改、删除--------------------------------------------------------------------
			saveLiensFirm=function(){
				$('body').window({
					id: 'window_LiensFirm',
					title: '<fmt:message key="insert" /> <fmt:message key ="scm_psLine" />',
					content: '<iframe id="saveLiensFirmFrame" frameborder="0" src="<%=path%>/linesFirm/saveLiensFirm.do"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			};
			function deleteLiensFirm(){
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert("<fmt:message key="Please_choose_the_route" />！");
					return;
				}
				var code = $('.grid').find('.table-body').find(':checkbox:checked');
				var acct = code.next("input").val();
				if(code.val()!=null){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var action = '<%=path%>/linesFirm/deleteLiensFirm.do?code='+code.val()+'&acct='+acct;
						$('body').window({
							title: '<fmt:message key="Delete_distribution_line" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			};
			updateLiensFirm=function(){
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert("<fmt:message key="Please_choose_the_route" />！");
					return;
				}
				var code=$('.grid').find('.table-body').find(':checkbox:checked');
				var acct=code.next("input").val();
				$('body').window({
					id: 'window_update_surveyd',
					title: '<fmt:message key="update" /><fmt:message key ="scm_psLine" />',
					content: '<iframe id="updateLiensFirmFrame" frameborder="0" src="<%=path%>/linesFirm/update.do?code='+code.val()+'&acct='+acct+'"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			}
			//-------------------------------------------------------------------------------
		})
		</script>
</body>
</html>