<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送线路分店管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<div class="tool"> </div>
<input type="hidden" id="firmCode">
<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td style="width:30px;text-align: center;">
										<span style="width:30px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td style="width:280px;text-align: center;">
									<span style="width: 270px;">
										<fmt:message key="branche"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="scm_modify_time"/>
									</span>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
				 	<input type="hidden" id="code" value="${linesFirm.code}"/>
				 	<input type="hidden" id="acct" value="${linesFirm.acct}"/>
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="AcctLineFirm" varStatus="step" items="${AcctLineFirmList}">
								<tr>
									<td class="num"><span  style="width:16px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<span style="width:30px;">
										 	<input type="checkbox" name="idList" id="chk_${AcctLineFirm.firm.code}" value="${AcctLineFirm.firm.code}"/>
										 </span>
									</td>
									<td style="width:280px; text-align: left;">
										<span style="width: 270px;">${AcctLineFirm.firm.des }</span>
									</td>
									<td style="width:80px; text-align: left;">
										<span style="width: 70px;">${AcctLineFirm.tim }</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
				</table>
		</div>
</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							addAcctLineFirm();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteAcctLineFirm();
						}
					},{
						text: '<fmt:message key="update"/><fmt:message key="scm_modify_time"/>',
						title: '<fmt:message key="update"/><fmt:message key="scm_modify_time"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							updateTim();
						}
					}
				]
			});
		
			setElementHeight('.grid',['.moduleInfo'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',50);	//计算.table-body的高度
		
 			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
	 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//---------增、删、改--------------------------------------------------------------------------
			addAcctLineFirm=function(){
				if(!!!top.customWindow){
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn=3&mold=oneTmany'),null,$('#firmDes'),$('#firmCode'),700,600,'isNUll',handler);
					//cust(titl,path,offset,nameInput,idInput,width,height,isNUll,handler)
				}
			};
			function handler(firmCode){
				var code = $("#code").val();
				var acct = $("#acct").val();
				if(firmCode==undefined || ''==firmCode){
					return;
				}
				$.ajax({
					type: "POST",
					url: "<%=path%>/linesFirm/saveAcctLineFirm.do",
					data: {code:code,acct:acct,'firm.code':firmCode},
					dataType: "json",
					success:function(date){
						if(date==""){
							alert("<fmt:message key='successful_added'/>");
						}else{
							alert(date);
						}
						location.href = location.href;
					}
				});
			}
			deleteAcctLineFirm=function(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var code = $("#code").val();
				var acct = $("#acct").val();
				if(checkboxList&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/linesFirm/deleteAcctLinesFirm.do?firm.code='+chkValue.join(",")+'&acct='+acct+'&code='+code;
						$('body').window({
							title: '<fmt:message key ="delete" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			};
			updateTim = function(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var code = $("#code").val();
				var acct = $("#acct").val();
				if(checkboxList&& checkboxList.filter(':checked').size() > 0){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/linesFirm/updateTim.do?firm.code='+chkValue.join(",")+'&acct='+acct+'&code='+code;
						$('body').window({
							title: '<fmt:message key="update"/><fmt:message key="scm_modify_time"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 420,
							height: 130,
							draggable: true,
							isModal: true,
							confirmClose:false
						});
				}else{
					alert('<fmt:message key="please_select"/><fmt:message key="update"/><fmt:message key="data"/>！');
					return ;
				}
			};
			//------------------------------------------------------------------------------------
			
		});
		</script>
</body>
</html>