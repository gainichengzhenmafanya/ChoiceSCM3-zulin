<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改配送线路</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<div class="tool"> </div>
<form id="updateLiensFirmForm" action="<%=path%>/linesFirm/updateLineFirm.do" method="post">
	<div style="width:100%;height:auto;margin:0 auto;">
		<div class="form-line">
			<div class="form-label"><span class="red">*</span><fmt:message key="name" />：</div>
			<div class="form-input">
				<input type="hidden" name="code" value="${LineFirm.code }">
				<input type="text" id="des" name="des" value="${LineFirm.des }" maxlength="10">
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="scm_line" />：</div>
			<div class="form-input">
				<input type="text" id="des1" name="des1" value="${LineFirm.des1 }" maxlength="100">
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="scm_mileage" />：</div>
			<div class="form-input">
				<input type="text" id="mileage" name="mileage" value="${LineFirm.mileage }" maxlength="7">km
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="scm_standard_fuel" />：</div>
			<div class="form-input">
				<input type="text" id="staoil" name="staoil" value="${LineFirm.staoil }" maxlength="7">L
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="remark" />：</div>
			<div class="form-input">
				<textarea rows="5" id="memo" name="memo" maxlength="50">${LineFirm.memo }</textarea>
			</div>
		</div>
	</div>
</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			var validate;
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateLiensFirm()
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(".close",parent.document).click();
						}
					}
				]
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'mileage',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="number_be_not_number" />！']
					},{
						type:'text',
						validateObj:'staoil',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="number_be_not_number" />！']
					}]
				});
				
				//--------------------增加 删除---------------------------------
				function updateLiensFirm(){
					if(validate._submitValidate()){
						$("form").submit();
					}
				}
				//-----------------------------------------------------
			})
		</script>
</body>
</html>