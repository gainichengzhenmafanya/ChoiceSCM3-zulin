<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送线路分店管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<style type="text/css">
				.line{
					BORDER-LEFT-STYLE: none;
					BORDER-RIGHT-STYLE: none;
					BORDER-TOP-STYLE: none;
					width: 100%;
				}
			</style>
</head>
<body>
<div class="tool"> </div>
	<c:forEach items="${frimCode }" var="code">
		<input type="hidden" id="firmCode" value="${code }">
	</c:forEach>
	<input type="hidden" id="Code" value="${Code }">
	<input type="hidden" id="Acct" value="${Acct }">
	<div>
		<fmt:message key ="scm_modify_time" />：<input type="text" id="tim" name="tim"><br/><span style="color: red;"><fmt:message key ="explain" /></span>：<br/>
		(<fmt:message key ="scm_psLineExplain" />)
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							updateTime();
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(".close",parent.document).click();
						}
					}
				]
			});
		
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'tim',
					validateType:['num1'],
					param:['F'],
					error:['<fmt:message key ="error" />！']
				}]
			});
		
 			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
	 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//-----------------------------------------------------------------------------------
			updateTime=function(){
		 		var datas = {};
		 		var acctLineFirmNo = 0;
		 		var acctLineFirm = $("input[id=firmCode]");
		 		acctLineFirm.each(function(n,data){
		 			datas["acctLineFirmList["+acctLineFirmNo+"].firm.code"] = $.trim($(data).val());
		 			datas["acctLineFirmList["+acctLineFirmNo+"].tim"] = $.trim($("#tim").val());
		 			datas["acctLineFirmList["+acctLineFirmNo+"].code"] = $.trim($("#Code").val());
		 			datas["acctLineFirmList["+acctLineFirmNo+"].acct"] = $.trim($("#Acct").val());
		 			acctLineFirmNo++;
		 		});
		 		if(validate._submitValidate()){
			 		$.ajax({
			 				url:"<%=path%>/linesFirm/updateTimto.do",
		 					type:'POST',
		 					async:false,
		 					data:datas,
		  	 				success:function(data){
		  	 					if(data=="success"){
		  	 						alert("<fmt:message key ="update_successful" />！");
			  	 					parent.location.href = parent.location.href;
		  	 					}else{
		  	 						alert(data+"！");
		  	 					}
		  	 				}
					});
		 		}
		 	}
			//------------------------------------------------------------------------------------
			
		})
		</script>
</body>
</html>