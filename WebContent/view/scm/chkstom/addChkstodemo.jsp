<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.blueBGColor{background-color:	#F1F1F1;}
		.redBGColor{background-color:	#F1F1F1;}
		.onEdit{
			background:lightblue;
			border:1px solid;
			border-bottom-color: blue;
			border-top-color: blue;
			border-left-color: blue;
			border-right-color: blue;
		}
		.input{
			background:transparent;
			border:1px solid;
		}
		</style>		
</head>
<body>
	<div class="tool"></div>
	<input type="hidden" id="subSta" name="subSta"/>
	<form id="listForm" action="<%=path%>/chkstodemom/addChkstoDemo.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="selectTitle" name="selectTitle" value="${title}"/>
				<input type="hidden" id="firm" name="firm" value="${firm}"/><!-- 适用分店wjf -->
				<select class="select" id="title" name="title" onchange="findByTitle(this);">
<%-- 					<option value=""><fmt:message key="all" /></option>				 --%>
					<c:forEach var="chkstodemo" items="${listTitle}" varStatus="status">
						<option
							<c:if test="${chkstodemo.title==title}"> selected="selected" </c:if> value="${chkstodemo.title}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 25px;"></span></td>
							<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
							<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
							<td><span style="width:80px;"><fmt:message key="specification" /></span></td>
<!-- 							<td><span style="width:80px;">标准数量</span></td> -->
							<td><span style="width:80px;"><fmt:message key ="standard_unit" /></span></td>
							<td><span style="width:80px;"><fmt:message key ="scm_standard_sale" /></span></td>
<!-- 							<td><span style="width:80px;">标准金额</span></td> -->
<!-- 							<td><span style="width:80px;">参考数量</span></td> -->
							<td><span style="width:80px;"><fmt:message key ="reference_unit" /></span></td>
							<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table id="tblGrid" cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr>
								<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
								<td style="display:none;"><span style="text-align: center;"><input type="hidden" name="idList" id="chk_${chkstodemo.chkstodemono}" value="${chkstodemo.chkstodemono}"/></span></td>
<%-- 								<td style="width:100px;">${chkstodemo.title}</td> --%>
								<td title="${chkstodemo.supply.sp_code}"><span  style="width:80px;">${chkstodemo.supply.sp_code}</span></td>
								<td title="${chkstodemo.supply.sp_name}"><span  style="width:120px;">${chkstodemo.supply.sp_name}</span></td>
								<td title="${chkstodemo.supply.sp_desc}"><span  style="width:80px;">${chkstodemo.supply.sp_desc}</span></td>
<%-- 								<td><span style="width:80px;">${chkstodemo.amount}</span></td> --%>
								<td><span style="width:80px;">${chkstodemo.supply.unit}</span></td>
								<td><span style="width:80px;">${chkstodemo.price}</span></td>
								<!-- wangjie 2014年11月12日 14:35:02  报货单据模板，格式化标准售价、参考数量 -->
<%-- 								<td><span style="width:80px;"><fmt:formatNumber type="number" value="${chkstodemo.amount*chkstodemo.pricesale} " pattern="0.00"/>&nbsp;</span></td> --%>
<%-- 								<td><span style="width:80px;"><fmt:formatNumber type="number" value="${chkstodemo.amount*chkstodemo.supply.unitper} " pattern="0.00"/>&nbsp;</span></td> --%>
								<td><span style="width:80px;">${chkstodemo.supply.unit1}</span></td>
								<td><span style="width:120px;">${chkstodemo.memo}</span></td>
								<td style="display:none;"><span><input type="hidden" value="${chkstodemo.supply.unitper}"/></span></td>								
								<td style="display:none;"><span><input type="hidden" value="${chkstodemo.supply.mincnt}"/></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
<%-- 		<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />		 --%>
<!-- 	</form>				 -->
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript">
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	    		if(index=="5"){
		 			$(e.srcElement).unbind('blur').blur(function(e){
		 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		 			});
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
	    		}
	    	}
		}); 
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
	});
	
	//动态下拉列表框
	function findByTitle(e){
		$('#listForm').submit();
	}
	//确认修改
	function enterUpdate() {
		var checkboxList = $('.grid').find('.table-body').find('tr');
		var data = {};
		var i = 0;
		
		checkboxList.each(function(){
			data['chkstodemoList['+i+'].supply.sp_code'] = $(this).find('td:eq(2) span').text();
			data['chkstodemoList['+i+'].supply.sp_name'] = $(this).find('td:eq(3) span').text();
			data['chkstodemoList['+i+'].supply.sp_desc'] = $(this).find('td:eq(4) span').text();
			data['chkstodemoList['+i+'].cnt'] = 0;
// 			data['chkstodemoList['+i+'].cnt'] = $(this).find('td:eq(5)').text();
			data['chkstodemoList['+i+'].supply.unit'] = $(this).find('td:eq(5)').text();
			data['chkstodemoList['+i+'].supply.sp_price'] = $(this).find('td:eq(6)').text();
			data['chkstodemoList['+i+'].cnt1'] = 0;
// 			data['chkstodemoList['+i+'].cnt1'] = $(this).find('td:eq(9)').text();
			data['chkstodemoList['+i+'].supply.unit1'] = $(this).find('td:eq(7)').text();
			data['chkstodemoList['+i+'].memo'] = $(this).find('td:eq(8)').text();
			data['chkstodemoList['+i+'].unitper'] = $(this).find('td:eq(9)').find('input').val();
			data['chkstodemoList['+i+'].supply.mincnt'] = $(this).find('td:eq(10)').find('input').val();
			data['chkstodemoList['+i+'].iszs'] = '0';
			i++;
		});
		$.post("<%=path%>/chkstodemom/enterUpdate.do",data,function(data){
			parent.appendChkstodemo(data);
		});
// 		$('.close').click();
	}
	//编辑表格
	function editCells(){
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:11,
			widths:[26,80,120,80,80,80,80,80,80,80,120],
			colStyle:['','','','',{background:"#F1F1F1"},'','','','','','','',''],
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[5],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
			},
			cellAction:[{
				index:5,
				action:function(row,data2){
					if(Number(data2.value) == 0){
						alert('<fmt:message key="number_cannot_be_zero"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(Number(data2.value) < 0){
						alert('<fmt:message key ="number_cannot_be_negative" />！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					}else if(isNaN(data2.value)){
						alert('<fmt:message key="number_be_not_number"/>！');
						row.find("td:eq(5)").find('span').text(data2.ovalue);
						$.fn.autoGrid.setCellEditable(row,5);
					} else {
						row.find("td:eq(8) span").text((Number(row.find("td:eq(5)").text())*Number(row.find("td:eq(7)").text())).toFixed(2));
						var unitper = row.find("td:eq(12)").text();//编辑时data中无tax数据，先添加进data
						row.data("unitper",unitper);
						row.find("td:eq(9) span").text((Number(row.find("td:eq(5)").text())*Number(row.data("unitper"))).toFixed(2));
						$.fn.autoGrid.setCellEditable(row.next(),5);
					}
				}
			}]
		});
	}
	
	function validateByMincnt(index,row,data2){//最小申购量判断
		if(index=="5"){
			if(isNaN(data2.value)||Number(data2.value) <= 0){
				alert('<fmt:message key="please_enter_positive_integer"/>！');
				row.find("input").focus();
			}else{
				row.find("td:eq(5)").find('span').text(Number(data2.value).toFixed(2));
			}
		}
	}
	function validator(index,row,data2){//输入框验证
		if(index=="5"){
			if(isNaN(data2.value)||Number(data2.value) <= 0){
				data2.value=0;
			}
			row.find("input").data("ovalue",Number(data2.value).toFixed(2));
			row.find("td:eq(8)").find('span').text((Number(data2.value)*Number(row.find("td:eq(7)").text())).toFixed(2));
			var unitper = row.find("td:eq(12)").text();//编辑时data中无tax数据，先添加进data
			row.data("unitper",unitper);
			row.find("td:eq(9) span").text((Number(data2.value)*row.data("unitper")).toFixed(2));
		}
	}
	</script>
</body>
</html>