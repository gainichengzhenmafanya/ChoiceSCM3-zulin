<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_messages_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 90px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			} 
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/chkstom/listCheckedChkstom.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>						
				<div class="form-label"><fmt:message key="orders_num"/>：</div>
				<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo}" onkeyup="value=value.replace(/[^\d]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/></div>
				<div class="form-label"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
 				<div class="form-label" style="width:80px;margin-left:30px;"><fmt:message key="purchase_branche"/>：</div>
				<div class="form-input">
					<input type="text"  id="firmDes" name="positn.des" readonly="readonly" value="${chkstom.positn.des}"/>
					<input type="hidden" id="firm" name="firm" value="${chkstom.firm}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
				<div class="form-label"><fmt:message key="orders_maker"/>：</div>
				<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkstom.madeby}"/></div>
				<div class="form-label"><fmt:message key="orders_audit"/>：</div>
				<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="${chkstom.checby}"/></div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"></span></td>
								<td><span style="width:70px;"><fmt:message key="reported_num"/></span></td>
								<td><span style="width:100px;"><fmt:message key="reported_date"/></span></td>
								<td><span style="width:130px;"><fmt:message key="fill_time"/></span></td>
								<td><span style="width:100px;"><fmt:message key="purchase_branche"/></span></td>
								<td><span style="width:80px;"><fmt:message key="total_amount1"/></span></td>
								<td><span style="width:100px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_audit"/></span></td>
								<td><span style="width:80px;"><fmt:message key="whether_upload"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:70px;text-align: right;">${chkstom.chkstoNo }</span></td>
									<td><span style="width:100px;"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:130px;">${chkstom.madet}</span></td>
									<td><span style="width:100px;">${chkstom.positn.des }</span></td>
									<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstom.totalAmt }" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span style="width:100px;">${chkstom.madeby }</span></td>
									<td><span style="width:80px;">${chkstom.checby }</span></td>
									<td><span style="width:80px;">${chkstom.bak1}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			if($('#chkstoNo').val()==0)$('#chkstoNo').val('');
			/*过滤*/
			$('#firmDes').bind('keyup',function(){
		          $("#firm").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				searchCheckedChkstom();
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('.<fmt:message key="select"/>').click(); break;
	                case 80: $('.<fmt:message key="print"/>').click(); break;
	            }
			}); 
			   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
			
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="view" />(<u>V</u>)',
					title: '<fmt:message key="view"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						searchCheckedChkstom();
					}
				},{
					text: '<fmt:message key="uncheck" />',
					title: '<fmt:message key="uncheck" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-58px','-240px']
					},
					handler: function(){
						uncheckChkstom();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		 	/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkstoNo2',
					validateType:['canNull','intege'],
					param:['T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firm').val();;
					var defaultName = $('#firmDes').val();
					var offset = getOffset('chkstoNo');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firm'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();;
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
		});
		var detailWin;
		//查看已审核单据的详细信息
		function searchCheckedChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedChkstomDetail',
						title: '<fmt:message key ="The_query_has_audit_report_note_details" />',
						content: '<iframe id="checkedChkstomDetailFrame" frameborder="0" src="<%=path%>/chkstom/searchCheckedChkstom.do?chkstoNo='+chkValue+'"></iframe>',
						width: $(document.body).width()-20/* '1050px' */,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		//反审核
		function uncheckChkstom(){
			if(!confirm('<fmt:message key="Are_all_materials_to_be_examined_under_the_current_conditions_of_the_review"/>？'))return;
			var data = {};
	        data['chkstoNo'] = $('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
	        
	        //wangjie 2014年11月5日 13:54:40  （报货单填制提交，未选择数据，点反审核 无反应，不提示。）
	        if(data['chkstoNo'] == 'undefined' || data['chkstoNo'] == undefined){
	        	alert('<fmt:message key="please_select"/>1<fmt:message key="article"/><fmt:message key="data"/><fmt:message key="uncheck"/>!');
	        }else{
	        	//判断是否已经采购确认，采购审核，是否已经出库或入库
		    	$.post("<%=path%>/chkstom/checkYnUnChk.do",data,function(data){ 
					if(data!=0){
						msg="no";
						alert('<fmt:message key="there_has_been_the_purchase_confirmation_or_procurement_audit_materials"/>,<fmt:message key="can_not"/><fmt:message key="uncheck"/>！');
						return;
					}else{
						var chkstoNo=$('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
						if(chkstoNo!=null && chkstoNo!=""){
							//提交并返回值，判断执行状态
							$.post("<%=path%>/chkstom/uncheckChkstom.do?chkstoNo="+chkstoNo,function(data){
								var rs = eval('('+data+')');
								switch(Number(rs.status)){
								case 1:
									showMessage({
										type: 'success',
										msg: '<fmt:message key="uncheck"/><fmt:message key="successful"/>！',
										speed: 3000
									});
									$('#listForm').submit();
									break;
								case 0:
									alert('<fmt:message key="uncheck"/><fmt:message key="failure"/>！');
									break;
								}
							});
						}
					}
				});
	        }
		}
		
		//打印单条
		function printChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				window.open ("<%=path%>/chkstom/printChkstom.do?chkstoNo="+chkValue.join(","),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}else{
				alert('<fmt:message key="please_select_single_message_to_print"/>！');
				return ;
			}
		}
		</script>				
	</body>
</html>