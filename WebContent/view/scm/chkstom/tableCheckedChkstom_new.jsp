<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="audited_reported_manifest"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/chkstom/saveByAdd.do" method="post">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/></div>
					<div class="form-input" style="width:200px;">
						<input type="text" style="width:200px;" class="text" id="maded" name="maded" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd"/>" disabled="disabled"/>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/>:</div>
					<div class="form-input">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
<%-- 						<input type="text" value="${chkstom.chkstoNo }" class="text" disabled="disabled"/> --%>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="purchase_positions"/></div>
					<div class="form-input" style="width:200px;">
						<input type="text" id="firmDes" onfocus="this.select()" name="firmDes" style="width:40px;height:16px;margin-top:4px;vertical-align:top;" value="${chkstom.firm}" readonly="readonly"/>
						<select class="select" id="firm" name="firm" style="width:155px" onchange="findByDes(this);" disabled="disabled">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.firm}"> 
									   	selected="selected"
								</c:if>
								id="des" value="${positn.code}" >${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>					
					<div class="form-label"><fmt:message key="make_orders"/>:</div>
					<div class="form-input">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madeby}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
<%-- 						<input type="text" value="${chkstom.madeby }" class="text" disabled="disabled"/> --%>
					</div>					
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="width:200px;"></div>
					<div class="form-label"><fmt:message key="accounting"/>:</div>
					<div id="showChecby" class="form-input">
						<c:if test="${chkstom.checby!=null}"><c:out value="${chkstom.checby}"></c:out></c:if>				
						<input type="hidden" id="checby" name="checby" class="text" value="${chkstom.checby }"/>
<%-- 						<input type="text" id="showChecby" value="${chkstom.checby }" class="text" disabled="disabled"/> --%>
					</div>														
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0" id="table_head">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="2"><fmt:message key="procurement_unit"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstod" items="${chkstodList}" varStatus="status">
							<tr>
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstod.supply.sp_code }</span></td>
								<td><span style="width:100px;">${chkstod.supply.sp_name }</span></td>
								<td><span style="width:90px;">${chkstod.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;">${chkstod.amount1 }</span></td>
								<td><span style="width:55px;">${chkstod.supply.unit3 }</span></td>
								<td><span style="width:50px;text-align: right;">${chkstod.amount }</span></td>
								<td><span style="width:50px;">${chkstod.supply.unit }</span></td>
								<td><span style="width:50px;text-align: right;"><fmt:formatNumber value="${chkstod.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:50px;text-align: right;"><fmt:formatNumber value="${chkstod.supply.sp_price*chkstod.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:90px;">${chkstod.hoped}</span></td>
								<c:choose>
									<c:when test="${fn:contains(chkstod.memo, '##')}">
										<c:set var="memo" value="${fn:split(chkstod.memo, '##')}"></c:set>
										<td><span style="width:70px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
										<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
									</c:when>
									<c:otherwise>
										<td><span style="width:70px;" title="${chkstod.memo}">${chkstod.memo}</span></td>
										<td><span style="width:70px;"></span></td>
									</c:otherwise>
								</c:choose>
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + chkstod.amount1}"/>   
							<c:set var="sum_price" value="${sum_price + chkstod.supply.sp_price*chkstod.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num"><fmt:formatNumber value="${sum_num}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:100px;"></td>
							<td style="width:100px;background:#F1F1F1;">总金额：<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label></td>
							<td style="width:65px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:65px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;"></td>
							<td style="width:80px;"></td>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/colResizable-1.5.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/myResizableTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">		
		//工具栏
		$(document).ready(function(){
			//屏蔽鼠标右键
			//$(document).bind('contextmenu',function(){return false;});
			//键盘事件绑定
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 80: $('#autoId-button-101').click(); break;//打印
	            }
			}); 
			//判断按钮的显示与隐藏
			loadToolBar([true]);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),105);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			
			//使表列宽度可调整
			tableCanResize('table_head','tblGrid');
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printChkstom();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();						
						}
					}
				]
			});
		}			
		//打印单据
		function printChkstom(){ 
			if(null!=$("#chkstoNo").val() && ""!=$("#chkstoNo").val())
			{
				window.open ("<%=path%>/chkstom/printChkstom.do?chkstoNo="+$("#chkstoNo").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		}
		</script>			
	</body>
</html>