<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="view_upload"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<style type="text/css">
		.bgBlue{
			background-color:lightblue;
		}
		</style>
	</head>
	<body>
	<div class="tool"></div>	
	<div class="form">
		<form id="listForm" action="<%=path%>/chkstom/listChkstomState.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="date"/></div>
				<div class="form-input">
					<input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker()"/>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td ><span class="num" style="width:25px;text-align:center;">&nbsp;</span></td>
								<td style="width:120px;"><fmt:message key="branch_name"/></td>
								<td style="width:80px;"><fmt:message key="initial_time"/></td>
								<td style="width:80px;"><fmt:message key="last_time"/></td>
								<td style="width:60px;"><fmt:message key="documents_num"/></td>
								<td style="width:60px;"><fmt:message key="Uncommitted"/></td>
								<td style="width:60px;"><fmt:message key="already"/><fmt:message key="submit"/></td>
								<c:if test="${chkstom.bak2 == 1 }">
									<td style="width:70px;">未传中间表</td>
									<td style="width:70px;">已传中间表</td>
								</c:if>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chksto" items="${chkstomList }" varStatus="status">
								<tr 
									<c:choose>
										<c:when test="${chkstom.bak2 == 1 }">
											<c:if test="${chksto.WTJ > 0 || chksto.WCJJB > 0}"> 
											   	style="background-color: red;"
											</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${chksto.WTJ > 0 }"> 
											   	style="background-color: red;"
											</c:if>
										</c:otherwise>
									</c:choose>>
									<td ><span class="num" style="width:25px;text-align:center;">${status.index + 1}</span></td>
									<td ><span style="width:110px;">${chksto.POSITNDES}</span></td>
									<td title=""><span style="width:70px;">${chksto.MINMADET}</span></td>
									<td title=""><span style="width:70px;">${chksto.MAXMADET}</span></td>
									<td title=""><span style="width:50px;text-align:right;">${chksto.DJZS}</span></td>
									<td title=""><span style="width:50px;text-align:right;">${chksto.WTJ}</span></td>
									<td title=""><span style="width:50px;text-align:right;">${chksto.YTJ}</span></td>
									<c:if test="${chkstom.bak2 == 1 }">
										<td title=""><span style="width:60px;text-align:right;">${chksto.WCJJB}</span></td>
										<td title=""><span style="width:60px;text-align:right;">${chksto.YCJJB}</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>		
		</form>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
			});  
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),65);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法	
		});
		var tool = $('.tool').toolbar({
			items: [{
				text: '<fmt:message key="select"/>',
				title: '<fmt:message key="select"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					$("#listForm").submit();
				}
			},{
				text: '<fmt:message key="quit"/>',
				title: '<fmt:message key="quit"/>',
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
				}
			}]
		});
		</script>				
	</body>
</html>