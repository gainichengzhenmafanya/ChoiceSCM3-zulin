<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="update" /><fmt:message key="holidays" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<form id="holidayForm" method="post" action="<%=path %>/holidayBoh/saveByUpdate.do">
				<input type="hidden" id="pk_holidaydefine" name="pk_holidaydefine" value="${holiday.pk_holidaydefine}"/>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="years" /></div>
					<div class="form-input"><input type="text" id="vyear" name="vyear" class="text" value="${holiday.vyear}"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="date" /></div>
					<div class="form-input"><input  type="text" class="text" id="dholidaydate" name="dholidaydate"  value="${holiday.dholidaydate }" /></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="holidays" /><fmt:message key="name" /></div>
					<div class="form-input">
						<input type="text" id="vholidayname" name="vholidayname" class="text" value="${holiday.vholidayname }"/>
					</div>
				</div>
				<%-- <div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="enable_state" /></div>
					<div class="form-input">
						<select id="enablestate" class="select" name="enablestate" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
							<option value="1"  <c:if test="${holiday.enablestate=='1'}"> selected="selected"</c:if>  ><fmt:message key="not_enabled" /></option>
							<option value="2"  <c:if test="${holiday.enablestate=='2'}"> selected="selected"</c:if>  ><fmt:message key="have_enabled" /></option>
							<option value="3"  <c:if test="${holiday.enablestate=='3'}"> selected="selected"</c:if>  ><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div> --%>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="holidays" /><fmt:message key="level" /></div>
					<div class="form-input">
						<select id="vholidaygrade" class="select" name="vholidaygrade" style="height: 22px;margin-top: 3px; border: 1px solid #999999;">
<%-- 						<option value=""  <c:if test="${holiday.vholidaygrade==''}"> selected="selected"</c:if> ></option> --%>
						<option value="1" <c:if test="${holiday.vholidaygrade=='1'}"> selected="selected"</c:if> ><fmt:message key="a" /></option>
						<option value="2" <c:if test="${holiday.vholidaygrade=='2'}"> selected="selected"</c:if> ><fmt:message key="b" /></option>
						<option value="3" <c:if test="${holiday.vholidaygrade=='3'}"> selected="selected"</c:if> ><fmt:message key="c" /></option>
						<option value="4" <c:if test="${holiday.vholidaygrade=='4'}"> selected="selected"</c:if> ><fmt:message key="d" /></option>
						<option value="5" <c:if test="${holiday.vholidaygrade=='5'}"> selected="selected"</c:if> ><fmt:message key="e" /></option>
						<option value="6" <c:if test="${holiday.vholidaygrade=='6'}"> selected="selected"</c:if> ><fmt:message key="f" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" /></div>
					<div class="form-input">
<%-- 					<textarea name="vmemo" id="vmemo" style="white-space:wrap;" rows="7" cols="24">${holiday.vmemo }</textarea> --%>
						<input type="text" id="vmemo" name="vmemo" class="text" value="${holiday.vmemo }"/>
					</div>
				</div>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vyear',
						validateType:['canNull','num1','maxValue','minValue'],
						param:['F','F','2099','1900'],
						error:['<fmt:message key="years" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="only_digits" />！','<fmt:message key="years" /><fmt:message key="only_between" />【1990，2099】！','<fmt:message key="years" /><fmt:message key="only_between" />【1990，2099】！']
					},{
						type:'text',
						validateObj:'dholidaydate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vholidayname',
						validateType:['canNull','maxLength'],
						param:['F',50],
						error:['<fmt:message key="holidays" /><fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:[200],
						error:['<fmt:message key="the_maximum_length" />200！'],
					}],
				});
			
			$('#dholidaydate').bind('click',function(){
				new WdatePicker();
			});
			
			var tabArray; //定义需要进行切换的组件的id的数组			
			//以下部分是enter按键  代替tab键   当执行到最后一个输入框后，点击enter键   自动保存。
			tabArray=['vyear','dholidaydate','vholidayname','vholidaygrade','vmemo'];
			//定义加载后定位在第一个输入框上          
			$('#'+tabArray[0]).focus();            
			$('.text,.select').not($('input[type="hidden"],input:disabled')).keydown(function(e) { 
				//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
				var event = $.event.fix(e);
				//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
				if (event.keyCode == 13) {
					if($('#subflag').val()=='0'){
					var index = $.inArray($.trim($(event.target).attr("id")), tabArray);//
					if(index == tabArray.length-1){
						if(validate._submitValidate()){
							$('#subflag').val('1');
							$('#holidayForm').submit(); //切换到最后一个控件后，执行新增
						}
						return;
					}
					$('#'+tabArray[index]).blur();
					index=index+1;
					if(index==tabArray.length) index=0;
					$('#'+tabArray[index]).focus();	
					}
				}
			}); 
		});
		</script>
	</body>
</html>