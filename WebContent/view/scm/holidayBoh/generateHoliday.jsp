
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="bulk" /><fmt:message key="insert" /><fmt:message key="holidays" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.form{
				padding-top:60px;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="holidayForm" method="post" action="<%=path %>/holidayBoh/saveByGenerate.do">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="years" /></div>
					<div class="form-input"><input type="text" id="vyear" name="vyear" class="text"/></div>
				</div>
			</form>
		</div>
		<input type="hidden" id="subflag" value="0" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'vyear',
					validateType:['canNull','num1','maxValue','minValue'],
					param:['F','F','2099','1900'],
					error:['<fmt:message key="years" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="only_digits" />！','<fmt:message key="years" /><fmt:message key="only_between" />【1990，2099】！','<fmt:message key="years" /><fmt:message key="only_between" />【1990，2099】！']
				}]
			});

			$('#dholidaydate').bind('click',function(){
				new WdatePicker();
			});
		});
		</script>
	</body>
</html>