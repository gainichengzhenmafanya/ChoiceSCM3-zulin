<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="holidays" /><fmt:message key="set_up_the" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
				margin-bottom: 28px;
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="listForm" action="<%=path%>/holidayBoh/list.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td style="width:30px;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td><span style="width:45px;"><fmt:message key="years" /></span></td>
									<td><span style="width:75px;"><fmt:message key="date" /></span></td>
									<td><span style="width:150px;"><fmt:message key="holiday" /><fmt:message key="name" /></span></td>
									<td><span style="width:50px;"><fmt:message key="enable_state" /></span></td>
									<td><span style="width:50px;"><fmt:message key="holiday" /><fmt:message key="level" /></span></td>
									<td><span style="width:150px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="holiday" items="${holidayList}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${holiday.pk_holidaydefine }' />" value="<c:out value='${holiday.pk_holidaydefine }' />"/>
										</td>
										<td><span style="width:45px;">${holiday.vyear}</span></td>
										<td><span style="width:75px;">${holiday.dholidaydate}</span></td>
										<td><span style="width:150px;">${holiday.vholidayname}</span></td>
										<td align="center">
											<span style="width:50px;">
												<c:if test="${holiday.enablestate == ''}"></c:if>
												<c:if test="${holiday.enablestate == '1'}"><fmt:message key="not_enabled" /></c:if>
												<c:if test="${holiday.enablestate == '2'}"><fmt:message key="have_enabled" /></c:if>
												<c:if test="${holiday.enablestate == '3'}"><fmt:message key="stop_enabled" /></c:if>
											</span>										
										</td>
										<td align="center">
											<span style="width:50px;">
												<c:if test="${holiday.vholidaygrade == ''}"></c:if>
												<c:if test="${holiday.vholidaygrade == '1'}"><fmt:message key="a" /></c:if>
												<c:if test="${holiday.vholidaygrade == '2'}"><fmt:message key="b" /></c:if>
												<c:if test="${holiday.vholidaygrade == '3'}"><fmt:message key="c" /></c:if>
												<c:if test="${holiday.vholidaygrade == '4'}"><fmt:message key="d" /></c:if>
												<c:if test="${holiday.vholidaygrade == '5'}"><fmt:message key="e" /></c:if>
												<c:if test="${holiday.vholidaygrade == '6'}"><fmt:message key="f" /></c:if>
											</span>										
										</td>
										<td><span style="width:150px;">${holiday.vmemo}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>			
				</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key="years" />/<fmt:message key="holidays" /><fmt:message key="name" />：</td>
							<td><input type="text" id="vholidayname" name="vholidayname"  class="text" value="${holiday.vholidayname }"/></td>
							<td class="c-left"><fmt:message key="enable_state" />：</td>
							<td>
								<select id="enablestate" name="enablestate"
								style="height: 22px; margin-top: 3px; border: 1px solid #999999;">
									<option value=""><fmt:message key="all" /></option>
<!-- 									<option value="1"><fmt:message key="not_enabled" /></option> -->
<!-- 									<option value="2" selected="selected"><fmt:message key="have_enabled" /></option> -->
<!-- 									<option value="3"><fmt:message key="stop_enabled" /></option> -->
										<option value="1"  <c:if test="${holiday.enablestate=='1'}"> selected="selected"</c:if>  ><fmt:message key="not_enabled" /></option>
										<option value="2"  <c:if test="${holiday.enablestate=='2'}"> selected="selected"</c:if>  ><fmt:message key="have_enabled" /></option>
										<option value="3"  <c:if test="${holiday.enablestate=='3'}"> selected="selected"</c:if>  ><fmt:message key="stop_enabled" /></option>
								</select>
							</td>
						</tr>
					</table>
				</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
	       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
			</div>
		</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript">
		
			function clearQueryForm(){
				$('#queryForm input[type=text]').val('');
				$('#queryForm option:nth-child(1)').attr('selected','selected');
			}
			function pageReload(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
				$('.search-div').hide();
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveHoliday();
							}
						},{
							text: '<fmt:message key="bulk" /><fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								generateHoliday();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateHoliday();
							}
						},{
							text: '<fmt:message key="enable" />',
							title: '<fmt:message key="enable" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								enable();
							}
						},{
							text: '<fmt:message key="disable1" />',
							title: '<fmt:message key="disable1" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								disable();
							}
						},"-",{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$("#vholidayname").focus();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				//点击enter按键，执行查询条件
				$('#vholidayname').keydown(function(e){
					if(e.keyCode==13){
						$('.search-div').hide();
						$('#listForm').submit();
					}
				});
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				$('#logindate').bind('click',function(){
					new WdatePicker();
				});
				$('#logindateEnd').bind('click',function(){
					new WdatePicker();
				});

				//$('.grid').height($(document.body).height() - $('.tool').outerHeight() - $('.condition').outerHeight());
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			//双击弹出<fmt:message key="update" />
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				$(":checkbox").attr("checked", false);
				$(this).find(':checkbox').attr("checked", true);
				updateHoliday();
				if($(this).hasClass("show-firm-row"))return;
				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
				$(this).addClass("show-firm-row");
			 });
			function saveHoliday(){
				$('body').window({
					id: 'window_saverole',
					title: '<fmt:message key="insert" /><fmt:message key="holidays" />',
					content: '<iframe id="saveHolidayFrame" frameborder="0" src="<%=path%>/holidayBoh/add.do"></iframe>',
					width: '370px',
					height: '300px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="holidays" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveHolidayFrame')&&window.document.getElementById("saveHolidayFrame").contentWindow.validate._submitValidate()){
										var value=window.document.getElementById("saveHolidayFrame").contentWindow.document.getElementById('subflag').value;
										if(value==0){
											window.document.getElementById("saveHolidayFrame").contentWindow.document.getElementById('subflag').value="1";
											submitFrameForm('saveHolidayFrame','holidayForm');
										}
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//<fmt:message key="bulk" />添加
			function generateHoliday(){
				$('body').window({
					id: 'window_saverole',
					title: '<fmt:message key="bulk" /><fmt:message key="insert" />',
					content: '<iframe id="saveHolidayFrame" name="saveHolidayFrame" frameborder="0" src="<%=path%>/holidayBoh/generate.do"></iframe>',
					width: '370px',
					height: '300px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="holidays" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveHolidayFrame')&&window.document.getElementById("saveHolidayFrame").contentWindow.validate._submitValidate()){
										var value=window.document.getElementById("saveHolidayFrame").contentWindow.document.getElementById('subflag').value;
										if(value==0){
											window.document.getElementById("saveHolidayFrame").contentWindow.document.getElementById('subflag').value="1";
											submitFrameForm('saveHolidayFrame','holidayForm');
										}
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function updateHoliday(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					
					$('body').window({
						title: '<fmt:message key="update" /><fmt:message key="holidays" /><fmt:message key="information" />',
						content: '<iframe id="updateHolidayFrame" frameborder="0" src="<%=path%>/holidayBoh/update.do?pk_holidaydefine='+chkValue+'"></iframe>',
						width: '370px',
						height: '300px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="holidays" /><fmt:message key="information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateHolidayFrame')&&window.document.getElementById("updateHolidayFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('updateHolidayFrame','holidayForm');
										}
										
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alerterror('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
				
			}
			
		
			function enable(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm('<fmt:message key="are_you_want_to" /><fmt:message key="enable" />？',function(){
						var chkValue = [];
						var codeValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							codeValue.push($(this).closest('td').next().next().find('span').text());
						});
						
						var action = '<%=path%>/holidayBoh/enable.do?enablestate=2&pk_holidaydefine='+chkValue.join(",")+'&dholidaydate='+codeValue.join(",");
						
						$('body').window({
							title: '<fmt:message key="enable" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror("<fmt:message key='select_you_want_to_enable' />");
					return ;
				}
			}
			function disable(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm('<fmt:message key="are_you_want_to" /><fmt:message key="disable1" />？',function(){
						var chkValue = [];
						var codeValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							codeValue.push($(this).closest('td').next().next().find('span').text());
						});
						
						var action = '<%=path%>/holidayBoh/enable.do?enablestate=3&pk_holidaydefine='+chkValue.join(",")+'&dholidaydate='+codeValue.join(",");
						
						$('body').window({
							title: '<fmt:message key="disable1" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror("<fmt:message key='select_you_want_to_disable' />！");
					return ;
				}
			}
			
			function pageReload(){
		    	window.location.href = '<%=path%>/holidayBoh/list.do';
		    }

		</script>
	</body>
</html>