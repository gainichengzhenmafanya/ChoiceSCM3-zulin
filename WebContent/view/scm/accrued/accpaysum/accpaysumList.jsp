<%--
  应付款汇总
  User: mc
  Date: 14-12-27
  Time: 上午10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>spprice Info</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
    <style type="text/css">
        .tool {
            position: relative;
            height: 27px;
        }
        .page{
            margin-bottom: 25px;
        }
        .condition {
            position: relative;
            top: 1px;
            height: 31px;
            line-height: 31px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<form action="<%=path%>/accrued/findaccpaysum.do" id="listForm" name="listForm" method="post">
    <div class="bj_head">
        <div class="form-line">
            <div class="form-label" style="width: 50px;"><fmt:message key="startdate" />：</div>
            <div class="form-input">
                <input type="text" class="Wdate text" name="bdat" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd',isShowClear:false})" id="bdat" value="${accpaysum.bdat }"/>
            </div>
            <div class="form-label"><fmt:message key="positions" />：</div>
            <div class="form-input">
                <input type="text" style="vertical-align: baseline;" id="positn_name" value="${accpaysum.positn_name}" name="positn_name" class="text"/>
                <input type="hidden" id="positn" name="positn" class="text"/>
                <img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt="<fmt:message key="positions"/>">
            </div>
            <div style="margin-left: 30px;" class="form-input">
                <input type="checkbox" name="isCheck" <c:if test="${accpaysum.isCheck=='1'}">checked="checked"</c:if> value="1"><fmt:message key="only_audit" />
            </div>
        </div>
        <div class="form-line">
            <div class="form-label" style="width: 50px;"><fmt:message key="enddate" />：</div>
            <div class="form-input">
                <input type="text" class="Wdate text" name="edat" id="edat"  onfocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd',isShowClear:false})" value="${accpaysum.edat }"/>
            </div>
            <div class="form-label"><fmt:message key="suppliers" />：</div>
            <div class="form-input">
                <input type="text" id="deliverDes" name="deliverDes" value="${accpaysum.deliverDes}" style="vertical-align: baseline;" class="text"/>
                <input type="hidden" id="deliverCode" name="deliverCode" value="${accpaysum.deliverCode}" style="vertical-align: baseline;" class="text"/>
                <img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt="<fmt:message key="branche"/>">
            </div>
            <div style="margin-left: 30px;" class="form-input">
                <input type="checkbox" name="isDat" <c:if test="${accpaysum.isDat=='1'}">checked="checked"</c:if> value="1"><fmt:message key="date" />
            </div>
        </div>
    </div>
    <div class="grid" >
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td rowspan="2"><span style="width:40px;"></span></td>
                        <td rowspan="2"><span style="width:80px;"><fmt:message key="suppliers_coding" /></span></td>
                        <td rowspan="2"><span style="width:180px;"><fmt:message key="suppliers" /></span></td>
                        <td colspan="4"><span style="width:320px;"><fmt:message key="At_the_beginning_did_not_review" /></span></td>
                        <td colspan="2"><span style="width:160px;"><fmt:message key="This_issue_has_been_audit" /></span></td>
                        <td colspan="2"><span style="width:160px;"><fmt:message key="This_period_is_not_audit" /></span></td>
                        <td rowspan="2"><span style="width:100px;"><fmt:message key="This_issue_has_been_invoice" /></span></td>
                        <td rowspan="2"><span style="width:100px;"><fmt:message key="Total_unpaid" /></span></td>
                        <td rowspan="2"><span style="width:100px;"><fmt:message key="The_total_invoice_is_still_outstanding" /></span></td>
                    </tr>
                    <tr>
                        <td><span style="width:80px;"><fmt:message key="The_total_stock" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="The_total_payment" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="Not_paying" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="Not_invoice" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="Replenish_stock" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="Payment_has_been" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="The_total_stock" /></span></td>
                        <td><span style="width:80px;"><fmt:message key="Payment_has_been" /></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${accpaysumList}" var="accpaysum" varStatus="vars">
                        <tr>
                            <td><span style="width:40px;text-align: center;" title="${vars.index+1}">${vars.index+1}</span></td>
                            <td title="${accpaysum.code}"><span style="width:80px;">${accpaysum.code}</span></td>
                            <td title="${accpaysum.des}"><span style="width:180px;">${accpaysum.des}</span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.beginTotalamt}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.beginTotalamt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.beginTotalamt}" pattern="0.00" />
                            </span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.beginPay}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.beginPay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.beginPay}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.beginAmt}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.beginAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.beginAmt}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.beginAmtpay}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.beginAmtpay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.beginAmtpay}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.thisTotalamt}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.thisTotalamt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.thisTotalamt}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.thisPay}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.thisPay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.thisPay}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.thisTotalamtUnaud}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.thisTotalamtUnaud==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.thisTotalamtUnaud}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.thisPayUnaud}" pattern="0.00" />"><span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.thisPayUnaud==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.thisPayUnaud}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.thisInvpay}" pattern="0.00" />"><span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.thisInvpay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.thisInvpay}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.totalAmt}" pattern="0.00" />"><span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.totalAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.totalAmt}" pattern="0.00" /></span></td>
                            <td title="<fmt:formatNumber value="${accpaysum.totalInvAmt}" pattern="0.00" />"><span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.totalInvAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.totalInvAmt}" pattern="0.00" /></span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="table-foot" style="border-top: 1px solid #999;">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <c:forEach items="${accpaysumList}" var="accpaysum" begin="0" end="0">
                    <tr>
                        <td style="border:0px;"><span style="width:40px;"></span></td>
                        <td style="border:0px;"><span style="width:80px;"></span></td>
                        <td style="border:0px;"><span style="width:180px;"><fmt:message key="total"/></span></td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_beginTotalamt}" pattern="0.00"/>">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_beginTotalamt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_beginTotalamt}" pattern="0.00"/>
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_beginPay}" pattern="0.00"/>">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_beginPay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_beginPay}" pattern="0.00"/>
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_beginAmt}" pattern="0.00" />">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_beginAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_beginAmt}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_beginAmtpay}" pattern="0.00" />">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_beginAmtpay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_beginAmtpay}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_thisTotalamt}" pattern="0.00" />">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_thisTotalamt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_thisTotalamt}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_thisPay}" pattern="0.00" />">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_thisPay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_thisPay}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_thisTotalamtUnaud}" pattern="0.00"/>">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_thisTotalamtUnaud==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_thisTotalamtUnaud}" pattern="0.00"/>
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_thisPayUnaud}" pattern="0.00"/>">
                            <span style="width:80px;text-align: right;">
                                <c:if test="${accpaysum.t_thisPayUnaud==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_thisPayUnaud}" pattern="0.00"/>
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_thisInvpay}" pattern="0.00" />">
                            <span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.t_thisInvpay==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_thisInvpay}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_totalAmt}" pattern="0.00" />">
                            <span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.t_totalAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_totalAmt}" pattern="0.00" />
                            </span>
                        </td>
                        <td style="border:0px;" title="<fmt:formatNumber value="${accpaysum.t_totalInvAmt}" pattern="0.00" />">
                            <span style="width:100px;text-align: right;">
                                <c:if test="${accpaysum.t_totalInvAmt==null}">
                                    0.00
                                </c:if>
                                <fmt:formatNumber value="${accpaysum.t_totalInvAmt}" pattern="0.00" />
                            </span>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <page:page form="listForm" page="${pageobj}"></page:page>
    <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
    <input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
</form>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.jeditable.mini.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js.js"></script>
<script type="text/javascript">

    var t;
    function ajaxSearch(key){
        if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
            return; //回车 ，上下 时不执行
        }
        window.clearTimeout(t);
        t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
    }
    function pageReload(){
        $('#listForm').submit();
    }
    $(document).ready(function(){
        focus() ;//页面获得焦点
        if($("#bdat").val()=='' && $("#edat").val()==''){
            $("#bdat,#edat").htmlUtils("setDate","now");
        }
        $(document).bind('keydown',function(e){
            if(e.keyCode==27){
                $('#quit').click();
            }
        });
        var tool = $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="select" />',
                title: '<fmt:message key="select" />',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    position: ['0px','-40px']
                },
                handler: function(){
                    //$('.search-div').slideToggle(100);
                    $("#deliver").val($('select').val());
                    $('#listForm').submit();
                }
            },{
                text: '<fmt:message key="export" />',
                title: '<fmt:message key="export" />',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    position: ['-160px','-100px']
                },
                handler: function(){
                    exp();
                }
            },{
                text: '<fmt:message key="quit" />',
                title: '<fmt:message key="quit" />',
                id:'quit',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    position: ['-160px','-100px']
                },
                handler: function(){
                    invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                }
            }]
        });

        $("#edat,#bdat").bind('click',function(){
            new WdatePicker();
        });
        setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
        setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');				//计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );

        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            if ($(this).hasClass("bgBlue")) {
                $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
            }else{
                $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
            }
        });

        $("#seachPositn").click(function(){
			chooseStoreSCM({
				basePath:'<%=path%>',
				width:600,
				firmId:$("#positn").val(),
				single:false,
				tagName:'positn_name',
				tagId:'positn',
				title:'<fmt:message key="positions"/>'
			});
		});
        
        $("#seachDeliver").bind('click',function(){
            if (!!!top.customWindow) {
                var defaultCode = $('#deliverCode').val();
                var defaultName = $('#deliverDes').val();
                var offset = "";
                top.cust('<fmt:message key ="please_select_suppliers" />', encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode=' + defaultCode + '&defaultName=' + encodeURI(defaultName)), offset, $('#deliverDes'), $('#deliverCode'), '900', '500', 'isNull');
            }
        });
    });
    var exp=function(){
        $("#wait2").val("NO");
        var expAction = "<%=path%>/accrued/exportAccpaysum.do";
        $('#listForm').attr('action',expAction);
        $('#listForm').submit();
        $('#listForm').attr('target','');
        $('#listForm').attr('action','<%=path%>/accrued/findaccpaysum.do');
        $("#wait2").val("");
    }
</script>
</body>
</html>
