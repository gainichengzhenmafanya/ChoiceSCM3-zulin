<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>付款记录</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <style type="text/css">

    </style>
</head>
<body>
<!-- <div class="tool"></div> -->
<div class="grid">
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td ><span style="width:50px;"><fmt:message key="serial_number1"/></span></td>
                <td ><span style="width:80px;"><fmt:message key="date"/></span></td>
                <td ><span style="width:80px;"><fmt:message key="The_payment_amount"/></span></td>
                <td ><span style="width:120px;"><fmt:message key="explain"/></span></td>
                <td ><span style="width:80px;"><fmt:message key="operator"/></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <c:forEach var="folio" items="${brasetitemList }" varStatus="status">
                <tr>
                    <td title="${folio.id}"><span style="width:50px;text-align:center;">${folio.id }</span></td>
                    <td title="<fmt:formatDate value="${folio.dat }" type="date"/>"><span style="width:80px;"><fmt:formatDate value="${folio.dat }" type="date"/></span></td>
                    <td style="text-align: right;" title="<fmt:formatNumber value="${folio.amtpay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${folio.amtpay}" type="currency" pattern="0.00"/></span></td>
                    <td title="${folio.memo}"><span style="width:120px;">${folio.memo }</span></td>
                    <td title="${folio.madeby}"><span style="width:80px;">${folio.madeby }</span></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="table-foot" style="border-top: 1px solid #999;">
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <c:forEach items="${brasetitemList}" var="folio" begin="0" end="0">
                    <tr>
                        <td style="border: 0px;" title="<fmt:message key="total" />"><span style="width:50px;text-align:center;"><fmt:message key="total" /></span></td>
                        <td style="border: 0px;text-align: right;"><span style="width:80px;"></span></td>
                        <td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${folio.t_amtpay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${folio.t_amtpay}" type="currency" pattern="0.00"/></span></td>
                        <td style="border: 0px;text-align: right;"><span style="width:120px;"></span></td>
                        <td style="border: 0px;text-align: right;"><span style="width:80px;"></span></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');
        //自动实现滚动条
        setElementHeight('.grid',[],$(document.body),2);	//计算.grid的高度
        setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');	//计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
    });
</script>
</body>
</html>