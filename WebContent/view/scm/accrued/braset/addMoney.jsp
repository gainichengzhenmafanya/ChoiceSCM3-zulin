<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加期初金额</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
			
			</style>
	</head>
	<body>
    <div class="toolbar"></div>
		<form id="listForm" action="<%=path%>/accrued/toaddmoney.do" method="post">
			<br/>
			<br/>
			<input type="hidden" name="firm.firm" value="${braset.firm }"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="date_of_charge" /></div>
				<div class="form-input"><input type="text" id="dat" name="maded" class="Wdate text" onclick="new WdatePicker();"/></div>
			</div>
			<br/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="initial_position" /></div>
				<div class="form-input">
					<input type="text" readonly="readonly" id="positn_name"/>
					<input type="hidden"  id="positn"  name="positn.code"/>
                    <img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
			</div>
            <br/>
			<div class="form-line">
				<div class="form-label"><fmt:message key ="no_knot_amount" /></div>
				<div class="form-input">
					<input type="text"  id="pay"  name="totalamt"/>
				</div>
			</div>
			<br/>
			<div class="form-line">
				<div class="form-label"><fmt:message key ="scm_Remarks" /></div>
				<div class="form-input">
					<input type="text" name="memo" id="memo" value="<fmt:message key ="initial_period_of_time" />"/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			var validate;
            var mark=true;
			$(document).ready(function(){
                if($("#dat").val()==''){
                    $("#dat").htmlUtils("setDate","now");
                }
                $("#seachPositn").click(function(){
    				chooseStoreSCM({
    					basePath:'<%=path%>',
    					width:600,
    					firmId:$("#positn").val(),
    					single:false,
    					tagName:'positn_name',
    					tagId:'positn',
    					title:'<fmt:message key="please_select_positions"/>'
    				});
    			});
                $(".toolbar").toolbar({
                    items: [{
                        text: '<fmt:message key ="enter" />',
                        title: '<fmt:message key ="enter" />',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            position: ['-80px','-0px']
                        },
                        handler: function(){
                            if(mark&&validate._submitValidate()){
                                $("#listForm").submit()
                                mark=false;
                            }
                        }
                    },{
                        text: '<fmt:message key ="cancel" />',
                        title: '<fmt:message key ="cancel" />',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            position: ['-160px','-100px']
                        },
                        handler: function(){
                            $(".close",parent.document).click()
                        }
                    }]
                })
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'dat',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="date" /><fmt:message key ="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'pay',
						validateType:['canNull','maxValue','num2'],
						param:['F',10000000,'F'],
						error:['<fmt:message key ="amount" /><fmt:message key ="cannot_be_empty" />','<fmt:message key ="maximal_value" />10000000','<fmt:message key ="please_fill_in_figures" />！']
					},{
						type:'text',
						validateObj:'memo',
						validateType:['maxLength'],
						param:[30],
						error:['<fmt:message key ="the_maximum_length" />30']
					},{
                        type:'text',
                        validateObj:'positn_name',
                        validateType:['canNull'],
                        param:['F'],
                        error:['<fmt:message key ="positions" /><fmt:message key ="cannot_be_empty" />！']
                    }]
				});
			});
		</script>
	</body>
</html>