<%--
  分店结算
  User: mc
  Date: 14-12-27
  Time: 下午1:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>分店结算</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <style type="text/css">
        .page{margin-bottom: 230px;}
        .search{
            margin-top:-2px;
            cursor: pointer;
        }
        .form-line .form-label{
            width:80px;
        }
    </style>
</head>
<body>
<div class="tool"></div>
<form id="listForm" action="<%=path%>/accrued/findbraset.do" method="post" target="">
    <div class="bj_head">
        <div class="form-line">
            <div class="form-label"><fmt:message key="startdate"/>：</div>
            <div class="form-input"><input type="text" id="bdat" name="bdat" value="${braset.bdat}" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>

            <div class="form-label"><fmt:message key="positions"/>：</div>
            <div class="form-input">
                <input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value="${braset.positn_name}"/>
                <input type="hidden" id="positn" name="positn" value="${braset.positn}"/>
                <img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
            </div>

            <div class="form-label"><fmt:message key="suppliers"/>：</div>
            <div class="form-input">
                <input type="text" id="deliverDes" name="deliver_name" readonly="readonly" value="${braset.deliver_name}"/>
                <input type="hidden" id="deliverCode" name="deliver" value="${braset.deliver}"/>
                <img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
            </div>
            <div class="form-label"><fmt:message key ="clearing" />：</div>
            <div class="form-input">
                <input type="radio" name="folio" value="0" checked="checked"/><fmt:message key ="all" />
                <input type="radio" name="folio" value="1" <c:if test="${braset.folio eq '1'}"> checked="checked" </c:if>/><fmt:message key ="no_checkout" />
                <input type="radio" name="folio" value="2" <c:if test="${braset.folio eq '2'}"> checked="checked" </c:if>/><fmt:message key ="ed_checkout" />
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="enddate"/>：</div>
            <div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="${braset.edat}" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>

            <div class="form-label"><fmt:message key="branche"/>：</div>
            <div class="form-input" style="width:170px;">
                <input type="text" id="branche_name" name="firm_name" readonly="readonly" value="${braset.firm_name}"/>
                <input type="hidden" id="branche" name="firm" value="${braset.firm}"/>
                <img id="seachBranche" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
            </div>

            <div class="form-label" style="width:20px;"></div>
            <div class="form-input">
                <input type="radio" name="inout" value="0" checked="checked"/><fmt:message key ="all" />
                <input type="radio" name="inout" value="1" <c:if test="${braset.inout eq '出库' or braset.inout eq '1'}"> checked="checked" </c:if>/><fmt:message key ="only_the_library" />
                <input type="radio" name="inout" value="2" <c:if test="${braset.inout eq '直发' or braset.inout eq '2'}"> checked="checked" </c:if>/><fmt:message key ="only_direct_dial" />
            </div>

            <div class="form-label" style="width:120px;"><fmt:message key ="payments" />：</div>
            <div class="form-input">
                <input type="radio" name="bill" value="0" checked="checked"/><fmt:message key ="all" />
                <input type="radio" name="bill" value="1" <c:if test="${braset.bill eq '1'}"> checked="checked" </c:if>/><fmt:message key ="Not_paying" />
                <input type="radio" name="bill" value="2" <c:if test="${braset.bill eq '2'}"> checked="checked" </c:if>/><fmt:message key ="Payment_has_been" />
            </div>

            <div class="form-label" style="width:20px"></div>
            <div class="form-input" style="width:80px;">
                <input type="checkbox" name="partPay" value="1" <c:if test="${braset.partPay eq '1'}"> checked="checked" </c:if>/><fmt:message key ="partial_payment" />
            </div>
        </div>
    </div>
    <div class="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
                    <td rowspan="2" ><span style="width: 25px;"><input type="checkbox" id="chkAll" value=""/></span></td>
                    <td rowspan="2" ><span style="width:80px;"><fmt:message key="branches_encoding"/></span></td>
                    <td rowspan="2" ><span style="width:130px;"><fmt:message key="branches_name"/></span></td>
                    <td rowspan="2" ><span style="width:100px;"><fmt:message key="positions"/></span></td>
                    <td rowspan="2" ><span style="width:120px;"><fmt:message key="amount"/></span></td>
                    <td rowspan="2" ><span style="width:100px;"><fmt:message key="document_number"/></span></td>
                    <td rowspan="2" ><span style="width:100px;"><fmt:message key="document_types"/></span></td>
                    <td colspan="5" ><fmt:message key="clearing"/></td>
                </tr>
                <tr>
                    <td ><span style="width:80px;"><fmt:message key="Payment_has_been"/></span></td>
                    <td ><span style="width:80px;"><fmt:message key="Not_paying"/></span></td>
                    <td style="display: none;"><span style="width:80px;"><fmt:message key="This_payment"/></span></td>
                    <td ><span style="width:80px;"><fmt:message key="Has_ended"/></span></td>
                </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <c:forEach var="braset" items="${brasetList }" varStatus="status">
                    <tr>
                        <td class="num" >
                            <span style="width: 25px;">${status.index+1}</span>
                        </td>
                        <td><span style="width:25px;text-align:center;"><input type="checkbox" name="idList" id="chk_${braset.chkinno}" value="${braset.chkinno}"/></span></td>
                        <td ><span style="width:80px;" title="${braset.firm}">${braset.firm}</span></td>
                        <td ><span style="width:130px;" title="${braset.firm_name}">${braset.firm_name}</span></td>
                        <td ><span style="width:100px;" title="${braset.positn_name}">${braset.positn_name}</span></td>
                        <td ><span style="text-align:right;width:120px;" title="<fmt:formatNumber value="${braset.amt}" pattern="0.00"/>"><fmt:formatNumber value="${braset.amt}" pattern="0.00"/></span></td>
                        <td ><span style="width:100px;" title="${braset.vouno}">${braset.vouno}</span></td>
                        <td ><span style="width:100px;" data="${braset.billTyp}">${braset.typ}</span></td>
                        <td ><span style="text-align:right; width:80px;" title="<fmt:formatNumber value="${braset.pay}" pattern="0.00"/>"><fmt:formatNumber value="${braset.pay}" pattern="0.00"/></span></td>
                        <td ><span style="text-align:right; width:80px;" title="<fmt:formatNumber value="${braset.amtye}" pattern="0.00"/>"><fmt:formatNumber value="${braset.amtye}" pattern="0.00"/></span></td>
                        <td style="display: none;"><span style="text-align:right; width:80px;">0.00</span></td>
                        <td>
                            <span style="width:80px;text-align:center" title="${braset.folio}">
                                <c:choose>
                                    <c:when test="${braset.folio==1 }">
                                        <img src="<%=path%>/image/themes/icons/ok.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="<%=path%>/image/themes/icons/no.png"/>
                                    </c:otherwise>
                                </c:choose>
                            </span>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="table-foot" style="border-top: 1px solid #999;">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <c:forEach items="${brasetList}" var="braset" begin="0" end="0">
                    <tr>
                        <td style="border: 0px;" class="num" >
                            <span style="width: 25px;"></span>
                        </td>
                        <td style="border: 0px;"><span style="width:25px;text-align:center;"></span></td>
                        <td style="border: 0px;"><span style="width:80px;"><fmt:message key="total"/></span></td>
                        <td style="border: 0px;"><span style="width:130px;"></span></td>
                        <td style="border: 0px;"><span style="width:100px;"></span></td>
                        <td style="border: 0px;text-align:right;" title="<fmt:formatNumber value="${braset.t_amt }" type="currency" pattern="0.00"/>"><span style="width:120px;text-align: right;"><fmt:formatNumber value="${braset.t_amt }" type="currency" pattern="0.00"/></span></td>
                        <td style="border: 0px;"><span style="width:100px;"></span></td>
                        <td style="border: 0px;"><span style="width:100px;"></span></td>
                        <td style="border: 0px;text-align:right;" title="<fmt:formatNumber value="${braset.t_pay}" type="currency" pattern="0.00"/>"><span style="text-align:right;width:80px;"><fmt:formatNumber value="${braset.t_pay}" type="currency" pattern="0.00"/></span></td>
                        <td style="border: 0px;text-align:right;" title="<fmt:formatNumber value="${braset.t_Amtye}" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="${braset.t_Amtye}" type="currency" pattern="0.00"/></span></td>
                        <td style="border: 0px;text-align:right;display: none;" ><span style="width:80px;text-align:right"><fmt:formatNumber value="0" type="currency" pattern="0.00"/></span></td>
                        <td style="border: 0px;"><span style="width:80px;text-align:center"></span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <page:page form="listForm" page="${pageobj}"></page:page>
    <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
    <input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
</form>
<div id="littleWin" style="height: 200px;width: 100%;">
    <iframe style="margin-top:10px;border-right:1px solid #999999;" id="payFrame" src="" frameborder="0" ></iframe>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    if($("#bdat").val()=='' && $("#edat").val()==''){
        $("#bdat,#edat").htmlUtils("setDate","now");
    }
    //按钮快捷键
    $(document).bind('keyup',function(e){
        if(e.keyCode==27){
            $('#brasetQuit').click();
        }
        if(e.altKey ==false)return;
        switch (e.keyCode){
            case 70: $('#autoId-button-101').click(); break;
            case 65: $('#autoId-button-102').click(); break;
            case 80: $('#autoId-button-103').click(); break;
        }
    });
    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
    $('.grid').find('.table-body').find('tr').hover(
            function(){
                $(this).addClass('tr-over');
            },
            function(){
                $(this).removeClass('tr-over');
            }
    );
    $('.grid').find('.table-body').find('tr').find('td').not('td:eq(1)').live('click',function(){
        var chk = $(this).find(':checkbox');
        if(chk.get(0).checked){
            chk.get(0).checked = false;
        }else{
            chk.get(0).checked = true;
        }
    });
    $('.grid').find('.table-body').find('tr').live('dblclick',function(){
        var chk = $(this).find(':checkbox');
        var tag=$(this).find("td:eq(7) span").attr("data");
        if(tag==1&&tag=="1") {
            var parameters = {"vouno": $(this).find("td:eq(6)").find("span").text(), "chkoutno": chk.val(), "dat": $("#bdat").val(), "edat": $("#edat").val()};
            openTag("CkMingxiChaxun", "<fmt:message key ="query_storehouse" />", "<%=path%>/CkMingxiChaxun/toCkBill.do", parameters);
        }else{
            var parameters = {"vouno":$(this).find("td:eq(6)").find("span").text(),"chktag":2,
                "chkinno":chk.val(),"dat":$("#edat").val(),"maded":$("#bdat").val()};
            openTag("CkMingxiChaxun", "<fmt:message key ="Inout_order" />", "<%=path%>/RkMingxiChaxun/toRkBill.do", parameters);
        }
    });
    $('.tool').toolbar({
        items: [{
            text: '<fmt:message key="select"/>',
            title: '<fmt:message key="select"/>',
            useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                position: ['-20px','-40px']
            },
            handler: function(){
                if($.trim($("#branche").val())==""){
                    alert('<fmt:message key="please_select_branche" />')
                }else {
                    $('#listForm').submit();
                }
            }
        },{
			text: '<fmt:message key="add"/> <fmt:message key="beginning_of_period"/>',
			title: '<fmt:message key="add"/> <fmt:message key="beginning_of_period"/>',
            useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                position: ['-60px','-20px']
            },
            handler: function(){
                addStartMoney();
            }
        },'-',{
            text: '<fmt:message key ="payments" />',
            title: '<fmt:message key ="payments" />',
            useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                position: ['-20px','-40px']
            },
            handler: function(){
                payMoney();
            }
        },{
            text: '<fmt:message key ="audit_check" />',
            title: '<fmt:message key ="audit_check" />',
            useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                position: ['-20px','-40px']
            },
            handler: function(){
                checkBill();
            }
        },'-',{
            text: '<fmt:message key="quit" />',
            title: '<fmt:message key="quit"/>',
            id:'brasetQuit',
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                position: ['-160px','-100px']
            },
            handler: function(){
                invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
            }
        }]

    });
    //自动实现滚动条
    setElementHeight('.grid',['.tool','#littleWin','.bj_head'],$(document.body),60);	//计算.grid的高度
    setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');	//计算.table-body的高度
    loadGrid();//  自动计算滚动条的js方法
    $('.grid').find('.table-body').find('tr').hover(
            function(){
                $(this).addClass('tr-over');
            },
            function(){
                $(this).removeClass('tr-over');
            }
    );

    //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
    $('.grid').find('.table-body').find('tr').live("click", function () {
        if ($(this).hasClass("bgBlue")) {
            $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
        }else{
            $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
        }
    });
    
    $("#seachBranche").click(function(){
		chooseStoreSCM({
			basePath:'<%=path%>',
			width:600,
			firmId:$("#branche").val(),
			single:true,
			tagName:'branche_name',
			tagId:'branche',
			typn:'1203',
			title:'<fmt:message key="branche"/>'
		});
	});

    
    $('#seachDeliver').bind('click.custom',function(e){
        if(!!!top.customWindow){
            var defaultCode = $('#deliverCode').val();
            var defaultName = $('#deliverDes').val();
            var offset = getOffset('bdat');
            top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
        }
    });
    $('#seachTyp').bind('click.custom',function(e){
        if(!!!top.customWindow){
            var defaultCode = $('#typCode').val();
            var defaultName = $('#typDes').val();
            var offset = getOffset('positnCode');
            top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
        }
    });
    
    $("#seachPositn").click(function(){
		chooseStoreSCM({
			basePath:'<%=path%>',
			width:600,
			firmId:$("#positn").val(),
			single:false,
			tagName:'positn_name',
			tagId:'positn',
			title:'<fmt:message key="positions"/>'
		});
	});
    
    //加载付款和发票数据
    loadFrameData();
});
//加载付款记录和发票数据
function loadFrameData(){
    var str = $('#listForm').serialize();
    var payAction = "<%=path%>/accrued/findbrasetitem.do?"+str;
    $("#payFrame").attr("src",payAction);
}
//审核结账
function checkBill(){
    var checkboxList = $('.grid').find('.table-body').find(':checkbox');
    if(checkboxList && checkboxList.filter(':checked').size() > 0){
        if(confirm('<fmt:message key="To_determine_the_audit_check" />?')){
            var chkoutValue = [];//出库单
            var chkinValue = [];//直发单
            var vouno = '';
            var flag = true;
            $.each(checkboxList.filter(':checked'),function(i){
            	if('1' == $(this).parents("tr").find("td:eq(11)").find("span").attr('title')){
            		vouno = $(this).parents("tr").find("td:eq(6)").find("span").text();
            		flag = false;
            		return false;
            	}
                if($.trim($(this).parents("tr").find("td:eq(7)").find("span").attr("data"))=='1') {
                    chkoutValue.push($(this).val());
                }else{
                    chkinValue.push($(this).val());
                }
            });
            if(!flag){
            	alert('<fmt:message key ="document_number" />:['+vouno+']<fmt:message key ="te_data_have_been_audited" />!');
            	return;
            }
            detailWin = $('body').window({
                id: 'checkedChkstom',
                title: '<fmt:message key ="audit_check" />',
                content: '<iframe id="checkedChkstomFrame" frameborder="0" src="<%=path%>/accrued/checkedbill.do?invalue='+chkinValue.join(',')+'&outvalue='+chkoutValue.join(",")+'"></iframe>',
                width: '400px',
                height: '250px',
                draggable: true,
                isModal: true
            });
        }
    }else{
    	alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
        return ;
    }
}
//付款
function payMoney(){
    var checkboxList = $('.grid').find('.table-body').find(':checkbox');
    if(checkboxList && checkboxList.filter(':checked').size() > 0){
        var chkoutValue = [];//出库单
        var chkinValue = [];//直发单
        var chkMoney=0;
        var vouno = '';
        var flag = true;
        $.each(checkboxList.filter(':checked'),function(i){
        	if('1' == $(this).parents("tr").find("td:eq(11)").find("span").attr('title')){
        		vouno = $(this).parents("tr").find("td:eq(6)").find("span").text();
        		flag = false;
        		return false;
        	}
            if($.trim($(this).parents("tr").find("td:eq(7)").find("span").attr("data"))=='1') {
                chkoutValue.push($(this).val());
            }else{
                chkinValue.push($(this).val());
            }
            var money=$.trim($(this).parents("tr").find("td:eq(9)").find("span").text());
            chkMoney+=isNaN(money)?0:Number(money);
        });
        if(!flag){
        	alert('<fmt:message key ="document_number" />:['+vouno+']<fmt:message key ="te_data_have_been_audited" />!');
        	return;
        }
        detailWin = $('body').window({
            id: 'payMoney',
            title: '<fmt:message key ="Add_clearing_record" />',
            content: '<iframe id="payMoneyFrame" frameborder="0" src="<%=path%>/accrued/paymoney.do?invalue='+chkinValue.join(',')+'&outvalue='+chkoutValue.join(",")+'&t_pay='+chkMoney+'"></iframe>',
            width: '400px',
            height: '280px',
            draggable: true,
            isModal: true
        });
    }else{
    	alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
        return ;
    }
}
//添加期初未结金额
function addStartMoney(){
    var branche = $.trim($('#branche').val());
    if(branche == '' || branche == 'undefined'){
    	alert("<fmt:message key="please_select_branche" />！");
        return;
    }
    var  billWin = $('body').window({
        id: 'addDeliverMoney',
        title: '<fmt:message key ="Initial_value_added" />',
        content: '<iframe id="addDeliverMoneyFrame" frameborder="0" src="<%=path%>/accrued/addmoney.do?firm='+branche+'"></iframe>',
        width: '400px',
        height: '280px',
        draggable: true,
        isModal: true
    });
    billWin.max();

}

function pageReload(){
    $('#listForm').submit();
}
</script>
</body>
</html>