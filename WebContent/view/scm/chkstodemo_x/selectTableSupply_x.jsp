<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>suppply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supply/selectSupply_x.do?level=${level}&&code=${code}&&is_supply_x=${is_supply_x}" method="post">
			<div class="form-line">
				<div class="form-input">
					<fmt:message key="coding"/>：<input type="text" name="sp_code_x" id="sp_code_x" value="${supply.sp_code_x }"/> 
					<fmt:message key="name"/>:<input type="text" name="sp_name_x" id="sp_name_x"  value="${supply.sp_name_x }"/>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td style="width:80px;"><span><fmt:message key ="virtual" /><fmt:message key="supplies_code"/></span></td>
								<td style="width:120px;"><span><fmt:message key ="virtual" /><fmt:message key="supplies_name"/></span></td>
								<td style="width:90px;"><span><fmt:message key="abbreviation"/></span></td>
								<td style="width:70px;"><span><fmt:message key="unit"/></span></td>
<%-- 								<td style="width:70px;"><span><fmt:message key="status"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr>
									<td class="num" style="width:31px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code_x}" value="${supply.sp_code_x}"/>
									</td>
								   <td><span title="${supply.sp_code_x}" style="width:72px;text-align: right;">${supply.sp_code_x}&nbsp;</span></td>
								   <td><span title="${supply.sp_name_x}" style="width:110px;text-align: right;">${supply.sp_name_x}&nbsp;</span></td>
								   <td><span title="${supply.sp_init_x}" style="width:80px;text-align: right;">${supply.sp_init_x}&nbsp;</span></td>
								   <td><span title="${supply.unit_x}" style="width:60px;text-align: right;">${supply.unit_x}&nbsp;</span></td>
<%-- 								   <td><span title="${supply.sta}" style="width:60px;text-align: right;">${supply.sta}&nbsp;</span></td> --%>
						
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code_x"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var t;
		function search(){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout(searchSupply(),200);//延迟0.2秒
		}
		function searchSupply(){
			$('#listForm').submit();//提交表单
		}
		$(document).ready(function(){
			focus() ;//页面获得焦点
			$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.parent.$('.close').click();
		 		}
			}); 
			//排序start
				 var array = new Array();   
				array = ['sp_code','sp_name','sp_desc','sp_init','unit', 'sp_price', 'unit1', 'sta'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						var arr=$('#orderBy').val().split(","); 
						if(arr.length>8){
							arr.pop();
							$('#orderBy').val(b+','+arr.join(","));
						}else{
							$('#orderBy').val(b+','+$('#orderBy').val());
						}
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				} 
			//排序结束
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){ 
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key="enter"/>',
						title: '<fmt:message key="enter"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							selectSupply();
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							parent.parent.$('.close').click();
						}
					}
				]
			});

			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			changeTh();//拖动 改变table 中的td宽度 
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//------------------------------
			//单击每行选中前面的checkbox
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if($(this).find(':checkbox')[0].checked){
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			 });
			//禁用checkbox本身的事件
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				event.stopPropagation();
				if(this.checked){
					$(this).attr("checked",false);	
				}else{
					$(this).attr("checked",true);
				}
				$(this).closest('tr').click();
			});
			//---------------------------
			
			var t=$("#sp_init").val();
			$("#sp_init").val("").focus().val(t);
			
			//让之前选中的默认选中
			var defaultCode = '${defaultCode}';
			if(defaultCode!=''){
				$('.grid').find('.table-body').find(':checkbox').each(function(){
					if(this.id.substr(4,this.id.length)==defaultCode){
						$(this).attr("checked", true);
					}
				})	
			}
			$("#listForm").attr("action","<%=path%>/supply/selectSupply_x.do?level="+'${level}'+"&code="+'${code}'+"&defaultCode="+'${defaultCode}');
		});	
		function selectSupply(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				var sp_code_x='';sp_name_x ='';unit_x ='';
				checkboxList.filter(':checked').parents('tr').each(function(){
					var tr = $(this);
					sp_code_x += tr.find('td:eq(2)').find('span').attr('title')+",";
					sp_name_x += tr.find('td:eq(3)').find('span').attr('title')+",";
					unit_x +=  tr.find('td:eq(5)').find('span').attr('title')+",";
				});
				window.parent.dbSupplySelect(sp_code_x.substr(0,sp_code_x.length-1),sp_name_x.substr(0,sp_name_x.length-1),unit_x.substr(0,unit_x.length-1));
			}else{
				window.parent.dbSupplySelect('','','');
			}
		}
		</script>
	</body>
</html>