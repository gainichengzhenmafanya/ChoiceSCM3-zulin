<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>虚拟物料报货单模板主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<input type="hidden" name="sta" id="sta" value="select"/>
		<div class="tool"></div>
		<div style="height:35%;">
			<form id="listForm" action="<%=path%>/chkstodemom/listChkstoDemom_x.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
<!-- 										<input type="checkbox" id="chkAll"/> -->
									</td>
									<td><span style="width:100px;"><fmt:message key="title"/></span></td>
									<td><span style="width:100px;"><fmt:message key="scm_year"/></span></td>
									<td><span style="width:100px;"><fmt:message key="fill_time"/></span></td>
									<td><span style="width:100px;"><fmt:message key="time"/></span></td>
									<td><span style="width:100px;"><fmt:message key="orders_maker"/></span></td>
									<td><span style="width:100px;"><fmt:message key="document_number"/></span></td>
									<td><span style="width:100px;"><fmt:message key="total_amount1"/></span></td>
									<td><span style="width:100px;"><fmt:message key="positn_state"/></span></td>
									<td><span style="width:100px;"><fmt:message key="remark"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="chkstodemom_x" items="${listChkstoDemom_x}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${chkstodemom_x.chkstodemono}' />" value="<c:out value='${chkstodemom_x.chkstodemono}' />"/>
										</td>
										<td><span style="width:100px;">${chkstodemom_x.title}</span></td>
										<td><span style="width:100px;">${chkstodemom_x.yearr}</span></td>
										<td><span style="width:100px;"><fmt:formatDate value="${chkstodemom_x.maded}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:100px;">${chkstodemom_x.madet}</span></td>
										<td><span style="width:100px;">${chkstodemom_x.madeby}</span></td>
										<td><span style="width:100px;">${chkstodemom_x.vouno}</span></td>
										<td><span style="width:100px;"><fmt:formatNumber type="number" value="${chkstodemom_x.totalamt} " pattern="#.##"/></span></td>
										<td><span style="width:100px;">${chkstodemom_x.status}</span></td>
										<td><span style="width:100px;">${chkstodemom_x.memo}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			<div class="search-div" style="margin-left: 0px;">
			<div class="search-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left"><fmt:message key="title"/>：</td>
						<td>
							<input type="text" id="title" name="title" class="text" value="${chkstodemom_x.title }" />
						</td>
						<td class="c-left"><fmt:message key="scm_year"/>：</td>
						<td>
							<input type="text" id="yearr" name="yearr" class="text" value="${chkstodemom_x.yearr }" />
						</td>
						<td class="c-left"><fmt:message key="fill_time"/>：</td>
						<td>
							<input type="text" id="maded" name="maded" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${chkstodemom_x.maded}' pattern='yyyy-MM-dd'/>"/>
						</td>
					</tr>
				</table>
			</div>
			<div class="search-commit">
				<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />' />
				<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />' />
			</div>
		</div>
			</form>
		</div>
		<div class="mainFrame" style="height:60%;width:100%">
		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			if($.browser.msie){ 
				if ($.browser.version == 8.0) {
					setElementHeight('.grid',['.tool'],$(document.body),400);	//计算.grid的高度--适用IE8
				} else {
					setElementHeight('.grid',['.tool'],$(document.body),495);	//计算.grid的高度 -- 适用IE8以上版本
				}
			};
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var win1;
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select"/>',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						/* $('#sta').val("select");
						$("#listForm").submit(); */
						$('.search-div').slideToggle(100);
					}
				},"-",{
						text: '<fmt:message key="insert"/>',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							$('#sta').val("add");
							saveChkstoDemod();
						}
					},{
						text: '<fmt:message key="update"/>',
						title: '<fmt:message key="update"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							$('#sta').val("update");
							updateChkstoDemod();
						}
					},{
						text: '<fmt:message key="delete"/>',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							$('#sta').val("delete");
							deleteChkstoDemod();
						}
					},"-",{
						text: '<fmt:message key="scm_association_branch"/>',
						title: '<fmt:message key="scm_association_branch"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							$('#sta').val("select");
							toAccountFirm();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
			var win;
			function saveChkstoDemod(){
				win=$('body').window({
					id: 'window_saverole',
					title: '<fmt:message key ="insert" /><fmt:message key ="report_form_template" />',
					content: '<iframe id="saveChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemom/addChkstoDemod_x.do"></iframe>',
					width: $(document.body).width()-20/* '1100px' */,
					height: '500px',
					draggable: true,
					isModal: true
					
				});
			}
			
			function updateChkstoDemod(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					
					$('body').window({
						title: '<fmt:message key ="update" /><fmt:message key ="report_form_template" />',
						content: '<iframe id="updateChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemom/updChkstoDemom_x.do?chkstodemono='+chkValue+'"></iframe>',
						width: $(document.body).width()-20/* '1000px' */,
						height: '420px',
						draggable: true,
						isModal: true
					});
				}else{
					alert('<fmt:message key ="please_select_information_you_need_to_modify" />！');
					return ;
				}
				
			}
			
		
			function deleteChkstoDemod(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key ="Are_you_sure_you_want_to_delete_the_document_template_information" />？")){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						
						$('body').window({
							title: '<fmt:message key ="Delete_document_template_information" />',
							content: '<iframe id="deleteChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemom/deleteChkstoDemom_x.do?ids='+chkValue.join(",")+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert("<fmt:message key ="please_select_information_you_need_to_delete" />！");
					return ;
				}
			}
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
						chkstodemono=$(this).find(':checkbox').val();
						var  url="<%=path%>/chkstodemom/listChkstoDemod_x.do?chkstodemono="+chkstodemono;
						$('#mainFrame').attr('src',url);
				        window.mainFrame.location =url;
			     }
			 });
			//单击每行选中前面的checkbox
// 			$('.grid').find('.table-body').find('tr').live("click", function () {
// 				if($(this).find(':checkbox')[0].checked){
// 					$(":checkbox").attr("checked", false);
// 				}else{
// 					$(":checkbox").attr("checked", false);
// 					$(this).find(':checkbox').attr("checked", true);
// 				}
// 				if($(this).hasClass("show-firm-row"))return;
// 				$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
// 				$(this).addClass("show-firm-row");
// 				chkstodemono=$(this).find(':checkbox').val();
<%-- 		        window.mainFrame.location = "<%=path%>/chkstodemom/listChkstoDemod.do?chkstodemono="+chkstodemono; --%>
// 			 });
			//禁用checkbox本身的事件
// 			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
// 				event.stopPropagation();
// 				if(this.checked){
// 					$(this).attr("checked",false);	
// 				}else{
// 					$(this).attr("checked",true);
// 				}
// 				$(this).closest('tr').click();
// 			});
			
			$("#search").bind('click', function() {
				$('.search-div').hide();
				$('#sta').val("select");
				$("#listForm").submit();
			});
			/* 模糊<fmt:message key="select" />清空 */
			$("#resetSearch").bind('click', function() {
				$('.search-condition input').val('');
			});
		});
		//所属分店
		function toAccountFirm() {
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() < 1){
				alert('<fmt:message key ="please_select_at_least_one_data" />！');
				return;
			}
			var firmCode;
			var chkstodemo;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				chkstodemo = checkboxList.filter(':checked').eq(0).val();
				firmCode=checkboxList.filter(':checked').parents('tr').find('td:eq(4)').find('span').attr("title");
			}
			var chkValue = [];
			checkboxList.filter(':checked').each(function(){
				chkValue.push($(this).val());
			});
			path="<%=path%>/chkstodemom/listChkstoDemoFirm_x.do?chkstodemo="+chkValue.join(",");
			customWindow = $('body').window({
				id: 'window_selectFirm',
				title: '<fmt:message key="scm_association_branch"/>',
				content: '<iframe id="accountFirmFrame" frameborder="0" src="'+path+'"></iframe>',
				width: 800,
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="modify_supplier" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('accountFirmFrame')&&window.document.getElementById("accountFirmFrame").contentWindow.select_Positn()){
									submitFrameForm('accountFirmFrame','listForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		function pageReload(){
			/* var sta=$('#sta').val();
			if("update"==sta){//只刷新mainFrame
		    	window.mainFrame.location.href = window.mainFrame.location.href;
			}else{ */
	        	window.location.href = '<%=path%>/chkstodemom/listChkstoDemom_x.do';
			/* } */
	    }
		</script>
	</body>
</html>