<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>账号 加入所属分店</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<form action="<%=path%>/positnRole/firmByAccount.do" id="listForm" name="listForm" method="post">
		<input type="hidden" name="accountId" value="${accountId}" />
		<input type="hidden" id="firmCode" name="firmCode" value="${firmCode}" />
		<input type="hidden" id="typ" name="typ" value="1203" />
		<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="coding" />:</b></font>
			            <input type="text" id="code" name="code" class="text" value="<c:out value="${positn.code}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name" />:</b></font>
			            <input type="text" id="des" name="des" class="text" value="<c:out value="${positn.des}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="abbreviation" />:</b></font>
			            <input type="text" id="init" name="init" class="text" value="<c:out value="${positn.init}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key ="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;"><span class="num" style="width: 26px;"></span></td>
								<td style="width:30px;">
									
								</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:180px;"><fmt:message key="name" /></td>
								<td style="width:80px;"><fmt:message key="type" /></td>
								<td style="width:80px;"><fmt:message key="abbreviation" /></td>
								<td style="width:80px;"><fmt:message key="for_short" /></td>
								<td style="width:60px;"><fmt:message key="area" /></td>
								<td style="width:80px;"><fmt:message key="business_group" /></td>
								<td style="width:80px;"><fmt:message key="distribution_area" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" varStatus="status" items="${listPositn}">
								<tr>
									<td class="num" style="width:31px;"><span class="num" style="width: 26px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
									<span class="num" style="width: 20px;"><input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.code}"/></span>
									</td>
									<td><span title="${positn.code}" style="width:40px;text-align: left;"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:170px;text-align: left;"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span title="${positn.typ}" style="width:70px;text-align: left;"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span title="${positn.init}" style="width:70px;text-align: left;"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span title="${positn.des1}" style="width:70px;text-align: left;"><c:out value="${positn.des1}"/>&nbsp;</span></td>
									<td><span title="${positn.area}" style="width:50px;text-align: left;"><c:out value="${positn.area}"/>&nbsp;</span></td>
									<td><span title="${positn.mod2}" style="width:70px;text-align: left;"><c:out value="${positn.mod2}" />&nbsp;</span></td>
									<td><span title="${positn.psarea}" style="width:70px;text-align: left;"><c:out value="${positn.psarea}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				focus() ;//页面获得焦点
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				});
				$('#search').bind('click',function(e){
					$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" /><fmt:message key="position_select" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Positn();
								//alert($('#parentId').val());
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
				$('#chk_'+$('#firmCode').val()).attr('checked','checked');
/* 				var str=$('#parentId').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++)
				{ 
					$('#chk_'+strArry[i]).attr('checked','checked');
					$('#parentName').val($('.grid').find('.table-body').find('tr:eq(i)').find('td:eq(3)').find('span').attr('title'));
				}; */
				setElementHeight('.grid',['.tool'],$(document.body),24);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#parentId').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#parentName').val($(this).find('td:eq(3)').find('span').attr('title'));
				 });
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#parentId').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#parentName').val($(this).find('td:eq(3)').find('span').attr('title'));
					 select_Positn();
				 });
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==13){
			 			$('#listForm').submit();
			 		}
				});
			});
			function select_Positn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() !=1){
					if(!confirm('<fmt:message key ="Did_not_choose_any_information_whether_to_continue" />？'))return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action = '<%=path%>/positnRole/saveFirmAccount.do?accountId=${accountId}&positnIds='+chkValue.join(",");
	 			$('#listForm').attr("action",action);
	 			$('#listForm').submit();
			}
		</script>
	</body>
</html>