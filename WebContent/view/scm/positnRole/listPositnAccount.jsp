<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="startdate"/>账号-positn</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<style type="text/css"> 
	 	.page{
			margin-bottom: 25px;
		}
		</style>
	</head>
	<body>
		<form action="<%=path%>/positnRole/positnByAccount.do" id="listForm" name="listForm" method="post">
			<input type="hidden" name="accountId" value="${accountId}" />
			<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="coding"/>:</b></font>
			            <input type="text" id="code" name="code" class="text" value="<c:out value="${positn.code}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name"/>:</b></font>
			            <input type="text" id="des" name="des" class="text" value="<c:out value="${positn.des}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="abbreviation"/>:</b></font>
			            <input type="text" id="init" name="init" class="text" value="<c:out value="${positn.init}" />"/>
			        </td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select"/>'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:50px;"><fmt:message key="coding"/></td>
								<td style="width:180px;"><fmt:message key="name"/></td>
								<td style="width:80px;"><fmt:message key="type"/></td>
								<td style="width:80px;"><fmt:message key="abbreviation"/></td>
								<td style="width:80px;"><fmt:message key="for_short"/></td>
								<td style="width:60px;"><fmt:message key="area"/></td>
								<td style="width:80px;"><fmt:message key="business_group"/></td>
								<td style="width:80px;"><fmt:message key="distribution_area"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" varStatus="step" items="${listPositn}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.code}"/>
									</td>
									<td><span title="${positn.code}" style="width:40px;text-align: left;"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:170px;text-align: left;"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span title="${positn.typ}" style="width:70px;text-align: left;"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span title="${positn.init}" style="width:70px;text-align: left;"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span title="${positn.des1}" style="width:70px;text-align: left;"><c:out value="${positn.des1}"/>&nbsp;</span></td>
									<td><span title="${positn.area}" style="width:50px;text-align: left;"><c:out value="${positn.area}"/>&nbsp;</span></td>
									<td><span title="${positn.mod2}" style="width:70px;text-align: left;"><c:out value="${positn.mod2}" />&nbsp;</span></td>
									<td><span title="${positn.psarea}" style="width:70px;text-align: left;"><c:out value="${positn.psarea}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<input type="hidden" id="typ" name="typ" /> --%>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_role_of_position_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								savePositnAccount();
							}
						},{
							text: '<fmt:message key="back_roles_page" />',
							title: '<fmt:message key="back_roles_page"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								 window.location.href="<%=path%>/role/list.do";
							}
						}
					 ]
				});
				setElementHeight('.grid',['.tool'],$(document.body),22);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				<c:forEach var="positnAccount" varStatus="step" items="${PositnAccountList}">
					$('#chk_${positnAccount.positnId}').attr('checked',"true");
				</c:forEach>
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")){
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				}); 
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==13){
			 			$('#listForm').submit();
			 		}
				});
			});
			//保存
			function savePositnAccount(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action = '<%=path%>/positnRole/savePositnAccount.do?accountId=${accountId}&positnIds='+chkValue.join(",");
				$('body').window({
					title: '<fmt:message key="Assigned_account_number" />',
					content: '<iframe frameborder="0" src='+action+'></iframe>',
					width: 500,
					height: '245px',
					draggable: true,
					isModal: true
				});
			}
		    function pageReload(){
		    	parent.$('.close').click();
		    }
		</script>
	</body>
</html>