<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="startdate"/>仓位信息</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			.topFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.bottomFrame { 
				background-color:#FFF; 
				width:100%;
				height:81%;
			}
			.check{
				float: left;
				vertical-align: middle;
				margin-right: 0px;
				height: 30px;
				line-height: 30px;
				text-align: right;
				font-weight: bold;
				padding-right: 5px;
				background-color: #E1E1E1;
			}
			.formInput {
				height: 30px;
				vertical-align: middle;
				line-height: 30px;
				text-align: left;
				padding-left: 5px;
				background-color: #E1E1E1;
				white-space: normal;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<div class="topFrame">
			<form id="positnBatchForm" method="post" action="<%=path%>/positnRole/list.do">
			<input type="hidden" name="id" value="${roleId}" />
				<div class="form-line" style="background-color: #E1E1E1;height: 80px;;margin-right:0px;" id="positnForm">
					<fmt:message key="branches_positions_authorized_range"/>:<br/>
					<!-- <div style="overflow: auto;height: 60px;background-color: #E1E1E1;" id="addPoEle"></div> -->
					<input type="hidden" id="parentId" name="code" class="text" readonly="readonly" value=""/>
					<textarea style="width:750px; height:50px; boder:0px;background-color: #E1E1E1;" id="parentName" name="name" class="text" readonly="readonly">
					</textarea>
					
				</div>
			</form>
	</div>
	<div class="bottomFrame">
		<iframe src="<%=path%>/positn/selectPositn.do" frameborder="0" name="choiceFrame" id="choiceFrame"></iframe>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
				<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save_role_of_position_information"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							savePositnRole();
						}
					},{
						text: '<fmt:message key="back_roles_page" />',
						title: '<fmt:message key="back_roles_page"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							 window.location.href="<%=path%>/role/list.do";
						}
					}
				 ]
				});
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),550);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
			var chkCode = [];
			var chkName = [];
			//选择某一条  
			function selectPositn(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#positnBatchForm').find('div').find('input').val(chkCode);
				$('#positnBatchForm').find('div').find('textarea').val(chkName);
			}
			// 添加全选
			function selectAllPositn(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					return;
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#positnBatchForm').find('div').find('input').val(chkCode);
				$('#positnBatchForm').find('div').find('textarea').val(chkName);
			}
			//全部删除
			function selectZeroPositn(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					return;
				}
				$('#positnBatchForm').find('div').find('input').val(chkCode);
				$('#positnBatchForm').find('div').find('textarea').val(chkName);
			}
			//保存
			function savePositnRole(){
				var code = $('#parentId').val();
				if(confirm('<fmt:message key="save_the_data_or_not"/>？')){
					var action = '<%=path%>/positnRole/savePositnRole.do?roleId=${roleId}&positnIds='+code;
					$('body').window({
						title: '<fmt:message key="save_role_has_positions_permission"/>',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}
		    function pageReload(){
		    	$('#positnBatchForm').submit();
		    }
		</script>
	</body>
</html>