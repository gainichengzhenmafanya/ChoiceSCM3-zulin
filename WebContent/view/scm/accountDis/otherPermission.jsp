<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					//background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
		<div id="toolbar"></div>
		<div style="width:100%;height:300px;">
			<input type="hidden" id="accountId" name="accountId" value="${accountDis.accountId}" />
			<br/><br/><br/>
			<div style="width:100%;text-align:center;">
				<span>报货分拨是否可修改已做报价:</span>
				<input type="radio" name="ynzp" value="Y" <c:if test="${accountDis.ynzp == 'Y' }">checked="checked"</c:if>/><fmt:message key ="be" />
				<input type="radio" name="ynzp" value="N" <c:if test="${accountDis.ynzp != 'Y' }">checked="checked"</c:if>/><fmt:message key ="no1" />
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		
		<script type="text/javascript">

		var validate;
			$(document).ready(function(){
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save_account_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								$.ajax({
									type: 'POST',
									url: '<%=path %>/accountDis/ajaxSaveOtherPermission.do',
									data: 'accountId='+$('#accountId').val()+'&ynzp='+$('input[name=ynzp]:checked').val(),
									success: function(info){
										if(info == 'OK'){
											alert("<fmt:message key ="save_successful" />！");
											$('.close',parent.document).click();
										}
									}
								});
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close',parent.document).click();
							}
						}
					]
				});//end toolbar
				
			});//end $(document).ready()

		</script>
	</body>
</html>