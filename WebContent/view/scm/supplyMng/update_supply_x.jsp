<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/supplyMng/updateSupply_x?spcodeList=${spcodeList}" method="post">
			<input type="hidden" name="mod" value="${mode }" />
			<input type="hidden" name="sp_code" value="${sp_code }" />
			<input type="hidden" name="type" value="${type }" />
			<input type="hidden" name="code1" id="code1" value="${code1 }" />
			<input type="hidden" name="level" id="level" value="${level }"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									&nbsp;
								</td>
								<td style="width:150px;"><fmt:message key="coding" /></td>
								<td style="width:150px;"><fmt:message key="name" /></td>
								<td style="width:100px;"><fmt:message key="abbreviation" /></td>
								<td style="width:50px;"><fmt:message key="unit" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code_x}" value="${supply.sp_code_x}"/>
									</td>
									<td><span title="${supply.sp_code_x}" style="width:140px;text-align: left;"><c:out value="${supply.sp_code_x}" />&nbsp;</span></td>
									<td><span title="${supply.sp_name_x}" style="width:140px;text-align: left;"><c:out value="${supply.sp_name_x}" />&nbsp;</span></td>
									<td><span title="${supply.sp_init_x}" style="width:90px;text-align: left;"><c:out value="${supply.sp_init_x}" />&nbsp;</span></td>
									<td><span title="${supply.unit_x}" style="width:40px;text-align: left;"><c:out value="${supply.unit_x}" />&nbsp;</span></td>
									<td><span title="${supply.unitRate_x}" style="width:40px;text-align: left;"><c:out value="${supply.unitRate_x}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />										
		</form>
		<form id="tableForm" method="post" action="<%=path %>/supplyMng/saveSupply_x.do">
			<input type="hidden" id="sp_code_x" name="sp_code_x" />
	    	<input type="hidden" id="sp_name_x" name="sp_name_x" />
	    	<input type="hidden" id="sp_init_x" name="sp_init_x" />
	    	<input type="hidden" id="unit_x" name="unit_x" />
	    	<input type="hidden" id="unitRate_x" name="unitRate_x"/>
	    	<input type="hidden" id="code1" name="code1" value="${code1}"/>
	    	<input type="hidden" id="level" name="level" value="${level}"/>
	    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				setElementHeight('.grid',[],$(document.body),25);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#sp_code_x').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#sp_name_x').val($(this).find('td:eq(3)').find('span').attr('title'));
					 $('#sp_init_x').val($(this).find('td:eq(4)').find('span').attr('title'));
					 $('#unit_x').val($(this).find('td:eq(5)').find('span').attr('title'));
					 $('#unitRate_x').val($(this).find('td:eq(6)').find('span').attr('title'));
				 });
			});
			
			function updSupply_x(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size()==1){
					$("#positn").val(checkboxList.filter(':checked').val());
					return true;
				}else{
					alert('请选择唯一的虚拟物料！');
					return false;
				}
			}
		</script>
	</body>
</html>