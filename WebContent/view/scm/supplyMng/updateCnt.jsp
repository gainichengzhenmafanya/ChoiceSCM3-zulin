<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Classes Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	</head>
	<body>
 		<div  >
			<div class="form">
				<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:140px;margin-top:160px;">
					<form id="cntForm" method="post" action="<%=path %>/supplyMng/updateCnt.do">
						<input type="hidden" id="level" name="level" value="${level}"/>
					    <input type="hidden" id="code" name="code" value="${code}"/>
					    <div class="form-line">
							<div class="form-label"><font style="font-size:2.0ex;"><b><fmt:message key="minimum_stock_settings" />:</b></font></div>
							<div class="form-label" style="width: 58px;"><fmt:message key="average_daily_consumption" /><font style="align:left"><b>&nbsp;X</b></font></div>
							<div class="form-input">
								<input type="text" id="ratemin" name="ratemin" style="width:90px;" onblur="checkMin(this);"/>倍
							</div>
						</div>
						<div class="form-line">
							<div class="form-label"><font style="font-size:2.0ex;"><b><fmt:message key="highest_stock_settings" />:</b></font></div>
							<div class="form-label" style="width: 58px;"><fmt:message key="average_daily_consumption" /><font style="align:left"><b>&nbsp;X</b></font></div>
							<div class="form-input">
								<input type="text" id="ratemax" name="ratemax" style="width:90px;" onblur="checkMax(this);"/>倍
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#ratemin').focus();
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'ratemin',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="cannot_be_empty" />','<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratemax',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="cannot_be_empty" />','<fmt:message key ="not_valid_number" />！']
					}]
				});
			});
			//检查最高最低库存
			function checkMin(inputObj){
				if(isNaN(inputObj.value)){
					alert("<fmt:message key ="not_valid_number" />！");
					inputObj.focus();
					return false;
				}
				if ($('#ratemax').val()!=''&&$('#ratemax').val()!=null && inputObj.value > $('#ratemax').val()) {
					alert('<fmt:message key="Lowest_inventory_must_be_less_than_the_maximum_inventory" />！');
					inputObj.clear().focus();
					return false;
				}
			}
			function checkMax(inputObj){
				if(isNaN(inputObj.value)){
					alert("<fmt:message key ="not_valid_number" />！");
					inputObj.focus();
					return false;
				}
				if ($('#ratemin').val()!=''&&$('#ratemin').val()!=null && inputObj.value < $('#ratemin').val()) {
					alert('<fmt:message key="The_highest_inventory_must_be_greater_than_the_minimum_inventory" />！');
					inputObj.focus();
					return false;
				}
			}
	 	</script>
	</body>
</html>