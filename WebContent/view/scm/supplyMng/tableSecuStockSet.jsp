<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>安全库存设置</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">				
			.page {
				margin-bottom: 25px;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:140px;
			}
			.moduleInfo {
				position: relative;
				top: 1px;
				height: 80px;
				line-height: 90px;
			}
			.form-line .form-label {
				width: 40px;
			}
			.form-line .form-input {
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>						
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supplyMng/table.do" method="post">
			<input type="hidden" id="type" name="type" value="${type}"/>
			<input type="hidden" id="level" name="level" value="${level}"/>
		    <input type="hidden" id="code" name="code" value="${code}"/>
		    <input type="hidden" id="update" name="update" value="${update}"/>
			<div class="moduleInfo">
				<div class="form-line">
					<div class="form-label"><font style="font-size:2.0ex;"><b><fmt:message key="calcu_ave_daily_consum" />:</b></font></div>
					<div class="form-label"></div>
					<div class="form-label"></div>
					<div class="form-label" style="width: 58px;"><fmt:message key="startdate" />:</div>
					<div class="form-input">
						<input  style="width:90px;"  type="text" id="startdate" name="startdate"  class="Wdate text" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd"/>" />
					</div>
					<div class="form-label"></div>
					<div class="form-label"></div>
					<div class="form-label" style="width: 58px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="enddate" />:</div>
					<div class="form-input">
						&nbsp;&nbsp;&nbsp;&nbsp;<input  style="width:90px;margin-top:2px;margin-bottom:3px;" type="text" id="enddate" name="enddate"  class="Wdate text" value="<fmt:formatDate value="${edate}" pattern="yyyy-MM-dd"/>" />
					</div>
					<div class="form-input">
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" style="width:60px" id="calculate" name="calculate" value='<fmt:message key="calculate" />'/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><font style="font-size:2.0ex;"><b><fmt:message key="minimum_stock_settings" />:</b></font></div>
					<div class="form-label"></div>
					<div class="form-label"></div>
					<div class="form-label" style="width: 58px;"><fmt:message key="average_daily_consumption" /><font style="align:left"><b>&nbsp;X</b></font></div>
<!-- 					<div class="form-label" ><font style="align:left"><b>X&nbsp;&nbsp;&nbsp;&nbsp;</b></font></div> -->
					<div class="form-input">
						<input type="text" value="${ratemin}" id="ratemin" name="ratemin" style="width:90px;"/>
					</div>
					<div class="form-input">
						<input type="button" style="width:60px" id="calculate1" name="calculate1" value='<fmt:message key="calculate" />1'/>
					</div>
					<div class="form-label" style="width: 58px;"><fmt:message key="current_value" /><font style="align:left"><b>&nbsp;X</b></font></div>
<!-- 					<div class="form-label" ><font style="align:left"><b>X&nbsp;&nbsp;&nbsp;&nbsp;</b></font></div> -->
					<div class="form-input">
						<input type="text" value="${ratemin1}" id="ratemin1" name="ratemin1" style="width:90px;"/>
					</div>
					<div class="form-input">
						<input type="button" style="width:60px;" id="calculate3" name="calculate3" value='<fmt:message key="calculate" />3'/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><font style="font-size:2.0ex;"><b><fmt:message key="highest_stock_settings" />:</b></font></div>
					<div class="form-label"></div>
					<div class="form-label"></div>
					<div class="form-label" style="width: 58px;"><fmt:message key="average_daily_consumption" /><font style="align:left"><b>&nbsp;X</b></font></div>
<!-- 					<div class="form-label" ><font style="align:left"><b>X&nbsp;&nbsp;&nbsp;&nbsp;</b></font></div> -->
					<div class="form-input">
						<input type="text" value="${ratemax}" id="ratemax" name="ratemax" style="width:90px;"/>
					</div>
					<div class="form-input">
						<input type="button" style="width:60px" id="calculate2" name="calculate2" value='<fmt:message key="calculate" />2'/>
					</div>
					<div class="form-label" style="width: 58px;"><fmt:message key="current_value" /><font style="align:left"><b>&nbsp;X</b></font></div>
<!-- 					<div class="form-label" ><font style="align:left"><b>X&nbsp;&nbsp;&nbsp;&nbsp;</b></font></div> -->
					<div class="form-input">
						<input type="text" value="${ratemax1}" id="ratemax1" name="ratemax1" style="width:90px;"/>
					</div>
					<div class="form-input">
						<input type="button" style="width:60px" id="calculate4" name="calculate4" value='<fmt:message key="calculate" />4'/>
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:21px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:170px;"><fmt:message key="name" /></span></td>
								<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:40px;"><fmt:message key ="procurement_cycle" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:130px;"><fmt:message key="average_daily_consumption" /></span></td>
								<td><span style="width:110px;"><fmt:message key="minimum_inventory" /></span></td>
								<td><span style="width:110px;"><fmt:message key="highest_inventory" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr>
									<td class="num"><span style="width:21px;">${step.count}</span></td>
									<td>
										<span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${supply.sp_code}" value="${supply.sp_code}"/>
										</span>
									</td>
									<td><span title="${supply.sp_code}" style="width:100px;text-align: left;">${supply.sp_code}&nbsp;</span></td>
									<td><span title="${supply.sp_name}" style="width:170px;text-align: left;">${supply.sp_name}&nbsp;</span></td>
									<td><span title="${supply.sp_desc}" style="width:90px;text-align: left;">${supply.sp_desc}&nbsp;</span></td>
									<td><span title="${supply.datsto}" style="width:40px;text-align: left;">${supply.datsto}&nbsp;</span></td>
									<td><span title="${supply.unit}" style="width:50px;text-align: left;">${supply.unit}&nbsp;</span></td>
									<td><span title="${supply.cntuse}" style="width:130px;text-align: left;"><fmt:formatNumber value="${supply.cntuse}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td class="textInput"><span title="${supply.sp_min1}" style="width:120px;padding: 0px;">
										<input type="text" value="<fmt:formatNumber value="${supply.sp_min1}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
									<td class="textInput"><span title="${supply.sp_max1}" style="width:120px;padding: 0px;">
										<input type="text" value="<fmt:formatNumber value="${supply.sp_max1}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>" onfocus="this.select()" onblur="checkNum(this);"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
							<td><input type="text" id="sp_init" name="sp_init" class="text" onkeyup="ajaxSearch('sp_init')" style="text-transform:uppercase;" value="${supply.sp_init}" /></td>
							<td class="c-left"><fmt:message key="supplies_no" />：</td>
							<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${supply.sp_code}" /></td>
							<td class="c-left"><fmt:message key="supplies_name" />：</td>
							<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${supply.sp_name}" /></td>
						</tr>
<!-- 						<tr> -->
<%-- 							<td class="c-left"><fmt:message key="bigClass"/>：</td> --%>
<%-- 							<td><select  name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" code="${supply.grptyp}" des="${supply.grptypdes}" class="select"></select> --%>
<!-- 							</td> -->
<%-- 							<td class="c-left"><fmt:message key="middleClass"/>：</td> --%>
<%-- 							<td><select  name="grp" url="<%=path %>/grpTyp/findAllGrp.do" code="${supply.grp}" des="${supply.grpdes}" class="select"></select> --%>
<!-- 							</td> -->
<%-- 							<td class="c-left"><fmt:message key="smallClass"/>：</td> --%>
<%-- 							<td><select  name="typ" url="<%=path %>/grpTyp/findAllTyp.do" code="${supply.sp_type}" des="${supply.typdes}" class="select"></select> --%>
<!-- 							</td> -->
<!-- 						</tr> -->
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<form id="tableForm" action="" method="post"></form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">		
			var t;
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
				//点击分页时 检查页面数据是否被改动
				var pristineAction = pageAction;
	 			pageAction = function(nowPage){
	 				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
	 				if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="There_are_no_changes_to_save_save_(save_the_change_button_to_save)" />')){
	 						return false;
	 					}
	 				}
	 				pristineAction(nowPage);
	 			};
	 			var pristinePerPage = perPageColSizeAction;
	 			perPageColSizeAction = function(count){
	 				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
	 				if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('有修改未保存，是否保存?(点击保存修改按钮保存)')){
	 						return false;
	 					}
	 				}
	 				pristinePerPage(count);
	 			};

				focus() ;//页面获得焦点
				if('y'==parent.$('#bytyp').val()) {
					loadToolBar([true,true,true]);
				}else {
					loadToolBar([true,true,false]);
				}
				
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//排序start
				var array = new Array();      
				array = ['sp_code','sp_name','sp_desc', 'unit','cntuse' , 'sp_min1', 'sp_max1'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				$('#startdate').bind('click',function(){
				new WdatePicker();
				});
				$('#enddate').bind('click',function(){
					new WdatePicker();
				});
				new tabTableInput("table-body","text"); //input  上下左右移动
			
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
// 				$("select").each(function(){
// 	  	 			$(this).htmlUtils("select",[]);
// 	  	 		});

				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				if ($('#update').val()=="ok") {
					alert('<fmt:message key="update_complete" />！');
					$('#update').val("1");
				}
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#calculate').bind("click",function search(){
					var r = confirm('<fmt:message key="make_sure_recalculate" />！');
					if (r==true) {
						if(!$('#startdate').val()) {
							alert('<fmt:message key="please_enter" /><fmt:message key="startdate" />！');
							return;
						}
						if(!$('#enddate').val()) {
							alert('<fmt:message key="please_enter" /><fmt:message key="enddate" />！');
							return;
						}
						if($("#startdate").val() > $("#enddate").val()) {
							alert('<fmt:message key="startdate_littler_than_enddate" />！');
							return;
						}
						$('#update').val("0");
						var action = "<%=path%>/supplyMng/table.do?bdate="+$('#startdate').val()+"&edate="+$('#enddate').val();
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}
				});
				$('#calculate1').bind("click",function search(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 0){
						alert('<fmt:message key="please_select_one_material" />！');
						return;
					}
					if(!$('#ratemin').val()) {
						alert('<fmt:message key="please_enter_ratio" />！');
						return;
					}
					var re = /^\d+(?=\.{0,1}\d+$|$)/;
					var str = $("#ratemin").val();
					if(!re.test(str)){
						alert("<fmt:message key='please_enter_a_number_greater'/>");
						return;
					}
					checkboxList.filter(':checked').each(function(){
						var sto = $(this).parents('tr').find('td:eq(7)').find('span').attr('title');
						var rate = $('#ratemin').val();
						
						$(this).parents('tr').find('td:eq(8)').find('span').find('input').val(Math.round(sto*rate*100)/100);
					});
				});
				$('#calculate3').bind("click",function search(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 0){
						alert('<fmt:message key="please_select_one_material" />！');
						return;
					}
					if(!$('#ratemin1').val()) {
						alert('<fmt:message key="please_enter_ratio" />！');
						return;
					}
					var re = /^\d+(?=\.{0,1}\d+$|$)/;
					var str = $("#ratemin1").val();
					if(!re.test(str)){
						alert("<fmt:message key='please_enter_a_number_greater'/>");
						return;
					}
					checkboxList.filter(':checked').each(function(){
						var sto = $(this).parents('tr').find('td:eq(8)').find('span').find('input').val();
						var rate = $('#ratemin1').val();
						$(this).parents('tr').find('td:eq(8)').find('span').find('input').val(Math.round(sto*rate*100)/100);
					});
				});
				$('#calculate2').bind("click",function search(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 0){
						alert('<fmt:message key="please_select_one_material" />！');
						return;
					}
					if(!$('#ratemax').val()) {
						alert('<fmt:message key="please_enter_ratio" />！');
						return;
					}
					var re = /^\d+(?=\.{0,1}\d+$|$)/;
					var str = $("#ratemax").val();
					if(!re.test(str)){
						alert("<fmt:message key='please_enter_a_number_greater'/>");
						return;
					}
					checkboxList.filter(':checked').each(function(){
						var sto = $(this).parents('tr').find('td:eq(7)').find('span').attr('title');
						var rate = $('#ratemax').val();
						$(this).parents('tr').find('td:eq(9)').find('span').find('input').val(Math.round(sto*rate*100)/100);
					});
				});
				
				$('#calculate4').bind("click",function search(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 0){
						alert('<fmt:message key="please_select_one_material" />！');
						return;
					}
					if(!$('#ratemax1').val()) {
						alert('<fmt:message key="please_enter_ratio" />！');
						return;
					}
					var re = /^\d+(?=\.{0,1}\d+$|$)/;
					var str = $("#ratemax1").val();
					if(!re.test(str)){
						alert("<fmt:message key='please_enter_a_number_greater'/>");
						return;
					}
					checkboxList.filter(':checked').each(function(){
						var sto = $(this).parents('tr').find('td:eq(9)').find('span').find('input').val();
						var rate = $('#ratemax1').val();
						$(this).parents('tr').find('td:eq(9)').find('span').find('input').val(Math.round(sto*rate*100)/100);
					});
				});
			});
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							 $('.search-div').slideToggle(100);
							 var t = $('#sp_init').val();
							 $('#sp_init').focus().val(t);
						}
					},"-",{
						text: '<fmt:message key="save" /><fmt:message key="update" />',
						title: '<fmt:message key="save" /><fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							saveStock();
						}
					},{
						text: '<fmt:message key ="according_to_the_categories_of_odification" />',
						title: '<fmt:message key ="according_to_the_categories_of_odification" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							var level=$('#level').val();
							var code=$('#code').val();
							var url="<%=path%>/supplyMng/toUpdateCnt.do?level="+level+"&code="+code;
							$('body').window({
								id: 'window_updateCnt',
								title: '<fmt:message key="Update_safety_stock_by_category" />',
								content: '<iframe id="window_updateCnt" frameborder="0" src='+url+'></iframe>',
								width: '650px',
								height: '500px',
								draggable: true,
								confirmClose:false,
								isModal: true,
								topBar: {
									items: [{
											text: '<fmt:message key="save" />',
											title: '<fmt:message key="Save_new_security_inventory_settings" />',
											icon: {
												url: '<%=path%>/image/Button/op_owner.gif',
												position: ['-80px','-0px']
											},
											handler: function(){
												if(getFrame('window_updateCnt')&&getFrame('window_updateCnt').validate._submitValidate()){
													submitFrameForm('window_updateCnt','cntForm');
												}
											}
										},{
											text: '<fmt:message key="cancel" />',
											title: '<fmt:message key="cancel" />',
											icon: {
												url: '<%=path%>/image/Button/op_owner.gif',
												position: ['-160px','-100px']
											},
											handler: function(){
												$('.close').click();
											}
										}
									]
								}
							});
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			//保存登记
			function saveStock(){
				var selected = {};
				var msg = "ok";
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
					checkboxList.filter(':checked').each(function(i){
						selected['supplyList['+i+'].sp_code'] = $(this).val();
						selected['supplyList['+i+'].sp_min1'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
						selected['supplyList['+i+'].sp_max1'] = $(this).parents('tr').find('td:eq(9)').find('input').val();
						if (parseInt($(this).parents('tr').find('td:eq(8)').find('input').val()) > parseInt($(this).parents('tr').find('td:eq(9)').find('input').val())) {
							alert("<fmt:message key='not_be_higher_than_the_highest_inventory'/>");		
							msg="";
						}
					});
					if ("ok"!=msg) {
						return;
					}
					$.post('<%=path%>/supplyMng/updateStock.do',selected,function(data){
						showMessage({//弹出提示信息
							type: 'success',
							msg: '<fmt:message key="operation_successful" />！',
							speed: 1000
						});	
					});
				}
				}else{
					alert('<fmt:message key="please_select_options_you_need_save" />！');
					return ;
				}
			}
			
			function onBlurMethod(inputObj){
		    	$(inputObj).parent('span').text($(inputObj).val());
	    	}
			
			//检查最高最低库存是否有效数字
			function checkNum(inputObj){
				if(isNaN(inputObj.value)){
					alert("<fmt:message key ="invalid_number" />！");
					inputObj.focus();
					return false;
				}
			}
			function refresh(){
				alert("refresh");
			}
			
		</script>
	</body>
</html>