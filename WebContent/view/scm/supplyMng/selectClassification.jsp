<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>报货分类</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/codeDes/classification.do" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;"></td>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:100px;"><fmt:message key ="coding" /></td>
								<td style="width:130px;"><fmt:message key ="name" /></td>
<!-- 								<td style="width:80px;">类型</td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="codeDes" varStatus="step" items="${codeDesList}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" 
<%-- 										<c:forEach var="supply" items="${supplyList}"> --%>
<%-- 										<c:if test="${supply.typ_eas==codeDes.code }">checked="checked"</c:if>  --%>
<%-- 										</c:forEach> --%>
										 name="idList" id="chk_${codeDes.code}" value="${codeDes.code}"/>
									</td>
									<td><span title="${codeDes.code}" style="width:90px;text-align: left;">${codeDes.code}&nbsp;</span></td>
									<td><span title="${codeDes.des}" style="width:120px;text-align: left;">${codeDes.des}&nbsp;</span></td>
<%-- 									<td><span title="${codeDes.typ}" style="width:70px;text-align: left;">${codeDes.typ}&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />										
		</form>
		<form id="tableForm" method="post" action="<%=path %>/supplyMng/saveDelByUpd.do">
			<input type="hidden" id="code1" name="code1" value="${code1}"/>
	    	<input type="hidden" id="level" name="level" value="${level}"/>
			<input type="hidden" id="code" name="code" />
	    	<input type="hidden" id="des" name="des" />
	    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
    	</form>
    	
		<input type="hidden" id="spcode" name="spcode" value="${spcode}" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="enter" />',
							title: '<fmt:message key ="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								classification();
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								window.parent.$('.close').click();
							}
						}]
				});
				
				setElementHeight('.grid',[],$(document.body),27);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#code').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#des').val($(this).find('td:eq(3)').find('span').attr('title'));
				 });
			});
			function classification() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key="Please_select_the_goods_classification" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var spcode=$("#spcode").val();
				path="<%=path%>/supplyMng/updateTypEas.do?code="+chkValue.join(',')+"&spcode="+spcode+"&code1="+$('#code1').val()+"&level="+$('#level').val();
				$('#listForm').attr("action",path);
	 			$('#listForm').submit();
			}
			
		</script>
	</body>
</html>