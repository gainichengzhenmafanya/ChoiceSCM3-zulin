<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	    <style type="text/css">
			#tblGrid td{
				border-right: 1px solid #999999;
				border-bottom:1px solid #999999; 
			}
			.form-line .form-label {
				width:200px;
			}
			.divgrid td span {
			  display: block;
			  overflow: hidden;
			  white-space: nowrap;
			  text-overflow: ellipsis;
			  padding: 0px 5px;
			}
		</style>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:20px;margin-top:30px;">
			<form id="tableForm" method="post" action="<%=path %>/supplyMng/saveSupply.do">
				<div class="form-line">
					    <div class="form-label"><fmt:message key="smc_standard"/><fmt:message key="Raw_materials"/>：</div><!--标准原料 jmj -->
						<div class="form-input">
							<input type="hidden"  id="sp_code_x" name="sp_code_x" value=""/>
							<input type="text" id="sp_name_x" name="sp_name_x" readonly="readonly" class="text"/>
							<input type="hidden" id="sp_init_x" name="sp_init_x"/>
							<input type="hidden" id="unit_x" name="unit_x" />
							<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
						</div>
						<div class="form-label"><fmt:message key="the_conversion_rate"/>：</div>
						<div class="form-input"><input type="text" id="unitRate_x" name="unitRate_x" class="text" onblur="checkNum(this);" onkeypress="checkNum(this);" value="1.0"/></div>	
						<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
		    	        <input type="hidden" id="code1" name="code1" value="${code1}"/>
		    	        <input type="hidden" id="level" name="level" value="${level}"/>
					</div>
	    	</form>
    	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		var validate;
		//焦点离开检查输入是否合法
		function checkNum(inputObj){
			if(isNaN(inputObj.value)){
				alert('<fmt:message key ="invalid_number" />！');
				$(inputObj).val(1).focus();
				return false;
			}
		}
		
		/*选择物资*/
	 	$('#seachSupply').bind('click.custom',function(e){
	 		if(!!!top.customWindow){
				top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+$("#sp_code_x").val(),$("#sp_code_x"),$("#sp_name_x"),$("#unit_x"),null,null,null,handler2);
			}
	 	});
		
	 	//弹出物资的回调函数
		function handler2(sp_code){
			//仅支持单选
	 		//wangjie  支持物资多选   遍历所选择的物资编码 sp_code 2014年11月21日 14:04:11
		    $.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				async: false,
				success:function(supply){	
					$("#sp_code_x").val(supply.sp_code);
   					$("#sp_name_x").val(supply.sp_name);							
   					$("#sp_init_x").val(supply.sp_init);							
   					$("#unit_x").val(supply.unit);							
				}
			});
		}
	 	validate = new Validate({
	 		validateItem:[
               {
	            type:'text',
	            validateObj:'unitRate_x',
	            validateType:['canNull','num'],
	            param:['F','F'],
	            error:['<fmt:message key ="cannot_be_empty" />！！','<fmt:message key ="not_valid_number" />！！']
               }           
	 	    ]
	 		
	 		
	 	});
		
	 	
		</script>
		
		
		
		
	</body>
</html>