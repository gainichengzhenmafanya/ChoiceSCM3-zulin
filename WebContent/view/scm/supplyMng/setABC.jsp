<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:20px;margin-top:30px;">
			<form id="tableForm" method="post" action="<%=path %>/supplyMng/saveABCByUpd.do">
				<table  cellspacing="0" cellpadding="0">
					<tr >
						<td>
							&nbsp;&nbsp;<font style="font-size:2ex;"><b><fmt:message key="please_select_ABC_type" />:</b></font>
							&nbsp;&nbsp;<input type="radio" id="is_a" name="abc" value="A" checked="checked"/>A
							&nbsp;&nbsp;<input type="radio" id="is_b" name="abc" value="B"/>B
					    	&nbsp;&nbsp;<input type="radio" id="is_c" name="abc" value="C"/>C
				    	</td>
					</tr>
				</table>
		    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
		    	<input type="hidden" id="code1" name="code1" value="${code1}"/>
		    	<input type="hidden" id="level" name="level" value="${level}"/>
	    	</form>
    	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	</body>
</html>