<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/supplyMng/selectDeliver.do?spcodeList=${spcodeList}" method="post">
			<table  cellspacing="0" cellpadding="0">
				<tr >
					<td>
						<input type="radio" id="is_cd" name="prdctcard" value="0" 
							<c:if test="${prdctcard=='0'}"> checked="checked"</c:if>
						/><fmt:message key="suppliers_coding" />
						<input type="radio" id="is_des" name="prdctcard" value="1"
							<c:if test="${prdctcard=='1'}"> checked="checked"</c:if> 
						/><fmt:message key="name" />
				    	<input type="radio" id="is_sx" name="prdctcard" value="2"
			    			<c:if test="${prdctcard=='' || prdctcard=='2'}"> checked="checked"</c:if> 
				    	/><fmt:message key="abbreviation" />
				    	<input type="radio" id="is_nm" name="prdctcard" value="3"
				    		<c:if test="${prdctcard=='3'}"> checked="checked"</c:if> 
				    	/><fmt:message key="contact" />
					</td>
		    	</tr>
		    	<tr>
			  		<td width="200">&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="query_information" />:</b></font>
			            <input type="text" value="${inputVal}" id="inputVal" style="text-transform:uppercase;" name="inputVal" style="width:150px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
			    </tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:110px;"><fmt:message key="name" /></td>
								<td style="width:90px;"><fmt:message key="abbreviation" /></td>
								<td style="width:50px;"><fmt:message key="contact" /></td>
								<td style="width:50px;"><fmt:message key="contact" />2</td>
								<td style="width:80px;"><fmt:message key="tel" /></td>
								<td style="width:80px;"><fmt:message key="fax" /></td>
								<td style="width:100px;"><fmt:message key="address" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="deliver" varStatus="step" items="${deliverList}">
								<tr>
									<td class="num" style="width:31px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${deliver.code}" value="${deliver.code}"/>
									</td>
									<td><span title="${deliver.code}" style="width:40px;text-align: left;">${deliver.code}&nbsp;</span></td>
									<td><span title="${deliver.des}" style="width:100px;text-align: left;">${deliver.des}&nbsp;</span></td>
									<td><span title="${deliver.init}" style="width:80px;text-align: left;">${deliver.init}&nbsp;</span></td>
									<td><span title="${deliver.person}" style="width:40px;text-align: left;">${deliver.person}&nbsp;</span></td>
									<td><span title="${deliver.person1}" style="width:40px;text-align: left;">${deliver.person1}&nbsp;</span></td>
									<td><span title="${deliver.tel}" style="width:70px;text-align: left;">${deliver.tel}&nbsp;</span></td>
									<td><span title="${deliver.tax}" style="width:70px;text-align: left;">${deliver.tax}&nbsp;</span></td>
									<td><span title="${deliver.addr}" style="width:90px;text-align: left;">${deliver.addr}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<form id="tableForm" method="post" action="<%=path %>/supplyMng/saveDelByUpd.do">
			<input type="hidden" id="code1" name="code1" value="${code1}"/>
	    	<input type="hidden" id="level" name="level" value="${level}"/>
			<input type="hidden" id="code" name="code" />
	    	<input type="hidden" id="des" name="des" />
	    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				setElementHeight('.grid',['.tool','moduleInfo'],$(document.body),68);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#code').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#des').val($(this).find('td:eq(3)').find('span').attr('title'));
				 });
				var t=$("#inputVal").val();
				$("#inputVal").val("").focus().val(t);
				//判断初次加载如果没有选中标志，默认选择【缩写】
				if(null == '${prdctcard}' || '${prdctcard}' == ''){
					$("#is_sx").attr("checked","checked");
				}
			});
			
			function updDeliver(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size()==1){
					return true;
				}else {
					alert("<fmt:message key="please_select_one_supply" />！");
				}
			}
		</script>
	</body>
</html>