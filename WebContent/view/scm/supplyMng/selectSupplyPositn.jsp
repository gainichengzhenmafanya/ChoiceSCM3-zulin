<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/supplyMng/selectPositnByPage.do" method="post">
	    	<input type="hidden" id="code1" name="code1" value="${code1}"/>
	    	<input type="hidden" id="level" name="level" value="${level}"/>
	    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
			<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="coding"/>:</b></font>
			            <input type="text" id="pcode" name="pcode" style="width: 100px;" class="text" value="<c:out value="${positn.pcode}" />"/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name"/>:</b></font>
			            <input type="text" id="pname" name="pname" style="width: 100px;" class="text" value="<c:out value="${positn.pname}" />"/>
			        </td>
			        <td>&nbsp;
			        	<input type="button" id="search" style="width: 40px;" name="search" value='<fmt:message key ="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:230px;"><fmt:message key="name" /></td>
								<td style="width:90px;"><fmt:message key="area" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" varStatus="step" items="${listPositn}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.code}"/>
									</td>
									<td><span title="${positn.code}" style="width:40px;text-align: left;">${positn.code}&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:220px;text-align: left;">${positn.des}&nbsp;</span></td>
									<td><span title="${positn.area}" style="width:80px;text-align: left;">${positn.area}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />										
		</form>
		<form id="tableForm" method="post" action="<%=path %>/supplyMng/savePstByUpd.do">
			<input type="hidden" id="code" name="code" />
	    	<input type="hidden" id="des" name="des" />
	    	<input type="hidden" id="code1" name="code1" value="${code1}"/>
	    	<input type="hidden" id="level" name="level" value="${level}"/>
	    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
    	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				setElementHeight('.grid',[],$(document.body),45);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('#search').bind('click',function(e){
					$('#listForm').submit();
				});
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#code').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#des').val($(this).find('td:eq(3)').find('span').attr('title'));
				 });
			});
			
			function updPositn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size()==1){
					return true;
				}else {
					alert("<fmt:message key="please_select_one_positions" />！");
				}
			}
		</script>
	</body>
</html>