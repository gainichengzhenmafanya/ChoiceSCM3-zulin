<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="supplies_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			 #modDiv{
					border: 1px solid #5A7581;
					margin: 1px 1px 1px 1px;
				 	padding: 1px;
				 	padding-left: -8px;
				 	background-color:#CDDCEE;
				}
			</style>
		</head>
	<body>
		<div class="leftFrame">
			<div id="modDiv">
				<font title="<fmt:message key ="Modify_al_selected_Explain" />"><fmt:message key ="Modify_al_selected_category" />：</font>
				<input type="checkbox" title="<fmt:message key ="Modify_al_selected_Explain" />" <c:if test="${bytyp=='y' }"> checked="checked"</c:if> id="bytyp" name="bytyp" onclick="checktim_btn(this)" value="${bytyp}"/>
	            <input type="hidden" id="selectedsupply" name="selectedsupply" value=""/>
		  	</div>
	        <div id="toolbar"></div>
		    <div class="treePanel">
		    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key ="Material_management" />;method:changeUrl("0","0","")';
	          <c:forEach var="grpTyp" items="${grpTypList}" varStatus="status">
		          	tree.nodes['00000000000000000000000000000000_${grpTyp.code}'] 
		          		= 'text:${grpTyp.code},${grpTyp.des}; method:changeUrl("1","${grpTyp.code}","${grpTyp.des}")';
	          </c:forEach>
	          
	          <c:forEach var="grp" items="${grpList}" varStatus="status">
		          	tree.nodes['${grp.grptyp}_${grp.code}'] 
		          		= 'text:${grp.code},${grp.des}; method:changeUrl("2","${grp.code}","${grp.des}")';
	          </c:forEach>
	
	          <c:forEach var="typ" items="${typList}" varStatus="status">
		          	tree.nodes['${typ.grp}_${typ.code}'] 
 		          		= 'text:${typ.code},${typ.des}; method:changeUrl("3","${typ.code}","${typ.des}")';//谢昂修改，此行原来为“= 'text:${typ.code},${typ.des}; method:clearContent()';”，因为clearContent()方法没有故删除。
	          </c:forEach>
	          
	          tree.setIconPath("<%=path%>/image/tree/none/");
	          document.write(tree.toString());
	        </script>
		    </div>
	    </div>
	    <div class="mainFrame">
	      <iframe src='<%=path%>/supplyMng/table.do?type=${type}' frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
	    
	    <input type="hidden" id="level" name="level" />
	    <input type="hidden" id="code" name="code" />
	    <input type="hidden" id="sp_type" name="sp_type" />
	    <input type="hidden" id="type" name="type" value="${type}"/>
    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			function checktim_btn(obj){
				if($('#selectedsupply').val() == 0){
					if(obj.checked){
						$("#bytyp").val("y");
						window.document.getElementById("mainFrame").contentWindow.loadToolBar([true,true,true]);
					}else {
						$("#bytyp").val("");
						window.document.getElementById("mainFrame").contentWindow.loadToolBar([true,true,false]);
					}
				} else {
					$('#bytyp').attr('checked',false);
					alert('<fmt:message key="Currently_selected_materials_not_to_choose_any_of_the_categories" />！');
				}
			}
			
			function changeUrl(level,code,typ){
				$('#level').val(level);
				$('#code').val(code);
				$('#sp_type').val(typ);
				window.mainFrame.location = "<%=path%>/supplyMng/table.do?level="+level+"&code="+code+"&type="+${type};
		    }
				
		    function refreshTree(){
				window.location.href = '<%=path%>/supplyMng/list.do?type='+${type};
		    }
		    
		    function pageReload(){
		    	window.location.href = '<%=path%>/supplyMng/list.do?supplyId='+$("#supplyId").val();
		    }
			$(document).ready(function(){
				/* if ("1"==$('#type').val()) {
					$('#modDiv').show();
				} */
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						}
					]
				});
				
				setElementHeight('.treePanel',['#toolbar'],$(document.body),50);
			});
		</script>

	</body>
</html>