<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>						
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supplyMng/table.do" method="post">
		    <%-- <input type="hidden" id="bytyp" name="bytyp" value="${bytyp}"/> --%>
		    <input type="hidden" id="level" name="level" value="${level}"/>
		    <input type="hidden" id="code" name="code" value="${code}"/>
		    <input type="hidden" id="delcode" name="delcode" />
	    	<input type="hidden" id="deldes" name="deldes" />
		    <input type="hidden" id="checkboxList" name="checkboxList" />
		    <input type="hidden" id="type" name="type" value="${type}"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:100px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:170px;"><fmt:message key="name" /></span></td>
								<td><span style="width:90px;"><fmt:message key="supplyclassification"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:130px;"><fmt:message key="default_position" /></span></td>
								<td><span style="width:200px;"><fmt:message key="default_supplier" /></span></td>
								<td><span style="width:90px;"><fmt:message key="shelf" /></span></td>
								<td><span style="width:30px;">ABC</span></td>
								<td><span style="width:60px;"><fmt:message key="if_the_batch_management"/></span></td>
								<td><span style="width:30px;"><fmt:message key="the_period_of_validity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="supplyCanType"/></span></td>
								<td><span style="width:60px;"><fmt:message key="supplyattr"/></span></td>
								<td><span style="width:80px;">标准原料</span></td>
								<td><span style="width:60px;">标准原料转换率</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="status" items="${supplyList}">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code}" value="${supply.sp_code}"/></span>
									</td>
									<td><span title="${supply.sp_code}" style="width:100px;text-align: left;">${supply.sp_code}&nbsp;</span></td>
									<td><span title="${supply.sp_name}" style="width:170px;text-align: left;">${supply.sp_name}&nbsp;</span></td>
									<td><span title="${supply.typ_eas}" style="width:90px;text-align: left;">${supply.typ_eas}&nbsp;</span></td>
									<td><span title="${supply.sp_desc}" style="width:90px;text-align: left;">${supply.sp_desc}&nbsp;</span></td>
									<td><span title="${supply.unit}" style="width:50px;text-align: left;">${supply.unit}&nbsp;</span></td>
									<td><span title="${supply.positndes}" style="width:130px;text-align: left;">${supply.positndes}&nbsp;</span></td>
									<td><span title="${supply.deliverdes}" style="width:200px;text-align: left;">${supply.deliverdes}&nbsp;</span></td>
									<td><span title="${supply.positn1}" style="width:90px;text-align: left;">${supply.positn1}&nbsp;</span></td>
									<td><span title="${supply.abc}" style="width:30px;text-align: left;">${supply.abc}&nbsp;</span></td>
									<td><span title="${supply.ynbatch}" style="width:60px;text-align: left;">${supply.ynbatch}&nbsp;</span></td>
									<td><span title="${supply.sp_per1}" style="width:30px;text-align: right;">${supply.sp_per1}&nbsp;</span></td>
									<td><span title="${supply.typothdes}" style="width:60px;text-align: left;">${supply.typothdes}&nbsp;</span></td>
									<td><span title="${supply.attribute}" style="width:60px;text-align: left;">${supply.attribute}&nbsp;</span></td>
									<td><span title="${supply.sp_name_x}" style="width:80px;text-align: left;">${supply.sp_name_x}&nbsp;</span></td>
									<td><span title="${supply.unitRate_x}" style="width:60px;text-align: right;">${supply.unitRate_x}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
							<td><input type="text" id="sp_init" name="sp_init" class="text" onkeyup="ajaxSearch('sp_init')" style="text-transform:uppercase;" value="${supply.sp_init}" /></td>
							<td class="c-left"><fmt:message key="supplies_no" />：</td>
							<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${supply.sp_code}" /></td>
							<td class="c-left"><fmt:message key="supplies_name" />：</td>
							<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${supply.sp_name}" /></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			var t;
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//排序start
				var array = new Array();      
				array = ['sp_code','sp_name','typ_eas','sp_desc', 'unit','positndes' , 'deliverdes','positn1','abc','ynbatch','sp_per1','typothdes','attribute'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								 $('.search-div').slideToggle(100);
								 var t = $('#sp_init').val();
								 $('#sp_init').focus().val(t);
							}
						},"-",{
							text: '<fmt:message key="modify_the_default_supplier" />',
							title: '<fmt:message key="modify_the_default_supplier" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectDeliver();
							}
						},{
							text: '<fmt:message key="modify_positions" />',
							title: '<fmt:message key="modify_positions" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectPositn();
							}
						},{
							text: '<fmt:message key="modify_shelves" />',
							title: '<fmt:message key="modify_shelves" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectPositn1();
							}
						},{
							text: '<fmt:message key="ABC_set" />',
							title: '<fmt:message key="ABC_set" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								setABC();
							}
						},{
							text: '<fmt:message key="supplyclassification"/>',
							title: '<fmt:message key="supplyclassification"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								classification();
							}
						},{
							text: '<fmt:message key="update"/><fmt:message key="supplyCanType"/>',
							title: '<fmt:message key="update"/><fmt:message key="supplyCanType"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								batchTypoth("typoth");
							}
						},{
							text: '<fmt:message key ="Enable_batch_management" />',
							title: '<fmt:message key ="Enable_batch_management" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								batchTypoth("ynbatch");
							}
						},{
							text: '<fmt:message key="the_period_of_validity"/><fmt:message key="update"/>',
							title: '<fmt:message key="the_period_of_validity"/><fmt:message key="update"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								batchTypoth("sp_per1");
							}
						},{
							text: '<fmt:message key="modify_attribute"/>',
							title: '<fmt:message key="modify_attribute"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								modifyAttribute();
							}
						},{
							text: '<fmt:message key="processing_room"/><fmt:message key="The_material" /><fmt:message key="attribute" />',
							title: '<fmt:message key="processing_room"/><fmt:message key="The_material" /><fmt:message key="attribute" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								updateExkc();
							}
						},{
							text: '修改标准原料',
							title: '修改标准原料',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectSupply();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							}
						}]
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 

				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//获取已经选中的物资
				function getCheckList(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					alert(checkboxList.filter(':checked').size());
					return checkboxList.filter(':checked').size();
				}
				//点击事件
				function checktim_btn(obj){
					if(window.parent.$("#bytyp").val() == 'y'){
						alert('<fmt:message key="Currently_all_material_categories_have_been_selected_not_to_choose_the_material_alone" />！');
						return false;
					}
					return true;
				}
				var index = 0;
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				    	 $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				    	 index -= 1;
				    	 window.parent.$("#selectedsupply").val(index);
				     } else {
				    	 if(checktim_btn($(this).removeClass("bgBlue").find(":checkbox"))){
				         	$(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				    	 } else {
				    		$(this).addClass("bgBlue").find(":checkbox").attr("checked", false);
				    	 }
				    	 index += 1;
				    	 window.parent.$("#selectedsupply").val(index);
				     }
				 });
			});
			function selectDeliver() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/selectDeliver.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				
				
				customWindow = $('body').window({
					id: 'window_selectDeliver',
					title: '<fmt:message key="modify_supplier" />',
					content: '<iframe id="supplyDeliverFrame" name="supplyDeliverFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '700px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyDeliverFrame')&&window.document.getElementById("supplyDeliverFrame").contentWindow.updDeliver()){
										submitFrameForm('supplyDeliverFrame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function selectPositn() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
							alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/selectPositnByPage.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key="update" /><fmt:message key="positions_information" />',
					content: '<iframe id="supplyPositnFrame" name="supplyPositnFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '700px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyPositnFrame')&&window.document.getElementById("supplyPositnFrame").contentWindow.updPositn()){
										submitFrameForm('supplyPositnFrame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}

			function selectPositn1() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/selectPositn1.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_selectPositn1',
					title: '<fmt:message key="modify_shelf_information" />',
					content: '<iframe id="supplyPositn1Frame" name="supplyPositn1Frame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '200px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_shelf_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyPositn1Frame')&&window.document.getElementById("supplyPositn1Frame").contentWindow.validate._submitValidate()){
										submitFrameForm('supplyPositn1Frame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function selectSupply(){//修改标准物料
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
							alert('请选择至少一个物资或者选择按类别修改！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/updateSupply.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_selectSuppply_x',
					title: '修改标准原料',
					content: '<iframe id="supplyFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '700px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyFrame')&&window.document.getElementById("supplyFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('supplyFrame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
				
			}
			
			/*批量修改物资参考类别 wangjie 2014年12月29日*/
			function batchTypoth(menuname){
				//获取选择的物资
				var supply = [];
				$('.grid').find('.table-body').find(':checkbox').map(function(){
					if($(this).attr("checked")){
						supply.push($(this).val());
					}
				});
				if(supply.length == 0){
					alert('<fmt:message key ="please_select_one_material" />！');
					return;
				}
				var action = "<%=path%>/supply/toBatchTypoth.do?checkedcode="+supply.join(",")+"&menuname="+menuname;
				var divname = "";
				if(menuname == "typoth"){
					divname = "<fmt:message key ="Batch_modify_reference_class" />";
				}else if(menuname == "sp_per1"){
					divname = "<fmt:message key ="Effective_maintenance" />";
				}else{
					divname = "<fmt:message key ="Batch_enable_batch_management" />";
				}
				$('body').window({
					title: divname,
					content: '<iframe frameborder="0" id="saveWarnDaysFrame" name="saveWarnDaysFrame" src="'+action+'"></iframe>',
					width: '400px',
					height: '120px',
					draggable: true,
					isModal: true,
					topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" /><fmt:message key="suppliers_message" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(menuname == "sp_per1"){
									if(getFrame('saveWarnDaysFrame')&&window.document.getElementById("saveWarnDaysFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('saveWarnDaysFrame','DeliverForm');
									}
								}
								else if(menuname == "typoth"){
									if(getFrame('saveWarnDaysFrame')&&window.document.getElementById("saveWarnDaysFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('saveWarnDaysFrame','DeliverForm');
									}
								}
								else{
									submitFrameForm('saveWarnDaysFrame','DeliverForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
					}
				});
			}
			
			function setABC() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/setABC.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_setABC',
					title: '<fmt:message key="set_ABC_type" />',
					content: '<iframe id="supplyABCFrame" name="supplyABCFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="set_ABC_type" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyABCFrame')){
										submitFrameForm('supplyABCFrame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function classification() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/codeDes/classification.do?spcode="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val();
				customWindow = $('body').window({
					id: 'window_classiFicatione',
					title: '<fmt:message key ="supplyclassification" />',
					content: '<iframe id="supplyClassiFicationFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true
				});
			}
			
			//批量修改物资属性
			//add by jin shuai at 20160229
			function modifyAttribute(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
						alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
						return;
					}
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					path="<%=path%>/supplyMng/modifyAttribute.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
					customWindow = $('body').window({
						id: 'window_modifyAttribute',
						title: '<fmt:message key="modify_attribute" />',
						content: '<iframe id="modifyAttributeFrame" name="modifyAttributeFrame" frameborder="0" src="'+path+'"></iframe>',
						width: '550px',
						height: '350px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="enter" />',
									title: '<fmt:message key="modify_attribute" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('modifyAttributeFrame')&&window.document.getElementById("modifyAttributeFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('modifyAttributeFrame','tableForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
			
			//批量修改物资是否领料属性
			function updateExkc(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyMng/updateExkc.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_updateExkc',
					title: '<fmt:message key="update" /><fmt:message key="The_material" /><fmt:message key="attribute" />',
					content: '<iframe id="updateExkc" name="updateExkc" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '350px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('updateExkc')){
										submitFrameForm('updateExkc','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>