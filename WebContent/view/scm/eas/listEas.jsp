<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
				.search-div .form-line .form-label{
					width: 10%;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/eas/list.do" id="queryForm" name="queryForm" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:180px;">内容</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td><span class="num" style="width: 25px;">1</span></td>
								<td><span style="width:30px; text-align: center;">
									<input type="checkbox"  name="idList" id="chk_1' />" value="supply"/></span>
								</td>

								<td><span style="width:180px;" title="物资编码">物资编码&nbsp;</span></td>
							</tr>
							<tr>
								<td><span class="num" style="width: 25px;">2</span></td>
								<td><span style="width:30px; text-align: center;">
									<input type="checkbox"  name="idList" id="chk_2' />" value="grptyp"/></span>
								</td>

								<td><span style="width:180px;" title="物资大类">物资大类&nbsp;</span></td>
							</tr>
							<tr>
								<td><span class="num" style="width: 25px;">3</span></td>
								<td><span style="width:30px; text-align: center;">
									<input type="checkbox"  name="idList" id="chk_3' />" value="grp"/></span>
								</td>

								<td><span style="width:180px;" title="物资中类">物资中类&nbsp;</span></td>
							</tr>
							<tr>
								<td><span class="num" style="width: 25px;">4</span></td>
								<td><span style="width:30px; text-align: center;">
									<input type="checkbox"  name="idList" id="chk_4' />" value="typ"/></span>
								</td>

								<td><span style="width:180px;" title="物资中类">物资小类&nbsp;</span></td>
							</tr>
							<tr>
								<td><span class="num" style="width: 25px;">3</span></td>
								<td><span style="width:30px; text-align: center;">
									<input type="checkbox"  name="idList" id="chk_5' />" value="bhtyp"/></span>
								</td>

								<td><span style="width:180px;" title="报货分类">报货分类&nbsp;</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			  invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				var tool = $('.tool').toolbar({
					items: [{
							text: '物资同步',
							title: '物资同步',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								getEas();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }
					     else
					     {
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
				
			});
			
			function getEas(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("确认同步选中内容么?同步操作将删除原来的数据！！！！")){
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							codeValue.push($.trim($(this).val()));
						});
						var action = '<%=path%>/eas/updateToEas.do?code='+codeValue.join(",");
						$('body').window({
							title: '数据同步',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('请选择需要同步的内容！');
					return ;
				}
			}
		</script>
	</body>
</html>