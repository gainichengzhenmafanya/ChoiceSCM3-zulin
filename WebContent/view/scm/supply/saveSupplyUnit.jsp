<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.userInfo,.accountInfo {
				position: relative;
				top: 1px;
				background-color: #E1E1E1;
			}
			
			.userInfo {
				height: 91px;
				line-height: 91px;
			}
			
			.accountInfo {
				height: 91px;
				line-height: 91px;
			}
			
			.accountInfo .form-label{
				width: 80%;
			}
			
		</style>
	</head>
	<body>
	<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:20px;"><!-- http://222.175.157.61:8079/browse/JAVASCM-833 -->
			<form id="supplyUnitForm" method="post" action="<%=path %>/supply/saveUnitByAdd.do">
				<input type="hidden" id="sp_code" name="sp_code" value="${supplyUnit.sp_code}"/>
				<input type="hidden" id="unit1" value="${unit.unit}"/>
				<input type="hidden" id="units" value="${units}"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="unit" />：</div>
					<div class="form-input">
						<select class="select" id="unit" name="unit"  style="width:133px">
							<c:forEach var="unit" items="${unitList}" varStatus="status">
								<option id="${unit.code}" value="${unit.des}">${unit.des}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<c:if test="${units=='0'}">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="unit_conversion_rate" />：</div>
					<div class="form-input">
						<input type="text" id="unitper" name="unitper" readonly="readonly" class="text" value="1"/>
					</div>
				</div>
				</c:if>
				<c:if test="${units!='0'}">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="unit_conversion_rate" />：</div>
					<div class="form-input">
						<input type="text" id="unitper" name="unitper" onfocus="onOver()" class="text" />
<!-- 						<a ></a> -->
						<label id="txt" ></label>
					</div>
				</div>
				</c:if>
			</form>
		</div>
	</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<%-- 		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script> --%>
<%-- 		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script> --%>
<%-- 		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script> --%>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'unit',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'unitper',
						validateType:['canNull','maxLength','decmal'],
						param:['F',5,'F'],
						error:['<fmt:message key="cannot_be_empty" />！','<fmt:message key="length_too_long" />！','<fmt:message key ="please_enter_a_number_greater" />']
					}]
				});
				
			});
			
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
			}
			function onOver(){
				var a = '(?'+$('#unit').val()+'=1'+$('#unit1').val()+')';
				$("#txt").html(a);
			}
		</script>
	</body>
</html>