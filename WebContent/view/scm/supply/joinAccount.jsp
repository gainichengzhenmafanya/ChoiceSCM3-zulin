<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="account_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<style type="text/css">
				fieldset{
					padding: 5px;
					margin: 10px 6px;
					border: 1px solid #868686;
				}
				
				legend{
					font-size: 13px;
					font-weight: bold;
					font-style: italic;
					cursor: pointer;
					border: 1px solid #868686;
					padding-left: 20px;
				}
				
				.border_hover{
					border: 2px solid #4F98F6;
				}
				
				.color_hover{
					background-color: #4F98F6;
					color: #FFFFFF;
				}
				
				#info{
					position: relative;
					overflow: auto;
				}
				.form-line,
				.form-line .form-label,
				.form-line .form-input {
					height: 26px;
				}
				
				.accountState_0 {
					background:url(<%=path %>/image/off.gif) no-repeat 7px center;
				}
				
				.accountState_1 {
					background:url(<%=path %>/image/on.gif) no-repeat 7px center;
				}
				
			</style>
	</head>
	<body>
		<div id="toolbar"></div>
		<div id="info">
			<input type="hidden" id="ids" name="ids" value="${ids}"></input>
			<c:forEach var="user" items="${userList}">
				<fieldset>
					<legend class="sex_${user.sex}" id="${user.id}">${user.name}【${user.code}】</legend>
					<div class="userInfo">
						<div class="form-line">
							<div class="form-label"><fmt:message key="sector"/></div>
							<div class="form-input">
								${user.department.name}
							</div>
						</div>
					</div>
					<div class="accountInfo">
						<div class="grid">
							<div class="table-head">
								<table cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<td class="num">&nbsp;</td>
											<td class="chk">
												<input type="checkbox" id="chkAll_${user.id}" class="${user.id}"/>
											</td>
											<td style="width:150px;"><fmt:message key="account_name"/></td>
											<td style="width:120px;"><fmt:message key ="name" /></td>
											<td style="width:80px;"><fmt:message key="status"/></td>
											<td style="width:100px;"><fmt:message key ="current-store" /></td>
											<td style="width:200px;"><fmt:message key="Subordinate_role" /></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table cellspacing="0" cellpadding="0">
									<tbody>
										<c:forEach var="account" items="${user.accountList}" varStatus="status">
											<tr>
												<td class="num">${status.index+1}</td>
												<td class="chk">
													<input type="checkbox" class="${user.id}"  id="chk_<c:out value='${account.id}' />" value="<c:out value='${account.id}' />"/>
												</td>
												<td>
													<span style="width:140px;"><c:out value="${account.name}" />&nbsp;</span>
												</td>
												<td>
													<span style="width:110px;"><c:out value="${account.names}" />&nbsp;</span>
												</td>
												<td class="center" id="state_${account.id}">
													<span style="width:70px;" class="accountState_${account.state}"><c:out value='${ACCOUNT_STATE[account.state]}' />&nbsp;</span>
												</td>
												<td class="center">
													<span style="width:90px;" title="${account.positn.code}">${account.positn.des}&nbsp;</span>
												</td>
												<td class="left">
													<span style="width:190px;">
														<c:forEach var="role" items="${account.roleList}">
															${role.name}&nbsp;
														</c:forEach>
													</span>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</c:forEach>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/custom.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function(){
			var tool = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="enter" />',
						title: '<fmt:message key="enter" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-120px','0px']
						},
						handler: function(){
							joinAccount();
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}]
			});
			
			//设置列表区域的整体高度
			setElementHeight('#info',['#toolbar'],"",26);
			
			$('.userInfo').width($('.form-line').width());
			
			//设置账号列表信息的宽度
		  $('.accountInfo').width($('fieldset').width() - $('.userInfo').width() - 2);

			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//展开或者折叠账号列表
			$('legend').bind('click',function(){
				var $self = $(this);
				if($self.siblings().is(':hidden')){
					$self.siblings().slideDown('normal');
				}else if($self.siblings().is(':visible')){
					$self.siblings().slideUp('normal');
				}
			});
			
			$('legend').bind('click',function(){
				var $self = $(this);
				
				$('#userId').val($self.attr('id'));
				
				//设置选中的样式
				$('fieldset').removeClass('border_hover');
				$('legend').removeClass('border_hover color_hover');
				$self.parent().addClass('border_hover');
				$self.addClass('border_hover color_hover');
			});
			
			//全选多选框事件
			$(':checkbox[id^="chkAll_"]').click(function(){
				//选择右侧所有的checkbox
				$('.'+$(this).attr('class'),$('.table-body')).attr('checked',$(this).attr('checked'));
			});
			//全选同步
			$(':checkbox[id^="chk_"]').click(function(){
				var $tmp=$('.'+$(this).attr('class')+':gt(0)');
				$('#chkAll_'+$(this).attr('class')).attr('checked',$tmp.length==$tmp.filter(':checked').length);
			});
		});	//end $(document).ready();
		
		function joinAccount(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
				if(confirm('<fmt:message key="Determines_the_selected_material_to_be_associated_with_these_users" />？')){
					var codeValue=[];
					checkboxList.filter(':checked').each(function(){
						codeValue.push($(this).val());
					});
					var url = '<%=path%>/supply/joinAccount.do?accountids='+codeValue.join(",")+'&ids='+$('#ids').val();
					$("#wait").show();
					$('#wait2').show();
					$.ajax({
						type:'post',
						url:url,
						success:function(data){
							$("#wait").hide();
							$('#wait2').hide();
							if(data == 'OK'){
								alert('<fmt:message key="successful_added"/>！');
								parent.$('.close').click();//成功就关闭页面 wjf
							}else{
								alert('<fmt:message key="fail_added"/>！');
							}
						}
					});
				}
			}else{
				alert("<fmt:message key="Choose_the_user_to_associate" />");
			}
		}
		</script>
	</body>
</html>