<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>suppply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/costbom/DeleteCostbom_x.do" method="post">
			<input  type="hidden" id="item" name="item" value="${costbom.item}" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:21px;">&nbsp;</span></td>
								<td><span style="width:20px;text-align: center;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:80px;">虚拟编码</span></td>
								<td><span style="width:80px;">虚拟名称</span></td>
								<td><span style="width:50px;">取料率</span></td>
								<td><span style="width:40px;">单位</span></td>
								<td><span style="width:80px;">净用量</span></td>
								<td><span style="width:60px;">毛用量</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="costbom" varStatus="step" items="${costbomList}">
								<tr>
									<td class="num"><span style="width:21px;">${step.count}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${costbom.supply.sp_code_x}" value="${costbom.supply.sp_code_x}"/></span>
									</td>
									<td><span title="${costbom.supply.sp_code_x}" style="width:80px;">${costbom.supply.sp_code_x}&nbsp;</span></td>
									<td><span title="${costbom.supply.sp_name_x}" style="width:80px;">${costbom.supply.sp_name_x}&nbsp;</span></td>
									<td><span title="${costbom.exrate}" style="width:50px;">${costbom.exrate}&nbsp;</span></td>
									<td><span title="${costbom.supply.unit_x}" style="width:40px;">${costbom.supply.unit_x}&nbsp;</span></td>
									<td><span title="${costbom.excnt}" style="width:80px;text-align: right;">${costbom.excnt}&nbsp;</span></td>
									<td><span title="${costbom.cnt2}" style="width:60px;text-align: right;" >${costbom.cnt2}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			var t;
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
			     if ($(this).find(':checkbox')[0].checked) {
			    	 $(this).find(':checkbox').attr("checked", false);
			     }else{
			    	 $(this).find(':checkbox').attr("checked", true);
			     }
			 });
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body));	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});	
			//批量删除菜品
			function deleteCostbomByIds(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==0){
					if(!confirm('没有选择任何信息，是否继续？'))
						return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action = '<%=path%>/costbom/deleteCostbom_x.do?ids='+chkValue.join(",");
	 			$('#listForm').attr("action",action);
	 			$('#listForm').submit();
			}
		</script>
	</body>
</html>