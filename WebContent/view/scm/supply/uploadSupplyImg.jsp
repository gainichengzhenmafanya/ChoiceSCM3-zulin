<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	</head>
	<body>
		<div class="tool">
		</div>
		<div class="form">
			<form id="SupplyForm" method="post" action="<%=path %>/supply/uploadSupplyImg.do" enctype="multipart/form-data">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_code" />：</div>
						<div class="form-input">
							<input type="text" name="sp_code" class="text"  value="${supply.sp_code}" disabled="disabled"/>
							<input type="hidden" id="sp_code" name="sp_code" class="text"  value="${supply.sp_code}" />
						</div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="supplies_name" />：</div>
						<div class="form-input">
							<input type="text" name="sp_name" class="text"  value="${supply.sp_name}" disabled="disabled"/>
							<input type="hidden" id="sp_name" name="sp_name" class="text"  value="${supply.sp_name}"/>
						</div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="Upload_pictures" />：</div>
						<div class="form-input">
							<span><input type="file" id="image" name="image"/></span>
						</div>
					</div>
					<c:if test="${!empty message }">
						<div class="form-line">
						    <div class="form-label"></div>
							<div class="form-input">
								<span style="color:red">${message }</span>
							</div>
						</div>
					</c:if>
					<div class="form-line">
						<div class="form-label"><span><fmt:message key ="the_picture" />：</span></div>
						<div class="form-input">
							<c:choose>
								<c:when test="${fileExists == 'Y' }">
									<span><img id="img" src="" style="width:200px;height:200px;" /></span>
								</c:when>
								<c:otherwise>
									<span><fmt:message key="The_picture_has_not_been_uploaded" />！</span>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
			</form>
		</div>
		<form id="supplyForm2" name="supplyForm2" action="<%=path%>/supply/toUploadSupplyImg.do" method="post">
			<input type="hidden" name="sp_code" id="sp_code2"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			var message = '${message}';
			if(message == '上传成功'){
				alert(message);
				invokeClick($(window.parent.document).find('.close'));
			}
			$("#img").attr('src','<%=path%>/image/scm/supply/<c:out value='${supply.sp_code}'/>.jpg?'+Math.random());
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var file = $("#image").val();
						if(file != ""){	
							var pos = file.lastIndexOf(".");
							var lastname = file.substring(pos,file.length);
							if (!(lastname.toLowerCase()==".jpg" 
									/* || lastname.toLowerCase()==".bmp"
										|| lastname.toLowerCase()==".jpeg"
											|| lastname.toLowerCase()==".png"
												|| lastname.toLowerCase()==".gif" */)){
							   alert('<fmt:message key="You_upload_the_file_type_for" />'+lastname+'，<fmt:message key="File_must_be.Jpg_type" />');
							   return;
							}
						}else{
							alert('<fmt:message key="Images_can_not_be_empty" />！');
							return;
						}
						$("#SupplyForm").submit();
					}
				},{
					text: '<fmt:message key="On_a_material" />',
					title: '<fmt:message key="On_a_material" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						preSupply();
					}
				},{
					text: '<fmt:message key="Next_material" />',
					title: '<fmt:message key="Next_material" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						nextSupply();
					}
				},{
					text: '<fmt:message key="cancel" />',
					title: '<fmt:message key="cancel" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.document).find('.close'));
					}
				}]
			});
		});
		//上一个物资
		function preSupply(){
			var sp_code = $("#sp_code").val();
			var sp_code2 = parent.preSupply(sp_code);
			if(sp_code2 == null || sp_code2 == '' || sp_code2 == 'undefined'){
				alert('<fmt:message key="Its_the_first_one" />！');
				return;
			}
			$("#sp_code2").val(sp_code2);
			$("#supplyForm2").submit();
		}
		//下一个物资
		function nextSupply(){
			var sp_code = $("#sp_code").val();
			var sp_code2 = parent.nextSupply(sp_code);
			if(sp_code2 == null || sp_code2 == '' || sp_code2 == 'undefined'){
				alert('<fmt:message key="Its_the_last_one" />！');
				return;
			}
			$("#sp_code2").val(sp_code2);
			$("#supplyForm2").submit();
		}
		</script>
	</body>
</html>