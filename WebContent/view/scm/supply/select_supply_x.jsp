<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查询虚拟物资</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="" method="post">
			<div class="search-div">
					<div class="search-condition">
						<table class="search-table" cellspacing="0" cellpadding="0">
							<tr>
								<td class="c-left">虚拟物料编码：</td>
								<td><input type="text" id="sp_code_x" name="sp_code_x" class="text" value="${querySupply.sp_code_x }" /></td>
								<td class="c-left">虚拟物料名称：</td>
								<td><input type="text" id="sp_name_x" name="sp_name_x" class="text" value="${querySupply.sp_name_x }" /></td>
								<td class="c-left">虚拟物料缩写：</td>
								<td><input type="text" id="sp_init_x" name="sp_init_x" class="text" value="${querySupply.sp_init_x }" /></td>
		       					<td><input type="button" style="width:40px;" class="search-button" id="search" value='<fmt:message key="select" />'/></td>
							</tr>
						</table>
					</div>
				</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									&nbsp;
								</td>
								<td style="width:150px;"><fmt:message key="coding" /></td>
								<td style="width:150px;"><fmt:message key="name" /></td>
								<td style="width:100px;"><fmt:message key="abbreviation" /></td>
								<td style="width:50px;"><fmt:message key="unit" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code_x}" value="${supply.sp_code_x}"/>
									</td>
									<td><span title="${supply.sp_code_x}" style="width:140px;text-align: left;"><c:out value="${supply.sp_code_x}" />&nbsp;</span></td>
									<td><span title="${supply.sp_name_x}" style="width:140px;text-align: left;"><c:out value="${supply.sp_name_x}" />&nbsp;</span></td>
									<td><span title="${supply.sp_init_x}" style="width:90px;text-align: left;"><c:out value="${supply.sp_init_x}" />&nbsp;</span></td>
									<td><span title="${supply.unit_x}" style="width:40px;text-align: left;"><c:out value="${supply.unit_x}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="sp_code" name="sp_code" class="text"  value=""/>
<!-- 			<input type="hidden" id="sp_name_x" name="sp_name_x" class="text"  value=""/>  -->
<!-- 			<input type="hidden" id="unit_x" name="unit_x" class="text"  value=""/> -->
<!-- 			<input type="hidden" id="sp_init_x" name="sp_init_x" class="text"  value=""/> 		 -->
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				focus() ;//页面获得焦点
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 		if(e.keyCode==13){
			 			$('#search').click();
			 		}
				}); 
				
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//------------------------------
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								select_Typ();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.parent.$('.close').click();
							}
						}
					]
				});
			function select_Typ(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() ==1){ 
						var tr = checkboxList.filter(':checked').parents('tr');
						var sp_code_x = tr.find('td').eq(2).find('span').attr('title');
// 						var sp_name_x = tr.find('td').eq(3).find('span').attr('title');
// 						var sp_init_x = tr.find('td').eq(4).find('span').attr('title');
// 					 	var unit_x = tr.find('td').eq(5).find('span').attr('title');
					 	$('#sp_code').val(sp_code_x);
// 					 	parent.$('#sp_code_x').val());
// 					 	$('#unit_x').val(unit_x);
// 					 	$('#sp_init_x').val(sp_init_x);  
				}
				top.customWindow.afterCloseHandler('Y');
				top.closeCustom();
				}
		</script>
	</body>
</html>