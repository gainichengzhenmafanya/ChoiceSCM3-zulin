<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>unit Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
				.search-div .form-line .form-label{
					width: 10%;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/supply/tableUnit.do" id="queryForm" name="queryForm" method="post">
			<input type="hidden" id="sp_code" name="sp_code" value="${supplyUnit.sp_code}"/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;padding-left:8px">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;"><fmt:message key ="unit" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="unit_conversion_rate" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supplyUnit" items="${listSupplyUnit}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;padding-left:8px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" value="${supplyUnit.id}"/></span>
									</td>
									<td><span style="width:100px;" title="${supplyUnit.unit}"><c:out value="${supplyUnit.unit}" /></span></td>
									<td><span style="width:80px;" title="${supplyUnit.unitper}"><c:out value="${supplyUnit.unitper}" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key ="supplyunit" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveUnit();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" /><fmt:message key ="supplyunit" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateUnit();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key ="supplyunit" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteUnit();
							}
						},{
							text: '<fmt:message key ="enter" />',
							title: '<fmt:message key ="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								var option = '<option value="'+'"></option>';
								parent.$('#unit').empty();
								parent.$('#unit').append(option);
// 								parent.$('#unit1').empty();
// 								parent.$('#unit1').append(option);
								parent.$('#unit2').empty();
								parent.$('#unit2').append(option);
								parent.$('#unit3').empty();
								parent.$('#unit3').append(option);
								parent.$('#unit4').empty();
								parent.$('#unit4').append(option);
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								checkboxList.each(function(i){
									var unit = $(this).closest('tr').find('td:eq(2)').find('span').text();
									var unitper=$(this).closest('tr').find('td:eq(3)').find('span').text();
									var option = '<option value="'+unit+'" unitper="'+unitper+'">'+unit+'</option>';
									parent.$('#unit').append(option);
// 									parent.$('#unit1').append(option);
									parent.$('#unit2').append(option);
									parent.$('#unit3').append(option);
									parent.$('#unit4').append(option);
								});	
								parent.$('.close').click();
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					});
					
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	}
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
			});
			function saveUnit(){
				var url = "<%=path%>/supply/addUnit.do?sp_code="+$('#sp_code').val();
				$('body').window({
					id: 'window_saveUnit',
					title: '<fmt:message key="insert" /><fmt:message key="branches_and_positions_information" />',
					content: '<iframe id="saveUnitFrame" frameborder="0" src='+url+'></iframe>',
					width: '550px',
					height: '450px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveUnitFrame') && getFrame('saveUnitFrame').validate._submitValidate()){
										submitFrameForm('saveUnitFrame','supplyUnitForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			
			function updateUnit(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					$('body').window({
						title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
						content: '<iframe id="updateUnitFrame" frameborder="0" src="<%=path%>/supply/updateUnit.do?id='+aim.val()+'"></iframe>',
						width: '550px',
						height: '450px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateUnitFrame') && getFrame('updateUnitFrame').validate._submitValidate()){
											submitFrameForm('updateUnitFrame','supplyUnitForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alert('<fmt:message key="please_select_data" />!');
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
				
			}
			
			function viewUnit(){
				 pageReload();
			}
			
			function deleteUnit(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key="delete_data_confirm" />?")){
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							codeValue.push($.trim($(this).val()));
						});
						var action = '<%=path%>/supply/deleteUnitByIds.do?ids='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key ="delete" /><fmt:message key="branches_and_positions_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
		</script>
	</body>
</html>