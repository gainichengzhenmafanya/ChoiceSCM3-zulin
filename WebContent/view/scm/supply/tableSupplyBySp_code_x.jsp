<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>suppply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/costbom/saveOrUpdateOrDelCostbom_x.do" method="post">
			<input  type="hidden" name="exrate" id="exrate" value="${costbom.exrate }"/>
			<input  type="hidden" name="excnt" id="excnt" value="${costbom.excnt }"/>
			<input  type="hidden" name="excnts" id="excnts" value="${costbom.excnts }"/>
			<input  type="hidden" id="curStatus" name="curStatus" value="${costbom.curStatus}" />
			<input  type="hidden" id="item" name="item" value="${costbom.item}" />
			<input  type="hidden" name="unit" id="unit" value="${costbom.unit}" />
			<input  type="hidden" name="mods" id="mods" value="${costbom.mods }"/>
			<input  type="hidden" name="sp_code_x" id="sp_code_x" value="${costbom.sp_code_x }"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td style="width:90px;"><span><fmt:message key="supplies_code"/></span></td>
								<td style="width:90px;"><span><fmt:message key="supplies_name"/></span></td>
								<td style="width:70px;"><span><fmt:message key="specification"/></span></td>
								<td style="width:70px;"><span><fmt:message key ="cost_per_unit" /></span></td>
								<td style="width:70px;"><span><fmt:message key="unit_price"/></span></td>
								<td style="width:70px;"><span><fmt:message key ="Cost_quantity" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr>
								   <td class="num" style="width:31px;">${step.count}</td>
								   <td><span title="${supply.sp_code}" style="width:80px;">${supply.sp_code}&nbsp;</span></td>
								   <td><span title="${supply.sp_name}" style="width:80px;">${supply.sp_name}&nbsp;</span></td>
								   <td><span title="${supply.sp_desc}" style="width:60px;">${supply.sp_desc}&nbsp;</span></td>
								   <td><span title="${supply.unit}" style="width:60px;">${supply.unit}&nbsp;</span></td>
								   <td><span title="${supply.sp_price}" style="width:60px;text-align: right;">${supply.sp_price}&nbsp;</span></td>
								   <td><span title="${supply.cnt}" style="width:60px;"><input type='text' id='cnt' value='${supply.cnt}'></input></span></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
		var t;
		
		$(document).ready(function(){
			if ($('#curStatus').val()=='A'){
				var excnt = $('#excnt').val();
				var excnts = $('#excnts').val();
				var checkboxList = $('.grid').find('.table-body').find('.num');
				checkboxList.each(function(){
					$(this).parents('tr').find('td:eq(6)').find('span input').val(excnts);
				});
			}
			focus() ;//页面获得焦点
			$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.parent.$('.close').click();
		 		}
			}); 
			
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'cnt',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="cannot_be_empty"/>！']
				}]
			});
			
			setElementHeight('.grid',['.tool'],$(document.body));	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			changeTh();//拖动 改变table 中的td宽度 
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			var t=$("#sp_init").val();
			$("#sp_init").val("").focus().val(t);
			
			//让之前选中的默认选中
			var defaultCode = '${defaultCode}';
			if(defaultCode!=''){
				$('.grid').find('.table-body').find(':checkbox').each(function(){
					if(this.id.substr(4,this.id.length)==defaultCode){
						$(this).attr("checked", true);
					}
				});	
			}
			$("#listForm").attr("action","<%=path%>/supply/selectSupply.do?level="+'${level}'+"&code="+'${code}'+"&defaultCode="+'${defaultCode}');
		});	
		function selectSupply(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() ==1){
				var tr = checkboxList.filter(':checked').parents('tr');
				var sp_code = tr.find('td:eq(2)').find('span').attr('title');
				var sp_name = tr.find('td:eq(3)').find('span').attr('title');
				var unit =  tr.find('td:eq(6)').find('span').attr('title');
				var unit1 = tr.find('td:eq(8)').find('span').attr('title');
				window.parent.dbSupplySelect(sp_code,sp_name,unit,unit1);
			}else{
				window.parent.dbSupplySelect('','','','');
			}
		}
		function saveTo(){
			var sp_code='';
			var cntt='';//实际物料用量
			$('.grid').find('.table-body').find('tr').each(function(){
				sp_code+=$(this).find('td:eq(1)').find('span').attr('title')+',';
				cntt+=$(this).find('td:eq(6)').find('span').find('input').val()+',';
			});
			if(cntt.indexOf(",,") > -1||cntt.indexOf(",")==0){	
				//$("#curStatus").val("A");
				alert('<fmt:message key="No_corresponding_actual_material" />!');
			}else{
				$("#listForm").attr("action",'<%=path%>/costbom/saveOrUpdateOrDelCostbom_x.do?cntt='+cntt+'&sp_code='+sp_code);
				$('#listForm').submit();
				parent.$('close').click();
			}
		}
		</script>
	</body>
</html>