<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>costbom Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			form .form-line .form-label{
				width: 9%;
			}
			form .form-line .form-input{
				width: 14%;
			}
			form .form-line .form-input input[type=text]{
				width:80%;
			}
		</style>
	</head>
	<body>
		<div style="height:50%;">
		<div class="tool"></div>
			<input type="hidden" id="spcodex" name="spcodex" class="text" value="" />
			<form id="listForm" action="<%=path%>/supply/searchSupply_x.do" method="post">
				<div class="search-div">
					<div class="search-condition">
						<table class="search-table" cellspacing="0" cellpadding="0">
							<tr>
								<td class="c-left">虚拟物料编码：</td>
								<td><input type="text" id="sp_code_x" name="sp_code_x" class="text" value="${supply.sp_code_x}" /></td>
								<td class="c-left">虚拟物料缩写：</td>
								<td><input type="text" id="sp_init_x" name="sp_init_x" class="text" value="${supply.sp_init_x}" /></td>
								<td class="c-left">虚拟物料名称：</td>
								<td><input type="text" id="sp_name_x" name="sp_name_x" class="text" value="${supply.sp_name_x}" /></td>
		       					<td><input type="button" style="width:40px;" class="search-button" id="search" value='<fmt:message key="select" />'/></td>
		       					<td><input type="button" style="width:40px;" class="cancel-button" id="cancel" value='清空'/></td>
							</tr>
						</table>
					</div>
				</div>
			</form>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:21px;">&nbsp;</span></td>
								<td><span style="width:30px;"></span></td>
								<td><span style="width:100px;">虚拟物资编码</span></td>
								<td><span style="width:100px;">虚拟物资名称</span></td>
								<td><span style="width:100px;">虚拟物资缩写</span></td>
								<td><span style="width:100px;">虚拟物资单位</span></td>
							</tr>
						</thead>
					</table>
				</div> 
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:set var="totalSum" value="0"></c:set>
							<c:forEach var="supply" varStatus="step" items="${listSupply_x}">
								<tr>
									<td class="num"><span style="width:21px;">${step.count}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${supply.sp_code_x}' />" value="<c:out value='${supply.sp_code_x}' />"/></span>
									</td>
									<td><span title="${supply.sp_code_x}" style="width:100px;">${supply.sp_code_x}&nbsp;</span></td>
									<td><span title="${supply.sp_name_x}" style="width:100px;">${supply.sp_name_x}&nbsp;</span></td>
									<td><span title="${supply.sp_init_x}" style="width:100px;">${supply.sp_init_x}&nbsp;</span></td>
									<td><span title="${supply.unit_x}" style="width:100px;">${supply.unit_x}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="mainFrame" style="height:50%;width:100%">
		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				$('#cancel').bind("click",function search(){
					$('#sp_code_x').val('');
					$('#sp_init_x').val('');
					$('#sp_name_x').val('');
// 				 	$('#listForm').submit();
				});
				$('#itemNm').val(parent.$('#itemNm').val());
				parent.$('#mods').val($('#mods').val());
				parent.$('#unit').val($('#unit').val());
				$('#unit_sprice').val($(window.parent.document).find('#punit').val()+','+$(window.parent.document).find('#price').val());
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 spcode=$(this).find(":checkbox").val();
					 $('#parentId').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#parentName').val($(this).find('td:eq(3)').find('span').attr('title'));
				 });
				
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 65: $('#autoId-button-101').click(); break;
		                case 69: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 83: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-106').click(); break;
		            }
				}); 
			 	//双单 击事件
				$('.grid .table-body tr').live('click',function(){
					 $('#sp_code_x').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#sp_name_x').val($(this).find('td:eq(3)').find('span').attr('title'));
					 $('#sp_init_x').val($(this).find('td:eq(4)').find('span').attr('title'));
					 $('#spcodex').val($(this).find('td:eq(2)').find('span').attr('title'));
					 var sp_code_x=$(this).find('td:eq(2)').find('span').attr('title');
					 var sp_name_x=$(this).find('td:eq(3)').find('span').attr('title');
					 var sp_init_x=$(this).find('td:eq(4)').find('span').attr('title');
					 var  url="<%=path%>/supply/downSupply.do?sp_code_x="+sp_code_x+"&sp_name_x="+sp_name_x+"&sp_init_x="+sp_init_x;
					 $('#mainFrame').attr('src',encodeURI(url));
				});
				//新增
				var status = $("#curStatus").val();
				//判断按钮的显示与隐藏
				if(status == 'A'){
					loadToolBar([true,true,false,false,false,true,false]);
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
					$('#chk1memo').attr('disabled',false);
				}else{//init
					loadToolBar([true,true,true,false,false,true,true]);
				}
				if($.browser.msie){ 
					if($.browser.version == 8.0){
						setElementHeight('.grid',['.tool'],$(document.body),305);
					} else {
						setElementHeight('.grid',['.tool'],$(document.body),360);
					}
				} else {
					setElementHeight('.grid',['.tool'],$(document.body),330);	//计算.grid的高度
				};
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});

			function submitFrame(){
				$('#listForm').submit();
			}
			
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
						text: '编辑',
						title: '编辑',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							updateSupply_x();
						}
					},{
						text: '<fmt:message key ="quit" />',
						title: '<fmt:message key ="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}]
				});
			};
			//修改虚拟物料
			function updateSupply_x(){
				var sp_code_x = $('#spcodex').val();
				if(sp_code_x == ""){
					alert('请至少选中一条数据！');
					return;
				}
				var curwindow = $('body').window({
					id: 'window_searchChkstom',
					title: '虚拟物资修改',
					content: '<iframe id="searchChkstomFrame" frameborder="0" src="<%=path%>/supply/toUpdateSupply_x.do?sp_code_x='+sp_code_x+'"></iframe>',
					width: '500px',
					height: '400px',
					draggable: true,
					isModal: true
				});
			}
		</script>
	</body>
</html>