<%@page import="net.sf.jasperreports.engine.export.JRXlsExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperExportManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="net.sf.jasperreports.engine.data.JRBeanCollectionDataSource"%>
<%@page import="com.choice.scm.domain.Chkstom"%>
<%@page import="java.util.List"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.util.JRLoader"%>
<%@page import="net.sf.jasperreports.engine.JasperReport"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporter"%>
<%@page import="net.sf.jasperreports.j2ee.servlets.ImageServlet"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRHtmlExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="supplies_code" /><fmt:message key="print" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
				body{overflow: auto;}
			</style>
		</head>
	<body>
		   
      <%
      //定义传入参数，并赋值 
      HashMap<String, Object> parameters = new HashMap<String, Object>(); 
      String report_name=new String("供应链系统物资编码报表"); 
      String report_dw=new String("choice"); 
      String report_time=new String("20120423"); 
      parameters.put("report_name", report_name); 
      parameters.put("report_dw", report_dw); 
      parameters.put("report_time", report_time); 
      //关键语句，在jasper文件的同目录下，生成一个html报表结果文件 
       
     // HTML Export：
      File reportFile = new File(getServletContext().getRealPath("/report/supply/supplyReport.jasper"));
      System.out.print(getServletContext().getRealPath("/report/supply/supplyReport.jasper"));
      JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
      //或直接使用JasperReport的XML文件
    //  JasperReport jasperReport2 = JasperCompileManager.compileReport(getServletContext().getRealPath("/reports/WebappReport.jrxml"));

  	 List  list=(List)request.getAttribute("supplyList");
  	JRBeanCollectionDataSource jdt=new JRBeanCollectionDataSource(list);
      //填充报表
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,jdt);

      //导出HTML格式：
        JRHtmlExporter exporter = new JRHtmlExporter();
        request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE,jasperPrint);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);//--使用报表(JasperPrint)
        exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);//--输出到流
        exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,"../image/px.");
        exporter.exportReport();

    //  PDF Export：
     // byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
    //  response.setHeader("Content-Disposition","attachment;filename=JasperPDF.pdf");
      // 设置页面的输出格式
   //   response.setHeader("Content-Type", "application/pdf");
     // response.setContentLength(bytes.length);
      //ServletOutputStream ouputStream = response.getOutputStream();
      //ouputStream.write(bytes, 0, bytes.length);
     // ouputStream.flush();
     // ouputStream.close();
     
     
    // XLS Export：
    //response.setHeader("Content-Disposition","attachment;filename=JasperExcle.xls");
    //  response.setContentType("application/vnd.ms-excel");
    //String xlsFileName="supply";
	//ServletOutputStream outs = response.getOutputStream();
	//JRXlsExporter exporter = new JRXlsExporter();
	//exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	//exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,reportFile + xlsFileName);// --输出到文件
	//exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);//--输出到流
	//exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,Boolean.FALSE);//--每页使用一个SHEET
	//exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);//--移除空白
	//exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);//--背景色
	//exporter.exportReport();

	//out.clear();
   // out = pageContext.pushBody();
      %>
		
	</body>
</html>