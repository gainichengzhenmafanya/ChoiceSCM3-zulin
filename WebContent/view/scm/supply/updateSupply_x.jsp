<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>costbom Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div>
		<div class="tool"></div>
		<div style="margin:0px auto;margin-left:80px;margin-top:50px;">
			<form id="listForm" action="<%=path%>/supply/saveByUpdateSupply_x.do" method="post">
				<input type="hidden" id="is_supply_x" name="is_supply_x" class="text" value="Y"/>
				<div class="form-line">
					<div class="form-label">虚拟<fmt:message key="supplies_code" />：</div>
					<div class="form-input"><input type="text" id="sp_code_x" name="sp_code_x" class="text" readonly="readonly" value="${supply.sp_code_x}"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">虚拟物资名称：</div>
					<div class="form-input"><input type="text" id="sp_name_x" name="sp_name_x" class="text" value="${supply.sp_name_x}" onblur="getSpInit_x(this);"/></div>
				</div>
				<div class="form-line">
					<div class="form-label">虚拟物资缩写：</div>
					<div class="form-input"><input type="text" id="sp_init_x" name="sp_init_x" class="text" value="${supply.sp_init_x}"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="standard_unit" />：</div>
					<div class="form-input">
						<select class="select" id="unit_x" name="unit_x"  style="width:133px" onkeydown="www_zzjs_net(this)" onkeypress="zzjs_net(this)">
								<c:forEach var="unit_x" items="${unitList}" varStatus="status">
									<option
										<c:if test="${supply.unit_x==unit_x.des}"> 
									  	 	selected="selected"
										</c:if>  
									id="${unit_x.code}" value="${unit_x.des}">${unit_x.des}</option>
								</c:forEach>
							</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">虚拟单位转换率：</div>
					<div class="form-input"><input type="text" id="unitRate_x" name="unitRate_x" class="text" value="${supply.unitRate_x}" onkeypress="checkNum(this);"/></div>
				</div>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				$('#cancel').bind("click",function search(){
					$('#sp_code_x').val('');
					$('#sp_init_x').val('');
					$('#sp_name_x').val('');
// 				 	$('#listForm').submit();
				});
				$('#itemNm').val(parent.$('#itemNm').val());
				parent.$('#mods').val($('#mods').val());
				parent.$('#unit').val($('#unit').val());
				$('#unit_sprice').val($(window.parent.document).find('#punit').val()+','+$(window.parent.document).find('#price').val());
				
// 				$('.grid').find('.table-body').find('tr').live("click", function () {
// 					$(":checkbox").attr("checked", false);
// 					 $(this).find(":checkbox").attr("checked", true);
// 					 spcode=$(this).find(":checkbox").val();
// 					 $('#parentId').val($(this).find('td:eq(2)').find('span').attr('title'));
// 					 $('#parentName').val($(this).find('td:eq(3)').find('span').attr('title'));
// 				 });

				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 65: $('#autoId-button-101').click(); break;
		                case 69: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 83: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-106').click(); break;
		            }
				}); 
			 	//双单 击事件
				$('.grid .table-body tr').live('click',function(){
					 $('#sp_code_x').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#sp_name_x').val($(this).find('td:eq(3)').find('span').attr('title'));
					 $('#sp_init_x').val($(this).find('td:eq(4)').find('span').attr('title'));
					 var sp_code_x=$(this).find('td:eq(2)').find('span').attr('title');
					 var sp_name_x=$(this).find('td:eq(3)').find('span').attr('title');
					 var sp_init_x=$(this).find('td:eq(4)').find('span').attr('title');
					 var  url="<%=path%>/supply/downSupply.do?sp_code_x="+sp_code_x+"&sp_name_x="+sp_name_x+"&sp_init_x="+sp_init_x;
					 $('#mainFrame').attr('src',encodeURI(url));
				});
				//新增
				var status = $("#curStatus").val();
				//判断按钮的显示与隐藏
				if(status == 'A'){
					loadToolBar([true,true,false,false,false,true,false]);
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
					$('#chk1memo').attr('disabled',false);
				}else{//init
					loadToolBar([true,true,true,false,false,true,true]);
				}
				if($.browser.msie){ 
					if($.browser.version == 8.0){
						setElementHeight('.grid',['.tool'],$(document.body),305);
					} else {
						setElementHeight('.grid',['.tool'],$(document.body),360);
					}
				} else {
					setElementHeight('.grid',['.tool'],$(document.body),330);	//计算.grid的高度
				};
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", true);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				 }); 
				
			});

			function submitFrame(){
				$('#listForm').submit();
			}
			
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							saveByUpdateSupply_x();
						}
					},{
						text: '<fmt:message key ="quit" />',
						title: '<fmt:message key ="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}]
				});
			};
			//焦点离开检查输入是否合法
			function checkNum(inputObj){
				if(isNaN(inputObj.value)){
					alert("转换率为无效数字！");
					inputObj.focus();
					$("#unitRate_x").text();
					return false;
				}	
			}
			function www_zzjs_net(obj){
				switch(event.keyCode){
					case 13:
						obj.options[obj.length] = new Option("","",false,true);
						event.returnValue = false;
						break;
					case 8:
						obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text.substr(0,obj.options[obj.selectedIndex].text.length-1);
						event.returnValue = false;
					break;
					}
				}
				function zzjs_net(obj){
				  obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text + String.fromCharCode(event.keyCode);
				  event.returnValue = false;
				}
			//修改虚拟物料
			function saveByUpdateSupply_x(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>