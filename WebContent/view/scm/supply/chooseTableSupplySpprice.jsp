<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tr-select{
					background-color: #D2E9FF;
				}
				.separator ,div , .pgSearchInfo{
					display: none;
				}
				div[class]{
					display:block;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
	</head>
	<body style="height: 100%;">
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/supply/chooseTableSupply.do?level=${level}&&code=${code}&use=spprice" method="post">
			<div class="form-line">
					<div class="form-label" style="width: 60px"><fmt:message key="supplies_abbreviations"/>：</div>
					<div class="form-input" style="width: 60px"><input type="text" id="sp_init" name="sp_init" class="text" value="${supply.sp_init }" style="text-transform:uppercase;width:70px;"/></div>
					<div class="form-label" style="width: 60px;padding-left: 15px;"><fmt:message key="supplies"/><fmt:message key="name"/>：</div>
					<div class="form-input" style="width: 60px"><input type="text" id="sp_name" name="sp_name" class="text" value="${supply.sp_name }" style="width:70px;"/></div>
			</div>
			<div class="form-line">
					<div class="form-label" style="width: 60px"><fmt:message key="supplies"/><fmt:message key="coding"/>：</div>
					<div class="form-input" style="width: 60px"><input type="text" id="sp_code" name="sp_code" class="text" value="${supply.sp_code }" style="width:70px;"/></div>
			</div>
			<div class="form-line">
					<div class="form-label" style="width: 60px;"><fmt:message key="suppliers"/>：</div>
					<div class="form-input" style="width: 60px">
						<input name="deliverdes" id="deliver" class="text" readonly="readonly" value="${supply.deliverdes }"/>
						<input name="deliver" id="delivercode" class="text" readonly="readonly" type="hidden"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 						<select class="select" name="deliver"> -->
<!-- 							<option value=""></option> -->
<%-- 							<c:forEach items="${deliverList }" var="deliver"> --%>
<%-- 								<option value="${deliver.code }" <c:if test="${supply.deliver == deliver.code }">selected="selected"</c:if>>${deliver.des }</option> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
					</div>
			</div>
			<div class="grid" style="overflow: auto;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:30px;">&nbsp;</span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_code" /></span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_name" /></span></td>
								<td><span style="width:50px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:60px;"><fmt:message key="standard_price" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr class="">
									<td class="num"><span style="width:30px;">${step.index+1}</span><input type="hidden" value="<c:out value='${supply.sp_code}'/>"/></td>
									<td><span style="width:90px;"><c:out value='${supply.sp_code}'/></span></td>
									<td><span style="width:70px;"><c:out value='${supply.sp_name}'/></span></td>
									<td><span style="width:50px;"><c:out value='${supply.sp_init}'/></span></td>
									<td><span style="width:60px;"><c:out value='${supply.sp_price}'/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				if(typeof(eval(parent.parent.addAction)) == 'function'){
					parent.parent.addAction();
				};
				$(".pgPanel").children("div:first").css('display','block');
				$(".changePageSize , .pgSearchInfo").css("display","none");
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select"/>',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								//var key = $.trim($("#key").val());
								//if(isNaN(key))
								//	$("#key").attr("name","sp_init");
								//else
								//	$("#key").attr("name","sp_code");
								$('#listForm').submit();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(window.parent.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();									
							}
						}]
				});
				
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $("#delivercode").val();
						var deliver = $("#deliver").val();
// 						var offset = getOffset('sp_code');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode),null,$('#deliver'),$('#delivercode'),'900','500','isNull');
					}
				});
				
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 		if(e.keyCode==13){
			 			//var key = $.trim($("#key").val());
						//if(isNaN(key))
						//	$("#key").attr("name","sp_init");
						//else
						//	$("#key").attr("name","sp_code");
						//alert($("input[name=sp_code]").val());
						$('#listForm').submit();
			 		}
			 	});
				var key = $.trim("${supply.sp_code}") || "${supply.sp_init}";
				$("#key").val(key);
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.grid',['.tool'],$(document.body),53);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();
				changeTh();
			});
		</script>
	</body>
</html>