<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
		<div class="form">
			<form id="SupplyForm" method="post" action="<%=path %>/supply/saveByAdd.do">
			<input type="hidden" id="acct" name="acct" class="text" value="1"/>
			<div class="easyui-tabs" fit="false" plain="true" style="height:420px;width:595px;z-index:88;margin:0px auto;">
				<div title='<fmt:message key="basic_information" />' style="padding:15px;margin-left:100px;margin-top:1px;">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_category" />：</div>
						<div class="form-input">
							<input type="text" id="typdes" name="typdes" class="selectDepartment text" readonly="readonly"/>
							<input type="hidden" id="sp_type" name="sp_type" class="text"  />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_code" />：</div>
						<div class="form-input"><input type="text" id="sp_code" name="sp_code" class="text" value="${sp_code}"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="supplies_name" />：</div>
						<div class="form-input"><input type="text" id="sp_name" name="sp_name" class="text" onblur="getSpInit(this);"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_abbreviations" />：</div>
						<div class="form-input"><input type="text" id="sp_init" name="sp_init" class="text"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="supplies_specifications" />：</div>
						<div class="form-input"><input type="text" id="sp_desc" name="sp_desc" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="shelf" />：</div>
						<div class="form-input"><input type="text" id="positn1" name="positn1" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="barcode" />：</div>
						<div class="form-input"><input type="text" id="sp_code1" name="sp_code1" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="default_position" />：</div>
						<div class="form-input">
							<select class="select" id="sp_position" name="sp_position"  style="width:133px">
								<c:forEach var="positn" items="${positnList}" varStatus="status">
									<option id="${positn.code}" value="${positn.code}">${positn.des}</option>
								</c:forEach>
							</select>
						</div>
						<input type="hidden" id="positndes" name="positndes" ></input>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supply_units" />：</div>
						<div class="form-input">
							<input type="hidden" id="deliverdes" name="deliverdes"/>
							<select class="select" id="deliver" name="deliver" style="width:133px">
								<c:forEach var="deliver" items="${deliverList}" varStatus="status" >
									<option id="${deliver.code}" value="${deliver.code}">${deliver.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
				    	<div class="form-label"><fmt:message key="status" />：</div>
						<div class="form-input">
							<fmt:message key="enable" /><input type="radio" id="sta" name="sta" class="text" value="Y" checked="checked"/>
							<fmt:message key="disabled" /><input type="radio" id="sta" name="sta" class="text" value="N"/>
						</div>
					</div>
<!-- 					<div class="form-line"> -->
<%-- 				    	<div class="form-label">使用<fmt:message key="status" />：</div> --%>
<!-- 						<div class="form-input"> -->
<!-- 							已被引用<input type="radio" id="locked" name="locked" class="text" value="Y" readonly="readonly"/> -->
<!-- 							尚未引用<input type="radio" id="locked" name="locked" class="text" value="N"  readonly="readonly" checked="checked"/> -->
<!-- 						</div> -->
<!-- 					</div>  -->
				</div>
				<div title="换算单位" style="padding:10px;margin-left:100px;margin-top:50px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="standard_unit" />：</div>
						<div class="form-input">
							<select class="select" id="unit" name="unit"  style="width:133px" onkeydown="www_zzjs_net(this)" onkeypress="zzjs_net(this)">
								<c:forEach var="unit" items="${unitList}" varStatus="status">
									<option id="${unit.code}" value="${unit.des}">${unit.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="reference_unit" />：</div>
						<div class="form-input">
							<select class="select" id="unit1" name="unit1"  style="width:133px" onkeydown="www_zzjs_net(this)" onkeypress="zzjs_net(this)">
								<c:forEach var="unit" items="${unitList}" varStatus="status">
									<option id="${unit.code}" value="${unit.des}">${unit.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="unit_conversion_rate" />：</div>
						<div class="form-input">
							<input type="text" id="unitper" name="unitper" value="1"  class="text"/>
						</div>
					</div>
<!-- 					<div class="form-line"> -->
<%-- 						<div class="form-label" style="width:110px;text-align: right;"><fmt:message key="must_enter_units" />：</div> --%>
<!-- 						<div class="form-input"> -->
<%-- 							<fmt:message key="be" /><input type="radio" id="ynunit" name="ynunit" class="text" value="Y"/> --%>
<%-- 							<fmt:message key="no1" /><input type="radio" id="ynunit" name="ynunit" class="text" value="N" checked="checked"/> --%>
<!-- 						</div> -->
<!-- 					</div> -->
					<div class="form-line">
						<div class="form-label"><fmt:message key="cost_unit" />：</div>
						<div class="form-input">
							<select class="select" id="unit2" name="unit2"  style="width:133px" onkeydown="www_zzjs_net(this)" onkeypress="zzjs_net(this)">
								<c:forEach var="unit" items="${unitList}" varStatus="status">
									<option id="${unit.code}" value="${unit.des}">${unit.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="unit_conversion_rate" />：</div>
						<div class="form-input"><input type="text" id="unitper2" name="unitper2" value="1" class="text"/></div>
					</div>
				</div>
				<div title="采购周转" style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="procurement_cycle" />：</div>
						<div class="form-input"><input type="text" id="datsto" name="datsto" value="0" class="text"/></div>
						<div class="form-label"><fmt:message key="actual_stock_limit" />：</div>
						<div class="form-input"><input type="text" id="sp_max1" name="sp_max1" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="delivery_time" />：</div>
						<div class="form-input"><input type="text" id="cntminsto" name="cntminsto" value="0" class="text"/></div>
						<div class="form-label"><fmt:message key="reference_inventory_capping" />：</div>
						<div class="form-input"><input type="text" id="sp_max2" name="sp_max2" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="average_daily_consumption" />：</div>
						<div class="form-input"><input type="text" id="cntuse" name="cntuse" value="0" class="text"/></div>
						<div class="form-label"><fmt:message key="actual_stock_lower_limit" />：</div>
						<div class="form-input"><input type="text" id="sp_min1" name="sp_min1" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="stores_month_procurement_limit" />：</div>
						<div class="form-input"><input type="text" id="stomax" name="stomax" value="0" class="text"/></div>
						<div class="form-label"><fmt:message key="reference_stocks_lower_limit" />：</div>
						<div class="form-input"><input type="text" id="" name="" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="require_special_audit" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="stochk1" name="stochk1" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="stochk1" name="stochk1" class="text" value="N" checked="checked"/>
						</div>
						<div class="form-label">分店物资属性：</div>
						<div class="form-input">
							<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}" style="width:120px;"/>
							<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"  />
							<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="stores_to_purchase_single_limited" />：</div>
						<div class="form-input"><input type="text" id="stomax1" name="stomax1" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="branche_acceptance_ratio" />：</div>
						<div class="form-input"><input type="text" id="accprate" name="accprate" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="minimum_amount_of_purchase" />：</div>
						<div class="form-input"><input type="text" id="mincnt" name="mincnt" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="turnover_period" />：</div>
						<div class="form-input"><input type="text" id="sp_per1" name="sp_per1" value="0" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="number_of_days_alarm_in_advance" />：</div>
						<div class="form-input"><input type="text" id="sp_per2" name="sp_per2" value="0" class="text"/></div>
					</div>
				</div>
				<div title='<fmt:message key="whether_semi" />' style="padding:10px;margin-left:100px;margin-top:80px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="whether_branches_semi" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ex" name="ex" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="ex" name="ex" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="whether_base_processing" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ex1" name="ex1" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="ex1" name="ex1" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="produced_positions" />：</div>
						<div class="form-input">
					 		<select class="select" id="positnex" name="positnex" onchange="getValue(this);" style="width:133px">
								<option id="" value=""></option>
								<c:forEach var="positnex" items="${positnexList}" varStatus="status">
									<option
										<c:if test="${supply.positnex==positnex.code}"> 
									  	 	selected="selected"
										</c:if>  
									id="${positnex.code}" value="${positnex.code}">${positnex.des}</option>
								</c:forEach>
							</select>
							<input type="hidden" id="positnexdes" name="positnexdes" class="text"/>
						</div>
				     </div>
				     <div class="form-line">
						<div class="form-label">加工工时：</div>
						<div class="form-input">
							<input type="text" id="extim" name="extim" class="text" value="0.0"/>&nbsp;&nbsp;<fmt:message key ="hours" />
						</div>
				     </div>
<!-- 					<div class="form-line"> -->
<%-- 						<div class="form-label"><fmt:message key="cost_of_fare_increase_ratio" />：</div> --%>
<!-- 						<div class="form-input"> -->
<!-- 							<input type="text" id="" name="" class="text" value="0" style="width:136px"/> -->
<!-- 						</div> -->
<!-- 				     </div> -->
				</div>
				<div title='<fmt:message key="auxiliary_information" />' style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="materials_unit_price" />：</div>
						<div class="form-input"><input type="text" id="sp_price" name="sp_price" class="text" value="0"/></div>
						<div class="form-label"><fmt:message key="whether_direct_dial" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="inout" name="inout" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="inout" name="inout" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="materials_price" />：</div>
						<div class="form-input"><input type="text" id="pricesale" name="pricesale" class="text" value="0"/></div>
						<div class="form-label"><fmt:message key="whether_consignment" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="yndx" name="yndx" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="yndx" name="yndx" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_brands" />：</div>
						<div class="form-input"><input type="text" id="sp_mark" name="sp_mark" class="text"/></div>
						<div class="form-label"><fmt:message key="whether_solar_disk" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ydaypan" name="yndaypan" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="ndaypan" name="yndaypan" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_origin" />：</div>
						<div class="form-input"><input type="text" id="sp_addr" name="sp_addr" class="text"/></div>
						<div class="form-label"><fmt:message key="whether_week_disk" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="yweekpan" name="ynweekpan" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="nweekpan" name="ynweekpan" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line"><!-- 参考类别 -->
						<div class="form-label"><fmt:message key="reference_category" />：</div>
						<div class="form-input">
							<select class="select" id="typoth" name="typoth">
								<c:forEach items="${typothList }" var="typoth">
									<option value="${typoth.code }">${typoth.des }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="whether_boutique" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="sp_cost" name="sp_cost" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="sp_cost" name="sp_cost" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="tax_rate" />：</div>
						<div class="form-input">
							<select class="select" id="tax" name="taxId">
								<c:forEach items="${taxList }" var="tax">
									<option value="${tax.id }">${tax.taxdes }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label">是否可退货：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ynth" name="ynth" class="text" value="Y"/>
							<fmt:message key="no1" /><input type="radio" id="ynth" name="ynth" class="text" value="N" checked="checked"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="explain" />：</div>
						<div class="form-input"><input type="text" id="stomemo" name="stomemo" class="text"/></div>
<!-- 						<div class="form-label">条形码：</div> -->
<!-- 						<div class="form-input"><input type="text" id="barCode" name="barCode" class="text"/></div> -->
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="quality_requirements" />：</div>
						<div class="form-input"><input type="text" id="quamemo" name="quamemo" class="text"/></div>
						<div class="form-label">标准产品：</div>
						<div class="form-input">
<!-- 							<input type="hidden" id="vname" name="vname" ></input> -->
<!-- 							<select class="select" id="vcode" name="vcode" style="width:133px" onchange="findItemByCode(this)"> -->
<!-- 								<option id="" value=""></option> -->
<%-- 								<c:forEach var="pubitem" items="${pubitemList}" varStatus="status" > --%>
<%-- 									<option value="${pubitem.itcode}">${pubitem.itdes}</option> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
							<input type="hidden"  id="vcode" name="vcode"/>
							<input type="text" id="vname" name="vname" readonly="readonly" style="width:120px;" />
							<img id="seachPubitem" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">ABC：</div>
						<div class="form-input">
							<select class="select" id="abc" name="abc">
								<option value=""></option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
							</select>
						</div>
						
					</div>
					<div class="form-line">
						<div class="form-label">物资验货比：</div>
						<div class="form-input"><input type="text" id="yhrate" name="yhrate" class="text"/></div>
						<div class="form-label">物资降价比：</div>
						<div class="form-input"><input type="text" id="lower" name="lower" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label">物资涨价比：</div>
						<div class="form-input"><input type="text" id="upper" name="upper" class="text"/></div>
					
						<div class="form-label">物资出货比：</div>
						<div class="form-input"><input type="text" id="chrate" name="chrate" class="text"/></div>
					</div>
				</div>
				
				<div title='预估/报货' style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label">报货类别：</div>
						<div class="form-input">
							<select id="typ_eas" name="typ_eas" style="width:134px;">
								<option value=""></option>
								<c:forEach items="${typEasList }" var="typEas">
									<option value="${typEas.code }">${typEas.des }</option>
								</c:forEach>
							</select>
						</div>
					</div>
<!-- 					<div class="form-line"> -->
<!-- 						<div class="form-label">报货方式：</div> -->
<!-- 						<div class="form-input"> -->
<!-- 							<select name="stotyp" id="stotyp" class="select" style="width:134px;"> -->
<!-- 								<option value="0"></option> -->
<!-- 								<option value="1">千元用量</option> -->
<!-- 								<option value="2">千次</option> -->
<!-- 								<option value="3">菜品点击率</option> -->
<!-- 								<option value="4">安全库存</option> -->
<!-- 								<option value="5">历史耗用</option> -->
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<div class="form-line">
						<div class="form-label">系数A</div>
						<div class="form-input"><input type="text" id="ratioA" name="ratioA" class="text" value='1.0'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数B</div>
						<div class="form-input"><input type="text" id="ratioB" name="ratioB" class="text" value='1.0'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数C</div>
						<div class="form-input"><input type="text" id="ratioC" name="ratioC" class="text" value='1.0'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数D</div>
						<div class="form-input"><input type="text" id="ratioD" name="ratioD" class="text" value='1.0'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数E</div>
						<div class="form-input"><input type="text" id="ratioE" name="ratioE" class="text" value='1.0'/></div>
					</div>	
					<div class="form-line">
						<div class="form-label">系数F</div>
						<div class="form-input"><input type="text" id="ratioF" name="ratioF" class="text" value='1.0'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数G</div>
						<div class="form-input"><input type="text" id="ratioG" name="ratioG" class="text" value='1.0'/></div>
					</div>	 
				</div>
				<!-- 虚拟物料代码，不熟悉者务改动 -->
				<c:if test="${is_supply_x=='Y'}">
					<div title='虚拟编码' style="padding:10px;margin-left:0px;margin-top:20px;">
						<input type="hidden" id="is_supply_x" name="is_supply_x" class="text" value="Y"/>
						<div class="form-line">
							<div class="form-label"><span class="red">*</span>虚拟<fmt:message key="supplies_code" />：</div>
							<div class="form-input"><input type="text" id="sp_code_x" name="sp_code_x" class="text" value="${sp_code_x}"/>
							<img id="seachSupply_x" class="seachSupply_x" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' style='cursor: hand;'/>
							</div>
							<div class="form-label"><input type="button" id="resetSupply_x" value="清空重填"/></div>
						</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span>虚拟物资名称：</div>
							<div class="form-input"><input type="text" id="sp_name_x" name="sp_name_x" class="text" value="${sp_name_x}"/></div>
						</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span>虚拟物资缩写：</div>
							<div class="form-input"><input type="text" id="sp_init_x" name="sp_init_x" class="text" value="${sp_init_x}"/></div>
						</div>
						<div class="form-line">
							<div class="form-label">虚拟单位：</div>
							<div class="form-input">
								<input type="hidden" name="unit_x" value="${unitList[0].des }"/>
								<select class="select" id="unit_x" style="width:133px">
									<c:forEach var="unit" items="${unitList}" varStatus="status">
										<option id="${unit.code}" value="${unit.des}">${unit.des}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span>虚拟单位转换率：</div>
							<div class="form-input"><input type="text" id="unitRate_x" name="unitRate_x" class="text" value="1" onkeypress="checkNum(this);"/></div>
						</div>
					</div>
				</c:if>
				</div>
			</form>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			var validate;
			$(document).ready(function(){
				//清空虚拟物料充填按钮  wjf
				$('#resetSupply_x').click(function(){
					 $('#sp_code_x').val('').attr('readonly',false);
					 $('#sp_name_x').val('').attr('readonly',false);
					 $('#unit_x').attr('disabled',false);
					 $('input[name=unit_x]').val($('#unit_x').val());
					 $('#sp_init_x').val('').attr('readonly',false);
				});
// 				$(".tool").click(function(){
// 					var sp_code=$('#sp_code').val();
// 					var curwindow = $('body').window({
// 						id: 'window_unit',
// 						title: '多单位维护',
<%-- 						content: '<iframe id="searchUnitFrame" frameborder="0" src="<%=path%>/supply/tableUnit.do?sp_code=init'+sp_code+'"></iframe>', --%>
// 						width: '800px',
// 						height: '430px',
// 						draggable: true,
// 						isModal: true
// 					});
// 					curwindow.max();
// 				});

				$('#unitManagement').bind("click",function search(){
					var sp_code=$('#sp_code').val();
					if (sp_code==''||sp_code==null){
						alert("需要先填写物资编码！");
					}else{
						var curwindow = $('body').window({
							id: 'window_unit',
							title: '多单位维护',
							content: '<iframe id="searchUnitFrame" frameborder="0" src="<%=path%>/supply/tableUnit.do?sp_code='+sp_code+'"></iframe>',
							width: '800px',
							height: '430px',
							draggable: true,
							isModal: true
						});
						curwindow.max();
					}
				});
				
				//unit_x  change事件  wjf
				$('#unit_x').change(function(){
					$('input[name=unit_x]').val($(this).val());
				});
				$('#seachSupply_x').bind('click.custom',function(e){
					if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectAllSupply_x.do'),$('#sp_code_x'),null,null,null,null,null,handler);
					}
				});
				//弹出物资树回调函数
				function handler(sp_code){
					$('#sp_code_x').val(sp_code); 
					if(sp_code==undefined || ''==sp_code){
						 $('#sp_code_x').val('');
						 $('#sp_name_x').val('');
						 $('#unit_x').val('');
						 $('input[name=unit_x]').val('');
						 $('#sp_init_x').val('');
						return;
					}
					$('.validateMsg').remove(); 
					$.ajax({
						type: "POST",
						url: "<%=path%>/supply/findById_x.do",
						data: "sp_code_x="+sp_code,
						dataType: "json",
						success:function(supply){
							$('#sp_code_x').val(supply.sp_code_x).attr('readonly',true);
							 $('#sp_name_x').val(supply.sp_name_x).attr('readonly',true);
							 $('#unit_x').val(supply.unit_x).attr('disabled',true);
							 $('input[name=unit_x]').val(supply.unit_x);
							 $('#sp_init_x').val(supply.sp_init_x).attr('readonly',true);
						}
					});
				}
				//取上一级物资类别
				var sp_type=$('#sp_type', parent.parent.document).val();
				var code=$('#code', parent.parent.document).val();
				if(code!=""){
					$("#sp_type").val(code);
					$("#typdes").val(sp_type);
				}
				//回车输入
				   $('input:text:eq(1)').focus();
			        var $inp = $('input:text');
			        $inp.bind('keydown', function (e) {
			            var key = e.which;
			            if (key == 13) {
			                e.preventDefault();
			                var nxtIdx = $inp.index(this) + 1;
			                $(":input:text:eq(" + nxtIdx + ")").focus();
			            }
			        });

				/*弹出树*/
				$('#typdes').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('typdes');
						top.cust('<fmt:message key="please_select_category" />',encodeURI('<%=path%>/supply/selectGrptyp.do'),offset,$('#typdes'),$('#sp_type'),null,null,null,setSpcode);
					}
				});
				
				/*虚拟物资编码*/
				$('#sp_code').bind('blur', function() {
// 					 $('#sp_code_x').val($('#sp_code').val().substr(1, $('#sp_code').val().length));
					 if($('#sp_code_x').val() == ''){
						 $('#sp_code_x').val($('#sp_code').val());
					 }
				});
				$('#sp_name').bind('blur', function() {
					if($('#sp_name_x').val() == ''){
						 $('#sp_name_x').val($('#sp_name').val());
						 $('#sp_init_x').val($('#sp_init').val()+'_X');
						 $('#unit_x').val($('#unit').val());
						 $('input[name=unit_x]').val($('#unit_x').val());
					}
				});
				 
				/*验证*/
				validate = new Validate({
					validateItem:[
					   <c:if test="${is_supply_x=='Y'}">
					   {						
						type:'text',
						validateObj:'sp_code_x',
						validateType:['num'],
						param:['F'],
						error:['虚拟物资编码不是有效数字！']
					},{
						type:'text',
						validateObj:'sp_code_x',
						validateType:['canNull'],
						param:['F'],
						error:['虚拟物资编码不能为空！']
					},{
						type:'text',
						validateObj:'sp_name_x',
						validateType:['canNull'],
						param:['F'],
						error:['虚拟物资名称不能为空！']
					},{
						type:'text',
						validateObj:'unitRate_x',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['虚拟物资单位转换率不能为空！！','虚拟物资单位转换率不是有效数字！！']
					},</c:if>{
						type:'text',
						validateObj:'sp_code',
						validateType:['num'],
						param:['F'],
						error:['物资编码不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioA',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioB',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioC',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioD',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioE',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioF',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'ratioG',
						validateType:['num'],
						param:['F'],
						error:['系数不是有效数字！']
					},{
						type:'text',
						validateObj:'typdes',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="supplies_category" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'sp_code',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_name',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_init',
						validateType:['canNull','maxLength'],
						param:['F','30'],
						error:['<fmt:message key="supplies_abbreviations" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_abbreviations" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_desc',
						validateType:['maxLength'],
						param:['30'],
						error:['<fmt:message key="supplies_specifications" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'positn1',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="shelf" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_code1',
						validateType:['maxLength'],
						param:['50'],
						error:['<fmt:message key="barcode" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'unit1',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="reference_unit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'unitper',
						validateType:['num','maxLength'],
						param:['F','8'],
						error:['<fmt:message key="unit_conversion_rate" /><fmt:message key="format_for" />(1.1)','<fmt:message key="unit_conversion_rate" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'unit2',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="cost_unit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'unitper2',
						validateType:['num','maxLength'],
						param:['F','8'],
						error:['<fmt:message key="unit_conversion_rate" /><fmt:message key="format_for" />(1.1)','<fmt:message key="unit_conversion_rate" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'datsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="procurement_cycle" /><fmt:message key="format_for_digital" />','<fmt:message key="procurement_cycle" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntminsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="delivery_time" /><fmt:message key="format_for_digital" />','<fmt:message key="delivery_time" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntuse',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="average_daily_consumption" /><fmt:message key="format_for_digital" />','<fmt:message key="average_daily_consumption" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'stomax',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="stores_month_procurement_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="stores_month_procurement_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'stomax1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="stores_to_purchase_single_limited" /><fmt:message key="format_for_digital" />','<fmt:message key="stores_to_purchase_single_limited" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntminsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="delivery_time" /><fmt:message key="format_for_digital" />','<fmt:message key="delivery_time" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'accprate',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="branche_acceptance_ratio" /><fmt:message key="format_for_digital" />','<fmt:message key="branche_acceptance_ratio" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'mincnt',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="format_for_digital" />','<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_per1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="turnover_period" /><fmt:message key="format_for_digital" />','<fmt:message key="turnover_period" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'mincnt',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="format_for_digital" />','<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_per2',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="number_of_days_alarm_in_advance" /><fmt:message key="format_for_digital" />','<fmt:message key="number_of_days_alarm_in_advance" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_max1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="actual_stock_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_max2',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="reference_inventory_capping" /><fmt:message key="format_for_digital" />','<fmt:message key="reference_inventory_capping" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_min1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="actual_stock_lower_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_lower_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_price',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="materials_unit_price" /><fmt:message key="format_for_digital" />','<fmt:message key="materials_unit_price" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'pricesale',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="materials_price" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_lower_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'tax',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="tax_rate" /><fmt:message key="format_for_digital" />','<fmt:message key="tax_rate" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_code',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/supply/findById.do",{sp_code:$("#sp_code").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="supplies_code" /><fmt:message key="already_exists" />！']
					},
					//wangjie 2014年11月11日 09:49:29 （物资名称重复时需要有提示，或物资名称也不能重复）判断物资名称是否重复
					{
						type:'text',
						validateObj:'sp_name',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/supply/findById.do",{sp_name:$("#sp_name").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'upper',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="format_for_digital" />','<fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'lower',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="format_for_digital" />','<fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'yhrate',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="format_for_digital" />','<fmt:message key="length_too_long" />！']
					}]
				});
				$("#positndes").val($("#sp_position option:selected").text());
				$("#sp_position").bind('change',function(){
					$("#positndes").val($("#sp_position option:selected").text());
				});
				$("#deliverdes").val($("#deliver option:selected").text());
				$("#deliver").bind('change',function(){
					$("#deliverdes").val($("#deliver option:selected").text());
				});
				$("#vname").val($("#vcode option:selected").text());
				$("#vcode").bind('change',function(){
					$("#vname").val($("#vcode option:selected").text());
				});
				
				/**2014.6.25 wjf**/
				$("#yweekpan").bind('click',function(){
					$("input[name='yndaypan']").get(0).checked = true;
				});
			});
			function setSpcode(codeid){
// 				 $('#sp_code').val(codeid+".");//五芳斋项目就用这一句代码
				if(codeid){
					  $.ajax({
						  type:"post",
						  async:false,
						  url:"<%=path%>/supply/getMaxSpcode.do",
						  dataType:"text",
						  data:{code:codeid},
						  success:function(data){
							  $('#sp_code').val(data);
							  $('#sp_code_x').val(data);
						  },
						  error:function(htmlObject,status,index){
							  alert("获取最大物资编码的时候出错."+index);
						  }
					  });
				}
			}
			function getValue(a) {
				$('#positnexdes').val($(a).find('option:selected').text());
			}
			$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#firmCode").val();
					var defaultName = $("#firmDes").val();
					var offset = getOffset('sp_type');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/searchAllPositn.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'750','500','isNull');
				}
			});
			/**2014.6.25 wjf 标准产品**/
			$('#seachPubitem').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('sp_type');
					top.cust('选择标准产品',encodeURI('<%=path%>/supply/searchPubitemList.do?'),offset,$('#vname'),$('#vcode'),'750','500','isNull');
				}
			});
			function setAction(){
				var firmCodes = $('#firmCode').val();
				//检查一种虚拟物资对应多种实际物资时，成本单位是否一样
				$('#SupplyForm').attr('action','<%=path %>/supply/saveByAdd.do?firmCodes='+firmCodes);
			}
			
			//基地加工物资必须选择加工间
			function checkPositnex(){
				var flag = false;
				var positnex = $('#positnex').val();
				var ex1 = $('input[name=ex1]:checked').val();
				if(ex1 == "Y" && positnex == ""){
					alert('当前物资为基地加工，请选择加工间！');
					flag = false;
				} else {
					flag = true;
				}
				return flag;
			}
			//物资的标准单位必须跟对应虚拟物料的虚拟单位相同
			function checkUnit(){
// 				var flag = false;
// 				var unit = $('#unit').val();
// 				var unit_x = $('#unit_x').val();
// 				if(unit != unit_x){
// 					alert('虚拟物料的单位与对应实际物料的标准单位不相同，请修改！');
// 					flag = false;
// 				} else{
// 					flag = true;
// 				}
// 				return flag;
				return  true;
			}
			//检查一种虚拟物资对应多种实际物资时，成本单位是否一样
			function checkUnit2(){
// 				var sp_code_x = $('#sp_code_x').val();
// 				var sp_code = $('#sp_code').val();
// 				var unit = $('#unit2').val();
// 				var flag = false;
// 				$.ajax({
// 					type: "POST",
<%-- 					url: "<%=path%>/supply/checkUnit.do", --%>
// 					data: "sp_code_x="+sp_code_x+"&sp_code="+sp_code,
// 					async:false,
// 					dataType: "json",
// 					success:function(result){
// 						if(result != null && result != unit){
// 							alert("当前虚拟物料对应实际物料的成本单位与对应其他实际物料的成本单位不相同，请做相应修改！");
// 							flag = false;
// 						} else {
// 							flag = true;
// 						}
// 					}
// 				});
// 				return flag;
				return true;
			}
			//检查虚拟物资编码和名称是否重复
			function checkSupply_x(){
// 				var sp_code_x = $('#sp_code_x').val();
// 				var sp_name_x = $('#sp_name_x').val();
// 				var flag = false;
// 				$.ajax({
// 					type: "POST",
<%-- 					url: "<%=path%>/supply/checkSupply_x.do", --%>
// 					data: "sp_code_x="+sp_code_x+"&sp_name_x="+sp_name_x,
// 					async:false,
// 					dataType: "json",
// 					success:function(result){
// 						if(result > 0){
// 							alert("虚拟物资编码或者名称不能重复，请做相应修改！");
// 							flag = false;
// 						} else {
// 							flag = true;
// 						}
// 					}
// 				});
// 				return flag;
return true;
			}
			
			//焦点离开检查输入是否合法
			function checkNum(inputObj){
				if(isNaN(inputObj.value)){
					alert("转换率为无效数字！");
					inputObj.focus();
					$("#unitRate_x").text();
					return false;
				}	
			}
			function www_zzjs_net(obj){
				switch(event.keyCode){
					case 13:
						obj.options[obj.length] = new Option("","",false,true);
						event.returnValue = false;
						break;
					case 8:
						obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text.substr(0,obj.options[obj.selectedIndex].text.length-1);
						event.returnValue = false;
					break;
					}
				}
				function zzjs_net(obj){
				  obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text + String.fromCharCode(event.keyCode);
				  event.returnValue = false;
				}
				
				function changeSelect1(e){
					$(e).empty(); 
					if ($(e).attr('id') !='unit'){
						var unit=$('#unit').val();
						if (unit==''||unit==null){
							alert("请先选择标准单位！");
						}else{
							//设置非标准单位
							var unitper0=$('#unitper0').val();
							var selected = {};
							selected['sp_code'] = $('#sp_code').val();
							$.post('<%=path%>/supply/findSupplyUnit.do',selected,function(supplyUnitList){
								var option = '<option value="'+'"></option>';
								for(var i=0;i<supplyUnitList.length;i++){							
									option+= '<option value="'+supplyUnitList[i].unit+'" unitper="'+supplyUnitList[i].unitper+'">'+supplyUnitList[i].unit+'</option>';
								}
								$(e).append(option);
							});
						}
					}else{
						//设置标准单位
						var selected = {};
						selected['sp_code'] = $('#sp_code').val();
						$.post('<%=path%>/supply/findSupplyUnit.do',selected,function(supplyUnitList){
							var option = '<option value="'+'"></option>';
							var unitper=1;
							for(var i=0;i<supplyUnitList.length;i++){	
								option+= '<option value="'+supplyUnitList[i].unit+'" unitper="'+supplyUnitList[i].unitper+'">'+supplyUnitList[i].unit+'</option>';
							}
							$(e).append(option);
							
						});
					}
				}
				
				function changeSelect(e){
					if ($(e).attr('id') =='unit'){
						$('#unitper0').val(parseInt($(e).find('option:selected').attr('unitper')));
					}else{
						var unitper0=$('#unitper0').val();
						if ($(e).attr('id') =='unit1'){
							$('#unitper').val(parseInt($(e).find('option:selected').attr('unitper')));
						}else if ($(e).attr('id') =='unit2'){
							$('#unitper2').val(parseInt($(e).find('option:selected').attr('unitper')));
						}else if ($(e).attr('id') =='unit3'){
							$('#unitper3').val(parseInt($(e).find('option:selected').attr('unitper')));
						}else{
							$('#unitper4').val(parseInt($(e).find('option:selected').attr('unitper')));
						}
					}
				}
		</script>
	</body>
</html>