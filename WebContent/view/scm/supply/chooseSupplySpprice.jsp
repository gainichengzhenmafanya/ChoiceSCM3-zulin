<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="supplies_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
			<style type="text/css">
				.leftFrame{
				width:30%;
				}
				.mainFrame{
				width:70%;
				}
			</style>
		</head>
	<body>
		<div class="leftFrame">
      <div id="toolbar"></div>
	    <div class="treePanel" style="height: 100%">
	      <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
          var tree = new MzTreeView("tree");
          
          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("0","0","")';
          <c:forEach var="grpTyp" items="${grpTypList}" varStatus="status">
	          	tree.nodes['00000000000000000000000000000000_${grpTyp.code}'] 
	          		= 'text:${grpTyp.des}; method:changeUrl("1","${grpTyp.code}")';
          </c:forEach>
          
          <c:forEach var="grp" items="${grpList}" varStatus="status">
	          	tree.nodes['${grp.grptyp}_${grp.code}'] 
	          		= 'text:${grp.des}; method:changeUrl("2","${grp.code}","")';
          </c:forEach>

          <c:forEach var="typ" items="${typList}" varStatus="status">
	          	tree.nodes['${typ.grp}_${typ.code}'] 
	          		= 'text:${typ.des}; method:changeUrl("3","${typ.code}","${typ.des}")';
          </c:forEach>
          
          tree.setIconPath("<%=path%>/image/tree/none/");
          document.write(tree.toString());
        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="<%=path%>/supply/chooseTableSupply.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    <script type="text/javascript">
    	$('body').height($('body',parent.document).height() - $('.topFrame',parent.document).height());
    </script>
    <input type="hidden" id="level" name="level" />
    <input type="hidden" id="code" name="code" />
    <input type="hidden" id="sp_type" name="sp_type" />
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			function changeUrl(level,code,typ){
				$('#level').val(level);
				$('#code').val(code);
				$('#sp_type').val(typ);
				
		      window.mainFrame.location = "<%=path%>/supply/chooseTableSupply.do?level="+level+"&&code="+code;
		    }
				
		    function refreshTree(){
					window.location.href = '<%=path%>/supply/chooseSupply.do?use='+'spprice';
		    }
		    
		    function pageReload(){
		    	window.location.href = '<%=path%>/supply/chooseSupply.do?supplyId='+$("#supplyId").val();
		    }
			$(document).ready(function(){
				
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						}
					]
				});
				setElementHeight('.treePanel',['#toolbar','#modDiv'],$(document.body),2);
			});// end $(document).ready();
		</script>

	</body>
</html>