<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="" id="import" method="post" enctype="multipart/form-data">
			<div align="center" id="divImport">
						<FONT color=red>* </FONT><fmt:message key="select_imported_file" />：
								<input type="file" style="WIDTH: 250px" maxLength=5 name="file" id="file"/>
			</div> 
		</form>
		<br>
		----------------------------------------------------------------------<br>
		<FONT color=red><fmt:message key="import_goods_precautions" />： </FONT><br>
		 1.<fmt:message key="download_template_remind" /><br>
<%-- 		 2.编码必须为数字。<fmt:message key="coding_must_be_num" /><br>								 --%>
		 2.<fmt:message key="supplies_abbreviations_of_each_first_character" /><br>								
		 3.<fmt:message key="Non_required_columns_do_not_delete_the_account_such_as_the_default_is_1_do_not_delete_this_column" /><br>	
		 4.<fmt:message key="Do_not_adjust_the_sequence_of_the_various_columns" /><br>	
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			var tool;
			$(document).ready(function(){
				tool	= _tool();
				});
			
			var _tool=function(){
				return $('.tool').toolbar({
					items: [
					 {
						text: '<fmt:message key="import" />',
						title: '<fmt:message key="import" /><fmt:message key="supplies_information" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							handle();
						}
					},
					{
						text: '<fmt:message key="template_download" />',
						title: '<fmt:message key="template_download" />',
						icon: {
							url: '<%=path%>/image/Button/excel.bmp'
						},
						handler: function(){
							$("#import").attr('action','<%=path%>/supply/downloadTemplate.do').submit();
						}
					}
				]
				});
				}
			function handle(){
				var file = $("#file").val();
				if(file == ''){
					alert('<fmt:message key="please_select_the_imported" />');
					return false;
				}
				 var pos = file.lastIndexOf(".");
				 var lastname = file.substring(pos,file.length); 
				
			   if (!(lastname.toLowerCase()==".xls" || lastname.toLowerCase()==".xlsx")){
				     alert('<fmt:message key="type_of_file_uploaded" />'+lastname+'，<fmt:message key="type_of_file" />');
				     return false;
			   }else{
					$("#import").attr('action','<%=path%>/supply/loadExcel.do').submit();
			  	}
			}
			
		</script>
	</body>
</html>