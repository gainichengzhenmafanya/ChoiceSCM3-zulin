<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supply/searchPubitemList.do" method="post">
			<input type="hidden" id="parentId" name="parentId" value=""/>
			<input type="hidden" id="parentName" name="parentName" value=""/>
			<div class="search-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left"><fmt:message key ="coding" />：</td>
						<td><input type="text" id="itcode" name="itcode" class="text" value="" /></td>
						<td class="c-left"><fmt:message key ="The_dishes" />：</td>
						<td><input type="text" id="itdes" name="itdes" class="text" value="" /></td>
						<td class="c-left"><fmt:message key ="abbreviation" />：</td>
						<td><input type="text" id="init" name="init" class="text" value="" /></td>
		       			<td><input type="button" style="width:40px;" class="search-button" id="search" value='<fmt:message key="select" />'/></td>
					</tr>
				</table>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:30px;"/>
								</td>
								<td><span style="width:50px;"><fmt:message key ="coding" /></span></td>
								<td><span style="width:120px;"><fmt:message key ="The_dishes" /></span></td>
								<td><span style="width:70px;"><fmt:message key ="abbreviation" /></span></td>
								<td><span style="width:60px;"><fmt:message key ="unit" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="price" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="pubitem" items="${pubitemList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${pubitem.itcode}' />" value="<c:out value='${pubitem.itcode}' />"/></span>
									</td>
									<td><span style="width:50px;"title="${pubitem.itcode}"><c:out value="${pubitem.itcode}" />&nbsp;</span></td>
									<td><span style="width:120px;" title="${pubitem.itdes}"><c:out value="${pubitem.itdes}" />&nbsp;</span></td>
									<td><span style="width:70px;" title="${pubitem.init}"><c:out value="${pubitem.init}" />&nbsp;</span></td>
									<td><span style="width:60px;" title="${pubitem.unit}"><c:out value="${pubitem.unit}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${pubitem.price}"><c:out value="${pubitem.price}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					
					<br />
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
					<input type="hidden" name ="vvcode"  id="pageSize" value="${vvcode }" />
					
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==0) {
									alert("<fmt:message key ="Selection_of_standard_products" />！");
									return;
								}
								if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size() > 1) {
									alert('<fmt:message key="Can_only_choose_a_standard_product" />！');
									return;
								}
								var vcode = $('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
								var vname = $('.grid').find('.table-body').find(':checkbox').filter(':checked').parent().parent().parent().find('td:eq(3)').find('span').attr('title');
								 $('#parentId').val(vcode);
								 $('#parentName').val(vname);
							 	 top.customWindow.afterCloseHandler('Y');
							     top.closeCustom();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				
				//add by jin shuai at 20160311
				//默认选中标准菜品
				$(":checkbox[id=chk_${vvcode}]").attr("checked", true);
				
// 				var m=$("#code").val();
// 				$("#code").val("").focus().val(m);
// 				var n=$("#des").val();
// 				$("#des").val("").focus().val(n);
// 				var t=$("#init").val();
// 				$("#init").val("").focus().val(t);
				
// 				var defaultCode = '${defaultCode}';
// 				var codes = defaultCode.split(',');
// 				if(defaultCode!=''){
// 					$('.grid').find('.table-body').find(':checkbox').each(function(){
// 						for(var i in codes){
// 							if(this.id.substr(4,this.id.length)==codes[i]){
// 								$(this).attr("checked", true);
// 							}
// 						}
// 					})	
// 				}
				
			});
		</script>
	</body>
</html>