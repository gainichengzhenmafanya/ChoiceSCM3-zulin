<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.table-head td span{
			white-space: normal;
		}
		#div1 { width: 16px;  position: absolute; z-index:11111;
		right: 0px; top: 47px; background: #F3F3F3; 
		} 
		#div2 { width: 16px; height: 20px; position: absolute; z-index:11111;
		right: 0; top: 0; background: #C1D3FB; margin-bottom: 25px;} 
		
		</style>							
	</head>
	<body>
	    <div id="div1"> 
            <div id="div2"></div> 
        </div> 
	
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supply/table.do?level=${level}&&code=${code}" method="post">
			<input type="hidden" id="is_supply_x" name="is_supply_x" value="${is_supply_x}"/>
			<div class="grid" id="div3">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
									<td><span style="width:${dictColumns.columnWidth}px;text-align:center;">
										${dictColumns.zhColumnName}
										</span>
									</td>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" id="div4">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="status" items="${supplyList}">
								<tr>
									<td><span class="num" style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code}" value="${supply.sp_code}"/></span>
									</td>
									<c:forEach var="dictColumns" items="${dictColumnsListByAccount}" varStatus="col">
										<td  align="left">
											<!-- 单元格内容过长时  防止换行 -->
											<span title="${elf:append(supply,dictColumns.properties,null)}" style="width:${dictColumns.columnWidth}px;table-layout:fixed; word-break:break-all;<c:if test='${col.count == 1 }'>text-align:left;</c:if>">
											 	${elf:append(supply,dictColumns.properties,null)}
											</span>
										</td>
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	
			<div class="search-div">
					<div class="search-condition">
						<table class="search-table" cellspacing="0" cellpadding="0">
							<tr>
								<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
								<td><input type="text" id="sp_init" name="sp_init" class="text" style="text-transform:uppercase;" onkeyup="ajaxSearch('sp_init')" value="${supply.sp_init}" /></td>
								<td class="c-left"><fmt:message key="supplies_no" />：</td>
								<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${supply.sp_code}" /></td>
								<td class="c-left"><fmt:message key="supplies_name" />：</td>
								<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${supply.sp_name}" /></td>
<!-- 								<td class="c-left">虚拟物料编码：</td> -->
<%-- 								<td><input type="text" id="sp_code_x" name="sp_code_x" class="text" onkeyup="ajaxSearch('sp_code_x')" value="${supply.sp_code_x}" /></td> --%>
<!-- 								<td class="c-left">虚拟物料名称：</td> -->
<%-- 								<td><input type="text" id="sp_name_x" name="sp_name_x" class="text" onkeyup="ajaxSearch('sp_name_x')" value="${supply.sp_name_x}" /></td> --%>
							</tr>
						</table>
					</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
			</form>
		<div class="print-div">
			<form id="printForm" action="<%=path%>/supply/toReport.do" method="post">
			<input type="hidden" name="level" value="${level}"/>
			<input type="hidden" name="code" value="${code}"/>
			<input type="hidden" name="type" value="excel"/>
			<input  type="hidden" name="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input  type="hidden" name="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="pageSize" value="${pageobj.pageSize }" />
			<div class="print-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left"><fmt:message key="export_range" />：</td>
						<td><input type="text" id="reportFrom" name="reportFrom"   value="${pageobj.nowPage}" size="4" /></td>
						<td class="c-left"><fmt:message key="to" />：</td>
						<td><input type="text" id="reportTo" name="reportTo"  value="${pageobj.pageCount}" size="4"  /><fmt:message key="page" /></td>
			<%-- 			<td class="c-left">&nbsp;&nbsp;&nbsp;&nbsp;导出类型：</td>  
						<td><img src="<%=path%>/image/Button/excel.bmp" />Excel<input type="radio" id="type" name="type" value="excel" checked="checked" />&nbsp;&nbsp;</td>
						<td><img src="<%=path%>/image/Button/pdf.png" />Pdf<input type="radio" id="type" name="type" value="pdf" checked="checked"/>&nbsp;&nbsp;</td>
						<td><img src="<%=path%>/image/Button/word.bmp" />word<input type="radio" id="type" name="type" value="word" />&nbsp;&nbsp;</td> --%>
					<!-- <td><img src="<%=path%>/image/Button/html.png" />Html<input type="radio" id="type" name="type" value="html" />&nbsp;&nbsp;</td>   -->	
					</tr>
				</table>
			</div>
			<div class="print-commit">
	       		<input type="button" class="print-button" id="print" value='<fmt:message key="export" />'/>
			</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			var t;
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//排序start
				var array = new Array();   
					<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
						array.push('${dictColumns.columnName}');
					</c:forEach>
				//array = ['chkinno','chkinno','chkinno','vouno','typ', 'maded', 'madet', 'positn','deliver','totalamt','madeby','checby'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				} 
				//排序结束
				//对齐方式
				var pattern = /^\d+(?=\.{0,1}\d+$|$)/;
				$('.grid').find('.table-body').find('tr:eq(0)').find('td').each(function(i){
					var tes=($(this).find('span').text()+"");
					tes=$.trim(tes);
					$('.grid').find('.table-head').find('tr:eq(0)').find('td:eq('+i+')').css('text-align','center');
					 if(pattern.test(tes)){
						$('.grid').find('.table-body').find('tr').find('td:eq('+i+')').css('text-align','right');
					 }
				});
				//对齐方式end
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							 $('.search-div').slideToggle(100);
							 var t = $('#sp_init').val();
							 $('#sp_init').focus().val(t);
						}
					}
					<c:if test="${is_supply_x=='Y'}">,{
						text: '虚拟物资查询修改',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							searchChkstom();
						}
					}</c:if>,"-",{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key="supplies_code" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveSupply();
							}
						},{
							text: '<fmt:message key="scm_copy"/>',
							title: '<fmt:message key="scm_copy"/><fmt:message key="supplies_code" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								copySupply();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" /><fmt:message key="supplies_code" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								
								position: ['-18px','0px']
							},
							handler: function(){
								updateSupply();
							}
						},{
							text: '<fmt:message key="column_selection" />',
							title: '<fmt:message key="column_selection" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-60px']
							},
							handler: function(){
								toSelectColumns();
							}
						},{
							text: '<fmt:message key="export" />',
							title: '<fmt:message key="exporting_reports" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['2px','2px']
							},
							handler: function(){
								//if(confirm("是否要导出所有物资？由于网络原因可能会需要些时间，请耐心等待！建议使用IE下载，避免使用迅雷等下载工具.."))
								//$('#printForm').submit();
								 $('.print-div').slideToggle(100);
							}
						},{
							text: '<fmt:message key="import" />',
							title: '<fmt:message key="import" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['2px','2px']
							},
							handler: function(){
								parent.importSupply();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key="supplies_code" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteSupply();
							
							}
						},{
							text: '<fmt:message key="image_maintenance"/>',
							title: '<fmt:message key="image_maintenance"/>',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								uploadSupplyImg();
							
							}
						},{
							text: '<fmt:message key="join_firm"/>',
							title: '<fmt:message key="join_firm"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'build')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								joinPositn();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
								
							}
						}
					]
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				//来自删除的提示
				var result="${result}";
				if(""!=result){
					alert(result);
				}
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				/* 导出提交 */
				$("#print").bind('click', function() {
					var reportFrom=$('#reportFrom').val();
					var reportTo=$('#reportTo').val();
					var ex = /^[0-9]*[1-9][0-9]*$/;
					if (ex.test(reportFrom) && ex.test(reportTo)) {
						$('.print-div').hide();
						$('#printForm').submit();
						
					}else{
						alert('<fmt:message key="export_range_please_enter_positive_integer" />！');
					}

				});
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//查找虚拟物资
				function searchChkstom(){
					var curwindow = $('body').window({
						id: 'window_searchChkstom',
						title: '<fmt:message key ="query" />',
						content: '<iframe id="searchChkstomFrame" frameborder="0" src="<%=path%>/supply/searchSupply_x.do?init=init"></iframe>',
						width: '800px',
						height: '430px',
						draggable: true,
						isModal: true
					});
					curwindow.max();
				}
				
				function saveSupply(){
					var savebody=$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="insert" /><fmt:message key="supplies_code" />',
						content: '<iframe id="saveSupplyFrame" name="saveSupplyFrame" frameborder="0" src="<%=path%>/supply/add.do?level=${level}&&code=${code}"></iframe>',
						width: '750px',
						height: '550px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										//判断是否有税率
										if(!window["saveSupplyFrame"].judgeTax()){
											return;
										}
										var state=0;
// 										if($('#is_supply_x').val() == 'N'){
										state = window["saveSupplyFrame"].fuzhi();
// 										}
										if(state == 0){
											document.getElementById('saveSupplyFrame').contentWindow.setAction();
											if(getFrame('saveSupplyFrame')&&window.document.getElementById("saveSupplyFrame").contentWindow.validate._submitValidate()){
												//.先判断是不是有虚拟物料 wjf
												if($('#is_supply_x').val() == 'Y'){
													if(window.document.getElementById("saveSupplyFrame").contentWindow.checkUnit2() == 1 
															&& window.document.getElementById("saveSupplyFrame").contentWindow.checkPositnex()
															&& window.document.getElementById("saveSupplyFrame").contentWindow.checkSupply_x()
															&& window.document.getElementById("saveSupplyFrame").contentWindow.checkUnit()){
														$("#window_supply").find(".button:first").unbind();
														submitFrameForm('saveSupplyFrame','SupplyForm');
													}
												}else{
													if(window.document.getElementById("saveSupplyFrame").contentWindow.checkPositnex()
															&& window.document.getElementById("saveSupplyFrame").contentWindow.checkAccprate()){
														$("#window_supply").find(".button:first").unbind();
														submitFrameForm('saveSupplyFrame','SupplyForm');
													}
												}
											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					savebody.max();
				}
				function copySupply(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						var copybody=$('body').window({
							id:'window_supply_copy',
							title: '<fmt:message key="copy" /><fmt:message key="supplies_code" />',
							content: '<iframe id="saveSupplyFrame" name="saveSupplyFrame" frameborder="0" src="<%=path%>/supply/copy.do?sp_code='+chkValue+'"></iframe>',
							width: '750px',
							height: '550px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
										useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											//判断是否有税率
											if(!window["saveSupplyFrame"].judgeTax()){
												return;
											}
											var state=0;
// 											if($('#is_supply_x').val() == 'N'){
											state = window["saveSupplyFrame"].fuzhi();
// 											}
											if(state == 0){
												if(getFrame('saveSupplyFrame')&&window.document.getElementById("saveSupplyFrame").contentWindow.validate._submitValidate()){
													//.先判断是不是有虚拟物料 wjf
													if($('#is_supply_x').val() == 'Y'){
														if(window.document.getElementById("saveSupplyFrame").contentWindow.checkUnit2() == 1 
																&& window.document.getElementById("saveSupplyFrame").contentWindow.checkPositnex()
																&& window.document.getElementById("saveSupplyFrame").contentWindow.checkSupply_x()
																&& window.document.getElementById("saveSupplyFrame").contentWindow.checkUnit()){
															$("#window_supply_copy").find(".button:first").unbind();
															submitFrameForm('saveSupplyFrame','SupplyForm');
														}
													}else{
														if(window.document.getElementById("saveSupplyFrame").contentWindow.checkPositnex()
																&& window.document.getElementById("saveSupplyFrame").contentWindow.checkAccprate()){
															$("#window_supply_copy").find(".button:first").unbind();
															submitFrameForm('saveSupplyFrame','SupplyForm');
														}
													}
												}
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
						copybody.max();
					}else	if(checkboxList 
							&& checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="please_select_data" />！');
						return ;
					}
					else{
						alert('<fmt:message key ="Choose_the_material_to_be_copied" />！');
						return ;
					}
					
				}
				function updateSupply(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						var updatebody=$('body').window({
							id:'window_supply_update',
							title: '<fmt:message key="update" /><fmt:message key="supplies_code" />',
							content: '<iframe id="updateSupplyFrame" name="updateSupplyFrame" frameborder="0" src="<%=path%>/supply/update.do?sp_code='+chkValue+'"></iframe>',
							width: '750px',
							height: '550px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
										useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											//判断是否有税率
											if(!window["updateSupplyFrame"].judgeTax()){
												return;
											}
											var state=0;
// 											if($('#is_supply_x').val() == 'N'){
											state = window["updateSupplyFrame"].fuzhi();
// 											}
											if(state == 0){
												if(getFrame('updateSupplyFrame')&&window.document.getElementById("updateSupplyFrame").contentWindow.validate._submitValidate()){
													//.先判断是不是有虚拟物料 wjf
													if($('#is_supply_x').val() == 'Y'){
														if(window.document.getElementById("updateSupplyFrame").contentWindow.checkUnit2() == 1 
																&& window.document.getElementById("updateSupplyFrame").contentWindow.checkPositnex()
																&& window.document.getElementById("updateSupplyFrame").contentWindow.checkUnit()){
															$("#window_supply_update").find(".button:first").unbind();
															submitFrameForm('updateSupplyFrame','SupplyForm');
														}
													}else{
														if(window.document.getElementById("updateSupplyFrame").contentWindow.checkPositnex()
																&& window.document.getElementById("updateSupplyFrame").contentWindow.checkAccprate()){
															$("#window_supply_update").find(".button:first").unbind();
															submitFrameForm('updateSupplyFrame','SupplyForm');
														}
													}
												}
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
						updatebody.max();
					}else	if(checkboxList 
							&& checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="please_select_data" />！');
						return ;
					}
					else{
						alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						return ;
					}
					
				}
				function toReport(){
					
					$('window.parent.parent.body').window({
						title: '<fmt:message key="print" />',
						content: '<iframe frameborder="0" src="<%=path%>/supply/toReport.do?level=${level}&&code=${code}"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true
						});
				}
				function toSelectColumns(){
					$('body').window({
						title: '<fmt:message key="column_selection" />',
						content: '<iframe frameborder="0" src="<%=path%>/supply/toColumnsChoose.do"></iframe>',
						width: '460px',
						height: '430px',
						draggable: true,
						isModal: true
						});
				}
				
				function deleteSupply(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							
							var action = '<%=path%>/supply/delete.do?supplyId='+chkValue.join(",")+'&level=${level}&&code=${code}';
							
							$('#listForm').attr('action',action);
							$('#listForm').submit();
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//滚动开始
			    var oDiv1 = document.getElementById('div1');
			    var oDiv2 = document.getElementById('div2');
			    var oDiv4 = document.getElementById('div4');			    
				$('#div1').height($(".table-body").height());
				$(".table-body").scroll(function() {//滚动时候
					 oDiv2.style.top = $(".table-body").scrollTop()/oDiv4.scrollHeight*$(".table-body").height()+'px';
					 if($(".table-body").scrollTop()>=20){
						 oDiv2.style.top = ($(".table-body").scrollTop()/oDiv4.scrollHeight*$(".table-body").height()+20)+'px'
					 } 
				});
				$('#div2').height($(".table-body").height()/oDiv4.scrollHeight*$(".table-body").height()-17);
				if($(".table-body").height()>=oDiv4.scrollHeight){
					$('#div1').remove();
					$('#div2').remove();
				}	
			    var iMaxHeight = oDiv1.offsetHeight - oDiv2.offsetHeight;
			    oDiv2.onmousedown = function(ev) {
			        var ev = ev || event;
			        var disY = ev.clientY - this.offsetTop;
			        document.onmousemove = function(ev) {
			            var ev = ev || event;
			            var T = ev.clientY - disY;
			            if (T < 0) {
			                T = 0;
			            } else if (T > iMaxHeight) {
			                T = iMaxWidth;
			            }
			            var iScale = T / iMaxHeight;
			               oDiv2.style.top = T + 'px';
			             $(".table-body").scrollTop(T/$(".table-body").height()*oDiv4.scrollHeight);
			        }
			        document.onmouseup = function() {
			            document.onmousemove = document.onmouseup = null;
			        }
			        return false;
			    }
				
			    /***双击 修改***/
			    $(".table-body").find("tr").dblclick(function(){
			    	//取物资编码
			    	var chkValue = $(this).find("td:eq(1) input").val();
			    	var updatebody=$('body').window({
						id:'window_supply_update',
						title: '<fmt:message key="update" /><fmt:message key="supplies_code" />',
						content: '<iframe id="updateSupplyFrame" name="updateSupplyFrame" frameborder="0" src="<%=path%>/supply/update.do?sp_code='+chkValue+'"></iframe>',
						width: '750px',
						height: '550px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										//判断是否有税率
										if(!window["updateSupplyFrame"].judgeTax()){
											return;
										}
										var state=0;
//											if($('#is_supply_x').val() == 'N'){
										//单位通过此方法赋值
										state = window["updateSupplyFrame"].fuzhi();
//											}
										if(state == 0){
											if(getFrame('updateSupplyFrame')&&window.document.getElementById("updateSupplyFrame").contentWindow.validate._submitValidate()){
												//.先判断是不是有虚拟物料 wjf
												if($('#is_supply_x').val() == 'Y'){
													if(window.document.getElementById("updateSupplyFrame").contentWindow.checkUnit2() == 1 
															&& window.document.getElementById("updateSupplyFrame").contentWindow.checkPositnex()
															&& window.document.getElementById("updateSupplyFrame").contentWindow.checkUnit()){
														$("#window_supply_update").find(".button:first").unbind();
														submitFrameForm('updateSupplyFrame','SupplyForm');
													}
												}else{
													if(window.document.getElementById("updateSupplyFrame").contentWindow.checkPositnex()
															&& window.document.getElementById("updateSupplyFrame").contentWindow.checkAccprate()){
														$("#window_supply_update").find(".button:first").unbind();
														submitFrameForm('updateSupplyFrame','SupplyForm');
													}
												}
											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					updatebody.max();
			    });
		//滚动结束
			});
			function changer_hidden(id,enum_meaning)
		    {
				$("#hidden_id").attr("value",id);
				$("#hidden_name").attr("value",enum_meaning);
				 var form =window.frames['listForm'];
			      	form.action = "<%=path%>/supply/list.do?parent_id="+id;
			      	form.submit();
		    }
  			function pageReload(){
				$('#listForm').submit();
			}
  			
  			//图片维护
  			function uploadSupplyImg(){
  				var chkValue;
  				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					chkValue = checkboxList.filter(':checked').eq(0).val();
				}else{
					chkValue = checkboxList.eq(0).val();
				}
				$('body').window({
					title: '<fmt:message key="image_maintenance"/>',
					content: '<iframe id="uploadSupplyImg" name="uploadSupplyImg" frameborder="0" src="<%=path%>/supply/toUploadSupplyImg.do?sp_code='+chkValue+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true
				});
  			}
  			function preSupply(sp_code){
  				return $('#chk_'+sp_code).parent().parent().parent().prev().find('input[type=checkbox]').val();
  			}
  			function nextSupply(sp_code){
  				return $('#chk_'+sp_code).parent().parent().parent().next().find('input[type=checkbox]').val();
  			}
  			
  		//直接加入分店物资属性
  			function joinPositn(){
  				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						$('body').window({
							title: '<fmt:message key="join_firm"/>',
							content: '<iframe id="joinPositn" frameborder="0" src="<%=path%>/supply/toJoinPositn.do?ids='+chkValue.join(",")+'"></iframe>',
							width: '850px',
							height: '500px',
							draggable: true,
							isModal: true
						});
				}else{
					alert('<fmt:message key ="Choose_the_material_you_want_to_add" />！');
					return ;
				}
  			}
  		
  		//直接加入用户名下
  			function joinAccount(){
  				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						$('body').window({
							title: '<fmt:message key="Adapter_user" />',
							content: '<iframe id="joinAccount" frameborder="0" src="<%=path%>/supply/toJoinAccount.do?ids='+chkValue.join(",")+'"></iframe>',
							width: '750px',
							height: '500px',
							draggable: true,
							isModal: true
						});
				}else{
					alert('<fmt:message key="Choose_the_material_you_want_to_add" />！');
					return ;
				}
  			}
		</script>
	</body>
</html>