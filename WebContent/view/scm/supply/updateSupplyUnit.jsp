<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:40px;">
			<form id="supplyUnitForm" method="post" action="<%=path %>/supply/saveUnitByUpdate.do">
				<input type="hidden" id="sp_code" name="sp_code" value="${supplyUnit.sp_code}"/>
				<input type="hidden" id="id" name="id" value="${supplyUnit.id}"/>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />：</div>
					<div class="form-input">
						<select class="select" id="unit" name="unit"  style="width:133px">
							<c:forEach var="unit" items="${unitList}" varStatus="status">
								<option
									<c:if test="${supplyUnit.unit==unit.des}"> 
								  	 	selected="selected"
									</c:if>  
								id="${unit.code}" value="${unit.des}">${unit.des}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="unit_conversion_rate" />：</div>
					<div class="form-input"><input type="text" id="unitper" name="unitper" class="text"  value="${supplyUnit.unitper}"/></div>
				</div>
			</form>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'unit',
						validateType:['canNull'],
						param:['F'],
						error:['单位<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'unitper',
						validateType:['canNull','maxLength','num1'],
						param:['F',5,'F'],
						error:['<fmt:message key="cannot_be_empty" />！','<fmt:message key="length_too_long" />！','单位转换率必须为数字']
					}]
				});
			});
		</script>
	</body>
</html>