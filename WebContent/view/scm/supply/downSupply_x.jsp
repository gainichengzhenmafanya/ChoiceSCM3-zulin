<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品成本卡实际物料</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.page{margin-bottom: 25px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="grid" id="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span style="width:21px;">&nbsp;</span></td>
							<td><span style="width:100px;"><fmt:message key="supplies_code"/></span></td>
							<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
							<td><span style="width:100px;"><fmt:message key ="abbreviation" /></span></td>
							<td><span style="width:100px;"><fmt:message key ="supplies_specifications" /></span></td>
							<td><span style="width:100px;"><fmt:message key ="standard_unit" /></span></td>
							<td><span style="width:100px;"><fmt:message key ="cost_per_unit" /></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="supply" items="${downSupplyList}" varStatus="status">
							<tr>
								<td class="num"><span style="width:21px;">${status.count}</span></td>
								<td><span title="${supply.sp_code}" style="width:100px;">${supply.sp_code}</span></td>
								<td><span title="${supply.sp_name}" style="width:100px;">${supply.sp_name}</span></td>
								<td><span title="${supply.sp_init}" style="width:100px;">${supply.sp_init}</span></td>
								<td><span style="width:100px;">${supply.sp_desc}</span></td>
								<td><span style="width:100px;">${supply.unit}</span></td>
								<td><span style="width:100px;">${supply.unit2}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
	
		$(document).ready(function(){
			setElementHeight('.grid',0,$(document.body),25);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			//跳转到物资编辑页面
			//双单 击事件
			$('.grid .table-body tr').live('click',function(){
// 				 $('#sp_code').val($(this).find('td:eq(1)').find('span').attr('title'));
// 				 $('#sp_name').val($(this).find('td:eq(2)').find('span').attr('title'));
// 				 $('#sp_init').val($(this).find('td:eq(3)').find('span').attr('title'));
				 var sp_code=$(this).find('td:eq(1)').find('span').attr('title');
// 				 var sp_name=$(this).find('td:eq(2)').find('span').attr('title');
// 				 var sp_init=$(this).find('td:eq(3)').find('span').attr('title');
// 				 $("#sp_code").val(sp_code);
				updateSupply(sp_code);
			});
			
			function updateSupply(sp_code){
					parent.$('body').window({
						title: '<fmt:message key="update" /><fmt:message key="supplies_code" />',
						content: '<iframe id="updateSupplyFrame" frameborder="0" src="<%=path%>/supply/update.do?sp_code='+sp_code+'"></iframe>',
						width: '550px',
						height: '400px',
						async:false,
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(parent.getFrame('updateSupplyFrame')&&parent.window.document.getElementById("updateSupplyFrame").contentWindow.validate._submitValidate()){
											if(parent.window.document.getElementById("updateSupplyFrame").contentWindow.checkUnit2() == 1){
												parent.submitFrameForm('updateSupplyFrame','SupplyForm');
												parent.window.submitFrame();
											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										parent.$('.close').click();
									}
								}
							]
						}
					});
			}
		});
		</script>
	</body>
</html>