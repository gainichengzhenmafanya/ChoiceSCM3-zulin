<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	    <style type="text/css">
			#tblGrid td{
				border-right: 1px solid #999999;
				border-bottom:1px solid #999999; 
			}
			.form-line .form-label {
				width:200px;
			}
			.divgrid td span {
			  display: block;
			  overflow: hidden;
			  white-space: nowrap;
			  text-overflow: ellipsis;
			  padding: 0px 5px;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="SupplyForm" method="post" action="<%=path %>/supply/saveByAdd.do">
			<input type="hidden" id="orderno" name="orderno"/><!-- 存放单位的顺序 -->
			<input type="hidden" id="disunit" name="disunit"/><!-- 配送单位-->
				<div class="easyui-tabs" fit="false" plain="true" style="height:420px;width:845px;z-index:88;margin:0px auto;">
				<!-- 基本信息-->
				<div title='<fmt:message key="basic_information" />' style="padding:15px;margin-left:100px;margin-top:10px;">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_category" />：</div>
						<div class="form-input">
							<input type="text" id="typdes" name="typdes" class="selectDepartment text" value="${supply.typdes}"/>
							<input type="hidden" id="sp_type" name="sp_type" class="text"  value="${supply.sp_type}"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_code" />：</div>
						<div class="form-input">
							<input type="text" id="sp_code" name="sp_code" class="text"  value="${supply.sp_code}"/>
						</div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="supplies_name" />：</div>
						<div class="form-input"><input type="text" id="sp_name" name="sp_name" class="text"  value="${supply.sp_name}" onblur="getSpInit(this);"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_abbreviations" />：</div>
						<div class="form-input"><input type="text" id="sp_init" name="sp_init" class="text"  value="${supply.sp_init}"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="supplies_specifications" />：</div>
						<div class="form-input"><input type="text" id="sp_desc" name="sp_desc" class="text"  value="${supply.sp_desc}"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="shelf" />：</div>
						<div class="form-input">
							<select id="positn1" name="positn1"  style="width: 129px;" class="select">
								<option value=""></option>
								<c:forEach var="positn" items="${positn1List}" varStatus="status">
									<option id="${positn.code}" value="${positn.des}" <c:if test="${positn.des == supply.positn1 }">selected="selected"</c:if>>${positn.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="barcode" />：</div>
						<div class="form-input"><input type="text" id="sp_code1" name="sp_code1" class="text" value="${supply.sp_code1}"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="default_position" />：</div>
						<div class="form-input">
							<select id="sp_position" name="sp_position" style="width: 129px;" class="select">
								<c:forEach var="positn" items="${positnList}" varStatus="status">
									<option
										<c:if test="${supply.sp_position==positn.code}"> 
									  	 	selected="selected"
										</c:if>  
									id="${positn.code}" value="${positn.code}">${positn.des}</option>
								</c:forEach>
							</select>
						</div>
						<input type="hidden" id="positndes" name="positndes" ></input>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supply_units" />：</div>
						<div class="form-input">
							<input class="text" id="deliverdes" name="deliverdes" value="${supply.deliverdes }"/>
							<input type="hidden" id="deliver" name="deliver" class="text" value="${supply.deliver }"/>
							<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询供应商' />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplyclassification"/>：</div>
						<div class="form-input">
							<select id="typ_eas" name="typ_eas" style="width: 129px;" class="select"> 
<!-- 								<option value=""></option> -->
								<c:forEach items="${typEasList }" var="typEas">
									<c:if test="${supply.typ_eas==typEas.code}">
										<option value="${typEas.code }" selected="selected">${typEas.des }</option>
									</c:if>
									<c:if test="${supply.typ_eas!=typEas.code}">
										<option value="${typEas.code }">${typEas.des }</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="processing_room" /><fmt:message key="The_material" /><fmt:message key="attribute" />：</div>
						<div class="form-input">
							<select id="exkc" name="exkc" style="width: 129px;" class="select">
								<option value="Y" <c:if test="${supply.exkc=='Y' or supply.exkc==null}">selected</c:if>><fmt:message key="The_material" /></option>
								<option value="N" <c:if test="${supply.exkc=='N'}">selected</c:if>><fmt:message key="do_not" /><fmt:message key="The_material" /></option>
							</select>
						</div>
					</div>
					<div class="form-line">
				    	<div class="form-label"><fmt:message key="status" />：</div>
						<div class="form-input">
							<fmt:message key="enable" /><input type="radio" id="sta" name="sta" class="text" value="Y"
								<c:if test="${supply.sta=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="disabled" /><input type="radio" id="sta" name="sta" class="text" value="N"
								<c:if test="${supply.sta=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div>
					<%-- <div class="form-line">
				    	<div class="form-label"><fmt:message key="Use_state" />：</div>
						<div class="form-input">
							<fmt:message key="enable" /><input type="radio" id="locked" name="locked" class="text" value="Y"
								<c:if test="${supply.locked=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="disabled" /><input type="radio" id="locked" name="locked" class="text" value="N"
								<c:if test="${supply.locked=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div> --%>
				</div>
				<!-- 换算单位-->
				<div title="<fmt:message key ="convert_units" />" style="padding:10px;margin-left:0px;margin-top:10px;">
					<div id="unitForm" class="unitForm" style="width: 100%;height: 95%;margin: auto;margin-top: 10px;">
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="standard_unit" />:</div>
							<div class="form-input"><span class="unit">${supply.unit }</span></div>
						</div>
						<div id="divgrid" class="divgrid" style="border-left: 1px solid #999999;width: 100%;margin-top: 10px;">
							<div class="table-head">
								<table width="100%;">
									<thead>
										<tr style="height: 25px;">
											<td style="width:75px;*width:80px;" title='<fmt:message key="unit"/>'><span style="width:65px"><fmt:message key="unit"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="the_conversion_rate"/>'><span style="width:50px"><fmt:message key="the_conversion_rate"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="standard_unit"/>'><span style="width:50px"><fmt:message key="standard_unit"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="procurement_unit"/>'><span style="width:50px"><fmt:message key="procurement_unit"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="stock_keeping_unit"/>'><span style="width:50px"><fmt:message key="stock_keeping_unit"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="cost_per_unit"/>'><span style="width:50px"><fmt:message key="cost_per_unit"/></span></td>
											<td style="width:60px;*width:60px;" title='<fmt:message key="the_reference_unit"/>'><span style="width:50px"><fmt:message key="the_reference_unit"/></span></td>
											<td style="width:100px;*width:100px;" title='<fmt:message key="specifications"/>'><span style="width:90px"><fmt:message key="specifications"/></span></td>
											<td style="width:60px;text-align: center;"><span style="width:50px"><fmt:message key="Distribution_Unit"/></span></td>
											<td style="width:70px;text-align: center;"><span style="width:60px"><fmt:message key="min_chkstom_cnt"/></span></td>
											<td style="width:60px;*width:60px;"><input type="button" id="addrow" class="addrow" style="width: 50px;" value="<fmt:message key="add"/>"/></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table id="tblGrid" class="tblGrid">
									<tbody>
										<c:forEach items="${supplyunitList }" var="supplyunit" varStatus="index">
											<c:if test="${supplyunit.unit !='null' and supplyunit.unit!=''}">
												<tr style="height: 25px;" data-yndelete="${supplyunit.ynDelete }" data-yndisunit="${supplyunit.ynDisunit }" data-unit="${supplyunit.unit }">
													<td style="width:80px;" align="center">
<%-- 														<input style="width: 30px;outline: none;" class="writeunit" value="${fn:substring(str,0,fn:indexOf(str,',')) }" type="hidden"/> --%>
														<select class="select" id="pk_unit1" name="pk_unit1" style="width:50px;margin-top: -1px;" onchange="change(this)">
															<c:forEach var="unit" items="${unitList}" varStatus="status" >
																<option id="${unit.code}" value="${unit.des}" <c:if test="${supplyunit.unit == unit.des }">selected="selected"</c:if>>${unit.des}</option>
															</c:forEach>
														</select>
													</td>
													<td style="width:67px;" align="center">
														<input type="text" id="nrate" name="nrate" style="width:50px;text-align: right;" value="${supplyunit.unitper }" <c:if test="${index.count ==1 }">disabled="disabled"</c:if>/>
													</td>
													<td style="width:65px;text-align: center;"><input type="checkbox" disabled="disabled" <c:if test="${supplyunit.unit == supply.unit }">checked="checked"</c:if>/></td>
													<td style="width:65px;text-align: center;"><input type="checkbox" <c:if test="${supplyunit.unit == supply.unit3}">checked="checked"</c:if>/></td>
													<td style="width:65px;text-align: center;"><input type="checkbox" <c:if test="${supplyunit.unit == supply.unit4}">checked="checked"</c:if>/></td>
													<td style="width:65px;text-align: center;"><input type="checkbox" <c:if test="${supplyunit.unit == supply.unit2}">checked="checked"</c:if>/></td>
													<td style="width:65px;text-align: center;"><input type="checkbox" <c:if test="${supplyunit.unit == supply.unit1}">checked="checked"</c:if>/></td>
													<td style="width:106px;text-align: center;">
														<select>
															<c:if test="${index.count == 1 }">
																<option value="1"><fmt:message key="specifications1"/><fmt:message key="scm_one"/></option>
															</c:if>
															<c:if test="${index.count != 1 }">
															<option value="0" <c:if test="${supplyunit.sequence == 0 }">selected="selected"</c:if>>&nbsp;</option>
<%-- 															<option value="1" <c:if test="${supplyunit.sequence == 1 }">selected="selected"</c:if>><fmt:message key="specifications1"/><fmt:message key="scm_one"/></option> --%>
															<option value="2" <c:if test="${supplyunit.sequence == 2 }">selected="selected"</c:if>><fmt:message key="specifications1"/><fmt:message key="scm_two"/></option>
															<option value="3" <c:if test="${supplyunit.sequence == 3 }">selected="selected"</c:if>><fmt:message key="specifications1"/><fmt:message key="scm_three"/></option>
															<option value="4" <c:if test="${supplyunit.sequence == 4 }">selected="selected"</c:if>><fmt:message key="specifications1"/><fmt:message key="scm_four"/></option>
															</c:if>
														</select>
													</td>
													<td style="width:65px;text-align: center;"><input type="checkbox" class="ynDisunit" <c:if test="${supplyunit.ynDisunit == 'Y'}">checked="checked"</c:if>/></td>
													<td style="width:75px;" align="center">
														<input type="text" style="width:50px;text-align: right;" value="${supplyunit.dismincnt }"/>&nbsp;
													</td>
													<td style="width:65px;text-align: center;"><span style="cursor:pointer;color:red;">
														<c:if test="${index.count != 1 }">X</c:if>
														<c:if test="${index.count == 1 }">&nbsp;</c:if>
													</span></td>
												</tr>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
								<table style="display: none;">
									<tr>
										<td><input id="unit" name="unit" type="hidden"/></td>
										<td><input id="unitper0" name="unitper0" type="hidden"/></td>
									</tr>
									<tr>
										<td><input id="unit1" name="unit1" type="hidden"/></td>
										<td><input id="unitper" name="unitper" type="hidden"/></td>
									</tr>
									<tr>
										<td><input id="unit2" name="unit2" type="hidden"/></td>
										<td><input id="unitper2" name="unitper2" type="hidden"/></td>
									</tr>
									<tr>
										<td><input id="unit3" name="unit3" type="hidden"/></td>
										<td><input id="unitper3" name="unitper3" type="hidden"/></td>
									</tr>
									<tr>
										<td><input id="unit4" name="unit4" type="hidden"/></td>
										<td><input id="unitper4" name="unitper4" type="hidden"/></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- 采购周期-->
				<div title='<fmt:message key="procurement_cycle" />' style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="procurement_cycle" /><fmt:message key="dateunit" />：</div>
						<div class="form-input"><input type="text" id="datsto" name="datsto"  value="${supply.datsto}" class="text"/></div>
						<div class="form-label"><fmt:message key="actual_stock_limit" />：</div>
						<div class="form-input"><input type="text" id="sp_max1" name="sp_max1" value="${supply.sp_max1}" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="delivery_time" /><fmt:message key="dateunit" />：</div>
						<div class="form-input"><input type="text" id="cntminsto" name="cntminsto" value="${supply.cntminsto}" class="text"/></div>
						<div class="form-label"><fmt:message key="actual_stock_lower_limit" />：</div>
						<div class="form-input"><input type="text" id="sp_min1" name="sp_min1" value="${supply.sp_min1}" class="text"/></div>
<%-- 						<div class="form-label"><fmt:message key="reference_inventory_capping" />：</div> --%>
<%-- 						<div class="form-input"><input type="text" id="sp_max2" name="sp_max2" value="${supply.sp_max2}" class="text"/></div> --%>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="average_daily_consumption" />：</div>
						<div class="form-input"><input type="text" id="cntuse" name="cntuse"  value="<fmt:formatNumber value="${supply.cntuse}" pattern="##.##" minFractionDigits="2" />" class="text"/></div>
						<div class="form-label"><fmt:message key="stores_month_procurement_limit" />：</div>
						<div class="form-input"><input type="text" id="stomax" name="stomax"  value="${supply.stomax}" class="text"/></div>
						
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="branche_acceptance_ratio" /><fmt:message key="scm_ceiling"/>：</div>
						<div class="form-input"><input type="text" id="accprate" name="accprate"  value="${supply.accprate}" class="text"/></div>
						<div class="form-label"><fmt:message key="branche_acceptance_ratio" /><fmt:message key="the_lower_limit"/>：</div>
						<div class="form-input"><input type="text" id="accpratemin" name="accpratemin"  value="${supply.accpratemin}" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="require_special_audit" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="stochk1" name="stochk1" class="text" value="Y"
								<c:if test="${supply.stochk1=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="no1" /><input type="radio" id="stochk1" name="stochk1" class="text" value="N"
								<c:if test="${supply.stochk1=='N'}"> checked="checked"</c:if> 
							/>
						</div>
						<div class="form-label"><fmt:message key="whether_the_headquarters_to_review"/>：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ynCheck" name="ynCheck" class="text" value="Y"
								<c:if test="${supply.ynCheck=='Y'}"> checked="checked"</c:if>
							/>
							<fmt:message key="no1" /><input type="radio" id="ynCheck" name="ynCheck" class="text" value="N" 
								<c:if test="${supply.ynCheck=='N' || supply.ynCheck == ''}"> checked="checked"</c:if>
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="stores_to_purchase_single_limited" />：</div>
						<div class="form-input"><input type="text" id="stomax1" name="stomax1"  value="${supply.stomax1}" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Purchase_multiple" />：</div>
						<div class="form-input"><input type="text" id="mincnt" name="mincnt"  value="${supply.mincnt}" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="turnover_period" /><fmt:message key="dateunit" />：</div>
						<div class="form-input"><input type="text" id="sp_per1" name="sp_per1"  value="${supply.sp_per1}" class="text"/></div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="number_of_days_alarm_in_advance" /><fmt:message key="dateunit" />：</div>
						<div class="form-input"><input type="text" id="sp_per2" name="sp_per2" value="${supply.sp_per2}" class="text"/></div>
					</div>
				</div>
				<!-- 半成品-->
				<div title='<fmt:message key="whether_semi" />' style="padding:10px;margin-left:100px;margin-top:80px;" class="whether_semi">
					<div class="form-line">
						<div class="form-label"><fmt:message key="whether_branches_semi" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ex" name="ex" class="text" value="Y"
								<c:if test="${supply.ex=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="no1" /><input type="radio" id="ex" name="ex" class="text" value="N"
								<c:if test="${supply.ex=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="whether_the_center_of_semi_finished_products"/>：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ex1" name="ex1" class="text" value="Y"
								<c:if test="${supply.ex1=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="no1" /><input type="radio" id="ex1" name="ex1" class="text" value="N" 
								<c:if test="${supply.ex1=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="produced_positions" />：</div>
						<div class="form-input">
							<select class="select" id="positnex" name="positnex" onchange="getValue(this);" style="width: 129px;" class="select" <c:if test="${supply.ex=='N' && supply.ex1=='N' }">disabled="disabled"</c:if>>
								<option id="" value=""></option>
								<c:forEach var="positnex" items="${positnexList}" varStatus="status">
									<option
										<c:if test="${supply.positnex==positnex.code}"> 
									  	 	selected="selected"
										</c:if>  
									id="${positnex.code}" value="${positnex.code}">${positnex.des}</option>
								</c:forEach>
							</select>
							<input type="hidden" id="positnexdes" name="positnexdes" class="text"  value="${supply.positnexdes}"/>
						</div>
				     </div>
				     <div class="form-line">
						<div class="form-label"><fmt:message key="processing_time"/>：</div>
						<div class="form-input">
							<input type="text" id="extim" name="extim" class="text" value="${supply.extim}" <c:if test="${supply.ex=='N' && supply.ex1=='N' }">disabled="disabled"</c:if>/>&nbsp;&nbsp;<fmt:message key ="hours" />
						</div>
				     </div>
<!-- 					<div class="form-line"> -->
<%-- 						<div class="form-label"><fmt:message key="cost_of_fare_increase_ratio" />：</div> --%>
<!-- 						<div class="form-input"> -->
<!-- 							<input type="text" id="" name="" class="text" value="0" style="width:136px"/> -->
<!-- 						</div> -->
<!-- 				     </div> -->
				</div>
				<!-- 辅助信息-->
				<div title='<fmt:message key="auxiliary_information" />' style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="standard_price"/>：</div>
						<div class="form-input"><input type="text" id="sp_price" name="sp_price" class="text"  value="${supply.sp_price}"/></div>
						<div class="form-label"><fmt:message key="whether_direct_dial" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="inout" name="inout" class="text" value="Y"
								<c:if test="${supply.inout=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="no1" /><input type="radio" id="inout" name="inout" class="text" value="N" 
								<c:if test="${supply.inout=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="materials_price" />：</div>
						<div class="form-input"><input type="text" id="pricesale" name="pricesale" class="text"  value="${supply.pricesale}"/></div>
						<div class="form-label"><fmt:message key="whether_consignment" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="yndx" name="yndx" class="text" value="Y"
								<c:if test="${supply.yndx=='Y'}"> checked="checked"</c:if> 
							/>
							<fmt:message key="no1" /><input type="radio" id="yndx" name="yndx" class="text" value="N" 
								<c:if test="${supply.yndx=='N'}"> checked="checked"</c:if> 
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_brands" />：</div>
						<div class="form-input"><input type="text" id="sp_mark" name="sp_mark" class="text"  value="${supply.sp_mark}"/></div>
						<div class="form-label"><fmt:message key="whether_solar_disk" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ydaypan" name="yndaypan" class="text" value="Y"
								<c:if test="${supply.yndaypan=='Y'}"> checked="checked"</c:if>  
							/>
							<fmt:message key="no1" /><input type="radio" id="ndaypan" name="yndaypan" class="text" value="N" 
								<c:if test="${supply.yndaypan=='N'}"> checked="checked"</c:if>  
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_origin" />：</div>
						<div class="form-input"><input type="text" id="sp_addr" name="sp_addr" class="text"  value="${supply.sp_addr}"/></div>
						<div class="form-label"><fmt:message key="whether_week_disk" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="yweekpan" name="ynweekpan" class="text" value="Y"
								<c:if test="${supply.ynweekpan=='Y'}"> checked="checked"</c:if>  
							/>
							<fmt:message key="no1" /><input type="radio" id="nweekpan" name="ynweekpan" class="text" value="N"
								<c:if test="${supply.ynweekpan=='N'}"> checked="checked"</c:if>  
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="reference_category" />：</div>
						<div class="form-input">
							<select id="typoth" name="typoth" style="width: 133px;*width:128px;">
								<c:forEach items="${typothList }" var="typoth">
									<c:if test="${supply.typoth==typoth.code}">
										<option value="${typoth.code }" selected="selected">${typoth.des }</option>
									</c:if>
									<c:if test="${supply.typoth!=typoth.code}">
										<option value="${typoth.code }">${typoth.des }</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="whether_boutique" />：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="sp_cost" name="sp_cost" class="text" value="Y"
								<c:if test="${supply.sp_cost=='Y'}"> checked="checked"</c:if>  
							/>
							<fmt:message key="no1" /><input type="radio" id="sp_cost" name="sp_cost" class="text" 
								<c:if test="${supply.sp_cost=='N'}"> checked="checked"</c:if>  
							value="N" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="tax_rate" />：</div>
						<div class="form-input">
							<select id="tax" name="taxId"  style="width: 129px;" class="select">
								<c:forEach items="${taxList }" var="tax">
									<c:if test="${supply.tax==tax.tax }">
										<option value="${tax.id }" selected="selected">${tax.taxdes }</option>
									</c:if>
									<c:if test="${supply.tax!=tax.tax }">
										<option value="${tax.id }">${tax.taxdes }</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="whether_to_return_or_Chongxiao"/>：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ynth" name="ynth" class="text" value="Y"
								<c:if test="${supply.ynth=='Y'}"> checked="checked"</c:if>  
							/>
							<fmt:message key="no1" /><input type="radio" id="ynth" name="ynth" class="text" value="N"
								<c:if test="${supply.ynth=='N'}"> checked="checked"</c:if>  
							/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="explain" />：</div>
						<div class="form-input"><input type="text" id="stomemo" name="stomemo" class="text"  value="${supply.stomemo}"/></div>
						<div class="form-label"><fmt:message key="if_the_batch_management"/>：</div>
						<div class="form-input">
							<fmt:message key="be" /><input type="radio" id="ynbatch" name="ynbatch" class="text" value="Y" <c:if test="${supply.ynbatch == 'Y' }">checked="checked"</c:if>/>
							<fmt:message key="no1" /><input type="radio" id="ynbatch" name="ynbatch" class="text" value="N" <c:if test="${supply.ynbatch == 'N' }">checked="checked"</c:if>/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="quality_requirements" />：</div>
						<div class="form-input"><input type="text" id="quamemo" name="quamemo" class="text"  value="${supply.quamemo}"/></div>
<!-- 						<div class="form-label">条形码：</div> -->
<%-- 						<div class="form-input"><input type="text" id="barCode" name="barCode" class="text"  value="${supply.barCode}"/></div> --%>
						<div class="form-label"><fmt:message key="supplyattr"/>：</div>
						<div class="form-input">
							<select id="attribute" name="attribute" style="width: 129px;" class="select">
								<c:forEach items="${attributeList}" var="attribute">
									<c:if test="${supply.attribute==attribute.code}">
										<option value="${attribute.code}" selected="selected">${attribute.des}</option>
									</c:if>
									<c:if test="${supply.attribute!=attribute.code}">
										<option value="${attribute.code}">${attribute.des}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label">ABC：</div>
						<div class="form-input">
							<select id="abc" name="abc" style="width: 129px;" class="select">
								<option value=""></option>
								<option value="A"
									<c:if test="${supply.abc=='A'}"> selected="selected"</c:if> 
								>A</option>
								<option value="B"
									<c:if test="${supply.abc=='B'}"> selected="selected"</c:if>
								>B</option>
								<option value="C"
									<c:if test="${supply.abc=='C'}"> selected="selected"</c:if>
								>C</option>
							</select>
						</div>
						<div class="form-label"><fmt:message key="standard_products"/>：</div>
						<div class="form-input">
<!-- 							<input type="hidden" id="vname" name="vname" ></input> -->
<!-- 							<select class="select" id="vcode" name="vcode" style="width:133px" onchange="findItemByCode(this)"> -->
<!-- 								<option id="" value=""></option> -->
<%-- 								<c:forEach var="pubitem" items="${pubitemList}" varStatus="status" > --%>
<!-- 									<option  -->
<%-- 										<c:if test="${supply.vcode==pubitem.itcode}">  --%>
<!-- 									  	 	selected="selected"; -->
<%-- 										</c:if>   --%>
<%-- 									id="${pubitem.itcode}" value="${pubitem.itcode}">${pubitem.itdes}</option> --%>
<%-- 								</c:forEach> --%>
<!-- 							</select> -->
							<input type="hidden"  id="vcode" name="vcode" value="${supply.vcode }"/>
							<input type="text" id="vname" name="vname" readonly="readonly" value="${supply.vname}"  class="text"/>
							<img id="seachPubitem" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="the_ratio_of_material_inspection"/>：</div>
						<div class="form-input"><input type="text" id="yhrate" name="yhrate" class="text" value="${supply.yhrate }"/></div>
						<div class="form-label"><fmt:message key="material_delivery_ratio"/>：</div>
						<div class="form-input"><input type="text" id="chrate" name="chrate" class="text" value="${supply.chrate }"/></div>
						
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="material_prices_than"/>：</div>
						<div class="form-input"><input type="text" id="upper" name="upper" class="text" value="${supply.upper }"/></div>
						<div class="form-label"><fmt:message key="material_price_ratio"/>：</div>
						<div class="form-input"><input type="text" id="lower" name="lower" class="text" value="${supply.lower }"/></div>
					</div>
				</div>
			
				<!-- <div title='预估/报货' style="padding:10px;margin-left:0px;margin-top:20px;">
					<div class="form-line">
						<div class="form-label">报货类别：</div>
						<div class="form-input">
							<select id="typ_eas" name="typ_eas" style="width:134px;">
								<option value=""></option>
								<c:forEach items="${typEasList }" var="typEas">
									<c:if test="${supply.typ_eas==typEas.code}">
										<option value="${typEas.code }" selected="selected">${typEas.des }</option>
									</c:if>
									<c:if test="${supply.typ_eas!=typEas.code}">
										<option value="${typEas.code }">${typEas.des }</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
<!-- 					<div class="form-line"> -->
<!-- 						<div class="form-label">报货方式：</div> -->
<!-- 						<div class="form-input"> -->
<!-- 							<select name="stotyp" id="stotyp" class="select" style="width:134px;"> -->
<%-- 								<option value="0" <c:if test="${supply.stotyp=='0'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>></option> --%>
<%-- 								<option value="1" <c:if test="${supply.stotyp=='1'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>  >千元用量</option> --%>
<%-- 								<option value="2" <c:if test="${supply.stotyp=='2'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>  >千次</option> --%>
<%-- 								<option value="3" <c:if test="${supply.stotyp=='3'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>  >菜品点击率</option> --%>
<%-- 								<option value="4" <c:if test="${supply.stotyp=='4'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>  >安全库存</option> --%>
<%-- 								<option value="5" <c:if test="${supply.stotyp=='5'}">  --%>
<!-- 										  	 	selected="selected"; -->
<%-- 											</c:if>  >历史耗用</option> --%>
<!-- 							</select> -->
<!-- 						</div> -->
<!-- 					</div> -->
					 <!-- <div class="form-line">
						<div class="form-label">系数A</div>
						<div class="form-input"><input type="text" id="ratioA" name="ratioA" class="text" value='${supply.ratioA }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数B</div>
						<div class="form-input"><input type="text" id="ratioB" name="ratioB" class="text" value='${supply.ratioB }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数C</div>
						<div class="form-input"><input type="text" id="ratioC" name="ratioC" class="text" value='${supply.ratioC }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数D</div>
						<div class="form-input"><input type="text" id="ratioD" name="ratioD" class="text" value='${supply.ratioD }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数E</div>
						<div class="form-input"><input type="text" id="ratioE" name="ratioE" class="text" value='${supply.ratioE }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数F</div>
						<div class="form-input"><input type="text" id="ratioF" name="ratioF" class="text" value='${supply.ratioF }'/></div>
					</div>
					<div class="form-line">
						<div class="form-label">系数G</div>
						<div class="form-input"><input type="text" id="ratioG" name="ratioG" class="text" value='${supply.ratioG }'/></div>
					</div>
				</div>
			<!-- 虚拟物料代码，不熟悉者务改动 -->
 				<c:if test="${is_supply_x=='Y'}">
 					<div title='<fmt:message key="Virtual_encoding" />' style="padding:10px;margin-left:0px;margin-top:20px;">
						<input type="hidden" id="is_supply_x" name="is_supply_x" class="text" value="Y"/>
						<div class="form-line">
							<div class="form-label"><span class="red">*</span><fmt:message key="virtual"/><fmt:message key="supplies_code" />：</div>
							<div class="form-input"><input type="text" id="sp_code_x" name="sp_code_x" class="text" value="${supply.sp_code_x}" readonly="readonly"/>
							<img id="seachSupply_x" class="seachSupply_x" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' style='cursor: hand;'/>
							</div>
							<div class="form-label"><input type="button" id="resetSupply_x" value="<fmt:message key ="Empty_weight" />"/></div>
						</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="virtual"/><fmt:message key="virtual"/><fmt:message key="supplies_name"/>：</div>
							<div class="form-input"><input type="text" id="sp_name_x" name="sp_name_x" class="text" value="${supply.sp_name_x}" onblur="getSpInit_x(this);" readonly="readonly"/></div>
						</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="virtual"/><fmt:message key="supplies_abbreviations"/>：</div>
							<div class="form-input"><input type="text" id="sp_init_x" name="sp_init_x" class="text" value="${supply.sp_init_x}" readonly="readonly"/></div>
						</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="virtual"/><fmt:message key="unit"/>：</div>
						<div class="form-input">
							<input type="hidden" name="unit_x" value="${supply.unit_x }"/>
							<select class="select" id="unit_x" style="width:133px" disabled="disabled">
								<c:forEach var="unit" items="${unitList}" varStatus="status">
									<option
										<c:if test="${supply.unit_x==unit.des}"> 
									  	 	selected="selected"
										</c:if>  
									id="${unit.code}" value="${unit.des}">${unit.des}</option>
									<option id="${unit1.code}" value="${unit1.code}">${unit1.des}</option>
								</c:forEach>
							</select>
						</div>
					</div>
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="virtual"/><fmt:message key="the_conversion_rate"/>：</div>
							<div class="form-input"><input type="text" id="unitRate_x" name="unitRate_x" class="text"  onkeypress="checkNum(this);" value="${supply.unitRate_x}"/></div>
						</div>
					</div>
 				</c:if>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			var validate;
// 			function findItemByCode(obj){
// 				$('#vname').val($('#'+$(obj).val()).text());
// 			}
// 			function findDelByCode(obj){
// 				$('#deliverdes').val($('#'+$(obj).val()).text());
// 			}
			$(document).ready(function(){
				$("input[type=text]").each(function(){
					if($(this).css("width")=="131px" || $(this).css("width")=="0px"){
						$(this).css("width","127px");
					}
				});
				//清空虚拟物料充填按钮  wjf
				$('#resetSupply_x').click(function(){
					 $('#sp_code_x').val('').attr('readonly',false);
					 $('#sp_name_x').val('').attr('readonly',false);
					 $('#unit_x').attr('disabled',false);
					 $('input[name=unit_x]').val($('#unit_x').val());
					 $('#sp_init_x').val('').attr('readonly',false);
				});
				$('#unitManagement').bind("click",function search(){
					var sp_code=$('#sp_code').val();
					if (sp_code==''||sp_code==null){
						alert('<fmt:message key ="Need_to_fill_in_the_material_encoding" />！');
					}else{
						var curwindow = $('body').window({
							id: 'window_unit',
							title: '<fmt:message key ="Multi_unit_maintenance" />',
							content: '<iframe id="searchUnitFrame" frameborder="0" src="<%=path%>/supply/tableUnit.do?sp_code='+sp_code+'"></iframe>',
							width: '800px',
							height: '430px',
							draggable: true,
							isModal: true
						});
						curwindow.max();
					}
				});
				//unit_x  change事件  wjf
				$('#unit_x').change(function(){
					$('input[name=unit_x]').val($(this).val());
				});
				$('#seachSupply_x').bind('click.custom',function(e){
					if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectAllSupply_x.do'),$('#sp_code_x'),null,null,null,null,null,handler);
					}
				});
				//弹出物资树回调函数
				function handler(sp_code){
					$('#sp_code_x').val(sp_code); 
					if(sp_code==undefined || ''==sp_code){
						 $('#sp_code_x').val('');
						 $('#sp_name_x').val('');
						 $('#unit_x').val('');
						 $('input[name=unit_x]').val('');
						 $('#sp_init_x').val('');
						return;
					}
					$('.validateMsg').remove(); 
					$.ajax({
						type: "POST",
						url: "<%=path%>/supply/findById_x.do",
						data: "sp_code_x="+sp_code,
						dataType: "json",
						success:function(supply){
							$('#sp_code_x').val(supply.sp_code_x).attr('readonly',true);
							 $('#sp_name_x').val(supply.sp_name_x).attr('readonly',true);
							 $('#unit_x').val(supply.unit_x).attr('disabled',true);
							 $('input[name=unit_x]').val(supply.unit_x);
							 $('#sp_init_x').val(supply.sp_init_x).attr('readonly',true);
						}
					});
				}
				
				//回车输入
				   $('input:text:eq(2)').focus();
			        var $inp = $('input:text');
			        $inp.bind('keydown', function (e) {
			            var key = e.which;
			            if (key == 13) {
			                e.preventDefault();
			                var nxtIdx = $inp.index(this) + 1;
			                $(":input:text:eq(" + nxtIdx + ")").focus();
			            }
			        });

				/*弹出树*/
				$('#typdes').bind('focus.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('typdes');
						top.cust('<fmt:message key="please_select_category" />','<%=path%>/supply/selectGrptyp.do',offset,$(this),$('#sp_type'),null,null,null,setSpcode);
					}
				});
				/*选择供应商*/
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('sp_position');
						top.cust('<fmt:message key="please_select_suppliers"/>','<%=path%>/deliver/selectOneDeliver.do',offset,$('#deliverdes'),$('#deliver'),'900','500','isNull');
					}
				});
				
				/*控制半成品的出品仓位*/
				$(".whether_semi").find(":radio").each(function(){
					$(this).click(function(){
						//alert($(this).val());
						var count = 0;
						var ex = $("input[name=ex]:checked").val();
						var ex1 = $("input[name=ex1]:checked").val();
						//alert(count);
						if(ex == 'Y'){
							count++;
						}
						if(ex1 == 'Y'){
							count++;
						}
						if($(this).val()=='Y'){
							$("#extim").removeAttr("disabled");
							$("#positnex").removeAttr("disabled");
						}
						if($(this).val()=='N' && count==0){
							$("#extim").attr("disabled","disabled");
							$("#positnex").attr("disabled","disabled");
						}
					});
				});
				//手动输入单位
				$(".easyui-tabs").find(".writeunit").each(function(){
					$(this).keyup(function(){
						var w_unit = $(this).val();
						$(this).parent().find("option").each(function(){
							if($(this).val()==w_unit){
								$(this).attr("selected","selected");
							}
						});
					});
				});
				
				/*虚拟物资编码*/
				$('#sp_code').bind('blur', function() {
// 					 $('#sp_code_x').val($('#sp_code').val().substr(1, $('#sp_code').val().length));
					 if($('#sp_code_x').val() == ''){
						 $('#sp_code_x').val($('#sp_code').val());
					 }
				});
				$('#sp_name').bind('blur', function() {
					if($('#sp_name_x').val() == ''){
						 $('#sp_name_x').val($('#sp_name').val());
						 $('#sp_init_x').val($('#sp_init').val());
						 $('#unit_x').val($('#unit').val());
						 $('input[name=unit_x]').val($('#unit_x').val());
					}
				});
				
				/*添加行 wangjie 2014年12月27日 13:23:51*/
				$('#addrow').click(function (){
					var selectunit = [];
					$(".tblGrid").find("tr").map(function(){
						selectunit.push($(this).find("td:eq(0)").find("select").val());
					});
					var sequence = $(".tblGrid").find("tr").index()+2;
					var trtd = '<tr style="height: 25px;" class="addrow">'+
							'<td style="width:60px;" align="center">'+
							'<input style="width: 30px;outline: none;" class="writeunit" type="hidden"/>'+
							'<select class="select" id="pk_unit1" name="pk_unit1" style="width:50px;margin-top:-1px;" onchange="change(this)">';
// 							'<option value=""></option>';
							
							<c:forEach var="unit" items="${unitList}" varStatus="status" >
									var unitdes = '${unit.des}';
									if($.inArray(unitdes, selectunit)==-1){
										trtd+='<option id="${unit.code}" value="${unit.des}">${unit.des}</option>';
									}
							</c:forEach>
							trtd+='</select>'+
										'</td>'+
										'<td style="width:58px;" align="center">'+
											'<input type="text" id="nrate" name="nrate" style="width:50px;text-align: right;" value="1.0"/>&nbsp;'+
										'</td>'+
										'<td style="width:65px;text-align: center;"><input name="cgbox" type="checkbox" disabled="disabled"/></td>'+
										'<td style="width:65px;text-align: center;"><input name="cgbox" type="checkbox"/></td>'+
										'<td style="width:65px;text-align: center;"><input name="kcbox" type="checkbox"/></td>'+
										'<td style="width:65px;text-align: center;"><input name="cbbox" type="checkbox"/></td>'+
										'<td style="width:65px;text-align: center;"><input name="ckbox" type="checkbox"/></td>'+
										'<td style="width:75px;text-align: center;"><select><option value="0">&nbsp;</option>';
							trtd+='<option value="1"';
								if(sequence==1)
									trtd+='selected="selected"';
							trtd+='><fmt:message key="specifications1"/><fmt:message key="scm_one"/></option>';
							trtd+='<option value="2"';
								if(sequence==2)
									trtd+='selected="selected"';
							trtd+='><fmt:message key="specifications1"/><fmt:message key="scm_two"/></option>';
							trtd+='<option value="3"';
							if(sequence==3)
								trtd+='selected="selected"';
							trtd+='><fmt:message key="specifications1"/><fmt:message key="scm_three"/></option>';
							trtd+='<option value="4"';
							if(sequence==4)
									trtd+='selected="selected"';
							trtd+='><fmt:message key="specifications1"/><fmt:message key="scm_four"/></option>';
							trtd+='<td style="width:65px;text-align: center;"><input type="checkbox" class="ynDisunit" checked="checked"/></td>';
							trtd+='<td style="width:75px;text-align: center;"><input type="text" style="width:50px;text-align: right;" value="0.0"/></td>';
							trtd+='<td style="width:65px;text-align: center;"><span style="cursor:pointer;color:red;">X</span></td>';
							trtd+='</tr>';
					if($(".tblGrid").find("tr").index()>3){
						alert('<fmt:message key="Can_only_set_up_5_units" />！');
					}else{
						$('.tblGrid').append(trtd).find("tr").find("td:eq(10)").map(function(){
							if($(this).html()!='&nbsp;'){
								$(this).live("click",function(){//删除行
// 									alert($(this).parent().html());
									//var removetd = 0;
// 									$(this).parent().find("td:eq(0)").html("&nbsp;");
// 									$(this).parent().find("td:eq(8)").html("&nbsp;");
									$(this).parent().find(":checkbox").each(function(){
										if($(this).attr("checked")){
											var removetd = $(this).parent().index();
											$(".tblGrid").find("tr:eq(0)").find("td:eq("+removetd+")").find("input[type=checkbox]").attr("checked",true);
										}
									});
									$(this).parent().remove();
								});
							}
							
							//控制每列 只能选择一个checkbox
							$(this).parent().find("td").click(function(){
								var rownum = $(this).index();//行号
								if(rownum>2 && rownum<7){
									$('.tblGrid').find("tr").map(function(){
										$(this).find("td:eq("+rownum+")").find("input[type=checkbox]").removeAttr("checked");
									});
									$(this).find("input[type=checkbox]").attr("checked",true);
								}
							});
							
							//输入事件
							$(this).parent().find(".writeunit").keyup(function(){
								var w_unit = $(this).val();
								$(this).parent().find("option").each(function(){
									if($(this).val()==w_unit){
										$(this).attr("selected","selected");
									}
								});
							});
						});
					}
				});
				/*控制每列只能选1个*/
				$(".tblGrid").find("tr").each(function(){
					$(this).parent().find("td").click(function(){
						var rownum = $(this).index();//行号
						if(rownum>2 && rownum<7){
							$('.tblGrid').find("tr").map(function(){
								$(this).find("td:eq("+rownum+")").find("input[type=checkbox]").removeAttr("checked");
							});
							$(this).find("input[type=checkbox]").attr("checked",true);
						}
					});
				});
				
				/*删除行  wangjie 2014年12月27日 17:52:43*/
				$(".tblGrid").find("tr").map(function(){
					$(this).find("td:eq(10)").on("click",function(){
						if($(this).parent().index()!=0){
							if($(this).parent().parent().find("tr").length==1){
								alert('<fmt:message key="Please_keep_one_line_at_least" />！');
							}else{
// 								$(this).parent().find("td:eq(0)").html("&nbsp;");
// 								$(this).parent().find("td:eq(8)").html("&nbsp;");
								$(this).parent().find(":checkbox").each(function(){
									if($(this).attr("checked")){
										var removetd = $(this).parent().index();
										$(".tblGrid").find("tr:eq(0)").find("td:eq("+removetd+")").find("input[type=checkbox]").attr("checked",true);
									}
								});
								$(this).parent().remove();
							}
						}
					});
				});
				
				/*验证*/
				validate = new Validate({
					validateItem:[<c:if test="${is_supply_x=='Y'}">{
						type:'text',
						validateObj:'sp_code_x',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'sp_code_x',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！！']
					},{
						type:'text',
						validateObj:'sp_name_x',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！！']
					},{
						type:'text',
						validateObj:'unit_x',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！！']
					},{
						type:'text',
						validateObj:'unitRate_x',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！！']
					},</c:if>{
						type:'text',
						validateObj:'typdes',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="supplies_category" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'sp_code',
						validateType:['canNull','maxLength'],
						param:['F','50'],
						error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_name',
						validateType:['canNull','maxLength','handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/supply/findById.do",{sp_name:$("#sp_name").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F','25','F'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！','<fmt:message key="supplies_name" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'sp_init',
						validateType:['canNull','maxLength'],
						param:['F','30'],
						error:['<fmt:message key="supplies_abbreviations" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_abbreviations" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_desc',
						validateType:['maxLength'],
						param:['30'],
						error:['<fmt:message key="supplies_specifications" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'positn1',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="shelf" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_code1',
						validateType:['maxLength'],
						param:['50'],
						error:['<fmt:message key="barcode" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'datsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="procurement_cycle" /><fmt:message key="format_for_digital" />','<fmt:message key="procurement_cycle" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntminsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="delivery_time" /><fmt:message key="format_for_digital" />','<fmt:message key="delivery_time" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntuse',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="average_daily_consumption" /><fmt:message key="format_for_digital" />','<fmt:message key="average_daily_consumption" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'stomax',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="stores_month_procurement_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="stores_month_procurement_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'stomax1',
/* 						validateType:['num','maxLength'],
						param:['','10'],
						error:['分店申购单次限量格式为数字','分店申购单次限量长度太长！'] */
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="stores_to_purchase_single_limited" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'cntminsto',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="delivery_time" /><fmt:message key="format_for_digital" />','<fmt:message key="delivery_time" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'accprate',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="format_for_digital" />','<fmt:message key="branche_acceptance_ratio" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'accpratemin',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="format_for_digital" />','<fmt:message key="branche_acceptance_ratio" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'mincnt',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="format_for_digital" />','<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_per1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="turnover_period" /><fmt:message key="format_for_digital" />','<fmt:message key="turnover_period" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'mincnt',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="format_for_digital" />','<fmt:message key="minimum_amount_of_purchase" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_per2',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="number_of_days_alarm_in_advance" /><fmt:message key="format_for_digital" />','<fmt:message key="number_of_days_alarm_in_advance" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_max1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="actual_stock_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_limit" /><fmt:message key="length_too_long" />！']
					},
// 					{
// 						type:'text',
// 						validateObj:'sp_max2',
// 						validateType:['num','maxLength'],
// 						param:['F','10'],
// 						error:['<fmt:message key="reference_inventory_capping" /><fmt:message key="format_for_digital" />','<fmt:message key="reference_inventory_capping" /><fmt:message key="length_too_long" />！']
// 					},
					{
						type:'text',
						validateObj:'sp_min1',
						validateType:['num','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="actual_stock_lower_limit" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_lower_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_price',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['<fmt:message key ="cannot_be_empty" />','<fmt:message key="materials_unit_price" /><fmt:message key="format_for_digital" />','<fmt:message key="materials_unit_price" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'pricesale',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="materials_price" /><fmt:message key="format_for_digital" />','<fmt:message key="actual_stock_lower_limit" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'tax',
						validateType:['canNull','num','maxLength'],
						param:['T','F','10'],
						error:['','<fmt:message key="tax_rate" /><fmt:message key="format_for_digital" />','<fmt:message key="tax_rate" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'sp_code',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/supply/findById.do",{sp_code:$("#sp_code").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="supplies_code" /><fmt:message key="already_exists" />！']
					}]
				});
				$("#positndes").val($("#sp_position option:selected").text());
				$("#sp_position").bind('change',function(){
					$("#positndes").val($("#sp_position option:selected").text());
				});
// 				$("#deliverdes").val($("#deliver option:selected").text());
// 				$("#deliver").bind('change',function(){
// 					$("#deliverdes").val($("#deliver option:selected").text());
// 				});
				
				/**2014.6.25 wjf**/
				$("#ydaypan").bind('click',function(){
					$("input[name='ynweekpan']").get(0).checked = true;
				});
				/**2014.6.25 wjf 标准产品**/
				$('#seachPubitem').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var offset = getOffset('sp_type');
						top.cust('<fmt:message key="Please_select_the_standard_unit" />',encodeURI('<%=path%>/supply/searchPubitemList.do?vvcode=${supply.vcode }'),offset,$('#vname'),$('#vcode'),'750','500','isNull');
					}
				});
				/**配送单位 选择配送单位 才输入最小申购量**/
				$(".ynDisunit").click(function(){
					//如果已经引用的配送单位不能修改
					var yndelete = $(this).parents('tr').data('yndelete');
					var ynDisunit = $(this).parents('tr').data('yndisunit');
					if(ynDisunit == 'Y' && yndelete > 0){
						alert('仓位物资属性已经引用的配送单位不能删除或修改！');
						$(this).attr('checked','checked');
						return;
					}
					if($(this).attr('checked')){
						$(this).parents('td').next().find('input').attr('disabled',false);
					}else{
						$(this).parents('td').next().find('input').attr('disabled',true);
					}
				});
			});
			//基地加工物资必须选择加工间
			function checkPositnex(){
				var flag = false;
				var positnex = $('#positnex').val();
				var ex1 = $('input[name=ex1]:checked').val();
				if(ex1 == "Y" && positnex == ""){
					alert('<fmt:message key="The_current_material_for_the_base_of_processing_please_choose_between_processing" />！');
					flag = false;
				} else {
					flag = true;
				}
				return flag;
			}
			//验证验货比率
			function checkAccprate(){
				var flag = true;
				var accprate = Number($('#accprate').val());
				var accpratemin = Number($('#accpratemin').val());
				if(accprate < accpratemin){
					alert('<fmt:message key="Inspection_ratio_can_not_be_less_than_the_lower_limit" />！');
					flag = false;
				}
				return flag;
			}
			//物资的标准单位必须跟对应虚拟物料的虚拟单位相同
			function checkUnit(){
// 				var flag = false;
// 				var unit = $('#unit').val();
// 				var unit_x = $('#unit_x').val();
// 				if(unit != unit_x){
// 					alert('虚拟物料的单位与对应实际物料的标准单位不相同，请修改！');
// 					flag = false;
// 				} else{
// 					flag = true;
// 				}
// 				return flag;
				return  true;
			}
			//检查虚拟物资编码和名称是否重复
			function checkSupply_x(){
				var sp_code_x = $('#sp_code_x').val();
				var sp_name_x = $('#sp_name_x').val();
				var flag = false;
				$.ajax({
					type: "POST",
					url: "<%=path%>/supply/checkSupply_x.do",
					data: "sp_code_x="+sp_code_x+"&sp_name_x="+sp_name_x,
					async:false,
					dataType: "json",
					success:function(result){
						if(result > 0){
							alert('<fmt:message key="Virtual_material_encoding_or_name_can_not_be_repeated_please_do_the_appropriate_changes" />！');
							flag = false;
						} else {
							flag = true;
						}
					}
				});
				return flag;
			}
			
			//检查一种虚拟物资对应多种实际物资时，成本单位是否一样
			function checkUnit2(){
				var sp_code_x = $('#sp_code_x').val();
				var sp_code = $('#sp_code').val();
				var unit = $('#unit2').val();
				var flag = false;
				$.ajax({
					type: "POST",
					url: "<%=path%>/supply/checkUnit.do",
					data: "sp_code_x="+sp_code_x+"&sp_code="+sp_code,
					async:false,
					dataType: "json",
					success:function(result){
						if(result != null && result != unit){
							alert('<fmt:message key="At_present,_the_cost_per_unit_of_virtual_material_corresponding_to_actual_material_is_not_the_same_as_the_cost_per_unit" />！');
							flag = false;
						} else {
							flag = true;
						}
					}
				});
				return flag;
			}
			
			function setSpcode(codeid){
				if(codeid){
					  $.ajax({
						  type:"post",
						  async:false,
						  url:"<%=path%>/supply/getMaxSpcode.do",
						  dataType:"text",
						  data:{code:codeid},
						  success:function(data){
							  $('#sp_code').val(data);
						  },
						  error:function(htmlObject,status,index){
							  alert("<fmt:message key ="wrong" />。。。"+index);
						  }
					  });
				}
			}
			function getValue(a) {
				$('#positnexdes').val($(a).find('option:selected').text());
			}
			function www_zzjs_net(obj){
				switch(event.keyCode){
					case 13:
						obj.options[obj.length] = new Option("","",false,true);
						event.returnValue = false;
						break;
					case 8:
						obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text.substr(0,obj.options[obj.selectedIndex].text.length-1);
						event.returnValue = false;
					break;
				}
			}
			function zzjs_net(obj){
			  obj.options[obj.selectedIndex].text = obj.options[obj.selectedIndex].text + String.fromCharCode(event.keyCode);
			  event.returnValue = false;
			}
			function changeSelect(e){
				if ($(e).attr('id') =='unit'){
					$('#unitper0').val(parseInt($(e).find('option:selected').attr('unitper')));
				}else{
					var unitper0=$('#unitper0').val();
					if ($(e).attr('id') =='unit1'){
						$('#unitper').val(parseInt($(e).find('option:selected').attr('unitper'))/parseInt(unitper0));
					}else if ($(e).attr('id') =='unit2'){
						$('#unitper2').val(parseInt($(e).find('option:selected').attr('unitper'))/parseInt(unitper0));
					}else if ($(e).attr('id') =='unit3'){
						$('#unitper3').val(parseInt($(e).find('option:selected').attr('unitper'))/parseInt(unitper0));
					}else{
						$('#unitper4').val(parseInt($(e).find('option:selected').attr('unitper'))/parseInt(unitper0));
					}
				}
			}
			
			//给各个单位赋值
			function fuzhi(){
					var count = 0;
					var orderno = "";
					var sequences = [];
					//存放配送单位
					var disunit = "";
					$(".tblGrid").find("tr").map(function(i){//先遍历所有行
						var sequence = $(this).find("td:eq(7)").find("select").val();//规格
						if(sequence != '0'){
							sequences.push(sequence);
						}
						//设置配送单位  单位不是空 并且勾选了 配送单位
						var unit = $(this).find("td:eq(0)").find("select").val();
						var disunitCheck = $(this).find("td:eq(8)").find("input[type=checkbox]").attr("checked");
						var unitper = isNaN($(this).find("td:eq(1)").find('input').val())?1:Number($(this).find("td:eq(1)").find('input').val());//配送单位转换率
						var dismincnt = isNaN($(this).find("td:eq(9)").find('input').val())?0:Number($(this).find("td:eq(9)").find('input').val());//配送单位最小申购量
						if( unit != '' && disunitCheck){
							disunit += unit + "," + unitper + "," + sequence + "," + i + "," + dismincnt + "#";
						}
						$(this).find("td").map(function(){
							if($(this).find("input[type=checkbox]").attr("checked")){
								var tdno = $(this).index();
								var per = $(this).parent().find("td:eq(1)").find("input").val();//转换率
								//转换率必须为数字
								if(isNaN(per)){
									count++;
								}
								if(tdno==2){//标准单位
									$("#unit").val($(this).parent().find(".select").val());
									$("#unitper0").val(per);
								}else if(tdno==3){//采购单位
									$("#unit3").val($(this).parent().find(".select").val());
									$("#unitper3").val(per);
									disunit += unit + "," + unitper + "," + sequence + "," + i + "," + dismincnt + "#";//采购单位不选也一定是配送单位
								}else if(tdno == 4){//库存单位
									$("#unit4").val($(this).parent().find(".select").val());
									$("#unitper4").val(per);
								}else if(tdno == 5){//成本单位
									$("#unit2").val($(this).parent().find(".select").val());
									$("#unitper2").val(per);
								}else if(tdno == 6){//参考单位
									$("#unit1").val($(this).parent().find(".select").val());
									$("#unitper").val(per);
							    }
							}
						});
						//存放单位的排序信息
						if($(this).find(":checked").length!=0){
							orderno+=$(this).find("td:eq(0)").find(".select").val()+","+$(this).find("td:eq(1)").find("input").val()+","+sequence+ "," + i +"#";
						}
					});
					$("#orderno").val(orderno);
					$("#disunit").val(disunit);
					if(count!=0){
						alert('<fmt:message key ="must_be_numeric" />！');
						return 1;
					}
					if($(".tblGrid").find("tr:eq(0)").find("td:eq(0)").find(".select").val()==''){
						alert('<fmt:message key ="Choose_standard_unit" />！');
						return 1;
					}
// 					return 0;
					//判断规格是否重复
					if(ifRepeat(sequences)){
						alert('<fmt:message key ="Unit_size_can_not_be_repeated_please_choos" />！');
						return 1;
					}
					return 0;
			}
			
			//将下拉列表 选择的值 传到前面的文本框中
			function setunit(select){
				$(".tblGrid").find("tr:eq(0)").find("td:eq(0)").find("select").find("option").map(function(){
					if($(this).val()==$(select).val()){
						$(this).attr("selected","selected");
					}
				});
			} 
			//控制每种单位只能选择一次
			function change(select){
				//如果已经引用的配送单位不能修改
				var yndelete = $(select).parents('tr').data('yndelete');
				var ynDisunit = $(select).parents('tr').data('yndisunit');
				if(ynDisunit == 'Y' && yndelete > 0){
					alert('仓位物资属性已经引用的配送单位不能删除或修改！');
					var unit = $(select).parents('tr').data('unit');
					$(select).find('option[value="'+unit+'"]').attr('selected',true);
					return;
				}
				var count = $(select).parent().parent().index();
				if(count==0){
					$(".unit").html($(select).val());
				}
				var selected = [];
				var _trno = $(select).parent().parent().index();
				$(".tblGrid").find("tr").each(function(){
					if($(this).find("td:eq(0)").find("select").val()!='' && $(this).index() != _trno){
						selected.push($(this).find("td:eq(0)").find("select").val());
					}
				});
				if($.inArray($(select).val(),selected)!=-1){
					alert("'"+$(select).val()+"'<fmt:message key="Already_exists_please_choose_another_unit" />！");
					$(select).find("option:eq(0)").attr("selected","selected");
				}
			}
			
			//控制每个规格只能出现一次          （空可以出现多次）
			function ifRepeat(a){
			   return /(\x0f[^\x0f]+)\x0f[\s\S]*\1/.test("\x0f"+a.join("\x0f\x0f") +"\x0f");
			}
			
			//判断是否有税率
			function judgeTax(){
				var len = $('#tax option').length;
				if(len==0){
					alert('<fmt:message key="Please_first" /><fmt:message key="set_up_the" /><fmt:message key="tax_rate" />！！！');
					//样式改变
					$('ul.tabs').children('li').removeClass('tabs-selected');
					$('ul.tabs').children('li:eq(4)').click();
					return false;
				}else{
					return true;
				}
			}
		</script>
	</body>
</html>