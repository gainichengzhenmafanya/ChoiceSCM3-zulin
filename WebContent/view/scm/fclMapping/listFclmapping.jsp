<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>财务科目映射设置</title>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/FclMapping/list.do" method="post">
			<div class="form-line">
				<div class="form-label">类型:</div>
				<div class="form-input">
					<select class="select" id="type" name="type" onchange="pageReload()">
						<option value='1' <c:if test="${FclMapping.type=='1' }">selected="selected"</c:if>>要货单位</option>
						<option value='2' <c:if test="${FclMapping.type=='2' }">selected="selected"</c:if>>发货单位</option>
						<option value='3' <c:if test="${FclMapping.type=='3' }">selected="selected"</c:if>>客户</option>
						<option value='4' <c:if test="${FclMapping.type=='4' }">selected="selected"</c:if>>仓库</option>
<%-- 						<option value='5' <c:if test="${FclMapping.type=='5' }">selected="selected"</c:if>>支付类别</option> --%>
<%-- 						<option value='6' <c:if test="${FclMapping.type=='6' }">selected="selected"</c:if>>活动类别</option> --%>
<%-- 						<option value='7' <c:if test="${FclMapping.type=='7' }">selected="selected"</c:if>>固定项科目</option> --%>
					</select>
				</div>
			</div>
			<div class="grid" >
				<input type="hidden" id="pk_idList" value="${pk_idList }"/>
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td align="center" style="width: 30px;">
									<span><input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;">编码</span></td>
								<td><span style="width:250px;">名称</span></td>
								<td><span style="width:100px;">财务科目</span></td>
								<td><span style="width:250px;">科目名称</span></td>
								<td><span style="width:100px;">类型</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="FclMapping" items="${FclMappingList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<span><input type="checkbox" name="idList" id="chk_<c:out value='${FclMapping.pk_fclmapping}' />" value="<c:out value='${FclMapping.pk_fclmapping}' />"/></span>
									</td>
									<td><span style="width:100px;text-align: center;"><c:out value="${FclMapping.svcode}" /></span></td>
									<td><span style="width:250px;text-align: center;"><c:out value="${FclMapping.svname}" /></span></td>
									<td><span style="width:100px;text-align: center;"><c:out value="${FclMapping.vcode}" /></span></td>
									<td><span style="width:250px;text-align: center;"><c:out value="${FclMapping.vname}" /></span></td>
									<td><span style="width: 100px;text-align: center;">
											<c:if test="${FclMapping.type=='1' }">要货单位</c:if>
											<c:if test="${FclMapping.type=='2' }"><fmt:message key ="shouyefahuodanwei" /></c:if>
											<c:if test="${FclMapping.type=='3' }">客户</c:if>
											<c:if test="${FclMapping.type=='4' }">仓库</c:if>
									</span></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<div class="search-div" style="margin-left: 0px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left">编码：</td>
							<td>
								<input type="text" id="svcode" name="svcode" class="text" value="${FclMapping.svcode }"/>
							</td>
							<td class="c-left">名称：</td>
							<td>
								<input type="text" id="svname" name="svname" class="text"  value="${FclMapping.svname }"/>
							</td>
							<td class="c-left">财务科目：</td>
							<td>
								<input type="text" id="vcode" name="vcode" class="text" value="${FclMapping.vcode }"/>
							</td>
							<td class="c-left">科目名称：</td>
							<td>
								<input type="text" id="vname" name="vname" class="text"  value="${FclMapping.vname }"/>
							</td>
						</tr>
					</table>
				</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='确定'/>
	       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
			</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('.search-div').hide(); 
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
						editFclmapping("update");
					if($(this).hasClass("show-firm-row"))return;
					$('.grid').find('.table-body').find('tr').removeClass("show-firm-row");
					$(this).addClass("show-firm-row");
				 });
				
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearqueryForm();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="new_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								editFclmapping('save');
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_role_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								editFclmapping('update');
							}
						}, {
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete_role_of_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteFclmapping();
							}
						},"-",{
							text: '<fmt:message key ="select" />',
							title: '<fmt:message key="select" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				// 自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
			});
			function pageReload(){
				$('#listForm').submit();
			}
			
			//修改或新增，update：修改，save：新增
			function editFclmapping(edittype){
				var url="";
				if(edittype=="save"){
					url='<%=path%>/FclMapping/editopen.do?edittype='+edittype+'&type='+$('#type').val();
				}else if(edittype=='update'){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						url='<%=path%>/FclMapping/editopen.do?edittype='+edittype+'&pk_fclmapping='+chkValue;
					}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
						alert("每次只能修改一条数据，请重新选择！");
						return;
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
				}
				if(url=="")return;
				
				$('body').window({
					id: 'window_saveFclmapping',
					title: edittype=="save" ? '保存财务科目映射设置' : '修改财务科目映射设置',
					content: '<iframe id="SaveForm" frameborder="0" src="'+url+'"></iframe>',
					width: '700px',
					height: '450px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_role_of_information"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('SaveForm')&&window.document.getElementById("SaveForm").contentWindow.validate._submitValidate()){
										window.frames["SaveForm"].editdata();
									}
								}
							},{
								text: '<fmt:message key ="cancel" />',
								title: '<fmt:message key ="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//删除数据批量或单个
			function deleteFclmapping(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						if(chkValue.length<=0)return;
						var action = '<%=path%>/FclMapping/deletes.do?pk_fclmapping='+chkValue.join(",");
						$('body').window({
							title: '删除数据',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '340px',
							height: '255px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
		</script>
	</body>
</html>