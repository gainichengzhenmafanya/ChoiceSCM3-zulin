<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>财务科目映射设置</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		
		<style type="text/css">
			.form {
				position: relative;
				width: 100%;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="editForm" method="post" action="<%=path %>/FclMapping/edit.do?edittype=${edittype }">
				<input type="hidden" value="${FclMapping.pk_fclmapping }" id="pk_fclmapping" name="pk_fclmapping"/>
				<input type="hidden" value="${edittype }" id="edittype" name="edittype"/>
				<input type="hidden" value="${FclMapping.type }" id="type" name="type"/>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="coding" />：</div>
					<div class="form-input">
<%-- 						<input type="hidden" id="pk_id" name="pk_id" class="text" value="${FclMapping.pk_id }"/> --%>
						<input type="text" id="svcode" name="svcode" class="text" readonly="readonly"  value="${FclMapping.svcode }"/>
						<img id="selectopen" class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;" alt="选择数据" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="name" />：</div>
					<div class="form-input">
						<input type="text" id="svname" name="svname" readonly="readonly" class="text" value="${FclMapping.svname }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="financial_accounts" />：</div>
					<div class="form-input">
						<input type="hidden" id="pk_balatype" name="pk_balatype" class="text"  value="${FclMapping.pk_balatype }"/>
						<input id="vcode" name="vcode" class="text" readonly="readonly"  value="${FclMapping.vcode }"/>
						<img id="selectsettlement" class="search" src="<%=path%>/image/themes/icons/search.png" style="margin-top: 0px;" alt="选择数据" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key ="subjects" /><fmt:message key ="name" />：</div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" readonly="readonly" class="text"  value="${FclMapping.vname }"/>
					</div>
				</div>
<!-- 				<div class="form-line" style="display: none;"> -->
<!-- 					<div class="form-label">类型：</div> -->
<!-- 					<div class="form-input"> -->
<!-- 						<select class="select" disabled="disabled"> -->
<%-- 							<option value='1' <c:if test="${FclMapping.type=='1' }">selected="selected"</c:if>>仓位科目</option> --%>
<%-- 							<option value='2' <c:if test="${FclMapping.type=='2' }">selected="selected"</c:if>>供应商科目</option> --%>
<!-- 						</select> -->
<!-- 				</div> -->
<!-- 				</div> -->
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//数据选中界面
			var heights=380;
			var widths=560;
			
			$("#selectopen").click(function(){
				var flag=$('#type').val();
				switch(flag){
					case "1": //要货单位
						if(!!!top.customWindow){
							var defaultCode = $("#svcode").val();
							var defaultName = $("#svname").val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_to_goods_unit" />',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#svname'),$('#svcode'),'760','520','isNull');
						}
					case "2": //发货单位
						if(!!!top.customWindow){
							var defaultCode = $('#svcode').val();
							var defaultName = $('#svname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_the_delivery_unit" />',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#svname'),$('#svcode'),'760','520','isNull');
						}
					case "3": //客户
						if(!!!top.customWindow){
							var defaultCode = $('#svcode').val();
							var defaultName = $('#svname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_a_customer" />',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#svname'),$('#svcode'),'760','520','isNull');
						}
					case "4": //仓库
						if(!!!top.customWindow){
							var defaultCode = $('#svcode').val();
							var defaultName = $('#svname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_the_warehouse" />',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#svname'),$('#svcode'),'760','520','isNull');
						}
					default: return false;
				}
			});
			
			$("#selectsettlement").click(function(){
				var flag=$('#type').val();
				switch(flag){
					case "1": //要货单位
						if(!!!top.customWindow){
							var defaultCode = $("#vcode").val();
							var defaultName = $("#vname").val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_to_goods_unit" />>',encodeURI('<%=path%>/FclMapping/selectOnePositn.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#vname'),$('#vcode'),'600','520','isNull');
						}
					case "2": //发货单位
						if(!!!top.customWindow){
							var defaultCode = $('#vcode').val();
							var defaultName = $('#vname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_the_delivery_unit" />',encodeURI('<%=path%>/FclMapping/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#vname'),$('#vcode'),'600','520','isNull');
						}
					case "3": //客户
						if(!!!top.customWindow){
							var defaultCode = $('#vcode').val();
							var defaultName = $('#vname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_a_customer" />',encodeURI('<%=path%>/FclMapping/selectCustomer.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#vname'),$('#vcode'),'600','520','isNull');
						}
					case "4": //仓库
						if(!!!top.customWindow){
							var defaultCode = $('#vcode').val();
							var defaultName = $('#vname').val();
							var offset = getOffset('svcode');
							top.cust('<fmt:message key="Please_choose_the_warehouse" />',encodeURI('<%=path%>/FclMapping/selectWareHouse.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#vname'),$('#vcode'),'600','520','isNull');
						}
					default: return false;
				}
			});
			
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'svcode',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key ="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'svname',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key ="cannot_be_empty" />！']
				}]
			});
		});
		function editdata(){
			
			$.ajaxSetup({ 
				  async: false 
		  	});
			var data={};
			data["pk_fclmapping"]=$('#pk_fclmapping').val();
// 			data["pk_id"]=$('#pk_id').val();
// 			data["pk_balatype"]=$('#pk_balatype').val();
			data["svcode"]=$('#svcode').val();
			data["svname"]=$('#svname').val();
			data["vcode"]=$('#vcode').val();
			data["vname"]=$('#vname').val();
			data["type"]=$('#type').val();
			data["edittype"]=$('#edittype').val();
			var result=true;
			$.post("<%=path%>/FclMapping/edit.do",data,function(returndata){
				var rs = eval('('+returndata+')');
				if(rs != "1"){
					showMessage({
								type: 'error',
								msg: '<fmt:message key="save_fail"/>！',
								speed: 1500
								});
				}else{
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 1500,
								handler:function(){
										parent.pageReload();
										$(".close",parent.document).click();
								}
								});
				}
			});
		}
		//数据填充
		function appendData(data){
			if(!$.trim(data.code.join('')))return;
			var entitys=data.entity;
// 			var storeName=[];
// 			for(var i in entitys){
// 				storeName.push(entitys[i].vname);
// 			}
			$('#pk_id').val(data.code);
			$('#svcode').val(entitys[0].vcode);
			$('#svname').val(entitys[0].vname);
		}
		//科目数据填充
		function setbalatype(data){
			if(!$.trim(data.code.join('')))return;
			var entitys=data.entity;

			$('#pk_balatype').val(data.code);
			$('#vcode').val(entitys[0].vcode);
			$('#vname').val(entitys[0].vname);
		}
		</script>
	</body>
</html>