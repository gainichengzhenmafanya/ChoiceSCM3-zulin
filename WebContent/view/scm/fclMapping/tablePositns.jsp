<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="vendor_defined" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.page{margin-bottom: 0px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>		
		<form id="listForm" action="<%=path%>/FclMapping/selectOnePositn.do" method="post">
			<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="defaultCode" name="defaultCode" class="text" readonly="readonly" value="${defaultCode}"/>
			<input type="hidden" id="defaultName" name="defaultName" class="text" readonly="readonly" value="${defaultName}"/>
			<table  cellspacing="0" cellpadding="0">
				<tr >
					<td class="c-left"><fmt:message key="coding" />：</td>
					<td><input type="text" id="code" name="code" class="text" value="${queryPositn.code}" autocomplete="off" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
					<td class="c-left"><fmt:message key="name" />：</td>
					<td><input type="text" id="des" name="des" class="text" value="${queryPositn.des}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
					<td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
			    </tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"></span></td>
								<td><span style="width:100px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:250px;"><fmt:message key="name" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.id}"/></span>
									</td>
									<td><span title="${positn.code}" style="width:100px;text-align: center;">${positn.code}</span></td>
									<td><span title="${positn.des}"  style="width:250px;">${positn.des}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />			
			
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
			}
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
				});
		
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');
	
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				 $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-120px','0px']
								},
								handler: function(){
									select_Positn();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-38px','0px']
								},
								handler: function(){
// 									parent.$('.close').click();
									top.closeCustom();
								}
							}]
					});
				
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				
				
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				//---------------------------
				
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//让之前选中的默认选中
				var defaultCode = parent.$('#defaultCode').val();
				var defaultName = parent.$('#defaultName').val();
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						if(this.id.substr(4,this.id.length)==defaultCode){
							$(this).attr("checked", true);
							$('#parentId').val(defaultCode);
							$('#parentName').val(defaultName);
						}
					});	
				}
			});	
			
			function select_Positn(){
				if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
					$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
						$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
						$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
					});
					top.customWindow.afterCloseHandler('Y');
					top.closeCustom();
				}else{
					$('#parentId').val('');
					$('#parentName').val('');
					alert("<fmt:message key ="please_select_data" />");
				}
			}			
		</script>
	</body>
</html>