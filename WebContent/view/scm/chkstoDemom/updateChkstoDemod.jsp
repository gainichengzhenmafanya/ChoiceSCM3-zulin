<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>修改报货单单据模板</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		.choose{background: #FFEB8A;}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/chkstodemom/updateChkstoDemom.do" method="post">
		<input type="hidden" name="tempTyp" id="tempTyp" value="${chkstoDemom.tempTyp }"/><!-- 模板类型 用来区分报货单模板，出库单模板2015.9.17wjf -->
			<div class="bj_head">
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="title"/>：</div>
					<div class="form-input">
						<input type="hidden" id="chkstodemono" name="chkstodemono" value="${chkstoDemom.chkstodemono }" />
						<input type="text" id="title" name="title" value="${chkstoDemom.title }" class="text" />
					</div>
					<div class="form-label" style="width:100px;"><fmt:message key="please_select_materials"/>:</div>
					<div class="form-input" style="width:220px;">
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>	
				</div>
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="remark"/>：</div>				
					<div class="form-input">
						<textarea id="memo" name="memo" cols="30" rows="1" class="text">${chkstoDemom.memo }</textarea>
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td rowspan="2"><span style="width:55px;"><fmt:message key="procurement_unit"/></span></td>
								<td colspan="2"><fmt:message key="standard_unit"/></td>
								<td rowspan="2"><span style="width:55px;"><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="scm_sale_price"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="chkstoDemod" items="${listChkstoDemod}" varStatus="status">
							<tr data-mincnt="${chkstoDemod.supply.mincnt}" data-unitper="${chkstoDemod.supply.unitper }">
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${chkstoDemod.supply.sp_code }</span></td>
								<td><span style="width:90px;">${chkstoDemod.supply.sp_name }</span></td>
								<td><span style="width:70px;">${chkstoDemod.supply.sp_desc }</span></td>
								<td><span style="width:55px;">${chkstoDemod.supply.unit3 }</span></td>
<%-- 								<td><span style="width:70px;text-align: right;">${chkstoDemod.amount }</span></td> --%>
								<td><span style="width:70px;">${chkstoDemod.supply.unit }</span></td>
								<td><span style="width:70px;text-align: right;" price="${chkstoDemod.supply.sp_price}"><fmt:formatNumber value="${chkstoDemod.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
<%-- 								<td><span style="width:70px;text-align: center;"><fmt:formatNumber value="${chkstoDemod.supply.sp_price*chkstoDemod.amount}" type="currency" pattern="0.00"/></span></td> --%>
<%-- 								<td><span style="width:70px;text-align: right;">${chkstoDemod.reference_amount }</span></td> --%>
								<td><span style="width:55px;">${chkstoDemod.supply.unit1 }</span></td>
<%-- 								<td><span style="width:90px;"><fmt:formatDate value="${chkstoDemod.ind}" pattern="yyyy-MM-dd"/></span></td> --%>
								<td><span style="width:70px;">${chkstoDemod.memo }</span></td>
								<td style="display: none;"><span>${chkstoDemod.id }</span></td>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstoDemod.price }"/></span></td>
                                <td><span style="height:24px;width:40px;"><img style="height:21px;width:15px;" title="上移一行" src="../image/up.png" onclick="up(this)"/>&nbsp;&nbsp;<img style="height:21px;width:15px;" title="下移一行" src="../image/down.png" onclick="down(this)"/></span></td>
							</tr>
							<c:set var="sum_num" value="${status.index}"/>  
<%-- 							<c:set var="sum_amount" value="${sum_amount + chkstoDemod.amount}"/>    --%>
<%-- 							<c:set var="sum_price" value="${sum_price + chkstoDemod.supply.sp_price*chkstoDemod.amount}"/>    --%>
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num"><fmt:formatNumber value="${sum_num}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:100px;"></td>
<%-- 							<td style="width:100px;background:#F1F1F1;">总金额：<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label></td> --%>
							<td style="width:65px;"></td>
<%-- 							<td style="width:80px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td> --%>
							<td style="width:65px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;"></td>
<%-- 							<td><font color="blue"><fmt:message key="operation_tips"/></font></td> --%>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		
		<form action='<%=path%>/supply/selectSupplyLeft.do' method='post' target='selectFrame' id="spform">
			<input type="hidden" id="defaultCode" name="defaultCode"/>	
			<input type="hidden" id="single" name="single" value="false"/>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/autoTableWithSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var validate;
		var today;
		//工具栏
		$(document).ready(function(){
			//2014.11.5 wjf
			editCells();
			loadToolBar();
// 			$("#firmDes").val($("#firm").val());
			/*过滤*/
// 			$('#firmDes').bind('keyup',function(){
// 		          $("#firm").find('option')
// 		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
// 		                    .attr('selected','selected');
// 		    });
			//按钮快捷键
			focus() ;//页面获得焦点
			$('#seachSupply').bind('click.custom',function(e){
				var defaultCode = "";
				$(".table-body").find("tr").each(function(){
					defaultCode += $(this).find("td:eq(1)").find('span').text()+",";
				});
				$('#defaultCode').val(defaultCode);
		 		if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />','',$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
					$("#spform").submit();
				}
			});
			function handler2(sp_code){
				var spcode = sp_code.split(',');
				var defaultCode = $('#defaultCode').val();//已存在数据
				if(spcode.length>0 ){
					for(var i=0; i<spcode.length ;i++){
						if(spcode[i]=='' || defaultCode.indexOf(spcode[i])!=-1){
							continue;
						}
				 		$.ajax({
							type: "POST",
							url: "<%=path%>/supply/findById.do",
							data: "sp_code="+spcode[i],
							dataType: "json",
							success:function(supply){
								if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
									$.fn.autoGrid.addRow();
								}
								
								$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code);
								$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
								$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
								$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supply.unit3);
								$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(supply.unit);
// 								$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(1).css("text-align","right");
								$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(supply.sp_price).css("text-align","right");
// 								$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supply.sp_price).css("text-align","center");
								$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supply.unit1);
// 								$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(supply.unitper).css("text-align","right");
// 								$(".table-body").find("tr:last").find("td:eq(10)").find('span').text(today);
								$(".table-body").find("tr:last").data("unitper",supply.unitper);
								$(".table-body").find("tr:last").data("mincnt",supply.mincnt);
								changeSum();
							}
						});
					}
				}
		 	}
			
			//屏蔽鼠标右键
			//$(document).bind('contextmenu',function(){return false;});
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
// 		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
// 		 			var index=$(e.srcElement).closest('td').index();
// 		    		if(index=="4"||index=="8"){
// 			 			$(e.srcElement).unbind('blur').blur(function(e){
// 			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
// 			 			});
// 			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
// 		    		}
// 		    	}
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
			 		if(window.event && window.event.keyCode == 120) { 
			 			window.event.keyCode = 505; 
// 			 			searchChkstodemo();
			 		} 
			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 85: $('#autoId-button-101').click(); break;//查看上传
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 65: $('#autoId-button-103').click(); break;//新建
	                case 69: $('#autoId-button-104').click(); break;//编辑
	                case 83: $('#autoId-button-105').click(); break;//保存
					case 68: $('#autoId-button-106').click(); break;//删除
	                case 80: $('#autoId-button-107').click(); break;//打印
	                case 67: $('#autoId-button-108').click(); break;//审核
	            }
			}); 
	 		
// 	 		document.getElementById("firm").onfocus=function(){
// 	 			this.size = this.length;
// 	 			$('#firm').css('height','425px');
// 	 			$('#firm').parent('.form-input').css('z-index','2');
// 	 		};
//  			document.getElementById("firm").onblur = function(){
// 	 			this.size = 1;
// 	 			$('#firm').css('height','20px');
//  			};
// 	 		$('#firm').bind('dblclick', function() {
// 					$('#firmDes').val($(this).val());
// 					$('#firm').blur();
// 			});
 		    //回车换焦点end
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'title',
					validateType:['canNull','maxLength'],
					param:['F','10'],
					error:['<fmt:message key ="Title_can_not_be_empty" />！']
				},{
					type:'text',
					validateObj:'memo',
					validateType:['maxLength'],
					param:['20'],
					error:['<fmt:message key ="Remarks_must_be_less_than_20_words" />']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			<c:if test="${tableFrom=='table'}">
				$('#autoId-button-102').click();
			</c:if>
// 			today = getThisDay();
		});
		
// 		$(document).bind('keydown',function(e){
// 	 		if(e.keyCode==40){
// 	 			downMove();
// 	 		}else if(e.keyCode==38){
// 	 			upMove();
// 	 		}
// 	 	});
		
		//控制按钮显示
		function loadToolBar(){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									saveChkstom();
								}
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								parent.$('.close').click();
							}else{
								parent.$('.close').click();									
							}
						}
					}
				]
			});
		}			
		//编辑表格
		function editCells()
		{
			if($("#sta").val() == "add"){
				$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:9,
				widths:[26,80,100,80,65,80,80,65,80],
				colStyle:['','','','','','','','',''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[2,8],//能输入的位置(8暂去)
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					changeSum();
// 					$('#sum_amount').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(7)').text())).toFixed(2));//总金额
// 					$('#sum_amount').text(Math.round((Number($('#sum_amount').text())-Number(row.find('td:eq(5)').text()))*100)/100);//总数量
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(getName());
							return;
						}
					 	else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
					function getName(){
						var name='';
						$.ajaxSetup({ 
							  async: false 
							});
						$.get("<%=path %>/supply/findById.do",
								{sp_code:$.trim(data.curobj.closest('td').prev().text())},
								function(data){
									name =  data.sp_name;
								});
						return name;
					};
				},
				cellAction:[{
					index:2,
					action:function(row){
						$.fn.autoGrid.setCellEditable(row,8);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/supply/findTop.do';
						if(!isNaN(data.value))
							data['key'] = 'sp_code';
						else
							data['key'] = 'sp_init';
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '<fmt:message key="added_supplies_remind"/>！',
 								speed: 1000
 							});
 							$.fn.autoGrid.setCellEditable(row,2);
							return;
						}
						//查询售价
// 						$.ajax({
// 							type: "POST",
<%-- 							url: "<%=path%>/supply/findSprice.do", --%>
// 							data: "supply.sp_code="+data2.sp_code+"&area.code="+$('#firmDes').val()+"&madet="+$('#maded').val(),
// 							dataType: "json",
// 							success:function(spprice){
								row.find("td:eq(1) span").text(data2.sp_code);
								row.find("td:eq(2) span input").val(data2.sp_name).focus();
								row.find("td:eq(3) span").text(data2.sp_desc);
// 								row.find("td:eq(4) span").text(1).css("text-align","right");
								row.find("td:eq(4) span").text(data2.unit3);
								row.find("td:eq(5) span").text(data2.unit);
								row.find("td:eq(6) span").text(data2.sp_price).attr("price",data2.sp_price).css("text-align","right");
// 								row.find("td:eq(7) span").text(data2.sp_price).css("text-align","center");
// 								row.find("td:eq(8) span").text(0).css("text-align","right");
								row.find("td:eq(7) span").text(data2.unit1);
// 								row.find("td:eq(10) span").text(today);
								row.find("td:eq(8) span").text();
								row.data("unitper",data2.unitper);
								row.data("mincnt",data2.mincnt);
// 							}
// 						});
						//查询售价结束
					}
				},{
					index:8,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
// 						if(!row.next().html())$.fn.autoGrid.addRow().map(function(){
// 							$("#tblGrid").find("tr").map(function(){
// 								$(this).click(function(){
// 									$("#tblGrid").find("tr").removeClass();
// 									$(this).addClass("choose");
// 								});
// 							});
// 						});
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}
		//保存添加
		function saveChkstom()
		{
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key="not_add_empty_document"/>！');
				return;
			}
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","supply.unit3","supply.unit",
			            "price","supply.unit1","memo","id"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name)data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				var status=$(rows[i]).find('td:eq(0)').text();
				data["chkstoDemod["+i+"]."+"price"] = $(rows[i]).data('price') ? $(rows[i]).data('price'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
					if(value)
						data["chkstoDemod["+i+"]."+keys[j-1]] = value;
				}
			}

			$.post("<%=path%>/chkstodemom/updateChkstoDemom.do",data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					break;
				case 1:
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 3000
								});
					loadToolBar([true,true,true,true,false,true,true,true]);
					$("#sta").val("show");
					parent.pageReload();
					parent.$('.close').click();
					break;
				}
			});			 
		}
		
		//清空页面
		function clearValue(){
			$(':input','#listForm')  
			 .not(':button, :submit, :reset, :hidden')  
			 .val('')  
			 .removeAttr('checked')  
			 .removeAttr('selected');  
			$("#sta").val("show");
		}
		
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		
		function changeSum(){//计算统计数据
			$('#sum_num').text($(".table-body").find('tr').length);//总行数
		}
		function getThisDay(){
			var mydate = new Date();
			var year = mydate.getFullYear(); //获取完整的年份(4位,1970-????)
			var month = mydate.getMonth()+1; //获取当前月份(0-11,0代表1月)
			month = month<10?"0"+month:month;
			var day = mydate.getDate(); //获取当前日(1-31)
			day = day<10?"0"+day:day;
			return year+"-"+month+"-"+day;
		}
        function up(obj) {
            var objParentTR = $(obj).closest('tr');
            var prevTR = objParentTR.prev();
            var nextTR = objParentTR.next();
            if (prevTR.length > 0) {
                var curNum = $.trim(objParentTR.find('td:first').find('span').html());
                curNum = Number(curNum);
                if(curNum==2){//第二条和第一条互换
                    var delcell = '<td name="deleCell" style="width:10px;border:0;cursor: pointer;" onclick="deleteRow(this)">'+objParentTR.find('td:last').html()+'</td>';
                    objParentTR.find('td:last').remove();
                    $(delcell).appendTo(prevTR);
                }
                prevTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
                objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum-1)+'</span>');

                if(nextTR.length==0){//最后一行
                    var addCell = objParentTR.find('td:last');
                    prevTR.append(addCell);
                }
                prevTR.insertAfter(objParentTR);
            }

        }
        function down(obj) {
            var objParentTR = $(obj).closest('tr');
            var nextTR = objParentTR.next();
            var nextnextTR = objParentTR.next().next();
            if (nextTR.length > 0) {
                var curNum = $.trim(objParentTR.find('td:first').find('span').html());
                curNum = Number(curNum);
                if(curNum==1){//第一条和第二条互换
                    var delcell = '<td name="deleCell" style="width:10px;border:0;cursor: pointer;" onclick="deleteRow(this)">'+nextTR.find('td:last').html()+'</td>';
                    nextTR.find('td:last').remove();
                    $(delcell).appendTo(objParentTR);
                }
                nextTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
                objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum+1)+'</span>');

                if(nextnextTR.length==0){//和最后一行互换
                    var addCell = nextTR.find('td:last');
                    nextTR.find('td:last').remove();
                    objParentTR.append(addCell);
                }
                nextTR.insertBefore(objParentTR);
            }
        }
        function deleteRow(obj){
            var tb = $(obj).closest('table');
            var rowH = $(obj).parent("tr").height();
            var tbH = tb.height();
            $(obj).parent("tr").nextAll("tr").each(function(){
                var curNum = Number($.trim($(this).children("td:first").text()));
                $(this).children("td:first").html('<span style="width:24px;padding:0px;">'+Number(curNum-1)+'</span>');
            });

            if($(obj).next().length!=0){
                var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;display: none;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
                tb.find('tr:last').prev().append(addCell);
            }
            $(obj).parent("tr").remove();
            tb.height(tbH-rowH);
            tb.closest('div').height(tb.height());
        };
		</script>
	</body>
</html>