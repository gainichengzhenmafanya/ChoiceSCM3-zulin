<%@ page language="java" contentType="text/html; charset=UTF-8"  import="java.sql.*" 
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>仓位选择</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
  		<link type="text/css" rel="stylesheet" href="<%=path%>/css/chosen.css" />
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
		<div id='toolbar'></div>
		<div class="form" style="text-align: center;width: 98%;height: 85%">
			<form id="noticeForm" method="post" action="<%=path %>/publish/saveDistributionStores.do" style="height: 100%;">
			
				<input type="hidden" id="area" name="area" value=""/>
				<input type="hidden" id="typ" name="typ" value=""/>
				
				<div class="form-line">
					<div class="form-label" style="width:80px;"><fmt:message key ="coding" /> or <fmt:message key ="name" /></div>
					<div class="form-input">
						<select id="vscode" name="vscode"  data-placeholder="<fmt:message key ="select_positions" />(<fmt:message key ="branche" />)..." class="chosen-select" style="width:190px;" tabindex="2" onchange="setchangeValue(this)">
							<option value=""></option>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<option value="${positn.code }">${positn.code } -- ${positn.des }</option>
							</c:forEach>
						</select>
					</div>
				</div>
			
				<table cellspacing="0" cellpadding="0" style="height: 100%;">
					<tr>
						<td align="left" height="45%">
							<fieldset style="width: 155px;border-color: #B1C3D9; height: 95%;float: left ">
								<legend><fmt:message key ="area" /></legend>
								<div class="treePanel" style="width: 97%; height: 90%; overflow: auto;">
							        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
						        	<script type="text/javascript">
						          		var tree = new MzTreeView("tree");
						          
						          		tree.nodes['0_00000000000000000000000000000000'] = 'text:全部;method:setSelectStoreTeamList("00000000000000000000000000000000","")';
						          		<c:forEach var="areaobj" items="${area}" varStatus="status">
						          			tree.nodes["00000000000000000000000000000000_${areaobj.code }"] 
							          		= "text:${areaobj.des}; method:setSelectStoreTeamList('${areaobj.code}','')";
						          		</c:forEach>
						          		 tree.setIconPath("<%=path%>/image/tree/none/");
						          		document.write(tree.toString());
						        	</script>
						    	</div>
						    	
							</fieldset>
							<fieldset style="width: 110px;border-color: #B1C3D9; height: 95%; ">
								<legend><fmt:message key ="positions" /><fmt:message key ="scm_type" /></legend>
								<div class="treePanel" style="width: 97%; height: 90%; overflow: auto;">
						        	<script type="text/javascript">
						          		var treeTeam = new MzTreeView("treeTeam");
						          		treeTeam.nodes['0_1'] = 'text:全部;method:setSelectStoreTeamList("","00000000000000000000000000000000")';
						          		<c:forEach var="positntype" items="${listPositnTyp}" varStatus="status">
							          		treeTeam.nodes['1_${positntype.code}'] = "text:${positntype.des}; method:setSelectStoreTeamList('','${positntype.code}')";
						          		</c:forEach>
						          		 treeTeam.setIconPath("<%=path%>/image/tree/none/");
						          		document.write(treeTeam.toString());
						        	</script>
						    	</div>
						    	
							</fieldset>
						</td>
						<td rowspan="2" align="center" valign="middle" style="width: 35px; height: 90%; " >
							<input type="button" value=">>" onclick="_rightAll()" />
							<br />
							<br />
							<br />
							<input type="button" value="&nbsp;>&nbsp;" onclick="_right()" />
							<br />
							<br />
							<br />
							<input type="button" value="&nbsp;&lt;&nbsp;" onclick="_left()" />
							<br />
							<br />
							<br />
							<input type="button" value="&lt;&lt;" onclick="_leftAll()" />
						</td>
						<td rowspan="2"  align="left" height="95%">
							<fieldset style="width: 275px;border-color: #B1C3D9; height: 100%; ">
								<legend><fmt:message key ="selected" /><fmt:message key ="positions" /></legend>
								<div style="width: 99%; height: 95.5%; overflow: auto;">
									<select class="yixuan"  style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectedstore"  >
										<c:forEach var="positn" items="${listPositn1}" varStatus="status">
											<option value="${positn.code}">${positn.code} -- ${positn.des}</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td  align="left" height="50%">
							<fieldset style="width: 275px;border-color: #B1C3D9; height:100%; ">
								<legend><fmt:message key ="select_positions" /></legend>
								<div style="width: 97%; height: 90%; overflow: auto;">
									<select class="weixuan" style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectstore" >
										<c:forEach var="positn" items="${listPositn}" varStatus="status">
											<option value="${positn.code }">${positn.code } -- ${positn.des }</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
			<form action="<%=path %>/chkstodemom/saveChkstoDemoFirm.do" id="listForm" name="listForm" method="post">
				<input type="hidden" id="firm" name="firm"/>
				<input type="hidden" id="chkstodemonoa" name="chkstodemonoa" value="${chkstodemo }"/>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/chosen.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
			var editor;
			var single='${single}';
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vtitle',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="title"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="title_too_long"/>！']
					}]
				});
				
				//控制店铺选择框
				var config = {
			      '.chosen-select'           : {},
			      '.chosen-select-deselect'  : {allow_single_deselect:true},
			      '.chosen-select-no-single' : {disable_search_threshold:10},
			      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			      '.chosen-select-width'     : {width:"95%"}
			    };
			    for (var selector in config) {
			      $(selector).chosen(config[selector]);
			    }
			    
				//左侧未选择的 过滤掉 右侧已选择的
				var yixuan_arry = [];
				<c:forEach var="positn1" items="${listPositn1}">
					yixuan_arry.push('${positn1.code}');
				</c:forEach>
			    $(".weixuan option").each(function(){
			    	if($.inArray($(this).val(),yixuan_arry)>-1){
			    		$(this).hide();
			    	}
			    });
			    $("#vscode option").each(function(){
			    	if($.inArray($(this).val(),yixuan_arry)>-1){
			    		$(this).hide();
			    	}
			    });
			});
			
			var _nodetypes="";
			var _nodeid="";
			function setSelectStoreList(nodetypes,nodeid) {
// 				$("#area").val(nodeid);
				_nodetypes=nodetypes;
				_nodeid=nodeid;
				$("#selectstore").empty();
				<c:forEach var="positn" items="${listPositn}" varStatus="status">;
					if(nodetypes == "0"){
						$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
					} else if (nodetypes == "1") {
						if (nodeid == "${store['PK_MARKETID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					} else if (nodetypes == "2") {
						if (nodeid == "${store['PK_BOHID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					} else if (nodetypes == "3") {
						if (nodeid == "${store['PK_GOVERNORID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					}
				</c:forEach>;
			}
			
			function setSelectStoreTeamList(nodetypes,nodeid) {
				if(nodetypes!=""){
					$("#area").val(nodetypes);
				}
				if(nodeid!=""){
					$("#typ").val(nodeid);
				}
 				//$("#selectstore").empty();
 				$.ajaxSetup({async:false});
				$.post("<%=path %>/positn/searchAllPositnNew.do?typ="+$("#typ").val()+"&area="+$("#area").val(),function(data){
					//1、取出原来选择门店中含有的数据
					var array = new Array(); //定义数组
					//1、从此处进行清空，重新加载  选中区域列表中对应的数据
					$("#selectstore").empty();
// 					setSelectStoreList(_nodetypes,_nodeid);
					//2、再获取选择门店中的数据
				   $("#selectstore option").each(function(){ //遍历全部option
				        var txt = $(this).val(); //获取option的内容
				        array.push(txt); //添加到数组中
				    });
				   $("#selectstore").empty();
					if(array.length==0){
						for(var i in data){
							$("#selectstore").append("<option value='"+data[i].code+"'>"+data[i].code+" -- "+data[i].des+"</option>");
						}
					}else{
						//2、对新添加的数据进行比较，假如含有，则不往里面添加，没有  则添加
						for(var i in data){
							for(var j=0;j<array.length;j++){
								if(data[i].code==array[j]){
									$("#selectstore").append("<option value='"+data[i].code+"'>"+data[i].code+" -- "+data[i].des+"</option>");
									break;
								}
							}
							
						}
						
					}
				});
			}
			$('#selectstore').bind('dblclick',function(){
				_right();
			});
			function _rightAll() {
				if(single=='true' && ($(".yixuan option").size()>0||($(".weixuan option").size()+$(".yixuan option").size()>1))){
					alerterror('<fmt:message key="only_select_one_store" />!');
					return;
				}
				var v='';
				$(".weixuan option").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			
			function _right() {
				if(single=='true' && ($(".yixuan option").length>0 || $(".weixuan option:selected").length > 1)){
					alert('<fmt:message key ="Please_choose_a_position" />!');
					return;
				}
				var v='';
				$(".weixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);		
			}
						
			function _left() {
				var v='';
				$(".yixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".weixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$(".yixuan option:selected").each(function(){
					$(this).remove();
				});
			}
			
			function _leftAll() {
				$(".yixuan").empty();
			}
			
			//返回选中列表长度
			function reutrnValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).val());
				});
				$("#pk_stores").val(chkValue);
				return chkValue.length;
			}
			
			//返回选中列表门店编码--名称
			function reutrnChkValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).html());
				});
// 				$("#pk_stores").val(chkValue);
				return chkValue;
			}
			
			//返回选中列表店铺数据
			function reutrnChkStoreValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).val());
				});
				$("#pk_stores").val(chkValue);
				return chkValue;
			}
			
			function setchangeValue() {
				var v='';
				$("#vscode option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			//返回门店主键、编码、名称信息
			function returnStoreList(){
				var data = {show:[],code:[],mod:[],entity:[]};
				$(".yixuan option").each(function(){
					var entity = {};
					entity.pk_store = $.trim($(this).val());
					entity.vcode = $.trim($(this).html().substring(0,$(this).html().indexOf(" ")));
					alerterror($.trim($(this).html().substring(0,$(this).html().indexOf(" "))));
					entity.vname = $.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1));
					alerterror($.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1)));
					data.entity.push(entity);
					return ;
				});
// 				parent['${callBack}'](data);
// 				$(".close",parent.document).click();
			}
			
			function select_Positn(){
				if($(".yixuan option").length >0){
					var msg = "";
					$(".yixuan option").each(function(){
						msg +=$(this).val()+",";
					});
					$("#firm").val(msg);
					return true;
				}else{
					alert("<fmt:message key='please_select'/><fmt:message key='branche'/>");
					return false;
				}
			}
		</script>
	</body>
</html>