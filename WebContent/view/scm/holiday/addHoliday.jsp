<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加节假日</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.select{
				width: 86%;
			}
		</style>	   
		<script type="text/javascript">
			var path="<%=path%>";
		</script>	
	</head>
	<body>
		<div class="form">
			<form id="holidayForm" method="post" action="<%=path %>/holidays/saveByAddHoliday.do">
				<div class="form-line ">
					<div class="form-label">
						<span class="red">*</span><fmt:message key ="date" />
					</div>
					<div class="form-input">
						<input type="text" name="dat" id="dat" class="Wdate text" value="<fmt:formatDate value="${holiday.dat}" pattern="yyyy-MM-dd"/>" />
					</div>
					<div class="form-label">
						<span class="red">*</span><fmt:message key="Festival_type" />
					</div>
					<div class="form-input">
						<select name="typ" id="typ" class="select">
							<option value="节假日"><fmt:message key ="scm_holidays" /></option>
							<option value="周末"><fmt:message key ="weekend" /></option>
							<option value="法定假日"><fmt:message key="Legal_holiday" /></option>
							<option value="其他"><fmt:message key ="other" /></option>
						</select>
					</div>
				</div>
				<div class="form-line ">
					<div class="form-label">
						<span class="red">*</span><fmt:message key="Holiday_grades" />
					</div>
					<div class="form-input">
						<select id="grade" name="grade" class="select">
							<c:forEach begin="1" end="6" var="grade" >
								<option value="${grade }">${grade }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label">
						<span class="red"></span><fmt:message key ="remark" />
					</div>
					<div class="form-input">
						<input type="text" name="memo" id="memo" class="text" value="" maxlength="10"/>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>		
				
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
			 	$("#dat").htmlUtils("setDate","now");
				$("#dat").click(function(){
					new WdatePicker();
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'dat',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'grade',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					}]
				});
				var date = new Date();
				var year = date.getFullYear();
				$("#year").find("option[value='"+year+"']").attr("selected","selected");
			});
			//屏蔽空格
			$(document).keydown(function(event){
				if(event.keyCode==32){
					return false;
				}
			});
			
		</script>
	</body>
</html>