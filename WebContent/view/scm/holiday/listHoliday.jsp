<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>节假日设置</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.table-head td span{
			white-space: normal;
		}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/holidays/list.do" method="post">
			<input type="hidden" name="tableName" id="tableName" value="holiday"/>
			<input type="hidden" name="reportName" id="reportName" value="holiday"/>
			<input type="hidden" name="load" id="load" value="n"/>
			<input type="hidden" name="yyear" id="yyear" value="${holiday.year }"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td style="width:30px;text-align: center;">
									<span style="width: 20px;">
										<input type="checkbox" id="chkAll"/>
									</span>
								</td>
								<td><span style="width:60px;"><fmt:message key ="scm_type" /></span></td>
								<td><span style="width:85px;"><fmt:message key ="date" /></span></td>
								<td><span style="width:50px;"><fmt:message key="Holiday_grades" /></span></td>
								<td><span style="width:50px;"><fmt:message key ="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="holiday" varStatus="step" items="${holidayList}">
								<tr>
									<td class="num"><span  style="width:16px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<span style="width: 20px;"><input type="checkbox" name="idList" id="chk_${holiday.sdat}" value="${holiday.sdat}"/></span>
									</td>
									
									<td align="center"><span style="width:60px;" title="${holiday.typ}"><c:out value="${holiday.typ}" />&nbsp;</span></td>
									<td><span style="width:85px;" title="${holiday.sdat}"><c:out value="${holiday.sdat}" />&nbsp;</span></td>
									<td><span style="width:50px;" title="${holiday.grade}"><c:out value="${holiday.grade}" />&nbsp;</span></td>
									<td><span style="width:50px;" title="${holiday.memo}"><c:out value="${holiday.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${holiday.orderBy}" />" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${holiday.orderDes}" default="000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		
		<div class="search-div" style="margin-left: 0px;*margin-left:-20px;">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="c-left"><fmt:message key ="scm_year" />：</td>
							<td>
								<select id="year" name="year" class="select">
									<option value=""><fmt:message key ="all" />/option>
									<c:forEach begin="2008" end="2020" var="year" >
										<option value="${year }" <c:if test="${holiday.year == year }">selected="selected"</c:if>>${year }</option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</table>
				</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
	       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
			</div>
		</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		
		//查询等待start
		$("#listForm").submit( function () {
			$("#wait2").css("visibility","visible");
			$("#wait").css("visibility","visible");
		});
		//查询等待end
		
			var t;
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//排序start
					 var array = new Array();  
					array.push('typ');
					array.push('dat');
					array.push('grade');
					array.push('memo');
						/* <c:forEach var="dictColumns" items="${columns}">
							array.push('${dictColumns.columnName}');
						</c:forEach> */
					$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
						$(this).bind('click',function(){
							var orderDes=$('#orderDes').val();
							var  a=orderDes.charAt(i);
							var b='';
							a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
							a==1?a=0:a=1;
							//保存各个列的排序方式
							$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
							var str = $('#orderBy').val();
							str = str.replace(array[i]+' asc'+',','').replace(array[i]+' desc'+',','').replace(','+array[i]+' asc','').replace(','+array[i]+' desc','').replace(array[i]+' asc','').replace(array[i]+' desc','');
							if(str=='' || str == null || str =='undefined'){
								$('#orderBy').val(b);
							}else{
								$('#orderBy').val(b+','+str);
							}
							$('#listForm').submit();
						});
					});
					var order=$('#orderDes').val();
					for(var i=0; i<order.length; i++){
						if(order.charAt(i)==1)
							$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
					} 
				//排序结束
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								 $('.search-div').slideToggle(100);
							}
						},{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveHoliday();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteHoliday();
							
							}
						},{
							text: '<fmt:message key="Automatically_generated_Saturday_day" />',
							title: '<fmt:message key="Automatically_generated_Saturday_day" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								if($("#year").val() == ""){
									alert("<fmt:message key="Year_can_not_be_empty_please_choose_the_year" />！");
									return false;
								}
								var showMsg="";
								var year = $("#yyear").val();
								$.ajax({url:"<%=path%>/holidays/countHoliday.do",
			  	 					type:'POST',
			  	 					async:false,
			  	 					data:{syear:year},
			  	  	 				success:function(data){
			  	  	 					if(data>1){
			  	  	 						showMsg="<fmt:message key ="already_exists" /> "+year+" <fmt:message key="Saturday_day_of_the_year_determined_to_automatically_generate" /> "+year+" <fmt:message key="Is_the_Saturday_day_of_the_year" />？";
			  	  	 					}else{
			  	  	 						showMsg="<fmt:message key="Automatic_generation" /> "+year+ "<fmt:message key="Is_the_Saturday_day_of_the_year" />？";
			  	  	 					}
			  	  	 				}
		  	  	 				});
								if(confirm(showMsg)){
									$.ajax({url:"<%=path%>/holidays/createHolidayAuto.do",
				  	 					type:'POST',
				  	 					data:{year:year},
				  	 					async:false,
				  	  	 				success:function(data){
				  	  	 					if(data=="success"){
				  	  	 					 showMessage({
						  	  						type: 'success',
						  	  						msg: '<fmt:message key="operation_successful"/>',
						  	  						speed: 1500,
						  	  						handler:function(){
						  	  							pageReload();
						  	  						}
						  	  					});
				  	  	 					}else{
					  	  	 					showMessage({
						  	  						type: 'error',
						  	  						msg: data,
						  	  						speed: 1500
						  	  					});
				  	  	 					}
				  	  	 				}
									});
								}
							}
						},{
							text: '<fmt:message key="export" />',
							title: '<fmt:message key="exporting_reports" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['2px','2px']
							},
							handler: function(){
								if(confirm("<fmt:message key="Do_you_want_to_export_all_holiday_settings_Because_the_network_may_need_some_time_please_be_patient" />！")){
									$("#wait2").val('NO');//不用等待加载
									$('#listForm').attr('action',"<%=path%>/holidays/exportToExcel.do");
									$('#listForm').submit();
									$('#listForm').attr('action',"<%=path%>/holidays/list.do");
									$("#wait2").val('');//等待加载还原
								}
							}
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								$('#listForm').attr("action","<%=path%>/holidays/printReport.do");
								$('#listForm').submit();
								$('#listForm').attr("action","<%=path%>/holidays/list.do");
								$('#listForm').attr('target','');
								$("#wait2").val('');//等待加载还原
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition').find(".select").find('option:first').attr('selected','selected');
					$('.search-condition input').val('');
				});
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				function saveHoliday(){
					var saveHolidayFrame=$('body').window({
						id: 'window_Holiday',
						title: '<fmt:message key="insert" /><fmt:message key ="scm_holidays" />',
						content: '<iframe id="saveHolidayFrame" frameborder="0" src="<%=path%>/holidays/addHoliday.do"></iframe>',
						width: 600,
						height: 450,
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveHolidayFrame')&& window.document.getElementById("saveHolidayFrame").contentWindow.validate._submitValidate()){
											$.ajax({url:"<%=path%>/holidays/saveByAddHoliday.do",
						  	 					type:'POST',
						  	 					data:getParam($(getFrame("saveHolidayFrame").document).find("#holidayForm")),
						  	  	 				success:function(data){
						  	  	 					if(data=="success"){
						  	  	 					 showMessage({
								  	  						type: 'success',
								  	  						msg: '<fmt:message key="operation_successful"/>',
								  	  						speed: 1500,
								  	  						handler:function(){
								  	  							pageReload();
								  	  						}
								  	  					});
						  	  	 						saveHolidayFrame.close();
						  	  	 					}else{
						  	  	 					getFrame("saveHolidayFrame").showMessage({
								  	  						type: 'error',
								  	  						msg: data,
								  	  						speed: 1500
								  	  					});
						  	  	 					}
						  	  	 				}
											});
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				
				function toReport(){
					
					$('window.parent.parent.body').window({
						title: '<fmt:message key="print" />',
						content: '<iframe frameborder="0" src="<%=path%>/holidays/toReport.do?level=${level}&&code=${code}"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true
						});
				}
				function deleteHoliday(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />？')){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							var action = '<%=path%>/holidays/deleteHoliday.do?dat='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key ="delete" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '340px',
								height: '255px',
								draggable: true,
								isModal: true
							});
							pageReload();
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
// 				var date = new Date();
// 				var year = date.getFullYear();
// 				$("#syear").find("option[value='"+year+"']").attr("selected","selected");
			});
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>