<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width:auto;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
  	</style>
  </head>	
  <body>
    <div style="overflow-x:auto;width: 100%">
	  	<div style="width: 630px;">
	  		<a href="<%=path %>/JgYujingBizhong/toPriceWarning.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_jgyjhbz"/></a>
	  		<a href="<%=path %>/CgJiageYidong/toPurchasePriceChange.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_cgjgyd"/></a>
	  		<a href="<%=path %>/WzGysFenxi/toSupplyDeliver.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_wzgysfx"/></a>
	<%--   		<a href="<%=path %>/view/scm/reportAnalysis/jinhuojiage.jsp" class="easyui-linkbutton" plain="true" iconCls="icon-redo">进货价格比较</a> --%>
	  		<a href="<%=path %>/JhJiageFenxi/tofindFirmStockPrice.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_jhjgfx"/></a>
	  		<a href="<%=path %>/CgJiageYidong/toPurchasePriceChange1.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_cgjgyd"/>1</a>
	  	</div>
	  	<div style="width: 760px;">
	  		<a href="<%=path %>/KcZhouzhuanLv/toKuCunZhouZhuanLv.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_kczzl"/></a>
	  		<a href="<%=path %>/KlZongheFenxi/toInventoryAgingSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_klzhfx"/></a>
	  		<a href="<%=path %>/WzKulingMingxi/toSupplyKuLingDetail.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_wzklmx"/></a>
	  		<a href="<%=path %>/WzABCFenxi/toSupplyABCStatisticsSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_wzabcfx"/></a>
	  		<a href="<%=path %>/CkZhoufahuoDuibi/toCangKuZhouFaHuoDuiBi.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_ckzfhdb"/></a>
	  		<a href="<%=path %>/CkLingyongDuibi/toWarehouseRequisitionedContrast.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_cklydb"/></a>
	  	</div>
	  	<div style="width: 890px;">
	  		<a href="<%=path %>/ZjZhanyongQingkuang/toZiJinZhanYongQingKuang.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_zjzyqk"/></a>
	  		<a  href="<%=path %>/RkChengbenFenxi/toReceiptCostAnalysis.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_rkcbfx"/></a>
	  		<a href="<%=path %>/RkChengbenFenxi/toZhouRuKuChengBenFenXi.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_zrkcbfx"/></a>
	<%--   		<a href="<%=path %>/BhgJinhuoTongji/toUnqualifiedStatistics.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">不合格进货统计</a> --%>
	  	</div>
	  	<div style="width: 500px;">
	  		<a href="<%=path %>/SgDaohuoFenxi/toApplyArriveStatistics.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="scm_sgdhfx"/></a>
	  		<a href="<%=path %>/SgChukuFenxi/toApplyOutStatistics.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="scm_sgckfx"/></a>
	<%--   		<a href="<%=path %>/WlPeisongFenxi/toLogisticsAnalysis.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum">物流配送分析</a> --%>
	  	</div>
	  	<div style="width: 500px;">
	  		<a href="<%=path %>/KclYuguShiyong/toInventoryEstimates.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="scm_kclygsy"/></a>
	  		<a href="<%=path %>/CgCesuan/toCaiGouCeSuan.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="scm_cgcs"/></a> 
	  	</div>
	  	<c:if test="${!empty  YNCK}">		
		  	<div style="width: 500px;">
		  		<a href="<%=path %>/mdRiPanDianZhuangTaiBiao/findMdRiPanDianZhuangTaiBiao.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="scm_mdrpdztb"/></a>
		  	</div>
	  	</c:if>
	</div>  	
  	
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  	$('a').click(function(event){
			var self = $(this);
			if(!self.attr("tabid")){
				self.attr("tabid",self.text().replace(/[ ]/g,"")+new Date().getTime());
			}
			event.preventDefault();
			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
		});
  	</script>
  </body>
</html>
