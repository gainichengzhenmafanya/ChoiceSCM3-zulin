<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
response.setHeader("Pragma","No-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>入库成本分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}		
		form .form-line .form-label{
			width: 5%;
		}
		form .form-line .form-input{
			width: 18%;
		}
		.datagrid-header tr{height:11px;}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index:10;">
					<div class="form-label" ><fmt:message key="coding"/></div>
					<div class="form-input" style="margin-top: -3px;">
						<input type="text" id="sp_code" name="sp_code" value="${sp_code}" class="text" style="width: 136px;"/>
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
					</div>
					<div class="form-label"><fmt:message key ="months" /></div>
					<div class="form-input">
						<select id="month" name="month" class="select">
							<option value="1" selected="selected">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</div>
				</div>
				<div class="form-line" style="z-index:9;">
					<div class="form-label" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>><fmt:message key="positions"/></div>
					<div class="form-input" <c:if test='${type!=null}'>style="visibility: hidden;"</c:if>>
						<%-- <input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
						<input type="hidden" id="positn" name="positn" value=""/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
						<select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input"><select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div>
					<div class="form-label"><fmt:message key="smallClass"/></div>
					<div class="form-input"><select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"  class="select"></select></div>
				</div>
	</form>
 	<div id="datagrid"></div>
 	<div id="dataDetail"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
 		var firstLoad = true;//标记是否为第一次加载
 		//收集form表单数据的对象
 		var params = {};
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load",params);
							//$("#dataDetail").datagrid("load",params);
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/RkChengbenFenxi/exportReceiptCostAnalysisSum.do?typdes=detail");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/RkChengbenFenxi/printReceiptCostAnalysisSum.do?typdes=detail";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
			var month = new Date().getMonth()+1;
			$("#month > option[value="+month+"]").attr("selected","selected");
 
  	 		$("select").not("#month").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		
  	 		builtTable({
  	 			headUrl:"<%=path%>/RkChengbenFenxi/findReceiptCostAnalysisHeaders.do?typ=sum",
  	 			dataUrl:"<%=path%>/RkChengbenFenxi/findReceiptCostAnalysisSum.do",
  	 			title:'<fmt:message key ="scm_rkcbfx" />',
  	 			id:"datagrid",
  	 			height:tableHeight/2,
  	 			onClickRow:function(rowIndex, rowData){
  	 				var p = {};
  	 				p['grp']=rowData.GRP;
  	 				if($('#sp_code').val()){
	  	 				p['sp_code']=$('#sp_code').val();
  	 				}
  	 				if($('#grptyp').val()){
	  	 				p['grptyp']=$('#grptyp').val();
  	 				}
  	 				if($('#positn').val()){
	  	 				p['positn']=$('#positn').val();
  	 				}
  	 				if($('#typ').val()){
	  	 				p['typ']=$('#typ').val();
  	 				}
  	 				if($('#month').val()){
	  	 				p['month']=$('#month').val();
  	 				}
  	 				if($('#grptyp').val()){
	  	 				p['grptyp']=$('#grptyp').val();
  	 				}
//   	 				p['grptyp']=$('#grptyp').val();
//   	 				p['positn']=$('#positn').val();
//   	 				p['typ']=$('#typ').val();
//   	 				p['month']=$('#month').val();
  	 				$("#dataDetail").datagrid("load",p);
  	 			}
  	 		});
  	 		
  	 		builtTable({
  	 			headUrl:"<%=path%>/RkChengbenFenxi/findReceiptCostAnalysisHeaders.do?typ=detail",
  	 			dataUrl:"<%=path%>/RkChengbenFenxi/findReceiptCostAnalysisSum.do?typdes=detail",
  	 			title:'<fmt:message key="Storage_cost_analysis_-_material_details" />',
  	 			id:"dataDetail",
  	 			height:tableHeight/2
  	 		});
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/RkChengbenFenxi/toColChooseReceiptCostAnalysisSum.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  		//生成表格
  	 	function builtTable(params){
  	 		firstLoad = true;
  	 		var headUrl = params.headUrl;
  	 		var contentUrl = params.dataUrl;
  	 		var title = params.title;
  	 		var grid = $('#'+params.id);
  	 		var dateCols = params.dateCols ? params.dateCols : [];
  	 		var numCols = params.numCols ? params.numCols : [];
  	 		var filter = typeof(params.filter) == 'function' ? params.filter : function(data){return data;};
  	 		var singleSelect = params.singleSelect ? params.singleSelect : true;
  	 		var pagination = params.pagination ? params.pagination : true;
  	 		var showFooter = params.showFooter ? params.showFooter : true;
  	 		var pageList = params.pageList ? params.pageList : [10,20,30,40,50];
  	 		var gridHeight = params.height ? params.height : tableHeight;
  	 		var onClickRow = params.onClickRow ? params.onClickRow : function(a,b){return;};
  	 		
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//数字列
  	 		var numCols = ['ina1','ina2','ina3','ina4','ina5','ina6','ina7','ina8','ina9','ina10','ina11','ina12'];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:headUrl,
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 			var Cols = [];
  	 			var colsSecond = [];
  	 		var prev = '';
  	 		var temp;
  	 			for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 			var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 			if(t && !t.length){
  	 				for(var i in tableContent.columns){
  	 					var align = $.inArray(tableContent.columns[i].columnName.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 		 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 		 			else
  	 		 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 		 		}
  	 			head.push(columns);
  	 		 		frozenHead.push(frozenColumns);
  	 			}else{
  	 				for(var i in tableContent.columns){
  	 					var align = $.inArray(tableContent.columns[i].columnName.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 					if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 		 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					else{
  	 						var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 						if(cur && cur.length){
  	 							var cur = tableContent.columns[i].zhColumnName;
  	 							if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 								temp.colspan ++;
  	 							}else{
  	 								temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 								columns.push(temp);
  	 								prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 							}
  	 							colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 						}else{
  	 							if(tableContent.columns[i].columnName)
  	 								columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 						}
  	 					}
  	 				}
  	 				head.push(columns);
  	 				colsSecond.push({field:'POSITN',title:'positn',rowspan:2,hidden:true});
  	 				head.push(colsSecond);
  	 				frozenHead.push(frozenColumns);
  	 			}
  	 	 		//生成报表数据表格
  	 			grid.datagrid({
  	 		 			title:title,
  	 		 			width:'100%',
  	 		 			height:gridHeight,
  	 		 			nowrap: true,
  	 					striped: true,
  	 					singleSelect:singleSelect,
  	 					collapsible:true,
  	 					loadFilter:function(data){
  	 						if(dateCols.length){
  	 							var row = data.rows;
  	 							for(var i in row){
  	 								for(var j in dateCols){
  	 									row[i][j] = convertDate(row[i][j] ? Number(row[i][j]) : 0);
  	 								}
  	 							}
  	 						}
  	 		 				return filter(data);
  	 					},
  	 					url:contentUrl,
  	 					remoteSort: true,
  	 					//页码选择项
  	 					pageList:pageList,
  	 					frozenColumns:frozenHead,
  	 					columns:head,
  	 					queryParams:params,
  	 					showFooter:showFooter,
  	 					pagination:pagination,
  	 					rownumbers:true,
  	 					fitColumns:false,
  	 					onClickRow:onClickRow,
  	 					rowStyler:function(){
  	 						return 'line-height:11px';
  	 					},
  	 					onBeforeLoad:function(){
  	 						if(firstLoad){
  	 							firstLoad = false;
  	 							return false;
  	 						}	
  	 					}
  	 		 	});
  	 		 	$(".panel-tool").remove();
  	 	}
  	 </script>
  </body>
</html>
