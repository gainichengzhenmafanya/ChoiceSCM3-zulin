<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>采购价格异动</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
 		form .form-line .form-label{ 
 			width: 50px; 
 			margin-left: 20px;
 		} 
 		form .form-line .form-input{ 
 			width: 140px; 
 		} 
 		.form-line .form-input input[type=text] , .form-line .form-input select{ 
 			width:160px; 
 		} 
	</style>
</head>
<body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index:10;">
					<div class="form-label" <c:if test='${type!=null}'>style="display:none;"</c:if>><fmt:message key="positions"/></div>
					<div class="form-input" <c:if test='${type!=null}'>style="display:none;"</c:if>  style="top: 1px">
						<select  name="positn" id="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select>
<!-- 						<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/> -->
<!-- 						<input type="hidden" id="positn" name="positn" value=""/> -->
<%-- 						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
					</div>
					<div class="form-label" ><fmt:message key="coding"/></div>
					<div class="form-input" style="margin-top: -4px;">
						<input type="text" id="sp_code" name="sp_code" value="${sp_code}" class="text" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
					<div class="form-label" >月份</div>
					<div class="form-input" style=" margin-top: -2px;">
						<select id="month" name="month" class="select">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</div>
					<div class="form-label" <c:if test='${type!=null}'>style="margin-left:50px;"</c:if> style="margin-left:30px;">   <input id="monthnew" name="monthnew" type="checkbox" value="true"/>本月新购物资</div>
					<div class="form-input">
						
					</div>
				</div>
				<div class="form-line" style="z-index:9;">
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select  name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input"><select  name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div>
					<div class="form-label"><fmt:message key="smallClass"/></div>
					<div class="form-input"><select  name="typ" url="<%=path %>/grpTyp/findAllTyp.do"  class="select"></select></div>
					<div class="form-label" style="width: 80px;"><fmt:message key="price_volatility_is_greater_than"/>:</div>
					<div class="form-input" style="width: 30px;"><input type="text" class="text"  id="min" name="min" style="width: 30px;"/></div>
					<div class="form-label" style="width: 80px;"><fmt:message key="price_volatility_is_less_than"/>:</div>
					<div class="form-input" style="width: 30px;"><input type="text" class="text"  id="max" name="max" style="width: 30px;"/></div>
				</div>
	</form>
 	<div id="datagrid"></div>
 	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
 	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  		$(document).ready(function(){
	 		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var form = $("#queryForm").find("*[name]");
						form = form.filter(function(index){
							var cur = form[index];
							if($(cur).attr("name")){
								if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
									if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
										if($("input[name='"+$(cur).attr("name")+"']:checked").length){
											params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
										}else{
											params[$(cur).attr("name")] = undefined;
										}
									}else{
										params[$(cur).attr("name")] = $(cur).val();
									}
								}
							}
						});
						if(!params['positn']){
							alert('<fmt:message key="please_select_positions"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="please_select_positions"/>！',
								speed: 1500
							}); */
							return;
						}	
						
						if($('#monthnew').attr('checked')){
							if($("#datagrid").datagrid("getColumnOption","PREVPRICE"))
								$("#datagrid").datagrid("hideColumn","PREVPRICE");
							if($("#datagrid").datagrid("getColumnOption","VOLATILITY"))
								$("#datagrid").datagrid("hideColumn","VOLATILITY");
							if($("#datagrid").datagrid("getColumnOption","PPREVPRICE"))
								$("#datagrid").datagrid("hideColumn","PPREVPRICE");
						}else{
							if($("#datagrid").datagrid("getColumnOption","PREVPRICE"))
								$("#datagrid").datagrid("showColumn","PREVPRICE");
							if($("#datagrid").datagrid("getColumnOption","VOLATILITY"))
								$("#datagrid").datagrid("showColumn","VOLATILITY");
							if($("#datagrid").datagrid("getColumnOption","PPREVPRICE"))
								$("#datagrid").datagrid("showColumn","PPREVPRICE");
						};
						$("#datagrid").datagrid("load");
					}
				},{
					text: 'Excel',
					title: 'Excel',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-40px','-20px']
					},
					handler: function(){
						var positn = typeof($("#positn").data("checkedVal"))!="undefined"?$("#positn").data("checkedVal"):"";
						if(positn){
							$('#queryForm').attr('action',"<%=path%>/CgJiageYidong/exportPurchasePriceChange.do");
							$('#queryForm').submit();
						}else{
							alert('<fmt:message key="please_select_positions"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="please_select_positions"/>！',
								speed: 1500
							}); */
						}
					}
				},{
					text: '<fmt:message key="print" />',
					title: '<fmt:message key="print" />',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						var positn = typeof($("#positn").data("checkedVal"))!="undefined"?$("#positn").data("checkedVal"):"";
						if(positn){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/CgJiageYidong/printPurchasePriceChange.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}else{
							alert('<fmt:message key="please_select_positions"/>！');
							/* showMessage({
								type: 'error',
								msg: '<fmt:message key="please_select_positions"/>！',
								speed: 1500
							}); */
						}
					}
				},{
					text: '<fmt:message key="column_selection" />',
					title: '<fmt:message key="column_selection" />',
					useable:true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-100px','-60px']
					},
					handler: function(){
						toColsChoose();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					useable:true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}
			]
		});
	 		//设置默认波动范围
	 		if(!$('#max').val())$('#max').val(10);
	 		if(!$('#min').val())$('#min').val(0);
	 		//默认月份
	 		var month = new Date().getMonth()+1;
			$("#month > option[value="+month+"]").attr("selected","selected");
	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
	 		$("select").not('#month').each(function(){
	 			$(this).htmlUtils("select",[]);
	 		});
	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
	 		var dateCols = ['dat'];
	 		//数字列
	 		var numCols = ['cntin','pricein','prevprice','pprevprice'];
	 		//收集form表单数据的对象
	 		var params = {};
	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
	 		var tableContent = {};
	 		//表头行（单行）
	 		var columns = [];
	 		//表头（多行），其中元素为columns
	 		var head = [];
	 		//需要固定在左侧的列的表头（单行）
	 		var frozenHead = [];
	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
	 		var frozenColumns = [];
	 		//ajax获取报表表头
	 		$.ajax({url:"<%=path%>/CgJiageYidong/findPurchasePriceChangeHeader.do",
	 				async:false,
	 				success:function(data){
	 					tableContent = data;
	 				}
	 			});
	 		//解析获取的数据
	 		var frozenIndex = tableContent.frozenColumns.split(',');
	 		var Cols = [];
	 		var colsSecond = [];
		var prev = '';
		var temp;
	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
	 		if(t && !t.length){
	 			for(var i in tableContent.columns){
	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 			else
  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  	 		}
			head.push(columns);
  	 		frozenHead.push(frozenColumns);
	 		}else{
	 			for(var i in tableContent.columns){
	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
	 				else{
	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
	 					if(cur && cur.length){
	 						var cur = tableContent.columns[i].zhColumnName;
	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
	 							temp.colspan ++;
	 						}else{
	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
	 							columns.push(temp);
	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
	 						}
	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
	 					}else{
	 						if(tableContent.columns[i].columnName)
	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
	 					}
	 				}
	 			}
	 			head.push(columns);
	 			head.push(colsSecond);
	 			frozenHead.push(frozenColumns);
	 		}
	 		
	 		
	 		//生成报表数据表格
	 		$("#datagrid").datagrid({
	 			title:'<fmt:message key ="scm_cgjgyd" />',
	 			width:'100%',
	 			height:tableHeight,
	 			nowrap: true,
			striped: true,
			singleSelect:true,
			collapsible:true,
			//对从服务器获取的数据进行解析格式化
 			dataFilter:function(data,type){
 				var rs = eval("("+data+")");
 				var modifyRows = [];
 				var rows = rs.rows;
 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});;
 				for(var i in rows){
 					var cols = tableContent.columns;
					var curRow = {};
 					for(var j in cols){
 						try{
 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase()) ;
 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
 							curRow[cols[j].columnName.toUpperCase()] = value;
 						}catch(e){
 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
 						}
 					}
 					modifyRows.push(curRow);
 				}
 				rs.rows = modifyRows;
 				return $.toJSON(rs);
 			},
			url:"<%=path%>/CgJiageYidong/findPurchasePriceChange.do",
			remoteSort: true,
			//页码选择项
			pageList:[10,20,30,40,50],
			frozenColumns:frozenHead,
			columns:head,
			queryParams:params,
			showFooter:true,
			rowStyler:function(){
				return 'line-height:11px';
			},
			pagination:true,
			rownumbers:true,
			onBeforeLoad:function(){
				if(!$("#firstLoad").val())
					return false;
			}
	 		});
	 		$("#firstLoad").val("true");
	 		
	 		$("#bdat,#edat").focus(function(){
	 			new WdatePicker();
	 		});
	 		
	 		
	 		$(".panel-tool").remove();

	 		// 请选择物资
	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
	 		//<fmt:message key="please_select_positions"/>
	 		$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('positn_name');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/searchAllPositn.do'),offset,$('#positn_name'),$('#positn'),'750','500','isNull');
				}
			});
	 	});
	 	
	 	//列选择
	 	function toColsChoose(){
	 		$('body').window({
			title: '<fmt:message key="column_selection"/>',
			content: '<iframe frameborder="0" src="<%=path%>/CgJiageYidong/toColChoosePurchasePriceChange.do"></iframe>',
			width: '460px',
			height: '430px',
			draggable: true,
			isModal: true
		});
	 	}
  	 </script>
</body>
</html>