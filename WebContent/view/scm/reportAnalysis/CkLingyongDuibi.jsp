<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>仓库领用对比</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		form .form-line .form-input input[type=text]{
			width: 90%;
		}
		form .form-line .form-input select{
			width: 92%;
		}
	</style>
</head>
<body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index:10;">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
			<div class="form-label" ><fmt:message key="positions"/></div>
			<div class="form-input">
				<select  name="positn" url="<%=path %>/positn/findAllPositnOut1.do"   class="select"></select>
				<%-- <input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
				<input type="hidden" id="positn" name="positn" value=""/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label"><fmt:message key="requisitioned_positions"/></div>
			<div class="form-input">
				<input type="text"  id="firm_name"  name="firm_name" readonly="readonly" value=""/>
				<input type="hidden" id="firm" name="firm" value=""/>
				<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
			</div>
			<div class="form-label" ><fmt:message key="coding"/></div>
			<div class="form-input" style="margin-top: -3px;">
				<input type="text" id="sp_code" name="sp_code" value="${sp_code}" class="text" />
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
			</div>
		</div>
		<div class="form-line" style="z-index:9;">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
			<div class="form-label" ><fmt:message key="bigClass"/></div>
			<div class="form-input">
				<select  name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
			</div>
			<div class="form-label" ><fmt:message key="middleClass"/></div>
			<div class="form-input">
				<select  name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select>
			</div>
			<div class="form-label" ><fmt:message key="smallClass"/></div>
			<div class="form-input" >
				<select  name="typ" url="<%=path %>/grpTyp/findAllTyp.do"  class="select"></select>
			</div>
		</div>
	</form>
 	<div id="datagrid"></div>
 	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
 	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  		$(document).ready(function(){
	 		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" />',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var form = $("#queryForm").find("*[name]");
						form = form.filter(function(index){
							var cur = form[index];
							if($(cur).attr("name")){
								if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
									if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
										if($("input[name='"+$(cur).attr("name")+"']:checked").length){
											params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
										}else{
											params[$(cur).attr("name")] = undefined;
										}
									}else{
										params[$(cur).attr("name")] = $(cur).val();
									}
								}
							}
							
						});
						getHeader(tableContent,columns,head,columnSecond,frozenColumns,tableHeight,frozenHead,params,numCols,dateCols);
// 						$("#datagrid").datagrid("load");
					}
				},{
					text: 'Excel',
					title: 'Excel',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-40px','-20px']
					},
					handler: function(){
						$('#queryForm').attr('action',"<%=path%>/CkLingyongDuibi/exportCangKuLingYongDuiBi.do");
						$('#queryForm').submit();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					useable:true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}
			]
		});
	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
	 		
  	 		$("#seachPositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'firm_name',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
	 		
			$("#bdat,#edat").htmlUtils("setDate","now");
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").not("#month,#positn1").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 	//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['amtin','amttotal','totalin','amtsuper','amtsuperrate','amtless','amtlessrate','amtjhw','ratejhw'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		var columnSecond = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		getHeader(tableContent,columns,columnSecond,frozenColumns,head,tableHeight,frozenHead,params,numCols,dateCols);
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/WlPeisongFenxi/toColChooseLogisticsAnalysis.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 	
  	 	function getHeader(tableContent,columns,columnSecond,frozenColumns,head,tableHeight,frozenHead,params,numCols,dateCols){
  	 		tableContent = {};
  	 		columns = [];
  	 		columnSecond = [];
  	 		head = [];
  	 		frozenHead = [];
  	 		frozenColumns = [];
  	 		$.ajax({url:"<%=path%>/CkLingyongDuibi/findWarehouseRequisitionedContrastHeader.do",
	 				async:false,
	 				data:{firm:$('#firm').val()},
	 				success:function(data){
	 					tableContent = data.columns;
	 				}
	 			});
  	 		frozenColumns.push({field:'SP_CODE',title:'<fmt:message key ="supplies_code" />',rowspan:2,width:65,align:'left'});
  	 		frozenColumns.push({field:'SP_NAME',title:'<fmt:message key ="supplies_name" />',rowspan:2,width:100,align:'left'});
  	 		frozenColumns.push({field:'SPDESC',title:'<fmt:message key ="supplies_specifications" />',rowspan:2,width:80,align:'left'});
  	 		frozenColumns.push({field:'UNIT',title:'<fmt:message key ="unit" />',rowspan:2,width:30,align:'left'});
		for(var i in tableContent){
	 		columns.push({colspan:2,title:tableContent[i].DES,align:'center'});
	 		columnSecond.push({field:tableContent[i].CODE+'CNT',title:'<fmt:message key ="quantity" />',width:60,align:'right'});
	 		columnSecond.push({field:tableContent[i].CODE+'AMT',title:'<fmt:message key ="amount" />',width:60,align:'right'});
	 	}
		head.push(columns);
		head.push(columnSecond);
	 	frozenHead.push(frozenColumns);

		//生成报表数据表格
	 		$("#datagrid").datagrid({
	 			title:'<fmt:message key ="scm_cklydb" />',
	 			width:'100%',
	 			height:tableHeight,
	 			nowrap: true,
			striped: true,
			singleSelect:true,
			collapsible:true,
			//对从服务器获取的数据进行解析格式化
 			dataFilter:function(data,type){
 				 var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var modifyFoot = [];
	 				var row = {};
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				var spcode = rows[0].SP_CODE;
	 				for(var i in rows){
	 					var code = rows[i].SP_CODE;
	 					if(code != spcode || !Number(i)){
	 						if(Number(i)) modifyRows.push(row);
	 						row = {};
	 						spcode=code;
	 						row['SP_CODE'] = rows[i].SP_CODE;
	 						row['SP_NAME'] = rows[i].SP_NAME;
	 						row['SPDESC'] = rows[i].SPDESC;
	 						row['UNIT'] = rows[i].UNIT;
	 					}
	 					row[rows[i].FIRM+'CNT'] = rows[i].CNT.toFixed(2);
	 					row[rows[i].FIRM+'AMT'] = rows[i].AMT.toFixed(2);
	 				}
	 				modifyRows.push(row);
	 				rs.rows = modifyRows;
	 				var footer = rs.footer;
	 				var foot = {};
	 				foot['SP_NAME'] = '总计';
	 				for(var j in footer){
	 					foot[footer[j].FIRM+'CNT'] = footer[j].CNT.toFixed(2);
	 					foot[footer[j].FIRM+'AMT'] = footer[j].AMT.toFixed(2);
	 				}
	 				modifyFoot.push(foot);
	 				rs.footer = modifyFoot;
	 				return $.toJSON(rs);
 			},
			url:"<%=path%>/CkLingyongDuibi/findWarehouseRequisitionedContrast.do",
			remoteSort: true,
			//页码选择项
			pageList:[10,20,30,40,50],
			frozenColumns:frozenHead,
			columns:head,
			queryParams:params,
			showFooter:true,
			rowStyler:function(){
				return 'line-height:11px';
			},
			pagination:true,
			rownumbers:true,
			onBeforeLoad:function(){
				if(!$("#firstLoad").val())
					return false;
			}
	 		});
	 		$("#firstLoad").val("true");
	 		
	 		$(".panel-tool").remove();
  	 	}
  	 </script>
</body>
</html>