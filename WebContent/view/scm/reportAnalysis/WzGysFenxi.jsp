<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>物资供应商分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16.5%;
		}
		.form-line .form-input input , .form-line .form-line select{
			width:80%;
		}
		
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label" ><fmt:message key="coding"/></div>
			<div class="form-input"  style="margin-top: -4px;">
				<input type="text" id="sp_code" name="spcode" class="text" />
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
			</div>
			<div class="form-input" ><input id="sp_name" style="border: 0;margin-top: 4px;" readonly="readonly"/></div>
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  		var tableHeight ;
		//收集form表单数据的对象
		var params = {};
		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
		var tableContent = {};
		//表头行（单行）
		var columns = [];
		var columnsSecond = [];
		//表头（多行），其中元素为columns
		var head = [];
		//需要固定在左侧的列的表头（单行）
		var frozenHead = [];
		//需要固定在左侧的列的表头（多行），元素为frozenHead
		var frozenColumns = [];
  	 	$(document).ready(function(){
  	 		
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							buildGrid();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							if(!$('#sp_code').val() && $("#firstLoad").val()){
								alert('<fmt:message key ="please_select_materials" />！');
								/* showMessage({
									type: 'error',
									msg: '请选择物资！',
									speed: 1500
								}); */
								return;
							}
// 							if($("#datagrid").val()){
// 								alert('无数据！');
// 								return;
// 							}
							$('#queryForm').attr('action',"<%=path%>/WzGysFenxi/exportSupplyDeliver.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});

  	  		tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  			
  	 		buildGrid();
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/SupplyAcct/toColumnsChoose.do?reportName=${reportName}"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 	function buildGrid(){
  	 		columns = [];
  			columnsSecond = [];
  			head = [];
  			frozenHead = [];
  			frozenColumns = [];
  			if($('#sp_code').val()){
  				//ajax获取报表表头
  	  	 		$.ajax({url:"<%=path %>/WzGysFenxi/findSupplyDeliverHeader.do",
  	  	 				async:false,
  	  	 				data:{spcode:$('#sp_code').val()},
  	  	 				success:function(data){
  	  	 					tableContent = data;
  	  	 				}
  	  	 			});
  			}
  	 		//解析获取的数据
 			frozenColumns.push({field:'YEAR',title:'<fmt:message key ="scm_year" />',width:70,align:'left',rowspan:2});
 			frozenColumns.push({field:'MONTH',title:'<fmt:message key ="months" />',width:70,align:'left',rowspan:2});
			for(var i in tableContent){
  	 			columns.push({colspan:5,title:tableContent[i].DES});
  	 			columnsSecond.push({field:tableContent[i].CODE+'INC',title:'<fmt:message key="Storage_quantity" />',width:80,align:'right'});
  	 			columnsSecond.push({field:tableContent[i].CODE+'INA',title:'<fmt:message key ="storage_amount" />',width:80,align:'right'});
  	 			columnsSecond.push({field:tableContent[i].CODE+'AVG',title:'<fmt:message key="Average_unit_price" />',width:80,align:'right'});
  	 			columnsSecond.push({field:tableContent[i].CODE+'MAX',title:'<fmt:message key="Maximum_unit_price" />',width:80,align:'right'});
  	 			columnsSecond.push({field:tableContent[i].CODE+'MIN',title:'<fmt:message key="Lowest_price" />',width:80,align:'right'});
  	 		}
			columns.push({colspan:5,title:'<fmt:message key ="total" />'});
 	 			columnsSecond.push({field:'TOTALINC',title:'<fmt:message key="Storage_quantity" />',width:80,align:'right'});
 	 			columnsSecond.push({field:'TOTALINA',title:'<fmt:message key ="storage_amount" />',width:80,align:'right'});
 	 			columnsSecond.push({field:'TOTALAVG',title:'<fmt:message key="Average_unit_price" />',width:80,align:'right'});
 	 			columnsSecond.push({field:'TOTALMAX',title:'<fmt:message key="Maximum_unit_price" />',width:80,align:'right'});
 	 			columnsSecond.push({field:'TOTALMIN',title:'<fmt:message key="Lowest_price" />',width:80,align:'right'});
			head.push(columns);
			head.push(columnsSecond);
			frozenHead.push(frozenColumns);
			if(!$('#sp_code').val() && $("#firstLoad").val()){
				alert('<fmt:message key ="please_select_materials" />！');
				return;
			}
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key ="scm_wzgysfx" />',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				var modifyRows = [];
	 				var month = rows[0].MONTHH;
	 				var row = {};
	 				for(var i in rows){
	 					var m = rows[i].MONTHH;
	 					if(month != m || !Number(i)){
	 						if(Number(i))modifyRows.push(row);
	 						row = {};
	 						month = m;
	 						row['YEAR'] = rows[i].YEARR;
		 					row['MONTH'] = m;
	 					}
	 						row[rows[i].DELIVER+'INC'] = rows[i].CNT.toFixed(2);
	 						row[rows[i].DELIVER+'INA'] = rows[i].AMT.toFixed(2);
	 						row[rows[i].DELIVER+'AVG'] = rows[i].PRICEAVG.toFixed(2);
	 						row[rows[i].DELIVER+'MAX'] = rows[i].PRICEMAX.toFixed(2);
	 						row[rows[i].DELIVER+'MIN'] = rows[i].PRICEMIN.toFixed(2);
	 				}
	 				modifyRows.push(row);
	 				rs.rows = modifyRows;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/WzGysFenxi/findSupplyDeliver.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		$(".panel-tool").remove();
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key ="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'),$('#sp_name'),$('<input/>'),$('<input/>'));	
				}
			});
  	 		//获取物资名称
  	 		function getName(){
				var name;
				$.ajaxSetup({ 
					  async: false 
					  });
				$.get("<%=path %>/supply/findById.do",
						{sp_code:$.trim($('#sp_code').val())},
						function(data){
							name =  data.sp_name;
						});
				return name;
			};
  	 	}
  	 </script>
  </body>
</html>
