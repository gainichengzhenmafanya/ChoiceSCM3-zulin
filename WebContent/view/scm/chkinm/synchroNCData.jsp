<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				.load {
					margin-bottom: 220px;
				}
			</style>
		</head>
	<body>
		<div class="msg">
			导入中...
		</div>
		<div class="load">
			<img alt="" src="<%=path%>/image/scm_load.gif" style="height: 200px;width: 250px;margin-top: -30px;margin-left: 50px;">
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(function(){
				var type = '${type}';
				if(type == 'chkinm'){
					$.ajax({
						type: "POST",
						url:'<%=path%>/chkinm/doSynData.do',
						data: "sp_code=",
						dataType: "json",
						success:function(data){
							var json = eval("(" + data + ")");
							if(json.state == 'success'){
								$(".msg").html('导出成功！共导入'+json.size+'条数据。');
							}else if(json.state == 'fail'){
								$(".msg").html("导入失败！");
							}else{
								$(".msg").html(json.state);
							}
							$(".load").hide();
						}
					});
				}else{
					$.ajax({
						type: "POST",
						url:'<%=path%>/chkinm/doNcBasicData.do',
						data: "sp_code=",
						dataType: "json",
						success:function(data){
							var json = eval("(" + data + ")");
							if(json.state == 'success'){
								$(".msg").html('同步成功！共导入'+json.size+'条数据。');
							}else if(json.state == 'fail'){
								$(".msg").html("同步失败！");
							}else{
								$(".msg").html(json.state);
							}
							$(".load").hide();
						}
					});
				}
			});	
		</script>
	</body>
</html>