<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>chkinm Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 60px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/chkinm/list.do" method="post">
			<input  type="hidden" name="checkOrNot" id="checkOrNot" value="${checkOrNot}"/>
			<input  type="hidden" name="inout" id="inout" value="${chkinm.inout}"/>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${chkinm.orderBy}" default="chkinno"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${chkinm.orderDes}" default="0000000000"/>" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" /></div>			
					<div class="form-label" ><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkinno" name="chkinno" class="text" value="${chkinm.chkinno}"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkinm.madeby}"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
						<input type="text"  id="deliver" name="deliver.des" readonly="readonly" value="${chkinm.deliver.des}"/>
						<input type="hidden" id="deliverCode" name="deliver.code" value="${chkinm.deliver.code}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key='query_suppliers'/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="<c:out value="${chkinm.vouno}" />"/></div>
					<div class="form-label"><fmt:message key="orders_audit"/></div>
					<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="<c:out value="${chkinm.checby}" />"/></div>
					<div class="form-label"><fmt:message key="storage_positions"/></div>
					<div class="form-input">
						<input type="text"  id="positn"  name="positn.des" readonly="readonly" value="${chkinm.positn.des}"/>
						<input type="hidden" id="positnCode" name="positn.code" value="${chkinm.positn.code}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<!-- add wangjie -->
<!-- 					<div class="form-label" style="width:100px;">分店:</div> -->
<!-- 					<div class="form-input" style="width:220px;"> -->
<%-- 						<input type="text"  id="firm"  name="firm" readonly="readonly" value="${chkinm.firm}"/> --%>
<%-- 						<input type="hidden" id="indept" name="indept" value="${chkinm.indept}"/> --%>
<%-- 						<img id="searchFendian" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="please_select_branche"/>' /> --%>
<!-- 					</div> -->
				</div>
		
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td><span style="width:20px;"></span>
									<!-- <input type="checkbox" id="chkAll"/></span> -->
								</td>
								<td><span style="width:70px;"><fmt:message key="warehousing_single_number"/></span></td>
								<td><span style="width:100px;"><fmt:message key="document_number"/></span></td>
								<td><span style="width:70px;"><fmt:message key="document_types"/></span></td>
								<td><span style="width:80px;"><fmt:message key="date_of_the_system_alone"/></span></td>
								<td><span style="width:130px;"><fmt:message key="time_of_the_system_alone"/></span></td>
								<td><span style="width:90px;"><fmt:message key="storage_positions"/></span></td>
								<td><span style="width:90px;"><fmt:message key="suppliers"/></span></td>
								<!-- wangjie 2014年11月27日 16:21:18 增加分店列 -->
<!-- 								<td><span style="width:90px;">分店</span></td> -->
								<td><span style="width:160px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:70px;"><fmt:message key="total_amount"/></span></td>
								<td><span style="width:70px;"><fmt:message key="scm_taxes_pre" /><fmt:message key="total_amount"/></span></td>
								<td><span style="width:70px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:70px;"><fmt:message key="orders_audit"/></span></td>
							</tr>
						</thead>
					</table>
				</div> 
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkinm" varStatus="step" items="${chkinmList}">
								<tr>
									<td class="num"><span  style="width:16px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_${chkinm.chkinno}" value="${chkinm.chkinno}"/></span>
									</td>
									<td><span title="${chkinm.chkinno}" style="width:70px;text-align: right;">${chkinm.chkinno}&nbsp;</span></td>
									<td><span title="${chkinm.vouno}" style="width:100px;">${chkinm.vouno}&nbsp;</span></td>
									<td><span title="${chkinm.typ}" style="width:70px;">${chkinm.typ}&nbsp;</span></td>
									<td><span title="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${chkinm.madet}" style="width:130px;">${chkinm.madet}&nbsp;</span></td>
									<td><span title="${chkinm.positn.des}" style="width:90px;">${chkinm.positn.des}&nbsp;</span></td>
									<td><span title="${chkinm.deliver.des}" style="width:90px;">${chkinm.deliver.des}&nbsp;</span></td>
<%-- 									<td><span title="${chkinm.firm}" style="width:90px;">${chkinm.firm}&nbsp;</span></td> --%>
									<td><span title="${chkinm.deliver.des}" style="width:160px;">${chkinm.deliver.des}&nbsp;</span></td>
									<td><span title="${chkinm.totalamt}" style="width:70px;text-align: right;"><fmt:formatNumber type="number" value="${chkinm.totalamt} " pattern="#.00"/>&nbsp;</span></td>
									<td><span title="${chkinm.noTaxTotalamt}" style="width:70px;text-align: right;"><fmt:formatNumber type="number" value="${chkinm.noTaxTotalamt} " pattern="#.00"/>&nbsp;</span></td>
									<td><span title="${chkinm.madeby}" style="width:70px;">${chkinm.madeby}&nbsp;</span></td>
									<td><span title="${chkinm.checby}" style="width:70px;">${chkinm.checby}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<form id="reportForm" method="post">
			<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			  invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="query_documents"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var maded = $('#listForm').find("#maded").val().toString();
							var madedEnd= $('#listForm').find("#madedEnd").val().toString();
							if(validate._submitValidate()){
								if(maded<=madedEnd)
								$('#listForm').submit();
								else
									alert("<fmt:message key="starttime"/><fmt:message key="not_less_than"/><fmt:message key="endtime"/>");
							}
						}
					},{
						text: '<fmt:message key="view" />',
						title: '<fmt:message key="view_the_document_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							toCheckedChkinm();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
					]
				});
				//排序start
				var array = new Array();      
				array = ['chkinno','vouno','typ', 'maded', 'madet', 'positn','deliver','totalamt','madeby','checby'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b);
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'chkinno',
						validateType:['canNull','intege'],
						param:['T','T'],
						error:['','<fmt:message key="single_number_is_digital_or_empty"/>']
					}]
				});
				//页面过滤供应商和仓位
				$('#deliver').bind('keyup',function(){
			          $("#deliverCode").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#positn').bind('keyup',function(){
			          $("#positnCode").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			    });
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			$('#autoId-button-103').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click();; break;
		                case 80: $('#autoId-button-102').click();; break;
		            }
				});
				
			 	//wangjie 选择分店
			 	$('#searchFendian').bind('click.custom',function(e){
		 			if(!!!top.customWindow){
						var defaultCode = $("#positn_id").val();
						var offset = getOffset('seachOnePositn');
						top.cust('<fmt:message key="please_select_branche"/>',encodeURI('<%=path%>/positn/searchAllPositn.do?defaultCode='+defaultCode)+'&mis=y&single=1',offset,$('#firm'),$('#indept'),'750','500','isNull',initValue);
			 		}
				});
			 	function initValue(code){
			 		//分店编码
					$("#indept").val(code);
			 	}
			 	
				$('#maded').bind('click',function(){
					new WdatePicker();
				});
				$('#madedEnd').bind('click',function(){
					new WdatePicker();
				});
				//双击事件
				$('.grid .table-body tr').live('dblclick',function(){
					$(":checkbox").attr("checked", false);
					$(this).find(":checkbox").attr("checked", true);
					toCheckedChkinm();
				});
				setElementHeight('.grid',['.tool'],$(document.body),102);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positnCode').val();
						var defaultName = $('#positn').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&typ=123&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn'),$('#positnCode'),'1000','500','isNull');
					}
				});
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliver').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliver'),$('#deliverCode'),'900','500','isNull');
					}
				});
				
				//判断是直发单 还是 入库单   直发单的话  显示分店列,入库单 则不显示 wangjie
				var inout = $("#inout").val();
				if(inout == 'in'){
					//隐藏查询条件
					$("body").find(".form-line:eq(1)").find("div:eq(8)").hide();
					$("body").find(".form-line:eq(1)").find("div:eq(9)").hide();
					//隐藏table列
					$(".table-head").find("tr").find("td:eq(8)").hide();
					$(".table-body").find("tr").find("td:eq(8)").hide();
				}
			});	
				function findPositnByDes(inputObj){
					$('#positn').val($(inputObj).val());
		        }
				function findDeliverByDes(inputObj){
					$('#deliver').val($(inputObj).val());
		        }
				var detailWin;
				//跳转到详细信息页面
				function toCheckedChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
						var chkValue = checkboxList.filter(':checked').val();
						if ($('#inout').val()=="inout"){
							detailWin = $('body').window({
								id: 'checkedChkinmDetail',
								title: '<fmt:message key ="select" /><fmt:message key ="checked" /><fmt:message key ="Inout_order_detail" />',
								content: '<iframe id="checkedChkinmDetailFrame" frameborder="0" src="<%=path%>/chkinm/update.do?chkinno='+chkValue+'&isCheck=isCheck&inout=zf"></iframe>',
								width: $(document.body).width()-20/* '1050px' */,
								height: '460px',
								draggable: true,
								isModal: true,
								confirmClose:false
							});
						}else {
							detailWin = $('body').window({
								id: 'checkedChkinmDetail',
								title: '<fmt:message key ="select" /><fmt:message key ="checked" /><fmt:message key ="In_order_detail" />',
								content: '<iframe id="checkedChkinmDetailFrame" frameborder="0" src="<%=path%>/chkinm/update.do?chkinno='+chkValue+'&isCheck=isCheck"></iframe>',
								width: $(document.body).width()-20/* '1050px' */,
								height: '460px',
								draggable: true,
								isModal: true,
								confirmClose:false
							});
						}
						
					}else{
						alert('<fmt:message key="please_select_data_to_view"/>！');
						return ;
					}
				}

				//打印
				function printChkinm(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
							var chkValue = [];
							
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
							});
							$('#reportForm').find('#chkinno').attr('value',chkValue.join(","));
							$('#reportForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
							var action = '<%=path%>/chkinm/viewChkinm.do';	
							$('#reportForm').attr('action',action);
							$('#reportForm').submit();
					}else{
						alert('<fmt:message key="please_select_single_message_to_print"/>！');
						return ;
					}
				}
				
				/**2014.6.28 wjf 修改checkbox为单选**/
				$("input[name='idList']").bind('click',function(){
					$("input[name='idList']").each(function(){
						$(this).attr("checked",false);
					});
					$(this).attr("checked",true);
				});
		</script>
	</body>
</html>