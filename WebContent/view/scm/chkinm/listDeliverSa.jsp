<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应商结算</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 200px;}
		.search{
			margin-top:-2px;
			cursor: pointer;
		}
		.form-line .form-label{
			width:80px;
		}
		#littleWin{
			width:100%;
			height:160px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkinm/deliverSaList.do" method="post" target="">
			<div class="bj_head">
				<div class="form-line">	
					<div class="form-label"><fmt:message key="startdate"/>：</div>
					<div class="form-input"><input type="text" id="maded" name="maded" value="<fmt:formatDate value="${Chkinm.maded}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'madedEnd\')}'});"/></div>		
					
					<div class="form-label"><fmt:message key="positions"/>：</div>
					<div class="form-input">
						<input type="text"  id="firmDes"  name="positn.des" readonly="readonly" value="${Chkinm.positn.des}"/>
						<input type="hidden" id="firmCode" name="positn.code" value="${Chkinm.positn.code}"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
					
					<div class="form-label"><fmt:message key="document_types"/>：</div>
					<div class="form-input">
						<select id="typ" name="typ" class="select">
							<option value=""></option>
							<option value="9900" <c:if test="${Chkinm.typ eq '9900'}"> selected="selected" </c:if>><fmt:message key="normal_storage"/></option>
							<option value="9916" <c:if test="${Chkinm.typ eq '9916'}"> selected="selected" </c:if>><fmt:message key="normal_shipments"/></option>
<%-- 							<option value="9917" <c:if test="${Chkinm.typ eq '9917'}"> selected="selected" </c:if>><fmt:message key="gifts_shipments"/></option> --%>
<%-- 							<option value="9918" <c:if test="${Chkinm.typ eq '9918'}"> selected="selected" </c:if>><fmt:message key="consignment_shipments"/></option> --%>
							<option value="9906" <c:if test="${Chkinm.typ eq '9906'}"> selected="selected" </c:if>><fmt:message key="storage"/><fmt:message key="returns"/></option>
							<option value="9907" <c:if test="${Chkinm.typ eq '9907'}"> selected="selected" </c:if>><fmt:message key="storage"/><fmt:message key="reversal"/></option>
							<option value="9919" <c:if test="${Chkinm.typ eq '9919'}"> selected="selected" </c:if>><fmt:message key="straight_hair"/><fmt:message key="returns"/></option>
							<option value="9920" <c:if test="${Chkinm.typ eq '9920'}"> selected="selected" </c:if>><fmt:message key="straight_hair"/><fmt:message key="reversal"/></option>
							<option value="9923" <c:if test="${Chkinm.typ eq '9923'}"> selected="selected" </c:if>><fmt:message key="open_at_the_beginning_of_the_period"/></option>
<%-- 							<option value="退货" <c:if test="${Chkinm.typ eq '退货'}"> selected="selected" </c:if>><fmt:message key="returns"/></option> --%>
<%-- 							<option value="冲消" <c:if test="${Chkinm.typ eq '冲消'}"> selected="selected" </c:if>><fmt:message key="reversal"/></option> --%>
						</select>
					</div>
					<div class="form-label"><fmt:message key="clearing"/>：</div>
					<div class="form-input">
					     <input type="radio" name="folio" value="" <c:if test="${Chkinm.folio eq null}"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="folio" value="0" <c:if test="${Chkinm.folio eq 0}"> checked="checked" </c:if>/><fmt:message key="outstanding"/>
					     <input type="radio" name="folio" value="1" <c:if test="${Chkinm.folio eq 1}"> checked="checked" </c:if>/><fmt:message key="closed"/>
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="enddate"/>：</div>
					<div class="form-input"><input type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'maded\')}'});"/></div>	
				
					<div class="form-label"><span style="color:red;font-size:1.2em;">*</span><fmt:message key="suppliers"/>：</div>
					<div class="form-input" style="width:170px;">
						<input type="text" id="deliverDes" name="deliver.des" readonly="readonly" value="${Chkinm.deliver.des}"/>
						<input type="hidden" id="deliverCode" name="deliver.code" value="${Chkinm.deliver.code}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					
					<div class="form-label" style="width:20px;"></div>
					<div class="form-input">
					     <input type="radio" name="inout" value="" <c:if test="${Chkinm.inout eq null or Chkinm.inout eq '' }"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="inout" value="in" <c:if test="${Chkinm.inout eq 'in'}"> checked="checked" </c:if>/><fmt:message key="only_storage"/>
					     <input type="radio" name="inout" value="inout" <c:if test="${Chkinm.inout eq 'inout'}"> checked="checked" </c:if>/><fmt:message key="only_straight_hair"/>
					</div>
					
					<div class="form-label" style="width:120px;"><fmt:message key ="payments" />：</div>
					<div class="form-input">
					     <input type="radio" name="bill" value="" <c:if test="${Chkinm.bill eq null}"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="bill" value="0" <c:if test="${Chkinm.bill eq '0'}"> checked="checked" </c:if>/><fmt:message key="Not_paying"/>
					     <input type="radio" name="bill" value="1" <c:if test="${Chkinm.bill eq '1'}"> checked="checked" </c:if>/><fmt:message key="Payment_has_been"/>
					</div>
					
					<div class="form-label" style="width:20px"></div>
					<div class="form-input" style="width:80px;">
					     <input type="checkbox" name="pay" value="-1.00" <c:if test="${Chkinm.pay eq -1.00}"> checked="checked" </c:if>/><fmt:message key ="partial_payment" />
					</div>
				</div>
			</div>
		    <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2" ><span style="width: 25px;"><input type="checkbox" id="chkAll" value=""/></span></td>
								<td rowspan="2" ><span style="width:80px;"><fmt:message key="suppliers"/><fmt:message key="coding"/></span></td>
								<td rowspan="2" ><span style="width:130px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="storage_amount"/></span></td>
								<td rowspan="2" ><span style="width:120px;"><fmt:message key="warehousing_certificate_number"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="document_types"/></span></td>
								<td colspan="5" ><fmt:message key="clearing"/></td>
							</tr>
							<tr>
								<td ><span style="width:80px;"><fmt:message key="Payment_has_been"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="Not_paying"/></span></td>
								<td style="display: none;"><span style="width:80px;"><fmt:message key="This_payment"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="Check_now"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="chkinm" items="${listChkinm }" varStatus="status">
								<tr>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
									</td>
									<td><span style="width:25px;text-align:center;"><input type="checkbox" name="idList" id="chk_${chkinm.chkinno}" value="${chkinm.chkinno}"/></span></td>
									<td title="${chkinm.deliver.code}"><span style="width:80px;">${chkinm.deliver.code }</span></td>
									<td title="${chkinm.deliver.des }"><span style="width:130px;">${chkinm.deliver.des }</span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.totalamt }" type="currency" pattern="0.00"/>"><span style="width:100px;"><fmt:formatNumber value="${chkinm.totalamt }" type="currency" pattern="0.00"/></span></td>
									<td title="${chkinm.vouno }"><span style="width:120px;">${chkinm.vouno }</span></td>
									<td title="${chkinm.typ }"><span style="width:100px;">${chkinm.typ }</span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${chkinm.pay}" type="currency" pattern="0.00"/></span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.totalamt-chkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="${chkinm.totalamt-chkinm.pay}" type="currency" pattern="0.00"/></span></td>
									<td style="text-align: right;display: none;" title="<fmt:formatNumber value="0" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="0" type="currency" pattern="0.00"/></span></td>
									<td>
										<span style="width:80px;text-align:center" title="${chkinm.folio}">
											<c:choose>
												<c:when test="${chkinm.folio==1 }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="table-foot" style="border-top: 1px solid #999;">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<tr>
								<td style="border: 0px;" class="num" >
									<span style="width: 25px;"></span>
								</td>
								<td style="border: 0px;"><span style="width:25px;text-align:center;"></span></td>
								<td style="border: 0px;" title="${totalChkinm.deliver.code}"><span style="width:80px;">${totalChkinm.deliver.code }</span></td>
								<td style="border: 0px;"><span style="width:130px;"></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.totalamt }" type="currency" pattern="0.00"/>"><span style="width:100px;"><fmt:formatNumber value="${totalChkinm.totalamt }" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;"><span style="width:120px;"></span></td>
								<td style="border: 0px;"><span style="width:100px;"></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${totalChkinm.pay}" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.totalamt-totalChkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="${totalChkinm.totalamt-totalChkinm.pay}" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;display: none;"><span style="width:80px;text-align:right"><fmt:formatNumber value="0" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;"><span style="width:80px;text-align:center"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<div id="littleWin">
			<iframe style="width:49%;margin-top:20px;border-right:1px solid #999999;" id="payFrame" src="" frameborder="0" ></iframe>
			<iframe style="width:50%;margin-top:20px;" id="billFrame" src="" frameborder="0" ></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('#gysjs_quit').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 65: $('#autoId-button-102').click(); break;
	                case 80: $('#autoId-button-103').click(); break;
	            }
			}); 
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                if ($(this).hasClass("bgBlue")) {
                    $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
                }else{
                    $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
                }
            });
            $('.grid').find('.table-body').find('tr').live('dblclick',function(){
                var chk = $(this).find(':checkbox');
                var tag=$(this).find("td:eq(5) span").text();
                if(tag.indexOf("RK")>=0){
                    tag=1;
                }else{
                    tag=2
                }
                var parameters = {"vouno":$(this).find("td:eq(5)").find("span").text(),"chktag":tag,
                    "chkinno":chk.val(),"dat":$("#madedEnd").val(),"maded":$("#maded").val()};
                openTag("CkMingxiChaxun", tag==2?"<fmt:message key ="Inout_order_detail" />":"<fmt:message key ="query_storage_lists" />", "<%=path%>/RkMingxiChaxun/toRkBill.do", parameters);
            });
			setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="select"/>',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								if(!$("#deliverCode").val()){
									alert('<fmt:message key="the_supplier_is_required"/>！');
								}else{
									$('#listForm').submit();									
								}
							}
						},{
							text: '<fmt:message key="add"/> <fmt:message key="beginning_of_period"/>',
							title: '<fmt:message key="add"/> <fmt:message key="beginning_of_period"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								addStartMoney();
							}
						},'-',{
							text: '<fmt:message key ="payments" />',
							title: '<fmt:message key ="payments" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								payMoney();
							}
						},{
							text: '<fmt:message key ="audit_check" />',
							title: '<fmt:message key ="audit_check" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								checkBill();
							}
						},{
							text: '<fmt:message key="invoice"/>',
							title: '<fmt:message key="invoice"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								addBill();
							}
						},'-',{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
                            id:'gysjs_quit',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
				
			});
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool','#littleWin'],$(document.body),130);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliverDes').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
					}
				});
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#typCode').val();
						var defaultName = $('#typDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					}
				});
				
				$("#seachPositn").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#firmCode").val(),
						single:false,
						tagName:'firmDes',
						tagId:'firmCode',
						title:'<fmt:message key="positions"/>'
					});
				});
				
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
					}
				});
			
				$('#seachOnePositn1').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#firmCode').val();
						var defaultName = $('#firmDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_branche"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
					}
				});
				//加载付款和发票数据
				loadFrameData();
		});
		//加载付款记录和发票数据
		function loadFrameData(){
			var str = $('#listForm').serialize();
			var payAction = "<%=path%>/chkinm/findPayData.do?"+str;
			var billAction = "<%=path%>/chkinm/findBillData.do?"+str;
			$("#payFrame").attr("src",payAction);
			$("#billFrame").attr("src",billAction);
		}
		//审核结账
		function checkBill(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key ="To_determine_the_audit_check" />?')){
					var chkValue = [];
					var vouno = '';
	                var flag = true;
					$.each(checkboxList.filter(':checked'),function(i){
						if('1' == $(this).parents("tr").find("td:eq(10)").find("span").attr('title')){
	                		vouno = $(this).parents("tr").find("td:eq(5)").find("span").text();
	                		flag = false;
	                		return false;
	                	}
						chkValue.push($(this).val());
					});
					if(!flag){
	                	alert('<fmt:message key ="document_number" />:['+vouno+']<fmt:message key ="te_data_have_been_audited" />!');
	                	return;
	                }
					detailWin = $('body').window({
						id: 'checkedChkstom',
						title: '<fmt:message key ="audit_check" />',
						content: '<iframe id="checkedChkstomFrame" frameborder="0" src="<%=path%>/chkinm/checkedBill.do?chkinno='+chkValue.join(',')+'"></iframe>',
						width: '400px',
						height: '250px',
						draggable: true,
						isModal: true
					});
				}
			}else{
				alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
				return ;
			}
		}
		//付款
		function payMoney(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
                var chkValue = [];
                var chkMoney=0;
                var vouno = '';
                var flag = true;
                $.each(checkboxList.filter(':checked'),function(i){
                	if('1' == $(this).parents("tr").find("td:eq(10)").find("span").attr('title')){
                		vouno = $(this).parents("tr").find("td:eq(5)").find("span").text();
                		flag = false;
                		return false;
                	}
                    chkValue.push($(this).val());
                    var money=$.trim($(this).parents("tr").find("td:eq(8)").find("span").text());
                    chkMoney+=isNaN(money)?0:Number(money);
                });
                if(!flag){
                	alert('<fmt:message key ="document_number" />:['+vouno+']<fmt:message key ="te_data_have_been_audited" />!');
                	return;
                }
                detailWin = $('body').window({
                    id: 'payMoney',
                    title: '<fmt:message key ="Add_clearing_record" />',
                    content: '<iframe id="payMoneyFrame" frameborder="0" src="<%=path%>/chkinm/toPayMoney.do?chkinno='+chkValue.join(',')+'&money='+chkMoney+'"></iframe>',
                    width: '400px',
                    height: '280px',
                    draggable: true,
                    isModal: true
                });
			}else{
				alert('<fmt:message key="please_select_data_need_to_be_operated"/>！');
				return ;
			}
		}
		//添加发票记录
		function addBill(){
			var deliverCode = $('#deliverCode').val();
			if(deliverCode == '' || deliverCode == 'undefined'){
				alert('<fmt:message key="the_supplier_is_required"/>!');
				return;
			}
			billWin = $('body').window({
				id: 'addBill',
				title: '<fmt:message key="add"/><fmt:message key="invoice"/>',
				content: '<iframe id="addBillFrame" frameborder="0" src="<%=path%>/chkinm/toAddBill.do?deliverCode='+deliverCode+'"></iframe>',
				width: $(document.body).width()-20,
				height: $(document.body).height()-20,
				draggable: true,
				isModal: true
			});
		
		}
		//添加期初未结金额
		function addStartMoney(){
			var deliverCode = $('#deliverCode').val();
			if(deliverCode == '' || deliverCode == 'undefined'){
				alert('<fmt:message key="the_supplier_is_required"/>！');
				return;
			}
			billWin = $('body').window({
				id: 'addDeliverMoney',
				title: '<fmt:message key ="add_the_initial_amount_of_supplier" />',
				content: '<iframe id="addDeliverMoneyFrame" frameborder="0" src="<%=path%>/chkinm/toAddDeliverMoney.do?deliverCode='+deliverCode+'"></iframe>',
				width: '900px',
				height: '480px',
				draggable: true,
				isModal: true
			});
		
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		</script>
	</body>
</html>