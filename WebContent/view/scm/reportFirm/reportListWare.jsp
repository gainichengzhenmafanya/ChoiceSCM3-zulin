<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<title>分店仓储报表</title>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width:auto;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
  	</style>
  </head>	
  <body>
  <div style="overflow-x:auto;width: 100%">
  	<input  type="hidden" name="scm_project" id="scm_project" value="${scm_project}" />
  	<div style="width: 630px;">
  		<a href="<%=path %>/RkMingxiChaxunFirm/toChkinDetailS.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_query"/></a>
  		<a href="<%=path %>/RkHuizongChaxunFirm/toChkinSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_aggregate_query"/></a>
  		<c:if test="${scm_project != 'wfz'}">
	  		<a href="<%=path %>/RkMingxiHuizongFirm/toChkinDetailSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_summary"/></a>
	  		<a href="<%=path %>/rkLeibieHuizongFirm/toChkinCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_category_summary"/></a>
<%-- 	  		<a href="<%=path %>/RkZongheChaxunFirm/toChkinmSynQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_comprehensive_inquiry"/></a> --%>
  		</c:if> 
  	</div>
  	<div style="width: 760px;">
  		<a href="<%=path %>/CkMingxiChaxunFirm/toChkoutDetailQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="details_of_the_library_inquiry"/></a>
  		<a href="<%=path %>/CkHuizongChaxunFirm/toChkoutSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="aggregate_query_library"/></a> 
  		<c:if test="${scm_project != 'wfz'}">
  		    <a href="<%=path %>/CkMingxiHuizongFirm/toChkoutDetailSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_Detail_Summary"/></a> 
<%--        <a href="<%=path %>/CkYingliChaxunFirm/toChkoutProfitQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_profitability_query"/></a> --%>
<%--   		<a href="<%=path %>/CkRiqiHuizongFirm/toChkoutDateSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/></a> --%><%--   		<a href="<%=path %>/CkRiqiHuizongFirm/toChkoutDateSum1.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/>1</a> --%>
<%--   		<a href="<%=path %>/CkRiqiHuizongFirm/toChkoutDateSum2.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/>2</a> --%>
		</c:if>
  	</div>
<!--   	<div style="width: 890px;"> -->
<%--   		<a href="<%=path %>/CkLeibieHuizong/toChkoutCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="out_library_category_summary"/></a> --%>
<%--   		<a href="<%=path %>/LeibieFendianHuizong/toCategoryPositnSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="class_stores_summary"/></a> --%>
<%--   		<a href="<%=path %>/FendianLeibieHuizong/toPositnCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="bnranch_category_are_summarized"/></a> --%>
<%--   		<a href="<%=path %>/CkDanjuHuizong/toChkoutOrderSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="storehouse_data_summary"/></a> --%>
<%--   		<a href="<%=path %>/PsYanhuoChaxun/toDeliveryExamineQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="delivery_inspection_queries"/></a> --%>
  		<a href="<%=path %>/DbMingxiChaxunFirm/toChkAllotDetailQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="allocate_detail_query"/></a>
<%--   		<a href="<%=path %>/DbHuizongChaxunFirm/toChkAllotSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="allocation_of_aggregate_query"/></a> --%>
<!--   	</div> -->
<!--   	<div style="width: 500px;"> -->
<%--   		<a href="<%=path %>/GysJinhuoHuizongFirm/toDeliverStockSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="suppliers_purchase_summary"/></a> --%>
<%--   		<a href="<%=path %>/LeibieGysHuizongFirm/toCategoryDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/></a> --%>
<%--   		<a href="<%=path %>/LeibieGysHuizongFirm/toCategoryDeliverSum1.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/>1</a> --%>
<%--   		<a href="<%=path %>/GysFendianHuizongFirm/toDeliverPositnSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="vendor_stores_summary"/></a> --%>
<!--   	</div> -->
<!--   	<div style="width: 500px;"> -->
<%--   		<a href="<%=path %>/GysLeibieHuizong/toDeliverCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="supplier_category_summary"/></a> --%>
<%--   		<a href="<%=path %>/GysHuizongLiebiao/toDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="summary_list_of_suppliers"/></a> --%>
<%--   		<a href="<%=path %>/JhDanjuHuizong/toStockBillSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="purchase_invoices_summary"/></a> --%>
<%--   		<a href="<%=path %>/GysFukuanQingkuang/toDeliverPayment.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="vendor_payments_situation"/></a> --%>
<!--   	</div> -->
<!--   	<div style="width: 890px;"> -->
<%--   		<a href="<%=path %>/WzMingxiZhang/toSupplyDetailsInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_ledger"/></a> --%>
<%--   		<a href="<%=path %>/WzYueChaxun/toSupplyBalance.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_balance_inquiry"/></a> --%>
  		<a href="<%=path %>/WzMingxiJinchuFirm/toSupplyInOutInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_detail_out_of_the_table"/></a>
<%--   		<a href="<%=path %>/WzLeibieJinchubiaoFirm/toSupplyTypInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_category_out_of_the_table"/></a> --%>
<%--   		<a href="<%=path %>/WzZongheJinchubiaoFirm/toSupplySumInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_integrated_out_of_the_table"/></a> --%>
<%--   		<a href="<%=path %>/WzCangkuJinchubiaoFirm/toGoodsStoreInout.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="materials_warehouse_and_out_of_form"/></a> --%>
<%--   		<a href="<%=path %>/CangkuLeibieJinchu/toStockCategoryInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehouse_category_out_of_the_table"/></a> --%>
<!--   	</div> -->
<!-- 	<div style="width: 250px;"> -->
<%--   		<a href="<%=path %>/shipAndReceive/list.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">收发货报表</a> --%>
<!--   	</div> -->
<%-- 		<a href="<%=path %>/DbHuizong/toDbHuizong.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">调拨汇总表</a> --%>
	</div>
  	<div style="width: 250px;">
  		<a href="<%=path %>/shipAndReceive/list.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_sfhbb"/></a>
  		<a href="<%=path %>/intransit/list.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_ztqd"/></a>
<%--   		<a href="<%=path %>/bohfirmsupply/list.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">物料盘存表</a> --%>
  	</div>

  	<div style="width: 890px;">
<%--   		<a href="<%=path %>/CunhuoPandian/toCunhuoPandian.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">存货盘点表</a> --%>
<%--   		<a href="<%=path %>/YuemoPandian/toYuemoPandian.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">月末盘点报表</a> --%>
<%--   		<a href="<%=path %>/ChayiGuanli/toChayiGuanli.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">差异管理</a> --%>
<%--   		<a href="<%=path %>/MeiriChayiDuizhao/toMeiriChayiDuizhao.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">每日差异对照</a> --%>
<%--   		<a href="<%=path %>/CunhuoHuizong/toCunhuoHuizong.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">存货汇总表</a> --%>
<%--   		<a href="<%=path %>/LishiPandianBiao/toLishiPandian.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">历史盘点报表</a> --%>
  	</div>
  	<div style="width: 890px;">
<%--   		<a href="<%=path %>/SecondUnit/toSecondUnit.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">第二单位查询</a> --%>
<%--   		<a href="<%=path %>/SecondUnit/toSecondUnitCy.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">第二单位查询-差异</a> --%>
  	</div>
  	</div>
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  		$('a').click(function(event){
  			var self = $(this);
  			if(!self.attr("tabid")){
  				self.attr("tabid",self.text().replace(/[ ]/g,"")+new Date().getTime());
  			}
  			event.preventDefault();
  			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
  		});
  	</script>
  </body>
</html>
