<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6%;
		}
		form .form-line .form-input{
			width: 17%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width: 80%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
			
			<div class="form-label"><fmt:message key="bigClass"/></div>
			<div class="form-input">
				<select  name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" name="grptyp" code="${supplyAcct.grptyp}" des="${supplyAcct.grptypdes}" class="select"></select>
				<input type="hidden" id="grptypcode" value="${supplyAcct.grptyp}"/>
			</div>
			<div class="form-label" ><fmt:message key="middleClass"/></div>
			<div class="form-input">
				<select  name="grp" url="<%=path %>/grpTyp/findAllGrp.do" name="grp" code="${supplyAcct.grp}" des="${supplyAcct.grpdes}" class="select"></select>
			</div>
			<div class="form-label" ><fmt:message key="smallClass"/></div>
			<div class="form-input" >
				<select  name="typ" url="<%=path %>/grpTyp/findAllTyp.do" name="typ" code="${supplyAcct.typ}" des="${supplyAcct.typdes}" class="select"></select>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
			<div class="form-label"><fmt:message key="coding"/></div>
			<div class="form-input">
				<input type="text" name="sp_code" id="sp_code" autocomplete="off" class="text" value="<c:out value="${chkoutm.chkoutno}" />"/>
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />	
			</div>
			<div class="form-label"><fmt:message key ="branche" /></div>
			<div class="form-input">
				<input type="text"  id="firmdes"  name="firmdes" readonly="readonly" value="${supplyAcct.firmdes}"/>
				<input type="hidden" id="firm" name="firm" value="${supplyAcct.firm}"/>
				<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
			</div>
			<div class="form-label" style="padding-left: 50px;">
				<input type="radio" name="querytype" checked="checked" value="1"/><fmt:message key ="supplies" />
				<input type="radio" name="querytype" value="2"/><fmt:message key ="date" />
			</div>
			
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
 	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height()-23;
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var firm = $("#queryForm").find("#firm").val();
							
							var bdat = $("#queryForm").find("#bdat").val().toString();
							var edat = $("#queryForm").find("#edat").val().toString();
							if(bdat<=edat){
								if(firm==''||firm==null){
									alert("<fmt:message key ="please_choose_store" />！");
								}else{
									var form = $("#queryForm").find("*[name]").filter(function(){
										return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val();
									});
									var mul = ['radio','checkbox'];
									var temp = {};
									var param = {};
									form.each(function(){
										this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();
									});
									for(var i in temp){
										$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()
												: param[i] = form.filter('input[name="'+i+'"]:checked').val();
									}
									params = param;
									head=[];//数字列
						  	 		var numCols = ['amtout','amt','price'];
									columns=[];
									$.ajax({url:"<%=path%>/SecondUnit/findSecondUnitHeaders.do?querytype="+$(":radio[name='querytype']:checked").val(),
					  	 				async:false,
					  	 				success:function(data){
					  	 					tableContent = data;
					  	 				}
					  	 			});
						  	 		var Cols = [];
						  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
										for(var i in tableContent.columns){
											var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
											columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});	
							  	 		}
										head.push(columns);
						  	 		//生成报表数据表格
						  	 		$("#datagrid").datagrid({
						  	 			title:'<fmt:message key="Second_unit_query" />',
						  	 			width:'100%',
						  	 			height:tableHeight,
						  	 			nowrap: true,
										striped: true,
										singleSelect:true,
										collapsible:true,
										//对从服务器获取的数据进行解析格式化
							 			loadFilter:function(data){
							 				if(dateCols.length <= 0)return data;
							 				var rows = data.rows;
							 				if(!rows || rows.length <= 0){
							 					var item = $('#datagrid').datagrid('getRows');
							 		            if (item) {
							 		                for (var i = item.length - 1; i >= 0; i--) {
							 		                    var index = $('#datagrid').datagrid('getRowIndex', item[i]);
							 		                    $('#datagrid').datagrid('deleteRow', index);
							 		                }
							 		            }
							 		           $('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
							 				}
							 				for(var i in rows){
							 					for(var j in dateCols){
							 						rows[i][dateCols[j]] = convertDate(rows[i][dateCols[j]]);
							 					}
							 				}
							 				return data;
							 			},
										url:"<%=path%>/SecondUnit/findSecondUnit.do",
										remoteSort: true,
										//页码选择项
										pageList:[10,20,30,40,50],
										columns:head,
										queryParams:params,
										showFooter:true,
										pagination:true,
										rownumbers:true
						  	 		});
								}
							}else{
							    alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/SecondUnit/exportSecondUnit.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/SecondUnit/printSecondUnit.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});

  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);

  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['DAT'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/SecondUnit/findSecondUnitHeaders.do?querytype="+$(":radio[name='querytype']:checked").val(),
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		var Cols = [];
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
				for(var i in tableContent.columns){
					columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:'center'});	
	  	 		}
				head.push(columns);
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="warehousing_comprehensive_inquiry"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			loadFilter:function(data){
	 				if(dateCols.length <= 0)return data;
	 				var rows = data.rows;
	 				for(var i in rows){
	 					for(var j in dateCols){
	 						rows[i][dateCols[j]] = convertDate(rows[i][dateCols[j]]);
	 					}
	 				}
	 				return data;
	 			},
				url:"<%=path%>/SecondUnit/findSecondUnit.do?querytype="+$("input[name='querytype']").val(),
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				columns:head,
				queryParams:params,
				showFooter:true,
				pagination:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		
  	 		$("#seachPositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'firmdes',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		
	 		$("select").each(function(){
				$(this).htmlUtils("select");
			});
  	 	});
  	 </script>
  </body>
</html>
