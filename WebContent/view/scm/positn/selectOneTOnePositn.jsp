<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<style type="text/css"> 
		.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/positn/selectPositnByTyp.do?mold=oneTone" method="post">
		<input type="hidden" name="mis" value="${mis}"></input>
		<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="coding" />:</b></font>
			            <input type="text" id="code" name="code" style="width: 100px;" class="text" value="<c:out value="${queryPositn.code}" />" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="name" />:</b></font>
			            <input type="text" id="des" name="des" style="width: 100px;" class="text" value="<c:out value="${queryPositn.des}" />" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="abbreviation" />:</b></font>
			            <input type="text" id="init" name="init" style="width: 100px;" class="text" value="<c:out value="${queryPositn.init}" />" style="text-transform:uppercase;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td>&nbsp;
			        	<input type="hidden" name="typ" id="typ" value="${queryPositn.typ }" />
			        </td>
			        <td>&nbsp;
			        	<input type="button" id="search" style="width: 60px;" name="search" value='<fmt:message key="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;"><span class="num" style="width: 25px;"></span></td>
								<td style="width:30px;">
									&nbsp;
								</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:180px;"><fmt:message key="name" /></td>
								<td style="width:80px;"><fmt:message key="type" /></td>
								<td style="width:80px;"><fmt:message key="abbreviation" /></td>
								<td style="width:80px;"><fmt:message key="for_short" /></td>
								<td style="width:60px;"><fmt:message key="area" /></td>
								<td style="width:100px;"><fmt:message key="business_group" /></td>
								<td style="width:100px;"><fmt:message key="distribution_area" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" varStatus="status" items="${listPositn}">
								<tr>
									<td class="num" style="width:30px;"><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
									<span class="num" style="width: 20px;"><input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.code}"/></span>
									</td>
									<td><span title="${positn.code}" style="width:40px;text-align: left;"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:170px;text-align: left;"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span title="${positn.typ}" style="width:70px;text-align: left;"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span title="${positn.init}" style="width:70px;text-align: left;"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span title="${positn.des1}" style="width:70px;text-align: left;"><c:out value="${positn.des1}"/>&nbsp;</span></td>
									<td><span title="${positn.area}" style="width:50px;text-align: left;"><c:out value="${positn.area}"/>&nbsp;</span></td>
									<td><span title="${positn.mod2}" style="width:90px;text-align: left;"><c:out value="${positn.mod2}" />&nbsp;</span></td>
									<td><span title="${positn.psarea}" style="width:90px;text-align: left;"><c:out value="${positn.psarea}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value="${parentId}"/>
			<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				});
				$('#search').bind('click',function(e){
					$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" /><fmt:message key="position_select" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Positn();
								//alert($('#parentId').val());
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
/* 				var str=$('#parentId').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++)
				{ 
					$('#chk_'+strArry[i]).attr('checked','checked');
					$('#parentName').val($('.grid').find('.table-body').find('tr:eq(i)').find('td:eq(3)').find('span').attr('title'));
				}; */
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//------------------------------
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				//---------------------------
				
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//让之前选中的默认选中
				var defaultCode = '${defaultCode}';
				var defaultName = '${defaultName}';
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						if(this.id.substr(4,this.id.length)==defaultCode){
							$(this).attr("checked", true);
							$('#parentId').val(defaultCode);
							$('#parentName').val(defaultName);
						}
					})	
				}
				
			});
			function select_Positn(){
				if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
					$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
						$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
						$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
					})
				}else{
					$('#parentId').val('');
					$('#parentName').val('');
				}
				top.customWindow.afterCloseHandler('Y');
				top.closeCustom();
			}
			function updPositn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size()==1){
					return true;
				}else {
					alert("<fmt:message key="please_select_one_positions" />！");
				}
			}
		</script>
	</body>
</html>