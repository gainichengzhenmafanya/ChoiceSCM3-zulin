<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="org" /><fmt:message key="select1" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
				.button :hover {
					background-image:url('/JMUASST/image/Button_new/newbtns.png');
				 	background-position:0px 0px;
				 	width:98px;
				 	height:32px;
				    color: #FFFFFF;  
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<form id="queryForm" name="queryForm" action="<%=path %>/positn/setDeliver.do" method="post">
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" style="text-align: center;"><span style="width: 25px;"></span></td>
								<td colspan="3"><span><fmt:message key ="positions" /></span></td>
								<td colspan="6"><span><fmt:message key ="suppliers" /></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key="type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:100px;"><fmt:message key="name" /></span></td>
								<td><span style="width:120px;"><fmt:message key="type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:100px;"><fmt:message key="name" /></span></td>
								<td><span style="width:70px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:80px;"><fmt:message key="person_in_charge" /></span></td>
								<td><span style="width:80px;"><fmt:message key="contact" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${positnList}"	varStatus="status">
								<tr>
									<td><span style="width: 25px;">${status.index+1}</span></td>
									<td><span title="${positn.typ}" style="width:60px;">${positn.typ}</span></td>
									<td><span title="${positn.code}" style="width:80px;">${positn.code}</span></td>
									<td><span title="${positn.des}" style="width:100px;">${positn.des}</span></td>
									<td>
										<span style="width:120px;"> 
											<select id="typ" name="typ" class="select" style="width:110px;">
											    <c:forEach var="codeDes" items="${codeDesList}" varStatus="status">
											    	<option value="${codeDes.des }" <c:if test="${codeDes.des == '其他' }">selected="selected"</c:if>>${codeDes.des}</option>
											    </c:forEach>
											</select>
										</span>
									</td>
									<td><span title="${positn.code}" style="width:80px;">${positn.code }</span></td>
									<td><span style="width:100px;">${positn.des }</span></td>
									<td><span style="width:70px;">${positn.init }</span></td>
									<td><span style="width:80px;">${positn.des }</span></td>
									<td><span style="width:80px;">${positn.des }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript">
	//ajax同步设置
	$.ajaxSetup({
		async: false
	});
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		//屏蔽鼠标右键
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		//表格数量一变，校验一下，价格就接着变
	 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
	 			var index=$(e.srcElement).closest('td').index();
	    		if(index=="6"){
	    			$(e.srcElement).unbind('blur').blur(function(e){
		 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		 			});
		    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
	    		}
	    	}
		});
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		editCells();
		$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),5);
	});
	
	//动态下拉列表框
	function findByTitle(e){
		$('#listForm').submit();
	}
	//确认修改
	function saveDeliver() {
		var checkboxList = $('.grid').find('.table-body').find('tr');
		if(checkboxList.size() == 0){
			alert('<fmt:message key="No_data_to_be_saved" />！');
			return;
		}
		var data = {};
		checkboxList.each(function(i){
			var typ = $(this).find('td:eq(4)').find('span').find('select').val();
			//把1999盈亏库同步的供应商编码修改为1999
			var codeTitle = $(this).find('td:eq(5) span').attr('title');
			if(codeTitle=='1999'){
				$(this).find('td:eq(5) span').text('1999');
			}
			var code = $(this).find('td:eq(5)').text()?$(this).find('td:eq(5)').text():$(this).find('td:eq(5) input').val();
			var des = $(this).find('td:eq(6)').text()?$(this).find('td:eq(6)').text():$(this).find('td:eq(6) input').val();
			var init = $(this).find('td:eq(7)').text()?$(this).find('td:eq(7)').text():$(this).find('td:eq(7) input').val();
			var person = $(this).find('td:eq(8)').text()?$(this).find('td:eq(8)').text():$(this).find('td:eq(8) input').val();
			var person1 = $(this).find('td:eq(9)').text()?$(this).find('td:eq(9)').text():$(this).find('td:eq(9) input').val();
			var positn = $(this).find('td:eq(2)').find('span').attr('title');
			data['deliverList['+i+'].typ'] = typ;
			data['deliverList['+i+'].code'] = code;
			data['deliverList['+i+'].des'] = des;
			data['deliverList['+i+'].init'] = init;
			data['deliverList['+i+'].person'] = person;
			data['deliverList['+i+'].person1'] = person1;
			data['deliverList['+i+'].positn'] = positn;
		});
		//提交并返回值，判断执行状态
		$.ajax({
			url:"<%=path%>/positn/saveDeliver.do",
			type:"post",
			data:data,
			success:function(msg){
				var rs = eval('('+msg+')');
				switch(Number(rs.pr)){
				case -1:
					alert('<fmt:message key="Encoding_repeat_please_modify" />:'+rs.error);
					$('#queryForm').submit();
					break;
				case 1:
					alert('<fmt:message key ="save_successful" />！');
					parent.$('.close').click();
				}
			}
		});
	}
	//编辑表格
	function editCells(){
		if($(".table-body").find('tr').length == 0){
			return;
		}
		$(".table-body").autoGrid({
			initRow:1,
			colPerRow:10,
			widths:[25,70,90,110,130,90,110,80,90,90],
			colStyle:['','','','','',{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1"}],
			VerifyEdit:{verify:true,enable:function(cell,row){
				return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
			}},
			onEdit:$.noop,
			editable:[5,6,7,8,9],//能输入的位置
			onLastClick:$.noop,
			onEnter:function(data){
				$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
				//盈亏库编码不允许修改
				var tcode = data.curobj.find('span').attr('title');
				if($.trim(tcode)=='1999'){
					data.curobj.find('span').html('1999');
				}
			},
			cellAction:[{
				index:5,
				action:function(row,data2){
					$.fn.autoGrid.setCellEditable(row,6);
				}
			},{
				index:6,
				action:function(row,data2){
					var str=data2.value.replace(/\s/g, "");//删除所有空格
				    var arrResult = new Array(); //保存中间结果的数组
				    for(var i=0;i<str.length;i++){
				        //获得unicode码
				        var ch = str.charAt(i);
				        //检查该unicode码是否在处理范围之内,在则返回该码对映汉字的拼音首字母,不在则调用其它函数处理
				        arrResult.push(checkCh(ch));
				    }
				    row.find('td:eq(7)').find('span').text(arrResult.join(''));
					$.fn.autoGrid.setCellEditable(row,7);
				}
			},{
				index:7,
				action:function(row,data2){
					$.fn.autoGrid.setCellEditable(row,8);
				}
			},{
				index:8,
				action:function(row,data2){
					$.fn.autoGrid.setCellEditable(row,9);
				}
			},{
				index:9,
				action:function(row,data2){
					$.fn.autoGrid.setCellEditable(row.next(),5);
				}
			}]
		});
	}
	
	function validateByMincnt(index,row,data2){//最小申购量判断
		if(index=="6"){
			var str=data2.value.replace(/\s/g, "");//删除所有空格
		    var arrResult = new Array(); //保存中间结果的数组
		    for(var i=0;i<str.length;i++){
		        //获得unicode码
		        var ch = str.charAt(i);
		        //检查该unicode码是否在处理范围之内,在则返回该码对映汉字的拼音首字母,不在则调用其它函数处理
		        arrResult.push(checkCh(ch));
		    }
		    row.find('td:eq(7)').find('span').text(arrResult.join(''));
		}
	}
	
	function validator(index,row,data2){//输入框验证
		if(index=="6"){
			var str=data2.value.replace(/\s/g, "");//删除所有空格
		    var arrResult = new Array(); //保存中间结果的数组
		    for(var i=0;i<str.length;i++){
		        //获得unicode码
		        var ch = str.charAt(i);
		        //检查该unicode码是否在处理范围之内,在则返回该码对映汉字的拼音首字母,不在则调用其它函数处理
		        arrResult.push(checkCh(ch));
		    }
		    row.find('td:eq(7)').find('span').text(arrResult.join(''));
		}
	}
	</script>
	</body>
</html>