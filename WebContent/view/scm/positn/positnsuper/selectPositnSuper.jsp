<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css"> 
	 	.page{
			margin-bottom: 25px;
		}
		.text,select{
			width:80px;
		}
		</style>
	</head>
	<body>
		<form action="<%=path%>/positn/findPositnSuper.do" id="queryForm" name="queryForm" method="post">
			<input type="hidden" name="mis" value="${mis}"></input>
			<input type="hidden" name="deliverDirect" value="${deliverDirect}"></input>
			<input type="hidden" name="typn" value="${queryPositn.typn}"></input>
			<input type="hidden" name="ucode" value="${queryPositn.ucode}"></input>
			<input type="hidden" name="delivercode" value="${queryPositn.delivercode}"></input>
			<input type="hidden" name="sp_code" value="${queryPositn.sp_code}"></input>
			<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td width="200">&nbsp;
						<font style="font-size:2ex;"><b><fmt:message key ="business_group" />：</b></font>
						<select name="mod2" class="select">
							<option value=""><fmt:message key="please_select" /><fmt:message key="business_group" /></option>
							<c:forEach items="${listMod }" var="curMod">
								<option value="<c:out value="${curMod.des }"/>" 
									<c:if test="${curMod.des == queryPositn.mod2 }">
										selected="selected"
									</c:if>
									><c:out value="${curMod.des }"/></option>
							</c:forEach>
						</select>
					</td>
					<td width="200">&nbsp;
						<font style="font-size:2ex;"><b><fmt:message key ="area" />：</b></font>
						<select name="area" class="select">
							<option value=""><fmt:message key="please_select" /><fmt:message key="area" /></option>
							<c:forEach items="${listArea }" var="curArea">
									<option value="<c:out value="${curArea.des }"/>" 
									<c:if test="${curArea.des == queryPositn.area }">
										selected="selected"
									</c:if>
									><c:out value="${curArea.des }"/></option>
							</c:forEach>
						</select>
					</td>
					<td width="200">&nbsp;
						<font style="font-size:2ex;"><b><fmt:message key ="category" />：</b></font>
						<select name="typ" class="select" onkeyup="ajaxSearch()">
							<option value=""><fmt:message key ="all" /></option>
							<c:forEach items="${listPositnTyp }" var="positnTyp">
								<option value="${positnTyp.typ }" <c:if test="${positnTyp.typ == queryPositn.typ }">selected="selected"</c:if>>${positnTyp.typ }</option>
							</c:forEach>
						</select>
					</td>
					<td width="200">&nbsp;
			        	<font style="font-size:2ex;"><b><fmt:message key="coding" />:</b></font>
			            <input type="text" id="code" name="code" class="text" value="<c:out value="${queryPositn.code}" />" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2ex;"><b><fmt:message key="name" />:</b></font>
			            <input type="text" id="des" name="des" class="text" value="<c:out value="${queryPositn.des}" />" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td width="200">&nbsp;
			        	<font style="font-size:2ex;"><b><fmt:message key="abbreviation" />:</b></font>
			            <input type="text" id="init" name="init" class="text" style="text-transform:uppercase;" value="<c:out value="${queryPositn.init}" />" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/>
			        </td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:45px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:180px;"><fmt:message key="name" /></td>
								<td style="width:80px;"><fmt:message key="type" /></td>
								<td style="width:80px;"><fmt:message key="abbreviation" /></td>
								<td style="width:80px;"><fmt:message key="for_short" /></td>
								<td style="width:60px;"><fmt:message key="area" /></td>
								<td style="width:60px;"><fmt:message key="business_group" /></td>
								<td style="width:80px;"><fmt:message key="distribution_area" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="positn" varStatus="step" items="${listPositn}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${positn.code}" value="${positn.code}"/>
									</td>
									<td><span title="${positn.code}" style="width:40px;text-align: left;"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span title="${positn.des}" style="width:170px;text-align: left;"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span title="${positn.typ}" style="width:70px;text-align: left;"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span title="${positn.init}" style="width:70px;text-align: left;"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span title="${positn.des1}" style="width:70px;text-align: left;"><c:out value="${positn.des1}"/>&nbsp;</span></td>
									<td><span title="${positn.area}" style="width:50px;text-align: left;"><c:out value="${positn.area}"/>&nbsp;</span></td>
									<td><span title="${positn.mod2}" style="width:50px;text-align: left;"><c:out value="${positn.mod2}" />&nbsp;</span></td>
									<td><span title="${positn.psarea}" style="width:70px;text-align: left;"><c:out value="${positn.psarea}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<input type="hidden" id="typ" name="typ" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),70);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#queryForm').submit();
				});
				var str=parent.$('#parentId').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++)
				{ 
					$('#chk_'+strArry[i]).attr('checked','checked');
				};
				$('.grid').find('.table-body').find('tr').each(function(){
					if($(this).find(":checkbox").attr("checked")) {
						$(this).addClass("bgBlue");
					}
				});
				$('.grid').find('.table-head').find('#chkAll').click(function(){
					var checkboxList = $('.grid').find('.table-body').find('tr');
					if($(this).attr("checked")) {
						checkboxList.each(function(){
							$('.grid').find('.table-body').find('tr').each(function(){
								$(this).addClass("bgBlue");
							});
							parent.selectAllPositn($(this).find('td:eq(2)').find('span').attr('title'),$(this).find('td:eq(3)').find('span').attr('title'));
						});
					}else {
						checkboxList.each(function(){
							$('.grid').find('.table-body').find('tr').each(function(){
								$(this).removeClass("bgBlue");
							});
							parent.selectZeroPositn($(this).find('td:eq(2)').find('span').attr('title'),$(this).find('td:eq(3)').find('span').attr('title'));
						});
					}
				});
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", true);
				     }else{
				    	 $(this).attr("checked", false);
				     }
				     parent.selectPositn($(this).parent().next().find('span').attr('title'),$(this).parent().next().next().find('span').attr('title'));
				 });
				/* // 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				     parent.selectPositn($(this).find('td:eq(2)').find('span').attr('title'),$(this).find('td:eq(3)').find('span').attr('title'));
				 }); */
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
			});
			//清空页面
			function clearValue(){
				$('#tblGrid').find('input').removeAttr('checked');
			}
		</script>
	</body>
</html>