<%@ page language="java" contentType="text/html; charset=UTF-8"  import="java.sql.*" 
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>仓位选择</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
  		<link type="text/css" rel="stylesheet" href="<%=path%>/css/chosen.css" />
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
		<div id='toolbar'></div>
		<div class="form" style="text-align: center;width: 98%;height: 85%">
			<form id="noticeForm" method="post" action="<%=path %>/publish/saveDistributionStores.do" style="height: 100%;">
				<input type="hidden" id="sta" name="sta" value="${positn.sta }"/>
				<input type="hidden" id="typn" name="typn" value="${positn.typn }"/>
				
				<input type="hidden" id="area" name="area"/>
				<input type="hidden" id="psarea" name="psarea"/>
				<input type="hidden" id="typ" name="typ"/>
				<input type="hidden" id="firmtyp" name="firmtyp"/>
				<input type="hidden" id="ucode" name="ucode"/>
				
				<div class="form-line">
					<div class="form-label" style="width:150px;"><fmt:message key ="coding" /> or <fmt:message key ="abbreviation" /> or <fmt:message key ="name" /></div>
					<div class="form-input">
						<select id="vscode" name="vscode"  data-placeholder="<fmt:message key ="select_positions" />(<fmt:message key ="branche" />)..." class="chosen-select" style="width:190px;" tabindex="2" onchange="setchangeValue(this)">
							<option value=""></option>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<option value="${positn.code }">${positn.code } -- ${positn.init } -- ${positn.des }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<table cellspacing="0" cellpadding="0" style="width:99%;height: 100%;">
					<tr>
						<td align="left" height="45%" width="60%">
							<div style="width:50%;height:100%;float:left;">
								<fieldset style="border-color: #B1C3D9;height:100px;">
									<legend><fmt:message key ="area" /></legend>
									<div class="treePanel" style="width: 98%; height: 85%; overflow: auto;">
								        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
							        	<script type="text/javascript">
							          		var tree = new MzTreeView("tree");
							          
							          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key ="all" />;method:setSelectStoreTeamList("area","")';
							          		<c:forEach var="areaobj" items="${area}" varStatus="status">
							          			tree.nodes["00000000000000000000000000000000_${areaobj.code }"] 
								          		= "text:${areaobj.des}; method:setSelectStoreTeamList('area','${areaobj.code}')";
							          		</c:forEach>
							          		 tree.setIconPath("<%=path%>/image/tree/none/");
							          		document.write(tree.toString());
							        	</script>
							    	</div>
							    	
								</fieldset>
								<fieldset style="border-color: #B1C3D9;height:100px;">
									<legend><fmt:message key ="psarea" /></legend>
									<div class="treePanel" style="width: 98%; height: 85%; overflow: auto;">
							        	<script type="text/javascript">
							          		var tree1 = new MzTreeView("tree1");
							          
							          		tree1.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key ="all" />;method:setSelectStoreTeamList("psarea","")';
							          		<c:forEach var="psareaobj" items="${psarea}" varStatus="status">
							          			tree1.nodes["00000000000000000000000000000000_${psareaobj.code }"] 
								          		= "text:${psareaobj.des}; method:setSelectStoreTeamList('psarea','${psareaobj.code}')";
							          		</c:forEach>
							          		 tree1.setIconPath("<%=path%>/image/tree/none/");
							          		document.write(tree1.toString());
							        	</script>
							    	</div>
								</fieldset>
							</div>
							<div style="width:50%;height:100%;float:left;">
								<fieldset style="border-color: #B1C3D9;height:204px;margin:0px;width:96%;">
									<legend><fmt:message key ="positions" /><fmt:message key ="scm_type" /></legend>
									<div class="treePanel" style="width: 98%; height: 90%; overflow: auto;">
							        	<script type="text/javascript">
							          		var treeTeam = new MzTreeView("treeTeam");
							          		treeTeam.nodes['0_1'] = 'text:全部;method:setSelectStoreTeamList("typ","")';
							          		<c:forEach var="positntype" items="${listPositnTyp}" varStatus="status">
								          		<c:choose>
									          		<c:when test="${positntype.code == '1203'}">
									          			treeTeam.nodes['1_${positntype.code}'] = 
									          				"text:${positntype.des};method:setSelectStoreTeamList('typ','${positntype.code}')";
										          		//遍历所有门店类型
										          		<c:forEach var="ft" items="${firmtyp }">
										          			treeTeam.nodes['${positntype.code}_${ft.code }'] 
										          				= "text:${ft.des};method:setSelectStoreTeamList('firmtyp','${ft.code}')";
										          				
										          			//遍历所有分店
											          		<c:forEach var="posi" items="${firmList }">
											          			<c:if test="${posi.firmtyp == ft.code }">
											          				treeTeam.nodes['${ft.code}_${posi.code }'] 
												          				= "text:${posi.des};method:setSelectStoreTeamList('ucode','${posi.code}')";
											          			</c:if>
											          		</c:forEach>	
										          		</c:forEach>
										          		
									          		</c:when>
									          		<c:otherwise>
									          			treeTeam.nodes['1_${positntype.code}'] = "text:${positntype.des}; method:setSelectStoreTeamList('typ','${positntype.code}')";
									          		</c:otherwise>
									          	</c:choose>
							          		</c:forEach>
							          		 treeTeam.setIconPath("<%=path%>/image/tree/none/");
							          		document.write(treeTeam.toString());
							        	</script>
							    	</div>
								</fieldset>
							</div>
						</td>
						<td rowspan="2" align="left" valign="middle" style="width: 5%; height: 90%; " >
							<input type="button" style="width:25px;height:25px;" value=">>" onclick="_rightAll()" />
							<br />
							<br />
							<br />
							<input type="button" style="width:25px;height:25px;" value="&nbsp;>&nbsp;" onclick="_right()" />
							<br />
							<br />
							<br />
							<input type="button" style="width:25px;height:25px;" value="&nbsp;&lt;&nbsp;" onclick="_left()" />
							<br />
							<br />
							<br />
							<input type="button" style="width:25px;height:25px;" value="&lt;&lt;" onclick="_leftAll()" />
						</td>
						<td rowspan="2"  align="left" height="95%" width="35%">
							<fieldset style="border-color: #B1C3D9;height: 100%; ">
								<legend><fmt:message key ="selected" /><fmt:message key ="positions" /></legend>
								<div style="width: 99%; height: 95.5%; overflow: auto;">
									<select class="yixuan"  style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectedstore"  >
										<c:forEach var="positn" items="${listPositn1}" varStatus="status">
											<option value="${positn.code}">${positn.code} -- ${positn.init } -- ${positn.des}</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
					<tr>
						<td  align="left" height="55%" width="60%">
							<fieldset style="width: 98%;border-color: #B1C3D9; height:100%; ">
								<legend><fmt:message key ="select_positions" /></legend>
								<div style="width: 97%; height: 90%; overflow: auto;">
									<select class="weixuan" style="width: 100%; height: 100%; overflow: auto;" multiple="multiple" id="selectstore" >
										<c:forEach var="positn" items="${listPositn}" varStatus="status">
											<option value="${positn.code }">${positn.code } -- ${positn.init } -- ${positn.des }</option>
										</c:forEach>
									</select>
								</div>
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/chosen.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		
		<script type="text/javascript">
			var editor;
			var single='${single}';
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vtitle',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="title"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="title_too_long"/>！']
					}]
				});
				$("#toolbar").toolbar({
					items: [{
						text: '<fmt:message key ="enter" />',
						useable:true,
						handler: function(){
							var data = {code:[],des:[],entity:[]};
							$(".yixuan option").each(function(){
									data.code.push($(this).val());
									data.des.push($.trim($(this).html().substring($(this).html().lastIndexOf(" -- ")+4)));
// 								}
							});
							var tagId = '${tagId}';
							var tagName = '${tagName}';
							$("#"+tagName,parent.document).val(data.des.join(","));
							$("#"+tagId,parent.document).val(data.code.join(","));
							var callBack = '${callBack}';//回调函数
							if(callBack !=null && callBack != ''){
								if(callBack == 'initValue'){
									window.parent.initValue(data.code.join(","),data.des.join(","));
								}
								else if(callBack == 'addNewSpprice'){
									window.parent.addNewSpprice();
								}
								else if(callBack == 'addNewSppriceAll'){
									window.parent.addNewSppriceAll();
								}
								else if(callBack == 'saveDeliverDirect'){
									window.parent.saveDeliverDirect();
								}else{
									//根据传进来的方法名调用父页面方法
									window.parent[callBack](data.code.join(","),data.des.join(","));
								}
							}
			 				$(".close",parent.document).click();
						}
					},{
						text: '<fmt:message key ="cancel" />',
						useable: true,
						handler: function(){
// 							parent.$('#listStoreFrame').closest('.window').find(".close").click();
							$('.close',parent.document).click();
						}
					}
				]
			});
				
				//控制店铺择框
				$(".chosen-select").chosen({allow_single_deselect:true,
					disable_search_threshold:10,
					no_results_text:'Oops, nothing found!',
					width:"200%",
					search_contains:true,
				});
			    //设置查询输入框宽度，留出来一部分给按钮
		    	$("input[tabindex=2]").css("width","77%");
		    	//设置按钮
		    	$(".chosen-search").append('<button onclick="checkResults();">选中结果</button>');
		    	
			    //左侧未选择的 过滤掉 右侧已选择的
				var yixuan_arry = [];
				<c:forEach var="positn1" items="${listPositn1}">
					yixuan_arry.push('${positn1.code}');
				</c:forEach>
			    $(".weixuan option").each(function(){
			    	if($.inArray($(this).val(),yixuan_arry)>-1){
			    		$(this).hide();
			    	}
			    });
			    $("#vscode option").each(function(){
			    	if($.inArray($(this).val(),yixuan_arry)>-1){
			    		$(this).hide();
			    	}
			    });
			    
			});
			
			var _nodetypes="";
			var _nodeid="";
			function setSelectStoreList(nodetypes,nodeid) {
// 				$("#area").val(nodeid);
				_nodetypes=nodetypes;
				_nodeid=nodeid;
				$("#selectstore").empty();
				<c:forEach var="positn" items="${listPositn}" varStatus="status">;
					if(nodetypes == "0"){
						$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
					} else if (nodetypes == "1") {
						if (nodeid == "${store['PK_MARKETID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					} else if (nodetypes == "2") {
						if (nodeid == "${store['PK_BOHID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					} else if (nodetypes == "3") {
						if (nodeid == "${store['PK_GOVERNORID']}") {
							$("#selectstore").append("<option value='${store['PK_STORE']}'>${store['VCODE']} -- ${store['VNAME']}</option>");
						}
					}
				</c:forEach>;
			}
			
			function setSelectStoreTeamList(key,value) {
 				$.ajaxSetup({async:false});
 				$('#ucode').val('');
 				$('#'+key).val(value);
 				var sta = $('#sta').val();
 				var typn = $('#typn').val();
 				var area = $('#area').val();
 				var psarea = $('#psarea').val();
 				var typ = $('#typ').val();
 				var firmtyp = $('#firmtyp').val();
 				var ucode = $('#ucode').val();
 				if(key == 'ucode'){
 					area = '';
 					psarea = '';
 					$('#typ').val('');
 					$('#firmtyp').val('');
 					typ = '1207';
 					firmtyp = '';
 				}
 				if(key == 'firmtyp'){
 					$('#typ').val('1203');
 				}
 				if(key == 'typ'){
 					$('#firmtyp').val('');
 				}
				$.post("<%=path %>/positn/searchAllPositnNew.do?sta="+sta+"&typn="+typn+"&typ="+typ+"&area="+area+"&psarea="+psarea+"&firmtyp="+firmtyp+"&ucode="+ucode,function(data){
					//1、取出原来选择门店中含有的数据
					var array = new Array(); //定义数组
					//1、从此处进行清空，重新加载  选中区域列表中对应的数据
					$("#selectstore").empty();
// 					setSelectStoreList(_nodetypes,_nodeid);
					//2、再获取选择门店中的数据
				   $("#selectstore option").each(function(){ //遍历全部option
				        var txt = $(this).val(); //获取option的内容
				        array.push(txt); //添加到数组中
				    });
				   $("#selectstore").empty();
					if(array.length==0){
						for(var i in data){
							$("#selectstore").append("<option value='"+data[i].code+"'>"+data[i].code+" -- "+data[i].init+" -- "+data[i].des+"</option>");
						}
					}else{
						//2、对新添加的数据进行比较，假如含有，则不往里面添加，没有  则添加
						for(var i in data){
							for(var j=0;j<array.length;j++){
								if(data[i].code==array[j]){
									$("#selectstore").append("<option value='"+data[i].code+"'>"+data[i].code+" -- "+data[i].init+" -- "+data[i].des+"</option>");
									break;
								}
							}
							
						}
						
					}
				});
			}
			$('#selectstore').bind('dblclick',function(){
				_right();
			});
			$('#selectedstore').bind('dblclick',function(){
				_left();
			});
			function _rightAll() {
				if(single=='true' && ($(".yixuan option").size()>0||($(".weixuan option").size()+$(".yixuan option").size()>1))){
					alert('<fmt:message key="only_select_one_store" />!');
					return;
				}
				var v='';
				$(".weixuan option").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			
			function _right() {
				if(single=='true' && ($(".yixuan option").length>0 || $(".weixuan option:selected").length > 1)){
					alert('<fmt:message key ="Please_choose_a_position" />!');
					return;
				}
				var v='';
				$(".weixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);		
			}
						
			function _left() {
				var v='';
				$(".yixuan option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".weixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$(".yixuan option:selected").each(function(){
					$(this).remove();
				});
			}
			
			function _leftAll() {
				$(".yixuan").empty();
			}
			
			//返回选中列表长度
			function reutrnValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).val());
				});
				$("#pk_stores").val(chkValue);
				return chkValue.length;
			}
			
			//返回选中列表门店编码--名称
			function reutrnChkValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).html());
				});
// 				$("#pk_stores").val(chkValue);
				return chkValue;
			}
			
			//返回选中列表店铺数据
			function reutrnChkStoreValue() {
				var chkValue = [];
				$(".yixuan option").each(function(){
					var r=$(this);
					chkValue.push($(this).val());
				});
				$("#pk_stores").val(chkValue);
				return chkValue;
			}
			
			function setchangeValue() {
				var v='';
				$("#vscode option:selected").each(function(){
					var b=true;
					var r=$(this);
					$(".yixuan option").each(function(){
						if(r.val()==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						v+= '<option value="'+$(this).val()+'" >'+$(this).html()+'</option>';
					}
				});
				$('.yixuan').append(v);
			}
			//返回门店主键、编码、名称信息
			function returnStoreList(){
				var data = {show:[],code:[],mod:[],entity:[]};
				$(".yixuan option").each(function(){
					var entity = {};
					entity.pk_store = $.trim($(this).val());
					entity.vcode = $.trim($(this).html().substring(0,$(this).html().indexOf(" ")));
					alert($.trim($(this).html().substring(0,$(this).html().indexOf(" "))));
					entity.vname = $.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1));
					alert($.trim($(this).html().substring($(this).html().lastIndexOf(" ")+1)));
					data.entity.push(entity);
					return ;
				});
// 				parent['${callBack}'](data);
// 				$(".close",parent.document).click();
			}
			
			//选择查询的结果集
			function checkResults(){
				var searchResult = $('.chosen-results').html();
				//拆分字符串
				//<li class="active-result result-selected" style="" data-option-array-index="1">1000 -- <em>P</em>SZX -- 配送中心</li>
				//<li class="active-result" style="" data-option-array-index="9">1999 -- <em>P</em>YPK -- 盘盈盘亏</li>
				//得到值
				$(searchResult).each(function(){
					var allstr = $.trim($(this).text());
					var theval = $.trim(allstr.split(" ")[0]);
					//判断是否已经存在
					var b=true;
					$(".yixuan option").each(function(){
						if(theval==$(this).val()){
							b=false;
							return false;
						}
					});
					if(b){
						$('.yixuan').append('<option value="'+theval+'" >'+allstr+'</option>');
					}
				});
			}
			
			
		</script>
	</body>
</html>