<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/positn/listDetail.do?descode=${descode}" id="queryForm" name="queryForm" method="post">
			<input type="hidden" id="descodeName" value="${descode}"/>
			<input type="hidden" id="area" value="${queryPositn.area}"/>
			<input type="hidden" id="ucode" name="ucode" value="${queryPositn.ucode}"/>
			<input type="hidden" id="ynUseDept" value="${ynUseDept}"/>
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label"><fmt:message key ="abbreviation" /></div>
					<div class="form-input">
						<input type="text" id="init" name="init" style="text-transform:uppercase;" value="${queryPositn.init}" onkeyup="ajaxSearch()" class="text"/>
					</div>
					<div class="form-label"><fmt:message key ="category" /></div>
					<div class="form-input">
						<select name="typ" class="select" onkeyup="ajaxSearch()">
							<option value=""><fmt:message key ="all" /></option>
							<c:forEach items="${listPositnTyp }" var="positnTyp">
								<option value="${positnTyp.typ }"/>${positnTyp.typ }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="brands" /></div>
					<div class="form-input">
						<select name="mod2" class="select" onkeyup="ajaxSearch()">
							<option value=""><fmt:message key="please_select" /><fmt:message key="brands" /></option>
							<c:forEach items="${listMod }" var="curMod">
								<option value="<c:out value="${curMod.des }"/>" 
									<c:if test="${curMod.des == queryPositn.mod2 }">
										selected="selected"
									</c:if>
									><c:out value="${curMod.des }"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label">
						<fmt:message key="area" /></div>
						<div class="form-input">
							<select name="area" class="select" onkeyup="ajaxSearch()">
								<option value=""><fmt:message key="please_select" /><fmt:message key="area" /></option>
								<c:forEach items="${listArea }" var="curArea">
										<option value="<c:out value="${curArea.code }"/>" 
										<c:if test="${curArea.des == queryPositn.area }">
											selected="selected"
										</c:if>
										><c:out value="${curArea.des }"/></option>
								</c:forEach>
							</select>
						</div>
					
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;padding-left:8px">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:80px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:100px;"><fmt:message key="for_short" /></span></td>
								<td><span style="width:40px;"><fmt:message key="status" /></span></td>
								<td><span style="width:40px;"><fmt:message key="if_inuse" /></span></td>
								<td><span style="width:80px;"><fmt:message key="area" /></span></td>
								<td><span style="width:80px;"><fmt:message key="brands" /></span></td>
								<td><span style="width:80px;"><fmt:message key="distribution_area" /></span></td>
								<c:choose>
									<c:when test="${descode == '1203' }">
										<td><span style="width:60px;"><fmt:message key="firmtype"/></span></td>
										<td><span style="width:50px;"><fmt:message key="yn_use_dept"/></span></td>
										<td><span style="width:60px;"><fmt:message key="whether_the_headquarters_to_review" /></span></td>
									</c:when>
									<c:when test="${descode == '1207' }">
										<td><span style="width:90px;"><fmt:message key="department_of_property"/></span></td>
									</c:when>
								</c:choose>
<%-- 								<td><span style="width:90px;"><fmt:message key="whether_based_virtual_to_real" /></span></td> --%>
<!-- 								<td><span style="width:180px;">IP</span></td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<tr data-pcode="${positn.pcode }" data-ucode="${positn.ucode }">
									<td><span class="num" style="width: 25px;padding-left:8px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${positn.acct}' />" value="<c:out value='${positn.acct}' />"/></span>
									</td>

									<td><span style="width:80px;" title="${positn.code}"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span style="width:180px;" title="${positn.des}"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.typ}"><c:out value="${positn.typ}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.init}"><c:out value="${positn.init}" />&nbsp;</span></td>
									<td><span style="width:100px;" title="${positn.des1}"><c:out value="${positn.des1}" />&nbsp;</span></td>
<%-- 									<c:if test="${descodeName !='' and descodeName =='部门' }"> --%>
<%-- 										<td><span style="width:60px;" title="${positn.deptattr}"><c:out value="${positn.deptattr}" />&nbsp;</span></td> --%>
<%-- 									</c:if> --%>
									<td><span style="width:40px;" title="${positn.sta}"><c:if test="${positn.sta=='N'}"><fmt:message key ="disabled" /></c:if>
									<c:if test="${positn.sta=='Y'}"><fmt:message key ="enable" /></c:if></span></td>
									<td><span style="width:40px;" title="${positn.locked}"><c:if test="${positn.locked=='N'}"><fmt:message key="No_reference" /></c:if>
									<c:if test="${positn.locked=='Y'}"><fmt:message key="Has_been_cited" /></c:if></span></td>
									<td><span style="width:80px;" title="${positn.area}"><c:out value="${positn.area}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.mod2}"><c:out value="${positn.mod2}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.psarea}"><c:out value="${positn.psarea}" />&nbsp;</span></td>
									<c:choose>
										<c:when test="${descode == '1203' }">
											<td><span style="width:60px;" title="${positn.firmtypdes }">${positn.firmtypdes }</span></td>
											<td><span style="width:50px;" title="${positn.ynUseDept}"><c:if test="${positn.ynUseDept=='N'}"><fmt:message key ="no1" /></c:if>
											<c:if test="${positn.ynUseDept=='Y'}"><fmt:message key ="be" /></c:if></span></td>
											<td><span style="width:60px;" title="${positn.ynZbChk}"><c:if test="${positn.ynZbChk=='N'}"><fmt:message key ="no1" /></c:if>
											<c:if test="${positn.ynZbChk=='Y'}"><fmt:message key ="be" /></c:if></span></td>
										</c:when>
										<c:when test="${descode == '1207' }">
											<td><span style="width:90px;" title="${positn.deptattr}"><c:out value="${positn.deptattr}" />&nbsp;</span></td>
										</c:when>
									</c:choose>
<%-- 									<td><span style="width:90px;" title="${positn.ynZbXs}"><c:if test="${positn.ynZbXs=='N'}"><fmt:message key ="no1" /></c:if> --%>
<%-- 									<c:if test="${positn.ynZbXs=='Y'}"><fmt:message key ="be" /></c:if></span></td> --%>
<%-- 									<td><span style="width:180px;" title="${positn.ip}"><c:out value="${positn.ip}" />&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="queryForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#queryForm').submit();
			}
			function clearQueryForm(){
				$('#queryForm').find(".select").find('option:first').attr('selected','selected');
				$('#init').val('').focus();
			}
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			  invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								var t = $('#init').val();
								 $('#init').focus().val(t);
							}
						},"-",{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								savePositn();
							}
						},{
							text: '<fmt:message key ="scm_copy" />',
							title: '<fmt:message key ="scm_copy" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								copyPositn();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updatePositn();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key="branches_and_positions_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deletePositn();
							}
						},
						//wangjie 增加刷新按钮
						{
							text: '<fmt:message key ="refresh" />',
							title: '<fmt:message key ="refresh" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								$('#queryForm').submit();
							}
						},'-',{
							text: '<fmt:message key="scm_related_departments" />',
							title: '<fmt:message key="scm_related_departments" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								connectDept();
							}
						},
						{
							text: '<fmt:message key ="synchronous-virtual-provider" />',
							title: '<fmt:message key ="synchronous-virtual-provider" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								setDeliver();
							}
						},
						{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }
					     else
					     {
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
					//每一行双击
					$('.grid').find('.table-body').find('tr').live("dblclick", function () {
						var chkValue = $(this).find(':checkbox').val();
						chkValue += '&code=' + $.trim($(this).find('td:eq(2)').find('span').attr('title'));
						updatePositnWindow(chkValue);
					});
				
			});
			function savePositn(){
				/**********分店添加部门时，首先要判断该分店是否设置为需要档口 wangjie 2015年3月4日 10:48:02************/
				var ucode = $("#ucode").val();//所属分店
				var ynUseDept = $("#ynUseDept").val();//是否启用档口
				if(ucode!='' && (ynUseDept=='N' || ynUseDept=='')){
					alert("<fmt:message key='the_store_did_not_enable_the_stalls'/>，<fmt:message key='please_enable_stalls_and_try_again'/>！");
					return;
				}
				/**********************/
				var url = "<%=path%>/positn/add.do?descodeName="+$('#descodeName').val()+"&areacode="+$('#area').val()+"&ucode="+$('#ucode').val();
				$('body').window({
					id: 'window_savePositn',
					title: '<fmt:message key="insert" /><fmt:message key="branches_and_positions_information" />',
					content: '<iframe id="savePositnFrame" name="savePositnFrame" frameborder="0" src='+url+'></iframe>',
					width: '600px',
					height: '450px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savePositnFrame')){
										if(getFrame('savePositnFrame').validate._submitValidate()){
											if(!getFrame('savePositnFrame').checkCode()){
												alert("<fmt:message key='gyszmbm' /><fmt:message key='cannot' /><fmt:message key='have' /><fmt:message key='Special' /><fmt:message key='character' />！");
												return;
											}
											submitFrameForm('savePositnFrame','positnForm');
										}
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function copyPositn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					chkValue += '&code=' + $.trim($(aim).parent().parent().next().text());
					
					$('body').window({
						title: '<fmt:message key="copy" /><fmt:message key="branches_and_positions_information" />',
						content: '<iframe id="copyPositnFrame" name="copyPositnFrame" frameborder="0" src="<%=path%>/positn/copy.do?acct='+chkValue+'"></iframe>',
						width: '600px',
						height: '450px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('copyPositnFrame') && getFrame('copyPositnFrame').validate._submitValidate()){
											submitFrameForm('copyPositnFrame','positnForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alert('<fmt:message key="please_select_data" />!');
				}else{
					alert('<fmt:message key="please_select" /><fmt:message key="copy" /><fmt:message key="positions" />！');
					return ;
				}
				
			}
			
			function updatePositn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					chkValue += '&code=' + $.trim($(aim).parent().parent().next().text());
					updatePositnWindow(chkValue);
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alert('<fmt:message key="please_select_data" />!');
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			
			function updatePositnWindow(chkValue){
				$('body').window({
					title: '<fmt:message key="update" /><fmt:message key="branches_and_positions_information" />',
					content: '<iframe id="updatePositnFrame" name="updatePositnFrame" frameborder="0" src="<%=path%>/positn/update.do?acct='+chkValue+'"></iframe>',
					width: '600px',
					height: '450px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('updatePositnFrame') && getFrame('updatePositnFrame').validate._submitValidate()){
										submitFrameForm('updatePositnFrame','positnForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function viewPositn(){
				 pageReload();
			}
			
			function deletePositn(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var flag = false;
				var positnName = "";
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key="delete_data_confirm" />?")){
						var acctValue = 1;
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							acctValue = $(this).val();
							codeValue.push($.trim($(this).parent().parent().next().text()));
							if($.trim($(this).closest('tr').find('td:eq(8)').text())=='已引用'){
								flag = true;
								positnName += $.trim($(this).closest('tr').find('td:eq(3)').text())+"\n";
// 								alert($.trim($(this).closest('tr').find('td:eq(3)').text())+"已经被引用，不可删除！");
// 								return;
							}
						});
						if(flag){
							alert('<fmt:message key ="referenced_can_no_be_deleted" />！'+'\n'+positnName);
							return;
						}
						var action = '<%=path%>/positn/deleteByIds.do?code='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="branches_and_positions_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
			
			//关联分店部门  总部档口to BOH门店档口 绑定
			function connectDept(){
// 				var ucode = $("#ucode").val();//所属分店
// 				if(ucode == ""){
// 					alert("<fmt:message key='the_only_branch_departments_can_set_the_property'/>！");
// 					return;
// 				}
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0){
					alert('<fmt:message key="please_select"/><fmt:message key="scm_related_departments"/>！');
					return;
				}
				if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key ="please_select_data" />！');
					return;
				}
				var codeValue=[];
				var flag = true;
				var ucode = '';
				checkboxList.filter(':checked').each(function(){
					if($(this).parents("tr").data('pcode') != '1207'){
						flag = false;
						return false;
					}
					codeValue.push($(this).parents("tr").find("td:eq(2) span").attr("title"));
					ucode = $(this).parents("tr").attr('data-ucode');
				});
				if(!flag){
					alert("<fmt:message key='the_only_branch_departments_can_set_the_property'/>！");
					return;
				}
				var action = '<%=path%>/positn/toConnectDept.do?code='+codeValue.join(",")+'&firm='+ucode;
				$('body').window({
					title: '<fmt:message key="scm_related_departments"/>',
					content: '<iframe id="bohdept" name="bohdept" frameborder="0" src="'+action+'"></iframe>',
					width: '700px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									//windows["connectDept"]
// 									alert(window.frames["connectDept"].document.getElementById('code').value);
// 									child.window.competorSp();
									window.frames["bohdept"].competorSp();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//设置虚拟供应商
			function setDeliver(){
				alert('<fmt:message key="This_function_will_not_synchronized_set_corresponding_to_the_virtual_suppliers_the_main_direct_library_base_depot_processing_branch_other_types_of_positions_corresponding_to_the_virtual_supplier" />');
				$('body').window({
					title: '<fmt:message key ="synchronous-virtual-provider" />',
					content: '<iframe id="setDeliverFrame" name="setDeliverFrame" frameborder="0" src="<%=path%>/positn/setDeliver.do"></iframe>',
					width: '90%',
					height: '90%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									window.frames["setDeliverFrame"].saveDeliver();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
		</script>
	</body>
</html>