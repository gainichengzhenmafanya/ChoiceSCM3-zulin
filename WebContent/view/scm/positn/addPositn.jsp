<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="branches_and_positions_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body onload="positnForm.code.focus()">
<!-- 	<div class="form-group">分店和仓位信息</div> -->
	<div class="form">
		<div style="height:500px; width:580px;left:50%;top:50%;margin:0px;margin-top: 40px;"><!-- http://222.175.157.61:8079/browse/JAVASCM-833 -->
			<form id="positnForm" method="post" action="<%=path %>/positn/saveByAdd.do">
				<c:if test="${tele_boh=='tele'}">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span>boh <fmt:message key ="store" /><fmt:message key="coding" />：</div>
						<div class="form-input">
							<input type="text" id="firm" name="firm" class="text"/>
						</div>
					</div>
				</c:if>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />：</div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" onblur="getFirm(this);"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="des" name="des" class="text" onkeypress="return checkKey();" onblur="getSpInit(this);"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="abbreviation" />：</div>
					<div class="form-input">
						<input type="text" id="init" name="init" class="text"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="for_short" />：</div>
					<div class="form-input">
						<input type="text" id="des1" name="des1" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="positions"/><fmt:message key="type" />：</div>
					<div class="form-input">
						<input type="hidden" id="descodeName" name="descodeName" value="${descodeName }"/>
						<select name="typ" id="typ" class="select" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${typ}">
								<option value="${cur.code}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="distribution_area" />：</div>
					<div class="form-input">
						<select name="psarea" class="select" id="psarea" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${psarea}">
								<option value="${cur.code}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
					
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="area" />：</div>
					<div class="form-input">
						<select name="area" class="select" id="area" style="width:134px;">
							<option value="">${areacode }</option>
							<c:forEach var="cur" items="${area}">
								<option value="${cur.code}" <c:if test="${areacode == cur.code }">selected="selected"</c:if>><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="brands" />：</div>
					<div class="form-input">
						<select name="mod2" class="select" id="mod2" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${syq}">
								<option value="${cur.code}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<br/>
				<!-- 档口相关属性 -->
				<div class="deptDiv form-line" style="display: none;">
					<div class="form-label"><span class="red">*</span><fmt:message key="their_shop"/>：</div>
					<div class="form-input">
						<select name="ucode" class="select" id="ucode" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${firmList}">
								<option value="${cur.code}" <c:if test="${cur.code == ucode }">selected="selected"</c:if>><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="department_of_property"/>：</div>
					<div class="form-input">
						<select id="deptattr" name="deptattr" class="select" style="width:134px;">
							<option value="A,厨房"><fmt:message key="scm_Kitchen" /></option>
							<option value="B,吧台"><fmt:message key="scm_Bar_counter" /></option>
							<option value="C,展台"><fmt:message key="scm_Exhibition_booth" /></option>
							<option value="D,加工间"><fmt:message key ="processing_room" /></option>
							<option value="E,员工餐"><fmt:message key="scm_Staff_meal" /></option>
							<option value="F,其他"><fmt:message key ="other" /></option>
						</select>
					</div>
				</div>
				<!-- 分店相关属性 -->
				<div class="firmDiv form-line" style="display:none;">
					<div class="form-label"><span class="red">*</span><fmt:message key="firmtype"/>：</div>
					<div class="form-input">
						<select name="firmtyp" class="select" id="firmtyp" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${firmtyps}">
								<option value="${cur.code}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="yn_use_dept"/>：</div>
					<div class="form-input">
						<input type="radio" name="ynUseDept" value="Y" /><fmt:message key ="enable" />
						<input type="radio" name="ynUseDept" value="N" checked="checked"/><fmt:message key ="disabled" />
					</div>
				</div>
				<div class="firmDiv form-line" style="display: none;">
					<div class="form-label"><span class="red">*</span><fmt:message key="costtype" />：</div>
					<div class="form-input">
						<select name="costtyp" class="select" id="costtyp" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cost" items="${costtyps}">
								<option value="${cost.code}"><c:out value="${cost.des}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="forecast_the_way"/>：</div>
					<div class="form-input">
						<select name="vplantyp" class="select" id="vplantyp" style="width:134px;">
							<option value=""></option>
							<option value="AMT">营业额</option>
							<option value="PAX">客流</option>
							<option value="TC">单数</option>
						</select>
					</div>
				</div>
				<div class="firmDiv form-line" style="display:none;">
					<div class="form-label"><fmt:message key="whether_the_headquarters_to_review"/>：</div>
					<div class="form-input">
						<input type="radio" name="ynZbChk" value="Y" /><fmt:message key ="Need_to_be" />
						<input type="radio" name="ynZbChk" value="N" checked="checked"/><fmt:message key ="not_Need_to_be" />
					</div>
					<div class="form-label">是否启用配送班表：</div>
					<div class="form-input">
						<input type="radio" name="visuseschedule" value="Y" /><fmt:message key ="Need_to_be" />
						<input type="radio" name="visuseschedule" value="N" checked="checked"/><fmt:message key ="not_Need_to_be" />
					</div>
				</div>
				<br/>
<!-- 				<div class="form-line"> -->
<%-- 					<div class="form-label" style="width:auto; display:inline-block !important; display:inline; "><fmt:message key="whether_based_virtual_to_real"/>：</div> --%>
<!-- 					<div class="form-input"> -->
<%-- 						<input type="radio" name="ynZbXs" value="Y" /><fmt:message key ="Need_to_be" /> --%>
<%-- 						<input type="radio" name="ynZbXs" value="N" checked="checked"/><fmt:message key ="not_Need_to_be" /> --%>
<!-- 					</div> -->
<!-- 				</div>	 -->
				<div class="form-line">
			    	<div class="form-label"><fmt:message key="status" />：</div>
					<div class="form-input">
						<input type="radio" id="sta" name="sta" class="text" value="Y" checked="checked"/><fmt:message key="enable" /><!-- http://222.175.157.61:8079/browse/JAVASCM-833 -->
						<input type="radio" id="sta" name="sta" class="text" value="N" /><fmt:message key="disabled" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="contact" />：</div>
					<div class="form-input">
						<input type="text" id="vcontact" name="vcontact" class="text" value="<c:out value="${positn.vcontact}"/>"/>
					</div>
					<div class="form-label"><fmt:message key="tel" />：</div>
					<div class="form-input">
						<input type="text" id="vtel" name="vtel" class="text" value="<c:out value="${positn.vtel}"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="zip_code" />：</div>
					<div class="form-input">
						<input type="text" id="vzipcode" name="vzipcode" class="text" value="<c:out value="${positn.vzipcode}"/>"/>
					</div>
					<div class="form-label">IP：</div>
					<div class="form-input">
						<input type="text" id="ip" name="ip" class="text" value="<c:out value="${positn.ip}"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="address" />：</div>
					<div class="form-input">
						<input style="width: 403px;" type="text" id="vaddr" name="vaddr" class="text" value="<c:out value="${positn.vaddr}"/>"/><br />
						<input type="hidden" id="vmalladdrid" name="vmalladdrid" class="text" value="<c:out value="${positn.vmalladdrid}"/>"/>
					</div>
				</div>
			</form>
		</div>
	</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#typ').val($('#descodeName').val());//默认赋值类型
				if($("#typ").val() == "1203"){
					$('.firmDiv').show();
				}
				if($("#typ").val() == "1207"){
					$('.deptDiv').show();
				}
				$('#typ').bind('change',function(){
					if($(this).val() == '1203'){
						$('.firmDiv').show();
					}else{
						$('#firmtyp').val('');
						$('.firmDiv').hide();
					}
					if($(this).val() == '1207'){
 						$('.deptDiv').show();
 					}else{
 						$('#ucode').val('');
 						$('.deptDiv').hide();
 					}
				});
				var ucode = '${ucode}';//判断是否为部门
				
				var areacode = '${areacode}';//区域
				if(areacode !=''){
					$('.firmDiv').show();
					$("#typ").find("option:eq(3)").attr("selected","selected");
					
					if(ucode != ''){//部门的话 显示部门属性
						$('.deptDiv').show();
						$('.firmDiv').hide();
						$("#typ").find("option:last").attr("selected","selected");
					}
				}
				
				//回车输入
		 	 	$('input:text:eq(0)').focus();
		        var $inp = $('input:text');
		        $inp.bind('keydown', function (e) {
		            var key = e.which;
		            if (key == 13) {
		                e.preventDefault();
		                var nxtIdx = $inp.index(this) + 1;
		                $(":input:text:eq(" + nxtIdx + ")").focus();
		            }
		        });
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull','maxLength'],
						param:['F','10'],
						error:['<fmt:message key="number" /><fmt:message key="cannot_be_empty" />！','<fmt:message key ="the_maximum_length" />10！']
					}<c:if test="${tele_boh=='tele'}">,{
						
						type:'text',
						validateObj:'firm',
						validateType:['canNull','maxLength','num'],
						param:['F',10,'F'],
						error:['<fmt:message key="number" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="number_input_extended" />！','决策对应编码必须是数字！']
					}</c:if>,{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="name_enter_the_ultra_long" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:[30],//BS供应链，分店仓位设置中，仓位名称、缩写支持30个汉字。
						error:['<fmt:message key="name" /><fmt:message key ="the_maximum_length" />30！']
					},{
						type:'text',
						validateObj:'init',
						validateType:['canNull','maxLength'],
						param:['F',30],//BS供应链，分店仓位设置中，仓位名称、缩写支持30个汉字。
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="abbreviation" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'des1',
						validateType:['canNull','maxLength'],
						param:['F',20],
						error:['<fmt:message key="for_short" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="for_short" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'des1',
						validateType:['maxLength'],
						param:[10],
						error:['<fmt:message key="for_short" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="type" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'area',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="area" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'psarea',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="distribution_area" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'mod2',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="business_group" /><fmt:message key="cannot_be_empty" />！']
					},{
// 						type:'text',
// 						validateObj:'vtel',
// 						validateType:['phone'],
// 						param:['F'],
// 						error:['<fmt:message key="telephone_format_is_not_correct" />！']
// 					},{
// 						type:'text',
// 						validateObj:'vzipcode',
// 						validateType:['zipCode'],
// 						param:['F'],
// 						error:['<fmt:message key="zip_zip_format_can_not_be_empty_javapro" />！']
// 					},{
						type:'text',
						validateObj:'typ',
						validateType:['handler'],
						handler:function(){
							var result = true;
							if($("#typ").val() != '1201')return result;
							$.ajaxSetup({async:false});
							$.get("<%=path %>/positn/findMainPositn.do",{psarea:$("#psarea").val()},function(data){
								if($.trim(data))result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="distribution_area" /><fmt:message key="main_direct_dial_library" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.get("<%=path %>/positn/findPositnByCode.do",{code:$("#code").val()},function(data){
								if($.trim(data)) result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.get("<%=path %>/positn/findPositnByCode.do",{des:$("#des").val()},function(data){
								if($.trim(data)) result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="Name_already_exists" />！']
					},{
// 						type:'text',
// 						validateObj:'ip',
// 						validateType:['handler'],
// 						handler:function(){
// 							var result = true;
// 							if($("#ip").val())
// 								验证ip(首尾不能为0,各数字不能大于255)
// 								result = $("#ip").val().match(/^((?:(?:25[0-4]|2[0-4]\d|1\d{2}|[1-9]\d|[1-9])\.)(?:(?:25[0-4]|2[0-4]\d|1\d{2}|[1-9]?\d)\.){2}(?:25[0-4]|2[0-4]\d|1\d{2}|[1-9]\d|[1-9]))$/);
// 							$('#init').val($('#init').val().toUpperCase());//添加缩写字母转大写的操作，防止有小写字母存入库中
// 							return result;
// 						},
// 						param:['F'],
// 						error:['不合法ip地址！']
					},{
						type:'text',
						validateObj:'firmtyp',
						validateType:['handler'],
						handler:function(){
							var result = true;
							if($('#typ').val() == '1203' && $('#firmtyp').val() == ''){
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'costtyp',
						validateType:['handler'],
						handler:function(){
							var result = true;
							if($('#typ').val() == '1203' && $('#costtyp').val() == ''){
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vplantyp',
						validateType:['handler'],
						handler:function(){
							var result = true;
							if($('#typ').val() == '1203' && $('#vplantyp').val() == ''){
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！']
					}]
				});
			});
			
			//编码不能有下划线_
			function checkCode(){
				var codeStr = $("#code").val();
				if(codeStr!=null){
					if(codeStr.indexOf("_")!=-1){
						$("#code").focus();
						return false;
					}
				}
				return true;
			}
			
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
			}
			
			//决策对应编码默认和门店编码一致
			function getFirm(e) {
				if ($('#firm').val()==null||$('#firm').val()==''){
					$('#firm').val($('#code').val());
				}
			}
		</script>
	</body>
</html>