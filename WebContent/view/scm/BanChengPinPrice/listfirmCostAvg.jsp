<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>中心费用</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css"> 
			.line{
				BORDER-LEFT-STYLE: none;
				BORDER-RIGHT-STYLE: none;
				BORDER-TOP-STYLE: none;
				margin:0px;
	 			width: 115px;
				height: 15px;
			}
		</style>
	</head>
	<body>
		<div class="grid"> 
			<div class="table-head">
				<div class="tool"></div>
				<table cellspacing="0" cellpadding="0" >
					<thead>
						<tr>
							<td style="text-align: center;">
								<span style="width: 50px;">
									<fmt:message key ="number" />
								</span>
							</td>
							<td style="text-align: center;">
								<span style="width: 80px;">
									<fmt:message key ="scm_cost_" />
								</span>
							</td>
							<td style="text-align: center;">
								<span style="width: 120px;">
									<fmt:message key ="amount" />
								</span>
							</td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody id="firmCostAvg">
					<c:forEach items="${firmCostAvgList }" var="firmCostAvg" varStatus="count">
						<tr>
						    <td style=" text-align: left;">
						     	<span style="width: 50px;" id="code">${firmCostAvg.fei.code }</span>
					     	</td>
					        <td style="text-align: left;">
						        <span style="width: 80px;" id="des">${firmCostAvg.fei.des}</span>
					        </td>
					        <td style="text-align: right: ;">
					        	<span style="width: 120px;">
					        		 <input class="line" name="amt" id="amt" type="text" onfocus="this.select()" value="${firmCostAvg.amt}" style="text-align: right;"/>
					        	</span>
					        </td>
					      </tr>
				       </c:forEach>
					</tbody>
					<tr>
					    <td style="text-align: left;">
					     	<span style="width: 50px;" id="heji"><fmt:message key ="total" />：</span>
				     	</td>
					    <td style="text-align: left;">
					     	<span style="width: 80px;"></span>
				     	</td>
				        <td style="text-align: left;">
					        <span style="width: 120px;text-align: right;" id="allAmt">${allAmt }</span>
					         <input id="month" type="hidden" value="${month }"/>
				        </td>
				   	</tr>
				</table>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				new tabTableInput("firmCostAvg","text"); //input  上下左右移动
				$(".tool").toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							savefirmCostAvg();
						}
					}]
				});

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'amt',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					}]
				});
				
				savefirmCostAvg=function(){
						/* var firmCostAvgSet={};
						var amt=$("input[id=amt]");
						var month=$("#month").val();
						amt.each(function(index){
							firmCostAvgSet["firmCostAvgList["+index+"].amt"]=$(this).val();
							firmCostAvgSet["firmCostAvgList["+index+"].monthh"]=month;
							alert($(this).val());
						});
						var code=$("span[id=code]");
						code.each(function(index){
							firmCostAvgSet["firmCostAvgList["+index+"].fei.code"]=$(this).text();
						}); */
						var selected = {};
						selected['monthh']=$("#month").val();
						var checkboxList = $('.grid').find('.table-body').find('tr');
						checkboxList.find('td:eq(1) span').each(function(i){
							if ($(this).text()!='') {
								selected['firmCostAvgList['+i+'].amt'] = $(this).parents('tr').find('td:eq(2) input').val();
								selected['firmCostAvgList['+i+'].monthh'] = $("#month").val();
								selected['firmCostAvgList['+i+'].fei.code'] = $(this).parents('tr').find('td:eq(0) span').text();
							}
						});
						
						if(validate._submitValidate()){
							$.ajax({
								url : '<%=path%>/BanChengPinPrice/savefirmCostAvg.do',
								type:'POST',
								async:false,
								data:selected,
								success : function(date) {
									if(date=="success"){
										showMessage({
				  	  						type: 'success',
				  	  						msg: '<fmt:message key ="save_successful" />！',
				  	  						speed: 2500
				  	  					});
										setTimeout(function(){window.location=window.location}, 2000);
									}else{
										showMessage({
				  	  						type: 'error',
				  	  						msg: '<fmt:message key ="save_fail" />！',
				  	  						speed: 2500
				  	  					});
										setTimeout(function(){window.location=window.location}, 2000);
									}
								}
							});
						}
				};
			});
		</script>
	</body>
</html>