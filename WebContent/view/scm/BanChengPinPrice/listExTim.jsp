<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>加工工时</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css"> 
			.line{
				BORDER-LEFT-STYLE: none;
				BORDER-RIGHT-STYLE: none;
				BORDER-TOP-STYLE: none;
				margin:0px;
	 			width: 100%;
				height: 15px;
			}
		</style>
	</head>
	<body>
		<div class="grid"> 
				<div class="table-head" style="position: static;margin: 0 auto;" >
				<div class="tool"></div>
					<table cellspacing="0" cellpadding="0" >
						<thead>
							<tr>
								<td><span style="width: 80px;"><fmt:message key="processing_room"/></span></td>
								<td><span style="width: 70px;"><fmt:message key="whether_semi"/><fmt:message key="coding"/></span></td>
								<td><span style="width: 70px;"><fmt:message key="whether_semi"/><fmt:message key="name"/></span></td>
								<td><span style="width: 70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width: 70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width: 70px;"><fmt:message key="scm_process_time"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="position: static;margin: 0 auto;">
					<table cellspacing="0" cellpadding="0">
						<tbody id="supplyAcct">
						<c:forEach items="${spCodeExPriceList }" var="spCodeExPrice" varStatus="count">
							<tr id="SAtr">
							    <td style="width:90px; text-align: left;">
							     	<span style="width: 80px;" id="code">
							     	${spCodeExPrice.supply.positnexdes}
							     	<input type="hidden" id="positnex" value="${spCodeExPrice.supply.positnex}"/>
							     	<input type="hidden" id="month" value="${month }"/>
							     	</span>
						     	</td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 70px;" id="sp_code">${spCodeExPrice.supply.sp_code}</span>
						        </td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 70px;" id="sp_name">${spCodeExPrice.supply.sp_name}</span>
						        </td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 70px;" id="unit">${spCodeExPrice.supply.unit}</span>
						        </td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 70px;" id="amount">${spCodeExPrice.amount}</span>
						        </td>
						        <td style="width:80px; text-align: left;">
							        <input type="text" style="width:80px;" class="line" onfocus="this.select()" id="extim" value="${spCodeExPrice.extim}"/>
						        </td>
						      </tr>
					       </c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		//Insert into SPCODEEXPRICE (Acct,Yearr,monthh,Positn,Sp_Code,ExTim,Amount)
			$(document).ready(function(){
				new tabTableInput("supplyAcct","text"); //input  上下左右移动
				$(".tool").toolbar({
					items: [{
						text: '<fmt:message key="save"/>',
						title: '<fmt:message key="save"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							saveExTim();
						}
					}]
				});

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'extim',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					}]
				});
				
				function saveExTim(){
					var selected={};
					$("tr[id=SAtr]").each(function(index){
						selected["spCodeExPriceList["+index+"].supply.positnex"]=$(this).find("#positnex").val();
						selected["spCodeExPriceList["+index+"].supply.sp_code"]=$(this).find("#sp_code").text();
						selected["spCodeExPriceList["+index+"].amount"]=$(this).find("#amount").text();
						selected["spCodeExPriceList["+index+"].Extim"]=$(this).find("#extim").val();
						selected["spCodeExPriceList["+index+"].monthh"]=$("#month").val();
					});
					if(validate._submitValidate()){
						$.ajax({
							url : '<%=path%>/BanChengPinPrice/saveExTim.do',
							type:'POST',
							async:false,
							data:selected,
							success : function(date) {
								if(date=="success"){
									showMessage({
			  	  						type: 'success',
			  	  						msg: '<fmt:message key ="save_successful" />！',
			  	  						speed: 2500
			  	  					});
									setTimeout(function(){window.location=window.location}, 2000);
								}else{
									showMessage({
			  	  						type: 'error',
			  	  						msg: '<fmt:message key ="save_fail" />！',
			  	  						speed: 2500
			  	  					});
									setTimeout(function(){window.location=window.location}, 2000);
								}
							}
						});
					}
				};
			});
		</script>
	</body>
</html>