<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>半成品价格计算</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.line{
			BORDER-LEFT-STYLE: none;
			BORDER-RIGHT-STYLE: none;
			BORDER-TOP-STYLE: none;
			width: 70px;
		}
	</style>
</head>
<body>
	<div class="bj_head">
		<div class="form-line">	
			<div class="form-label"><fmt:message key ="Center_cost" /></div>
			<div class="form-input" style="width:10px;">
				<input type="radio" checked="checked" id="typ" name="typ" value="1"/>
			</div>
			<div class="form-label"><fmt:message key ="processing_time" /></div>
			<div class="form-input" style="width:10px;">
				<input type="radio" id="typ" name="typ" value="2"/>
			</div>
			<div class="form-label"><fmt:message key ="Half_finished_product_cost" /></div>
			<div class="form-input" style="width:10px;">
				<input type="radio" id="typ" name="typ" value="3"/>
			</div>
			<div class="form-label"><fmt:message key ="months" /></div>
			<div class="form-input" style="width:10px;">
				<select id="month" name="month">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
			<div class="form-label"></div>
			<div class="form-input" style="width:10px;">
				<input type="button" style="width: 45px;" id="findBCP" onclick="findBanChengPin()" value="<fmt:message key ="select" />"/>
			</div>
		</div>
	</div>
	<div style="display: inline-block;width: 100%;" class="mainFrame">
		<iframe src="" style="overflow-x:scroll;" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
   	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		findBanChengPin=function(){
			var typ=$("input[name=typ]:checked").val();
			var month=$("#month").val();
			window.mainFrame.location="<%=path%>/BanChengPinPrice/findData.do?typ="+typ+"&month="+month;
		};
	});
	</script>
</body>
</html>