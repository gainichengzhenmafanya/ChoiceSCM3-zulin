<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>中心费用</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css"> 
			.line{
				BORDER-LEFT-STYLE: none;
				BORDER-RIGHT-STYLE: none;
				BORDER-TOP-STYLE: none;
				margin:0px;
	 			width: 100%;
				height: 15px;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form  id="listForm"  action="<%=path %>/BanChengPinPrice/findDataNew.do?typ=1" method="post">
			<input type='hidden' id='monthHid' value="${month }"/>
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="months" />:</div>
				<div class="form-input" >
					<select id="month" name="month" class="select" style="width:120px;">
						<option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="January" /></option>
						<option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="February" />&nbsp;&nbsp;&nbsp;&nbsp; </option>
						<option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="March" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="April" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="May" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="June" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="July" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="August" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="September" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="October" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="11">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="November" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<option value="12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="December" />&nbsp;&nbsp;&nbsp;&nbsp;</option>
					</select>
				</div>
			</div>
		</form>
		<div class="grid"> 
			<div class="table-head">
				<table cellspacing="0" cellpadding="0" >
					<thead>
						<tr>
							<td style="width:80px;text-align: center;">
								<span style="width: 70px;">
									<fmt:message key="number"/>
								</span>
							</td>
							<td style="width:80px;text-align: center;">
								<span style="width: 70px;">
									<fmt:message key="scm_cost_"/><fmt:message key="coding"/>
								</span>
							</td>
							<td style="width:80px;text-align: center;">
								<span style="width: 70px;">
									<fmt:message key="scm_cost_"/><fmt:message key="name"/>
								</span>
							</td>
							<td style="width:80px;text-align: center;">
								<span style="width: 70px;">
									<fmt:message key="scm_cost_"/><fmt:message key="amount"/>
								</span>
							</td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body" style="width: 325px;">
				<table cellspacing="0" cellpadding="0">
					<tbody id="firmCostAvg">
					<c:forEach items="${firmCostAvgList }" var="firmCostAvg" varStatus="count">
						<tr>
							<td style="width:80px; text-align: center;">
						     	<span style="width: 70px;" >${count.count}</span>
					     	</td>
						    <td style="width:80px; text-align: left;">
						     	<span style="width: 70px;" id="code">${firmCostAvg.fei.code }</span>
					     	</td>
					        <td style="width:80px; text-align: left;">
						        <span style="width: 70px;" id="des">${firmCostAvg.fei.des}</span>
					        </td>
					        <td style="width:80px; text-align: left;">
						        <input class="line" name="amt${count.count}" id="amt${count.count}" type="text" onfocus="this.select()" value="${firmCostAvg.amt}" onblur="subValue(this)" disabled style="text-align: right;"/>
					        </td>
					      </tr>
				       </c:forEach>
						<tr style="height:200px;">
						    <td style="width:80px; text-align: left;">
						     	<span style="width: 70px;" id="heji"></span>
					     	</td>
					     	<td style="width:80px; text-align: left;">
						     	<span style="width: 70px;" ></span>
					     	</td>
						    <td style="width:80px; text-align: left;">
						     	<span style="width: 70px;"></span>
					     	</td>
					        <td style="width:80px; text-align: left;">
						        <span style="width: 70px;" id="allAmt"></span>
					        </td>
					   	</tr>
					</tbody>
					<tr>
					    <td style="width:80px; text-align: left;">
					     	<span style="width: 70px;" id="heji"><fmt:message key="total"/>：</span>
				     	</td>
					    <td style="width:80px; text-align: left;">
					     	<span style="width: 70px;"></span>
				     	</td>
				     	<td style="width:80px; text-align: left;">
					     	<span style="width: 70px;"></span>
				     	</td>
				        <td style="width:80px; text-align: left;">
					        <span style="width: 70px;text-align: right;" id="allAmt">${allAmt }</span>
					         <input id="month" type="hidden" value="${month }"/>
				        </td>
				   	</tr>
				</table>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#month').val($('#monthHid').val());
				new tabTableInput("firmCostAvg","text"); //input  上下左右移动
				$(".tool").toolbar({
					items: [{
						text: '<fmt:message key="select"/>',
						title: '<fmt:message key="select"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key="edit"/>',
						title: '<fmt:message key="edit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							$('input:text').attr('disabled',false);
						}
					},{
						text: '<fmt:message key="save"/>',
						title: '<fmt:message key="save"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							savefirmCostAvg();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'amt1',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt2',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt3',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt4',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt5',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt6',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					},{
						type:'text',
						validateObj:'amt7',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="please_enter_positive_integer" />！']
					}]
				});
				
				savefirmCostAvg=function(){
						/* var firmCostAvgSet={};
						var amt=$("input[id=amt]");
						var month=$("#month").val();
						amt.each(function(index){
							firmCostAvgSet["firmCostAvgList["+index+"].amt"]=$(this).val();
							firmCostAvgSet["firmCostAvgList["+index+"].monthh"]=month;
							alert($(this).val());
						});
						var code=$("span[id=code]");
						code.each(function(index){
							firmCostAvgSet["firmCostAvgList["+index+"].fei.code"]=$(this).text();
						}); */
						var selected = {};
						selected['monthh']=$("#month").val();
						var checkboxList = $('.grid').find('.table-body').find('tr');
						checkboxList.find('td:eq(1) span').each(function(i){
							if ($(this).text()!='') {
								selected['firmCostAvgList['+i+'].amt'] = $(this).parents('tr').find('td:eq(3) input').val()?$(this).parents('tr').find('td:eq(3) input').val():0;
								selected['firmCostAvgList['+i+'].monthh'] = $("#month").val();
								selected['firmCostAvgList['+i+'].fei.code'] = $(this).parents('tr').find('td:eq(1) span').text();
							}
						});
						
						if(validate._submitValidate()){
							$.ajax({
								url : '<%=path%>/BanChengPinPrice/savefirmCostAvg.do',
								type:'POST',
								async:false,
								data:selected,
								success : function(date) {
									if(date=="success"){
										showMessage({
				  	  						type: 'success',
				  	  						msg: '<fmt:message key ="save_successful" />！',
				  	  						speed: 2500
				  	  					});
										setTimeout(function(){window.location=window.location}, 2000);
									}else{
										showMessage({
				  	  						type: 'error',
				  	  						msg: '<fmt:message key ="save_fail" />！',
				  	  						speed: 2500
				  	  					});
										setTimeout(function(){window.location=window.location}, 2000);
									}
								}
							});
						}
				};
			});
			//失去焦点，保存两位小数 wjf
			function subValue(obj){
				$(obj).val(Number($(obj).val()).toFixed(2));
			}
		</script>
	</body>
</html>