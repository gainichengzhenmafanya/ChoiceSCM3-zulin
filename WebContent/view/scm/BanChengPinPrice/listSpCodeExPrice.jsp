<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>半成品费用</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css"> 
			.line{
				BORDER-LEFT-STYLE: none;
				BORDER-RIGHT-STYLE: none;
				BORDER-TOP-STYLE: none;
				margin:0px;
	 			width: 100%;
				height: 15px;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
	<div class="mainFrame" style="height: 50%;">
		<div class="grid"> 
			<div class="table-head" style="position: static;margin: 0 auto;" >
			<div class="tool"></div>
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
				            <td rowspan="2"><span style="width: 80px;"><fmt:message key ="processing_room" /></span></td>
							<td colspan="6"><span style="width: 470px;"><fmt:message key ="whether_semi" /></span></td>
				            <td rowspan="2"><span style="width: 70px;"><fmt:message key ="scm_process_time" /></span></td>
				            <td rowspan="2"><span style="width: 70px;"><fmt:message key ="the_cost_of_raw_materials" /></span></td>
				            <td rowspan="2"><span style="width: 70px;"> <fmt:message key ="scm_cost" /></span></td>
				         </tr>
				         <tr>
							<td><span style="width: 70px;"><fmt:message key ="coding" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="name" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="unit" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="the_output_of_this_month" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="cost_price" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="cost_amount" /></span></td>
				   		 </tr>
					</thead>
				</table>
			</div>
			<div class="table-body" style="position: static;margin: 0 auto;">
				<table cellspacing="0" cellpadding="0">
					<tbody id="supplyAcct">
					<c:forEach items="${spCodeExPriceList }" var="spCodeExPrice" varStatus="count">
						<tr id="SAtr">
						    <td style="width:90px; text-align: left;">
						     	<span style="width: 80px;" id="code">${spCodeExPrice.supply.positnexdes}
							     	<input type="hidden" id="positnex" value="${spCodeExPrice.supply.positnex}"/>
							     	<input type="hidden" id="month" value="${month}"/>
						     	</span></td>
					        <td><span style="width: 70px;" id="sp_code">${spCodeExPrice.supply.sp_code}</span></td>
					        <td><span style="width: 70px;" id="sp_name">${spCodeExPrice.supply.sp_name}</span></td>
					        <td><span style="width: 70px;" id="unit">${spCodeExPrice.supply.unit}</span></td>
					        <td><span style="width: 70px;" id="amount">${spCodeExPrice.amount}</span></td>
					        <td><span style="width: 70px;" id="priceout"><fmt:formatNumber value="${spCodeExPrice.price}" type="currency" pattern="0.00"/></span></td>
					        <td><span style="width: 70px;" id="amt"><fmt:formatNumber value="${spCodeExPrice.amt}" type="currency" pattern="0.00"/></span></td>
					        <td><span style="width: 70px;" id="extim">${spCodeExPrice.extim}</span></td>
					        <td><span style="width: 70px;" id="spCost">${spCodeExPrice.spcost}</span></td>
					        <td><span style="width: 70px;" id="feiCost">${spCodeExPrice.feicost}</span></td>
					      </tr>
				       </c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="leftFrame" style="width: 60%;">
		<div class="grid"> 
			<div class="table-head" style="width: 507px;" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td colspan="6"><fmt:message key ="the_cost_of_raw_materials" /></td></tr>
				         <tr>
							<td><span style="width: 90px;"><fmt:message key ="shouyewuzimc" /></span></td>
							<td><span style="width: 70px;"><fmt:message key ="unit" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="scm_consumption" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="unit_price" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="amount" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="theoretical_amount" /></span></td>
				   		 </tr>
					</thead>
				</table>
			</div>
			<div class="table-body" style="width: 507px;">
				<table cellspacing="0" cellpadding="0">
					<tbody id="YuanLiaoChengBen">
						<tr id="YLCBtr">
						    <td><span style="width: 90px;" id="sp_name">${spcodeexm.sp_name}</span></td>
					        <td><span style="width: 70px;" id="unit">${spcodeexm.unit}</span></td>
					        <td><span style="width: 70px;" id="cnt">${spcodeexm.cnt}</span></td>
					        <td><span style="width: 70px;" id="unit">${spcodeexm.sp_name}</span></td>
					        <td><span style="width: 70px;" id="unit">${spcodeexm.sp_name}</span></td>
					        <td><span style="width: 70px;" id="unit">${spcodeexm.sp_name}</span></td>
					      </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="rightFrame" style="float: left;width: 40%;">
		<div class="grid"> 
			<div class="table-head" style="width: 233px;" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td colspan="6"><fmt:message key ="scm_cost" /></td>
						</tr>
				        <tr>
							<td><span style="width: 140px;"><fmt:message key ="expenses_name" /></span></td>
				            <td><span style="width: 70px;"><fmt:message key ="amount" /></span></td>
				   		 </tr>
					</thead>
				</table>
			</div>
			<div class="table-body" style="width: 233px;">
				<table cellspacing="0" cellpadding="0">
					<tbody id="FeiYongChengBen">
						<tr id="FYCBtr">
						    <td><span style="width: 140px;" id="code"></span></td>
					        <td><span style="width: 70px;" id="sp_code"></span></td>
					     </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			new tabTableInput("supplyAcct","text"); //input  上下左右移动
			$(".tool").toolbar({
				items: [{
					text: '<fmt:message key ="the_re_computation_of_price_of_semi-finished_products" />',
					title: '<fmt:message key ="the_re_computation_of_price_of_semi-finished_products" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','0px']
					},
					handler: function(){
						calculate();
					}
				}]
			});	
			var extimSum = 0;
			$('.mainFrame').find('.table-body').find('tr').each(function () {
				extimSum+=Number($(this).find('td:eq(7)').find('span').text());
			});
			$('.mainFrame').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$('.mainFrame').find('.table-body').find('tr').not(this).removeClass('tr-select');
				spcode = $(this).find('td:eq(1)').find('span').text();
				cnt = $(this).find('td:eq(4)').find('span').text();
				extim = $(this).find('td:eq(7)').find('span').text();
				month = $('#month').val();
				positn = $(this).find('td:eq(0)').find('span').find('input:eq(0)').val();
				//查询原料成本
				$.ajax({
					type: "POST",
					url: '<%=path%>/BanChengPinPrice/findProductList.do',
					data: "item="+spcode+"&monthh="+month,
					dataType: 'json',
					success:function(productList){
						$('.leftFrame').find('table').find('tbody').empty();
						for(var i=0;i<productList.length;i++){
							$('.leftFrame').find('table').find('tbody').append('<tr><td><span title='
								+productList[i].supply.sp_name+'" style="width:90px;">'
								+productList[i].supply.sp_name+'</span></td><td><span title="'
								+productList[i].supply.unit+'" style="width:70px;">'
								+productList[i].supply.unit+'</span></td><td><span title="'
								+0+'" style="width:70px;">0.0</span></td><td><span title="'
								+0+'" style="width:70px;">0.0</span></td><td><span title="'
								+0+'" style="width:70px;">0.0</span></td><td><span title="'
								+(productList[i].cnt*cnt).toFixed(2)+'" style="width:70px;">'
								+(productList[i].cnt*cnt).toFixed(2)+'&nbsp;</span></td></tr>');
						}
					},
					error: function(){
						alert('<fmt:message key="server_busy_please_try_again"/>');
						}
					});
		      	
				//查询费用成本
				$.ajax({
					type: "POST",
					url: '<%=path%>/BanChengPinPrice/findSpexfeicostList.do',
					data: "monthh="+month+"&item="+spcode+"&firm="+positn,
					dataType: 'json',
					success:function(spexfeicostList){
						$('.rightFrame').find('table').find('tbody').empty();
						for(var i=0;i<spexfeicostList.length;i++){
							$('.rightFrame').find('table').find('tbody').append('<tr><td><span title='
								+spexfeicostList[i].fei.des+'" style="width:140px;">'
								+spexfeicostList[i].fei.des+'</span></td><td><span title="'
								+(spexfeicostList[i].amt).toFixed(2)+'" style="width:70px;">'
								+(spexfeicostList[i].amt).toFixed(2)+'</span></td></tr>');
						}
					}
				});
			 });
		});
		
		function calculate() {
			var extimSum = 0;
			$('.mainFrame').find('.table-body').find('tr').each(function () {
				extimSum+=Number($(this).find('td:eq(7)').find('span').text());
			});
			var month = $('#month').val();
			//查询原料成本
			$.ajax({
				type: "POST",
				url: '<%=path%>/BanChengPinPrice/calculate.do',
				data: "month="+month+"&extimSum="+extimSum,
				dataType: 'json',
				success:function(date){
					showMessage({
	  						type: 'success',
	  						msg: '<fmt:message key ="Computational_success" />！',
	  						speed: 2500
	  					});
					setTimeout(function(){window.location=window.location}, 2000);
				},
				error: function(){
					showMessage({
	  						type: 'error',
	  						msg: '<fmt:message key ="Computational_failure" />！',
	  						speed: 2500
	  					});
					setTimeout(function(){window.location=window.location}, 2000);
				}
			});
		}
		</script>
	</body>
</html>