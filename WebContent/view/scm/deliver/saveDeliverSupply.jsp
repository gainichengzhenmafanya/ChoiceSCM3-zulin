<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_add" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	    	
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:-5px;margin-top:-5px;">
		<div class="form">
			<form id="saveDeliverSupply" action="<%=path%>/deliver/tosaveDeliverSupply.do?typ=1" method="post">
				<input type="hidden" id="delivercode" name="delivercode" value="${delivercode}"/>
<!-- 				<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" > -->
<!-- 					<tr> -->
<!-- 				        <td> -->
<!-- 				        	<input type="button" id="search" style="width: 60px;" name="search" value='添加物资'/> -->
<!-- 				        </td> -->
<!-- 				        <td> -->
<!-- 				        	<input type="button" id="getSearch" style="width: 60px;" name="getSearch" value='删除'/> -->
<!-- 				        </td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td style="width:30px;">&nbsp;</td>
									<td style="width:30px;text-align: center;">
										
									</td>
									<td><span style="width:80px;"></span><fmt:message key ="supplies_code" /></td>
									<td><span style="width:100px;"></span><fmt:message key ="supplies_name" /></td>
									<td><span style="width:50px;"></span><fmt:message key ="unit" /></td>
									<td><span style="width:80px;"></span><fmt:message key ="specification" /></td>
									<td><span style="width:40px;"></span><fmt:message key ="unit_price" /></td>
									<td><span style="width:100px;"></span><fmt:message key ="category" /></td>
									<td><span style="width:100px;"></span><fmt:message key ="Mall_goods_code" /></td>
									<td><span style="width:100px;"></span><fmt:message key ="Mall_goods_name" /></td>
									<td><span style="width:100px;"></span>
									<fmt:message key = "tax_rate" />
										<select onchange="changetaxp(this);">
											<!-- 税率 -->
											<c:forEach var="itemtax" items="${listTax}">
													<option name="${itemtax.taxdes}" value="${itemtax.tax}">${itemtax.taxdes}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" id = "addTable" class = "addTable">
							<tbody>
								<c:forEach var="materialScope" varStatus="step" items="${materialScopeList}">
									<tr>
										<td class="num" style="width:30px;">${step.count}</td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${materialScope.pk_materialscope}" value="${materialScope.pk_materialscope}"/>
										</td>
										<td><span title="" style="width:80px;text-align: left;">${materialScope.sp_code}</span></td>
										<td><span title="" style="width:100px;text-align: left;">${materialScope.sp_name}</span></td>
										<td><span title="" style="width:50px;text-align: left;">${materialScope.unit}</span></td>
										<td><span title="" style="width:80px;text-align: left;">${materialScope.sp_desc}</span></td>
										<td><span title="" style="width:40px;text-align: right;">
											${materialScope.sp_price}
										</span></td>
										<td><span title="" style="width:100px;text-align: left;">${materialScope.typdes}</span></td>
										<td><span title="" style="width:100px;text-align: left;">${materialScope.vcodejmu}</span></td>
										<td><span title="" style="width:100px;text-align: left;">${materialScope.vnamejmu}</span></td>
										<td><span title="" style="width:100px;text-align: left;display: none;">${materialScope.pk_materialjmu}</span></td>
										<td><span title="" style="width:100px;text-align: left;display: none;">${materialScope.vhspec}</span></td>
										
										<td>
										<span title="" style="width:100px;text-align: left;">
										<select class="taxclass">
											<!-- 税率 -->
											<c:forEach var="itemtax" items="${listTax}">
												<c:if test="${itemtax.tax==materialScope.tax}">
													<option name="${itemtax.taxdes}" value="${itemtax.tax}" selected>${itemtax.taxdes}</option>
												</c:if>
												<c:if test="${itemtax.tax!=materialScope.tax}">
													<option name="${itemtax.taxdes}" value="${itemtax.tax}">${itemtax.taxdes}</option>
												</c:if>
											</c:forEach>
										</select>
										</span>
										</td>
										
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>										
			</form>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
		
		//批量修改税率 jinshuai 20160425
		function changetaxp(select){
			$(".taxclass").val(select.value);
		}
		
		var validate;
			$(document).ready(function(){
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),26);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				//按钮快捷键
				focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
			 		 
				if("error"==$("#str").val()){
					alert('<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！');
					/* showMessage({
						type: 'error',
						msg: '<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！',
						speed: 1000
					}); */
				}				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'warmDays',
						validateType:['canNull','num'],
						param:['F','F'],
						error:['<fmt:message key ="cannot_be_empty" />','<fmt:message key ="stay_only_as_an_integer" />']
					}]
				});
			});
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
				return true;
			}
			
			//保存
			function updateSprice(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				materialScopeList = {};
				if (checkboxList) {
					checkboxList.each(function(i){
						materialScopeList["listMaterialScope["+ i +"].pk_materialscope"] = $(this).val();
						materialScopeList["listMaterialScope["+ i +"].sp_price"] = $.trim($(this).parent("td").parent("tr").find("td:eq(6) span").text());
					});
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/deliver/updateDeliverSupply.do",materialScopeList,function(data){
					if(data){
						alert("<fmt:message key ="operation_successful" />！");
					} else {
						alert("<fmt:message key ="operation_failed" />！");
					};
				});
				parent.$('.close').click();
			}
			
			//保存
			//保存税率 jinshuai 20160422
			function updateTaxRate(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				materialScopeList = {};
				if (checkboxList) {
					checkboxList.each(function(i){
						materialScopeList["listMaterialScope["+ i +"].pk_materialscope"] = $(this).val();
						materialScopeList["listMaterialScope["+ i +"].tax"] = $.trim($(this).parent("td").parent("tr").find("option:selected").val());
						materialScopeList["listMaterialScope["+ i +"].taxdes"] = $.trim($(this).parent("td").parent("tr").find("option:selected").attr('name'));
					});
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/deliver/updateTaxRate.do",materialScopeList,function(data){
					if(data){
						alert("<fmt:message key ="operation_successful" />！");
					} else {
						alert("<fmt:message key ="operation_failed" />！");
					};
				});
				parent.$('.close').click();
			}
			
			
			//删除物资
			function deleteSupply(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/deliver/deleteDeliverSupply.do?code='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="suppliers" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
			
			//冲洗加载页面
			function pageReload(){
				$('#saveDeliverSupply').submit();
			}
			
			//关联商城物资
			function saveJmuSupply(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
					checkboxList.filter(':checked').each(function(i){
						var sp_code = $.trim($(this).closest('tr').find('td').eq(2).find('span').text());
						var sp_name = $.trim($(this).closest('tr').find('td').eq(3).find('span').text());
						var vcodejmu = $.trim($(this).closest('tr').find('td').eq(8).find('span').text());
						var delivercode = $('#delivercode').val();
						if(vcodejmu != null && vcodejmu != ''){
							alert('当前物资已经关联商城物资，不可重复关联！');
							return;
						}
						var action = encodeURI('<%=path%>/deliver/supplyJmuList.do?sp_code='+sp_code+'&delivercode='+delivercode+'&sp_name='+encodeURI(sp_name));
						$('body').window({
							title: '<fmt:message key="relevance_supplier" />',
							content: '<iframe id="updateJmuSupplyFrame" name="updateJmuSupplyFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '550px',
							height: '400px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-18px','-0px']
										},
										handler: function(){
											if(getFrame('updateJmuSupplyFrame')){
												getFrame('updateJmuSupplyFrame').saveJmuSupply();
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}]
							}
						});
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除商城物资
			function deleteJmuSupply(){
				var data={};
				data['delivercode'] = $('#delivercode').val();
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					checkboxList.filter(':checked').each(function(i){
						if(confirm('<fmt:message key="delete_data_confirm" />？')){
							var chkValue = [];
							checkboxList.filter(':checked').each(function(){
								chkValue.push($.trim($(this).closest('tr').find('td').eq(2).find('span').text()));
							});
							$.post("<%=path%>/deliver/deleteJumSupply.do?code="+chkValue.join(","),data,function(data){
								var rs = data;
		                        if(isNaN(rs)){
		                            alert('删除关联失败！');
		                            return;
		                        }
								switch(Number(rs)){
								case -1:
									alert('删除关联失败！');
									break;
								case 1:
									showMessage({
										type: 'success',
										msg: '<fmt:message key ="successful_deleted" />！！',
										speed: 3000,
										handler:function(){
											pageReload();
										}
									});
									break;
								}
							});
						}
					});
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
		</script>				
	</body>
</html>