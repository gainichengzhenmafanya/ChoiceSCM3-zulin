<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_add" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	    	
	</head>
	<body>
		<input type="hidden" id="str" name="str" value="${str }"/>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:-10px;margin-top:0px;">
		<div class="form">
			<form id="DeliverForm" method="post" action="<%=path %>/deliver/saveByAdd.do">
				<div class="form-line">	
					<div class="form-label"><span class="red">*</span><fmt:message key="suppliers_typ" />：</div>
					<div class="form-input">
						<select id="typ" name="typ" class="select" style="width:133px;">
						    <option selected="selected" disabled="disabled"></option>
						    <c:forEach var="codeDes" items="${codeDesList}" varStatus="status">
						    	<option value="${codeDes.des }"
									<c:if test="${typCode eq codeDes.code }"> selected="selected" </c:if>
								>${codeDes.des }</option>
						    </c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="tax_id" />：</div>
					<div class="form-input"><input type="text" id="taxcode" name="taxcode" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="suppliers_coding" />：</div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" value=""/>
					</div>
					<div class="form-label"><fmt:message key="material_remind_days"/>：</div>
					<div class="form-input"><input type="text" id=warnDays name="warnDays" class="text" value="10"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="suppliers_name" />：</div>
					<div class="form-input"><input type="text" id="des" name="des" class="text" onkeypress="return checkKey();" onblur="getSpInit(this);"/></div>
					<div class="form-label"></div>
					<div class="form-input"><input type="checkbox" name="yntax" id="yntax" value="Y"/>&nbsp;<fmt:message key="whether_general_taxpayer" /></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="abbreviation" />：</div>
					<div class="form-input"><input type="text" id="init" name="init" class="text" onkeypress="return checkKey();" /></div>
					<div class="form-label"></div>
					<div class="form-input"><input type="checkbox" name="credit" id="credit" value="Y"/>&nbsp;<fmt:message key="whether_invoicing" /></div>
				</div>
				
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="person_in_charge" />：</div>
					<div class="form-input"><input type="text" id="person" name="person" class="text" onkeypress="return checkKey();"/></div>
					<div class="form-label"><fmt:message key="bank" />：</div>
					<div class="form-input"><input type="text" id="bank" name="bank" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="contact" />：</div>
					<div class="form-input"><input type="text" id="person1" name="person1" class="text"/></div>
					<div class="form-label"><fmt:message key="bank_account" />：</div>
					<div class="form-input"><input type="text" id="bankcode" name="bankcode" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="address" />：</div>
					<div class="form-input"><input type="text" id="addr" name="addr" class="text"/></div>
					<div class="form-label"><fmt:message key ="Account_type" />：</div>
					<div class="form-input">
						<select id="iaccttype" name="iaccttype" class="select" style="width:134px;">
							<option value="0"><fmt:message key ="immediately" /></option>
							<option value="1"><fmt:message key ="January" /></option>
							<option value="2"><fmt:message key ="February" /></option>
							<option value="3"><fmt:message key ="quarter" /></option>
							<option value="4"><fmt:message key ="half_a_year" /></option>
							<option value="5"><fmt:message key ="A_year" /></option>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="email" />：</div>
					<div class="form-input"><input type="text" id="mail" name="mail" class="text" onblur="alert(fChkMail(document.getElementById('mail').value));" /></div>
					<div class="form-label"><fmt:message key="Expiration_date" />：</div>
					<div class="form-input"><input type="text" id="expirydate" name="expirydate" class="Wdate text" onfocus="new WdatePicker({minDate:new Date()})"/></div>
<%-- 					<div class="form-label"><fmt:message key="relevance_supplier" />：</div> --%>
<!-- 					<div class="form-input"> -->
<%-- 						<input type="text" id="jmudelivername" name="jmudelivername" class="text" readonly="readonly" value="${deliver.jmudelivername}"/> --%>
<%-- 						<input type="hidden" id="jmudelivercode" name="jmudelivercode" value="${deliver.jmudelivercode}"/> --%>
<%-- 						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
<!-- 					</div> -->
				</div>
				
				<div class="form-line">
					<div class="form-label"><fmt:message key="www" />：</div>
					<div class="form-input"><input type="text" id="www" name="www" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="fax" />：</div>
					<div class="form-input"><input type="text" id="tel" name="tel" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="tel" />：</div>
					<div class="form-input"><input type="text" id="tel1" name="tel1" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label" style="height: 50px;"><fmt:message key="remark" />：</div>
					<div class="form-input" style="height: 50px;">
						<textarea id="memo" name="memo" style="height: 44px;width: 370px;"></textarea>
					</div>
				</div>
				
			</form>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				$("#code").focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
			 		 
				if("error"==$("#str").val()){
					alert('<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！');
					/* showMessage({
						type: 'error',
						msg: '<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！',
						speed: 1000
					}); */
				}
				
				//回车输入
		 	 	$('input:text:eq(0)').focus();
		        var $inp = $('input:text');
		        $inp.bind('keydown', function (e) {
		            var key = e.which;
		            if (key == 13) {
		                e.preventDefault();
		                var nxtIdx = $inp.index(this) + 1;
		                $(":input:text:eq(" + nxtIdx + ")").focus();
		            }
		        });
				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'typ',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="suppliers_coding" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="suppliers_coding" /><fmt:message key ="the_maximum_length" />10！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/deliver/findDeliverByCode.do",{code:$("#code").val()},function(data){
								if($.trim(data)) result = false;
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['canNull','handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/deliver/findDeliverByCode.do",{des:$("#des").val()},function(data){
								if($.trim(data)) result = false;
							});
							return result;
						},
						param:['F','F'],
						error:['<fmt:message key="suppliers_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="suppliers_name" /><fmt:message key="already_exists" />']
					},{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:['40'],
						error:['<fmt:message key="suppliers_name" /><fmt:message key ="the_maximum_length" />40！']
					},{
						type:'text',
						validateObj:'init',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="abbreviation" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'init',
						validateType:['maxLength'],
						param:['20'],
						error:['<fmt:message key="abbreviation" /><fmt:message key ="the_maximum_length" />20！']
					},{
						type:'text',
						validateObj:'person',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="person_in_charge" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'person',
						validateType:['maxLength'],
						param:['20'],
						error:['<fmt:message key="person_in_charge" /><fmt:message key ="the_maximum_length" />20！']
					},{
						type:'text',
						validateObj:'person1',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="contact" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'person1',
						validateType:['maxLength'],
						param:['20'],
						error:['<fmt:message key="contact" /><fmt:message key ="the_maximum_length" />20！']
					},{						
						type:'text',
						validateObj:'warnDays',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="stay_only_as_an_integer" />！']
					},{
						type:'text',
						validateObj:'addr',
						validateType:['maxLength'],
						param:['25'],
						error:['<fmt:message key="address" /><fmt:message key ="the_maximum_length" />25！']
					},{
						type:'text',
						validateObj:'mail',
						validateType:['maxLength','email'],
						param:['100','T'],
						error:['<fmt:message key="email" /><fmt:message key ="the_maximum_length" />100！','<fmt:message key ="incorrect_format" />']
					},{
						type:'text',
						validateObj:'www',
						validateType:['maxLength','handler'],
						handler:function(){
							if($('#www').val()=='') return true;
							var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
							        + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
							        + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
							        + "|" // 允许IP和DOMAIN（域名）
							        + "([0-9a-z_!~*'()-]+\.)*" // 域名- www.
							        + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // 二级域名
							        + "[a-z]{2,6})" // first level domain- .com or .museum
							        + "(:[0-9]{1,4})?" // 端口- :80
							        + "((/?)|" // a slash isn't required if there is no file name
							        + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
					        var re=new RegExp(strRegex);
					        if (re.test($('#www').val())){
					            return (true);
					        }else{
					            return (false);
					        }
						},
						param:['50','F'],
						error:['<fmt:message key ="the_maximum_length" />50！','<fmt:message key ="incorrect_format" />']
					},{
						type:'text',
						validateObj:'tel',
						validateType:['maxLength','telephone'],
						param:['20','T'],
						error:['<fmt:message key ="the_maximum_length" />20！','<fmt:message key ="incorrect_format" />']
					},{
						type:'text',
						validateObj:'tel1',
						validateType:['maxLength'],
						param:['20'],
						error:['<fmt:message key="tel" /><fmt:message key ="the_maximum_length" />20！']
					},{
						type:'text',
						validateObj:'bank',
						validateType:['maxLength'],
						param:['15'],
						error:['<fmt:message key="bank" /><fmt:message key ="the_maximum_length" />15！']
					},{
						type:'text',
						validateObj:'bankcode',
						validateType:['maxLength'],
						param:['20'],
						error:['<fmt:message key="bank_account" /><fmt:message key ="the_maximum_length" />20！']
					},{
						type:'text',
						validateObj:'taxcode',
						validateType:['maxLength'],
						param:['15'],
						error:['<fmt:message key="tax_id" /><fmt:message key ="the_maximum_length" />15！']
					},{
						type:'text',
						validateObj:'memo',
						validateType:['maxLength'],
						param:['50'],
						error:['<fmt:message key="remark" /><fmt:message key ="the_maximum_length" />50！']
					}]
				});
			});
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
				return true;
			}
		</script>				
	</body>
</html>