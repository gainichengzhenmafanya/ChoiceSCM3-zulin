<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_add" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	    	
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:0px;margin-top:0px;">
		<div class="form">
			<form id="DeliverForm" method="post" action="<%=path %>/deliver/saveByAddWarmDays.do">
				<input type="hidden" id="typCode" name="typCode" value="${typCode }"/>
				<div class="form-line">	
					<div class="form-label"><span class="red">*</span><fmt:message key="material_remind_days"/>：</div>
					<div class="form-input">
						<input type="text" id="warmDays" name="warmDays" class="text" value="" default="10"/><br/>
						<span class="red"><fmt:message key ="System_default_alert_number_of_days_for_10_days" /></span>
					</div>
				</div>
			</form>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>			
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
			 		 
				if("error"==$("#str").val()){
					alert('<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！');
					/* showMessage({
						type: 'error',
						msg: '<fmt:message key="suppliers_coding" /><fmt:message key="already_exists" />！',
						speed: 1000
					}); */
				}				
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'warmDays',
						validateType:['canNull','num1'],
						param:['F','F'],
						error:['<fmt:message key ="cannot_be_empty" />','<fmt:message key ="stay_only_as_an_integer" />']
					}]
				});
			});
			function checkKey(){
				if(event.keyCode==39){
					return false;
				}
				return true;
			}
		</script>				
	</body>
</html>