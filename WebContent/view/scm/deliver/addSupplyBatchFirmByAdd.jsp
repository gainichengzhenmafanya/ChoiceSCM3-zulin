<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="supplies_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			.topFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.bottomFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.check{
				float: left;
				vertical-align: middle;
				margin-right: 0px;
				height: 30px;
				line-height: 30px;
				text-align: right;
				font-weight: bold;
				padding-right: 5px;
				background-color: #E1E1E1;
			}
			.formInput {
				height: 30px;
				vertical-align: middle;
				line-height: 30px;
				text-align: left;
				padding-left: 5px;
				background-color: #E1E1E1;
				white-space: normal;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<div class="topFrame">
		<input type="hidden" id = "delivercode" name = "delivercode" value="${chkValue}" /> 
		<form id="supplyBatchForm" method="post" action="">
			<div class="form-line" style="background-color: #E1E1E1;height: 80px;;margin-right:0px;" id="supplyForm">
				<fmt:message key="supplies"/>:<br/>
				<!-- <div style="overflow: auto;height: 60px;background-color: #E1E1E1;" id="addPoEle"></div> -->
				<input type="hidden" id="parentId" name="code" class="text" readonly="readonly" value="${defaultCode}"/>
				<textarea style="width:100%; height:50px; boder:0px;background-color: #E1E1E1;" id="parentName" name="name" class="text" readonly="readonly">
				</textarea>
				
			</div>
		</form>
	</div>
	<div class="bottomFrame" style="height:85%">
		<iframe src="<%=path%>/firmSupply/selectNSupply.do" frameborder="0" name="choiceFrame" id="choiceFrame"></iframe>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//判断虚拟物料实际物料是不是一对一
				var is_supply_1to1 = '${is_supply_1to1}';
				if(is_supply_1to1 == 'Y'){
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="enter"/>',
								title: '<fmt:message key="determine_materials_selection"/>',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-120px','0px']
								},
								handler: function(){
									select_Supply();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-38px','0px']
								},
								handler: function(){
									parent.$('.close').click();
								}
							},{
								text: '<fmt:message key ="empty" />',
								title: '<fmt:message key ="empty" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-20px']
								},
								handler: function(){
									chkCode.splice(0,chkCode.length);
									chkName.splice(0,chkName.length);
									$('#supplyBatchForm').find('div').find('input').val(chkCode);
									$('#supplyBatchForm').find('div').find('textarea').val(chkName);
									window.frames["choiceFrame"].clearValue();
								}
							}]
					});
				}else{
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="enter"/>',
								title: '<fmt:message key="determine_materials_selection"/>',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-120px','0px']
								},
								handler: function(){
									select_Supply();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-38px','0px']
								},
								handler: function(){
									parent.$('.close').click();
								}
							},{
								text: '<fmt:message key ="empty" />',
								title: '<fmt:message key ="empty" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-20px']
								},
								handler: function(){
									chkCode.splice(0,chkCode.length);
									chkName.splice(0,chkName.length);
									$('#supplyBatchForm').find('div').find('input').val(chkCode);
									$('#supplyBatchForm').find('div').find('textarea').val(chkName);
									window.frames["choiceFrame"].clearValue();
								}
							},{
								text: '<fmt:message key ="bigClass" />',
								title: '<fmt:message key ="bigClass" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									position: ['0px','-20px']
								},
								handler: function(){
									findDivisionSupply();
								}	
							},{
								text: '<fmt:message key ="middleClass" />',
								title: '<fmt:message key ="middleClass" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-20px']
								},
								handler: function(){
									findGroupTyp();
								}
							},{
								text: '<fmt:message key ="smallClass" />',
								title: '<fmt:message key ="smallClass" />',
								//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-20px']
								},
								handler: function(){
									findSmallClass();
								}
							}]
					});
				}
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
			});
			function select_Supply(){
				
				var code = $('#parentId').val();    
				var delivercode = $("#delivercode").val();
				parent.select_Supply(delivercode,code);
			}
			
			var defaultCode = '${defaultCode}';
			var defaultName = '${defaultName}';
			if(defaultCode!='' && defaultName!=''){
				$('#supplyBatchForm').find('div').find('input').val(defaultCode);
				$('#supplyBatchForm').find('div').find('textarea').val(defaultName);
			}
			var chkCode = defaultCode==''?[]:defaultCode.split(',');
			var chkName = defaultName==''?[]:defaultName.split(',');
			function selectSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectAllSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					return;
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectZeroSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					return;
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			function findDivisionSupply(){
				 $('body').window({
						title: '<fmt:message key="bigClass"/>',
						content: '<iframe id="findDivisionSupplyFrame" frameborder="0" src="<%=path%>/deliver/findDivisionSupply.do"></iframe>',
						width: '550px',
						height: '380px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="enter" />',
									title: '<fmt:message key="enter" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										var code = 	document.getElementById('findDivisionSupplyFrame').contentWindow.valueCode();
										//大中小类改为在后台直接添加
										var delivercode = $("#delivercode").val();
										$('#wait2').show();
										$.post("<%=path%>/deliver/saveSupplyByClass.do?classType=0&classCode="+code+"&delivercode="+delivercode,function(data){
											$('#wait2').hide();
											if(data=='0'){//成功
												showMessage({
													type: 'success',
													msg: "<fmt:message key ="save_successful" />！",
													speed: 1000
												});	
												$('.close').click();
												parent.window.setTimeout("pageReload()",1500);
												$('.close',parent.parent.document).click();
											}else{//失败
												alert("<fmt:message key ="save_fail" />!");
											}
										});
// 										$.post("<%=path%>/supply/findGrp.do?code="+code,function(data){ 
// 											if(''!=data&&null!=data){
// 												var date = data.split("_");
// 												$("#parentId").val(($("#parentId").val()+','+date[0]).toString());
// 												$("#parentName").val(($("#parentName").val().toString()+','+date[1]).toString());
// 											}
// 											$('.close').click();
// 										});
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
			 	}
		function findGroupTyp(){
			 $('body').window({
					title: '<fmt:message key="middleClass"/>',
					content: '<iframe id="findTypFrame" frameborder="0" src="<%=path%>/deliver/findGroupTyp.do"></iframe>',
					width: '550px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									var code = 	document.getElementById('findTypFrame').contentWindow.valueCode();
									var delivercode = $("#delivercode").val();
									$('#wait2').show();
									$.post("<%=path%>/deliver/saveSupplyByClass.do?classType=1&classCode="+code+"&delivercode="+delivercode,function(data){
										$('#wait2').hide();
										if(data=='0'){//成功
											showMessage({
												type: 'success',
												msg: "<fmt:message key ="save_successful" />！",
												speed: 1000
											});	
											$('.close').click();
											parent.window.setTimeout("pageReload()",1500);
											$('.close',parent.parent.document).click();
										}else{//失败
											alert("<fmt:message key ="save_fail" />!");
										}
									});
//									$.post("<%=path%>/supply/findTyp.do?code="+code,function(data){
// 										if(''!=data&&null!=data){
// 											var date = data.split("_");
// 											$("#parentId").val(($("#parentId").val()+','+date[0]).toString());
// 											$("#parentName").val(($("#parentName").val().toString()+','+date[1]).toString());
// 										}
// 										$('.close').click();
// 									});
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
		 	}
		function findSmallClass(){
			 $('body').window({
					title: '<fmt:message key="smallClass"/>',
					content: '<iframe id="findSmallFrame" frameborder="0" src="<%=path%>/deliver/findSmallClass.do"></iframe>',
					width: '550px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="enter" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									var code = 	document.getElementById('findSmallFrame').contentWindow.valueCode();
									var delivercode = $("#delivercode").val();
									$('#wait2').show();
									//type 0大类 1中类 2小类
									$.post("<%=path%>/deliver/saveSupplyByClass.do?classType=2&classCode="+code+"&delivercode="+delivercode,function(data){
										$('#wait2').hide();
										if(data=='0'){//成功
											showMessage({
												type: 'success',
												msg: "<fmt:message key ="save_successful" />！",
												speed: 1000
											});	
											$('.close').click();
											parent.window.setTimeout("pageReload()",1500);
											$('.close',parent.parent.document).click();
										}else{//失败
											alert("<fmt:message key ="save_fail" />!");
										}
									});
//									$.post("<%=path%>/supply/findSupplyByTyp.do?code="+code,function(data){
// 										if(''!=data&&null!=data){
// 											var date = data.split("_");
// 											$("#parentId").val(($("#parentId").val()+','+date[0]).toString());
// 											$("#parentName").val(($("#parentName").val().toString()+','+date[1]).toString());
// 										}
// 										$('.close').click();
// 									});
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
		 	}
		</script>
	</body>
</html>