<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_cargo_distribution"/>--供应商报货分拨</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
		<style type="text/css">
  			.search{  
 				margin-top: -2px; 
/* 				padding-left:20px; */
/* 				width:16px; */
/* 				height:16px; */
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				text-align: center;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			.page{
				margin-bottom:25px;
			}
			.table-head td span{
				white-space: normal;
 			} 
/* 			.form-line .form-label{ 
				width: 12%; 
			} */
			.form-line .form-input{
 				width: 10%; 
 			}
			.form-line .form-input input[type=text]{ 
 				width: 87%; 
			}
			.form-line .form-input select{
 				width: 100%; 
 			}
		</style>	
	</head>
	<body>
		<div class="tool"></div>
		<c:if test="${!empty deliverMessage1 }">
		<div style="width:100%;height:30px;text-align:left;color:red;line-height:30px;">
			<c:choose>
				<c:when test="${deliverMessage2 != '0' }">
					<fmt:message key ="Under_the_current_date_you_have" /> ${deliverMessage1 } <fmt:message key ="A_total_of_two_stores" /> ${deliverMessage2 } <fmt:message key ="Delivery_of_goods_not_recognized!_Please_confirm_as_soon_as_possible" />！
				</c:when>
				<c:otherwise>
					<fmt:message key ="Under_the_current_date,_all_goods_have_been_confirmed_delivery" />。
				</c:otherwise>
			</c:choose>
		</div>
		</c:if>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="accountName" name="accountName" value="${accountName }"/>
		<input type="hidden" id="selectIndSta" name="selectIndSta" value="NO"/>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<form id="listForm" action="<%=path%>/deliver/deliverdis.do" method="post">
		<div class="bj_head">
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="date" />：</div>
				<div class="form-input">
					<input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
				</div>
<!-- 				<div class="form-label">结束日期：</div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/> --%>
<!-- 				</div> -->
				<div class="form-label"><fmt:message key="reported_goods_stores"/>：</div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='选择分店' />
				</div>	
				<div class="form-label"><fmt:message key ="supply_units" />：</div>
				<div class="form-input">
					<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>		
			</div>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="abbreviation_code"/>：</div>
				<div class="form-input">
					<input type="text" value="${dis.sp_init}" id="sp_init" name="sp_init" style="text-transform:uppercase;" onkeyup="ajaxSearch()" onfocus="this.select()" />
				</div>
				<div class="form-label" style="margin-left:40px;vertical-align:middle;">
					&nbsp;&nbsp;
					<font size="3">[</font><input type="radio" name="deliveryn" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.deliveryn=='Y' }"> checked="checked"</c:if>  name="deliveryn" value="Y"/>确认送货
					<input type="radio" <c:if test="${dis.deliveryn=='N' }"> checked="checked"</c:if>  name="deliveryn" value="N"/>未送货<font size="3">]</font>	
<%-- 					<font size="3">[</font><input type="radio" name="inout" value="" checked="checked"/><fmt:message key="all"/> --%>
				   <%--  <input type="radio" <c:if test="${dis.inout=='出库' }"> checked="checked"</c:if> name="inout" value="出库"/><fmt:message key="only_the_library"/> --%>
<%-- 				    <input type="radio" <c:if test="${dis.inout=='直发' }"> checked="checked"</c:if> name="inout" value="直发"/><fmt:message key="only_straight_hair"/> --%>
<%-- 				    <input type="radio" <c:if test="${dis.inout=='入库' }"> checked="checked"</c:if> name="inout" value="入库"/><fmt:message key="only_storage"/> --%>
<%-- 				    <input type="radio" <c:if test="${dis.inout=='直配' }"> checked="checked"</c:if> name="inout" value="直配"/><fmt:message key="only_straight_with"/><font size="3">]</font>	 --%>
				     <font size="3">[</font><input type="radio" name="ynArrival" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.ynArrival=='1' }"> checked="checked"</c:if> name="ynArrival" value="1"/>已知到货日期
					<input type="radio" <c:if test="${dis.ynArrival=='2' }"> checked="checked"</c:if> name="ynArrival" value="2"/><fmt:message key="unknown_arrival"/><font size="3">]</font>		
					<font size="3">[</font><input type="radio" name="yndo" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key ="scm_have_done" />
					<input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key ="scm_un_done" /><font size="3">]</font>		
				</div>
			</div>
		</div>
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
							<td rowspan="2"><span style="width:20px;text-align: center;"><input type="checkbox" id="chkAll"/></span></td>								
							<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
							<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
							<td rowspan="2"><span style="width:100px;"><fmt:message key="specification"/></span></td>
							<td rowspan="2"><span style="width:50px;"><fmt:message key="requisitioning_company"/></span></td>
							<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
							<td rowspan="2"><span style="width:40px;"><fmt:message key="the_number_of_procurement"/></span></td>
							<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
							<td rowspan="2"><span style="width:70px;"><fmt:message key="arrival_date"/></span></td>
							<td rowspan="2"><span style="width:25px;"><fmt:message key="the_unknown_arrives"/></span></td>
							<td colspan="2"><span style="width:65px;text-align:center;" ><fmt:message key="reference"/></span></td>
							<td rowspan="2"><span style="width:25px;"><fmt:message key ="Confirmation_of_delivery" /></span></td>							
						</tr>
						<tr>
							<td><span style="width:45px;"><fmt:message key ="quantity" /></span></td>
							<td><span style="width:20px;"></span><fmt:message key="unit"/></td>
						</tr>							
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" id="table-body">
					<tbody>
						<c:forEach var="dis" items="${disesList1}" varStatus="status">
							<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
								<td class="num"><span style="width:25px;">${status.index+1}</span></td>
								<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}" deliveryn="${dis.deliveryn }"/></span></td>
								<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
								<td><span title="${dis.sp_name }" style="width:70px;">${dis.sp_name }</span></td>
								<td><span title="${dis.sp_desc }" style="width:100px;">${dis.sp_desc }</span></td>
								<td><span title="${dis.firmDes}" style="width:50px;"><input type="hidden" value="${dis.firmCode }"/>${dis.firmDes }</span></td>
								<td>
									<c:choose>
										<c:when test="${dis.unitper == 0 }">
											<span title="${dis.unit1 }" style="width:25px;">${dis.unit1 }</span>
										</c:when>
										<c:otherwise>
											<span title="${dis.unit }" style="width:25px;">${dis.unit }</span>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${dis.unitper == 0 }">
											<span title="${dis.amount1}" style="width:40px;text-align: right;"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span>
										</c:when>
										<c:otherwise>
											<span title="${dis.amount}" style="width:40px;text-align: right;"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span>
										</c:otherwise>
									</c:choose>
								</td>
								<td><span title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" style="width:40px;"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
								<td><span title="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" style="width:70px;"><fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/></span></td>
								<td>
									<span style="width:25px;text-align:center;">
									<img src="<%=path%>/image/kucun/${dis.chksend}.png"/>
									</span>
								</td>
								<td><span title="${dis.amount1 }" style="width:45px;text-align: right;">${dis.amount1 }</span></td>
								<td><span title="${dis.unit1 }" style="width:20px;">${dis.unit1 }</span></td>		
								<td>
									<span style="width:25px;text-align:center;">
										<c:choose>
											<c:when test="${dis.deliveryn == 'Y' }">
												<img src="<%=path%>/image/kucun/Y.png"/>
											</c:when>
											<c:otherwise>
												<img src="<%=path%>/image/kucun/N.png"/>
											</c:otherwise>
										</c:choose>
									</span>
								</td>																
							</tr>
						</c:forEach>	
					</tbody>
				</table>
			</div>	
		</div>
		<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),135);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('#chectim').attr('disabled','disabled');
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.keyCode==32){return false;}//屏蔽空格
		 		if(e.altKey == false) return;
		 			switch (e.keyCode){
				                case 70: $('#autoId-button-101').click(); break;
				                case 69: $('#autoId-button-102').click(); break;
				                case 83: updateDis(); break;
				                case 65: $('#autoId-button-104').click(); break;
				                case 68: printDis("printInspection.do"); break;
				                case 67: $('#autoId-button-106').click(); break;
				                case 88: changeFocus();break;//Alt+X
			            	}
			});
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
			if('${action}'=='init'){
				parent.trlist=undefined;//将trlist清空
				$("#sp_init").focus();
			}
         

		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
 								$("#listForm").submit();
// 							if(null!=$('#bdat').val() && ""!=$('#bdat').val() && null!=$('#edat').val() && ""!=$('#edat').val()){
//  							}else{
//  								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
//  							}
						}
					},{
						text: '<fmt:message key="print_distribution_single"/>(<u>A</u>)',
						title: '<fmt:message key="print_distribution_single"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if($('.grid').find('.table-body').find("tr").size()<1){
							alert('<fmt:message key ="Data_is_empty_can_not_print" />！！');
						}else{
							printDis("printIndent.do");	
						}
					}
				},{
					text: '<fmt:message key="print_inspection_single"/>(<u>D</u>)',
					title: '<fmt:message key="print_inspection_single"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if($('.grid').find('.table-body').find("tr").size()<1){
							alert('<fmt:message key ="Data_is_empty_can_not_print" />！！');
						}else{
							printDis("printInspection.do");
						}
					}
				},{
					text: '<fmt:message key="print_delivery_note"/>(<u>C</u>)',
					title: '<fmt:message key="print_delivery_note"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if($('.grid').find('.table-body').find("tr").size()<1){
							alert('<fmt:message key ="Data_is_empty_can_not_print" />！！');
						}else{
							printDis("printDelivery.do");
						}
					}
				},{
					text: '<fmt:message key ="Confirmation_of_delivery" />',
					title: '<fmt:message key ="Confirmation_of_delivery" />',
					useable:true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						if($('.grid').find('.table-body').find("tr").find(':checkbox').filter(':checked').size()<1){
							alert('<fmt:message key ="please_select_at_least_one_data" />！');
							return;
						}else{
							confirmSh();
						}
					}
				},{
					text: '<fmt:message key ="Confirmation_of_delivery" />(ALL)',
					title: '<fmt:message key ="Confirmation_of_delivery" />(ALL)',
					useable:true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						confirmShAll();
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
					}
				}]
			});
		});
		
		//采购汇总
		function cgtotal(){
			if (confirm("<fmt:message key ="Do_you_want_to_purchase_a_summary_of_the_selected_data" />？")==true) {
				if(null!=$('#bdat').val() && ""!=$('#bdat').val() && null!=$('#edat').val() && ""!=$('#edat').val()){
					$('body').window({
						id: 'window_cgtotal',
						title: '<fmt:message key ="procurement_summary" />',
						content: '<iframe id="cgtotalFrame" frameborder="0" src="'+"<%=path%>/deliver/findCaiGouTotal.do?bdat="+$('#bdat').val()+"&edat="+$('#edat').val()+'"></iframe>',
						width: '1000px',
						height: '480px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
										text: '<fmt:message key="print" />',
										title: '<fmt:message key="print"/>',
										handler: function()
										{
											window.frames["cgtotalFrame"].printDis();
										}
									},{
										text: '<fmt:message key="quit"/>(ESC)',
										title: '<fmt:message key="quit"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}]
								}
						});
				}else{
					alert('<fmt:message key ="please_select_the_collection_date" />！');
				}
			}
		};
		//打印单据
		function printDis(e){ 
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/deliver/'+e;	
			var action1="<%=path%>/deliver/deliverdis.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		};		
		//根据缩写码查询
		function ajaxSearch(){
			if(window.event.keyCode==13){
				if(null!=$('#bdat').val() && ""!=$('#bdat').val() && null!=$('#edat').val() && ""!=$('#edat').val()){
					$("#listForm").submit();
				}else{
					alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
				}
			}
		};
		//查询供应商
		$('#seachDeliver').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#deliverCode').val();
				var defaultName = $('#deliverDes').val();
				var offset = getOffset('maded');
				top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
			}
		});
		//确认送货
		function confirmSh(){
			var flag = true;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if (confirm("<fmt:message key ="Is_all_the_data_under_the_current_conditions_to_confirm_Operation_can_not_be_returned" />！")) {
				var codeValue=[];
				checkboxList.filter(':checked').each(function(){
					if($(this).attr("deliveryn") == 'Y'){
						alert("<fmt:message key ="The_selected_data_already_contains_the_confirmed_data" />！");
						flag = false;
					}
					codeValue.push($.trim($(this).val()));
				});
				if(flag){
					var action = '<%=path%>/deliver/confirmSh.do?ids='+codeValue.join(",");
					$.ajax({
						url:action,
						type:'post',
						success:function(data){
							if(data == "OK"){
								alert("<fmt:message key ="Confirmation_of_success" />！");
								$("#listForm").submit();
							}
						}
					});
				}
			}
		};
		var maded = '<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>';
		var firmCode = '<c:out value="${dis.firmCode }"/>';
		var deliverCode = '<c:out value="${dis.deliverCode }"/>';
		//所有确认送货
		function confirmShAll(){
			if(confirm("<fmt:message key ="Is_all_the_data_under_the_current_conditions_to_confirm_Operation_can_not_be_returned" />！")) {
				var action = '<%=path%>/deliver/confirmShAll.do?maded='+maded+'&firmCode='+firmCode+"&deliverCode="+deliverCode;
				$.ajax({
					url:action,
					type:'post',
					success:function(data){
						if(data == "OK"){
							alert("<fmt:message key ="Confirmation_of_success" />！");
							$("#listForm").submit();
						}
					}
				});
			}
		};
		</script>
	</body>
</html>