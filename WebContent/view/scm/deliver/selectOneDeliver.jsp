<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<style type="text/css"> 
		.page{
			margin-bottom: 0px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/deliver/selectOneDeliver.do" method="post">
			<table  cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td class="c-left">&nbsp;<fmt:message key="coding" />：</td>
					<td><input type="text" id="code" name="code" style="width: 100px;" class="text" value="${queryDeliver.code}" /></td>
					<td class="c-left">&nbsp;<fmt:message key="name" />：</td>
					<td><input type="text" id="des" name="des" style="width: 100px;" class="text" value="${queryDeliver.des}" /></td>
					<td class="c-left">&nbsp;<fmt:message key="abbreviation" />：</td>
					<td><input type="text" id="init" name="init" style="width: 100px;" class="text" style="text-transform:uppercase;" value="${queryDeliver.init}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
			        <td width="200">&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 16px;">&nbsp;</span></td>
								<td><span style="width: 20px;">&nbsp;</span></td>
								<td><span style="width: 50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width: 150px;"><fmt:message key="name" /></span></td>
								<td><span style="width: 60px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width: 70px;"><fmt:message key="contact" /></span></td>
								<td><span style="width: 70px;"><fmt:message key="contact" />2</span></td>
								<td><span style="width: 80px;"><fmt:message key="tel" /></span></td>
								<td><span style="width: 80px;"><fmt:message key="fax" /></span></td>
								<td><span style="width: 100px;"><fmt:message key="address" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="deliver" varStatus="step" items="${deliverList}">
								<tr>
									<td class="num"><span style="width:16px;">${step.count}</span></td>
									<td style="text-align: center;">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_${deliver.code}" value="${deliver.code}"/></span>
									</td>
									<td><span title="${deliver.code}" style="width:50px;text-align: left;">${deliver.code}&nbsp;</span></td>
									<td><span title="${deliver.des}" style="width:150px;text-align: left;">${deliver.des}&nbsp;</span></td>
									<td><span title="${deliver.init}" style="width:60px;text-align: left;">${deliver.init}&nbsp;</span></td>
									<td><span title="${deliver.person}" style="width:70px;text-align: left;">${deliver.person}&nbsp;</span></td>
									<td><span title="${deliver.person1}" style="width:70px;text-align: left;">${deliver.person1}&nbsp;</span></td>
									<td><span title="${deliver.tel}" style="width:80px;text-align: left;">${deliver.tel}&nbsp;</span></td>
									<td><span title="${deliver.tax}" style="width:80px;text-align: left;">${deliver.tax}&nbsp;</span></td>
									<td><span title="${deliver.addr}" style="width:100px;text-align: left;">${deliver.addr}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="typ" name="typ" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				}); 
				function search(){
					$('#listForm').submit();//提交表单
				}
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Deliver();
								//alert($('#parentId').val());
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				setElementHeight('.grid',['.tool'],$(document.body),47);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);

				//------------------------------
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				//---------------------------
				
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//让之前选中的默认选中
				var defaultCode = '${defaultCode}';
				var defaultName = '${defaultName}';
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						if(this.id.substr(4,this.id.length)==defaultCode){
							$(this).attr("checked", true);
							$('#parentId').val(defaultCode);
							$('#parentName').val(defaultName);
						}
					})	
				}
				
			});
			function select_Deliver(){
				if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
					$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
						$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
						$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
					})
				}else{
					$('#parentId').val('');
					$('#parentName').val('');
				}
				top.customWindow.afterCloseHandler('Y');
				top.closeCustom();
			}
		</script>
	</body>
</html>