<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>firmsupply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tr-select{
					background-color: #D2E9FF;
				}
				.separator ,div , .pgSearchInfo{
					display: none;
				}
				div[class]{
					display:block;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
				.page{
					margin-bottom: 35px;
				}
			</style>
	</head>
	<body style="height: 100%;">
		<form id="listForm" action="<%=path%>/firmSupply/selectTableNSupply.do?level=${level}&&code=${code}" method="post">
			<table id="dataTable" cellspacing="0" cellpadding="0" style="background-color:#EEE;" >
				<tr>
					<td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="supplies_code" />:</b></font>
			            <input type="text" id="code" name="sp_code" style="width:80px;" class="text" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${supply.sp_code}" />"/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="supplies_name" />:</b></font>
			            <input type="text" id="des" name="sp_name" class="text" style="width:80px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${supply.sp_name}" />"/>
			        </td>
			        <td>&nbsp;
			        	<font style="font-size:2.2ex;"><b><fmt:message key="abbreviation" />:</b></font>
			            <input type="text" id="init" name="sp_init" class="text" style="width:80px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();}" value="<c:out value="${supply.sp_init}" />"/>
			        </td>
			        <td>&nbsp;
			        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />'/>
			        </td>
				</tr>
			</table>
			<div class="grid" style="overflow: auto;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_code" /></span></td>
								<td><span style="width:140px;"><fmt:message key="supplies_name" /></span></td>
								<td><span style="width:70px;"><fmt:message key="abbreviation" /></span></td>
								<td><span style="width:120px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:80px;"><fmt:message key="standard_price" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="supply" varStatus="step" items="${supplyList}">
								<tr class="">
									<td class="num"><span style="width:25px;">${step.index+1}</span></td>
									<td>
										<span style="width:30px; text-align: center;"><input type="checkbox" name="idList" id="chk_<c:out value='${supply.sp_code}'/>" value="<c:out value='${supply.sp_code}'/>"/></span>
									</td>
									<td><span style="width:100px;" title="${supply.sp_code}"><c:out value='${supply.sp_code}'/></span></td>
									<td><span style="width:140px;" title="${supply.sp_name}"><c:out value='${supply.sp_name}'/></span></td>
									<td><span style="width:70px;" title="${supply.sp_init}"><c:out value='${supply.sp_init}'/></span></td>
									<td><span style="width:120px;" title="${supply.sp_desc}"><c:out value='${supply.sp_desc}'/></span></td>
									<td><span style="width:80px;text-align: right;" title="${supply.sp_price}"><c:out value='${supply.sp_price}'/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var str=parent.parent.$('#parentId').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++)
				{ 
					$('#chk_'+strArry[i]).attr('checked','checked');
				};
				$('.grid').find('.table-body').find('tr').each(function(){
					if($(this).find(":checkbox").attr("checked")) {
						$(this).addClass("bgBlue");
					}
				});
				$('.grid').find('.table-head').find('#chkAll').click(function(){
					var checkboxList = $('.grid').find('.table-body').find('tr');
					if($(this).attr("checked")) {
						checkboxList.each(function(){
							$('.grid').find('.table-body').find('tr').each(function(){
								$(this).addClass("bgBlue");
							});
							parent.parent.selectAllSupply($(this).find('td:eq(2)').find('span').text(),$(this).find('td:eq(3)').find('span').text());
						});
					}else {
						checkboxList.each(function(){
							$('.grid').find('.table-body').find('tr').each(function(){
								$(this).removeClass("bgBlue");
							});
							parent.parent.selectZeroSupply($(this).find('td:eq(2)').find('span').text(),$(this).find('td:eq(3)').find('span').text());
						});
					}
				});
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", true);
				     }else{
				    	 $(this).attr("checked", false);
				     }
				     parent.parent.selectSupply($(this).parent().parent().next().find('span').attr('title'),$(this).parent().parent().next().next().find('span').attr('title'));
				 });
				/* // 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				     parent.parent.selectSupply($(this).find('td:eq(2)').find('span').text(),$(this).find('td:eq(3)').find('span').text());
				 }); */
				
				$(".pgPanel").children("div:first").css('display','block');
				var key = $.trim("${supply.sp_code}") || "${supply.sp_init}";
				$("#key").val(key);
				
				setElementHeight('.grid',['#dataTable'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();
			});
			//清空页面
			function clearValue(){
				$('#tblGrid').find('input').removeAttr('checked');
			}

		</script>
	</body>
</html>