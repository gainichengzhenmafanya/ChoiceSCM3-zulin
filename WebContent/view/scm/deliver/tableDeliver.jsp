<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="vendor_defined" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 45px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>		
		<form id="listForm" action="<%=path%>/deliver/deliverList.do?typCode=${typCode}" method="post">
			<input type="hidden" id="orderBy" name="orderBy" value="${deliver.orderBy}" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<!--  <td><span style="width:60px;"><fmt:message key="suppliers_typ" /></span></td>-->
								<td><span style="width:50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:140px;"><fmt:message key="suppliers"/><fmt:message key="name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="abbreviation_code" /></span></td>
 								<td><span style="width:50px;"><fmt:message key="person_in_charge" /></span></td> 								
								<td><span style="width:50px;"><fmt:message key="contact" /></span></td>
		 						<td><span style="width:70px;"><fmt:message key ="Mall_supplier_encoding" /></span></td>
								<td><span style="width:70px;"><fmt:message key ="Mall_supplier_name" /></span></td>
								<td><span style="width:90px;"><fmt:message key="corresponding_position_encoding" /></span></td>								
								<td><span style="width:90px;"><fmt:message key="tel" /></span></td>
								<td><span style="width:90px;"><fmt:message key="fax" /></span></td>
 								<td><span style="width:140px;"><fmt:message key="email" /></span></td>
								<td><span style="width:170px;"><fmt:message key="address" /></span></td>
								<td><span style="width:50px;"><fmt:message key="material_remind_days"/></span></td>
 								<td><span style="width:90px;"><fmt:message key="remark" /></span></td>
 								<td><span style="width:90px;"><fmt:message key="Expiration_date" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="deliver" items="${deliverList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${deliver.code}" value="${deliver.code}"/></span>
									</td>
									<!-- <td><span title="${deliver.typ}" style="width:60px;">${deliver.typ}</span></td>-->
									<td><span title="${deliver.code}" style="width:50px;text-align: center;">${deliver.code}</span></td>
									<td><span title="${deliver.des}"  style="width:140px;">${deliver.des}</span></td>
									<td><span title="${deliver.init}" style="width:60px;">${deliver.init}</span></td>
									<td><span title="${deliver.person}" style="width:50px;">${deliver.person}</span></td>
									<td><span title="${deliver.person1}" style="width:50px;">${deliver.person1}</span></td>
									<td><span title="${deliver.jmudelivercode}" style="width:70px;">${deliver.jmudelivercode}</span></td>
									<td><span title="${deliver.jmudelivername}" style="width:70px;">${deliver.jmudelivername}</span></td>
									<td><span title="${deliver.positn}" style="width:90px;">${deliver.positn}</span></td>									
									<td><span title="${deliver.tel1}" style="width:90px;">${deliver.tel1}</span></td>
									<td><span title="${deliver.tel}" style="width:90px;">${deliver.tel}</span></td>
									<td><span title="${deliver.mail}" style="width:140px;">${deliver.mail}</span></td>
									<td><span title="${deliver.addr}" style="width:170px;">${deliver.addr}</span></td>
									<td><span title="${deliver.warnDays}" style="width:50px;text-align: right;">${deliver.warnDays}</span></td>
									<td><span title="${deliver.memo}" style="width:90px;">${deliver.memo}</span></td>
									<td>
									<c:choose>
										<c:when test="${!empty deliver.expiryday }">
											<span title="<fmt:formatDate value="${deliver.expirydate }" pattern="yyyy-MM-dd"/>" style="width:90px;<c:if test="${deliver.expiryday <= 10}"> color:red;</c:if>">
												<fmt:formatDate value="${deliver.expirydate }" pattern="yyyy-MM-dd"/>(${deliver.expiryday})
											</span>
										</c:when>
										<c:otherwise>
											<span title="<fmt:formatDate value="${deliver.expirydate }" pattern="yyyy-MM-dd"/>" style="width:90px;">
												<fmt:formatDate value="${deliver.expirydate }" pattern="yyyy-MM-dd"/>
											</span>
										</c:otherwise>
									</c:choose>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />			
			
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="suppliers_typ" />:</td>
							<td>
								<div class="form-input">
									<input type="hidden" id="typ1" name="typ1" value="${deliver.typ}"/>
									<select id="typ" name="typ" class="select" onkeyup="ajaxSearch()">
									    <option selected="selected" disabled="disabled"></option>
									    <c:forEach var="codeDes" items="${codeDesList}" varStatus="status">
									    	<option value="${codeDes.des }">${codeDes.des }</option>
									    </c:forEach>
									</select>
								</div>
							</td>
							<td class="c-left"><fmt:message key="abbreviation" />：</td>
							<td><input type="text" id="init" name="init" class="text" style="text-transform:uppercase;" onkeyup="ajaxSearch()" value="${deliver.init}" /></td>
							<td class="c-left"><fmt:message key="coding" />：</td>
							<td><input type="text" id="code" name="code" class="text" onkeyup="ajaxSearch()" value="${deliver.code}" /></td>
							<td class="c-left"><fmt:message key="name" />：</td>
							<td><input type="text" id="des" name="des" class="text" onkeyup="ajaxSearch()" value="${deliver.des}" /></td>
							<td class="c-left"><fmt:message key="person_in_charge" />：</td>
							<td><input type="text" id="person" name="person" class="text" onkeyup="ajaxSearch()" value="${deliver.person}" /></td>							
						</tr>
						<tr>
							<td class="c-left"><fmt:message key="tel" />：</td>
							<td><input type="text" id="tel1" name="tel1" class="text" onkeyup="ajaxSearch()" value="${deliver.tel1}" /></td>
							<td class="c-left"><fmt:message key="fax" />：</td>
							<td><input type="text" id="tel" name="tel" class="text" onkeyup="ajaxSearch()" value="${deliver.tel}" /></td>
							<td class="c-left"><fmt:message key="email" />：</td>
							<td><input type="text" id="mail" name="mail" class="text" onkeyup="ajaxSearch()" value="${deliver.mail}" /></td>							
							<td class="c-left"><fmt:message key="address" />：</td>
							<td><input type="text" id="addr" name="addr" class="text" onkeyup="ajaxSearch()" value="${deliver.addr}" /></td>
							<td class="c-left"><fmt:message key="contact" />：</td>
							<td><input type="text" id="person1" name="person1" class="text" onkeyup="ajaxSearch()" value="${deliver.person1}" /></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
	       			<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
	       			<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
			}
			$(document).ready(function(){
				//typ是否有值，有，默认选中
				f_typ();
				//按钮快捷键
				focus() ;//页面获得焦点
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
				});
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								var t = $('#init').val();
								 $('#init').focus().val(t);
							}
						},"-",{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveDeliver();
							}
						},{
							text: '<fmt:message key="scm_copy" />',
							title: '<fmt:message key="scm_copy" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								copyDeliver();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateDeliver();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteDeliver();
							}
						}
						,{
							text: '<fmt:message key="Scope_of_supplies" />',
// 							title: '<fmt:message key="delete" />',
// 							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								setSupply();
							}
						}
						,{
							text: '<fmt:message key="the_corresponding_position_code"/>',
							title: '<fmt:message key="the_corresponding_position_code"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updatePCode();
							}
						},
						{
							text: '<fmt:message key="material_remind_days"/><fmt:message key="scm_set_up_the"/>',
							title: '<fmt:message key="material_remind_days"/><fmt:message key="scm_set_up_the"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								warnDays();
							}
						}
						<c:if test="${isOrNOtRelationCG=='1'}">
						,{
							text: '<fmt:message key="relevance_supplier" />',
							title: '<fmt:message key="relevance_supplier" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								selectJmuDeliver();
							}
						},{
							text: '<fmt:message key="Delete_associated_vendor" />',
							title: '<fmt:message key="Delete_associated_vendor" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								deleteJmuDeliver();
							}
						}
						</c:if>
						,{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printDeliver();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
// 				setElementHeight('.table-body',['.table-head'],'.grid');
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
					$('#typ').val('');
				});
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
				//排序start
				$('.grid').find('.table-head').find('td:eq(16)').bind('click',function(){
					var sc = "ASC";
					if($(this).find('span').attr('class') == 'datagrid-sort-icon'){
						sc = "DESC";
					}
					$('#orderBy').val("ORDER BY EXPIRYDATE "+sc);
					$('#listForm').submit();
				});
				if($('#orderBy').val() == "ORDER BY EXPIRYDATE ASC"){
					$('.grid').find('.table-head').find('td:eq(16)').find('span').addClass('datagrid-sort-icon');
				}
			});	
			function updatePCode() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() != 1){
					alert('<fmt:message key="please_select"/>1<fmt:message key="ge"/><fmt:message key="suppliers"/>！');
					return;
				}
				var chkValue = [];
				var positncode;//供应商对应仓位编码，传到下个页面 wjf
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
					positncode = $(this).parent('span').parent('td').parent('tr').find('td:eq(9)').find('span').attr('title');
				});
				path="<%=path%>/deliver/selectDeliverPositns.do?deliver="+chkValue.join(',')+"&positncode="+positncode;
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key="Select_the_supplier_to_the_corresponding_position" />',
					content: '<iframe id="supplyPositnFrame" name="supplyPositnFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyPositnFrame')&&window.document.getElementById("supplyPositnFrame").contentWindow.updPositn()){
										window.document.getElementById("supplyPositnFrame").contentWindow.$("#tableForm").submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增供应商
			function saveDeliver(){
				$('body').window({
					id: 'window_saveDeliver',
					title: '<fmt:message key="insert" /><fmt:message key="suppliers_message" />',
					content: '<iframe id="saveDeliverFrame" name="saveDeliverFrame" frameborder="0" src="<%=path%>/deliver/add.do?typCode=${typCode}"></iframe>',
					width: '650px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveDeliverFrame')&&window.document.getElementById("saveDeliverFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('saveDeliverFrame','DeliverForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}]
					}
				});				
			}
			
			//供应商物资到期提醒设置
			function warnDays(){
				var codeList = [];
				$('.grid').find('.table-body').find(':checkbox').map(function(){
					if($(this).attr("checked") == 'checked'){
						codeList.push($(this).val());
					}
				});
				if(codeList.length>0){
					var action = '<%=path%>/deliver/addWarnDays.do?typCode='+codeList.join(",");
					$('body').window({
						id: 'window_warnDays',
						title: '<fmt:message key="Supplier_material_expiration_reminder_set" />',
						content: '<iframe id="saveWarnDaysFrame" name="saveWarnDaysFrame" frameborder="0" src="'+action+'"></iframe>',
						width: '400px',
						height: '150px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="suppliers_message" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveWarnDaysFrame')&&window.document.getElementById("saveWarnDaysFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('saveWarnDaysFrame','DeliverForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});	
				}else{
					alert('<fmt:message key="please_select"/><fmt:message key="suppliers"/>!');
				}
			}
			
			
			var returnSypply = function (){
				selectSupplyBatchAsst({
			        basePath: '<%=path%>',
			        title: "123",
			        height: 400,
			        width: 650,
			        callBack: 'setSupplier',
			        single: true
			    });
			}
			
			function setSupplier(data){
				supplyData = data;
			}
			
			//供应物资范围
			function setSupply(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					var action = '<%=path%>/deliver/tosaveDeliverSupply.do?typ=1&delivercode='+chkValue;
					$('body').window({
						id: 'window_warnDays',
						title: '<fmt:message key="add" /><fmt:message key="Scope_of_supplies" />',
						content: '<iframe id="saveDeliverSupplyFrame" name="saveDeliverSupplyFrame" frameborder="0" src="'+action+'"></iframe>',
						width: '700px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [
							<c:if test="${1==2}">
							{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="suppliers_message" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveDeliverSupplyFrame')){
											getFrame('saveDeliverSupplyFrame').updateSprice();
										}
									}
								},
							</c:if>
								{
									text: '<fmt:message key="save" /><fmt:message key = "tax_rate" />',
									title: '<fmt:message key="save" /><fmt:message key = "tax_rate" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveDeliverSupplyFrame')){
											getFrame('saveDeliverSupplyFrame').updateTaxRate();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								},{
									text: '<fmt:message key="Add_material" />',
									title: '<fmt:message key="Add_material" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('body').window({
											id: 'window_saveSupply',
											title: '<fmt:message key="Add_material" />',
											content: '<iframe id="saveDeliverSupplyPageFrame" frameborder="0" src="<%=path%>/deliver/addSupplyBatch.do?type=firm&chkValue='+chkValue+'"></iframe>',
											width: ($(document.body).width()-50+'px')/* '820px' */,
											height:($(document.body).height()-110+'px')/* '550px' */,
											draggable: true,
											isModal: true
										});
										$("#window_saveSupply").css("z-index","500");
									}
								},{
									text: '<fmt:message key="Delete_material" />',
									title: '<fmt:message key="Delete_material" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										getFrame('saveDeliverSupplyFrame').deleteSupply();
										getFrame('saveDeliverSupplyFrame').pageReload();
									}
								},{
									text: '<fmt:message key="Relation_mallmaterial" />',
									title: '<fmt:message key="Relation_mallmaterial" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										getFrame('saveDeliverSupplyFrame').saveJmuSupply();
// 										getFrame('saveDeliverSupplyFrame').pageReload();
									}
								},{
									text: '<fmt:message key="Delete_associated_material" />',
									title: '<fmt:message key="Delete_associated_material" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										getFrame('saveDeliverSupplyFrame').deleteJmuSupply();
// 										getFrame('saveDeliverSupplyFrame').pageReload();
									}
								}]
						}
					});	
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="Choose_a_supplier_to_add_material" />！');
					return ;
				}
			}
			//复制供应商
			function copyDeliver(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('body').window({
						title: '<fmt:message key="scm_copy" /><fmt:message key="suppliers_message" />',
						content: '<iframe id="copyDeliverFrame" name="copyDeliverFrame" frameborder="0" src="<%=path%>/deliver/copy.do?code='+chkValue+'"></iframe>',
						width: '650px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="scm_copy" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-18px','-0px']
									},
									handler: function(){
										if(getFrame('copyDeliverFrame')&&window.document.getElementById("copyDeliverFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('copyDeliverFrame','DeliverForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="select_the_need_to_copy_the_supplier_information"/>！');
					return ;
				}
			}
			
			//查询供应商
			function searchDeliver(){
				$('body').window({
					id: 'window_searchDeliver',
					title: '<fmt:message key="select" /><fmt:message key="suppliers_message" />',
					content: '<iframe id="searchDeliverFrame" name="searchDeliverFrame" frameborder="0" src="<%=path%>/deliver/search.do"></iframe>',
					width: '550',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="select" /><fmt:message key="suppliers_message" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('searchDeliverFrame')){
										submitFrameForm('searchDeliverFrame','SearchForm');
									}
								}
							}
						]
					}
				});
			}
			//修改供应商
			function updateDeliver(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					$('body').window({
						title: '<fmt:message key="update" />',
						content: '<iframe id="updateDeliverFrame" name="updateDeliverFrame" frameborder="0" src="<%=path%>/deliver/update.do?code='+chkValue+'"></iframe>',
						width: '650px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="update" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-18px','-0px']
									},
									handler: function(){
										if(getFrame('updateDeliverFrame')&&window.document.getElementById("updateDeliverFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('updateDeliverFrame','DeliverForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除系统编码信息
			function deleteDeliver(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var data = getAjaxReturn(chkValue.join(","));
						if(data == ""){
							var action = '<%=path%>/deliver/delete.do?code='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete" /><fmt:message key="suppliers" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '500px',
								height: '245px',
								draggable: true,
								isModal: true
							});
						} else {
							alert('<fmt:message key="referenced_can_no_be_deleted" />:'+'\n'+data);
						}
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
			
			//ajax 校验当前信息是否被引用
			function getAjaxReturn(code){
				var dataSource = null;
				$.ajax({
					url:'<%=path%>/deliver/deleteyh.do?code='+code,
					type:"POST",
					async:false,
					success:function(data){
						dataSource = data;
					}
				});
				return dataSource;
			}
			
			//供应商信息打印
			function printDeliver(){
				window.open ("<%=path%>/deliver/printDeliver.do",'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
			function pageReload(){
				$('#listForm').submit();
			}
			//判断让typ有值的select的option项选中
		   function f_typ(){
			   var typ=$("#typ1").val();
			   $("select[@name=typ] option").each(function(){
					if($(this).val() == typ){
						$(this).attr("selected", true);
					}
			   });
		   }
		   function select_Supply(delivercode,code){
//				getFrame('saveDeliverSupplyFrame').select_Supply(delivercode,code);
				$("#window_saveSupply").remove();
				$.ajax({
					url:'<%=path%>/deliver/insertDeliverSupply.do?delivercode='+delivercode+'&code='+code,
					type:"POST",
					async:false,
					success:function(data){
						if(data == 'OK'){
							alert("<fmt:message key="operation_successful" />！");
						}else{
							alert("<fmt:message key="operation_failed" />！");
						}
					}
				});
				getFrame('saveDeliverSupplyFrame').pageReload();
			}
		   function selectJmuDeliver(){
			   var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
					checkboxList.filter(':checked').each(function(i){
						var code = $.trim($(this).closest('tr').find('td').eq(2).find('span').text());
						var jmucode = $.trim($(this).closest('tr').find('td').eq(7).find('span').text());
						var des = $.trim($(this).closest('tr').find('td').eq(3).find('span').text());
						if(jmucode != null && jmucode != ''){
							alert('当前供应商已经关联商城供应商，不可重复关联！');
							return;
						}
						var action = encodeURI('<%=path%>/deliver/supplierJmuList.do?code='+code+'&des='+encodeURI(des));
						$('body').window({
							title: '<fmt:message key="relevance_supplier" />',
							content: '<iframe id="updateJmuDeliverFrame" name="updateJmuDeliverFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '550px',
							height: '400px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-18px','-0px']
										},
										handler: function(){
											if(getFrame('updateJmuDeliverFrame')){
												getFrame('updateJmuDeliverFrame').setJmuDeliver();
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}]
							}
						});
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
		   }
		   //删除关联供应商
		   function deleteJmuDeliver(){
			   var data={};
			   var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						$.post("<%=path%>/deliver/deleteJumDeliver.do?code="+chkValue.join(","),data,function(data){
							var rs = data;
	                        if(isNaN(rs)){
	                            alert('删除关联失败！');
	                            return;
	                        }
							switch(Number(rs)){
							case -1:
								alert('删除关联失败！');
								break;
							case 1:
								showMessage({
									type: 'success',
									msg: '删除成功！！',
									speed: 3000,
									handler:function(){
										reloadPage();
									}
								});
								break;
							}
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
		   }
		   function reloadPage(){
			   $('#listForm').submit();
		   }
		</script>
	</body>
</html>