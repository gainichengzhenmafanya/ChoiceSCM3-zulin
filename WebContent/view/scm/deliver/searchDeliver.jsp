<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_find" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <style type="text/css">
		.bgBlue{background-color:blue;}
		.datagrid {background-color: #6595d6;}
		.datagrid caption {}
		.datagrid th {
		 /*background-image: url("images/div.th.background-image.gif" );*/
		 background-color: #6595d6;
		 color: #ffffff;
		 font-size: 12px;
		 font-weight: bold;
		 height: 25px;
		 line-height: 25px;
		 text-align: center;
		}
		.datagrid tr {}
		.datagrid td {background-color: #ffffff;}
		/* css for OEC form page*/
		.row_focus {
		 background: #B0FFB0;
		 border: 1px solid #00cc33;
		}
		/* css or table row effect */
		tr.alt td {background: #ecf6fc;}
		tr.over td {background: #bcd4ec;}
		</style>
	</head>
	<body>
<%-- 		<form id="SearchForm" action="<%=path%>/deliver/searchByKeyOnDis.do" method="post"> --%>
		<form id="SearchForm" action="<%=path%>/deliver/searchDeliver.do" method="post">
		<input type="hidden" name="firm" value="${firm }"/>
		<input type="hidden" name="inout" value="${inout }"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="condition" /><fmt:message key ="scm_type" />：</div>
					<div class="form-input" style="width:220px;">
						<input type="radio" id="code" name="sech" <c:if test="${sech=='code'}"> checked="checked"</c:if> class="radio" value="code"/>&nbsp;<fmt:message key="coding" />&nbsp;&nbsp;
						<input type="radio" id="des" name="sech" <c:if test="${sech=='des'}"> checked="checked"</c:if> class="radio" value="des"/>&nbsp;<fmt:message key="name" />&nbsp;&nbsp;
						<input type="radio" id="init" name="sech" style="text-transform:uppercase;" <c:if test="${sech=='init'}"> checked="checked"</c:if> class="radio" value="init"/>&nbsp;<fmt:message key="abbreviation" />&nbsp;&nbsp;
						<input type="radio" id="person1" name="sech" <c:if test="${sech=='person1'}"> checked="checked"</c:if> class="radio" value="person1"/>&nbsp;<fmt:message key="contact" />			
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="query_information" />：</div>
					<div class="form-input">
						<input type="text" id="key" name="key" class="text" value="${key}" onkeyup="keyupup();"/>
					</div>
				</div>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td><span style="width:60px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:230px;"><fmt:message key="name" /></span></td>
									<td><span style="width:70px;"><fmt:message key="abbreviation_code" /></span></td>
	 								<td><span style="width:70px;"><fmt:message key="contact" /></span></td>
	 								<td><span style="width:90px;"><fmt:message key="tel" /></span></td>
									<td><span style="width:90px;"><fmt:message key="address" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="deliver" items="${deliverList}" varStatus="status">
									<tr>
										<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
										<td><span style="width:60px;">${deliver.code}</span></td>
										<td><span style="width:230px;">${deliver.des}</span></td>
										<td><span style="width:70px;">${deliver.init}</span></td>
										<td><span style="width:70px;">${deliver.person}</span></td>
										<td><span style="width:90px;">${deliver.tel1}</span></td>
										<td><span style="width:90px;">${deliver.addr}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type="hidden" name="prevTrIndex" id="prevTrIndex" value="-1"/>
					</div>			
				</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.hotkeys-0.7.9.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).focus();
			 			parent.$('.close').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: parent.$('.<fmt:message key="query_suppliers" />').click(); break;
		            }
				});  
				//箭头移动选择项
				selectDeliver();
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});			
			//求单选按纽的值，传radio名字作为参数。未选返回false；有选择项，返回选项值。    
			function getRadioValue(name){    
				var radioes = document.getElementsByName(name);     
				for(var i=0;i<radioes.length;i++) {    
				     if(radioes[i].checked){    
				      return radioes[i].value;    
				     }    
				}    
				return false;    
			}    
			//箭头移动选择项
			function selectDeliver()
			{
				$("#prevTrIndex").val("-1");//默认行索引-1
				var trSize = $(".datagrid tr").size();//datagrid中tr的数量
				//单击行事件
				function clickTr(currTrIndex){
					var prevTrIndex = $("#prevTrIndex").val();
					if (currTrIndex > -1){
						$("#tr_" + currTrIndex).addClass("over");
					}
					$("#tr_" + prevTrIndex).removeClass("over");
					$("#prevTrIndex").val(currTrIndex);
				}
				$(".datagrid tr").mouseover(function(){//鼠标滑过
					//$(this).addClass("over");
				}).mouseout(function(){ //鼠标滑出
					//$(this).removeClass("over"); 
				}).each(function(i){ //初始化 id 和 index 属性
					$(this).attr("id", "tr_" + i).attr("index", i);
				}).click(function(){ //鼠标单击
					clickTr($(this).attr("index"));
				}).dblclick(function(){ //鼠标双击
					var deliverCode=$("#tr_" + $("#prevTrIndex").val()).find("td:eq(1)").text();
					var deliverDes=$("#tr_" + $("#prevTrIndex").val()).find("td:eq(2)").text();
					$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).val(deliverCode);
					$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).closest("td").next().find("span").text(deliverDes);
					$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).focus();
					parent.$('.close').click();
				});
				$(".datagrid tr:even").addClass("alt"); //偶行变色
				//回车选择数据
				$(document).bind('keydown', function(evt){
					if(evt.which==13){
						var deliverCode=$("#tr_" + $("#prevTrIndex").val()).find("td:eq(1)").text();
						var deliverDes=$("#tr_" + $("#prevTrIndex").val()).find("td:eq(2)").text();
						$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).val(deliverCode);//改变供应商编码的那列值
// 						$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).closest("td").next().find("span").text(deliverDes);//改变供应商名称那列值
						$("#c_delv_"+$("#selectDelCodeId",parent.document).val(),parent.document).focus();
						parent.$('.close').click();
					}
				});
				//向上键
				$(document).bind('keydown', 'up', function(evt){ //↑	
					var trIndex=Number($("#prevTrIndex").val())+1;//列索引
					var lineHeight = $(".table-body").find("table tr").height();//单元格的高度
					//var scrHeight = $(".table-body").height();//table高度
					//var scrolltop = $(".table-body").scrollTop();//滚动条的高度
					//var positnY = $(".over").offset().top;//选中项的高度
					$(".table-body").scrollTop(trIndex*lineHeight-38);
					var prevTrIndex = parseInt($("#prevTrIndex").val());
					if (prevTrIndex == -1 || prevTrIndex == 0){
						clickTr(trSize - 1);
						$(".table-body").scrollTop(trSize*lineHeight);
					} else if(prevTrIndex > 0){
						clickTr(prevTrIndex - 1);
					}
					return false;
				//向下键	
				}).bind('keydown', 'down', function(evt){ //↓
					var trIndex=Number($("#prevTrIndex").val())+1;
					var lineHeight = $(".table-body").find("table tr").height();//19
					//var scrHeight = $(".table-body").height();//279
					//var scrolltop = $(".table-body").scrollTop();
					//var positnY = $(".over").offset().top;
					$(".table-body").scrollTop(trIndex*lineHeight);
					var prevTrIndex = parseInt($("#prevTrIndex").val());
					if (prevTrIndex == -1 || prevTrIndex == (trSize - 1)){
					clickTr(0);
					$(".table-body").scrollTop(0);
					} else if (prevTrIndex < (trSize - 1)) {
						clickTr(prevTrIndex + 1);
					}
					return false;
				//返回
				}).bind('keydown', 'return', function(evt){ //↙
					//var prevTrIndex = parseInt($("#prevTrIndex").val());
					//$("#txt_no").val(($("#tr_" + prevTrIndex).find("td")[0]).innerHTML);
					return false;
				});
				//第一行选中
				clickTr(0);
			}
			
			//输入框内容改变
			function keyupup(){
				var radioValue = $("#init").attr("checked");
				if(radioValue){
					$("#key").val($("#key").val().toUpperCase());
				}
			}
		</script>
	</body>
</html>