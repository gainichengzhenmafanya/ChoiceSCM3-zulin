<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"/>
	<title>填报申购单</title>
	<link href="<%=path%>/js/3gwap/jquery.mobile-1.0.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.css" rel="stylesheet" type="text/css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<script src="<%=path%>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/jquery.mobile-1.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/time.js" type="text/javascript"></script>
	<style>
	.lable{
		height: 25px;
		line-height: 25px;
		text-align: right;
		padding-right: 5px;
		width: 110px;
	}
</style>
</head> 
<body> 

<div data-role="page" id="page">
	<div data-role="header">
		<h1>填报申购单 </h1>
		<a class="ui-btn-right">分店:${firmName }</a>
	</div>
	<form action="">
	      <table width="100%" align="center">
	    	<tr align="center">
	        	<td width="30%"><a>物品名</a></td>
	            <td width="30%"><a>规格</a></td>
	            <td width="15%"><a>单位</a></td>
	            <td width="25%"><a>数量</a></td>
	        </tr>
	        <tr><td colspan="4"><hr/></td></tr>
	        <c:forEach items="${listChkstodemo }" var="chkstodemo">
		        <tr align="center" id="chTr">
		        	<td width="30%">
		        		<a id="sp_name">${chkstodemo.supply.sp_name }</a>
		        		<input type="hidden" id="title" value="${chkstodemo.title }"/>
		        		<input type="hidden" id="rec" value="${chkstodemo.rec }"/>
		        		<input type="hidden" id="cnt1" value="${chkstodemo.cnt1 }"/>
		        		<input type="hidden" id="unitper" value="${chkstodemo.unitper }"/>
		        		<input type="hidden" id="sp_code" value="${chkstodemo.supply.sp_code }"/>
		        		<input type="hidden" id="unit1" value="${chkstodemo.supply.unit1 }"/>
		        	</td>
		            <td width="30%"><a id="sp_desc">${chkstodemo.supply.sp_desc }</a></td>
		            <td width="15%"><a id="unit">${chkstodemo.supply.unit }</a></td>
		            <td width="25%"><input type="text" style="width: 30px;" id="cnt" value="0"/></td>
		        </tr>
	        </c:forEach>
	    </table>
	    <div data-role="footer" align="right">
	   		<input type="reset" value="重置" data-icon="back"/>
	    	<button onclick="saveChkstodemo()" data-icon="Plus" data-theme="b">提交</button>
	    </div>
    </form>
    <script type="text/javascript">
		$(document).ready(function(){
			saveChkstodemo=function(){
				var chkstodlist={};
				var result=$("tr[id=chTr]");
				result.each(function(index){
					var cnt=$(this).find("#cnt").val();
					if(Number(cnt)>0){
						chkstodlist["chkstodList["+index+"].supply.sp_name"]=$(this).find("#sp_name").text();
						chkstodlist["chkstodList["+index+"].supply.sp_desc"]=$(this).find("#sp_desc").text();
						chkstodlist["chkstodList["+index+"].supply.sp_code"]=$(this).find("#sp_code").val();
						chkstodlist["chkstodList["+index+"].supply.unit1"]=$(this).find("#unit1").val();
						chkstodlist["chkstodList["+index+"].supply.unit"]=$(this).find("#unit").text();
						chkstodlist["chkstodList["+index+"].amount"]=cnt;
						chkstodlist["chkstodList["+index+"].amount1"]=$(this).find("#cnt1").val();
					}
				});
				$.ajax({
					url : '<%=path%>/wapFirmMis/saveChkstodemo.do',
					data : chkstodlist,
					type : 'POST',
					async:false,
					success : function(message) {
						if(message=="success"){
							alert("-=添加成功=-");
							location.href="<%=path%>/wapFirmMis/findTodayChkstodemo.do";
						}else{
							alert("-="+message+"=-");
						}
					}
				});
			};
		});
	</script>
</div>
</body>
</html>