<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"/>
	<title>用户登录</title>
	<link href="<%=path%>/js/3gwap/jquery.mobile-1.0.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.css" rel="stylesheet" type="text/css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<script src="<%=path%>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/jquery.mobile-1.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/time.js" type="text/javascript"></script>
	<style>
	.lable{
		height: 25px;
		line-height: 25px;
		text-align: right;
		padding-right: 5px;
		width: 110px;
	}
</style>
</head> 
<body>
<div data-role="page" id="page">
	<div data-role="header">
		<h1>申购单·用户登陆</h1>
	</div>
	<form action="<%=path%>/wapFirmMis/waploginIn.do" method="post">
	    <table width="100%">
	    	<tr>
	        	<td align="right" width="30%">用户名：</td>
	            <td width="50%" colspan="2">
	            	<input type="text" name="name" maxlength="16"/>
	            	${result }
	            </td>
	            <td width="30%">
	            </td>
	        </tr>
	        <tr>
	        	<td align="right" width="30%">密码：</td>
	            <td width="50%" colspan="2">
	             	<input type="password" name="password" maxlength="16"/>
	            </td>
	            <td width="30%">
	            </td>
	        </tr>
	        <tr>
	        	<td></td>
	            <td  align="right">
	           	 	<input type="button" value="重置"/>
	            </td>
	            <td align="right">
	            	<input type="submit" value="登录"/>
	            </td>
	        </tr>
	    </table>
    </form>
</div>
</body>
</html>