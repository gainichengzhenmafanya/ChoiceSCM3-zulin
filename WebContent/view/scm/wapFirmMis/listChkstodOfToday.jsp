<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"/>
	<title>今日报货</title>
	<link href="<%=path%>/js/3gwap/jquery.mobile-1.0.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.css" rel="stylesheet" type="text/css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<script src="<%=path%>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/jquery.mobile-1.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/time.js" type="text/javascript"></script>
	<style>
	.lable{
		height: 25px;
		line-height: 25px;
		text-align: right;
		padding-right: 5px;
		width: 110px;
	}
</style>
</head> 
<body> 

<div data-role="page" id="page">
	<div data-role="header">
		<h1>今日报货</h1>
	</div>
     <table width="100%" align="center">
   		<tr align="center">
       	   <td width="30%"><a>凭证号</a></td>
           <td width="20%"><a>报货单号</a></td>
           <td width="25%"><a>申请仓位</a></td>
           <td width="25%"><a>制单人</a></td>
       </tr>
       <tr><td colspan="4"><hr/></td></tr>
       <c:forEach items="${listChkstom }" var="chkstom">
        <tr align="center" id="chTr">
        	<td width="30%"><a id="sp_name">${chkstom.vouno }</a></td>
            <td width="20%"><a id="sp_desc">${chkstom.chkstoNo }</a></td>
            <td width="25%"><a id="unit">${chkstom.positn.des }</a></td>
            <td width="25%"><a id="unit">${chkstom.madeby }</a></td>
        </tr>
       </c:forEach>
   </table>
   <div data-role="footer" id="ShenGou" align="center">
		<a  type="reset" href="<%=path %>/wapFirmMis/findChkstodemo.do" data-icon="back"> 申购   </a>
   </div>
</div>
<script type="text/javascript">
    	if("${firmName}"==null||"${firmName}"==""){
    		$("#ShenGou").css("display","none");
    	}
</script>
</body>
</html>