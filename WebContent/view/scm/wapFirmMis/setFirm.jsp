<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1"/>
	<title>设置分店</title>
	<link href="<%=path%>/js/3gwap/jquery.mobile-1.0.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.css" rel="stylesheet" type="text/css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<script src="<%=path%>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/jquery.mobile-1.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/mobiscroll.custom-2.5.0.min.js" type="text/javascript"></script>
	<script src="<%=path%>/js/3gwap/time.js" type="text/javascript"></script>
	<style>
	.ui-dialog-contain {
		width: 92.5%;
		max-width: 500px;
		margin: 10% auto 15px auto;
		 padding: 0;
		 position: relative;
		top: -15px;
		 }
</style>
</head> 
<body> 

<div data-role="page" id="page">
	<div data-role="header">
		<h1>分店选择</h1>
	</div>
	<form action="<%=path %>/wapFirmMis/findChkstodemo.do" method="post">
	<select name="firm">
		<c:forEach items="${firmList }" var="firm">
			<option value="${firm.code }">${firm.des }</option>
		</c:forEach>
	</select>
	<div class="ui-grid-a">
		<div class="ui-block-a"><button>取消</button></div>
		<div class="ui-block-b"><input type="submit" value=" 确定 "/></div>
	</div>
	</form>
</div>
</body>
</html>