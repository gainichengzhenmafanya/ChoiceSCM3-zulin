<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<form action="<%=path%>/chkout/saveByAdd.do" id="listForm" name="listForm" method="post">
		<input type="hidden" id="curChkout" name="chkoutno" disabled="disabled" value="${chkoutm.chkoutno}"/>
		<%-- curStatus 0:init;1:edit;2:add;3:show --%>
		<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		<div class="form-line">
			<div class="form-label"><fmt:message key="document_types"/></div>
			<div class="form-input" style="width:220px;">
				<select disabled="disabled" name="typ" id="typ" class="select" style="width:225px;">
					<c:forEach items="${listBill }" var="codedes">
						<option value="${codedes.code }" <c:if test="${codedes.code == chkoutm.typ }">selected="selected"</c:if>>${codedes.des }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="document_number"/>:</div>
			<div class="form-input">
				<c:if test="${chkoutm.vouno!=null}"><c:out value="${chkoutm.vouno}"></c:out></c:if>			
				<input type="hidden" name="vouno" id="vouno" value="${chkoutm.vouno }"/>
<%-- 				<input disabled="disabled" class="text" id="vouno" value="${chkoutm.vouno }"/> --%>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/></div>
			<div class="form-input" style="width:220px;">
				<input style="width:224px;" disabled="disabled" class="Wdate text" id="maded" onfocus="WdatePicker({onpicked:function(){pickedFunc()}})" name="maded" value="<fmt:formatDate type="Date" pattern="yyyy-MM-dd" value="${chkoutm.maded }"/>"/>
			</div>
			<div class="form-label"><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${chkoutm.chkoutno!=null}"><c:out value="${chkoutm.chkoutno}"></c:out></c:if>
				<input type="hidden" name="chkoutno" value="${chkoutm.chkoutno }"/>
<%-- 				<input class="text" disabled="disabled" value="${chkoutm.chkoutno }"/> --%>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input" style="width:220px;">
				<input type="text"  id="positn"  name="positn.des" style="width:222px;" disabled="disabled" value="${chkoutm.positn.des}"/>
				<input type="hidden" id="positn_select" name="positn.code" value="${chkoutm.positn.code}"/>
<%-- 				<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />			 --%>
			</div>
			<div class="form-label"><fmt:message key="make_orders"/>:</div>
			<div class="form-input">
				<c:if test="${chkoutm.madeby!=null}"><c:out value="${chkoutm.madeby}"></c:out></c:if>
				<input type="hidden" name="madeby" value="${chkoutm.madeby }"/>
<%-- 				<input readonly="readonly" class="text" disabled="disabled" value="${chkoutm.madeby }"/> --%>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="use_positions"/></div>
			<div class="form-input" style="width:220px;">
				<input type="text"  id="firm"  name="firm.des" style="width:222px;" disabled="disabled" value="${chkoutm.firm.des}"/>
				<input type="hidden" id="firm_select" name="firm.code" value="${chkoutm.firm.code}"/>
<%-- 				<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
			</div>
			<div class="form-label"><fmt:message key="accounting"/>:</div>
			<div class="form-input">
				<c:if test="${chkoutm.checby!=null}"><c:out value="${chkoutm.checby}"></c:out></c:if>
				<input type="hidden" name="checby" value="${chkoutm.checby }"/>
<%-- 				<input class="text" disabled="disabled" name="checby" value="${chkoutm.checby }"/> --%>
			</div>			
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="summary"/></div>
			<div class="form-input" style="width:245px;">
				<input disabled="disabled" type="text" style="width:225px;" id="memo" class="text" name="memo" value="${chkoutm.memo }"/>
			</div>
			<div class="form-label"><fmt:message key="audit_remarks"/>:</div>
			<div class="form-input"><c:if test="${chkoutm.checby!=null}"><c:out value="${chkoutm.chk1memo}"></c:out></c:if>
				<input type="hidden" name="chk1memo" id="chk1memo" class="text" value="${chkoutm.chk1memo}" />				
			</div>
		</div>
		<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;">&nbsp;</td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td colspan="2"><fmt:message key="scm_sales"/></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="inventory"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td class="num"><span style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:140px;"><fmt:message key="name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:70px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:70px;"><fmt:message key="scm_sale_price"/></span></td>
								<td><span style="width:70px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height:100%;overflow: auto;">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
							<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
							<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
							<c:set var="sum_priceamt" value="${0}"/>  <!-- 总陈本金额 -->
							<c:forEach var="chkoutd" items="${chkoutm.chkoutd}" varStatus="status">
								<c:set var="sum_num" value="${status.index+1}"/>  
								<c:set var="sum_amount" value="${sum_amount + chkoutd.amount}"/>  
								<c:set var="sum_totalamt" value="${sum_totalamt + chkoutd.totalamt}"/>  
								<c:set var="sum_priceamt" value="${sum_priceamt + chkoutd.amount*chkoutd.pricesale}"/>  
								<tr data-unitper="${chkoutd.sp_code.unitper}" data-batchno="${chkoutd.batchno }">
									<td class="num"><span style="width:16px;">${status.index+1}</span></td>
									<td><span style="width:70px;" data-sp_name="${chkoutd.sp_code.sp_name }"><c:out value="${chkoutd.sp_code.sp_code }"/></span></td>
									<td><span style="width:140px;"><c:out value="${chkoutd.sp_code.sp_name }"/></span></td>
									<td><span style="width:70px;"><c:out value="${chkoutd.sp_code.sp_desc }"/></span></td>
									<td><span style="width:30px;"><c:out value="${chkoutd.sp_code.unit }"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.amount }"><fmt:formatNumber value="${chkoutd.amount }" pattern="##.##"/></span></td>
									<td><span style="width:40px;text-align: right;" title="${chkoutd.price }"><input type="hidden" value="${chkoutd.price }"/><fmt:formatNumber value="${chkoutd.price }" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.totalamt }"><fmt:formatNumber value="${chkoutd.totalamt }" pattern="##.##"/></span></td>
									<td><span style="width:30px;"><c:out value="${chkoutd.sp_code.unit1 }"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.amount1 }"><fmt:formatNumber value="${chkoutd.amount1 }" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.pricesale}"><fmt:formatNumber value="${chkoutd.pricesale}" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.pricesale * chkoutd.amount }"><fmt:formatNumber value="${chkoutd.pricesale * chkoutd.amount }" pattern="##.##"/></span></td>
									<td><span style="width:70px;text-align: right;" title="${chkoutd.sp_code.cnt }"><fmt:formatNumber value="${chkoutd.sp_code.cnt }" pattern="##.##"/></span></td>
<%-- 									<td><span style="width:80px;"><c:out value="${chkoutd.memo }"/>&nbsp;</span></td> --%>
									<c:choose>
										<c:when test="${fn:contains(chkoutd.memo, '##')}">
											<c:set var="memo" value="${fn:split(chkoutd.memo, '##')}"></c:set>
											<td><span style="width:80px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
<%-- 											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td> --%>
										</c:when>
										<c:otherwise>
											<td><span style="width:80px;" title="${chkoutd.memo}">${chkoutd.memo}</span></td>
<!-- 											<td><span style="width:70px;"></span></td> -->
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 26px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>&nbsp;&nbsp;</u></td>
							<td style="width:240px;"><fmt:message key="total_amount"/>:
								<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
							</td>
							<td style="width:240px;"><fmt:message key="scm_total_sale"/>:
								<u>&nbsp;&nbsp;<label id="sum_priceamt"><fmt:formatNumber value="${sum_priceamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
							</td>
						</tr>
					</thead>
				</table>
		   </div>	
	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			 	$(document).bind('keyup',function(e){
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 80: $('#autoId-button-102').click(); break;
		            }
				});  
			 	$('input').filter(':disabled').addClass('textDisable');
				<%-- curStatus 0:init;1:edit;2:add;3:show --%>
				//判断按钮的显示与隐藏
				loadToolBar([true,true]);
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),170);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法				
				$("select[name='typ'] option[value='<c:out value="${chkoutm.typ}"/>']").attr("selected","selected");
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						//selectPositn();
						var offset = getOffset('typ');
						top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#positn'),$('#positn_select'),'760','520','isNull');
					}
				});
				$('#seachOnePositn1').bind('click.custom',function(e){
					if(!!!top.customWindow){
						//selectPositn();
						var offset = getOffset('typ');
						top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#firm'),$('#firm_select'),'760','520','isNull');
					}
				});
			});
			
			
			//打印
			function printChkout(){
				window.open ("<%=path%>/chkout/printChkout.do?chkoutno="+$("#curChkout").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/><fmt:message key="storehouse_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkout();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								window.parent.detailWin.close();
							}
						}
					]
				});
				
			}
			function openChkout(chkoutno){
				window.location.replace("<%=path%>/chkout/updateChkout.do?chkoutno="+chkoutno);
			}
			function pickedFunc(){
				var cur=$('#maded').val();
				var uri = "<%=path%>/chkinm/getVouno.do?maded=" + cur;
				$.get(uri,function(data){
					$("input[name='vouno']").val(data);
				});
			}
			
		</script>
	</body>
</html>