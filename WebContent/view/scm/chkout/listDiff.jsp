<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--配送差异处理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">		
			.tool {
				position: relative;
				height: 27px;
			}
			.page{
				margin-bottom: 25px;
			}
			.grid td span{
				padding:0px;
			}
			.table-head td span{
				white-space: normal;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkout/listDiff.do" method="post">
		<!-- <input type="hidden" id="mold" name="mold"/> -->	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top;" value="${dis.positn}"/>
						<select name="positn" id="positn" class="select" style="width:320px;" onchange="$('#positn_select').val($('#positn').val());">
							<option value="">--</option>
							<c:forEach items="${positnOut }" var="positn">
								<option value="${positn.code }" title="${positn.des}"
								<c:if test="${positn.code == dis.positn }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label" style="margin-left:10px;"><fmt:message key ="Date_of_delivery" />：</div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:100px;" id="dat" name="dat" value="<fmt:formatDate value="${dis.dat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker();"/>
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="use_positions"/>:</div>
				<div class="form-input" style="width:370px;">
					<input type="text" class="text" id="firm_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${dis.firm}"/>
						<select name="firm" id="firm" class="select" style="width:320px;" onchange="$('#firm_select').val($('#firm').val());">
							<option value="">--</option>
							<c:forEach items="${positnIn }" var="positn">
								<option value="${positn.code }" title="${positn.des}"
								<c:if test="${positn.code == dis.firm }">
									selected="selected"
								</c:if>
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-input" style="width:220px;text-align:right; margin-left:10px;">
					[<input type="radio" id="iscy1" name="iscy" value="1"
						<c:if test="${dis.iscy=='1'}"> checked="checked"</c:if> /><fmt:message key ="Store_surplus" />
					<input type="radio" id="iscy2" name="iscy" value="2" 
						<c:if test="${dis.iscy=='2'}"> checked="checked"</c:if> /><fmt:message key ="Branch_loss" />]
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:15px;">&nbsp;</span></td>
								<td style="width:20px;text-align: center;"><input type="checkbox" id="chkAll"/></td>
								<td><span style="width:120px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:100px;"><fmt:message key ="library_positions" /></span></td>
								<td><span style="width:100px;"><fmt:message key ="branches_name" /></span></td>
								<td><span style="width:70px;"><fmt:message key ="supplies_code" /></span></td>
								<td><span style="width:100px;"><fmt:message key ="supplies_name" /></span></td>
								<td><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:60px;" title="<fmt:message key="standard_unit"/>"><fmt:message key="standard_unit"/></span></td>
								<td><span style="width:60px;" title="<fmt:message key="reference_unit"/>"><fmt:message key="reference_unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:80px;"><fmt:message key ="Quantity_of_delivery" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="Quantity_of_goods" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="Difference_quantity" /></span></td>
								<td><span style="width:80px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></td>
									<td><span title="${dis.vouno}" style="width:120px;text-align: center;">${dis.vouno}</span></td>
									<td><span title="${dis.positn }" style="width:100px;">${dis.positn }</span></td>
									<td><span title="${dis.firm }" style="width:100px;">${dis.firm }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
									<td><span title="${dis.spdesc }" style="width:60px;">${dis.spdesc }</span></td>
									<td><span title="${dis.unit }" style="width:60px;">${dis.unit }</span></td>
									<td><span title="${dis.unit1 }" style="width:60px;">${dis.unit1 }</span></td>
									<td><span title="${dis.pricesale }" style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.pricesale }" pattern="##.#" minFractionDigits="2" /></span></td>
									<td><span title="${dis.cntout}" style="width:80px;text-align: right;"><fmt:formatNumber value="${dis.cntout }" pattern="##.#" minFractionDigits="2" /></span></td>
									<td><span title="${dis.cntfirm}" style="width:80px;text-align: right;"><fmt:formatNumber value="${dis.cntfirm }" pattern="##.#" minFractionDigits="2" /></span></td>
									<td><span title="${dis.cntout-dis.cntfirm}" style="width:80px;text-align: right;"><fmt:formatNumber value="${dis.cntout-dis.cntfirm}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td>
										<span title="${dis.des}" style="width:80px;text-align: center;">${dis.des}
<%-- 											<input type="text" value="${dis.des}" style="width:60px;height:13px;"/> --%>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			loadToolBar();
			$("#positn_select").val($("#positn").val());
			$("#firm_select").val($("#firm").val());
			/*过滤*/
			$('#positn_select').bind('keyup',function(){
	          $("#positn").find('option')
	                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
	                    .attr('selected','selected');
	       });
			$('#firm_select').bind('keyup',function(){
	          $("#firm").find('option')
	                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
	                    .attr('selected','selected');
	       });
			//页面获得焦点
			focus() ;
			//按钮快捷键
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			});
		    
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();			
		});	
		
		//控制按钮显示
		function loadToolBar(){
			$('.tool').html('');
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key ="Offset_and_loss" />',
						title: '<fmt:message key ="Offset_and_loss" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							dealDiff(2);
						}
 			 		},{
 						text: '<fmt:message key ="Only_offset" />',
						title: '<fmt:message key ="Only_offset" />',
 						icon: { 
 							url: '<%=path%>/image/Button/op_owner.gif', 
 							position: ['-160px','-240px']
 						},
 						handler: function(){
 							//事件方法
 							dealDiff(1);
 						}
					},{
 						text: '<fmt:message key ="Create_offgain" />',
						title: '<fmt:message key ="Create_offgain" />',
 						icon: { 
 							url: '<%=path%>/image/Button/op_owner.gif', 
 							position: ['-240px','-240px']
 						},
 						handler: function(){
 							//事件方法
 							dealDiff(3);
 						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-320px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		}
		//查询出库和入库仓位
		$('#seachOnePositn').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#positn_select').val();
				var defaultName = $('#positn').val();
				var offset = getOffset('positn');
				top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positn_select'),'760','520','isNull',handler);
			}
		});
	 	function handler(a){
	 		 $("#positn").find("option[value='"+a+"']").attr('selected','selected');
	 	}
	 	$('#seachOnePositn1').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#firm_select').val();
				var defaultName = $('#firm').val();
				var offset = getOffset('positn');
				top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#firm_select'),'760','520','isNull',handler1);
			}
		});
	 	function handler1(a){
	 		 $("#firm").find("option[value='"+a+"']").attr('selected','selected');
	 	}
	 	
	 	//冲消并报损  报盈处理
	 	function dealDiff(flag){
	 		if((flag == 1 || flag == 2) && $('input[name="iscy"]:checked').val() != 2){
	 			alert('请选择分店亏并进行查询。');
	 			return;
	 		}else if(flag == 3 && $('input[name="iscy"]:checked').val() != 1){
	 			alert('请选择分店盈并进行查询。');
	 			return;
	 		}
	 		var msg = '';
	 		if(flag == 1){
	 			msg = '<fmt:message key ="The_system_will_generate_the_corresponding_single_offset_continue_to_confirm" />？';
	 		}else if(flag == 2){
	 			msg = '<fmt:message key ="The_system_will_generate_the_corresponding_single_offset_and_the_selected_materials_against_loss_while_generating_loss_Chukuchan_continue_to_confirm" />？';
	 		}else if(flag == 3){
	 			msg = '本操作将生成一个报盈补入的到门店的直拨单，确认继续？';
	 		}
	 		if(confirm(msg)){
	 			if($('.grid').find('.table-body').find('tr').size() == 0){
	 				alert('<fmt:message key="there_is_no"/><fmt:message key="data"/>！');
	 				return;
	 			}
	 			if($('#positn').val() == '' || $('#firm').val() == ''){
	 				alert('<fmt:message key="please_select_positions"/>、<fmt:message key="store"/>！');
	 				return;
	 			}
	 			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(i){
						chkValue.push($(this).val());
					});
					$.ajax({
		 				url:'<%=path%>/chkout/dealDiff.do?typ='+flag+'&dat='+$('#dat').val()+'&positn='+$('#positn').val()+'&firm='+$('#firm').val()+'&iscy='+$('input[name="iscy"]:checked').val()+'&ids='+chkValue.join(","),
		 				type:'post',
		 				success:function(data){
		 					try{
			 					var rs = eval("("+data+")");
			 					if(rs.pr == 1){
			 						alert('<fmt:message key="operation_successful"/>！');
			 						$('#listForm').submit();
								}else{
									showMessage({
										type: 'error',
										msg: rs.msg,
										speed: 3000
										});
								}
		 					}catch(e){
		 						alert(data);
		 					}
		 				}
		 			});	 	
				}else{
					alert('<fmt:message key="please_select"/><fmt:message key="operation"/><fmt:message key="data"/>！');
				}
	 		}
	 	}
		</script>
	</body>
</html>