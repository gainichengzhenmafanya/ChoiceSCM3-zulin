<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>门店报价列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
/* 			.page{margin-top: -150px;} */
		</style>
	</head>
	<body>
		<input type="hidden" name="sta" id="sta" value="select"/>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/sppriceFirm/toSppriceSale.do?positnCode=${positnCode }" method="post">
				<div class="grid" style="height: 400px;">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td colspan="6" align="left"><fmt:message key="supplies"/><fmt:message key="scm_sale_price"/></td>
								</tr>
								<tr>
									<td><span style="width:80px;"><fmt:message key="supplies_code"/></span></td>
									<td><span style="width:80px;"><fmt:message key="supplies_name"/></span></td>
									<td><span style="width:80px;"><fmt:message key="supplies"/><fmt:message key="price"/></span></td>
									<td><span style="width:80px;"><fmt:message key="previous_price"/></span></td>
									<td><span style="width:100px;"><fmt:message key="starttime"/></span></td>
									<td><span style="width:100px;"><fmt:message key="endtime"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="spprice" items="${sppriceSales}" varStatus="status">
									<tr>
										<td><span style="width:80px;">${spprice.sp_code}</span></td>
										<td><span style="width:80px;">${spprice.sp_name}</span></td>
										<td><span style="width:80px;text-align: right;">${spprice.price }</span></td>
										<td><span style="width:80px;text-align: right;">${spprice.priceold}</span></td>
										<td><span style="width:100px;"><fmt:formatDate value="${spprice.bdat}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:100px;"><fmt:formatDate value="${spprice.edat}" pattern="yyyy-MM-dd"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),1);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',30);	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		});
		
		function pageReload(){
			/* var sta=$('#sta').val();
			if("update"==sta){//只刷新mainFrame
		    	window.mainFrame.location.href = window.mainFrame.location.href;
			}else{ */
	        	window.location.href = '<%=path%>/chkstodemom/listChkstoDemom.do';
			/* } */
	    }
		</script>
	</body>
</html>