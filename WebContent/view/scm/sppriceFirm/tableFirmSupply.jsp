<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店物资</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.line{
					BORDER-LEFT-STYLE: none;
					BORDER-RIGHT-STYLE: none;
					BORDER-TOP-STYLE: none;
					width: 70px;
				}
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.spprice{
					width: 100%;
					height: 240px;
					border: 1px solid #f2f2f2;
/* 					overflow-y: auto; */
					margin-top: -1px;
				}
				.sppricesale{
					width: 100%;
					height: 239px;
					display:inline-block;
					border: 1px solid #f2f2f2;
					margin-top:20px;
				}
				table{width: 98%;padding: 0px;margin-top: -2px;}
				.page{position: absolute;margin-bottom: 301px;}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/sppriceFirm/table.do" id="listForm" name="listForm" method="post">
		<input type="hidden" name="positn" id="positnCode" value="${positnCode}"></input>
		<!-- 报价 -->
		<div class="spprice">
			<iframe frameborder="0" src="<%=path%>/sppriceFirm/toSpprice.do?positnCode=${positnCode }" scrolling="no" id="spprice" name="spprice"></iframe>
		</div>
		<!-- 售价 -->
		<div class="sppricesale">
			<iframe frameborder="0" src="<%=path%>/sppriceFirm/toSppriceSale.do?positnCode=${positnCode }" scrolling="no" id="sppricesale" name="sppricesale"></iframe>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				//分店编码
// 				var positnCode = window.parent().find("tr").find(":checked").val();
// 				alert(positnCode);
				
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="offer"/><fmt:message key="scm_copy"/>',
							title: '<fmt:message key="offer"/><fmt:message key="scm_copy"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var code = [];
								window.parent.$(".grid").find(".table-body").find('tr').find(":checkbox").each(function(){
									if($(this).attr("checked") == 'checked'){
										code.push($(this).val());
									}
								});
								
								if(code.length == 1){
									$("#positnCode").val(code);//执行一次返回就没值了，再附上 wjf
									copySpprice();								
								}else if(code.length > 1){
									alert("<fmt:message key='please_select'/>一家<fmt:message key='branche'/>！");
								}else{
									alert("<fmt:message key='please_select_branche'/>！");
								}
							}
						},{
							text: '<fmt:message key="scm_sale_price"/><fmt:message key="scm_copy"/>',
							title: '<fmt:message key="scm_sale_price"/><fmt:message key="scm_copy"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var code = [];
								window.parent.$(".grid").find(".table-body").find('tr').find(":checkbox").each(function(){
									if($(this).attr("checked") == 'checked'){
										code.push($(this).val());
									}
								});
								
								if(code.length == 1){
									$("#positnCode").val(code);//执行一次返回就没值了，再附上 wjf
									copySppriceSale();								
								}else if(code.length > 1){
									alert("<fmt:message key='please_select'/>一家<fmt:message key='branche'/>！");
								}else{
									alert("<fmt:message key='please_select_branche'/>！");
								}
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
								
				function copySpprice() {
					var positnCode = $("#positnCode").val();
					var action = '<%=path%>/sppriceFirm/copy.do?positnCode='+positnCode+'&type=0';
					$('body').window({
						title: '<fmt:message key ="Copy_the_data_from_the_store_to_the_following" />！',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 750,
						height: '500px',
						draggable: true,
						isModal: true
					});
				}
				
				function copySppriceSale() {
					var positnCode = $("#positnCode").val();
					var action = '<%=path%>/sppriceFirm/copy.do?positnCode='+positnCode+'&type=1';
					$('body').window({
						title: '<fmt:message key ="Copy_the_data_from_the_store_to_the_following" />！',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 750,
						height: '500px',
						draggable: true,
						isModal: true
					});
				}
			});
			
			function pageReload(){
				parent.refreshTree();
			}
		</script>
	</body>
</html>