<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>产品理论成本分析</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
				.form-line .form-label{
					width: 5%;
				}
			</style>								
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/exAna/exTheoryCostAna.do" id="listForm" name="listForm" method="post">
		<input type="hidden" id="sortName" name="sortName" value="${exThCostAna.sortName}"/>
		<input type="hidden" id="sortOrder" name="sortOrder" value="<c:out value="${exThCostAna.sortOrder}" default="asc"/>"/>
		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input">
			    <input autocomplete="off"  type="text" id="dat" name="dat" class="Wdate text" value="<fmt:formatDate value="${exThCostAna.dat}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="processing_room"/></div>
			<div class="form-input">
	 			<input type="text" name="positnexdes" id="positnexdes" class="selectDepartment text" readonly="readonly" value="${exThCostAna.positnexdes}"/>
				<input type="hidden" name="positnex" id="positnex" class="text" value="${exThCostAna.positnex}" />
			</div>		
			<div class="form-label"><fmt:message key="coding"/></div>
			<div class="form-input">
				<input type="text" class="text" id="sp_code" name="sp_code" value="${exThCostAna.sp_code}" /><img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="查询物资" />
			</div>						
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input">
			 	  <input autocomplete="off"  type="text" id="datEnd" name="datEnd" class="Wdate text" value="<fmt:formatDate value="${exThCostAna.datEnd}" pattern="yyyy-MM-dd"/>" />
			</div>
			<div class="form-label"><fmt:message key="category"/></div>
			<div class="form-input">
				<input type="text" name="grpdes" id="grpdes" class="selectDepartment text" readonly="readonly" value="${exThCostAna.grpdes}"/>
				<input type="hidden" name="grp" id="grp" class="text" value="${exThCostAna.grp}" />
			</div>							
		</div>
			 
		<table id="test" style="width:'100%';height:200px"
				title="<fmt:message key ="Product_theoretical_cost_analysis" />" singleSelect="true" rownumbers="true" showFooter=true remoteSort="true"
				sortName="${exThCostAna.sortName}"
				sortOrder="${exThCostAna.sortOrder}"
				idField="itemid" >
			<thead>
				<tr>
					<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
						<th field="${dictColumns.columnName}" width="${dictColumns.columnWidth}" align="center"><span id="_${dictColumns.columnName}"/>${dictColumns.zhColumnName}</span></th>
					</c:forEach>
				</tr>
			</thead>
		</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="query_scheduling_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								var dat = $("#dat").val();
								var eat = $("#datEnd").val();
								if(dat<=eat){		
									$('#listForm').submit();
								}else{
									alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />!");
								}
							}
						},{
							text: '<fmt:message key ="print" />',
							title: '<fmt:message key ="print" />',
						//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								var action="<%=path%>/exAna/printExTheoryCostAna.do";
								$('#listForm').attr('action',action);
								$('#listForm').submit();	
								$('#listForm').attr('target','');
								$('#listForm').attr('action','<%=path%>/exAna/exTheoryCostAna.do');
								$("#wait2").val('');//等待加载还原
							}
						},{
							text: 'Excel',
							title: 'Excel',
					//		useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-80px']
							},
							handler: function(){ 
								$("#wait2").val('NO');
								$('#listForm').attr("action","<%=path%>/exAna/exportExTheoryCostAna.do");
								$('#listForm').submit();
								$('#listForm').attr("action","<%=path%>/exAna/exTheoryCostAna.do");
								$("#wait2").val('');
							}
						},{
							text: '<fmt:message key ="column_selection" />',
							title: '<fmt:message key ="column_selection" />',
						//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								toSelectColumns();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
//datagrid  开始

 				$('#test').datagrid({
 					rowStyler:function(){
 						return 'line-height:11px';
 					}
 				});
// 				    url: null,
// 					title: 'DataGrid',
// 					width: '100%',
// 					height: 400,
// 					fitColumns: true,
// 					nowrap:false,
// 					rownumbers:true,
// 					showFooter:true,
// 					total:[20]        
// 					columns:[[
// 					          {title:'物资',colspan:5},
// 								{field:'price',title:'单价',rowspan:2,width:100},
// 								{field:'amt',title:'金额',rowspan:2,width:100},
// 								{field:'costamt',title:'理论成本',rowspan:2,width:100,align:'right'}
// 					          ],[
// 								{field:'sp_code',title:'产品编码',width:100},
// 								{field:'sp_name',title:'产品名称',width:100},
// 								{field:'sp_desc',title:'产品规格',width:100},
// 								{field:'unit',title:'单位',width:100},
// 								{field:'amount',title:'产量',width:100}
// 								]]
// 				});		
			//	alert(rows);
			//	var rows = {};

// 			var obj={"total":28,"rows":[
// 			                       {"sp_code":"AV-CB-01","sp_name":92.00,"status":"P","listprice":63.50,"attr1":"Adult Male","itemid":"EST-18"}
// 			                        ],"footer":[
// 			                        	{"sp_code":19.80,"listprice":60.40,"productid":"Average:"},
// 			                        	{"unitcost":198.00,"listprice":604.00,"productid":"Total:"}
// 			                        ]};
				var obj={};
				var rows = ${ListBean};
				var footer = ${ListBean_total};
 				obj['rows']=rows;
 				obj['footer']=footer;
				$('#test').datagrid('loadData',obj);
				//排序开始
				$('.datagrid-header').find(".datagrid-cell").click(function (){
	                $('#sortName').val($(this).parent("td").attr("field"));
					if($('#sortOrder').val()=='asc'){
						$('#sortOrder').val('desc');
					}else{
						$('#sortOrder').val('asc');
					};
					$('#listForm').submit();
				});
				$('#_'+$('#sortName').val()).parents('.datagrid-cell').addClass('datagrid-sort-'+$('#sortOrder').val());
				//排序结束
					setElementHeight('.panel-body',['.tool'],$(document.body),125);
// 				$('.panel-body').height(800);
//datagrid  结束
				/*弹出树*/
				$('#grpdes').bind('focus.custom',function(e){
					var grp=$('#grp').val();
					if(!!!top.customWindow){
						var offset = getOffset('grpdes');
						top.custom('<fmt:message key="please_select_category"/>','<%=path%>/explan/selectGrp.do?grp='+grp,offset,$(this),$('#grp'),null,null,null,'150','300','isNull');
					}
				});
				$('#positnexdes').bind('focus.custom',function(e){
					var positnex=$('#positnex').val();
					if(!!!top.customWindow){
						var offset = getOffset('positnexdes');
						top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#positnex'),null,null,null,'150','300','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
					}
				});
				$('#dat').bind('click',function(){
					new WdatePicker();
				});
				$('#datEnd').bind('click',function(){
					new WdatePicker();
				});
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				$(".datagrid-body").find("table").find("tr").map(function(){
					$(this).find("td:eq(4)").find(".datagrid-cell").css("text-align","right");
					$(this).find("td:eq(5)").find(".datagrid-cell").css("text-align","right");
					$(this).find("td:eq(6)").find(".datagrid-cell").css("text-align","right");
					$(this).find("td:eq(7)").find(".datagrid-cell").css("text-align","right");
					$(this).find("td:eq(8)").find(".datagrid-cell").css("text-align","right");
					$(this).find("td:eq(9)").find(".datagrid-cell").css("text-align","right");
				});
		}); 
			/*弹出物资树*/
			function supply_select(){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'));	
				}
				
			};
			function toSelectColumns(){
				$('body').window({
					title: '<fmt:message key="column_selection" />',
					content: '<iframe frameborder="0" src="<%=path%>/exAna/toColumnsChoose.do"></iframe>',
					width: '460px',
					height: '430px',
					isModal: true
				});
			}
		</script>
	</body>
</html>