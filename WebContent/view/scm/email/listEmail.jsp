<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>收件箱</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.ico-unread {
				    background-position: 0px -15px;
				    background-image: url("<%=path%>/image/maillist.png");
			    	    background-repeat: no-repeat;
			    	    width: 15px;
			    	    height: 18px;
				}
				.ico-read {
				    background-position: -16px -15px;
				    background-image: url("<%=path%>/image/maillist.png");
			    	    background-repeat: no-repeat;
			    	    width: 15px;
			    	    height: 18px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				form .search-div .form-line .form-label{
					width: 5%;
				}
				form .search-div .form-line .form-input{
					width: 13%;
				}
				form .search-div .form-line .form-input input{
					width: 90%;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>								
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/message/list.do" id="listForm" name="listForm" method="post">
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label"><fmt:message key ="Sender" />：</div>
					<div class="form-input"><input type="text" id="senderName" name="senderName" onkeyup="ajaxSearch()" value="${senderName}" class="text" /></div>
					<div class="form-label"><fmt:message key="title"/>：</div>
					<div class="form-input"><input type="text" id="title" name="title" onkeyup="ajaxSearch()" value="${title}" class="text" /></div>
					<div class="form-label"><fmt:message key="content"/>：</div>
					<div class="form-input"><input type="text" id="content" name="content" onkeyup="ajaxSearch()" value="${content}"  class="text" /></div>
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input"><input type="text" id="startTime" name="startTime" class="Wdate text" value="<fmt:formatDate value="${startTime}" pattern="yyyy-MM-dd"/>" onclick="date();"/>到<input type="text" id="endTime" name="endTime" class="Wdate text" value="<fmt:formatDate value="${endTime}" pattern="yyyy-MM-dd"/>" onclick="date();"/></div>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select"/>'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty"/>'/>
				</div>
			</div>
			<!-- <div>排序方式：时间<input type="radio" name="orderby" value="sendtime desc" checked="checked" onchange="sort('sendtime desc')" />发件人<input type="radio" name="orderby" value="sender"  onchange="sort('sender')"/></div> -->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll" value='<c:out value="${id}" />'></input>
								</td>
								<td style="width:30px;"></td>
								<td><span style="width:100px;"><fmt:message key ="Sender" /></span></td>
								<td><span style="width:200px;"><fmt:message key="title"/></span></td>
								<td><span style="width:300px;"><fmt:message key="content"/></span></td>
								<td><span style="width:150px;"><fmt:message key="time"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="message" items="${messageList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${message.id}' />" value="<c:out value='${message.id}' />"/>
									</td>
									<td style="text-align: center;"><span style="margin-left:8px;width:22px;">
										<c:choose>
											<c:when test="${message.state==1}"><span class="ico-read" title="已读" ></span></c:when>
											<c:otherwise><span class="ico-unread" title="未读"></span></c:otherwise>
										</c:choose>
									</span></td>
									<td><span style="width:100px;"><c:out value="${message.senderName}" />&nbsp;</span></td>
									<td><span style="width:200px;"><c:out value="${message.title}" />&nbsp;</span></td>
									<td><span style="width:300px;"><c:out value="${message.content}" />&nbsp;</span></td>
									<td><span style="width:150px;"><fmt:formatDate value="${message.sendTime}" pattern="yyyy-MM-dd HH:mm:ss"/>&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">		
			var win;
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearlistForm();
				});
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select"/>',
						title: '<fmt:message key="select_the_information_notice"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
							var t = $('#senderName').val();
							$('#senderName').focus().val(t);
						}
					},"-",{
							text: '<fmt:message key ="Write_information" />',
							title: '<fmt:message key ="Write_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								sendMessage();
							}
						},{
							text: '<fmt:message key ="Delete_information" />',
							title: '<fmt:message key ="Delete_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteMessage();
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
			                if (!!$("#chkAll").attr("checked")) {
			                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
			                }else{
			                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
		                	      }
			            });
				$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				    var messageId = $(this).find('td :checkbox')[0].value;
				    var value = $(this).find('td span')[1].title;
				    var state;
				    if(value=='未读'){
				    	state=0;
				    }else{
				    	state=1;
				    }
				    showMessage(messageId,state);
				    $(this).find('td span')[1].className='ico-read';
				});
				
			});
			
			function sendMessage(id){
				var action='<%=path%>/message/add.do';
				if(win && id!=undefined){
					win.close();
					action='<%=path%>/message/add.do?id='+id;
				}
				
				$('body').window({
					id: 'window_addMessage',
					title: '<fmt:message key ="Write_information" />',
					content: '<iframe id="addMessageFrame" frameborder="0" src='+action+'></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true,
					confirmClose:false,
					topBar: {
						items: [{
								text: '<fmt:message key ="Send_out" />',
								title: '<fmt:message key ="Send_out" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('addMessageFrame') && getFrame('addMessageFrame').validate._submitValidate()){
										submitFrameForm('addMessageFrame','messageForm');
									}
								}
							}
						]
					}
				});
			}
			
			function deleteMessage(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm"/>？')){
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							codeValue.push($(this).val());
						});
						var action = '<%=path%>/message/delete.do?ids='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key ="Delete_message" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 538,
							height: '215px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
					return ;
				}
			}
			
			//查看信息
			function showMessage(messageId,state){
				win=$('body').window({
					id: 'window_addMessagement',
					title: '<fmt:message key ="View_information" />',
					content: '<iframe id="showMessagementFrame" frameborder="0" src="<%=path%>/message/show.do?id='+messageId+'&state='+state+'"></iframe>',
					width: 500,
					height: '300px',
					confirmClose:false,
					draggable: true,
					isModal: true
				});
				
			};
			
			function clearlistForm(){
				$("#senderName").val("");
				$("#title").val("");
				$("#content").val("");
				$("#startTime").val("");
				$("#endTime").val("");
			}
			function date(){
				new WdatePicker();
			}
			//排序
			function sort(type){
				 
			}
		</script>
	</body>
</html>