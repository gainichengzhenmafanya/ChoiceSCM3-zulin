<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	</head>
	<body>
	<div class="form">
			<form id="messageForm" method="post" action="<%=path %>/message/saveByAdd.do">
				<div style="font-size:medium;">
					<span style="font-size: small;"><fmt:message key ="Sender" />：</span>
					<span style="color: blue;"><c:out value="${message.senderName }"/></span>
					<span style="float: right;margin-right: 20px;color: orange;"><a href="#" onclick="reply()" style="text-decoration: none;">快速回复</a></span>
				</div>
				<p style="font-weight:bold;text-align: center;margin-top: 5px;">
					<c:out value="${message.title }"/>
				</p>
				
				<div><textarea wrap="physical" style="width: 480px;height: 200px;margin-left: 5px;margin-top: 5px;" readonly="readonly"><c:out value="${message.content }" /></textarea></div>
				
			</form>
			</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		
		<script type="text/javascript">
			function reply(){
				var id = '${message.id}';
				parent.sendMessage(id);
			}
		</script>	
	</body>
</html>