<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="suppliers_find" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.tool {
				position: relative;
				height: 27px;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<input type="hidden" name="id" id="id"/>
	<input type="hidden" name="name" id="name"/>
	<form id="listForm" action="<%=path%>/message/findAccount.do" method="post">
			<div class="form-line">
				<div class="form-input" style="width: 100px;">
					<input type="text" name="searchInfo" id="key" title="<fmt:message key ="Fuzzy_query" />" value="${searchInfo }" style="width: 100px;margin-bottom: 1px;"/>
					<input type="button" id="search" value="<fmt:message key ="select" />" />
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:25px;">&nbsp;</td>
								<td><span style="width:80px;"><fmt:message key ="Full_name" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" class="datagrid">
						<tbody>
							<c:forEach var="message" items="${messageList}" varStatus="status">
								<tr>
									<td style="width:25px;text-align: center;"><input type="checkbox" value='<c:out value="${message.sender}" />' /></td>
									<td><span style="width:80px;">${message.senderName}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>			
			</div>
	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="enter"/>',
						title: '',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							getReciever();
							top.closeCustom();
						}
					}]
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				         $(this).removeClass('tr-select');
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				         $(this).addClass('tr-select');
				     }
				 });
				
				$('#search').bind('click',function(){
					$('#listForm').submit();
				})
			});	
			
			function getReciever(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var codeValue=[];
				var nameValue=[];
				checkboxList.filter(':checked').each(function(){
					codeValue.push($(this).val());
					nameValue.push($(this).parent().next().find('span')[0].innerHTML);
				});
				$("#id").val(codeValue);
				$('#name').val(nameValue);
			}
		</script>
	</body>
</html>