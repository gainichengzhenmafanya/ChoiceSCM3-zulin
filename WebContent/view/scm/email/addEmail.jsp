<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="new_information_notice"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				.form-line .form-lable{
					width: 8%;
				}
				.form-line .form-input{
					width: 80%;
				}
				
			</style>
	</head>
	<body>
	<div class="form">
			<form id="messageForm" method="post" action="<%=path %>/message/saveByAdd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key ="Addressee" />：</div>
					<div class="form-input">
						<input type="text" id="receiverName" name="receiverName" value="${receiverName }"  class="text"  style="width: 400px;cursor: pointer;" readonly="readonly" />
						<input type="hidden" id="receiver" name="receiver" value="${receiver }" class="text" style="width: 400px;margin-left: 70px;"/>
						<img src="../image/selectDepartment.gif" id="openAccount" style="cursor: pointer;" title="<fmt:message key ="Select_recipients" />" />
					</div>
				</div>
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key ="title" />&nbsp;&nbsp;&nbsp;：</div>				
					<div class="form-input">
						<input type="text" id="title" name="title" class="text" value="${title }" style="width: 400px;"/>
					</div>
				</div>
				
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key ="content" />&nbsp;&nbsp;&nbsp;：</div>
<!-- 					<div style="float: left;"></div> -->
					<div class="form-input" style="margin-top: 5px;">
						<textarea wrap="physical" id="content" onpropertychange="checkLength()" name="content" style="width: 400px;height: 180px;"></textarea>
					</div>
				</div>
			</form>
			</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var offset = getOffset('openAccount');
				$('#openAccount,#receiverName').bind('click',function(e){
					top.simpleCust('<fmt:message key ="Select_recipients" />','<%=path%>/message/findAccount.do',offset,'name','id','150','300',setReceiver);
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'receiverName',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="Addressee" /><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'title',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="title"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'title',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="title_too_long"/>！']
					},{
						type:'text',
						validateObj:'content',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="content"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				
				
			});
			function checkLength(){
				var curLength = $("#content").val().length;
				$("#count").html(curLength+'/2000');
				if(curLength>2000){
					$("#content").val($("#content").val().substring(0,2000));
					$("#count").html($("#content").val().length+'/2000');
				}
			}
			
			function setReceiver(obj){
				var receiver = obj['id'];
				var receiverName = obj['name'];
				$("#receiverName").val(receiverName);
				$("#receiver").val(receiver);
			}
		</script>
	</body>
</html>