<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Basic Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="" id="import" method="post" enctype="multipart/form-data">
			<div  class="form-input">
			    <FONT align="center" color=red>* </FONT>请选择上传模版类型：
				<select id="codeIn" class="select" name="codeIn">		
					<c:forEach var="template" items="${listTemplate}" varStatus="status">
						<option value="${template.code }">${template.code } -- ${template.name }</option>
					</c:forEach>
				</select>
			</div>
			<div  id="divImport">
						<FONT color=red align="center">* </FONT>请选择需要上传的个性化模版：
								<input type="file" style="WIDTH: 250px" maxLength=5 name="file" id="file"/>
			</div> 
			
			<div  class="form-input">
			    <FONT align="center" color=red>* </FONT>请选择导出标准模版类型：
				<select id="codeOutStandard" class="select" name="codeOutStandard">				
					<c:forEach var="template" items="${listTemplate}" varStatus="status">
						<option value="${template.code }">${template.code } -- ${template.name }</option>
					</c:forEach>
				</select>
			</div>
			<div  id="divImport">
					<button id="exportStandard" type="button">导出!</button>
			</div> 
			<div  class="form-input">
			    <FONT align="center" color=red>* </FONT>请选择导出个性化模版类型：
				<select id="codeOutIndividualization" class="select" name="codeOutIndividualization">				
					<c:forEach var="template" items="${listTemplate}" varStatus="status">
						<option value="${template.code }">${template.code } -- ${template.name }</option>
					</c:forEach>
				</select>
			</div>
			
			<div  id="divImport">
					<button id="exportIndividualization" type="button">导出!</button>
			</div> 
		</form>
		<br align="center">
		----------------------------------------------------------------------
		<br>
		<FONT color=red>导入模版注意事项： </FONT><%-- <br>
		 1.<fmt:message key="download_template_remind" /><br> --%>
<%-- 		 2.编码必须为数字。<fmt:message key="coding_must_be_num" /><br>								 --%>
		 <br>1.导入的模版必须为*.jrxml 格式<br>
		 <FONT color=red><br>2.上传之前,请确认文件可以在ireport中编译成功<br></FONT>
		 <br>3.下载模版请按照需求下载标准模版或者个性模版
		 <br>	
		<br>	
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			var tool;
			$(document).ready(function(){
				tool = _tool();
				
				$('#exportStandard').click(function(){
				      $("#import").attr('action','<%=path%>/template/filePathUseStandard.do').submit();
				});
				
				$('#exportIndividualization').click(function(){
					
					$.ajax({
						type: "POST",
						url: "<%=path%>/template/fileExitsOrNot.do",
						data: "codeOutIndividualization="+$('#codeOutIndividualization').val(),
						dataType: "json",
						success:function(reult){
							var err = eval("(" + reult + ")")
							if(err=='0'){
								$("#import").attr('action','<%=path%>/template/filePathUseIndividualization.do').submit();
							}else{
								alert("您尚未上传过该个性化模版。");
							}
						}
					});
				   
				});		
					
			});
				
			
			var _tool=function(){
				return $('.tool').toolbar({
					items: [
					 {
						text: '<fmt:message key="import" />',
						title: '<fmt:message key="import" /><fmt:message key="supplies_information" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							handle();
						}
					}<%-- ,
					{
						text: '<fmt:message key="template_download" />',
						title: '<fmt:message key="template_download" />',
						icon: {
							url: '<%=path%>/image/Button/excel.bmp'
						},
						handler: function(){
							$("#import").attr('action','<%=path%>/supply/downloadTemplate.do').submit();
						}
					} --%>
				]
				});
				}
			function handle(){
				var file = $("#file").val();
				if(file == ''){
					alert('<fmt:message key="please_select_the_imported" />');
					return false;
				}
				 var pos = file.lastIndexOf(".");
				 var lastname = file.substring(pos,file.length); 
				
			   if (!(lastname.toLowerCase()==".jrxml")){
				     alert('<fmt:message key="type_of_file_uploaded" />'+lastname+'，文件必须为*.jrxml格式');
				     return false;
			   }else{
					$("#import").attr('action','<%=path%>/template/loadJrxml.do').submit();
			  	}
			}
			
	
		</script>
	</body>
</html>