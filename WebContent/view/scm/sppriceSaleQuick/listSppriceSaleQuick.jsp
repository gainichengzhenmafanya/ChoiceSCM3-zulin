<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>报价快速加入</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.line{
			BORDER-LEFT-STYLE: none;
			BORDER-RIGHT-STYLE: none;
			BORDER-TOP-STYLE: none;
			width: 70px;
		}
	</style>
</head>
<body>
	<div class="bj_head">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="historical_supply_reference"/></div>
			<div class="form-input" style="width:190px;">
				<input type="text"  id="firm" name="firm" readonly="readonly"/>
				<input type="hidden" id="firm_select" name="firm_select"/>
				<input type="hidden" id="sp_name" name="sp_name"/>
				<input type="hidden" id="sp_code" name="sp_code"/>
				<img id="seachFirm" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
			</div>
			<div class="form-input">
				<input type="button" id="saveSp" onclick="seachSupply()" value="<fmt:message key="insert"/><fmt:message key="supplies"/>"/>
				<input type="button" id="updateTim" onclick="upTime()" value="<fmt:message key="scm_bulk_update"/><fmt:message key="time"/>"/>
				<input type="button" id="saveQuick" onclick="saveSppriceSale()" value="<fmt:message key="confirm_the_added_to_the_sale_quotation_and_review"/>"/>
				<input type="button" id="againSave" disabled="disabled" onclick="againSave()" value="<fmt:message key="lift_the_grey_continue_to_add"/>"/>
			</div>
		</div>
	</div>
	<div class="leftFrame" style="width: 62%">
		<iframe src="<%=path%>/sppriceSaleQuick/findSppriceSaleQuick.do?deliver.code=''" style="overflow-x:scroll;" frameborder="0" name="sppriceFrame" id="sppriceFrame"></iframe>
	</div>
	<div style="display: inline-block;float: left;width: 38%;" class="mainFrame">
   		<iframe src="<%=path%>/sppriceQuick/searchAllPositn.do" style="overflow-x:scroll;" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
   	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		//供应单位
		$("#seachFirm").click(function(){
			chooseStoreSCM({
				basePath:'<%=path%>',
				width:800,
				firmId:$("#firm_select").val(),
				single:true,
				tagName:'firm',
				tagId:'firm_select',
				title:'<fmt:message key="please_select_branche"/>',
				callBack: 'saveSpOk'
			});
		});
		
		
		//激活 物质添加按钮
		saveSpOk=function(firm_select){
			$("#saveSp").prop("disabled", false);
			$("#saveQuick").prop("disabled", false);
			$("#updateTim").prop("disabled", false);
			var firm_select  = $("#firm_select").val();
			if(firm_select!=''){//YUKU-126
				window.sppriceFrame.location = '<%=path%>/sppriceSaleQuick/findSppriceSaleQuick.do?deliver.code='+firm_select;//修改为不去查询demo表，直接查询sppricesail wjf
			}
		}
		seachSupply=function(){
			if(!!!top.customWindow){
				top.cust('<fmt:message key ="please_select_materials" />','<%=path%>/supply/addSupplyBatch.do',0,$('#sp_name'),$('#sp_code'),'850','550','isNull',addSp);	
			}
		};
		addSp=function(id){
			$.ajax({
				type : 'GET',
				contentType : 'application/json;charset=UTF-8',
				url : '<%=path%>/sppriceSaleQuick/findById.do?sp_code='+id,
				dataType : 'json',
				success : function(list) {
					$("#saveQuick").prop("disabled", false);
					$('#sppriceFrame')[0].contentWindow.NewInput();
					var bdat=$("#sppriceFrame").contents().find("#updateBdat").val();
					var edat=$("#sppriceFrame").contents().find("#updateEdat").val();
					var codes = '';
					$('#sppriceFrame').contents().find('#sp_data').find('tr').each(function(e){
						codes += $(this).find('td:eq(1)').text()+",";
					});
					for ( var i = 0; i < list.length; i++) {
						if(codes.indexOf(list[i].sp_code) != -1 ){//已存在
							continue;
						}
						var sp_desc = "";
						if(list[i].sp_desc!=null){
							sp_desc =list[i].sp_desc;
						}
						var sp_mark = "";
						if(list[i].sp_mark!=null){
							sp_mark =list[i].sp_mark;
						}
						$('#sppriceFrame').contents().find('#sp_data').append('<tr>'
							+'<td style="width:30px; text-align: center;">'
								+'<input type="checkbox" name="idList" id="chk_'+list[i].sp_code+'" value="'+list[i].sp_code+'"/>'
							+'</td>'
							+'<td style="width:100px; text-align: left;">'
								+'<span style="width: 90px;" id="sp_code">'+list[i].sp_code+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_name">'+list[i].sp_name+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_desc">'+sp_desc+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
							+'<span style="width: 70px;" id="sp_mark">'+sp_mark+'</span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;padding: 0px;"><input type="text" style="text-align: right;" onfocus="this.select()" class="line" id="price" value="'+list[i].sp_price+'"/></span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;" id="unit1">'+list[i].unit+'</span>'
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+bdat+'" style="width: 95px;height: 15px;margin: 0px;" id="bdat" name="bdat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+edat+'" style="width: 95px;height: 15px;margin: 0px;" id="edat" name="edat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:60px; text-align: left;">'
								+'<span style="width: 60px;padding: 0px;"><input type="text" style="text-align: right;" disabled="disabled" onfocus="this.select()" class="line" id="sp_price" value="'+list[i].priceold+'"/></span>'
							+'</td>'
						+'</tr>');
					}
					$('#sppriceFrame')[0].contentWindow.NewInput();
				}
			});
		};
		upTime=function(){
			if($("#sppriceFrame").contents().find('#sp_data :checkbox').filter(':checked').size()>0){
				$("#sppriceFrame")[0].contentWindow.showTimWindow();
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Select_the_data_you_want_to_modify" />..！',
					speed: 2500
				});
			}
		};
		saveSppriceSale=function(){
			var SppriceSale={};
			var size=0;
			var NotNull=true;
			var sppriceList=$("#sppriceFrame").contents().find('#sp_data :checkbox');
			sppriceList.filter(':checked').each(function(index){
				var price=$(this).parent().parent().find("td:eq(5) input").val();//单价
				var bdat=$(this).parent().parent().find("td:eq(7) input").val();//开始时间
				var edat=$(this).parent().parent().find("td:eq(8) input").val();//结束时间
				if(price==""||bdat==""||edat==""||price==null||bdat==null||edat==null){
					NotNull=false;
					return;
				}
				SppriceSale["sppriceSaleList["+index+"].Supply.sp_code"]=$(this).val();
				SppriceSale["sppriceSaleList["+index+"].deliver.code"]=$("#firm_select").val();
				SppriceSale["sppriceSaleList["+index+"].price"]=price;//单价
				SppriceSale["sppriceSaleList["+index+"].bdat"]=bdat;//开始时间
				SppriceSale["sppriceSaleList["+index+"].edat"]=edat;//结束时间
				/* SppriceSale["sppriceSaleList["+index+"].priceold"]=price;//上期价格 */
				var positnList=$("#mainFrame").contents().find('.table-body :checkbox');
				positnList.filter(':checked').each(function(indexs){
					SppriceSale["sppriceSaleList["+index+"].positnList["+indexs+"].code"]=$(this).val();
					size=++indexs;
				});
			});
			if(NotNull){
				if(size>0){
					$.ajax({
						url : '<%=path%>/sppriceSaleQuick/saveSppriceSaleQuick.do',
						type:'POST',
						async:false,
						data:SppriceSale,
						success : function(date) {
							if(date=="success"){
								showMessage({
		  	  						type: 'success',
		  	  						msg: '<fmt:message key="successful_added"/>！',
		  	  						speed: 2500
		  	  					});
								$("#saveQuick").prop("disabled", true);
								$('#againSave').prop("disabled",false);
							}else if(date == "error"){
								showMessage({
		  	  						type: 'error',
		  	  						msg: '<fmt:message key="fail_added"/>！',
		  	  						speed: 2500
		  	  					});
							}else{
								alert(date);
								$("#saveQuick").prop("disabled", true);
								$('#againSave').prop("disabled",false);
							}
						}
					});
				}else{
					showMessage({
							type: 'error',
							msg: '<fmt:message key="Donot_forget_to_choose_the_material_and_the_store" />..！',
							speed: 2500
						});
				};
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Unit_price_and_time_can_not_be_empty" />..！',
					speed: 2500
				});
			};
		};
	});
	
	//解除灰色，继续添加
	function againSave(){
		$("#saveQuick").prop("disabled",false);
		$('#againSave').prop("disabled",true);
	}

	</script>
</body>
</html>