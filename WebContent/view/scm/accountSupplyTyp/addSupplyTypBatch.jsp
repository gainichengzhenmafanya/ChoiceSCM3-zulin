<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Insert title here</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tr-select{
					background-color: #D2E9FF;
				}
				.separator ,div , .pgSearchInfo{
					display: none;
				}
				div[class]{
					display:block;
				}
				.tr-select{
					background-color: #D2E9FF;
				}
				.grid td span{
					padding:0px;
				}
				.page{
					margin-bottom: 25px;
				}
			</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="supplyBatchForm" method="post" action="<%=path%>/accountSupplyTyp/updateAccountSupplyTyp">
			<input type="hidden" id="accountId" name="accountId" class="text" readonly="readonly" value="${accountId}"/>
			<input type="hidden" id="defaultCode" class="text" readonly="readonly" value="${defaultCode}"/>
			<input type="hidden" id="typcode" class="text" value="${defaultCode}"/>
			<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 40px;">&nbsp;</span></td>
								<td><span style="width:35px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:100px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="result" items="${typList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 40px;text-align:center;">${status.index+1}</span></td>
									<td><span style="width:35px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${result.code}" value="${result.code}"/>
										</span>
									</td>
									<td><span title="${result.code}" style="width:100px;">${result.code}&nbsp;</span></td>
									<td><span title="${result.des}" style="width:180px;">${result.des}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>  
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(function(){
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="determine_materials_selection"/>',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								addSupplyTyp();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
				
				var str=$('#defaultCode').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++){ 
					$('#chk_'+strArry[i]).attr('checked','checked');
				};
				
			});
			
			//确定保存
			function addSupplyTyp(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var code = [];
				if(checkboxList){//不用非得选择
					var aim = checkboxList.filter(':checked'); 
					aim.each(function(){
						code.push($(this).val());
					});
					var url = "<%=path%>/accountSupplyTyp/updateAccountSupplyTyp.do?accountId="+$('#accountId').val()+"&typcode="+code;
					$.ajax({
						url:url,
						type:"post",
						success:function(msg){
							if(msg == '0'){
								alert('<fmt:message key ="successful_added" />！');
								parent.$('.close').click();
							}else{
								alert('<fmt:message key ="fail_added" />！');
							}
						}
					});
				}
			}
		</script>
</body>
</html>