<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>产品出品车间设置</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>							
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supplyDefaultDeliver/table.do" method="post">
		    <input type="hidden" id="type" name="type" value="${type}"/>
		    <input type="hidden" id="level" name="level" value="${level}"/>
		    <input type="hidden" id="init" name="init" value="${init}"/>
		    <input type="hidden" id="code" name="code"  value="${code}"/>
		    <input type="hidden" id="mod" name="mod"  value="${mode}"/>
		    <input type="hidden" id="delcode" name="delcode" />
	    	<input type="hidden" id="deldes" name="deldes" />
		    <input type="hidden" id="checkboxList" name="checkboxList" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:20px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:80px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
								<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:150px;"><fmt:message key="production_workshop"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="resultMap" varStatus="step" items="${supplyList}">
								<tr>
									<td><span  class="num" style="width:20px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${resultMap['SP_CODE']}" value="${resultMap['SP_CODE']}"/>
									</td>
									<td><span title="${resultMap['SP_CODE']}" style="width:80px;text-align: left;">${resultMap['SP_CODE']}&nbsp;</span></td>
									<td><span title="${resultMap['SP_NAME']}" style="width:180px;text-align: left;">${resultMap['SP_NAME']}&nbsp;</span></td>
									<td><span title="${resultMap['SP_DESC']}" style="width:90px;text-align: left;">${resultMap['SP_DESC']}&nbsp;</span></td>
									<td><span title="${resultMap['UNIT']}" style="width:50px;text-align: left;">${resultMap['UNIT']}&nbsp;</span></td>
									<td><span title="${resultMap['DES']}" style="width:150px;text-align: left;">${resultMap['DES']}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name="orderBy" id="orderBy" value="<c:out value="${orderBy}" default="sp_code"/>" />
			<%-- <input type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" /> --%>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
							<td><input type="text" id="sp_init" name="sp_init" class="text" onkeyup="ajaxSearch('sp_init')" style="text-transform:uppercase;" value="${supply.sp_init}" /></td>
							<td class="c-left"><fmt:message key="supplies_no" />：</td>
							<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${supply.sp_code}" /></td>
							<td class="c-left"><fmt:message key="supplies_name" />：</td>
							<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${supply.sp_name}" /></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			var t;
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
/* 				//排序start
				var array = new Array();      
				array = ['SP_CODE','SP_NAME','SP_DESC', 'UNIT','DES'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束 */
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								 $('.search-div').slideToggle(100);
								 var t = $('#sp_init').val();
								 $('#sp_init').focus().val(t);
							}
						},{
							text: '<fmt:message key="update"/><fmt:message key="production_workshop"/>',
							title: '<fmt:message key="update"/><fmt:message key="production_workshop"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectPositn();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							}
						}]
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 

				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					var mod = window.parent.$("#mod").val();
					var init = window.parent.$("#init").val();
					var type = window.parent.$("#type").val();
					var level = window.parent.$("#level").val();
					var code = window.parent.$("#code").val();
					$('.search-div').hide();
					$("#listForm").action="<%=path%>/supplyDefaultDeliver/table.do?mod="+mod+"&init="+init+"&type="+type+"&level="+level+"&code="+code;
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});
			
			function selectPositn() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 0){
					alert('<fmt:message key="please_select_one_material" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var mod = window.parent.$("#mod").val();
				path="<%=path%>/supplyDefaultDeliver/selectPositnByPage.do?sp_code="+chkValue.join(',')+"&mod="+mod+"&type=POSITNEX";
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key="update" /><fmt:message key="positions_information" />',
					content: '<iframe id="supplyPositnFrame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyPositnFrame')&&window.document.getElementById("supplyPositnFrame").contentWindow.updPositn()){
										//submitFrameForm('supplyPositnFrame','tableForm');
										window.document.getElementById("supplyPositnFrame").contentWindow.$("#tableForm").submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			function pageReload(){
				$('#listForm').attr('action','<%=path%>/supplyDefaultDeliver/table.do');
				$('#listForm').submit();
			}
		</script>
	</body>
</html>