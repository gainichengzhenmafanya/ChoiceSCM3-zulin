<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--菜品预估实际对比</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:55px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>
	</head>
	<body> 	
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<form id="listForm" action="<%=path%>/forecast/costCmpList.do" method="post">	
		<input type="hidden" id="msg" name="msg" value="${msg}"/>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key="Date_selection" />:</div>
				<div class="form-input" style="width:300px;">
					<font style="color:blue;"><fmt:message key ="startdate" />:</font>
					<input type="text" style="width:90px;" id="startdate" name="startdate" value="<fmt:formatDate value="${startdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'enddate\')}'});"/>
					<font style="color:blue;"><fmt:message key ="enddate" />:</font>
					<input type="text" style="width:90px;" id="enddate" name="enddate" value="<fmt:formatDate value="${enddate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'startdate\')}'});"/>
				</div>
				<div class="form-label"><fmt:message key ="branche" />:</div>
				<div class="form-input">
					<input type="text"  id="firm" name="firmName" readonly="readonly" value="${firmName}"/>
					<input type="hidden" id="firm_select" name="firmId" value="${firmId}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
			</div>	
		   		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:22px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:120px;"><fmt:message key ="branche" /></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key ="coding" /></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key ="name" /></span></td>
								<td rowspan="2"><span style="width:25px;"><fmt:message key ="unit" /></span></td>
								
								<td colspan="5"><span><fmt:message key ="shift1" /></span></td>
 								<td colspan="5"><span><fmt:message key ="shift2" /></span></td>
 								<td colspan="5"><span><fmt:message key ="shift3" /></span></td>
 								<td colspan="5"><span><fmt:message key ="shift4" /></span></td>
 								<td colspan="5"><span><fmt:message key ="total" /></span></td>
							</tr>
							<tr>
								<td><span style="width:60px;"><fmt:message key ="estimates" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="adjustment" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="actual" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_ygcyl" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_tzcyl" /></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key ="estimates" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="adjustment" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="actual" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_ygcyl" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_tzcyl" /></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key ="estimates" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="adjustment" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="actual" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_ygcyl" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_tzcyl" /></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key ="estimates" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="adjustment" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="actual" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_ygcyl" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_tzcyl" /></span></td>
 								
 								<td><span style="width:60px;"><fmt:message key ="estimates" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="adjustment" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="actual" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_ygcyl" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="scm_tzcyl" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="posItemPlan" items="${posItemPlanList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:22px;">${status.index+1}</span></td>
									<td><span title="${posItemPlan.firmNm}" style="width:120px;">${posItemPlan.firmNm}</span></td>
									<td><span title="${posItemPlan.itcode}" style="width:50px;">${posItemPlan.itcode}</span></td>
									<td><span title="${posItemPlan.itdes }" style="width:80px;">${posItemPlan.itdes }</span></td>
									<td><span title="${posItemPlan.itunit }" style="width:25px;">${posItemPlan.itunit }</span></td>
									
									<td><span title="${posItemPlan.cnt1old }" style="width:60px;text-align: right;">${posItemPlan.cnt1old }</span></td>
									<td><span title="${posItemPlan.cnt1 }" style="width:60px;text-align: right;">${posItemPlan.cnt1 }</span></td>
									<td><span title="${posItemPlan.cnt1act }" style="width:60px;text-align: right;">${posItemPlan.cnt1act }</span></td>
									<!-- 修改所有差异率只显示两位小数  wjf -->
									<td><span title="${posItemPlan.lv1 }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv1 }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${posItemPlan.lv1old }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv1old }" type="currency" pattern="0.00"/></span></td>

									<td><span title="${posItemPlan.cnt2old }" style="width:60px;text-align: right;">${posItemPlan.cnt2old }</span></td>
									<td><span title="${posItemPlan.cnt2 }" style="width:60px;text-align: right;">${posItemPlan.cnt2 }</span></td>
									<td><span title="${posItemPlan.cnt2act }" style="width:60px;text-align: right;">${posItemPlan.cnt2act }</span></td>
									<td><span title="${posItemPlan.lv2 }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv2 }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${posItemPlan.lv2old }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv2old }" type="currency" pattern="0.00"/></span></td>
									
									<td><span title="${posItemPlan.cnt3old }" style="width:60px;text-align: right;">${posItemPlan.cnt3old }</span></td>
									<td><span title="${posItemPlan.cnt3 }" style="width:60px;text-align: right;">${posItemPlan.cnt3 }</span></td>
									<td><span title="${posItemPlan.cnt3act }" style="width:60px;text-align: right;">${posItemPlan.cnt3act }</span></td>
									<td><span title="${posItemPlan.lv3 }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv3 }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${posItemPlan.lv3old }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv3old }" type="currency" pattern="0.00"/></span></td>
									
									<td><span title="${posItemPlan.cnt4old }" style="width:60px;text-align: right;">${posItemPlan.cnt4old }</span></td>
									<td><span title="${posItemPlan.cnt4 }" style="width:60px;text-align: right;">${posItemPlan.cnt4 }</span></td>
									<td><span title="${posItemPlan.cnt4act }" style="width:60px;text-align: right;">${posItemPlan.cnt4act }</span></td>
									<td><span title="${posItemPlan.lv4 }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv4 }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${posItemPlan.lv4old }" style="width:60px;text-align: right;"><fmt:formatNumber value="${posItemPlan.lv4old }" type="currency" pattern="0.00"/></span></td>
									
									<td><span title="${posItemPlan.totalold }" style="width:60px;text-align: right;">${posItemPlan.totalold }</span></td>
									<td><span title="${posItemPlan.total }" style="width:60px;text-align: right;">${posItemPlan.total }</span></td>
									<td><span title="${posItemPlan.totalact }" style="width:60px;text-align: right;">${posItemPlan.totalact }</span></td>
									<td><span title="${posItemPlan.totallv }" style="width:60px;text-align: right;">${posItemPlan.totallv }</span></td>
									<td><span title="${posItemPlan.totallvold }" style="width:60px;text-align: right;">${posItemPlan.totallvold }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			focus() ;//页面获得焦点
			if ("a"!=$("#msg").val()) {
				alert($("#msg").val());
			}
			if($('.grid').find('.table-body').find('.num').size()>0){
				loadToolBar([true,true,true]);
			}else {
				loadToolBar([true,false,false]);
			}
		 	$(document).bind('keydown',function(e){//按钮快捷键
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});
		 	//排序结束
			$('#bdate').bind('click',function(){
			new WdatePicker();
			});
			$('#edate').bind('click',function(){
				new WdatePicker();
			});

			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if ($("#firm").val()==''||$("#firm_select").val()=='') {
								alert("<fmt:message key='please_select_branche'/>!");
								return;
							}
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/forecast/printCostCmp.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/costCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/forecast/exportCostCmp.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/forecast/costCmpList.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
		
			
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();	
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#firm_select").val();
					var defaultName = $("#firm").val();
					var offset = getOffset('firm');
					top.cust('<fmt:message key="please_select_branche"/>',encodeURI('<%=path%>/positn/selectPositnFirm.do?mold=one&mis=y&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firm'),$('#firm_select'),'760','520','isNull');
				}
			});
		});	
		</script>
	</body>
</html>