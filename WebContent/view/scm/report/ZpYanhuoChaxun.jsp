<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
				<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
					<input type="text"  id="deliver_name"  name="deliver_name" readonly="readonly" />
					<input type="hidden" id="delivercode" name="delivercode" />
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label"><fmt:message key="category"/></div>
					<div class="form-input">
						<input type="hidden" id="typ" name="typ" />
						<input type="text"  id="typdes" name="typdes" readonly="readonly" class="text" />
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
					</div>
<%-- 					<div class="form-label"><fmt:message key="bigClass"/></div> --%>
<!-- 					<div class="form-input"> -->
<%-- 						<select  name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select> --%>
<!-- 					</div> -->
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" /></div>
					<div class="form-label"><fmt:message key ="branche" /></div>
					<div class="form-input">
					<input type="text"  id="positn_name1"  name="positn_name1" readonly="readonly" value=""/>
					<input type="hidden" id="firm" name="firm" value=""/>
					<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="coding"/></div>
					<div class="form-input">
						<input type="text" name="sp_code" id="sp_code" autocomplete="off" class="text"/>
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
					<%-- <select  name="firm" url="<%=path %>/positn/findAllPositnIn.do"  class="select"></select> --%>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"></div>
					<div class="form-input"><input type="checkbox" name="showtyp" value="1"/><fmt:message key="According_to_the"/><fmt:message key="arrival_date1"/><fmt:message key="select"/></div>
					<div class="form-label"></div>
					<div class="form-input" style="margin-left:20px;"><input type="checkbox" name="dif" value="true"/><fmt:message key="inspection_data_is_not_the_same"/></div>
				</div>
<%-- 					<div class="form-label"><fmt:message key="middleClass"/></div> --%>
<%-- 					<div class="form-input"><select  name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div> --%>
<%-- 					<div class="form-label"><fmt:message key="smallClass"/></div> --%>
<%-- 					<div class="form-input"><select  name="typ" url="<%=path %>/grpTyp/findAllTyp.do"  class="select"></select></div> --%>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var bdat = $("#queryForm").find("#bdat").val().toString();
							var edat = $("#queryForm").find("#edat").val().toString();
							if(bdat<=edat){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
							}else{
								alert('<fmt:message key ="Start_time_not_later_than_the_end_of_time" />');
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/ZpYanhuoChaxun/exportDireInspectionQuery.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/ZpYanhuoChaxun/printDireInspectionQuery.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
/*   	 	$("select").each(function(){
  	 			$(this).htmlUtils("select",[
  	 			                        	{key:'<fmt:message key="normal_storage"/>',value:'<fmt:message key="normal_storage"/>'},
	 			  	 			            {key:'<fmt:message key="overage_storage"/>',value:'<fmt:message key="overage_storage"/>'},
	 			  	 			            {key:'<fmt:message key="gifts_warehousing"/>',value:'<fmt:message key="gifts_warehousing"/>'},
	 			  	 			            {key:'<fmt:message key="recycling_storage"/>',value:'<fmt:message key="recycling_storage"/>'},
	 			  	 			            {key:'<fmt:message key="consignment_warehousing"/>',value:'<fmt:message key="consignment_warehousing"/>'},
	 			  	 			           	{key:'<fmt:message key="purchase_storage"/>',value:'<fmt:message key="purchase_storage"/>'},
	 			  	 			        	{key:'<fmt:message key="normal_shipments"/>',value:'<fmt:message key="normal_shipments"/>'},
	 			  	 			     		{key:'<fmt:message key="gifts_shipments"/>',value:'<fmt:message key="gifts_shipments"/>'},
	 			  	 			  			{key:'<fmt:message key="consignment_shipments"/>',value:'<fmt:message key="consignment_shipments"/>'},
	 			  	 						{key:'<fmt:message key="returns"/>',value:'<fmt:message key="returns"/>'},
	 			  	 						{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
	 			  	 			         ]);
  	 		}); */
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = [''];
  	 		//数字列
  	 		var numCols = ['amount','amountin','amountsto','amountcy','pricein','totalamt'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/ZpYanhuoChaxun/findDireInspectionQueryHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 	//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="Dire_inspection"/><fmt:message key="select"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				 var rs = eval("("+data+")");
		 				var modifyRows = [];
		 				var rows = rs.rows;
		 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
		 				for(var i in rows){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
		 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '') : (value ?  value:'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"rows["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyRows.push(curRow);
		 				}
		 				rs.rows = modifyRows;
		 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/ZpYanhuoChaxun/findDireInspectionQuery.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		$("#seachPositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'positn_name1',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var defaultName = $("#deliver_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 		$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typ').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
				}
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/ZpYanhuoChaxun/toColChooseDireInspectionQuery.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
