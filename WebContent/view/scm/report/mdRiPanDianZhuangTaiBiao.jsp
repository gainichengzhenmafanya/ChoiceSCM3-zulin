<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.choice.tele.domain.Condition" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>月度客流综合分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<style type="text/css">
		.select{
			margin-top: 2px;
		}	
	</style>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key ="startdate" /></div>
			<div class="form-input">
<%-- 				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /> --%>
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'})"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="enddate" /></div>
			<div class="form-input">
<%-- 				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/> --%>
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'})"/>
			</div>
		</div>
	</form>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//选择分店
			$("#searchFirm").click(function(){
				selectFirm({
  	 				basePath:'<%=path%>'
   	 			});
			});
  	 		//默认时间
  	 		$("#bdat,#edat").htmlUtils("setDate","yes");
			$("#bdat,#edat").click(function(){
	  			new WdatePicker();
	  		});
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			verifyFun:function(){
  	 				if(($.getDateFromStr($("#edat").val()).getTime() - $.getDateFromStr($("#bdat").val()).getTime())/(1000*60*60*24)>30){
  	 					alert('<fmt:message key="Choose_time_is_too_long_may_cause_the_browser_to_run_slowly" />！');
  	 					return false;
  	 				}
					return true;
  	 			},
  	 			gridId:'datagrid',
  	 			exportTyp:true,
  	 			excelUrl:'',
  	 			toolbar:['search','exit'],
  	 			searchFun:function(grid,form){
  	 				getParam(form);
  	 				creatGrid();
  	 			}
  	 		});
  	 		creatGrid();
  	 	});
		function creatGrid(){
	 		//生成表格
	  		builtTable({
	 			headUrl:"<%=path%>/mdRiPanDianZhuangTaiBiao/findFreetimeHeader.do",
	 			dataUrl:"<%=path%>/mdRiPanDianZhuangTaiBiao/listMdRiPanDianZhuangTaiBiao.do",
	 			title:'<fmt:message key ="scm_mdrpdztb" />',
	 			id:'datagrid',
	 			pagination:false,
	 			createHeader:function(data,head,frozenHead){
  	 				var colFirst = [];
  	 				var colFrozen = [];
  	 				colFrozen.push({field:'FIRMCODE',width:100,title:'branches_encoding'});
  	 				colFrozen.push({field:'FIRMDES',width:150,title:'branches_name'});
  	 				frozenHead.push(colFrozen);
  	 				for(var i in data){
  	 					colFirst.push({field:data[i],title:data[i],width:100,align:'right',styler:function(value){
  	 						if(value == "1")
  	 							return "color:red;background:red";
  	 					}});
  	 				}
  	 				head.push(colFirst);
  	 			}
	 		});
		}
		//方法延时执行
		var delay = function(t,func){
			var self = this;
			if(self.curTime)
				clearTimeout(self.curTime);
			self.curTime = setTimeout(function(){
				func.apply(self);
				},t*1000);
		};
		//生成工具栏
		function builtToolBar(params){
			var form = $('#'+params.formId);//页面formid
			var grid = params.gridId ? $('#'+params.gridId) : params.grid;//表格所在div
			var basePath = params.basePath;
			var curtoolbar = params.toolbar;//需要的工具按钮,可能的值search,excel,print,option,exit
			var searchFun = params.searchFun; //自定义查询方法
			var verifyFun = params.verifyFun;
			var exportTyp = params.exportTyp;//excel导出时获取表头的方式，默认为从数据库查询。设置为true时从页面获取
			var items = [];
			if(grid)grid.data("verifyFun",verifyFun);
			var toolbar = {search:{
				text: $.messager.defaults.search,
				title: $.messager.defaults.search,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					delay(0.5,function(){
						if(!(verifyFun ? verifyFun() : true))return;
						searchFun ? searchFun(grid,form) :
						grid.datagrid("load",getParam(form));
					});
				}
			},
			excel:{
				text: $.messager.defaults.excel,
				title: $.messager.defaults.excel,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-40px','-20px']
				},
				handler: function(){
					if(!(verifyFun ? verifyFun() : true))return;
					var headers = [];
					if(exportTyp){
						var panel = grid.datagrid('getPanel');
						var content = panel.panel('body');
						function clearHead(head){
							head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
							head.find('td').each(function(){
								if($(this).css('display') == 'none'){
									$(this).remove();
								}else{
									$(this).removeAttr('class');
									$(this).children('div').html($.trim($(this).text()));
									$(this).children('div').removeAttr('class');
								}
							});
							return head.html();
						}
						headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));
						headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));
						headers.push("<fieldMap>"+$.toJSON(fieldMap)+"</fieldMap>");
					}
					headers = headers.join("");
					var rs = headers.match(/\w+\s*=\w+/g);
					for(var s in rs){
						var string = String(rs[s]);
						string.match(/(\w+)$/g);
						headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));
					}
					var head = $("<input type='hidden' name='headers'/>");
					form.find("input[name='headers']").remove();
					head.val(headers.replace(/\r\n/g,""));
					head.appendTo(form);
					form.attr('action',params.excelUrl);
					form.submit();
				}
			},
			print:{
				text: $.messager.defaults.print,
				title: $.messager.defaults.print,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-140px','-100px']
				},
				handler: function(){
					if(!(verifyFun ? verifyFun() : true))return;
					form.attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					var action=params.printUrl;
					form.attr('action',action);
					form.submit();
				}
			},
			option:{
				text: $.messager.defaults.option,
				title: $.messager.defaults.option,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-100px','-60px']
				},
				handler: function(){
					toColsChoose(params.colsChooseUrl);
				}
			},
			exit:{
				text: $.messager.defaults.exit,
				title: $.messager.defaults.exit,
				useable:true,
				icon: {
					url: basePath+'/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
				}
			}
			};
			
			for(var i in curtoolbar){
				if(typeof(curtoolbar[i]) == 'string')
					items.push(toolbar[curtoolbar[i]]);
				else
					items.push(curtoolbar[i]);
			}
			$('#'+params.toolbarId).html('');
			$('#'+params.toolbarId).toolbar({
				items:items
			});
			bodyHeight = $(".layout-panel-center",top.document).children('div[region="center"]').height() - $(".tab-control",top.document).height();
			tableHeight = bodyHeight - $("#tool").height() - $("#queryForm").height() - $(".tabs-header").height();
			$('body').height(bodyHeight);
		}
		//解析获取表单数据
		function getParam(form){
			form = form.find("*[name]").filter(function(){
				return $.inArray($(this).attr('type') ? $(this).attr('type').toLowerCase() : undefined ,['button','submit','reset','image','file']) < 0 && $(this).val() 
				&& !$(this).attr('disabled');
			});
			var mul = ['radio','checkbox'];
			var temp = {};
			var param = {};
			form.each(function(){
				this.tagName.toLowerCase() == 'input' ? (temp[$(this).attr('name')] = $(this).attr('type') ? $(this).attr('type') : 'text') : temp[$(this).attr('name')] = this.tagName.toLowerCase();
			});
			for(var i in temp){
				$.inArray(temp[i],mul) < 0 ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() ? param[i] = form.filter(temp[i]+'[name="'+i+'"]').val() : form.filter('input[name="'+i+'"]').val()
						: param[i] = form.filter('input[name="'+i+'"]:checked').val();
			}
			queryParams = param;
			return param;
		}
		
  	 </script>
  </body>
</html>
