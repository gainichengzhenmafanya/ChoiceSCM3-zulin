<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
		.page{
			margin-bottom: 25px;
		}
		.datagrid-sort-icon{
			background:none; 
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<form id="listForm" action="<%=path %>/GysLeibieHuizong/toDeliverCategorySum2.do" name="listForm" method="post">
					<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
					<div class="form-label"><fmt:message key="positions"/></div>
<!-- 					<div class="form-input"> -->
<%-- 						<input type="hidden" name="positndes" value="${supplyAcct.positndes}"/> --%>
<%-- 						<select id="positn" name="positn" url="<%=path %>/positn/findAllPositnOut.do" code="${supplyAcct.positn}" des="${supplyAcct.positndes}"  class="select"></select> --%>
<!-- 					</div> -->
					<div class="form-input">
<%-- 						<select id="positn" name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
						<input type="text"  id="positn_name"  name="positndes" readonly="readonly" value="${supplyAcct.positndes}"/>
						<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn}"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<input type="hidden" name="grptypdes" value="${supplyAcct.grptypdes}"/>
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" code="${supplyAcct.grptyp}" des="${supplyAcct.grptypdes}" class="select"></select>
					</div>
 					<div class="form-input">
 						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="isTax" id="isTaxid" <c:if test="${!empty supplyAcct.isTax}">checked</c:if> /><fmt:message key="Tax_in" />
 						&nbsp;&nbsp;&nbsp;
 						<input type="checkbox" id="showtyp" name="showtyp" <c:if test="${supplyAcct.showtyp=='1'}">checked</c:if> value="1"/><fmt:message key="Contain_transfer"/>
 					</div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
					<div class="form-input">
					<input type="text"  id="deliver_name"  name="deliverdes" readonly="readonly" value="${supplyAcct.deliverdes }"/>
					<input type="hidden" id="delivercode" name="delivercode" value="${supplyAcct.delivercode }"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
					<%-- <select  name="delivercode" url="<%=path %>/deliver/findAllDeliver.do"   class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input">
						<input type="hidden" name="grpdes" value="${supplyAcct.grpdes}"/>
						<select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do" code="${supplyAcct.grp}" des="${supplyAcct.grpdes}" class="select"></select></div>
					<div class="form-label"><fmt:message key="document_types"/></div>
					<div class="form-input">
						<select id="chktyp" name="chktyp" url="<%=path %>/codeDes/findAllBillType.do?codetyp=RK,ZF" class="select"></select>
					</div>
				</div>
			
 			<table id="test" style="width:'100%';" title="供应商类别报表"  rownumbers="true" remoteSort="true" idField="SP_CODE">
				<thead>
					<tr>
						<th rowspan="2" field="ck" checkbox="true"></th>
						<th rowspan="2" field="DELIVERCODE" width="50px"><span id="DELIVERCODE"><fmt:message key ="coding" /></span></th>
						<th rowspan="2" field="DELIVERDES" width="100px"><span id="DELIVERDES"><fmt:message key ="suppliers" /></span></th>
						<c:forEach var="grptyp" items="${columns}">
							<th colspan="${fn:length(grptyp.grpList)+1 }" style="text-align:right;"><span>${grptyp.des}</span></th>
						</c:forEach>
						<th rowspan="2" field="ZJ" width="100px" style="text-align:right;"><span id="ZJ"><fmt:message key ="total" /></span></th>
					</tr>
					<tr>
						<c:forEach var="grptyp" items="${columns}">
							<c:forEach var="grp" items="${grptyp.grpList}">
								<th field="F_${grp.code}" width="50px;" style="text-align:right;"><span id="_${grp.code}"/>${grp.des}</th>
							</c:forEach>
							<th field="XJ_${grptyp.code}" width="50px;" style="text-align:right;"><span id="XJ_${grptyp.code}"/>小计</th>
						</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  		setElementHeight('#test',['.tool'],$(document.body),126);
		$('#test').datagrid({});
		var obj={};
		var rows = ${ListBean};
		obj['rows']=rows;
		$('#test').datagrid('loadData',obj);
		$('.datagrid-body').find('tr').find('td').find('.datagrid-cell').css('text-align','right');
		$('.datagrid-body').find('tr').find('td:eq(1)').find('.datagrid-cell').css('text-align','left');
		$('.datagrid-body').find('tr').find('td:eq(2)').find('.datagrid-cell').css('text-align','left');
  	 	$(document).ready(function(){

  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
// 							var positndes = $("#positn").next('span').find('input:first').val();
// 							$('input[name=positndes]').val(positndes);
							var grptypdes = $("#grptyp").next('span').find('input:first').val();
							$('input[name=grptypdes]').val(grptypdes);
							var grpdes = $("#grp").next('span').find('input:first').val();
							$('input[name=grpdes]').val(grpdes);
							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action',"<%=path%>/GysLeibieHuizong/exportDeliverCategorySum2.do");
							$('#listForm').submit();
							$("#wait2 span").html("loading...");
							$('#listForm').attr('action',"<%=path %>/GysLeibieHuizong/toDeliverCategorySum2.do");
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		//var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$(".select").each(function(){
  	 			$(this).htmlUtils("select",[
/*   	 			                        {key:'<fmt:message key="normal_storage"/>',value:'<fmt:message key="normal_storage"/>'},
	 			  	 			            {key:'<fmt:message key="overage_storage"/>',value:'<fmt:message key="overage_storage"/>'},
	 			  	 			            {key:'<fmt:message key="gifts_warehousing"/>',value:'<fmt:message key="gifts_warehousing"/>'},
	 			  	 			            {key:'<fmt:message key="recycling_storage"/>',value:'<fmt:message key="recycling_storage"/>'},
	 			  	 			            {key:'<fmt:message key="consignment_warehousing"/>',value:'<fmt:message key="consignment_warehousing"/>'},
	 			  	 			           	{key:'<fmt:message key="purchase_storage"/>',value:'<fmt:message key="purchase_storage"/>'},
	 			  	 			        	{key:'<fmt:message key="normal_shipments"/>',value:'<fmt:message key="normal_shipments"/>'},
	 			  	 			     		{key:'<fmt:message key="gifts_shipments"/>',value:'<fmt:message key="gifts_shipments"/>'},
	 			  	 			  			{key:'<fmt:message key="consignment_shipments"/>',value:'<fmt:message key="consignment_shipments"/>'},
	 			  	 						{key:'<fmt:message key="returns"/>',value:'<fmt:message key="returns"/>'},
	 			  	 						{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'}, */
	 			  	 			         ]);
  	 		});
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var defaultName = $("#deliver_name").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliver_name'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 		
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 </script>
  </body>
</html>
