<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_acceptance"/>--配送中心到货验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">		
			.page{margin-bottom: 25px;}
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:35px;
			}
		</style>
		<script type="text/javascript">
			var path="<%=path%>";
		</script>					
	</head>
	<body>
		<div class="tool"></div>
		<%--当前登录用户 --%>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<form id="listForm" action="<%=path%>/shipAndReceive/list.do?mold=select" method="post">
		<!-- <input type="hidden" id="mold" name="mold"/> -->	
		<input type="hidden" id="sta" name="sta"/>		
			<div class="form-line">	
				<div class="form-label" <c:if test='${type!=null}'>style="display:none;"</c:if>><fmt:message key="branches_name"/></div>
				<div class="form-input" <c:if test='${type!=null}'>style="display:none;"</c:if>>
					<input type="text"  id="positndes"  name="positndes" readonly="readonly" value="${dis.positndes}"/>
					<input type="hidden" id="positn" name="positn" value="${dis.positn}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
				</div>
				<div class="form-label" <c:if test='${type!=null}'>style="display:none;"</c:if> style="padding-left: 60px;">出库仓位</div>
				<div class="form-input" <c:if test='${type!=null}'>style="display:none;"</c:if>>
					<input type="text"  id="firmdes"  name="firmdes" readonly="readonly" value="${dis.firmdes}"/>
					<input type="hidden" id="firm" name="firm" value="${dis.firm}"/>
					<img id="seachFirm" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
				</div>
				<div class="form-label"><fmt:message key="arrival_date"/></div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:90px;" id="bdate" name="bdate" value="<fmt:formatDate value="${bdate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:90px;" id="edate" name="edate" value="<fmt:formatDate value="${edate}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/>
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="whether_the_present" /></div>
				<div class="form-input" style="width:190px;">
					<input type="radio" id="alliszs" name="iszs" value="" 
						<c:if test="${dis.iszs==''}"> checked="checked"</c:if> /><fmt:message key="all"/>
					<input type="radio" id="" name="iszs" value="N"
						<c:if test="${dis.iszs=='N'}"> checked="checked"</c:if> /><fmt:message key="Non_gift" />
					<input type="radio" id="" name="iszs" value="Y" 
						<c:if test="${dis.iszs=='Y'}"> checked="checked"</c:if> /><fmt:message key ="giving" />
				</div>
				<div class="form-label"></div>
				<div class="form-input" style="width:170px;">
					<input type="radio" id="all" name="check" value="0" 
						<c:if test="${check=='0'}"> checked="checked"</c:if> /><fmt:message key="all"/>
					<input type="radio" id="chk" name="check" value="1"
						<c:if test="${check=='1'}"> checked="checked"</c:if> /><fmt:message key ="Have_the_inspection" />
					<input type="radio" id="unchk" name="check" value="2" 
						<c:if test="${check=='2'}"> checked="checked"</c:if> /><fmt:message key ="no_inspection" />
				</div>
				<div class="form-label"></div>
				<div class="form-input" style="width:170px;">
					<input type="radio" id="alliscy" name="iscy" value="0" 
						<c:if test="${dis.iscy=='0'}"> checked="checked"</c:if> /><fmt:message key="all"/>
					<input type="radio" id="iscy1" name="iscy" value="1"
						<c:if test="${dis.iscy=='1'}"> checked="checked"</c:if> /><fmt:message key="Difference" />
					<input type="radio" id="iscy2" name="iscy" value="2" 
						<c:if test="${dis.iscy=='2'}"> checked="checked"</c:if> /><fmt:message key="No_difference" />
                         <!-- 	iscy是否有差异字段，  减少再加入一个字段。   2014.11.7  xlh -->
				</div>
			</div>	
		   	<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:15px;">&nbsp;</span></td>
								<td rowspan="2" style="width:20px;"><input type="checkbox" id="chkAll"/></td>
                                <td rowspan="2"><span style="width:50px;"><fmt:message key="orders_num"/></span></td>
                                <td rowspan="2"><span style="width:80px;"><fmt:message key ="branches_name" /></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="coding"/></span></td>
                                <td rowspan="2"><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key ="library_positions" /></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" title="<fmt:message key="standard_unit"/>"><span style="width:60px;"><fmt:message key="standard_unit"/></span></td>
								<td rowspan="2" title="<fmt:message key="reference_unit"/>"><span style="width:60px;"><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="unit_price"/></span></td>
								<td colspan="2" rowspan="2"><span style="width:71px;"><fmt:message key="distribution"/><fmt:message key ="quantity" /></span></td>
								<td colspan="2" rowspan="2"><span style="width:71px;"><fmt:message key="inspection"/><fmt:message key ="quantity" /></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td colspan="3" rowspan="3"><span style="width:60px;"><fmt:message key ="Difference_quantity" /></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key ="whether_the_present" /></span></td>
							</tr>
							<tr>
<%--  								<td><span style="width:35px;"><fmt:message key="quantity"/></span></td> --%>
<%--  								<td><span style="width:35px;"><fmt:message key="quantity"/>2</span></td> --%>
<%--  								<td><span style="width:35px;"><fmt:message key="quantity"/></span></td> --%>
<%--  								<td><span style="width:35px;"><fmt:message key="quantity"/>2</span></td> --%>
<%--  								<td><span style="width:60px;"><fmt:message key="inspection_ratio"/></span></td> --%>
<%--  								<td><span style="width:50px;"><fmt:message key="branch_loss"/></span></td> --%>
<%--  								<td><span style="width:50px;"><fmt:message key="center_loss"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>	
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:15px;">${status.index+1}</span></td>
									<td style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></td>
									<td><span title="${dis.chkno}" style="width:50px;">${dis.chkno}</span></td>
                                    <td><span title="${dis.firm }" style="width:80px;">${dis.firm }</span></td>
									<td><span title="${dis.sp_code }" style="width:80px;"><fmt:formatDate value="${dis.dat }" pattern="yyyy-MM-dd"/></span></td>
									<td><span title="${dis.sp_code }" style="width:80px;">${dis.sp_code }</span></td>
                                    <td><span title="${dis.sp_name }" style="width:80px;">${dis.sp_name }</span></td>
									<td><span title="${dis.positn }" style="width:80px;">${dis.positn }</span></td>
									<td><span title="${dis.spdesc }" style="width:80px;">${dis.spdesc }</span></td>
									<td><span title="${dis.unit }" style="width:60px;">${dis.unit }</span></td>
									<td><span title="${dis.unit1 }" style="width:60px;">${dis.unit1 }</span></td>
									<td>
                                        <c:choose>
                                            <c:when test="${dis.priceout>0}">
                                                <span title="${dis.priceout }" style="width:60px;text-align: right;">
                                                    <fmt:formatNumber value="${dis.priceout }" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber>
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span title="${dis.pricesale }" style="width:60px;text-align: right;">
                                                    <fmt:formatNumber value="${dis.pricesale }" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber>
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
									<td colspan="2"><span title="${dis.cntout}" style="width:71px;text-align: right;"><fmt:formatNumber value="${dis.cntout }" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td class="textInput" style="display: none"><span title="" style="width:35px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:35px;text-align: right;padding: 0px;" /></span></td>
									<td class="textInput"  style="display: none"><span title="${dis.cntfirm}" style="width:91px;padding: 0px;text-align: right;">${dis.cntfirm}</span></td>
									<td>
                                        <span title="${dis.cntfirm}" style="width:81px;padding: 0px;text-align: right;"><fmt:formatNumber value="${dis.cntfirm}" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber></span>
                                    </td>
									<td>
                                        <span title="" style="width:60px;text-align: right;">
                                            <fmt:formatNumber value="${dis.amount}" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber>
                                        </span>
                                    </td>
									<td  style="display: none"><span title="${dis.supply.accprate}" style="width:60px;"><fmt:formatNumber value="${dis.supply.accprate}" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td><span title="${dis.cntfirm-dis.cntout}" style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.cntfirm-dis.cntout}" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td  style="display: none"><span title="${dis.cntlogistks}" style="width:50px;text-align: right;"><fmt:formatNumber value="${dis.cntlogistks}" pattern="0.00" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td><span title="${dis.des}" style="width:40px;">${dis.des}</span></td>
									<td><span title="${dis.iszs}" style="width:60px;">${dis.iszs}</span></td>
<%-- 									<td><input type="hidden" id="checby" name="checby" value="${dis.chkyh }"/>	</td> --%>
<%-- 									<td style="width:40px;"><input type="hidden" id="unitper" name="unitper" value="${dis.supply.unitper}"/>	</td> --%>
								</tr>
							</c:forEach>
						</tbody>
<!-- 						<tfoot> -->
<!-- 							<tr> -->
<!-- 								<td></td> -->
<!-- 								<td></td> -->
<%-- 								<td style="width:60px;"><fmt:message key="total"/></td> --%>
<!-- 								<td colspan="9"></td> -->
<!-- 								<td class="textInput"><span title="" style="width:40px;"><input type="text" onfocus="this.blur()"  readonly="readonly" style="width:40px;text-align: right;padding: 0px;" /></span></td> -->
<!-- 								<td colspan="4"></td> -->
<!-- 							</tr>							 -->
<!-- 						</tfoot> -->
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){	
			// 只有筛选未审的时候才可以编辑数量、数量2，以及进行审核操作。
			if($('input:radio[name="check"]:checked').val()=='0'){
				loadToolBar([true,false,true,false,true]);
			}else if($('input:radio[name="check"]:checked').val()=='1'){
				loadToolBar([true,false,true,false,true]);
			}else if($('input:radio[name="check"]:checked').val()=='2'){
				loadToolBar([true,true,true,true,true]);
			}else{
				$('#all').attr("checked",true);
				$('#alliszs').attr("checked",true);
				$('#iscy1').attr("checked",true);
				loadToolBar([true,true,true,true,true]);
			};
			//页面获得焦点
			focus() ;
			//按钮快捷键
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(window.event && window.event.keyCode == 118) { 
			 		window.event.keyCode = 505; 
		 		} 
		 		if(window.event && window.event.keyCode == 505){ 
		 			window.event.returnValue=false; 
		 		}; 
		 		if(e.altKey ==false){
		 			return;
		 		}
		 		switch (e.keyCode) {
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 67: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
					case 80: $('#autoId-button-106').click(); break;
	            }
			});

			// 筛选条件为未审时，数量和数量2变为可编辑状态。
			if($('input:radio[name="check"]:checked').val()=='2') {
				$('.grid input').removeAttr("readonly");
			}
			var totalAmount=0;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.each(function(){
				// 自动计算金额
				var price=$(this).parents('tr').find('td:eq(8)').find('span').attr('title');
				var amount=$(this).parents('tr').find('td:eq(11)').find('input').val();
				$(this).parents('tr').find('td:eq(13)').find('span').find('input').val(Math.round((price*amount)*10)/10);
				// 根据第二单位对第一单位的转化率自动计算报货数量2
				var value=$(this).parents('tr').find('td:eq(9)').find('span').attr('title');
				var unitper=$(this).parents('tr').find('td:eq(19)').find('input').val();
				$(this).parents('tr').find('td:eq(10)').find('span').find('input').val(Math.round((unitper*value)*10)/10);
				totalAmount+=price*amount;
			});
			 if(0==totalAmount) {
				$('tfoot').hide();
			} 
			$('tfoot').find('tr').find('td:eq(4)').find('span').find('input').val(Math.round((totalAmount)*10)/10);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				});
		    
		    
		    $("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positndes',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		$("#seachFirm").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'firmdes',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
		    
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();			
		});	
		
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
// 						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},
// 					{
// 						text: '<fmt:message key="save" />(<u>S</u>)',
// 						title: '<fmt:message key="save"/>',
// 						useable:use[1],
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-80px','-0px']
// 						},
// 						handler: function(){
// 							updateDis();
// 						}
// 					},
// 					{
// 						text: '<fmt:message key="print" />(<u>P</u>)',
// 						title: '<fmt:message key="print"/>',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[2],
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							$("#wait2").val('NO');//不用等待加载
// 							$('#sta').val("print");
// 							$('#listForm').attr('target','report');
// 							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
<%-- 							var action="<%=path%>/firmMis/printDispatch.do"; --%>
// 							$('#listForm').attr('action',action);
// 							$('#listForm').submit();
// 							$('#listForm').attr('target','');
<%-- 							$('#listForm').attr('action','<%=path%>/firmMis/dispatch.do'); --%>
// 							$("#wait2").val('');//等待加载还原
// 						}
// 					},
// 					{
// 						text: '<fmt:message key="check" />(<u>C</u>)',
// 						title: '<fmt:message key="check"/>',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[3],
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-58px','-240px']
// 						},
// 						handler: function(){
// 							check();
// 						}
// 					},
					{
						text: 'Excel',
						title: 'Excel',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/shipAndReceive/exportDispatch.do');
							$('#listForm').submit();
							$("#wait2").val('');//等待加载还原
							$("#wait2 span").html("loading...");
							$('#listForm').attr('action','<%=path%>/shipAndReceive/list.do?mold=select');
							$("#wait2").val('');//等待加载还原
						}
// 					},{
// 						text: '验货数量等于配送数量',
// 						title: '验货数量等于配送数量',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-160px','-20px']
// 						},
// 						handler: function(){
// 							peiSongToyanHuo();
// 						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
		}
		//配送数量等于验货数量
		function peiSongToyanHuo(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					$(this).parents('tr').find('td:eq(11)').find('span').find('input').val($(this).parents('tr').find('td:eq(9)').find('span').html());
					$(this).parents('tr').find('td:eq(12)').find('span').find('input').val($(this).parents('tr').find('td:eq(10)').find('span').find('input').val());
					var price=$(this).parents('tr').find('td:eq(8)').find('span').attr('title');
					var amount=$(this).parents('tr').find('td:eq(11)').find('input').val();
					$(this).parents('tr').find('td:eq(13)').find('span').find('input').val(Math.round(parseFloat(price*amount)*10)/10);
				});
			}else{
				alert('<fmt:message key ="Select_the_data_you_want_to_modify" />！');
			}
		}
		// 点击保存以后保存验货数量和验货差异
		function updateDis() {
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue" />!')){
					checkboxList.filter(':checked').each(function(i){
						var cntfirm=$(this).parents('tr').find('td:eq(11)').find('span').find('input').val();
						selected['supplyAcctList['+i+'].id'] =  $(this).parents('tr').find('td:eq(1)').find('input').val();
						selected['supplyAcctList['+i+'].cntfirm'] =  cntfirm;
						selected['supplyAcctList['+i+'].cntfirm1'] = $(this).parents('tr').find('td:eq(12)').find('span').find('input').val();
						selected['supplyAcctList['+i+'].cntfirmks'] =  $(this).parents('tr').find('td:eq(15)').find('span').attr('title');
						selected['supplyAcctList['+i+'].cntlogistks'] = $(this).parents('tr').find('td:eq(16)').find('span').attr('title');
						if(Number(cntfirm)==0){
							count++;
						}
					});
					if(count>0){
						if(confirm('<fmt:message key="The_quantity_of_goods_is_" />'+count+'<fmt:message key="_0_data_whether_to_continue" />？')){
							savePost(selected);
							}
						}else{
							savePost(selected);
						}
					}
				}else{
					alert('<fmt:message key="please_select_options_you_need_save" />！');
					return ;
				}
		}
		function savePost(selected){
			$.post('<%=path%>/firmMis/updateAcct.do',selected,function(data){
				showMessage({//弹出提示信息
					type: 'success',
					msg: '<fmt:message key="operation_successful" />！',
					speed: 1000
					});	
				});
		}
		//审核数据
		function check(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
					if(Number($(this).parents('tr').find('td:eq(11)').find('span').find('input').val())==0){
						count++;
					}
				});
				if(count>0){
					if(confirm('<fmt:message key="The_quantity_of_goods_is_" />'+count+'<fmt:message key="_0_data_whether_to_continue" />？')){
						checkSave(chkValue);
					}
				}else{
					checkSave(chkValue);
				}
				
			}else{
				alert('<fmt:message key="please_select_check_information"/>！！');
				return ;
			}
		}
		function checkSave(chkValue){
			var action = '<%=path%>/firmMis/check.do?ids='+chkValue.join(",");
			$('body').window({
				title: '<fmt:message key="audit_arrival_information"/>',
				content: '<iframe frameborder="0" src='+action+'></iframe>',
				width: 500,
				height: '245px',
				draggable: true,
				isModal: true
			});
		}
		//修改验货数量时，验货数量2、金额自动计算
		function update(e,f){				
			if(($(e).attr("readonly")=="readonly")==true){
				return;
			}else if(Number(e.value)<0){
				alert('<fmt:message key="number_cannot_be_negative"/>');
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			var value=e.value;
			var unitper=$(e).closest('tr').find('td:eq(19)').find('input').val();
			$(e).closest('tr').find('td:eq(12)').find('span').find('input').val(Math.round(parseFloat(unitper*value)*10)/10);

			
			var price=$(e).closest('tr').find('td:eq(8)').find('span').attr('title');
			var amount=$(e).closest('tr').find('td:eq(11)').find('input').val();
			$(e).closest('tr').find('td:eq(13)').find('span').find('input').val(Math.round(parseFloat(price*amount)*10)/10);
		}
		//修改验货数量2时，验货数量、金额自动计算
		function update1(e,f){
			if(($(e).attr("readonly")=="readonly")==true){
				return;
			}else if(Number(e.value)<0){
				alert('<fmt:message key="number_cannot_be_negative"/>');
				e.value=e.defaultValue;
				e.focus();
				return;
			}else if(isNaN(e.value)){
				alert('<fmt:message key="number_be_not_valid_number"/>！');
				e.value=e.defaultValue;
				e.focus();
				return;
			};
			var value=e.value;
			var unitper=$(e).closest('tr').find('td:eq(19)').find('input').val();
			$(e).closest('tr').find('td:eq(11)').find('span').find('input').val(Math.round(parseFloat(value/unitper)*10)/10);
			
			var price=$(e).closest('tr').find('td:eq(8)').find('span').attr('title');
			var amount=$(e).closest('tr').find('td:eq(11)').find('input').val();
			$(e).closest('tr').find('td:eq(13)').find('span').find('input').val(Math.round(parseFloat(price*amount)*10)/10);
		}
		
		//可编辑表格的样式，获得焦点时候	
		function onEdit(obj){
			if(($(obj).attr("readonly")=="readonly")==true){
				return;
			}else{
				obj.select();
				$(obj).removeClass("input");
				$(obj).addClass("onEdit");
			}
		}
		//可编辑表格的样式，焦点离开时候
		function outEdit(obj){
			if(($(obj).attr("readonly")=="readonly")==true){
				return;
			}else{
				$(obj).removeClass("onEdit");
				$(obj).addClass("input");
			}
		}
		function pageReload(){
			$('#listForm').submit();
		}
		</script>
	</body>
</html>