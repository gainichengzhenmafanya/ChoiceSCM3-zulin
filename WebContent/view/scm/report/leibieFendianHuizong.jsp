<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 6.5%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
					<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
					<input type="hidden" id="positn" name="positn" value=""/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
						<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
					</div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="requisitioned_positions"/></div>
					<div class="form-input">
					<input type="text"  id="positn_name1"  name="positn_name1" readonly="readonly" value=""/>
					<input type="hidden" id="firm" name="firm" value=""/>
					<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='查询仓位' />
					<%-- <select  name="firm" url="<%=path %>/positn/findAllPositnIn.do"  class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input"><select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select></div>
				</div>
				<div class="form-line"  style="z-index:8;">
					<div class="form-label" style="margin-left:15px;text-align: center;width: 275px;">
						<input type="radio" checked="checked" name="grpdes" value="grptyp"/><fmt:message key="a"/>
						<input type="radio" name="grpdes" value="grp"/><fmt:message key="b"/>
						<input type="radio" name="grpdes" value="typ"/><fmt:message key="c"/>
					</div>
					<div class="form-input" style="width: 231.5px;"></div>
					<div class="form-label" style="margin-left:15px;"><fmt:message key="document_types"/></div>
					<div class="form-input"><select id="chktyp" name="chktyp" url="<%=path %>/codeDes/findAllBillType.do?codetyp=CK,ZF" class="select"></select></div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  		
  	 	$(document).ready(function(){

  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var bdat = $("#queryForm").find("#bdat").val().toString();
							var edat = $("#queryForm").find("#edat").val().toString();
							if(bdat<=edat){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
							}else{
								alert('<fmt:message key="starttime"/><fmt:message key="not_less_than"/><fmt:message key="endtime"/>');
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/LeibieFendianHuizong/exportCategoryPositnSum.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$('#queryForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/LeibieFendianHuizong/printCategoryPositnSum.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[
  	 										{key:'<fmt:message key="normal_library"/>',value:'<fmt:message key="normal_library"/>'},
  	 										{key:'<fmt:message key="allocate_library"/>',value:'<fmt:message key="allocate_library"/>'},
  	 										{key:'<fmt:message key="shortage_library"/>',value:'<fmt:message key="shortage_library"/>'},
  	 										{key:'<fmt:message key="giveaways_out_library"/>',value:'<fmt:message key="giveaways_out_library"/>'},
  	 										{key:'<fmt:message key="consignment_out_library"/>',value:'<fmt:message key="consignment_out_library"/>'},
  	 										{key:'<fmt:message key="scrapped"/>',value:'<fmt:message key="scrapped"/>'},
  	 										{key:'<fmt:message key="refunding"/>',value:'<fmt:message key="refunding"/>'},
  	 										{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
		  	 			         ]);
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['amt'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/LeibieFendianHuizong/findCategoryPositnSumHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//收集表单参数
  	 		var form = $("#queryForm").find("*[name]");
			form = form.filter(function(index){
				var cur = form[index];
				if($(cur).attr("name")){
					if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
						if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
							if($("input[name='"+$(cur).attr("name")+"']:checked").length){
								params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
							}
						}else{
							params[$(cur).attr("name")] = $(cur).val();
						}
					}
				}
				
			});
						  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="class_stores_summary"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				//当前列grp
	 				var currentCol = '';
	 				//当前grp小计
	 				var currentTotal = 0;
	 				for(var i in rows){
	 					if(rows[i].GRP != currentCol){
	 						if(Number(i)){
	 							modifyRows.push({
	 								GRP:'<fmt:message key="subtotal"/>',
	 								AMT:currentTotal.toFixed(2)
	 							});
	 							currentTotal = 0;
	 						}
	 						rows[i].AMT = rows[i].AMT.toFixed(2);
	 						modifyRows.push(rows[i]);
	 						currentTotal += modifyRows[i].AMT ? Number(rows[i].AMT) : 0;
	 						currentCol = rows[i].GRP;
	 					}else{
	 						rows[i].GRP = '';
	 						rows[i].GRPDES = '';
	 						rows[i].AMT = rows[i].AMT.toFixed(2);
	 						currentTotal += rows[i].AMT ? Number(rows[i].AMT) : 0;
	 						modifyRows.push(rows[i]);
	 					}
	 					if(i == rows.length-1)modifyRows.push({
	 						GRP:'<fmt:message key="subtotal"/>',
							AMT:currentTotal.toFixed(2)
	 					});
	 				}
	 				rs.rows = modifyRows;
	 				rs.footer[0][columns[0].field] = '<fmt:message key="total"/>';
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/LeibieFendianHuizong/findCategoryPositnSum.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
				onDblClickRow:function(index,data){
					if(data['GRP'] == "小计") return false;
  	 				var grp = data['GRP'];
  	 				grp = getData("datagrid",index,grp);
  	 				var grpdes = data['GRPDES'];
  	 				grpdes = getData2("datagrid",index,grpdes);
					var grpname = typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
					var params = {"firmcode":data['DELIVER'],"firmdes":data['DELIVERDES'],"grptyp":grp,"grptypdes":grpdes,"bdat":$("#bdat").val(),"edat":$("#edat").val(),
						"positn":$("#positn").val(),"positndes":$("#positn_name").val(),
						"grp":$("#grp").data("checkedVal"),"grpdes":grpname,
						"chktyp":chktypname};
					openTag("chkoutdetailquery",'<fmt:message key ="details_of_the_library_inquiry" />',"<%=path%>/CkMingxiChaxun/toChkoutDetailQuery.do",params);
  	 			}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$(".panel-tool").remove();
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		$("#seachPositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'positn_name1',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/LeibieFendianHuizong/toColChooseCategoryPositnSum.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  		//一列多行相同时除第一个外剩余行都隐藏，选择时获取第一个行此列不为空的值
		function getData(id,index,dat){
				var start = index;
				//如果双击的行key列为空，向上找直到找到第一个key列不为空的行
				while(index>=0 && !dat){
					$("#"+id).datagrid("selectRow",index);
					dat = $("#"+id).datagrid("getSelected")['GRP'];
 	 				index--;
				}
 	  			//返回到被双击的行
	  			$("#"+id).datagrid("selectRow",start);
 	  		return dat;
		}
		//一列多行相同时除第一个外剩余行都隐藏，选择时获取第一个行此列不为空的值
		function getData2(id,index,dat){
				var start = index;
				//如果双击的行key列为空，向上找直到找到第一个key列不为空的行
				while(index>=0 && !dat){
					$("#"+id).datagrid("selectRow",index);
					dat = $("#"+id).datagrid("getSelected")['GRPDES'];
 	 				index--;
				}
 	  			//返回到被双击的行
	  			$("#"+id).datagrid("selectRow",start);
 	  		return dat;
		}
  	 </script>
  </body>
</html>
