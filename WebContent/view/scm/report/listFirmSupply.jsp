<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>物料盘存表</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		.textInput span{
			padding:0px;
		}
		.textInput input{
			border:0px;
			background: #F1F1F1;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/bohfirmsupply/list.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="positions"/></div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${firmsupply1.firmDes}"/>
					<input type="hidden" id="firm" name="firm" value="${firmsupply1.firm}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>	
				<div class="form-label"><fmt:message key="supplies_code"/></div>
				<div class="form-input">
					<input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${firmSupply1.sp_code}"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label"><fmt:message key="supplies_category" /></div>
				<div class="form-input">
					<input type="text" id="typdes" name="typdes" class="selectDepartment text" readonly="readonly" value="${firmsupply1.typdes}"/>
					<input type="hidden" id="sp_type" name="sp_type" class="text"  value="${firmsupply1.sp_type}"/>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:100px;"><fmt:message key ="supplies_code" /></span></td>
								<td><span style="width:100px;"><fmt:message key ="supplies_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="bigClass" /> </span></td>
								<td><span style="width:80px;"><fmt:message key ="middleClass" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="smallClass" /> </span></td>
								<td><span style="width:80px;"><fmt:message key ="standard_unit" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="supplies_specifications" /></span></td>
								<td><span style="width:70px;"><fmt:message key ="scm_standard_quantity" /> </span></td>
								<td><span style="width:70px;"><fmt:message key ="scm_the_reference_number" /></span></td>								
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="supply" items="${firmSupplyList}" varStatus="status">
							<tr>
								<td align="center"><span style="width:25px;">${status.index+1}</span></td>
								<td><span style="width:100px;">${supply.sp_code }</span></td>
								<td><span style="width:100px;">${supply.spname }</span></td>
								<td><span style="width:80px;">${supply.grptypdes }</span></td>
								<td><span style="width:80px;">${supply.grpdes }</span></td>
								<td><span style="width:80px;">${supply.typdes }</span></td>
								<td><span style="width:80px;">${supply.unit }</span></td>
								<td><span style="width:80px;">${supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;">${supply.cnt }</span></td>
								<td><span style="width:70px;text-align: right;">${supply.cnt1 }</span></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>	
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 70: $('#autoId-button-102').click(); break;//查询
	            }
			}); 
			
		 	//取上一级物资类别
// 			var sp_type=$('#sp_type', parent.parent.document).val();
// 			var code=$('#code', parent.parent.document).val();
// 			if(code!=""){
// 				$("#sp_type").val(code);
// 				$("#typdes").val(sp_type);
// 			}
			/*弹出树*/
			$('#typdes').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('typdes');
					top.cust('<fmt:message key="please_select_category" />',encodeURI('<%=path%>/supply/selectGrptyp.do'),offset,$('#typdes'),$('#sp_type'),null,null,null,setSpcode);
				}
			});
			
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firm').val();
					var defaultName = $('#firmDes').val();
					var offset = getOffset('firmDes');
// 					alert(offset);
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firm'),'760','520','isNull');
				}
			});
			
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();;
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    //自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
 		
		//控制按钮显示
		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$('#listForm').submit();
					}
				},{
					text: 'Excel',
					title: 'Excel',
					//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
				    icon: {
					  		url: '<%=path%>/image/Button/op_owner.gif',
					  		position: ['-40px','-20px']
				      		},
				    handler:function(){
				    	$("#wait2").val('NO');//不用等待加载
						$('#listForm').attr('action','<%=path%>/bohfirmsupply/exportFirmSupply.do');
						$('#listForm').submit();
						$("#wait2 span").html("loading..");
						$('#listForm').attr('action','<%=path%>/bohfirmsupply/list.do');
						$("#wait2").val('');//等待加载还原
				    }
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
					}
				}
			]
		});
		
		function setSpcode(codeid){
//			 $('#sp_code').val(codeid+".");//五芳斋项目就用这一句代码
			if(codeid){
				  $.ajax({
					  type:"post",
					  async:false,
					  url:"<%=path%>/supply/getMaxSpcode.do",
					  dataType:"text",
					  data:{code:codeid},
					  success:function(data){
						  $('#sp_code').val(data);
					  },
					  error:function(htmlObject,status,index){
						  alert('<fmt:message key="Make_the_most_of_the_time_when_the_material_encoding" />.'+index);
					  }
				  });
			}
		}
		function pageReload(){
			$('#listForm').submit();
		}
		</script>			
	</body>
</html>