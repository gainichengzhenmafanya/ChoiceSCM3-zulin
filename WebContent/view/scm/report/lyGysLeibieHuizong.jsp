<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>领用供应商类别汇总</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>					
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/LyGysLeibieHuizong/findLyGysLeibieHuizong.do" id="listForm" name="listForm" method="post">
			<div class="form-line"  style="z-index:10;">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.bdat}" pattern="yyyy-MM-dd"/>" /></div>
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input">
				<input type="text"  id="positndes"  name="positndes" readonly="readonly" value="${supplyAcct.positndes}"/>
				<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn}"/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
			</div>
			<div class="form-label"><fmt:message key="requisitioned_positions"/></div>
			<div class="form-input">
				<input type="text"  id="acceptPositnName"  name="acceptPositnName" readonly="readonly" value="${supplyAcct.acceptPositnName}"/>
				<input type="hidden" id="acceptPositn" name="acceptPositn" value="${supplyAcct.acceptPositn}"/>
				<img id="seachPositn2" style="margin-right:2px;" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>'  />
			</div>
			<div class="form-label"><fmt:message key="bigClass"/></div>
			<div class="form-input">
				<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" class="select"></select>
				<input type="hidden" id="grptypdes" name="grptypdes" value="${supplyAcct.grptypdes }"/>
			</div>
		</div>
		<div class="form-line"  style="z-index:9;">
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.edat}" pattern="yyyy-MM-dd"/>"/></div>
			<div class="form-label"><fmt:message key="suppliers"/></div>
			<div class="form-input">
			<input type="text"  id="deliverdes"  name="deliverdes" readonly="readonly" value="${supplyAcct.deliverdes}"/>
			<input type="hidden" id="delivercode" name="delivercode" value="${supplyAcct.delivercode}"/>
			<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' />
			</div>
			<div class="form-label"><fmt:message key="document_types"/></div>
			<div class="form-input">
			<select id="chktyp" name="chktyp" url="<%=path %>/codeDes/findAllBillType.do?codetyp=RK,ZF" class="select"></select>
			<input type="hidden" id="chktypdes" name="chktypdes" value="${supplyAcct.chktypdes }"/>
			</div>
			<div class="form-label"><fmt:message key="middleClass"/></div>
			<div class="form-input">
				<select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do" class="select"></select>
				<input type="hidden" id="grpdes" name="grpdes" value="${supplyAcct.grpdes }"/>
			</div>
		</div>
		<table id="test" style="width:'100%';" title="<fmt:message key="accept"/><fmt:message key="supplier_category_summary"/>"  rownumbers="true" remoteSort="true">
			<thead>
				<tr>
				<th field="c1" width="100px"><fmt:message key="gyszmbm"/></th>
				<th field="c2" width="100px"><fmt:message key="gyszmmcs"/></th>
				<th align="right" field="c3" width="100px"><fmt:message key="total"/></th>
				<c:forEach items="${delivers}" var="deliverItem" varStatus="in">
					<th align="right" field="${in.index}" width="100px">${deliverItem.des}</th>
				</c:forEach>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="dataItem" items="${list}">
					<tr>
						<td>${dataItem.TYP}</td>
						<td>${dataItem.TYPDES}</td>
						<td>${dataItem.AMTIN}</td>
						<c:forEach items="${delivers}" var="deliverItem">
							<td width="100px">${dataItem[deliverItem.alias]}</td>
						</c:forEach>
					</tr>
				</c:forEach>
				<c:forEach var="footItem" items="${foot}">
					<tr>
						<td><fmt:message key="total"/></td>
						<td></td>
						<td>${footItem.AMTIN}</td>
						<c:forEach items="${delivers}" var="deliverItem">
							<td width="100px">${footItem[deliverItem.alias]}</td>
						</c:forEach>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form>
	<input type="hidden" id="hidgrptyp" value="${supplyAcct.grptyp }"></input>
	<input type="hidden" id="hidgrp" value="${supplyAcct.grp }"></input>
	<input type="hidden" id="hidchktyp" value="${supplyAcct.chktyp }"></input>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[]);
  	 		});
  	 		
			//为了查询之后select继续能附上值  wjf
			var hidgrptyp = $("#hidgrptyp").val();
			//hidgrptyp = hidgrptyp.substring(1,hidgrptyp.length-1);
			$("#grptyp").next('span').find('input').first().val($('#grptypdes').val());
			$("#grptyp").next('span').find('input').last().val(hidgrptyp);
			
			
			var hidgrp = $("#hidgrp").val();
			//hidgrp = hidgrp.substring(1,hidgrp.length-1);
			$("#grp").next('span').find('input').first().val($('#grpdes').val());
			$("#grp").next('span').find('input').last().val(hidgrp);
			
			
			var hidchktyp = $("#hidchktyp").val();
			//hidgrp = hidgrp.substring(1,hidgrp.length-1);
			$("#chktyp").next('span').find('input').first().val($('#chktypdes').val());
			$("#chktyp").next('span').find('input').last().val(hidchktyp);
			
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
// 							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
// 								alert('日期不能为空！');
// 								return;
// 							}
							var grptypdes = $("#grptyp").next('span').find('input').first().val();
							$("#grptypdes").val(grptypdes);
							
							var grpdes = $("#grp").next('span').find('input').first().val();
							$("#grpdes").val(grpdes);
							
							var chktypdes = $("#chktyp").next('span').find('input').first().val();
							$("#chktypdes").val(chktypdes);

							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
 							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							
							var grptypdes = $("#grptyp").next('span').find('input').first().val();
							$("#grptypdes").val(grptypdes);
							
							var grpdes = $("#grp").next('span').find('input').first().val();
							$("#grpdes").val(grpdes);
							
							var chktypdes = $("#chktyp").next('span').find('input').first().val();
							$("#chktypdes").val(chktypdes);
							
							
							$("#wait2").val('NO');//不用等待加载
 							$('#listForm').attr('action','<%=path%>/LyGysLeibieHuizong/exportLyGysLeibieHuizong.do');
							$('#listForm').submit();
							$("#wait2 span").html("Loading...");
							$("#listForm").attr("action","<%=path%>/LyGysLeibieHuizong/findLyGysLeibieHuizong.do");
							$("#wait2").val('');//等待加载还原
						}
 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),125);
  	 		$('#test').datagrid({
  	 			selectOnCheck: true,
  	 		 	onClickRow: function (rowIndex, rowData) {
  	                $(this).datagrid('unselectRow', rowIndex);
  	            }
  	 		});

			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positndes',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	
		 	//领用仓位
		 	$("#seachPositn2").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#acceptPositn").val(),
					single:false,
					tagName:'acceptPositnName',
					tagId:'acceptPositn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
	 	
	 		$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#delivercode").val();
					var defaultName = $("#deliverdes").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliverdes'),$('#delivercode'),'900','500','isNull');
				}
			});
  	 	});
  	 </script>
  </body>
</html>
