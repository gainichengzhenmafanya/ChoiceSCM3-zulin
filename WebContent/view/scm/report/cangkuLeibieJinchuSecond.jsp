<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 10%;
		}
		form .form-line .form-input{
			width: 19%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width:85%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
  		<div class="form-line">
			<div class="form-label"><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>"/></div>
		
			<div class="form-label"><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
		
			<div class="form-input">
				&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkstoNo" value="chkstoNo"/><fmt:message key="store_jinhaoyong"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input">
			<input type="text"  id="positnNm"  name="positnNm" readonly="readonly" value="${supplyAcct.positnNm}"/>
			<input type="hidden" id="positn" name="positn" value="${supplyAcct.positn}"/>
			<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /><%-- 
			<select id="firm" name="firm" url="<%=path %>/positn/findAllPositnIn.do"  class="select"></select> --%>
			</div>
			<div class="form-label"><fmt:message key="bigClass"/></div>
			<div class="form-input">
				<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"  class="select"></select>
			</div>
		</div>
	</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/CangkuLeibieJinchuSecond/exportCangkuLeibieJinchuSecond.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		/* $("#firm").htmlUtils("select",[]); */
  	 		$("#grptyp").htmlUtils("select",[]);
  	 		//var m = new Date().getMonth();
  	 		//$("#month > option[value="+(m+1)+"]").attr("selected","selected");
  	 		
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		var columns_sec = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/CangkuLeibieJinchuSecond/findCangkuLeibieJinchuSecondHeaders.do?grptyp="+$("#grptyp").val(),
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data.columns;
  	 				}
  	 			});
  	 		//解析获取的数据
			for(var i in tableContent){
  	 			columns.push({title:tableContent[i].des,align:'center',colspan:4});
  	 			columns_sec.push({field:tableContent[i].code+"BAMOUNT",title:'<fmt:message key="beginning_of_period"/>',align:'right',width:80});
  	 			columns_sec.push({field:tableContent[i].code+"INAMOUNT",title:'<fmt:message key="led_into"/>',align:'right',width:80});
  	 			columns_sec.push({field:tableContent[i].code+"OUTAMOUNT",title:'<fmt:message key="library"/>',align:'right',width:80});
  	 			columns_sec.push({field:tableContent[i].code+"EAMOUNT",title:'<fmt:message key="balances"/>',align:'right',width:80});
  	 		}
			columns.push({title:'<fmt:message key="total"/>',align:'center',colspan:4});
 	 			columns_sec.push({field:"TOTALBAMOUNT",title:'<fmt:message key="beginning_of_period"/>',align:'right',width:80});
 	 			columns_sec.push({field:"TOTALINAMOUNT",title:'<fmt:message key="led_into"/>',align:'right',width:80});
 	 			columns_sec.push({field:"TOTALOUTAMOUNT",title:'<fmt:message key="library"/>',align:'right',width:80});
 	 			columns_sec.push({field:"TOTALEAMOUNT",title:'<fmt:message key="balances"/>',align:'right',width:80});
  	 		frozenColumns.push({field:'DEPT',title:'<fmt:message key="coding"/>',width:100,align:'left',rowspan:2});
  	 		frozenColumns.push({field:'POSITNDES',title:'<fmt:message key="name"/>',width:100,align:'left',rowspan:2});
			head.push(columns);
			head.push(columns_sec);
  	 		frozenHead.push(frozenColumns);
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="warehouse_category_out_of_the_table"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			loadFilter:function(data,type){
	 				var rows = data.rows;
	 				var footer = data.footer;
	 				var modifyRows = [];
	 				var modifyFooter = [];
	 				var foot = {};
	 				var curpositn = rows[0].DEPT;
	 				var curdes = rows[0].POSITNDES;
	 				var row = {};
	 				var totalb = 0;
	 				var totalin = 0;
	 				var totalout = 0;
	 				var totale = 0;
	 				var alltotalb = 0;
	 				var alltotalin = 0;
	 				var alltotalout = 0;
	 				var alltotale = 0;
	 				for(var i in rows){
	 					var cur = rows[i].DEPT;
	 					if(cur != curpositn || i > rows.length - 1){
	 						row['TOTALBAMOUNT'] = totalb.toFixed(2);
	 						row['TOTALINAMOUNT'] = totalin.toFixed(2);
	 						row['TOTALOUTAMOUNT'] = totalout.toFixed(2);
	 						row['TOTALEAMOUNT'] = totale.toFixed(2);
	 						alltotalb += totalb;
	 						alltotalin += totalin;
	 						alltotalout += totalout;
	 						alltotale += totale;
	 						row['DEPT'] = curpositn;
	 						row['POSITNDES'] = curdes;
	 						modifyRows.push(row);
	 						row = {};
	 						curpositn = cur;
	 						curdes = rows[i].POSITNDES;
	 						totalb = 0;
	 		 				totalin = 0;
	 		 				totalout = 0;
	 		 				totale = 0;
	 					}
	 					row[rows[i].GRP + 'BAMOUNT'] = Number(rows[i].BAMOUNT).toFixed(2);
 						row[rows[i].GRP + 'INAMOUNT'] = Number(rows[i].INAMOUNT).toFixed(2);
 						row[rows[i].GRP + 'OUTAMOUNT'] = Number(rows[i].OUTAMOUNT).toFixed(2);
 						row[rows[i].GRP + 'EAMOUNT'] = Number(rows[i].EAMOUNT).toFixed(2);
 						totalb += Number(rows[i].BAMOUNT);
 		 				totalin += Number(rows[i].INAMOUNT);
 		 				totalout += Number(rows[i].OUTAMOUNT);
 		 				totale += Number(rows[i].EAMOUNT);
	 				}
	 				row['TOTALBAMOUNT'] = totalb.toFixed(2);
					row['TOTALINAMOUNT'] = totalin.toFixed(2);
					row['TOTALOUTAMOUNT'] = totalout.toFixed(2);
					row['TOTALEAMOUNT'] = totale.toFixed(2);
					alltotalb += totalb;
					alltotalin += totalin;
					alltotalout += totalout;
					alltotale += totale;
					row['DEPT'] = curpositn;
					row['POSITNDES'] = curdes;
					modifyRows.push(row);
	 				data.rows = modifyRows;
	 				for(var i in footer){
	 					var cur = footer[i];	 					
	 					foot[cur.GRP + 'BAMOUNT'] = Number(cur.BAMOUNT).toFixed(2);
	 					foot[cur.GRP + 'INAMOUNT'] = Number(cur.INAMOUNT).toFixed(2);
	 					foot[cur.GRP + 'OUTAMOUNT'] = Number(cur.OUTAMOUNT).toFixed(2);
	 					foot[cur.GRP + 'EAMOUNT'] = Number(cur.EAMOUNT).toFixed(2);
	 				}
	 				foot['TOTALBAMOUNT'] = alltotalb.toFixed(2);
					foot['TOTALINAMOUNT'] = alltotalin.toFixed(2);
					foot['TOTALOUTAMOUNT'] = alltotalout.toFixed(2);
					foot['TOTALEAMOUNT'] = alltotale.toFixed(2);
	 				foot['DEPT'] = '<fmt:message key="total"/>';
	 				modifyFooter.push(foot);
	 				data.footer = modifyFooter;
	 				return data;
	 			},
				url:"<%=path%>/CangkuLeibieJinchuSecond/findCangkuLeibieJinchuSecond.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				pagination:false,
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
  	 		
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positnNm',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 </script>
  </body>
</html>
