<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css" />
<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
<style type="text/css">
.text {
	font-size: 12px, border:0px, line-height:20px, height:20px, padding:0px,*height:18px,*line-height:18px,
		_height:18px, _line-height:18px;
}

.search {
	margin-top: 3px;
	cursor: pointer;
}

form .form-line .form-label {
	width: 5%;
}

form .form-line .form-input {
	width: 13%;
}

form .form-line .form-input input[type=text] {
	width: 80%;
}

form .form-line .form-input select {
	width: 82%;
}
</style>
</head>
<body>
	<div class="tool"></div>
	<input id="firstLoad" type="hidden" />
	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line" style="z-index: 10;">
			<div class="form-label">
				<fmt:message key="positions" />
			</div>
			<div class="form-input">
				<input type="text" id="positn_name" name="positn_name" readonly="readonly" value="" /><!-- 仓位 -->				
				<input type="hidden" id="positn" name="positn" value="" />
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
			</div>
			<div class="form-label">
				<fmt:message key="Accounting_year" />
			</div>
			<div class="form-input">
				<select id="yearr" name="yearr" key="yearr" data="yearr" url="<%=path%>/mainInfo/findAllYears.do" class="select" single="true"
					code="" des=""></select><!-- 查询会计年 -->				
			</div>
			<div class="form-label">
				<fmt:message key="months" /><!-- 月份 -->				
			</div>
			<div class="form-input">
				<select id="month" name="month" class="select">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
				</select>
			</div>
		</div>
		<div class="form-line" style="z-index: 9;">
			<div class="form-label">
				<fmt:message key="requisitioned_positions" /><!-- 领用仓位 -->				
			</div>
			<div class="form-input">
				<input type="text" id="positn_name1" name="positn_name1" readonly="readonly" value="" />
				<input type="hidden" id="firm" name="firm" value="" />
				<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png"
					alt='<fmt:message key="query_position"/>' />
				<%-- <select  name="firm" url="<%=path %>/positn/findAllPositnIn.do"  class="select"></select> --%>
			</div>
			<div class="form-label">
				<fmt:message key="coding" /><!-- 编码 -->				
			</div>
			<div class="form-input" style="margin-top: -3px;">
				<input type="text" name="sp_code" id="sp_code" autocomplete="off" class="text" value="<c:out value="${chkoutm.chkoutno}" />" />
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
			</div>
			<div class="form-label">
				<fmt:message key="category" /><!-- 类别 -->				
			</div>
			<div class="form-input" style="margin-top: -3px; *margin-top: -2px;">
				<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}" />
				<input type="text" id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" />
				<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			</div>
		</div>
		<div class="form-line" style="z-index: 8;">
			<div class="form-label">
				<fmt:message key="document_types" /><!-- 单据类型-->				
			</div>
			<div class="form-input">
				<select id="chktyp" name="chktyp" url="<%=path%>/codeDes/findAllBillType.do?codetyp=CK" class="select"></select>
			</div>
			<div class="form-label"></div>

			<div class="form-label"></div>
			<div class="form-input">
				[
				<input type="radio" name="showtyp" value="0" checked="checked" />
				按领用仓位
				<input type="radio" name="showtyp" value="1" />
				按耗用仓位 ]
			</div>
		</div>

	</form>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
  	$('#yearr').attr('code',new Date().getFullYear());//会计年默认当前年
	$('#yearr').attr('des',new Date().getFullYear());
  	 	$(document).ready(function(){
  	 	//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',//查询
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){														
							 tableContent = {};
				  	 		 columns = [];
				  	 		 head = [];
				  	 		 frozenHead = [];
				  	 		 frozenColumns = [];
							 getHead();
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							getdatagrid();
						//	$("#datagrid").datagrid("load");							
						}
					},{
						text: 'Excel',//导出excel
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/CkMonthHuizong/exportChkoutMonthSum.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		var year=new Date().getFullYear(); 
  	 		var m = new Date().getMonth()+1;  	 		
	 		$("#month > option[value="+m+"]").attr("selected","selected");	 		
	 		$("select").not("#month").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});	 		
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['amtout','query1','query2','query3','query4','query5','query6','query7','query8','query9','query10','query11','query12','query13','query14','query15','query16','query17','query18','query19','query20','query21','query22','query23','query24','query25','query26','query27','query28','query29','query30','query31'];
  	 		//收集form表单数据的对象
  	 		var params = {};  
  	 		
  	 		<!--生成datagrid表头 -->
  	 		function getHead(){  	 			
  	 			var year=new Date().getFullYear();
  	 			var month=$("#month").val();  	 			
  	  	 		//ajax获取报表表头
  	  	 		$.ajax({url:"<%=path%>/CkMonthHuizong/findChkoutMonthSumHeaders.do",  	  	 		
  	  	 			   data:{"yearr":year,"month":month},
  	  	 			   type:"post",
  	  	 			   async:false,
  	  	 			   success:function(data){
  	  	 					tableContent = data;
  	  	 				}
  	  	 			});  	  	 		
  	  	 		//解析获取的数据
  	  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	  	 		var Cols = [];
  	  	 		var colsSecond = [];
  				var prev = '';
  				var temp;
  	  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);  	  	 		
  	  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	  	 		if(t && !t.length){
  	  	 			for(var i in tableContent.columns){
  	  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  		  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  		  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  		  	 			else
  		  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
  		  	 		}
  					head.push(columns);
  		  	 		frozenHead.push(frozenColumns);
  	  	 		}else{
  	  	 			for(var i in tableContent.columns){
  	  	 				
  	  	 			var align = "right"; 
  	  	 			if (tableContent.columns[i].properties.toLowerCase()=="firm" ||tableContent.columns[i].properties.toLowerCase()=="firmdes"){
  	  	 					align ="left";
	  	 				}
  	  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
  		  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	  	 				else{
  	  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	  	 					if(cur && cur.length){
  	  	 						var cur = tableContent.columns[i].zhColumnName;
  	  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	  	 							temp.colspan ++;
  	  	 						}else{
  	  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	  	 							columns.push(temp);
  	  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	  	 						}
  	  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	  	 					}else{
  	  	 						if(tableContent.columns[i].columnName)
  	  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	  	 					}
  	  	 				}
  	  	 			}
  	  	 			head.push(columns);
  	  	 			head.push(colsSecond);
  	  	 			frozenHead.push(frozenColumns);
  	  	 		}
  	 		}
  	 		
  	 		<!--生成datagrid内容 -->
  	 		function getdatagrid(){  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="library_month_summary"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var cols = tableContent.columns;	 					
						var curRow = {};
	 					for(var j in cols){
	 						try{
	 						//	alert(rows[0].cols[j].properties);
	 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase()) ;	 							
	 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');	 							
	 							curRow[cols[j].columnName.toUpperCase()] = value;
	 						}catch(e){
	 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}	 				
	 				
	 				var currentCol = '';
	 				//当前delivercode小计
	 				var currentTotal = 0;
	 				var m = 0;	
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/CkMonthHuizong/findChkoutMonthSum.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
  	 			onDblClickRow:function(index,data){//双击查看明细开始
  	 				var year=$("#yearr").data("checkedVal");    	 				
  	  	 			var month=$("#month").val();  	  	  	 			   
  	 				var begindate;var enddate;
  	 				$.ajax({url:"<%=path%>/CkMonthHuizong/queryDate.do",  	  	 		
  	 					data:{"yearr":year,"month":month},
   	  	 			   type:"post",
   	  	 				async:false,
   	  	 				success:function(data){
   	  	 					begindate=data.begindate;
   	  	 					enddate=data.enddate;	
   	  	 				}
   	  	 			});
  	 				
  	 				if(data['FIRMDES'] == "小计") return false;  	 				
  	 				var positn = $("#positn").val();
  	 				var positndes = $("#positn_name").val();
  	 				var firm = $("#firm").val();
  	 				var firmdes = $("#positn_name1").val();
 					if($('input[name="showtyp"]:checked').val() == 0){
 						firmdes = data['FIRMDES'];
 					}else{
 						positndes = data['FIRMDES'];
 					}
 					 					
  	 				var typ = typeof($("#typ").data("checkedName"))!="undefined"?$("#typ").data("checkedName"):"";
 	 				var grptypname = typeof($("#grptyp").data("checkedName"))!="undefined"?$("#grptyp").data("checkedName"):"";
  	 				var grpname = typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"";
  	 				var typname = typeof($("#typ").data("checkedName"))!="undefined"?$("#typ").data("checkedName"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
 	 				var parameters = {"bdat":begindate,"edat":enddate,
 	 						"firm":firm,"firmdes":firmdes,"sp_code":$("#sp_code").val(),
 	 						"positn":positn,"positndes":positndes,
  	 						"grptyp":$("#grptyp").data("checkedVal"),"grptypdes":grptypname,
  	 						"grp":$("#grp").data("checkedVal"),"grpdes":grpname,
  	 						"typ":typ,"typdes":typname,
  	 						"chktyp":$("#chktyp").data("checkedVal"),"chktypdes":chktypname};
   	 	  			openTag("ChkoutDetailQuery",'<fmt:message key ="details_of_the_library_inquiry" />',"<%=path%>/CkMingxiChaxun/toChkoutDetailQuery.do",parameters);
  	 			}//双击查看明细结束
  	 		});
  	 		}
  	 		getHead();
  	 		getdatagrid();
  	 		
  	 		$("#firstLoad").val("true");
  	  	 	$(".panel-tool").remove();

  	  	$('#seachSupply').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#sp_code').val();
				top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
			}
		});
	  	  $("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
	 		$("#seachPositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firm").val(),
					single:false,
					tagName:'positn_name1',
					tagId:'firm',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});

  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/CkMonthHuizong/toColChooseChkoutMonthSum.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
		//一列多行相同时除第一个外剩余行都隐藏，选择时获取第一个行此列不为空的值
		function getData(id,index,dat,key){
			var start = index;
			//如果双击的行key列为空，向上找直到找到第一个key列不为空的行
			while(index>=0 && !dat){
				$("#"+id).datagrid("selectRow",index);
				dat = $("#"+id).datagrid("getSelected")[key];
	 				index--;
			}
	  			//返回到被双击的行
  			$("#"+id).datagrid("selectRow",start);
 	  		return dat;
		}
		
  	 	//择类别的方法，返回类别两边不包含（）
	  	$('#seachTyp').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#typ').val();
				//var defaultName = $('#typDes').val();
				var offset = getOffset('seachTyp');
				top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?resulttyp=1&defaultCode='+ defaultCode), offset,$('#typdes'), $('#typ'), '320','460', 'isNull');
							}
						});
	</script>
</body>
</html>
