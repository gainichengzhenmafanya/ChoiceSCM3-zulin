<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width:auto;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
		
		fieldset{
			padding: 5px;
			margin: 10px 6px;
			border: 1px solid #868686;
		}
		
		legend{
			font-size: 13px;
			font-weight: bold;
			font-style: italic;
			cursor: pointer;
			border: 1px solid #868686;
			padding-left: 20px;
		}
  	</style>
  </head>	
  <body>
  <div style="overflow-x:auto;width: 100%">
  	<fieldset>
		<legend>【<fmt:message key="In_report"/>】</legend>
	  	<div style="width: 630px;">
	  		<a href="<%=path %>/RkMingxiChaxun/toChkinDetailS.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_query"/></a>
	  		<a href="<%=path %>/RkMingxiHuizong/toChkinDetailSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_detail_summary"/></a>
	  		<a href="<%=path %>/RkHuizongChaxun/toChkinSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_aggregate_query"/></a>
	  		<a href="<%=path %>/RkLeibieHuizong/toChkinCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_category_summary"/></a>
	  		<a href="<%=path %>/RkZongheChaxun/toChkinmSynQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="warehousing_comprehensive_inquiry"/></a>
	  		<a href="<%=path %>/YxqChaxun/toYxqDetailS.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="the_period_of_validity"/><fmt:message key="select"/></a><!-- 有效期查询 -->
	  	</div>
	  </fieldset>
	  <fieldset>
		<legend>【<fmt:message key="Out_report"/>】</legend>
	  	<div style="width: 760px;">
	  		<a href="<%=path %>/CkMingxiChaxun/toChkoutDetailQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="details_of_the_library_inquiry"/></a>
	  		<a href="<%=path %>/CkMingxiHuizong/toChkoutDetailSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_Detail_Summary"/></a>
	  		<a href="<%=path %>/CkHuizongChaxun/toChkoutSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="aggregate_query_library"/></a>
	  		<a href="<%=path %>/CkYingliChaxun/toChkoutProfitQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_profitability_query"/></a>
	  		<a href="<%=path %>/CkRiqiHuizong/toChkoutDateSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/></a>
	  		<a href="<%=path %>/CkRiqiHuizong/toChkoutDateSum1.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/>1</a>
	  		<a href="<%=path %>/CkRiqiHuizong/toChkoutDateSum2.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_date_summary"/>2</a>
	  		<a href="<%=path %>/CkMonthHuizong/toChkoutMonthSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="library_month_summary"/></a>
	  	</div>
	  	<div style="width: 890px;">
	  		<a href="<%=path %>/CkLeibieHuizong/toChkoutCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="out_library_category_summary"/></a>
	  		<a href="<%=path %>/LeibieFendianHuizong/toCategoryPositnSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="class_stores_summary"/></a>
	  		<a href="<%=path %>/FendianLeibieHuizong/toPositnCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="bnranch_category_are_summarized"/></a>
	  		<a href="<%=path %>/CkDanjuHuizong/toChkoutOrderSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="storehouse_data_summary"/></a>
	  		<a href="<%=path %>/PsYanhuoChaxun/toDeliveryExamineQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="delivery_inspection_queries"/></a>
	  		<a href="<%=path %>/DbMingxiChaxun/toChkAllotDetailQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="allocate_detail_query"/></a>
	  		<a href="<%=path %>/DbHuizongChaxun/toChkAllotSumQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="allocation_of_aggregate_query"/></a>
	  		<a href="<%=path %>/WzFendianHuizong/toWzFendianHuizong.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="supply_firm_sum"/></a>
	  		
	  		<!-- 销售订单报表  香他她项目使用-->
	  		<c:if test="${scm_project=='xtt'}">
	  		<a href="<%=path %>/XiaoShouDingDan/toXiaoShouDingDanQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_sales"/><fmt:message key="ORDR"/><fmt:message key="Report_form"/></a>
	  		</c:if>
	  	</div>
	  </fieldset>
	  <fieldset>
		<legend>【<fmt:message key="Supplier_report"/>】</legend>
	  	<div style="width: 500px;">
	  		<a href="<%=path %>/GysJinhuoHuizong/toDeliverStockSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="suppliers_purchase_summary"/></a>
	  		<a href="<%=path %>/LeibieGysHuizong/toCategoryDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/></a>
	  		<a href="<%=path %>/LeibieGysHuizong/toCategoryDeliverSum1.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="category_supplier_summary"/>1</a>
	  		<a href="<%=path %>/GysFendianHuizong/toDeliverPositnSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="vendor_stores_summary"/></a>
	  	</div>
	  	<div style="width: 500px;">
	  		<a href="<%=path %>/GysLeibieHuizong/toDeliverCategorySum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="supplier_category_summary"/></a>
	  		<a href="<%=path %>/GysLeibieHuizong/toDeliverCategorySum2.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="supplier_category_summary"/>2</a>
	  		<a href="<%=path %>/GysLeibieHuizong/toDeliverCategorySum3.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="supplier_category_summary"/>3</a>
	  		<a href="<%=path %>/GysHuizongLiebiao/toDeliverSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="summary_list_of_suppliers"/></a>
	  		<a href="<%=path %>/ZpYanhuoChaxun/toDireInspectionQuery.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="direct"/><fmt:message key="inspection"/><fmt:message key="select"/></a><!-- 直配验货查询 -->
	<%--   		<a href="<%=path %>/JhDanjuHuizong/toStockBillSum.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="purchase_invoices_summary"/></a> --%>
	<%--   		<a href="<%=path %>/GysFukuanQingkuang/toDeliverPayment.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="vendor_payments_situation"/></a> --%>
	  		<!-- 领用供应商类别汇总 -->
	  		<a href="<%=path %>/LyGysLeibieHuizong/toLyGysLeibieHuizong.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum"><fmt:message key="accept"/><fmt:message key="supplier_category_summary"/></a>
	  	</div>
	  </fieldset>
	  <fieldset>
		<legend>【<fmt:message key="Stock_report"/>】</legend>
	  	<div style="width: 890px;">
	  		<a href="<%=path %>/WzMingxiZhang/toSupplyDetailsInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_ledger"/></a>
	  		<a href="<%=path %>/WzYueChaxun/toSupplyBalance.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_balance_inquiry"/></a>
	  		<a href="<%=path %>/WzMingxiJinchu/toSupplyInOutInfo.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="material_detail_out_of_the_table"/></a>
	  		<a href="<%=path %>/WzLeibieJinchubiao/toSupplyTypInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_category_out_of_the_table"/></a>
	  		<a href="<%=path %>/WzZongheJinchubiao/toSupplySumInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="supplies_integrated_out_of_the_table"/></a>
	  		<a href="<%=path %>/WzCangkuJinchubiao/toGoodsStoreInout.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="materials_warehouse_and_out_of_form"/></a>
	  		<a href="<%=path %>/CangkuLeibieJinchu/toStockCategoryInOut.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="warehouse_category_out_of_the_table"/></a>
	  		<a href="<%=path %>/CangkuLeibieJinchuSecond/toCangkuLeibieJinchuSecond.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="store_leibie_jinchutwo"/></a>
	  		<a href="<%=path %>/lsPandianchaxun/toLsPandianchaxun.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="scm_lspdcx"/></a>
	  	</div>
	  </fieldset>
	  <fieldset>
		<legend>【<fmt:message key="Stroe_BOH_report"/>】</legend>
	  	<div style="width: 890px;">
	  		<a href="<%=path %>/MdPdhjZhuangtaiChaxun/toMdPdhjState.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_state_query" /></a>
	  		<a href="<%=path %>/MdPandianchaxun/toMdPandianchaxun.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_inventory_query" /></a>
	  		<a href="<%=path %>/MdPandianchaxun/toMdPandianMingxi.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_inventory_details" /></a>
	  		<a href="<%=path %>/MdPandianHaoyongFenxi/toMdPandianFenxi.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_inventory_analysis" /></a>
	  		<a href="<%=path %>/MdPandianHaoyongFenxi/toMdHaoyongFenxi.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_consumption_analysis" /></a>
	  		<a href="<%=path %>/MdYanhuoChaxun/toMdYanhuoChaxun.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Store_mdyanhuo_query" /></a>
	  	</div>
		<div style="width: 890px;">
	  		<a href="<%=path %>/customReportScm/customReportTree.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key ="Custom_report" /></a>
<%-- 	  		<a href="<%=path %>/FdYingliQingkuang/toFdYingliQingkuang.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">分店盈利情况表</a> --%>
	  	</div>
	  </fieldset>
  	</div>
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  		$('a').click(function(event){
  			var self = $(this);
  			if(!self.attr("tabid")){
  				self.attr("tabid",self.text().replace(/[ ]/g,"")+new Date().getTime());
  			}
  			event.preventDefault();
  			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
  		});
  	</script>
  </body>
</html>
