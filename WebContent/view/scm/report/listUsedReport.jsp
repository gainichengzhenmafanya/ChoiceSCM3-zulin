<%@ page import="com.choice.framework.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_operation_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				#toolbar {
					position: relative;
					height: 27px;
				}
			</style>
		</head>
	<body>
		<ul id="tt" class="easyui-tree" checkbox="true" style="width:310px;height:400px;overflow:auto;"></ul>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		    //加载数据
			$('#tt').tree({
                onlyLeafCheck:true,//只在最小的节点显示checkbox
				data : eval('('+'${data}'+')')
			});
		});

		function saveUsedModel(){
			var node = $('#tt').tree('getChecked');
			if(node && node.length > 0 && node.length < 21){
				if(confirm('<fmt:message key="do_you_want_to_submit_data"/>？')){
					var selected = {};
					for (var i = 0; i < node.length; i++) {
						selected['rms['+i+'].accountModuleId'] = $(node[i]).attr('id');
					}
					$.ajax({url: '<%=path%>/mainInfo/saveUsedReport.do',
						type: "POST",
						async: false,
						data: selected,
						success: function(data){
							if(data == "success"){
							 	//弹出提示信息
							 	alert('<fmt:message key="save_the_settings_successfully_relogin_system_effect"/>！');
							 	parent.$('.close').click();
							}else{
								alert('<fmt:message key="save_fail"/>！');
							}
						}
					});
				}
			}else if(node.length > 20){
				alert('<fmt:message key="Can_only_choose_20_statements" />！');
				return ;
			}else{
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return ;
			}
		}
		</script>

	</body>
</html>