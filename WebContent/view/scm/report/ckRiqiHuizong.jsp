<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
				<div class="form-line"  style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
					<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
					<input type="hidden" id="positn" name="positn" value=""/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
						<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="category"/></div>
					<div class="form-input" style="margin-top: -3px;*margin-top:-2px;">
						<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
						<input type="text" id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" />
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
					</div>
				</div>
				<div class="form-line"  style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="requisitioned_positions"/></div>
					<div class="form-input">
					<input type="text"  id="positn_name1"  name="positn_name1" readonly="readonly" value=""/>
					<input type="hidden" id="firm" name="firm" value=""/>
					<img id="seachPositn1" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					<%-- <select  name="firm" url="<%=path %>/positn/findAllPositnIn.do"  class="select"></select> --%>
					</div>
					<div class="form-label"><fmt:message key="coding"/></div>
					<div class="form-input" style="margin-top: -3px;">
						<input type="text" name="sp_code" id="sp_code" autocomplete="off" class="text" value="<c:out value="${chkoutm.chkoutno}" />"/>
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />	
					</div>
				</div>
				<div class="form-line"  style="z-index:8;">
					<div class="form-label" style="margin-left:15px;text-align: center;width: 186px;"><input type="checkbox" name="bysale" value="true"/><fmt:message key="by_selling_price"/></div>
					<div class="form-label" style="margin-left:15px;"><fmt:message key="document_types"/></div>
					<div class="form-input"><select id="chktyp" name="chktyp" url="<%=path %>/codeDes/findAllBillType.do?codetyp=CK" class="select"></select></div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var bdat = $("#queryForm").find("#bdat").val().toString();
							var edat = $("#queryForm").find("#edat").val().toString();
							if(bdat<=edat){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							
							//动态显示仓位（仓位和领用仓位的筛选不会影响下边仓库和门店的显示，可以优化一下）wangjie 2014年12月11日 14:09:39
							var firm = $("#firm").val();
							var tableContent1 = {};
				  	 		//表头行（单行）
				  	 		var columns1 = [];
				  	 		//表头（多行），其中元素为columns
				  	 		var head1 = [];
							$.ajax({url:"<%=path%>/CkRiqiHuizong/findChkoutDateSumHeaders.do?firm="+firm,
			  	 				async:false,
			  	 				success:function(data){
			  	 					tableContent1 = data;
			  	 				}
			  	 			});
							columns1.push({field:'TOTAL',title:'<fmt:message key="total"/>',width:100,align:'right'});
							for(var i in tableContent1){
				  	 			columns1.push({field:tableContent1[i].code,title:tableContent1[i].des,width:100,align:'right'});
				  	 		}
							head1.push(columns1);
							$("#datagrid").datagrid({columns:head1});
							}else{
								alert("<fmt:message key='startdate'/><fmt:message key='can_not_be_greater_than'/><fmt:message key='enddate'/>");
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#queryForm').attr('action',"<%=path%>/CkRiqiHuizong/exportChkoutDateSum.do");
							$('#queryForm').submit();
							$('#queryForm').attr('target','');
							$('#queryForm').attr('action','<%=path%>/CkRiqiHuizong/toChkoutDateSum.do');
							$("#wait2").val('');//等待加载还原
							
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[
  	 										{key:'<fmt:message key="normal_library"/>',value:'<fmt:message key="normal_library"/>'},
  	 										{key:'<fmt:message key="allocate_library"/>',value:'<fmt:message key="allocate_library"/>'},
  	 										{key:'<fmt:message key="shortage_library"/>',value:'<fmt:message key="shortage_library"/>'},
  	 										{key:'<fmt:message key="giveaways_out_library"/>',value:'<fmt:message key="giveaways_out_library"/>'},
  	 										{key:'<fmt:message key="consignment_out_library"/>',value:'<fmt:message key="consignment_out_library"/>'},
  	 										{key:'<fmt:message key="scrapped"/>',value:'<fmt:message key="scrapped"/>'},
  	 										{key:'<fmt:message key="refunding"/>',value:'<fmt:message key="refunding"/>'},
  	 										{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
	 			  	 			         ]);
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['cntout','amtout','priceout','cntoutprev','amtoutprev','priceoutprev','subvalue'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/CkRiqiHuizong/findChkoutDateSumHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 	//解析获取的数据
	 			columns.push({field:'TOTAL',title:'<fmt:message key="total"/>',width:100,align:'right'});
			for(var i in tableContent){
  	 			columns.push({field:tableContent[i].code,title:tableContent[i].des,width:100,align:'right'});
  	 		}
  	 		frozenColumns.push({field:'DATE',title:'<fmt:message key="project"/>',width:200,align:'left'});
			head.push(columns);
  	 		frozenHead.push(frozenColumns);
  	 		
  	 		//alert("11111");
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="library_date_query"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				var curdat = rows[0].dat;
					var curRow = {};
					var rowTotal = 0;
	 				for(var i in rows){
	 					if(rows[i].dat != curdat){
	 						curRow['DATE'] = convertDate(curdat);
	 						curRow['TOTAL'] = rowTotal.toFixed(2);
	 						modifyRows.push(curRow);
	 						curRow = {};
	 						rowTotal = 0;
	 						curdat = rows[i].dat;
	 					}
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].code;
		 						if(rows[i].firm == currentCol){
		 							curRow[currentCol] = rows[i].amtout.toFixed(2);
		 							rowTotal += Number(rows[i].amtout);
		 						} 
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 				}
	 				curRow['DATE'] = convertDate(curdat);
					curRow['TOTAL'] = rowTotal.toFixed(2);
					modifyRows.push(curRow);
	 				rs.rows = modifyRows;
	 				var modifyFoot = [];
	 				var totalAll = 0;
	 				var foot = rs.footer;
	 				var ft = {};
	 				ft['DATE'] = '<fmt:message key="total"/>';
	 				for(var i in foot){
	 					ft[foot[i].FIRM] = foot[i].AMTOUT;
	 					totalAll += foot[i].AMTOUT;
	 				}
	 				ft['TOTAL'] = totalAll.toFixed(2);
	 				modifyFoot.push(ft);
	 				rs.footer = modifyFoot;
	 				rs.total = rs.rows.length;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/CkRiqiHuizong/findChkoutDateSum.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:false,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
  	 			onDblClickRow:function(index,data){
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
  	 				var parameters = {"bdat":data['DATE'],"edat":data['DATE'],"sp_code":$("#sp_code").val(),
  	 						"positn":$("#positn").val(),"positndes":$("#positn_name").val(),
  	 						"firmcode":$("#firm").val(),"firmdes":$("#positn_name1").val(),
  	 						"typ":$("#typ").val(),"typdes":$("#typdes").val(),
  	 						"chktyp":$("#chktyp").data("checkedVal"),"chktypdes":chktypname};
    	 	  		openTag("ChkoutDetailQuery",'<fmt:message key ="details_of_the_library_inquiry" />',"<%=path%>/CkMingxiChaxun/toChkoutDetailQuery.do",parameters);
  	 			}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 	
  	 		
  	  	 	$(".panel-tool").remove();

	  	  	$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
		  	  $("#seachPositn").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#positn").val(),
						single:false,
						tagName:'positn_name',
						tagId:'positn',
						title:'<fmt:message key="please_select_positions"/>'
					});
				});
		 		$("#seachPositn1").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#firm").val(),
						single:false,
						tagName:'positn_name1',
						tagId:'firm',
						title:'<fmt:message key="please_select_positions"/>'
					});
				});
  	 	});

//   	 	function toColsChoose(){
//   	 		$('body').window({
// 				title: '<fmt:message key="column_selection"/>',
<%-- 				content: '<iframe frameborder="0" src="<%=path%>/CkRiqiHuizong/toColChooseChkoutDateSum.do"></iframe>', --%>
// 				width: '460px',
// 				height: '430px',
// 				draggable: true,
// 				isModal: true
// 			});
//   	 	}

	//择类别的方法，返回类别两边不包含（）
	$('#seachTyp').bind('click.custom',function(e){
		if(!!!top.customWindow){
			var defaultCode = $('#typ').val();
			//var defaultName = $('#typDes').val();
			var offset = getOffset('seachTyp');
			top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?resulttyp=1&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
		}
	});
  	 </script>
  </body>
</html>
