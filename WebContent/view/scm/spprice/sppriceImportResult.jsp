<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="import" />Excel</title>
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/lib.ui.core.css" />
</head>
<body>
	<input type="hidden" id="realFilePath" value='${realFilePath}' />
	<div id="load" style="display: block;">
	  <div id='wBox_loading' align="center">
		<div class="wBox_content" id="wBoxContent">
		  <img src='<%=path%>/image/upload/loading.gif' /><fmt:message key="loading_wait_please" />
		</div>
	 </div>
	</div>
	<div id="result"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript">
		$(function(){
			var realFilePath=$("#realFilePath").val();
			$.ajax({
				type:"POST",
				url:'<%=path%>/spprice/importExcel.do?',
			    data : "realFilePath=" + realFilePath,
				dataType : "json",
				success : function(listError) {
				$("#load").hide();
				if (listError.length > 0) {
					var html_ = "";
					for ( var i = 0; i < listError.length; i += 1) {
						var num = listError[i];
						var rowCol = num.split("_");
						html_ += "<span style='color:red;margin-left:5px;'><fmt:message key="the" />" + rowCol[0] + "<fmt:message key="line" /><fmt:message key="the" />" + rowCol[1]
								+ "<fmt:message key="row" />-"+rowCol[2]+"</span></br>";
					}
					$("#result").html(html_);
				} else {
					$("#result").html("<span style='color:red;margin-left:5px;'><fmt:message key="import_successful" /></span>");
					window.setTimeout(function(){
						window.close();
						parent.pageReload();
					}, 1500);
				}
			}
		});
	});
	</script>
</body>
</html>