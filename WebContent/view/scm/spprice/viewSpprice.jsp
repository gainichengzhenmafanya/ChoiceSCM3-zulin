<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>spprice Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/spprice/viewSpprice.do" id="listForm">
			<input type="hidden" name="sp_code" id="sp_code"/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td style="display: none;"><span style="width:50px;"><fmt:message key="status" /></span></td>
								<td><span style="width:73px;"><fmt:message key="branche" /></span></td>
								<td><span style="width:80px;"><fmt:message key="startdate" /></span></td>
								<td><span style="width:80px;"><fmt:message key="enddate" /></span></td>
								<td><span style="width:50px;"><fmt:message key="contract_price" /></span></td>
								<td><span style="width:140px;"><fmt:message key="default_supplier" /></span></td>
								<td><span style="width:80px;"><fmt:message key="operator" /></span></td>
								<td><span style="width:50px;"><fmt:message key="status" /></span></td>
								<td><span style="width:80px;"><fmt:message key="remark" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="spprice" items="${sppriceList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;"><span>${status.index+1}</span>
									</td>
									<td style="display: none;"><span style="width:50px;"><c:out value="${spprice.sta}" />&nbsp;</span></td>
									<td><span style="width:73px;"><c:out value="${spprice.areaDes}" /></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.bdat}" type="date"/></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.edat}" type="date"/></span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${spprice.price}" /></span></td>
									<td><span style="width:140px;"><c:out value="${spprice.deliverDes}" /></span></td>
									<td id="checby_<c:out value='${spprice.rec}'/>"><span style="width:80px;"><c:out value="${spprice.checby}" />&nbsp;</span></td>
									<td><span style="width:50px;"><c:if test="${spprice.sta=='Y'}"><fmt:message key="checked" /></c:if><c:if test="${spprice.sta!='Y'}"><fmt:message key="unchecked" /></c:if></span></td>
									<td><span style="width:80px;"><c:out value="${spprice.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.jeditable.mini.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>

		<script type="text/javascript">
		
			$(document).ready(function(){
				$('.tool').toolbar();
				setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }else{
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
			});
		</script>
	</body>
</html>