<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<style type="text/css">
				.leftFrame{
					width:40%;
				}
				.mainFrame{
					width:60%;
				}
			</style>
			<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
			<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
			<script type="text/javascript">
				var h = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height();
				$(document).ready(function(){
					$("body , .leftFrame , .mainFrame").height(h);
					$("#choiceFrame").get(0).onload = function(){
						addAction();
						$("#choiceFrame").contents().find(".leftFrame").contents().find(".MzTreeview").click(function(){
							var frame = $("#choiceFrame").contents().find("#mainFrame").get(0);
							$(frame).load(function(){
								addAction();
							});
						});
					};
					
				});
				
				function addAction(){
					$("#choiceFrame").contents().find("#mainFrame").contents().find(".table-body").find('tr').click(function(){
						var spcode = $(this).find("td:first").find('input').val();
						$(this).closest("body").find("#sp_code").val($(this).find(":checkbox").val());
						$(this).addClass('tr-select');
						$(this).siblings("tr").not($(this)).removeClass('tr-select');
						$("#mainFrame").attr("src","<%=path%>/spprice/viewSpprice.do?sp_code=" + spcode);
						$("#mainFrame").load(function(){
							$(this).contents().find("#sp_code").val(spcode);
						});
					});
				}
			</script>
		</head>
	<body>
		<div class="leftFrame">
			<iframe id="choiceFrame" src="<%=path%>/supply/chooseSupply.do?use=spprice"></iframe>
	    </div>
	    <div class="mainFrame">
			<iframe id="mainFrame" src=""></iframe>
	    </div>
	    <script type="text/javascript">
			$('body').height(h);
	    </script>
	</body>
</html>