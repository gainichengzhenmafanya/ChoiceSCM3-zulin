<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>spprice Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/spprice/findSppriceByEdat.do?action=1" id="listForm" name="listForm" method="post">
			<!-- <div class="search-div" style="position: absolute;z-index: 99;"> -->
				<div class="form-line">
					<div class="form-label"><fmt:message key="suppliers" />：</div>
					<div class="form-input" style="width:180px;">
<!-- 						<select name="deliver" class="select"> -->
<%-- 							<option value=""><fmt:message key="please_select_suppliers" /></option> --%>
<%-- 							<c:forEach items="${deliverList }" var="deliver"> --%>
<%-- 								<c:if test="${deliver.code == spprice.deliver }"> --%>
<%-- 									<option value="<c:out value="${deliver.code }"/>" selected="selected"><c:out value="${deliver.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 								<c:if test="${deliver.code != spprice.deliver }"> --%>
<%-- 									<option value="<c:out value="${deliver.code }"/>"><c:out value="${deliver.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
							<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${spprice.deliverDes }"/>
							<input type="hidden" id="deliverCode" name="deliver" value="${spprice.deliver }" style="width: 50px;"/>
							<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label">
						<fmt:message key="Expiration_date" />：
					</div>
					<div class="form-input" style="width:180px;">
						<input type="text" class="Wdate text" name="edat" id="edat" value="<fmt:formatDate value="${spprice.edat }" pattern="yyyy-MM-dd" type="date"/>"/>
						<span class="red">(<fmt:message key="the_ending_date_is_the_date_the_data_before"/>)</span>
					</div>
					
				</div>
				<%-- <div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div> --%>
			<!-- </div> -->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td colspan="4"><fmt:message key="supplies" /></td>
								<td colspan="5"><fmt:message key="pending_offer" /></td>
								<td style="width:80px;" rowspan="2"><span style="width:80px;"><fmt:message key="remaining_maturity_days"/></span></td>
								<td style="width:80px;" rowspan="2"><span style="width:80px;"><fmt:message key="last_offer" /></span></td>
								<td style="width:80px;" rowspan="2"><span style="width:80px;"><fmt:message key="operator" /></span></td>
								<td style="width:150px;" rowspan="2"><span style="width:80px;"><fmt:message key="fill_time" /></span></td>
								<td style="width:80px;" rowspan="2"><span style="width:80px;"><fmt:message key="remark" /></span></td>
							</tr>
							<tr>
								<td><span style="width:80px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:180px;"><fmt:message key="name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="specification" /></span></td>
<%-- 								<td><span style="width:80px;"><fmt:message key="brands" /></span></td> --%>
<%-- 								<td><span style="width:80px;"><fmt:message key="origin" /></span></td> --%>
								<td><span style="width:30px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:90px;"><fmt:message key="branche" /></span></td>
								<td><span style="width:73px;"><fmt:message key="offer" /></span></td>
								<td><span style="width:150px;"><fmt:message key="suppliers" /></span></td>
								<td><span style="width:80px;"><fmt:message key="startdate" /></span></td>
								<td><span style="width:80px;"><fmt:message key="enddate" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="spprice" items="${sppriceList}" varStatus="status">
								<tr>
									<td class="num">
									<span class="num" style="width: 25px;">${status.index+1}</span>
									</td>
									<td><span style="width:80px;" title="${spprice.sp_code}"><c:out value="${spprice.sp_code}" />&nbsp;</span></td>
									<td><span style="width:180px;" title="${spprice.sp_name}"><c:out value="${spprice.sp_name}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${spprice.sp_desc}" />&nbsp;</span></td>
<%-- 									<td><span style="width:80px;"><c:out value="${spprice.sp_mark}" />&nbsp;</span></td> --%>
<%-- 									<td><span style="width:80px;"><c:out value="${spprice.sp_addr}" />&nbsp;</span></td> --%>
									<td><span style="width:30px;"><c:out value="${spprice.unit}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${spprice.areaDes}" /></span></td>
									<td><span style="width:73px;"><c:out value="${spprice.price}" /></span></td>
									<td><span style="width:150px;"><c:out value="${spprice.deliverDes}" /></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.bdat}" type="date"/></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.edat}" type="date"/></span></td>
									<td><span style="width:80px;text-align: center;">
										<c:if test="${spprice.deltaT == 1}"><span class="red"><fmt:message key ="inadequate" />${spprice.deltaT }<fmt:message key ="day" /></span></c:if>
										<c:if test="${spprice.deltaT >1 }">${spprice.deltaT }</c:if>
										<c:if test="${spprice.deltaT <0 }"><fmt:message key ="Is_greater_than" />${spprice.warnDays }<fmt:message key ="day" /></c:if>
									</span></td>
									<td><span style="width:80px;"><c:out value="${spprice.priceold}" />&nbsp;</span></td>
									<td id="checby_<c:out value='${spprice.rec}'/>"><span style="width:80px;"><c:out value="${spprice.checby}" />&nbsp;</span></td>
									<td><span style="width:150px;"><c:out value="${spprice.madet}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${spprice.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.jeditable.mini.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript">
		
		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
		}
			function pageReload(){
				$('#listForm').submit();
			}
/* 			function clearQueryForm(){
				$('#listForm input[type=text]').val('');
				$('#listForm select option').removeAttr("selected");
				$('#listForm select option[value=""]').attr("selected","selected");
				$('#listForm #status option[value="all"]').attr("selected","selected");
			} */
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" /><fmt:message key="price_template_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								//$('.search-div').slideToggle(100);
								$("#deliver").val($('select').val());
								$('#listForm').submit();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
				
				$("#edat").bind('click',function(){
					new WdatePicker();
				});
				//选择供应商
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliverDes').val();
						//var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),null,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
					}
				});
				var edat = $.trim('<fmt:formatDate value="${spprice.edat}" type="date" pattern="yyyy-MM-dd"/>');
				/* 模糊查询提交 */
				/* $("#search").bind('click', function() {
					$('.search-div').hide();
					$("#deliver").val($('.search-div select').val());
					$('#listForm').submit();
				}); */
				/* 模糊查询清空 */
				/* $("#resetSearch").bind('click', function() {
					clearQueryForm();
				}); */

				setElementHeight('.grid',['.tool'],$(document.body),75);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});
		</script>
	</body>
</html>