<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>spprice Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/spprice/listByCon.do" id="listForm" name="listForm" method="post">
				<div class="form-line">
					<div class="form-label">
						<fmt:message key="supplies_code" />
					</div>
					<div class="form-input" style="width:180px;">
						<input class="text" type="text" name="sp_code" id="sp_code" value="${spprice.sp_code }"  style="width:128px;"/>
						<img id="seachSupply" class="seachSupply" style="margin-top: 4px" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' />
					</div>
					<div class="form-label"><fmt:message key="suppliers"/></div>
						<div class="form-input" style="width:370px;">
							<input type="text" class="text" id="deliver_select"  onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${spprice.deliver}"/>&nbsp;
							<select class="select" id="deliver" name="deliver" style="width:320px;" onchange="findDeliverByDes(this);">
								<option value=""><fmt:message key="please_select_suppliers" /></option>
								<c:forEach var="deliver" items="${deliverList}" varStatus="status">
									<option 
										<c:if test="${deliver.code==spprice.deliver}"> 
												   	selected="selected"
										</c:if> 
										 value="${deliver.code}" title="${deliver.des }">${deliver.code}-${deliver.init}-${deliver.des}
									</option>
								</c:forEach>
							</select>
						</div>
<%-- 					<div class="form-label"><fmt:message key="suppliers" /></div> --%>
<!-- 					<div class="form-input" style="width:180px;"> -->
<!-- 						<select id="deliver" name="deliver" class="select"> -->
<%-- 							<option value=""><fmt:message key="please_select_suppliers" /></option> --%>
<%-- 							<c:forEach items="${deliverList }" var="deliver"> --%>
<%-- 								<c:if test="${deliver.code == spprice.deliver }"> --%>
<%-- 									<option value="<c:out value="${deliver.code }"/>" selected="selected"><c:out value="${deliver.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 								<c:if test="${deliver.code != spprice.deliver }"> --%>
<%-- 									<option value="<c:out value="${deliver.code }"/>"><c:out value="${deliver.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
<!-- 					</div> -->
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="supplies_category" /></div>
					<div class="form-input" style="width:180px;">
						<select id="typ" name="typ" class="select" style="width:130px;">
							<option value=""><fmt:message key="please_select_supplies_category" /></option>
							<c:forEach items="${typList}" var="typ">
								<c:if test="${typ.code == spprice.typ }">
									<option value="<c:out value="${typ.code }"/>" selected="selected"><c:out value="${typ.des }"/></option>
								</c:if>
								<c:if test="${typ.code != spprice.typ }">
									<option value="<c:out value="${typ.code }"/>"><c:out value="${typ.des }"/></option>
								</c:if>
							</c:forEach>
						</select>
					</div><div class="form-label"><fmt:message key="branche" /></div>
					<div class="form-input" style="width:370px;">
						<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${spprice.area}"/>
						<input type="hidden" class="text"  id="positnDes" />
						<select class="select" id="area" name="area" style="width:320px; margin-left: 3px;" onchange="findPositnByDes(this);">
							<option value=""><fmt:message key="please_select_branche" /></option>
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option 
									<c:if test="${positn.code==spprice.area}"> 
											   	selected="selected"
									</c:if> 
									id="${positn.code}" value="${positn.code}" title="${positn.des }">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
					</div>
<!-- 					<div class="form-input" style="width:180px;"> -->
<!-- 						<select id="area" name="area" class="select"> -->
<%-- 							<option value=""><fmt:message key="please_select_branche" /></option> --%>
<%-- 							<c:forEach items="${positnList }" var="positn"> --%>
<%-- 								<c:if test="${positn.code == spprice.area }"> --%>
<%-- 									<option value="<c:out value="${positn.code }"/>" selected="selected"><c:out value="${positn.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 								<c:if test="${positn.code != spprice.area }"> --%>
<%-- 									<option value="<c:out value="${positn.code }"/>"><c:out value="${positn.des }"/></option> --%>
<%-- 								</c:if> --%>
<%-- 							</c:forEach> --%>
<!-- 						</select> -->
<!-- 					</div> -->
				</div>
				<div class="form-line">
					<div class="form-label">
						<fmt:message key="date" />
					</div>
					<div class="form-input" style="width:180px;">
						<input type="text" class="Wdate text" style="width:128px;" name="edat" id="edat" value="<fmt:formatDate value="${spprice.edat }" pattern="yyyy-MM-dd" type="date"/>"/>
					</div>
					<div class="form-label">
						<fmt:message key="status" />
					</div>
					<div class="form-input" style="width:180px;">
						<input type="radio" id="checby" name="checby" value="checked"/><fmt:message key="checked" />
						<input type="radio" id="checby" name="checby" value="uncheck"/><fmt:message key="unchecked" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="width:180px;">
						<input type="radio" name="useDate" id="disableDate"/><fmt:message key="date_invalid" />
						<input type="radio" name="useDate" checked="checked" id="enableDate"/><fmt:message key="date_effective" />
					</div>
					<div class="form-label"></div>
					<div class="form-input" style="width:180px;"></div>
				</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 23px;" rowspan="2">&nbsp;</td>
								<td style="width:30px;" rowspan="2">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td colspan="6"><fmt:message key="supplies" /></td>
								<td colspan="5"><fmt:message key="pending_offer" /></td>
								<td style="width:80px;" rowspan="2"><fmt:message key="last_offer" /></td>
								<td style="width:80px;" rowspan="2"><fmt:message key="operator" /></td>
								<td style="width:140px;" rowspan="2"><fmt:message key="fill_time" /></td>
								<td style="width:80px;" rowspan="2"><fmt:message key="remark" /></td>
							</tr>
							<tr>
								<td style="width:80px;"><fmt:message key="coding" /></td>
								<td style="width:180px;"><fmt:message key="name" /></td>
								<td style="width:80px;"><fmt:message key="specification" /></td>
								<td style="width:80px;"><fmt:message key="brands" /></td>
								<td style="width:80px;"><fmt:message key="origin" /></td>
								<td style="width:30px;"><fmt:message key="unit" /></td>
								<td style="width:95px;"><fmt:message key="branche" /></td>
								<td style="width:73px;"><fmt:message key="offer" /></td>
								<td style="width:95px;"><fmt:message key="suppliers" /></td>
								<td style="width:80px;"><fmt:message key="startdate" /></td>
								<td style="width:80px;"><fmt:message key="enddate" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<!-- 该登录人员是否是特殊人员还是一般的审核人员 -->
					<input  type="hidden" id="audit" value="${audit}"/>
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="map" items="${mapList}" varStatus="status">
								<tr>
									<td class="num" style="width: 23px;"><span>${status.index+1}</span>
									<input type="hidden" class="upper" id="upper" value="<c:out value="${map.upper}"/>"/>
									<input type="hidden" class="lower" id="lower" value="<c:out value="${map.lower}"/>"/>
									</td>
									<td><span style="width:30px; text-align: center;">
										<input name="idList" type="checkbox" id="chk_<c:out value='${map.rec}' />" value="<c:out value='${map.rec}' />"/></span>
									</td>
									<td><span style="width:80px;"><c:out value="${map.sp_code}" />&nbsp;</span></td>
									<td><span style="width:180px;"><c:out value="${map.sp_name}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${map.sp_desc}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${map.sp_mark}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${map.sp_addr}" />&nbsp;</span></td>
									<td><span style="width:30px;"><c:out value="${map.unit}" />&nbsp;</span></td>
									<td><span style="width:95px;"><c:out value="${map.areaDes}" /></span></td>
									<td><span style="width:73px;text-align: right;"><c:out value="${map.price}" /></span></td>
									<td><span style="width:95px;"><c:out value="${map.deliverDes}" /></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${map.bdat}" type="date"/></span></td>
									<td><span style="width:80px;"><fmt:formatDate value="${map.edat}" type="date"/></span></td>
									<td><span style="width:80px;text-align: right;"><c:out value="${map.priceold}" />&nbsp;</span></td>
									<td id="checby_<c:out value='${map.rec}'/>"><span style="width:80px;"><c:out value="${map.checby}" />&nbsp;</span></td>
									<td><span style="width:140px;"><c:out value="${map.madet}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${map.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.jeditable.mini.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript">
		

		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
		}
			function pageReload(){
				$('#listForm').submit();
			}
			function clearQueryForm(){
				$('#listForm input[type=text]').val('');
				$('#listForm select option').removeAttr("selected");
				$('#listForm select option[value=""]').attr("selected","selected");
				$('#listForm #status option[value="all"]').attr("selected","selected");
			}
			function updateStatus(){
				var audite = $("#audit").val();
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="whether_to_offer_check" />？')){
						var chkValue=[];
						if(audite==1){	
							
						}
						checkboxList.filter(':checked').each(function(){
							var num = $(this).val();
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/spprice/updateBySave.do?sta=audit&rec='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="check_offer" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_check_information" />！');
					return ;
				}
			}
			//全部审核--审核所有未审核的物资
			function updateAllStatus(){
				var sp_code = $('#sp_code').val();
				var deliver = $('#deliver').val();
				var typ = $('#typ').val();
				var area = $('#area').val();
				var edat = '';
				if(!$('#edat').attr('disabled') || $('#edat').attr('disabled') != 'disabled'){
					edat = $('#edat').val();
				}
				var checby = $("input[name='checby']:checked").val();
				if(confirm('<fmt:message key="Is_to_determine_all_the_materials_under_the_current_conditions" />？')){
					var action = '<%=path%>/spprice/antiAllAudit.do?sp_cdoe='+sp_code+'&deliver='+deliver+'&typ='+typ+'&area='+area+'&edat='+edat+'&checby='+checby;
					$('body').window({
						title: '<fmt:message key ="check_offer" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}
			function antiAudit(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="whether_to_offer_uncheck" />？')){
						var chkValue=[];
						checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
						});
						var action = '<%=path%>/spprice/updateBySave.do?sta=anti&rec='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="uncheck_offer" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_uncheck_information" />！');
					return ;
				}
			}
			//全部反审核----反审核所有已经审核的物资
			function antiAllAudit(){
				var sp_code = $('#sp_code').val();
				var deliver = $('#deliver').val();
				var typ = $('#typ').val();
				var area = $('#area').val();
				var edat = '';
				if(!$('#edat').attr('disabled') || $('#edat').attr('disabled') != 'disabled'){
					edat = $('#edat').val();
				}
				var checby = $("input[name='checby']:checked").val();
				if(confirm('<fmt:message key="Are_all_materials_to_be_examined_under_the_current_conditions_of_the_review" />？')){
					var action = '<%=path%>/spprice/antiAllAudit.do?sp_cdoe='+sp_code+'&deliver='+deliver+'&typ='+typ+'&area='+area+'&edat='+edat+'&checby='+checby;
					$('body').window({
						title: '<fmt:message key="uncheck_offer" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}
			function deleteSpprice(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="whether_delete_quotes" />？')){
						var chkValue=[];
						checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
						});
						var action = '<%=path%>/spprice/deleteSpprice.do?rec='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_quotes" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
			
			function loadToolBar(op){
				return $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" /><fmt:message key="price_template_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')} & op[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								//$('.search-div').slideToggle(100);
								$("#listForm").submit();
							}
						},"-",{
							text: '<fmt:message key="check" />',
							title: '<fmt:message key="check" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')} & op[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-100px']
							},
							handler: function(){
								updateStatus();
							}
						},{
							text: '<fmt:message key ="checkAll" />',
							title: '<fmt:message key="Audit_of_all_non_audit_materials" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')} & op[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-100px']
							},
							handler: function(){
								updateAllStatus();
							}
						},{
							text: '<fmt:message key="uncheck" />',
							title: '<fmt:message key="uncheck" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')} & op[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-100px']
							},
							handler: function(){
								antiAudit();
							}
						},{
							text: '<fmt:message key ="uncheckAll" />',
							title: '<fmt:message key="Review_of_all_the_materials_that_have_been_reviewed" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')} & op[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-100px']
							},
							handler: function(){
								antiAllAudit();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')} & op[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteSpprice();
							}
						},{
							text: '<fmt:message key="import" />',
							title: '<fmt:message key="import" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
							icon: {
								url: '<%=path%>/image/Button/excel.bmp',
								position: ['2px','2px']
							},
							handler: function(){
								importSpprice();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
			}
			
			//导入 jinshuai 20160504
		    function importSpprice(){
		    	$('body').window({
					id: 'window_importSpprice',
					title: '<fmt:message key="import" /><fmt:message key="Material_price" />',
					content: '<iframe id="importSpprice" frameborder="0" src="<%=path%>/spprice/importSpprice.do"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		    }
			
			//2014.10.21 wjf
			function findPositnByDes(inputObj){
				$('#positn_select').val($(inputObj).val());
	        }
			function findDeliverByDes(inputObj){
				$('#deliver_select').val($(inputObj).val());
	        }
			$(document).ready(function(){
				//2014.10.21 wjf
				$("#positn_select").val($("#area").val());
				$("#deliver_select").val($("#deliver").val());
//  			alert($(".table-head").find("tr:eq(0)").find("td:eq(0)").css("width"));
				if($(".table-head").find("tr:eq(0)").find("td:eq(0)").css("width")=="17px"){
					$(".table-head").find("tr:eq(0)").find("td:eq(0)").css("width","24px");
				}
				/*过滤*/
				$('#deliver_select').bind('keyup',function(){
			          $("#deliver").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				$('#positn_select').bind('keyup',function(){
			          $("#area").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
				
				
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				var tool ;
				var op_chked = [true,false,true,false];
				var op_unchk = [true,true,false,true];
				$(".editable").each(function(){
					var num = $(this).parent().parent().find("td").first().text().trim();
					var url = '<%=path%>/sppriceDemo/saveByUpdate.do?acct='+$("#acct").val()+'&firm='+$.trim($("#firm_"+num).text())+'&deliver='+$.trim($("#deliver_"+num).text())+'&sp_code='+$.trim($("#sp_code_"+num).text());
					var param = $(this).parent().attr("id").replace("_"+num,"");
					$(this).editable(url,{
						name:param
					});
				});
				var chk = '${spprice.checby}';
				$(":radio[name='checby'][value='${spprice.checby}']").attr("checked","checked");
				if(chk == "checked")
					tool = loadToolBar(op_chked);
				else
					tool = loadToolBar(op_unchk);
				$("#edat").bind('click',function(){
					new WdatePicker();
				});
				
				$("#enableDate").click(function(){
					$("#edat").removeAttr("disabled");
					$("#edat").bind('click',function(){
						new WdatePicker();
					});
				});
				
				$("#disableDate").click(function(){
					if($(this).attr("checked")){
						$("#edat").attr("disabled","disabled");
						$("#edat").unbind("click");
					}
				});
				var edat = $.trim('<fmt:formatDate value="${spprice.edat}" type="date" pattern="yyyy-MM-dd"/>');
				if(edat){
					$("#edat").val(edat);
				}else{
					$("#edat").htmlUtils("setDate","now");
					$("#disableDate").attr('checked','checked');
					$("#edat").attr("disabled","disabled");
					$("#edat").unbind("click");
				}
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'));	
					}
				});
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$("#deliver").val($('.search-div select').val());
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});

				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.table-head').width($('.table-head').width()+17);
				$('.table-body').width($('.table-body').width()+17);
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }
					     else
					     {
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
			});
		</script>
	</body>
</html>