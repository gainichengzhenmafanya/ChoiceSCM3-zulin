<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>预估报货</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
			<style type="text/css">
			.onEdit{
				border:1px solid;
				border-bottom-color: blue;
				border-top-color: blue;
				border-left-color: blue;
				border-right-color: blue;
			}
			.input{
				background:transparent;
				border:0px solid;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.textInput span {
				padding:0px;
			}
			.textInput input {
				border:0px;
				width:35px;
			}
			</style>							
		</head>
	<body>
		<div class="tool">
		</div>
		<form  id="listForm"  action="<%=path %>/spCodeUse/tableCk.do" method="post">
			<input type="hidden" name="firm"  id="firm" class="text" value="${spCodeUse.firm}"/> 
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/>:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" style="width: 120px;" pattern="yyyy-MM-dd" value="<fmt:formatDate value="${spCodeUse.bdat}" pattern="yyyy-MM-dd"/>"/>
					</div>
					<div class="form-label"><fmt:message key="enddate"/>:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" style="width: 120px;" pattern="yyyy-MM-dd" value="<fmt:formatDate value="${spCodeUse.edat}" pattern="yyyy-MM-dd"/>"/>
					</div>
					<div class="form-label">系数：</div>
					<div class="form-input">
						<input type="text" id="safetystock" name="safetystock" class="text" value="${spCodeUse.safetystock}" default="10"/>
					</div>
					<div class="form-label">到货日期:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="ddat" name="ddat" class="Wdate text" style="width: 120px;" pattern="yyyy-MM-dd" value="<fmt:formatDate value="${spCodeUse.ddat}" pattern="yyyy-MM-dd"/>"/>
					</div>
				</div>
			</div>
			<div id="tableAll" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 30px;">&nbsp;</td>
								<td style="width:100px;">物资编码</td>
								<td style="width:150px;">物资名称</td>
								<td style="width:150px;">物资规格</td>
								<td style="width:100px;">单位</td>
								<td style="width:100px;">日均用量*系数</td>
								<td style="width:100px;">出库预估量</td>
								<td style="width:100px;">在途数量</td>
								<td style="width:100px;">订货量</td>
								<td style="width:100px;">调整量</td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="SpCodeUse" items="${listSpSupplyCal}" varStatus="status">
								<tr>
									<td class="num" style="width: 30px;">${status.index+1}</td>
<!-- 									<td style="width:30px; text-align: center;"> -->
<%-- 										<input type="checkbox"  name="idList" id="chk_<c:out value='' />" value=""/> --%>
<!-- 									</td> -->
									<td><span style="width:90px;"><c:out value="${SpCodeUse.supply.sp_code}" />&nbsp;</span></td>
									<td><span style="width:140px;"><c:out value="${SpCodeUse.supply.sp_name}" />&nbsp;</span></td>
									<td><span style="width:140px;"><c:out value="${SpCodeUse.supply.sp_desc}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${SpCodeUse.supply.unit}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${SpCodeUse.cnt}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${SpCodeUse.supply.cnt}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${SpCodeUse.cntzt}" />&nbsp;</span></td>
									<td><span style="width:90px;"><c:out value="${SpCodeUse.prorequi}" />&nbsp;</span></td>
									<td class="textInput"><span title="${SpCodeUse.prorequi}" style="width:100px;padding: 0px;">
										<input type="text" value="${SpCodeUse.prorequi}" onkeyup="validate(this)" onblur="xx(this)" style="width:100px;text-align: right;padding: 0px;" />
									</span></td>
									<td style="display:none;"><span><input type="hidden" value="${SpCodeUse.supply.unitper}"/></span></td>
									<td style="display:none;"><span><input type="hidden" value="${SpCodeUse.supply.mincnt}"/></span></td>
									<td style="display:none;"><span><input type="hidden" value="${SpCodeUse.supply.sp_price}"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
			 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
			  <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
			 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
			 <script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){
				$('.table-body').find('tr').each(function(i){
					amount = Math.round(($(this).find('td:eq(8) span input').val())*100/100);
					$(this).find('td:eq(8) span input').val(amount);
				});
				var tool = $('.tool').toolbar({
					items: [{
								text: '<fmt:message key ="select" />',
								title: '<fmt:message key="select" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-100px','-40px']
								},
								handler: function(){
									$('#listForm').submit();
								}
							},{
								text: '生成报货单',
								title: '生成报货单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-100px','-40px']
								},
								handler: function(){
									saveStoreCargo();
								}
							},{
								text: '<fmt:message key ="quit" />',
								title: '<fmt:message key ="quit" />',
								icon: {
									url: '/Choice/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}
						]
					});
				setElementHeight('.grid',['.tool'],$(document.body),31);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#bdat').bind('click',function(){
					new WdatePicker();
				});
				$('#edat').bind('click',function(){
					new WdatePicker();
				});
				$('#ddat').bind('click',function(){
					new WdatePicker();
				});
				//取整
				$('.table-body').find('tr').each(function(i){
					amount = $(this).find('td:eq(9) span input').val();
					$(this).find('td:eq(9) span input').val(Number(amount)-Number(amount)%1+1);
				});
			});
			function saveStoreCargo(){
				if ($('#ddat').val()==null || $('#ddat').val()=='') {alert("请选择到货日期！");return;}
				
				var selected = {};
				var flag=1;
				$('.table-body').find('tr').each(function(i){
					amount = $(this).find('td:eq(9) span input').val();
					if ((flag==1)&&(Number(amount)%1)!=0){
						alert("报货数量应该为整数！");
						flag=0;
						return;
					}
					selected['chkstodList['+i+'].supply.sp_code'] = $.trim($(this).find('td:eq(1) span').text());
					selected['chkstodList['+i+'].amount'] = $(this).find('td:eq(9) span input').val();
					selected['chkstodList['+i+'].price'] = $(this).find('td:eq(12) span input').val();
					selected['chkstodList['+i+'].memo'] = '';
					selected['chkstodList['+i+'].amount1'] = Number($(this).find('td:eq(9) span input').val())*Number($(this).find('td:eq(10) span input').val());//数量1
					selected['chkstodList['+i+'].hoped'] = $('#ddat').val();
				});
				if (flag==1) {
					$.post("<%=path%>/spCodeUse/saveByAddOrUpdate.do",selected,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs)){
						case -1:
							alert('<fmt:message key="save_fail"/>！');
							break;
						case 1:
							showMessage({
										type: 'success',
										msg: '<fmt:message key="save_successful"/>！',
										speed: 3000
										});
							break;
						}
					});	
				}
			}
			//焦点离开检查输入是否合法
			function validate(e){
				if(null==e.value || ""==e.value){
					e.value=e.defaultValue;
					showMessage({
								type: 'error',
								msg: '<fmt:message key="cannot_be_empty"/>！',
								speed: 1000
								});
					$(e).focus();
					return;
				}
				if(Number(e.value)<0 || isNaN(e.value)){
					e.value=e.defaultValue;
					showMessage({
								type: 'error',
								msg: '不是有效数字！',
								speed: 1000
								});
					$(e).focus();
					return;
				}
			}
			function xx(e){
				var mincnt = $(e).parents().find("tr").find("td:eq(10)").find('input').val();
				if ((Number(e.value)%mincnt)!=0 && mincnt!=0){
					showMessage({
						type: 'error',
						msg: '不是最小申购量的整数倍！',
						speed: 1000
						});
// 					$(e).focus();
					return;
				}
			}
			//编辑采购数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
		 		if(e.keyCode==13){
		 			var lie = $(this).parent().parent().prevAll().length;
					var hang= $(this).parent().parent().parent().prevAll().length + 1;
					$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
				}
			});
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
		</script>
	</body>
</html>