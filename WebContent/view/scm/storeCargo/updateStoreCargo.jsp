<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="audit_filled_reported_manifest"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/chkstom/saveByAdd.do" method="post">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" style="width:200px;" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker();" readonly="readonly"/>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${chkstom.chkstoNo!=null}"><c:out value="${chkstom.chkstoNo }"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
<%-- 						<input type="text" value="${chkstom.chkstoNo }" class="text" disabled="disabled"/> --%>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="purchase_positions"/>：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" class="text" id="firmDes" onfocus="this.select()" name="firmDes" style="width:40px;margin-top:4px;vertical-align:top;" value="${chkstom.firm}"/>
						<select class="select" id="firm" name="firm" style="width:155px" onchange="findByDes(this);">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==chkstom.firm}"> 
									   	selected="selected"
								</c:if>
								id="des" value="${positn.code}" >${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
					</div>					
					<div class="form-label"><fmt:message key="make_orders"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${chkstom.madeby!=null}"><c:out value="${chkstom.madeby}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/>
<%-- 						<input type="text" value="${chkstom.madeby }" class="text" disabled="disabled"/> --%>
					</div>					
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="width:205px;"></div>
					<div class="form-label"><fmt:message key="accounting"/>:</div>
					<div id="showChecby" class="form-input">
						<c:if test="${chkstom.checby!=null}"><c:out value="${chkstom.checby}"></c:out></c:if>
						<input type="hidden" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo }"/> 					
						<input type="hidden" id="checby" name="checby" class="text" value="${chkstom.checby }"/>
<%-- 						<input type="text" id="showChecby" value="${chkstom.checby }" class="text" disabled="disabled"/> --%>
					</div>														
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;">售价</span></td>
								<td><span style="width:70px;">金额</span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="SpCodeUse" items="${listSpSupplyCal}" varStatus="status">
							<c:if test="${SpCodeUse.prorequi>0 }">
								<tr data-mincnt="" data-unitper="">
									<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
									<td><span style="width:70px;">${SpCodeUse.supply.sp_code }</span></td>
									<td><span style="width:90px;">${SpCodeUse.supply.sp_name }</span></td>
									<td><span style="width:70px;">${SpCodeUse.supply.sp_desc }</span></td>
									<td><span style="width:70px;text-align: right;">${SpCodeUse.prorequi }</span></td>
									<td><span style="width:70px;">${SpCodeUse.supply.unit }</span></td>
									<td><span style="width:70px;text-align: center;" price="${SpCodeUse.supply.sp_price}">${SpCodeUse.supply.sp_price}</span></td>
									<td><span style="width:70px;text-align: center;">${SpCodeUse.supply.sp_price*SpCodeUse.prorequi}</span></td>
									<td><span style="width:70px;text-align: right;">${SpCodeUse.prorequi }</span></td>
									<td><span style="width:55px;">${SpCodeUse.supply.unit }</span></td>
									<td><span style="width:90px;"><fmt:formatDate value="${hoped}" pattern="yyyy-MM-dd"/></span></td>
									<td><span style="width:70px;">${chkstod.memo }</span></td>
									<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${chkstod.price }"/></span></td>								
								</tr>
							</c:if>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + SpCodeUse.prorequi}"/>   
							<c:set var="sum_price" value="${sum_price + SpCodeUse.supply.sp_price*SpCodeUse.prorequi}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num"><fmt:formatNumber value="${sum_num}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:100px;"></td>
							<td style="width:100px;background:#F1F1F1;">总金额：<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label></td>
							<td style="width:65px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:65px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;"></td>
							<td><font color="blue"><fmt:message key="operation_tips"/></font></td>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var validate;
		//工具栏
		$(document).ready(function(){

			$("#firmDes").val($("#firm").val());
			/*过滤*/
			$('#firmDes').bind('keyup',function(){
		          $("#firm").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//按钮快捷键
			focus() ;//页面获得焦点
			//屏蔽鼠标右键
			//$(document).bind('contextmenu',function(){return false;});
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="4"||index=="8"){
			 			$(e.srcElement).unbind('blur').blur(function(e){
			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			 			});
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    		}
		    	}
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
			 		if(window.event && window.event.keyCode == 120) { 
			 			window.event.keyCode = 505; 
			 			searchChkstodemo();
			 		} 
			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 85: $('#autoId-button-101').click(); break;//查看上传
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 65: $('#autoId-button-103').click(); break;//新建
	                case 69: $('#autoId-button-104').click(); break;//编辑
	                case 83: $('#autoId-button-105').click(); break;//保存
					case 68: $('#autoId-button-106').click(); break;//删除
	                case 80: $('#autoId-button-107').click(); break;//打印
	                case 67: $('#autoId-button-108').click(); break;//审核
	            }
			}); 
		 	//回车换焦点start
		    var array = new Array();        
	 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
	 	    array = ['firmDes','firm'];        
	 		//定义加载后定位在第一个输入框上          
	 		$('#'+array[0]).focus();            
	 		$('select,input[type="text"]').keydown(function(e) {                  
		 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
		 		var event = $.event.fix(e);                
		 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
		 		if (event.keyCode == 13) {                
		 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
	 				$('#'+array[++index]).focus();
	 				if(index==2){
	 					$('#firmDes').val($('#firm').val());
	 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
	 				} 
		 		}
	 		});    
	 		
	 		document.getElementById("firm").onfocus=function(){
	 			this.size = this.length;
	 			$('#firm').css('height','425px');
	 			$('#firm').parent('.form-input').css('z-index','2');
	 		};
 			document.getElementById("firm").onblur = function(){
	 			this.size = 1;
	 			$('#firm').css('height','20px');
 			};
	 		$('#firm').bind('dblclick', function() {
					$('#firmDes').val($(this).val());
					$('#firm').blur();
			});
 		    //回车换焦点end
			//判断按钮的显示与隐藏
			if($("#sta").val() == "add"){
				loadToolBar([true,true,true,false,true,false,false,false]);
				editCells();
// 				searchChkstodemo();
			}else if($("#sta").val() == "show"){
				loadToolBar([true,true,true,true,false,true,true,true]);
			}else{
				loadToolBar([true,true,true,false,false,false,false,false]);
				$('#maded').attr("disabled","disabled");
				$('#firmDes').attr("disabled","disabled");
				$('#firm').attr("disabled","disabled");
			}
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'maded',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			<c:if test="${tableFrom=='table'}">
				$('#autoId-button-102').click();
			</c:if>
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									if(null!=$('#firm').val() && ""!=$('#firm').val()){
										saveChkstom();
									}else{
										alert('申购仓位不能为空！');
									}
								}
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}	
		
		//编辑表格
		function editCells()
		{
			if($("#sta").val() == "add"){
				$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:12,
				widths:[26,80,100,80,80,80,80,80,80,65,100,80],
				colStyle:['','','','',{background:"#F1F1F1"},'','','','','','',''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[2,4,10,11],//能输入的位置(8暂去)
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(4)').text());//总数量
					$('#sum_price').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(7)').text())).toFixed(2));//总金额
// 					$('#sum_amount').text(Math.round((Number($('#sum_amount').text())-Number(row.find('td:eq(5)').text()))*100)/100);//总数量
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(getName());
							return;
						}
					 	else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					function getName(){
						var name='';
						$.ajaxSetup({ 
							  async: false 
							});
						$.get("<%=path %>/supply/findById.do",
								{sp_code:$.trim(data.curobj.closest('td').prev().text())},
								function(data){
									name =  data.sp_name;
								});
						return name;
					};
				},
				cellAction:[{
					index:2,
					action:function(row){
						$.fn.autoGrid.setCellEditable(row,4);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/supply/findTop.do';
						if(!isNaN(data.value))
							data['key'] = 'sp_code';
						else
							data['key'] = 'sp_init';
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '<fmt:message key="added_supplies_remind"/>！',
 								speed: 1000
 							});
						}
						//查询售价
						$.ajax({
							type: "POST",
							url: "<%=path%>/supply/findSprice.do",
							data: "supply.sp_code="+data2.sp_code+"&area.code="+$('#firmDes').val()+"&madet="+$('#maded').val(),
							dataType: "json",
							success:function(spprice){
								row.find("td:eq(1) span").text(data2.sp_code);
								row.find("td:eq(2) span input").val(data2.sp_name).focus();
								row.find("td:eq(3) span").text(data2.sp_desc);
								row.find("td:eq(4) span").text(0).css("text-align","right");
								row.find("td:eq(5) span").text(data2.unit);
								row.find("td:eq(6) span").text(spprice.price).attr("price",spprice.price).css("text-align","center");
								row.find("td:eq(7) span").text(0).css("text-align","center");
								row.find("td:eq(8) span").text(0).css("text-align","right");
								row.find("td:eq(9) span").text(data2.unit1);
								row.find("td:eq(10) span").text();
								row.find("td:eq(11) span").text();
								row.data("unitper",data2.unitper);
								row.data("mincnt",data2.mincnt);
							}
						});
						//查询售价结束
					}
				},{
					index:4,
					action:function(row,data2){
						if(Number(data2.value) == 0){
							alert('<fmt:message key="number_cannot_be_zero"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(data2.value) < 0){
							alert('数量不能为负数！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(data2.value)<Number(row.data("mincnt"))){
							alert('该物质最小申购数量为'+row.data("mincnt")+'!')
							row.find("td:eq(4)").find('span').text(data2.value);
							$.fn.autoGrid.setCellEditable(row,4);
						}
						else {
							//$('#sum_amount').text(Number($('#sum_amount').text())+Number(row.find("td:eq(4)").text())-data2.ovalue);//总数量
							var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(7)').text());
							row.find("td:eq(7) span").text((Number(row.find("td:eq(4)").text())*Number(row.find("td:eq(6)").text())).toFixed(2));
							row.find("td:eq(8) span").text((Number(row.find("td:eq(4)").text())*Number(row.data("unitper"))).toFixed(2));
							Sprice=Sprice+Number(row.find('td:eq(7)').text());//总金额
							$('#sum_price').text(Sprice.toFixed(2));//总金额
							$.fn.autoGrid.setCellEditable(row,10);
						}
					}
				},{
					index:8,
					action:function(row,data){
						if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							$.fn.autoGrid.setCellEditable(row,8);
						}else if((Number(data.value)/Number(row.data("unitper"))).toFixed(2)<Number(row.data("mincnt"))){
							alert((Number(data.value)/Number(row.data("unitper"))).toFixed(2));
							alert('该物质参考单位数量最小为'+row.data("mincnt")*Number(row.data("unitper"))+'!')
							row.find("td:eq(8) span").text(data.value);
							$.fn.autoGrid.setCellEditable(row,8);
						}else{
							$.fn.autoGrid.setCellEditable(row,10);
							row.find("td:eq(4) span").text((Number(row.find("td:eq(8)").text())/Number(row.data("unitper"))).toFixed(2));
						}
					}
				},{
					index:10,
					action:function(row,data){
						$.fn.autoGrid.setCellEditable(row,11);
					},CustomAction:function(event,data){
						var input = data.curobj.find('span').find('input');
						input.addClass("Wdate text");
						input.bind('click',function(){
							input.unbind('keyup');
							new WdatePicker({
								startDate:'%y-%M-{%d+1}',
								el:'input',
								onpicked:function(dp){
									data.curobj.find('span').html(dp.cal.getNewDateStr());
									$.fn.autoGrid.setCellEditable(data.row,11);
								}
							});
						});
						input.trigger('click');
					}
				},{
					index:11,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}
		
		//保存添加
		function saveChkstom()
		{
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					var amount1=$(this).find('td:eq(8)').text()?$(this).find('td:eq(8)').text():$(this).find('td:eq(8) input').val();
					var ind=$(this).find('td:eq(10)').text()?$(this).find('td:eq(10)').text():$(this).find('td:eq(10) input').val();
					if(Number(amount)==0||Number(amount1)==0){
						numNull=1;//1数量为0
					}
					if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
						numNull=2;//2数量为空
					}
					if(ind==''||ind==null){
						indNull=1;//1日期为空
					}
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key ="not_add_empty_document" />！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('数量不是有效数字！');
				return;
			}
			if(Number(indNull)==1){//日期为空
				alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
				return;
			}
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount","supply.unit",
			            "price","totalAmt","amount1","supply.unit1","hoped","memo"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name)data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				var status=$(rows[i]).find('td:eq(0)').text();
				data["chkstodList["+i+"]."+"price"] = $(rows[i]).data('price') ? $(rows[i]).data('price'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
					if(value)
						data["chkstodList["+i+"]."+keys[j-1]] = value;
				}
			}
			//检查是否设置供应商
// 			var msg="ok";
//			$.post("<%=path%>/chkstom/checkSaveNewChk.do",data,function(data){
// 				var rs = eval('('+data+')');
// 				if(rs.pr==1){
// 					msg="no";
// 				}
// 			});
// 			if("no"==msg){
// 				alert("本仓位的虚拟供应商没设置！请检查！");
// 				return;
// 			}
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkstom/saveByAddOrUpdate.do?sta="+$("#sta").val(),data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					break;
				case 1:
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 3000
								});
					loadToolBar([true,true,true,true,false,true,true,true]);
					$("#sta").val("show");
					break;
				}
			});			 
		}


		//动态下拉列表框
		function findByDes(e){
	    	$('#firmDes').val($(e).val());
		}
	
		//清空页面
		function clearValue(){
			$(':input','#listForm')  
			 .not(':button, :submit, :reset, :hidden')  
			 .val('')  
			 .removeAttr('checked')  
			 .removeAttr('selected');  
			$("#sta").val("show");
		}
		
		//快捷键查找添加申购单模板
		function searchChkstodemo(){
			$('body').window({
				id: 'window_addChkstodemo',
				title: '申购模板',
				content: '<iframe id="addChkstodemoFrame" frameborder="0" src="<%=path%>/chkstodemo/addChkstodemo.do"></iframe>',
				width: '1100px',
				height: '420px',
				draggable: true,
				isModal: true,
				confirmClose: false,
				topBar: {
					items: [{
							text: '<fmt:message key="confirmation_modify"/>',
							title: '<fmt:message key="confirmation_modify"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(getFrame('addChkstodemoFrame')){
									window.frames["addChkstodemoFrame"].enterUpdate();
								}
							}
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
				}
			});
		}
		
		//模板追加数据
		function appendChkstodemo(data){
			if($('.table-body').find('tr').find('td:eq(1)').text()=='')$('.table-body').find('tr').remove();			
			var rs = eval('('+data+')');
			for ( var i in rs.chkstodemoList) {	
				var lastIndex=$("#tblGrid").find("tr:last td:first").text();
				var tr=$('<tr></tr>');	
				
				tr.append($('<td style="width:26px;" align="center"><span >'+Number(Number(lastIndex)+1)+'</span></td>'));
				tr.append($('<td ><span style="width:70px;">'+rs.chkstodemoList[i].supply.sp_code+'</span></td>'));
				tr.append($('<td ><span style="width:90px;">'+rs.chkstodemoList[i].supply.sp_name+'</span></td>'));
				tr.append($('<td ><span style="width:70px;">'+rs.chkstodemoList[i].supply.sp_desc+'</span></td>'));
				tr.append($('<td ><span style="width:70px;text-align: right;">'+rs.chkstodemoList[i].cnt+'</span></td>'));
				tr.append($('<td ><span style="width:70px;">'+rs.chkstodemoList[i].supply.unit+'</span></td>'));
				tr.append($('<td ><span style="width:70px;text-align: center;" price="'+rs.chkstodemoList[i].supply.sp_price+'">'+rs.chkstodemoList[i].supply.sp_price+'</span></td>'));
				tr.append($('<td ><span style="width:70px;text-align: center;">'+Number(rs.chkstodemoList[i].supply.sp_price*rs.chkstodemoList[i].cnt).toFixed(2)+'</span></td>'));
				tr.append($('<td ><span style="width:70px;text-align: right;">'+rs.chkstodemoList[i].cnt1+'</span></td>'));
				tr.append($('<td ><span style="width:55px;">'+rs.chkstodemoList[i].supply.unit1+'</span></td>'));
				tr.append($('<td ><span style="width:90px;">'+getInd()+'</span></td>'));
				tr.append($('<td ><span style="width:70px;">'+rs.chkstodemoList[i].memo+'</span></td>'));				
				$('#tblGrid').find('tbody').append(tr);
				tr.data("unitper",rs.chkstodemoList[i].unitper);
				tr.data("mincnt",rs.chkstodemoList[i].supply.mincnt);
				//$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
				$('#sum_amount').text(Number($('#sum_amount').text())+Number(rs.chkstodemoList[i].cnt));
				$('#sum_price').text(Number(Number($('#sum_price').text())+Number(rs.chkstodemoList[i].supply.sp_price)*Number(rs.chkstodemoList[i].cnt)).toFixed(2));
				editCells();
			}
			var total=$('.table-body').find('tr').length;
			$('#sum_num').text(total);
			//重新加载样式
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		    //关闭弹出窗口
			$('.close').click();
		}
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//获取系统时间
		function getInd(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate()+1;
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//双击打开详细
		function openChkstom(chkstoNo){
			window.location.replace("<%=path%>/chkstom/findChk.do?chkstoNo="+chkstoNo);
		}
		function validateByMincnt(index,row,data2){//最小申购量判断
			if(index=="4"){
				if(Number(data2.value) == 0){
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					row.find("input").focus();
				}else if(Number(data2.value)<Number(row.data("mincnt"))){
					alert('该物质最低申购数量为'+row.data("mincnt")+"!");
					row.find("input").focus();
				}
			}else if(index=="8"){
				if((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)<Number(row.data("mincnt"))){
					alert('该物质参考单位数量最小为'+row.data("mincnt")*Number(row.data("unitper"))+'!')
					row.find("input").focus();
				}
			}
		}
		function validator(index,row,data2){//输入框验证
			if(index=="4"){
				if(Number(data2.value) < 0){
					alert('数量不能为负数！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else {
					if(isNaN(Number(row.data("unitper")))){
						row.data("unitper",Number(data2.value)/data2.ovalue);
					}
					$('#sum_amount').text(Number($('#sum_amount').text())+Number(data2.value)-Number(data2.ovalue));//总数量
					row.find("input").data("ovalue",data2.value);
					var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(7)').text());
					row.find("td:eq(7) span").text((Number(data2.value)*Number(row.find("td:eq(6)").text())).toFixed(2));
					row.find("td:eq(8) span").text((Number(data2.value)*Number(row.data("unitper"))).toFixed(2));
					Sprice=Sprice+Number(row.find('td:eq(7)').text());//总金额
					$('#sum_price').text(Sprice.toFixed(2));//总金额
				}
			}else if(index=="8"){
				if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(8)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,8);
				}else{
					row.find("td:eq(4) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2));
				}
			}
		}
		</script>			
	</body>
</html>