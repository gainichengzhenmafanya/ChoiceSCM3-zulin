<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>inventory Info 盘点</title>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
        <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		</head>
	<body>
        <form id="grptypForm" name="grptypForm" action="<%=path%>/spCodeUse/findstorecargo.do?next=2"method="post">
               <div id="grptypWin" style="visibility: hidden;text-align: center;">
                   <div class="form-line" style="margin-left: 50px;margin-top: 20px;margin-bottom:20px;width: 300px">
                       <div class="form-label">
                           	<fmt:message key ="Daily_goods_category" />：
                       </div>
                       <div id="type" class="form-input">
                           <select class="select" id="grptyp" name="grptyp" style="width:133px">
								<c:forEach items="${grptypList}" var="grptyp" varStatus="status">
									<option
										<c:if test="${spCodeUse.grptyp==grptyp.code}"> 
									  	 	selected="selected"
										</c:if>  
									 value="${grptyp.code}">${grptyp.des}</option>
								</c:forEach>
							</select>
                           
                       </div>
                   </div>
                   <div style="text-align:center;padding:5px;">
                       <button onclick="closeWin()"><fmt:message key ="cancel" /></button>
                       <button onclick="nextWin()"><fmt:message key ="the_next_step" /></button>
                   </div>
               </div>
		</form>
        <form id="grpdatForm" name="grpdatForm" action="<%=path%>/spCodeUse/findstorecargo.do?next=3"method="post">
            <div id="grpdatWin" style="visibility: hidden;text-align: center;">
            	<input type="hidden" name="grptyp"  class="text" value="${spCodeUse.grptyp}"/>
                <div class="form-line" style="margin-left: 50px;margin-top: 20px;width: 300px">
                    <div class="form-label"><fmt:message key ="reported_date" />：</div>
                    <div class="form-input">
                        <input autocomplete="off" style="width:100px;" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${spCodeUse.bdat}" pattern="yyyy-MM-dd"/>" />
                    </div>
                </div>
                <div class="form-line" style="margin-left: 50px;width: 300px">
                    <div class="form-label"><fmt:message key ="arrival_date" />：</div>
                    <div class="form-input">
                        <input autocomplete="off" style="width:100px;" type="text" id="ddat" name="ddat" class="Wdate text" value="<fmt:formatDate value="${spCodeUse.ddat}" pattern="yyyy-MM-dd"/>" />
                    </div>
                </div>
                <div class="form-line" style="margin-left: 50px;width: 300px">
                    <div class="form-label"><fmt:message key ="misboh_DeclareGoodsGuide_nextArrival_date" />：</div>
                    <div class="form-input">
                        <input autocomplete="off" style="width:100px;" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${spCodeUse.edat}" pattern="yyyy-MM-dd"/>" />
                    </div>
                </div>
                <div style="text-align:center;padding:5px;">
                    <button onclick="datLast()" ><fmt:message key ="misboh_DeclareGoodsGuide_last" /></button>
                    <button onclick="datNext()"><fmt:message key ="misboh_DeclareGoodsGuide_next" /></button>
                </div>
            </div>
        </form>
        <form id="grp3Form" name="grp3Form" action="<%=path%>/spCodeUse/table.do"method="post">
            <input type="hidden" value="${spCodeUse.firm}" name="firm"/>
            <input type="hidden" name="grptyp"  class="text" value="${spCodeUse.grptyp}"/>
            <input type="hidden" name="bdat"  class="text" value="<fmt:formatDate value="${spCodeUse.bdat}" pattern="yyyy-MM-dd"/>"/>
            <input type="hidden" name="ddat"  class="text" value="<fmt:formatDate value="${spCodeUse.ddat}" pattern="yyyy-MM-dd"/>"/>
            <input type="hidden" name="edat"  class="text" value="<fmt:formatDate value="${spCodeUse.edat}" pattern="yyyy-MM-dd"/>"/>
            <div id="grp3Win" style="visibility: hidden;text-align: center;">
                <div style="width: 90%;">
                <div class="grid">
                    <div class="table-head">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <td><span style="width:120px;"><fmt:message key ="date" /></span></td>
                                <td><span style="width:60px;"><fmt:message key ="misboh_DeclareGoodsGuide_yg1" /></span></td>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="table-body">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <c:forEach var="pos" varStatus="step" items="${posSalePlanList}">
                                <tr>
                                    <td><span title="${pos.dat}" style="width:120px;"><fmt:formatDate value="${pos.dat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
                                    <td><span title="${pos.total}" style="width:60px;" class="myTotal">${pos.total}&nbsp;</span></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><span style="width:120px;"><fmt:message key ="total" />&nbsp;</span></td>
                                    <td>
                                        <span style="width:60px;" id="total"></span>
                                        <input type="hidden" id="totalInput" name="total" value=""/>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key ="safety_coefficient" />：</div>
                    <div class="form-input"><input name="safetystock" id="safetystock" value="1.0" onkeyup="verIsNaN(this)" /></div>
                </div>
                </div>
                <div style="text-align:center;padding:5px;">
                    <button onclick="threeLast()" ><fmt:message key ="the_last_step" /></button>
                    <button onclick="threeNext()"><fmt:message key ="the_next_step" /></button>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
        <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
        <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
        <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
        <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
        <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
        <script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
        <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
        <script type="text/javascript" src="<%=path%>/js/validate.js"></script>
        <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
                $('#bdat').bind('click',function(){
                    new WdatePicker();
                });
                $('#ddat').bind('click',function(){
                    new WdatePicker();
                });
                $('#edat').bind('click',function(){
                    new WdatePicker();
                });
                if('${status_pd}'=='N'){//判断是否盘点
                    var text="错误：今天没有盘点，请盘点后再继续！";
                    if('${status_psbb}'=='N'){//判断是否有下次到货日期
                        text+="\n错误：请去配送班表设置下次到货日期！";
                    }
                    confirm(text);
                    closeWin();
                }else{
                    if('${status_psbb}'=='N'){//判断是否有下次到货日期
                        confirm("请去配送班表设置下次到货日期！");
                        closeWin();
                    }
                    if('${status_yyyg}'=='N'){//判断是否有营业预估
                        confirm("请去计算下次到货日期前的营业预估数据！");
                        closeWin();
                    }
                    if('${status_qwyg}'=='N'){//判断是否有千人万元用量预估
                        confirm("请去计算千人万元用量预估！");
                        closeWin();
                    }
                    if('${next}'==1){//第一次进入界面
                        showWin("报货类别选择","grptypWin");
                    }else if('${next}'==2){//第二次进入界面
                        showWin("报货周期确认","grpdatWin");
                    }else if('${next}'==3){
                        $("body").window({
                            title:'报货营业额确认',
                            id:'Win_grp3Win',
                            height:450,
                            width:400,
                            content:$("#grp3Win").html(),
                            isModal:true,
                            minimizable:false,
                            maximizable:true,
                            closable:false,
                            closed:true
                        });
                        var total=0;
                        $('.myTotal').each(function(i){
                        	if(i>=$('.myTotal').length/2) return;
                        	total += Number($.trim($(this).text()));
                        });
                        $('tfoot').find("tr").find("td:eq(1)").find("span").text(total);
                        $('tfoot').find("tr").find("td:eq(1)").find("#totalInput").val(total);
                    };
                };
            });
            function verIsNaN(e){
                if(isNaN(e.value)){
                    alert("安全系数的值必须为数字！");
                    e.value="";
                }
                $("#safetystock").val(e.value);
            }
            function closeWin(){//关闭窗口
                invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
            }
            function nextWin(){//类别选择下一步
                $("#grptypForm").submit();
            }
            function datLast(){//周期选择 上一步
            	$("#grptypForm").attr("action","<%=path%>/spCodeUse/findstorecargo.do?next=1");
            	$("#grptypForm").submit();
            }
            function datNext(){//周期选择 下一步
                $("#grpdatForm").submit();
            }
            //---------------------三页面
            function threeLast(){
            	$("#grp3Form").attr("action","<%=path%>/spCodeUse/findstorecargo.do?next=2");
            	$("#grp3Form").submit();
            }
            function threeNext(){
            	if($('#safetystock').val()==null || $('#safetystock').val()==''){
            		alert("请输入安全系数！");
            		return;
            	}
            	$("#grp3Form").submit();
            }
            function showWin(tit,Id){
                $("body").window({
                    title:tit,
                    id:'Win_'+Id,
                    height:200,
                    width:400,
                    content:$("#"+Id).html(),
                    isModal:true,
                    minimizable:false,
                    maximizable:false,
                    closable:false,
                    closed:true
                });
            }
		</script>
	</body>
</html>