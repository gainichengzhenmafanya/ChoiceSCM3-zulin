<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="expense_claims"/>———报销单</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	
		<style type="text/css">
		.page{margin-bottom: 25px;}
		.bgBlue{background-color:lightblue;}
		</style>
	</head>
	<body>
		<div class="tool"></div>		
		<%--保存操作状态，判断是何种操作 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>		
		<form id="listForm" action="<%=path%>/chkpaym/addChkpaym.do" method="post">
				<div class="form-line" style="z-index:14">
					<div class="form-label"><fmt:message key="date"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" style="width:83px;" value="<fmt:formatDate value="${chkpaym.bdat}" pattern="yyyy-MM-dd" type="date"/>" readonly="readonly" onclick="date();"/><fmt:message key="to2"/><input type="text" id="edat" name="edat" class="Wdate text" style="width:84px;" value="<fmt:formatDate value="${chkpaym.edat}" pattern="yyyy-MM-dd" type="date"/>" readonly="readonly" onclick="date();"/></div>
					<div class="form-label"><fmt:message key="positions"/></div>
					<div class="form-input">
						<input type="text" class="text" id="positn_select" name="positn_select" style="width:40px;margin-top:6px;vertical-align:top"  value="${chkpaym.positn.code}"/>
						<select class="select" id="positn" name="positn.code" style="width:235px;margin-top:2px" onchange="findByDes(this);">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option 
									<c:if test="${positn.code==chkpaym.positn.code}"> selected="selected" </c:if> id="des" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
					</div>				
				</div>
				<div class="form-line" style="z-index:13">
					<div class="form-label"><fmt:message key="remark"/></div>
					<div class="form-input"><input type="text" id="memo" name="memo" value="${chkpaym.memo }" class="text" style="width:181px;"/></div>	
					<div class="form-label"><fmt:message key="category"/></div>
					<div class="form-input" style="width:183px;top:2px"><select id="typeCode2" name="typeCode2" url="<%=path%>/grpTyp/findAllGrp.do" style="width:181px;"></select></div>				
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input">
						<c:if test="${chkpaym.vouno!=null}"><c:out value="${chkpaym.vouno}"></c:out></c:if>
						<input type="hidden" id="vouno" name="vouno" value="${chkpaym.vouno }"/>
<%-- 						<input type="text" class="text" value="${chkpaym.vouno }" disabled="disabled"/> --%>
					</div>
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input">
						<c:if test="${chkpaym.chkno!=null}"><c:out value="${chkpaym.chkno}"></c:out></c:if>					
						<input type="hidden" id="chkno" name="chkno" value="${chkpaym.chkno }"/>
<%-- 						<input type="text" value="${chkpaym.chkno }" class="text" disabled="disabled"/> --%>
					</div>	
					<div class="form-label"><fmt:message key="make_orders"/></div>
					<div class="form-input">
						<c:if test="${chkpaym.madeby!=null}"><c:out value="${chkpaym.madeby}"></c:out></c:if>		
						<input type="hidden" id="madeby" name="madeby" value="${chkpaym.madeby }"/>
<%-- 						<input type="text" value="${chkpaym.madeby }" class="text" disabled="disabled"/> --%>
					</div>					
				</div>				
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 15px;">&nbsp;</span></td>
								<td><span style="width:90px;"><fmt:message key="category"/></span></td>
								<td><span style="width:70px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:90px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:70px;"><fmt:message key="storage_amount"/></span></td>
								<td><span style="width:90px;"><fmt:message key="audit_may_be_paid"/></span></td>
								<td><span style="width:70px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_typAmt" value="${0}"/>  <!-- 金额 -->
				<c:set var="sum_deliverAmt" value="${0}"/>  <!-- 入库 -->
				<c:set var="sum_deliverPay" value="${0}"/>  <!-- 支付 -->				
				<div class="table-body" style="height: 100%">
						<table cellspacing="0" cellpadding="0">
						<thead>
						<c:forEach var="chkpayd" items="${chkpaydList}" varStatus="status">
							<tr>
								<td style="width:26px;" align="center">${status.index+1}</td>
								<td style="width:100px;">${chkpayd.typDes }</td>
								<td style="width:80px;text-align: right;">${chkpayd.typAmt }</td>
								<td style="width:100px;">${chkpayd.deliverDes }</td>
								<td style="width:80px;text-align: right;">${chkpayd.deliverAmt }</td>
								<td style="width:100px;text-align: right;">${chkpayd.deliverPay }</td>
								<td style="width:80px;">${chkpayd.memo }</td>
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_typAmt" value="${sum_typAmt + chkpayd.typAmt}"/>  
							<c:set var="sum_deliverAmt" value="${sum_deliverAmt + chkpayd.deliverAmt}"/>
							<c:set var="sum_deliverPay" value="${sum_deliverPay + chkpayd.deliverPay}"/>  
						</c:forEach>
						</thead>
					</table>										
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:100px;background:#F1F1F1;"><fmt:message key="total"/><label id="sum_num">${sum_num}</label></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="amount"/><u><label id="sum_typAmt"><fmt:formatNumber value="${sum_typAmt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元</u></td>
							<td style="width:100px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="storage"/><u><label id="sum_deliverAmt"><fmt:formatNumber value="${sum_deliverAmt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元</u></td>
							<td style="width:100px;background:#F1F1F1;"><fmt:message key="pay"/><u><label id="sum_deliverPay"><fmt:formatNumber value="${sum_deliverPay}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元</u></td>
							<td style="width:80px;"></td>
						</tr>
					</thead>
				</table>
		   </div>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
		
		var validate;
		$(document).ready(function(){
			$("#positn_select").val($("#positn").val());
			$("#typeCode2").htmlUtils("select",[]);
			/*过滤*/
			$('#positn_select').bind('keyup',function(){
		          $("#positn").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 65: $('#autoId-button-102').click(); break;
	                case 80: $('#autoId-button-105').click(); break;
	                case 68: $('#autoId-button-103').click(); break;
	                case 69: $('#autoId-button-104').click(); break;
	                case 83: $('#autoId-button-106').click(); break;
	                case 67: $('#autoId-button-107').click(); break;
	            }
			}); 
		 	//回车换焦点start
		    var array = new Array();        
	 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
	 	    array = ['positn_select','positn','memo'];        
	 		//定义加载后定位在第一个输入框上          
	 		$('#'+array[0]).focus();            
	 		$('select,input[type="text"]').keydown(function(e) {                  
		 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
		 		var event = $.event.fix(e);                
		 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
		 		if (event.keyCode == 13) {                
		 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
		 			 
		 				$('#'+array[++index]).focus();
		 				if(index==3){
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),1);
		 				} 
		 		}
	 		});    
	 		
	 		document.getElementById("positn").onfocus=function(){
	 			this.size = this.length;
	 			$('#positn').css('height','100px');
	 			$('#positn').parent('.form-input').css('z-index','2');
	 		};
 			document.getElementById("positn").onblur = function(){
	 			this.size = 1;
	 			$('#positn').css('height','20px');
 			};
	 		$('#positn').bind('dblclick', function() {
					$('#positn_select').val($(this).val());
					$('#positn').blur();
			});
 		    //回车换焦点end

			//判断按钮的显示与隐藏
			if($("#sta").val() == "add"){
				loadToolBar([true,true,false,false,true,true]);
				editCells();
			}else if($("#sta").val() == "show"){
				loadToolBar([true,true,true,true,true,false]);
			}else{
				loadToolBar([true,true,false,false,false,false]);
				$('#positn_select').attr('disabled','disabled');
				$('#positn').attr('disabled','disabled');
				$('#memo').attr('disabled','disabled');
				$('#bdat').attr('disabled','disabled');
				$('#edat').attr('disabled','disabled');
			}
			$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色				
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'bdat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="startdate"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'edat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="enddate"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'vouno',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="document_number"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'chkno',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
				}]
			});
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),121);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法	
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									searchChkpaym();
								}
							}else{
								searchChkpaym();
							}
						}
					},{
						text: '<fmt:message key="insert" />(<u>A</u>)',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									addChkpaym();
								}
							}else{
								addChkpaym();
							}
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteChkpaym();
						}
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="edit"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','0px']
						},
						handler: function(){
							if($("#checby").val()!=null && $("#checby").val()!=""){
								alert('<fmt:message key="orders_audited_cannot_edited"/>！');
								/* showMessage({
											type: 'error',
											msg: '<fmt:message key="orders_audited_cannot_edited"/>！',
											speed: 1000
											}); */
								return false;
							}else if($("#chkno").val()==null || $("#chkno").val()==""){
								return false;
							}else if($("#sta").val()=="add"){
								return false;
							}else{
								$("#sta").val("edit");
								loadToolBar([true,true,false,true,true,true,true]);
								editCells();
							}
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){return;
							var bdat=$("#bdat").val();
							var edat=$("#edat").val();
							var chkno=$("#chkno").val();
							if(null!=chkno && ""!=chkno)
							{
								window.open ("<%=path%>/chkpaym/printChkpaym.do?chkno="+chkno+"&bdat="+bdat+"&edat="+edat,'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
							}
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: use[5],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){saveChkpaym();};
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
								}
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}
		//编辑表格
		function editCells(){
			$('#sum_num').text(Number($('#sum_num').text())+1);//总行数				
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:7,
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 1 || (row.find("td:eq(1)").text() == null || $.trim(row.find("td:eq(1)").text()) != '' ) ? true : false;
				}},
				widths:[26,100,80,100,80,100,80],
				colStyle:['','',{background:"#F1F1F1"},'',{background:"#F1F1F1"},{background:"#F1F1F1"},''],
				onEdit:$.noop,
				editable:[1,2,3,4,5,6],
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					$('#sum_typAmt').text(Number($('#sum_typAmt').text())-row.find('td:eq(2)').text());//总金额
					$('#sum_deliverAmt').text(Number(($('#sum_deliverAmt').text())-row.find('td:eq(4)').text()).toFixed(2));//总入库
					$('#sum_deliverPay').text(Number(($('#sum_deliverPay').text())-row.find('td:eq(5)').text()).toFixed(2));//总支付
				},
				onEnter:function(data){
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
				},
				cellAction:[{
					index:1,
					action:function(row,data){
						if((data.value) == '' || data.value==null){
							alert('<fmt:message key="category"/><fmt:message key="cannot_be_empty"/>！');
							/* showMessage({
										type: 'error',
										msg: '<fmt:message key="category"/><fmt:message key="cannot_be_empty"/>！',
										speed: 1000
							}); */
							$.fn.autoGrid.setCellEditable(row,1);
							return;
						}else{
							$.fn.autoGrid.setCellEditable(row,2);
						}
					}
				},{
					index:2,
					action:function(row,data){
						if(Number(data.value) == 0){
							alert('<fmt:message key="amount"/><fmt:message key="cannot_be_zero_or_empty"/>！');
							/* showMessage({
										type: 'error',
										msg: '<fmt:message key="amount"/><fmt:message key="cannot_be_zero_or_empty"/>！',
										speed: 1000
							}); */
							$.fn.autoGrid.setCellEditable(row,2);
							return;
						}else
							$('#sum_typAmt').text(Number($('#sum_typAmt').text())+Number(row.find("td:eq(2)").text())-data.ovalue);//总金额
							$.fn.autoGrid.setCellEditable(row,3);
					}
				},{
					index:3,
					action:function(row,data){
						$.fn.autoGrid.setCellEditable(row,4);
					}
				},{
					index:4,
					action:function(row,data){
						if(Number(data.value) == 0){
							alert('<fmt:message key="storage_amount"/><fmt:message key="cannot_be_zero_or_empty"/>！');
							/* showMessage({
										type: 'error',
										msg: '<fmt:message key="storage_amount"/><fmt:message key="cannot_be_zero_or_empty"/>！',
										speed: 1000
							}); */
							$.fn.autoGrid.setCellEditable(row,4);
							return;
						}else
							$('#sum_deliverAmt').text(Number($('#sum_deliverAmt').text())+Number(row.find("td:eq(4)").text())-data.ovalue);//总入库
							$.fn.autoGrid.setCellEditable(row,5);
					}
				},{
					index:5,
					action:function(row,data){
						if(Number(data.value) == 0){
							alert('<fmt:message key="audit_may_be_paid"/><fmt:message key="cannot_be_zero_or_empty"/>！');
							/* showMessage({
											type: 'error',
											msg: '<fmt:message key="audit_may_be_paid"/><fmt:message key="cannot_be_zero_or_empty"/>！',
											speed: 1000
							}); */
							$.fn.autoGrid.setCellEditable(row,5);
							return;
						}else
							$('#sum_deliverPay').text(Number($('#sum_deliverPay').text())+Number(row.find("td:eq(5)").text())-data.ovalue);//总数量
							$.fn.autoGrid.setCellEditable(row,6);
					}
				},{
					index:6,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),1);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}					
		//新建报销单
		function addChkpaym(){
			window.location.replace("<%=path%>/chkpaym/addChkpaym.do");
		}		
		//保存报销单
		function saveChkpaym(){
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){	
					isNull=1;
					//wangjie 2014年11月20日 09:11:33 （报销单填制，提示有问题，金额已输入不为0.）
					var typAmt=$(this).find('td:eq(2)').text()?$(this).find('td:eq(2)').text():$(this).find('td:eq(2) input').val();
					var deliverAmt=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					var deliverPay=$(this).find('td:eq(5)').text()?$(this).find('td:eq(5)').text():$(this).find('td:eq(5) input').val();
					if(Number(typAmt)==0||Number(deliverAmt)==0||Number(deliverPay)==0){
						isNull=2;
					}
				}
			});
			
			if(Number(isNull)==0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！！');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="empty_document_unallowed_please_select_supplies"/>！！',
					speed: 1000
					}); */
				return;
			}
			if(Number(isNull)==2){
				alert('<fmt:message key="amount_cannot_be_zero"/>！！');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="amount_cannot_be_zero"/>！！',
					speed: 1000
					}); */
				return;
			}
			var keys = ["typDes","typAmt","deliverDes","deliverAmt","deliverPay","memo"];
			var data = {};
			var i = 0;
			$("#listForm *").each(function(){
				var name = $(this).attr("name"); 
				if(name)data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
					if(value)
						data["chkpayd["+i+"]."+keys[j-1]] = value;
				}
			}
			
			//提交并返回值，判断执行状态
			$.post("<%=path%>/chkpaym/saveByAdd.do",data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs.pr)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					/* showMessage({
									type: 'error',
									msg: '<fmt:message key="save_fail"/>！',
									speed: 1000
									}); */
					break;
				case 1:
					showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000
									});
					loadToolBar([true,true,true,true,true,false]);
					$("#sta").val("show");
					break;
				}
			});			
		}		
		//日期控件
		function date(){
			new WdatePicker();
		}		
		//动态下拉列表框
		function findByDes(e){
			$('#positn_select').val($(e).val());
		}		
		//查找报销单
		function searchChkpaym(){
			$('body').window({
				id: 'window_searchChkpaym',
				title: '<fmt:message key="query_expense_claims"/>',
				content: '<iframe id="searchChkpaymFrame" frameborder="0" src="<%=path%>/chkpaym/searchByKey.do?init=init"></iframe>',
				width: 750,
				height: 430,
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="query_expense_claims"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(getFrame('searchChkpaymFrame')&&window.document.getElementById("searchChkpaymFrame").contentWindow.validate._submitValidate()){
									submitFrameForm('searchChkpaymFrame','SearchForm');
								}
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete_messages_manifest"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								window.frames["searchChkpaymFrame"].deleteChkpaym();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}		
		//修改报销单
		function updateChkpaym(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() ==1){
				var chkValue = checkboxList.filter(':checked').eq(0).val();
				$('body').window({
					title: '<fmt:message key="edit_messages_manifest"/>',
					content: '<iframe id="updateChkpaymFrame" frameborder="0" src="<%=path%>/chkpaym/update.do?chkno='+chkValue+'"></iframe>',
					width: 538,
					height: '215px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save_messages_manifest"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('updateChkpaymFrame')&&window.document.getElementById("updateChkpaymFrame").contentWindow.validate._submitValidate()){
										submitFrameForm('updateChkpaymFrame','updateChkpaymForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data"/>！');
					return ;
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
				return ;
			}
		}
		//删除报货单
		function deleteChkpaym(){
			var chkno2=$("#chkno").val();
			if($("#chkno").val()!=null && $("#chkno").val()!="" && $("#chkno").val()!=0){
				if(confirm('<fmt:message key="whether_delete_orders"/>？')){
					//提交并返回值，判断执行状态
					$.post("<%=path%>/chkpaym/deleteOneChkpaym.do?chkno2="+chkno2,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 0:
							alert('<fmt:message key="data_unsaved"/>！');
							break;
						case 1:
							alert('<fmt:message key="successful_deleted"/>！');
							window.location.replace("<%=path%>/chkpaym/table.do");
							break;
						}
					});
				}
			}
		}
		//清空页面
		function clearValue(){
			$(':input','#listForm')  
			 .not(':button, :submit, :reset, :hidden')  
			 .val('')  
			 .removeAttr('checked')  
			 .removeAttr('selected');  
			$("#sta").val("show");
		}			
	</script>
	</body>
</html>