<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_expense_claims"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			form .form-line .form-input , form .form-line .form-input input[type=text],form .form-line .form-input select{
				width: 100px;
			}
			form .form-line .form-label{
				width: 60px;
				margin-left: 25px;
			}
			form .form-line .form-label:first-child{
				margin-left:0px;
				width: 50px;
			}
		</style>
	</head>
	<body>
			<div class="tool"></div>
			<form id="SearchForm" action="<%=path%>/chkpaym/searchByKey.do" method="post">
				<div class="form-line">
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input" style="width: 200px;"><input  style="width: 200px;" type="text" id="chkno2" name="chkno2" class="text" value="${chkpaym.chkno }"/></div>
					<div class="form-label"><fmt:message key="document_number"/></div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${chkpaym.vouno }"/></div>
					<div class="form-label"><fmt:message key="purchase_positions"/></div>
					<div class="form-input">
						<input type="text" id="positn_select" name="positn_select" style="width:40px;height:16px;margin-top:4px;vertical-align:top;" value="${chkpaym.positn.code}"/>
						<select class="select" id="positn" name="positn.code" style="width:80px" onchange="findByDes(this);">							
							<option id="" value=""><fmt:message key="please_select_positions"/></option>
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
									<c:if test="${positn.code==chkpaym.positn.code}"> selected="selected"</c:if> id="des" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/></div>
					<div class="form-input" style="width: 200px;"><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${chkpaym.bdat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'})"/>到<input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${chkpaym.edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'})"/></div>									
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkpaym.madeby }"/></div>
					<div class="form-label"><fmt:message key="orders_author"/></div>
					<div class="form-input"><input type="text" id="checby" name="checby" style="width: 130px;" class="text" value="${chkpaym.checby }"/></div>
				</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td style="width:80px;"><fmt:message key="orders_num"/></td>
								<td style="width:100px;"><fmt:message key="document_number"/></td>
								<td style="width:80px;"><fmt:message key="startdate"/></td>
								<td style="width:80px;"><fmt:message key="enddate"/></td>
								<td style="width:80px;"><fmt:message key="positions"/></td>
								<td style="width:100px;"><fmt:message key="orders_maker"/></td>
								<td style="width:80px;"><fmt:message key="orders_audit"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkpaym" items="${chkpaymList }" varStatus="status">
								<tr ondblclick="findChk(${chkpaym.chkno });">
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkpaym.chkno }" value="${chkpaym.chkno }"/></td>
									<td style="width:80px;">${chkpaym.chkno }</td>
									<td style="width:100px;">${chkpaym.vouno }</td>
									<td style="width:80px;"><fmt:formatDate value="${chkpaym.bdat}" pattern="yyyy-MM-dd" type="date"/></td>
									<td style="width:80px;"><fmt:formatDate value="${chkpaym.edat}" pattern="yyyy-MM-dd" type="date"/></td>
									<td style="width:80px;">${chkpaym.positn.des }</td>
									<td style="width:100px;">${chkpaym.madeby }</td>
									<td style="width:80px;">${chkpaym.checby }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			if($("#chkno2").val()==0)$("#chkno2").val("");
			$("#type").htmlUtils("select",[]);
			/*过滤*/
			$('#positn_select').bind('keyup',function(){
		          $("#positn").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkno2',
					validateType:['canNull','intege'],
					param:['T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		});
		//双击事件
		function findChk(chkno){
			parent.window.location.href = "<%=path%>/chkpaym/findChk.do?chkno1="+chkno;;
		}				
		//删除报销单
		function deleteChkpaym(){
			var data={};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm"/>？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					data['chkno2']=chkValue.join(",");
					//提交并返回值，判断执行状态
					$.post("<%=path%>/chkpaym/deleteMoreChkpaym.do",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 0:
							alert('<fmt:message key="data_unsaved"/>！');
							break;
						case 1:
							alert('<fmt:message key="successful_deleted"/>！');
							var action="<%=path%>/chkpaym/searchByKey.do";
							$('#SearchForm').attr('action',action);
							$('#SearchForm').submit();
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
				return ;
			}
		}			
		//动态下拉列表框
		function findByDes(e){
			$('#positn_select').val($(e).val());
		}
		</script>				
	</body>
</html>