<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>						
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/supplyForecast/table.do" method="post">
		    <%-- <input type="hidden" id="bytyp" name="bytyp" value="${bytyp}"/> --%>
		    <input type="hidden" id="level" name="level" value="${level}"/>
		    <input type="hidden" id="code" name="code" value="${code}"/>
		    <input type="hidden" id="delcode" name="delcode" />
	    	<input type="hidden" id="deldes" name="deldes" />
		    <input type="hidden" id="checkboxList" name="checkboxList" />
		    <input type="hidden" id="type" name="type" value="${type}"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:100px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:170px;"><fmt:message key="name" /></span></td>
								<td><span style="width:90px;"><fmt:message key ="Daily_goods_category" /></span></td>
								<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />A</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />B</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />C</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />D</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />E</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />F</span></td>
								<td><span style="width:40px;"><fmt:message key="Coefficient" />G</span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" varStatus="status" items="${supplyList}">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${supply.sp_code}" value="${supply.sp_code}"/></span>
									</td>
									<td><span title="${supply.sp_code}" style="width:100px;text-align: left;">${supply.sp_code}&nbsp;</span></td>
									<td><span title="${supply.sp_name}" style="width:170px;text-align: left;">${supply.sp_name}&nbsp;</span></td>
									<td><span title="${supply.typ_eas}" style="width:90px;text-align: left;">${supply.typ_eas}&nbsp;</span></td>
									<td><span title="${supply.sp_desc}" style="width:90px;text-align: left;">${supply.sp_desc}&nbsp;</span></td>
									<td><span title="${supply.unit}" style="width:50px;text-align: left;">${supply.unit}&nbsp;</span></td>
									<td><span title="${supply.ratioA}" style="width:40px;text-align: left;">${supply.ratioA}&nbsp;</span></td>
									<td><span title="${supply.ratioB}" style="width:40px;text-align: left;">${supply.ratioB}&nbsp;</span></td>
									<td><span title="${supply.ratioC}" style="width:40px;text-align: left;">${supply.ratioC}&nbsp;</span></td>
									<td><span title="${supply.ratioD}" style="width:40px;text-align: left;">${supply.ratioD}&nbsp;</span></td>
									<td><span title="${supply.ratioE}" style="width:40px;text-align: left;">${supply.ratioE}&nbsp;</span></td>
									<td><span title="${supply.ratioF}" style="width:40px;text-align: left;">${supply.ratioF}&nbsp;</span></td>
									<td><span title="${supply.ratioG}" style="width:40px;text-align: left;">${supply.ratioG}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name="orderBy" id="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
			<input type="hidden" name="orderDes" id="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="supplies_abbreviations" />：</td>
							<td><input type="text" id="sp_init" name="sp_init" class="text" onkeyup="ajaxSearch('sp_init')" style="text-transform:uppercase;" value="${supply.sp_init}" /></td>
							<td class="c-left"><fmt:message key="supplies_no" />：</td>
							<td><input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${supply.sp_code}" /></td>
							<td class="c-left"><fmt:message key="supplies_name" />：</td>
							<td><input type="text" id="sp_name" name="sp_name" class="text" onkeyup="ajaxSearch('sp_name')" value="${supply.sp_name}" /></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			var t;
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
				if (event.keyCode == 38 ||event.keyCode == 40){	
					return; //上下 时不执行
				} 
				if (key!='sp_name') {
					 window.clearTimeout(t); 
					   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
				}
			}
			$(document).ready(function(){
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//排序start
				var array = new Array();      
				array = ['sp_code','sp_name','sp_desc', 'unit','positndes' , 'deliverdes','positn1','abc'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							title: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								 $('.search-div').slideToggle(100);
								 var t = $('#sp_init').val();
								 $('#sp_init').focus().val(t);
							}
						},"-",{
							text: '<fmt:message key="Modified_prediction_coefficient" />',
							title: '<fmt:message key="Modified_prediction_coefficient" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								selectABC();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							}
						}]
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 

				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//获取已经选中的物资
				function getCheckList(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					alert(checkboxList.filter(':checked').size());
					return checkboxList.filter(':checked').size();
				}
				//点击事件
				function checktim_btn(obj){
					if(window.parent.$("#bytyp").val() == 'y'){
						alert('<fmt:message key="Currently_all_material_categories_have_been_selected_not_to_choose_the_material_alone" />！');
						return false;
					}
					return true;
				}
				var index = 0;
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				    	 $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				    	 index -= 1;
				    	 window.parent.$("#selectedsupply").val(index);
				     } else {
				    	 if(checktim_btn($(this).removeClass("bgBlue").find(":checkbox"))){
				         	$(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				    	 } else {
				    		$(this).addClass("bgBlue").find(":checkbox").attr("checked", false);
				    	 }
				    	 index += 1;
				    	 window.parent.$("#selectedsupply").val(index);
				     }
				 });
			});

			function selectABC() {
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 0 && 'y'!=parent.$('#bytyp').val()){
					alert('<fmt:message key ="Please_choose_at_least_one_material_or_choose_to_modify_by_category" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				path="<%=path%>/supplyForecast/selectABC.do?spcodeList="+chkValue.join(',')+"&code1="+$('#code').val()+"&level="+$('#level').val()+"&bytyp="+parent.$('#bytyp').val();
				customWindow = $('body').window({
					id: 'window_selectPositn1',
					title: '<fmt:message key="Modified_prediction_coefficient" />',
					content: '<iframe id="supplyPositn1Frame" frameborder="0" src="'+path+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_shelf_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('supplyPositn1Frame')&&window.document.getElementById("supplyPositn1Frame").contentWindow.validate._submitValidate()){
										submitFrameForm('supplyPositn1Frame','tableForm');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
						
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>