<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:20px;margin-top:30px;">
			<form id="tableForm" method="post" action="<%=path %>/supplyForecast/saveABCByUpd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key ="Daily_goods_category" />：</div>
					<div class="form-input">
						<select id="typ_eas" name="typ_eas" style="width:134px;"> 
							<option value=""></option>
							<c:forEach items="${typEasList }" var="typEas">
								<c:if test="${supply.typ_eas==typEas.code}">
									<option value="${typEas.code }" selected="selected">${typEas.des }</option>
								</c:if>
								<c:if test="${supply.typ_eas!=typEas.code}">
									<option value="${typEas.code }">${typEas.des }</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />A：</div>
					<div class="form-input"><input type="text" id="ratioA" name="ratioA" class="text" value='${supply.ratioA }'/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />B：</div>
					<div class="form-input"><input type="text" id="ratioB" name="ratioB" class="text" value='${supply.ratioB }'/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />C：</div>
					<div class="form-input"><input type="text" id="ratioC" name="ratioC" class="text" value='${supply.ratioC }'/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />D：</div>
					<div class="form-input"><input type="text" id="ratioD" name="ratioD" class="text" value='${supply.ratioD }'/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />E：</div>
					<div class="form-input"><input type="text" id="ratioE" name="ratioE" class="text" value='${supply.ratioE }'/></div>
				</div>
				 <div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />F：</div>
					<div class="form-input"><input type="text" id="ratioF" name="ratioF" class="text" value='${supply.ratioF }'/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Coefficient" />G：</div>
					<div class="form-input"><input type="text" id="ratioG" name="ratioG" class="text" value='${supply.ratioG }'/></div>
				</div>
		    	<input type="hidden" id="spcodeList" name="spcodeList" value="${spcodeList}" />
		    	<input type="hidden" id="code1" name="code1" value="${code1}"/>
		    	<input type="hidden" id="level" name="level" value="${level}"/>
	    	</form>
    	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'ratioA',
						validateType:['num'],
						param:['F'],
						error:['not_valid_number！']
					},{
						type:'text',
						validateObj:'ratioB',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratioC',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratioD',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratioE',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratioF',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					},{
						type:'text',
						validateObj:'ratioG',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key ="not_valid_number" />！']
					}]
				});
			});
		</script>
	</body>
</html>