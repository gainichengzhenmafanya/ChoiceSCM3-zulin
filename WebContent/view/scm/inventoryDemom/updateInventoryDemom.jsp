<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>修改盘点模板模板</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/inventoryDemom/updateInventoryDemom.do" method="post">
			<div class="bj_head">
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="title"/>：</div>
					<div class="form-input">
						<input type="hidden" id="acct" name="acct" value="${inventoryDemom.acct }" />
						<input type="hidden" id="yearr" name="yearr" value="${inventoryDemom.yearr }" />
						<input type="hidden" id="chkstodemono" name="chkstodemono" value="${inventoryDemom.chkstodemono }" />
						<input type="text" id="title" name="title" class="text" value="${inventoryDemom.title }"/>
					</div>
					<div class="form-label"><fmt:message key="type"/>：</div>
					<div class="form-input" style="width:50px;">
						<select id="status" name="status" style="margin-top:3px;">
							<option value="daypan" <c:if test="${inventoryDemom.status=='daypan' }">selected="selected"</c:if>><fmt:message key="daypan" /></option>
							<option value="weekpan" <c:if test="${inventoryDemom.status=='weekpan' }">selected="selected"</c:if>><fmt:message key="weekpan" /></option>
						</select>
					</div>
				</div>
				<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="remark"/>：</div>				
					<div class="form-input">
						<input type="text" id="memo" name="memo" class="text" value="${inventoryDemom.memo }"/>
					</div>
					<div class="form-label" style="width:100px;"><fmt:message key="please_select_materials"/>:</div>
					<div class="form-input" style="width:220px;">
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="4"><fmt:message key="supplies"/></td>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:55px;"><fmt:message key ="cost_unit" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="inventoryDemod" items="${listInventoryDemod}" varStatus="status">
							<tr>
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${inventoryDemod.supply.sp_code }</span></td>
								<td><span style="width:90px;">${inventoryDemod.supply.sp_name }</span></td>
								<td><span style="width:70px;">${inventoryDemod.supply.sp_desc }</span></td>
								<td><span style="width:55px;">${inventoryDemod.supply.unit2 }</span></td>
								<td><span style="height:24px;width:40px;"><img style="height:21px;width:15px;" title="<fmt:message key="Move_a_line" />" src="../image/up.png" onclick="up(this)"/>&nbsp;&nbsp;<img style="height:21px;width:15px;" title="<fmt:message key="Move_down_one_line" />" src="../image/down.png" onclick="down(this)"/></span></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
		</form>	
		
		<form action='<%=path%>/supply/selectSupplyLeft.do' method='post' target='selectFrame' id="spform">
			<input type="hidden" id="defaultCode" name="defaultCode"/>	
			<input type="hidden" id="single" name="single" value="false"/>	
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/autoTableWithSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var validate;
		//工具栏
		$(document).ready(function(){
			editCells();
			loadToolBar();
			//按钮快捷键
			focus() ;//页面获得焦点
			$('#seachSupply').bind('click.custom',function(e){
				var defaultCode = "";
				$(".table-body").find("tr").each(function(){
					defaultCode += $(this).find("td:eq(1)").find('span').text()+",";
				});
				$('#defaultCode').val(defaultCode);
		 		if(!!!top.customWindow){
		 			top.customSupply('<fmt:message key="please_select_materials" />','',$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
					$("#spform").submit();
				}
			});
			function handler2(sp_code){
				var spcode = sp_code.split(',');
				var defaultCode = $('#defaultCode').val();//已存在数据
				if(spcode.length>0 ){
					for(var i=0; i<spcode.length ;i++){
						if(spcode[i]=='' || defaultCode.indexOf(spcode[i])!=-1){
							continue;
						}
				 		$.ajax({
							type: "POST",
							url: "<%=path%>/supply/findById.do",
							data: "sp_code="+spcode[i],
							dataType: "json",
							success:function(supply){
								if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''||$(".table-body").find("tr").size()<=0){
									$.fn.autoGrid.addRow();
								}
								$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code);
								$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
								$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
								$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supply.unit2);
							}
						});
					}
					
				}
		 	}
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
			 		if(window.event && window.event.keyCode == 120) { 
			 			window.event.keyCode = 505; 
			 		} 
			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
		 		}
		 		if(e.altKey ==false)return;
			}); 
	 		
			//判断按钮的显示与隐藏
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'title',
					validateType:['canNull','maxLength'],
					param:['F','10'],
					error:['<fmt:message key ="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'memo',
					validateType:['maxLength'],
					param:['20'],
					error:['<fmt:message key ="the_maximum_length" />20']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//控制按钮显示
		function loadToolBar(){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="save" />',
						title: '<fmt:message key="save"/>',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									saveChkstom();
								}
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								parent.$('.close').click();
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}			
		//编辑表格
		function editCells()
		{
			if($("#sta").val() == "add"){
				$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:5,
				widths:[26,80,100,80,65],
				colStyle:['','',{background:"#F1F1F1"},'',''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[2],//能输入的位置
				onLastClick:function(row){
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(getName());
							return;
						}
					 	else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					function getName(){
						var name='';
						$.ajaxSetup({ 
							  async: false 
							});
						$.get("<%=path %>/supply/findById.do",
								{sp_code:$.trim(data.curobj.closest('td').prev().text())},
								function(data){
									name =  data.sp_name;
								});
						return name;
					};
				},
				cellAction:[{
					index:2,
					action:function(row){
						if(!row.find('td:eq(1)').text()){
							alert('<fmt:message key="Please_add_material" />！');
							return;
						}
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/supply/findTop.do';
						if(!isNaN(data.value))
							data['key'] = 'sp_code';
						else
							data['key'] = 'sp_init';
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '<fmt:message key="added_supplies_remind"/>！',
 								speed: 1000
 							});
						}
                        row.find("td:eq(1) span").text(data2.sp_code);
                        row.find("td:eq(2) span input").val(data2.sp_name).focus();
                        row.find("td:eq(3) span").text(data2.sp_desc);
                        row.find("td:eq(4) span").text(data2.unit2);
					}
				}]
			});
		}
		//保存添加
		function saveChkstom()
		{
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key="not_add_empty_document"/>！');
				return;
			}
			var data = {};
			data["acct"] = $('#acct').val();
			data["yearr"] = $('#yearr').val();
			data["chkstodemono"] = $('#chkstodemono').val();
			data["title"] = $('#title').val();
			data["status"] = $('#status').val();
			data["memo"] = $('#memo').val();
			var i = 0;
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				if($(rows[i]).find("td:eq(1)").text()){
					data["InventoryDemod["+i+"].sp_code"] = $.trim($(rows[i]).find("td:eq(1)").text());
				}
			}
			$.post("<%=path%>/inventoryDemom/updateInventoryDemom.do?sta="+$("#sta").val(),data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					break;
				case 1:
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 3000
								});
					loadToolBar([true,true,true,true,false,true,true,true]);
					$("#sta").val("show");
					parent.pageReload();
					parent.$('.close').click();
					break;
				}
			});			 
		}
	
        function up(obj) {
            var objParentTR = $(obj).closest('tr');
            var prevTR = objParentTR.prev();
            var nextTR = objParentTR.next();
            if (prevTR.length > 0) {
                var curNum = $.trim(objParentTR.find('td:first').find('span').html());
                curNum = Number(curNum);
                if(curNum==2){//第二条和第一条互换
                    var delcell = '<td name="deleCell" style="width:10px;border:0;cursor: pointer;" onclick="deleteRow(this)">'+objParentTR.find('td:last').html()+'</td>';
                    objParentTR.find('td:last').remove();
                    $(delcell).appendTo(prevTR);
                }
                prevTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
                objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum-1)+'</span>');

                if(nextTR.length==0){//最后一行
                    var addCell = objParentTR.find('td:last');
                    prevTR.append(addCell);
                }
                prevTR.insertAfter(objParentTR);
            }

        }
        function down(obj) {
            var objParentTR = $(obj).closest('tr');
            var nextTR = objParentTR.next();
            var nextnextTR = objParentTR.next().next();
            if (nextTR.length > 0) {
                var curNum = $.trim(objParentTR.find('td:first').find('span').html());
                curNum = Number(curNum);
                if(curNum==1){//第一条和第二条互换
                    var delcell = '<td name="deleCell" style="width:10px;border:0;cursor: pointer;" onclick="deleteRow(this)">'+nextTR.find('td:last').html()+'</td>';
                    nextTR.find('td:last').remove();
                    $(delcell).appendTo(objParentTR);
                }
                nextTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
                objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum+1)+'</span>');

                if(nextnextTR.length==0){//和最后一行互换
                    var addCell = nextTR.find('td:last');
                    nextTR.find('td:last').remove();
                    objParentTR.append(addCell);
                }
                nextTR.insertBefore(objParentTR);
            }
        }
        function deleteRow(obj){
            var tb = $(obj).closest('table');
            var rowH = $(obj).parent("tr").height();
            var tbH = tb.height();
            $(obj).parent("tr").nextAll("tr").each(function(){
                var curNum = Number($.trim($(this).children("td:first").text()));
                $(this).children("td:first").html('<span style="width:24px;padding:0px;">'+Number(curNum-1)+'</span>');
            });

            if($(obj).next().length!=0){
                var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;display: none;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
                tb.find('tr:last').prev().append(addCell);
            }
            $(obj).parent("tr").remove();
            tb.height(tbH-rowH);
            tb.closest('div').height(tb.height());
        };
		</script>			
	</body>
</html>