<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>盘点模板</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.page{margin-bottom: 25px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/inventoryDemom/listInventoryDemod.do" method="post">
			<div class="grid" id="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="4"><fmt:message key="supplies"/></td>
<%-- 								<td colspan="4"><fmt:message key="standard_unit"/></td> --%>
<%-- 								<td colspan="2"><fmt:message key="reference_unit"/></td> --%>
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:55px;"><fmt:message key ="cost_unit" /></span></td>
<%-- 								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td> --%>
<%-- 								<td><span style="width:70px;"><fmt:message key="unit"/></span></td> --%>
<!-- 								<td><span style="width:70px;">售价</span></td> -->
<!-- 								<td><span style="width:70px;">金额</span></td> -->
<%-- 								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								 --%>
<%-- 								<td><span style="width:55px;"><fmt:message key="unit"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="inventoryDemod" items="${listInventoryDemod}" varStatus="status">
								<tr data-mincnt="${inventoryDemod.supply.mincnt}" data-unitper="${inventoryDemod.supply.unitper }">
									<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
									<td><span style="width:70px;">${inventoryDemod.supply.sp_code }</span></td>
									<td><span style="width:90px;">${inventoryDemod.supply.sp_name }</span></td>
									<td><span style="width:70px;">${inventoryDemod.supply.sp_desc }</span></td>
									<td><span style="width:55px;">${inventoryDemod.supply.unit2 }</span></td>
<%-- 									<td><span style="width:70px;text-align: right;">${inventoryDemod.amount }</span></td> --%>
<%-- 									<td><span style="width:70px;">${inventoryDemod.supply.unit1 }</span></td> --%>
<%-- 									<td><span style="width:70px;text-align: center;" price="${inventoryDemod.supply.sp_price}">${inventoryDemod.supply.sp_price}</span></td> --%>
<%-- 									<td><span style="width:70px;text-align: center;">${inventoryDemod.supply.sp_price*inventoryDemod.amount}</span></td> --%>
<%-- 									<td><span style="width:70px;text-align: right;">${inventoryDemod.amount }</span></td> --%>
<%-- 									<td><span style="width:55px;">${inventoryDemod.supply.unit1 }</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				 --%>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
	
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		</script>
	</body>
</html>