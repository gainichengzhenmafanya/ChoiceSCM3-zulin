<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>报价快速加入</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.line{
			BORDER-LEFT-STYLE: none;
			BORDER-RIGHT-STYLE: none;
			BORDER-TOP-STYLE: none;
			width: 70px;
		}
	</style>
</head>
<body>
	<div class="bj_head">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="supply_units"/></div>
			<div class="form-input" style="width:170px;">
				<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly"/>
				<input type="hidden" id="deliverCode" name="deliverCode"/>
				<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
			</div>
			<div class="form-input">
				<input type="button" id="saveSp" disabled="disabled"  onclick="seachSupply()" value="<fmt:message key="insert"/><fmt:message key="supplies"/>"/>
				<input type="button" id="updateTim" disabled="disabled"  onclick="upTime()" value="<fmt:message key="scm_bulk_update"/><fmt:message key="time"/>"/>
				<input type="button" id="saveQuick" disabled="disabled"  onclick="saveSpprice()" value="<fmt:message key="confirm_the_added_to_the_new_quotation_and_review"/>"/>
				<input type="button" id="againSave" disabled="disabled" onclick="againSave()" value="<fmt:message key="lift_the_grey_continue_to_add"/>"/>
				<input type="text" id="sp_code" name="sp_code"/>
				<input type="button" id="searchQuick"  onclick="searchQuick()" value="<fmt:message key="retrieval"/>"/>
			</div>
		</div>
	</div>
	<div class="leftFrame" style="width: 60%">
		<iframe src="" style="overflow-x:scroll;" frameborder="0" name="sppriceFrame" id="sppriceFrame"></iframe>
	</div>
	<div style="display: inline-block;float: left;width: 40%;" class="mainFrame">
   		<iframe src="<%=path%>/sppriceQuick/searchAllPositn.do" style="overflow-x:scroll;" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
   	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		//供应单位
		$('#seachDeliver').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var offset = getOffset('deliverDes');
				top.cust('<fmt:message key ="please_select_suppliers" />',encodeURI('<%=path%>/deliver/selectOneDeliver.do?'),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull',saveSpOk);
			}
		});
		//激活 物质添加按钮
		saveSpOk=function(deliverCode){
			$("#saveSp").prop("disabled", false);
			$("#saveQuick").prop("disabled", false);
			$("#updateTim").prop("disabled", false);
			window.sppriceFrame.location = '<%=path%>/sppriceQuick/findSppriceByDeliverCode.do?code='+deliverCode;
		}
		seachSupply=function(){
			if(!!!top.customWindow){
				top.cust('<fmt:message key ="please_select_materials" />','<%=path%>/supply/addSupplyBatch.do',0,$('#sp_name'),$('#sp_code'),'850','550','isNull',addSp);	
			}
		};
		addSp=function(id){
			//如果没值就不往下走了
			if(id==undefined||$.trim(id)==''){
				return;
			}
			$.ajax({
				type : 'GET',
				contentType : 'application/json;charset=UTF-8',
				url : '<%=path%>/sppriceQuick/findById.do?sp_code='+id,
				dataType : 'json',
				success : function(list) {
					$("#saveQuick").prop("disabled", false);
					$('#sppriceFrame')[0].contentWindow.NewInput();
					var bdat=$("#sppriceFrame").contents().find("#updateBdat").val();
					var edat=$("#sppriceFrame").contents().find("#updateEdat").val();
					var codes = '';
					$('#sppriceFrame').contents().find('#sp_data').find('tr').each(function(e){
						codes += $(this).find('td:eq(1)').text()+",";
					});
					for ( var i = 0; i < list.length; i++) {
						if(codes.indexOf(list[i].sp_code) != -1 ){//已存在
							continue;
						}
						var sp_desc = "";
						if(list[i].sp_desc!=null){
							sp_desc =list[i].sp_desc;
						}
						$('#sppriceFrame').contents().find('#sp_data').append('<tr>'
							+'<td style="width:30px; text-align: center;">'
								+'<input type="checkbox" name="idList" id="chk_'+list[i].sp_code+'" value="'+list[i].sp_code+'"/>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_code">'+list[i].sp_code+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 100px;" id="sp_name">'+list[i].sp_name+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_desc">'+sp_desc+'</span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;padding: 0px;"><input type="text"  onfocus="this.select()" class="line" id="price" value="'+list[i].sp_price+'"/></span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;" id="unit1">'+list[i].unit+'</span>'
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+bdat+'" style="width: 95px;height: 15px;margin: 0px;" id="bdat" name="bdat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+edat+'" style="width: 95px;height: 15px;margin: 0px;" id="edat" name="edat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:60px; text-align: left;">'
								+'<span style="width: 60px;padding: 0px;"><input type="text"  onfocus="this.select()" class="line" id="sp_price" value="'+list[i].priceold+'"/></span>'
							+'</td>'
						+'</tr>');
					}
					$('#sppriceFrame')[0].contentWindow.NewInput();
				}
			});
		};
		upTime=function(){
			if($("#sppriceFrame").contents().find('#sp_data :checkbox').filter(':checked').size()>0){
				$('#sppriceFrame')[0].contentWindow.showTimWindow();
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Select_the_data_you_want_to_modify" />..！',
					speed: 2500
				});
			}
		};
		saveSpprice=function(){
			var Spprice={};
			var size=0;
			var NotNull=true;
			var sppriceList=$("#sppriceFrame").contents().find('#sp_data :checkbox');
			sppriceList.filter(':checked').each(function(index){
				var price=$(this).parent().parent().find("td:eq(4) input").val();//单价
				var bdat=$(this).parent().parent().find("td:eq(6) input").val();//开始时间
				var edat=$(this).parent().parent().find("td:eq(7) input").val();//结束时间
				Spprice["sppriceList["+index+"].sp_code"]=$(this).val();
				Spprice["sppriceList["+index+"].deliver"]=$("#deliverCode").val();
				Spprice["sppriceList["+index+"].price"]=price;//单价
				Spprice["sppriceList["+index+"].bdat"]=bdat;//开始时间
				Spprice["sppriceList["+index+"].edat"]=edat;//结束时间
				Spprice["sppriceList["+index+"].priceold"]=$(this).parent().parent().find("td:eq(8) input").val();//上期价格
				if(price==""||bdat==""||edat==""){
					NotNull=false;
					return;
				}
				var positnList=$("#mainFrame").contents().find('.table-body :checkbox');
				positnList.filter(':checked').each(function(index1){
					Spprice["sppriceList["+index+"].positnList["+index1+"].area"]=$(this).val();
					size=++index1;
				});
			});
			if(NotNull){
				if(size>0){
					$.ajax({
						url : '<%=path%>/sppriceQuick/saveSpprice.do',
						type:'POST',
						async:false,
						data:Spprice,
						success : function(date) {
							if(date=="success"){
								showMessage({
		  	  						type: 'success',
		  	  						msg: '<fmt:message key ="successful_added" />！',
		  	  						speed: 2500
		  	  					});
								$("#saveQuick").prop("disabled", true);
								$('#againSave').prop("disabled",false);
							}else if(date == "error"){
								showMessage({
		  	  						type: 'error',
		  	  						msg: '<fmt:message key ="fail_added" />！',
		  	  						speed: 2500
		  	  					});
							}else{
								alert(date);
								$("#saveQuick").prop("disabled", true);
								$('#againSave').prop("disabled",false);
							}
						}
					});
				}else{
					showMessage({
							type: 'error',
							msg: '<fmt:message key="Donot_forget_to_choose_the_material_and_the_store" />..！',
							speed: 2500
						});
				};
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Unit_price_and_time_can_not_be_empty" />..！',
					speed: 2500
				});
			};
		};
	});
	//解除灰色，继续添加
	function againSave(){
		$("#saveQuick").prop("disabled",false);
		$('#againSave').prop("disabled",true);
	}
	//检索物资
	function searchQuick(){
		clearSelection()
        var searchText =$('#sp_code').val();//获取你输入的关键字；
        if(""==searchText){
        	alert("<fmt:message key="Please_enter_the_material_encoding_or_material_name_and_then_retrieve" />！");
        	return;
        }
        var regExp = new RegExp(searchText, 'g');//创建正则表达式，g表示全局的，如果不用g，则查找到第一个就不会继续向下查找了；
        $("#sppriceFrame").contents().find('.searchText').each(function()//遍历文章；
        {
            var html = $(this).html();
            var newHtml = html.replace(regExp, '<span class="highlight">'+searchText+'</span>');//将找到的关键字替换，加上highlight属性；
            $(this).html(newHtml);//更新文章；
        });
	}
    function clearSelection()
    {
    	$("#sppriceFrame").contents().find('.searchText').each(function()//遍历
        {
            $(this).find('.highlight').each(function()//找到所有highlight属性的元素；
            {
                $(this).replaceWith($(this).html());//将他们的属性去掉；
            });
        });
    }
	</script>
</body>
</html>