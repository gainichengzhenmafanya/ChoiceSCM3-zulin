<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css"> 
		.line{
			BORDER-LEFT-STYLE: none;
			BORDER-RIGHT-STYLE: none;
			BORDER-TOP-STYLE: none;
			margin:0px;
 			width: 100%;
			height: 15px;
			text-align: right;
		}
		.highlight { background-color:yellow; }
		</style>
	</head>
	<body style="overflow-x:scroll; ">
		<div id="updateTimDiv" style="display: none;">
			<div id="TimDiv" style="padding-left: 18%; padding-top: 15%;">
				&nbsp;       <fmt:message key ="starttime" />    ：<input type="text"  id="updateBdat" name="bdat" class="Wdate text"  onclick="new WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'updateEdat\');}'})"/><br/>
				 &nbsp; &nbsp; <fmt:message key ="endtime" />    ：<input type="text"  id="updateEdat" name="edat" class="Wdate text"  onclick="new WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'updateBdat\');}'})"/><br/>
				<input type="button" style="left: 100px;position: absolute;top: 100px;" onclick="updateTim()" value='<fmt:message key="confirm" />' /><input type="button" style="left: 170px;position: absolute;top: 100px;" onclick="TimeClose()" value='<fmt:message key="cancel" />'/>
			</div>
		</div>
		<div class="grid"> 
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0" style="overflow: scroll;">
						<thead>
							<tr>
								<td style="width:30px;text-align: center;">
									<input type="checkbox" id="chkAll" />
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="supplies_code"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 100px;">
										<fmt:message key="supplies_name"/>
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key="supplies_specifications"/>
									</span>
								</td>
								<td style="width:40px;text-align: center;">
									<span style="width: 30px;">
										<fmt:message key="materials_unit_price"/>
									</span>
								</td>
								<td style="width:40px;text-align: center;">
									<span style="width: 40px;">
										<fmt:message key="unit"/>
									</span>
								</td>
								<td style="width:95px;text-align: center;">
									<span style="width: 85px;">
										<fmt:message key="starttime"/>
									</span>
								</td>
								<td style="width:95px;text-align: center;">
									<span style="width: 85px;">
										<fmt:message key="endtime"/>
									</span>
								</td>
								<td style="width:50px;text-align: center;">
									<span style="width: 50px;">
										<fmt:message key="previous_price"/>
									</span>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" style="overflow: scroll;">
						<tbody id="sp_data" style="overflow: scroll;">
						<c:forEach items="${sppriceList }" var="spprice">
							<tr>
								<td style="width:30px; text-align: center;">
									<input type="checkbox" name="idList" id="chk_${spprice.sp_code }" value="${spprice.sp_code }"/>
								</td>
							    <td style="width:80px; text-align: left;">
							     	<span style="width: 70px;" class="searchText"  id="sp_code">${spprice.sp_code }</span>
						     	</td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 100px;" class="searchText"  id="sp_name">${spprice.sp_name}</span>
						        </td>
						        <td style="width:80px; text-align: left;">
							        <span style="width: 70px;" id="sp_desc">${spprice.sp_desc}</span>
						        </td>
						        <td style="width:40px; text-align: right;">
							        <span style="width: 40px;padding: 0px;"><input type="text" style="text-align: right;" onfocus="this.select()" class="line" id="price" value="${spprice.price }"/></span>
						        </td>
						        <td style="width:40px; text-align: left;">
							        <span style="width: 40px;" id="unit1">${spprice.unit}</span>
						        </td>
						        <td style="width:95px; text-align: left;">
							        <span style="width: 95px;padding: 0px;"><input type="text"  style="width: 95px;height: 15px;margin: 0px;" id="bdat" name="bdat" value="<fmt:formatDate value="${spprice.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text"  onclick="WdatePicker()"/></span>
						        </td>
						        <td style="width:95px; text-align: left;">
							        <span style="width: 95px;padding: 0px;"><input type="text" style="width: 95px;height: 15px;margin: 0px;" id="edat" name="edat" value="<fmt:formatDate value="${spprice.edat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text"  onclick="WdatePicker()"/></span>
						        </td>
						        <td style="width:60px; text-align: right;">
							        <span style="width: 60px;padding: 0px;"><input type="text" style="text-align: right;"  onfocus="this.select()" class="line" id="priceold" value="${spprice.priceold}"/></span>
						        </td>
						      </tr>
					       </c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
// 				$("#updateBdat").htmlUtils("setDate","now");
// 				$("#updateEdat").htmlUtils("setDate","lastDay");
				setElementHeight('.grid',['.tool'],$(document.body),70);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				new tabTableInput("sp_data","text"); //input  上下左右移动
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: []
				});
				//点击checkbox改变
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", false);
				     }else{
				    	 $(this).attr("checked", true);
				     }
				 });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').bind("click", function (e) {
					var index=$(e.srcElement).closest('td').index();//点价格的时候不要触发  wjf
					if(index == 4 || index == 8){return;}
				     if ($(this).find(':checkbox')[0].checked) {
				    	 $(this).find(':checkbox').attr("checked", false);
				     }else{
				    	 $(this).find(':checkbox').attr("checked", true);
				     }
				 });
				showTimWindow=function(){
					$('body').window({   
						title:'<fmt:message key="Batch_modify_time"/>',
						width:300,   
						height:200,   
						draggable: true,
						maximizable:false,
						isModal: true,
						confirmClose:false,
						content:$('#updateTimDiv').html()
					});   
				};
				TimeClose=function(){
					$('.close').click();
				};
				updateTim=function(){
					var bdat=$(".window-content").find("#updateBdat").val();
					var edat=$(".window-content").find("#updateEdat").val();
					if(bdat>edat){
						alert('<fmt:message key ="Start_time_not_later_than_the_end_of_time" />!');
					}else{
						var sppriceList=$('#sp_data :checkbox');
						sppriceList.filter(':checked').each(function(index){
							if(bdat!=''){
								$(this).parent().parent().find("td:eq(6) input").val(bdat);
							}
							if(edat!=''){
								$(this).parent().parent().find("td:eq(7) input").val(edat);
							}
							$('.close').click();
						});
					}
				};
				NewInput=function(){
					tabTableInput("sp_data","text"); //input  上下左右移动
					loadGrid();//  自动计算滚动条的js方法
					changeTh();//拖动 改变table 中的td宽度 
					$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
					$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					//点击checkbox改变
					$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
					     if ($(this)[0].checked) {
					    	 $(this).attr("checked", false);
					     }else{
					    	 $(this).attr("checked", true);
					     }
					 });
					// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').bind("click", function () {
					     if ($(this).find(':checkbox')[0].checked) {
					    	 $(this).find(':checkbox').attr("checked", false);
					     }else{
					    	 $(this).find(':checkbox').attr("checked", true);
					     }
					 });
				}
				
			});
		</script>
	</body>
</html>