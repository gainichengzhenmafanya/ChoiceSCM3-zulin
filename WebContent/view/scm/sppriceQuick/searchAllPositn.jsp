<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css"> 
	 	.page{
			margin-bottom: 25px;
		}
		</style>
	</head>
	<body style="overflow-x:scroll; ">
		<form id="listForm" action="<%=path%>/sppriceQuick/searchAllPositn.do" method="post">
			<input type="hidden" id="mis" name="mis" value="${mis}"/>
			<input type="hidden" id="parentId" name="parentId" value=""/>
			<input type="hidden" id="parentName" name="parentName" value=""/>
			<table  cellspacing="0" cellpadding="0">
				<tr >
					<td align="right" style="width: 33%;"><fmt:message key="coding" />：<input type="text" id="code" name="code" class="text" value="${positn.code}" autocomplete="off" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} " style="width:80px;"/></td>
					<td align="right" style="width: 33%;"><fmt:message key="name" />：<input type="text" id="des" name="des" class="text" value="${positn.des}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "style="width:80px;"/></td>
					<td align="right" style="width: 33%;"><fmt:message key="abbreviation" />：<input type="text" id="init" name="init" class="text" value="${positn.init}" style="text-transform:uppercase;width:80px;" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
			    </tr>
			    <tr>
			    	<td align="right" style="width: 33%;"><fmt:message key="positions"/><fmt:message key="scm_type"/>：
						<select name="typ" id="typ"  class="select" style="width: 82px">
							<option value=""><fmt:message key ="all" /></option>
							<c:forEach var="positnTyp" items="${listPositnTyp }">
								<option  value="${positnTyp.typ }">${positnTyp.typ }</option>
							</c:forEach>
						</select>
					</td>
					<td align="right" style="width: 33%;"><fmt:message key="branche"/><fmt:message key="scm_type"/>：
						<select name="firmtyp" id="firmtyp"  class="select"  style="width: 82px">
							<option value=""><fmt:message key ="all" /></option>
							<c:forEach var="firmtyp" items="${firmtypList }">
								<option  value="${firmtyp.code }" 
									<c:if test="${firmtyp.code == positn.firmtyp }">selected="selected"</c:if>
								>${firmtyp.des }</option>
							</c:forEach>
						</select>
					</td>
					
					<td align="center" style="width: 33%;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" style="width:83px" id="search" name="search" value='<fmt:message key="select" />'/>
					</td>
			    </tr>
			</table>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:50px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:120px;"><fmt:message key="name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="type" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${positn.code}' />" value="<c:out value='${positn.code}' />"/></span>
									</td>

									<td><span style="width:50px;" title="${positn.code}"><c:out value="${positn.code}" />&nbsp;</span></td>
									<td><span style="width:120px;" title="${positn.des}"><c:out value="${positn.des}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${positn.typ}"><c:out value="${positn.typ}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
<%-- 			<page:page form="listForm" page="${pageobj}"></page:page> --%>
<%-- 			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" /> --%>
<%-- 	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" /> --%>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#typ").val('${typ}');
				setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$(".pgSearchInfo").hide();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				//点击checkbox改变
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", false);
				     }else{
				    	 $(this).attr("checked", true);
				     }
				 });
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').bind("click", function () {
				     if ($(this).find(':checkbox')[0].checked) {
				    	 $(this).find(':checkbox').attr("checked", false);
				     }else{
				    	 $(this).find(':checkbox').attr("checked", true);
				     }
				 });
				
				var m=$("#code").val();
				$("#code").val("").focus().val(m);
				var n=$("#des").val();
				$("#des").val("").focus().val(n);
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				var defaultCode = '${defaultCode}';
				var codes = defaultCode.split(',');
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						for(var i in codes){
							if(this.id.substr(4,this.id.length)==codes[i]){
								$(this).attr("checked", true);
							}
						}
					})	
				}
				
			});
		</script>
	</body>
</html>