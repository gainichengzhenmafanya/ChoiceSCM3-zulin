<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>站点网络监控</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		.datagrid-header tr{height:11px;}
		.datagrid-body-inner{
			width:100%;
			height:450px;
			overflow:auto;
		}
	</style>
  </head>	
  <body>
  	<div id="tool"></div>
	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/tele/teleUtil.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//生成工具栏
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			gridId:'datagrid',
  	 			toolbar:['search','exit'],
  	 			searchFun:initTable
  	 		});
  	 		initTable(true);
  	 	});
  	 	function initTable(flag){
  	 		if(!flag)firstLoad=false;
  	 		queryParams = getParam($("#queryForm"));
  	 		//生成表格
  	  		builtTable({
  	 			dataUrl:"<%=path%>/monit/getMonit.do",
  	 			title:'站点网络监控',
  	 			id:'datagrid',
  	 			pagination:false,
  	 			createHeader:function(data,head,frozenHead){
  	 				frozenHead.push([
  	 				{field:'code',width:100,rowspan:2,title:'门店编码',align:'center'},
  	 				{field:'des',width:120,rowspan:2,title:'门店名称',align:'center'},
  	 				{field:'ip',width:120,rowspan:2,title:'IP',align:'center'},
					{field:'ipPing',width:100,title:'是否连通',align:'center'},
					{field:'ipTime',width:100,title:'连接状况',align:'center'}]);
  	 			}
  	 		});
  	 	}
  	 </script>
  </body>
</html>
