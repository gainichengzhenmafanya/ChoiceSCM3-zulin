<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店月结</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		a.l-btn-plain{
			border:1px solid #7eabcd; 
			text-align: center;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/teShuCaoZuo/toFenDianYueJie.do" method="post">
			<div class="form-line">
				<div class="form-label">缩写：</div>
				<div class="form-input"><input type="text" class="text" id="init" name="init" value="${init}"/></div>
				<a id="add" href="#" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:yueJie();">月结</a>
				<a id="add" href="#" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:unCheckChkin();">未审核入库单</a>
			</div>	
			<div class="grid">
				<div class="table-head" >
					<table id="tblHead" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:90px;">物资编码</span></td>
								<td><span style="width:90px;">物资名称</span></td>
								<td><span style="width:70px;">盘点状态</span></td>
								<td><span style="width:70px;">当前月份</span></td>
								<c:forEach var="date" items="${dateList }" varStatus="status">
									<td><span style="width:70px;">${date }</span></td>
								</c:forEach>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%">
					<table cellspacing="0" cellpadding="0">
						<tbody>
<%-- 						<c:forEach var="firmMis" items="${firmMis }" varStatus="status"> --%>
							<tr>
								<td align="center"><span style="width:25px;">${status.index+1}</span></td>
								<td><span style="width:90px;"></span></td>
								<td><span style="width:90px;"></span></td>
								<td><span style="width:70px;"></span></td>
								<td><span style="width:70px;"></span></td>	
								<c:forEach var="date" items="${dateList }" varStatus="status">
									<td><span style="width:70px;text-align:center;"></span></td>
								</c:forEach>						
							</tr>
<%-- 						</c:forEach> --%>
						</tbody>
					</table>					
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			$("#grptyp").htmlUtils("select",[]);
			$("#grp").htmlUtils("select",[]);
			$("#typ").htmlUtils("select",[]);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'));	
				}
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
			
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//月结
		function yueJie(){
			//alert('1');
		}
		//未审核入库单
 		function unCheckChkin(){
 			//alert('2');
 		}
		</script>			
	</body>
</html>