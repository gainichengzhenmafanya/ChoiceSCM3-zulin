<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>清空数据</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	</head>
	<body>
	<div style="text-align:center;color:red;font-size:30px;margin-top:50px;">
		整个系统的数据将被删除，请谨慎操作之！！！
	</div>
	<input type="hidden" id="str" name="str" value="${str}"></input>
	<form id="listForm" action="<%=path%>/teShuCaoZuo/toClearData.do" method="post">
		<input type="hidden" id="action" name="action"/>
		<input type="hidden" id="type" name="type" value="${type}"/>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			if(null!=$('#str').val() && ""!=$('#str').val() && "1"==$('#str').val()){
				alert('清空数据成功！');
				invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
				return;
			}
			if("a"==$('#type').val()){
				if(confirm("确定清空数据库初始数据吗？")){
					if(confirm("再次警告！确定清空数据库初始数据吗？")){
						$('#listForm').submit();
					}else{
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
 				}else{
 					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
 				}
			}else{
				if(confirm("确定清空数据库操作数据吗？")){
					if(confirm("再次警告！确定清空数据库操作数据吗？")){
						$('#type').val('N');
						$('#listForm').submit();
					}else{
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
 				}else{
 					invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
 				}				
			}
		});
	</script>
	</body>
</html>