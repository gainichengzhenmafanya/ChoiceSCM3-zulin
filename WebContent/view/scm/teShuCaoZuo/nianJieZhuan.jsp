<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>年终结转</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		p span input{
			width: 90px;
		}
		a.l-btn-plain{
			border:1px solid #7eabcd; 
			text-align: center;
		}
		#loading{width:295px; height:22px;border-style:groove;text-align:center;}
		#loading div{height:22px;text-align:center;}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<div>
		<table cellpadding="0" cellspacing="1" border="0" style="marign-top: 6px; width: 98%" >
			<tr>
				<td colspan="3" >
					<fieldset style="margin-left:10px;margin-top:5px;">
						<span>
							<font color="blue" size="3">【<fmt:message key ="nianzhongjiezhuan1" />】&nbsp;</font><font color="red" size="3"><fmt:message key ="nianzhongjiezhuan2" />【${main.yearr }】年--->【${main.yearr+1 }】年</font>&nbsp;
							<button id="jieZhuan" class="easyui-linkbutton"><font style=" COLOR: blue; HEIGHT: 20px; WIDTH: 58px" ><fmt:message key ="nianzhongjiezhuan3" /></font></buttton>
						</span>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td rowspan="2">
					<fieldset style="width:300px;height:350px;margin-left:10px;text-align:center;">
						<legend style="text-align:left;">&lt;<fmt:message key ="nianzhongjiezhuan4" />[<font color="blue">${main.yearr }</font>]--<fmt:message key ="nianzhongjiezhuan6" />&gt;</legend>
						<p><span><fmt:message key ="January" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_1" name="bdat_1" class="Wdate text" value="<fmt:formatDate value="${main.bdat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat1\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_1" name="edat_1" class="Wdate text" value="<fmt:formatDate value="${main.edat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat1\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="February" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_2" name="bdat_2" class="Wdate text" value="<fmt:formatDate value="${main.bdat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat2\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_2" name="edat_2" class="Wdate text" value="<fmt:formatDate value="${main.edat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat2\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="March" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_3" name="bdat_3" class="Wdate text" value="<fmt:formatDate value="${main.bdat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat3\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_3" name="edat_3" class="Wdate text" value="<fmt:formatDate value="${main.edat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat3\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="April" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_4" name="bdat_4" class="Wdate text" value="<fmt:formatDate value="${main.bdat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat4\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_4" name="edat_4" class="Wdate text" value="<fmt:formatDate value="${main.edat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat4\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="May" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_5" name="bdat_5" class="Wdate text" value="<fmt:formatDate value="${main.bdat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat5\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_5" name="edat_5" class="Wdate text" value="<fmt:formatDate value="${main.edat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat5\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="June" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_6" name="bdat_6" class="Wdate text" value="<fmt:formatDate value="${main.bdat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat6\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_6" name="edat_6" class="Wdate text" value="<fmt:formatDate value="${main.edat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat6}\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="July" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_7" name="bdat_7" class="Wdate text" value="<fmt:formatDate value="${main.bdat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat7\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_7" name="edat_7" class="Wdate text" value="<fmt:formatDate value="${main.edat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat7}\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="August" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_8" name="bdat_8" class="Wdate text" value="<fmt:formatDate value="${main.bdat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat8\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_8" name="edat_8" class="Wdate text" value="<fmt:formatDate value="${main.edat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat8\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="September" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_9" name="bdat_11" class="Wdate text" value="<fmt:formatDate value="${main.bdat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat9\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_9" name="edat_11" class="Wdate text" value="<fmt:formatDate value="${main.edat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat9\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="October" />：&nbsp;&nbsp;</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_10" name="bdat_12" class="Wdate text" value="<fmt:formatDate value="${main.bdat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat10\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_10" name="edat_12" class="Wdate text" value="<fmt:formatDate value="${main.edat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat10\')}'});"/>
						</span></p>
						<p><span><fmt:message key ="November" />：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_11" name="bdat_11" class="Wdate text" value="<fmt:formatDate value="${main.bdat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat11}\')}'});"/>
							--
							<input type="text" autocomplete="off" id="edat_11" name="edat_11" class="Wdate text" value="<fmt:formatDate value="${main.edat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat11}\')}'});"/>
						</span></p>	
						<p><span><fmt:message key ="December" />：</span>
						<span>
							<input type="text" autocomplete="off" id="bdat_12" name="bdat_12" class="Wdate text" value="<fmt:formatDate value="${main.bdat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat12\')}'});"/>
							--
							<input type="text"  autocomplete="off" id="edat_12" name="edat_12" class="Wdate text" value="<fmt:formatDate value="${main.edat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'bdat12\')}'});"/>
						</span></p>											
					</fieldset>
				</td>
				<td rowspan="2">
					<form id="listForm" action="<%=path%>/teShuCaoZuo/jiezhuan.do" method="post">
						<fieldset style="width:300px;height:350px;text-align:center;">
						<legend style="text-align:left;">&lt;<fmt:message key ="nianzhongjiezhuan5" />[<font color="blue">${main.yearr+1 }</font>]--<fmt:message key ="nianzhongjiezhuan6" />&gt;</legend>
							<input type="hidden" id="acct" name="acct" value="${main.acct}"></input>
							<input type="hidden" id="yearr" name="yearr" value="${main.yearr+1}"></input>
							<p><span><fmt:message key ="January" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat1" name="bdat1" class="Wdate text" value="<fmt:formatDate value="${main.bdat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat1" name="edat1" class="Wdate text" value="<fmt:formatDate value="${main.edat1}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="February" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat2" name="bdat2" class="Wdate text" value="<fmt:formatDate value="${main.bdat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat2" name="edat2" class="Wdate text" value="<fmt:formatDate value="${main.edat2}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="March" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat3" name="bdat3" class="Wdate text" value="<fmt:formatDate value="${main.bdat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat3" name="edat3" class="Wdate text" value="<fmt:formatDate value="${main.edat3}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="April" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat4" name="bdat4" class="Wdate text" value="<fmt:formatDate value="${main.bdat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat4" name="edat4" class="Wdate text" value="<fmt:formatDate value="${main.edat4}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="May" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat5" name="bdat5" class="Wdate text" value="<fmt:formatDate value="${main.bdat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat5" name="edat5" class="Wdate text" value="<fmt:formatDate value="${main.edat5}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="June" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat6" name="bdat6" class="Wdate text" value="<fmt:formatDate value="${main.bdat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat6" name="edat6" class="Wdate text" value="<fmt:formatDate value="${main.edat6}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="July" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat7" name="bdat7" class="Wdate text" value="<fmt:formatDate value="${main.bdat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat7" name="edat7" class="Wdate text" value="<fmt:formatDate value="${main.edat7}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="August" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat8" name="bdat8" class="Wdate text" value="<fmt:formatDate value="${main.bdat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat8" name="edat8" class="Wdate text" value="<fmt:formatDate value="${main.edat8}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="September" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat9" name="bdat9" class="Wdate text" value="<fmt:formatDate value="${main.bdat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat9" name="edat9" class="Wdate text" value="<fmt:formatDate value="${main.edat9}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="October" />：&nbsp;&nbsp;</span>
							<span>
								<input type="text" autocomplete="off" id="bdat10" name="bdat10" class="Wdate text" value="<fmt:formatDate value="${main.bdat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat10" name="edat10" class="Wdate text" value="<fmt:formatDate value="${main.edat10}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="November" />：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat11" name="bdat11" class="Wdate text" value="<fmt:formatDate value="${main.bdat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat11" name="edat11" class="Wdate text" value="<fmt:formatDate value="${main.edat11}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>
							<p><span><fmt:message key ="December" />：</span>
							<span>
								<input type="text" autocomplete="off" id="bdat12" name="bdat12" class="Wdate text" value="<fmt:formatDate value="${main.bdat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 	--
							 	<input type="text" autocomplete="off" id="edat12" name="edat12" class="Wdate text" value="<fmt:formatDate value="${main.edat12}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker();"/>
							 </span></p>							 
						</fieldset>
					</form>
				</td>
				<td>
					<fieldset style="width:300px;height:270px;text-align:center;">
						<legend style="text-align:left;">&lt;<fmt:message key ="nianzhongjiezhuan7" />&gt;</legend>
						<iframe name="frame-textarea" style="width:290px;height:250Px" >
							<textarea id="listInfo" rows="10" cols="2" style="resize:none;" readonly="readonly"></textarea>
						</iframe>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset style="width:300px;height:75px;text-align:center;">
						<label style="color:red;font-size:20px;"><fmt:message key ="nianzhongjiezhuan7" />！！！</label>
						<div id="message"><font size="3">0%</font></div>
						<div id="loading"><div id="percent"></div></div>
					</fieldset>
				</td>
			</tr>
		</table>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		/* var progress_id = "loading";
		var str="监视信息\n";
		function SetProgress(progress) {
			if (progress) {
				$("#" + progress_id + " > div").css("width",String(progress) + "%"); //控制#loading div宽度
  				$("#" + progress_id + " > div").css("background-color","royalblue");
// 				$("#" + progress_id + " > div").html(String(progress) + "%"); //显示百分比
  				$("#message").html("<font size='3'>"+String(progress) + "%</font>");
			}
		}
		var i = 0;
		function doProgress() {
			if (i > 100) {
				$("#message").html("<font size='3' color='blue'>年终结转完成！</font>").fadeIn("slow");//加载完毕提示
				return;
			}
			if (i <= 100) {
				$('#listInfo').val(i);
				str+=$('#listInfo').val()+"\n";
				$('#listInfo').val(str);
				document.getElementById("listInfo").scrollTop=document.getElementById("listInfo").scrollHeight;
				setTimeout("doProgress()",100);
				SetProgress(i);
				i++;
			}
		} */
		
 		var bdat1 = $('#bdat_1').val();
		var edat1 = $('#edat_1').val();
		var bdat2 = $('#bdat_2').val();
		var edat2 = $('#edat_2').val();
		var bdat3 = $('#bdat_3').val();
		var edat3 = $('#edat_3').val();
		var bdat4 = $('#bdat_4').val();
		var edat4 = $('#edat_4').val();
		var bdat5 = $('#bdat_5').val();
		var edat5 = $('#edat_5').val();
		var bdat6 = $('#bdat_6').val();
		var edat6 = $('#edat_6').val();
		var bdat7 = $('#bdat_7').val();
		var edat7 = $('#edat_7').val();
		var bdat8 = $('#bdat_8').val();
		var edat8 = $('#edat_8').val();
		var bdat9 = $('#bdat_9').val();
		var edat9 = $('#edat_9').val();
		var bdat10 = $('#bdat_10').val();
		var edat10 = $('#edat_10').val();
		var bdat11 = $('#bdat_11').val();
		var edat11 = $('#edat_11').val();
		var bdat12 = $('#bdat_12').val();
		var edat12 = $('#edat_12').val();
		var str = [bdat1,edat1,bdat2,edat2,bdat3,edat3,bdat4,edat4,bdat5,edat5,bdat6,edat6,bdat7,edat7,bdat8,edat8,bdat9,edat9,bdat10,edat10,bdat11,edat11,bdat12,edat12];
		for(var i in str){
			str[i]=((parseInt(str[i].split('-')[0]))+1)+"-"+str[i].split('-')[1]+"-"+str[i].split('-')[2];			
		} 
		$('#bdat1').val(str[0]);
		$('#edat1').val(str[1]);
		$('#bdat2').val(str[2]);
		var year = parseInt(str[3].split('-')[0]);
		var edat2 = (0 == year%4 && (year%100 !=0 || year%400 == 0)) ? '29' : '28';
		$('#edat2').val(year + "-" + str[3].split('-')[1] + "-" +edat2);
		$('#bdat3').val(str[4]);
		$('#edat3').val(str[5]);
		$('#bdat4').val(str[6]);
		$('#edat4').val(str[7]);
		$('#bdat5').val(str[8]);
		$('#edat5').val(str[9]);
		$('#bdat6').val(str[10]);
		$('#edat6').val(str[11]);
		$('#bdat7').val(str[12]);
		$('#edat7').val(str[13]);
		$('#bdat8').val(str[14]);
		$('#edat8').val(str[15]);
		$('#bdat9').val(str[16]);
		$('#edat9').val(str[17]);
		$('#bdat10').val(str[18]);
		$('#edat10').val(str[19]);
		$('#bdat11').val(str[20]);
		$('#edat11').val(str[21]);
		$('#bdat12').val(str[22]);
		$('#edat12').val(str[23]);
		var edat = [$('#edat1').val().toString(),$('#edat2').val(),$('#edat3').val(),$('#edat4').val(),$('#edat5').val(),$('#edat6').val(),$('#edat7').val(),$('#edat8').val(),$('#edat9').val(),$('#edat10').val(),$('#edat11').val(),$('#edat12').val()];
		var bdat = [$('#bdat1').val().toString(),$('#bdat2').val(),$('#bdat3').val(),$('#bdat4').val(),$('#bdat5').val(),$('#bdat6').val(),$('#bdat7').val(),$('#bdat8').val(),$('#bdat9').val(),$('#bdat10').val(),$('#bdat11').val(),$('#bdat12').val()];
		var monthh = $("#monthh").val(); 
		$(document).ready(function(){
			$('#jieZhuan').bind('click',function(){
				/* for(var j=0;j<12;j++){
					if(edat[j]<bdat[j])
						alert("第"+j+"会计月的月末不能早于该会计月月初");
				} */
				if(monthh<13){
					alert('<fmt:message key ="nianzhongjiezhuan8" />!');	
				}else{
				 if(confirm('<fmt:message key ="nianzhongjiezhuan9" />！')){
					 $("#listForm").submit();
					 alert("<fmt:message key ="nianzhongjiezhuan10" />!");
				 }
				}
			});
			
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
/* 		function jieZhuan(){
			//$("#jieZhuan").attr("disabled","disabled");
			doProgress();
			} */
		});
		</script>			
	</body>
</html>
