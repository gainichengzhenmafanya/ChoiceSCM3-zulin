<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>查错信息列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui/jquery.multiselect.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui/assets/style.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/jquery-ui/assets/prettify.css" />

		<style type="text/css">
			.tool {
				position: relative;
				height: 27px;
			}
			
			.search-div .form-line .form-label {
				width: 10%;
			}
		</style>
		</head>
	<body style="height: 575px; padding: 0px; margin: 0px">
	<form action="<%=path%>/teShuCaoZuo/maincheck.do" id="listForm"	name="listForm" method="post">
		<div region="north" class="topPart" style="margin: 0px; padding: 0px">
			<div class="tool" style="height: 27px;"></div>
			<div class="data">
				<input type="hidden" id="action" value="${action}"/>
				<input type="hidden" id="spPositionSize" value="${spPositionSize}"/>
				<input type="hidden" id="deliverSize" value="${deliverSize}"/>
				<input type="hidden" id="positnEx1Size" value="${positnEx1Size}"/>
				<input type="hidden" id="ex1BomSize" value="${ex1BomSize}"/>
				<input type="hidden" id="ex1BomSpCodeSize" value="${ex1BomSpCodeSize}"/>
				<input type="hidden" id="itemBomSpCodeSize" value="${itemBomSpCodeSize}"/>
				<input type="hidden" id="mainPositnSize" value="${mainPositnSize}"/>
				<input type="hidden" id="positnDes1Size" value="${positnDes1Size}"/>
				<input type="hidden" id="deliverPositnSize" value="${deliverPositnSize}"/>
				<input type="hidden" id="holidaySize" value="${holidaySize}"/>
				<input type="hidden" id="positnSpcodeSize" value="${positnSpcodeSize}"/>
				<input type="hidden" id="SupplyAttributeSize" value="${SupplyAttributeSize}"/>
				<textarea id="listInfo" style="width:950px; height:230px; boder:0px;background-color: #E1E1E1;" readonly="readonly"></textarea>
				
			</div>
		</div>
		<div id="dataDiv" style="width: 1163px;height:300px;position: fixed; top: 260px;">
			<div class="easyui-tabs" fit="true" plain="true" id="tabs">
				<div title="<fmt:message key ="default_position" />">
					<div class="grid"  id="title1">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="supplies_code" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="default_position" /><fmt:message key ="coding" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="default_position" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:500px;border:1px;">
							<table cellspacing="0" cellpadding="0" id="spPositionList">
								<tbody>
								<c:forEach var="spPosition" items="${listSpPosition}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${spPosition.sp_code}">${spPosition.sp_code}&nbsp;</span></td>
										<td><span style="width:100px;" title="${spPosition.sp_name}">${spPosition.sp_name}&nbsp;</span></td>
										<td><span style="width:100px;" title="${spPosition.sp_position}">${spPosition.sp_position}&nbsp;</span></td>
										<td><span style="width:100px;" title="${spPosition.positndes}">${spPosition.positndes}&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="default_supplier" />">
					<div class="grid"  id="title2">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="supplies_code" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="default_supplier" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0" id="deliverList">
								<tbody>
								<c:forEach var="deliver" items="${listDeliver}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${deliver.sp_code}">${deliver.sp_code}&nbsp;</span></td>
										<td><span style="width:100px;" title="${deliver.sp_name}">${deliver.sp_name}&nbsp;</span></td>
										<td><span style="width:100px;" title="${deliver.deliverdes}">${deliver.deliverdes}&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="The_default" /> <fmt:message key ="processing_room" />" >
					<div class="grid" id="title3">
						<div class="table-head">
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="supplies_code" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="The_default" /> <fmt:message key ="processing_room" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0" id="positnEx1List">
								<tbody>
								<c:forEach var="positnEx1" items="${listPositnEx1}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${positnEx1.sp_code}"><c:out value="${positnEx1.sp_code}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnEx1.sp_name}"><c:out value="${positnEx1.sp_name}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnEx1.positnexdes}"><c:out value="${positnEx1.positnexdes}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="whether_semi" /> bom" >
					<div class="grid" id="title4">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="supplies_code" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_name" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0" id="ex1BomList">
								<tbody>
								<c:forEach var="ex1Bom" items="${listEx1Bom}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${ex1Bom.sp_code}"><c:out value="${ex1Bom.sp_code}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${ex1Bom.sp_name}"><c:out value="${ex1Bom.sp_name}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="whether_semi" /> bom <fmt:message key ="raw_material" />" >
					<div class="grid" id="title5">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="whether_semi" /> <fmt:message key ="supplies_name" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="ex1BomSpCode" items="${listEx1BomSpCode}" varStatus="status">
									<tr>
											<td><span style="width:100px;" title="${ex1BomSpCode.item}"><c:out value="${ex1BomSpCode.item}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="pubitem" /> bom <fmt:message key ="raw_material" />" >
					<div class="grid" id="title6">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="scm_pubitem_code" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_code" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="itemBomSpCode" items="${listItemBomSpCode}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${itemBomSpCode.item}"><c:out value="${itemBomSpCode.item}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${itemBomSpCode.sp_code}"><c:out value="${itemBomSpCode.sp_code}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="positions_information" />" >
					<div class="grid" id="title7">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="positions" /> <fmt:message key ="coding" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="positions" /> <fmt:message key ="name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="positions" /><fmt:message key ="for_short" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="area" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="psarea" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="brand" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="positnDes1" items="${listPositnDes1}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${positnDes1.code}"><c:out value="${positnDes1.code}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnDes1.des}"><c:out value="${positnDes1.des}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnDes1.des1}"><c:out value="${positnDes1.des1}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnDes1.area}"><c:out value="${positnDes1.area}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnDes1.psarea}"><c:out value="${positnDes1.psarea}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnDes1.mod2}"><c:out value="${positnDes1.mod2}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="positions" /> <fmt:message key ="scscgongyingshang" />" >
					<div class="grid" id="title8">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="positions" /> <fmt:message key ="coding" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="positions" /> <fmt:message key ="coding" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="deliverPositn" items="${listDeliverPositn}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${deliverPositn.code}"><c:out value="${deliverPositn.code}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${deliverPositn.des}"><c:out value="${deliverPositn.des}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="<fmt:message key ="material_properties" />" >
					<div class="grid" id="title8">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="branches_name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="virtual" /><fmt:message key ="supplies_name" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplies_name" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="positnSpcode" items="${listPositnSpcode}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${positnSpcode.sp_position}"><c:out value="${positnSpcode.sp_position}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnSpcode.sp_name_x}"><c:out value="${positnSpcode.sp_name_x}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnSpcode.sp_name}"><c:out value="${positnSpcode.sp_name}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				
				<!-- add by js at 20160328 新增物资属性检查  -->
				<div title="<fmt:message key ="supplyattr" />" >
					<div class="grid" id="title8">
						<div class="table-head" style="height: 20px">
							<table cellspacing="0" cellpadding="0" style="text-align: center;">
								<thead>
									<tr>
										<td><span style="width: 100px;"><fmt:message key ="shouyewuzibm" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="shouyewuzimc" /></span></td>
										<td><span style="width: 100px;"><fmt:message key ="supplyattr" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table cellspacing="0" cellpadding="0">
								<tbody>
								<c:forEach var="positnSpcode" items="${listSupplyAttribute}" varStatus="status">
									<tr>
										<td><span style="width:100px;" title="${positnSpcode.sp_code}"><c:out value="${positnSpcode.sp_code}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnSpcode.sp_name}"><c:out value="${positnSpcode.sp_name}" />&nbsp;</span></td>
										<td><span style="width:100px;" title="${positnSpcode.attribute}"><c:out value="${positnSpcode.attribute}" />&nbsp;</span></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				
				
				
				
			</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-ui/prettify.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery-ui/jquery.multiselect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			function pageReload(){
				$('#listForm').submit();
			}
			$(document).ready(function(){
				var tr;
				//调节页面各部分的高度
				$('.topPart').attr('style','height: 260px;margin: 0px;');
				$('.data').attr('style','height: 228px;margin: 0px;padding: 0px;');
				$('#listForm').attr('style','height: 228px;padding: 0px');
				$('.dataDiv').attr('style','height: 282px; width: 100%;');
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				loadGrid();//  自动计算滚动条的js方法
				setElementHeight('.grid',['.tool'],$(document.body),10);	//计算.grid的高度
				$('.table-body').attr('style','height: 500px;');
				if ($('#action').val()=='') {
					doProgress();
				}
				function doProgress() {
					if ($('#spPositionSize').val()==0) {
						str=$('#listInfo').val();
						str+='1.<fmt:message key="Default_position_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='1.<fmt:message key="Default_vendor_exception_check_out" />'+$('#spPositionSize').val()+'<fmt:message key="Default_the_position_is_not_correct" />';
						$('#listInfo').val(str);
					}
					if ($('#deliverSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='2.<fmt:message key="Default_vendor_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='2.<fmt:message key="Default_vendor_exception_check_out" />'+$('#deliverSize').val()+'<fmt:message key="The_default_vendor_settings_are_not_correct" />';
						$('#listInfo').val(str);
					}
					if ($('#positnEx1Size').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='3.<fmt:message key="Processing_products_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='3.<fmt:message key="Processing_product_exception_check_out" />'+$('#positnEx1Size').val()+'<fmt:message key="A_record_set_up_a_half_finished_but_not_set_up_the_processing_position" />';
						$('#listInfo').val(str);
					}
					if ($('#ex1BomSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='4.<fmt:message key="BOM_normal_processing" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='4.<fmt:message key="Processing_product_BOM_exception_a_total_of_check_out" />'+$('#ex1BomSize').val()+'<fmt:message key="A_record_set_up_a_half_finished_product_but_no_set_BOM" />';
						$('#listInfo').val(str);
					}
					if ($('#ex1BomSpCodeSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='5.<fmt:message key="Processing_BOM_raw_materials_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='5.<fmt:message key="Processing_BOM_raw_materials_exception_a_total_of_check_out" />'+$('#ex1BomSpCodeSize').val()+'<fmt:message key="Article_records_the_original_material_for_BOM_has_been_deleted" />';
						$('#listInfo').val(str);
					}
					if ($('#itemBomSpCodeSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='6.<fmt:message key="BOM_raw_materials_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='6.<fmt:message key="Dishes_BOM_raw_materials_exception_a_total_of_check_out" />'+$('#itemBomSpCodeSize').val()+'<fmt:message key="Article_records_the_original_material_for_BOM_has_been_deleted" />';
						$('#listInfo').val(str);
					}
					if ($('#mainPositnSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='7.<fmt:message key="The_main_library_direct_abnormal_no_direct_dial_set_main_base" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='7.<fmt:message key="The_main_library_normal_direct_dial" />';
						$('#listInfo').val(str);
					}
					if ($('#positnDes1Size').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='8.<fmt:message key="Warehouse_abbreviation_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='8.<fmt:message key="Warehouse_information_exception_there_is_no_position_to_set_the_name_area_distribution_area_or_brand_will_affect_the_use_of" />';
						$('#listInfo').val(str);
					}
					if ($('#deliverPositnSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='9.<fmt:message key="Warehouse_supplier_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='9.<fmt:message key="Warehouse_supplier_exception_there_is_no_definition_in_the_supplier_of_the_main_warehouse_or_process" />';
						$('#listInfo').val(str);
					}
					if ($('#holidaySize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='10.<fmt:message key="Holiday_exception_no_definition_of_holiday_the_need_to_synchronize_from_the_decision-making_system" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='10.<fmt:message key="Holiday_normal" />';
						$('#listInfo').val(str);
					}
					if ($('#positnSpcodeSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='11.<fmt:message key="Store_material_properties_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='11.<fmt:message key="Store_material_properties_there_are_more_than_one_store_under_the_same_material_belonging_to_the_same_virtual_material" />';
						$('#listInfo').val(str);
					}
					
					//add by js at 20160328
					//物资属性检查
					if ($('#SupplyAttributeSize').val()==0) {
						str=$('#listInfo').val()+"\n";
						str+='12.<fmt:message key="Store_material_properties_normal" />';
						$('#listInfo').val(str);
					}else{
						str=$('#listInfo').val()+"\n";
						str+='12.<fmt:message key="Supply_properties_notset_or_nothastyp" />';
						$('#listInfo').val(str);
					}
					
					
				}
				
			});
			
			//加载工具栏
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key ="start_check" />',
						title: '<fmt:message key="start_check" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key ="quit" />',
						title: '<fmt:message key ="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
		</script>
</body>
</html>