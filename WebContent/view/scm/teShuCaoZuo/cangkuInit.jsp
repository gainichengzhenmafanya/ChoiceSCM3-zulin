<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>仓库期初</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.search{
					margin-top:3px;
					cursor: pointer;
				}
				.page{
					margin-bottom: 25px;
				}
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
				form .form-line .form-label{
					width: 10%;
				}
				form .form-line .form-input{
					width: 20%;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>								
		</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" name="listForm" action="<%=path%>/teShuCaoZuo/ckInit.do"method="post">
		<!--status是否已盘点 y  n -->
		<%-- <input type="hidden" id="type" name="type" value="<c:out value="${type}" default="init"/>" /> --%>
		<input type="hidden" id="status" name="status" value="<c:out value="${status}" default="init"/>" />
		<input type="hidden" id="importError" name="importError" value="<c:out value="${importError}" default="init"/>" />
		<%-- <input type="hidden" id="positn_copy" name="positn_copy" value="${inventory.positn.code}"/> --%>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="positions" />：</div>
			<div class="form-input">
				<input type="text" id="positn_name"  name="positnNm" readonly="readonly" value="${positnNm}"/>
				<input type="hidden" id="positn" name="positnCd" value="${positnCd}"/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
			</div>	
			<div class="form-label"><fmt:message key ="supplies_category" />：</div>
			<div class="form-input">
				<input type="text" id="typDes" name="typDes" readonly="readonly" value="${typDes}"/>
				<input type="hidden" id="typCode" name="typCode" value="${typCode}"/>
				<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
		    </div>					
		</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 16px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key ="coding" /></span></td>
								<td rowspan="2"><span style="width:95px;"><fmt:message key ="name" /></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key ="specification" /></span></td>
								<td colspan="3"><span style="width:180px;"><fmt:message key ="standard_unit" /></span></td>
								<td colspan="2"><span style="width:100px;"><fmt:message key ="reference_unit" /></span></td>
							</tr>
							<tr>
								<td><span style="width:40px;"><fmt:message key ="unit" /></span></td>
								<td><span style="width:60px;"><fmt:message key ="quantity" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="amount" /></span></td>
								<td><span style="width:40px;"><fmt:message key ="unit" /></span></td>
								<td><span style="width:60px;"><fmt:message key ="quantity" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="positnSupply" items="${positnSupplyList}" varStatus="status">
								<tr data-unitper="${positnSupply.unitper}">
									<td class="num" ><span style="width:16px;">${status.index+1}</span></td>
									<td><span title="${positnSupply.sp_code}" style="width:80px;" data-sp_name="${positnSupply.sp_name}">${positnSupply.sp_code}&nbsp;</span></td>
									<td><span title="${positnSupply.sp_name}" style="width:95px;">${positnSupply.sp_name}&nbsp;</span></td>
									<td><span title="${positnSupply.sp_desc}" style="width:70px;">${positnSupply.sp_desc}&nbsp;</span></td>
									<td><span title="${positnSupply.unit}" style="width:40px;">${positnSupply.unit}&nbsp;</span></td>
									<td><span title="${positnSupply.inc0}" style="width:60px;text-align: right;"><fmt:formatNumber value="${positnSupply.inc0}" pattern="##.##" minFractionDigits="2" />&nbsp;</span></td>
									<td><span title="${positnSupply.ina0}" style="width:80px;text-align: right;"><fmt:formatNumber value="${positnSupply.ina0}" pattern="##.##" minFractionDigits="2" />&nbsp;</span></td>
									<td><span title="${positnSupply.unit1}" style="width:40px;">${positnSupply.unit1}&nbsp;</span></td>
									<td><span title="${positnSupply.incu0}" style="width:60px;text-align: right;"><fmt:formatNumber value="${positnSupply.incu0}" pattern="##.##" minFractionDigits="2" />&nbsp;</span></td>
								</tr>
								<c:set var="sum_num" value="${status.index+1}"/>  
								<c:set var="sum_amount" value="${sum_amount + positnSupply.inc0}"/>  
								<c:set var="sum_totalamt" value="${sum_totalamt + positnSupply.ina0}"/>  
								
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="height: 10px">	
					<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
						<thead>
							<tr>
								<td style="width: 25px;">&nbsp;</td>
								<td style="width:80px;"><fmt:message key="total"/>:</td>
								<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
								<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" pattern="##.##" minFractionDigits="2" /></label>&nbsp;&nbsp;</u></td>
								<td style="width:240px;"><fmt:message key="total_amount"/>:
									<u>&nbsp;&nbsp;<label id="sum_totalamt"><fmt:formatNumber value="${sum_totalamt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></label>元&nbsp;&nbsp;</u>
								</td>
								 
							</tr>
						</thead>
					</table>
			   </div>	
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){				
				//	focus() ;//页面获得焦点
				function validator(weiz,row,data){
					row.find("input").data("ovalue",data.value);
					if(weiz=="5"){
						if(Number(data.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							row.find("td:eq(5)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,5);
						}else if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(5)").find('span').text(data.ovalue);
							$.fn.autoGrid.setCellEditable(row,5);
						}else {
							row.find("td:eq(5)").find('span').attr('title',data.value);
							row.find("td:eq(8) span").text((Number(data.value)*Number(row.data("unitper"))).attr('title',Number(data.value)*Number(row.data("unitper"))).toFixed(2));
						}
					}else if(weiz=="6"){
						if(Number(data.value) < 0){
							alert('<fmt:message key="number_cannot_be_negative"/>！');
							$.fn.autoGrid.setCellEditable(row,6);
						}else if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							$.fn.autoGrid.setCellEditable(row,6);
						}
						row.find("td:eq(6)").find('span').attr('title',data.value);
					}else if(weiz=="8"){
						if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							$.fn.autoGrid.setCellEditable(row,8);
						}else{
							row.find("td:eq(8)").find('span').attr('title',data.value);
							if(Number(row.data("unitper")) != 0){
								row.find("td:eq(5) span").text((Number(data.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data.value)/Number(row.data("unitper")));
							}
						}
					}
				}
			 	//键盘事件绑定
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
				 		if(window.event && window.event.keyCode == 120) { 
				 			window.event.keyCode = 505; 
				 			searchChkstodemo();
				 		} 
				 		if(window.event && window.event.keyCode == 505)
				 			window.event.returnValue=false;
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 85: $('#autoId-button-101').click(); break;//查看上传
		                case 70: $('#autoId-button-102').click(); break;//查询
		                case 65: $('#autoId-button-103').click(); break;//新建
		                case 69: $('#autoId-button-104').click(); break;//编辑
		                case 83: $('#autoId-button-105').click(); break;//保存
						case 68: $('#autoId-button-106').click(); break;//删除
		                case 80: $('#autoId-button-107').click(); break;//打印
		                case 67: $('#autoId-button-108').click(); break;//审核
		            }
				}); 
			 	var errMsg = $("#importError").val();
				if(errMsg == 'init' || '' == errMsg){
					console.log("错误消息提示： "+ errMsg);					
				}else{
					alert("错误消息提示： "+ errMsg);   //导入物资错误提示 lbh
				}
		 		//定义加载后定位在第一个输入框上     
		 		//$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 		$('positn_name').focus().select();    
			 	//判断工具栏状态
		 		var status = $("#status").val();
			 	if(status == 'select'){
			 		loadToolBar([true,false]);
			 	}else if(status == 'init'){
			 		loadToolBar([false,false]);
			 	}else if(status == 'import'){
			 		$("#status").val('select');
			 		loadToolBar([false,true]);
			 		editCells();
			 	}else{
			 		loadToolBar([true,true]);
			 	} 
				function loadToolBar(use){
					$('.tool').html('');
					var tool = $('.tool').toolbar({
						items: [{
								text: '<fmt:message key="select" />',
								title: '<fmt:message key="select"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){	
									if($('#positn').val()==''){
										alert('<fmt:message key="please_select_positions"/>！');
										return;
									}else{
										$("#status").val('select');
										$('#listForm').submit();
									}
								}
							},{
								text: '<fmt:message key ="update" />',
								title: '<fmt:message key ="update" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','0px']
								},
								handler: function(){
									$('.textInput').find('input').attr('disabled',false);
									loadToolBar([false,true]);
									$('.table-body').find('tr:first').find("td:eq(2)").focus();
									editCells();
								}
							},{
								text: '<fmt:message key ="save" />',
								title: '<fmt:message key ="save" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){ 
									resolve();
								}
							},{
								text: '<fmt:message key ="initial_recognition" />',
								title: '<fmt:message key ="initial_recognition" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'selete')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-20px']
								},
								handler: function(){
									initSpatch();
								}
							},'-',{
								text: '<fmt:message key="import" />',
								title: '<fmt:message key="import" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')}&&use[0],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									importCangkuInit();
								}
							},{
								text: '<fmt:message key="export" />',
								title: '<fmt:message key="export" />',
// 								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-20px']
								},
								handler: function(){
									exportCangkuInit();
								}
							},{
								text: '<fmt:message key="quit" />',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}
						]
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),48);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid',20);				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#textInput').each(function(i){
					$(this).find('input').bind('focus',function(e){
						$(this).focus().select();
					});
				});
				
				/*弹出树*/
				$('#seachPositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positn').val();
						var defaultName = $('#positn_name').val();
						var offset = getOffset('positn');
						
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn=1-2-4'+'&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
					}
				});

				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#typCode').val();
						var defaultName = $('#typDes').val();	
						var offset = getOffset('typCode');
						top.cust('<fmt:message key="please_select_category"/>','<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+defaultName,offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					}
				});
				
				new tabTableInput("table-body","text"); //input  上下左右移动
				$('.edit').bind('click', function(){
					var tdObj = $(this);
					$(this).html('<input id="editText" text="text" size="10" value="'+$(this).text()+'" name="name" onblur="onBlurMethod(this)"/>');
			    	$('#editText').focus();
				});
			});
				
		    function onBlurMethod(inputObj){
		    	$(inputObj).parent('span').text($(inputObj).val());
	    	}
		    
		    //确认初始
			function initSpatch(){
				var selected = {};
				if(confirm('<fmt:message key="Are_you_sure_you_want_to_confirm_the_initial_data_for_this_position_and_then_you_cant_change_it" />？')){
					$("#wait2").css('display','');
	  	 			$("#wait").css('display','');
					selected['positn'] = $('#positn').val();
					$.post('<%=path%>/teShuCaoZuo/initation.do',selected,function(data){
						$("#wait2").css('display','none');
		  	 			$("#wait").css('display','none');
					 	//弹出提示信息
					 	alert('<fmt:message key="has_been_initialized_can_not_be_modified"/>！');
// 						showMessage({
// 							type: 'success',
// 							msg: '已初始化完成,不能再修改！',
// 							speed: 2000
// 						});	
						$("#status").val('init');
						pageReload();
					});
				}
			}	
		    
			//保存修改数据
			function resolve(){
				var sp_num = '';
				var sp_name = '';
				var flag1 = true;//有数量没金额
				var flag2 = true;//有金额没数量
				$('#positn_name').focus().select();
				var selected = {};
				var result=0;
				if($('.grid').find('.table-body').find('tr').size() > 0&&$('.grid').find('.table-body').find('tr').find('td:eq(1)').text()!=""){
					selected['positn'] = $('#positn').val();
					var i = 0;
					$('.grid').find('.table-body').find('tr').each(function(){
						sp_num = $(this).find('td:eq(0) span').text()?$(this).find('td:eq(0) span').text():$(this).find('td:eq(0) span').find('input').val();
						sp_name = $(this).find('td:eq(2) span').text()?$(this).find('td:eq(2) span').text():$(this).find('td:eq(2) span').find('input').val();
						var inc0 = Number($(this).find('td:eq(5) span').attr('title'));//数量
						var ina0 = Number($(this).find('td:eq(6) span').attr('title'));
						if(inc0 != 0 && ina0 == 0){//有数量没金额
							flag1 = false;
							return false;
						}
						if(ina0 != 0 && inc0 == 0){//有金额没数量
							flag2 = false;
							return false;
						}
						if(inc0 == 0 && ina0 == 0){//都为0的过滤掉
							return true;
						}	
						if ($.trim($(this).find('td:eq(1)').text())!=null && $.trim($(this).find('td:eq(1)').text()) !='') {
							if(Number($(this).find('td:eq(5)').text()) < 0){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="smc_standard"/><fmt:message key="number_cannot_be_negative"/>！');
								result=1;
							}else if(isNaN($(this).find('td:eq(5)').text())){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="number_of_standards"/><fmt:message key="not_valid_number"/>！');
								result=1;
							}else if ($(this).find('td:eq(5)').text()=='') {
								if (isNaN($(this).find('td:eq(5)').find('span').find('input').val())) {
									alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="number_of_standards"/><fmt:message key="not_valid_number"/>！');
									result=1;
								}else {
									$(this).find('td:eq(5)').text($(this).find('td:eq(5)').find('span').find('input').val());
								}
							}
							
							if(Number($(this).find('td:eq(6)').text()) < 0){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="amount"/><fmt:message key="cannot_be_negative"/>！');
								result=1;
							}else if(isNaN($(this).find('td:eq(6)').text())){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="amount"/><fmt:message key="not_valid_number"/>！');
								result=1;
							}else if ($(this).find('td:eq(6)').text()=='') {
								if (isNaN($(this).find('td:eq(6)').find('span').find('input').val())) {
									alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="amount"/><fmt:message key="not_valid_number"/>！');
									result=1;
								}else {
									$(this).find('td:eq(6)').text($(this).find('td:eq(6)').find('span').find('input').val());
								}
							}
							
							if(Number($(this).find('td:eq(8)').text()) < 0){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="reference"/><fmt:message key="number_cannot_be_negative"/>！');
								result=1;
							}else if(isNaN($(this).find('td:eq(8)').text())){
								alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="reference_number"/><fmt:message key="not_valid_number"/>！');
								result=1;
							}else if ($(this).find('td:eq(8)').text()=='') {
								if (isNaN($(this).find('td:eq(8)').find('span').find('input').val())) {
									alert($.trim($(this).find('td:eq(2)').text())+'<fmt:message key="reference_number"/><fmt:message key="not_valid_number"/>！');
									result=1;
								}else {
									$(this).find('td:eq(8)').text($(this).find('td:eq(8)').find('span').find('input').val());
								}
							}
							selected['positnSupplyList['+i+'].sp_code'] = $.trim($(this).find('td:eq(1)').text());
							//update by jinshuai at 20160414
							if($(this).find('td:eq(5) span').attr('title')!=undefined){
								selected['positnSupplyList['+i+'].inc0'] = Number($(this).find('td:eq(5) span').attr('title'));
							}
							selected['positnSupplyList['+i+'].inc0'] = Number($(this).find('td:eq(5)').text());
							
							if($(this).find('td:eq(6) span').attr('title')!=undefined){
								selected['positnSupplyList['+i+'].ina0'] = Number($(this).find('td:eq(6) span').attr('title'));
							}
							selected['positnSupplyList['+i+'].ina0'] = Number($(this).find('td:eq(6)').text());
							
							if($(this).find('td:eq(8) span').attr('title')!=undefined){
								selected['positnSupplyList['+i+'].incu0'] = Number($(this).find('td:eq(8) span').attr('title'));
							}
							selected['positnSupplyList['+i+'].incu0'] = Number($(this).find('td:eq(8)').text());
							
							i++;
						}
					});
					if (result==1) {
						return;
					}
					if(!flag1){						
						alert('序号： '+ sp_num +', <fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="has_quantity_no_amount"/>！');
						return;
					}
					if(!flag2){
						alert('序号： '+ sp_num +', <fmt:message key="supplies"/>:['+sp_name+']<fmt:message key="has_amount_no_quantity"/>！');
						return;
					}
					$.post('<%=path%>/teShuCaoZuo/updateCkInit.do',selected,function(data){
					 	if(data=="success"){
							showMessage({
								type: 'success',
								msg: '<fmt:message key="data"/><fmt:message key="already"/><fmt:message key="save"/>！',
								speed: 1000
							});	
							pageReload('parent');
					 	}else{
					 		showMessage({
								type: 'error',
								msg: '<fmt:message key="data"/><fmt:message key="save"/><fmt:message key="failure"/>！',
								speed: 1000
							});	
					 	}
					});
				}else{
					alert('<fmt:message key="there_is_no"/><fmt:message key="data"/>！');
					return ;
				}
			}				
			
		    function pageReload(par){
		    	if('parent'==par){
		    		alert('<fmt:message key="operation_successful"/>！');
		    	}
		    	$('#listForm').submit();
		    }
			function getTotalSum(){//计算统计数据
				var sum_amount = 0; 
				var sum_totalamt = 0;
				$('.table-body').find('tr').each(function (){
					if($(this).find('td:eq(1)').text()!=''){//非空行
						var amount = $(this).find('td:eq(5)').text();
						if(!amount){
							amount = $(this).find('td:eq(5)').find("input:eq(0)").val();
						}
						sum_amount += Number(amount);
						var totalamt = $(this).find('td:eq(6)').text();
						if(!totalamt){
							totalamt = $(this).find('td:eq(6)').find("input:eq(0)").val();
						}
						sum_totalamt += Number(totalamt);
					}
				});
				$('#sum_num').text($(".table-body").find('tr').length);//总行数
				$('#sum_amount').text(sum_amount);//总数量
				$('#sum_totalamt').text(Number(sum_totalamt).toFixed(2));//总金额
			}
		  //编辑表格
			function editCells()
			{
				$('.table-body').find('tr:first').find("td:eq(2)").focus().select();
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:9,
					widths:[26,90,105,80,50,70,90,50,70],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"}],
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					onEdit:$.noop,
					editable:[2,5,6,8],
					onLastClick:function(row){
						getTotalSum();
					},
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(data.curobj.closest('td').prev().find('span').data('sp_name'));
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							data['url'] = '<%=path%>/supply/findTop.do';
							if(!isNaN(data.value))
								data['key'] = 'sp_code';
							else
								data['key'] = 'sp_init';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text().trim()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
								return;
							}
							$(row).data("price", data2.sp_price);
							row.find("td:eq(1) span").text(data2.sp_code).data('sp_name',data2.sp_name);
							row.find("td:eq(2) span input").val(data2.sp_name).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.unit);
							row.find("td:eq(5) span").text(0).attr('title',0).css("text-align","right");
							row.find("td:eq(6) span").text(0).attr('title',0).css("text-align","right");
							row.find("td:eq(7) span").text(data2.unit1);
							row.find("td:eq(8) span").text(0).attr('title',0).css("text-align","right");
							row.data("unitper",data2.unitper);
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}
							else {
								row.find("td:eq(5)").find('span').attr('title',data2.value);
								row.find("td:eq(8) span").text((Number(data2.value)*Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data2.value)*Number(row.data("unitper")));
								$.fn.autoGrid.setCellEditable(row,6);
								getTotalSum();
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(Number(data.value) < 0){
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								row.find("td:eq(6)").find('span').attr('title',data.value);
								if(!row.next().html())
									$.fn.autoGrid.addRow();
								$.fn.autoGrid.setCellEditable(row.next(),2);
								$('#sum_num').text($('.table-body').find('tr').size());//总行数
								
// 								$.fn.autoGrid.setCellEditable(row,8);
								getTotalSum();
// 								if(!row.next().html())
// 									$.fn.autoGrid.addRow();
// 								$.fn.autoGrid.setCellEditable(row.next(),2);
							}
						}
					},{
						index:8,
						action:function(row,data){
							if(isNaN(data.value)){
								alert('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(row,8);
							}else{
								row.find("td:eq(8)").find('span').attr('title',data.value);
								if(Number(row.data("unitper")) != 0){
									row.find("td:eq(5) span").text((Number(data.value)/Number(row.data("unitper"))).toFixed(2)).attr('title',Number(data.value)/Number(row.data("unitper")));
								}
								if(!row.next().html())
									$.fn.autoGrid.addRow();
								$.fn.autoGrid.setCellEditable(row.next(),2);
								$('#sum_num').text($('.table-body').find('tr').size());//总行数
							}
						}
					}]
				});
			}
		  //导出
		    function exportCangkuInit(){
		    	var positnCode=$('#positn').val();
				if(positnCode==''){
					alert('<fmt:message key="please_select_positions"/>！');
					return;
				}else{
					$("#wait2").val('NO');//不用等待加载
					$("#listForm").attr("action","<%=path%>/teShuCaoZuo/exportCangkuInit.do");
					$('#listForm').submit();
					$("#wait2 span").html("loading...");
					$("#listForm").attr("action","<%=path%>/teShuCaoZuo/ckInit.do");
					$("#wait2").val('');//等待加载还原
				}
		    }
		  
		  //导入
			  function importCangkuInit(){
				  var positn = $("#positn").val();
				  $('body').window({
						id: 'window_importInit',
						title: '<fmt:message key="import" /><fmt:message key="beginning_of_period" />',
						content: '<iframe id="importInit" frameborder="0" src="<%=path%>/teShuCaoZuo/importInit.do?positn='+positn+'"></iframe>',
						width: '400px',
						height: '200px',
						draggable: true,
						isModal: true
					});
			  }
		</script>
	</body>
</html>