<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>月末结账</title>
	</head>
	<body>
	<div style="text-align:center;font-size:30px;margin-top:50px;">
		<input type="button" id="search" style="width: 100px;" name="search" value='<fmt:message key ="end_of_turn" />'/>
	</div>
	<div style="text-align:center;color:red;font-size:30px;margin-top:50px;">
		<fmt:message key ="yuemojiezhuan1" />！！！
	</div>
	<form id="listForm" action="<%=path%>/teShuCaoZuo/toYueMoJieZhang.do" method="post">
		<input type="hidden" id="action" name="action" value="${action}"/>
		<input type="hidden" id="num" name="num" value="${num}"></input>
		<input type="hidden" id="isChk" name="isChk" value="${isChk}"></input>
		<input type="hidden" id="monthh" name="monthh" value="${monthh}"></input>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript">
		$('#search').bind('click',function(e){
// 			$('#listForm').submit();
// 		});
// 		$(document).ready(function(){
			if($('#isChk').val()!=''){
				alert($('#isChk').val());
				return;
			}
			if("init"==$('#action').val()){
				if(confirm("<fmt:message key ="yuemojiezhuan2" />？")){
					if(confirm("<fmt:message key ="yuemojiezhuan3" />？")){
						if($('#num').val()>0){
							alert('<fmt:message key ="yuemojiezhuan4" />....');
							return;
						}else if(0==$('#num').val() && 12>=$('#monthh').val()){
							alert('【'+$('#monthh').val()+'<fmt:message key ="yuemojiezhuan6" />！');	
							top.location="<%=path%>/login/loginOut.do";//添加自动跳转重新登录功能
						}else{
							alert('<fmt:message key ="yuemojiezhuan5" />！');
							return;
						}				
						$('#action').val("");
 						$('#listForm').submit();
					}
 				}
			}
		});
	</script>
	</body>
</html>