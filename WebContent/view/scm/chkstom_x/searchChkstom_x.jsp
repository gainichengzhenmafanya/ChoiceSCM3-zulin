<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_messages_manifest"/>_查询未审核</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">		
		.page{
			margin-bottom: 25px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label:first-child{
			margin-left: 0px;
			width: 40px;
		}
		form .form-line .form-label{
			width: 60px;
			margin-left: 20px;
		}
		form .form-line .form-input{
			width: 100px;
			display: inline-block;
		}
		form .form-line .form-input , form .form-line .form-input input[type=text]{
			width: 100px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>	
			<form id="SearchForm" action="<%=path%>/chkstom_x/searchByKey.do" method="post">
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="bdate" name="bMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.bMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'})"/></div>			
					<div class="form-label"><fmt:message key="orders_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${chkstom.chkstoNo}"/></div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input"><input type="text" id="sp_code" name="sp_code" class="text" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /></div>
					<div class="form-label"><fmt:message key="purchase_positions"/></div>
					<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" style="width:100px;" readonly="readonly" value="${firmDes}"/>
					<input type="hidden" id="positn" name="firm" value="${chkstom.firm}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label" style="width:60px;"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="edate" name="eMaded" class="Wdate text" value="<fmt:formatDate value="${chkstom.eMaded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'})"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${chkstom.madeby }"/></div>
					<div class="form-label"><fmt:message key="orders_author"/></div>
					<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="${chkstom.checby }"/></div>
				</div>
				<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
								<td style="width:130px;"><fmt:message key="reported_num"/></td>
								<td style="width:100px;"><fmt:message key="date"/></td>
								<td style="width:130px;"><fmt:message key="time"/></td>
								<td style="width:100px;"><fmt:message key="purchase_branche"/></td>
								<td style="width:80px;"><fmt:message key="total_amount1"/></td>
								<td style="width:100px;"><fmt:message key="orders_maker"/></td>
								<td style="width:80px;"><fmt:message key="orders_audit"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstom" items="${chkstomList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstom.chkstoNo }" value="${chkstom.chkstoNo }"/></span></td>									
									<td><span style="width:120px;">${chkstom.chkstoNo }</span></td>
									<td><span style="width:90px;"><fmt:formatDate value="${chkstom.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:120px;">${chkstom.madet}</span></td>
									<td><span style="width:90px;">${chkstom.positn.des }</span></td>
									<td><span style="width:70px;">${chkstom.totalAmt }</span></td>
									<td><span style="width:90px;">${chkstom.madeby }</span></td>
									<td><span style="width:70px;">${chkstom.checby }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="SearchForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			//双击事件
			$('.grid .table-body tr').live('dblclick',function(){
				var status = $(window.parent.document).find("#sta").val();
				if(status == 'add' || status == 'edit'){
					if(!confirm('<fmt:message key="reported_manifest_unsaved_remind"/>')){
						return;
					}
				}
				var chkstoNo = $(this).find("input:checkbox").val();
				chkstoNo ? window.parent.openChkstom(chkstoNo) && window.close() : alert("error!");
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: parent.$('.<fmt:message key="query_messages_manifest"/>').click(); break;
	                case 67: parent.$('.<fmt:message key="check_messages_manifest"/>').click(); break;
	                case 68: parent.$('.<fmt:message key="delete_messages_manifest"/>').click(); break;
	            }
			}); 
		 	//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
		 	/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['canNull','intege'],
					param:['T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			setElementHeight('.table-body',['.table-head'],'.grid');
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),77);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('chkstoNo');
					top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#firmDes'),$('#positn'),'740','400','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'));	
				}
			});
		});
		var tool = $('.tool').toolbar({
			items: [{
				text: '<fmt:message key="select" />(<u>F</u>)',
				title: '<fmt:message key="query_messages_manifest"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['0px','-40px']
				},
				handler: function(){
					if(validate._submitValidate()){
						$("#SearchForm").submit();
					}
				}
			},{
				text: '<fmt:message key="delete" />(<u>D</u>)',
				title: '<fmt:message key="delete_messages_manifest"/>',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-38px','0px']
				},
				handler: function(){
					deleteChkstom();
				}
			},{
				text: '<fmt:message key ="Converted_into_actual_material" />(<u>C</u>)',
				title: '<fmt:message key ="Converted_into_actual_material" />',
				useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-58px','-240px']
				},
				handler: function(){
					checkMoreChkstom();
				}								
			},{
				text: '<fmt:message key="quit"/>',
				title: '<fmt:message key="quit"/>',
				icon: {
					url: '<%=path%>/image/Button/op_owner.gif',
					position: ['-160px','-100px']
				},
				handler: function(){
					parent.$('.close').click();
				}
			}]
		});					

		//转换成实际物料
		function checkMoreChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				//提交并返回值，判断执行状态
				$.post("<%=path%>/chkstom_x/checkChkstom_x.do?chkstoNoIds="+chkValue.join(","),function(data){
					var rs = eval('('+data+')');
					switch(Number(rs.pr)){
					case -1:
						alert('<fmt:message key ="Conversion_failure" />！');
						break;
					case 1:
						$('#showChecby').text(rs.checby);
						showMessage({
							type: 'success',
							msg: "<fmt:message key ="Conversion_success" />！",
							speed: 3000
						});
						loadToolBar([true,true,true,false,false,false,true,false]);
						break;
					case 0:
						alert('<fmt:message key ="Unable_to_repeat_the_conversion" />！');
						break;
					}
				});
			}else{
				alert('<fmt:message key ="please_select_data_need_to_be_operated" />');
			}
		}
		//删除报货单主表
		function deleteChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm"/>？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$.post("<%=path%>/chkstom/deleteChkstom.do?chkstoNoIds="+chkValue.join(","),function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 0:
							alert('<fmt:message key="orders_unsaved_cannot_audited"/>！');
							break;
						case 1:
							alert('<fmt:message key="successful_deleted"/>！');
							$('#SearchForm').submit();
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
				return ;
			}
		}			
		
		//查看上传
		function searchUpload(){
			$('body').window({
				id: 'window_searchUpload',
				title: '<fmt:message key="view_upload"/>',
				content: '<iframe id="searchUploadFrame" frameborder="0" src="<%=path%>/chkstom/searchUpload.do"></iframe>',
				width: '950',
				height: '300px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="view"/>',
							title: '<fmt:message key="view_branch_upload_data"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(getFrame('searchUploadFrame')){
									submitFrameForm('searchUploadFrame','SearchForm');
								}
							}
						}
					]
				}
			});
		}
		function pageReload(){
	    	$('#SearchForm').submit();
		}
		</script>
	</body>
</html>