<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应商直配</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/deliverDirect/listDeliverDirect.do" id="listForm" name="listForm" method="post">
		<input type="hidden" name="deliverCode" id="deliverCode" value="${deliverCode}"></input>
		<input type="hidden"  id="positn_name"  name="positn_name" value=""/>
		<input type="hidden" id="positn" name="positn" value=""/>
		<input type="hidden" id="positnCodes" name="positnCodes" value="${positnCodes}"/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:40px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:200px;"><fmt:message key="name"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="deliverDirect" items="${listDeliverDirect}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" value="${deliverDirect.code}"/>
									</td>
									<td><span style="width:40px;"><c:out value="${deliverDirect.code}" />&nbsp;</span></td>
									<td><span style="width:200px;"><c:out value="${deliverDirect.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="insert" />',
							title: '<fmt:message key ="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var flag = false;
								var trArray = window.parent.$(".grid").find(".table-body").find('tr');
								for(var i=0;i<trArray.length;i++){
									if(trArray[i].className=='tr-select'){
										flag = true;
										break;
									}
								}
								
								if(flag){
									showDeliverDirect();
								}else{
									alert("<fmt:message key='please_select'/><fmt:message key='suppliers'/>！");
								}
								
								
							}
						},{
							text: '<fmt:message key="delete"/>',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteDeliverDirect();
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				function showDeliverDirect(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#positn").val(),
						single:false,
						tagName:'positn_name',
						tagId:'positn',
						title:'<fmt:message key="positions"/>',
						callBack:'saveDeliverDirect'
					});
				}
				
				function deleteDeliverDirect(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var deliverCode = $("#deliverCode").val();
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var codeValue=[];
							checkboxList.filter(':checked').each(function(){
								codeValue.push($(this).val());
							});
							var action = '<%=path%>/deliverDirect/delete.do?deliverCode='+deliverCode+'&positn='+codeValue.join(",");
							$('body').window({
								title: '<fmt:message key="Delete_branch" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
							window.setTimeout("pageReload()",1500);
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
			
			function saveDeliverDirect(){
				var positn = $("#positn").val();
				var deliverCode = $("#deliverCode").val();
				var positnCodes = $("#positnCodes").val();
				var positnNames = $("#positn_name").val();
				var positnNamesArray = positnNames.split(',');
				var positnMsg = "";
				var titleMsg = "";
				if(""!=positn && "---"!=deliverCode){
					var sta=0;
					var selected={};
					selected['firm']=positn;
					selected['deliver']=deliverCode;
					
					var action = '<%=path%>/deliverDirect/saveByAdd.do?deliverCode='+deliverCode+'&positn='+positn+'&positnCodes='+positnCodes;
					var positnArray1 = positn.split(',');
					var positnArray2 = positnCodes.split(',');
					for(var i in positnArray1){
						for(var j in positnArray2){
							if(positnArray1[i]==positnArray2[j]){
								positnMsg = positnMsg + positnNamesArray[i]+"、";
							}
						}
					}
					if(positnMsg ==''){
						titleMsg = '<fmt:message key="successful_added"/>';	
					}else{
						titleMsg = positnMsg.substr(0, positnMsg.length-1)+'<fmt:message key="already_exists" />！';
						alert(titleMsg);
						return;
					}
					$('body').window({
						title: titleMsg,
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
				window.setTimeout("pageReload()",1500);
			}
			
			//左侧列表点击事件
			function textSubmit(deliverCode){
				$('#listForm').attr('action','<%=path%>/deliverDirect/listDeliverDirect.do');
				$('#deliverCode').val(deliverCode);
				$('#listForm').submit();
			}
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>