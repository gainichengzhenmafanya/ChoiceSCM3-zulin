<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div class="leftFrame">
      	<div id="toolbar"></div>
      	<form id="listForm" action="<%=path%>/firmDeliver/list.do" method="post">
	      	<div class="form-line" style="margin-top: 2px;">
				<div class="form-label" style="width: 45px;"><fmt:message key ="branches_name" />：</div>
				  <div class="form-input" style="width: 80px;">
					<input type="text" name="searchInfo" id="searchInfo" style="width:70px;" value="${searchInfo }" style="width: 80px;" onkeydown="javascript: if(event.keyCode==13){$('#listForm').submit();} "/>
				</div>
				<div class="tool"></div>
			</div>
		</form><hr/>
	    <div class="treePanel1" style="overflow-x:auto;height: 89%;">
	    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key ="branche" />;method:changeURl("","")';
	          <c:forEach var="area" items="${areaList}" varStatus="status">
	         	tree.nodes['00000000000000000000000000000000_${area.code}'] 
    				= 'text:${area.code},${area.des};';
          		<c:forEach var="positn" items="${listPositn}">
          			<c:if test="${area.code == positn.area}">
	          			tree.nodes['${area.code}_${positn.code}'] 
		          			= 'text:${positn.code},${positn.des};method:changeURl("${positn.code}","${positn.des}")';
          			</c:if>
          		</c:forEach>
	          </c:forEach>
	          tree.setIconPath("<%=path%>/image/tree/none/");
	          document.write(tree.toString());
	          //tree.expandAll();//默认展开全部节点
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="<%=path%>/firmDeliver/listFirmDeliver.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//实现滚动条
			setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
			
			var offset = $(".tool").prev("div").offset();
			offset.left = $(".tool").prev("div").width() + offset.left + 10;
			$(".tool").button({
				text:'<fmt:message key="select" />',
				container:$(".tool"),
				position: {
					type: 'absolute',
					top: offset.top,	
					left: offset.left
				},
				handler:function(){
					$("#listForm").submit();
				}
			});
			//自动实现滚动条
		//	setElementHeight('.grid',['.tool'],$(document.body),40);	//计算.grid的高度
			//setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
		//	loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				var positnCode=$(this).find('td:eq(0)').find('span').text();
				window.mainFrame.textSubmit(positnCode);
			});
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		});// end $(document).ready();
		
		function changeURl(descode,descodeName){
			$("#mainFrame").attr("src","<%=path%>/firmDeliver/listFirmDeliver.do?positnCode="+descode+"&descodeName="+descodeName);
		}
		
		function turnTo(descode){
			$("#mainFrame").attr("src","<%=path%>/positn/listDetail.do?ucode="+descode);
		}
		
		//根据区域 获取改区域下的所有分店area
		function findByArea(areacode){
			$("#mainFrame").attr("src","<%=path%>/positn/listDetail.do?area="+areacode+"&typ=分店");
		}
		
	</script>

	</body>
</html>