<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店供应商</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/firmDeliver/listFirmDeliver.do" id="listForm" name="listForm" method="post">
		<input type="hidden" name="positnCode" id="positnCode" value="${positnCode}"></input>
		<input type="hidden"  id="deliver_name"  name="deliver_name" value=""/>
		<input type="hidden" id="deliver" name="deliver" value=""/>
		<input type="hidden" id="deliverCodes" name="deliverCodes" value="${deliverCodes}"/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:150px;"><fmt:message key="name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="abbreviation_code"/></span></td>
								<td><span style="width:50px;"><fmt:message key="person_in_charge"/></span></td>
								<td><span style="width:50px;"><fmt:message key="contact"/></span></td>
								<td><span style="width:80px;"><fmt:message key="tel"/></span></td>
								<td><span style="width:80px;"><fmt:message key="fax"/></span></td>
								<td><span style="width:80px;"><fmt:message key="email"/></span></td>
								<td><span style="width:100px;"><fmt:message key="www"/></span></td>
								<td><span style="width:80px;"><fmt:message key="billings"/></span></td>
								<td><span style="width:80px;"><fmt:message key="closed_amount"/></span></td>
								<td><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="firmDeliver" items="${listFirmDeliver}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" value="${firmDeliver.code}"/>
									</td>
									<td><span style="width:50px;" title="${firmDeliver.code}"><c:out value="${firmDeliver.code}" />&nbsp;</span></td>
									<td><span style="width:150px;" title="${firmDeliver.des}"><c:out value="${firmDeliver.des}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmDeliver.init}"><c:out value="${firmDeliver.init}" />&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmDeliver.person}"><c:out value="${firmDeliver.person}" />&nbsp;</span></td>
									<td><span style="width:50px;" title="${firmDeliver.person1}"><c:out value="${firmDeliver.person1}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmDeliver.tel1}"><c:out value="${firmDeliver.tel1}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmDeliver.tel}"><c:out value="${firmDeliver.tel}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmDeliver.mail}"><c:out value="${firmDeliver.mail}" />&nbsp;</span></td>
									<td><span style="width:100px;" title="${firmDeliver.addr}"><c:out value="${firmDeliver.addr}" />&nbsp;</span></td>
									<td><span style="width:80px;text-align:right;" title="${firmDeliver.amtc}"><c:out value="${firmDeliver.amtc}" />&nbsp;</span></td>
									<td><span style="width:80px;text-align:right;" title="${firmDeliver.amtd}"><c:out value="${firmDeliver.amtd}" />&nbsp;</span></td>
									<td><span style="width:100px;" title="${firmDeliver.memo}"><c:out value="${firmDeliver.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert"/>',
							title: '<fmt:message key="insert"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var flag = false;
// 								var trArray = window.parent.$(".grid").find(".table-body").find('tr');
// 								for(var i=0;i<trArray.length;i++){
// 									if(trArray[i].className=='tr-select'){
// 										flag = true;
// 										break;
// 									}
// 								}
								if($('#positnCode').val()!=''){
									showFirmDeliver();
								}else{
									alert("<fmt:message key="please_select_branche"/>！");
								}
								
								
							}
						},{
							text: '<fmt:message key="delete"/>',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteFirmDeliver();
							}
						},{
							text: '<fmt:message key="quit"/>',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				function showFirmDeliver(){
					if(!!!top.customWindow){
						var positnCode = $("#positnCode").val();
						if(null==positnCode || ''==positnCode || '---'==positnCode){
							alert('<fmt:message key="please_select_branche" />');
							return;
						}
						var defaultCode = $("#deliver").val();
						var defaultName = $("#deliver_name").val();
						var offset = getOffset('deliver_name');
						top.cust('<fmt:message key ="please_select_suppliers" />',encodeURI('<%=path%>/deliver/addDeliverBatch.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliver_name'),$('#deliver'),'900','570','isNull',saveFirmDeliver);
					}
					// if(!!!top.customWindow){
					//	var offset = getOffset('deliver_name');
					<%-- top.cust('供应商多选',encodeURI('<%=path%>/deliver/searchAllDeliver.do'),offset,$('#deliver_name'),$('#deliver'),'900','500','isNull',saveFirmDeliver); --%>
					//} //不分页时使用
				}
				
				function saveFirmDeliver(){
					var deliver = $('#deliver').val();
					if(deliver==undefined || ''==deliver){
						return;
					}
					var positnCode = $("#positnCode").val();
					var deliverCodes = $("#deliverCodes").val();
					var deliverNames = $("#deliver_name").val();
					var deliverNamesArray = deliverNames.split(',');
					var deliverMsg = "";
					var titleMsg = "";
					if(""!=deliver && "---"!=positnCode){
						var sta=0;
						var selected={};
						selected['firm']=positnCode;
						selected['deliver']=deliver;
						$.post("<%=path%>/firmDeliver/checkOne.do",selected,function(data){
// 							if("OK"!=data){//判断该供应商在该分店下是否已经设置过直配或者直发
// 								alert(data);
// 								sta=1;								
// 							}
						});
// 						if("1"==sta){
// 							return;
// 						}
						var action = '<%=path%>/firmDeliver/saveByAdd.do?positnCode='+positnCode+'&deliver='+deliver+'&deliverCodes='+deliverCodes;
						var deliverArray1 = deliver.split(',');
						var deliverArray2 = deliverCodes.split(',');
						for(var i in deliverArray1){
							for(var j in deliverArray2){
								if(deliverArray1[i]==deliverArray2[j]){
									deliverMsg = deliverMsg + deliverNamesArray[i]+"、";
								}
							}
						}
						if(deliverMsg ==''){
							titleMsg = '<fmt:message key="successful_added"/>';	
						}else{
							titleMsg = deliverMsg.substr(0, deliverMsg.length-1)+"<fmt:message key ="already_exists" />！";
							alert(titleMsg);
							return;
						}
						
						$('body').window({
							title: '<fmt:message key="Add_a_supplier" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '650px',
							height: '500px',
							draggable: true,
							isModal: true
						});
					}
					window.setTimeout("pageReload()",1500);
				}
				
				function deleteFirmDeliver(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var positnCode = $("#positnCode").val();
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var codeValue=[];
							checkboxList.filter(':checked').each(function(){
								codeValue.push($(this).val());
							});
							var action = '<%=path%>/firmDeliver/delete.do?positnCode='+positnCode+'&deliver='+codeValue.join(",");
							$('body').window({
								title: '<fmt:message key="Delete_supplier" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
							window.setTimeout("pageReload()",1500);
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
			
			//左侧列表点击事件
			function textSubmit(positnCode){
				$('#listForm').attr('action','<%=path%>/firmDeliver/listFirmDeliver.do');
				$('#positnCode').val(positnCode);
				$('#listForm').submit();
			}
			
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>