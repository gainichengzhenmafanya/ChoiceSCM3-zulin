<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>售价审核</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
			<style type="text/css">
				a.l-btn-plain{
					border:1px solid #7eabcd; 
				}
				.FColor{
					width: 100px;
				}
				.form-line .form-label{
					width: 100px;
				}
				.form-line .form-input{
					width: 100px;
				}
				.search{
					margin-top:-3px;
				}
				.page{margin-bottom: 25px;}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" name="listForm" method="post" action="<%=path%>/sppriceSaleDemo/check.do">
			<input type="hidden" id="h_area" value="${sppriceSale.area.code }"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="reported_goods_stores"/>：</div>
				<div class="form-input" style="width:150px;">
					<input type="text"  id="firmDes"  name="area.des" readonly="readonly" value="${sppriceSale.area.des }"/>
					<input type="hidden" id="firmCode" name="area.code" value="${sppriceSale.area.code }"/>
<%-- 					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
<!-- 				<div class="form-label" >分店</div> -->
<!-- 				<div class="form-input"> -->
<!-- 				<select id="area" name="area.code" class="select" onchange="findByFirm(this);"> -->
<!-- 					<option value="">请选择分店</option> -->
<%-- 					<c:forEach var="firm" items="${firmList}" varStatus="status"> --%>
<!-- 						<option  -->
<%-- 						<c:if test="${firm.code==sppriceSale.area.code }"> --%>
<!-- 						selected="selected" -->
<%-- 						</c:if> --%>
<%-- 						value="${firm.code }">${firm.des }</option> --%>
<%-- 					</c:forEach> --%>
<!-- 				</select>	 -->
<!-- 				</div> -->
				<div class="form-label" style="padding-left:15px;"><fmt:message key="supplies_code" />：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="supply.sp_code" value="${sppriceSale.supply.sp_code }" />
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
			</div>
			<div class="grid" id="grid">		
				<input type="hidden" name="firm" id="firm" value="${firm}"/>
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width:25px;"></span></td>
								<td rowspan="2"><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td colspan="5"><span style="text-align:center;"><fmt:message key="supplies"/></span></td>
								<td colspan="4"><span style="text-align:center;"><fmt:message key="pending_price"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="the_last_price"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="operator"/></span></td>
								<td rowspan="2"><span style="width:130px;"><fmt:message key="fill_time"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="positn_state"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:110px;"><fmt:message key="name"/></span></td>
								<td><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="brand"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:80px;"><fmt:message key="branche"/></span></td>
								<td><span style="width:40px;"><fmt:message key="scm_sale_price"/></span></td>
								<td><span style="width:70px;"><fmt:message key="starttime"/></span></td>
								<td><span style="width:70px;"><fmt:message key="endtime"/></span></td>
							</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="sppriceSale" items="${sppriceSaleList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td title="${sppriceSale.rec}" >
										<span style="width:20px;">
											<input type="checkbox" name="idList" id="chk_${sppriceSale.rec}" value="${sppriceSale.rec}"/>
										</span>
									</td>
									<td title="${sppriceSale.supply.sp_code}" ><span id="sp_code_${status.index+1}" style="width:70px;">${sppriceSale.supply.sp_code}</span></td>
									<td title="${sppriceSale.supply.sp_name}" ><span style="width:110px;">${sppriceSale.supply.sp_name}</span></td>
									<td title="${sppriceSale.supply.sp_desc}" ><span style="width:50px;">${sppriceSale.supply.sp_desc}</span></td>
									<td title="${sppriceSale.supply.sp_mark}" ><span style="width:70px;">${sppriceSale.supply.sp_mark}</span></td>
									<td title="${sppriceSale.supply.unit}" ><span style="width:30px;">${sppriceSale.supply.unit}</span></td>
									<td title="${sppriceSale.area.des}">
										<span style="width:80px;">
											${sppriceSale.area.des}
											<input type="hidden" id="area_${sppriceSale.rec}" value="${sppriceSale.area.code }"/>
										</span>
									</td>
									<td title="${sppriceSale.price}" ><span style="width:40px;text-align: right;">${sppriceSale.price}</span></td>
									<td title="<fmt:formatDate value="${sppriceSale.bdat}" pattern="yyyy-MM-dd" type="date"/>" ><span style="width:70px;"><fmt:formatDate value="${sppriceSale.bdat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td title="<fmt:formatDate value="${sppriceSale.edat}" pattern="yyyy-MM-dd" type="date"/>" ><span style="width:70px;"><fmt:formatDate value="${sppriceSale.edat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td title="${sppriceSale.priceold}" ><span style="width:50px;text-align: right: ;">${sppriceSale.priceold}</span></td>
									<td title="${sppriceSale.emp}" ><span style="width:50px;">${sppriceSale.emp}</span></td>
									<td title="${sppriceSale.madet}" ><span style="width:130px;">${sppriceSale.madet}</span></td>
									<td title="${sppriceSale.sta}" ><span style="width:50px;">${sppriceSale.sta}</span></td>
									<td title="${sppriceSale.memo}" ><span style="width:50px;">${sppriceSale.memo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
		<page:page form="listForm" page="${pageobj}"></page:page>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
	</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒  
		}		
		//工具栏
		$(document).ready(function(){
			focus();//页面获得焦点
			document.getElementById("sp_code").focus();
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 67: $('#autoId-button-102').click(); break;
	            }
		 	});
		   //光标移动
		   new tabTableInput("tblGrid","text"); 
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			}); 	
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		   
		   $("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:true,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="reported_goods_stores"/>'
				});
			});
		   
		   $('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key="check" />(<u>C</u>)',
						title: '<fmt:message key="check" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							checkSppriceSale();
						}
					},"-",{
						text: '<fmt:message key="all"/><fmt:message key="check"/>',
						title: '<fmt:message key="all"/><fmt:message key="check"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							checkAllSppriceSale();
						}
					},"-",{
						text: '<fmt:message key="import"/>',
						title: '<fmt:message key="import"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							importSppriceSale();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}]
			});
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),75);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法								
		});				
		//弹出物资树回调函数
		function handler(sp_code){
			if(sp_code==undefined || ''==sp_code){
				$('#sp_code').val(sp_code); 
				$('#sp_name').val(sp_name); 
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					$('#sp_code').val(supply.sp_code); 
					$('#sp_name').val(supply.sp_name); 
				}
			});
		}
		//根据分店查询
		function findByFirm(obj){
			window.location.replace('<%=path%>/sppriceSaleDemo/check.do?area.code='+obj.value);
		}
		//审核单条
		function checkSppriceSale(){
			var data={};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key ="want_to_audit" />？')){
					var chkValue = [];
					var areaValue=[];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
						areaValue.push($("#area_"+$(this).val()).val());
					});
					data['rec']=chkValue.join(",");
					data['area.code']=areaValue.join(",");
					$.post("<%=path%>/sppriceSaleDemo/checkSppriceSale.do",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 1:
							alert('<fmt:message key ="check_successful" />！');
 							var action="<%=path%>/sppriceSaleDemo/check.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="Select_the_data_you_need_to_review" />！');
				return ;
			}
		}
		//审核全部数据    wjf  重新优化  2014.11.26
		function checkAllSppriceSale(){
			//1.先判断界面上有没有数据
			var trList = $('.grid').find('.table-body').find('tr').length;
			if(trList<=0){
				alert('<fmt:message key="There_is_no_data_to_audit" />！');
				return;
			}
			//2.判断是不是审核所有
			var firmCode = $('#firmCode').val();
			var flag = true;
			if(firmCode == ''){
				flag = confirm('<fmt:message key="You_do_not_choose_the_store_Continue_to_review_the_price_of_all_the_stores" />?');
			}
			//3.确定全部审核
			if(flag && confirm('<fmt:message key ="Determine_the_audit_price" />?')){
				$.post("<%=path%>/sppriceSaleDemo/checkSppriceSale.do?checkAll=checkAll&supply.sp_code="+$('#sp_code').val()+"&area.code="+firmCode,null,function(data){
					var rs = eval('('+data+')');
					switch(Number(rs.pr)){
					case 1:
						alert('<fmt:message key ="check_successful" />！');
						var action="<%=path%>/sppriceSaleDemo/check.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
						break;
					}
				});
			}
		}
		
		//导入excel售价单
		function importSppriceSale(){
	    	$('body').window({
				id: 'window_importSppriceSale',
				title: '<fmt:message key="import" /><fmt:message key="Price_list" />',
				content: '<iframe id="importSppriceSale" frameborder="0" src="<%=path%>/sppriceSaleDemo/importSppriceSale.do"></iframe>',
				width: '400px',
				height: '200px',
				draggable: true,
				isModal: true
			});
	    }
		</script>
	</body>
</html>