<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>售价单模板</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />		
			<style type="text/css">
				.search-button {
					color:#000000;
					background:transparent url("<%=path%>/image/search-button.gif") no-repeat;
					height:21px;
					vertical-align:middle;
					width:60px;
					border:0;
					cursor:pointer;
				}
				.input{
					background:transparent;
					border:0px solid;
				}
				.page{margin-bottom: 25px;}
				.table-head td span{
					white-space: normal;
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<!-- 保存选中的多选仓位 -->
		<form id="listForm" action="<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do" method="post">
			<input type="hidden" name="firm" id="firm" value="${firm}"/>
			<input type="hidden" id="h_positnCode" name="h_positn" value=""/>
			<input type="hidden" id="h_positnDes" name="h_positnDes" value=""/>
			<input type="hidden" id="h_sp_code" name="h_sp_code" value=""/>
			<input type="hidden" id="h_sp_name" name="h_sp_name" value=""/>
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label">
						<input type="radio" name='editTyp' checked="checked" value="bdat"/>
						<fmt:message key="update" /><fmt:message key="startdate" />
					</div>
					<div class="form-label">
						<input type="radio" name='editTyp' value="edat"/>
						<fmt:message key="update" /><fmt:message key="enddate" />
					</div>
						<!-- (三合一库，售价单模板，批量编辑可去掉价格编辑。有统一修改价格按钮) wangjie -->
<!-- 					<div class="form-label"> -->
<!-- 						<input type="radio" name='editTyp' value="price"/> -->
<%-- 						<fmt:message key="update" /><fmt:message key="price" /> --%>
<!-- 					</div> -->
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="supplies_code" />：</div>
				<div class="form-input">
					<input type="text" name="supply.sp_code" id="sp_code" value="${sppriceSaleDemo.supply.sp_code}"/>
					<img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' />
				</div>
				<div class="form-label"><fmt:message key="supplies_name" />：</div>
				<div class="form-input">
					<input type="text" class="text" name="supply.sp_name" id="sp_name" value="${sppriceSaleDemo.supply.sp_name }"/>
				</div>
			</div>
			<div class="grid" id="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
								<td><span style="width:120px;"><fmt:message key="supplies_name" /></span></td>
								<td><span style="width:50px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:70px;"><fmt:message key="brand"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:100px;"><fmt:message key="starttime"/></span></td>
								<td><span style="width:100px;"><fmt:message key="endtime"/></span></td>
								<td><span style="width:50px;"><fmt:message key="price" /></span></td>
								<td><span style="width:50px;"><fmt:message key="previous_price"/></span></td>
							</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="tblGrid">
						<tbody>
							<c:forEach var="sppriceSaleDemo" items="${sppriceSaleDemoList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td title="${sppriceSaleDemo.supply.sp_code}" >
										<span style="width:20px;">
											<input type="checkbox" name="idList" id="chk_${status.index+1}" value="${status.index+1}"/>
											<input type="hidden" id="firm_${status.index+1}" value="${sppriceSaleDemo.firm }"/>
										</span>
									</td>
									<td title="${sppriceSaleDemo.supply.sp_code}" ><span id="sp_code_${status.index+1}" style="width:80px;">${sppriceSaleDemo.supply.sp_code}</span></td>
									<td title="${sppriceSaleDemo.supply.sp_name}" ><span style="width:120px;">${sppriceSaleDemo.supply.sp_name}</span></td>
									<td title="${sppriceSaleDemo.supply.sp_desc}" ><span style="width:50px;">${sppriceSaleDemo.supply.sp_desc}</span></td>
									<td title="${sppriceSaleDemo.supply.sp_mark}" ><span style="width:70px;">${sppriceSaleDemo.supply.sp_mark}</span></td>
									<td title="${sppriceSaleDemo.supply.unit}" ><span style="width:40px;">${sppriceSaleDemo.supply.unit}</span></td>
									<td title="<fmt:formatDate value="${sppriceSaleDemo.bdat}" pattern="yyyy-MM-dd" type="date"/>" ><span id="bdat_${status.index+1}" style="width:100px;"><fmt:formatDate value="${sppriceSaleDemo.bdat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td title="<fmt:formatDate value="${sppriceSaleDemo.edat}" pattern="yyyy-MM-dd" type="date"/>" ><span id="edat_${status.index+1}" style="width:100px;"><fmt:formatDate value="${sppriceSaleDemo.edat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td title="${sppriceSaleDemo.price}" ><span style="width:50px;text-align: right;"><input type="text" id="price_${status.index+1}" value="${sppriceSaleDemo.price}" disabled="disabled" style="text-align: right;width: 50px;"/></span></td>
									<td title="${sppriceSaleDemo.priceold}" ><span id="priceold_${status.index+1}" style="width:50px;text-align: right;">${sppriceSaleDemo.priceold}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒  
		}		
		//工具栏
		$(document).ready(function(){
			focus();//页面获得焦点
			document.getElementById("sp_code").focus();
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
		   //光标移动
		   new tabTableInput("tblGrid","text"); 
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),handler);
				}
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'sp_code',
					validateType:['canNull','maxLength'],
					param:['F'],
					error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
				},{
					type:'text',
					validateObj:'sp_name',
					validateType:['canNull','maxLength'],
					param:['F','10'],
					error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
				},{
					type:'text',
					validateObj:'bdat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key ="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'edat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key ="cannot_be_empty" />！']
				}]
			});						
			loadToolbar([false,true,true]);
			
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),75);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			changeTh();
			/* 批量修改确认 */
			$("#search").bind('click', function() {
				$('.search-div').hide();
				var editTyp = $(":radio[name='editTyp']:checked").val();
				var url = "<%=path%>/sppriceDemo/toUpdateDate.do";
 				if(editTyp == 'price'){
 					url = "<%=path%>/sppriceDemo/toUpdatePrice.do";
 				}
				var curWindow = $('body').window({
					id: 'window_updateSppriceSaleDemo',
					title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
					content: '<iframe id="window_updateSppriceSaleDemo" name="window_updateSppriceSaleDemo" frameborder="0" src='+url+'></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					confirmClose:false,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(editTyp == 'price'){
										var price = $(getFrame('window_updateSppriceSaleDemo').document).find('#price').val();
										if(!price || isNaN(price)){
											alert('<fmt:message key="please_enter"/><fmt:message key="price"/>！');
											return;
										}
										execUpdate(price,editTyp);
									}else{
										var date = $(window.frames["window_updateSppriceSaleDemo"].document).find('#date').val();
// 										alert(date);
										execUpdate(date,editTyp);	
 									}
									curWindow.close();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			});			
		});
		
		function loadToolbar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="insert" />',
					title: '<fmt:message key="insert"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if(null!=$('#firm').val() && ""!=$('#firm').val()){
							addSppriceSaleDemo();
						}else{
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
					}
				},{
					text: '<fmt:message key="update" />',
					title: '<fmt:message key="update"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[2],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if(null!=$('#firm').val() && ""!=$('#firm').val()){
							updateSppriceSaleDemo();
						}else{
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
					}
				},{
					text: '<fmt:message key="bulk" /><fmt:message key="edit" />',
					title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						var code = $.trim($('#firm').val());
						if(!code){
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
						var checkboxList = $('.grid').find('.table-body').find(':checkbox');
						if(checkboxList 
								&& checkboxList.filter(':checked').size() > 0){
							$('.search-div').slideToggle(100);
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						}
					}
				},{
					text: '<fmt:message key="delete" />',
					title: '<fmt:message key="delete" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						deletesppriceSaleDemo();
					}
				},{
					text: '<fmt:message key="price"/><fmt:message key="edit" />',
					title: '<fmt:message key="price"/><fmt:message key="edit" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-18px','0px']
					},
					handler: function(){
						var code = $.trim($('#firm').val());
						if(!code){
							alert('<fmt:message key ="please_click_left_list_select" />！');
							return;
						}
						var checkboxList = $('.grid').find('.table-body').find(':checkbox');
						if(checkboxList 
								&& checkboxList.filter(':checked').size() > 0){
							checkboxList.filter(':checked').each(function(){
								var num = $.trim($(this).closest("tr").find(".num").text());
								$("#price_"+num).attr('disabled',false);
							});
							loadToolbar([true,false,false]);
						}else{
							alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						}
					}
				},{
					text: '<fmt:message key="save" />',
					title: '<fmt:message key="save" />',
					useable: use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						priceSave();
					}
				},"-",{
					text: '<fmt:message key="bulk_add"/>',
					title: '<fmt:message key="bulk_add"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if(null!=$('#firm').val() && ""!=$('#firm').val()){
							addMore();
						}else{
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
					}
				},{
					text: '<fmt:message key="added_to_the_new_price"/>',
					title: '<fmt:message key="added_to_the_new_price"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if(null!=$('#firm').val() && ""!=$('#firm').val()){
							addNewPrice();
						}else{
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
					}
				},{
					text: '<fmt:message key="added_to_the_new_price"/><fmt:message key="all"/>',
					title: '<fmt:message key="added_to_the_new_price"/><fmt:message key="all"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','0px']
					},
					handler: function(){
						if(null!=$('#firm').val() && ""!=$('#firm').val()){
							addNewPriceAll();
						}else{
							alert('<fmt:message key="please_click_left_list_select"/>！');
							return;
						}
					}
				},"-",{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
					}
				}]
		});
		}
		
		//单条编辑页面
		function updateSppriceSaleDemo(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() ==1){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						$('body').window({
							id: 'window_updateSppriceSaleDemo',
							title: '<fmt:message key="Change_price_list" />',
							content: '<iframe id="updateSppriceSaleDemoFrame" frameborder="0" src="'+"<%=path%>/sppriceSaleDemo/toUpdateSppriceSaleDemo.do?firm="+$('#firm').val()+'&supply.sp_code='+$('#sp_code_'+chkValue).text()+'"></iframe>',
							width: '550px',
							height: '400px',
							confirmClose: false,
							draggable: true,
							isModal: true
						});
			}else if(checkboxList 
					&& checkboxList.filter(':checked').size() > 1){
				alert('<fmt:message key="please_select_data" />！');
				return ;
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
				return ;
			}
		}
		//弹出物资树回调函数
		function handler(sp_code){
			$('#sp_code').val(sp_code); 
			if(sp_code==undefined || ''==sp_code){
				return;
			}
			$('.validateMsg').remove(); 
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					 $('#sp_init').val(supply.sp_init);
				}
			});
		}
		//执行修改
		function execUpdate(value,typ){
			var selected = {};
			var i = 0;
			var maxBdat = 0;
			var minEdat = 0;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.filter(':checked').each(function(){
				var num = $.trim($(this).closest("tr").find(".num").text());
				if(typ != 'price'){
					var curBdat = Number($.trim($("#bdat_"+num).text().replace(/-/g,'')));
					var curEdat = Number($.trim($("#edat_"+num).text().replace(/-/g,'')));
					maxBdat = maxBdat < curBdat ? curBdat : maxBdat;
					minEdat = minEdat < curEdat && minEdat != 0 ? minEdat : curEdat;
					if(typ == 'bdat')selected['sppriceSaleDemoList['+i+'].bdat'] = $.trim(value);
					if(typ == 'edat')selected['sppriceSaleDemoList['+i+'].edat'] = $.trim(value);
				}else{
					selected['sppriceSaleDemoList['+i+'].price'] = value;
				}
				selected['sppriceSaleDemoList['+i+'].firm'] = $.trim($('#firm').val());
				selected['sppriceSaleDemoList['+i+'].supply.sp_code'] = $.trim($("#sp_code_"+num).text());
				i ++;
			});
			var cur = Number(value.replace(/-/g,''));
			if(typ == 'edat' ? cur > maxBdat : (typ == 'bdat' ? cur < minEdat : true)){
				$.post('<%=path%>/sppriceSaleDemo/saveByUpdateBatch.do',selected,function(data){
					alert('<fmt:message key="update_successful" />！');
					pageReload();
				});
			}else{
				alert('<fmt:message key ="Start_time_not_later_than_the_end_of_time" />！');
				return;
			}
		}		
		//删除申购信息
		function deletesppriceSaleDemo(){
			var data={};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm" />？')){
					var chkValue = [];
					var firmValue= [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($('#sp_code_'+$(this).val()).text());
						firmValue.push($('#firm_'+$(this).val()).val());
					});
					data['supply.sp_code']=chkValue.join(",");
					data['firm']=firmValue.join(",");
					$.post("<%=path%>/sppriceSaleDemo/delete.do",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 1:
							alert('<fmt:message key ="successful_deleted" />！');
							$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do');
							$('#listForm').submit();
							break;
						case 2:
							alert('<fmt:message key ="delete_fail" />！');
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
				return ;
			}
		}
		//批量添加
		function addMore(){
			if(null!=$('#firm').val() && ""!=$('#firm').val()){
				if(!!!top.customWindow){
					var offset = getOffset('sp_code');
					top.cust('<fmt:message key ="please_select_materials" />','<%=path%>/supply/addSupplyBatch.do',offset,$('#h_sp_name'),$('#h_sp_code'),'750','500','isNull',addNewSppriceSaleDemoMore);
				}
			}else{
				alert('<fmt:message key ="please_click_left_list_select" />！');
			}
		}
		//批量添加售价单模板
		function addNewSppriceSaleDemoMore(){
			var selected = {};
			selected['firm']=$('#firm').val();
			selected['h_sp_code']=$('#h_sp_code').val();
			selected['h_sp_name']=$('#h_sp_name').val();
			$.post("<%=path%>/sppriceSaleDemo/addSppriceSaleDemoMore.do",selected,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs.pr)){
				case 1:
					alert('<fmt:message key="Data_already_exists_add_failed" />！！');
					break;
				case 2:
// 					alert('添加成功！');
					$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do');
					$('#listForm').submit();
					break;
				case 3:
					break;
				}
			});
		}
		//添加到新报价
		function addNewPrice(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 0){
				alert("<fmt:message key='please_select_materials'/>");
				return;
			}
			
			if(confirm('<fmt:message key="Are_you_sure_you_want_to_generate_a_new_price_for_the_updated_data" />？')){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#h_positnCode").val(),
					single:false,
					tagName:'h_positnDes',
					tagId:'h_positnCode',
					title:'<fmt:message key="positions"/>',
					callBack:'addNewSpprice'
				});
			}
		}
		//添加到新报价  全部
		function addNewPriceAll(){
			if(confirm('<fmt:message key="Are_you_sure_you_want_to_generate_a_new_price_for_the_updated_data_The_operation_will_add_all_the_data_to_the_new_price"/>')){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#h_positnCode").val(),
					single:false,
					tagName:'h_positnDes',
					tagId:'h_positnCode',
					title:'<fmt:message key="positions"/>',
					callBack:'addNewSppriceAll'
				});
			}
		}
		//添加到新报价   
		function addNewSpprice(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			//2015年6月9日 14:02:02   jira HXL-93  wangjie
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				var selected = {};
				var i = 0;
				checkboxList.filter(':checked').each(function(){
					var num = $.trim($(this).closest("tr").find(".num").text());
					selected['sppriceSaleDemoList['+i+'].supply.sp_code'] = $.trim($("#sp_code_"+num).text());
					selected['sppriceSaleDemoList['+i+'].bdat'] = $.trim($("#bdat_"+num).text());
					selected['sppriceSaleDemoList['+i+'].edat'] = $.trim($("#edat_"+num).text());
					selected['sppriceSaleDemoList['+i+'].price'] = $.trim($("#price_"+num).val());
					selected['sppriceSaleDemoList['+i+'].priceold'] = $.trim($("#priceold_"+num).text());
					i++;
				});
				selected['firm'] = $('#h_positnCode').val();
				$.post("<%=path%>/sppriceSaleDemo/addSppriceSale.do?type=addSppriceSale",selected,function(data){
					var rs = eval('('+data+')');
 					switch(Number(rs.pr)){
 					case 2:
 						alert('<fmt:message key="The_new_price_has_been_added_after_the_audit_is_valid,_please_check" />！');
 						$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do');
 						$('#listForm').submit();
 						break;
 					}
				});
			}else{
				alert("<fmt:message key='please_select_materials'/>");
			}
		}
		//添加到新报价     全部
		function addNewSppriceAll(){
				var selected = {}; 
				selected['firm'] = $('#h_positnCode').val();
				selected['all'] = 'Y';
				$.post("<%=path%>/sppriceSaleDemo/addSppriceSale.do?type=addSppriceSale",selected,function(data){
					var rs = eval('('+data+')');
 					switch(Number(rs.pr)){
 					case 2:
 						alert('<fmt:message key="The_new_price_has_been_added_after_the_audit_is_valid,_please_check" />！');
 						$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do');
 						$('#listForm').submit();
 						break;
 					}
				});
		}
		//左侧列表点击事件
		function textSubmit(firm){
			$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSppriceSaleDemo.do');
			$('#firm').val(firm);
			$('#listForm').submit();
		}
		//设置默认值
		function fPrice(obj){
			if(obj.value==""){
				obj.value=obj.defaultValue;
			}
		}
		function pageReload(){
			$('#listForm').submit();
		}
		//屏蔽空格
		$(document).keydown(function(event){
			if(event.keyCode==32){
				return false;
			}
		});
		//新增售价单模板
		function addSppriceSaleDemo(){
			$('body').window({
				id: 'window_addSppriceSaleDemo',
				title: '<fmt:message key="New_price_list" />',
				content: '<iframe id="addSppriceSaleDemoFrame" frameborder="0" src="'+"<%=path%>/sppriceSaleDemo/toAddSppriceSaleDemo.do?firm="+$('#firm').val()+'"></iframe>',
				width: '550px',
				height: '400px',
				confirmClose: false,
				draggable: true,
				isModal: true
			});
		}
		//双击修改数量
// 		$('#tblGrid').find('tr').find('td:eq(7) span,td:eq(8) span,td:eq(9) span').bind('dblclick',function(){
// 			alert($(this).closest('tr').find('td').index($(this).closest('td')));
// 			var parm=$(this).text();
// 			if(($(this).children().attr('id')=='editText'))return;
// 			$(this).html('<input type="text" class="Wdate input" id="editText" style="width:'+$(this).width()+'px;" value="'+$(this).text()+'" onblur="onBlurMethod(this)"/>');
// 			$(this).children('input').focus(function(){new WdatePicker({onpicked:function(){parm=$dp.cal.getNewDateStr();}});}).select();
// 			$(this).children('input').blur(function(){
// 				$(this).parent().html(parm);
// 			});
// 			alert($(this).closet('tr').find('td').index(this));
// 		});
		
		//价格批量修改后保存
		function priceSave(){
			var selected = {};
			var i = 0;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList.filter(':checked').size()==0){
				alert('<fmt:message key="Choose_the_information_you_want_to_save" />！');
				return;
			}
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(){
					var num = $.trim($(this).closest("tr").find(".num").text());
					selected['sppriceSaleDemoList['+i+'].price'] = $("#price_"+num).val();
					selected['sppriceSaleDemoList['+i+'].firm'] = $.trim($('#firm').val());
					selected['sppriceSaleDemoList['+i+'].supply.sp_code'] = $.trim($("#sp_code_"+num).text());
					i++;
				});
				$.post('<%=path%>/sppriceSaleDemo/saveByUpdateBatch.do',selected,function(data){
					alert('<fmt:message key="update_successful" />！');
					pageReload();
				});
			}
		}
		</script>
	</body>
</html>