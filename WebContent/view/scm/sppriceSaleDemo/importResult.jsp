<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="import" />Excel</title>
<link type="text/css" rel="stylesheet"
	href="<%=path%>/css/lib.ui.core.css" />
</head>
<body>
	<input type="hidden" id="realFilePath" value='${realFilePath}' />
	<div id="load" style="display: block;">
	  <div id='wBox_loading' align="center">
		<div class="wBox_content" id="wBoxContent">
		  <img src='<%=path%>/image/upload/loading.gif' /><fmt:message key="loading_wait_please" />
		</div>
	 </div>
	</div>
	<div id="result"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript">
		$(function(){
			var realFilePath=$("#realFilePath").val();
<%-- 			var url = '<%=path%>/sppriceSaleDemo/importExcel.do?realFilePath='+realFilePath; --%>
// 			parent.location.href = url;
			$.ajax({
				type:"POST",
				url:'<%=path%>/sppriceSaleDemo/importExcel.do',
				data : "realFilePath=" + realFilePath,
				dataType : "json",
				success : function(listError) {
				$("#load").hide();
				if (listError.length > 0) {
					var html_ = "";
					for ( var i = 0; i < listError.length; i ++) {
						var error = listError[i];
						html_ += "<span>" + error + "</span></br>";
					}
					$("#result").html(html_);
				} else {
					$("#result").html("<fmt:message key="import_successful" />");
					window.setTimeout(function(){
						window.close();
					}, 1500);
				}
			}
		});
	});
	</script>
</body>
</html>