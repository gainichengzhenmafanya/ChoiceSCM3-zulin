<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>售价单综合查询</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		.numAlign{
			text-align: right;
		}
		.form-line .form-label{
			width: 12%;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="toolBarSta" value="${sppriceSale.sta}"/>
		<input type="hidden" id="nodatevalue" value="${nodate}"/>
		<form id="listForm" action="<%=path%>/sppriceSaleDemo/findSupplySppriceSale.do" method="post">
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
<!-- 					<div class="form-label"> -->
<!-- 						<input type="radio" name='editTyp' checked="checked" value="bdat"/> -->
<!-- 						<fmt:message key="update" /><fmt:message key="startdate" /> --!>
<!-- 					</div> -->
					<div class="form-label">
						<input type="radio" name='editTyp' checked="checked" value="edat"/>
						<fmt:message key="update" /><fmt:message key="enddate" />
					</div>
<!-- 						<div class="form-label"> -->
<!-- 							<input type="radio" name='editTyp' value="price"/> -->
<!--  							<fmt:message key="update" /><fmt:message key="price" /> -->
<!-- 						</div> -->
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input">
					<input type="text" id="bdat" style="width:100px;" name="bdat" class="Wdate text" value="<fmt:formatDate value="${sppriceSale.bdat }" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
					<input type="checkbox" style="vertical-align:middle;" id="nodate" name="nodate" class="checkbox" <c:if test="${nodate=='Y' }"> checked="checked" </c:if> value="Y" /><label style="color:red;vertical-align:middle;cursor:pointer;" id="nodateLabel"><fmt:message key ="date_invalid" /></label>
				</div>
				<div class="form-label"><fmt:message key="category_selection"/>：</div>
				<div class="form-input">
					<input type="text" id="typDes" name="typDes" readonly="readonly" value="${typDes}"/>
					<input type="hidden" id="typCode" name="supply.sp_type" value="${sppriceSale.supply.sp_type}"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
			    </div>
				<div class="form-label"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="supply.sp_code" value="${sppriceSale.supply.sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>										
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input">
					<input type="text" id="edat" style="width:100px;" name="edat" class="Wdate text" value="<fmt:formatDate value="${sppriceSale.edat }" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
				</div>
				<div class="form-label"><fmt:message key="branche"/>：</div>
				<div class="form-input">
					<input type="text" id="firmDes" name="firmDes" readonly="readonly" value="${firmDes }"/>
					<input type="hidden" id="area" name="area.code" value="${sppriceSale.area.code }"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="positn_state"/>：</div>
				<div class="form-input">
					<select id="sta" name="sta" class="select">
<!-- 						<option value=""><fmt:message key ="all" /></option> -->
						<option <c:if test="${sppriceSale.sta=='N' }">selected="selected"</c:if> value="N"><fmt:message key ="unchecked" /></option>
						<option <c:if test="${sppriceSale.sta=='Y' }">selected="selected"</c:if> value="Y"><fmt:message key ="checked" /></option>
					</select>
				</div>					
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2">
									<span style="width:20px;"><input type="checkbox" id="chkAll"/></span>
								</td>
								<td colspan="5"><span><fmt:message key="supplies" /></span></td>
								<td colspan="4"><span><fmt:message key="pending_price"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="the_last_price"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="operator" /></span></td>
								<td rowspan="2"><span style="width:125px;"><fmt:message key="fill_time" /></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="positn_state"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="remark" /></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:120px;"><fmt:message key="name" /></span></td>
								<td><span style="width:50px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:40px;"><fmt:message key="brands" /></span></td>
								<td><span style="width:30px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:80px;"><fmt:message key="branche" /></span></td>
								<td><span style="width:50px;"><fmt:message key="scm_sale_price"/></span></td>
								<td><span style="width:70px;"><fmt:message key="startdate" /></span></td>
								<td><span style="width:70px;"><fmt:message key="enddate" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="spprice" items="${sppriceSaleList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span>
									</td>
									<td><span style="width:20px; text-align: center;">
										<input type="hidden" id="h_rec_${status.index+1}" value="${spprice.rec }"/>
										<input name="idList" type="checkbox" id="chk_<c:out value='${status.index+1}' />" value="<c:out value='${status.index+1}' />"/></span>
									</td>
									<td><span style="width:70px;"><c:out value="${spprice.supply.sp_code}" />&nbsp;</span></td>
									<td><span style="width:120px;"><c:out value="${spprice.supply.sp_name}" />&nbsp;</span></td>
									<td><span style="width:50px;"><c:out value="${spprice.supply.sp_desc}" />&nbsp;</span></td>
									<td><span style="width:40px;"><c:out value="${spprice.supply.sp_mark}" />&nbsp;</span></td>
									<td><span style="width:30px;"><c:out value="${spprice.supply.unit}" />&nbsp;</span></td>
									<td>
										<span style="width:80px;">
											<input type="hidden" id="h_area_${status.index+1}" value="${spprice.area.code }"/>
											<c:out value="${spprice.area.des}" />
										</span>
									</td>
									<td><span style="width:50px;text-align: right;"><c:out value="${spprice.price}" /></span></td>
									<td><span id="bdat_${status.index+1}" style="width:70px;"><fmt:formatDate value="${spprice.bdat}" type="date"/></span></td>
									<td><span id="edat_${status.index+1}" style="width:70px;"><fmt:formatDate value="${spprice.edat}" type="date"/></span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${spprice.priceold}" />&nbsp;</span></td>
									<td><span style="width:50px;"><c:out value="${spprice.emp}" />&nbsp;</span></td>
									<td><span style="width:125px;"><c:out value="${spprice.madet}" />&nbsp;</span></td>
									<td><span style="width:40px;"><c:out value="${spprice.sta}" />&nbsp;</span></td>
									<td><span style="width:60px;"><c:out value="${spprice.memo}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
// 			if($('#toolBarSta').val()=="" || $('toolBarSta').val()==null)loadToolBar([true,true,true,true]);
			if($('#toolBarSta').val()=="Y")loadToolBar([true,false,true,false]);
			if($('#toolBarSta').val()=="N")loadToolBar([true,true,false,true]);
// 			$("#typ").htmlUtils("select",[]);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#area").val(),
					single:true,
					tagName:'firmDes',
					tagId:'area',
					title:'<fmt:message key="please_select_branche"/>'
				});
			});
			
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					var defaultName = $('#typDes').val();	
					var offset = getOffset('firmDes');
					top.cust('<fmt:message key="please_select_category"/>','<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+defaultName,offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			/* 批量修改确认 */
			$("#search").bind('click', function() {
				$('.search-div').hide();
				var editTyp = $(":radio[name='editTyp']:checked").val();
				var url = "<%=path%>/sppriceDemo/toUpdateDate.do";
 				if(editTyp == 'price'){
 					url = "<%=path%>/sppriceDemo/toUpdatePrice.do";
 				};
				var curWindow = $('body').window({
					id: 'window_updateSppriceSale',
					title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
					content: '<iframe id="window_updateSppriceSale" name="window_updateSppriceSale" frameborder="0" src='+url+'></iframe>',
					width: 500,
					height:258,
					draggable: true,
					confirmClose:false,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
// 									if(editTyp == 'price'){
// 										var price = $(getFrame('window_updateSppriceDemo').document).find('#price').val();
// 										if(!price || isNaN(price)){
// 											alert('<fmt:message key="please_enter"/><fmt:message key="price"/>！');
// 											return;
// 										}
// 										execUpdate(price,editTyp);
// 									}else{
// 										var date=window.window_updateSppriceSale.getElementById("date").val();
										var date = window.frames["window_updateSppriceSale"].document.getElementById('date').value;
										execUpdate(date,editTyp);	
// 									}
									curWindow.close();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			});			
		});
// 		function toolBarSta(obj){
// 			if(""==$(obj).val()){
// 				loadToolBar([true,true,true,true]);
// 			}
// 			if("已审核"==$(obj).val()){
// 				loadToolBar([true,false,true,false]);
// 			}
// 			if("未审核"==$(obj).val()){
// 				loadToolBar([true,true,false,true]);
// 			}
// 		}
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key ="Date_can_not_be_empty" />！');
								return;
							}else{
								$('#listForm').submit();
							}
						}
					},{
						text: '<fmt:message key="bulk" /><fmt:message key="edit" />',
						title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							var checkboxList = $('.grid').find('.table-body').find(':checkbox');
							if(checkboxList 
									&& checkboxList.filter(':checked').size() > 0){
								$('.search-div').slideToggle(100);
							}else{
								alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
							}
						}
					},{
						text: '<fmt:message key="uncheck" />',
						title: '<fmt:message key ="uncheck_offer" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-100px']
						},
						handler: function(){
							unCheck();
						}
					},{
						text: '<fmt:message key="all"/><fmt:message key="uncheck"/>',
						title: '<fmt:message key="all"/><fmt:message key="uncheck"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-100px']
						},
						handler: function(){
							unAllCheck();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="Delete_the_selected_non_audit_price" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteSpprice();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
		}	
		//执行修改
		function execUpdate(value,typ){
			var selected = {};
			var i = 0;
			var maxBdat = 0;
			var minEdat = 0;
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			checkboxList.filter(':checked').each(function(){
				var num = $.trim($(this).closest("tr").find(".num").text());
				if(typ != 'price'){
					var curBdat = Number($.trim($("#bdat_"+num).text().replace(/-/g,'')));
					var curEdat = Number($.trim($("#edat_"+num).text().replace(/-/g,'')));
					maxBdat = maxBdat < curBdat ? curBdat : maxBdat;
					minEdat = minEdat < curEdat && minEdat != 0 ? minEdat : curEdat;
//					if(typ == 'bdat')selected['sppriceSaleDemoList['+i+'].bdat'] = $.trim(value);
					if(typ == 'edat')selected['sppriceSaleList['+i+'].edat'] = $.trim(value);
				}else{
// 					selected['demos['+i+'].price'] = value;
// 					selected['demos['+i+'].priceold'] = $.trim($("#price_"+num).text());
				}
// 				selected['demos['+i+'].acct'] = $("#acct").val();
//				selected['sppriceSaleDemoList['+i+'].firm'] = $.trim($('#addSppriceSaleDemoForm').find('input:hidden[name="firm"]').val());
// 				selected['demos['+i+'].deliver'] = $.trim($("#deliver").val());
				selected['sppriceSaleList['+i+'].rec'] = $.trim($('#h_rec_'+$(this).val()).val());
				selected['sppriceSaleList['+i+'].area.code'] = $.trim($('#h_area_'+$(this).val()).val());
				i ++;
			});
			var cur = Number(value.replace(/-/g,''));
// 			if(typ == 'edat' ? cur > maxBdat : (typ == 'bdat' ? cur < minEdat : true)){
				$.post('<%=path%>/sppriceSaleDemo/saveByUpdateBatch.do?type=updateSppriceSale',selected,function(data){
					alert('<fmt:message key="update_successful" />！');
					pageReload();
				});
// 			}else{
// 				alert('开始日期不能大于结束日期！');
// 				return;
// 			}
		}
		function pageReload(){
			$('#listForm').submit();
		}
		//反审核
		function unCheck(){
			var data={};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="Determine_the_reverse_audit" />？')){
					var chkValue = [];
					var areaValue= [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($('#h_rec_'+$(this).val()).val());
						areaValue.push($('#h_area_'+$(this).val()).val());
					});
					data['rec']=chkValue.join(",");
					data['area.code']=areaValue.join(",");
					$.post("<%=path%>/sppriceSaleDemo/checkSppriceSale.do?type=uncheck",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 1:
							alert('<fmt:message key="Reverse_audit_success" />！');
							$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSupplySppriceSale.do');
							$('#listForm').submit();
							break;
						case 2:
							alert('<fmt:message key="Reverse_audit_failure" />！');
							break;
						}
					});
				}
			}else{
				alert('<fmt:message key="Choose_the_data_that_needs_to_be_reversed" />！');
				return ;
			}
		}
		//全部反审核
		function unAllCheck(){
			var data={};
			data['bdat'] = $('#bdat').val();
			data['nodate'] = $('#nodatevalue').val();
			data['typDes'] = $('#typDes').val();
			data['typCode'] = $('#typCode').val();
			data['sp_code'] = $('#sp_code').val();
			data['edat'] = $('#edat').val();
			data['firmDes'] = $('#firmDes').val();
			data['sta'] = $('#sta').val();
			if(data){
				if(confirm('<fmt:message key="Determine_the_reverse_audit" />？')){
					$.post("<%=path%>/sppriceSaleDemo/reverseCheckSppriceSale.do",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 1:
							alert('<fmt:message key="Reverse_audit_success" />！');
							$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSupplySppriceSale.do');
							$('#listForm').submit();
							break;
						case 2:
							alert('<fmt:message key="Reverse_audit_failure" />！');
							break;
						}
					});
				}
			} else {
				alert('<fmt:message key="The_current_no_audit_data,_not_all_audit" />！');
				return;
			}
		}
		
		//删除售价单
		function deleteSpprice(){
			var data={};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="Delete_the_selected_non_audit_price" />？')){
					var chkValue = [];
					var areaValue= [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($('#h_rec_'+$(this).val()).val());
						areaValue.push($('#h_area_'+$(this).val()).val());
					});
					data['rec']=chkValue.join(",");
					data['area.code']=areaValue.join(",");
					$.post("<%=path%>/sppriceSaleDemo/deleteUncheckSpprice.do",data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs.pr)){
						case 1:
							if(Number(rs.failNum)>0){
								alert('<fmt:message key ="successful_deleted" />：'+Number(rs.succNum)+'<fmt:message key ="article" />。\n'+'<fmt:message key ="delete_fail" />：'+Number(rs.failNum)+'<fmt:message key ="article" />，原因：报价已审核。');
							}else{
								alert('<fmt:message key ="successful_deleted" />：'+Number(rs.succNum)+'<fmt:message key ="article" />。\n'+'<fmt:message key ="delete_fail" />：'+Number(rs.failNum)+'<fmt:message key ="article" />');	
							}
							
							$('#listForm').attr('action','<%=path%>/sppriceSaleDemo/findSupplySppriceSale.do');
							$('#listForm').submit();
							break;
						case 2:
							alert('<fmt:message key="Reverse_audit_failure" />！');
							break;
						}
					});
				}
			}else{
				alert('请选择需要反审核的数据！');
				return ;
			}

		}
		//仅查日盘物资
		$('#nodateLabel').toggle(
			function(){
				$("#nodate").attr("checked",true);
			},
			function(){
				$("#nodate").attr("checked",false);
			}
		);
		//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
		$('.grid').find('.table-body').find('tr').live("click", function () {
		     if ($(this).hasClass("bgBlue")) {
		         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
		     }
		     else
		     {
		         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
		     }
		 });
		//打印单据
		function printDayDifCompare(){ 
			window.open ("<%=path%>/firmMis/printDayDifCompare.do",'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
		}
		</script>			
	</body>
</html>