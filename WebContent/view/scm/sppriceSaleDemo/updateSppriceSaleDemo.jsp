<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改售价单模板</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
</head>
<body>
	<div class="tool"></div>
	<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:90px;">
	<form id="updateSppriceSaleDemoForm" name="updateSppriceSaleDemoForm" method="post" action="<%=path%>/sppriceSaleDemo/saveSppriceSaleDemo.do">
		<input type="hidden" name="firm" id="firm" value="${sppriceSaleDemo.firm}"/>
		<div class="form-line">
			<div class="form-label" ><fmt:message key="supplies_code" />：</div>
			<div class="form-input">
				<input type="text" class="text" readonly="readonly" name="supply.sp_code" id="sp_code" value="${sppriceSaleDemo.supply.sp_code }"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="supplies_name" />：</div>
			<div class="form-input">
				<input type="text" class="text" readonly="readonly" name="supply.sp_name" id="sp_name" value="${sppriceSaleDemo.supply.sp_name }"/>
			</div>
		</div>
		<div class="form-line">	
			<div class="form-label"><fmt:message key="specification" />：</div>
			<div class="form-input">
				<input type="text" class="text"  readonly="readonly" name="supply.sp_desc" id="sp_desc" value="${sppriceSaleDemo.supply.sp_desc }"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="unit" />：</div>
			<div class="form-input">
				<input type="text" class="text" readonly="readonly" name="supply.unit" id="unit" value="${sppriceSaleDemo.supply.unit }"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" ><font style="font-size:15px;color:red;vertical-align:middle;">*</font>价格：</div>
			<div class="form-input">
				<input type="text" class="text"  name="price" id="price" onblur="fPrice(this);" value="${sppriceSaleDemo.price }"/>
			</div>
		</div>
		<div class="form-line">		
			<div class="form-label"><font style="font-size:15px;color:red;vertical-align:middle;">*</font>开始日期：</div>
			<div class="form-input">
				<input type="text" value="<fmt:formatDate value="${sppriceSaleDemo.bdat}" pattern="yyyy-MM-dd" type="date"/>" name="bdat" id="bdat" class="Wdate text" value="${sppriceSaleDemo.bdat }" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
			</div>	
		</div>
		<div class="form-line">
			<div class="form-label"><font style="font-size:15px;color:red;vertical-align:middle;">*</font>结束日期：</div>
			<div class="form-input">
				<input type="text" value="<fmt:formatDate value="${sppriceSaleDemo.edat}" pattern="yyyy-MM-dd" type="date"/>" name="edat" id="edat" class="Wdate text" value="${sppriceSaleDemo.edat }" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
			</div>					
		</div>
	</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
	var validate;
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		if(e.altKey ==false)return;
	 		switch (e.keyCode){
	 			case 83: $('#autoId-button-105').click(); break;//保存
            }
		}); 
		/*验证*/
		validate = new Validate({
			validateItem:[{
				type:'text',
				validateObj:'price',
				validateType:['canNull'],
				param:['F'],
				error:['<fmt:message key ="cannot_be_empty" />！']
			},{
				type:'text',
				validateObj:'sp_code',
				validateType:['canNull'],
				param:['F'],
				error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！']
			},{
				type:'text',
				validateObj:'bdat',
				validateType:['canNull'],
				param:['F'],
				error:['<fmt:message key ="cannot_be_empty" />！']
			},{
				type:'text',
				validateObj:'edat',
				validateType:['canNull'],
				param:['F'],
				error:['<fmt:message key ="cannot_be_empty" />！']
			}]
		});
	});
	var tool = $('.tool').toolbar({
		items: [{
			text: '<fmt:message key="save" />(<u>S</u>)',
			title: '<fmt:message key="save" />',
			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'save')},
			icon: {
				url: '<%=path%>/image/Button/op_owner.gif',
				position: ['-80px','-0px']
			},
			handler: function(){
				if(validate._submitValidate()){
					$('#updateSppriceSaleDemoForm').submit();
				}
			}
		},{
			text: '<fmt:message key="quit"/>',
			title: '<fmt:message key="quit"/>',
			icon: {
				url: '<%=path%>/image/Button/op_owner.gif',
				position: ['-160px','-100px']
			},
			handler: function(){
				parent.$('.close').click();
			}
		}]
	});	
	//弹出物资树回调函数
	function handler(sp_code){
		$('#sp_code').val(sp_code); 
		if(sp_code==undefined || ''==sp_code){
			 $('#sp_code').val('');
			 $('#sp_name').val('');
			 $('#sp_desc').val('');
			 $('#unit').val('');
			 $('#unit1').val('');
			return;
		}
		$('.validateMsg').remove(); 
		$.ajax({
			type: "POST",
			url: "<%=path%>/supply/findById.do",
			data: "sp_code="+sp_code,
			dataType: "json",
			success:function(supply){
				$('#sp_code').val(supply.sp_code);
				 $('#sp_name').val(supply.sp_name);
				 $('#sp_desc').val(supply.sp_desc);
				 $('#unit').val(supply.unit);
				 $('#unit1').val(supply.unit1);
			}
		});
	}
	</script>
</body>
</html>