<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16.5%;
		}
		.form-line .form-input input , .form-line .form-line select{
			width:80%;
		}
		
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		var firm = '${firm}';
  	 		var bdat = $("#bdat").val();
  	 		var edat = $("#edat").val();
  	 		
  	 		
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/prdPrcCostManage/exportChkinCategorySum.do");
							$('#queryForm').submit();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/prdPrcCostManage/findGrpWeekHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data.columns;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 			columns.push({field:'TOTAL',title:'<fmt:message key="total"/>',width:100,align:'right'});
				for(var i in tableContent){
	  	 			columns.push({field:tableContent[i].CODE,title:tableContent[i].DES,width:70,align:'right'});
	  	 		}
	  	 		frozenColumns.push({field:'WEEKNO',title:'<fmt:message key ="Weekly" />',width:50,align:'left'});
	  	 		frozenColumns.push({field:'WEEKDES',title:'<fmt:message key ="Starting_date" />',width:100,align:'left'});
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="warehousing_category_summary"/>',
  	 			width:900,
  	 			height:280,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				var curWeekNo = rows[0].WEEKNO;
	 				var curWeekDes = rows[0].DES;
					var curRow = {};
					var rowTotal = 0;
	 				for(var i in rows){
	 					if(rows[i].WEEKNO != curWeekNo){
	 						curRow['WEEKNO'] = curWeekNo;
	 						curRow['WEEKDES'] = curWeekDes;
	 						curRow['TOTAL'] = rowTotal.toFixed(2);
	 						modifyRows.push(curRow);
	 						curRow = {};
	 						rowTotal = 0;
	 						curWeekNo = rows[i].WEEKNO;
	 						curWeekDes = rows[i].DES;
	 					}
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].CODE;
		 						if(rows[i].DEPT == currentCol){
		 							curRow[currentCol] = rows[i].AMTOUT.toFixed(2);
		 							rowTotal += Number(rows[i].AMTOUT);
		 						} 
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 				}
	 				curRow['WEEKNO'] = curWeekNo;
	 				curRow['WEEKDES'] = curWeekDes;
					curRow['TOTAL'] = rowTotal.toFixed(2);
					modifyRows.push(curRow);
	 				rs.rows = modifyRows;
	 				
	 				var modifyFoot = [];
	 				var totalAll = 0;
	 				var foot = rs.footer;
	 				var ft = {};
	 				ft['WEEKNO'] = '<fmt:message key="total"/>';
	 				for(var i in foot){
	 					ft[foot[i].DEPT] = foot[i].AMTIN.toFixed(2);
	 					totalAll += foot[i].AMTIN;
	 				}
	 				ft['TOTAL'] = totalAll.toFixed(2);
	 				modifyFoot.push(ft);
	 				rs.footer = modifyFoot;
	 				rs.total = rs.rows.length;
	 				return $.toJSON(rs);
	 			},
				url:encodeURI("<%=path%>/prdPrcCostManage/findGrpWeek.do?bdat="+bdat+"&edat="+edat+"&firm="+encodeURI(firm)),
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				}/* ,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				} */
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		$(".panel-tool").remove();
  	 	});
  	 	
  	 </script>
  </body>
</html>
