<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
  	<style type="text/css">
  		.btn {
  			width: 120px;
  			height: 50px;
  		}
  		.easyui-linkbutton{
		  	width: 130px;
  			height: 0px;
  			margin-top: 10px;
  			margin-left: 5px;
  			
  		}
  		a.l-btn-plain{
			border:1px solid #7eabcd; 
		}
  	</style>
  </head>	
  <body>
  	<div style="width: 630px;">
  		<a href="<%=path %>/firmMis/toFirmFoodProfit.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_cplrmx"/></a>
  		<a href="<%=path %>/prdPrcCostManage/toCostVariance.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_wzcbcy"/></a>
  		<a href="<%=path %>/prdPrcCostManage/openHeJian.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="Check_list"/></a>
  	</div>
  	<div style="width: 760px;">
<%--   		<a href="<%=path %>/SupplyAcct/toGrossProfitList.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">毛利汇总查询</a> --%>
  		<a href="<%=path %>/profitAna/profitDay.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_mlrqs"/></a>
<%--   		<a href="<%=path %>/prdPrcCostManage/toMaoWeekAna.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">毛利周趋势</a> --%>
  		<a href="<%=path %>/profitAna/profitMonth.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><fmt:message key="scm_mlyqs"/></a>
  		
  		<a href="<%=path %>/leiBieChengBen/toLeiBieChengBen.do" class="easyui-linkbutton" plain="true" iconCls="icon-undo">类别成本报表</a>
  	</div>
<!--   	<div style="width: 500px;"> -->
<%--   		<a href="<%=path %>/zongheMaoLiAna/toFirmWeekMaoLiAna.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum">周综合毛利分析</a> --%>
<%--   		<a href="<%=path %>/zongheMaoLiAna/toFirmMonthMaoLiAna.do" class="easyui-linkbutton" plain="true" iconCls="icon-sum">月综合毛利分析</a> --%>
<!--   	</div> -->
  	<div style="width: 890px;">
  		<a href="<%=path %>/ptivityAna/toPtivityAna.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="scm_yclfx"/></a>
  		<a href="<%=path %>/profitAna/firmDayProfit.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="scm_fdrmlfx"/></a>
  		<a href="<%=path %>/prdPrcCostManage/toFirmUseCompare.do " class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="scm_fdhydb"/></a>
<%--   		<a href="<%=path %>/useCostAna/toUseCostAna.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip">耗用成本分析</a> --%>
  		<a href="<%=path %>/useFirmCompare/toUseFirmCompare.do" class="easyui-linkbutton" plain="true" iconCls="icon-tip"><fmt:message key="scm_hyfddb"/></a>
  	</div>
  	<div style="width: 890px;">
  		<a href="<%=path %>/costProfitAna/firmProfitCmp.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_fdmldb"/></a>
  		<a href="<%=path %>/firmDangKouProfit/findDangKouProfit.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_fddkml"/></a>
  		<a href="<%=path %>/caiPinShiJiMaoLi/toCaiPinShiJiMaoLi.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_cpsjml"/></a>
  	</div>
<!--   	<div style="width: 890px;"> -->
<%--   		<a href="<%=path %>/wzChengbenHuizong/toWzChengbenHuizong.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-redo">物资成本汇总</a> --%>
<%--   		<a href="<%=path %>/wzChengbenMingxi/toWzChengbenMingxi.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-redo">物资成本明细</a> --%>
<!--   	</div> -->
	<!-- 以下几个报表都是西贝专用 -->
<%--   	 <div style="width: 890px;"> 
  		 <a href="<%=path %>/YueChengbenZongheFenxi/toYueChengbenZongheFenxi.do?action=init" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_ycbzhfx"/></a> 
  		 <a href="<%=path %>/wzChengbenChayi/toCostVariance.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="scm_wzcbcy"/></a>  <!-- 物资成本差异 --> 
  		 <a href="<%=path %>/gdMaolilv/toGdMaolilv.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="Stores_gross_profit"/></a>  <!-- 各店毛利率 --> 
  		 <a href="<%=path %>/dcMaolilv/toDcMaolilv.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="Single_food_gross_profit_comparison"/></a>  <!-- 单菜毛利横向对比 --> 
  		 <a href="<%=path %>/ylYingchanlv/toYlYingchanlv.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo"><fmt:message key="The_yield_of_raw_materials_should_be_compared"/></a>  <!-- 原料应产率横向对比 --> 
  	 </div>  --%>
	<!-- 九毛九专用报表 -->
<%--   	 <div style="width: 890px;"> 
  		 <a href="<%=path %>/WzWanyuanyongliangFenxi/toWzWanyuanyongliangFenxi.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">物资万元用量分析</a> 
  		 <a href="<%=path %>/WzYongliangFenchaFenxi/toWzYongliangFenchaFenxi.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">物资用量分差出成率分析</a> 
  	 </div>  --%>
<!--   	<div style="width: 890px;"> -->
<%--   		<a href="<%=path %>/MdChengbenDuibi/toMdChengbenDuibi.do" class="easyui-linkbutton" plain="true" iconCls="icon-redo">门店成本对比</a> --%>
<!--   	</div> -->
  	
  	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  	<script type="text/javascript">
  		function show(url){
  			window.location.href = url;
  		}
  		$('a').click(function(event){
  			var self = $(this);
  			if(!self.attr("tabid")){
  				self.attr("tabid",self.text()+new Date().getTime());
  			}
  			event.preventDefault();
  			top.showInfo(self.attr("tabid"),self.text(),self.attr("href").replace("<%=path %>",""));
  		});
  	</script>
  </body>
</html>
