<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>类别成本报表</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width:80%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
				<div class="form-line" style="z-index:10;">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',isShowClear:false});"/></div>
							<div class="form-label"><fmt:message key="branche" /></div>
							<div class="form-input">
								<input type="text" id="positn_name" name="positn_name" class="text" style="width: 148px;margin-bottom: 4px;" readonly="readonly"/>
								<input type="hidden" id="positn" name="positn" value=""/>
								<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="branches_selection"/>' />
							</div>
							<%--<div class="form-label"><fmt:message key="sector" /></div>
							<div class="form-input">
								<select id="firmDept" name="firmDept" url="<%=path%>/firmDept/findFirmDept.do" loadOnce="false" class="select" select="true"></select>
							</div>--%>
					<!-- 
					<div class="form-label"><fmt:message key ="scm_dept" /></div>
					<div class="form-input">
						<input type="text" name="deptdes" id="deptdes" style="width: 136px;margin-top: -3px;" autocomplete="off" class="text" value="<c:out value="${posSalePlan.deptdes}" />"/>
						<img id="seachDept" class="search" src="<%=path%>/image/themes/icons/search.png" alt='查询仓位' />
						<input type="hidden" id="dept" name="firmDept" value=""/>
					</div>
					-->
<%-- 				<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input">
						<input type="text" class="text" style="width: 148px;margin-bottom: 4px;" id="sp_code" name="sp_code" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div> --%>
				</div>
				<div class="form-line" style="z-index:9;">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',isShowClear:false});"/></div>
					<div class="form-label"><fmt:message key="category"/></div>
					<div class="form-input" style="margin-top: -1px;*margin-top:-2px;">
						<select name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do" code="${supplyAcct.grptyp}" des="${supplyAcct.grptypdes}" class="select"></select>
						<input type="hidden" id="grptypcode" value="${supplyAcct.grptyp}"/>
					</div>
				</div>
			</form>
 	 <div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  		var checkMis='${type}';
  	 	$(document).ready(function(){
  	 		$("select").not("#firmDept").each(function(){
  	 			$(this).htmlUtils("select");
  	 		});
  	 		$("#firmDept").htmlUtils("select",{
  	 			beforLoad:function(url){
  	 				$("#firmDept").attr("url","<%=path %>/firmDept/findFirmDept.do?firm="+$('#firmCode').val());
  	 			}
  	 		});
  	 		$("#firmDeptMis").htmlUtils("select");
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			//加载按钮
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(!isHashDate()){
								return;
							}
							//if((null!=$('#firmCode').val() && ""!=$('#firmCode').val())||checkMis=='mis'){							
								if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
									alert('<fmt:message key ="Date_can_not_be_empty" />！');
									return;
								}else if($('#bdat').val().substr(0,4)!=$('#edat').val().substr(0,4)){
									alert('<fmt:message key ="cannot_span_years_query" />！');
									return;
								}
								var form = $("#queryForm").find("*[name]");
								form = form.filter(function(index){
									var cur = form[index];
									if($(cur).attr("name")){
										if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
											if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
												if($("input[name='"+$(cur).attr("name")+"']:checked").length){
													params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
												}else{
													params[$(cur).attr("name")] = undefined;
												}
											}else{
												params[$(cur).attr("name")] = $(cur).val();
											}
										}
									}
								});
								$("#datagrid").datagrid("load");
							//}else{
							//	alert('<fmt:message key ="please_select_branche" />！');
							//	return false;
							//}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							if(!isHashDate()){
								return;
							}
							//if((null!=$('#firmCode').val() && ""!=$('#firmCode').val())||checkMis=='mis'){
								$('#queryForm').attr('action','<%=path%>/leiBieChengBen/exportLeiBieChengBen.do');
								$('#queryForm').submit();
							//}else{
							//	alert('<fmt:message key ="please_select_branche" />！');
							//	return false;
							//}
							//window.location="<%=path%>/leiBieChengBen/exportLeiBieChengBen.do";
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if(checkMis=='mis'){
								window.location.replace("<%=path%>/SupplyAcct/toWareReport.do?checkMis="+checkMis);
							}else{
								//window.location.replace("<%=path%>/leiBieChengBen/reportList.do");
								$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
							}
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = [''];
  	 		//数字列
  	 		var numCols = ['theory_use_amt','true_use_amt','chayi'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({type:"POST",
  	 				url:"<%=path%>/leiBieChengBen/findHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//收集表单参数
  	 		var form = $("#queryForm").find("*[name]");
			form = form.filter(function(index){
				var cur = form[index];
				if($(cur).attr("name")){
					if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
						if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
							if($("input[name='"+$(cur).attr("name")+"']:checked").length){
								params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
							}
						}else{
							params[$(cur).attr("name")] = $(cur).val();
						}
					}
				}
				
			});
						  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'类别成本报表',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				onDblClickRow:function(a,b){
					//openHeJian(b.SP_CODE);
				},
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				 var rs = eval("("+data+")");
		 				var modifyRows = [];
		 				var modifyFoot = [];
		 				var rows = rs.rows;
		 				var footer = rs.footer;
		 				if(!rows || rows.length <= 0||!footer || footer.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
		 				for(var i in rows){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
		 							if($.inArray(cols[j].properties,numCols) >=0){
		 								if(isNaN(value) && value.indexOf('%') >= 0){
		 									value = value.replace('%','');
		 									value = Number(value).toFixed(2) + '%';
		 								}else{
		 									value = value ? value.toFixed(2) : '0.00';
		 								}
		 							}else if($.inArray(cols[j].properties,dateCols) >= 0){
		 								value = convertDate(value);
		 							}
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"rows["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyRows.push(curRow);
		 				}
		 				rs.rows = modifyRows;
		 				for(var i in footer){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
		 							if($.inArray(cols[j].properties,numCols) >=0){
 		 								if(isNaN(value) && value.indexOf('%') >= 0){
 		 									value = value.replace('%','');
 		 									value = Number(value).toFixed(2) + '%';
 		 								}else{
 		 									value = value ? value.toFixed(2) : '0.00';
 		 								}
		 								//value = value ? value.toFixed(2) : '0.00';
		 							}else if($.inArray(cols[j].properties,dateCols) >= 0){
		 								value = convertDate(value);
		 							}
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							curRow[cols[j].columnName.toUpperCase()] = 0;
		 							//alert('<fmt:message key="wrong"/>'+"footer["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyFoot.push(curRow);
		 				}
		 				rs.footer = modifyFoot;
		 				return $.toJSON(rs);
		 				function convertDate(time){
		 					if(!$.trim(time)) return "";
		 					var date=new Date(time); 
		 					var str="";     
		 					str+=date.getFullYear()+"-";     
		 					str+=(date.getMonth()+1)+"-";     
		 					str+=date.getDate();
		 					return str;
		 				}
	 			},
				url:"<%=path%>/leiBieChengBen/findLeiBieChengBen.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		$(".panel-tool").remove();
  	 		
  	 		//分店
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					sta:'all',
					tagName:'positn_name',
					tagId:'positn',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});	
			
  	 		/*档口  弹出树*/
			$('#seachDept').bind('click.custom',function(e){
				var ucode = $('#firmCode').val();
				if(ucode==''){
					alert('<fmt:message key="Please_choose_a_branch_to_choose_the_next_stall" />');
					return; 
				}
				if(!!!top.customWindow){
					var defaultCode = $('#dept').val();
					var defaultName = $('#deptdes').val();
					//alert(defaultCode+"==="+defaultName);
					var offset = getOffset('dept');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?ucode='+ucode+'&typn='+'7&mold='+'oneTone&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deptdes'),$('#dept'),'760','520','isNull');
				}
			});
			
			
  	 	});
  	 	
  	 	//择类别的方法，返回类别两边不包含（）
	  	$('#seachTyp').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#typ').val();
				//var defaultName = $('#typDes').val();
				var offset = getOffset('seachTyp');
				top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?resulttyp=1&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
			}
		});
  	 	
  	 	//判断是否为空
  	 	function isHashDate(){
			var bdat = $("#bdat").val();
			var edat = $("#edat").val();
			if(bdat==''||bdat==undefined){
				alert('开始日期不能为空！');
				return false;
			}
			if(edat==''||edat==undefined){
				alert('结束日期不能为空！');
				return false;
			}
			return true;
  	 	}
  	 </script>
  </body>
</html>
