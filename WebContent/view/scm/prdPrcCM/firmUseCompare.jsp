<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>分店耗用对比</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		.numAlign{
			text-align: right;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/prdPrcCostManage/findFirmUseCompare.do" method="post">
			<div class="form-line">
				<div class="form-label" style="width: 55px;"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd" type="date"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label" style="width: 20px;"><fmt:message key="branche" /></div>
				<div class="form-input">
					<input type="text"  id="positn_name"  name="firmDes" value="${firmDes }" readonly="readonly" class="text" style="width: 135px;margin-bottom: 4px;"/>
					<input type="hidden" id="positn" name="firmCode" value="${firmCode }"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
				</div>				
				<div class="form-label" style="width: 55px;"><fmt:message key="supplies_code"/></div>
				<div class="form-input">
					<input type="text" style="width: 135px;margin-bottom: 4px;" id="sp_code" name="sp_code" value="${supplyAcct.sp_code }" class="text"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<%-- <div class="form-input">
					<input style="margin-left:15px;vertical-align:middle;" id="dayCheckBox" name="dayCheckBox" value="Y" type="checkbox" <c:if test="${dayCheckBox=='Y' }"> checked="checked"</c:if>/><label style="color:red;vertical-align:middle;cursor:pointer;" id="dayCheckLabel">仅查日盘物资</label>
				</div> --%>
			</div>
			<div class="form-line">
				<div class="form-label" style="width: 55px;"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label" style="width: 20px;"><fmt:message key="category"/></div>
				<div class="form-input" style="margin-top: -3px;*margin-top:-2px;">
					<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
					<input type="text" id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" style="width: 135px;margin-bottom: 4px;"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table id="tblHead" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_specifications"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<c:forEach var="positn" items="${positnHeadList }" varStatus="status">
									<td colspan="2">${positn.des }</td>
								</c:forEach>
								<td colspan="2"><fmt:message key="total"/></td>
							</tr>
							<tr>
								<c:forEach var="positn" items="${positnList }" varStatus="status">
									<td><span style="width:80px;"><fmt:message key="quantity"/></span></td>
									<td><span style="width:80px;"><fmt:message key="amount"/></span></td>
								</c:forEach>
								<td><span style="width:90px;color:blue;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:90px;color:blue;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
<%-- 						<c:forEach var="obj" items="${dataList }" varStatus="status"> --%>
<!-- 							<tr> -->
<%-- 								<td align="center"><span style="width:25px;">${status.index+1}</span></td> --%>
<%-- 								<td><span style="width:90px;">${obj.SP_CODE }</span></td> --%>
<%-- 								<td><span style="width:100px;">${obj.SP_NAME }</span></td> --%>
<%-- 								<td><span style="width:70px;">${obj.SP_DESC }</span></td> --%>
<%-- 								<td><span style="width:30px;">${obj.UNIT }</span></td> --%>
<%-- 								<td><span class="numAlign" style="width:50px;color:blue;">${obj.AMOUNT_TOTAL }</span></td> --%>
<%-- 								<td><span class="numAlign" style="width:50px;color:blue;">${obj.AMT_TOTAL }</span></td>	 --%>
<%-- 								<c:forEach var="positn" items="${positnList }" varStatus="status"> --%>
<!-- 									<td><span class="numAlign" style="width:50px;"> -->
<%-- 										<c:if test="${positn.code==obj.FIRM }"> --%>
<%-- 										${obj.AMOUNT } --%>
<%-- 										</c:if> --%>
<!-- 									</span></td> -->
<!-- 									<td><span class="numAlign" style="width:50px;"> -->
<%-- 										<c:if test="${positn.code==obj.FIRM }"> --%>
<%-- 										${obj.AMT } --%>
<%-- 										</c:if> --%>
<!-- 									</span></td> -->
<%-- 								</c:forEach> --%>
<!-- 							</tr> -->
<%-- 						</c:forEach> --%>
						</tbody>
					</table>					
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key ="Date_can_not_be_empty" />！');
								return;
							}else if($('#bdat').val().substr(0,4)!=$('#edat').val().substr(0,4)){
								alert('<fmt:message key ="cannot_span_years_query" />！');
								return;
							}else{
								//将选择的类别名称传到后台
								//$("#grptypdes").val(typeof($("#grptyp").data("checkedName"))!="undefined"?$("#grptyp").data("checkedName"):"");
								//$("#grpdes").val(typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"");
								//$("#typdes").val(typeof($("#typ").data("checkedName"))!="undefined"?$("#typ").data("checkedName"):"");
								$('#listForm').submit();
							}
						}
					},{
// 						text: '<fmt:message key="export" />',
// 						title: '<fmt:message key="exporting_reports" />',
// 						icon: {
// 							url: '<%=path%>/image/Button/excel.bmp',
// 							position: ['2px','2px']
// 						},
// 						handler: function(){
							
// 						}
// 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/prdPrcCostManage/reportList.do");	
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
			var obj='${dataList}';
 			var data = eval('('+obj+')');
 			var positnList='${positnList}';
 			var positn = eval('('+positnList+')');
 			var tbody=$('.table-body').find('tbody');
 			var amtsum=0;
 			var cntsum=0;
			for ( var i in data) {
				var tr=$('<tr></tr>');
				var lastIndex=$("#tblGrid").find("tr:last td:first").text();
				tr.append($('<td><span style="width:25px;">'+Number(Number(lastIndex)+1)+'</span></td>'));
				tr.append($('<td><span style="width:90px;">'+data[i]["SP_CODE"]+'</span></td>'));
				tr.append($('<td><span style="width:100px;">'+data[i]["SP_NAME"]+'</span></td>'));
				tr.append($('<td><span style="width:70px;">'+data[i]["SP_DESC"]+'</span></td>'));
				tr.append($('<td><span style="width:30px;">'+data[i]["UNIT"]+'</span></td>'));
				for ( var j in positn) {
					tr.append($('<td><span style="width:80px;text-align:right;">'+data[i][positn[j].code+"CNT"].toFixed(2)+'</span></td>'));
					tr.append($('<td><span style="width:80px;text-align:right;">'+data[i][positn[j].code+"AMT"].toFixed(2)+'</span></td>'));
					cntsum+=data[i][positn[j].code+"CNT"];
					amtsum+=data[i][positn[j].code+"AMT"];
				}
				tr.append($('<td><span style="width:90px;text-align:right;">'+cntsum.toFixed(2)+'</span></td>'));
				tr.append($('<td><span style="width:90px;text-align:right;">'+amtsum.toFixed(2)+'</span></td>'));
				cntsum=0;
				amtsum=0;
				tbody.append(tr);
			}
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//仅查日盘物资
		$('#dayCheckLabel').toggle(
			function(){
				$("#dayCheckBox").attr("checked",true);
			},
			function(){
				$("#dayCheckBox").attr("checked",false);
			}
		);
		//打印单据
		function printFirmUseCompare(){ 
			window.open ("<%=path%>/proPrcCostManage/printFirmUseCompare.do",'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
		}
		
  	 	//择类别的方法，返回类别两边不包含（）
	  	$('#seachTyp').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#typ').val();
				//var defaultName = $('#typDes').val();
				var offset = getOffset('seachTyp');
				top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?resulttyp=1&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
			}
		});
		</script>			
	</body>
</html>