<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		input[type=radio] , input[type=checkbox]{
			height:13px; 
			vertical-align:text-top; 
			margin-top:1px;
			margin-right: 3px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 5%;
		}
		form .form-line .form-input{
			width: 13%;
		}
		form .form-line .form-input input[type=text]{
			width: 80%;
		}
		form .form-line .form-input select{
			width: 82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key ="startdate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'})"/>
			</div>
			<div class="form-label"><span style="color:red">*</span><fmt:message key="pubitem"/></div>
			<div class="form-input">
				<input type="hidden"  id="vcode" name="sp_code"/>
				<input type="text" id="vname" name="sp_name" readonly="readonly" class="text"/>
				<img id="seachPubitem" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="pubitem"/>' />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="enddate" /></div>
			<div class="form-input">
				<input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'})"/>
			</div>
			<div class="form-label"><fmt:message key="branche"/></div>
			<div class="form-input">
				<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
				<input type="hidden" id="positn" name="positn" value=""/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				<%-- <select  name="positn" url="<%=path %>/positn/findAllPositnOut.do"   class="select"></select> --%>
			</div>
			<div class="form-label"></div>
			<div class="form-input">
				<input type="radio" name="showtyp" value="2" checked="checked"/><fmt:message key ="week" />
                <input type="radio" name="showtyp" value="3" /><fmt:message key ="misboh_month" />
			</div>
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.5.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($('#vcode').val() == ''){
								alert('<fmt:message key="Can_only_choose_one" />。');
								return;
							}
							var bdat = $("#queryForm").find("#bdat").val().toString();
							var edat = $("#queryForm").find("#edat").val().toString();
							if(bdat<=edat){
								var form = $("#queryForm").find("*[name]");
								form = form.filter(function(index){
									var cur = form[index];
									if($(cur).attr("name")){
										if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
											if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
												if($("input[name='"+$(cur).attr("name")+"']:checked").length){
													params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
												}else{
													params[$(cur).attr("name")] = undefined;
												}
											}else{
												params[$(cur).attr("name")] = $(cur).val();
											}
										}
									}
									
								});
								
								var bdat = $('#bdat').val();
								var edat = $('#edat').val();
								var showtyp = $('input[name="showtyp"]:checked').val();
								var tableContent1 = {};
					  	 		//表头（多行），其中元素为columns
					  	 		head = [];
								$.ajax({url:"<%=path%>/dcMaolilv/findDcMaolilvHeaders.do?bdat="+bdat+"&edat="+edat+"&showtyp="+showtyp,
				  	 				async:false,
				  	 				success:function(data){
				  	 					tableContent1 = data;
				  	 					tableContent = data;
				  	 				}
				  	 			});
								//解析获取的数据
						 		var col11 = [];
								for(var i in tableContent1){
									if(showtyp == 2){
						  	 			col11.push({field:'W_'+tableContent1[i].weekk,title:tableContent1[i].weekk+'('+tableContent1[i].des+')',width:110,align:'right'});
									}else{
										col11.push({field:'W_'+tableContent1[i].weekk,title:tableContent1[i].weekk,width:60,align:'right'});
									}
					  	 		}
								head.push(col11);
								$("#datagrid").datagrid({columns:head});
							}else{
								alert("<fmt:message key='startdate'/><fmt:message key='can_not_be_greater_than'/><fmt:message key='enddate'/>");
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							if($('#vcode').val() == ''){
								alert('<fmt:message key="Can_only_choose_one" />。');
								return;
							}
							$('#queryForm').attr('action',"<%=path%>/dcMaolilv/exportDcMaolilv.do");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/gdMaolilv/findGdMaolilvHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
	 		var col1 = [];
			for(var i in tableContent){
				col1.push({field:'W_'+tableContent[i].weekk,title:tableContent[i].weekk+'('+tableContent[i].des+')',width:110,align:'right'});
  	 		}
  	 		frozenColumns.push({field:'POSITN',title:'<fmt:message key="coding"/>',width:70,align:'left'});
  	 		frozenColumns.push({field:'POSITNDES',title:'<fmt:message key="name"/>',width:100,align:'left'});
			head.push(col1);
  	 		frozenHead.push(frozenColumns);
  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="Single_food_gross_profit_comparison"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var modifyFooter = [];
	 				var rows = rs.rows;
	 				var footer = rs.footer;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var curRow = {};
	 					curRow["POSITN"] = eval("rows["+i+"].POSITN");
	 					curRow["POSITNDES"] = eval("rows["+i+"].POSITNDES");
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].weekk;
	 							curRow["W_"+currentCol] =  eval("rows["+i+"].W_"+currentCol)?eval("rows["+i+"].W_"+currentCol).toFixed(2)+'%':'0.00%';
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}
	 				for(var i in footer){
	 					var curRow = {};
	 					curRow["POSITN"] = eval("footer["+i+"].POSITN");
	 					curRow["POSITNDES"] = eval("footer["+i+"].POSITNDES");
	 					for(var j in tableContent){
	 						try{
	 							var currentCol = tableContent[j].weekk;
	 							curRow["W_"+currentCol] =  eval("footer["+i+"].W_"+currentCol)?eval("footer["+i+"].W_"+currentCol).toFixed(2)+'%':'0.00%';
	 						}catch(e){
	 							alert('Exception');
	 						}
	 					}
	 					modifyFooter.push(curRow);
	 				}
	 				rs.rows = modifyRows;
	 				rs.footer = modifyFooter;
	 				//非固定列，第一列显示数据总行数
// 	 				rs.footer[0][columns[0].field] = rs.total;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/dcMaolilv/findDcMaolilv.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				rownumbers:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	  	 	$(".panel-tool").remove();

		  	$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
		  	
		  	$('#seachPubitem').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="Please_select_the_standard_unit" />',encodeURI('<%=path%>/supply/searchPubitemList.do?'),offset,$('#vname'),$('#vcode'),'750','500','isNull');
				}
			});
  	 	});

  	 </script>
  </body>
</html>
