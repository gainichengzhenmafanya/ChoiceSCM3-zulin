<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>加工耗用分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		.form-line .form-input input[type=text]{
			width:80%;
		}
		.form-line .form-input select{
			width:82%;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" /></div>
<!-- 					<div class="form-label">车间</div> -->
<!-- 					<div class="form-input"> -->
<!-- 						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly"/> -->
<!-- 						<input type="hidden" id="firmCode" name="firm" value=""/> -->
<%-- 						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 					</div> -->
					<div class="form-label"><fmt:message key="processing_room"/></div>
					<div class="form-input">
			 			<input type="text" name="firmDes" id="firmDes" class="selectDepartment text" readonly="readonly" />
						<input type="hidden" name="firm" id="firmCode" class="text"/>
					</div>	
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input">
						<input type="text" style="margin-top:-2px;vertical-align:middle;" id="sp_code" name="sp_code" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
					<!-- 生产耗用分析 增加一个过滤0值的条件 可以勾选  wangjie -->
					<div class="form-label"><fmt:message key ="filter_zero_value" /></div>
					<div class="form-input">
						<input type="checkbox" name="ifZero" id="ifZero" value="1"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>"/></div>
					<div class="form-label"><fmt:message key="bigClass"/></div>
					<div class="form-input">
						<select id="grptyp" name="grptyp" url="<%=path %>/grpTyp/findAllGrpTyp.do"   class="select"></select>
					</div>
					<div class="form-label"><fmt:message key="middleClass"/></div>
					<div class="form-input">
						<select id="grp" name="grp" url="<%=path %>/grpTyp/findAllGrp.do"   class="select"></select>
					</div>
					<div class="form-label"><fmt:message key="smallClass"/></div>
					<div class="form-input">
						<select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"   class="select"></select>
					</div>
				</div>
			</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  		
  	 	$(document).ready(function(){
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var bdat = $('#queryForm').find("#bdat").val().toString();
							var edat= $('#queryForm').find("#edat").val().toString();
							if(bdat<=edat){
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
							});
							$("#datagrid").datagrid("load");
							}else{
								alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
							}
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							//window.location="<%=path%>/prdPrcCostManage/exportProCostAna.do";
							var action="<%=path%>/prdPrcCostManage/exportProCostAna.do";
							$('#queryForm').attr('action',action);
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
								$('#queryForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								var action="<%=path%>/prdPrcCostManage/printProCostAna.do";
								$('#queryForm').attr('action',action);
								$('#queryForm').submit();								
							}else{
								alert('<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！');
								/* showMessage({
									type: 'error',
									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
									speed: 1000
									}); */
								return ;
							}
						}
					},{
						text: '<fmt:message key="column_selection" />',
						title: '<fmt:message key="column_selection" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-60px']
						},
						handler: function(){
							toColsChoose();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[
						{key:'<fmt:message key="normal_storage"/>',value:'<fmt:message key="normal_storage"/>'},
					    {key:'<fmt:message key="overage_storage"/>',value:'<fmt:message key="overage_storage"/>'},
					    {key:'<fmt:message key="gifts_warehousing"/>',value:'<fmt:message key="gifts_warehousing"/>'},
					    {key:'<fmt:message key="recycling_storage"/>',value:'<fmt:message key="recycling_storage"/>'},
					    {key:'<fmt:message key="consignment_warehousing"/>',value:'<fmt:message key="consignment_warehousing"/>'},
						{key:'<fmt:message key="returns"/>',value:'<fmt:message key="returns"/>'},
						{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
					]);
  	 		});
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = [''];
  	 		//数字列
  	 		var numCols = ['qcprice',					
  	 		           	   'qccnt',				
		  	 		       'qcamt',			
		  	 		       'inprice',						
		  	 		       'incnt',						
		  	 		       'inamt',					
		  	 		       'shprice',			
		  	 		       'shcnt',			
		  	 		       'shamt',		
		  	 		       'sjprice',		
		  	 		       'sjcnt',		
		  	 		       'sjamt',
		  	 		       'lhcnt',		
		  	 		       'lhamt',		
		  	 		       'difcnt',					
		  	 		       'difamt','difrate'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/prdPrcCostManage/findProCostAnaHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//收集表单参数
  	 		var form = $("#queryForm").find("*[name]");
			form = form.filter(function(index){
				var cur = form[index];
				if($(cur).attr("name")){
					if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
						if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
							if($("input[name='"+$(cur).attr("name")+"']:checked").length){
								params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
							}
						}else{
							params[$(cur).attr("name")] = $(cur).val();
						}
					}
				}
				
			});
						  	 		
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="The_processing_cost_analysis" />',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				 var rs = eval("("+data+")");
		 				var modifyRows = [];
		 				var modifyFoot = [];
		 				var rows = rs.rows;
		 				var footer = rs.footer;
		 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
		 				for(var i in rows){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("rows["+i+"]."+cols[j].properties.toUpperCase());
		 							value = $.inArray(cols[j].properties,numCols) >=0 && cols[j].properties != 'difrate' ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"rows["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyRows.push(curRow);
		 				}
		 				rs.rows = modifyRows;
		 				for(var i in footer){
		 					var cols = tableContent.columns;
							var curRow = {};
		 					for(var j in cols){
		 						try{
		 							var value = eval("footer["+i+"]."+cols[j].properties.toUpperCase()) ;
		 							value = $.inArray(cols[j].properties,numCols) >=0 && cols[j].properties != 'difrate' ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
		 							curRow[cols[j].columnName.toUpperCase()] = value;
		 						}catch(e){
		 							alert('<fmt:message key="wrong"/>'+"footer["+i+"]."+cols[j].properties);
		 						}
		 					}
		 					modifyFoot.push(curRow);
		 				}
		 				rs.footer = modifyFoot;
		 				return $.toJSON(rs);
		 				function convertDate(time){
		 					if(!$.trim(time)) return "";
		 					var date=new Date(time); 
		 					var str="";     
		 					str+=date.getFullYear()+"-";     
		 					str+=(date.getMonth()+1)+"-";     
		 					str+=date.getDate();
		 					return str;
		 				}
	 			},
				url:"<%=path%>/prdPrcCostManage/findProCostAna.do",
				remoteSort: true,
				//页码选择项
				pageList:[10,20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				pagination:true,
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$(".panel-tool").remove();
  	 		$('#firmDes').bind('focus.custom',function(e){
				var positnex=$('#firmCode').val();
				if(!!!top.customWindow){
					var offset = getOffset('firmDes');
					top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#firmCode'),null,null,null,'150','300','isNull');
				}
			});
  	 		
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 	});
  	 	//列选择
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/prdPrcCostManage/toColChooseProCostAna.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
