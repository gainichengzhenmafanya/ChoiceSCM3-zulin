<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>请购单填制提交</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/praybillm/saveByAdd.do" method="post">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="date"/>：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" style="width:200px;" id="maded" readonly="readonly" name="maded" class="Wdate text" value="<fmt:formatDate value="${praybillm.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker();"/>
					</div>
					<div class="form-label">请购<fmt:message key="orders_num"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${praybillm.praybillmNo!=null}"><c:out value="${praybillm.praybillmNo }"></c:out></c:if>
						<input type="hidden" id="praybillmNo" name="praybillmNo" class="text" value="${praybillm.praybillmNo }"/> 					
					</div>
					<div class="form-label"><font size="1px" color="red">(保存前，统一修改到货日期用)</font></div>
					<div class="form-label">到货日期：</div>
					<div class="form-input" style="width:160px;">
						<input type="text" style="width:150px;" id="hoped" class="Wdate text" value="<fmt:formatDate value="${praybillm.maded}" pattern="yyyy-MM-dd"/>" onfocus="WdatePicker();"/>
					</div>
					<input type="button" value="修&nbsp;&nbsp;改" onclick="setHoped()"/>
				</div>
				<div class="form-line">
					<div class="form-label">请购仓位：</div>
					<div class="form-input" style="width:205px;">
						<input type="text" class="text" id="firmDes" onfocus="this.select()" name="firmDes" style="width:40px;margin-top:4px;vertical-align:top;" value="${praybillm.firm}"/>
						<select class="select" id="firm" name="firm" style="width:155px" onchange="findByDes(this);">							
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
								<c:if test="${positn.code==praybillm.firm}"> 
									   	selected="selected"
								</c:if>
								id="des" value="${positn.code}" >${positn.code}-${positn.init}-${positn.des}</option>
							</c:forEach>
						</select>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>					
					<div class="form-label"><fmt:message key="make_orders"/>:</div>
					<div class="form-input" style="width:205px;">
						<c:if test="${praybillm.madeby!=null}"><c:out value="${praybillm.madeby}"></c:out></c:if>					
						<input type="hidden" id="madeby" name="madeby" class="text" value="${praybillm.madeby }"/>
					</div>		
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="accounting"/>:</div>
					<div id="showChecby" class="form-input">
						<c:if test="${praybillm.checby!=null}"><c:out value="${praybillm.checby}"></c:out></c:if>
						<input type="hidden" id="checby" name="checby" class="text" value="${praybillm.checby }"/>
					</div>	
					<div class="form-label" style="width:100px;">选择物资:</div>
					<div class="form-input" style="width:220px;">
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>											
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="4"><fmt:message key="standard_unit"/></td>
								<td colspan="2"><fmt:message key="reference_unit"/></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="remark"/></span></td>
								<!-- <td rowspan="2"><span style="width:70px;">赠送数量</span></td> -->
							</tr>
							<tr>									
								<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:70px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:70px;">单价</span></td>
								<td><span style="width:70px;">金额</span></td>
								<td><span style="width:70px;"><fmt:message key="quantity"/></span></td>								
								<td><span style="width:55px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<c:set var="sum_price" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="praybilld" items="${praybilldList}" varStatus="status">
							<tr data-mincnt="${praybilld.supply.mincnt}" data-unitper="${praybilld.supply.unitper }">
								<td align="center" style="width:26px;"><span >${status.index+1}</span></td>
								<td><span style="width:70px;">${praybilld.supply.sp_code }</span></td>
								<td><span style="width:90px;">${praybilld.supply.sp_name }</span></td>
								<td><span style="width:70px;">${praybilld.supply.sp_desc }</span></td>
								<td><span style="width:70px;text-align: right;">${praybilld.amount }</span></td>
								<td><span style="width:70px;">${praybilld.supply.unit }</span></td>
								<td><span style="width:70px;text-align: center;" price="${praybilld.supply.sp_price}"><fmt:formatNumber value="${praybilld.supply.sp_price}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: center;"><fmt:formatNumber value="${praybilld.supply.sp_price*praybilld.amount}" type="currency" pattern="0.00"/></span></td>
								<td><span style="width:70px;text-align: right;">${praybilld.amount1 }</span></td>
								<td><span style="width:55px;">${praybilld.supply.unit1 }</span></td>
								<td><span style="width:90px;">${praybilld.hoped}</span></td>
								<td><span style="width:70px;">${praybilld.memo}</span></td>
								<%-- <td><span style="width:70px;text-align: right;">${praybilld.iszs}</span></td> --%>
								<td style="display:none;"><span><input type="hidden" id="price_${status.index+1}" value="${praybilld.price }"/></span></td>								
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + praybilld.amount}"/>   
							<c:set var="sum_price" value="${sum_price + praybilld.supply.sp_price*praybilld.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num"><fmt:formatNumber value="${sum_num}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:100px;"></td>
							<td style="width:100px;background:#F1F1F1;">总金额：<label id="sum_price"><fmt:formatNumber value="${sum_price}" type="currency" pattern="0.00"/></label></td>
							<td style="width:65px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:65px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;"></td>
							<%-- <td><font color="blue"><fmt:message key="operation_tips"/></font></td> --%>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">

		var validate;
		//工具栏
		$(document).ready(function(){
			$("#firmDes").val($("#firm").val());
			/*过滤*/
			$('#firmDes').bind('keyup',function(){
		          $("#firm").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//按钮快捷键
			focus() ;//页面获得焦点
			//屏蔽鼠标右键
			//$(document).bind('contextmenu',function(){return false;});
			//在备注上添加回车可换行提示  wjf 2014.7.8
			$(document).bind('focusin',function(e){
				if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="11"){
				    	//创建换行提示信息
				    	var msgDiv = $('<div style="color:red;" class="hhMsg"></div>'); 
				    	msgDiv.css({
				    		'top': $(e.srcElement).offset().top,
				    		'left': $(e.srcElement).offset().left,
				    		'height': $(e.srcElement).height()-2,
				    		'width': $(e.srcElement).width()-4,
				    		'line-height': $(e.srcElement).height()+'px'
				    	}).html("回车可增加行！")
				    	.insertAfter($(e.srcElement))
				    	.bind('click',function(){
				    		$(e.srcElement).siblings('.hhMsg').remove();
				    	}); 
				    	
				    	$(e.srcElement).bind('blur',function(){
				    		$(e.srcElement).siblings('.hhMsg').remove();
				    	});
		    		}
		    	}
			});
			//键盘事件绑定
		 	$(document).bind('keyup',function(e){
		 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index=="4"||index=="8"||index=="12"){
			 			$(e.srcElement).unbind('blur').blur(function(e){
			 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			 			});
			    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
		    		}
		    	}
		 		if(e.keyCode==27){//esc键
		 			$('.<fmt:message key="quit"/>').click();
		 		}
// 		 		if($("#sta").val() == "edit" || $("#sta").val() == "add"){
		 			
// 			 		if(window.event && window.event.keyCode == 120) { 
// 			 			window.event.keyCode = 505; 
// 			 			getChkstoDemo();
//			 			searchChkstodemo();
// 			 		} 
// 			 		if(window.event && window.event.keyCode == 505)window.event.returnValue=false;
// 		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 85: $('#autoId-button-101').click(); break;//查看上传
	                case 70: $('#autoId-button-102').click(); break;//查询
	                case 65: $('#autoId-button-103').click(); break;//新建
	                case 69: $('#autoId-button-104').click(); break;//编辑
	                case 83: $('#autoId-button-105').click(); break;//保存
					case 68: $('#autoId-button-106').click(); break;//删除
	                case 80: $('#autoId-button-107').click(); break;//打印
	                case 67: $('#autoId-button-108').click(); break;//审核
	            }
			}); 
		 	$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firm').val();
					var defaultName = $('#firm').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firm'),$('#firmdes'),'760','520','isNull',handler);
				}
			});
		 	function handler(a){//修复赋值bug
		 		$('#firmDes').val(a);
		 		$('#firm').val(a);
		 	}
		 	$('#seachSupply').bind('click.custom',function(e){
		 		if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectSupplyLeft.do'),$('#sp_code'),null,null,$('#unit1'),$('.unit'),$('.unit1'),handler2);
				}
			});
		 	function handler2(sp_code){
		 		$.ajax({
					type: "POST",
					url: "<%=path%>/supply/findById.do",
					data: "sp_code="+sp_code,
					dataType: "json",
					success:function(supply){
						if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
							$.fn.autoGrid.addRow();
						}
						$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code);
						$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
						$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
						$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(supply.unit);
						$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(1).css("text-align","right");
						$(".table-body").find("tr:last").find("td:eq(6)").find('span').text(supply.sp_price).css("text-align","right");
						$(".table-body").find("tr:last").find("td:eq(7)").find('span').text(supply.sp_price).css("text-align","right");
						$(".table-body").find("tr:last").find("td:eq(9)").find('span').text(supply.unit1);
						$(".table-body").find("tr:last").find("td:eq(8)").find('span').text(supply.unitper).css("text-align","right");
						$(".table-body").find("tr:last").find("td:eq(10)").find('span').text($('#hoped').val());
						$(".table-body").find("tr:last").data("unitper",supply.unitper);
						$(".table-body").find("tr:last").data("mincnt",supply.mincnt);
					}
				});
		 	}
		 	//回车换焦点start
		    var array = new Array();        
	 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
	 	    array = ['firmDes','firm'];        
	 		//定义加载后定位在第一个输入框上          
	 		$('#'+array[0]).focus();            
	 		$('select,input[type="text"]').keydown(function(e) {                  
		 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
		 		var event = $.event.fix(e);                
		 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
		 		if (event.keyCode == 13) {                
		 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
	 				$('#'+array[++index]).focus();
	 				if(index==2){
	 					$('#firmDes').val($('#firm').val());
	 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
	 				} 
		 		}
	 		});    
	 		
	 		document.getElementById("firm").onfocus=function(){
	 			this.size = this.length;
	 			$('#firm').css('height','425px');
	 			$('#firm').parent('.form-input').css('z-index','2');
	 		};
 			document.getElementById("firm").onblur = function(){
	 			this.size = 1;
	 			$('#firm').css('height','20px');
 			};
	 		$('#firm').bind('dblclick', function() {
					$('#firmDes').val($(this).val());
					$('#firm').blur();
			});
 		    //回车换焦点end
			//判断按钮的显示与隐藏
			if($("#sta").val() == "add"){
				loadToolBar([true,true,true,false,true,false,false,false]);
				editCells();
			}else if($("#sta").val() == "show"){
				loadToolBar([true,true,true,true,false,true,true,true]);
			}else{
				loadToolBar([true,true,true,false,false,false,false,false]);
				$('#maded').attr("disabled","disabled");
				$('#firmDes').attr("disabled","disabled");
				$('#firm').attr("disabled","disabled");
			}
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'maded',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'praybillmNo',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			<c:if test="${tableFrom=='table'}">
				$('#autoId-button-102').click();
			</c:if>
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									searchPraybillm();
								}
							}else{
								searchPraybillm();
							}
						}
					},{
						text: '<fmt:message key="insert" />(<u>A</u>)',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									addPraybillm();
								}
							}
							else{
								addPraybillm();
							}
						}
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="edit"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','0px']
						},
						handler: function(){
							if($("#checby").val()!=null && $("#checby").val()!=""){
								alert('<fmt:message key="orders_audited_cannot_edited"/>！');
								return false;
							}else if($("#praybillmNo").val()==null || $("#praybillmNo").val()==""){
								return false;
							}else if($("#sta").val()=="add"){
								return false;
							}else{
								$("#sta").val("edit");
								loadToolBar([true,true,true,false,true,true,true,false]);
								editCells();
							}
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if($("#sta").val()=="add" || $("#sta").val()=="edit"){
								if(validate._submitValidate()){
									if(null!=$('#firm').val() && ""!=$('#firm').val()){
										savePraybillm();
									}else{
										alert('申购仓位不能为空！');
									}
								}
							}
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[5],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deletePraybillm();
						}
					},{
						text: '<fmt:message key="print" />(<u>P</u>)',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printPraybillm();
						}
					},{
						text: '<fmt:message key="check" />(<u>C</u>)',
						title: '<fmt:message key="check"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[7],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-58px','-240px']
						},
						handler: function(){
							checkPraybillm();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							if($("#sta").val()=="add"||$("#sta").val()=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}
				]
			});
		}	
		
		//编辑表格
		function editCells()
		{
			if($("#sta").val() == "add"){
				$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
			}
			$(".table-body").autoGrid({
				initRow:1,
				colPerRow:13,
				widths:[26,80,100,80,80,80,80,80,80,65,100,80,80],
				colStyle:['','','','',{background:"#F1F1F1"},'','','','','','','',''],
				VerifyEdit:{verify:true,enable:function(cell,row){
					return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
				}},
				onEdit:$.noop,
				editable:[2,4,10,11,12],//能输入的位置(8暂去)
				onLastClick:function(row){
					$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
					$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(4)').text());//总数量
					$('#sum_price').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(7)').text())).toFixed(2));//总金额
// 					$('#sum_amount').text(Math.round((Number($('#sum_amount').text())-Number(row.find('td:eq(5)').text()))*100)/100);//总数量
				},
				onEnter:function(data){
					var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
					if(pos == 2){
						if($.trim(data.curobj.closest('td').prev().text())){
							data.curobj.find('span').html(getName());
							return;
						}
					 	else if(!data.actionobj){
							$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),2);
							return;
						} 
					}
					$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
					function getName(){
						var name='';
						$.ajaxSetup({ 
							  async: false 
							});
						$.get("<%=path %>/supply/findById.do",
								{sp_code:$.trim(data.curobj.closest('td').prev().text())},
								function(data){
									name =  data.sp_name;
								});
						return name;
					};
				},
				cellAction:[{
					index:2,
					action:function(row){
						$.fn.autoGrid.setCellEditable(row,4);
					},
					onCellEdit:function(event,data,row){
						data['url'] = '<%=path%>/supply/findTop1.do';
						if (data.value.split(".").length>2) {
							data['key'] = 'sp_code';
						}else if(!isNaN(data.value)){
							data['key'] = 'sp_code';
						}else{
							data['key'] = 'sp_init';
						}
						data.sp_position = $('#firm').val();
						$.fn.autoGrid.ajaxEdit(data,row);
					},
					resultFormat:function(data){
						return data.sp_init+'-'+data.sp_code+'-'+data.sp_name;
					},
					afterEnter:function(data2,row){
						var num=0;
						$('.grid').find('.table-body').find('tr').each(function (){
							if($(this).find("td:eq(1)").text()==data2.sp_code){
								num=1;
							}
						});
						if(num==1){
							showMessage({
 								type: 'error',
 								msg: '物资重复添加！',
 								speed: 1000
 							});
						}
						//查询价格
						if($('#firmDes').val()==''){
							showMessage({
 								type: 'error',
 								msg: '请先选择申购仓位！',
 								speed: 1000
 							});
						}else{
							//判断是取报价还是售价 出库售价 入库报价
							$.ajax({
								type:"POST",
								url:"<%=path%>/positn/findTypByPositn.do?code="+$('#firmDes').val(),
								dataType: "json",
								success:function(data){//默认入库取报价
									var url ="<%=path%>/supply/findBprice.do";
									var data1 = "sp_code="+data2.sp_code+"&area="+$('#firmDes').val()+"&madet="+$('#maded').val();
									if(data=='CK'){//出库取售价
										 url ="<%=path%>/supply/findSprice.do";
										 data1 = "supply.sp_code="+data2.sp_code+"&area.code="+$('#firmDes').val()+"&madet="+$('#maded').val();
									}
									$.ajax({
										type: "POST",
										url: url,
										data: data1,
										dataType: "json",
										success:function(spprice){
											row.find("td:eq(1) span").text(data2.sp_code);
											row.find("td:eq(2) span input").val(data2.sp_name).focus();
											row.find("td:eq(3) span").text(data2.sp_desc);
											row.find("td:eq(4) span").text(0).css("text-align","right");
											row.find("td:eq(5) span").text(data2.unit);
											row.find("td:eq(6) span").text(spprice.price).attr("price",spprice.price).css("text-align","center");
											row.find("td:eq(7) span").text(0).css("text-align","center");
											row.find("td:eq(8) span").text(0).css("text-align","right");
											row.find("td:eq(9) span").text(data2.unit1);
											row.find("td:eq(10) span").text(data2.receiveDate);
											row.find("td:eq(11) span").text();
											row.data("unitper",data2.unitper);
											row.data("mincnt",data2.mincnt);
											row.find("td:eq(12) span").text(0).css("text-align","right");
										}
									});
								}
							});
							
							
							//查询售价结束
						}
					}
				},{
					index:4,
					action:function(row,data2){
						if(Number(data2.value) == 0){
							alert('<fmt:message key="number_cannot_be_zero"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(data2.value) < 0){
							alert('数量不能为负数！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(isNaN(data2.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							row.find("td:eq(4)").find('span').text(data2.ovalue);
							$.fn.autoGrid.setCellEditable(row,4);
						}else if(Number(row.data("mincnt"))!=0 && !(/(^[1-9]\d*$)/.test(Number(data2.value)/Number(row.data("mincnt"))))){
							alert('该物质最低请购数量为'+row.data("mincnt")+"，请购量必须是最小请购量的整数倍!");
							row.find("td:eq(4)").find('span').text(data2.value);
							$.fn.autoGrid.setCellEditable(row,4);
						}
						else {
							//$('#sum_amount').text(Number($('#sum_amount').text())+Number(row.find("td:eq(4)").text())-data2.ovalue);//总数量
							var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(7)').text());
							row.find("td:eq(7) span").text((Number(row.find("td:eq(4)").text())*Number(row.find("td:eq(6)").text())).toFixed(2));
							row.find("td:eq(8) span").text((Number(row.find("td:eq(4)").text())*Number(row.data("unitper"))).toFixed(2));
							Sprice=Sprice+Number(row.find('td:eq(7)').text());//总金额
							$('#sum_price').text(Sprice.toFixed(2));//总金额
							$.fn.autoGrid.setCellEditable(row,10);
						}
					}
				},{
					index:8,
					action:function(row,data){
						if(isNaN(data.value)){
							alert('<fmt:message key="number_be_not_number"/>！');
							$.fn.autoGrid.setCellEditable(row,8);
						}else if((Number(data.value)/Number(row.data("unitper"))).toFixed(2)<Number(row.data("mincnt"))){
							alert((Number(data.value)/Number(row.data("unitper"))).toFixed(2));
							alert('该物质参考单位数量最小为'+row.data("mincnt")*Number(row.data("unitper"))+'!');
							row.find("td:eq(8) span").text(data.value);
							$.fn.autoGrid.setCellEditable(row,8);
						}else{
							$.fn.autoGrid.setCellEditable(row,10);
							row.find("td:eq(4) span").text((Number(row.find("td:eq(8)").text())/Number(row.data("unitper"))).toFixed(2));
						}
					}
				},{
					index:10,
					action:function(row,data){
						$.fn.autoGrid.setCellEditable(row,11);
					},CustomAction:function(event,data){
						var input = data.curobj.find('span').find('input');
						input.addClass("Wdate text");
						input.bind('click',function(){
							input.unbind('keyup');
							new WdatePicker({
								startDate:'%y-%M-{%d+1}',
								el:'input',
								onpicked:function(dp){
									data.curobj.find('span').html(dp.cal.getNewDateStr());
									$.fn.autoGrid.setCellEditable(data.row,11);
								}
							});
						});
						input.trigger('click');
					}
				},{
					index:11,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				},{
					index:12,
					action:function(row,data){
						if(!row.next().html())$.fn.autoGrid.addRow();
						$.fn.autoGrid.setCellEditable(row.next(),2);
						$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
					}
				}]
			});
		}
		
		//新增请购单
		function addPraybillm(){			
			window.location.replace("<%=path%>/praybillm/addPraybillm.do");	
		}	
		
		//保存添加
		function savePraybillm(){
			var indNull=0;//默认0代表成功
			var numNull=0;//默认0代表成功
			var isNull=0;
			$('.table-body').find('tr').each(function(){
				if($(this).find('td:eq(1)').text()!=''){
					isNull=1;
					var amount=$(this).find('td:eq(4)').text()?$(this).find('td:eq(4)').text():$(this).find('td:eq(4) input').val();
					var amount1=$(this).find('td:eq(8)').text()?$(this).find('td:eq(8)').text():$(this).find('td:eq(8) input').val();
					var ind=$(this).find('td:eq(10)').text()?$(this).find('td:eq(10)').text():$(this).find('td:eq(10) input').val();
					if(Number(amount)==0||Number(amount1)==0){
						numNull=1;//1数量为0
					}
					if(amount=="" || amount==null || isNaN(amount) || amount1=="" || amount1==null || isNaN(amount1)){
						numNull=2;//2数量为空
					}
					if(ind==''||ind==null){
						indNull=1;//1日期为空
					}
				}
			});
			if(Number(isNull)==0){
				alert('<fmt:message key ="not_add_empty_document" />！');
				return;
			}
			if(Number(numNull)==1){//数量不为0
				alert('<fmt:message key="number_cannot_be_zero"/>！');
				return;
			}
			if(Number(numNull)==2){//数量为空或字母
				alert('数量不是有效数字！');
				return;
			}
			if(Number(indNull)==1){//日期为空
				alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
				return;
			}
			var keys = ["supply.sp_code","supply.sp_name","supply.sp_desc","amount","supply.unit",
			            "price","totalAmt","amount1","supply.unit1","hoped","memo","iszs"];
			var data = {};
			var i = 0;
			$("#listForm *[name]").each(function(){
				var name = $(this).attr("name"); 
				if(name) data[name] = $(this).val();
			});
			var rows = $(".grid .table-body table tr");
			for(i=0;i<rows.length;i++){
				var status=$(rows[i]).find('td:eq(0)').text();
				data["praybilldList["+i+"]."+"price"] = $(rows[i]).data('price') ? $(rows[i]).data('price'):$('#price_'+status).val();
				cols = $(rows[i]).find("td");
				var j = 0;
				for(j=1;j <= keys.length;j++){
					var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
					value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
					if(value)
						data["praybilldList["+i+"]."+keys[j-1]] = value;
				}
			}

			//提交并返回值，判断执行状态
			$.post("<%=path%>/praybillm/saveByAddOrUpdate.do?sta="+$("#sta").val(),data,function(data){
				var rs = eval('('+data+')');
				switch(Number(rs)){
				case -1:
					alert('<fmt:message key="save_fail"/>！');
					break;
				case 1:
					showMessage({
								type: 'success',
								msg: '<fmt:message key="save_successful"/>！',
								speed: 3000
								});
					loadToolBar([true,true,true,true,false,true,true,true]);
					$("#sta").val("show");
					break;
				}
			});			 
		}

		//查询请购单
		function searchPraybillm(){
			var curwindow = $('body').window({
				id: 'window_searchPraybillm',
				title: '查询请购单',
				content: '<iframe id="searchPraybillmFrame" frameborder="0" src="<%=path%>/praybillm/searchByKey.do?init=init"></iframe>',
				width: 800,
				height: 430,
				draggable: true,
				isModal: true
			});
			curwindow.max();
		}	

		//删除请购单
		function deletePraybillm(){
			var praybillmNo=$("#praybillmNo").val();
			if($("#praybillmNo").val()!=null && $("#praybillmNo").val()!="" && $("#praybillmNo").val()!=0){
				if($("#checby").val()!=null && $("#checby").val()!=""){
					alert('<fmt:message key="orders_audited_cannot_deleted"/>');
					return false;
				}
				if($("#sta").val()!="show"){
					alert('<fmt:message key="orders_unsaved_cannot_deleted"/>！');
				}else{
					if(confirm('<fmt:message key="whether_delete_orders"/>？')){
						//提交并返回值，判断执行状态
						$.post("<%=path%>/praybillm/deletePraybillm.do?praybillmNo="+praybillmNo,function(data){
							var rs = eval('('+data+')');
							switch(Number(rs.pr)){
							case 0:
								alert('<fmt:message key="orders_unsaved_cannot_audited"/>！');
								break;
							case 1:
								alert('<fmt:message key="successful_deleted"/>！');
								window.location.replace("<%=path%>/praybillm/table.do");
								break;
							}
						});
					}						
				}
			}
		}
		
		//动态下拉列表框
		function findByDes(e){
	    	$('#firmDes').val($(e).val());
		}
	
		//审核
		function checkPraybillm(){
			var data = {};
	        data['code'] = $('#firm').val();	    
	        data['praybillmNo'] = $('#praybillmNo').val();	    
			//判断价格是否足够
// 			var msg="ok";
<%-- 	    	$.post("<%=path%>/praybillm/checkSppriceSale.do",data,function(data){  --%>
// 				var rs = eval('('+data+')');
// 				if(rs.pr==1){
// 					msg="no";
// 				}
// 			});
// 			if("no"==msg){
// 				alert("分店可用金额不足，请上缴预付款！");
// 				return;
// 			}
			
			
			var praybillmNo = $("#praybillmNo").val();
			if(praybillmNo != null && praybillmNo != ""){
				//提交并返回值，判断执行状态
				$.post("<%=path%>/praybillm/checkPraybillm.do?praybillmNo="+praybillmNo,function(data){
					var rs = eval('('+data+')');
					switch(Number(rs.pr)){
					case -1:
						alert('<fmt:message key="data_unsaved"/>！');
						break;
					case 1:
						$('#showChecby').text(rs.checby);
						showMessage({
							type: 'success',
							msg: "<fmt:message key="check_successful"/>！",
							speed: 3000
						});
						loadToolBar([true,true,true,false,false,false,true,false]);
						break;
					case 0:
						alert('<fmt:message key="cannot_repeat_audit"/>！');
						break;
					}
				});
			}
		}
		
		//清空页面
		function clearValue(){
			$(':input','#listForm')  
			 .not(':button, :submit, :reset, :hidden')  
			 .val('')  
			 .removeAttr('checked')  
			 .removeAttr('selected');  
			$("#sta").val("show");
		}
		
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//获取系统时间
		function getInd(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate()+1;
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//打印单据
		function printPraybillm(){ 
			if(null!=$("#praybillmNo").val() && ""!=$("#praybillmNo").val()) {
				window.open("<%=path%>/praybillm/printPraybillm.do?praybillmNo="+$("#praybillmNo").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		}
		//双击打开详细
		function openPraybillm(praybillmNo){
			window.location.replace("<%=path%>/praybillm/findPraybill.do?praybillmNo="+praybillmNo);
		}
		function validateByMincnt(index,row,data2){//最小申购量判断
			if(index=="4"){
				if(Number(data2.value) == 0){
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					row.find("input").focus();
				}else if(Number(data2.value)<Number(row.data("mincnt"))){
					alert('该物质最低申购数量为'+row.data("mincnt")+"!");
					row.find("input").focus();
				}
			}else if(index=="8"){
				if((Number(data2.value)/Number(row.data("unitper"))).toFixed(2)<Number(row.data("mincnt"))){
					alert('该物质参考单位数量最小为'+row.data("mincnt")*Number(row.data("unitper"))+'!')
					row.find("input").focus();
				}
			}
		}
		function validator(index,row,data2){//输入框验证
			if(index=="4"){
				if(Number(data2.value) < 0){
					alert('数量不能为负数！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(4)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,4);
				}else {
					if(isNaN(Number(row.data("unitper")))){
						row.data("unitper",Number(data2.value)/data2.ovalue);
					}
					$('#sum_amount').text(Number($('#sum_amount').text())+Number(data2.value)-Number(data2.ovalue));//总数量
					row.find("input").data("ovalue",data2.value);
					var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(7)').text());
					row.find("td:eq(7) span").text((Number(data2.value)*Number(row.find("td:eq(6)").text())).toFixed(2));
					row.find("td:eq(8) span").text((Number(data2.value)*Number(row.data("unitper"))).toFixed(2));
					Sprice=Sprice+Number(row.find('td:eq(7)').text());//总金额
					$('#sum_price').text(Sprice.toFixed(2));//总金额
				}
			}else if(index=="8"){
				if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(8)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,8);
				}else{
					row.find("td:eq(4) span").text((Number(data2.value)/Number(row.data("unitper"))).toFixed(2));
				}
			}else if(index=="12"){
				if(isNaN(data2.value)){
					alert('<fmt:message key="number_be_not_number"/>！');
					row.find("td:eq(12)").find('span').text(data2.ovalue);
					$.fn.autoGrid.setCellEditable(row,12);
				}else{
				}
			}
		}
		//统一修改到货日期
		function setHoped(){
			$('.table-body').find('tr').each(function(){
				if ($(this).find('td:eq(1)').text()!=null&&$(this).find('td:eq(1)').text()!='') {
					$(this).find('td:eq(10)').find('span').text($('#hoped').val());
				}
			});
		}
		</script>			
	</body>
</html>