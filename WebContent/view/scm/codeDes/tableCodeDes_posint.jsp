<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="system_encoding" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 25px;}
		</style>
	</head>	
	<body>
		<div class="tool"></div>
		<input type="hidden" id="str" name="str" value="${str }"/>
		<input type="hidden" id="isornot" name="isornot" value="${isornot}"/>
		<!-- 父类编码 -->
		<form id="listForm" action="<%=path%>/codeDes/table.do" method="post">
		<input type="hidden" id="typ_${codeDes.code}" name="pageTyp" value="${pageTyp}"/>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:80px;"><fmt:message key="system_encoding" /></span></td>
								<td><span style="width:100px;"><fmt:message key="encoding_name" /></span></td>
								<c:if test="${pageTyp == 18 }">
									<td><span style="width:100px;"><fmt:message key="type" /></span></td>
								</c:if>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="codeDes" items="${codeDesList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;"><span style="width:25px;">${status.index+1}</span></td>
									<td><span title="${codeDes.code}"  style="width:80px;">${codeDes.code}</span></td>
									<td><span title="${codeDes.des}" style="width:100px;">${codeDes.des}</span></td>
									<c:if test="${pageTyp == 18 }">
										<td><span title="${codeDes.des}" style="width:100px;">
											<c:if test="${codeDes.codetyp == 'RK'}"><fmt:message key="storage"/><fmt:message key="document_types"/></c:if>
											<c:if test="${codeDes.codetyp == 'CK'}"><fmt:message key="library"/><fmt:message key="document_types"/></c:if>
											<c:if test="${codeDes.codetyp == 'ZF'}"><fmt:message key="straight_hair"/><fmt:message key="document_types"/></c:if>
										</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />			
		</form>			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">		
		//工具栏
			$(document).ready(function(){
				if($("#typ_${codeDes.code}").val()=="root"){
					loadToolBar([false,false,false]);
				}else{
					loadToolBar([true,true,true]);
				}
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			   $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');								
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});	
		
		 	//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '',
							title: '',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){								
								if($("#typ").val()=="root"){	
									alert('<fmt:message key="not_allowed_added_items" />');	
									return;
								}else{
									saveCodeDes();
								}
							}
						}
					]
				});
		 	}	
		
			//新增系统编码
			function saveCodeDes(){
				$('body').window({
					id: 'window_codeDes',
					title: '<fmt:message key="add_systemcoding_information" />',
					content: '<iframe id="saveCodeDesFrame" name="saveCodeDesFrame" frameborder="0" src="<%=path%>/codeDes/add.do?typ=${pageTyp}"></iframe>',
					width: '550px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '',
								title: '',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
							}]
					}
				});
			}			
			
			//修改系统编码信息
			function updateCodeDes(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() ==1){
							var chkValue = [];
							var typValue = [];
							checkboxList.filter(':checked').each(function(){
								chkValue.push($(this).val());
								typValue.push($('#typ_'+$(this).val()).val());
							});
							
						$('body').window({
						title: '<fmt:message key="modify_systemcoding_information" />',
						content: '<iframe id="updateCodeDesFrame" name="updateCodeDesFrame" frameborder="0" src="<%=path%>/codeDes/update.do?code='+chkValue+"&typ="+typValue+'"></iframe>',
						width: '550px',
						height: '380px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="modify_systemcoding_information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateCodeDesFrame')&&window.document.getElementById("updateCodeDesFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('updateCodeDesFrame','CodeDesForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="modify_cancelled" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size() > 1){
					alert('<fmt:message key="please_select_data" />！');
					return ;
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除系统编码信息
			function deleteCodeDes(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');	
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key="delete_data_confirm" />？")){
						var chkValue = [];
						var typValue = '';
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							typValue=$(this).parent().next().val();
						});
						if(getAjaxReturn(typValue,chkValue.join(","))){
							var action = '<%=path%>/codeDes/delete.do?typ='+typValue+'&&code='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete_systemcoding_information" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '500px',
								height: '245px',
								draggable: true,
								isModal: true
							});
						} else {
							alert("<fmt:message key='the_information_has_been_quoted'/>，<fmt:message key='can_not'/><fmt:message key='delete'/>！");
						}
					};
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			};
			//ajax 校验当前信息是否被引用
			function getAjaxReturn(typ,code){
				var flag = false;
				$.ajax({
					url:'<%=path%>/codeDes/deleteyh.do?typ='+typ+'&&code='+code,
					type:"POST",
					async:false,
					success:function(data){
						if(data == 0){
							flag = true;
						}
					},
				});
				return flag;
			}
			function pageReload(){
		    	$('#listForm').submit();
			}
		</script>
	</body>
</html>