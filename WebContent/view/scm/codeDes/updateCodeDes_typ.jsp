<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="modify_systemcoding_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	</head>
	<body>
		<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:0px;">
			<form id="CodeDesForm_typ" method="post" action="<%=path %>/codeDes/saveByUpdate_typ.do">
				<input type="hidden" id="oldCode" name="oldCode" value="${codeDes.code}"/>
				<input type="hidden" id="typ" name="typ" value="${codeDes.typ}" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="number" />：</div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" value="${codeDes.code}" disabled="disabled"/>
					</div>
				</div><br/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="des" name="des" class="text" value="${codeDes.des}" />
					</div>
				</div><br/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="procurement_lead_time" />：</div>
					<div class="form-input">
						<input type="text" id="days" name="days" class="text" value="${codeDes.days}" />
					</div>
				</div><br/>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="dailygoodsway" />：</div>
					<div class="form-input">
						<select name="codetyp" id="codetyp" value="${codeDes.codetyp}" style="width: 128px;"> 
							<option value="4"<c:if test="${codeDes.codetyp == '4'}">selected="selected"</c:if>><fmt:message key ="Safety_stock" /></option> 
							<option value="1"<c:if test="${codeDes.codetyp == '1'}">selected="selected"</c:if>><fmt:message key ="misboh_DeclareGoodsGuide_ThousandsYuanOfDosage" /></option> 
							<option value="6"<c:if test="${codeDes.codetyp == '6'}">selected="selected"</c:if>><fmt:message key ="Sales_plan" /></option> 
<%-- 							<option value="7"<c:if test="${codeDes.codetyp == '7'}">selected="selected"</c:if>>菜品点击率</option> --%>
							<!-- wangjie 2014年11月20日 14:20:49 -->
							<option value="3"<c:if test="${codeDes.codetyp == '3'}">selected="selected"</c:if>><fmt:message key ="Purchase_template" /></option>
							<option value="2"<c:if test="${codeDes.codetyp == '2'}">selected="selected"</c:if>><fmt:message key ="Directly_fill_in_purchase_order" /></option>
							<option value="8"<c:if test="${codeDes.codetyp == '8'}">selected="selected"</c:if>><fmt:message key ="Periodic_average" /></option>
<%-- 							<option value="5"<c:if test="${codeDes.codetyp == '5'}">selected="selected"</c:if>>千人用量</option> --%>
						</select> 
					</div>
				</div><br/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="safety_coefficient" />：</div>
					<div class="form-input">
						<input type="text" id="safetyvalue" name="safetyvalue" class="text" value="${codeDes.safetyvalue}" />
					</div>
				</div><br/>
				<div class="form-line">
					<div class="form-label"><span class="red"></span><fmt:message key="sort_column" />：</div>
					<div class="form-input"><input type="text" id="beatsequence" name="beatsequence" class="text" value="${codeDes.beatsequence}"/></div>
				</div>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				$("#des").focus();//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
				
			 	//回车输入
		 	 	$('input:text:eq(1)').focus();
		        var $inp = $('input:text');
		        $inp.bind('keydown', function (e) {
		            var key = e.which;
		            if (key == 13) {
		                e.preventDefault();
		                var nxtIdx = $inp.index(this) + 1;
		                $(":input:text:eq(" + nxtIdx + ")").focus();
		            }
		        });
				
			 	$("#des").blur(function(){
					$.post('<%=path %>/codeDes/getDes.do',{typ:$("#typ").val(),des:$("#des").val(),code:$("#code").val()},
						  function(data){
							  if(data==1){
								  alert("<fmt:message key ="Name_already_exists" />");
								  $("#des").val("");
							  }
						  
						});
					
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull','num'],
						param:['F','T'],
						error:['<fmt:message key ="coding" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="encoding_name" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="encoding_name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'days',
						validateType:['num1'],
						param:['F'],
						error:['<fmt:message key ="stay_only_as_an_integer" />！']
					},{
						type:'text',
						validateObj:'safetyvalue',
						validateType:['num1'],
						param:['F'],
						error:['<fmt:message key ="safety_value_only_for_integer" />！']
					},{
						type:'text',
						validateObj:'beatsequence',
						validateType:['canNull','handler'],
						handler:function(){
							var result = true;
							var beatsequence = $('#beatsequence').val();
							if(beatsequence < 1){
								result = false;
							}
							return result;
						},
						param:['F','F'],
						error:['<fmt:message key ="Sort_columns_can_not_be_empty" />！','<fmt:message key ="Sort_columns_should_be_greater_than_0" />！']
					}]
				});
			});
		</script>
	</body>
</html>