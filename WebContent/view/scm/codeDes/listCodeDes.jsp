<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="system_encoding_message" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">			
			</style>
		</head>
	<body>
	<div class="leftFrame">
		<div id="toolbar"></div>
	    <div class="treePanel">
			<script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
			<script type="text/javascript">
				var tree = new MzTreeView("tree");
				tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="system_encoding" />;method:changeUrl("0","root")';
	      			tree.nodes['00000000000000000000000000000000_0'] = 'text:<fmt:message key="supplyunit"/>; method:changeUrl("1","0")';
	      			tree.nodes['00000000000000000000000000000000_15'] = 'text:<fmt:message key="supplyattr" />; method:changeUrl("1","15")';
	      			//增加物资参考类别 （鱼酷) wangjie 2014年12月22日 13:55:51
	      			tree.nodes['00000000000000000000000000000000_16'] = 'text:<fmt:message key="supplyCanType" />; method:changeUrl("1","16")';
	      			tree.nodes['00000000000000000000000000000000_13'] = 'text:<fmt:message key="scrapreason" />; method:changeUrl("1","13")';
	      			tree.nodes['00000000000000000000000000000000_11'] = 'text:<fmt:message key="supplyclassification" />; method:changeUrl("1","11")';
	      			tree.nodes['00000000000000000000000000000000_14'] = 'text:<fmt:message key="purchasedirection" />; method:changeUrl("1","14")';
	      			tree.nodes['00000000000000000000000000000000_12'] = 'text:<fmt:message key="positntype" />; method:changeUrl("1","12")';
	      			tree.nodes['00000000000000000000000000000000_17'] = 'text:<fmt:message key="shelves" />; method:changeUrl("1","17")';
	      			tree.nodes['00000000000000000000000000000000_1'] = 'text:<fmt:message key="delivertype" />; method:changeUrl("1","1")';
	      			tree.nodes['00000000000000000000000000000000_9'] = 'text:<fmt:message key="costtype" />; method:changeUrl("1","9")';
	      			tree.nodes['00000000000000000000000000000000_2'] = 'text:<fmt:message key="makecost" />; method:changeUrl("1","2")';
	      			tree.nodes['00000000000000000000000000000000_3'] = 'text:<fmt:message key="psarea" />; method:changeUrl("1","5")';
	      			
	      			tree.nodes['00000000000000000000000000000000_4'] = 'text:<fmt:message key="brand" />; method:changeUrl("1","4")';
	      			tree.nodes['00000000000000000000000000000000_5'] = 'text:<fmt:message key="businessarea" />; method:changeUrl("1","3")';
// 	      			tree.nodes['00000000000000000000000000000000_6'] = 'text:加工制作人; method:changeUrl("1","6")';
// 	      			tree.nodes['00000000000000000000000000000000_7'] = 'text:司机; method:changeUrl("1","7")';
// 	      			tree.nodes['00000000000000000000000000000000_8'] = 'text:供货不合格类型; method:changeUrl("1","8")';
	      			tree.nodes['00000000000000000000000000000000_10'] = 'text:<fmt:message key="firmtype" />; method:changeUrl("1","10")';
	      			tree.nodes['00000000000000000000000000000000_18'] = 'text:<fmt:message key="documenttype" />; method:changeUrl("1","18")';
	      			
	      			//维护税率 jinshuai 20160428
	      			tree.nodes['00000000000000000000000000000000_19'] = 'text:<fmt:message key="tax_rate" />; method:changeUrl("1","19")';
	      			//附加项原因，验收出库，附加项的选择下拉
	      			tree.nodes['00000000000000000000000000000000_20'] = 'text:<fmt:message key="additional_items" /><fmt:message key="reason" />; method:changeUrl("1","20")';
	      			//不合格原因
	      			//tree.nodes['00000000000000000000000000000000_21'] = 'text:<fmt:message key="unqualified" /><fmt:message key="reason" />; method:changeUrl("1","21")';
	      			//付款方式
	      			tree.nodes['00000000000000000000000000000000_22'] = 'text:<fmt:message key="payments" /><fmt:message key="way" />; method:changeUrl("1","22")';
	      			
// 				<c:forEach var="codeDes" items="${codeDesList}" varStatus="status">
// 	      			tree.nodes['00000000000000000000000000000000_${codeDes.code}'] = 'text:${codeDes.code},${codeDes.des}; method:changeUrl("1","${codeDes.code}")';
// 	         	</c:forEach>
	        tree.setIconPath("<%=path%>/image/tree/none/");
				document.write(tree.toString());
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>   
    <input type="hidden" id="level" name="level" />
    <input type="hidden" id="code" name="code" />
    <input type="hidden" id="typ" name="typ" />		
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript">
		function changeUrl(level,code){
			$('#level').val(level);
			$('#code').val(code);			
		    window.mainFrame.location = "<%=path%>/codeDes/table.do?pageTyp="+code;
	    }			
	    function refreshTree(){
			window.location.href = '<%=path%>/codeDes/list.do';
	    }
	  //按钮快捷键
		focus() ;//页面获得焦点
	 	$(document).bind('keydown',function(e){
	 		if(e.keyCode==27){
	 			$('.<fmt:message key="quit" />').click();
	 		}
	 	});
		$(document).ready(function(){		
			var toolbar = $('#toolbar').toolbar({
				items: [
// 				    {
// 						text: '<fmt:message key="expandAll" />',
// 						title: '<fmt:message key="expandAll" />',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-160px','-80px']
// 						},
// 						handler: function(){
// 							tree.expandAll();
// 						}
// 					},
					{
						text: '<fmt:message key="refresh" />',
						title: '<fmt:message key="refresh" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							refreshTree();
						}
					}
				]
			});
			setElementHeight('.treePanel',['#toolbar']);
			window.mainFrame.location="<%=path%>/codeDes/table.do";
		});// end $(document).ready();
	</script>
	</body>
</html>