<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="modify_systemcoding_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	</head>
	<body>
		<input type="hidden" id="str" name="str" value="${str }"/>
		<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:50px;">
			<form id="CodeDesForm" method="post" action="<%=path %>/codeDes/saveByUpdate.do">
				<input type="hidden" id="oldCode" name="oldCode" value="${codeDes.code}"/>
				<c:if test="${fz == null or fz == '' }">
					<input type="hidden" id="typ" name="typ" value="${codeDes.typ}" />
				</c:if>
				<div class="form-line"></div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="number" />：</div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text" value="${codeDes.code}" disabled="disabled"/>
					</div>
				</div>
				<div class="form-line"></div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name" />：</div>
					<div class="form-input">
						<input type="text" id="des" name="des" class="text" value="${codeDes.des}" />
					</div>
				</div>
				<c:if test="${fz == null or fz == '' }">
					<div class="form-line"></div>
					<div class="form-line">
						<div class="form-label"><span class="red"></span><fmt:message key="sort_column" />：</div>
						<div class="form-input"><input type="text" id="beatsequence" name="beatsequence" class="text" value="${codeDes.beatsequence}"/></div>
					</div>
				</c:if>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
				
				if("desError"==$("#str").val()){
					alert("<fmt:message key='coding'/><fmt:message key='name'/><fmt:message key='already_exists' />！！");
					var t = $("#des").val();
					$("#des").val("").focus().val(t);
				}
				
			 	//回车输入
		 	 	$('input:text:eq(1)').focus();
		        var $inp = $('input:text');
		        $inp.bind('keydown', function (e) {
		            var key = e.which;
		            if (key == 13) {
		                e.preventDefault();
		                var nxtIdx = $inp.index(this) + 1;
		                $(":input:text:eq(" + nxtIdx + ")").focus();
		            }
		        });
				
			 	//解决即使编码存在也可以保存成功问题，删除失去焦点事件，在验证里面添加校验 wjf
// 			 	$("#des").blur(function(){
<%-- 					$.post('<%=path %>/codeDes/getDes.do',{typ:$("#typ").val(),des:$("#des").val(),code:$("#code").val()}, --%>
// 						  function(data){
// 							  if(data==1){
// 								  alert("编码名称已存在");
// 								  $("#des").val("");
// 							  }
						  
// 						});
					
// 				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="coding" /><fmt:message key="cannot_be_empty" />！']
					},
					
					
					
					
					//update by js at 20160323 除物资单位其它不改
					<c:if test="${typ != '0'}">
					{
						type:'text',
						validateObj:'code',
						validateType:['num'],
						param:['T'],
						error:['<fmt:message key="system_encoding" /><fmt:message key="please_fill_in_figures" />！']
					},
					</c:if>
					
					//update by js at 20160323 物资单位可以使用字母
					<c:if test="${typ == '0'}">
					{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							var result = false;
							var codestr = $('#code').val();
							var regExp = /^[0-9A-Za-z]+$/;
							if(regExp.test(codestr)){
								result = true;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="The_letter" /><fmt:message key="or" /><fmt:message key="digital" /><fmt:message key="validation" /><fmt:message key="failure" />！']
					},
					</c:if>
					
					
					
					
					
					{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="encoding_name" /><fmt:message key="cannot_be_empty" />！']
					},
					<c:if test="${fz == null or fz == '' }">
					{
						type:'text',
						validateObj:'beatsequence',
						validateType:['canNull','handler'],
						handler:function(){
							var result = true;
							var beatsequence = $('#beatsequence').val();
							if(beatsequence < 1){
								result = false;
							}
							return result;
						},
						param:['F','F'],
						error:['排序列不能为空！','排序列应该大于0！']
					},
					</c:if>
					{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:['10'],
						error:['<fmt:message key="encoding_name" /><fmt:message key="maximum_length" />10！']
					}]
				});
			});
		</script>
	</body>
</html>