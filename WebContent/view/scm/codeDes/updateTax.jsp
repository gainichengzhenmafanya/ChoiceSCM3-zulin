<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="add_systemcoding_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
    	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body onload="CodeDesForm.code.focus()">
		<input type="hidden" id="str" name="str" value="${str }"/>
		<div class="form">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:50px;">
				<form id="CodeDesForm" method="post" action="<%=path %>/codeDes/saveTaxByUpdate.do">
					<input type="hidden" id="id" name="id" value="${tax.id}"/>
					<div class="form-line"></div>
					<div class="form-line">
						<div class="form-label">
							<span class="red">*</span><fmt:message key="tax_rate" /><fmt:message key="gyszmmcs" />：
						</div>
						<div class="form-input">
							<input type="text" id="taxdes" name="taxdes" class="text" value="${tax.taxdes}"/>
						</div>
					</div>
					<div class="form-line"></div>
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="tax_rate" />：</div>
						<div class="form-input"><input type="text" id="tax" name="tax" class="text" value="${tax.tax}"/></div>
					</div>
					
				</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>				
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点			
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
				
				if("desError"==$("#str").val()){
					alert("<fmt:message key='coding'/><fmt:message key='name'/><fmt:message key='already_exists' />！！");
					var t = $("#des").val();
					$("#des").val("").focus().val(t);
				}
				if("taxError"==$("#str").val()){
					alert("<fmt:message key='tax_rate'/><fmt:message key='already_exists' />！！");
					var t = $("#des").val();
					$("#des").val("").focus().val(t);
				}
				

				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'taxdes',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="tax_rate" /><fmt:message key="gyszmmcs" /><fmt:message key="cannot_be_empty" />！']
					},
					{
						type:'text',
						validateObj:'tax',
						validateType:['num2'],
						param:['F'],
						error:['<fmt:message key="tax_rate" /><fmt:message key="please_fill_in_figures" />！']
					},{
						type:'text',
						validateObj:'tax',
						validateType:['maxValue'],
						param:[1],
						error:['<fmt:message key="tax_rate" /><fmt:message key="can_not_be_greater_than" />1！']
					}]
				});
			});
			//屏蔽空格
			$(document).keydown(function(event){
				if(event.keyCode==32){
					return false;
				}
			});
		</script>
	</body>
</html>