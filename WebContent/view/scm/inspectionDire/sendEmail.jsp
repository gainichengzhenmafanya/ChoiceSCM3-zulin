<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>直配按单据验收发邮件</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
	</head>
	<body>
		<form id="listForm" action="<%=path%>/inspectionDire/sendEmail.do" method="post">
			<p style="font-size:18px;color:red;width:100%;text-align:center;"><c:if test="${flag == 1 }">验收成功!</c:if>是否发送邮件给供应商:${arrivalm.deliverDes }?</p>
			<input type="hidden" id="pk_arrival" name="pk_arrival" value="${arrivalm.pk_arrival }"/>
			<div class="form-line">	
				<div class="form-label">收件人:</div>
				<div class="form-input">
					<input type="text" id="mail" name="mail" value="${arrivalm.mail }" style="width:400px;"/>
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label">主题:</div>
				<div class="form-input">
					<input type="text" id="title" name="title" value="九毛九${arrivalm.positnDes }${arrivalm.darrbilldate }验货单" style="width:400px;"/>
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label">正文:</div>
				<div class="form-input">
<textarea id="content" name="content" rows="2" cols="80">亲爱的${arrivalm.deliverDes }:
&nbsp;&nbsp;九毛九${arrivalm.positnDes }${arrivalm.darrbilldate }验货单<c:if test="${flag == 1 }">今日</c:if>已验收,物资详情见附件。</textarea>
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label">附件格式:</div>
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="reports"/><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:80px;">到货<fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${arrivaldList }" varStatus="status">
								<tr data-unitper="${dis.unitper }">
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td><span title="${dis.vcode }" style="width:65px;">${dis.vcode }</span></td>
									<td><span title="${dis.vname }" style="width:100px;">${dis.vname }</span></td>
									<td><span title="${dis.sp_desc }" style="width:80px;">${dis.sp_desc }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:80px;" title="<fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.pk_unit }" style="width:40px;">${dis.pk_unit }</span></td>
									<td><span style="width:40px;text-align:right;" title="<fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:60px;text-align:right" title="<fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.vmemo }" style="width:100px;">${dis.vmemo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'mail',
					validateType:['canNull','email'],
					param:['F','F'],
					error:['<fmt:message key="cannot_be_empty" />！','邮箱不正确！']
				},{
					type:'text',
					validateObj:'title',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'content',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="cannot_be_empty" />！']
				}
				]
			});
		});
		</script>
	</body>
</html>