<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.form-line .form-label {
				height: 25px;
				line-height: 25px;
				text-align: right;
				padding-right: 5px;
				width: 60px;
			}
			
			.form-line .form-input {
				height: 25px;
				line-height: 25px;
				text-align: left;
				padding-left: 5px;
				width: 130px;
				white-space: normal;
			}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/inspectionDireScm/listArrivalm.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>:</div>
				<div class="form-input"><input type="text" id="bdate" name="bdate" class="Wdate text" value="${arrivalm.bdate }" onclick="new WdatePicker()" style="width:100px;"/></div>						
				<div class="form-label">到货单单号:</div>
				<div class="form-input"><input type="text" id="varrbillno" name="varrbillno" class="text" value="${arrivalm.varrbillno}" style="width:100px;"/></div>
				<div class="form-label"><fmt:message key="supply_units"/>:</div>
				<div class="form-input">
					<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${arrivalm.deliverDes}" class="text" style="width:100px;"/>
					<input type="hidden" id="pk_supplier" name="pk_supplier" value="${arrivalm.pk_supplier}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_suppliers"/>' /> 
				</div>
				<div class="form-label"><fmt:message key="coding"/></div>
				<div class="form-input" style="margin-top: -3px;">
					<input type="text" name="sp_code" id="sp_code" style="width: 100px;" class="text" value="<c:out value="${arrivalm.sp_code}" />"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>'/>	
				</div>
				<div class="form-label">单据<fmt:message key="status"/>:</div>
				<div class="form-input">
					<select name="istate" style="margin-top:3px;"><!-- 0:未操作1已保存 2已确认 3已验货 -->
						<option value="" <c:if test="${empty arrivalm.istate }">selected="selected"</c:if>>全部</option>
						<option value="0" <c:if test="${arrivalm.istate == 0 }">selected="selected"</c:if>>门店未编辑</option>
						<option value="1" <c:if test="${arrivalm.istate == 1 }">selected="selected"</c:if>>供应商未确认(门店已编辑)</option>
						<option value="2" <c:if test="${arrivalm.istate == 2 }">selected="selected"</c:if>>供应商已确认</option>
						<option value="3" <c:if test="${arrivalm.istate == 3 }">selected="selected"</c:if>>已审核</option>
					</select>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>:</div>
				<div class="form-input">
					<input type="text" id="edate" name="edate" class="Wdate text" value="${arrivalm.edate }" onclick="new WdatePicker()" style="width:100px;"/>
				</div>
				<div class="form-label">报货单单号:</div>
				<div class="form-input"><input type="text" id="vbatchno" name="vbatchno" class="text" value="${arrivalm.vbatchno}" style="width:100px;"/></div>
				<div class="form-label"><fmt:message key="stores"/>:</div>
				<div class="form-input">
					<input type="text" id="positn_name" name="positnDes" readonly="readonly" value="${arrivalm.positnDes}" style="width:100px;"/>
					<input type="hidden" id="positn" name="pk_org" value="${arrivalm.pk_org}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label" style="margin-left:40px;">[
					<input type="radio" name="selectType" value="0" <c:if test="${arrivalm.selectType == 0 }">checked="checked"</c:if>/>按单据
					<input type="radio" name="selectType" value="1" <c:if test="${arrivalm.selectType == 1 }">checked="checked"</c:if>/>按物资
					<input type="radio" name="selectType" value="2" <c:if test="${arrivalm.selectType == 2 }">checked="checked"</c:if>/>按日期
				]</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:60px;"><fmt:message key="suppliers"/></span></td>
								<td><span style="width:150px;"><fmt:message key="suppliers_name"/></span></td>
								<td><span style="width:60px;"><fmt:message key="branches_encoding"/></span></td>
								<td><span style="width:100px;"><fmt:message key="branches_name"/></span></td>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:80px;"><fmt:message key="reports"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:80px;">到货<fmt:message key="quantity"/></span></td>
								<td><span style="width:80px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:80px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:60px;"><fmt:message key="amount"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="am" items="${arrivalmList }" varStatus="status">
								<tr <c:if test="${am.narrnum != am.ninsnum }"> style="color:red;" </c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${am.pk_arrival }" value="${am.pk_arrival }"/></span></td>									
									<td><span style="width:60px;" title="${am.pk_supplier }">${am.pk_supplier }</span></td>
									<td><span style="width:150px;" title="${am.deliverDes }">${am.deliverDes }</span></td>
									<td><span style="width:60px;" title="${am.pk_org }">${am.pk_org }</span></td>
									<td><span style="width:100px;" title="${am.positnDes }">${am.positnDes }</span></td>
									<td><span title="${am.vcode }" style="width:65px;">${am.vcode }</span></td>
									<td><span title="${am.vname }" style="width:100px;">${am.vname }</span></td>
									<td><span title="${am.sp_desc }" style="width:80px;">${am.sp_desc }</span></td>
									<td><span title="<fmt:formatNumber value="${am.npurnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${am.npurnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${am.narrnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${am.narrnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${am.ninsnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${am.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${am.narrnum - am.ninsnum}" type="currency" pattern="0.00"/>" style="width:80px;text-align:right"><fmt:formatNumber value="${am.narrnum - am.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${am.pk_unit }" style="width:40px;">${am.pk_unit }</span></td>
									<td><span style="width:40px;text-align:right;" title="<fmt:formatNumber value="${am.nprice}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${am.nprice}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:60px;text-align:right" title="<fmt:formatNumber value="${am.nmoney}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${am.nmoney}" type="currency" pattern="0.00"/></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
			});  
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: 'Excel',
					title: 'Excel',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						exportArrivalmSupply();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#pk_supplier').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('bdate');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/searchAllDeliver.do?defaultCode='+defaultCode),offset,$('#deliverDes'),$('#pk_supplier'),'900','500','isNull');
				}
			});
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					sta:'all',
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>',
					typn:'1203'
				});
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
		});
		
		//导出
		function exportArrivalmSupply(){
			$("#wait2").val('NO');//不用等待加载
			$("#listForm").attr("action","<%=path%>/inspectionDireScm/exportArrivalm.do");
			$('#listForm').submit();
			$("#wait2 span").html("数据导出中，请稍后...");
			$("#listForm").attr("action","<%=path%>/inspectionDireScm/listArrivalm.do");
			$("#wait2").val('');//等待加载还原
		}
		</script>				
	</body>
</html>