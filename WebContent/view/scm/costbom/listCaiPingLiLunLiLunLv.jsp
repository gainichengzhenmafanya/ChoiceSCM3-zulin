<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品理论利润率</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>								
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/costbom/listCaiPingLiLunLiLunLv.do" id="listForm" name="listForm" method="post">
		<div class="form-line">
			<div class="form-label"><fmt:message key="category"/>:</div>
			<div class="form-input">
				<select class="select" id="pgrp" name="pgrp" style="margin:1.5px 0px 0px -4px;">
					<option value=""></option>
					<c:forEach var="pubgrp" items="${pubgrpList}" varStatus="status">
						<option 
							<c:if test="${pubgrp.pubgrp==pubitem.pgrp}"> 
									   	selected="selected"
							</c:if> 
							 value="${pubgrp.pubgrp}">${pubgrp.code},${pubgrp.pgrpdes}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="abbreviation"/>:</div>
			<div class="form-input">
				<input type="text" name="init" id="init" class="text" value="${pubitem.init}" style="margin:1.5px 0px 0px -4px;" /> 
			</div>
			<div class="form-label"><fmt:message key ="pubitem" />:</div>
			<div class="form-input">
<%-- 				<input type="text" name="itcode"  id="itcode" class="text" value="${pubitem.itcode}"  style="margin:-2px 0px 0px -4px;"/>  --%>
<%-- 				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' /> --%>
<!-- 				<select class="select" id="vcode" name="itcode" style="width:133px"> -->
<!-- 				<option id="" value=""></option> -->
<%-- 				<c:forEach var="pubitem" items="${pubitemList}" varStatus="status" > --%>
<%-- 					<option value="${pubitem.itcode}">${pubitem.itdes}</option> --%>
<%-- 				</c:forEach> --%>
<!-- 			</select> -->
				<select class="select" id="vcode" name="itcode" style="width:133px">
					<option id="" value=""></option>
					<c:forEach var="pubitems" items="${pubitemList}" varStatus="status" >
						<option 
							<c:if test="${pubitem.itcode==pubitems.itcode}"> 
						  	 	selected="selected";
							</c:if>  
						id="${pubitems.itcode}" value="${pubitems.itcode}">${pubitems.itdes}</option>
					</c:forEach>
				</select>
			</div>
			
		</div>
		<table id="test" style="width:'100%';height:200px"
				title="<fmt:message key ="Theoretical_profit_rate_of_dishes" />" singleSelect="true" rownumbers="true" showFooter=true remoteSort="true"
				sortName="${exThCostAna.sortName}"
				sortOrder="${exThCostAna.sortOrder}"
				idField="itemid" >
			<thead>
				<tr>
					<th field="itcode" width="80px"><fmt:message key ="pubitem_code" /></th>
					<th field="itdes" width="140px"><fmt:message key ="pubitem_name" /></th>
					<th field="unit" width="40px"><fmt:message key ="unit" /></th>
					<th field="price" width="80px"><fmt:message key ="unit_price" /></th>
					<th field="cost" width="80px"><fmt:message key ="Cost_scm" /></th>
					<th field="liRun" width="80px"><fmt:message key ="Profit" /></th>
					<th field="baiFenBi" width="80px"><fmt:message key ="The_percentage" /></th>
					
				</tr>
			</thead>
		</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="query_scheduling_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('#listForm').submit();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
//datagrid  开始

 				$('#test').datagrid({
 					rowStyler:function(){
 						return 'line-height:11px';
 					}
 				});
// 				    url: null,
// 					title: 'DataGrid',
// 					width: '100%',
// 					height: 400,
// 					fitColumns: true,
// 					nowrap:false,
// 					rownumbers:true,
// 					showFooter:true,
// 					total:[20]        
// 					columns:[[
// 					          {title:'物资',colspan:5},
// 								{field:'price',title:'单价',rowspan:2,width:100},
// 								{field:'amt',title:'金额',rowspan:2,width:100},
// 								{field:'costamt',title:'理论成本',rowspan:2,width:100,align:'right'}
// 					          ],[
// 								{field:'sp_code',title:'产品编码',width:100},
// 								{field:'sp_name',title:'产品名称',width:100},
// 								{field:'sp_desc',title:'产品规格',width:100},
// 								{field:'unit',title:'单位',width:100},
// 								{field:'amount',title:'产量',width:100}
// 								]]
// 				});		
			//	alert(rows);
			//	var rows = {};

// 			var obj={"total":28,"rows":[
// 			                       {"sp_code":"AV-CB-01","sp_name":92.00,"status":"P","listprice":63.50,"attr1":"Adult Male","itemid":"EST-18"}
// 			                        ],"footer":[
// 			                        	{"sp_code":19.80,"listprice":60.40,"productid":"Average:"},
// 			                        	{"unitcost":198.00,"listprice":604.00,"productid":"Total:"}
// 			                        ]};
				var obj={};
				var rows = ${ListBean};
				//var footer = ${ListBean_total};
 				obj['rows']=rows;
 			//	obj['footer']=footer;
				$('#test').datagrid('loadData',obj);
				//排序开始
				$('.datagrid-header').find(".datagrid-cell").click(function (){
					return;
	                $('#sortName').val($(this).parent("td").attr("field"));
					if($('#sortOrder').val()=='asc'){
						$('#sortOrder').val('desc');
					}else{
						$('#sortOrder').val('asc');
					};
					$('#listForm').submit();
				});
				$('#_'+$('#sortName').val()).parents('.datagrid-cell').addClass('datagrid-sort-'+$('#sortOrder').val());
				//排序结束
					setElementHeight('.panel-body',['.tool'],$(document.body),100);
// 				$('.panel-body').height(800);
//datagrid  结束
				/*弹出树*/
				$('#grpdes').bind('focus.custom',function(e){
					var grp=$('#grp').val();
					if(!!!top.customWindow){
						var offset = getOffset('grpdes');
						top.custom('<fmt:message key="please_select_category"/>','<%=path%>/explan/selectGrp.do?grp='+grp,offset,$(this),$('#grp'),null,null,null,'150','300','isNull');
					}
				});
				$('#positnexdes').bind('focus.custom',function(e){
					var positnex=$('#positnex').val();
					if(!!!top.customWindow){
						var offset = getOffset('positnexdes');
						top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#positnex'),null,null,null,'150','300','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#itcode').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#itcode'));	
					}
				});
				$('#dat').bind('click',function(){
					new WdatePicker();
				});
				$('#datEnd').bind('click',function(){
					new WdatePicker();
				});
		
		}); 
			/*弹出物资树*/
			function supply_select(){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'));	
				}
				
			};
			function toSelectColumns(){
				$('body').window({
					title: '<fmt:message key="column_selection" />',
					content: '<iframe frameborder="0" src="<%=path%>/exAna/toColumnsChoose.do"></iframe>',
					width: '460px',
					height: '430px',
					isModal: true
				});
			}
		</script>
	</body>
</html>