<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>costbom Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			form .form-line .form-label{
				width: 9%;
			}
			form .form-line .form-input{
				width: 14%;
			}
			form .form-line .form-input input[type=text]{
				width:80%;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/costbom/saveOrUpdateOrDelCostbom.do" method="post"> 
		<!-- 加入隐藏内容 -->
		<div class="bj_head">
		<%-- curStatus 0:init;1:edit(E);2:add(A);3:show 4,delete(D)--%>
		<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${costbom.orderBy}" default="c.sp_code"/>" />
		<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${costbom.orderDes}" default="0000000000"/>" />
		<input  type="hidden" id="item" name="item" value="${costbom.item}" />
		<input  type="hidden" id="itcode" name="itcode" value="${costbom.itcode}" />
		<input  type="hidden" name="unit" id="costbom_unit" value="${costbom.unit_sprice}" />
<%-- 		<input  type="hidden" name="unit" id="costbom_unit" value="${costbom.unit}" /> --%>
		<input  type="hidden" name="itemNm" id="itemNm" value="" />
		<input  type="hidden" name="sp_exdes" id="sp_exdes" class="text" value="" />
		<%-- update by js at 20160324 存放左侧条件 在导出excel的时候使用 --%>
		<input type="hidden" name="lefttj" id="lefttj" value="" />
		<div class="form-line">
			<div class="form-label" style="font-weight:bold"><fmt:message key="sales_units_price"/>:</div>
			<div class="form-input" >
<!-- 				<input style="font-weight:bold" type="text" name="unit_sprice" id="unit_sprice" class="text" value="" readonly="readonly"/> -->
				<select name="unit_sprice" id="unit_sprice" class="select" onchange="onSearch(this)">
				</select>
			</div>
			<div class="form-label"><fmt:message key="coding"/>:</div>
			<div class="form-input">
				<input type="text" name="supply.sp_code" readonly="readonly" id="sp_code" class="text" value="${costbom.supply.sp_code }"/> 
			</div>
			<div class="form-label"><fmt:message key="name"/>:</div>
			<div class="form-input">
				<input type="text" name="supply.sp_name" id="sp_name" class="text" readonly="readonly"  value="${costbom.supply.sp_name }"/>
			</div>
					
									
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="the_cost_of_the_card_type"/>:</div>
			<div class="form-input" >
				<select class="select" id="mods" name="mods" onchange="onSearch(this)">
					<c:forEach var="mods" items="${modList}" varStatus="status">
						<option 
							<c:if test="${mods.code == costbom.mods}"> 
									   	selected="selected"
							</c:if> 
							 value="${mods.code}">${mods.code},${mods.des}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label"><fmt:message key="standard_unit"/>:</div>
			<div class="form-input" >
				<input type="text" name="supply.unit" id="unit" class="text" readonly="readonly"  value="${costbom.supply.unit}" />
			</div>
			<div class="form-label"><fmt:message key="reference_unit"/>:</div>
			<div class="form-input" >
				<input type="text" name="supply.unit1" id="unit1" class="text" readonly="readonly"  value="${costbom.supply.unit1}" />
			</div>						
			<div class="form-label"><fmt:message key="The_main_material"/>:</div>
			<div class="form-input">
				<input type="checkbox" name="status" id="status" value="Y" <c:if test="${costbom.status == 'Y' }">checked="checked"</c:if> style="margin-top:7px;"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="reclaimer_rate"/>:</div>
			<div class="form-input">
				<input type="text" name="exrate" id="exrate" class="text" value="<c:out value="${costbom.exrate}" default="1.0"/>"/>
			</div>	
			<div class="form-label"><fmt:message key="cost_quantity"/>:</div>
			<div class="form-input">
				<input type="text" name="excnt" id="excnt" class="text" value="${costbom.excnt}"/><label class="unit2"></label>
			</div>
			<div class="form-label"><fmt:message key="reference_number"/>:</div>
			<div class="form-input">
				<input type="text" name="cnt1" id="cnt1" class="text" value="${costbom.cnt1}" /><label class="unit1"></label>
			</div>
			<div class="form-label"><fmt:message key="whether"/><fmt:message key="Subtract"/>:</div>
			<div class="form-input">
				<input type="checkbox" name="iscost" id="iscost" value="Y" <c:if test="${costbom.iscost == 'Y' }">checked="checked"</c:if> style="margin-top:7px;"/>
			</div>
<%-- 			<div class="form-label"><fmt:message key="net_material_name"/>:</div> --%>
<!-- 			<div class="form-input"> -->
<%-- 				<input type="text" name="sp_exdes" id="sp_exdes" class="text" value="${costbom.sp_exdes}" /> --%>
<!-- 			</div>	 -->
		</div>
		</div>
		</form>
		<div class="grid">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span style="width:21px;">&nbsp;</span></td>
							<td colspan="5"><span style="width:260px;"><fmt:message key="supplies"/></span></td>
							<td colspan="3"><span style="width:135px;"><fmt:message key="cost_unit"/></span></td>
							<td colspan="4"><span style="width:150px;"><fmt:message key="standard_unit"/></span></td>
							<td colspan="2"><span style="width:95px;"><fmt:message key="reference_unit"/></span></td>
							<td style="display: none;" rowspan="2"><span style="width:50px;"><fmt:message key="net_material_name"/></span></td>
							<td rowspan="2"><span style="width:50px;"><fmt:message key="The_main_material"/></span></td>
							<td rowspan="2"><span style="width:50px;"><fmt:message key="whether"/><fmt:message key="Subtract"/></span></td>
						</tr>
						<tr>
							<td><span style="width:21px;">&nbsp;</span></td>
							<td><span style="width:20px;">
<!-- 								<input type="checkbox" id="chkAll"/>--></span> 
							</td>
							<td><span style="width:70px;"><fmt:message key="supplies_code"/>&nbsp;</span></td>
							<td><span style="width:90px;"><fmt:message key="supplies_name"/>&nbsp;</span></td>
							<td><span style="width:40px;"><fmt:message key="specification"/>&nbsp;</span></td>
							<td><span style="width:40px;"><fmt:message key="reclaimer_rate"/>&nbsp;</span></td>
							
							<td><span style="width:35px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:50px;"><fmt:message key="net_usage"/>&nbsp;</span></td>
							<td><span style="width:50px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
							
							<td><span style="width:30px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="unit_price"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="amount"/>&nbsp;</span></td>
							
							<td><span style="width:35px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
						</tr>
					</thead>
				</table>
			</div> 
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:set var="totalSum" value="0"></c:set>
						<c:forEach var="costbom" varStatus="step" items="${costbomList}">
							<tr>
								<td class="num"><span style="width:21px;">${step.count}</span></td>
								<td style="width:30px; text-align: center;">
									<input type="checkbox" name="idList" id="chk_${costbom.acct}" value="${costbom.acct}"/>
								</td>
								<td><span title="${costbom.supply.sp_code}" style="width:70px;">${costbom.supply.sp_code}&nbsp;</span></td>
								<td><span title="${costbom.supply.sp_name}" style="width:90px;">${costbom.supply.sp_name}&nbsp;</span></td>
								<td><span title="${costbom.supply.sp_desc}" style="width:40px;">${costbom.supply.sp_desc}&nbsp;</span></td>
								<td><span title="${costbom.exrate}" style="width:40px;text-align: right;"><fmt:formatNumber value="${costbom.exrate}" pattern="0.00"></fmt:formatNumber></span></td>
								
								<td><span title="${costbom.supply.unit2}" style="width:35px;">${costbom.supply.unit2}&nbsp;</span></td>
								<td><span title="${costbom.excnt}" style="width:50px;text-align: right;"><fmt:formatNumber value="${costbom.excnt}" pattern="0.00"></fmt:formatNumber></span></td>
								<td><span title="${costbom.cnt2}" style="width:50px;text-align: right;" ><fmt:formatNumber value="${costbom.cnt2}" pattern="0.00"></fmt:formatNumber></span></td>
								
								<td><span title="${costbom.supply.unit}" style="width:30px;">${costbom.supply.unit}&nbsp;</span></td>
								<td><span title="${costbom.cnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.cnt}" pattern="0.00"></fmt:formatNumber></span></td>
								<td><span title="${costbom.supply.sp_price}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.supply.sp_price}" pattern="0.00"></fmt:formatNumber></span></td>
								<td><span title="${costbom.supply.amt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.supply.amt}" pattern="0.00"></fmt:formatNumber></span></td>
								
								<td><span title="${costbom.supply.unit1}" style="width:35px;">${costbom.supply.unit1}&nbsp;</span></td>
								<td><span title="${costbom.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.cnt1}" pattern="0.00"></fmt:formatNumber></span></td>
								
								<td style="display: none;"><span title="${costbom.sp_exdes}" style="width:50px;text-align: right;">${costbom.sp_exdes}&nbsp;</span></td>
								<td style="display: none;"><input type="hidden" id="mods" style="" value="${costbom.mods}"/></td>
								<td><span title="${costbom.status}" style="width:50px;">${costbom.status}&nbsp;</span></td>
								<td><span title="${costbom.iscost}" style="width:50px;">${costbom.iscost}&nbsp;</span></td>
							</tr>
							<c:set var="totalSum" value="${totalSum+costbom.supply.amt }"></c:set>
						</c:forEach>
					</tbody>
					<c:if test="${totalSum!=0 }">
						<tfoot>
							<tr>
								<td style="width:31px;">&nbsp;</td>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:70px;"><span><fmt:message key="total"/></span></td>
								<td colspan="9">&nbsp;</td>
								<td><span style="width:30px;text-align: right;">
									<fmt:formatNumber value="${totalSum }" pattern="0.00" ></fmt:formatNumber>
								</span></td>
								<td colspan="3"><span></span></td>
							</tr>
						</tfoot>
					</c:if>
				</table>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#itemNm').val(parent.$('#itemNm').val());
				$('#item').val(parent.$('#item').val());
				parent.$('#mods').val($('#mods').val());
				parent.$('#unit').val($('#unit_sprice').val());
				
// 				$('#unit_sprice').val($(window.parent.document).find('#price').val()+','+$(window.parent.document).find('#punit').val());
				var punit = $(window.parent.document).find('#punit').val();
				var price = $(window.parent.document).find('#price').val();
				var punit2 = $(window.parent.document).find('#punit2').val();
				var price2 = $(window.parent.document).find('#price2').val();
				var punit3 = $(window.parent.document).find('#punit3').val();
				var price3 = $(window.parent.document).find('#price3').val();
				var costbom_unit = $('#costbom_unit').val();
				var pprice = $(window.parent.document).find('#pprice').val();
				var option = '<option value="'+punit+'" ';
				if(costbom_unit==punit&&pprice==price){
					option+= 'selected="selected" ';
				}
				option+= '>'+
					punit+','+price+"</option>";
				if(punit2!=''&&punit2!=null && punit2 != 'null'){
					option+='<option value="'+punit2+'"';
					if (costbom_unit==punit2&&pprice==price2){
						option+= 'selected="selected" ';
					}
					option+= '>'+
						punit2+','+price2+"</option>";
				}	
				if(punit3!=''&&punit3!=null && punit3 != 'null'){
					option+='<option value="'+punit3+'"';
					if (costbom_unit==punit3&&pprice==price3){
						option+= 'selected="selected" ';
					}
					option+= '>'+
						punit3+','+price3+'</option>';
				}
				$('#unit_sprice').append($(option));
				
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 65: $('#autoId-button-101').click(); break;
		                case 69: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 83: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-106').click(); break;
		            }
				}); 
			 	//双单 击事件
				$('.grid .table-body tr').live('dblclick click',function(){
					 $('#sp_code').val($(this).find('td:eq(2)').find('span').attr('title'));
					 $('#sp_name').val($(this).find('td:eq(3)').find('span').attr('title'));
					 $('#unit1').val($(this).find('td:eq(13)').find('span').attr('title'));//参考单位
					 $('#unit').val($(this).find('td:eq(9)').find('span').attr('title'));//标准单位
					 $('#excnt').val($(this).find('td:eq(7)').find('span').attr('title'));
					 $('#exrate').val($(this).find('td:eq(5)').find('span').attr('title'));
					 $('#cnt1').val($(this).find('td:eq(14)').find('span').attr('title'));
					 $('#sp_exdes').val($(this).find('td:eq(15)').find('span').attr('title'));
					 $('.unit2').html($(this).find('td:eq(6)').find('span').attr('title'));//成本单位
					 $('.unit1').html($(this).find('td:eq(13)').find('span').attr('title'));
					 $(":checkbox").attr("checked", false);
					 $(this).find(":checkbox").attr("checked", true);
					 if($(this).find('td:eq(17)').find('span').attr('title') == 'Y')
					 	$('#status').attr("checked", true);
					 if($(this).find('td:eq(18)').find('span').attr('title') == 'Y'){
					 	$('#iscost').attr("checked", true);
					 }
					 loadToolBar([true,true,true,true,true,false,true]);
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'sp_code',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="supplies_code"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'excnt',
						validateType:['canNull','num2'],
						param:['F','T'],
						error:['<fmt:message key="please_enter_the_number"/>！']
					},{
						type:'text',
						validateObj:'excnt',
						validateType:['num'],
						param:['F'],
						error:['<fmt:message key="please_enter_the_number"/>！']
					},{
						type:'text',
						validateObj:'cnt1',
						validateType:['num2'],
						param:['T'],
						error:['<fmt:message key="please_enter_the_number"/>！']
					},{
						type:'text',
						validateObj:'exrate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="reclaimer_rate"/><fmt:message key="cannot_be_empty"/>！']
					},
// 					{
// 						type:'text',
// 						validateObj:'exrate',
// 						validateType:['maxValue'],
// 						param:['1'],
// 						error:['<fmt:message key="reclaimer_rate"/><fmt:message key="should_be_between_0_and_1"/>！']
// 					},
					{
						type:'text',
						validateObj:'exrate',
						validateType:['minValue'],
						param:['0'],
						error:['<fmt:message key ="please_enter_a_number_greater" />！']
					}]
				});
				//新增
				var status = $("#curStatus").val();
				<%-- curStatus 0:init;1:edit(E);2:add(A);3:show 4,delete(D)--%>
				//判断按钮的显示与隐藏
				if(status == 'A'){
					loadToolBar([true,true,false,false,false,true,false]);
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true]);
					$('#chk1memo').attr('disabled',false);
				}else{//init
					loadToolBar([true,false,false,false,false,true,true]);
				}
				setElementHeight('.grid',['.tool'],$(document.body),102);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			function onSearch(obj){
// 				alert($('#unit').val());
				parent.$('#mods').val($('#mods').val());
				parent.$('#unit').val($('#unit').val());
				var pprice=$('#unit_sprice option:selected').text().split(',')[1];
				parent.$('#pprice').val(pprice);
				var action="<%=path%>/costbom/table.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}  

			/*弹出物资树*/
			function supply_select(){
				if($('#unit_sprice').val()=='') {
					alert('<fmt:message key="please_select_the_correct_sales_unit"/>！');
				}else if ($('#unit_sprice option:selected').text().split(',')[0]==''){
					alert("<fmt:message key ="The_sales_unit_is_empty_please_go_to_the_headquarters_Boh" />！");
				}else if ($('#unit_sprice option:selected').text().split(',')[1]==''){
					alert("<fmt:message key ="The_sales_unit_is_empty_please_go_to_the_headquarters_Boh" />！");
				}else{
					if(!!!top.customWindow){
						top.customSupplyUnit2('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),$('.unit2'),
						function(){
							loadToolBar([false,false,false,true,true,true,true]);
							$("#curStatus").val("A");
						});	
					}
				}
			};
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />(<u>A</u>)',
							title: '<fmt:message key="insert"/><fmt:message key="cost_card_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								supply_select();
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="edit"/><fmt:message key="cost_card_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								$("#curStatus").val("E");
								loadToolBar([false,false,false,true,true,true,true]);
							}
						},{
							text: '<fmt:message key ="copy" />',
							title: '<fmt:message key ="copy" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								copyCostbom("one");
							}
						},{
							text: '<fmt:message key ="Bulk_copy" />',
							title: '<fmt:message key ="Copy_cost_cards_by_category" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								copyCostbom("more");
							}
						},{
							text: '<fmt:message key="delete" />(<u>D</u>)',
							title: '<fmt:message key="delete"/><fmt:message key="cost_card_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								if(confirm('<fmt:message key="confirm_the_deletion_of_the_material"/>？')){
									$('#item').val(parent.$('#item').val());
									if ($('#item').val()==null || $('#item').val()=='') {
										alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
									}else if(validate._submitValidate()){
										
										//update by jin shuai at 20160310
										var test = $('#item').val();
										test = $.trim(test);
										$('#item').val(test);
										
										$("#curStatus").val("D");
										$('#listForm').submit();
									}
								}
							}
						},{
							text: '<fmt:message key ="Batch_delete" />',
							title: '<fmt:message key ="Batch_delete_cost_card" /><fmt:message key ="supplies" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								$('#item').val(parent.$('#item').val());
								if ($('#item').val()==null || $('#item').val()=='') {
									alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
								}else{
									deleteSupplyBySp_code();
								}
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/><fmt:message key="cost_card_information"/>',
							useable: use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								var curStatus=$("#curStatus").val();
								if(curStatus=='A'){
									var cur_a='1';
									$('.grid').find('.table-body').find('tr').each(function(i){
										if($(this).find('td:eq(2)').find('span').attr('title')==$('#sp_code').val()){
											cur_a='0';
											return;
										}
									});
									if(cur_a!='1'){
										alert('<fmt:message key="the_material_has_been_added"/>！');
										return;
									}
								}
								$('#item').val(parent.$('#item').val());
								if ($('#item').val()==null || $('#item').val()=='') {
									alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
								}else if(validate._submitValidate()){
									
									//菜品id有空格
									var tv1 = $("#itcode").val();
									tv1 = $.trim(tv1);
									$("#itcode").val(tv1);
									
									var tv2 = $("#item").val();
									tv2 = $.trim(tv2);
									$("#item").val(tv2);
									
									$('#listForm').submit();
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/><fmt:message key="cost_card_information"/>',
							useable: use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$("#curStatus").val("init");
								loadToolBar([true,true,true,false,false,true,true]);
							}
						},{
						  	text: 'Excel',
						  	title: 'Excel',
						  	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					      	icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					      	},
						  	handler: function(){
								$("#wait2").val('NO');//不用等待加载
								
								//update by js at 20160324
								//传左侧的条件
								var parent = window.parent.document;
								//菜谱方案
								var prgidval = parent.getElementById("prgid").value;
								//类别
								var pgrpval = parent.getElementById("pgrp").value;
								//缩写
								var initval = parent.getElementById("init").value;
								//编码
								var itcodeval = parent.getElementById("itcode").value;
								//全部 已做 未做
								var is_qb = parent.getElementById("is_qb").checked;
								var is_qr = parent.getElementById("is_qr").checked;
								var is_wz = parent.getElementById("is_wz").checked;
								//判断选的是哪一个
								var qyw = 1;
								if(is_qb){
									qyw = 1;
								}else if(is_qr){
									qyw = 2;
								}else if(is_wz){
									qyw = 3;
								}
								var allstr = prgidval+":"+pgrpval+":"+initval+":"+itcodeval+":"+qyw;
								//把数据放到隐藏域
								$("#lefttj").val(allstr);
								
								$('#listForm').attr('action','<%=path%>/costbom/export.do');
								$('#listForm').submit();
								$("#wait2 span").html("loading...");
								$('#listForm').attr('action','<%=path%>/costbom/table.do');
								$("#wait2").val('');//等待加载还原
					 
							}
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								var action="<%=path%>/costbom/print.do";
								$('#listForm').attr('action',action);
								$('#listForm').submit();
								$('#listForm').attr('target','');
								$('#listForm').attr('action','<%=path%>/costbom/table.do');
								$("#wait2").val('');//等待加载还原
							}
						},{
							text: '<fmt:message key ="Updated_material_unit" />',
							title: '<fmt:message key ="Updated_material_unit" />',
							useable: use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								var selected = {};
								$.post('<%=path%>/costbom/updateUnit.do',selected,function(data){
								 	if(data=="1"){
										showMessage({
											type: 'success',
											msg: '<fmt:message key ="update_successful" />！',
											speed: 1000
										});	
								 	}else{
								 		showMessage({
											type: 'error',
											msg: '<fmt:message key ="update_fail" />.....',
											speed: 1000
										});	
								 	}
								});
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							useable: use[6],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			};
			//批量删除菜品
			function deleteSupplyBySp_code(){
				var item = $('#item').val();//菜品编码
				var mods = $('#mods').val();//成本卡类型
				var unit = $('#costbom_unit').val();//菜品销售单位
				var excnt = $('#excnt').val();
				var exrate = $('#exrate').val();//取料率
				var curStatus =$('#curStatus').val();//状态
				var unit = window.encodeURI(window.encodeURI(unit));
				$('body').window({
					title: '<fmt:message key ="Batch_delete_cost_card" /><fmt:message key ="supplies" />',
					content: '<iframe id="deleteSupplyBySp_code" frameborder="0" src="<%=path%>/supply/deleteSupplyBySp_code.do?excnt='+excnt+'&item='+item+'&mods='+mods+'&unit='+unit+'&curStatus='+curStatus+'&exrate='+exrate+'"></iframe>',
					width: '550px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key ="delete" />',
								title: '<fmt:message key ="Batch_delete" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									$('#item').val(parent.$('#item').val());
									if ($('#item').val()==null || $('#item').val()=='') {
										alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
									}else if(document.getElementById('deleteSupplyBySp_code').contentWindow.deleteCostbomByIds()){
										parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
									}
									parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
// 									$('#listForm').submit();
// 									document.getElementById('deleteSupplyBySp_code').contentWindow.deleteCostbomByIds();
// 									submitFrameForm('deleteSupplyBySp_code_x','listForm');
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//复制成本卡
			function copyCostbom(str){
				var item = "";
				var mods = "";
				if(str == "one"){
					$('#item').val(parent.$('#item').val());
					if ($('#item').val()==null || $('#item').val()=='') {
						alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
						return;
					}
					item = $('#item').val();//菜品编码
					mods = $('#mods').val();//成本卡类型
				}
				if(str == "more"){
					item = "";//菜品编码
					mods = $('#mods').val();//成本卡类型
					
					//add by jinshuai at 20160406
					var cfm = window.confirm('此操作将会把本页面选择的成本卡类型下面所有的成本卡复制到弹出页面选中的成本卡类型下面！');
					if(!cfm){
						return;
					}
					
				}
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key ="Copy_cost_card" />',
					content: '<iframe id="saveCostbom" frameborder="0" src="<%=path%>/costbom/toCopyCostbom.do?item='+item+'&mods='+mods+'"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key ="enter" />',
								title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									$('#item').val(parent.$('#item').val());
									if ($('#item').val()==null || $('#item').val()=='') {
										alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
									}else if(getFrame('saveCostbom') && window.document.getElementById("saveCostbom").contentWindow.checkValue()){
										parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
									}
									parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
		</script>
	</body>
</html>
