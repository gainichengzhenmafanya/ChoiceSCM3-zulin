<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品成本卡替代物料</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			form .form-line .form-label{
				width: 9%;
			}
			form .form-line .form-input{
				width: 14%;
			}
			form .form-line .form-input input[type=text]{
				width:80%;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/costbom/saveOrUpdateOrDelCostbom_n.do" method="post" style=""> 
			<!-- 加入隐藏内容 -->
			<div class="bj_head">
			<%-- curStatus 0:init;1:edit(E);2:add(A);3:show 4,delete(D)--%>
			<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${costbom.orderBy}" default="c.sp_code"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${costbom.orderDes}" default="0000000000"/>" />
			<input  type="hidden" id="item" name="item" value="${costbom.item}" />
			<input  type="hidden" id="sp_code_n" name="sp_code_n" value="${costbom.sp_code_n}" />
			<input  type="hidden" id="mods" name="mods" value="${costbom.mods}" />
			<input  type="hidden" id="itcode" name="itcode" value="${costbom.itcode}" />
			<input  type="hidden" name="unit" id="costbom_unit" value="${costbom.unit_sprice}" />
	<%-- 		<input  type="hidden" name="unit" id="costbom_unit" value="${costbom.unit}" /> --%>
			<input  type="hidden" name="itemNm" id="itemNm" value="" />
			<input  type="hidden" name="sp_exdes" id="sp_exdes" class="text" value="" />
			<div class="form-line">
<%-- 				<div class="form-label" style="font-weight:bold"><fmt:message key="sales_units_price"/>:</div> --%>
<!-- 				<div class="form-input" > -->
	<!-- 				<input style="font-weight:bold" type="text" name="unit_sprice" id="unit_sprice" class="text" value="" readonly="readonly"/> -->
<!-- 					<select name="unit_sprice" id="unit_sprice" class="select" onchange="onSearch(this)"> -->
<!-- 					</select> -->
<!-- 				</div> -->
				<div class="form-label"><fmt:message key="coding"/>:</div>
				<div class="form-input">
					<input type="text" name="supply.sp_code" readonly="readonly" id="sp_code" class="text" value="${costbom.supply.sp_code }"/> 
				</div>
				<div class="form-label"><fmt:message key="name"/>:</div>
				<div class="form-input">
					<input type="text" name="supply.sp_name" id="sp_name" class="text" readonly="readonly"  value="${costbom.supply.sp_name }"/>
				</div>
						
										
			</div>
			<div class="form-line">
<%-- 				<div class="form-label"><fmt:message key="the_cost_of_the_card_type"/>:</div> --%>
<!-- 				<div class="form-input" > -->
<!-- 					<select class="select" id="mods" name="mods" onchange="onSearch(this)"> -->
<%-- 						<c:forEach var="mods" items="${modList}" varStatus="status"> --%>
<!-- 							<option  -->
<%-- 								<c:if test="${mods.code == costbom.mods}">  --%>
<!-- 										   	selected="selected" -->
<%-- 								</c:if>  --%>
<%-- 								 value="${mods.code}">${mods.code},${mods.des} --%>
<!-- 							</option> -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
<!-- 				</div> -->
				<div class="form-label"><fmt:message key="standard_unit"/>:</div>
				<div class="form-input" >
					<input type="text" name="supply.unit" id="unit" class="text" readonly="readonly"  value="${costbom.supply.unit}" />
				</div>
				<div class="form-label"><fmt:message key="reference_unit"/>:</div>
				<div class="form-input" >
					<input type="text" name="supply.unit1" id="unit1" class="text" readonly="readonly"  value="${costbom.supply.unit1}" />
				</div>						
				
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="reclaimer_rate"/>:</div>
				<div class="form-input">
					<input type="text" name="exrate" id="exrate" class="text" value="<c:out value="${costbom.exrate}" default="1.0"/>"/>
				</div>	
				<div class="form-label"><fmt:message key="cost_quantity"/>:</div>
				<div class="form-input">
					<input type="text" name="excnt" id="excnt" class="text" value="${costbom.excnt}"/><label class="unit2"></label>
				</div>
				<div class="form-label"><fmt:message key="reference_number"/>:</div>
				<div class="form-input">
					<input type="text" name="cnt1" id="cnt1" class="text" value="${costbom.cnt1}" /><label class="unit1"></label>
				</div>			
	<%-- 			<div class="form-label"><fmt:message key="net_material_name"/>:</div> --%>
	<!-- 			<div class="form-input"> -->
	<%-- 				<input type="text" name="sp_exdes" id="sp_exdes" class="text" value="${costbom.sp_exdes}" /> --%>
	<!-- 			</div>	 -->
			</div>
			</div>
			</form>
		<div class="grid" id="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span style="width:21px;">&nbsp;</span></td>
							<td colspan="5"><span style="width:260px;"><fmt:message key="supplies"/></span></td>
							<td colspan="3"><span style="width:135px;"><fmt:message key="cost_unit"/></span></td>
							<td colspan="4"><span style="width:150px;"><fmt:message key="standard_unit"/></span></td>
							<td colspan="2"><span style="width:95px;"><fmt:message key="reference_unit"/></span></td>
							<td style="display: none;" rowspan="2"><span style="width:50px;"><fmt:message key="net_material_name"/></span></td>
						</tr>
						<tr>
							<td><span style="width:21px;">&nbsp;</span></td>
							<td><span style="width:20px;">
<!-- 								<input type="checkbox" id="chkAll"/>--></span> 
							</td>
							<td><span style="width:70px;"><fmt:message key="supplies_code"/>&nbsp;</span></td>
							<td><span style="width:90px;"><fmt:message key="supplies_name"/>&nbsp;</span></td>
							<td><span style="width:40px;"><fmt:message key="specification"/>&nbsp;</span></td>
							<td><span style="width:40px;"><fmt:message key="reclaimer_rate"/>&nbsp;</span></td>
							
							<td><span style="width:35px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:50px;"><fmt:message key="net_usage"/>&nbsp;</span></td>
							<td><span style="width:50px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
							
							<td><span style="width:30px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
							<td><span style="width:30px;"><fmt:message key="unit_price"/>&nbsp;</span></td>
							<td><span style="width:30px;"><fmt:message key="amount"/>&nbsp;</span></td>
							
							<td><span style="width:35px;"><fmt:message key="unit"/>&nbsp;</span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/>&nbsp;</span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:set var="totalSum" value="0"></c:set>
							<c:forEach var="costbom" varStatus="step" items="${listDownCostbom}">
								<tr>
									<td class="num"><span style="width:21px;">${step.count}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${costbom.acct}" value="${costbom.acct}"/>
									</td>
									<td><span title="${costbom.supply.sp_code}" style="width:70px;">${costbom.supply.sp_code}&nbsp;</span></td>
									<td><span title="${costbom.supply.sp_name}" style="width:90px;">${costbom.supply.sp_name}&nbsp;</span></td>
									<td><span title="${costbom.supply.sp_desc}" style="width:40px;">${costbom.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${costbom.exrate}" style="width:40px;text-align: right;"><fmt:formatNumber value="${costbom.exrate}" pattern="0.00"></fmt:formatNumber></span></td>
									
									<td><span title="${costbom.supply.unit2}" style="width:35px;">${costbom.supply.unit2}&nbsp;</span></td>
									<td><span title="${costbom.excnt}" style="width:50px;text-align: right;"><fmt:formatNumber value="${costbom.excnt}" pattern="0.00"></fmt:formatNumber></span></td>
									<td><span title="${costbom.cnt2}" style="width:50px;text-align: right;" ><fmt:formatNumber value="${costbom.cnt2}" pattern="0.00"></fmt:formatNumber></span></td>
									
									<td><span title="${costbom.supply.unit}" style="width:30px;">${costbom.supply.unit}&nbsp;</span></td>
									<td><span title="${costbom.cnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.cnt}" pattern="0.00"></fmt:formatNumber></span></td>
									<td><span title="${costbom.supply.sp_price}" style="width:30px;text-align: right;"><fmt:formatNumber value="${costbom.supply.sp_price}" pattern="0.00"></fmt:formatNumber></span></td>
									<td><span title="${costbom.supply.amt}" style="width:30px;text-align: right;"><fmt:formatNumber value="${costbom.supply.amt}" pattern="0.00"></fmt:formatNumber></span></td>
									
									<td><span title="${costbom.supply.unit1}" style="width:35px;">${costbom.supply.unit1}&nbsp;</span></td>
									<td><span title="${costbom.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${costbom.cnt1}" pattern="0.00"></fmt:formatNumber></span></td>
									
									<td style="display: none;"><span title="${costbom.sp_exdes}" style="width:50px;text-align: right;">${costbom.sp_exdes}&nbsp;</span></td>
									<td style="display: none;"><input type="hidden" id="mods" style="" value="${costbom.mods}"/></td>
								</tr>
								<c:set var="totalSum" value="${totalSum+costbom.supply.amt }"></c:set>
							</c:forEach>
						</tbody>
				</table>
			</div>
		</div>			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
		loadToolBar([true,false,false,false,false]);
		$(document).ready(function(){
			setElementHeight('.grid',0,$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('#listForm').css('display','none');
			$('.grid .table-body tr').live('dblclick click',function(){
				 $('#sp_code').val($(this).find('td:eq(2)').find('span').attr('title'));
				 $('#sp_name').val($(this).find('td:eq(3)').find('span').attr('title'));
				 $('#unit1').val($(this).find('td:eq(13)').find('span').attr('title'));//参考单位
				 $('#unit').val($(this).find('td:eq(9)').find('span').attr('title'));//标准单位
				 $('#excnt').val($(this).find('td:eq(7)').find('span').attr('title'));
				 $('#exrate').val($(this).find('td:eq(5)').find('span').attr('title'));
				 $('#cnt1').val($(this).find('td:eq(14)').find('span').attr('title'));
				 $('#sp_exdes').val($(this).find('td:eq(15)').find('span').attr('title'));
				 $('.unit2').html($(this).find('td:eq(6)').find('span').attr('title'));//成本单位
				 $('.unit1').html($(this).find('td:eq(13)').find('span').attr('title'));
				 $(":checkbox").attr("checked", false);
				 $(this).find(":checkbox").attr("checked", true);
				 loadToolBar([false,true,true,false,true]);
				 $('#listForm').css('display','');
				 
				 var sp_code=$(this).find('td:eq(2)').find('span').attr('title');
				 var mods=$('#mods').val();
				 var item=$('#item').val();
				 var excnt = $('#exrate').val();
				 var unit_sprice=$(window.parent.document).find('#punit').val();
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'sp_code',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="supplies_code"/><fmt:message key="cannot_be_empty"/>！']
				},{
					type:'text',
					validateObj:'excnt',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="please_enter_the_number"/>！']
				},{
					type:'text',
					validateObj:'excnt',
					validateType:['num'],
					param:['F'],
					error:['<fmt:message key="please_enter_the_number"/>！']
				},{
					type:'text',
					validateObj:'exrate',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="reclaimer_rate"/><fmt:message key="cannot_be_empty"/>！']
				},
//					{
//						type:'text',
//						validateObj:'exrate',
//						validateType:['maxValue'],
//						param:['1'],
//						error:['<fmt:message key="reclaimer_rate"/><fmt:message key="should_be_between_0_and_1"/>！']
//					},
				{
					type:'text',
					validateObj:'exrate',
					validateType:['minValue'],
					param:['0'],
					error:['<fmt:message key ="please_enter_a_number_greater" />！']
				}]
			});
		});
		
		/*弹出物资树*/
		function supply_select(){
	
				if(!!!top.customWindow){
					top.customSupplyUnit2('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),$('.unit2'),
					function(){
						 $('#listForm').css('display','');
						 loadToolBar([false,false,false,true,true,true]);
						 $("#curStatus").val("A");
					});	
				}
			
		};
		function loadToolBar(use){
			$('.tool').html('');
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key ="Replacement_material" />',
						title: '<fmt:message key ="Replacement_material" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							supply_select();
						}
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="edit"/><fmt:message key="cost_card_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							$("#curStatus").val("E");
							loadToolBar([false,false,false,true,true,true,true]);
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete"/><fmt:message key="cost_card_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="confirm_the_deletion_of_the_material"/>？')){
								$('#item').val(parent.$('#item').val());
								if ($('#item').val()==null || $('#item').val()=='') {
									alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
								}else if(validate._submitValidate()){
									$("#curStatus").val("D");
									$('#listForm').submit();
								}
							}
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/><fmt:message key="cost_card_information"/>',
						useable: use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var curStatus=$("#curStatus").val();
							if(curStatus=='A'){
								var cur_a='1';
								$('.grid').find('.table-body').find('tr').each(function(i){
									if($(this).find('td:eq(2)').find('span').attr('title')==$('#sp_code').val()){
										cur_a='0';
										return;
									}
								});
								if(cur_a!='1'){
									alert('<fmt:message key="the_material_has_been_added"/>！');
									return;
								}
							}
							$('#item').val(parent.$('#item').val());
							if ($('#item').val()==null || $('#item').val()=='') {
								alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
							}else if(validate._submitValidate()){
								$('#listForm').submit();
							}
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel"/><fmt:message key="cost_card_information"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							$("#curStatus").val("init");
							loadToolBar([true,false,false,false,false]);
							$('#listForm').css('display','none');
						}
					},{
						text: '<fmt:message key ="One_key_replacement" />',
						title: '<fmt:message key ="A_key_to_replace_all_the_materials_this_operation_will_give_other_dishes_to_set_up_the_same_material_to_set_the_same_alternative_materials" />！',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							if(confirm('<fmt:message key ="A_key_to_replace_all_supplies_this_operation_will_give_other_dishes_to_set_up_the_same_material_to_set_up_the_same_alternative_materials_whether_to_continue" />？')){
								$('#item').val(parent.$('#item').val());
								if ($('#item').val()==null || $('#item').val()=='') {
									alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
								}else if(validate._submitValidate()){
									$("#curStatus").val("A-ALL");
									$('#listForm').submit();
								}
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						useable: use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
		};
		</script>
	</body>
</html>