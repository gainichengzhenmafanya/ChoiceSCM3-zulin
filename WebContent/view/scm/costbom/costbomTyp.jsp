<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
		<div class="form">
			<form id="listFrame" method="post" action="<%=path %>/costbom/table.do">
				<input type="hidden" id="item" name="item" value="${item}" />
				<input type="hidden" id="mods" name="mods" value="${mods}" />
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span style="width:21px;">&nbsp;</span></td>
									<td><span style="width:20px;">
		 								<input type="checkbox" id="chkAll"/></span> 
									</td>
									<td><span style="width:80px;"><fmt:message key ="Cost_card_type" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="Cost_card_type_name" /></span></td>
								</tr>
							</thead>
						</table>
					</div> 
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:set var="totalSum" value="0"></c:set>
								<c:forEach var="codedes" varStatus="step" items="${codedesList}">
									<tr>
										<td class="num"><span style="width:21px;">${step.count}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${codedes.code}" value="${codedes.code}"/>
										</td>
										<td><span title="${codedes.code}" style="width:80px;">${codedes.code}&nbsp;</span></td>
										<td><span title="${codedes.des}" style="width:100px;">${codedes.des}&nbsp;</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
			});
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
			     if ($(this)[0].checked) {
			    	 $(this).attr("checked", false);
			     }else{
			    	 $(this).attr("checked", true);
			     }
			 });
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
			     if ($(this).find(':checkbox')[0].checked) {
			    	 $(this).find(':checkbox').attr("checked", false);
			     }else{
			    	 $(this).find(':checkbox').attr("checked", true);
			     }
			 });
			function checkValue(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 0){
					alert('<fmt:message key ="please_select_at_least_one_data" />！');
					return;
				}
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				var action="<%=path%>/costbom/copyCostBom.do?ids="+chkValue.join(",");
				$('#listFrame').attr('action',action);
				$('#listFrame').submit();
			}
		</script>
	</body>
</html>