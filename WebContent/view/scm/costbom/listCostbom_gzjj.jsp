<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="cost_card_information"/>-菜品bom</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.leftFrame{
				width:22%;
				}
				.mainFrame{
				width:78%;
				}
				.tr-select{
				background-color: #D2E9FF;
				}
				#modDiv{
					border: 1px solid #5A7581;
					margin: 1px 1px 1px 1px;
				 	padding: 1px;
				 	padding-left: -8px;
				 	background-color:#CDDCEE;
				}
			</style>
		</head>
	<body>
		<input type="hidden" name="punit"  id="punit" class="text" value=""/> <!-- 隐藏 单位 -->
		<input type="hidden" name="price"  id="price" class="text" value=""/> <!-- 隐藏 单位 -->
		<input type="hidden" name="punit2"  id="punit2" class="text" value=""/> <!-- 隐藏 单位 -->
		<input type="hidden" name="price2"  id="price2" class="text" value=""/> <!-- 隐藏 单位 -->
		<input type="hidden" name="punit3"  id="punit3" class="text" value=""/> <!-- 隐藏 单位 -->
		<input type="hidden" name="price3"  id="price3" class="text" value=""/> <!-- 隐藏 单位 -->
		<div class="leftFrame"> 
		<div id="modDiv">
			<form id="listForm" action="<%=path%>/costbom/list.do" method="post">
			<input  type="hidden" name="itemNm" id="itemNm" value="" />
			<input  type="hidden" name="item" id="item" value="" />
			<div class="table-head_" style="background-color:#CDDCEE;">
				<div class="form-line">
					<div class="form-input" style="width: 80px">
						<fmt:message key ="the_menu_plan" />:
					</div>
						<select class="select" id="prgid" name="pubitem.prgid" style="width:134px;" onchange="changeSelect()">
							<option value=""></option>
							<c:forEach var="itemPrgm" items="${itemPrgmList}" varStatus="status">
								<option 
									<c:if test="${itemPrgm.id == pubitem.prgid}"> 
											   	selected="selected"
									</c:if> 
									 value="${itemPrgm.id}">${itemPrgm.des}
								</option>
							</c:forEach>
						</select>
				</div>
				<div class="form-line">
					<div class="form-input" style="width: 80px">
						<fmt:message key="category"/>:
					</div>
						<select class="select" id="pgrp" name="pubitem.pgrp" style="width:134px;" onchange="changeSelect()">
							<option value=""></option>
							<c:forEach var="pubgrp" items="${pubgrpList}" varStatus="status">
								<option 
									<c:if test="${pubgrp.pubgrp==pubitem.pgrp}"> 
											   	selected="selected"
									</c:if> 
									 value="${pubgrp.pubgrp}">${pubgrp.code},${pubgrp.pgrpdes}
								</option>
							</c:forEach>
						</select>
				</div>
				<div class="form-line">
					<div class="form-input" style="width: 80px">
						<fmt:message key="abbreviation"/>:
					</div>
						<input type="text" name="init" id="init" class="text" value="${pubitem.init}" /> 
				</div>
				<div class="form-line">
					<div class="form-input" style="width: 80px">
						<fmt:message key="coding"/>:
					</div>
						<input type="text" name="itcode"  id="itcode" class="text" value="${costbom.itcode}"/> 
				</div>
				<div class="form-line">
					<div class="form-input" >
						<input type="radio" id="is_qb" name="prdctcard" value="QB" checked="checked" onclick="changeSelect(null);"/><fmt:message key="all"/>
						<input type="radio" id="is_qr" name="prdctcard" value="QR" onclick="changeSelect(null);"/><fmt:message key="been_made_cost_card" />
						<input type="radio" id="is_wz" name="prdctcard" value="WZ" onclick="changeSelect(null);"/><fmt:message key="not_been_made_cost_card"/>
					</div>
				</div>
				<input type="hidden" id="mods" name="" value="${costbom.mods}"/>	
				<input type="hidden" id="unit" name="" value="${costbom.unit}"/>
				<input type="hidden" id="pprice" name="" value="${costbom.pprice}"/>	
			</div>
			</form>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0" >
						<thead>
							<tr>
								<td class="num"><span style="width: 25px;">&nbsp;</span></td>
<%-- 								<td ><span id="code1" style="width:32px;" ><fmt:message key="internal_code"/></span></td> --%>
								<td ><span id="coding1" style="width:50px;"><fmt:message key="coding"/></span></td>
								<td ><span id="name1" style="text-align: center;"><fmt:message key="name"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="pubitem" items="${pubitemList}" varStatus="status">
								<tr>
									<td class="num" ><span id= "staindex" style="width: 25px;">${status.index+1}</span></td>
<%-- 									<td ><span id="code2" title="" style="width:32px;">${pubitem.pubitem}</span></td> --%>
									<td >
										<span id="coding2" title="${pubitem.unit},${pubitem.unit2},${pubitem.unit3},${pubitem.price},${pubitem.price2},${pubitem.price3}" style="width:50px;">${pubitem.itcode}</span>
									</td>
									<td><span id="name2" title="${pubitem.itdes}">${pubitem.itdes}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
    	</div>
	    <div class="mainFrame">
	 		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			var t;
			$('#init,#itcode').bind('keyup', function(e) { 
				if (e.keyCode == 13){
					return; //回车  
				}
			    window.clearTimeout(t); 
			    t=window.setTimeout(changeSelect,500);//延迟0.2秒
			});
			//自动实现滚动条
			setElementHeight('.grid',['.table-head_'],$(document.body),30);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);

			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click dbclick", function () {
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				var staindex = $(this).find('td:eq(0)').find('span').text();
// 				var item=window.encodeURI(window.encodeURI($(this).find('td:eq(1)').find('span').text()));
				var itcode=window.encodeURI(window.encodeURI($.trim($(this).find('td:eq(1)').find('span').text())));
				var unit=$(this).find('td:eq(1)').find('span').attr('title').split(",");
// 				var price=$(this).find('td:eq(2)').find('span').attr('title').split(",");
				$('#punit').val(unit[0]);
				$('#price').val(unit[3]);
				$('#punit2').val(unit[1]);
				$('#price2').val(unit[4]);
				$('#punit3').val(unit[2]);
				$('#price3').val(unit[5]);
 				$('#unit').val(unit[0]);
				$('#itemNm').val($(this).find('td:eq(2)').find('span').attr('title'));
				$('#item').val($.trim($(this).find('td:eq(1)').find('span').text()));
				var unit =  window.encodeURI(window.encodeURI($('#unit').val()));
				var pprice =  $('#pprice').val();
		        window.mainFrame.location = "<%=path%>/costbom/table.do?staindex="+staindex+"&itcode="+itcode+"&mods="+$('#mods').val()+"&unit_sprice="+unit+"&pprice="+pprice+"&item="+$.trim($('#item').val());
			 });
			if(''==$('#mods').val()){
				$('#mods').val('01');
			}
			$('.grid').find('.table-body').find('tr:eq(0)').click();
			var widthds = $('.grid').width();
			var name1 = widthds/2;
			$('#name1,#name2').width(name1);
		});
		function changeSelect(){
			var prdctcard = $('input[name="prdctcard"]:checked').val();
			var selected = {};
			selected['prdctcard'] = prdctcard;
			selected['pgrp'] = $('#pgrp').val();
			selected['prgid'] = $('#prgid').val();
			var str=$('#init').val();
			selected['init'] = str.toUpperCase();
			selected['itcode'] = $('#itcode').val();
			$.post('<%=path%>/costbom/findAllPubitemByAjax.do',selected,function(pubitemList){
				$('.table-body').find('table').find('tbody').empty();
				for(var i=0;i<pubitemList.length;i++){
					if (pubitemList[i].pubitem==null) {
						pubitemList[i].pubitem='';
					}
					if (pubitemList[i].itcode==null) {
						pubitemList[i].itcode='';
					}
					$('.table-body').find('table').find('tbody').append('<tr><td class="num"><span style="width: 25px;">'
						+(i+1)+'</span></td><td><span title="'+pubitemList[i].unit+','+pubitemList[i].unit2+','+pubitemList[i].unit3+','+pubitemList[i].price+','+pubitemList[i].price2+','+pubitemList[i].price3+'" style="width:50px;" >'
						+pubitemList[i].itcode+'&nbsp;</span></td><td><span title="'+pubitemList[i].itdes+'" style="width:'+($('.grid').width()/2)+'px;" >'
						+pubitemList[i].itdes+'&nbsp;</span></td></tr>');
				}
			});
		}
	</script>
	</body>
</html>