<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>菜品成本卡实际物料</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.page{margin-bottom: 25px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<input  type="hidden" id="sp_code_x" name="sp_code_x" value="${costbom.sp_code_x}" />
		<input  type="hidden" id="mods" name="mods" value="${costbom.mods}" />
		<input  type="hidden" id="item" name="item" value="${costbom.item}" />
		<input  type="hidden" id="unit_sprice" name="unit_sprice" value="${costbom.unit_sprice}" />
		<div class="grid" id="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
						<!-- 	<td class="num" style="width: 26px;"><span >&nbsp;</span></td> -->
							<td colspan="3"><fmt:message key="supplies"/></td>
							<td colspan="2"><fmt:message key="standard_unit"/></td>
							<td colspan="2"><fmt:message key ="cost_unit" /></td>
						</tr>
						<tr>									
<!-- 							<td class="num" style="width: 26px;"><span >&nbsp;</span></td>
 -->							<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
							<td><span style="width:90px;"><fmt:message key="supplies_name"/></span></td>
							<td><span style="width:90px;"><fmt:message key ="specifications" /></span></td>
							<td><span style="width:40px;"><fmt:message key ="standard_unit" /></span></td>
							<td><span style="width:70px;"><fmt:message key ="number_of_standards" /></span></td>
							<td><span style="width:40px;"><fmt:message key ="cost_unit" /></span></td>
							<td><span style="width:70px;"><fmt:message key ="Cost_quantity" /></span></td>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="downCostbom" items="${listDownCostbom}" varStatus="status">
							<tr data-mincnt="${downCostbom.supply.mincnt}" data-unitper="${downCostbom.supply.unitper }">
								<td><span style="width:70px;">${downCostbom.supply.sp_code }</span></td>
								<td><span style="width:90px;">${downCostbom.supply.sp_name }</span></td>
								<td><span style="width:90px;">${downCostbom.supply.sp_desc }</span></td>
								<td><span style="width:40px;">${downCostbom.supply.unit }</span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${downCostbom.cnts }" pattern="0.0000"/></span></td>
								<td><span style="width:40px;">${downCostbom.supply.unit2 }</span></td>
								<td><span style="width:70px;text-align: right;"><fmt:formatNumber value="${downCostbom.cnt2s }" pattern="0.0000"/></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>			
		<page:page form="listForm" page="${pageobj}"></page:page>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />  				
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
	
		$(document).ready(function(){
			setElementHeight('.grid',0,$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		</script>
	</body>
</html>