<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="price_template_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.topFrame { 
				background-color:#FFF; 
				width:100%;
				height: 100%;
			}
			.text{
				border: 0px;
				background-color: #E1E1E1;
			}
			textarea{
				width:90%; 
				height:80%; 
				boder:1px;
				background-color: #E1E1E1;
				margin-top: 0px;
			}
			.content{
				overflow: auto;
				height: 100%;
				width: 100%;
				margin-left: 80px;
			}
			.form-label-cur{
				background-color: #E1E1E1;
				width: 100%;
				text-align: left;
			}
			.form-line-cur{
				height: 40%;
				background-color: #E1E1E1;
			}
		</style>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript">
			
		</script>
	</head>
	<body>
	<div class="topFrame">
			<form id="sppriceBatchForm" method="post" action="<%=path %>/sppriceDemo/saveByBatch.do" style="height:100%;background-color: #E1E1E1;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="suppliers" /></div>
					<div class="formInput" style="background-color: #E1E1E1;height: 30px">
						<input class="text" readonly="readonly" value="${deliver.des }"/>
						<input type="hidden" value="${deliver.code }" name="deliver" id="deliver"/>
					</div>
				</div>
				<div class="form-line form-line-cur" >
					<div class="form-label form-label-cur"><fmt:message key="supplies" /></div>
					<div class="content">
						<textarea id="supplyShow" name="name" readonly="readonly"></textarea>
						<input type="hidden" id="supply" name="sp_code"/>
					</div>
				</div>
				<div class="form-line form-line-cur">
					<div class="form-label form-label-cur" ><fmt:message key="branche" /></div>
					<div class="content">
						<textarea id="positnShow" name="name" readonly="readonly"></textarea>
						<input type="hidden" id="positn" name="firm"/>
					</div>
				</div>
			</form>
	</div>
	</body>
</html>