<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>报价快速加入</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<style type="text/css">
		.line{
			BORDER-LEFT-STYLE: none;
			BORDER-RIGHT-STYLE: none;
			BORDER-TOP-STYLE: none;
			width: 70px;
		}
	</style>
</head>
<body>
	<div id="updateTimDiv" style="display: none;">
		<div style="padding-left: 18%; padding-top: 15%;">
			<fmt:message key ="starttime" /> ：<input type="text"  id="updateBdat" name="bdat" class="Wdate text"  onclick="WdatePicker()"/><br/>
			<fmt:message key ="endtime" />   ：<input type="text"  id="updateEdat" name="edat" class="Wdate text"  onclick="WdatePicker()"/><br/>
			<input type="button" style="left: 100px;position: absolute;top: 100px;" onclick="updateTim()" value="<fmt:message key="confirm" />"/><input type="button" style="left: 170px;position: absolute;top: 100px;" onclick="TimeClose()" value="<fmt:message key="cancel" />"/>
			<input type="hidden" id="deliverCode" value="${code }"/>
		</div>
	</div>
	<div class="leftFrame" style="width: 60%">
	<div class="tool"></div>
		<div class="grid"> 
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;text-align: center;">
									<input type="checkbox" id="chkAll" />
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key ="supplies_code" />
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key ="supplies_name" />
									</span>
								</td>
								<td style="width:80px;text-align: center;">
									<span style="width: 70px;">
										<fmt:message key ="supplies_specifications" />
									</span>
								</td>
								<td style="width:40px;text-align: center;">
									<span style="width: 30px;">
										<fmt:message key ="unit_price" />
									</span>
								</td>
								<td style="width:40px;text-align: center;">
									<span style="width: 40px;">
										<fmt:message key ="unit" />
									</span>
								</td>
								<td style="width:95px;text-align: center;">
									<span style="width: 85px;">
										<fmt:message key ="starttime" />
									</span>
								</td>
								<td style="width:95px;text-align: center;">
									<span style="width: 85px;">
										<fmt:message key ="endtime" />
									</span>
								</td>
								<td style="width:50px;text-align: center;">
									<span style="width: 50px;">
										<fmt:message key ="previous_period_price" />
									</span>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody id="sp_data">
						</tbody>
					</table>
				</div>
			</div>
	</div>
	<div style="display: inline-block;float: left;width: 40%;" class="mainFrame">
   		<iframe src="<%=path%>/sppriceQuick/searchAllPositn.do" style="overflow-x:scroll;" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
   	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#updateBdat").htmlUtils("setDate","now");
		$("#updateEdat").htmlUtils("setDate","lastDay"); 
		$(".tool").toolbar({
			items: [{
					text: '<fmt:message key="Add_material" />',
					title: '<fmt:message key="Add_material" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','0px']
					},
					handler: function(){
						seachSupply();
					}
				},{
					text: '<fmt:message key="Batch_modify_time" />',
					title: '<fmt:message key="Batch_modify_time" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-100px','-60px']
					},
					handler: function(){
						showUpdateTim();
					}
				},{
					text: '<fmt:message key="Confirmation_submission" />',
					title: '<fmt:message key="Confirmation_submission" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-120px','0px']
					},
					handler: function(){
						saveSppriceDemo()
					}
				}]
		});
		seachSupply=function(){
			$('body').window({
				id: 'window_saveSppriceDemoQuick',
				title: '<fmt:message key="insert" /><fmt:message key="price_template_information" />',
				content: '<iframe id="saveSppriceDemoQuickFrame" frameborder="0" src="<%=path%>/supply/addSupplyBatch.do?type=quick"></iframe>',
				width: '820px',
				height: '550px',
				draggable: true,
				isModal: true
			})
		};
		//向Table内添加数据
		addSp=function(id){
			$.ajax({
				type : 'GET',
				contentType : 'application/json;charset=UTF-8',
				url : '<%=path%>/sppriceQuick/findById.do?sp_code='+id,
				dataType : 'json',
				success : function(list) {
					NewInput();
					var bdat=$("#updateBdat").val();
					var edat=$("#updateEdat").val();
					for ( var i = 0; i < list.length; i++) {
						
						if (list[i].sp_desc==null) {
							list[i].sp_desc='';
						}
						$('#sp_data').append('<tr>'
							+'<td style="width:30px; text-align: center;">'
								+'<input type="checkbox" name="idList" id="chk_'+list[i].sp_code+'" value="'+list[i].sp_code+'"/>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_code">'+list[i].sp_code+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_name">'+list[i].sp_name+'</span>'
							+'</td>'
							+'<td style="width:80px; text-align: left;">'
								+'<span style="width: 70px;" id="sp_desc">'+list[i].sp_desc+'</span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;padding: 0px;"><input  onfocus="this.select()" type="text" class="line" id="price" style="text-align:right;width:40px;" value="'+list[i].sp_price+'"/></span>'
							+'</td>'
							+'<td style="width:40px; text-align: left;">'
								+'<span style="width: 40px;" id="unit1">'+list[i].unit+'</span>'//修改参考单位为标准单位
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+bdat+'" style="width: 95px;height: 15px;margin: 0px;" id="bdat" name="bdat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:95px; text-align: left;">'
								+'<span style="width: 95px;padding: 0px;"><input type="text" value="'+edat+'" style="width: 95px;height: 15px;margin: 0px;" id="edat" name="edat" class="Wdate text"  onclick="WdatePicker()"/></span>'
							+'</td>'
							+'<td style="width:60px; text-align: left;">'
								+'<span style="width: 60px;padding: 0px;"><input type="text"  onfocus="this.select()" class="line" id="sp_price" style="text-align:right;width:60px;" value="'+list[i].priceold+'"/></span>'
							+'</td>'
						+'</tr>');
					}
					NewInput();
				}
			});
		};
		//解决 table特效
		NewInput=function(){
			tabTableInput("sp_data","text"); //input  上下左右移动
			setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');
//			loadGrid();//  自动计算滚动条的js方法
// 			changeTh();//拖动 改变table 中的td宽度 
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
			     if ($(this)[0].checked) {
			    	 $(this).attr("checked", false);
			     }else{
			    	 $(this).attr("checked", true);
			     }
			 });
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
			     if ($(this).find(':checkbox')[0].checked) {
			    	 $(this).find(':checkbox').attr("checked", false);
			     }else{
			    	 $(this).find(':checkbox').attr("checked", true);
			     }
			 });
		};
// 		显示批量修改时间
		showUpdateTim=function(){
			if($('#sp_data :checkbox').filter(':checked').size()>0){
				$('body').window({   
					title:"<fmt:message key="Batch_modify_time" />",
					width:300,   
					height:200,   
					draggable: true,
					maximizable:false,
					isModal: true,
					confirmClose:false,
					content:$('#updateTimDiv').html()
				});   
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Select_the_data_you_want_to_modify" />..！',
					speed: 2500
				});
			}
		};
		//关闭批量修改时间窗口
		TimeClose=function(){
			$('.close').click();
		};
		//批量修改时间
		updateTim=function(){
			var bdat=$(".window-content").find("#updateBdat").val();
			var edat=$(".window-content").find("#updateEdat").val();
			var sppriceList=$('#sp_data :checkbox');
			if(bdat<=edat){
				sppriceList.filter(':checked').each(function(index){
					if(bdat!=''){
						$(this).parent().parent().find("td:eq(6) input").val(bdat);
					}
					if(edat!=''){
						$(this).parent().parent().find("td:eq(7) input").val(edat);
					}
					$('.close').click();
				});
			}else{
				alert("<fmt:message key="starttime"/><fmt:message key="not_less_than"/><fmt:message key="endtime"/>!")
			}
		};
		//确认保存
		saveSppriceDemo=function(){
			var SppriceDemo={};
			var size=0;
			var NotNull=true;
			var sppriceList=$('#sp_data :checkbox');
			sppriceList.filter(':checked').each(function(index){
				var price=$(this).parent().parent().find("td:eq(4) input").val();//单价
				var bdat=$(this).parent().parent().find("td:eq(6) input").val();//开始时间
				var edat=$(this).parent().parent().find("td:eq(7) input").val();//结束时间
				SppriceDemo["suppriceDemoList["+index+"].sp_code"]=$(this).val();
				SppriceDemo["suppriceDemoList["+index+"].deliver"]=$("#deliverCode").val();
				SppriceDemo["suppriceDemoList["+index+"].price"]=price;//单价
				SppriceDemo["suppriceDemoList["+index+"].bdat"]=bdat;//开始时间
				SppriceDemo["suppriceDemoList["+index+"].edat"]=edat;//结束时间
				SppriceDemo["suppriceDemoList["+index+"].priceold"]=$(this).parent().parent().find("td:eq(8) input").val();//上期价格
				if(price==""||bdat==""||edat==""){
					NotNull=false;
					return;
				}
				var positnList=$("#mainFrame").contents().find('.table-body :checkbox');
				positnList.filter(':checked').each(function(index1){
					SppriceDemo["suppriceDemoList["+index+"].positnList["+index1+"].code"]=$(this).val();
					size=++index1;
				});
			});
			if(NotNull){
				if(size>0){
					$.ajax({
						url : '<%=path%>/sppriceQuick/saveSppriceDemo.do',
						type:'POST',
						async:false,
						data:SppriceDemo,
						success : function(date) {
							if(date=="success"){
								showMessage({
			  						type: 'success',
			  						msg: '<fmt:message key="successful_added"/>！',
			  						speed: 2000
			  					});
								setTimeout(function(){
									top.customWindow.afterCloseHandler('Y');
									top.closeCustom();
								}, 1500);
								
							}else{
								showMessage({
		  	  						type: 'error',
		  	  						msg: '<fmt:message key="fail_added"/>,<fmt:message key="Data_is_repeated" />！',
		  	  						speed: 2000
		  	  					});
							}
						}
					});
				}else{
					showMessage({
							type: 'error',
							msg: '<fmt:message key="Donot_forget_to_choose_the_material_and_the_store" />..！',
							speed: 2000
						});
				};
			}else{
				showMessage({
					type: 'error',
					msg: '<fmt:message key="Unit_price_and_time_can_not_be_empty" />..！',
					speed: 2000
				});
			};
		};
	});
	</script>
</body>
</html>