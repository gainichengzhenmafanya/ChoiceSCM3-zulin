<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Classes Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	</head>
	<body>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:160px;">
			<div class="form">
				<div class="form-line">
					<div class="form-label" style="padding-left:5px;"><fmt:message key="price" />:</div>
					<div class="form-input"><input type="text" id="price" class="text"/></div>
				</div>
			</div>
		</div>
	</body>
</html>