<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>sppriceDemo Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.rightMenu{
					position:absolute;
					z-index:99;
					background-color: gray;
					border: 1px solid gray;
				}
				.search-button {
				  color:#000000;
				  background:transparent url("<%=path%>/image/search-button.gif") no-repeat;
				  height:21px;
				  vertical-align:middle;
				  width:60px;
				  border:0;
				  cursor:pointer;
				}
				.grid td span{
					padding:0px;
				}
				.form-line .form-label{
					width: 120px;
					text-align: left;
				}
				.table-head td span{
					white-space: normal;
				}
			</style>
		</head>
	<body>
		<div class="tool"></div>
		<fmt:message key="supplies"/><fmt:message key="coding"/>：<input type="text" id="ssp_code" value="${sppriceDemo.sp_code}" style="width: 80px;" onkeypress="ajaxSearch()"/>
		<fmt:message key="supplies"/><fmt:message key="name"/>：<input type="text" id="ssp_name" value="${sppriceDemo.sp_name}" style="width: 80px;" onkeypress="ajaxSearch()"/>
		<fmt:message key="branche"/><fmt:message key="coding"/>：<input type="text" id="sfirm" value="${sppriceDemo.firm}" style="width: 80px;" onkeypress="ajaxSearch()"/>
		<fmt:message key="branche"/><fmt:message key="name"/>：<input type="text" id="sfirmDes" value="${sppriceDemo.firmDes}" style="width: 80px;" onkeypress="ajaxSearch()"/>
		<input type="button" value="<fmt:message key="select"/>" onclick="searchMore()"/>
		<form action="<%=path%>/sppriceDemo/list.do" id="listForm" name="listForm" method="post">
			<input type="hidden" name="deliver" id="deliver" value="${sppriceDemo.deliver}"/>
			<input type="hidden" name="sp_code" id="sp_code" value="${sppriceDemo.sp_code}"/>
			<input type="hidden" name="sp_name" id="sp_name" value="${sppriceDemo.sp_name}"/>
			<input type="hidden" name="firm" id="firm" value="${sppriceDemo.firm}"/>
			<input type="hidden" name="firmDes" id="firmDes" value="${sppriceDemo.firmDes}"/>
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label">
						<input type="radio" name='editTyp' checked="checked" value="bdat"/>
						<fmt:message key="update" /><fmt:message key="startdate" />
					</div>
					<div class="form-label">
						<input type="radio" name='editTyp' value="edat"/>
						<fmt:message key="update" /><fmt:message key="enddate" />
					</div>
					<div class="form-label">
						<input type="radio" name='editTyp' value="price"/>
						<fmt:message key="update" /><fmt:message key="price" />
					</div>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />'/>
				</div>
			</div>
<%-- 			<input type="hidden" name="deliver" id="deliver" value="${sppriceDemo.deliver}"/> --%>
<!-- 				<div class="form-line"> -->
<!-- 					<div class="form-input" style="height:28px;"> -->
<!-- 			       		<input type="text" class="Wdate text" id="modifyDate" value=""/> -->
<!-- 					</div> -->
<!-- 					<div id="modifyButton" style="float:left;"></div> -->
<!-- 				</div> -->
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 26px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:90px;"><fmt:message key="supplies_code" /></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name" /></span></td>
								<td><span style="width:50px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:50px;"><fmt:message key="brands" /></span></td>
								<td><span style="width:50px;"><fmt:message key="origin" /></span></td>
								<td><span style="width:30px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:75px;"><fmt:message key="startdate" /></span></td>
								<td><span style="width:75px;"><fmt:message key="enddate" /></span></td>
								<td><span style="width:100px;"><fmt:message key="branche" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price" /></span></td>
								<td><span style="width:50px;"><fmt:message key="previous_period_price" /></span></td>
<%-- 								<td><span style="width:180px;"><fmt:message key="suppliers" /></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="demo" items="${demoList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 26px;">${status.index+1}</span>
										<input type="hidden" id="acct" value="<c:out value='${acct}'/>"/>
									</td>
									<td><span style="width:30px; text-align: center;">
										<input name="idList" type="checkbox" id="chk_<c:out value='${demo.firm}' />" value="<c:out value='${demo.firm}' />"/></span>
									</td>
									<td id="sp_code_<c:out value='${status.index+1}' />"><span style="width:90px;"><c:out value="${demo.sp_code}" />&nbsp;</span></td>
									<td id="sp_name_<c:out value='${status.index+1}' />" ><span style="width:100px;"><c:out value="${demo.sp_name}" />&nbsp;</span></td>
									<td id="sp_desc_<c:out value='${status.index+1}' />" ><span style="width:50px;"><c:out value="${demo.sp_desc}" />&nbsp;</span></td>
									<td id="sp_mark_<c:out value='${status.index+1}' />" ><span style="width:50px;"><c:out value="${demo.sp_mark}" />&nbsp;</span></td>
									<td id="sp_addr_<c:out value='${status.index+1}' />" ><span style="width:50px;"><c:out value="${demo.sp_addr}" />&nbsp;</span></td>
									<td id="unit_<c:out value='${status.index+1}' />" ><span style="width:30px;"><c:out value="${demo.unit}" />&nbsp;</span></td>
									<td id="bdat_<c:out value='${status.index+1}' />" ><span style="width:75px;" class="editabledate"><fmt:formatDate value="${demo.bdat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td id="edat_<c:out value='${status.index+1}' />" ><span style="width:75px;" class="editabledate"><fmt:formatDate value="${demo.edat}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td id="firm_<c:out value='${status.index+1}' />" ><input type="hidden" value="${demo.firm}"/><span style="width:100px;"><c:out value="${demo.firmDes}" />&nbsp;</span></td>
									<td id="price_<c:out value='${status.index+1}' />" ><span style="width:50px;text-align: right;" class="editable"><c:out value="${demo.price}" /></span></td>
									<td id="priceold_<c:out value='${status.index+1}' />" ><span style="width:50px;text-align: right;"><c:out value="${demo.priceold}" /></span></td>
<%-- 									<td id="deliver_<c:out value='${status.index+1}' />" ><input type="hidden" value="${demo.deliver}"/><span style="width:180px;"><c:out value="${demo.deliverDes}" />&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.jeditable.mini.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>

		<script type="text/javascript">
			function pageReload(){
				$('#listForm').submit();
			}
			$(document).ready(function(){
				
				/* 批量修改确认 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					var editTyp = $(":radio[name='editTyp']:checked").val();
					var url = "<%=path%>/sppriceDemo/toUpdateDate.do";
					if(editTyp == 'price'){
						url = "<%=path%>/sppriceDemo/toUpdatePrice.do";
					};
					var curWindow = $('body').window({
						id: 'window_updateSppriceDemo',
						title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
						content: '<iframe id="window_updateSppriceDemo" name="window_updateSppriceDemo" frameborder="0" src='+url+'></iframe>',
						width: '550px',
						height: '400px',
						draggable: true,
						confirmClose:false,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(editTyp == 'price'){
											var price =  $(window.frames["window_updateSppriceDemo"].document).find('#price').val();
											if(!price || isNaN(price)){
												alert('<fmt:message key="please_enter"/><fmt:message key="price"/>！');
												return;
											}
											execUpdate(price,editTyp);
										}else{
											var date = $(window.frames["window_updateSppriceDemo"].document).find('#date').val();
											execUpdate(date,editTyp);	
										}
										curWindow.close();
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				});
				
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key="price_template_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveSppriceDemo();
							}
						},{
							text: '<fmt:message key="bulk_add"/>',
							title: '<fmt:message key="bulk_add"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								quickSaveSppriceDemo();
							}
						},{
							text: '<fmt:message key="bulk" /><fmt:message key="edit" />',
							title: '<fmt:message key="update" /><fmt:message key="price_template_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								var code = $.trim($("#deliver").val());
								if(!code){
									alert('<fmt:message key="please_select_suppliers" />！');
									return;
								}
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								if(checkboxList 
										&& checkboxList.filter(':checked').size() > 0){
									$('.search-div').slideToggle(100);
								}else{
									alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
								}
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key="price_template_information" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteSppriceDemo();
							}
						},"-",{
							text: '<fmt:message key="add_to_the_new_offer" />',
							title: '<fmt:message key="add_to_the_new_offer" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								insertBatch();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				setElementHeight('.grid',['.tool','.form-line','#ssp_code'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }
	                else
	                {
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	}
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				
				function saveSppriceDemo(){
					var code = $.trim($("#deliver").val());
					if(!code){
						alert('<fmt:message key="please_select_suppliers" />！');
						return;
					}
					var curWindow = $('body').window({
						id: 'window_saveSppriceDemo',
						title: '<fmt:message key="insert" /><fmt:message key="price_template_information" />',
						content: '<iframe id="saveSppriceDemoFrame" name="saveSppriceDemoFrame" frameborder="0" src="<%=path%>/sppriceDemo/add.do?deliver='+code+'"></iframe>',
						width: '550px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="price_template_information" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveSppriceDemoFrame') && getFrame('saveSppriceDemoFrame').validate._submitValidate()){
											submitFrameForm('saveSppriceDemoFrame','sppriceDemoForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										curWindow.close();
									}
								}
							]
						}
					});
				}
				function quickSaveSppriceDemo(){
					var code = $.trim($("#deliver").val());
					if(!code){
						alert('<fmt:message key="please_select_suppliers" />！');
						return;
					}
					if(!!!top.customWindow){
						top.cust('<fmt:message key="Quick_add" />','<%=path%>/sppriceQuick/quickListSppriceDemo.do?code='+code,0,$("#null"),$("#null"),($(top.document.body).width()-20),'600','isNull',quickSppriceDemoUpdate);	
					}
				}
				function quickSppriceDemoUpdate(){
					location.href = location.href;
				}
				function insertBatch(){
					var deliver = $("#deliver").val();
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(!deliver){
						alert('<fmt:message key="please_select_suppliers" />！');
						return;
					}else if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="decide_to_add_quotes_to_the_new_offer" />？')){
							var selected = {};
							var i = 0;
							checkboxList.filter(':checked').each(function(){
								var num = $.trim($(this).closest("tr").find(".num").text());
								selected['demos['+i+'].acct'] = $("#acct").val();
								selected['demos['+i+'].firm'] = $.trim($("#firm_"+num+" input").val());
								selected['demos['+i+'].deliver'] = $.trim($("#deliver").val());
								selected['demos['+i+'].sp_code'] = $.trim($("#sp_code_"+num).text());
								i ++;
							});
							$.ajaxSetup({async:false});
							$.post('<%=path%>/spprice/insertBatch.do',selected,function(data){
								alert('<fmt:message key="successful_added" />！');
								pageReload();
							});
						}else
							return;
					}else{
						alert('<fmt:message key="please_select_add_quotes" />！');
						return ;
					}
				}

				function execUpdate(value,typ){
					var selected = {};
					var i = 0;
					var maxBdat = 0;
					var minEdat = 0;
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					checkboxList.filter(':checked').each(function(){
						var num = $.trim($(this).closest("tr").find(".num").text());
						if(typ != 'price'){
							var curBdat = Number($.trim($("#bdat_"+num).text().replace(/-/g,'')));
							var curEdat = Number($.trim($("#edat_"+num).text().replace(/-/g,'')));
							maxBdat = maxBdat < curBdat ? curBdat : maxBdat;
							minEdat = minEdat < curEdat && minEdat != 0 ? minEdat : curEdat;
							if(typ == 'bdat')selected['demos['+i+'].bdat'] = $.trim(value);
							if(typ == 'edat')selected['demos['+i+'].edat'] = $.trim(value);
						}else{
							selected['demos['+i+'].price'] = value;
							selected['demos['+i+'].priceold'] = $.trim($("#price_"+num).text());
						}
						selected['demos['+i+'].acct'] = $("#acct").val();
						selected['demos['+i+'].firm'] = $.trim($("#firm_"+num).children("input").val());
						selected['demos['+i+'].deliver'] = $.trim($("#deliver").val());
						selected['demos['+i+'].sp_code'] = $.trim($("#sp_code_"+num).text());
						i ++;
					});
					var cur = Number(value.replace(/-/g,''));
					if(typ == 'edat' ? cur > maxBdat : (typ == 'bdat' ? cur < minEdat : true)){
						$.post('<%=path%>/sppriceDemo/saveByUpdateBatch.do',selected,function(data){
							alert('<fmt:message key="update_successful" />！');
							pageReload();
						});
					}else{
						alert('<fmt:message key="startdate_littler_than_enddate" />！');
						return;
					}
				}
				
				function deleteSppriceDemo(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm" />?')){
							var selected = {};
							var i = 0;
							checkboxList.filter(':checked').each(function(){
								var num = $.trim($(this).closest("tr").find(".num").text());
								selected['demos['+i+'].acct'] = $("#acct").val();
								selected['demos['+i+'].firm'] = $.trim($("#firm_"+num+" input").val());
								selected['demos['+i+'].deliver'] = $.trim($("#deliver").val());
								selected['demos['+i+'].sp_code'] = $.trim($("#sp_code_"+num).text());
								i ++;
							});
							$.post('<%=path%>/sppriceDemo/delete.do',selected,function(data){
								alert('<fmt:message key="successful_deleted" />!');
								pageReload();
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
			});
			
			function searchMore(){
				$("#sp_code").val($("#ssp_code").val());
				$("#sp_name").val($("#ssp_name").val());
				$("#firm").val($("#sfirm").val());
				$("#firmDes").val($("#sfirmDes").val());
				
				$("#listForm").submit();
			}
			function ajaxSearch(){
				if (event.keyCode == 13){	
					searchMore();
				} 
			}
		</script>
	</body>
</html>