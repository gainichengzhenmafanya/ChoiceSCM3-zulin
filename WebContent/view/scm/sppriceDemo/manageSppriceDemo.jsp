<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> </title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<style type="text/css">
				.leftFrame{
				width:22%;
				}
				.mainFrame{
				width:78%;
				}
			</style>
		</head>
	<body>
		<div class="leftFrame"> 
		    <iframe src="<%=path%>/deliver/searchByKey.do?use=spprice" id="deliverFrame"></iframe>
    	</div>
	    <div class="mainFrame" id="sppriceFrame">
	 	  <iframe src="<%=path%>/sppriceDemo/list.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
		var curWindow;
		$("#deliverFrame").load(function(){
			$(this).contents().find(".table-body tr").click(function(){
				var deliver = $(this).find("td:eq(1)").text();
				$("#mainFrame").contents().find("#deliver").val(deliver);
				$("#mainFrame").attr("src","<%=path%>/sppriceDemo/list.do?deliver="+deliver);
			});
		});
		
		function pageReload(){
			if(curWindow)
				curWindow.close();
			$("#mainFrame").attr("src","<%=path%>/sppriceDemo/list.do?deliver="+$("#mainFrame").contents().find("#deliver").val());
		}
		
// 		function saveBatchDemo(){
// 			var code = $.trim($("#mainFrame").contents().find("#deliver").val());
// 			if(!code){
// 				alert('<fmt:message key="please_select_suppliers" />！');
// 				return;
// 			}
<%-- 			var url =  "<%=path%>/sppriceDemo/addBatch.do?code="+code; --%>
// 				curWindow = $('body').window({
// 				id: 'window_saveBatchDemo',
// 				title: '<fmt:message key="insert" /><fmt:message key="price_template_information" />',
// 				content: '<iframe id="saveBatchDemoFrame" frameborder="0" src="'+url+'"></iframe>',
// 				width: $(this).width(),
// 				height: $(this).height()-30,
// 				draggable: true,
// 				isModal: true,
// 				confirmClose:false,
// 				topBar: {
// 					items: [{
// 							text: '<fmt:message key="save" />',
// 							title: '<fmt:message key="save" /><fmt:message key="price_template_information" />',
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['-80px','-0px']
// 							},
// 							handler: function(){
								
// 								if(getFrame('saveBatchDemoFrame')){
// 									var frame = $(getFrame('saveBatchDemoFrame').document);
// 									var deliver = frame.find("#deliver").val();
// 									var supply = frame.find("#supply").val();
// 									var positn = frame.find('#positn').val();
									
// 									if(!deliver){alert('<fmt:message key="please_select_suppliers" />！');return;}
// 									if(!supply){
// 										alert('<fmt:message key="please_select_materials" />！');
// 										if(!!!top.customWindow){
// 											var frame = $(getFrame('saveBatchDemoFrame').document);
// 											var offset = {top:0,left:0};
<%-- 											top.cust('请选择物资','<%=path%>/supply/addSupplyBatch.do',offset,frame.find('#supplyShow'),frame.find('#supply'),'760','575','isNull'); --%>
// 										}
// 										return;
// 									}
// 									if(!positn){
// 										alert('<fmt:message key="please_select_branche" />！');
// 										if(!!!top.customWindow){
// 											var frame = $(getFrame('saveBatchDemoFrame').document);
// 											var offset = {top:0,left:0};
<%-- 											top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/addPositnBatch.do',offset,frame.find('#positnShow'),frame.find('#positn'),'760','575','isNull'); --%>
// 										}
// 										return;
// 									}
// 									submitFrameForm('saveBatchDemoFrame','sppriceBatchForm');
// 								}
// 							}
// 						},{
// 							text: '<fmt:message key="select1" /><fmt:message key="supplies_information" />',
// 							title: '<fmt:message key="select1" /><fmt:message key="supplies_information" />',
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['0px','-40px']
// 							},
// 							handler: function(){
// 									if(!!!top.customWindow){
// 										var frame = $(getFrame('saveBatchDemoFrame').document);
// 										var offset = {top:0,left:0};
<%-- 										top.cust('请选择物资','<%=path%>/supply/addSupplyBatch.do',offset,frame.find('#supplyShow'),frame.find('#supply'),'760','575','isNull'); --%>
// 									}
// 								}
// 							},{
// 							text: '<fmt:message key="select1" /><fmt:message key="branches_positions" />',
// 							title: '<fmt:message key="select1" /><fmt:message key="branches_positions" />',
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['0px','-40px']
// 							},
// 							handler: function(){
// 									if(!!!top.customWindow){
// 										var frame = $(getFrame('saveBatchDemoFrame').document);
// 										var offset = {top:0,left:0};
<%-- 										top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/addPositnBatch.do',offset,frame.find('#positnShow'),frame.find('#positn'),'760','575','isNull'); --%>
// 									}
// 								}
// 							}
// 					]
// 				}
// 			});
// 			curWindow.max();
// 		}
		
		</script>
	</body>
</html>