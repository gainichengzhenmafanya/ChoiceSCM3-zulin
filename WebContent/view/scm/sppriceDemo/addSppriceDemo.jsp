<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="price_template_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	</head>
	<body>
	<div class="form"><div>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:60px;">
			<form id="sppriceDemoForm" method="post" action="<%=path %>/sppriceDemo/saveByAdd.do">
			<input type="hidden" value="${deliver.code}" name="deliver" id="deliver"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="supplies_code" />：</div>
					<div class="form-input">
						<input type="text" id="sp_code" readonly="readonly" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')"  autocomplete="off"/>
						<img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="supplies_name" />：</div>
					<div class="form-input">
						<input type="text" id="sp_name" readonly="readonly" name="sp_init" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="specification" />：</div>
					<div class="form-input">
						<input type="text" id="sp_desc" readonly="readonly" name="sp_desc" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="unit" />：</div>
					<div class="form-input">
						<input type="text" id="unit" readonly="readonly" name="unit" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="price" />：</div>
					<div class="form-input">
						<input type="text" id="price" name="price" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate" />：</div>
					<div class="form-input">
						<input type="text" id="bdat" name="bdat" class="Wdate text" value="${bdat}"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate" />：</div>
					<div class="form-input">
						<input type="text" id="edat" name="edat" class="Wdate text" value="${edat}"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="branches_positions" />：</div>
					<div class="form-input">
						<input id="show" readonly="readonly" class="text"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt="查询仓位"/>
						<input type="hidden" id="value" name="firm"/>
					</div>
				</div>
			</form>
			</div>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript">
		var t;
		function ajaxSearch(key){
			return;
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		function clearQueryForm(){
			$('#listForm select option').removeAttr("selected");
			$('#listForm select option[value=""]').attr("selected","selected");
		}
		//弹出物资树回调函数
		function handler(sp_code){
			$('#sp_code').val(sp_code); 
			if(sp_code==undefined || ''==sp_code){
				 $('#sp_init').val('');
				 $('#sp_name').val('');
				 $('#sp_desc').val('');
				 $('#price').val('');
				 $('#unit').val('');
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					 $('#sp_init').val(supply.sp_init);
					 $('#sp_name').val(supply.sp_name);
					 $('#sp_desc').val(supply.sp_desc);
					 $('#price').val(supply.sp_price);
					 $('#unit').val(supply.unit);
				}
			});
		}
		$(document).ready(function(){
			$('#seachSupply').bind('click.custom',function(e){
				var defaultCode = $('#sp_code').val();
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'),null,null,null,null,null,handler);
				}
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'deliver',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="suppliers" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'sp_code',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'price',
					validateType:['canNull','num'],
					param:['F','F'],
					error:['<fmt:message key="price" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="price" />不是有效数字！']
				},{
					type:'text',
					validateObj:'bdat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="startdate" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'edat',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="enddate" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'show',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="branches_positions" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'edat',
					validateType:['handler'],
					handler:function(){
						var b = $("#bdat").val();
						var e = $("#edat").val();
						var bdat = new Date();
						var edat = new Date();
						bdat.setFullYear(b.substring(0,4),b.substring(5,7),b.substring(8,10));
						edat.setFullYear(e.substring(0,4),e.substring(5,7),e.substring(8,10));
						if(edat.getTime() >= bdat.getTime())
							return true;
						else
							return false;
					},
					param:['F'],
					error:['<fmt:message key="startdate_littler_than_enddate" />！']
				}]
			});
			$('#seachPositn').bind('click.custom',function(e){
				$('#show').next('.validateMsg').remove();
				if(!!!top.customWindow){
					var defaultCode = $('#value').val();
					var defaultName = $('#show').val();
					var offset = {top:0,left:0};
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?typn=1-2-3-4-5-6-7&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#show'),$('#value'),'750','500','isNull');
				}
			});
			$("#bdat").bind('click',function(){
				new WdatePicker();
			});
			$("#edat").bind('click',function(){
				new WdatePicker();
			});
		});
		</script>
	</body>
</html>