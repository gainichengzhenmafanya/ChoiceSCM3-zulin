<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
   	<title>入库汇总查询</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:-3px;
			cursor: pointer;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
		<input type="hidden" id="deliverCode" name="delivercode" value="${supplyAcct.delivercode}"/>
		<div class="form-line"  style="z-index:10;">
			<div class="form-label" ><fmt:message key="startdate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" /></div>
			<div class="form-label"><fmt:message key="positions"/></div>
			<div class="form-input" >
				<input type="text"  id="positn_name"  name="positn_name" readonly="readonly" value=""/>
				<input type="hidden" id="positn" name="positn" value=""/>
				<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
			</div>
		</div>
		<div class="form-line" >
			<div class="form-label" ><fmt:message key="enddate"/></div>
			<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text"/></div>
			<div class="form-label"><fmt:message key="coding"/></div>
			<div class="form-input" style="margin-top: -3px;*margin-top:-2px;">
				<input type="text" name="sp_code" id="sp_code" style="width: 136px;*width:134px;" autocomplete="off" class="text" value="<c:out value="${supplyAcct.sp_code}" />"/>
				<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>'/>	
			</div>
<%-- 			<div class="form-label"><fmt:message key="suppliers"/></div> --%>
<!-- 			<div class="form-input"> -->
<%-- 			<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${supplyAcct.deliverdes}"/> --%>
<%-- 			<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
<!-- 			</div> -->
		</div>
	</form>
 	<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
	  	var deliverCode = $('#deliverCode').val();
		if(deliverCode == ''){
			alert('<fmt:message key="The_current_account_is_not_set_the_corresponding_suppliers"/>！<fmt:message key="This_node_is_not_available"/>！');
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
// 							var deCode = $("#positn").val();
// 							if(deCode==""||deCode==null){
// 								alert("请先选择仓位");
// 								return;
// 							}
							var form = $("#queryForm").find("*[name]");
							form = form.filter(function(index){
								var cur = form[index];
								if($(cur).attr("name")){
									if((cur.tagName.toLowerCase() != 'select' || $.trim($(cur).css("display")) != 'none')){
										if(cur.tagName.toLowerCase() == 'input' && ($(cur).attr("type").toLowerCase() == 'radio' || $(cur).attr("type").toLowerCase() == 'checkbox')){
											if($("input[name='"+$(cur).attr("name")+"']:checked").length){
												params[$(cur).attr("name")] = $("input[name='"+$(cur).attr("name")+"']:checked").val();
											}else{
												params[$(cur).attr("name")] = undefined;
											}
										}else{
											params[$(cur).attr("name")] = $(cur).val();
										}
									}
								}
								
							});
							$("#datagrid").datagrid("load");
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$('#queryForm').attr('action',"<%=path%>/deliverPlat/exportChkinDetailSum.do");
							$('#queryForm').submit();
						}
					},{
// 						text: '<fmt:message key="print" />',
// 						title: '<fmt:message key="print" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							$('#queryForm').attr('target','report');
// 							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
<%-- 							var action="<%=path%>/deliverPlat/printChkinDetailSum.do"; --%>
// 							$('#queryForm').attr('action',action);
// 							$('#queryForm').submit();
// 						}
// 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		
  	 		var tableHeight = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height() - $(".tool").height() - $("#queryForm").height();
  	 		$("select").each(function(){
  	 			$(this).htmlUtils("select",[
  	 			  	 			            {key:'<fmt:message key="normal_storage"/>',value:'<fmt:message key="normal_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="overage_storage"/>',value:'<fmt:message key="overage_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="gifts_warehousing"/>',value:'<fmt:message key="gifts_warehousing"/>'},
  	 			  	 			            {key:'<fmt:message key="recycling_storage"/>',value:'<fmt:message key="recycling_storage"/>'},
  	 			  	 			            {key:'<fmt:message key="consignment_warehousing"/>',value:'<fmt:message key="consignment_warehousing"/>'},
  	 			  	 			           	{key:'<fmt:message key="purchase_storage"/>',value:'<fmt:message key="purchase_storage"/>'},
  	 			  	 			        	{key:'<fmt:message key="normal_shipments"/>',value:'<fmt:message key="normal_shipments"/>'},
  	 			  	 			     		{key:'<fmt:message key="gifts_shipments"/>',value:'<fmt:message key="gifts_shipments"/>'},
  	 			  	 			  			{key:'<fmt:message key="consignment_shipments"/>',value:'<fmt:message key="consignment_shipments"/>'},
  	 			  	 						{key:'<fmt:message key="returns"/>',value:'<fmt:message key="returns"/>'},
  	 			  	 						{key:'<fmt:message key="reversal"/>',value:'<fmt:message key="reversal"/>'},
  	 			  	 			         ]);
  	 		});
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		var des = $('#des'); 
  	 		$('#des').parent().html("").append(des);
  	 		$('#des').htmlUtils('select',[{key:'<fmt:message key="unqualified"/>',value:'<fmt:message key="unqualified"/>'}]);
  	 		//定义类型为Date的列，js将根据此变量解析Data类型数据默认解析成yyyy-MM-dd样式
  	 		var dateCols = ['dat'];
  	 		//数字列
  	 		var numCols = ['pricein','cntin','amtin','priceintax','amtintax'];
  	 		//收集form表单数据的对象
  	 		var params = {};
  	 		//Controller传来的map对象，包含所需要显示的报表所有列的dictColumns对象，和需要固定在左侧的col的index（用，分割）
  	 		var tableContent = {};
  	 		//表头行（单行）
  	 		var columns = [];
  	 		//表头（多行），其中元素为columns
  	 		var head = [];
  	 		//需要固定在左侧的列的表头（单行）
  	 		var frozenHead = [];
  	 		//需要固定在左侧的列的表头（多行），元素为frozenHead
  	 		var frozenColumns = [];
  	 		//ajax获取报表表头
  	 		$.ajax({url:"<%=path%>/deliverPlat/findChkinDSHeaders.do",
  	 				async:false,
  	 				success:function(data){
  	 					tableContent = data;
  	 				}
  	 			});
  	 		//解析获取的数据
  	 		var frozenIndex = tableContent.frozenColumns.split(',');
  	 		var Cols = [];
  	 		var colsSecond = [];
			var prev = '';
			var temp;
  	 		for(var i in tableContent.columns)Cols.push(tableContent.columns[i].zhColumnName);
  	 		var t = Cols.toString().match(/,([\d\D]+?)\|[\d\D]+?(?=,)/g);
  	 		if(t && !t.length){
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
	  	 			if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 			else
	  	 				columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,align:align});
	  	 		}
				head.push(columns);
	  	 		frozenHead.push(frozenColumns);
  	 		}else{
  	 			for(var i in tableContent.columns){
  	 				var align = $.inArray(tableContent.columns[i].properties.toLowerCase(),numCols) >= 0 ? "right" : "left"; 
  	 				if($.inArray(tableContent.columns[i].id,frozenIndex) >= 0)
	  	 				frozenColumns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 				else{
  	 					var cur = tableContent.columns[i].zhColumnName.match(/^([\d\D]+)\|[\d\D]+$/g);
  	 					if(cur && cur.length){
  	 						var cur = tableContent.columns[i].zhColumnName;
  	 						if(cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1") == prev){
  	 							temp.colspan ++;
  	 						}else{
  	 							temp = {title:cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1"),colspan:1};
  	 							columns.push(temp);
  	 							prev = cur.replace(/^([\d\D]+)\|[\d\D]+$/g,"$1");
  	 						}
  	 						colsSecond.push({field:tableContent.columns[i].columnName.toUpperCase(),title:cur.replace(/^([\d\D]+)\|([\d\D]+)$/g,"$2"),width:tableContent.columns[i].columnWidth,sortable:true,colspan:1,align:align});
  	 					}else{
  	 						if(tableContent.columns[i].columnName)
  	 							columns.push({field:tableContent.columns[i].columnName.toUpperCase(),title:tableContent.columns[i].zhColumnName,width:tableContent.columns[i].columnWidth,sortable:true,rowspan:2,align:align});
  	 					}
  	 				}
  	 			}
  	 			head.push(columns);
  	 			head.push(colsSecond);
  	 			frozenHead.push(frozenColumns);
  	 		}
  	 		//生成报表数据表格
  	 		$("#datagrid").datagrid({
  	 			title:'<fmt:message key="warehousing_aggregate_query"/>',
  	 			width:'100%',
  	 			height:tableHeight,
  	 			nowrap: true,
				striped: true,
				singleSelect:true,
				collapsible:true,
				//对从服务器获取的数据进行解析格式化
	 			dataFilter:function(data,type){
	 				var rs = eval("("+data+")");
	 				var modifyRows = [];
	 				var rows = rs.rows;
	 				if(!rows || rows.length <= 0)$('#datagrid').datagrid('loadData',{total:0,rows:[],footer:[]});
	 				for(var i in rows){
	 					var cols = tableContent.columns;
						var curRow = {};
	 					for(var j in cols){
	 						try{
	 							var value = eval("rows["+i+"]."+cols[j].properties) ;
	 							value = $.inArray(cols[j].properties,numCols) >=0 ? (value ? value.toFixed(2) : '0.00') : (value ? ($.inArray(cols[j].properties,dateCols) >= 0 ? convertDate(value) : value):'');
	 							curRow[cols[j].columnName.toUpperCase()] = value;
	 						}catch(e){
	 							alert('Exception'+"rows["+i+"]."+cols[j].properties);
	 						}
	 					}
	 					modifyRows.push(curRow);
	 				}
	 				rs.rows = modifyRows;
	 				//非固定列，第一列显示数据总行数
	 				rs.footer[0][columns[0].field] = rs.total;
	 				return $.toJSON(rs);
	 			},
				url:"<%=path%>/deliverPlat/findChkinDetailSum.do",
				remoteSort: true,
				//页码选择项
				pageList:[20,30,40,50],
				frozenColumns:frozenHead,
				columns:head,
				queryParams:params,
				showFooter:true,
				pagination:true,
				rownumbers:true,
				rowStyler:function(){
					return 'line-height:11px';
				},
				onBeforeLoad:function(){
					if(!$("#firstLoad").val())
						return false;
				},
  	 			onDblClickRow:function(index,data){
  	 				var grptypname = typeof($("#grptyp").data("checkedName"))!="undefined"?$("#grptyp").data("checkedName"):"";
  	 				var grpname = typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"";
  	 				var typname = typeof($("#typ").data("checkedName"))!="undefined"?$("#typ").data("checkedName"):"";
  	 				var chktypname = typeof($("#chktyp").data("checkedName"))!="undefined"?$("#chktyp").data("checkedName"):"";
  	 				var parameters = {"sp_code":data['SP_CODE'],"bdat":$("#bdat").val(),"edat":$("#edat").val(),
  	 						"delivercode":$("#delivercode").val(),"deliverdes":$("#deliver_name").val(),
  	 						"grptyp":$("#grptyp").data("checkedVal"),"grptypdes":grptypname,
  	 						"positn":$("#positn").val(),"positndes":$("#positn_name").val(),
  	 						"grp":$("#grp").data("checkedVal"),"grpdes":grpname,
  	 						"typ":$("#typ").data("checkedVal"),"typdes":typname,
  	 						"chktyp":chktypname};
    	 	  		openTag("81004c210e0d46329fcca4bb9d54c4c6","<fmt:message key ="warehousing_detail_query" />","<%=path%>/deliverPlat/toChkinDetailS.do",parameters);
  	 			}
  	 		});
  	 		$("#firstLoad").val("true");
  	 		
  	 		$("#bdat,#edat").focus(function(){
  	 			new WdatePicker();
  	 		});
  	 		
  	 		$(".panel-tool").remove();

  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#positn").val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn=1-5&mold=oneTmany&defaultCode='+defaultCode),offset,$('#positn_name'),$('#positn'),'760','520','isNull');
				}
			});
  	 	});
  	 </script>
  </body>
</html>
