<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>门店报货查询</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/deliverPlat/listChkstom.do" method="post">
			<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode }"/>
			<div class="form-line">
				<div class="form-label"><fmt:message key="reported_date"/></div>
				<div class="form-input"><input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker()"/></div>						
 				<div class="form-label"><fmt:message key="branche"/></div>
				<div class="form-input" style="width:150px;">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="orders_num"/>(<fmt:message key="document_number"/>)</div>
				<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="${dis.vouno }"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="arrival_date"/></div>
				<div class="form-input"><input type="text" id="ind" name="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker()"/></div>
				<div class="form-label"><fmt:message key="coding"/></div>
				<div class="form-input" style="margin-top: -3px;*margin-top:-2px;">
					<input type="text" name="sp_code" id="sp_code" style="width: 136px;*width:134px;" class="text" value="<c:out value="${dis.sp_code}" />"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>'/>	
				</div>
				<div class="form-label"><fmt:message key="Whether_to_confirm"/></div>
				<div class="form-input" style="width:150px;">
					<select id="deliveryn" name="deliveryn" style="width:135px;">
						<option value="" <c:if test="${empty dis.deliveryn }">selected="selected"</c:if>><fmt:message key="all"/></option>
						<option value="N" <c:if test="${dis.deliveryn == 'N' }">selected="selected"</c:if>><fmt:message key="Unconfirmed"/></option>
						<option value="Y" <c:if test="${dis.deliveryn == 'Y' }">selected="selected"</c:if>><fmt:message key="Confirmed"/></option>
					</select>
				</div>
				<div class="form-label"><input type="checkbox" id="yndo" name="yndo" value="Y"/><fmt:message key="collect"/></div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width:25px;"></span></td>
								<td rowspan="2"><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="branches_encoding"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="branches_name"/></span></td>
								<td rowspan="2"><span style="width:85px;"><fmt:message key="arrival_date"/></span></td>
								<td rowspan="2"><span style="width:85px;"><fmt:message key="reported_date"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="reported_num"/></span></td>
								<td rowspan="2"><span style="width:130px;"><fmt:message key="document_number"/></span></td>
								<td colspan="3"><span><fmt:message key="supplies"/></span></td>
								<td colspan="2"><span><fmt:message key="procurement_unit"/></span></td>
								<td colspan="2"><span><fmt:message key="standard_unit"/></span></td>
								<td rowspan="2" <c:if test="${isReportJmj == 0 }"></c:if>><span style="width:60px;"><fmt:message key="unit_price"/></span></td>
								<td rowspan="2" <c:if test="${isReportJmj == 0 }"></c:if>><span style="width:70px;"><fmt:message key="amount"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="whether"/><fmt:message key="confirm"/></span></td>
							</tr>
							<tr>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr data-id="${dis.id }" data-deliveryn="${dis.deliveryn }">
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span>
									</td>
									<td><span style="width:60px;" title="${dis.firmCode }">${dis.firmCode }</span></td>
									<td><span style="width:100px;" title="${dis.firmDes }">${dis.firmDes }</span></td>
									<td><span style="width:85px;" title="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:85px;" title="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:70px;" title="${dis.chkstoNo }">${dis.chkstoNo }</span></td>
									<td><span style="width:130px;" title="${dis.vouno }">${dis.vouno }</span></td>
									<td><span title="${dis.sp_code }" style="width:65px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:60px;">${dis.sp_desc }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:40px;" title="${dis.unit }">${dis.unit }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:40px;" title="${dis.unit }">${dis.unit }</span></td>
									<td <c:if test="${isReportJmj == 0 }"></c:if>><span title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td <c:if test="${isReportJmj == 0 }"></c:if>><span title="<fmt:formatNumber value="${dis.amountin * dis.pricein}" type="currency" pattern="0.00"/>" style="width:70px;text-align:right"><fmt:formatNumber value="${dis.amountin * dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.memo }" style="width:60px;">${dis.memo }</span></td>
									<td>
										<span style="width:50px;text-align:center;">
											<img src="<%=path%>/image/kucun/${dis.deliveryn}.png"/>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var deliverCode = $('#deliverCode').val();
		if(deliverCode == ''){
			alert('<fmt:message key="The_current_account_is_not_set_the_corresponding_suppliers"/>！<fmt:message key="This_node_is_not_available"/>！');
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
			}); 
			
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="confirm" />',
					title: '<fmt:message key="confirm" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-58px','-240px']
					},
					handler: function(){
						confirmChkstom();
					}
				},'-',{
					text: 'Excel',
					title: 'Excel',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						exportChkstom();
					}
				},{
					text: '<fmt:message key="print" />',
					title: '<fmt:message key="print" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','0px']
					},
					items:[{
						text: '<fmt:message key="print_distribution_single" />',
						title: '<fmt:message key="print_distribution_single" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							printDis("printIndent.do");
						}
					},{
						text: '<fmt:message key="print_delivery_note" />',
						title: '<fmt:message key="print_delivery_note" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							printDis("printDelivery.do");
						}
					},{
						text: '<fmt:message key="print_inspection_single" />',
						title: '<fmt:message key="print_inspection_single" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							printDis("printInspection.do");
						}
					}]
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		  //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			
// 			$("#seachPositn").click(function(){
// 				chooseStoreSCM({
<%-- 					basePath:'<%=path%>', --%>
// 					width:600,
// 					firmId:$("#firmCode").val(),
// 					single:false,
// 					tagName:'firmDes',
// 					tagId:'firmCode',
// 					title:'<fmt:message key="please_select_positions"/>'
// 				});
// 			});
			
			$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#firmCode").val();
					var defaultName = $("#firmDes").val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn=1-5&mold=oneTmany&defaultCode='+defaultCode),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
				}
			});
			
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
		});
		
		//导出
		function exportChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$("#listForm").attr("action","<%=path%>/deliverPlat/exportChkstom.do");
			$('#listForm').submit();
			$("#wait2 span").html("数据导出中，请稍后...");
			$("#listForm").attr("action","<%=path%>/deliverPlat/listChkstom.do");
			$("#wait2").val('');//等待加载还原
		}
		
		//确认操作
		function confirmChkstom(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() >= 1){
				var flag = true;
				if (confirm('<fmt:message key ="Are_the_selected_data_to_confirm?_Operation_can_not_be_returned" />！')) {
					var codeValue=[];
					checkboxList.filter(':checked').each(function(){
						if($(this).parents('tr').data("deliveryn") == 'Y'){
							alert('<fmt:message key ="The_selected_data_already_contains_the_confirmed_data" />！');
							flag = false;
						}
						codeValue.push($.trim($(this).val()));
					});
					if(flag){
						var action = '<%=path%>/deliverPlat/confirmChkstom.do?ids='+codeValue.join(",");
						$.ajax({
							url:action,
							type:'post',
							success:function(data){
								alert('<fmt:message key ="Confirmation_of_success" />！');
								$('#listForm').submit();
							}
						});
					}
				}
			}else{
				alert('<fmt:message key="please_select_data" />！');
				return ;
			}
		}
		
		//打印单据
		function printDis(e){
			if($('.grid').find('.table-body').find("tr").size()<1){
				alert('<fmt:message key ="Data_is_empty_can_not_print" />！！');
				return;
			}
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/deliverPlat/'+e;	
			var action1="<%=path%>/deliverPlat/listChkstom.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		};
		
		</script>				
	</body>
</html>