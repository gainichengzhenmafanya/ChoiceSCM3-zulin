<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>门店验货确认按物资</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
		</style>					
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/deliverPlat/listArrivalm.do" method="post">
			<input type="hidden" id="pk_supplier" name="pk_supplier" value="${arrivalm.pk_supplier }"/>
			<div class="bj_head">
			<table>
				<tr>
					<td>
						<fieldset  style="width: 200px;height: 70px;float: left;">
		 					<legend><input type="radio" name="dr" value="0" <c:if test="${arrivalm.dr == 0}">checked="checked"</c:if>/>按到货
		 					&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="dr" value="1" <c:if test="${arrivalm.dr == 1}">checked="checked"</c:if>/>按报货</legend>
		  					<span><fmt:message key="startdate"/>:
								<input type="text" id="bdate" name="bdate" class="Wdate text" value="${arrivalm.bdate }" onclick="new WdatePicker()"/>
							</span><br/>
							<span><fmt:message key="enddate"/>:
								<input type="text" id="edate" name="edate" class="Wdate text" value="${arrivalm.edate }" onclick="new WdatePicker()"/>
							</span>
						</fieldset>
					</td>
					<td>
						<div class="form-line">
							<div class="form-label">到货单单号:</div>
							<div class="form-input"><input type="text" id="varrbillno" name="varrbillno" class="text" value="${arrivalm.varrbillno}"/></div>
						</div>
						<div class="form-line">
							<div class="form-label">报货单单号:</div>
							<div class="form-input"><input type="text" id="vbatchno" name="vbatchno" class="text" value="${arrivalm.vbatchno}"/></div>
						</div>
					</td>
					<td>
						<div class="form-line">
							<div class="form-label"><fmt:message key="stores"/>:</div>
							<div class="form-input">
								<input type="text" id="positn_name" name="positnDes" readonly="readonly" value="${arrivalm.positnDes}"/>
								<input type="hidden" id="positn" name="pk_org" value="${arrivalm.pk_org}"/>
								<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
							</div>
						</div>
						<div class="form-line">
							<div class="form-label">单据<fmt:message key="status"/>:</div>
							<div class="form-input">
								<select name="istate" style="width:135px;margin-top:3px;"><!-- 0:未操作1已保存 2已确认 3已验货 -->
									<option value="" <c:if test="${empty arrivalm.istate }">selected="selected"</c:if>>全部</option>
									<option value="0" <c:if test="${arrivalm.istate == 0 }">selected="selected"</c:if>>门店未编辑</option>
									<option value="1" <c:if test="${arrivalm.istate == 1 }">selected="selected"</c:if>>供应商未确认(门店已编辑)</option>
									<option value="2" <c:if test="${arrivalm.istate == 2 }">selected="selected"</c:if>>供应商已确认</option>
									<option value="3" <c:if test="${arrivalm.istate == 3 }">selected="selected"</c:if>>已审核</option>
								</select>
							</div>
						</div>
					</td>
					<td>
						<fieldset style="width: 200px;height: 70px;float: left;margin-left:50px;">
		 					<legend><input type="checkbox" name="selectType" id="selectType" value="1" <c:if test="${arrivalm.selectType == 1}">checked="checked"</c:if>/>按物资汇总</legend>
<%-- 		  					<div id="supplydiv" <c:if test="${arrivalm.selectType != 1}"> style="display:none;"</c:if>> --%>
							<span>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="chkstono" value="0" <c:if test="${arrivalm.chkstono == 0}">checked="checked"</c:if>/>全部
								<input type="radio" name="chkstono" value="1" <c:if test="${arrivalm.chkstono == 1}">checked="checked"</c:if>/>标准单位
								<input type="radio" name="chkstono" value="3" <c:if test="${arrivalm.chkstono == 3}">checked="checked"</c:if>/>配送单位
							</span>
		  					<br/>
							<span>&nbsp;&nbsp;&nbsp;&nbsp;物资编码:
								<input type="text" name="sp_code" id="sp_code" class="text" value="${arrivalm.sp_code}" style="width:100px;"/>
								<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>'/>	
							</span>
<!-- 							</div> -->
						</fieldset>
<!-- 						<div class="form-line"> -->
<!-- 							<div class="form-label"></div> -->
<!-- 							<div class="form-input"> -->
<%-- 								<input type="checkbox" name="selectType" id="selectType" value="1" <c:if test="${arrivalm.selectType == 1}">checked="checked"</c:if>/>按物资汇总 --%>
<!-- 							</div> -->
<!-- 						</div> -->
<%-- 						<div class="form-line" id="supplydiv" <c:if test="${arrivalm.selectType != 1}"> style="display:none;"</c:if>> --%>
<!-- 							<div class="form-label">物资编码:</div> -->
<!-- 							<div class="form-input"> -->
<%-- 								<input type="text" name="sp_code" id="sp_code" class="text" value="${arrivalm.sp_code}"/> --%>
<%-- 								<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>'/>	 --%>
<!-- 							</div> -->
<!-- 						</div> -->
					</td>
				</tr>
			</table>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<c:choose>
								<c:when test="${arrivalm.chkstono == 0 }">
									<tr>
										<td rowspan="2" class="num"><span style="width:25px;"></span></td>
										<td rowspan="2"><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
										<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
										<td rowspan="2"><span style="width:80px;"><fmt:message key="specification"/></span></td>
										<td rowspan="2"><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
										<td colspan="3"><span><fmt:message key="total"/></span></td>
										<c:forEach var="positn" items="${positnList }">
											<td colspan="3"><span>${positn.des }</span></td>
										</c:forEach>
									</tr>
									<tr>
										<td><span style="width:60px;">标准数量</span></td>
<!-- 										<td><span style="width:40px;">单位</span></td> -->
										<td><span style="width:60px;">配送数量</span></td>
										<td><span style="width:40px;">单位</span></td>
										<c:forEach var="positn" items="${positnList }">
											<td><span style="width:40px;">标准<br/>数量</span></td>
											<td><span style="width:40px;">配送<br/>数量</span></td>
											<td><span style="width:40px;">配送<br/>单位</span></td>
										</c:forEach>
									</tr>
								</c:when>
								<c:when test="${arrivalm.chkstono == 3 }">
									<tr>
										<td rowspan="2" class="num"><span style="width:25px;"></span></td>
										<td rowspan="2"><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
										<td rowspan="2"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
										<td rowspan="2"><span style="width:80px;"><fmt:message key="specification"/></span></td>
										<td rowspan="2"><span style="width:40px;"><fmt:message key="standard_unit_br"/></span></td>
										<td colspan="2"><span><fmt:message key="total"/></span></td>
										<c:forEach var="positn" items="${positnList }">
											<td colspan="2"><span>${positn.des }</span></td>
										</c:forEach>
									</tr>
									<tr>
										<td><span style="width:60px;">配送数量</span></td>
										<td><span style="width:40px;">单位</span></td>
										<c:forEach var="positn" items="${positnList }">
											<td><span style="width:40px;">数量</span></td>
											<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
										</c:forEach>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<td class="num"><span style="width:25px;"></span></td>
										<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
										<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
										<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
										<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
										<td><span style="width:80px;"><fmt:message key="total"/></span></td>
										<c:forEach var="positn" items="${positnList }">
											<td><span style="width:80px;">${positn.des }</span></td>
										</c:forEach>
									</tr>
								</c:otherwise>
							</c:choose>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="ad" items="${arrivaldList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:65px;" title="${ad.SP_CODE }">${ad.SP_CODE }</span></td>
									<td><span style="width:100px;" title="${ad.SP_NAME }">${ad.SP_NAME }</span></td>
									<td><span style="width:80px;" title="${ad.SP_DESC }">${ad.SP_DESC }</span></td>
									<td><span style="width:40px;" title="${ad.UNIT }">${ad.UNIT }</span></td>
									<c:choose>
										<c:when test="${arrivalm.chkstono == 0 }">
											<td><span style="text-align:right;width:60px;" title="${ad.TOTAL }"><fmt:formatNumber value="${ad.TOTAL }" type="currency" pattern="0.00"/></span></td>
<%-- 											<td><span style="width:40px;" title="${ad.UNIT }">${ad.UNIT }</span></td> --%>
											<td><span style="text-align:right;width:60px;" title="${ad.DISTOTAL }"><fmt:formatNumber value="${ad.DISTOTAL }" type="currency" pattern="0.00"/></span></td>
											<td><span style="width:40px;" title="${ad.DISUNIT }">${ad.DISUNIT }</span></td>
											<c:forEach var="positn" items="${positnList }">
												<c:set var="p" value="C_${positn.code }"></c:set>
												<c:set var="d" value="D_${positn.code }"></c:set>
												<c:set var="u" value="U_${positn.code }"></c:set>
												<td><span style="text-align:right;width:40px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[p] }'>
													<fmt:formatNumber value="${ad[p] }" type="currency" pattern="0.00"/></span></td>
												<td><span style="text-align:right;width:40px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[u] }'>
													<fmt:formatNumber value="${ad[u] }" type="currency" pattern="0.00"/></span></td>
												<td><span style="width:40px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[d] }'>${ad[d] }</span></td>
											</c:forEach>
										</c:when>
										<c:when test="${arrivalm.chkstono == 3 }">
											<td><span style="text-align:right;width:60px;" title="${ad.DISTOTAL }"><fmt:formatNumber value="${ad.DISTOTAL }" type="currency" pattern="0.00"/></span></td>
											<td><span style="width:40px;" title="${ad.DISUNIT }">${ad.DISUNIT }</span></td>
											<c:forEach var="positn" items="${positnList }">
												<c:set var="p" value="C_${positn.code }"></c:set>
												<c:set var="d" value="D_${positn.code }"></c:set>
												<c:set var="u" value="U_${positn.code }"></c:set>
												<td><span style="text-align:right;width:40px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[u] }'>
													<fmt:formatNumber value="${ad[u] }" type="currency" pattern="0.00"/></span></td>
												<td><span style="width:40px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[d] }'>${ad[d] }</span></td>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<td><span style="text-align:right;width:80px;" title="${ad.TOTAL }"><fmt:formatNumber value="${ad.TOTAL }" type="currency" pattern="0.00"/></span></td>
											<c:forEach var="positn" items="${positnList }">
												<c:set var="p" value="C_${positn.code }"></c:set>
												<td><span style="text-align:right;width:80px;<c:if test='${ad[p]== 0}'>color:#B7B7B7;</c:if>" title='${ad[p] }'>
													<fmt:formatNumber value="${ad[p] }" type="currency" pattern="0.00"/></span></td>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var pk_supplier = $('#pk_supplier').val();
		if(pk_supplier == ''){
			alert('<fmt:message key="The_current_account_is_not_set_the_corresponding_suppliers"/>！<fmt:message key="This_node_is_not_available"/>！');
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
		var validate;
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
			});  
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: 'Excel',
					title: 'Excel',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						exportArrivalm();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
// 			changeTh();
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					sta:'all',
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>',
					typn:'1203'
				});
			});
			
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
			$("#selectType").click(function(){
				if($(this).get(0).checked)
					$('#supplydiv').show();
				else
					$('#supplydiv').hide();
			});
			
			//导出
			function exportArrivalm(){
				$("#wait2").val('NO');//不用等待加载
				$("#listForm").attr("action","<%=path%>/deliverPlat/exportArrivalm.do");
				$('#listForm').submit();
				$("#wait2 span").html("数据导出中，请稍后...");
				$("#listForm").attr("action","<%=path%>/deliverPlat/listArrivalm.do");
				$("#wait2").val('');//等待加载还原
			}
		});
		</script>				
	</body>
</html>