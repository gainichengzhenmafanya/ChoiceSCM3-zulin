<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>直配按单据验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>	
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.ico-clear {
				    background-position: 0px 0px;
				    background-image: url("<%=path%>/image/Window/operator.gif");
			    	    background-repeat: no-repeat;
			    	    cursor: pointer;
			    	    width: 15px;
			    	    height: 15px;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				height:22px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="pr" value="${arrivalm.pr }"/>
		<form id="listForm" action="<%=path%>/deliverPlat/findArrivald.do" method="post">
			<input type="hidden" id="pk_arrival" name="pk_arrival" value="${arrivalm.pk_arrival }"/>
			<input type="hidden" id="vbatchno" name="vbatchno" value="${arrivalm.vbatchno }"/>
			<input type="hidden" id="pk_org" name="pk_org" value="${arrivalm.pk_org }"/>
			<input type="hidden" id="istate" value="${arrivalm.istate }"/>
			<div class="form-line">	
				<div class="form-label">到货单号:</div>
				<div class="form-input">
					${arrivalm.varrbillno }
					<input type="hidden" id="varrbillno" name="varrbillno" value="${arrivalm.varrbillno }"/>
				</div>
				<div class="form-label"><fmt:message key="branches_name"/>:</div>
				<div class="form-input">
					${arrivalm.positnDes}
					<input type="hidden" id="positnDes" name="positnDes" value="${arrivalm.positnDes}"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label">单据日期:</div>
				<div class="form-input">
					${arrivalm.darrbilldate}
					<input type="hidden" id="darrbilldate" name="darrbilldate" value="${arrivalm.darrbilldate}"/>
				</div>
				<div class="form-label"><fmt:message key="status"/>:</div>
				<div class="form-input">
					<c:choose>
						<c:when test="${arrivalm.istate == 0 }">门店未编辑</c:when>
						<c:when test="${arrivalm.istate == 1 }">供应商未确认(门店已编辑)</c:when>
						<c:when test="${arrivalm.istate == 2 }">供应商已确认</c:when>
						<c:otherwise>已审核</c:otherwise>
					</c:choose>
				</div>
				<div class="form-label"><fmt:message key="summary"/>:</div>
				<div class="form-input">
					${arrivalm.vmemo}
					<input type="hidden" id="vmemo" name="vmemo" value="${arrivalm.vmemo }" />					
				</div>
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="5"><fmt:message key="standard_unit"/></td>
								<td colspan="4">配送单位</td>
<%-- 								<td rowspan="2"><span style="width:40px;"><fmt:message key="unit_price"/></span></td> --%>
<%-- 								<td rowspan="2"><span style="width:60px;"><fmt:message key="amount"/></span></td> --%>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:80px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:60px;"><fmt:message key="reports"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;">到货<fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;">到货<fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="inspection"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:60px;"><fmt:message key="difference"/><fmt:message key="quantity"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${arrivaldList }" varStatus="status">
								<tr data-unitper="${dis.unitper }"
									<c:if test="${dis.narrnum != dis.ninsnum}">style="color:red;"</c:if>>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td><span title="${dis.vcode }" style="width:65px;">${dis.vcode }</span></td>
									<td><span title="${dis.vname }" style="width:100px;">${dis.vname }</span></td>
									<td><span title="${dis.sp_desc }" style="width:80px;">${dis.sp_desc }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.npurnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum - dis.ninsnum}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum - dis.ninsnum}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.pk_unit }" style="width:40px;">${dis.pk_unit }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.narrnum*dis.disunitper}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.narrnum*dis.disunitper}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.ninsnum*dis.disunitper}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${dis.ninsnum*dis.disunitper}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${(dis.narrnum - dis.ninsnum)*dis.disunitper}" type="currency" pattern="0.00"/>" style="width:60px;text-align:right"><fmt:formatNumber value="${(dis.narrnum - dis.ninsnum)*dis.disunitper }" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.disunit }" style="width:40px;">${dis.disunit }</span></td>
<%-- 									<td><span style="width:40px;text-align:right;" title="<fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/></span></td> --%>
<%-- 									<td><span style="width:60px;text-align:right" title="<fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/></span></td> --%>
									<td><span title="${dis.vmemo }" style="width:100px;">${dis.vmemo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
			});
			if($('#istate').val() == 1){//已修改
				loadToolBar([true,true,false,true]);
			}else if($('#istate').val() == 2){//已确认
				loadToolBar([true,false,true,true]);
			}else if($('#istate').val() == 0){
				alert('提示：此单据门店尚未进行编辑，不能进行确认或者反确认！');
				loadToolBar([true,false,false,true]);
			}else if($('#istate').val() == 3){
				alert('提示：此单据已经验收冻结，不能进行任何操作！');
				loadToolBar([true,false,false,true]);
			}
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select"/>',
					useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '确认',
					title: '确认',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[1],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-20px','0px']
					},
					handler: function(){
						//首先判断现在的状态是否能修改
						if(!checkState()){
							alert('总部已审核，不能继续确认！');
							return;
						}
						confirmArrivalm(2);
					}
				},{
					text: '反确认',
					title: '反确认',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[2],
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-80px','-0px']
					},
					handler: function(){
						//首先判断现在的状态是否能修改
						if(!checkState()){
							alert('总部已审核，不能继续反确认！');
							return;
						}
						confirmArrivalm(1);
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						if(parent.bhysEditState=="edit"){
							if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>?')){
								parent.pageReload();
								parent.$('.close').click();
							}
						}else{
							parent.pageReload();
							parent.$('.close').click();
						}
					}
				}]
			});
		}
		
		function checkState(){
			var bool = true;
			$.ajax({
				url:"<%=path%>/deliverPlat/checkState.do?pk_arrival="+$('#pk_arrival').val(),
				type:"post",
				async: false,
				success:function(data){
					if(data == 1 || data == 2){
						bool = true;
					}else{
						bool = false;
					}
				}
			});
			return bool;
		}
		
		function confirmArrivalm(flag){
			if($('#istate').val() == 1 && flag == 1){
				alert('此单据尚未确认！不能反确认！');
				return;
			}
			if($('#istate').val() == 2 && flag == 2){
				alert('此单据已确认！不能再确认！');
				return;
			}
			if(confirm(flag == 2 ? '是否确认此单据？':'是否反确认此单据？')){
				$('#wait').show();
				$('#wait2').show();
				$.ajax({
					url:"<%=path%>/deliverPlat/updateArrivald.do?istate="+flag+"&pk_arrival="+$('#pk_arrival').val(),
					type:"post",
					success:function(data){
						$('#wait').hide();
						$('#wait2').hide();
						var rs = eval('('+data+')');
						if(rs.pr=="succ"){
							alert('<fmt:message key="operation_successful"/>！');
						}else{
							alert('<fmt:message key="operation_failed"/>！');
						}
						$('#listForm').submit();
					}
				});
			}
		}
		
		//打印单据
		function printDis(){
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/deliverPlat/printReceipt.do';	
			var action1="<%=path%>/deliverPlat/tableCheck.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		
		function pageReload(){
			$('#listForm').submit();
		}
		</script>
	</body>
</html>