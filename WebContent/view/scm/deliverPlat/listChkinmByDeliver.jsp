<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应商结算</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 200px;}
		.search{
			margin-top:-2px;
			cursor: pointer;
		}
		.form-line .form-label{
			width:80px;
		}
		#littleWin{
			width:100%;
			height:160px;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/deliverPlat/listChkinmByDeliver.do" method="post">
			<input type="hidden" id="deliverDes" name="deliver.des" value="${Chkinm.deliver.des}"/>
			<input type="hidden" id="deliverCode" name="deliver.code" value="${Chkinm.deliver.code}"/>
			<div class="bj_head">
				<div class="form-line">	
					<div class="form-label"><fmt:message key="startdate"/>：</div>
					<div class="form-input"><input type="text" id="maded" name="maded" value="<fmt:formatDate value="${Chkinm.maded}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'madedEnd\')}'});"/></div>		
					
					<div class="form-label"><fmt:message key="positions"/>：</div>
					<div class="form-input">
						<input type="text"  id="firmDes"  name="positn.des" readonly="readonly" value="${Chkinm.positn.des}"/>
						<input type="hidden" id="firmCode" name="positn.code" value="${Chkinm.positn.code}"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="document_types"/>：</div>
					<div class="form-input">
						<select id="typ" name="typ" class="select">
							<option value=""></option>
							<option value="9900" <c:if test="${Chkinm.typ eq '9900'}"> selected="selected" </c:if>><fmt:message key="normal_storage"/></option>
							<option value="9916" <c:if test="${Chkinm.typ eq '9916'}"> selected="selected" </c:if>><fmt:message key="normal_shipments"/></option>
<%-- 							<option value="9917" <c:if test="${Chkinm.typ eq '9917'}"> selected="selected" </c:if>><fmt:message key="gifts_shipments"/></option> --%>
<%-- 							<option value="9918" <c:if test="${Chkinm.typ eq '9918'}"> selected="selected" </c:if>><fmt:message key="consignment_shipments"/></option> --%>
							<option value="9906" <c:if test="${Chkinm.typ eq '9906'}"> selected="selected" </c:if>><fmt:message key="storage"/><fmt:message key="returns"/></option>
							<option value="9907" <c:if test="${Chkinm.typ eq '9907'}"> selected="selected" </c:if>><fmt:message key="storage"/><fmt:message key="reversal"/></option>
							<option value="9919" <c:if test="${Chkinm.typ eq '9919'}"> selected="selected" </c:if>><fmt:message key="straight_hair"/><fmt:message key="returns"/></option>
							<option value="9920" <c:if test="${Chkinm.typ eq '9920'}"> selected="selected" </c:if>><fmt:message key="straight_hair"/><fmt:message key="reversal"/></option>
							<option value="9923" <c:if test="${Chkinm.typ eq '9923'}"> selected="selected" </c:if>><fmt:message key="open_at_the_beginning_of_the_period"/></option>
						</select>
					</div>
					<div class="form-label"><fmt:message key="clearing"/>：</div>
					<div class="form-input">
					     <input type="radio" name="folio" value="" <c:if test="${Chkinm.folio eq null}"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="folio" value="0" <c:if test="${Chkinm.folio eq 0}"> checked="checked" </c:if>/><fmt:message key="outstanding"/>
					     <input type="radio" name="folio" value="1" <c:if test="${Chkinm.folio eq 1}"> checked="checked" </c:if>/><fmt:message key="closed"/>
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="enddate"/>：</div>
					<div class="form-input"><input type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'maded\')}'});"/></div>
					<div class="form-label"><fmt:message key="document_number"/>：</div>
					<div class="form-input"><input type="text" id="vouno" name="vouno" class="text" value="<c:out value="${chkinm.vouno}" />"/></div>	
					<div class="form-label" style="width:40px;"></div>
					<div class="form-input">
					     <input type="radio" name="inout" value="" <c:if test="${Chkinm.inout eq null or Chkinm.inout eq '' }"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="inout" value="in" <c:if test="${Chkinm.inout eq 'in'}"> checked="checked" </c:if>/><fmt:message key="only_storage"/>
					     <input type="radio" name="inout" value="inout" <c:if test="${Chkinm.inout eq 'inout'}"> checked="checked" </c:if>/><fmt:message key="only_straight_hair"/>
					</div>
					
					<div class="form-label" style="width:120px;"><fmt:message key ="payments" />：</div>
					<div class="form-input">
					     <input type="radio" name="bill" value="" <c:if test="${Chkinm.bill eq null}"> checked="checked" </c:if>/><fmt:message key="all"/>
					     <input type="radio" name="bill" value="0" <c:if test="${Chkinm.bill eq '0'}"> checked="checked" </c:if>/><fmt:message key="Not_paying"/>
					     <input type="radio" name="bill" value="1" <c:if test="${Chkinm.bill eq '1'}"> checked="checked" </c:if>/><fmt:message key="Payment_has_been"/>
					</div>
					
					<div class="form-label" style="width:20px"></div>
					<div class="form-input" style="width:80px;">
					     <input type="checkbox" name="pay" value="-1.00" <c:if test="${Chkinm.pay eq -1.00}"> checked="checked" </c:if>/><fmt:message key ="partial_payment" />
					</div>
				</div>
			</div>
		    <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width: 25px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="Storage_date"/></span></td>
								<td rowspan="2"><span style="width:80px;"><fmt:message key="positions"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="warehousing_single_number"/></span></td>
								<td rowspan="2"><span style="width:120px;"><fmt:message key="warehousing_certificate_number"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="document_types"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="orders_maker"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="orders_audit"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="storage_amount"/></span></td>
								<td colspan="5"><fmt:message key="clearing"/></td>
							</tr>
							<tr>
								<td ><span style="width:80px;"><fmt:message key="Payment_has_been"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="Not_paying"/></span></td>
								<td style="display: none;"><span style="width:80px;"><fmt:message key="This_payment"/></span></td>
								<td ><span style="width:80px;"><fmt:message key="Check_now"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="chkinm" items="${listChkinm }" varStatus="status">
								<tr>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
									</td>
									<td><span style="width:25px;text-align:center;"><input type="checkbox" name="idList" id="chk_${chkinm.chkinno}" value="${chkinm.chkinno}"/></span></td>
									<td><span title="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${chkinm.positn.des}" style="width:80px;">${chkinm.positn.des}&nbsp;</span></td>
									<td><span title="${chkinm.chkinno}" style="width:50px;">${chkinm.chkinno}&nbsp;</span></td>
									<td><span title="${chkinm.vouno}" style="width:120px;">${chkinm.vouno}&nbsp;</span></td>
									<td><span title="${chkinm.typ}" style="width:60px;">${chkinm.typ}&nbsp;</span></td>
									<td><span title="${chkinm.madeby}" style="width:60px;">${chkinm.madeby}&nbsp;</span></td>
									<td><span title="${chkinm.checby}" style="width:60px;">${chkinm.checby}&nbsp;</span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.totalamt }" type="currency" pattern="0.00"/>"><span style="width:100px;"><fmt:formatNumber value="${chkinm.totalamt }" type="currency" pattern="0.00"/></span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${chkinm.pay}" type="currency" pattern="0.00"/></span></td>
									<td style="text-align: right;" title="<fmt:formatNumber value="${chkinm.totalamt-chkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="${chkinm.totalamt-chkinm.pay}" type="currency" pattern="0.00"/></span></td>
									<td style="text-align: right;display: none;" title="<fmt:formatNumber value="0" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="0" type="currency" pattern="0.00"/></span></td>
									<td>
										<span style="width:80px;text-align:center" title="${chkinm.folio}">
											<c:choose>
												<c:when test="${chkinm.folio==1 }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="table-foot" style="border-top: 1px solid #999;">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<tr>
								<td style="border: 0px;" class="num" >
									<span style="width: 25px;"></span>
								</td>
								<td style="border: 0px;"><span style="width:25px;text-align:center;"></span></td>
								<td style="border: 0px;"><span style="width:60px;"><fmt:message key="total"/></span></td>
								<td style="border: 0px;"><span style="width:120px;"></span></td>
								<td style="border: 0px;"><span style="width:60px;"></span></td>
								<td style="border: 0px;"><span style="width:80px;"></span></td>
								<td style="border: 0px;"><span style="width:70px;"></span></td>
								<td style="border: 0px;"><span style="width:60px;"></span></td>
								<td style="border: 0px;"><span style="width:60px;"></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.totalamt }" type="currency" pattern="0.00"/>"><span style="width:100px;"><fmt:formatNumber value="${totalChkinm.totalamt }" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;"><fmt:formatNumber value="${totalChkinm.pay}" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;text-align: right;" title="<fmt:formatNumber value="${totalChkinm.totalamt-totalChkinm.pay}" type="currency" pattern="0.00"/>"><span style="width:80px;text-align:right"><fmt:formatNumber value="${totalChkinm.totalamt-totalChkinm.pay}" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;display: none;"><span style="width:80px;text-align:right"><fmt:formatNumber value="0" type="currency" pattern="0.00"/></span></td>
								<td style="border: 0px;"><span style="width:80px;text-align:center"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<div id="littleWin">
			<iframe style="width:49%;margin-top:20px;border-right:1px solid #999999;" id="payFrame" src="" frameborder="0" ></iframe>
			<iframe style="width:50%;margin-top:20px;" id="billFrame" src="" frameborder="0" ></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var deliverCode = $('#deliverCode').val();
		if(deliverCode == ''){
			alert('<fmt:message key="The_current_account_is_not_set_the_corresponding_suppliers"/>！<fmt:message key="This_node_is_not_available"/>！');
			invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
		}
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('#gysjs_quit').click();
		 		}
			}); 
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
            	$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
            });
          	//双击事件
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				toCheckedChkinm();
			});
			setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="select"/>',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								if(!$("#deliverCode").val()){
									alert('<fmt:message key="the_supplier_is_required"/>！');
								}else{
									$('#listForm').submit();									
								}
							}
						},{
							text: '<fmt:message key="view"/>',
							title: '<fmt:message key="view"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								toCheckedChkinm();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
                            id:'gysjs_quit',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
				
				});
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool','#littleWin'],$(document.body),130);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head','.table-foot'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('#seachPositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $("#firmCode").val();
						var defaultName = $("#firmDes").val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/findPositnSuper.do?typn=1-5&mold=oneTmany&defaultCode='+defaultCode),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
					}
				});
				
				//加载付款和发票数据
				loadFrameData();
		});
		//加载付款记录和发票数据
		function loadFrameData(){
			var str = $('#listForm').serialize();
			var payAction = "<%=path%>/chkinm/findPayData.do?"+str;
			var billAction = "<%=path%>/chkinm/findBillData.do?"+str;
			$("#payFrame").attr("src",payAction);
			$("#billFrame").attr("src",billAction);
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		
		var detailWin;
		//跳转到详细信息页面
		function toCheckedChkinm(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 1){
				var chkValue = checkboxList.filter(':checked').val();
				var tag = checkboxList.filter(':checked').parents('tr').find("td:eq(5) span").text();
				if($.trim(tag) == ''){
					return;
				}
				if (tag.indexOf("RK")<0){
					detailWin = $('body').window({
						id: 'checkedChkinmDetail',
						title: '<fmt:message key ="select" /><fmt:message key ="checked" /><fmt:message key ="Inout_order_detail" />',
						content: '<iframe id="checkedChkinmDetailFrame" frameborder="0" src="<%=path%>/chkinmZb/update.do?chkinno='+chkValue+'&isCheck=isCheck&inout=zf"></iframe>',
						width: $(document.body).width()-20/* '1050px' */,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}else {
					detailWin = $('body').window({
						id: 'checkedChkinmDetail',
						title: '<fmt:message key ="select" /><fmt:message key ="checked" /><fmt:message key ="In_order_detail" />',
						content: '<iframe id="checkedChkinmDetailFrame" frameborder="0" src="<%=path%>/chkinm/update.do?chkinno='+chkValue+'&isCheck=isCheck"></iframe>',
						width: $(document.body).width()-20/* '1050px' */,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		</script>
	</body>
</html>