<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css"> 
	 	.page{
			margin-bottom: 25px;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/firmModTime/add.do" method="post">
			<input type="hidden" id="parentId" name="parentId" value=""/>
			<input type="hidden" id="parentName" name="parentName" value=""/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:120px;"><fmt:message key="name" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="codeDes" items="${listCodeDes}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${codeDes.code}' />" value="<c:out value='${codeDes.code}' />"/></span>
									</td>

									<td><span style="width:70px;" title="${codeDes.code}"><c:out value="${codeDes.code}" />&nbsp;</span></td>
									<td><span style="width:120px;" title="${codeDes.des}"><c:out value="${codeDes.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="enter"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								/* if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==0) {
									alert("请选择分店仓位(可多选)！");
									return;
								} */
								var positn = '';
								var positnName = '';
								var first=true;
								$('.grid').find('.table-body').find(':checkbox').filter(':checked').each(function(){
									if (!first) {
										positn = positn+','+$(this).val();
										positnName=positnName+','+$(this).parent().parent().parent().find('td:eq(3)').find('span').attr('title');
									}else {
										positn = positn+$(this).val();
										positnName=positnName+$(this).parent().parent().parent().find('td:eq(3)').find('span').attr('title');
										first=false;
									}
								});
								//add wangjie 2014年10月31日 10:58:58
								var single = $("#single").val();
								if(single==1 && single!=null){
									var pos = new Array();
									pos = positn.split(",");
									if(pos.length>2){
										alert("<fmt:message key ="Please_select_a_store" />");
									}else{
										$('#parentId').val(positn);
										$('#parentName').val(positnName);
									 	top.customWindow.afterCloseHandler('Y');
									    top.closeCustom();
									}
								}else{
									$('#parentId').val(positn);
									$('#parentName').val(positnName);
								 	top.customWindow.afterCloseHandler('Y');
								    top.closeCustom();
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//点击checkbox改变
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", false);
				     }else{
				    	 $(this).attr("checked", true);
				     }
				 });
				
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行  
				// wangjie 2014年10月30日 14:28:28
				
				var single = $("#single").val();
				$('.grid').find('.table-body').find('tr').bind("click", function () {
					if($(this).find(':checkbox')[0].checked){
						if(single=='1'){//单选
							$(":checkbox").attr("checked", false);
						}else{
							$(this).find(':checkbox').attr("checked", false);
						}
					}else{
						if(single=='1'){//单选
							$(":checkbox").attr("checked", false);
							$(this).find(':checkbox').attr("checked", true);
						}else{
							$(this).find(':checkbox').attr("checked", true);
						}
					}
				});
				
				var m=$("#code").val();
				$("#code").val("").focus().val(m);
				var n=$("#des").val();
				$("#des").val("").focus().val(n);
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				var defaultCode = '${defaultCode}';
				var codes = defaultCode.split(',');
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						for(var i in codes){
							if(this.id.substr(4,this.id.length)==codes[i]){
								$(this).attr("checked", true);
							}
						}
					})	
				}
				
			});
		</script>
	</body>
</html>