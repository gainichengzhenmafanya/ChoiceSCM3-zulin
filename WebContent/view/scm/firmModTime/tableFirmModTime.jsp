<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>成本卡方案</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/firmModTime/table.do" id="tableForm" name="tableForm" method="post">
		<input type="hidden" name="firm" id="firm" value="${firmModTime.firm}"></input>
		<input type="hidden"  id="modCode"  name="modCode" value=""/>
		<input type="hidden" id="modName" name="modName" value=""/>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td><span style="width:80px;"><fmt:message key ="Cost_card_type" /></span></td>
								<td><span style="width:80px;"><fmt:message key="Start_month" /></span></td>
								<td><span style="width:80px;"><fmt:message key="End_of_month" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="firmModTime" items="${listFirmModTime}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" value="${firmModTime.id}"/>
									</td>
									<td><span style="width:80px;" title="${firmModTime.modCode}"><c:out value="${firmModTime.modCode}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmModTime.bmonth}"><c:out value="${firmModTime.bmonth}" />&nbsp;</span></td>
									<td><span style="width:80px;" title="${firmModTime.emonth}"><c:out value="${firmModTime.emonth}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="insert" />',
							title: '<fmt:message key ="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								findMod();
							}
						},{
							text: '<fmt:message key ="update" />',
							title: '<fmt:message key ="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								updateFirmModTime();
							}
						},{
							text: '<fmt:message key="delete"/>',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteFirmDeliver();
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});


				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				function findMod(){
					if(!!!top.customWindow){
						var offset = getOffset('firm');
						top.cust('<fmt:message key="Cost_card_scheme_selection" />',encodeURI('<%=path%>/firmModTime/add.do'),offset,$('#modName'),$('#modCode'),'750','500','isNull',saveFirmModTime);
					}
				}
				
				function saveFirmModTime(positn){
					var modCode = $("#modCode").val();
					var firm = $("#firm").val();
					var action = '<%=path%>/firmModTime/saveByAdd.do?firm='+firm+'&modCode='+modCode;
					$('body').window({
						title: '<fmt:message key ="save_cost_card_information" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
					window.setTimeout("pageReload()",1500);
				}	
				
				function updateFirmModTime(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var chkValue = checkboxList.filter(':checked').val();
						$('body').window({
							title: '<fmt:message key="update" /><fmt:message key="Cost_card_scheme" />',
							content: '<iframe id="modFrame" frameborder="0" src="<%=path%>/firmModTime/update.do?id='+chkValue+'"></iframe>',
							width: '550px',
							height: '450px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save" /><fmt:message key="branches_and_positions_information" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('modFrame') && getFrame('modFrame').validate._submitValidate()){
												submitFrameForm('modFrame','modForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else if(checkboxList 
							&& checkboxList.filter(':checked').size()>1){
						alert('<fmt:message key="please_select_data" />!');
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						return ;
					}
					
				}
				
				function deleteFirmDeliver(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var codeValue=[];
							checkboxList.filter(':checked').each(function(){
								codeValue.push($(this).val());
							});
							var action = '<%=path%>/firmModTime/delete.do?ids='+codeValue.join(",");
							$('body').window({
								title: '<fmt:message key="Delete_supplier" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
							window.setTimeout("pageReload()",1500);
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
			
			//左侧列表点击事件
// 			function textSubmit(firm){
<%-- 				$('#tableForm').attr('action','<%=path%>/firmModTime/table.do'); --%>
// 				$('#firm').val(firm);
// 				$('#tableForm').submit();
// 			}
			
			function pageReload(){
				$('#tableForm').submit();
			}
		</script>
	</body>
</html>