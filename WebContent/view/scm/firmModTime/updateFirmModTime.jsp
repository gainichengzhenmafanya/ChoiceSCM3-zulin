<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
			<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:40px;">
			<form id="modForm" method="post" action="<%=path %>/firmModTime/saveByUpdate.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key="Start_month" />：</div>
					<div class="form-input">
						<input type="hidden" id="id" readonly="readonly" name="id" class="text" value="<c:out value="${firmModTime.id}"/>"/>
						<input type="text" id="bmonth" name="bmonth" class="text" value="<c:out value="${firmModTime.bmonth}"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="End_of_month" />：</div>
					<div class="form-input">
						<input type="text" id="emonth" name="emonth" class="text" value="<c:out value="${firmModTime.emonth}"/>"/>
					</div>
				</div>
			</form>
			</div>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'emonth',
						validateType:['handler','intege'],
						handler:function(){
							var result = true;
							if(Number($('#emonth').val())>12||Number($('#emonth').val())<1){
								result = false;
							}
							return result;
						},
						param:['F','F'],
						error:['<fmt:message key="Failed_in_March" />！','<fmt:message key="Not_the_right_month" />']
					},{
						type:'text',
						validateObj:'bmonth',
						validateType:['handler','intege'],
						handler:function(){
							var result = true;
							if(Number($('#bmonth').val())>12||Number($('#bmonth').val())<1){
								result = false;
							}
							return result;
						},
						param:['F','F'],
						error:['<fmt:message key="Failed_in_March" />！','<fmt:message key="Not_the_right_month" />']
					}]
				});
			});
		</script>
	</body>
</html>