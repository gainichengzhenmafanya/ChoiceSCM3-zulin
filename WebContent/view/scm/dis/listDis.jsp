<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_cargo_distribution"/>--报货分拨</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
		<style type="text/css">
			#grid{
				position: static;
				z-index: 1;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				text-align: center;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			.page{
				margin-bottom:25px;
			}
			.table-head td span{
				white-space: normal;
 			} 
/* 			.form-line .form-label{ 
				width: 12%; 
			} */
			.form-line .form-input{
 				width: 10%; 
 			}
			.form-line .form-input input[type=text]{ 
 				width: 97%; 
			}
			.form-line .form-input select{
 				width: 100%; 
 			}
 			.form-line {
				position: static;
				margin: 0px auto;
				+z-index:1;
				height: 25px;
				width:100%;	
			}
			
			.form-line .form-label,
			.form-line .form-input,
			.form-line .form-input-merge {
				position: static;
				float: left;
				vertical-align: middle;
				margin-right: 1px;
			}
		</style>	
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="ZHGLTSQX" value="${ZHGLTSQX }"/><!-- 判断是否要有特殊权限  :修改直配方向物资价格 wjf-->
		<input type="hidden" id="ynzp" value="${accountDis.ynzp }"/>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="accountName" name="accountName" value="${accountName }"/>
		<input type="hidden" id="selectIndSta" name="selectIndSta" value="NO"/>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<input type="hidden" id="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" />
		<form id="listForm" action="<%=path%>/disFenBo/list.do" method="post">
		<input type="hidden" name="orderBy" id="orderBy" value="${dis.orderBy }" />
		<div class="bj_head">
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;"><fmt:message key ="date" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;：</div>
				<div class="form-input">
					<span><input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/></span>
		            <span><input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked"</c:if> id="ind1" name="ind1" value="ind"/></span>
		            <span><font style="color:blue;"><fmt:message key="arrival_date1"/></font></span>
				</div>
				<div class="form-label"><fmt:message key ="positions" />：</div>
				<div class="form-input">
					<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
					<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="supply_units"/>：</div>
				<div class="form-input">
					<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label"><fmt:message key="reported_goods_stores"/>：</div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${dis.sp_code }" />
					<img id="seachSupply" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label" style="width:200px;padding-left:50px;margin-top:0px;vertical-align:middle;text-align: right;">
					<font size="3">[</font><input type="radio" name="yndo" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key ="scm_have_done" />
					<input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key ="scm_un_done" /><font size="3">]</font>
				</div>
				<div class="form-label" style="width:200px;padding-left:10px;margin-top:0px;vertical-align:middle;text-align: right;font-weight: bold;">您有    <span id="changeNum" style="color: red;">0</span>    条数据未保存,共有 <span id="changeNum" style="color: green;">${totalCount}</span>条数据</div>
				<div class="form-input" style="margin-left: 50px;margin-top:8px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" >
					<div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div>
				</div><div id="per" class="form-input" style="width:50px;">0</div>
			</div>
			</div>
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key ="supplies" />：</td>
							<td>
								<input type="hidden" style="margin-top:0px;vertical-align:middle;" id="spCode" name="spCode" value="" />
								<input type="text" style="margin-top:0px;vertical-align:middle;" id="spName" name="spName" value="" />
								<img id="seachSupply1" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
							</td>
							<td class="c-left"><fmt:message key ="quantity" />：</td>
							<td><input type="text" id="num" name="num" onkeyup="validate(this);" class="text"/></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key ="select" />'/>
				</div>
			</div>
			<div id="moreSelect" style="z-index:99;display:none;">
				<div class="search-condition">
					<div class="form-line">	
						<div class="form-label" style="width:216px;">
							<input type="checkbox" <c:if test="${chectim1=='checked'}"> checked="checked"</c:if> id="chectim1" name="chectim1" value="checked" onclick="checktim_btn(this)"/>
				            <font style="color:blue;"><fmt:message key="submitted_greater_than"/></font>
				           	<input type="text" style="width:86px;margin-bottom:10px;" id="chectim" name="chectim" class="Wdate text" value="${dis.chectim}" onfocus="new WdatePicker({dateFmt:'HH:mm:ss'})"/>
						</div>
						<div class="form-label"><fmt:message key="category_selection"/>：</div>
						<div class="form-input">
							<input type="text" id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
							<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
							<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
					    </div>
						<div class="form-label"><fmt:message key ="psarea" />：</div>
						<div class="form-input">
							<select class="select" name="psarea" style="margin-top:1px;vertical-align:middle;">
								<option value=""></option>
								<c:forEach var="codeDes" items="${codeDesList }">
									<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key ="scm_psLine" />：</div>
						<div class="form-input">
							<select class="select" name="lineCode" style="margin-top:1px;vertical-align:middle;">
								<option value=""></option>
								<c:forEach var="linesFirm" items="${linesFirmList }">
									<option <c:if test="${dis.lineCode==linesFirm.code }"> selected="selected" </c:if> value="${linesFirm.code }">${linesFirm.des }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width: 6.1%;"><fmt:message key="abbreviation_code"/>：</div>
						<div class="form-input">
							<input type="text" value="${dis.sp_init}" id="sp_init" name="sp_init" style="text-transform:uppercase;" onkeyup="ajaxSearch()" onfocus="this.select()" />
						</div>
						<div class="form-label" style="margin-left:0px;">
							&nbsp;&nbsp;
							<font size="3">[</font><input type="radio" name="inout" value="" checked="checked"/><fmt:message key="all"/>
						    <input type="radio" <c:if test="${dis.inout=='出库' }"> checked="checked"</c:if> name="inout" value="出库"/><fmt:message key="only_the_library"/>
						    <input type="radio" <c:if test="${dis.inout=='直发' }"> checked="checked"</c:if> name="inout" value="直发"/><fmt:message key="only_straight_hair"/>
						    <input type="radio" <c:if test="${dis.inout=='入库' }"> checked="checked"</c:if> name="inout" value="入库"/><fmt:message key="only_storage"/>
						    <input type="radio" <c:if test="${dis.inout=='直配' }"> checked="checked"</c:if> name="inout" value="直配"/><fmt:message key="only_straight_with"/><font size="3">]</font>
						    <font size="3">[</font><input type="radio" name="ex1" value="" checked="checked"/><fmt:message key="all"/>
							<input type="radio" <c:if test="${dis.ex1=='Y' }"> checked="checked"</c:if>  name="ex1" value="Y"/><fmt:message key ="whether_semi" />
							<input type="radio" <c:if test="${dis.ex1=='N' }"> checked="checked"</c:if>  name="ex1" value="N"/>非半成品<font size="3">]</font>	
						    <font size="3">[</font><input type="radio" name="ynArrival" value="" checked="checked"/><fmt:message key="all"/>
							<input type="radio" <c:if test="${dis.ynArrival=='1' }"> checked="checked"</c:if> name="ynArrival" value="1"/><fmt:message key ="scm_arrival" />
							<input type="radio" <c:if test="${dis.ynArrival=='2' }"> checked="checked"</c:if> name="ynArrival" value="2"/><fmt:message key="unknown_arrival"/><font size="3">]</font>								
						</div>
					</div>
				</div>
			</div>
			<div id="grid" class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;text-align: center;">&nbsp;</span></td>								
								<td rowspan="2"><span style="width:60px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
 								<td rowspan="2"><span style="width:55px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="suppliers"/></span></td>
 								<td rowspan="2"><span style="width:120px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2"><span style="width:120px;"><fmt:message key="requisitioning_company"/></span></td>
 								<td rowspan="2"><span style="width:25px;"><fmt:message key="unit"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="the_number_of_the_reported_goods"/></span></td>
 								<td rowspan="2"><span style="width:40px;"><font color="blue"><fmt:message key="the_number_of_procurement"/></font></span></td>
 								<td rowspan="2"><span style="width:30px;">报价<fmt:message key="unit_price"/></span></td>
 								<td rowspan="2"><span style="width:30px;">售价<fmt:message key="unit_price"/></span></td>
 								<td rowspan="2"><span style="width:25px;"><fmt:message key="direction"/></span></td>
 								<td rowspan="2"><span style="width:70px;"><fmt:message key="arrival_date"/></span></td>
 								<td rowspan="2"><span style="width:40px;"><fmt:message key="inventory"/></span></td> 								
 								<td rowspan="2"><span style="width:25px;"><fmt:message key="which_procurement_indicating"/></span></td>
 								<td rowspan="2"><span style="width:40px;"><fmt:message key="orderer"/></span></td>
 								<td rowspan="2"><span style="width:120px;"><fmt:message key="confirm_the_time"/></span></td>
 								<td rowspan="2"><span style="width:25px;"><fmt:message key="the_unknown_arrives"/></span></td>
 								<td rowspan="2"><span style="width:25px;"><fmt:message key="purchase_audit"/></span></td>
<%--  								<td rowspan="2"><span style="width:15px;"><fmt:message key="print1"/></span></td> --%>
 								<td rowspan="2"><span style="width:35px;"><fmt:message key="remark"/></span></td>
 								<td colspan="2"><span style="width:50px;"><fmt:message key="reference"/></span></td>							
							</tr>
							<tr>
 								<td><span style="width:45px;"><fmt:message key="the_number_of_purchase"/></span></td>
 								<td><span style="width:25px;"></span><fmt:message key="unit"/></td>
							</tr>							
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="dis" items="${disesList1}" varStatus="status">
								<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.id }" style="width:60px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:70px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:55px;">${dis.sp_desc }</span></td>
									<td class="textInput">
										<span style="width:40px;">
											<input title="${dis.deliverCode }" type="text" id="c_delv_${dis.id}" value="${dis.deliverCode}" onfocus="this.select()" onblur="findByDeliver(this,'${dis.id}')" onkeydown="searchDeliver(this,'${dis.id}')"/>
										</span>
									</td>
									<td><span title="${dis.deliverDes}" id="d_del_${dis.id }" style="width:120px;">${dis.deliverDes }</span></td>
									<td><span title="${dis.firmDes}" style="width:120px;"><input type="hidden" value="${dis.firmCode }"/>${dis.firmDes }</span></td>
									<td>
<%-- 										<c:choose> --%>
<%-- 											<c:when test="${dis.unitper == 0 }"> --%>
<%-- 												<span title="${dis.unit1 }" style="width:25px;">${dis.unit1 }</span> --%>
<%-- 											</c:when> --%>
<%-- 											<c:otherwise> --%>
												<span title="${dis.unit }" style="width:25px;">${dis.unit }</span>
<%-- 											</c:otherwise> --%>
<%-- 										</c:choose> --%>
									</td>	
									<td>
										<c:choose>
											<c:when test="${dis.unitper == 0 }">
												<span title="${dis.amount1}" style="width:40px;text-align: right;"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span>
											</c:when>
											<c:otherwise>
												<span title="${dis.amount}" style="width:40px;text-align: right;"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span>
											</c:otherwise>
										</c:choose>
									</td>
									<td class="textInput">
										<c:choose>
											<c:when test="${dis.unitper == 0 }">
												<span style="width:40px;">
													<input class="nextclass"  title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" type="text" style="width: 40px;text-align: right;" value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1in(this,'${dis.id}')" onkeyup="validate(this)"/>
												</span>
											</c:when>
											<c:otherwise>
												<span style="width:40px;">
													<input class="nextclass"  title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" type="text" style="width: 40px;text-align: right;" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}','${dis.unitper }')" onkeyup="validate(this)"/>
												</span>
											</c:otherwise>
										</c:choose>
										
									</td>
									<td class="textInput">
										<span style="width:30px;">
											<input class="nextclass" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width: 30px;text-align: right;" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onchange="checkPrice(this,'12')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td class="textInput">
										<span style="width:30px;">
											<input class="nextclass" title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" type="text" style="width: 30px;text-align: right;" value="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricesale(this,'${dis.id}')" onkeyup="validate(this)" onchange="checkPrice(this,'13')"  onkeyup="validate(this)"/>
										</span>
									</td>
									<td><span title="${dis.inout}" style="width:25px;">${dis.inout}</span></td>
									<td class="textInput">
										<span style="width:70px;">
											<input title="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" type="text" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>"  onfocus="this.select()" onblur="saveInd(this,'${dis.id}')" onkeypress="selectInd(this)"/>
										</span>
									</td>
									<td><span title="${dis.cnt}" style="width:40px;text-align: right;">${dis.cnt}</span></td>
									<td>
										<span style="width:25px;text-align:center;">
											<input type="hidden" id="qr_${dis.id}" value="${dis.chk1 }"/>
											<img src="<%=path%>/image/kucun/${dis.chk1}.png"/>
										</span>
									</td>
									<td><span title="${dis.chk1emp }" style="width:40px;">${dis.chk1emp }</span></td>
									<td><span title="${dis.chk1tim }" style="width:120px;">${dis.chk1tim }</span></td>
									<td>
										<span style="width:25px;text-align:center;">
										<img src="<%=path%>/image/kucun/${dis.chksend}.png"/>
										</span>
									</td>
									<td>
										<span style="width:25px;text-align:center;" title="${dis.sta}"><!-- 用来判断是否采购审核 2014.12.5wjf -->
										<img src="<%=path%>/image/kucun/${dis.sta}.png"/>
										</span>
									</td>
<%-- 									<td><span title="${dis.prncnt }" style="width:15px;">${dis.prncnt }</span></td> --%>
									<td><span title="${dis.memo}" style="width:35px;">${dis.memo}</span></td>
									<td><span title="${dis.amount1 }" style="width:45px;text-align: right;">${dis.amount1 }</span></td>
									<td><span title="${dis.unit1 }" style="width:25px;">${dis.unit1 }</span></td>																		
								</tr>
							</c:forEach>	
						</tbody>
					</table>
				</div>	
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var isCanUse = true;//是否采购确认 那些按钮可用 2014.12.5wjf
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		$(document).ready(function(){
			//遍历tr，判断如果是直配方向售价显示报价
			$('.grid').find('.table-body').find('tr').each(function(i){
				var fx = $(this).find('td:eq(14)').find('span').attr('title');
				if(fx == '直配'){
					$(this).find('td:eq(13)').find('input').val($(this).find('td:eq(12)').find('input').val());
				}
			});
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('#chectim').attr('disabled','disabled');
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.keyCode==32){return false;}//屏蔽空格
		 		if(e.altKey == false) return;
	 			switch (e.keyCode){
			                case 70: $('#autoId-button-101').click(); break;
			                case 69: $('#autoId-button-102').click(); break;
			                case 83: updateDis(); break;
			                case 65: $('#autoId-button-104').click(); break;
			                case 68: printDis("printInspection.do"); break;
			                case 67: $('#autoId-button-106').click(); break;
			                case 88: changeFocus();break;//Alt+X
		            	}
			});
			
			//编辑采购数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhfbEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
						return;
			 		}
					var valOld = $(this).val();			 		//ZHGLTSQX == 1 时，直配方向物资价格修改加权限wjf
					if(parseFloat(valOld) !=0 && $('#ZHGLTSQX').val() == '1' && $('#ynzp').val() != 'Y' && $(this).parent().parent().prevAll().length == 12){
						$(this).blur();
						alert('您没有修改直配方向物资价格的权限！');
						$(this).val(valOld);
						return;
					}
				}
			});
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			$('#seachPositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#firmCode").val();
					var defaultName = $("#firmDes").val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/searchAllPositn.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'750','500','isNull');
				}
			});
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positnCode').val();
					var defaultName = $('#positnDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachSupply1').bind('click.custom',function(e){
				if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
					if(!!!top.customWindow){
						var defaultCode = $('.grid').find('.table-body').find(':checkbox').filter(':checked').parents('tr').find('td:eq(3)').find('span').attr('title');
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_materials"/>',encodeURI('<%=path%>/supply/findAllSupplySXS.do?defaultCode='+defaultCode),offset,$('#spName'),$('#spCode'),'560','420','isNull');
					}
				}else {
					$('.search-div').hide();
					alert("请选择需要调整物料的一条数据！");
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('positnDes');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
			if('${action}'!='init' && '${currState}'!='1'){
				addTr('first');	
			}else if('${action}'!='init' && '${currState==1}'){
				$("#per").text('100%');
				$("#currState").width(200);
			}
			pageSize = '${pageSize}';
			var ndivHeight = $(".table-body").height();
			$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		          });
			
			if('${action}'=='init'){
				parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.trlist=undefined;//将trlist清空
				parent.changeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#changeNum").text(parent.changeNum);
			}
			
			//判断如果不是编辑状态
			if(parent.bhfbEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				loadToolBar([true,true,false,true,true,true]);
			}else{
				loadToolBar([true,false,true,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',false);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
			
			$('.grid').find('.table-head').find('td:eq(2)').each(function(){
				$(this).bind('click',function(){//按序号排序
					if($('#orderBy').val()=="c.id desc"){
						$('#orderBy').val("c.id asc");
					}else{
						$('#orderBy').val("c.id desc");
					}
					$('#listForm').submit();
				});
			});
			if($('#chectim1').get(0).checked){
				$("#chectim").removeAttr("disabled","disabled");
			}
			if($('#maded').val()==''){
				$('#maded').val($('#ind').val());
			}
			
			//如果选的是已处理或全部   则很多按钮不能用 2014.10.21wjf
			var val = $('input[name=yndo]:checked').val();
			if(val == "NO"){
				isCanUse = true;
				loadToolBar([true,true,false,true,true,true]);
			}else{
				isCanUse = false;
				loadToolBar([true,false,false,true,true,true]);
			}
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null!=$('#maded').val() && ""!=$('#maded').val()){
								//如果編輯状态下 点查询要有提示  2014.12.11wjf
								if(parent.bhfbEditState=='edit'){
									if(confirm('您正处于编辑状态下，可能有未保存的数据，是否继续？')){
										parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
										$("#listForm").submit();
									}
								}else{
									parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
									$("#listForm").submit();
								}
 							}else{
 								alert('日期不能为空！');
 							}
						},
						items:[{
							text: '高级查询',
							title: '高级查询',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								$('#moreSelect').slideToggle(100);
							}
						}]
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="edit"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if($('.grid').find('.table-body').find("tr").size()<1){
								alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							}else{
								parent.bhfbEditState='edit';
								loadToolBar([true,false,true,false,false,false]);
								//遍历tr，判断如果是非采购确认，采购审核的才可以修改2014.12.5wjf
								$('.grid').find('.table-body').find('tr').each(function(i){
									var qr = $(this).find('td:eq(17)').find('span').find('input').val();//采购确认
									var sh = $(this).find('td:eq(21)').find('span').attr('title');//采购审核
									if(qr != 'Y' && sh != 'Y'){//如果不是采购确认和采购审核的才能修改
										$(this).find('input[type="text"]').attr('disabled',false);
									}
								});
								//判断是否是洞庭的需求：ZHGLTSQX = 1时，价格不可以修改，只有直配方向物资并且有权限的才可以修改wjf
								if($('#ZHGLTSQX').val()=='1'){
									//遍历tr，判断如果是直配方向的才可以修改
									$('.grid').find('.table-body').find('tr').each(function(i){
										var fx = $(this).find('td:eq(14)').find('span').attr('title');
										if(fx != '直配'){
											$(this).find('td:eq(12)').find('input').attr('disabled',true);
											$(this).find('td:eq(13)').find('input').attr('disabled',true);
										}
									});
								}
								new tabTableInput("table-body","text"); //input  上下左右移动
								if($('tbody tr:first .nextclass').length!=0){
									$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
								}
							}
						},
						items:[{
							text: '拆分',
							title: '拆分',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								dispatchDis();
							}
						},{
							text: '调整物资',
							title: '调整物资',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
									$('.search-div').slideToggle(100);
								}else {
									alert("请选择需要调整物料的一条数据！");
								}
							}
						}]
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateDis();
						}
					},{
						text: '采购处理',
						title: '采购处理',
						useable: isCanUse,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						items:[{
							text: '采购确认',
							title: '采购确认',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('chk1','Y');
							}
						},{
							text: '取消确认',
							title: '取消确认',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('chk1','N');
							}
						},{
							text: '全部采购确认',
							title: '全部采购确认',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('confirm');
							}
						},{
							text: '全部取消确认',
							title: '全部取消确认',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelConfirm');
							}
						},{
							text: '采购审核',
							title: '采购审核',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('sta','Y');
							}
						},{
							text: '取消审核',
							title: '取消审核',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('sta','N');
							}
						},{
							text: '全部采购审核',
							title: '全部采购审核',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('buyCheck');
							}
						},{
							text: '全部取消审核',
							title: '全部取消审核',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelBuyCheck');
							}
						},{
							text: '未知到货',
							title: '未知到货',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('chksend','Y');
							}
						},{
							text: '已知到货',
							title: '已知到货',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('chksend','N');
							}
						},{
							text: '全部未知到货',
							title: '全部未知到货',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('unKnownArrival');
							}
						},{
							text: '全部已知到货',
							title: '全部已知到货',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif', 
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelUnKnownArrival');
							}
						},{
							text: '部门明细',
							title: '部门明细',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								deptDetail();
							}
						}]
					},{
						text: '打印/导出',
						title: '打印/导出',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						items:[{
							text: 'Excel',
							title: 'Excel',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-20px']
							},
							handler: function(){
//	 							fucEnter('excel','');
								exportExcel();
							}
						},{
							text: '导出分拨单',
							title: '导出分拨单',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('数据为空，不能打印！！');
								}else{
									excelDis();
								}
							}
						},{
							text: '<fmt:message key="print_distribution_single"/>(<u>A</u>)',
							title: '<fmt:message key="print_distribution_single"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('数据为空，不能打印！！');
								}else{
									printDis("printIndent.do");	
								}
							}
						},{
							text: '<fmt:message key="print_inspection_single"/>(<u>D</u>)',
							title: '<fmt:message key="print_inspection_single"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('数据为空，不能打印！！');
								}else{
									printDis("printInspection.do");
								}
							}
						},{
							text: '<fmt:message key="print_delivery_note"/>(<u>C</u>)',
							title: '<fmt:message key="print_delivery_note"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('数据为空，不能打印！！');
								}else{
									printDis("printDelivery.do");
								}
							}
						},{
							text: '采购汇总',
							title: '采购汇总',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								cgtotal();
							}
						}]
					},{
						text: '发送邮件',
						title: '发送邮件',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					    icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					    },
						handler: function(){ 
							var deliverCode = $('#deliverCode').val();
							if(null != deliverCode && "" != deliverCode){
								var data = {};
								$("#listForm *[name]").each(function(){
									var name = $(this).attr("name"); 
									if(name)data[name] = $(this).val();
									if(name == 'ind1'){
										if($(this).is(':checked') || $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'ex1'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'yndo'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'inout'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'ynArrival'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}
								});
								var url = "<%=path%>/disFenBo/sendEmail.do";
								$.ajax({
									url: url,
									data: data,
									type: 'post',
									success:function(msg){
										if(msg == 'OK'){
											alert("发送成功。");
										}else if(msg == 'NSH'){
											alert('发送失败！当前日期下此供应商的物资都未审核！请先审核。');
										}else{
											alert('发送失败！');
										}
									}
								});
							}else{
								alert("<fmt:message key ="please_select_suppliers" />");
							}
						}
					},<%-- ,{
						text: '导出采购订单',
						title: '导出采购订单',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					    icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					    },
						handler: function(){
							if(null!=$('#deliverCode').val() && ""!=$('#deliverCode').val()){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('action','<%=path%>/disFenBo/exportCaiGou.do');
								$('#listForm').submit();
								$("#wait2 span").html("数据导出中，请稍后...");
								$('#listForm').attr('action','<%=path%>/disFenBo/list.do');	
								$("#wait2").val('');//等待加载还原
							}else{
								alert("请选择供应商");
							}
						}
					}, --%>{
						text: '<fmt:message key="quit"/>',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){							
							if(parent.bhfbEditState=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}]
			});
		}
		
		$("#search").bind('click', function() {
			$('.search-div').hide();
			dispatchX();
		});
		
		//虚拟报货时候选择另一种实际物料
		function dispatchX(){
			
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				if(confirm('确认添加申购该物资？')){
					if($('#num').val()==null||$('#num').val()==''){
						alert("请填写调整数量！");
						$('.search-div').slideToggle(100);
					}else{
						checkboxList.filter(':checked').each(function(i){
							selected['id'] =  $(this).val();
							selected['supply.sp_code'] = $('#spCode').val();
							selected['amountin'] = Number($('#num').val());
// 							selected['amount'] = Number($('#num').val());
							selected['amount'] = $(this).parents('tr').find('td:eq(10)').find('span').attr('title');
							selected['memo'] = $(this).parents('tr').find('td:eq(22)').find('span').attr('title');
							selected['hoped'] = $(this).parents('tr').find('td:eq(15)').find('span').find('input').val();
						});
						$.post('<%=path%>/disFenBo/updateDispatchX.do',selected,function(data){
							showMessage({//弹出提示信息
								type: 'success',
								msg: '<fmt:message key="operation_successful" />！',
								speed: 1000
								});	
							});
					}
				}
			}else{
				alert('请选择单条需要调整的数据！');
				return ;
			}
		}
		
		//拆分
		function dispatchDis(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('确认对选中的数据进行拆分吗？')){
					var flag = true;//用来判断是否已经采购确认或采购审核 2014.12.5wjf
					checkboxList.filter(':checked').each(function(i){
						var qr = $(this).find('td:eq(17)').find('span').find('input').val();//采购确认
						var sh = $(this).find('td:eq(21)').find('span').attr('title');//采购审核
						if(qr == 'Y' | sh == 'Y'){
							flag = false;
							return;
						}
						selected['chkstodList['+i+'].id'] =  $(this).val();
						selected['chkstodList['+i+'].supply.sp_code'] = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
						selected['chkstodList['+i+'].amountin'] = parseFloat($(this).parents('tr').find('td:eq(11)').find('span').find('input').val());
						selected['chkstodList['+i+'].amount'] = parseFloat($(this).parents('tr').find('td:eq(10)').find('span').attr('title'));
						selected['chkstodList['+i+'].supply.sp_price'] = $(this).parents('tr').find('td:eq(12)').find('span').attr('title');
						selected['chkstodList['+i+'].memo'] = $(this).parents('tr').find('td:eq(22)').find('span').attr('title');
						selected['chkstodList['+i+'].hoped'] = $(this).parents('tr').find('td:eq(15)').find('span').find('input').val();
					});
					if(!flag){
						alert('已经采购确认采购审核的物资不能进行拆分！');
						return;
					}
					$.post('<%=path%>/disFenBo/updateDispatch.do',selected,function(data){
						showMessage({//弹出提示信息
							type: 'success',
							msg: '<fmt:message key="operation_successful" />！',
							speed: 1000
							});	
						});
				}
			}else{
				alert('请选择需要拆分的数据！');
				return ;
			}
		}
		//采购汇总
		function cgtotal(){
// 			if (confirm("要对所选数据进行采购汇总操作吗？")==true) {
				if(null!=$('#maded').val() && ""!=$('#maded').val()){
					$('body').window({
						id: 'window_cgtotal',
						title: '采购汇总',
						content: '<iframe id="cgtotalFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findCaiGouTotal.do?maded="+$('#maded').val()+'"></iframe>',
						width: '1000px',
						height: '480px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
										text: '<fmt:message key="print" />',
										title: '<fmt:message key="print"/>',
										handler: function()
										{
											window.frames["cgtotalFrame"].printDis();
										}
									},{
										text: '<fmt:message key="quit"/>(ESC)',
										title: '<fmt:message key="quit"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}]
								}
						});
				}else{
					alert('请选择汇总日期！');
				}
// 			}
		}
		//配送汇总
		function pstotal(){
			if(null!=$('#maded').val() && ""!=$('#maded').val()){
				$('body').window({
					id: 'window_pstotal',
					title: '配送汇总',
					content: '<iframe id="pstotalFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findPeiSongTotal.do?maded="+$('#maded').val()+'"></iframe>',
					width: '1000px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
									text: '<fmt:message key="print" />',
									title: '<fmt:message key="print"/>',
									handler: function()
									{
										window.frames["pstotalFrame"].printDis();
									}
								},{
									text: '<fmt:message key="quit"/>(ESC)',
									title: '<fmt:message key="quit"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
							}
					});
			}else{
				alert('请选择汇总日期！');
			}
		}
		//部门明细
		function deptDetail(){
			var maded = $('#maded').val();
		    var positnCode = $("#positnCode").val();
			var deliverCode = $('#deliverCode').val();
			var typCode = $("#typCode").val(); 
			var firmCode = $('#firmCode').val();
			var sp_code = $('#sp_code').val();
			var ex1 = $("input[name='ex1']:checked").val();
			var yndo = $("input[name='yndo']:checked").val();
			var sp_init = $("#sp_init").val();
		    var inout = $("input[name='inout']:checked").val();
			var chksend = $("input[name='ynArrival']:checked").val();
			if(null!=$('#maded').val() && ""!=$('#maded').val()){
				if(null!=$('#deliverCode').val() && ""!=$('#deliverCode').val()){
					if(null!=$('#firmCode').val() && ""!=$('#firmCode').val()){
						if($('#firmCode').val().indexOf(',')<=0){
						$('body').window({
							id: 'window_deptDetail',
							title: '部门明细',
							content: '<iframe id="deptDetailFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findDeptDetail.do?maded="+maded+"&positnCode="+positnCode+"&deliverCode="+deliverCode+"&typCode="+typCode+"&firmCode="+firmCode+"&sp_code="+sp_code+"&chksend="+chksend+"&ex1="+ex1+"&yndo="+yndo+"&sp_init="+sp_init+"&inout="+inout+'"></iframe>',
							width: '1000px',
							height: '380px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										  text: 'Excel',
										  title: 'Excel',
										  useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
									      icon: {
										  		url: '<%=path%>/image/Button/op_owner.gif',
										  		position: ['-40px','-20px']
									      		},
											handler: function(){
													$("#wait2").val('NO');//不用等待加载
													$('#listForm').attr('action','<%=path%>/disFenBo/exportDeptDetail.do');
													$('#listForm').submit();
													$("#wait2 span").html("数据导出中，请稍后...");
													$('#listForm').attr('action','<%=path%>/disFenBo/list.do');	
													$("#wait2").val('');//等待加载还原
									 
											}
										},{
// 											text: '<fmt:message key="print" />',
// 											title: '<fmt:message key="print"/>',
// 											handler: function()
// 											{
// 												window.frames["deptDetailFrame"].printDis();
// 											}
// 										},{
											text: '<fmt:message key="quit"/>(ESC)',
											title: '<fmt:message key="quit"/>',
											icon: {
												url: '<%=path%>/image/Button/op_owner.gif',
												position: ['-160px','-100px']
											},
											handler: function(){
												$('.close').click();
											}
										}]
									}
							});
						}else{
							alert('只能选择一家分店');
						}
					}else{
						alert('请选择分店！');
					}
				}else{
					alert('<fmt:message key ="please_select_suppliers" />！');
				}
			}else{
				alert('请选择汇总日期！');
			}
		}
		//配送拆分
		function psChaiFen(){
			alert('1');
		}
		//验货直发
		function yhZhiFa(){
			var curwindow = $('body').window({
				id: 'window_yhZhiFa',
				title: '验货直发',
				content: '<iframe id="yhZhiFaFrame" frameborder="0" src="<%=path%>/disFenBo/findYanHuoZhiFa.do?action=init"></iframe>',
				width: '1000px',
				height: '420px',
				draggable: true,
				isModal: true,
				confirmClose: false
			});
			curwindow.max();
		}
		
		//采购确认  采购审核  未知到货
		function fucEnter(colomn,value){
			
			if(parent.bhfbEditState=='edit'){
				alert('请先保存未保存的数据！');
				return;
			}
			if(colomn=='excel'){
				printDis("exportExcel.do");
				return;
			}
			
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				msg="";
				if (colomn=='chk1'&&value=='Y') {
					msg="要对所选数据进行采购确认操作吗？";
				}
				if (colomn=='chk1'&&value=='N') {
					msg="要对所选数据进行采购取消操作吗？";
				}
				if (colomn=='chksend'&&value=='Y') {
					msg="要对所选数据进行未知到货操作吗？";
				}
				if (colomn=='chksend'&&value=='N') {
					msg="要对所选数据进行取消未知操作吗？";
				}
				if (colomn=='sta'&&value=='Y') {
					msg="要对所选数据进行采购审核操作吗？";
				}
				if (colomn=='sta'&&value=='N') {
					msg="要对所选数据进行取消审核操作吗？";
				}
				if (colomn=='excel') {
					msg="要对所选数据进行导出excel操作吗？";
				}
				
				if (confirm(msg)==true) {
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					selected['id'] = chkValue.join(',');
					//采购确认
					if(colomn=="chk1"){	
						selected['chk1'] = value;
						$.post('<%=path%>/disFenBo/update.do',selected,function(data){
							checkboxList.filter(':checked').each(function(){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(17)').find('span').html('<input type="hidden" id="qr_'+$(this).val()+'" value="Y"/><img src="<%=path%>/image/kucun/Y.png"/>');
									$(this).parents('tr').find('td:eq(18)').find('span').html($("#accountName").val());
									$(this).parents('tr').find('td:eq(19)').find('span').html(getDate());
								}else{
									$(this).parents('tr').find('td:eq(17)').find('span').html('<input type="hidden" id="qr_'+$(this).val()+'" value="N"/><img src="<%=path%>/image/kucun/N.png"/>');
									$(this).parents('tr').find('td:eq(18)').find('span').html($("#accountName").val());
									$(this).parents('tr').find('td:eq(19)').find('span').html(getDate());

								}
							});
						});
					}
					//到货未知
					if(colomn=="chksend"){
						selected['chksend'] = value;
						$.post('<%=path%>/disFenBo/update.do',selected,function(data){
							checkboxList.filter(':checked').each(function(){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(20)').find('span').html('<img src="<%=path%>/image/kucun/Y.png"/>');
								}else{
									$(this).parents('tr').find('td:eq(20)').find('span').html('<img src="<%=path%>/image/kucun/N.png"/>');
								}
							});
						});
					}
					//审核确认
					if(colomn=="sta"){
						selected['sta'] = value;
						checkboxList.filter(':checked').each(function(){
							if($('#qr_'+$(this).val()).val()=='Y'){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(21)').find('span').html('<img src="<%=path%>/image/kucun/Y.png"/>');
									$.post('<%=path%>/disFenBo/update.do',selected);
								}else{
									$(this).parents('tr').find('td:eq(21)').find('span').html('<img src="<%=path%>/image/kucun/N.png"/>');
									$.post('<%=path%>/disFenBo/update.do',selected);
								}
							}else{
								return;
							}
						});
					}
				}
			}else{
				alert('请勾选需要操作的选项！');
				return ;
			}
		}
		//全部 采购确认、未知到货、采购审核
		function fucEnterAll(editType){
			
			if($('.grid').find('.table-body').find("tr").size()<1){
				alert('数据为空无法操作！');
				return;
			}
			if(parent.bhfbEditState=='edit'){
				alert('请先保存未保存的数据！');
				return;
			}
			msg="";
			if (editType=='confirm') {
				msg="要对所选数据进行全部采购确认操作吗？";
			}
			if (editType=='unKnownArrival') {
				msg="要对所选数据进行全部未知到货操作吗？";
			}
			if (editType=='buyCheck') {
				msg="要对所选数据进行全部采购审核操作吗？";
			}
			if (editType=='cancelConfirm') {
				msg="要对所选数据进行全部取消确认操作吗？";
			}
			if (editType=='cancelUnKnownArrival') {
				msg="要对所选数据进行全部取消未知操作吗？";
			}
			if (editType=='cancelBuyCheck') {
				msg="要对所选数据进行全部取消审核操作吗？";
			}
			if (confirm(msg)==true) {
				var action="<%=path%>/disFenBo/updateAll.do";
				if(editType=='confirm'){//采购确认
					action="<%=path%>/disFenBo/updateAll.do?param=chk1-Y";
				}else if(editType=='cancelConfirm'){//取消采购确认
					action="<%=path%>/disFenBo/updateAll.do?param=chk1-N";
				}else if(editType=='unKnownArrival'){//未知到货
					action="<%=path%>/disFenBo/updateAll.do?param=chksend-Y";
				}else if(editType=='cancelUnKnownArrival'){//取消未知到货
					action="<%=path%>/disFenBo/updateAll.do?param=chksend-N";
				}else if(editType=='buyCheck'){//采购审核
					action="<%=path%>/disFenBo/updateAll.do?param=sta-Y";
				}else if(editType=='cancelBuyCheck'){//取消采购审核
					action="<%=path%>/disFenBo/updateAll.do?param=sta-N";
				}
				
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		//修改报货------------		
		function updateDis(){
			$("#sp_init").focus();
			//如果修改过数据就保存 
			if(parent.trlist){
				var predata = parent.trlist;
				var disList = {};
				var num=0;
				for(var i in predata){
					disList['listDis['+num+'].id']=i;
					undefined==predata[i].deliverCode?'':disList['listDis['+num+'].deliverCode']=predata[i].deliverCode;
					undefined==predata[i].amountin?'':disList['listDis['+num+'].amountin']=predata[i].amountin;
					undefined==predata[i].amount1in?'':disList['listDis['+num+'].amount1in']=predata[i].amount1in;
					undefined==predata[i].pricein?'':disList['listDis['+num+'].pricein']=predata[i].pricein;
					undefined==predata[i].pricesale?'':disList['listDis['+num+'].pricesale']=predata[i].pricesale;
					undefined==predata[i].ind?'':disList['listDis['+num+'].ind']=predata[i].ind;
					undefined==predata[i].inout?'':disList['listDis['+num+'].inout']=predata[i].inout;
					num++;
				}
				$.post("<%=path%>/disFenBo/updateDis.do",disList,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
						alert('成功修改'+rs.updateNum+'条数据！');
						var action="<%=path%>/disFenBo/list.do";
						$('#listForm').attr('action',action);
					}else{
						alert('修改失败！\n单据已经入库或出库！');
						var action="<%=path%>/disFenBo/list.do";
						$('#listForm').attr('action',action);
					}
				});	
			}else{
				alert('您没有修改任何数据！');
			}
			parent.bhfbEditState = '';
			parent.changeNum = 0;
			parent.trlist=undefined;
			loadToolBar([true,true,false,true,true,true]);
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#listForm').submit();
		}
		//查找供应商
		function searchDeliver(e,obj){
			if(window.event.keyCode==13){
				var inoutNum;
				var inout=$(e).closest("tr").find('td:eq(14)').find('span').text();
				var firmCode=$(e).closest("tr").find('td:eq(8)').find('span input').val();
				if("入库"==inout){
					return;
				}
				if("出库"==inout){
					inoutNum=2;
				}
				if("直发"==inout){
					inoutNum=0;
				}
				if("直配"==inout){
					inoutNum=1;
				}
				$("#selectDelCodeId").val(obj);
				$('body').window({
					id: 'window_searchDeliver',
					title: '<fmt:message key="query_suppliers"/>',
					content: '<iframe id="searchDeliverFrame" frameborder="0" src="'+"<%=path%>/deliver/searchDeliver.do?action=init&firm="+firmCode+"&inout="+inoutNum+'"></iframe>',
					width: '800px',
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />(<u>F</u>)',
								title: '<fmt:message key="query_suppliers"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('searchDeliverFrame')){
										submitFrameForm('searchDeliverFrame','SearchForm');
									}
								}
							},{
								text: '<fmt:message key="quit"/>',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});								
			}else{
				return;
			}
		}
		//输入日期回车
		function selectInd(obj){
			if(window.event.keyCode==13){
				var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig); 
				if(null!=date && ""!=date){
					if($("#selectIndSta").val()=="YES"){
						$("#selectIndSta").val("NO");
						new WdatePicker();
						$(obj).focus();
					}else{
						$("#selectIndSta").val("YES");
						new WdatePicker();
						$(obj).focus();
					}
				}else{
					if(null!=obj.defaultValue && ""!=obj.defaultValue){
						obj.value=obj.defaultValue;
					}else{
						obj.value=getInd();
					}
					showMessage({
								type: 'error',
								msg: '日期格式不正确！',
								speed: 1000
								});
					$(obj).focus();
					return;
				}
			}
		}
		//鼠标单击选择日期，屏蔽光标方向移动
		function selectInd2(){
			if($("#selectIndSta").val()=="YES"){
				$("#selectIndSta").val("NO");
				new WdatePicker();
			}else{
				$("#selectIndSta").val("YES");
				new WdatePicker();
			}
		}
		//日期验证
		function validateDate(obj){
			var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
			if(null==date || ""==date){
				if(null!=obj.defaultValue && ""!=obj.defaultValue){
					obj.value=obj.defaultValue;
				}else{
					obj.value=getInd();
				}
				showMessage({
							type: 'error',
							msg: '日期格式不正确！',
							speed: 1000
							});
				$(obj).focus();
				return;
			}
		}
		
		//导出excel分拨单
		function excelDis(){ 
			$("#wait2").val("NO");
// 			$('#listForm').attr('target','report');
// 			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disFenBo/excelDis.do';	
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
// 			$('#listForm').attr('target','');
			$("#wait2").val("");
		}	
		
		//打印单据
		function printDis(e){ 
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disFenBo/'+e;	
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}	
		
		//让时间选中框可不可选
		function checktim_btn(obj){
			if(obj.checked){
				var today=new Date();
				var hh=today.getHours();
				if(hh<10)
					hh="0"+hh;
				var mm=today.getMinutes();
				if(mm<10)
					mm="0"+mm;
				var ss=today.getSeconds();
				if(ss<10)
					ss="0"+ss;
				var curTime=hh+":"+mm+":"+ss;
				$("#chectim").val(curTime);
				$("#chectim").removeAttr("disabled","disabled");
			}else{
				$("#chectim").val("");
				$("#chectim").attr("disabled","disabled");
			}
		}
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			var hh=myDate.getHours();
			var mm=myDate.getMinutes();
			var ss=myDate.getSeconds();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			if(hh<10)
				hh="0"+hh;
			if(mm<10)
				mm="0"+mm;
			if(ss<10)
				ss="0"+ss;
			return fullDate=yy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss;
		}
		//获取系统时间
		function getInd(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '不是有效数字！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		
		//根据code查询名称
		function findByDeliver(e,f){
			if(e.value==e.defaultValue){
				return;
			}
			var deliverDes=$(e).closest("tr").find('td:eq(7) span').text();
			var firmCode=$(e).closest("tr").find('td:eq(8)').find('span input').val();
			var inout=$(e).closest("tr").find('td:eq(14)').find('span').text();
			var selected = {};
			selected['code'] = e.value;
			selected['oldCode'] = e.defaultValue;
			selected['firm']=firmCode;
			if(null!=e.value && ""!=e.value){
				//提交并返回值，判断执行状态
 				$.post("<%=path%>/deliver/findDeliverTyp.do",selected,function(data){
					if(null!=data && ""!=data){
						var rs = eval('('+data+')');
						if("1"==rs.sta){
							alert('基地仓库与供应商之间不能相互更改！');
							e.value=e.defaultValue;
							$(e).focus();
							return;
						}else if("2"==rs.sta){
							alert('直配方向与直发方向之间不能相互更改！');
							e.value=e.defaultValue;
							$(e).focus();
							return;
						}else{
							if("入库"!=inout && "出库"!=inout){
								$("#d_del_"+f).text(rs.deliver.des);
								if("1"==rs.inout){
									$(e).closest("tr").find('td:eq(14)').find('span').text('直配');
								}else if((deliverDes).indexOf('自购')!=-1){
									$(e).closest("tr").find('td:eq(14)').find('span').text('直配');
								}else{
									$(e).closest("tr").find('td:eq(14)').find('span').text('直发');
								}
								//记录改变保存
								if(parent.trlist){
									//已经存在这条数据
									if(parent.trlist[f]){
										parent.trlist[f]['deliverCode']=e.value;
										parent.trlist[f]['inout']=e.value;
									}else{
										parent.trlist[f]={};
										parent.changeNum = parent.changeNum+1;//修改条数+1
										$("#changeNum").text(parent.changeNum);
										parent.trlist[f]['deliverCode']=e.value;
										parent.trlist[f]['inout']=e.value;
									}
								}else{
									parent.trlist = {};
									parent.trlist[f]={};
									parent.changeNum = 1;
									$("#changeNum").text(1);
									parent.trlist[f]['deliverCode']=e.value;
									parent.trlist[f]['inout']=e.value;
								}
							}else{
								e.value=e.defaultValue;
								alert('该方向供应商不允许修改！');
								return;
							}
						}
					}else{
						alert('<fmt:message key="supplier_unfound"/>！');
						e.value=e.defaultValue;
						$(e).focus();
						return;
					}
				});
			}
		}
		//保存采购数量
		function saveAmountin(e,f,u){//参数u：物资标准单位和参考单位之间的转换率；用来计算修改标准数量后参考数量也要变  wjf
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['amountin']=e.value;
						parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['amountin']=e.value;
						parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['amountin']=e.value;
					parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);
				}
			}
		}
		//保存参考采购数量
		function saveAmount1in(e,f){//这里是转换率为0的物资参考数量修改方法 此物资现在默认 参考数量 = 标准数量 所以参考数量修改标准数量也改 wjf
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['amount1in']=e.value;
						parent.trlist[f]['amountin']=e.value;
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['amount1in']=e.value;
						parent.trlist[f]['amountin']=e.value;
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['amount1in']=e.value;
					parent.trlist[f]['amountin']=e.value;
				}
			}
		}
		//保存单价
		function savePricein(e,f){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['pricein']=e.value;
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['pricein']=e.value;
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['pricein']=e.value;
				}
			}
		}
		//保存售价
		function savePricesale(e,f){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['pricesale']=e.value;
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['pricesale']=e.value;
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['pricesale']=e.value;
				}
			}
		}
		//批量修改价格
		function checkPrice(e,num){
			var fx = $(e).closest('tr').find('td:eq(14)').find('span').attr('title');
			var sp_code =$(e).closest('tr').find('td:eq(3)').find('span').text();
			if(fx == '直配'){
				var typ ="报价";
				var priceTyp ='pricein';
				if(num=='13'){
					typ="售价";
					priceTyp ='pricesale';
				} 
				if(confirm('是否将该物资所有直配方向的'+typ+'均改为此价格？')){
		 			$('.grid').find('.table-body').find('tr').each(function(i){
		 				var fx2 = $(this).find('td:eq(14)').find('span').attr('title');
		 				var sp_code_ =$(this).find('td:eq(3)').find('span').text();
		 				if(fx2 == '直配' && sp_code==sp_code_){
		 					var defaultValue = $(this).find('td:eq('+num+')').find('input').val();
		 					var ff = $(this).find('td:eq(2)').find('span').text();
		 					$(this).find('td:eq('+num+')').find('input').val(e.value);//售价
		 					if(defaultValue!=e.value){
		 						if(parent.trlist){
		 							if(parent.trlist[ff]){
		 								parent.trlist[ff][priceTyp]=e.value;
		 							}else{
		 								parent.trlist[ff]={};
		 								parent.changeNum = parent.changeNum+1;
		 								$("#changeNum").text(parent.changeNum);
		 								parent.trlist[ff][priceTyp]=e.value;
		 							}
		 						}else{
		 							parent.trlist = {};
		 							parent.trlist[ff]={};
		 							parent.changeNum = 1;
		 							$("#changeNum").text(1);
		 							parent.trlist[ff][priceTyp]=e.value;
		 						}
		 					}
		 				}
		 			});
		 			$(e).closest('tr').click();
				}
			}
		}
		//保存日期
		function saveInd(e,f){
			var date = e.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
			if(null!=date && ""!=date){
				if(e.defaultValue!=e.value){
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['ind']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['ind']=e.value;
						}
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['ind']=e.value;
					}
				}
			}
		}
		
		//编辑状态下用户按Alt+X的时候 将焦点定位到缩写码
		function changeFocus(){
			$("#sp_init").focus();
		}
		
		//根据缩写码查询
		function ajaxSearch(){
			if(window.event.keyCode==13){
				if(null!=$('#maded').val() && ""!=$('#maded').val()){
					$("#listForm").submit();
				}else{
					alert('日期不能为空！');
				}
			}
		}
		
		var totalCount;
		var condition;
		var currPage;
		function addTr(check){
			if(check=='first'){
				totalCount = '${totalCount}';
				condition = ${disJson};
				currPage= 1;
				condition.maded = new Date(condition.maded.time).format("yyyy-mm-dd");
				condition.bdat = "";
				condition.edat = "";
				condition.ind = "";
				condition.dued = "";
				condition['ind1'] = '${ind1}';
				condition['totalCount'] = totalCount;
				condition['currPage']=1;
				$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
				$("#currState").width(Number('${currState}')*200);
				return;
			}
			$.post("<%=path%>/disFenBo/listAjax.do",condition,function(data){
				var rs = eval('('+data+')');
				//var rs = data;
				$("#per").text((rs.currState*100).toFixed(0)+'%');
				$("#currState").width(""+rs.currState*200+"px");
				//不是最后一页
				var num = rs.currPage*pageSize;
				var disesList1 = rs.disesList1;
				for(var i in disesList1){
					var dis = disesList1[i];
					
					var tr = '<tr ';
					if(dis.amount!=dis.amountin){
						tr = tr+'style="color:red;"';
					}
					tr = tr + '>';
					tr = tr + '<td class="num"><span style="width:25px;">'+ ++num +'</span></td>';
					tr = tr + '<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_'+dis.id+'" value="'+dis.id+'"/></span></td>';
					tr = tr + '<td><span title="'+dis.id+'" style="width:40px;">'+dis.id+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_code+'" style="width:70px;">'+dis.sp_code+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_name+'" style="width:70px;">'+dis.sp_name+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_desc+'" style="width:55px;">'+dis.sp_desc+'</span></td>';
					tr = tr + '<td class="textInput"><span style="width:40px;"><input title="'+dis.deliverCode+'" type="text" id="c_delv_'+dis.id+'" value="'+dis.deliverCode+'" onfocus="this.select()" onkeyup="validate(this);" onblur="findByDeliver(this,'+dis.id+')" onkeydown="searchDeliver(this,'+dis.id+')"/></span></td>';
					tr = tr + '<td><span title="'+dis.deliverDes+'" id="d_del_'+dis.id+'" style="width:120px;">'+dis.deliverDes+'</span></td>';	
					tr = tr + '<td><span title="'+dis.firmDes+'" style="width:120px;"><input type="hidden" value="'+dis.firmCode+'"/>'+dis.firmDes+'</span></td>';
// 					if(dis.unitper == 0){//如果转换率为0的物资,前面放参考数量 wjf
// 						tr = tr + '<td><span title="'+dis.unit1+'" style="width:25px;">'+dis.unit1+'</span></td>';
// 						tr = tr + '<td><span title="'+dis.amount1+'" style="width:40px;text-align:right;">'+((undefined==dis.amount1)?"":dis.amount1.toFixed(2))+'</span></td>';
// 						tr = tr + '<td class="textInput"><span style="width:40px;"><input class="nextclass" title="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" type="text" style="width: 40px;text-align: right;" value="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" onfocus="this.select()" onblur="saveAmount1in(this,'+dis.id+')" onkeyup="validate(this)"/></span></td>';
// 					}else{
						tr = tr + '<td><span title="'+dis.unit+'" style="width:25px;">'+dis.unit+'</span></td>';
						tr = tr + '<td><span title="'+dis.amount+'" style="width:40px;text-align:right;">'+((undefined==dis.amount)?"":dis.amount.toFixed(2))+'</span></td>';
						tr = tr + '<td class="textInput"><span style="width:40px;"><input class="nextclass" title="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" type="text" style="width: 40px;text-align: right;" value="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" onfocus="this.select()" onblur="saveAmountin(this,'+dis.id+','+dis.unitper+')" onkeyup="validate(this)"/></span></td>';
// 					}
					tr = tr + '<td class="textInput"><span style="width:30px;"><input class="nextclass" title="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" type="text" style="width: 30px;text-align: right;" value="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'"" onfocus="this.select()" onblur="savePricein(this,'+dis.id+')" onchange="checkPrice(this,'+12+')" onkeyup="validate(this)"/></span></td>';
					tr = tr + '<td class="textInput"><span style="width:30px;"><input class="nextclass" title="'+((undefined==dis.pricesale)?"":dis.pricesale.toFixed(2))+'" type="text" style="width: 30px;text-align: right;" value="'+((undefined==dis.pricesale)?"":dis.pricesale.toFixed(2))+'"" onfocus="this.select()" onblur="savePricesale(this,'+dis.id+')" onchange="checkPrice(this,'+13+')" onkeyup="validate(this)" /></span></td>';
					tr = tr + '<td><span title="'+dis.inout+'" style="width:25px;">'+dis.inout+'</span></td>';
					tr = tr + '<td class="textInput"><span style="width:70px;"><input title="'+((undefined==dis.ind.time)?"":new Date(dis.ind.time).format("yyyy-mm-dd"))+'"" type="text" class="Wdate text" value="'+((undefined==dis.ind.time)?"":new Date(dis.ind.time).format("yyyy-mm-dd"))+'"  onfocus="this.select()" onblur="saveInd(this,'+dis.id+')" onkeypress="selectInd(this)"/></span></td>';
					tr = tr + '<td><span title="'+dis.cnt+'" style="width:40px;text-align:right;">'+((undefined==dis.cnt)?"":dis.cnt.toFixed(2))+'</span></td>';
					tr = tr + '<td><span style="width:25px;text-align:center;"><input type="hidden" id="qr_'+dis.id+'" value="'+dis.chk1+'"/><img src="<%=path%>/image/kucun/'+dis.chk1+'.png"/></span></td>';
					tr = tr + '<td><span title="'+dis.chk1emp+'" style="width:40px;">'+dis.chk1emp+'</span></td>';
					tr = tr + '<td><span title="'+dis.chk1tim+'" style="width:120px;">'+dis.chk1tim+'</span></td>';
					tr = tr + '<td><span style="width:25px;text-align:center;"><img src="<%=path%>/image/kucun/'+dis.chksend+'.png"/></span></td>';
					tr = tr + '<td><span style="width:25px;text-align:center;" title="'+dis.sta+'"><img src="<%=path%>/image/kucun/'+dis.sta+'.png"/></span></td>';
// 					tr = tr + '<td><span title="'+dis.prncnt+'" style="width:15px;">'+dis.prncnt+'</span></td>';
					tr = tr + '<td><span title="'+dis.memo+'" style="width:35px;">'+dis.memo+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount+'" style="width:45px;text-align:right;">'+((undefined==dis.amount)?"":dis.amount.toFixed(2))+'</span></td>';
					tr = tr + '<td><span title="'+dis.unit+'" style="width:20px;">'+dis.unit+'</span></td>';
					tr = tr + '</tr>';	
					
					$(".grid .table-body tbody").append($(tr));
				}
				if(rs.over!='over'){
					condition['currPage']=++currPage;
					returnInfo = true;
				}
				if(parent.bhfbEditState!='edit'){
					$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				}else{
					new tabTableInput("table-body","text"); //input  上下左右移动	
				}
			});
		}
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		};
		
		//导出excel
		function exportExcel(){
			$("#wait2").val("NO");
			var action = '<%=path%>/disFenBo/exportExcel.do';	
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action1);
			$("#wait2").val("");
		}
		</script>
	</body>
</html>