<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Acceptance_to_library"/>验收出库</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.table-head td span{
				white-space: normal;
			}
			.form-line .form-label{
				width: 9%;
			}
		</style>
	</head>
	<body>	
		<div class="tool"></div>
		<%--验收出库返回的信息 --%>
		<input type="hidden" id="chkMsg" name="chkMsg" value="${chkMsg }"/>
		<form id="listForm" action="<%=path%>/disChuKu/listCheckout.do" method="post">
			<%-- curStatus 0:init;1:edit;2:add;3:show --%>
			<input type="hidden" id="action" name="action" value="<c:out value="${action}" default="init"/>" />
			<div class="bj_head">
				<input type="hidden" id="inout" name="inout" value="${dis.inout }"/>
	<!-- 			<input type="hidden" id="ysckStr" name="ysckStr"/> -->
				     	
				<div class="form-line">
					<div class="form-label"><fmt:message key="reported_goods_stores"/></div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
						<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode }"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
			        <div class="form-label">
	 			    	<fmt:message key="category_selection"/>
			        </div>
			        <div class="form-input">
				        <input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
						<input type="hidden" id="typCode" name="typCode" value="${dis.typCode }"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
			        </div>
			        <div class="form-label"><fmt:message key="psarea"/></div>
					<div class="form-input">
						<select class="select" name="psarea">
							<option value=""></option>
							<c:forEach var="codeDes" items="${codeDesList }">
								<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
							</c:forEach>
						</select>
					</div>
			    </div> 
				<div class="form-line">	
					<div class="form-label">
			            <fmt:message key="library_positions"/>
					</div>
					<div class="form-input" style="width:14.7%;">
						<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
						<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
<%-- 					<div class="form-label"><fmt:message key="document_date"/></div> --%>
					<div class="form-label"><fmt:message key="library"/><fmt:message key="make_orders"/><fmt:message key="date"/></div>
					<div class="form-input">
				    	<input type="text" id="maded1" name="maded1" class="Wdate text" value="<fmt:formatDate value="${maded1}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>	    				
					</div>
					<div class="form-label"><fmt:message key="reported_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${dis.chkstoNo }" /></div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="arrival_date"/></div>
					<div class="form-input">
		                <input type="text" id="ind" name="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
		            	&nbsp;<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
						<font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>            					    
				    </div>
				</div>   
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width: 30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2" title="<fmt:message key="serial_number"/>"><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" title="<fmt:message key="reported_num"/>"><span style="width:65px;"><fmt:message key="reported_num"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_code"/>"><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_name"/>"><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2" title="<fmt:message key="specification"/>"><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" title="<fmt:message key="suppliers"/>"><span style="width:50px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2" title="<fmt:message key="suppliers_name"/>"><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2" title="<fmt:message key="requisitioning_company"/>"><span style="width:120px;"><fmt:message key="requisitioning_company"/></span></td>	
								<td colspan="3" title="<fmt:message key="reports"/>"><fmt:message key="reports"/></td>
								<td colspan="6" title="<fmt:message key="deliver_goods"/>"><fmt:message key="deliver_goods"/></td>
								<td rowspan="2" title="<fmt:message key="direction"/>"><span style="width:30px;"><fmt:message key="direction"/></span></td>								
								<td rowspan="2" title="<fmt:message key="remark"/>"><span style="width:40px;"><fmt:message key="remark"/></span></td>
<%-- 								<td rowspan="2"><span style="width:30px;"><fmt:message key="whether_storage"/></span></td> --%>
<!-- 								<td rowspan="2"><span style="width:30px;">是否<br/>入库</span></td> -->
								<td rowspan="2" title="<fmt:message key="whether_library"/>"><span style="width:30px;"><fmt:message key="whether_library"/></span></td>
								<td rowspan="2" title="<fmt:message key="current_inventory"/>"><span style="width:50px;"><fmt:message key="current_inventory"/></span></td>
								<td rowspan="2" title="<fmt:message key="inventory_sufficient"/>"><span style="width:30px;"><fmt:message key="inventory_sufficient"/></span></td>
								<td rowspan="2" title="<fmt:message key="production_date"/>"><span style="width:70px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2" title="<fmt:message key="additional_items"/>"><span style="width:120px;"><fmt:message key="additional_items"/></span></td>
							</tr>
							<tr>
								<td title="<fmt:message key="procurement_unit_br"/>"><span style="width:30px;"><fmt:message key="procurement_unit_br"/></span></td>
								<td title="<fmt:message key="reports"/><br/><fmt:message key="quantity"/>"><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td title="<fmt:message key="adjustment_amount"/>"><span style="width:50px;"><fmt:message key="adjustment_amount"/></span></td>
								
								<td title="<fmt:message key="standard_unit_br"/>"><span style="width:30px;"><fmt:message key="standard_unit_br"/></span></td>
								<td title="<fmt:message key="quantity"/>"><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td title="<fmt:message key="unit_price"/>"><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td title="<fmt:message key="amount"/>"><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td title="<fmt:message key="the_reference_unit_br"/>"><span style="width:30px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td title="<fmt:message key="quantity"/>"><span style="width:50px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr <c:if test="${dis.amountin>dis.cnt}">style="color:red;"</c:if>>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
									</td>
									<td>
										<span style="width:30px; text-align: center;">
											<input type="checkbox"  name="idList" id="chk_${dis.id}" value="${dis.id}" sp_per1 = "${dis.sp_per1 }"/>
										</span>
									</td>
									<td ><span style="width:45px;" title="${dis.id }">${dis.id }</span></td>
									
									<td ><span style="width:65px;text-align:right;" title="${dis.chkstoNo }" >${dis.chkstoNo }</span></td>
									<td ><span style="width:65px;" title="${dis.sp_code }" >${dis.sp_code }</span></td>
									<td ><span style="width:100px;" title="${dis.sp_name }" >${dis.sp_name }</span></td>
									<td ><span style="width:50px;" title="${dis.sp_desc }" >${dis.sp_desc }</span></td>
									<td><span style="width:50px;" title="${dis.deliverCode }" >${dis.deliverCode }</span></td>
									<td><span style="width:100px;" title="${dis.deliverDes }" >${dis.deliverDes }</span></td>
									<td ><span style="width:120px;" title="${dis.firmDes }" >${dis.firmDes }</span></td>
									<td title="${dis.unit3 }"><span style="width:30px;">${dis.unit3 }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','tzshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','tzshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="tzshuliang" value="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>"/></td>
									<td title="${dis.unit }"><span style="width:30px;">${dis.unit }</span></td>
									<td title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','bzshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','bzshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="bzshuliang" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"/></td>
									<td title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>"><span style="width:40px;text-align:right"><fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin*dis.pricesale}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amountin*dis.pricesale}" pattern="##.##" minFractionDigits="2" /></span></td>
									<td title="${dis.unit1 }"><span style="width:30px;">${dis.unit1 }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','ckshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','ckshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="ckshuliang" value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>"/></td>
									<td><span style="width:30px;" title="${dis.inout }" >
										<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
										<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
										<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
										<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>
									</span></td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:40px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:40px;" title="${dis.memo}">${dis.memo}</span></td>
										</c:otherwise>
									</c:choose>
<!-- 									<td> -->
<!-- 										<span style="width:30px;text-align:center;"> -->
<%-- 											<img src="<%=path%>/image/kucun/${dis.chkin}.png"/> --%>
<!-- 										</span> -->
<!-- 									</td> -->
									<td>
										<span style="width:30px;text-align:center;">
											<img src="<%=path%>/image/kucun/${dis.chkout}.png"/>
										</span>
									</td>
									<td><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.cnt}" type="currency" pattern="0.00"/></span></td>
									<td>
										<span style="width:30px;text-align:center;">
											<c:choose>
												<c:when test="${dis.amountin<=dis.cnt}">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
									<td><span style="width:70px;" title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td>
										<c:if test="${fn:contains(dis.memo, '##')}">
											<c:set var="fjxstr" value="${fn:split(dis.memo, '##')}"></c:set>
										</c:if>
										<select style="width:130px;" disabled class="fjxselect">
											<option value=""></option>
											<c:forEach items="${fuJiaXiangList}" var="item">
												<option <c:if test="${(empty fjxstr[1]?fjxstr[0]:fjxstr[1]) eq item.des}">selected</c:if> value="${item.des}">${item.des}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">	
		
		//焦点离开检查输入是否合法
		function validatee(e,line,type,per,per3){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			
			//值
			var value = e.value;
			
			//采购数量，标准数量，参考数量
			if('tzshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value);
				if(per3!='0'&&per3!=undefined&&$.trim(per3)!=''&&!isNaN(per3)){
					$('.table-body tr:eq('+line+') td:eq(14) input').val((value/per3).toFixed(2));
					$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value/per3);
					if(per!=0&&per!=undefined&&$.trim(per)!=''&&!isNaN(per)){
						$('.table-body tr:eq('+line+') td:eq(18) input').val((value/per3*per).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value/per3*per);
					}
				}
			}else if('bzshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value);
				if(!isNaN(per3)){
					if(per3!='0'){
						$('.table-body tr:eq('+line+') td:eq(12) input').val((value*per3).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value*per3);
					}
				}
				if(!isNaN(per)){
					if(per!='0'){
						$('.table-body tr:eq('+line+') td:eq(18) input').val((value*per).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value*per);
					}
				}
			}else if('ckshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value);
				if(per!='0'&&per!=undefined&&$.trim(per)!=''&&!isNaN(per)){
					$('.table-body tr:eq('+line+') td:eq(14) input').val((value/per).toFixed(2));
					$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value/per);
					if(per3!=0&&per3!=undefined&&$.trim(per3)!=''&&!isNaN(per3)){
						$('.table-body tr:eq('+line+') td:eq(12) input').val((value/per*per3).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value/per*per3);
					}
				}
			}

			//金额修改
			var price = $('.table-body tr:eq('+line+') td:eq(15) span').html();
			var amt = $('.table-body tr:eq('+line+') td:eq(14) input').val();
			var jine = price * amt;
			$('.table-body tr:eq('+line+') td:eq(16) span').html(jine.toFixed(2));
			$('.table-body tr:eq('+line+') td:eq(16)').attr('title',jine);
			
			//判断库存是否充足
			//得到库存
			var kcnum = $('.table-body tr:eq('+line+') td:eq(22) span').html();
			
			//得到标准数量
			var bznum = $('.table-body tr:eq('+line+') td:eq(14) input').val();
			
			//判断是否充足
			if(Number(bznum)>Number(kcnum)){
				//不充足
				$('.table-body tr:eq('+line+') td:eq(23) span img').attr('src','<%=path%>/image/themes/icons/no.png');
				//改变字体颜色
				$('.table-body tr:eq('+line+')').css('color','red');
			}else{
				//充足
				$('.table-body tr:eq('+line+') td:eq(23) span img').attr('src','<%=path%>/image/themes/icons/ok.png');
				//改变字体颜色
				$('.table-body tr:eq('+line+')').css('color','black');
			}
			
		}
		
		//验证对象
		var validate = new Validate({
			validateItem:[
			{
				type:'text',
				validateObj:'chkstoNo',
				validateType:['num1'],
				param:['T'],
				error:['<fmt:message key="reported_num" /><fmt:message key="please_fill_in_figures" />！']
			}]
		});
		
		//提交前验证
		function validate_beforesm(){
			return validate._submitValidate();
		}
		
		$(document).ready(function(){				
		    chkMsg();
			//按钮快捷键
			focus();//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 67: $('#autoId-button-102').click(); break;
	                case 65: $('#autoId-button-103').click(); break;
	                case 68: $('#autoId-button-104').click(); break;
	                case 69: $('#autoId-button-105').click(); break;
	                case 80: $('#autoId-button-106').click(); break;
	            }
			}); 
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		 //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			//判断按钮的显示与隐藏
			var status = $("#action").val();
			if(status == 'init'){
				loadToolBar([true,true,false,false,true,false]);
			}else if(status == 'zfcx'){//库存查询
				loadToolBar([true,true,false,true,true,false]);
			}else if(status == 'ckcx'){//出库查询
				loadToolBar([true,true,false,false,true,true]);
			}
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),135);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:true,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="reported_goods_stores"/>'
				});
			});
			
			$("#seachOnePositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positnCode").val(),
					single:true,
					tagName:'positnDes',
					tagId:'positnCode',
					title:'<fmt:message key="library_positions"/>'
				});
			});
			
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positnCode').val();
					var defaultName = $('#positnDes').val();
					var offset = getOffset('ind');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					var defaultName = $('#typDes').val();
					var offset = getOffset('ind');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
		});
		
		//按钮控制  点直发查询后，库存出库按钮灰化，点出库查询时，直发出库按钮灰化  wjf
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="library"/><fmt:message key="select"/>',
							title: '<fmt:message key="library"/><fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')} && use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								var maded1 = $("#maded1").val();
								if(maded1 == ''){
									alert("<fmt:message key ="Please_select_a_single_date" />！");
									return;
								}
								//验证报货单号
								if(validate_beforesm()){
									chkoutFormSubmit2();
								}
							}
						},{
							text: '<fmt:message key="edit"/>',
							title: '<fmt:message key="edit"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')} && use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								//判断有无数据
								var size = $(".table-body tr").size();
								if(size==0){
									alert('<fmt:message key="no_data"/>！！！');
									return;
								}
								//可编辑状态
								$('.bzshuliang').removeAttr("disabled");
								$('.tzshuliang').removeAttr("disabled");
								$('.ckshuliang').removeAttr("disabled");
								$('.fjxselect').removeAttr("disabled");
								//加载按钮
								loadToolBar([true,false,true,true,true,false]);
							}
						},{
							text: '<fmt:message key="save"/>',
							title: '<fmt:message key="save"/>',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								//组装集合
								var list = new Array();
								$(".table-body tr").each(function(i){
									//i 0开始
									var id = $(this).find("td:eq(2) span").html();
									//var amountin = $(this).find("td:eq(14) input").val();
									//var amount1in = $(this).find("td:eq(18) input").val();
									//var amount1sto = $(this).find("td:eq(12) input").val();
									var amountin = $(this).find("td:eq(14)").attr('title'); //标准数量
									var amount1in = $(this).find("td:eq(18)").attr('title'); //参考数量
									var amount1sto = $(this).find("td:eq(12)").attr('title'); //采购数量
									var fjx = $(this).find("td:eq(25) select").val();  //附加项
									//对象
									list[i] = new Object();
									list[i]['id']=id;
									//标准数量
									list[i]['amountin']=amountin;
									//参考数量
									list[i]['amount1in']=amount1in;
									//采购数量
									list[i]['amount1sto']=amount1sto;
									//附加项
									list[i]['memo']=fjx;
								});
								//转换为json字符串
								var jsonStr = JSON.stringify(list);
								var data = "listStr="+jsonStr;
								//提交保存数量，判断执行状态
								$.ajax({
									url:"<%=path%>/disChuKu/updateChkoutNum.do",
									type:"post",
									data:data,
									success:function(msg){
										switch(msg){
										case '1':
											alert('<fmt:message key="update_fail" />');
											$('#listForm').submit();
											break;
										case '0':
											alert('<fmt:message key ="update_successful" />！');
											$('.tzshuliang').attr("disabled",true);
											$('.bzshuliang').attr("disabled",true);
											$('.ckshuliang').attr("disabled",true);
											$('.fjxselect').attr("disabled",true);
										}
									}
								});
								//按钮显示控制
								loadToolBar([true,true,false,true,true,true]);
							}
						},{
							text: '<fmt:message key="in_stock_new"/>',
							title: '<fmt:message key="in_stock_new"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')} && use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_library"/>？')){
									loadToolBar([true,true,false,false,true,false]);
									ChkstomKcck();
								}
							}
						},{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printYsckChkstom();	
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]

			});
		}
		//生成出库单的状态
		function chkMsg(){
			var chkMsg=$("#chkMsg").val();
			if(null!=chkMsg && ""!=chkMsg){
				if('0' == chkMsg)
					alert('<fmt:message key="empty_data_cannot_be_carried_out_from_the_library"/>');//数据为空不能进行出库
				else if('1' == chkMsg)
					alert('<fmt:message key="The_success_of_the_library_please_go_to_the_library_to_check_out_the_corresponding_documents"/>');//出库完毕，请到出库单查询查看打印相应单据！
				else
					alert(chkMsg);
				var action = '<%=path%>/disChuKu/listCheckout.do';
				$('#listForm').attr('action',action);
// 				$('#listForm').submit();
			}
		}
		//直发出库
		function ChkstomZfck(){
// 			$('#ysckStr').val('zfck');
			$('#inout').val('<fmt:message key="straight_hair"/>');
// 			$('#listForm').submit();
			var action="<%=path%>/disChuKu/saveYsckChkout.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//库存出库
		function ChkstomKcck(){
// 			$('#ysckStr').val('kcck');
			$('#inout').val('out');
// 			$('#listForm').submit();
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			var isNull=0;
			var codes = [];
			var sp_code = "";
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					isNull=1;
					var amount = Number($(this).parents('tr').find('td:eq(14)').attr('title'));
					var amount1 = Number($(this).parents('tr').find('td:eq(18)').attr('title'));//参考数量
					if(amount==0 || amount==0.0 ||amount==0.00 || amount1==0 || amount1==0.0 || amount1==0.00){
						isNull=2;
						sp_code = $(this).parents('tr').find('td:eq(5) span').attr('title');
						return false;
					}
					codes.push($(this).val());
				});
			}
			if(isNull==0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			if(isNull==2){
// 				alert('物资:['+sp_code+']标准数量或参考数量不能为0！请返回报货分拨进行修改！');
				alert('<fmt:message key="supplies"/>:['+sp_code+']<fmt:message key="scm_standard_quantity"/><fmt:message key="or"/><fmt:message key="scm_the_reference_number"/><fmt:message key="can_not"/>0！<fmt:message key="please_return_the_report_distribution_of_goods"/><fmt:message key="update"/>！');
				return;
			}
			var action="<%=path%>/disChuKu/saveYsckChkout.do?inCondition="+codes.join(',');
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//直发查询
		function chkoutFormSubmit1(){
			$('#inout').val('<fmt:message key="straight_hair"/>');
			$('#action').val('zfcx');
			var action = '<%=path%>/disChuKu/listCheckout.do';
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//出库查询
		function chkoutFormSubmit2(){
			$('#inout').val('out');
			$('#action').val('ckcx');
			var action = '<%=path%>/disChuKu/listCheckout.do';			
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//打印
		function printYsckChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disChuKu/viewYsckChkstom.do';	
			var action1="<%=path%>/disChuKu/listCheckout.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val('');//等待加载还原
     	}
		</script>
	</body>
</html>