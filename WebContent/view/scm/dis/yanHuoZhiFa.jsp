<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_cargo_distribution"/>验货直发</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
			<style type="text/css">
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				text-align: center;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			.page{
				margin-bottom:25px;
			}
			</style>	
	</head>
	<body>
		<div class="tool"></div>
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<form id="listForm" action="<%=path%>/disFenBo/findYanHuoZhiFa.do" method="post">
			<div class="form-line">	
				<div class="form-label"><fmt:message key="arrival_date"/></div>
				<div class="form-input" style="width:190px;">
					<input type="text" style="width:82px;" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
					<font style="color:blue;"><fmt:message key="to"/></font>
					<input type="text" style="width:82px;" id="edat" name="edat" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
				</div>
				<div class="form-label">
					<!-- 三合一库，验货直发，未知日期到货，勾选查询无记忆 wangjie 2014年12月12日 13:53:21 -->
					<input type="checkbox" <c:if test="${dis.ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
					<font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>				
				</div>
				<div class="form-label"><fmt:message key="document_date"/></div>
				<div class="form-input">
					 <input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
				</div>				
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="supply_units"/> </div>
				<div class="form-input">
					<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />            
				</div>
				<div class="form-label"><fmt:message key="category_selection"/></div>
				<div class="form-input">
					<input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
					<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				</div>
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key="position_select"/> </div>
				<div class="form-input">
					<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
					<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="note_type"/></div>
				<div class="form-input">
				     <select class="select" id="memo1" name="memo1">
		        		 <option value=""></option>
		                 <option
		                 <c:if test="${dis.memo1=='不合格' }">
		                 selected="selected"
		                 </c:if>
		                  value="不合格"><fmt:message key="unqualified"/></option>
		        	 </select>
				</div>			
			</div>
			<div class="form-line">	
				<div class="form-label"><fmt:message key ="branches_selection" /></div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
<%-- 					<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
				</div>
				<div class="form-label"><fmt:message key ="distribution_area" /></div>
				<div class="form-input">
					<select class="select" name="psarea">
						<option value=""></option>
						<c:forEach var="codeDes" items="${codeDesList }">
							<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
						</c:forEach>
					</select>
				</div>
			</div>
		   <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;text-align:center;"><input type="checkbox" id="chkAll"/></span></td>								
								<td rowspan="2"><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:35px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="requisitioning_company"/></span></td>
								<td colspan="5"><span><fmt:message key ="standard_unit" /></span></td>
								<td colspan="3"><span><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key="direction"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key ="whether_storage" /></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key="whether_library"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key ="remark" /></span></td>
							</tr>
							<tr>
 								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:40px;"><fmt:message key="order_goods"/><br/><fmt:message key="quantity"/></span></td>
 								<td><span style="width:40px;"><font color="blue"><fmt:message key ="actual" /><br/><fmt:message key="quantity"/></font></span></td>
 								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
 								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:40px;"><fmt:message key="order_goods"/><br/><fmt:message key="quantity"/></span></td>
 								<td><span style="width:40px;"><font color="blue"><fmt:message key ="actual" /><br/><fmt:message key="quantity"/></font></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>		
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.chkstoNo}" style="width:45px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code}" style="width:70px;">${dis.sp_code}</span></td>
									<td><span title="${dis.sp_name}" style="width:70px;">${dis.sp_name}</span></td>
									<td><span title="${dis.sp_desc}" style="width:35px;">${dis.sp_desc}</span></td>
									<td><span title="${dis.firmCode}" style="width:70px;">${dis.firmDes}</span></td>
									<td><span title="${dis.unit}" style="width:30px;">${dis.unit}</span></td>
									<td><span title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>" style="width:40px;"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" style="width:40px;"><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" style="width:40px;"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td><span title="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>" style="width:50px;"><fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td><span title="${dis.unit1}" style="width:30px;">${dis.unit1}</span></td>
									<td><span title="${dis.amount1}" style="width:40px;">${dis.amount1}</span></td>
									<td><span title="${dis.amount1in}" style="width:40px;">${dis.amount1in}</span></td>
									<td><span title="${dis.inout}" style="width:30px;">${dis.inout}</span></td>
									<td><span title="${dis.deliverCode}" style="width:50px;">${dis.deliverCode}</span></td>
									<td><span title="${dis.deliverDes}" id="d_del_${dis.id}" style="width:100px;">${dis.deliverDes}</span></td>
									<td>
										<span style="width:30px;text-align:center;">
											<img src="<%=path%>/image/kucun/${dis.chkin}.png"/>
										</span>
									</td>
									<td>
										<span style="width:30px;text-align:center;">
											<img src="<%=path%>/image/kucun/${dis.chkout}.png"/>
										</span>
									</td>
									<td>
										<span title="${dis.memo}" style="width:30px;">${dis.memo}</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点
			$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey == false) return;
		 			switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 83: $('#autoId-button-103').click(); break;
	                case 65: $('#autoId-button-104').click(); break;
	                case 68: $('#autoId-button-105').click(); break;
	                case 67: $('#autoId-button-106').click(); break;
            	}
			});
			loadToolBar([true,true,false,true,true,true]);
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('#textInput').each(function(i){
				$(this).find('input').bind('focus',function(e){
					$(this).focus().select();
				});
			});
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('.Wdate').bind('click',function(){
				if($("#selectIndSta").val()=="YES"){
					$("#selectIndSta").val("NO");
					new WdatePicker();
				}else{
					$("#selectIndSta").val("YES");
					new WdatePicker();
				}
			});
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#resetVal').attr('disabled',true);//不可编辑
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>','<%=path%>/deliver/selectOneDeliver.do',offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			
			$("#seachOnePositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positnCode").val(),
					single:false,
					tagName:'positnDes',
					tagId:'positnCode',
					typn:'1201,1202',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
			$('#seachOnePositn123').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('bdat');
					//三合一库，验货直发，仓位选择，可控制显示主直拨库和基地仓库  wangjie 2014年12月12日 08:46:27
					top.cust('<fmt:message key="please_select_positions"/>','<%=path%>/positn/selectPositn.do?mold='+'one'+'&typ=cangku',offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_branche"/>','<%=path%>/positn/selectPositn.do?mold='+'one',offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'));	
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					var offset = getOffset('bdat');
					//top.cust('<fmt:message key="please_select_category"/>','<%=path%>/grpTyp/selectOneGrpTyp.do',offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="select" /><fmt:message key ="straight_hair" />(<u>F</u>)',
							title: '<fmt:message key="select"/><fmt:message key ="straight_hair" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$("#listForm").submit();
							}
						},{
							text: '<fmt:message key ="generation_of_hair" />',
							title: '<fmt:message key ="generation_of_hair" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								saveOrUpdateChkinm();
							}
// 						},{
// 							text: '<fmt:message key="print"/>',
// 							title: '<fmt:message key="print"/>',
// 							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[5],
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['-140px','-100px']
// 							},
// 							handler: function(){
// 								printDis("printReceipt.do");
// 							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
			});
		}		
		//修改报货
		function updateDis(){
			var sta=1;//为1时表示数据不为空，2数据为空;
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(":checkbox[name='idList']");
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_saved_whether_continue"/>!')){
					checkboxList.filter(':checked').each(function(i){
						if(null!=$(this).val() && ""!=$(this).val()){
							selected["listDisList["+i+"].id"] = $(this).val();
						}else{
							sta=2;
							return;
						}
						if(null!=$(this).closest("tr").find('td:eq(19)').find('input').val() && ""!=$(this).closest("tr").find('td:eq(19)').find('input').val()){
							selected["listDisList["+i+"].deliverCode"] = $(this).closest("tr").find('td:eq(19)').find('input').val();
						}else{
							sta=2;
							return;
						}
						if(null!=$(this).closest("tr").find('td:eq(9)').find('input').val() && ""!=$(this).closest("tr").find('td:eq(9)').find('input').val()){
							selected["listDisList["+i+"].amountin"] = $(this).closest("tr").find('td:eq(9)').find('input').val();
						}else{
							sta=2;
							return;
						}
						if(null!=$(this).closest("tr").find('td:eq(10)').find('input').val() && ""!=$(this).closest("tr").find('td:eq(10)').find('input').val()){
							selected["listDisList["+i+"].pricein"] = $(this).closest("tr").find('td:eq(10)').find('input').val();
						}else{
							sta=2;
							return;
						}
						if(null!=$(this).closest("tr").find('td:eq(11)').find('input').val() && ""!=$(this).closest("tr").find('td:eq(11)').find('input').val()){
							selected["listDisList["+i+"].totalAmt"] = $(this).closest("tr").find('td:eq(11)').find('input').val();
						}else{
							sta=2;
							return;
						}
						if(null!=$(this).closest("tr").find('td:eq(12)').find('input[name="bak2"]').val() && ""!=$(this).closest("tr").find('td:eq(12)').find('input[name="bak2"]').val()){
							selected["listDisList["+i+"].bak2"] = $(this).closest("tr").find('td:eq(12)').find('input[name="bak2"]').val();
						}else{
							sta=2;
							return;
						}
					});
					if(sta==2){alert('<fmt:message key ="Data_can_not_be_empty" />！');return;}
					$.post("<%=path%>/disFenBo/updateDis.do",selected,function(data){
						var rs = eval('('+data+')');
						if(rs.pr=="succ"){
							alert('<fmt:message key ="update_successful" />！');
							var action="<%=path%>/disFenBo/listCheck.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
						}else{
							alert('<fmt:message key ="Change_failed_documents_have_been_put_in_storage_or_out_of_stock" />！');
							var action="<%=path%>/disFenBo/listCheck.do";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
						}
					});
					loadToolBar([true,true,false,true,true,true]);
				}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return ;
			}
		}
		//查找供应商
		function searchDeliver(obj){
			if(window.event.keyCode==13){
				$("#selectDelCodeId").val(obj);
				$('body').window({
					id: 'window_searchDeliver',
					title: '<fmt:message key="query_suppliers"/>',
					content: '<iframe id="searchDeliverFrame" frameborder="0" src="<%=path%>/deliver/searchDeliver.do"></iframe>',
					width: '800px',
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />(<u>F</u>)',
								title: '<fmt:message key="query_suppliers"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('searchDeliverFrame')){
										submitFrameForm('searchDeliverFrame','SearchForm');
									}
								}
							},{
								text: '<fmt:message key="quit"/>',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});								
			}else{
				return;
			}
		}
		//输入日期回车
		function selectInd(){
			if(window.event.keyCode==13){
				if($("#selectIndSta").val()=="YES"){
					$("#selectIndSta").val("NO");
					new WdatePicker();
				}else{
					$("#selectIndSta").val("YES");
					new WdatePicker();
				}
			}
		}
		//打印单据
		function printDis(e){ 
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disFenBo/'+e;	
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		//让时间选中框可不可选
		function checktim_btn(obj){
			if(obj.checked){
				var today=new Date();
				var hh=today.getHours();
				if(hh<10)
					hh="0"+hh;
				var mm=today.getMinutes();
				if(mm<10)
					mm="0"+mm;
				var ss=today.getSeconds();
				if(ss<10)
					ss="0"+ss;
				var curTime=hh+":"+mm+":"+ss;
				$("#chectim").val(curTime);
				$("#chectim").removeAttr("disabled","disabled");
			}else{
				$("#chectim").val("");
				$("#chectim").attr("disabled","disabled");
			}
		}
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			var hh=myDate.getHours();
			var mm=myDate.getMinutes();
			var ss=myDate.getSeconds();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			if(hh<10)
				hh="0"+hh;
			if(mm<10)
				mm="0"+mm;
			if(ss<10)
				ss="0"+ss;
			return fullDate=yy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss;
		}
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key ="not_valid_number" />！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		//计算单价
		function fPrice(){
			var tNum=$("#tNum").val();
			var tAmt=$("#tAmt").val();
			var result=tAmt/tNum;
			if(!isNaN(tNum) && !isNaN(tAmt) && ""!=tNum && ""!=tAmt && 0!=tNum && 0!=tAmt){
				$("#result").val(result);
			}else{
				$("#result").val("0");
			}
		}
		//计算总额
		function sumTotalAmt(e,f){
			$("#h_pricein_"+f).val($("#pricein_"+f).val());
			var price=$("#pricein_"+f).val();
			var amti=$("#amountin_"+f).val();
			$("#totalAmt_"+f).val((price*amti).toFixed(2));
		}
		//计算单价
		function sumPrice(e,f){
			var totalAmt = e.value;
			var amountin=$("#amountin_"+f).val();
			var pricein="0.00";
			if(Number(amountin)!=0){
				pricein=(totalAmt/amountin).toFixed(2);
			}
			if(isNaN(totalAmt)){
				e.value=e.defaultValue;
				showMessage({
					type: 'error',
					msg: '<fmt:message key ="Amount_is_not_valid" />！',
					speed: 1000
				});
				$(e).focus();
				return;
			}else{
				$("#h_pricein_"+f).val((totalAmt/amountin));
				$("#pricein_"+f).val((pricein));
			}
		}
		//还原零值
		function resetZero(){
			var items=$("input[name='idList']");	
			for ( var i = 0; i < items.length; i++){
				if(items[i].checked){
					//$("#amountin_"+items[i].value).val("0.00");
					$("#pricein_"+items[i].value).val("0.00");
					$("#totalAmt_"+items[i].value).val("0.00");
				}
			}
		}
		//已至
		function imgClick(e){
			var bak2=$('#bak2_'+e).val();
			var ok='<%=path%>/image/themes/icons/ok.png';
			var no='<%=path%>/image/themes/icons/no.png';
			if(bak2=='1'){
				$('#img_'+e).attr('src',no);
				$('#bak2_'+e).val('2');
			}else{
				$('#img_'+e).attr('src',ok);
				$('#bak2_'+e).val('1');
			}
		}
		//增加 或者 修改
		function saveOrUpdateChkinm(){	
			if($('#deliverDes').val()=="" ||  $('#deliverDes').val() == null){
				alert("<fmt:message key ="please_select_suppliers" />！");
				return;
			}
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			var selected = {};
			selected['positn.code'] = $('#positnCode').val();
			selected['deliver.code'] = $('#deliverCode').val();
			//table 数组
			var isNull=0;
			if(confirm("<fmt:message key ="Please_confirm_whether_the_input_is_correct_This_will_generate_the_straight_single_Are_you_sure_you_want_to_continue" />？")){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					checkboxList.filter(':checked').each(function(i){
						isNull=1;
						var amount=$(this).parents('tr').find('td:eq(9)').find('span').attr('title');
						if(amount===0 || amount==0.0 ||amount==0.00 ){
							isNull=2;
						}
						selected['chkindList['+i+'].supply.sp_code'] = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
						selected['chkindList['+i+'].supply.sp_name'] = $(this).parents('tr').find('td:eq(4)').find('span').attr('title');
						selected['chkindList['+i+'].supply.sp_desc'] = $(this).parents('tr').find('td:eq(5)').find('span').attr('title');
						selected['chkindList['+i+'].supply.unit'] = $(this).parents('tr').find('td:eq(6)').find('span').attr('title');//单位
						selected['chkindList['+i+'].amount'] = $(this).parents('tr').find('td:eq(9)').find('span').attr('title');//数量
						selected['chkindList['+i+'].price'] = $(this).parents('tr').find('td:eq(10)').find('span').attr('title');//单价
						selected['chkindList['+i+'].totalamt'] = $(this).parents('tr').find('td:eq(11)').find('span').attr('title');//金额
						selected['chkindList['+i+'].amount1'] = $(this).parents('tr').find('td:eq(14)').find('span').attr('title');//数量1
						selected['chkindList['+i+'].indept'] = $(this).parents('tr').find('td:eq(6)').find('span').attr('title');//分店代码
						selected['chkindList['+i+'].memo'] = $(this).parents('tr').find('td:eq(20)').find('span').attr('title');//备注
						selected['chkindList['+i+'].chkstono'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');//申购单号
						selected['chkindList['+i+'].id'] = $(this).parents('tr').find('td:eq(2)').find('span').text();//申购单号
					});
				}
				
				//wangjie 2014年11月5日 15:54:40 (解决点击取消按钮 有错误提示的修改)
				if(isNull==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(isNull==2){
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				$.post('<%=path%>/disFenBo/addzb.do',selected,function(data){
					var rs = eval('('+data+')');
					if(rs=="1"){
						showMessage({
							type: 'success',
							msg: '<fmt:message key ="storage_completed_to_query_documents" />！',
							speed: 1000
						});	
						$('#listForm').submit();
					}else{
						showMessage({
							type: 'error',
							msg: '<fmt:message key ="The_store_to_generate_a_single_error_storage_termination" />！',
							speed: 1000
						});	
					}
				});
			}
		}	
		</script>
	</body>
</html>