
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="summary_of_reported_cargo"/>—部门明细</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
/* 		.page{margin-bottom: 25px;} */
		</style>
	</head>
	<body>
	<form action="<%=path%>/disFenBo/findDeptDetail2.do" id="listForm" name="listForm" method="post">
		<div class="bj_head">
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;"><fmt:message key ="date" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;：</div>
				<div class="form-input">
					<span><input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/></span>
		            <span><input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked"</c:if> id="ind1" name="ind1" value="ind"/></span>
		            <span><font style="color:blue;"><fmt:message key="arrival_date1"/></font></span>
				</div>
				<div class="form-label" style="width:65px;margin-left:55px;"><fmt:message key ="positions" />：</div>
				<div class="form-input">
					<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
					<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="supply_units"/>：</div>
				<div class="form-input">
					<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="reported_goods_stores"/>：</div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
			</div>	
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${dis.sp_code }" />
					<img id="seachSupply" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label" style="margin-left:6px;"><fmt:message key="category_selection"/>：</div>
				<div class="form-input">
					<input type="text" id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
					<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
			    </div>
			    <div class="form-label" style="width:180px;padding-left:50px;margin-top:0px;vertical-align:middle;text-align: right;margin-left:20px;">
					<font size="3">[</font><input type="radio" name="yndo" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key ="scm_have_done" />
					<input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key ="scm_un_done" /><font size="3">]</font>
				</div>
			</div>
		</div>
			 <div id="grid" class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:35px;"><fmt:message key ="the_serial_number" /></span></td>
								<td><span style="width:50px;"><fmt:message key ="orders_num" /></span></td>
								<td><span style="width:50px;"><fmt:message key ="serial_number" /></span></td>
								<td><span style="width:120px;"><fmt:message key ="suppliers" /></span></td>
								<td><span style="width:120px;"><fmt:message key ="scm_dept" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="supplies_code" /></span></td>
								<td><span style="width:100px;"><fmt:message key ="supplies_name" /></span></td>
 								<td><span style="width:60px;"><fmt:message key ="supplies_specifications" /></span></td>
 								<td><span style="width:40px;"><fmt:message key ="procurement_unit_br" /></span></td>
 								<td><span style="width:40px;"><fmt:message key ="quantity" /></span></td>
								<td><span style="width:80px;"><fmt:message key ="remark" /></span></td>
<!--  								<td><span style="width:40px;">报货<br/>数量</span></td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr class="tr-toggle">
									<td class="num"><span style="width:35px;">${status.index+1}</span></td>
									<td><span title="${dis.chkstoNo }" style="width:50px;">${dis.chkstoNo }</span></td>
									<td><span title="${dis.id }" style="width:50px;">${dis.id }</span></td>
									<td><span title="${dis.deliverDes }" style="width:120px;">${dis.deliverDes }</span></td>
									<td><span title="${dis.firmDes }" style="width:120px;">${dis.firmDes }</span></td>
									<td><span title="${dis.sp_code }" style="width:80px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:100px;">${dis.sp_name }</span></td>
	 								<td><span title="${dis.sp_desc }" style="width:60px;">${dis.sp_desc }</span></td>
	 								<td><span title="${dis.unit3 }" style="width:40px;">${dis.unit3 }</span></td>
	 								<td><span title="<fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/>" style="width:40px;text-align:right;"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span></td>
	 								<td><span title="${dis.memo }" style="width:80px;">${dis.memo }</span></td>
<%-- 	 								<td><span title="${dis.amount }" style="width:40px;text-align:right;">${dis.amount }</span></td> --%>
	 							</tr>
	 							<c:if test="${!empty dis.deptList }">
	 								<c:forEach var="chkstod" items="${dis.deptList}" varStatus="statu">
									<tr>
										<td class="num"><span style="width:35px;"></span></td>
										<td><span style="width:35px;"></span></td>
										<td><span style="width:35px;"></span></td>
										<td><span style="width:35px;"></span></td>
										<td><span title="${chkstod.position.des }" style="width:120px;">${chkstod.position.des }</span></td>
										<td><span style="width:35px;"></span></td>
										<td><span style="width:35px;"></span></td>
		 								<td><span style="width:35px;"></span></td>
		 								<td><span style="width:35px;"></span></td>
		 								<td><span title="<fmt:formatNumber value="${chkstod.amount}" type="currency" pattern="0.00"/>" style="width:40px;text-align:right;"><fmt:formatNumber value="${chkstod.amount}" type="currency" pattern="0.00"/></span></td>
		 								<td><span title="${chkstod.memo }" style="width:80px;">${chkstod.memo }</span></td>
	 								</tr>
	 								</c:forEach>
	 							</c:if>
	 						</c:forEach>
	 					</tbody>
	 				</table>
	 			</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" /> 
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				});  
// 			    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			    $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),62);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid',25);	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				$("#seachPositn").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#firmCode").val(),
						single:false,
						tagName:'firmDes',
						tagId:'firmCode',
						title:'<fmt:message key="please_select_positions"/>'
					});
				});
				
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliverDes').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
					}
				});
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positnCode').val();
						var defaultName = $('#positnDes').val();
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
					}
				});
				$('#seachSupply1').bind('click.custom',function(e){
					if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
						if(!!!top.customWindow){
							var defaultCode = $('.grid').find('.table-body').find(':checkbox').filter(':checked').parents('tr').find('td:eq(3)').find('span').attr('title');
							var offset = getOffset('maded');
							top.cust('<fmt:message key="please_select_materials"/>',encodeURI('<%=path%>/supply/findAllSupplySXS.do?defaultCode='+defaultCode),offset,$('#spName'),$('#spCode'),'560','420','isNull');
						}
					}else {
						$('.search-div').hide();
						alert("<fmt:message key ="select_a_need_to_adjust_the_material_data" />！");
					}
				});
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#typCode').val();
						//var defaultName = $('#typDes').val();
						var offset = getOffset('positnDes');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					}
				});
			});
			
			//excel
			function exportDis(){
				$("#wait2").val('NO');//不用等待加载
				$('#listForm').attr('action','<%=path%>/disFenBo/exportDeptDetail2.do');
				$('#listForm').submit();
				$("#wait2 span").html("loading...");
				$('#listForm').attr('action','<%=path%>/disFenBo/findDeptDetail2.do');	
				$("#wait2").val('');//等待加载还原
			};
			
			//部门分拨单
			function exportDeptExcel(){
				$("#wait2").val('NO');//不用等待加载
				$('#listForm').attr('action','<%=path%>/disFenBo/exportDeptExcel.do');
				$('#listForm').submit();
				$("#wait2 span").html("loading...");
				$('#listForm').attr('action','<%=path%>/disFenBo/findDeptDetail2.do');	
				$("#wait2").val('');//等待加载还原
			};
			
			//打印单据
			function printDis(){ 
				$("#wait2").val("NO");
				$('#listForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/disFenBo/printDeptDetail2.do';	
				var action1="<%=path%>/disFenBo/findDeptDetail2.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
				$('#listForm').attr('action',action1);
				$('#listForm').attr('target','');
				$("#wait2").val("");
			};
			
			function pageReload(){
				$('#listForm').submit();
			}
		</script>
	</body>
</html>