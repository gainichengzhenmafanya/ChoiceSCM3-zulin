<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_cargo_distribution"/>报货验收</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
			<style type="text/css">
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				text-align: center;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			.page{
				margin-bottom:25px;
			}
			.form-line .form-label{
				width: 11%;
			}
			.form-line .form-input input[type=text]{
				width: 150px;
			}
			.nextclass{
				text-align: right;
			}
			</style>	
	</head>
	<body>
		<div class="tool"></div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="buttonQiet" name="buttonQiet"/>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<form id="listForm" action="<%=path%>/disYanShou/listCheck.do" method="post">
			<div class="bj_head">
				<div class="form-line">	
					<div class="form-label"><fmt:message key="arrival_date"/>：</div>
					<div class="form-input" style="width:190px;">
						<input type="text" style="width:82px;" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/>
						<font style="color:blue;"><fmt:message key="to"/></font>
						<input type="text" style="width:82px;" id="edat" name="edat" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/>
					</div>
					<div class="form-label">
						<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
						<font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>				
					</div>
					<div class="form-input"></div>
					<div class="form-input">
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="supply_units"/>：</div>
					<div class="form-input">
						<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}" style="width:85%;"/>
						<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />            
					</div>
					<div class="form-label"><fmt:message key="category_selection"/>：</div>
					<div class="form-input">
						<input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
						<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
					</div>
					<span style="margin-left:50px; font-weight: bold;"><fmt:message key ="You_have" />  <span id="changeNum" style="color: red;">0</span>    <fmt:message key ="unsaved_data" /></span>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="position_select"/>：</div>
					<div class="form-input">
						<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}" style="width:85%;"/>
						<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="supplies_code"/>：</div>
					<div class="form-input">
						<input type="text" id="sp_code" name="sp_code" value="${dis.sp_code }"/>			
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>	
					<div class="form-label" style="margin-left: 50px;margin-top:5px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" >
					<div id="currState" class="form-label" style="background-color: #28FF28;height:15px;width:0px;" ></div></div>
					<div id="per" class="form-input" style="margin-left: 5px;">0</div>			
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key ="requisitioning_company" />：</div>
					<div class="form-input">
						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}" style="width:85%;"/>
						<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key ="psarea" />：</div>
					<div class="form-input">
						<select class="select" name="psarea">
							<option value=""></option>
							<c:forEach var="codeDes" items="${codeDesList }">
								<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label" style="width: 40px;"></div>
					<div class="form-input">
						<a class="easyui-linkbutton" plain="true" iconCls="" style="margin-bottom:5px;" onclick="javascript:allConfirm('Y');">全部已对</a>
						<a class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:allConfirm('N');">全部未对</a>
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label">
				    	<fmt:message key="abbreviation_code"/>：
				    </div>
					<div class="form-input">
						<input type="text" id="sp_init" name="sp_init" value="${dis.sp_init }" onkeyup="ajaxSearch()" onfocus="this.select();" style="text-transform:uppercase;"/>					
					</div>
<%-- 					<div class="form-label"><fmt:message key="total_number"/></div> --%>
<!-- 					<div class="form-input"> -->
<!-- 						<input style="width:60px;" id="tNum" name="tNum" type="text"/> -->
<%-- 						<fmt:message key="total_amount"/> --%>
<!-- 						<input style="width:60px;" id="tAmt" name="tAmt" type="text"/> -->
<!-- 					</div> -->
<!-- 					<div class="form-label" style="width: 40px;"> -->
<!-- 					</div> -->
<!-- 					<div class="form-input"> -->
<%-- 						<a id="add" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:fPrice();"><fmt:message key="unit_price"/></a> --%>
<!-- 						<input style="width:60px;" id="result" name="result" type="text" readonly="readonly"/> -->
<%-- 						<a id="resetVal" class="easyui-linkbutton" plain="true" iconCls="" onclick="javascript:resetZero();" title="只有勾选中的才会被还原！"><fmt:message key="restore_value_to_zero"/></a> --%>
<!-- 					</div> -->
				</div>	
			</div>
		   	
		   <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;text-align:center;"><input type="checkbox" id="chkAll"/></span></td>								
								<td rowspan="2"><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2"><span style="width:35px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="requisitioning_company"/></span></td>
								<td colspan="5"><span><fmt:message key ="standard_unit" /></span></td>
								<td rowspan="2"><span style="width:20px;"><fmt:message key="already"/><br/><fmt:message key="to1"/></span></td>
								<td colspan="3"><span><fmt:message key="reference_unit"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key="direction"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="inventory"/></span></td>
								<td rowspan="2"><span style="width:30px;"><fmt:message key="stock"/><br/><fmt:message key="inadequate"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2"><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2"><span style="width:50px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
 								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:40px;"><fmt:message key="order_goods"/><br/><fmt:message key="quantity"/></span></td>
 								<td><span style="width:40px;"><font color="blue"><fmt:message key="receive_goods"/><br/><fmt:message key="quantity"/></font></span></td>
 								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
 								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
 								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:40px;"><fmt:message key="order_goods"/><br/><fmt:message key="quantity"/></span></td>
 								<td><span style="width:40px;"><font color="blue"><fmt:message key="receive_goods"/><br/><fmt:message key="quantity"/></font></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>		
							<c:forEach var="dis" items="${disesList1}" varStatus="status">
								<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.id }" style="width:45px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:70px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:35px;">${dis.sp_desc }</span></td>
									<td><span title="${dis.firmDes }" style="width:70px;">${dis.firmDes }</span></td>
									<td><span title="${dis.unit }" style="width:30px;">${dis.unit }</span></td>
									<td><span title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>" style="width:40px;text-align: right;"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td class="textInput">
										<span style="width:40px;">
											<input title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" type="text" style="width:40px;text-align: right;" class="nextclass" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" id="amountin_${dis.id }" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}')" onkeyup="validate(this);sumTotalAmt(this,'${dis.id }')"/>
										</span>
									</td>
									<td class="textInput">
										<span style="width:40px;">
											<input type="hidden" id="h_pricein_${dis.id }" value="${dis.pricein}"/>
											<input title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width:40px;text-align: right;" class="nextclass" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" id="pricein_${dis.id }" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onkeyup="validate(this);sumTotalAmt(this,'${dis.id }')"/>
										</span>
									</td>
									<td class="textInput">
										<span style="width:50px;">
											<input title="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width:50px;text-align: right;" class="nextclass" value="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>" id="totalAmt_${dis.id }" onblur="saveTotalPrice(this,'${dis.id}')" onfocus="this.select()" onkeyup="validate(this);sumPrice(this,'${dis.id }')"/>
										</span>
									</td>
									<td>
										<span style="width:20px;text-align:center;" onclick="saveBak2('${dis.id}')">
											<input type="hidden" id="bak2_${dis.id }" name="bak2" value="${dis.bak2 }"/>
											<input type="hidden" name="h_id" value="${dis.id }"/>
											<img id="img_${dis.id }" class="img_bak2" src="<%=path%>/image/kucun/${dis.bak2}.png"/>
										</span>
									</td>
									<td><span title="${dis.unit1 }" style="width:30px;">${dis.unit1 }</span></td>
									<td><span title="${dis.amount1 }" style="width:40px;text-align: right;">${dis.amount1 }</span></td>
									<td><span title="${dis.amount1in }" style="width:40px;text-align: right;">${dis.amount1in }</span></td>
									<td><span title="${dis.inout }" style="width:30px;">${dis.inout }</span></td>
									<td><span title="${dis.cnt }" style="width:40px;text-align:right;">${dis.cnt }</span></td>
									<td>
										<span style="width:30px;">
											<c:choose>
												<c:when test="${dis.amount>dis.cnt}">
													<img src="<%=path%>/image/kucun/Y.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/kucun/N.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
									<td class="textInput">
										<span title="${dis.deliverCode }" style="width:50px;">${dis.deliverCode }
										</span>
									</td>
									<td><span title="${dis.deliverDes }" id="d_del_${dis.id }" style="width:100px;">${dis.deliverDes }</span></td>
									<td><span style="width:50px;">${dis.memo }</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		$(document).ready(function(){
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),190);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#resetVal').attr('disabled',true);//不可编辑
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(e.keyCode==32){return false;}//屏蔽空格
		 		if(e.altKey == false) return;
		 			switch (e.keyCode){
				                case 70: $('#autoId-button-101').click(); break;
				                case 69: $('#autoId-button-102').click(); break;
				                case 83: $('#autoId-button-103').click(); break;
				                case 65: $('#autoId-button-104').click(); break;
				                case 68: $('#autoId-button-105').click(); break;
				                case 67: $('#autoId-button-106').click(); break;
				                case 88: changeFocus();break;//Alt+X
			            	}
			});
			//编辑到货数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhysEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
			 		}
				}
			});
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positnCode').val();
					var defaultName = $('#positnDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firmCode').val();
					var defaultName = $('#firmDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_branche"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					var defaultName = $('#typDes').val();
					var offset = getOffset('bdat');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
			
			if('${action}'!='init' && '${currState}'!='1'){
				addTr('first');	
			}else if('${action}'!='init' && '${currState==1}'){
				$("#per").text('100%');
				$("#currState").width(200);
			}
			pageSize = '${pageSize}';
			var ndivHeight = $(".table-body").height();
			$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		          });
			if('${action}'=='init'){
				parent.bhysEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.ysTrList=undefined;//将trlist清空
				parent.ysChangeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#changeNum").text(parent.ysChangeNum);
			}
			//判断如果不是编辑状态
			if(parent.bhysEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				loadToolBar([true,true,false,true,true,true]);
			}else{
				new tabTableInput("table-body","text"); //input  上下左右移动
				loadToolBar([true,false,true,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',false);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
			
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');						
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="select"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								if(null!=$('#bdat').val() && ""!=$('#bdat').val() && null!=$('#edat').val() && ""!=$('#edat').val()){
	 								$("#listForm").submit();
	 							}else{
	 								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
	 							}
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="edit"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','0px']
							},
							handler: function(){
								
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="data_empty_edit_invalid"/>！！');
								}else{
									parent.bhysEditState='edit';
									loadToolBar([true,false,true,false,false,false]);
									$('tbody input[type="text"]').attr('disabled',false);
									$('#resetVal').attr('disabled',false);
									new tabTableInput("table-body","text"); //input  上下左右移动	
									if($('tbody tr:first .nextclass').length!=0){
										$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
									}
								}
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/>',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								updateDis();
							}
						},{
							text: '<fmt:message key="acceptance_to_storage"/>',
							title: '<fmt:message key="acceptance_to_storage"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								openCheckin();
							}
						},{
							text: '<fmt:message key="Acceptance_to_library"/>',
							title: '<fmt:message key="Acceptance_to_library"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								openCheckout();
							}
						},{
							text: '<fmt:message key="print_acceptance_single"/>',
							title: '<fmt:message key="print_acceptance_single"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printDis("printReceipt.do");
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
// 								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								
								if(parent.bhysEditState=="edit"){
									if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}else{
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
								}
							}
						}]
			});
		}		
		//修改报货
		function updateDis(){
			$("#sp_init").focus();
			
			//如果修改过数据就保存 
			if(parent.ysTrList){
				var predata = parent.ysTrList;
				var disList = {};
				var num=0;
				for(var i in predata){
					disList['listDis['+num+'].id']=i;
					undefined==predata[i].amountin?'':disList['listDis['+num+'].amountin']=predata[i].amountin;
					undefined==predata[i].pricein?'':disList['listDis['+num+'].pricein']=predata[i].pricein;
					undefined==predata[i].totalAmt?'':disList['listDis['+num+'].totalAmt']=predata[i].totalAmt;
					undefined==predata[i].bak2?'':disList['listDis['+num+'].bak2']=predata[i].bak2;
					num++;
				}
				$.post("<%=path%>/disYanShou/updateDis.do",disList,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
// 						alert('成功修改'+rs.updateNum+'条数据！');
						alert('<fmt:message key="successful"/><fmt:message key="update"/>'+rs.updateNum+'<fmt:message key="article"/><fmt:message key="data"/>！');
						var action="<%=path%>/disYanShou/listCheck.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}else{
						alert('<fmt:message key="update"/><fmt:message key="failure"/>！\n<fmt:message key="the_documents_of_inbound_or_outbound"/>！');
						var action="<%=path%>/disYanShou/listCheck.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}
				});
				
			}else{
				alert('<fmt:message key="do_not_modify_any_data"/>！');
			}
			parent.bhysEditState = '';
			parent.ysChangeNum = 0;
			parent.ysTrList=undefined;
			loadToolBar([true,true,false,true,true,true]);
			$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			$('#listForm').submit();
		}
		
		//打印单据
		function printDis(e){ 
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disYanShou/'+e;	
			var action1="<%=path%>/disYanShou/listCheck.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}		
		//让时间选中框可不可选
		function checktim_btn(obj){
			if(obj.checked){
				var today=new Date();
				var hh=today.getHours();
				if(hh<10)
					hh="0"+hh;
				var mm=today.getMinutes();
				if(mm<10)
					mm="0"+mm;
				var ss=today.getSeconds();
				if(ss<10)
					ss="0"+ss;
				var curTime=hh+":"+mm+":"+ss;
				$("#chectim").val(curTime);
				$("#chectim").removeAttr("disabled","disabled");
			}else{
				$("#chectim").val("");
				$("#chectim").attr("disabled","disabled");
			}
		}
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			var hh=myDate.getHours();
			var mm=myDate.getMinutes();
			var ss=myDate.getSeconds();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			if(hh<10)
				hh="0"+hh;
			if(mm<10)
				mm="0"+mm;
			if(ss<10)
				ss="0"+ss;
			return fullDate=yy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss;
		}
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		//计算单价
		function fPrice(){
			var tNum=$("#tNum").val();
			var tAmt=$("#tAmt").val();
			var result=tAmt/tNum;
			if(!isNaN(tNum) && !isNaN(tAmt) && ""!=tNum && ""!=tAmt && 0!=tNum && 0!=tAmt){
				$("#result").val(Number(result).toFixed(3));
			}else{
				$("#result").val("0");
			}
		}
		//计算总额
		function sumTotalAmt(e,f){
			if(e.value==e.defaultValue){
				return;
			}
			$("#h_pricein_"+f).val($("#pricein_"+f).val());
			var price=$("#pricein_"+f).val();
			var amti=$("#amountin_"+f).val();
			$("#totalAmt_"+f).val((price*amti).toFixed(2));
			if(parent.ysTrList){
				if(parent.ysTrList[f]){
					parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
				}else{
					parent.ysTrList[f]={};
					parent.ysChangeNum = parent.ysChangeNum+1;
					$("#changeNum").text(parent.ysChangeNum);
					parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
				}
			}else{
				parent.ysTrList = {};
				parent.ysTrList[f]={};
				parent.ysChangeNum = 1;
				$("#changeNum").text(1);
				parent.ysTrList[f]['totalAmt']=(price*amti).toFixed(2);
			}
		}
		//计算单价
		function sumPrice(e,f){
			if(e.value==e.defaultValue){
				return;
			}
			var totalAmt = e.value;
			var amountin=$("#amountin_"+f).val();
			var pricein="0.00";
			if(Number(amountin)!=0){
				pricein=(totalAmt/amountin).toFixed(2);
			}
			if(isNaN(totalAmt)){
				e.value=e.defaultValue;
				showMessage({
					type: 'error',
					msg: "<fmt:message key='amount'/><fmt:message key='not_valid_number'/>！",
					speed: 1000
				});
				$(e).focus();
				return;
			}else{
				$("#h_pricein_"+f).val((totalAmt/amountin));
				$("#pricein_"+f).val((pricein));
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['pricein']=totalAmt/amountin;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['pricein']=totalAmt/amountin;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['pricein']=totalAmt/amountin;
				}
			}
		}
		//还原零值
		function resetZero(){
			var items=$("input[name='idList']").filter(":checked");	
			if(items.length>0){
				for (var i = 0; i < items.length; i++){
					//$("#amountin_"+items[i].value).val("0.00");
					$("#pricein_"+items[i].value).val("0.00");
					$("#totalAmt_"+items[i].value).val("0.00");
					var f = items[i].value;
					if(parent.ysTrList){
						if(parent.ysTrList[f]){
							parent.ysTrList[f]['pricein']=0;
							parent.ysTrList[f]['totalAmt']=0;
						}else{
							parent.ysTrList[f]={};
							parent.ysChangeNum = parent.ysChangeNum+1;
							$("#changeNum").text(parent.ysChangeNum);
							parent.ysTrList[f]['pricein']=0;
							parent.ysTrList[f]['totalAmt']=0;
						}
					}else{
						parent.ysTrList = {};
						parent.ysTrList[f]={};
						parent.ysChangeNum = 1;
						$("#changeNum").text(1);
						parent.ysTrList[f]['pricein']=0;
						parent.ysTrList[f]['totalAmt']=0;
					}
				}
			}else{
				alert("<fmt:message key='select_the_data_you_want_to_restore'/>！");
			}
		}
		//打开验收入库界面
		function openCheckin(){
			var bdat=$('#bdat').val();
			var edat=$('#edat').val();
			window.location.replace("<%=path%>/disRuKu/listCheckin.do?bdat="+bdat+"&edat="+edat);
		}
		//打开验收出库界面
		function openCheckout(){
			window.location.replace("<%=path%>/disChuKu/listCheckout.do?action=init");
		}
		
		//保存到货数量
		function saveAmountin(e,f){
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['amountin']=e.value;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['amountin']=e.value;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['amountin']=e.value;
				}
			}
		}
		//保存单价
		function savePricein(e,f){
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['pricein']=e.value;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['pricein']=e.value;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['pricein']=e.value;
				}
			}
		}
		//保存金额
		function saveTotalPrice(e,f){
			if(e.defaultValue!=e.value){
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['totalAmt']=e.value;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['totalAmt']=e.value;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['totalAmt']=e.value;
				}
			}
		}
		
		//保存是否已对
		function saveBak2(f){
			if(parent.bhysEditState=='edit'){
				var bak2=$('#bak2_'+f).val();
				var val = 0;
				var ok='<%=path%>/image/kucun/1.png';
				var no='<%=path%>/image/kucun/0.png';
				if(bak2=='1'){
					$('#img_'+f).attr('src',no);
					$('#bak2_'+f).val('0');
					val = 0;
				}else{
					$('#img_'+f).attr('src',ok);
					$('#bak2_'+f).val('1');
					val = 1;
				}
				if(parent.ysTrList){
					if(parent.ysTrList[f]){
						parent.ysTrList[f]['bak2']=val;
					}else{
						parent.ysTrList[f]={};
						parent.ysChangeNum = parent.ysChangeNum+1;
						$("#changeNum").text(parent.ysChangeNum);
						parent.ysTrList[f]['bak2']=val;
					}
				}else{
					parent.ysTrList = {};
					parent.ysTrList[f]={};
					parent.ysChangeNum = 1;
					$("#changeNum").text(1);
					parent.ysTrList[f]['bak2']=val;
				}
			}
		}
		
		//编辑状态下用户按Alt+X的时候 将焦点定位到缩写码
		function changeFocus(){
			$("#sp_init").focus();
		}
		
		//根据缩写码查询
		function ajaxSearch(){
			if(window.event.keyCode==13){
				$("#listForm").submit();
			}
		}
		//编辑状态下全部已对
		function allConfirm(state){
			if($('.grid').find('.table-body').find("tr").size()<1){
				alert('<fmt:message key="data_for_the_air_to_operate"/>！');
				return;
			}
			if(parent.bhysEditState=='edit'){
				alert('<fmt:message key="please_save_the_unsaved_data"/>！');
				return;
			}
			var action="<%=path%>/disYanShou/updateAll.do?param="+state;
			if("Y"==state&&confirm("<fmt:message key='determine_whether_all_the_records_for_all_yes_to_modify'/>?")){
// 				parent.bhysEditState='edit';
// 				loadToolBar([true,false,false,false,false,false]);
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			if("N"==state&&confirm("<fmt:message key='determine_whether_all_the_records_for_all_not_to_modify'/>?")){
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		
		var totalCount;
		var condition;
		var currPage;
		function addTr(check){
			if(check=='first'){
				totalCount = '${totalCount}';
				condition = ${disJson};
				currPage= 1;
				condition.maded = "";
				condition.bdat = new Date(condition.bdat.time).format("yyyy-mm-dd");
				condition.edat = new Date(condition.edat.time).format("yyyy-mm-dd");
				condition.ind = "";
				condition.dued = "";
				condition['ind1'] = '${ind1}';
				condition['totalCount'] = totalCount;
				condition['currPage']=1;
				$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
				$("#currState").width(Number('${currState}')*200);
				return;
			}
			$.post("<%=path%>/disYanShou/listAjax.do",condition,function(data){
				var rs = eval('('+data+')');
				//var rs = data;
				$("#per").text((rs.currState*100).toFixed(0)+'%');
				$("#currState").width(""+rs.currState*200+"px");
				//不是最后一页
				var num = rs.currPage*pageSize;
				var disesList1 = rs.disesList1;
				for(var i in disesList1){
					var dis = disesList1[i];
					
					var tr = '<tr ';
					if(dis.amount!=dis.amountin){
						tr = tr+'style="color:red;"';
					}
					tr = tr + '>';
					tr = tr + '<td class="num"><span style="width:25px;">'+ ++num +'</span></td>';
					tr = tr + '<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_'+dis.id+'" value="'+dis.id+'"/></span></td>';
					tr = tr + '<td><span title="'+dis.id+'" style="width:45px;">'+dis.id+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_code+'" style="width:70px;">'+dis.sp_code+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_name+'" style="width:70px;">'+dis.sp_name+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_desc+'" style="width:35px;">'+dis.sp_desc+'</span></td>';
					tr = tr + '<td><span title="'+dis.firmDes+'" style="width:70px;">'+dis.firmDes+'</span></td>';
					tr = tr + '<td><span title="'+dis.unit+'" style="width:30px;">'+dis.unit+'</span></td>';
					tr = tr + '<td><span title="'+((undefined==dis.amount)?"":dis.amount.toFixed(2))+'" style="width:40px;text-align:right;">'+((undefined==dis.amount)?"":dis.amount.toFixed(2))+'</span></td>';
					tr = tr + '<td class="textInput"><span style="width:40px;"><input class="nextclass" title="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" type="text" style="width:40px;text-align:right;" value="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" id="amountin_'+dis.id+'" onfocus="this.select()" onblur="saveAmountin(this,'+dis.id+')" onkeyup="validate(this);sumTotalAmt(this,'+dis.id+')"/></span></td>';
					tr = tr + '<td class="textInput"><span style="width:40px;"><input type="hidden" id="h_pricein_'+dis.id+'" value="'+dis.pricein+'"/><input class="nextclass" id="pricein_'+dis.id+'" title="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" type="text" style="width:40px;text-align:right;" value="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" onfocus="this.select()" onblur="savePricein(this,'+dis.id+')" onkeyup="validate(this);sumTotalAmt(this,'+dis.id+')"/></span></td>';
					tr = tr + '<td class="textInput"><span style="width:50px;text-align:right;"><input class="nextclass" id="totalAmt_'+dis.id+'" title="'+(Number(dis.amountin)*Number(dis.pricein)).toFixed(2)+'" type="text" style="width:50px;text-align:right;" value="'+(Number(dis.amountin)*Number(dis.pricein)).toFixed(2)+'"" onfocus="this.select()" onblur="saveTotalPrice(this,'+dis.id+')" onkeyup="validate(this);sumPrice(this,'+dis.id+')"/></span></td>';
					tr = tr + '<td><span style="width:20px;text-align:center;" onclick="saveBak2('+dis.id+')"><input type="hidden" id="bak2_'+dis.id+'" name="bak2" value="'+dis.bak2+'"/><input type="hidden" name="h_id" value="'+dis.id+'"/><img id="img_'+dis.id+'" class="img_bak2" src="<%=path%>/image/kucun/'+dis.bak2+'.png"/></span></td>';
					tr = tr + '<td><span title="'+dis.unit1+'" style="width:30px;">'+dis.unit1+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount1+'" style="width:40px;text-align:right;">'+((undefined==dis.amount1)?"":dis.amount1.toFixed(2))+'</span></td>';
					tr = tr + '<td><span title="'+dis.amount1in+'" style="width:40px;text-align:right;">'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'</span></td>';
					tr = tr + '<td><span title="'+dis.inout+'" style="width:30px;">'+dis.inout+'</span></td>';
					tr = tr + '<td><span title="'+dis.cnt+'" style="width:40px;text-align:right;">'+dis.cnt+'</span></td>';
					tr = tr + '<td><span style="width:30px;text-align:center;">'+((dis.amount>dis.cnt)?'<img src="<%=path%>/image/kucun/Y.png"/>':'<img src="<%=path%>/image/kucun/N.png"/>')+'</span></td>';
					tr = tr + '<td class="textInput"><span title="'+dis.deliverCode+'" style="width:50px;">'+dis.deliverCode+'</span></td>';
					tr = tr + '<td><span title="'+dis.deliverDes+'" id="d_del_'+dis.id+'" style="width:100px;">'+dis.deliverDes+'</span></td>';
					tr = tr + '</tr>';	
					$(".grid .table-body tbody").append($(tr));
				}
				if(rs.over!='over'){
					condition['currPage']=++currPage;
					returnInfo = true;
				}
				if(parent.bhysEditState!='edit'){
					$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				}else{
					new tabTableInput("table-body","text"); //input  上下左右移动	
				}
			});
		}
		
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		}
		</script>
	</body>
</html>