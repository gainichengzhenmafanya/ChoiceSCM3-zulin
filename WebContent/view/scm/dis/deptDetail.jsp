
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="summary_of_reported_cargo"/>—部门明细</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.page{margin-bottom: 0px;}
		</style>
	</head>
	<body>
		<label><font color="blue"><fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/></font></label>
		<form action="<%=path%>/disFenBo/findTotal.do" id="listForm" name="listForm" method="post">
			<table id="test" style="width:'100%';" title="分店明细"  rownumbers="true" remoteSort="true"
				idField="SP_CODE">
				<thead>
					<tr>
					<!-- <th field="ck" checkbox="true"></th> -->
					<th field="SP_CODE" width="100px"><span id="SP_CODE"><fmt:message key ="supplies_code" /></span></th>
					<th field="SP_NAME" width="100px"><span id="SP_NAME"><fmt:message key ="supplies_name" /></span></th>
					<th field="SP_DESC" width="100px"><span id="SP_DESC"><fmt:message key ="supplies_specifications" /></span></th> 
					<th field="UNIT" width="100px"><span id="UNIT"><fmt:message key ="supplyunit" /></span></th> 
					<th field="PRICE" width="100px"><span id="PRICE"><fmt:message key ="price" /></span></th>
					<c:forEach var="deptdes" items="${columns}">
						<th field="F_${deptdes}" width="100px;"><span id="_${deptdes}"/>${deptdes}</th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			 <page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" /> 
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				});  
			    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			    $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),42);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('#test').datagrid({});
	  	 		var obj={};
				var rows = ${ListBean};
				obj['rows']=rows;
				$('#test').datagrid('loadData',obj);
			});
			//打印单据
			function printDis(){ 
				$("#wait2").val("NO");
				$('#listForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/disFenBo/printDisTotal.do';	
				var action1="<%=path%>/disFenBo/list.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
				$('#listForm').attr('action',action1);
				$('#listForm').attr('target','');
				$("#wait2").val("");
			};
		</script>
	</body>
</html>