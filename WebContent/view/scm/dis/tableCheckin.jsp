<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="acceptance_to_storage"/>验收入库</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 25px;}
		.search{
			margin-top:-2px;
			cursor: pointer;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<!-- 验收入库返回的信息 -->
		<input type="hidden" id="chkMsg" name="chkMsg" value="${chkMsg}"/>
		<form id="listForm" action="<%=path%>/disRuKu/listCheckin.do" method="post" target="">
			<div class="bj_head">
				<input type="hidden" id="select_positn" value="${dis.positnCode}"/>
	<!-- 			<input type="hidden" id="ysrkStr" name="ysrkStr"/>	 -->
				<div class="form-line">	
					<div class="form-label"><font style="vertical-align:middle;" color="red" size="3"><b>*</b></font><fmt:message key="position_select"/></div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
						<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key ="distribution_area" /></div>
					<div class="form-input">
						<select class="select" name="psarea">
							<option value=""></option>
							<c:forEach var="codeDes" items="${codeDesList }">
								<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
							</c:forEach>
						</select>
					</div>	
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>		
					<div class="form-input">
						<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
				        <font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>
					</div>	
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="supply_unit"/></div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
						<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label"><fmt:message key="note_type"/></div>
					<div class="form-input">
					     <select class="select" id="memo1" name="memo1">
					        		 <option value=""></option>
					                 <option
					                 <c:if test="${dis.memo1=='不合格' }">
					                 selected="selected"
					                 </c:if>
					                  value="不合格"><fmt:message key="unqualified"/></option>
					        	 </select>
					</div>
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>	
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key ="requisitioning_company" /></div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
						<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="category_selection"/></div>
					<div class="form-input" >
						<input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
						<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
					</div>
					<div class="form-label"><fmt:message key="document_date"/></div>
					<div class="form-input">
						 <input type="text" id="maded1" name="maded1" class="Wdate text" value="<fmt:formatDate value="${maded1}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
					</div>						
				</div>
			</div>
		    <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2" ><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" ><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" ><span style="width:80px;"><fmt:message key="requisitioning_company"/></span></td>
								<td colspan="5" ><fmt:message key="unit"/></td>
								<td colspan="3" ><fmt:message key="reference_unit"/></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2" ><span style="width:30px;"><fmt:message key="direction"/></span></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2" ><span style="width:80px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="whether_storage"/></span></td>
							</tr>
							<tr>
								<td ><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="actual"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="actual"/><br/><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
										<input type="hidden" name="idList" id="chk_${dis.id}" value="${dis.id}"/>									
									</td>
									<td title="${dis.id }"><span style="width:45px;">${dis.id }</span></td>
									<td title="${dis.sp_code }"><span style="width:65px;">${dis.sp_code }</span></td>
									<td title="${dis.sp_name }"><span style="width:100px;">${dis.sp_name }</span></td>
									<td title="${dis.sp_desc }"><span style="width:50px;">${dis.sp_desc }</span></td>
									<td title="${dis.firmDes }"><span style="width:80px;">${dis.firmDes }</span></td>
									<td title="${dis.unit }"><span style="width:30px;">${dis.unit }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>"><span style="width:40px;text-align:right"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.pricein*dis.amountin}" pattern="##.##" minFractionDigits="2" /></span></td>
									<td title="${dis.unit }"><span style="width:30px;">${dis.unit }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td title="${dis.memo1 }"><span style="width:40px;">${dis.memo1}</span></td>
									<td title="${dis.inout }"><span style="width:30px;">${dis.inout}</span></td>
									<td title="${dis.deliverCode }"><span style="width:40px;">${dis.deliverCode }</span></td>
									<td title="${dis.deliverDes }"><span style="width:80px;">${dis.deliverDes }</span></td>
									<td>
										<span style="width:50px;text-align:center">
											<c:choose>
												<c:when test="${dis.chkin=='Y' }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		   	//生成入库单时的状态
		   	chkMsg();
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 65: $('#autoId-button-102').click(); break;
	                case 80: $('#autoId-button-103').click(); break;
	            }
			}); 
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			setElementHeight('.table-body',['.table-head'],'.grid');
			var action = '${action}';
			if(action=='init'){
				loadToolBar([false]);
			}else{
				loadToolBar([true]);
			}
			function loadToolBar(use){
			  $('.tool').toolbar({
				items: [{
							text: '<fmt:message key="query_storage"/>',
							title: '<fmt:message key="query_storage"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								if(!$("#positnCode").val()){
									alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
									/* showMessage({
										type: 'error',
										msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
										speed: 1000
									}); */
								}else{
									$('#listForm').submit();									
								}
							}
						},{
							text: '<fmt:message key="acceptance_to_storage"/>',
							title: '<fmt:message key="acceptance_to_storage"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&& use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-20px']
							},
							handler: function(){
								if(!$("#positnCode").val()){
									alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
									/* showMessage({
										type: 'error',
										msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
										speed: 1000
									}); */
								}else{
									if($('#select_positn').val() != $('#positnCode').val()){
										if(confirm("<fmt:message key ="To_put_in_the_stock_position_and_the_position_is_not_consistent_with_the_position_of_the_query_determine_to_continue" />？")){
											if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_storage"/>？')){
												ChkstomYsrk();
											}
										}
									}else{
										if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_storage"/>？')){
											ChkstomYsrk();
										}
									}
								}
							}
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printYsrkChkstom();	
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
			});
			}
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('#seachDeliver').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#deliverCode').val();
						var defaultName = $('#deliverDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
					}
				});
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#typCode').val();
						var defaultName = $('#typDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					}
				});
				$('#seachSupply').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#sp_code').val();
						top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
					}
				});
				$('#seachOnePositn').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#positnCode').val();
						var defaultName = $('#positnDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
					}
				});
				$('#seachOnePositn1').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#firmCode').val();
						var defaultName = $('#firmDes').val();
						var offset = getOffset('positnCode');
						top.cust('<fmt:message key="please_select_branche"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
					}
				});
		});
		//生成入库单的状态
		function chkMsg(){
			var chkMsg=$("#chkMsg").val();
			if(Number(chkMsg==2)){
				alert('<fmt:message key="empty_data_cannot_to_store"/>！');
				var action="<%=path%>/disRuKu/listCheckin.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			if(Number(chkMsg==1)){
				alert('<fmt:message key="storage_completed_to_query_documents"/>！');
				var action="<%=path%>/disRuKu/listCheckin.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		//验收入库
		function ChkstomYsrk(){
// 			$('#ysrkStr').val('ysrk');//验收入库
// 			$('#listForm').submit();
			var action="<%=path%>/disRuKu/saveYsrkChkinm.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//打印
		function printYsrkChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disRuKu/viewYsrkChkstom.do';
			var action1="<%=path%>/disRuKu/listCheckin.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val('');//等待加载还原
     	}
		</script>
	</body>
</html>