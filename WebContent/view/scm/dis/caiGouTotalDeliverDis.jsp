<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="summary_of_reported_cargo"/>——采购汇总</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<style type="text/css">
		.page{margin-bottom: 0px;}
		</style>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/deliver/findCaiGouTotal.do" method="post">
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;"></span>&nbsp;</td>
								<td rowspan="2" ><span style="width:80px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" ><span style="width:150px;"><fmt:message key="supplies_name"/></span></td>
 								<td rowspan="2" ><span style="width:80px;"><fmt:message key="specification"/></span></td>
 								<td rowspan="2" ><span style="width:50px;"><fmt:message key="unit"/></span></td>
 								<td rowspan="2" ><span style="width:50px;"><fmt:message key="purchase_quantity"/></span></td>
 								<td rowspan="2" ><span style="width:150px;"><fmt:message key="order_time"/></span></td>
 								<td colspan="2" ><span style="width:100px;"><fmt:message key="remark"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList}" varStatus="status">
								<tr>
									<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
									<td ><span style="width:80px;">${dis.sp_code }</span></td>
									<td ><span style="width:150px;">${dis.sp_name }</span></td>
									<td ><span style="width:80px;">${dis.sp_desc }</span></td>
									<td ><span style="width:50px;">${dis.unit }</span></td>
									<td ><span style="width:50px;">${dis.amountin }</span></td>
									<td ><span style="width:150px;">${dis.chk1tim }</span></td>
									<td ><span style="width:100px;">${dis.memo }</span></td>									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				});  
			    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			    $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				setElementHeight('.table-body',['.table-head'],'.grid');
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
			});
			//打印单据
			function printDis(){ 
				$("#wait2").val("NO");
				$('#listForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/deliver/printDisTotal.do';	
				var action1="<%=path%>/deliver/deliverdis.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
				$('#listForm').attr('action',action1);
				$('#listForm').attr('target','');
				$("#wait2").val("");
			}
		</script>
	</body>
</html>