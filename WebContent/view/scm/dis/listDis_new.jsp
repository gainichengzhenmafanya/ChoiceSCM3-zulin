<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="reported_cargo_distribution"/>--报货分拨</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
			#grid{
				position: static;
				z-index: 1;
			}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
				text-align: center;
			}
			.textInput span{
				padding:0px;
			}
			.textInput input{
				border:0px;
				background: #F1F1F1;
			}
			.page{
				margin-bottom:25px;
			}
			.table-head td span{
				white-space: normal;
 			} 
/* 			.form-line .form-label{ 
				width: 12%; 
			} */
			.form-line .form-input{
 				width: 10%; 
 			}
			.form-line .form-input input[type=text]{ 
 				width: 97%; 
			}
			.form-line .form-input select{
 				width: 100%; 
 			}
 			.form-line {
				position: static;
				margin: 0px auto;
				+z-index:1;
				height: 25px;
				width:100%;	
			}
			
			.form-line .form-label,
			.form-line .form-input,
			.form-line .form-input-merge {
				position: static;
				float: left;
				vertical-align: middle;
				margin-right: 1px;
			}
		</style>	
	</head>
	<body>
		<div class="tool"></div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="accountName" name="accountName" value="${accountName }"/>
		<input type="hidden" id="selectIndSta" name="selectIndSta" value="NO"/>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<input type="hidden" id="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" />
		<form id="listForm" action="<%=path%>/disFenBo/list.do" method="post">
		<input type="hidden" name="orderBy" id="orderBy" value="${dis.orderBy }" />
		<div class="bj_head">
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="date" />：</div>
				<div class="form-input">
					<span><input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/></span>
		            <span><input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked"</c:if> id="ind1" name="ind1" value="ind"/></span>
		            <span><font style="color:blue;"><fmt:message key="arrival_date1"/></font></span>
				</div>
				<div class="form-label" style="width:65px;margin-left:55px;"><fmt:message key="positions"/>：</div>
				<div class="form-input">
					<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
					<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="supply_units"/>：</div>
				<div class="form-input">
					<input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="reported_goods_stores"/>：</div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
				
				<c:if test="${scm_project=='zdps' }">
				<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="shelf"/>：</div>
				<div class="form-input">
					<select id="positn1" name="positn1">
						<c:forEach items="${positn1s }" var="positnitem">
							<option value="${positnitem.des }">${positnitem.des }</option>
						</c:forEach>
					</select>
				</div>
				</c:if>
			</div>	
			<div class="form-line">	
				<div class="form-label" style="width: 6.1%;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${dis.sp_code }" />
					<img id="seachSupply" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label" style="width:180px;padding-left:50px;margin-top:0px;vertical-align:middle;text-align: right;margin-left:20px;">
					<font size="3">[</font><input type="radio" name="yndo" value="" checked="checked"/><fmt:message key="all"/>
					<input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key="scm_have_done"/>
					<input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key="scm_un_done"/><font size="3">]</font>
				</div>
				<div class="form-label" style="width:200px;padding-left:10px;margin-top:0px;vertical-align:middle;text-align: right;font-weight: bold;"><fmt:message key ="do_you_have" />    <span id="changeNum" style="color: red;">0</span>    <fmt:message key ="the_data_is_not_saved_there_are_a_total_of" /> <span id="changeNum" style="color: green;">${totalCount}</span> <fmt:message key ="article_data" /></div>
				<div class="form-input" style="margin-left: 50px;margin-top:8px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" >
					<div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div>
				</div><div id="per" class="form-input" style="width:50px;">0</div>
			</div>
			</div>
			<div class="search-div">
				<div class="search-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="supplies"/>：</td>
							<td>
								<input type="hidden" style="margin-top:0px;vertical-align:middle;" id="spCode" name="spCode" value="" />
								<input type="text" style="margin-top:0px;vertical-align:middle;" id="spName" name="spName" value="" />
								<img id="seachSupply1" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
							</td>
							<td class="c-left"><fmt:message key="quantity"/>：</td>
							<td><input type="text" id="num" name="num" onkeyup="validate(this);" class="text"/></td>
						</tr>
					</table>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="insert"/>'/>
				</div>
			</div>
			<div id="moreSelect" style="z-index:99;display:none;">
				<div class="search-condition">
					<div class="form-line">	
						<div class="form-label" style="width:200px;">
							<input type="checkbox" <c:if test="${chectim1=='checked'}"> checked="checked"</c:if> id="chectim1" name="chectim1" value="checked" onclick="checktim_btn(this)"/>
				            <font style="color:blue;"><fmt:message key="submit"/><fmt:message key="time"/>：</font>
				           	<input type="text" style="width:60px;margin-bottom:10px;" id="bchectim" name="bchectim" class="Wdate text" value="${dis.bchectim}" onfocus="new WdatePicker({dateFmt:'HH:mm:ss'})"/>-
				           	<input type="text" style="width:60px;margin-bottom:10px;" id="echectim" name="echectim" class="Wdate text" value="${dis.echectim}" onfocus="new WdatePicker({dateFmt:'HH:mm:ss'})"/>
						</div>
						<div class="form-label" style="margin-left:6px;"><fmt:message key="category_selection"/>：</div>
						<div class="form-input">
							<input type="text" id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
							<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
							<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
					    </div>
						<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="psarea"/>：</div>
						<div class="form-input">
							<select class="select" name="psarea" style="margin-top:1px;vertical-align:middle;">
								<option value=""></option>
								<c:forEach var="codeDes" items="${codeDesList }">
									<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="scm_line"/>：</div>
						<div class="form-input">
							<select class="select" name="lineCode" style="margin-top:1px;vertical-align:middle;">
								<option value=""></option>
								<c:forEach var="linesFirm" items="${linesFirmList }">
									<option <c:if test="${dis.lineCode==linesFirm.code }"> selected="selected" </c:if> value="${linesFirm.code }">${linesFirm.des }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><fmt:message key="reported_num"/></div>
						<div class="form-input"><input class="text" id="chkstoNo" name="chkstoNo" value="${dis.chkstoNo }"/></div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width: 6.1%;"><fmt:message key="abbreviation_code"/>：</div>
						<div class="form-input">
							<input type="text" value="${dis.sp_init}" id="sp_init" name="sp_init" style="text-transform:uppercase;" onkeyup="ajaxSearch()" onfocus="this.select()" />
						</div>
						<div  style="clear:both"></div>
						<div class="form-label" style="margin-left:0px;">
							&nbsp;&nbsp;
							<font size="3">[</font><input type="radio" name="inout" value="" checked="checked"/><fmt:message key="all"/>
						    <input type="radio" <c:if test="${dis.inout=='out' }"> checked="checked"</c:if> name="inout" value="out"/><fmt:message key="only_the_library"/>
						    <input type="radio" <c:if test="${dis.inout=='inout' }"> checked="checked"</c:if> name="inout" value="inout"/><fmt:message key="only_straight_hair"/>
						    <input type="radio" <c:if test="${dis.inout=='in' }"> checked="checked"</c:if> name="inout" value="in"/><fmt:message key="only_storage"/>
						    <input type="radio" <c:if test="${dis.inout=='dire' }"> checked="checked"</c:if> name="inout" value="dire"/><fmt:message key="only_straight_with"/><font size="3">]</font>
						    &nbsp;&nbsp;&nbsp;&nbsp;
						    <font size="3">[</font><input type="radio" name="ex1" value="" checked="checked"/><fmt:message key="all"/>
							<input type="radio" <c:if test="${dis.ex1=='Y' }"> checked="checked"</c:if>  name="ex1" value="Y"/><fmt:message key ="whether_semi" />
							<input type="radio" <c:if test="${dis.ex1=='N' }"> checked="checked"</c:if>  name="ex1" value="N"/><fmt:message key ="Non-semi-finished_product" /><font size="3">]</font>	
							 &nbsp;&nbsp;&nbsp;&nbsp;
						    <font size="3">[</font><input type="radio" name="ynArrival" value="" checked="checked"/><fmt:message key="all"/>
							<input type="radio" <c:if test="${dis.ynArrival=='1' }"> checked="checked"</c:if> name="ynArrival" value="1"/><fmt:message key="scm_arrival"/>
							<input type="radio" <c:if test="${dis.ynArrival=='2' }"> checked="checked"</c:if> name="ynArrival" value="2"/><fmt:message key="unknown_arrival"/><font size="3">]</font>								
						</div>
					</div>
					<div  style="clear:both"></div>
					<div class="form-line">
						<div class="form-label" style="margin-left:0px;">
							&nbsp;&nbsp;
							<font size="3">[</font>
							<input type="radio" name="chk1condition" checked="checked" value="0"/><fmt:message key="all"/>
						    <input type="radio" <c:if test="${dis.chk1condition=='1' }"> checked="checked"</c:if> name="chk1condition" value="1"/><fmt:message key="procurement_confirm"/>
						    <input type="radio" <c:if test="${dis.chk1condition=='2' }"> checked="checked"</c:if> name="chk1condition" value="2"/><fmt:message key="procurement_audit"/>
						    <input type="radio" <c:if test="${dis.chk1condition=='3' }"> checked="checked"</c:if> name="chk1condition" value="3"/><fmt:message key="no_wei"/><fmt:message key="procurement_confirm"/>
						    <input type="radio" <c:if test="${dis.chk1condition=='4' }"> checked="checked"</c:if> name="chk1condition" value="4"/><fmt:message key="no_wei"/><fmt:message key="procurement_audit"/>
						    <input type="radio" <c:if test="${dis.chk1condition=='5' }"> checked="checked"</c:if> name="chk1condition" value="5"/><fmt:message key="suo_you"/><fmt:message key="scm_un_done"/>
						    <font size="3">]</font>							
						</div>
					</div>
					
				</div>
			</div>
			<div id="grid" class="grid">		
				<div class="table-head" id="grid-head-div">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>	
								<td rowspan="2"><span style="width:25px;text-align: center;">&nbsp;</span></td>							
								<td rowspan="2" title="<fmt:message key="serial_number"/>"><span style="width:60px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_code"/>"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_name"/>"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
 								<td rowspan="2" title="<fmt:message key="specification"/>"><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" title="<fmt:message key="suppliers"/>"><span style="width:60px;"><fmt:message key="suppliers"/></span></td>
 								<td rowspan="2" title="<fmt:message key="suppliers_name"/>"><span style="width:120px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2" title="<fmt:message key="daily_cargo_space"/>"><span style="width:120px;"><fmt:message key="daily_cargo_space"/></span></td>
								<td colspan="3" title="<fmt:message key="procurement_unit"/>"><span style="width:105px;"><fmt:message key="procurement_unit"/></span></td><!-- 采购单位 -->
								<td colspan="5" title="<fmt:message key="standard_unit"/>"><span style="width:185px;"><fmt:message key="standard_unit"/></span></td><!-- 标准单位 -->
								<td colspan="2" title="<fmt:message key="the_reference_unit"/>"><span style="width:85px;"><fmt:message key="the_reference_unit"/></span></td><!-- 参考单位 -->
								<td rowspan="2" title="<fmt:message key="direction"/>"><span style="width:60px;"><fmt:message key="direction"/></span></td>
 								<td rowspan="2" title="<fmt:message key="arrival_date"/>"><span style="width:70px;"><fmt:message key="arrival_date"/></span></td>
 								<td rowspan="2" title="<fmt:message key="inventory"/>"><span style="width:60px;"><fmt:message key="inventory"/></span></td> 								
 								<td rowspan="2" title="<fmt:message key="which_procurement_indicating"/>"><span style="width:60px;"><fmt:message key="which_procurement_indicating"/></span></td>
 								<td rowspan="2" title="<fmt:message key="orderer"/>"><span style="width:60px;"><fmt:message key="orderer"/></span></td>
 								<td rowspan="2" title="<fmt:message key="confirm_the_time"/>"><span style="width:120px;"><fmt:message key="confirm_the_time"/></span></td>
 								<td rowspan="2" title="<fmt:message key="the_unknown_arrives"/>"><span style="width:60px;"><fmt:message key="the_unknown_arrives"/></span></td>
 								<td rowspan="2" title="<fmt:message key="purchase_audit"/>"><span style="width:60px;"><fmt:message key="purchase_audit"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="remark"/>"><span style='width:${scm_project=="dttc"?"300px":"70px"};'><fmt:message key="remark"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="production_date"/>"><span style="width:70px;"><fmt:message key="production_date"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="pc_no"/>"><span style="width:60px;"><fmt:message key="pc_no"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="reported_num"/>"><span style="width:70px;"><fmt:message key="reported_num"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="additional_items"/>"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="suppliers"/><fmt:message key="confirm"/>"><span style="width:60px;"><fmt:message key="suppliers"/><fmt:message key="deliver_goods"/><br/><fmt:message key="confirm"/></span></td>
 								<c:if test="${isOrNOtRelationCG=='1' }">
	 								<td rowspan="2"><span style="width:60px;">订单制单人</span></td>
	 								<td rowspan="2"><span style="width:120px;">订单制单时间</span></td>
	 								<td rowspan="2"><span style="width:50px;">是否已生成</span></td>
 								</c:if>
							</tr>
							<tr>
								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
								<td title="<fmt:message key="the_number_of_the_reported_goods"/>"><span style="width:60px;"><fmt:message key="the_number_of_the_reported_goods"/></span></td>
								<td title="<fmt:message key="adjustment_amount"/>"><span style="width:60px;"><fmt:message key="adjustment_amount"/></span></td>
								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
								<td title="<fmt:message key="number_of_standards"/>"><span style="width:60px;"><fmt:message key="number_of_standards"/></span></td>
 								<td title="<fmt:message key="offer"/><fmt:message key="unit_price"/>"><span style="width:60px;"><fmt:message key="offer"/><fmt:message key="unit_price"/></span></td>
 								<td title="<fmt:message key="scm_sale_price"/><fmt:message key="unit_price"/>"><span style="width:60px;"><fmt:message key="scm_sale_price"/><fmt:message key="unit_price"/></span></td>
 								<td title="<fmt:message key="amount"/>"><span style="width:60px;"><fmt:message key="amount"/></span></td>
 								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
 								<td title="<fmt:message key="reference_number"/>"><span style="width:60px;"><fmt:message key="reference_number"/></span></td>
							</tr>							
						</thead>
					</table>
				</div>
				<div class="table-body" id="grid-body-div" style="overflow: auto;">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="dis" items="${disesList1}" varStatus="status">
								<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:25px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.id }" style="width:60px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:70px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:70px;">${dis.sp_desc }</span></td>
									<c:choose>
										<c:when test="${dis.inout == 'out' }">
											<td>
												<span style="width:60px;" title="${dis.deliverCode }">${dis.deliverCode}</span>
											</td>
										</c:when>
										<c:otherwise>
											<td class="textInput">
												<span style="width:60px;">
													<input title="${dis.deliverCode }" type="text" id="c_delv_${dis.id}" value="${dis.deliverCode}" onfocus="this.select()" onblur="findByDeliver(this,'${dis.id}')" onkeydown="searchDeliver(this,'${dis.id}')"/>
												</span>
											</td>
										</c:otherwise>
									</c:choose>
									<td><span title="${dis.deliverDes}" id="d_del_${dis.id }" style="width:120px;">${dis.deliverDes }</span></td>
									<td><span title="${dis.firmDes}" style="width:120px;"><input type="hidden" value="${dis.firmCode }"/>${dis.firmDes }</span></td>
									<td>
										<span title="${dis.unit3 }" style="width:60px;">${dis.unit3 }</span>
									</td>
									<td>
										<span title="${dis.amount1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span>
									</td>
									<td>
										<span style="width:60px;">
											<input class="nextclass"  title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1sto(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td>
										<span title="${dis.unit }" style="width:60px;">${dis.unit }</span>
									</td>
									<td>
										<span style="width:60px;">
											<input class="nextclass"  title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<c:choose>
										<c:when test="${dis.ynprice == 'N'}"><!-- 如果没有取到报价，价格可以修改 -->
											<td class="textInput">
												<span style="width:60px;">
													<input class="nextclass" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onchange="checkPrice(this,'14')" onkeyup="validate(this)"/>
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${!empty accountDis && accountDis.ynzp == 'Y'}"><!-- 如果有权限修改报价 -->
													<td class="textInput">
														<span style="width:60px;">
															<input class="nextclass" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onchange="checkPrice(this,'14')" onkeyup="validate(this)"/>
														</span>
													</td>
												</c:when>
												<c:otherwise>
													<td><span style="width:60px;text-align:right;" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									<td class="textInput">
										<span style="width:60px;">
											<input class="nextclass" title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricesale(this,'${dis.id}')" onkeyup="validate(this)" onchange="checkPrice(this,'15','${dis.id}')"/>
										</span>
									</td>
									<td class="textInput">
										<span style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.pricein * dis.amountin}" type="currency" pattern="0.00"/></span>
									</td>
									<td>
										<span title="${dis.unit1 }" style="width:60px;">${dis.unit1 }</span>
									</td>
									<td>
										<span style="width:60px;">
											<input class="nextclass"  title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1in(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td>
										<span title="${dis.inout}" style="width:60px;display: none;">${dis.inout}</span>
										<span title="<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
											<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
											<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
											<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>" style="width:60px;">
											<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
											<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
											<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
											<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>
										</span>
									</td>
									<td class="textInput">
										<span style="width:70px;">
											<input title="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" type="text" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>"  onfocus="this.select()" onblur="saveInd(this,'${dis.id}')" onclick="selectInd(this,1)" onkeypress="selectInd(this)"/>
										</span>
									</td>
									<td><span title="${dis.cnt}" style="width:60px;text-align: right;">${dis.cnt}</span></td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.chk1}">
											<input type="hidden" id="qr_${dis.id}" value="${dis.chk1 }"/>
											<img src="<%=path%>/image/kucun/${dis.chk1}.png"/>
										</span>
									</td>
									<td><span title="${dis.chk1emp }" style="width:60px;">${dis.chk1emp }</span></td>
									<td><span title="${dis.chk1tim }" style="width:120px;">${dis.chk1tim }</span></td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.chksend}">
										<img src="<%=path%>/image/kucun/${dis.chksend}.png"/>
										</span>
									</td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.sta}"><!-- 用来判断是否采购审核 2014.12.5wjf -->
										<img src="<%=path%>/image/kucun/${dis.sta}.png"/>
										</span>
									</td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style='width:${scm_project=="dttc"?"300px":"70px"};' title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style='width:${scm_project=="dttc"?"300px":"70px"};' title="${dis.memo}">${dis.memo}</span></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${dis.sp_per1 != '0' and (dis.inout == 'in' or dis.inout == 'inout')}"><!-- 必须是直发或者入库方向的，并且设置有效期的才输有效期 -->
											<td class="textInput">
												<span style="width:70px;">
													<input title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>" type="text" class="Wdate text" value="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"  onfocus="this.select()" onblur="saveDued(this,'${dis.id}')" onkeypress="selectDued(this)"/>
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${dis.inout == 'in' or dis.inout == 'inout'}">
											<td class="textInput">
												<span style="width:60px;">
													<input title="${dis.pcno}" type="text" value="${dis.pcno}" onfocus="this.select()" onblur="savePcno(this,'${dis.id}')" />
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<td><span style="width:60px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td><span style="width:70px;">${dis.chkstoNo }</span></td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.deliveryn}">
										<img src="<%=path%>/image/kucun/${dis.deliveryn}.png"/>
										</span>
									</td>
									<c:if test="${isOrNOtRelationCG=='1' }">
										<td><span title="${dis.puprorderemp}" style="width:60px;">${dis.puprorderemp}</span></td>
										<td><span title="${dis.puprordertim}" style="width:120px;">${dis.puprordertim}</span></td>
										<td>
											<span style="width:50px;text-align:center;" title="${dis.ispuprorder}">
											<img src="<%=path%>/image/kucun/${dis.ispuprorder}.png"/>
											</span>
										</td>
									</c:if>
								</tr>
							</c:forEach>	
						</tbody>
					</table>
				</div>	
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
		var isCanUse = true;//是否采购确认 那些按钮可用 2014.12.5wjf
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		$(document).ready(function(){
			//遍历tr，判断如果是直配方向售价显示报价
			$('.grid').find('.table-body').find('tr').each(function(i){
				var fx = $(this).find('td:eq(19)').find('span:eq(0)').attr('title');
				if(fx == 'dire'){
					$(this).find('td:eq(15)').find('input').val($(this).find('td:eq(14)').find('input').val());
				}
			});
			//自动实现滚动条--放到最后加载了
			//setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			//setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			//loadGrid();//  自动计算滚动条的js方法		
			//changeTh();
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('#bchectim').attr('disabled','disabled');
			$('#echectim').attr('disabled','disabled');
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.keyCode==32){return false;}//屏蔽空格
		 		if(e.altKey == false) return;
	 			switch (e.keyCode){
			                case 70: $('#autoId-button-101').click(); break;
			                case 69: $('#autoId-button-102').click(); break;
			                case 83: updateDis(); break;
			                case 65: $('#autoId-button-104').click(); break;
			                case 68: printDis("printInspection.do"); break;
			                case 67: $('#autoId-button-106').click(); break;
			                case 88: changeFocus();break;//Alt+X
		            	}
			});
			
			//编辑采购数量和单价时，按回车可以跳到下一行的同一列
			$('tbody .nextclass').live('keydown',function(e){
				if(parent.bhfbEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).parent().parent().prevAll().length;
						var hang= $(this).parent().parent().parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
						return;
			 		}
				}
			});
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			
			$("#seachOnePositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positnCode").val(),
					single:true,
					tagName:'positnDes',
					tagId:'positnCode'
				});
			});
			
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positnCode').val();
					var defaultName = $('#positnDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachSupply1').bind('click.custom',function(e){
				if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
					if(!!!top.customWindow){
						var defaultCode = $('.grid').find('.table-body').find(':checkbox').filter(':checked').parents('tr').find('td:eq(3)').find('span').attr('title');
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_materials"/>',encodeURI('<%=path%>/supply/findAllSupplySXS.do?defaultCode='+defaultCode),offset,$('#spName'),$('#spCode'),'560','420','isNull');
					}
				}else {
					$('.search-div').hide();
					alert("<fmt:message key='select_a_need_to_adjust_the_material_data'/>！");
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('positnDes');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
			/*********************延迟加载相关***************************/
			if('${action}'!='init' && '${currState}'!='1'){
				addTr('first');	
			}else if('${action}'!='init' && '${currState==1}'){
				$("#per").text('100%');
				$("#currState").width(200);
			}
			pageSize = '${pageSize}';
			var ndivHeight = $(".table-body").height();
			$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		          });
			
			if('${action}'=='init'){
				parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.trlist=undefined;//将trlist清空
				parent.changeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#changeNum").text(parent.changeNum);
			}
			
			//判断如果不是编辑状态
			if(parent.bhfbEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				loadToolBar([true,true,false,true,true,true]);
			}else{
				loadToolBar([true,false,true,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',false);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
			
			$('.grid').find('.table-head').find('td:eq(2)').each(function(){
				$(this).bind('click',function(){//按序号排序
					if($('#orderBy').val()=="c.id desc"){
						$('#orderBy').val("c.id asc");
					}else{
						$('#orderBy').val("c.id desc");
					}
					$('#listForm').submit();
				});
			});
			
			//根据物资编码排序
			$('.grid').find('.table-head').find('td:eq(3)').each(function(){
				$(this).bind('click',function(){//物资编码排序
					if($('#orderBy').val()=="c.sp_code desc"){
						$('#orderBy').val("c.sp_code asc");
					}else{
						$('#orderBy').val("c.sp_code desc");
					}
					$('#listForm').submit();
				});
			});			
			
			if($('#chectim1').get(0).checked){
				$("#bchectim").removeAttr("disabled","disabled");
				$("#echectim").removeAttr("disabled","disabled");
			}
			if($('#maded').val()==''){
				$('#maded').val($('#ind').val());
			}
			
			//如果选的是已处理或全部   则很多按钮不能用 2014.10.21wjf
			var val = $('input[name=yndo]:checked').val();
			if(val == "NO"){
				isCanUse = true;
				loadToolBar([true,true,false,true,true,true]);
			}else{
				isCanUse = false;
				loadToolBar([true,false,false,true,true,true]);
			}
			
			//add by jin shuai at 20160309
			//初始进入页面或者没查询到数据时出现滚动条
			var headdivwidth = document.getElementById('grid-head-div').scrollWidth;
			$('#grid-head-div').width(headdivwidth);
			$('#grid-body-div').width(headdivwidth);
			//标题栏颜色修改
			document.getElementById('grid-head-div').style.background='#DCEFFE url("<%=path%>/image/Button_new/table_head2.png") repeat-x bottom';

			//如果是洞庭项目，默认显示高级查询条件
			if("${scm_project}"=="dttc"){
				//洞庭项目，默认显示高级查询
				$('#moreSelect').slideToggle(100);
			}
			
			//自动实现滚动条--最后计算
			setElementHeight('.grid',['.tool'],$(document.body),${scm_project=="dttc"?"150":"110"});	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
			
			//绑定行点击事件背景颜色改变
			bindTrClick();
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null!=$('#maded').val() && ""!=$('#maded').val()){
								//如果編輯状态下 点查询要有提示  2014.12.11wjf
								if(parent.bhfbEditState=='edit'){
									if(confirm('<fmt:message key="you_are_editing_state"/>，<fmt:message key="there_may_have_been_saved_data"/>，<fmt:message key="whether"/><fmt:message key="continue"/>？')){
										parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
										$("#listForm").submit();
									}
								}else{
									parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
									$("#listForm").submit();
								}
 							}else{
 								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
 							}
						},
						items:[{
							text: '<fmt:message key="advanced_search"/>',
							title: '<fmt:message key="advanced_search"/>',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								$('#moreSelect').slideToggle(100);
							}
						}]
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="edit"/>',
						useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if($('.grid').find('.table-body').find("tr").size()<1){
								alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							}else{
								parent.bhfbEditState='edit';
								loadToolBar([true,false,true,false,false,false]);
								//遍历tr，判断如果是非采购确认，采购审核的才可以修改2014.12.5wjf
								//$('.grid').find('.table-body').find('tr').find('input[type="text"]').attr('disabled',false);
 								$('.grid').find('.table-body').find('tr').each(function(i){
 									var qr = $(this).find('td:eq(22)').find('span').find('input').val();//采购确认
 									var sh = $(this).find('td:eq(26)').find('span').attr('title');//采购审核
 									if(qr != 'Y' && sh != 'Y'){//如果不是采购确认和采购审核的才能修改
 										$(this).find('input[type="text"]').attr('disabled',false);
 									}
 								});
								new tabTableInput("table-body","text"); //input  上下左右移动
								if($('tbody tr:first .nextclass').length!=0){
									$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
								}
							}
						},
						items:[{
							text: '<fmt:message key="scm_split"/>',
							title: '<fmt:message key="scm_split"/>',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								dispatchDis();
							}
						},{
							text: '<fmt:message key="adjust_material"/>',
							title: '<fmt:message key="adjust_material"/>',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
									$('.search-div').slideToggle(100);
								}else {
									alert("<fmt:message key='select_a_need_to_adjust_the_material_data'/>！");
								}
							}
						}]
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save"/>',
						useable: use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateDis();
						}
					},{
						text: '<fmt:message key="the_procurement_process"/>',
						title: '<fmt:message key="the_procurement_process"/>',
						useable: isCanUse,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						items:[{
							text: '<fmt:message key="purchase_confirmation"/>',
							title: '<fmt:message key="purchase_confirmation"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('chk1','Y');
							}
						},{
							text: '<fmt:message key="cancel_confirmation"/>',
							title: '<fmt:message key="cancel_confirmation"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('chk1','N');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="purchase_confirmation"/>',
							title: '<fmt:message key="all"/><fmt:message key="purchase_confirmation"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('confirm');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="cancel_confirmation"/>',
							title: '<fmt:message key="all"/><fmt:message key="cancel_confirmation"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelConfirm');
							}
						},{
							text: '<fmt:message key="purchase_check"/>',
							title: '<fmt:message key="purchase_check"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('sta','Y');
							}
						},{
							text: '<fmt:message key="cancel_check"/>',
							title: '<fmt:message key="cancel_check"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('sta','N');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="purchase_check"/>',
							title: '<fmt:message key="all"/><fmt:message key="purchase_check"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('buyCheck');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="cancel_check"/>',
							title: '<fmt:message key="all"/><fmt:message key="cancel_check"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelBuyCheck');
							}
						},{
							text: '<fmt:message key="scm_the_unknown"/>',
							title: '<fmt:message key="scm_the_unknown"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['20px','20px']
							},
							handler: function(){
								fucEnter('chksend','Y');
							}
						},{
							text: '<fmt:message key="scm_the_known"/>',
							title: '<fmt:message key="scm_the_known"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('chksend','N');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="scm_the_unknown"/>',
							title: '<fmt:message key="all"/><fmt:message key="scm_the_unknown"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('unKnownArrival');
							}
						},{
							text: '<fmt:message key="all"/><fmt:message key="scm_the_known"/>',
							title: '<fmt:message key="all"/><fmt:message key="scm_the_known"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif', 
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('cancelUnKnownArrival');
							}
						},{
							text: '<fmt:message key="Update_lastest_price"/>',
							title: '<fmt:message key="Update_lastest_price"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif', 
								position: ['0px','0px']
							},
							handler: function(){
								fucEnter('price','Y');
							}
						},{
							text: '<fmt:message key="All_update_lastest_price"/>',
							title: '<fmt:message key="All_update_lastest_price"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif', 
								position: ['0px','0px']
							},
							handler: function(){
								fucEnterAll('price');
							}
						}
						<c:if test="${isOrNOtRelationCG=='1'}">
							,{
								text: '<fmt:message key="insert_order"/>',
								title: '<fmt:message key="insert_order"/>',
								useable: isCanUse,
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','0px']
								},
								handler: function(){
									toSavePuprOrder();
								}
							}
						</c:if>]
					},{
						text: '<fmt:message key="scm_print_or_export"/>',
						title: '<fmt:message key="scm_print_or_export"/>',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						items:[{
							text: 'Excel',
							title: 'Excel',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-20px']
							},
							handler: function(){
//	 							fucEnter('excel','');
								exportExcel();
							}
						},{
							text: '<fmt:message key="export_distribution_list"/>',
							title: '<fmt:message key="export_distribution_list"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									excelDis('');
								}
							}
						},{
							text: '<fmt:message key="export_distribution_list"/>2',
							title: '<fmt:message key="export_distribution_list"/>2',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									excelDis(2);
								}
							}
						},{
							text: '<fmt:message key="export_distribution_list"/>3',
							title: '<fmt:message key="export_distribution_list"/>3',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									excelDis(3);
								}
							}
						},{
							text: '<fmt:message key="export_distribution_list"/>4',
							title: '<fmt:message key="export_distribution_list"/>4',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									excelDis(4);
								}
							}
						},{
							text: '<fmt:message key="print_distribution_single"/>(<u>A</u>)',
							title: '<fmt:message key="print_distribution_single"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									var checkboxList = $('.grid').find('.table-body').find('tr');
									var flag = false;
									if(checkboxList){
										checkboxList.each(function(i){
		 									var qr = $(this).find('td:eq(22)').find('span').find('input').val();//采购确认
		 									var sh = $(this).find('td:eq(26)').find('span').attr('title');//采购审核
		 									if(qr == 'Y' && sh == 'Y'){//如果是采购确认和采购审核的才能进行打印
		 										flag = true;
		 										return false;
		 									}
		 								});
									}
									if(flag){
										printDis("printIndent.do");	
									}else{
										alert('<fmt:message key="there_is_no"/><fmt:message key="purchase_confirmation"/>，采购审核的物资可打印！');
									}
								}
							}
						},{
							text: '<fmt:message key="print_distribution_single"/>(<u>A2</u>)',
							title: '<fmt:message key="print_distribution_single"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									var checkboxList = $('.grid').find('.table-body').find('tr');
									var flag = false;
									if(checkboxList){
										checkboxList.each(function(i){
		 									var qr = $(this).find('td:eq(22)').find('span').find('input').val();//采购确认
		 									var sh = $(this).find('td:eq(26)').find('span').attr('title');//采购审核
		 									if(qr == 'Y' && sh == 'Y'){//如果是采购确认和采购审核的才能进行打印
		 										flag = true;
		 										return false;
		 									}
		 								});
									}
									if(flag){
										printDis("printIndent2.do");	
									}else{
										alert('<fmt:message key="there_is_no"/><fmt:message key="purchase_confirmation"/>，采购审核的物资可打印！');
									}
								}
							}
						},{
							text: '<fmt:message key="print_inspection_single"/>(<u>D</u>)',
							title: '<fmt:message key="print_inspection_single"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									printDis("printInspection.do");
								}
							}
						},{
							text: '<fmt:message key="print_delivery_note"/>(<u>C</u>)',
							title: '<fmt:message key="print_delivery_note"/>',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								if($('.grid').find('.table-body').find("tr").size()<1){
									alert('<fmt:message key="no_data_no_print"/>！！');
								}else{
									printDis("printDelivery.do");
								}
							}
						},{
							text: '<fmt:message key="procurement_summary"/>',
							title: '<fmt:message key="procurement_summary"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								cgtotal();
							}
						},{
							text: '<fmt:message key="department_of_details"/>',
							title: '<fmt:message key="department_of_details"/>',
							useable: isCanUse,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								deptDetail2();
							}
						}]
					},{
						text: '<fmt:message key="scm_send_mail"/>',
						title: '<fmt:message key="scm_send_mail"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					    icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					    },
						handler: function(){ 
							var deliverCode = $('#deliverCode').val();
							if(null != deliverCode && "" != deliverCode){
								var data = {};
								$("#listForm *[name]").each(function(){
									var name = $(this).attr("name"); 
									if(name)data[name] = $(this).val();
									if(name == 'ind1'){
										if($(this).is(':checked') || $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'ex1'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'yndo'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'inout'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}else if(name == 'ynArrival'){
										if($(this).is(':checked')|| $(this).attr('checked')){
											data[name] = $(this).val();
										}else{
											data[name] = '';
										}
									}
								});
								var url = "<%=path%>/disFenBo/sendEmail.do";
								$.ajax({
									url: url,
									data: data,
									type: 'post',
									success:function(msg){
										if(msg == 'OK'){
											alert("<fmt:message key='scm_succ_sent'/>!");
										}else if(msg == 'NSH'){
											alert('<fmt:message key="scm_fail_sent"/>！当前日期下此供应商的物资都未审核！请先审核。');
										}else{
											alert("<fmt:message key='scm_fail_sent'/>!");
										}
									}
								});
							}else{
								alert('<fmt:message key="please_select"/><fmt:message key="suppliers"/>！');
							}
						}
					},<%-- ,{
						text: '导出采购订单',
						title: '导出采购订单',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					    icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					    },
						handler: function(){
							if(null!=$('#deliverCode').val() && ""!=$('#deliverCode').val()){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('action','<%=path%>/disFenBo/exportCaiGou.do');
								$('#listForm').submit();
								$("#wait2 span").html("数据导出中，请稍后...");
								$('#listForm').attr('action','<%=path%>/disFenBo/list.do');	
								$("#wait2").val('');//等待加载还原
							}else{
								alert("请选择供应商");
							}
						}
					}, --%>{
						text: '<fmt:message key="quit"/>',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){							
							if(parent.bhfbEditState=="edit"){
								if(confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？')){
									invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
								}
							}else{
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					}]
			});
		}
		
		$("#search").bind('click', function() {
			$('.search-div').hide();
			dispatchX();
		});
		
		//虚拟报货时候选择另一种实际物料
		function dispatchX(){
			
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() == 1){
				if(confirm('<fmt:message key ="the_material_purchase_confirmation" />？')){
					if($('#num').val()==null||$('#num').val()==''){
						alert("<fmt:message key ="please_fill_in_the_number_of_adjustment" />！");
						$('.search-div').slideToggle(100);
					}else{
						checkboxList.filter(':checked').each(function(i){
							selected['id'] =  $(this).val();
							selected['supply.sp_code'] = $('#spCode').val();
							selected['amountin'] = Number($('#num').val());
// 							selected['amount'] = Number($('#num').val());
							selected['amount'] = $(this).parents('tr').find('td:eq(10)').find('span').attr('title');
							selected['memo'] = $(this).parents('tr').find('td:eq(22)').find('span').attr('title');
							selected['hoped'] = $(this).parents('tr').find('td:eq(15)').find('span').find('input').val();
						});
						$.post('<%=path%>/disFenBo/updateDispatchX.do',selected,function(data){
							showMessage({//弹出提示信息
								type: 'success',
								msg: '<fmt:message key="operation_successful" />！',
								speed: 1000
								});	
							});
					}
				}
			}else{
				alert('<fmt:message key="please_select_a_need_to_adjust_the_data"/>！');
				return ;
			}
		}
		
		//拆分
		function dispatchDis(){
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var count=0;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="that_split_the_selected_data"/>？')){
					var flag = true;//用来判断是否已经采购确认或采购审核 2014.12.5wjf
					var i = 0;
					checkboxList.filter(':checked').each(function(){
						var qr = $(this).parents('tr').find('td:eq(22)').find('span').find('input').val();//采购确认
						var sh = $(this).parents('tr').find('td:eq(26)').find('span').attr('title');//采购审核
						if(qr == 'Y' || sh == 'Y'){
							flag = false;
							return false;
						}
						var amount1 = Number($(this).parents('tr').find('td:eq(10)').find('span').attr('title'));
						var amount1sto = Number($(this).parents('tr').find('td:eq(11)').find('span').find('input').val());
						var sp_name = $(this).parents('tr').find('td:eq(4)').find('span').attr('title');
						if(amount1sto >= amount1){
							alert('<fmt:message key="The_split_supplies" />:['+sp_name+']<fmt:message key="Adjustment_must_be_less_than_the_number_of_reported_goods_Has_filtered_the_material" />！');
							return true;
						}
						selected['chkstodList['+i+'].id'] =  $(this).val();
						selected['chkstodList['+i+'].supply.sp_code'] = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
						selected['chkstodList['+i+'].amount1sto'] = amount1sto;
						selected['chkstodList['+i+'].amount1'] = amount1;
						selected['chkstodList['+i+'].supply.sp_price'] = $(this).parents('tr').find('td:eq(14)').find('span').find('input').val();
						selected['chkstodList['+i+'].memo'] = $(this).parents('tr').find('td:eq(27)').find('span').attr('title');
						selected['chkstodList['+i+'].hoped'] = $(this).parents('tr').find('td:eq(20)').find('span').find('input').val();
						i++;
					});
					if(!flag){
						alert('<fmt:message key="confirmation_purchase_audit_materials_cannot_be_split"/>！');
						return;
					}
					$.post('<%=path%>/disFenBo/updateDispatch.do',selected,function(data){
						showMessage({//弹出提示信息
							type: 'success',
							msg: '<fmt:message key="operation_successful" />！',
							speed: 1000
							});	
						$('#listForm').submit();
					});
				}
			}else{
				alert('<fmt:message key="please_select_the_need_to_split_the_data"/>！');
				return ;
			}
		}
		//采购汇总
		function cgtotal(){
// 			if (confirm("要对所选数据进行采购汇总操作吗？")==true) {
// 				if(null!=$('#maded').val() && ""!=$('#maded').val()){
// 					$('body').window({
// 						id: 'window_cgtotal',
// 						title: '采购汇总',
<%-- 						content: '<iframe id="cgtotalFrame" name="cgtotalFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findCaiGouTotal.do?maded="+$('#maded').val()+'"></iframe>', --%>
// 						width: '1000px',
// 						height: '480px',
// 						draggable: true,
// 						isModal: true,
// 						topBar: {
// 							items: [{
// 										text: '<fmt:message key="print" />',
// 										title: '<fmt:message key="print"/>',
// 										handler: function(){
											$("#wait2").val("NO");
											$('#listForm').attr('target','report');
											window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
											var action = '<%=path%>/disFenBo/printDisTotal.do';	
											var action1="<%=path%>/disFenBo/list.do";
											$('#listForm').attr('action',action);
											$('#listForm').submit();
											$('#listForm').attr('action',action1);
											$('#listForm').attr('target','');
											$("#wait2").val("");
// 											window.frames["cgtotalFrame"].printDis();
// 										}
// 									},{
// 										text: '<fmt:message key="quit"/>(ESC)',
// 										title: '<fmt:message key="quit"/>',
// 										icon: {
<%-- 											url: '<%=path%>/image/Button/op_owner.gif', --%>
// 											position: ['-160px','-100px']
// 										},
// 										handler: function(){
// 											$('.close').click();
// 										}
// 									}]
// 								}
// 						});
// 				}else{
// 					alert('<fmt:message key="please_select_the_collection_date"/>！');
// 				}
// 			}
		}
		//配送汇总
		function pstotal(){
			if(null!=$('#maded').val() && ""!=$('#maded').val()){
				$('body').window({
					id: 'window_pstotal',
					title: '<fmt:message key ="Distribution_summary" />',
					content: '<iframe id="pstotalFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findPeiSongTotal.do?maded="+$('#maded').val()+'"></iframe>',
					width: '1000px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
									text: '<fmt:message key="print" />',
									title: '<fmt:message key="print"/>',
									handler: function()
									{
										window.frames["pstotalFrame"].printDis();
									}
								},{
									text: '<fmt:message key="quit"/>(ESC)',
									title: '<fmt:message key="quit"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
							}
					});
			}else{
				alert('<fmt:message key="please_select_the_collection_date"/>！');
			}
		}
		//部门明细
		function deptDetail(){
			var maded = $('#maded').val();
		    var positnCode = $("#positnCode").val();
			var deliverCode = $('#deliverCode').val();
			var typCode = $("#typCode").val(); 
			var firmCode = $('#firmCode').val();
			var sp_code = $('#sp_code').val();
			var ex1 = $("input[name='ex1']:checked").val();
			var yndo = $("input[name='yndo']:checked").val();
			var sp_init = $("#sp_init").val();
		    var inout = $("input[name='inout']:checked").val();
			var chksend = $("input[name='ynArrival']:checked").val();
			if(null!=$('#maded').val() && ""!=$('#maded').val()){
				if(null!=$('#deliverCode').val() && ""!=$('#deliverCode').val()){
					if(null!=$('#firmCode').val() && ""!=$('#firmCode').val()){
						if($('#firmCode').val().indexOf(',')<=0){
						$('body').window({
							id: 'window_deptDetail',
							title: '<fmt:message key="department_of_details"/>',
							content: '<iframe id="deptDetailFrame" frameborder="0" src="'+"<%=path%>/disFenBo/findDeptDetail.do?maded="+maded+"&positnCode="+positnCode+"&deliverCode="+deliverCode+"&typCode="+typCode+"&firmCode="+firmCode+"&sp_code="+sp_code+"&chksend="+chksend+"&ex1="+ex1+"&yndo="+yndo+"&sp_init="+sp_init+"&inout="+inout+'"></iframe>',
							width: '1000px',
							height: '380px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										  text: 'Excel',
										  title: 'Excel',
										  useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
									      icon: {
										  		url: '<%=path%>/image/Button/op_owner.gif',
										  		position: ['-40px','-20px']
									      		},
											handler: function(){
													$("#wait2").val('NO');//不用等待加载
													$('#listForm').attr('action','<%=path%>/disFenBo/exportDeptDetail.do');
													$('#listForm').submit();
													$("#wait2 span").html("loading...");
													$('#listForm').attr('action','<%=path%>/disFenBo/list.do');	
													$("#wait2").val('');//等待加载还原
									 
											}
										},{
// 											text: '<fmt:message key="print" />',
// 											title: '<fmt:message key="print"/>',
// 											handler: function()
// 											{
// 												window.frames["deptDetailFrame"].printDis();
// 											}
// 										},{
											text: '<fmt:message key="quit"/>(ESC)',
											title: '<fmt:message key="quit"/>',
											icon: {
												url: '<%=path%>/image/Button/op_owner.gif',
												position: ['-160px','-100px']
											},
											handler: function(){
												$('.close').click();
											}
										}]
									}
							});
						}else{
							alert('<fmt:message key ="only_select_one_store" />');
						}
					}else{
						alert('<fmt:message key="please_select_branche"/>！');
					}
				}else{
					alert('<fmt:message key="please_select"/><fmt:message key="suppliers"/>！');
				}
			}else{
				alert('<fmt:message key="please_select_the_collection_date"/>！');
			}
		}
		
		//部门明细
		function deptDetail2(){
			var checkboxList2 = $('.grid').find('.table-body').find(':checkbox');
			var maded = $('#maded').val();
		    var positnCode = $("#positnCode").val();
			var deliverCode = $('#deliverCode').val();
			var typCode = $("#typCode").val(); 
			var firmCode = $('#firmCode').val();
			var sp_code = $('#sp_code').val();
			var ex1 = $("input[name='ex1']:checked").val();
			var yndo = $("input[name='yndo']:checked").val();
			var sp_init = $("#sp_init").val();
		    var inout = $("input[name='inout']:checked").val();
			var chksend = $("input[name='ynArrival']:checked").val();
			var flag = true;
			var sp_codes = [];
			if(checkboxList2 && checkboxList2.filter(':checked').size() > 0){
				checkboxList2.filter(':checked').each(function(i){
					sp_codes.push($.trim($(this).closest('tr').find('td').eq(3).find('span').text()));
				});	}
			selects = {};
			//id 数组
		
			if(null!=$('#maded').val() && ""!=$('#maded').val()){
				$('body').window({
					id: 'window_deptDetail',
					title: '<fmt:message key="department_of_details"/>',
					content: '<iframe id="deptDetailFrame2" name="deptDetailFrame2" frameborder="0" src="'+"<%=path%>/disFenBo/findDeptDetail2.do?sp_codes="+sp_codes+"&maded="+maded+"&positnCode="+positnCode+"&deliverCode="+deliverCode+"&typCode="+typCode+"&firmCode="+firmCode+"&sp_code="+sp_code+"&chksend="+chksend+"&ex1="+ex1+"&yndo="+yndo+"&sp_init="+sp_init+"&inout="+inout+'"></iframe>',
					width: '99%',
					height: '90%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
							  text: '<fmt:message key="select"/>',
							  title: '<fmt:message key="select"/>',
						      icon: {
							  		url: '<%=path%>/image/Button/op_owner.gif',
							  		position: ['-40px','-20px']
						      },
							  handler: function(){
									window.frames['deptDetailFrame2'].pageReload();
							  }
						},{
							  text: 'Excel',
							  title: 'Excel',
							  useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						      icon: {
							  		url: '<%=path%>/image/Button/op_owner.gif',
							  		position: ['-40px','-20px']
						      },
							  handler: function(){
								  window.frames["deptDetailFrame2"].exportDis();
							  }
						},{
							text: '<fmt:message key ="Branch_Division" />',
							  title: '<fmt:message key ="Branch_Division" />',
							  useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						      icon: {
							  		url: '<%=path%>/image/Button/op_owner.gif',
							  		position: ['-40px','-20px']
						      },
							  handler: function(){
								  window.frames["deptDetailFrame2"].exportDeptExcel();
							  }
						},{
							text: '<fmt:message key="print" />',
							title: '<fmt:message key="print"/>',
							handler: function(){
								window.frames["deptDetailFrame2"].printDis();
							}
						},{
							text: '<fmt:message key="quit"/>(ESC)',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}]
					}
				});
			}else{
				alert('<fmt:message key="please_select_the_collection_date"/>！');
			}
		
		}
		
		//生成采购订单
		function toSavePuprOrder(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			selects = {};
			//id 数组
			var idss = [];
			var flag = true;
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					if ($.trim($(this).closest('tr').find('td').eq(22).find('span').attr('title')) != 'Y') {
						flag = false;
						alert('<fmt:message key="supplies"/>:'+$.trim($(this).closest('tr').find('td').eq(4).find('span').text())+'<fmt:message key="there_is_no"/><fmt:message key="procurement_confirm"/>!');
						return false;
					}
					if ($.trim($(this).closest('tr').find('td').eq(26).find('span').attr('title')) != 'Y') {
						flag = false;
						alert('<fmt:message key="supplies"/>:'+$.trim($(this).closest('tr').find('td').eq(4).find('span').text())+'<fmt:message key="there_is_no"/><fmt:message key="procurement_audit"/>!');
						return false;
					}
					if ($.trim($(this).closest('tr').find('td').eq(35).find('span').attr('title')) != 'N') {
						flag = false;
						alert('<fmt:message key="supplies"/>:'+$.trim($(this).closest('tr').find('td').eq(4).find('span').text())+'已经生成采购订单，不可重复生成!');
						return false;
					}
					selects['listPuprOrderd['+i+'].pk_material'] = $.trim($(this).closest('tr').find('td').eq(3).find('span').text());
					selects['listPuprOrderd['+i+'].sp_code'] = $.trim($(this).closest('tr').find('td').eq(3).find('span').text());
					selects['listPuprOrderd['+i+'].sp_name'] = $.trim($(this).closest('tr').find('td').eq(4).find('span').text());
					selects['listPuprOrderd['+i+'].unit'] = $.trim($(this).closest('tr').find('td').eq(9).find('span').text());
					selects['listPuprOrderd['+i+'].sp_desc'] = $.trim($(this).closest('tr').find('td').eq(5).find('span').text());
					selects['listPuprOrderd['+i+'].ncnt'] = $.trim($(this).closest('tr').find('td').eq(11).find('input').val());
					selects['listPuprOrderd['+i+'].sp_price'] = Number($.trim($(this).closest('tr').find('td').eq(14).find('input').val()));
					selects['listPuprOrderd['+i+'].vmemo'] = $.trim($(this).closest('tr').find('td').eq(27).find('span').text());
					selects['listPuprOrderd['+i+'].positncode'] = $.trim($(this).closest('tr').find('td').eq(8).find('input').val());
					selects['listPuprOrderd['+i+'].positnname'] = $.trim($(this).closest('tr').find('td').eq(8).find('span').text());
					selects['listPuprOrderd['+i+'].delivercode'] = $.trim($(this).closest('tr').find('td').eq(6).find('input').val());
					selects['listPuprOrderd['+i+'].delivername'] = $.trim($(this).closest('tr').find('td').eq(7).find('span').text());
					selects['listPuprOrderd['+i+'].dhopedate'] = $.trim($(this).closest('tr').find('td').eq(20).find('input').val());
					
					//id号
					idss.push($.trim($(this).closest('tr').find('td').eq(2).find('span').text()));
				});
				if (flag) {
					$.ajaxSetup({async:false});
					$.post('<%=path%>/disFenBo/toSavePuprOrder.do',selects,function(data){
						if(data!='ok'){
							alerterror('<fmt:message key="ORDR"/>：'+data+'，<fmt:message key="notyoucreatecantsubmit"/>');
							return;
						} else {
							alert('<fmt:message key="operation_successful"/>');
						}
					});

					var selected = {};
					selected['ispuprorder'] = 'Y';
					selected['id'] = idss.join(",");
					$.post('<%=path%>/disFenBo/update.do',selected,function(data){
						checkboxList.filter(':checked').each(function(){
							$(this).parents('tr').find('td:eq(35)').find('span').attr('title','Y').html('<input type="hidden" id="qr_'+$(this).val()+'" value="Y"/><img src="<%=path%>/image/kucun/Y.png"/>');
						});
					});
					
<%-- 				action="<%=path%>/disFenBo/updateAll.do?param=isPuprOrder-Y"; --%>
// 					$('#listForm').attr('action',action);
// 					$('#listForm').submit();
				}
			} else {
				alert('<fmt:message key ="The_data_is_not_available_for_purchase_orders" />！');
			}
		}
		
		
		
		//配送拆分
		function psChaiFen(){
			alert('1');
		}
		//验货直发
		function yhZhiFa(){
			var curwindow = $('body').window({
				id: 'window_yhZhiFa',
				title: '<fmt:message key ="Straight" />',
				content: '<iframe id="yhZhiFaFrame" frameborder="0" src="<%=path%>/disFenBo/findYanHuoZhiFa.do?action=init"></iframe>',
				width: '1000px',
				height: '420px',
				draggable: true,
				isModal: true,
				confirmClose: false
			});
			curwindow.max();
		}
		
		
		//判断是否供应商为空和采购数量为零的js
		//col 22采购确认 26采购审核
		function checkHashNull(flag,col){
			
			//只有金太子项目才判断是否供应商为空和采购数量为零的
			if("${dis.scm}"!="jtz"){
				return true;
			}
			
			var checkboxList;
			
			var isTrue = true;
			var wzname = "";
			var tline = 0;
			var gysorsl = 0;
			
			if(flag=='all'){
				//全部判断
				checkboxList = $(".grid .table-body tr");
				checkboxList.each(function(i){
					//如果已经确认或者审核过了，就继续下一行
					var sestr = "td:eq("+col+") span";
					var hasCk = $(this).find(sestr).attr('title');
					if(hasCk=='Y'){
						return true;
					}
					var delcode = $(this).find("td:eq(6) input").val()||$(this).find("td:eq(6) span").attr('title');
					var cgnum = $(this).find("td:eq(11) input").val();
					if(delcode=='' ||delcode==null||$.trim(delcode)==''){
						tline = i+1;
						wzname = $(this).find("td:eq(4) span").attr('title');
						gysorsl = 1;
						isTrue = false;
						return false;
					}
					if(cgnum=='' ||cgnum==null||$.trim(cgnum)==''||cgnum=='0.00'){
						tline = i+1;
						wzname = $(this).find("td:eq(4) span").attr('title');
						gysorsl = 2;
						isTrue = false;
						return false;
					}
				});
			}else{
				checkboxList = $(".grid .table-body :checkbox:checked");
				checkboxList.each(function(i){
					var trobj = $(this).parents('tr:first');
					//如果已经确认或者审核过了，就继续下一行
					var sestr = "td:eq("+col+") span";
					var hasCk = trobj.find(sestr).attr('title');
					if(hasCk=='Y'){
						return true;
					}
					var delcode = trobj.find("td:eq(6) input").val()||trobj.find("td:eq(6) span").attr('title');
					var cgnum = trobj.find("td:eq(11) input").val();
					if(delcode=='' ||delcode==null||$.trim(delcode)==''){
						tline = i+1;
						wzname = trobj.find("td:eq(4) span").attr('title');
						gysorsl = 1;
						isTrue = false;
						return false;
					}
					if(cgnum=='' ||cgnum==null||$.trim(cgnum)==''||cgnum=='0.00'){
						tline = i+1;
						wzname = trobj.find("td:eq(4) span").attr('title');
						gysorsl = 2;
						isTrue = false;
						return false;
					}
				});
			}

			if(!isTrue){
				alert("第"+tline+"行，物资："+wzname+"，"+(gysorsl==1?"供应商为空！！！":"采购量为0！！！"));
			}
			return isTrue;
		}
		
		
		//采购确认  采购审核  未知到货
		function fucEnter(colomn,value){
			
			if(parent.bhfbEditState=='edit'){
				alert('<fmt:message key="please_save_the_unsaved_data"/>！');
				return;
			}
			if(colomn=='excel'){
				printDis("exportExcel.do");
				return;
			}
			
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				msg="";
				if (colomn=='chk1'&&value=='Y') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations1'/>？";
				}
				if (colomn=='chk1'&&value=='N') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations4'/>？";
				}
				if (colomn=='chksend'&&value=='Y') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations2'/>？";
				}
				if (colomn=='chksend'&&value=='N') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations5'/>？";
				}
				if (colomn=='sta'&&value=='Y') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations3'/>？";
				}
				if (colomn=='sta'&&value=='N') {
					msg="<fmt:message key='selected_data_to_confirm_procurement_operations6'/>？";
				}
				if (colomn=='excel') {
					msg="<fmt:message key='the_selected_data_were_derived_excel_operation'/>？";
				}
				if (colomn=='price') {
					msg="<fmt:message key='Are_you_sure_update_lastest_price'/>？";
				}
				
				if (confirm(msg)) {
					var chkValue = [];
					var flag = true;
					checkboxList.filter(':checked').each(function(){
						//点取消采购确认 取消采购审核时 过滤掉供应商确认发货的 20151217wjf
						if ((colomn=='chk1'&&value=='N') || (colomn=='sta'&&value=='N')) {
							var deliveryn = $(this).parents('tr').find('td:eq(32)').find('span').attr('title');
							if(deliveryn == 'Y'){
								flag = false;
								return true;
							}
						}
						chkValue.push($(this).val());
					});
					if(!flag){
						alert('供应商确认发货的物资不能取消采购确认,采购审核！已过滤！');
					}
					selected['id'] = chkValue.join(',');
					//采购确认
					if(colomn=="chk1"){	
						//判断供应商和采购数量是否为0
						if(!checkHashNull('checked',22)){
							//不向下进行
							//alert('存在供应商为空或者采购数量为空的记录！！！');
							return;
						}
						selected['chk1'] = value;
						$.post('<%=path%>/disFenBo/update.do',selected,function(data){
							checkboxList.filter(':checked').each(function(){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(22)').find('span').attr('title','Y').html('<input type="hidden" id="qr_'+$(this).val()+'" value="Y"/><img src="<%=path%>/image/kucun/Y.png"/>');
									$(this).parents('tr').find('td:eq(23)').find('span').html($("#accountName").val());
									$(this).parents('tr').find('td:eq(24)').find('span').html(getDate());
								}else{
									var deliveryn = $(this).parents('tr').find('td:eq(32)').find('span').attr('title');
									if(deliveryn == 'Y'){
										return true;
									}
									$(this).parents('tr').find('td:eq(22)').find('span').attr('title','N').html('<input type="hidden" id="qr_'+$(this).val()+'" value="N"/><img src="<%=path%>/image/kucun/N.png"/>');
									$(this).parents('tr').find('td:eq(23)').find('span').html($("#accountName").val());
									$(this).parents('tr').find('td:eq(24)').find('span').html(getDate());

								}
							});
						});
					}
					//到货未知
					if(colomn=="chksend"){
						selected['chksend'] = value;
						$.post('<%=path%>/disFenBo/update.do',selected,function(data){
							checkboxList.filter(':checked').each(function(){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(25)').find('span').html('<img src="<%=path%>/image/kucun/Y.png"/>');
								}else{
									$(this).parents('tr').find('td:eq(25)').find('span').html('<img src="<%=path%>/image/kucun/N.png"/>');
								}
							});
						});
					}
					//审核确认
					if(colomn=="sta"){
						//判断供应商和采购数量是否为0
						if(!checkHashNull('checked',26)){
							//不向下进行
							//alert('存在供应商为空或者采购数量为空的记录！！！');
							return;
						}
						selected['sta'] = value;
						checkboxList.filter(':checked').each(function(){
							if($('#qr_'+$(this).val()).val()=='Y'){
								if(value=='Y'){
									$(this).parents('tr').find('td:eq(26)').find('span').attr('title','Y').html('<img src="<%=path%>/image/kucun/Y.png"/>').attr('title','Y');
									$.post('<%=path%>/disFenBo/update.do',selected);
								}else{
									var deliveryn = $(this).parents('tr').find('td:eq(32)').find('span').attr('title');
									if(deliveryn == 'Y'){
										return true;
									}
									$(this).parents('tr').find('td:eq(26)').find('span').attr('title','N').html('<img src="<%=path%>/image/kucun/N.png"/>').attr('title','N');
									$.post('<%=path%>/disFenBo/update.do',selected);
								}
							}else{
								return;
							}
						});
					}
					//更新报价
					if(colomn=="price"){
						$.ajax({
							url:'<%=path%>/disFenBo/updateLastPrice.do',
							type:'post',
							data:selected,
							success:function(msg){
								$('#listForm').submit();
							}
						});
					}
				}
			}else{
				alert("<fmt:message key='please_check_the_operating_options'/>！");
				return ;
			}
		}
		//全部 采购确认、未知到货、采购审核
		function fucEnterAll(editType){
			
			if($('.grid').find('.table-body').find("tr").size()<1){
				alert("<fmt:message key='data_for_the_air_to_operate'/>！");
				return;
			}
			if(parent.bhfbEditState=='edit'){
				alert("<fmt:message key='please_save_the_unsaved_data'/>！");
				return;
			}
			msg="";
			if (editType=='confirm') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations1'/>？";
			}
			if (editType=='unKnownArrival') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations2'/>？";
			}
			if (editType=='buyCheck') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations3'/>？";
			}
			if (editType=='cancelConfirm') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations4'/>？";
			}
			if (editType=='cancelUnKnownArrival') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations5'/>？";
			}
			if (editType=='cancelBuyCheck') {
				msg="<fmt:message key='selected_data_to_confirm_all_procurement_operations6'/>？";
			}
			if (editType=='price') {
				msg="<fmt:message key='Are_you_sure_all_update_lastest_price'/>？";
			}
			if (confirm(msg)) {
				var action="<%=path%>/disFenBo/updateAll.do";
				if(editType=='confirm'){//采购确认
					//判断供应商和采购数量是否为0
					if(!checkHashNull('all',22)){
						//不向下进行
						//alert('存在供应商为空或者采购数量为空的记录！！！');
						return;
					}
					action="<%=path%>/disFenBo/updateAll.do?param=chk1-Y";
				}else if(editType=='cancelConfirm'){//取消采购确认
					action="<%=path%>/disFenBo/updateAll.do?param=chk1-N";
				}else if(editType=='unKnownArrival'){//未知到货
					action="<%=path%>/disFenBo/updateAll.do?param=chksend-Y";
				}else if(editType=='cancelUnKnownArrival'){//取消未知到货
					action="<%=path%>/disFenBo/updateAll.do?param=chksend-N";
				}else if(editType=='buyCheck'){//采购审核
					//判断供应商和采购数量是否为0
					if(!checkHashNull('all',26)){
						//不向下进行
						//alert('存在供应商为空或者采购数量为空的记录！！！');
						return;
					}
					action="<%=path%>/disFenBo/updateAll.do?param=sta-Y";
				}else if(editType=='cancelBuyCheck'){//取消采购审核
					action="<%=path%>/disFenBo/updateAll.do?param=sta-N";
				}else if(editType=='price'){//全部更新报价 
					action="<%=path%>/disFenBo/updateAll.do?param=price-Y";
				}
				
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		//修改报货------------		
		function updateDis(){
			$("#sp_init").focus();
			//如果修改过数据就保存 
			if(parent.trlist){
				var predata = parent.trlist;
				var disList = {};
				var num=0;
				for(var i in predata){
					disList['listDis['+num+'].id']=i;
					undefined==predata[i].deliverCode?'':disList['listDis['+num+'].deliverCode']=predata[i].deliverCode;
					undefined==predata[i].amount1sto?'':disList['listDis['+num+'].amount1sto']=predata[i].amount1sto;
					undefined==predata[i].amountin?'':disList['listDis['+num+'].amountin']=predata[i].amountin;
					undefined==predata[i].amount1in?'':disList['listDis['+num+'].amount1in']=predata[i].amount1in;
					undefined==predata[i].pricein?'':disList['listDis['+num+'].pricein']=predata[i].pricein;
					undefined==predata[i].pricesale?'':disList['listDis['+num+'].pricesale']=predata[i].pricesale;
					undefined==predata[i].ind?'':disList['listDis['+num+'].ind']=predata[i].ind;
					undefined==predata[i].dued?'':disList['listDis['+num+'].dued']=predata[i].dued;
					undefined==predata[i].inout?'':disList['listDis['+num+'].inout']=predata[i].inout;
					undefined==predata[i].pcno?'':disList['listDis['+num+'].pcno']=predata[i].pcno;
					num++;
				}
				$.post("<%=path%>/disFenBo/updateDis.do",disList,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
						alert('<fmt:message key="successful"/><fmt:message key="update"/>'+rs.updateNum+'<fmt:message key="article"/><fmt:message key="data"/>！');
						var action="<%=path%>/disFenBo/list.do";
						$('#listForm').attr('action',action);
					}else{
						alert('<fmt:message key="update"/><fmt:message key="failure"/>！\n<fmt:message key="the_documents_of_inbound_or_outbound"/>！');
						var action="<%=path%>/disFenBo/list.do";
						$('#listForm').attr('action',action);
					}
				});	
				parent.bhfbEditState = '';
				parent.changeNum = 0;
				parent.trlist=undefined;
				loadToolBar([true,true,false,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				$('#listForm').submit();
			}else{
				alert('<fmt:message key="do_not_modify_any_data"/>！');
				parent.bhfbEditState = '';
				parent.changeNum = 0;
				parent.trlist=undefined;
				loadToolBar([true,true,false,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			}
		}
		//查找供应商
		function searchDeliver(e,obj){
			if(window.event.keyCode==13){
				var inoutNum;
				var inout=$(e).closest("tr").find('td:eq(19)').find('span:eq(0)').text();
				var firmCode=$(e).closest("tr").find('td:eq(8)').find('span input').val();
				if("in"==inout){
					inoutNum='2';//入库应该可以查所有的供应商
				}
				if("inout"==inout){
					inoutNum=0;
				}
				if("dire"==inout){
					inoutNum=1;
				}
				$("#selectDelCodeId").val(obj);
				$('body').window({
					id: 'window_searchDeliver',
					title: '<fmt:message key="query_suppliers"/>',
					content: '<iframe id="searchDeliverFrame" frameborder="0" src="'+"<%=path%>/deliver/searchDeliver.do?action=init&firm="+firmCode+"&inout="+inoutNum+'"></iframe>',
					width: '800px',
					height: '420px',
					draggable: true,
					isModal: true,
					confirmClose: false,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />(<u>F</u>)',
								title: '<fmt:message key="query_suppliers"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['0px','-40px']
								},
								handler: function(){
									if(getFrame('searchDeliverFrame')){
										submitFrameForm('searchDeliverFrame','SearchForm');
									}
								}
							},{
								text: '<fmt:message key="quit"/>',
								title: '<fmt:message key="quit"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});								
			}else{
				return;
			}
		}
		//输入日期回车
		function selectInd(obj,typ){
			//回车 或者点击事件进来的
			if(window.event.keyCode==13||typ==1){
				var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig); 
				if(null!=date && ""!=date){
					if($("#selectIndSta").val()=="YES"){
						$("#selectIndSta").val("NO");
						new WdatePicker();
						$(obj).focus();
					}else{
						$("#selectIndSta").val("YES");
						new WdatePicker();
						$(obj).focus();
					}
				}else{
					if(null!=obj.defaultValue && ""!=obj.defaultValue){
						obj.value=obj.defaultValue;
					}else{
						obj.value=getInd();
					}
					showMessage({
								type: 'error',
								msg: '<fmt:message key="the_date_is_not_in_the_correct_format"/>！',
								speed: 1000
								});
					$(obj).focus();
					return;
				}
			}
		}
		//鼠标单击选择日期，屏蔽光标方向移动
		function selectInd2(){
			if($("#selectIndSta").val()=="YES"){
				$("#selectIndSta").val("NO");
				new WdatePicker();
			}else{
				$("#selectIndSta").val("YES");
				new WdatePicker();
			}
		}
		//日期验证
		function validateDate(obj){
			var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
			if(null==date || ""==date){
				if(null!=obj.defaultValue && ""!=obj.defaultValue){
					obj.value=obj.defaultValue;
				}else{
					obj.value=getInd();
				}
				showMessage({
							type: 'error',
							msg: '<fmt:message key="the_date_is_not_in_the_correct_format"/>！',
							speed: 1000
							});
				$(obj).focus();
				return;
			}
		}
		
		//导出excel分拨单
		function excelDis(obj){ 
			$("#wait2").val("NO");
// 			$('#listForm').attr('target','report');
// 			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disFenBo/excelDis'+obj+'.do';	
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
// 			$('#listForm').attr('target','');
			$("#wait2").val("");
		}
		
		//打印单据
		function printDis(e){ 
			$("#wait2").val("NO");
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disFenBo/'+e;	
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val("");
		}	
		
		//让时间选中框可不可选
		function checktim_btn(obj){
			if(obj.checked){
				var today=new Date();
				var hh=today.getHours();
				if(hh<10)
					hh="0"+hh;
				var mm=today.getMinutes();
				if(mm<10)
					mm="0"+mm;
				var ss=today.getSeconds();
				if(ss<10)
					ss="0"+ss;
				var curTime=hh+":"+mm+":"+ss;
				$("#bchectim").val(curTime).removeAttr("disabled","disabled");
				$("#echectim").val(curTime).removeAttr("disabled","disabled");
			}else{
				$("#bchectim").val("").attr("disabled","disabled");
				$("#echectim").val("").attr("disabled","disabled");
			}
		}
		//获取系统时间
		function getDate(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			var hh=myDate.getHours();
			var mm=myDate.getMinutes();
			var ss=myDate.getSeconds();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			if(hh<10)
				hh="0"+hh;
			if(mm<10)
				mm="0"+mm;
			if(ss<10)
				ss="0"+ss;
			return fullDate=yy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss;
		}
		//获取系统时间
		function getInd(){
			var myDate=new Date();  
			var yy=myDate.getFullYear();
			var MM=myDate.getMonth()+1;
			var dd=myDate.getDate();
			if(MM<10)
				MM="0"+MM;
			if(dd<10)
				dd="0"+dd;
			return fullDate=yy+"-"+MM+"-"+dd;
		}
		//焦点离开检查输入是否合法
		function validate(e){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
		}
		
		//根据code查询名称
		function findByDeliver(e,f){
			if(e.value==e.defaultValue){
				return;
			}
			var deliverDes=$(e).closest("tr").find('td:eq(7) span').text();
			var firmCode=$(e).closest("tr").find('td:eq(8)').find('span input').val();
			var inout=$(e).closest('tr').find('td:eq(19)').find('span:eq(0)').attr('title');
			var selected = {};
			selected['code'] = e.value;
// 			selected['oldCode'] = e.defaultValue;
			selected['firm']=firmCode;
			if("in"==inout){
				selected['inout'] = 2;//入库应该可以查所有的供应商
			}
			if("inout"==inout){
				selected['inout'] = 0;
			}
			if("dire"==inout){
				selected['inout'] = 1;
			}
			if(null!=e.value && ""!=e.value){
				//提交并返回值，判断执行状态
 				$.post("<%=path%>/disFenBo/findDeliverTyp.do",selected,function(data){
					if(null!=data && ""!=data){
						var rs = eval('('+data+')');
// 						if("1"==rs.sta){
// 							alert('<fmt:message key="the_warehouse_and_suppliers_can_not_change_the_base"/>！');
// 							e.value=e.defaultValue;
// 							$("#d_del_"+f).text($("#d_del_"+f).attr('title'));
// 							$(e).focus();
// 							return;
// 						}else 
						if("2"==rs.sta){//直配直发不能相互改
							alert('<fmt:message key="the_straight_direction_can_not_be_change"/>！');
							e.value=e.defaultValue;
							$("#d_del_"+f).text($("#d_del_"+f).attr('title'));
							$(e).focus();
							return;
						}else{
// 							if("入库"!=inout && "出库"!=inout){
								$("#d_del_"+f).text(rs.deliver.des);
// 								if("1"==rs.inout){
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(0)').text('dire');
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(1)').text('<fmt:message key="direct"/>');
// 								}else if((deliverDes).indexOf('自购')!=-1){
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(0)').text('dire');
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(1)').text('<fmt:message key="direct"/>');
// 								}else{
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(0)').text('inout');
// 									$(e).closest("tr").find('td:eq(19)').find('span:eq(1)').text('<fmt:message key="straight_hair"/>');
// 								}
								//记录改变保存
								if(parent.trlist){
									//已经存在这条数据
									if(parent.trlist[f]){
										parent.trlist[f]['deliverCode']=e.value;
// 										parent.trlist[f]['inout']=e.value;
									}else{
										parent.trlist[f]={};
										parent.changeNum = parent.changeNum+1;//修改条数+1
										$("#changeNum").text(parent.changeNum);
										parent.trlist[f]['deliverCode']=e.value;
// 										parent.trlist[f]['inout']=e.value;
									}
								}else{
									parent.trlist = {};
									parent.trlist[f]={};
									parent.changeNum = 1;
									$("#changeNum").text(1);
									parent.trlist[f]['deliverCode']=e.value;
// 									parent.trlist[f]['inout']=e.value;
								}
// 							}else{
// 								e.value=e.defaultValue;
// 								$("#d_del_"+f).text($("#d_del_"+f).attr('title'));
// 								alert('该方向供应商不允许修改！');
// 								return;
// 							}
						}
					}else{
						alert('<fmt:message key="supplier_unfound"/>！');
						e.value=e.defaultValue;
						$("#d_del_"+f).text($("#d_del_"+f).attr('title'));
						$(e).focus();
						return;
					}
				});
			}
		}
		//保存采购调整数量
		function saveAmount1sto(e,f,u,u3){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['amount1sto']=e.value;
						if(u3 != 0){//如果转换率不为0，标准数量，参考数量，总金额都要跟着变
							parent.trlist[f]['amountin'] = Number(e.value)/Number(u3);
							parent.trlist[f]['amount1in'] = Number(e.value)/Number(u3)*Number(u);
							$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
							$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);
							var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
							$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u3)*Number(price)).toFixed(2));
						}
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['amount1sto']=e.value;
						if(u3 != 0){//如果转换率不为0，标准数量，参考数量，总金额都要跟着变
							parent.trlist[f]['amountin'] = Number(e.value)/Number(u3);
							parent.trlist[f]['amount1in'] = Number(e.value)/Number(u3)*Number(u);
							$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
							$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);
							var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
							$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u3)*Number(price)).toFixed(2));
						}
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['amount1sto']=e.value;
					if(u3 != 0){//如果转换率不为0，标准数量，参考数量，总金额都要跟着变
						parent.trlist[f]['amountin'] = Number(e.value)/Number(u3);
						parent.trlist[f]['amount1in'] = Number(e.value)/Number(u3)*Number(u);
						$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
						$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);
						var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u3)*Number(price)).toFixed(2));
					}
				}
			}
		}
		
		//保存标准数量
		function saveAmountin(e,f,u,u3){//参数u：物资标准单位和参考单位之间的转换率；用来计算修改标准数量后参考数量也要变  wjf
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['amountin']=e.value;//标准数量
						parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
						if(u3 != 0){
							parent.trlist[f]['amount1sto'] = Number(e.value)*Number(u3);//采购数量
							$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
						}
						$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);//显示参考数量
						var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(price)).toFixed(2));
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['amountin']=e.value;//标准数量
						parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
						if(u3 != 0){
							parent.trlist[f]['amount1sto'] = Number(e.value)*Number(u3);//采购数量
							$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
						}
						$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);//显示参考数量
						var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(price)).toFixed(2));
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['amountin']=e.value;//标准数量
					parent.trlist[f]['amount1in'] = Number(e.value)*Number(u);//参考数量
					if(u3 != 0){
						parent.trlist[f]['amount1sto'] = Number(e.value)*Number(u3);//采购数量
						$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
					}
					$(e).parents('tr').find('td:eq(18) span').find('input').val(parent.trlist[f]['amount1in']);//显示参考数量
					var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
					$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(price)).toFixed(2));
				}
			}
		}
		//保存参考采购数量
		function saveAmount1in(e,f,u,u3){//这里是转换率为0的物资参考数量修改方法 此物资现在默认 参考数量 = 标准数量 所以参考数量修改标准数量也改 wjf
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['amount1in']=e.value;//数量
						if(u != 0){
							parent.trlist[f]['amountin'] = Number(e.value)/Number(u);//标准数量
							$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
							var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
							$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u)*Number(price)).toFixed(2));
							if(u3 != 0){
								parent.trlist[f]['amount1sto'] = Number(e.value)/Number(u)*Number(u3);//采购数量
								$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
							}
						}
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['amount1in']=e.value;//数量
						if(u != 0){
							parent.trlist[f]['amountin'] = Number(e.value)/Number(u);//标准数量
							$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
							var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
							$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u)*Number(price)).toFixed(2));
							if(u3 != 0){
								parent.trlist[f]['amount1sto'] = Number(e.value)/Number(u)*Number(u3);//采购数量
								$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
							}
						}
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['amount1in']=e.value;//数量
					if(u != 0){
						parent.trlist[f]['amountin'] = Number(e.value)/Number(u);//标准数量
						$(e).parents('tr').find('td:eq(13) span').find('input').val(parent.trlist[f]['amountin']);
						var price = $(e).parents('tr').find('td:eq(14) span').find('input').val()?$(e).parents('tr').find('td:eq(14) span').find('input').val():$(e).parents('tr').find('td:eq(14) span').text();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)/Number(u)*Number(price)).toFixed(2));
						if(u3 != 0){
							parent.trlist[f]['amount1sto'] = Number(e.value)/Number(u)*Number(u3);//采购数量
							$(e).parents('tr').find('td:eq(11) span').find('input').val(parent.trlist[f]['amount1sto']);//显示调整数量
						}
					}
				}
			}
		}
		//保存单价
		function savePricein(e,f){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['pricein']=e.value;
						var amountin = $(e).parents('tr').find('td:eq(13) span').find('input').val();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(amountin)).toFixed(2));//重新计算总价
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['pricein']=e.value;
						var amountin = $(e).parents('tr').find('td:eq(13) span').find('input').val();
						$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(amountin)).toFixed(2));//重新计算总价
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['pricein']=e.value;
					var amountin = $(e).parents('tr').find('td:eq(13) span').find('input').val();
					$(e).parents('tr').find('td:eq(16) span').text(Number(Number(e.value)*Number(amountin)).toFixed(2));//重新计算总价
				}
			}
		}
		//保存售价
		function savePricesale(e,f){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['pricesale']=e.value;
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['pricesale']=e.value;
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['pricesale']=e.value;
				}
			}
		}
		//批量修改价格
		function checkPrice(e,num,disid){
			//售价
			if(num==15){
				//是否更新相同物资不同分店的售价，出库方向
				var sp_code =$(e).closest('tr').find('td:eq(3)').find('span').text();
				var updatedPrice =$(e).closest('tr').find('td:eq(15)').find('span input').val();
				var fangxiang = $(e).closest('tr').find('td:eq(19)').find('span:eq(0)').text();
				savePricesale($(e).closest('tr').find('td:eq(15) span input').get(0),$(e).closest('tr').find('td:eq(2) span').text());
				//提示信息
				var message = "";
				//出库
				if($.trim(fangxiang)=='out'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="library" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="scm_sale_price" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				//直发
				else if($.trim(fangxiang)=='inout'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="straight_hair" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="scm_sale_price" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				//直配
				else if($.trim(fangxiang)=='dire'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="direct" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="scm_sale_price" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				else{
					return;
				}
				//提示
				var isUpdateSameSpSale = confirm(message);
				if(isUpdateSameSpSale){
					$('#grid-body-div table tbody tr').each(function(i){
						var spCode = $(this).find('td:eq(3) span').text();
						//方向
						var ifangxiang = $(this).find('td:eq(19)').find('span:eq(0)').text();
						if($.trim(sp_code)==$.trim(spCode)&&ifangxiang==fangxiang){
							$(this).find('td:eq(15) span input').val(updatedPrice);
							$(this).find('td:eq(15) span input').attr('title',updatedPrice);
							savePricesale($(this).find('td:eq(15) span input').get(0),$(this).find('td:eq(2) span').text());
						}
					});
				}
			}
			
			//报价
			if(num==14){
				//是否更新相同物资不同分店的报价，出库方向
				var sp_code =$(e).closest('tr').find('td:eq(3)').find('span').text();
				var updatedPrice =$(e).closest('tr').find('td:eq(14)').find('span input').val();
				var fangxiang = $(e).closest('tr').find('td:eq(19)').find('span:eq(0)').text();
				//修改本行金额
				//数量
				var shuliang2 = $(e).closest('tr').find('td:eq(13)').find('span input').val();
				//金额
				var zongjia2 = shuliang2 * e.value;
				zongjia2 = zongjia2.toFixed(2);
				$(e).closest('tr').find('td:eq(14) span input').attr('title',e.value);
				$(e).closest('tr').find('td:eq(16)').find('span').text(zongjia2);
				//记录保存
				savePricein($(e).closest('tr').find('td:eq(14) span input').get(0),$(e).closest('tr').find('td:eq(2) span').text());
				//提示信息
				var message = "";
				//出库
				if($.trim(fangxiang)=='out'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="library" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="offer" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				//直发
				else if($.trim(fangxiang)=='inout'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="straight_hair" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="offer" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				//直配
				else if($.trim(fangxiang)=='dire'){
					message = '<fmt:message key="Whether_to_put_the_material_all" /><fmt:message key="direct" /><fmt:message key="direction" /><fmt:message key="de" /><fmt:message key="offer" /><fmt:message key="Are_changed_to_this_price" />？';
				}
				else{
					return;
				}
				//提示
				var isUpdateSameSpSale = confirm(message);
				if(isUpdateSameSpSale){
					$('#grid-body-div table tbody tr').each(function(i){
						var spCode = $(this).find('td:eq(3) span').text();
						//方向
						var ifangxiang = $(this).find('td:eq(19)').find('span:eq(0)').text();
						if($.trim(sp_code)==$.trim(spCode)&&ifangxiang==fangxiang){
							$(this).find('td:eq(14) span input').val(updatedPrice);
							$(this).find('td:eq(14) span input').attr('title',updatedPrice);
							//数量
							var shuliang = $(this).find('td:eq(13)').find('span input').val();
							//金额
							var zongjia = shuliang * updatedPrice;
							zongjia = zongjia.toFixed(2);
							$(this).find('td:eq(16) span').text(zongjia);
							savePricein($(this).find('td:eq(14) span input').get(0),$(this).find('td:eq(2) span').text());
						}
					});
				}
			}
		}
		//保存日期
		function saveInd(e,f){
			var date = e.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
			if(null!=date && ""!=date){
				if(e.defaultValue!=e.value){
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['ind']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['ind']=e.value;
						}
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['ind']=e.value;
					}
				}
			}
		}
		//保存生产日期
		function saveDued(e,f){
			var date = e.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
			if(null!=date && ""!=date){
				if(e.defaultValue!=e.value){
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['dued']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['dued']=e.value;
						}
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['dued']=e.value;
					}
				}
			}
		}
		//输入日期回车
		function selectDued(obj){
			if(window.event.keyCode==13){
				var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig); 
				if(null!=date && ""!=date){
					if($("#selectIndSta").val()=="YES"){
						$("#selectIndSta").val("NO");
						new WdatePicker();
						$(obj).focus();
					}else{
						$("#selectIndSta").val("YES");
						new WdatePicker();
						$(obj).focus();
					}
				}else{
					if(null!=obj.defaultValue && ""!=obj.defaultValue){
						obj.value=obj.defaultValue;
					}else{
						obj.value=getInd();
					}
					$(obj).focus();
					return;
				}
			}
		}
		//保存批次号
		function savePcno(e,f){
			if(e.defaultValue!=e.value){
				if(parent.trlist){
					if(parent.trlist[f]){
						parent.trlist[f]['pcno']=e.value;
					}else{
						parent.trlist[f]={};
						parent.changeNum = parent.changeNum+1;
						$("#changeNum").text(parent.changeNum);
						parent.trlist[f]['pcno']=e.value;
					}
				}else{
					parent.trlist = {};
					parent.trlist[f]={};
					parent.changeNum = 1;
					$("#changeNum").text(1);
					parent.trlist[f]['pcno']=e.value;
				}
			}
		}
		//编辑状态下用户按Alt+X的时候 将焦点定位到缩写码
		function changeFocus(){
			$("#sp_init").focus();
		}
		
		//根据缩写码查询
		function ajaxSearch(){
			if(window.event.keyCode==13){
				if(null!=$('#maded').val() && ""!=$('#maded').val()){
					$("#listForm").submit();
				}else{
					alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
				}
			}
		}
		
		var totalCount;
		var condition;
		var currPage;
		function addTr(check){
			if(check=='first'){
				totalCount = '${totalCount}';
				condition = ${disJson};
				currPage= 1;
				condition.maded = new Date(condition.maded.time).format("yyyy-mm-dd");
				condition.bdat = "";
				condition.edat = "";
				condition.ind = "";
				condition.dued = "";
				condition['ind1'] = '${ind1}';
				condition['totalCount'] = totalCount;
				condition['currPage']=1;
				$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
				$("#currState").width(Number('${currState}')*200);
				return;
			}
			$.post("<%=path%>/disFenBo/listAjax.do",condition,function(data){
				var rs = eval('('+data+')');
				//var rs = data;
				$("#per").text((rs.currState*100).toFixed(0)+'%');
				$("#currState").width(""+rs.currState*200+"px");
				//不是最后一页
				var num = rs.currPage*pageSize;
				var disesList1 = rs.disesList1;
				for(var i in disesList1){
					var dis = disesList1[i];
					
					var tr = '<tr ';
					if(dis.amount!=dis.amountin){
						tr = tr+'style="color:red;"';
					}
					tr = tr + '>';
					tr = tr + '<td class="num"><span style="width:25px;">'+ ++num +'</span></td>';
					tr = tr + '<td><span style="width:25px;text-align: center;"><input type="checkbox" name="idList" id="chk_'+dis.id+'" value="'+dis.id+'"/></span></td>';
					tr = tr + '<td><span title="'+dis.id+'" style="width:60px;">'+dis.id+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_code+'" style="width:70px;">'+dis.sp_code+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_name+'" style="width:70px;">'+dis.sp_name+'</span></td>';
					tr = tr + '<td><span title="'+dis.sp_desc+'" style="width:70px;">'+dis.sp_desc+'</span></td>';
					if(dis.inout == 'out'){
						tr = tr + '<td><span style="width:60px;" title="'+dis.deliverCode+'">'+dis.deliverCode+'</span></td>';
					}else{
						tr = tr + '<td class="textInput"><span style="width:60px;"><input title="'+dis.deliverCode+'" type="text" id="c_delv_'+dis.id+'" value="'+dis.deliverCode+'" onfocus="this.select()" onkeyup="validate(this);" onblur="findByDeliver(this,'+dis.id+')" onkeydown="searchDeliver(this,'+dis.id+')"/></span></td>';
					}
					tr = tr + '<td><span title="'+dis.deliverDes+'" id="d_del_'+dis.id+'" style="width:120px;">'+dis.deliverDes+'</span></td>';	
					tr = tr + '<td><span title="'+dis.firmDes+'" style="width:120px;"><input type="hidden" value="'+dis.firmCode+'"/>'+dis.firmDes+'</span></td>';
					//采购单位
					tr = tr + '<td><span title="'+dis.unit3+'" style="width:60px;">'+dis.unit3+'</span></td>';//采购单位数量
					tr = tr + '<td><span title="'+dis.amount1+'" style="width:60px;text-align: right;">'+dis.amount1.toFixed(2)+'</span></td>';
					tr = tr + '<td><span style="width:60px;"><input class="nextclass"  title="'+dis.amount1sto.toFixed(2)+'" type="text" style="width: 60px;text-align: right;" value="'+dis.amount1sto.toFixed(2)+'" onfocus="this.select()" onblur="saveAmount1sto(this,'+dis.id+','+dis.unitper+','+dis.unitper3+')" onkeyup="validate(this)"/></span></td>';
					//标准单位
					tr = tr + '<td><span title="'+dis.unit+'" style="width:60px;">'+dis.unit+'</span></td>';
					tr = tr + '<td><span style="width:60px;"><input class="nextclass" title="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" type="text" style="width: 60px;text-align: right;" value="'+((undefined==dis.amountin)?"":dis.amountin.toFixed(2))+'" onfocus="this.select()" onblur="saveAmountin(this,'+dis.id+','+dis.unitper+','+dis.unitper3+')" onkeyup="validate(this)"/></span></td>';
					if(dis.ynprice == 'N'){//有报价的不能修改
						tr = tr + '<td class="textInput"><span style="width:60px;"><input class="nextclass" title="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" type="text" style="width: 60px;text-align: right;" value="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'"" onfocus="this.select()" onblur="savePricein(this,'+dis.id+')" onchange="checkPrice(this,'+14+')" onkeyup="validate(this)"/></span></td>';
					}else{
						if('${accountDis}' && '${accountDis}' != null && '${accountDis}' != '' && '${accountDis.ynzp}' == 'Y'){
							tr = tr + '<td class="textInput"><span style="width:60px;"><input class="nextclass" title="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'" type="text" style="width: 60px;text-align: right;" value="'+((undefined==dis.pricein)?"":dis.pricein.toFixed(2))+'"" onfocus="this.select()" onblur="savePricein(this,'+dis.id+')" onchange="checkPrice(this,'+14+')" onkeyup="validate(this)"/></span></td>';
						}else{
							tr = tr + '<td><span style="width:60px;text-align:right;" title="'+dis.pricein.toFixed(2)+'">'+dis.pricein.toFixed(2)+'</span></td>';
						}
					}
					tr = tr + '<td class="textInput"><span style="width:60px;"><input class="nextclass" title="'+((undefined==dis.pricesale)?"":dis.pricesale.toFixed(2))+'" type="text" style="width: 60px;text-align: right;" value="'+((undefined==dis.pricesale)?"":dis.pricesale.toFixed(2))+'"" onfocus="this.select()" onblur="savePricesale(this,'+dis.id+')" onchange="checkPrice(this,'+15+')" onkeyup="validate(this)" /></span></td>';
					tr = tr + '<td><span style="width:60px;text-align: right;">'+ Number(dis.pricein * dis.amountin).toFixed(2)+'</span></td>';
					//参考单位
					tr = tr + '<td><span title="'+dis.unit1+'" style="width:60px;">'+dis.unit1+'</span></td>';
					tr = tr + '<td><span style="width:60px;"><input class="nextclass" title="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" type="text" style="width: 60px;text-align: right;" value="'+((undefined==dis.amount1in)?"":dis.amount1in.toFixed(2))+'" onfocus="this.select()" onblur="saveAmount1in(this,'+dis.id+','+dis.unitper+','+dis.unitper3+')" onkeyup="validate(this)"/></span></td>';
					tr = tr + '<td><span title="'+dis.inout+'" style="width:60px;display: none;">'+dis.inout+'</span>'
					if(dis.inout == 'in'){
						tr = tr + '<span title="<fmt:message key="storage"/>" style="width:60px;"><fmt:message key="storage"/></span></td>';
					}else if(dis.inout == 'out'){
						tr = tr + '<span title="<fmt:message key="library"/>" style="width:60px;"><fmt:message key="library"/></span></td>';
					}else if(dis.inout == 'inout'){
						tr = tr + '<span title="<fmt:message key="straight_hair"/>" style="width:60px;"><fmt:message key="straight_hair"/></span></td>';
					}else{
						tr = tr + '<span title="<fmt:message key="direct"/>" style="width:60px;"><fmt:message key="direct"/></span></td>';
					}
					
					tr = tr + '<td class="textInput"><span style="width:70px;"><input title="'+((undefined==dis.ind.time)?"":new Date(dis.ind.time).format("yyyy-mm-dd"))+'"" type="text" class="Wdate text" value="'+((undefined==dis.ind.time)?"":new Date(dis.ind.time).format("yyyy-mm-dd"))+'"  onfocus="this.select()" onblur="saveInd(this,'+dis.id+')" onkeypress="selectInd(this)"/></span></td>';
					tr = tr + '<td><span title="'+dis.cnt+'" style="width:60px;text-align:right;">'+((undefined==dis.cnt)?"":dis.cnt.toFixed(2))+'</span></td>';
					tr = tr + '<td><span style="width:60px;text-align:center;"><input type="hidden" id="qr_'+dis.id+'" value="'+dis.chk1+'"/><img src="<%=path%>/image/kucun/'+dis.chk1+'.png"/></span></td>';
					tr = tr + '<td><span title="'+dis.chk1emp+'" style="width:60px;">'+dis.chk1emp+'</span></td>';
					tr = tr + '<td><span title="'+dis.chk1tim+'" style="width:120px;">'+dis.chk1tim+'</span></td>';
					tr = tr + '<td><span style="width:60px;text-align:center;"><img src="<%=path%>/image/kucun/'+dis.chksend+'.png"/></span></td>';
					tr = tr + '<td><span style="width:60px;text-align:center;" title="'+dis.sta+'"><img src="<%=path%>/image/kucun/'+dis.sta+'.png"/></span></td>';
					var memo = dis.memo;
					var memo1 = '';
					if(''!=memo && null!=memo){
						if(memo.indexOf('##')!=-1){
							var memos = dis.memo.split('##');
							memo = memos[0];
							memo1 = memos[1];
						}
					}
					tr = tr + '<td><span title="'+memo+'" style="width:${scm_project=="dttc"?"300px":"70px"};">'+memo+'</span></td>';
					if(dis.sp_per1 != 0 &&　(dis.inout == 'in' || dis.inout == 'inout')){//需要生产日期
						tr = tr + '<td class="textInput"><span style="width:70px;"><input title="'+((undefined==dis.dued || dis.dued == '')?"":new Date(dis.dued.time).format("yyyy-mm-dd"))+'"" type="text" class="Wdate text" value="'+((undefined==dis.dued || dis.dued == '')?"":new Date(dis.dued.time).format("yyyy-mm-dd"))+'" onfocus="this.select()" onblur="saveDued(this,'+dis.id+')" onkeypress="selectDued(this)"/></span></td>';
					}else{
						tr = tr + '<td><span style="width:70px;"></span></td>';
					}
					if(dis.inout == 'in' || dis.inout == 'inout'){//批次号
						tr = tr + '<td class="textInput"><span style="width:60px;"><input title="'+dis.pcno+'" type="text" value="'+dis.pcno+'" onfocus="this.select()" onblur="savePcno(this,'+dis.id+')" /></span></td>';
					}else{
						tr = tr + '<td><span style="width:60px;"></span></td>';
					}
					tr = tr + '<td><span title="'+dis.chkstoNo+'" style="width:70px;">'+dis.chkstoNo+'</span></td>';//报货单号
					tr = tr + '<td><span title="'+memo1+'" style="width:70px;">'+memo1+'</span></td>';//附加项
					tr = tr + '<td><span title="'+dis.deliveryn+'" style="width:60px;text-align:center;"><img src="<%=path%>/image/kucun/'+dis.deliveryn+'.png"/></span></td>';
					
					//是否生成采购订单js
					if(rs.isOrNOtRelationCG=='1'){
						tr = tr + '<td><span title="'+dis.puprorderemp+'" style="width:60px;">'+dis.puprorderemp+'</span></td>';//订单生成人
						tr = tr + '<td><span title="'+dis.puprordertim+'" style="width:120px;">'+dis.puprordertim+'</span></td>';//订单生成时间
						tr = tr + '<td><span style="width:50px;text-align:center;" title="'+dis.ispuprorder+'"><img src="<%=path%>/image/kucun/'+dis.ispuprorder+'.png"/></span></td>';
					}
					
					tr = tr + '</tr>';
					$(".grid .table-body tbody").append($(tr));
				}
				if(rs.over!='over'){
					condition['currPage']=++currPage;
					returnInfo = true;
				}
				if(parent.bhfbEditState!='edit'){
					$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				}else{
					new tabTableInput("table-body","text"); //input  上下左右移动	
				}
				
				//绑定行点击事件背景颜色改变
				bindTrClick();
			});
		}
		Date.prototype.format = function(){	
			var yy = String(this.getFullYear());
			var mm = String(this.getMonth() + 1);
			var dd = String(this.getDate());
			if(mm<10){
				mm = ''+0+mm;
			}
			if(dd<10){
				dd = ''+0+dd;
			}
			var str = yy+"-"+mm+"-"+dd;
			return str;
		};
		
		//导出excel
		function exportExcel(){
			$("#wait2").val("NO");
			var action = '<%=path%>/disFenBo/exportExcel.do';	
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			var action1="<%=path%>/disFenBo/list.do";
			$('#listForm').attr('action',action1);
			$("#wait2").val("");
		}
		
		//数据行点击背景颜色改变
		function bindTrClick(){
			$("#grid-body-div tbody tr").bind('click',function(){
				$("#grid-body-div tbody tr").css("background","");
				$(this).css("background","#E6EDF2");
			});
		}
		</script>
	</body>
</html>