<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="Acceptance_to_library"/>验收出库</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			.table-head td span{
				white-space: normal;
			}
			.form-line .form-label{
				width: 9%;
			}
		</style>
	</head>
	<body>	
		<div class="tool"></div>
		<%--验收出库返回的信息 --%>
		<input type="hidden" id="chkMsg" name="chkMsg" value="${chkMsg }"/>
		<form id="listForm" action="<%=path%>/disChuKu/listCheckout.do" method="post">
			<%-- curStatus 0:init;1:edit;2:add;3:show --%>
			<input type="hidden" id="action" name="action" value="<c:out value="${action}" default="init"/>" />
			<div class="bj_head">
				<input type="hidden" id="inout" name="inout" value="${dis.inout }"/>
	<!-- 			<input type="hidden" id="ysckStr" name="ysckStr"/> -->
				     	
				<div class="form-line">
					<div class="form-label"><fmt:message key="reported_goods_stores"/></div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
						<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode }"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
					</div>
			        <div class="form-label">
	 			    	<fmt:message key="category_selection"/>
			        </div>
			        <div class="form-input">
				        <input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
						<input type="hidden" id="typCode" name="typCode" value="${dis.typCode }"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
			        </div>
			        <div class="form-label"><fmt:message key ="distribution_area" /></div>
					<div class="form-input">
						<select class="select" name="psarea">
							<option value=""></option>
							<c:forEach var="codeDes" items="${codeDesList }">
								<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
							</c:forEach>
						</select>
					</div>
			    </div> 
				<div class="form-line">	
					<div class="form-label">
			            <fmt:message key="library_positions"/>
					</div>
					<div class="form-input" style="width:170px;">
						<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
						<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
<%-- 					<div class="form-label"><fmt:message key="document_date"/></div> --%>
					<div class="form-label"><fmt:message key ="Date_of_delivery" /></div>
					<div class="form-input">
				    	<input type="text" style="width:133px;" id="maded1" name="maded1" class="Wdate text" value="<fmt:formatDate value="${maded1}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>	    				
					</div>
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="arrival_date"/></div>
					<div class="form-input">
		                <input type="text" id="ind" name="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
		            	&nbsp;<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
						<font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>            					    
				    </div>
				</div>   
			</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td ><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td ><span style="width:60px;"><fmt:message key="requisitioning_company"/></span></td>								
								<td ><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td ><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td ><span style="width:40px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:40px;"><fmt:message key="receive_goods"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="suppliers"/></span></td>
								<td ><span style="width:100px;"><fmt:message key="suppliers_name"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="direction"/></span></td>								
								<td ><span style="width:40px;"><fmt:message key="remark"/></span></td>
								<td ><span style="width:30px;"><fmt:message key ="whether_storage" /></span></td>
								<td ><span style="width:30px;"><fmt:message key="whether_library"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="current_inventory"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="inventory_sufficient"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr <c:if test="${dis.amountin>dis.cnt}">style="color:red;"</c:if>>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
										<input type="hidden" name="idList" id="chk_${dis.id}" value="${dis.id}"/>
									</td>
									<td ><span style="width:45px;" title="${dis.id }">${dis.id }</span></td>
									<td ><span style="width:60px;" title="${dis.firmDes }" >${dis.firmDes }</span></td>
									<td ><span style="width:65px;" title="${dis.sp_code }" >${dis.sp_code }</span></td>
									<td ><span style="width:100px;" title="${dis.sp_name }" >${dis.sp_name }</span></td>
									<td ><span style="width:50px;" title="${dis.sp_desc }" >${dis.sp_desc }</span></td>
									<td ><span style="width:30px;" title="${dis.unit }" >${dis.unit }</span></td>
									<td ><span style="width:40px;" title="<fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/>" ><fmt:formatNumber value="${dis.amount}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:40px;" title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" ><fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:40px;" title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" ><fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:50px;text-align: right;" title="<fmt:formatNumber value="${dis.pricesale * dis.amountin}" type="currency" pattern="0.00"/>" ><fmt:formatNumber value="${dis.pricesale * dis.amountin}" type="currency" pattern="0.00"/></span></td>
									<td><span style="width:50px;" title="${dis.deliverCode }" >${dis.deliverCode }</span></td>
									<td><span style="width:100px;" title="${dis.deliverDes }" >${dis.deliverDes }</span></td>
									<td><span style="width:30px;" title="${dis.inout }" >${dis.inout }</span></td>
									<td><span style="width:40px;" title="${dis.memo}" >${dis.memo}</span></td>
									<td>
										<span style="width:30px;text-align:center;">
										<img src="<%=path%>/image/kucun/${dis.chkin}.png"/>
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${dis.chkin=='Y' }"> --%>
<%-- 													<img src="<%=path%>/image/themes/icons/ok.png"/> --%>
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<%-- 													<img src="<%=path%>/image/themes/icons/no.png"/> --%>
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
										</span>
									</td>
									<td>
										<span style="width:30px;text-align:center;">
										<img src="<%=path%>/image/kucun/${dis.chkout}.png"/>
<%-- 											<c:choose> --%>
<%-- 												<c:when test="${dis.chkout=='Y' }"> --%>
<%-- 													<img src="<%=path%>/image/themes/icons/ok.png"/> --%>
<%-- 												</c:when> --%>
<%-- 												<c:otherwise> --%>
<%-- 													<img src="<%=path%>/image/themes/icons/no.png"/> --%>
<%-- 												</c:otherwise> --%>
<%-- 											</c:choose> --%>
										</span>
									</td>
									<td><span style="width:50px;"><fmt:formatNumber value="${dis.cnt}" type="currency" pattern="0.00"/></span></td>
									<td>
										<span style="width:30px;text-align:center;">
											<c:choose>
												<c:when test="${dis.amountin<=dis.cnt}">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">	
		$(document).ready(function(){				
		    chkMsg();
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 67: $('#autoId-button-102').click(); break;
	                case 65: $('#autoId-button-103').click(); break;
	                case 68: $('#autoId-button-104').click(); break;
	                case 69: $('#autoId-button-105').click(); break;
	                case 80: $('#autoId-button-106').click(); break;
	            }
			}); 
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			setElementHeight('.table-body',['.table-head'],'.grid');
			//判断按钮的显示与隐藏
			var status = $("#action").val();
			if(status == 'init'){
				loadToolBar([true,false,true,false]);
			}else if(status == 'zfcx'){//库存查询
				loadToolBar([true,true,true,false]);
			}else if(status == 'ckcx'){//出库查询
				loadToolBar([true,false,true,true]);
			}
				//自动实现滚动条 				
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$("#seachPositn").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#firmCode").val(),
						single:false,
						tagName:'firmDes',
						tagId:'firmCode',
						title:'<fmt:message key="please_select_positions"/>'
					});
				});
				
				$("#seachOnePositn").click(function(){
					chooseStoreSCM({
						basePath:'<%=path%>',
						width:600,
						firmId:$("#positnCode").val(),
						single:false,
						tagName:'positnDes',
						tagId:'positnCode',
						title:'<fmt:message key="please_select_positions"/>'
					});
				});
				
				$('#seachTyp').bind('click.custom',function(e){
					if(!!!top.customWindow){
						var defaultCode = $('#typCode').val();
						var defaultName = $('#typDes').val();
						var offset = getOffset('ind');
						top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
					}
				});
		});
		
		//按钮控制  点直发查询后，库存出库按钮灰化，点出库查询时，直发出库按钮灰化  wjf
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
							text: '<fmt:message key ="Query_out" />',
							title: '<fmt:message key ="Query_out" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')} && use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-20px','-40px']
							},
							handler: function(){
								chkoutFormSubmit2();
							}
						},{
							text: '<fmt:message key="in_stock_new"/>',
							title: '<fmt:message key="in_stock_new"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')} && use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-20px']
							},
							handler: function(){
								if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_library"/>？')){
									ChkstomKcck();
								}
							}
						},{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printYsckChkstom();	
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]

			});
		}
		//生成出库单的状态
		function chkMsg(){
			var chkMsg=$("#chkMsg").val();
			if(null!=chkMsg && ""!=chkMsg){
				alert(chkMsg);
				var action = '<%=path%>/disChuKu/listCheckout.do';
				$('#listForm').attr('action',action);
// 				$('#listForm').submit();
			}
		}
		//库存出库
		function ChkstomKcck(){
// 			$('#ysckStr').val('kcck');
			$('#inout').val('<fmt:message key="library"/>');
// 			$('#listForm').submit();
			var action="<%=path%>/disChuKu/saveYsckChkout.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//出库查询
		function chkoutFormSubmit2(){
			$('#inout').val('<fmt:message key="library"/>');
			$('#action').val('ckcx');
			var action = '<%=path%>/disChuKu/listCheckout.do';			
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//打印
		function printYsckChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disChuKu/viewYsckChkstom.do';	
			var action1="<%=path%>/disChuKu/listCheckout.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val('');//等待加载还原
     	}
		</script>
	</body>
</html>