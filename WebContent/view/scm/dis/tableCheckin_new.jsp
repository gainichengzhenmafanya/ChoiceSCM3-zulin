<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="acceptance_to_storage"/>验收入库</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
		.page{margin-bottom: 25px;}
		.search{
			margin-top:-2px;
			cursor: pointer;
		}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<!-- 验收入库返回的信息 -->
		<input type="hidden" id="chkMsg" name="chkMsg" value="${chkMsg}"/>
		<form id="listForm" action="<%=path%>/disRuKu/listCheckin.do" method="post" target="">
			<div class="bj_head">
				<input type="hidden" id="select_positn" value="${dis.positnCode}"/>
	<!-- 			<input type="hidden" id="ysrkStr" name="ysrkStr"/>	 -->
				<div class="form-line">	
					<div class="form-label"><font style="vertical-align:middle;" color="red" size="3"><b>*</b></font><fmt:message key="position_select"/></div>
					<div class="form-input" style="width:15%;">
						<input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
						<input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="psarea"/></div>
					<div class="form-input" style="width:15%;">
						<select class="select" name="psarea">
							<option value=""></option>
							<c:forEach var="codeDes" items="${codeDesList }">
								<option <c:if test="${dis.psarea==codeDes.code }"> selected="selected" </c:if> value="${codeDes.code }">${codeDes.des }</option>
							</c:forEach>
						</select>
					</div>	
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" value="<fmt:formatDate value="${dis.bdat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>		
					<div class="form-label"></div>
					<div class="form-input">
						<input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked" </c:if> id="ind1" name="ind1" value="ind"/>
				        <font style="color:blue;"><fmt:message key="unknown_arrival_date"/></font>
					</div>	
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="supply_unit"/></div>
					<div class="form-input" style="width:15%;">
						<input type="text"  id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
						<input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label"><fmt:message key="note_type"/></div>
					<div class="form-input" style="width:15%;">
					     <select class="select" id="memo1" name="memo1">
					        		 <option value=""></option>
					                 <option
					                 <c:if test="${dis.memo1=='不合格' }">
					                 selected="selected"
					                 </c:if>
					                  value="不合格"><fmt:message key="unqualified"/></option>
					        	 </select>
					</div>
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${dis.edat}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="reported_num"/></div>
					<div class="form-input"><input type="text" id="chkstoNo" name="chkstoNo" class="text" value="${dis.chkstoNo }" /></div>	
				</div>
				<div class="form-line">	
					<div class="form-label"><fmt:message key="requisitioning_company"/></div>
					<div class="form-input" style="width:15%;">
						<input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
						<input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
						<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					</div>
					<div class="form-label"><fmt:message key="category_selection"/></div>
					<div class="form-input" style="width:15%;">
						<input type="text"  id="typDes" name="typDes" readonly="readonly" value="${dis.typDes}"/>
						<input type="hidden" id="typCode" name="typCode" value="${dis.typCode}"/>
						<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_category"/>' />
					</div>
					<div class="form-label"><fmt:message key="document_date"/></div>
					<div class="form-input">
						 <input type="text" id="maded1" name="maded1" class="Wdate text" value="<fmt:formatDate value="${maded1}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
					</div>						
				</div>
			</div>
		    <div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" ><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width: 30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td rowspan="2" ><span style="width:45px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" ><span style="width:65px;"><fmt:message key="reported_num" /></span></td>
								<td rowspan="2" ><span style="width:65px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key="supplies_name"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="suppliers"/></span></td>
								<td rowspan="2" ><span style="width:80px;"><fmt:message key="suppliers_name"/></span></td>
								<td rowspan="2" ><span style="width:120px;"><fmt:message key="requisitioning_company"/></span></td>
								<td colspan="3" ><fmt:message key="reports"/></td>
								<td colspan="6" ><fmt:message key="deliver_goods"/></td>
								<td rowspan="2" ><span style="width:30px;"><fmt:message key="direction"/></span></td>
								<td rowspan="2" ><span style="width:40px;"><fmt:message key="remark"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="whether_storage"/></span></td>
								<td rowspan="2" ><span style="width:70px;"><fmt:message key="production_date"/></span></td>
								<td rowspan="2" ><span style="width:50px;"><fmt:message key="pc_no"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
							</tr>
							<tr>
								<td ><span style="width:30px;"><fmt:message key="procurement_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="reports"/><br/><fmt:message key="quantity"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="adjustment_amount"/></span></td>
								
								<td ><span style="width:30px;"><fmt:message key="standard_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td ><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td ><span style="width:30px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td ><span style="width:50px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>		
							<c:forEach var="dis" items="${disList }" varStatus="status">
								<tr>
									<td class="num" >
										<span style="width: 25px;">${status.index+1}</span>
									</td>
									<td>
										<span style="width:30px; text-align: center;">
											<input type="checkbox"  name="idList" id="chk_${dis.id}" value="${dis.id}" sp_per1 = "${dis.sp_per1 }" ynbatch="${dis.ynbatch }"/>
										</span>
									</td>
									<td title="${dis.id }"><span style="width:45px;">${dis.id }</span></td>
									<td title="${dis.chkstoNo }"><span style="width:65px;text-align:right;">${dis.chkstoNo }</span></td>
									<td title="${dis.sp_code }"><span style="width:65px;">${dis.sp_code }</span></td>
									<td title="${dis.sp_name }"><span style="width:100px;">${dis.sp_name }</span></td>
									<td title="${dis.sp_desc }"><span style="width:50px;">${dis.sp_desc }</span></td>
									<td title="${dis.deliverCode }"><span style="width:40px;">${dis.deliverCode }</span></td>
									<td title="${dis.deliverDes }"><span style="width:80px;">${dis.deliverDes }</span></td>
									<td title="${dis.firmDes }"><span style="width:120px;">${dis.firmDes }</span></td>
									<td title="${dis.unit3 }"><span style="width:30px;">${dis.unit3 }</span></td>
									<td title="<fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span></td>
									<!-- update by lbh -->
									<td title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','tzshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','tzshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="tzshuliang" value="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>"/></td>
									<td title="${dis.unit }"><span style="width:30px;">${dis.unit }</span></td>
									<!-- update by lbh -->
									<td title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','bzshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','bzshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="bzshuliang" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>"/></td>
									<td title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>"><span style="width:40px;text-align:right"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
									<td title="<fmt:formatNumber value="${dis.amountin*dis.pricein}" type="currency" pattern="0.00"/>"><span style="width:50px;text-align:right"><fmt:formatNumber value="${dis.pricein*dis.amountin}" pattern="##.##" minFractionDigits="2" /></span></td>
									<td title="${dis.unit1 }"><span style="width:30px;">${dis.unit1 }</span></td>
									<!-- update by lbh -->
									<td title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>"><input onkeyup="validatee(this,'${status.index}','ckshuliang','${dis.unitper}','${dis.unitper3}');" onblur="validatee(this,'${status.index}','ckshuliang','${dis.unitper}','${dis.unitper3}');" style="width:56px;text-align:right" type="text" disabled class="ckshuliang" value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>"/></td>
									<td title="${dis.inout }"><span style="width:30px;">
										<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
										<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
										<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
										<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>
									</span></td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:40px;" title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:40px;" title="${dis.memo}">${dis.memo}</span></td>
										</c:otherwise>
									</c:choose>
									<td>
										<span style="width:50px;text-align:center">
											<c:choose>
												<c:when test="${dis.chkin=='Y' }">
													<img src="<%=path%>/image/themes/icons/ok.png"/>
												</c:when>
												<c:otherwise>
													<img src="<%=path%>/image/themes/icons/no.png"/>
												</c:otherwise>
											</c:choose>
										</span>
									</td>
									<td><span style="width:70px;" title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"><fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:50px;" title="${dis.pcno}">${dis.pcno}</span></td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		
		//焦点离开检查输入是否合法
		function validatee(e,line,type,per,per3){
			if(null==e.value || ""==e.value){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot_be_empty"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			if(Number(e.value)<0 || isNaN(e.value)){
				e.value=e.defaultValue;
				showMessage({
							type: 'error',
							msg: '<fmt:message key="not_valid_number"/>！',
							speed: 1000
							});
				$(e).focus();
				return;
			}
			
			//值
			var value = e.value;
			
			//采购数量，标准数量，参考数量
			if('tzshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value);
				if(per3!='0'&&per3!=undefined&&$.trim(per3)!=''&&!isNaN(per3)){
					$('.table-body tr:eq('+line+') td:eq(14) input').val((value/per3).toFixed(2));
					$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value/per3);
					if(per!=0&&per!=undefined&&$.trim(per)!=''&&!isNaN(per)){
						$('.table-body tr:eq('+line+') td:eq(18) input').val((value/per3*per).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value/per3*per);
					}
				}
			}else if('bzshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value);
				if(!isNaN(per3)){
					if(per3!='0'){
						$('.table-body tr:eq('+line+') td:eq(12) input').val((value*per3).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value*per3);
					}
				}
				if(!isNaN(per)){
					if(per!='0'){
						$('.table-body tr:eq('+line+') td:eq(18) input').val((value*per).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value*per);
					}
				}
			}else if('ckshuliang'==type){
				$('.table-body tr:eq('+line+') td:eq(18)').attr('title',value);
				if(per!='0'&&per!=undefined&&$.trim(per)!=''&&!isNaN(per)){
					$('.table-body tr:eq('+line+') td:eq(14) input').val((value/per).toFixed(2));
					$('.table-body tr:eq('+line+') td:eq(14)').attr('title',value/per);
					if(per3!=0&&per3!=undefined&&$.trim(per3)!=''&&!isNaN(per3)){
						$('.table-body tr:eq('+line+') td:eq(12) input').val((value/per*per3).toFixed(2));
						$('.table-body tr:eq('+line+') td:eq(12)').attr('title',value/per*per3);
					}
				}
			}

			//金额修改
			var price = $('.table-body tr:eq('+line+') td:eq(15) span').html();
			var amt = $('.table-body tr:eq('+line+') td:eq(14) input').val();
			var jine = price * amt;
			$('.table-body tr:eq('+line+') td:eq(16) span').html(jine.toFixed(2));
			$('.table-body tr:eq('+line+') td:eq(16)').attr('title',jine);
			
			<%--//判断库存是否充足
			//得到库存
			 var kcnum = $('.table-body tr:eq('+line+') td:eq(22) span').html();
			
			//得到标准数量
			var bznum = $('.table-body tr:eq('+line+') td:eq(14) input').val();
			
			//判断是否充足
			if(Number(bznum)>Number(kcnum)){
				//不充足
				$('.table-body tr:eq('+line+') td:eq(23) span img').attr('src','<%=path%>/image/themes/icons/no.png');
				//改变字体颜色
				$('.table-body tr:eq('+line+')').css('color','red');
			}else{
				//充足
				$('.table-body tr:eq('+line+') td:eq(23) span img').attr('src','<%=path%>/image/themes/icons/ok.png');
				//改变字体颜色
				$('.table-body tr:eq('+line+')').css('color','black');
			} --%>
		}
		
		var validate;
		$(document).ready(function(){
		   	//生成入库单时的状态
		   	chkMsg();
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('#autoId-button-101').click(); break;
	                case 65: $('#autoId-button-102').click(); break;
	                case 80: $('#autoId-button-103').click(); break;
	            }
			}); 
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			var action = '${action}';
			if(action=='init'){
				loadToolBar([false]);
			}else{
				loadToolBar([true]);
			}
			
			//验证
			validate = new Validate({
				validateItem:[
				{
					type:'text',
					validateObj:'chkstoNo',
					validateType:['num1'],
					param:['T'],
					error:['<fmt:message key="reported_num" /><fmt:message key="please_fill_in_figures" />！']
				}]
			});
			
			//提交前验证
			function validate_beforesm(){
				return validate._submitValidate();
			}
			
			function loadToolBar(use){
			  $('.tool').html('');
			  $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="query_storage"/>',
						title: '<fmt:message key="query_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','-40px']
						},
						handler: function(){
							if(!$("#positnCode").val()){
								alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
								/* showMessage({
									type: 'error',
									msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
									speed: 1000
								}); */
							}else{
								//验证报货单号
								if(validate_beforesm()){
									$('#listForm').submit();
								}
							}
						}
					},{
						text: '<fmt:message key="edit"/>',
						title: '<fmt:message key="edit"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')} && use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							//判断有无数据
							var size = $(".table-body tr").size();
							if(size==0){
								alert('<fmt:message key="no_data"/>！！！');
								return;
							}
							//可编辑状态
							$('.bzshuliang').removeAttr("disabled");
							$('.tzshuliang').removeAttr("disabled");
							$('.ckshuliang').removeAttr("disabled");
							$('.fjxselect').removeAttr("disabled");
							//加载按钮
							loadToolBar([true,false,true,true,true,false]);
						}
					},{
						text: '<fmt:message key="save"/>',
						title: '<fmt:message key="save"/>',
						useable: use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							//组装集合
							var list = new Array();
							$(".table-body tr").each(function(i){
								//i 0开始
								var id = $(this).find("td:eq(2) span").html();
								//var amountin = $(this).find("td:eq(14) input").val();
								//var amount1in = $(this).find("td:eq(18) input").val();
								//var amount1sto = $(this).find("td:eq(12) input").val();
								var amountin = $(this).find("td:eq(14)").attr('title');
								var amount1in = $(this).find("td:eq(18)").attr('title');
								var amount1sto = $(this).find("td:eq(12)").attr('title');
								var fjx = $(this).find("td:eq(25) select").val();
								//对象
								list[i] = new Object();
								list[i]['id']=id;
								//标准数量
								list[i]['amountin']=amountin;
								//参考数量
								list[i]['amount1in']=amount1in;
								//采购数量
								list[i]['amount1sto']=amount1sto;
								//附加项
								list[i]['memo']=fjx;
							});
							//转换为json字符串
							var jsonStr = JSON.stringify(list);
							var data = "listStr="+jsonStr;
							//提交保存数量，判断执行状态
							$.ajax({
								url:"<%=path%>/disChuKu/updateChkoutNum.do",
								type:"post",
								data:data,
								success:function(msg){
									switch(msg){
									case '1':
										alert('<fmt:message key="update_fail" />');
										$('#listForm').submit();
										break;
									case '0':
										alert('<fmt:message key ="update_successful" />！');
										$('.tzshuliang').attr("disabled",true);
										$('.bzshuliang').attr("disabled",true);
										$('.ckshuliang').attr("disabled",true);
										$('.fjxselect').attr("disabled",true);
									}
								}
							});
							//按钮显示控制
							loadToolBar([true,true,false,true,true,true]);
						}
					},{
						text: '<fmt:message key="acceptance_to_storage"/>',
						title: '<fmt:message key="acceptance_to_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&& use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','-20px']
						},
						handler: function(){
							//先判断仓库有没有期初
							$.ajax({
								url:"<%=path%>/positn/checkQC.do?code="+$('#positnCode').val(),
								type:"post",
								success:function(data){
									if(data){
										if(!$("#positnCode").val()){
											alert('<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！');
											/* showMessage({
												type: 'error',
												msg: '<fmt:message key="please_select_positions_to_query_and_acceptance_storage"/>！',
												speed: 1000
											}); */
										}else{
											if($('#select_positn').val() != $('#positnCode').val()){
												if(confirm("<fmt:message key='the_position_is_not_consistent'/>，<fmt:message key='whether'/><fmt:message key='continue'/>？")){
													if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_storage"/>？')){
														ChkstomYsrk();
													}
												}
											}else{
												if(confirm('<fmt:message key="make_sure_the_input_be_correct_to_generate_storage"/>？')){
													ChkstomYsrk();
												}
											}
										}
									}else{
										alert($('#positnCode').val()+'<fmt:message key="the_storage_positions_do_not_at_the_beginning_of_the_period"/>！<fmt:message key="can_not"/><fmt:message key="storage"/>！');
										return;
									}
								}
							});
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							printYsrkChkstom();	
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
			}
			//自动实现滚动条 				
			setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('positnCode');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					var defaultName = $('#typDes').val();
					var offset = getOffset('positnCode');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectOneGrpTyp.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			
			$("#seachOnePositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positnCode").val(),
					single:true,
					tagName:'positnDes',
					tagId:'positnCode'
				});
			});
			
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#firmCode').val();
					var defaultName = $('#firmDes').val();
					var offset = getOffset('positnCode');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#firmDes'),$('#firmCode'),'760','520','isNull');
				}
			});
		});
		//生成入库单的状态
		function chkMsg(){
			var chkMsg=$("#chkMsg").val();
			if(Number(chkMsg==2)){
				alert('<fmt:message key="empty_data_cannot_to_store"/>！');
				var action="<%=path%>/disRuKu/listCheckin.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			if(Number(chkMsg==1)){
				alert('<fmt:message key="storage_completed_to_query_documents"/>！');
				var action="<%=path%>/disRuKu/listCheckin.do";
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		}
		//验收入库
		function ChkstomYsrk(){
// 			$('#ysrkStr').val('ysrk');//验收入库
// 			$('#listForm').submit();
			//改为先判断有没有选择物资  wjf
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() == 0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			var isNull=0;
			var codes = [];
			var flag = true;//判断该有生产日期但没有的报货单
			var flag1 = true;//用来判断有没有批次号
			var sp_code = "";
			if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					isNull=1;
					var amount = Number($(this).parents('tr').find('td:eq(14)').attr('title'));//标准数量
					var amount1 = Number($(this).parents('tr').find('td:eq(18)').attr('title'));//参考数量
					if(amount==0 || amount==0.0 || amount==0.00 || amount1==0 || amount1==0.0 || amount1==0.00){
						isNull=2;
						sp_code = $(this).parents('tr').find('td:eq(5)').attr('title');
						return false;
					}
					var sp_per1 = $(this).attr('sp_per1');
					var dued = $(this).parents('tr').find('td:eq(22) span').attr('title');
					if(sp_per1 != 0 && dued == ''){//如果没有生产日期
						flag = false;
						sp_code = $(this).parents('tr').find('td:eq(5)').attr('title');
						return false; 
					}
					var ynbatch = $(this).attr('ynbatch');
					var pcno = $(this).parents('tr').find('td:eq(23) span').attr('title');
					if(ynbatch == 'Y' && pcno == ''){//如果没有批次号
						flag1 = false;
						sp_code = $(this).parents('tr').find('td:eq(5)').attr('title');
						return false; 
					}
					codes.push($(this).val());
				});
			}
			if(isNull==0){
				alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
				return;
			}
			if(isNull==2){
				alert('<fmt:message key="supplies"/>:['+sp_code+']<fmt:message key="scm_standard_quantity"/><fmt:message key="or"/><fmt:message key="scm_the_reference_number"/><fmt:message key="can_not"/>0！<fmt:message key="please_return_the_report_distribution_of_goods"/><fmt:message key="update"/>！');
				return;
			}
			if(!flag){
				alert('<fmt:message key="supplies"/>:['+sp_code+']<fmt:message key="supplies"/><fmt:message key="supplies"/><fmt:message key="there_is_no"/><fmt:message key="production_date"/>,<fmt:message key="please_return_the_report_distribution_of_goods"/><fmt:message key="insert"/>！');
				return;
			}
			if(!flag1){
				alert('<fmt:message key="supplies"/>:['+sp_code+']<fmt:message key="supplies"/><fmt:message key="supplies"/><fmt:message key="there_is_no"/><fmt:message key="pc_no"/>,<fmt:message key="please_return_the_report_distribution_of_goods"/><fmt:message key="insert"/>！');
				return;
			}
			var action="<%=path%>/disRuKu/saveYsrkChkinm.do?inCondition="+codes.join(',');
			$('#listForm').attr('action',action);
			$('#listForm').submit();
		}
		//打印
		function printYsrkChkstom(){
			$("#wait2").val('NO');//不用等待加载
			$('#listForm').attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
			var action = '<%=path%>/disRuKu/viewYsrkChkstom.do';
			var action1="<%=path%>/disRuKu/listCheckin.do";
			$('#listForm').attr('action',action);
			$('#listForm').submit();
			$('#listForm').attr('action',action1);
			$('#listForm').attr('target','');
			$("#wait2").val('');//等待加载还原
     	}
		</script>
	</body>
</html>