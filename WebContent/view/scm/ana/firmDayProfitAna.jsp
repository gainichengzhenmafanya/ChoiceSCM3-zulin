<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>分店日毛利分析</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/profitAna/firmDayProfit.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label"><fmt:message key="branche" /></div>
				<div class="form-input">
					<input type="text"  id="firmDes" class="text" name="firmDes" readonly="readonly" value="${firmDes}"/>
					<input type="hidden" id="firm" name="firmDeptCode" value="${firmDeptCode}"/>
					<img id="seachOnePositn1" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="branches_selection"/>' />
				</div>
<%-- 				<div class="form-label"><fmt:message key="sector" /></div> --%>
<!-- 				<div class="form-input"> -->
<%-- 					<select id="firmDept" name="firmDeptCode" loadOnce="false" code="${firmDeptCode}" des="${firmDeptDes}" class="select" select="true" ></select> --%>
<%-- 					<input type="text" id="firmDeptDes" name="firmDeptDes" style="visibility: hidden;" value="${firmDeptDes }" /> --%>
<!-- 				</div> -->
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
<!-- 				<div class="form-input" style="margin-left: 50px;"> -->
<%-- 					<input type="radio" id="all" name="deptcheck" value="QB" <c:if test="${dept=='QB'}">checked="checked"</c:if> />全部部门 --%>
<%-- 					<input type="radio" id="cf" name="deptcheck" value="CF" <c:if test="${dept=='CF'}">checked="checked"</c:if>/>厨房部门 --%>
<!-- 				</div> -->
			</div>
			<div class="grid">
				<div class="table-head" >
					<table id="tblHead" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" ><span style="width:100px;"><fmt:message key ="date" /></span></td>
								<c:forEach var="firmDept" items="${firmDeptList}">
									<td colspan="3" >${firmDept.des}</td>
								</c:forEach>
								<td colspan="3" style="text-align: center;color: #0000ff;"><fmt:message key ="total" /></td>
							</tr>
							<tr>
								<c:forEach var="firmDept" items="${firmDeptList }" varStatus="status">
									<td><span style="width:80px;"><fmt:message key ="scm_sales" /></span></td>
									<td><span style="width:80px;"><fmt:message key ="use" /></span></td>
									<td><span style="width:80px;"><fmt:message key ="Gross_profit" /></span></td>
								</c:forEach>
								<td><span style="width:80px;color:blue;"><fmt:message key ="scm_sales" /></span></td>
								<td><span style="width:80px;color:blue;"><fmt:message key ="use" /></span></td>
								<td><span style="width:80px;color:blue;"><fmt:message key ="Gross_profit" /></span></td>
							</tr>
						</thead>
			</table>
				</div>
				<div class="table-body" style="height: 100%">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						</tbody>
					</table>					
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
//   	 		$("#firmDept").htmlUtils("select",{
//   	 			beforLoad:function(url){
//   	 				if(null!=$('#firmDes').val() && ""!=$('#firmDes').val())
//   	 				{
//   	 					$("#firmDept").attr("url","<%=path%>/firmDept/findFirmDept.do?firm="+$('#firm').val());
//   	 				}
//   	 			}
//   	 		});
			//按钮快捷键
			focus() ;//页面获得焦点	
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}
// 							if(null==$('#firmDes').val()||""==$('#firmDes').val()){
// 								alert('分店不能为空！');
// 								return;
// 							}
							$("#firmDeptDes").val(typeof($("#firmDept").data("checkedName"))!="undefined"?$("#firmDept").data("checkedName"):"");
							$('#listForm').submit();
						}
					},{
// 						text: 'Excel',
// 						title: 'Excel',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
// 						icon: {
// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-40px','-20px']
// 						},
// 						handler: function(){
// 							$('#queryForm').attr('action','<%=path%>/costProfitAna/exportGrossProfit.do?checkMis='+checkMis);
// 							$('#queryForm').submit();
// 						}
// 					},{
// 						text: '<fmt:message key="print" />',
// 						title: '<fmt:message key="print" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 						icon: {
// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
// 								$('#queryForm').attr('target','report');
// 								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
// 								var action="<%=path%>/costProfitAna/printGrossProfit.do?checkMis="+checkMis;
// 								$('#queryForm').attr('action',action);
// 								$('#queryForm').submit();								
// 							}else{
// 								showMessage({
// 									type: 'error',
// 									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
// 									speed: 1000
// 									});
// 								return ;
// 							}
// 						}
//					},{
						text: '<fmt:message key="quit" />',
 						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/prdPrcCostManage/reportList.do");
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		
  	 		$("#seachOnePositn1").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					typn:'1203',
					tagName:'firmDes',
					tagId:'firm',
					title:'<fmt:message key="branches_selection"/>'
				});
			});
  	 		
  	 		var obj='${dataList}';
  	 		if (obj!=null) {
	 			var data = eval('('+obj+')');
	 			var firmdept='${deptList}';
	 			var dept = eval('('+firmdept+')');
	 			var tbody=$('.table-body').find('tbody');
	 			var xsamtsum=0;
	 			var lyamtsum=0;
	 			var mlvsum=0;
// 	 			alert(data.val());
				for ( var i in data) {
					
					var tr=$('<tr></tr>');
					tr.append($('<td><span style="width:100px;text-align:left;">'+data[i]["DAT"]+'</span></td>'));
					for ( var j in dept) {
                        tr.append($('<td><span style="width:80px;text-align:right;">'+data[i]["XSAMT"+dept[j].code]+'</span></td>'));
                        tr.append($('<td><span style="width:80px;text-align:right;">'+data[i]["LYAMT"+dept[j].code]+'</span></td>'));
                        tr.append($('<td><span style="width:80px;text-align:right;">'+data[i]["MAOLILV"+dept[j].code]+'</span></td>'));
                        xsamtsum+=data[i]["XSAMT"+dept[j].code];
                        lyamtsum+=data[i]["LYAMT"+dept[j].code];
					}
					if(xsamtsum!=0){
						mlvsum=((xsamtsum-lyamtsum)/xsamtsum*100).toFixed(2);
					}else{
						mlvsum=(0).toFixed(2);
					}
                    tr.append($('<td><span style="width:80px;text-align:right;">'+xsamtsum.toFixed(2)+'</span></td>'));
                    tr.append($('<td><span style="width:80px;text-align:right;">'+lyamtsum.toFixed(2)+'</span></td>'));
                    tr.append($('<td><span style="width:80px;text-align:right;">'+mlvsum+'%</span></td>'));
                    xsamtsum=0;
					lyamtsum=0;
					mlvsum=0;
					if (i==0){
						tbody.clear;
						tbody.append(tr);
					}
					tbody.append(tr);
				}
			}
			setElementHeight('.grid',['.tool'],$(document.body),105);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
  	 	});
  	 </script>
  </body>
</html>
