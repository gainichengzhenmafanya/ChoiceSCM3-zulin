<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>耗用分店对比</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/useFirmCompare/findUseFirmCompare.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label" style="width: 55px;"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label" style="width: 25px;"><fmt:message key="branche" /></div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" value="${firmDes }" readonly="readonly" class="text" style="width: 136px;margin-bottom: 4px;"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${firmCode }"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
				</div>
				<div class="form-label"  style="width: 55px;"><fmt:message key="supplies_code"/></div>
				<div class="form-input">
					<input type="text" style="width: 136px;margin-bottom: 4px;" id="sp_code" name="sp_code" value="${supplyAcct.sp_code }" class="text"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>				
			</div>
			<div class="form-line">
				<div class="form-label" style="width: 55px;"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label" style="width: 25px;"><fmt:message key="category"/></div>
				<div class="form-input" style="margin-top: -3px;*margin-top:-2px;">
					<input type="hidden" id="typ" name="typ" value="${supplyAcct.typ}"/>
					<input type="text" id="typdes" name="typdes" readonly="readonly" value="${supplyAcct.typdes}" class="text" style="width: 135px;margin-bottom: 4px;"/>
					<img id="seachTyp" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_category"/>' />
				</div>			
			</div>
			<table id="test" style="width:'100%';" title="<fmt:message key ="scm_hyfddb" />" singleSelect="true" rownumbers="true" remoteSort="true"
				idField="itemid" >
				<thead>
					<tr>
					<th field="SP_CODE" width="100px" rowspan="2"><span id="SP_CODE"><fmt:message key="supplies_code"/></span></th>
					<th field="SP_NAME" width="100px" rowspan="2"><span id="SP_NAME"><fmt:message key="supplies_name"/></span></th>
					<th field="SP_DESC" width="100px" rowspan="2"><span id="SP_DESC"><fmt:message key="supplies_specifications"/></span></th>
					<th field="UNIT" width="100px" rowspan="2"><span id="UNIT"><fmt:message key="unit"/></span></th>
					<c:forEach var="positn" items="${columns}">
						<th width="100px;" align="right" colspan="2"><span id="_${positn.code}"/>${positn.des}</th>
					</c:forEach>
					</tr>
					<tr>
					<c:forEach var="positn" items="${columns}">
						<th field="F_${positn.code}_SL" width="100px;" align="right"><span id="_${positn.code}"/><fmt:message key="shouyeshuliang"/></th>
						<th field="F_${positn.code}" width="100px;" align="right"><span id="_${positn.code}"/><fmt:message key="amount"/></th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}
							//将选择的类别名称传到后台
							//$("#grptypdes").val(typeof($("#grptyp").data("checkedName"))!="undefined"?$("#grptyp").data("checkedName"):"");
							//$("#grpdes").val(typeof($("#grp").data("checkedName"))!="undefined"?$("#grp").data("checkedName"):"");
							//$("#typdes").val(typeof($("#typ").data("checkedName"))!="undefined"?$("#typ").data("checkedName"):"");
							$('#listForm').submit();
						}
/* 					},{
 						text: 'Excel',
 						title: 'Excel',
 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
 						icon: {
 							url: '<%=path%>/image/Button/op_owner.gif',
 							position: ['-40px','-20px']
 						},
 						handler: function(){
 							$('#listForm').attr('action','<%=path%>/useFirmCompare/exportGrossProfit.do');
 							$('#listForm').submit();
 						}
 					},{
 						text: '<fmt:message key="print" />',
 						title: '<fmt:message key="print" />',
 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
 						icon: {
 							url: '<%=path%>/image/Button/op_owner.gif',
 							position: ['-140px','-100px']
 						},
 						handler: function(){
 							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
 								$('#queryForm').attr('target','report');
 								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
 								var action="<%=path%>/useFirmCompare/printGrossProfit.do?checkMis="+checkMis;
 								$('#queryForm').attr('action',action);
 								$('#queryForm').submit();
 							}else{
 								showMessage({
 									type: 'error',
 									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
 									speed: 1000
 									});
 								return ;
 							}
 						}*/
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/prdPrcCostManage/reportList.do");
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),100);
  	 		$('#test').datagrid({});
  	 		var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});	
			

			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					typn:'1203',
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
  	 	});
  	 	
  	 	//择类别的方法，返回类别两边不包含（）
	  	$('#seachTyp').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#typ').val();
				//var defaultName = $('#typDes').val();
				var offset = getOffset('seachTyp');
				top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?resulttyp=1&defaultCode='+defaultCode),offset,$('#typdes'),$('#typ'),'320','460','isNull');
			}
		});
  	 </script>
  </body>
</html>
