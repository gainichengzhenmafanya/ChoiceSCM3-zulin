<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>分店毛利对比</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
				.panel-body{}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/costProfitAna/firmProfitCmp.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-input" style="margin-left: 50px;">
					<input type="radio" id="all" name="dept" value="QB" <c:if test="${dept=='QB'}">checked="checked"</c:if> />全部部门
					<input type="radio" id="cf" name="dept" value="CF" <c:if test="${dept=='CF'}">checked="checked"</c:if>/>厨房部门
					<input type="radio" id="qt" name="dept" value="QT" <c:if test="${dept=='QT'}">checked="checked"</c:if>/>其他部门
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label"><fmt:message key="branche" /></div>
				<div class="form-input">
					<input type="text"  id="firmDes" class="text" style="margin-bottom: 4px;" name="firmDes" value="${firmDes}" readonly="readonly"/>
					<input type="hidden" id="positn" name="positn" value="${supply.positn}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
				</div>
			</div>
			<table id="test" style="width:'100%';" class="panel-body" title="<fmt:message key ="Branch_cost_comparison" />" singleSelect="true" rownumbers="true" remoteSort="true"
				idField="itemid" >
				<thead>
					<tr>
					<th field="WEEKNO" width="100px"><span id="WEEKNO"><fmt:message key ="Weekly" /></span></th>
					<th field="DES" width="100px"><span id="DES"><fmt:message key ="Starting_date" /></span></th>
					<c:forEach var="positn" items="${columns}">
						<th field="F_${positn.code}" width="100px;"><span id="_${positn.code}"/>${positn.des}</span></th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}
							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}
							//----------------------------------------------------
							$("#wait2").val('NO');//不用等待加载
							var form = $('#listForm');
							var grid = $("#test");
							var panel = grid.datagrid('getPanel');
							var content = panel.panel('body');
							
							var headers = [];
							function clearHead(head){
								head.find('table').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
								head.find('td').each(function(){
									if($(this).css('display') == 'none'){
										$(this).remove();
									}else{
										$(this).removeAttr('class');
										$(this).html($.trim($(this).text()));
									}
								});
								return head.html();
							}
							headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view1').find('.datagrid-header-inner').clone()));
							headers.push(clearHead(content.find('.datagrid-view').find('.datagrid-view2').find('.datagrid-header-inner').clone()));
							headers.push("<fieldMap>{}</fieldMap>");
							headers = headers.join("");
							var rs = headers.match(/\w+\s*=\w+/g);
							for(var s in rs){
								var string = String(rs[s]);
								string.match(/(\w+)$/g);
								headers = headers.replace(string,string.replace(RegExp.$1,'"'+RegExp.$1+'"'));
							}
							var head = $("<input type='hidden' name='headers'/>");
							form.find("input[name='headers']").remove();
							head.val(headers.replace(/\r\n/g,""));
							head.appendTo(form);
							form.attr('action','<%=path%>/costProfitAna/exportGrossProfit.do');
							form.submit();
							$("#wait2").val('');//等待加载还原
							//--------------------------------------------------------
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/prdPrcCostManage/reportList.do");
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		setElementHeight('.panel-body',['.tool'],$(document.body),0);
  	 		$('#test').datagrid({});
  	 		var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
// 			var numCols = ['balances',					
//   	 		           	   'bperiod',				
// 		  	 		       'inamt',			
// 		  	 		       'costdiffrate',						
// 		  	 		       'costdiff',						
// 		  	 		       'theorymaorate',					
// 		  	 		       'theorycost',					
// 		  	 		       'maorate',					
// 		  	 		       'cost',					
// 		  	 		       'amt'];
			$('.datagrid-body').find('table').find('tbody').find('tr').find('td').each(function(){
// 				if ($(this).attr('field')!=null) {
// 					if($.inArray($(this).attr('field'),numCols) >=0){
// 						$(this).find('div').css("text-align","right");
// 					}
// 				}
				var value=$(this).find('div').text();
				if (!isNaN(value)){
					$(this).find('div').css("text-align","right");
				};
			})
			$('.datagrid-footer').find('table').find('tbody').find('tr').find('td').each(function(){
				var value=$(this).find('div').text();
				if (!isNaN(value)){
					$(this).find('div').css("text-align","right");
				};
			})
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					typn:'1203',
					tagName:'firmDes',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 </script>
  </body>
</html>
