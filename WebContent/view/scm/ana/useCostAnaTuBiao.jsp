<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>耗用成本分析</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	</head>
	<body>
		<input type="hidden" id="firm" value="${supplyAcct.firm }"/>
		<input type="hidden" id="grptyp" value="${supplyAcct.grptyp }"/>
		<div class="tool">
		</div>
		<div class="easyui-tabs" fit="false" plain="false" style="height:392px;">
			<div title="<fmt:message key ="Chart" /> ">
				<div id="tubiao" class="grid" style="height:343px;">
					
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
 		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script> 		
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
			 	});
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				loadChart();
			});
			//图表展示
			function loadChart(){
				var firm=$('#firm').val();
	  	 		var grptyp=$('#grptyp').val();
				var chartSwf = "MSColumn3D.swf";
				var w = 960;
				var h = 350;
				//图表
				var chart = new FusionCharts("<%=path%>/Charts/"+chartSwf, "<fmt:message key ="Consumption_cost_chart_analysis" />", w, h);
				chart.setDataURL("<%=path%>/useCostAna/findXmlForUseCostAnaTuBiao.do?firm="+firm+"&grptyp="+grptyp);
				chart.render("tubiao");
			}
		</script>
	</body>
</html>