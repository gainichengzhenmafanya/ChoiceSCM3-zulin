<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>门店成本对比</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  	<input id="firstLoad" type="hidden"/>
  	<input id="itcode" type="hidden"/>
  	<form id="queryForm" name="queryForm" method="post">
					<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input"><input type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
					<div class="form-label"><fmt:message key="branche" /></div>
					<div class="form-input">
						<input type="text"  id="positn_name" class="text" style="margin-bottom: 4px;" name="firmDes" readonly="readonly"/>
						<input type="hidden" id="positn" name="positn"/>
						<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
					<div class="form-label"><fmt:message key="supplies_code"/></div>
					<div class="form-input">
						<input type="text" style="margin-top:0px;margin-bottom:4px;vertical-align:middle;" class="text" id="sp_code" name="sp_code" />
						<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
					</div>
				</div>
			</form>
 	 <iframe id="MdChengbenDuibiFrame" name="MdChengbenDuibiFrame" style="border:0px;"></iframe>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var bdat = $('#bdat').val();
							var edat = $('#edat').val();
							var positn = $('#positn').val();
							var sp_code = $('#sp_code').val();
							$.ajax({
								url:'<%=path %>/MdChengbenDuibi/toReport.do?bdat='+bdat+'&edat='+edat+'&positn='+positn+'&sp_code='+sp_code,
								type:'post',
								success:function(msg){
									var date = new Date();
									window.MdChengbenDuibiFrame.location.href = "<%=path %>/report/ana/MdChengbenDuibi.html?date="+date;
								}
							});
						}
					},{
// 						text: 'Excel',
// 						title: 'Excel',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-40px','-20px']
// 						},
// 						handler: function(){
<%-- 							$('#queryForm').attr('action',"<%=path%>/caiPinShiJiMaoLi/exportCaiPinShiJiMaoLi.do"); --%>
// 							$('#queryForm').submit();
// 						}
// 					},{
// 						text: '<fmt:message key="print" />',
// 						title: '<fmt:message key="print" />',
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							$('#queryForm').attr('target','report');
// 							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
<%-- 							var action="<%=path%>/caiPinShiJiMaoLi/printCaiPinShiJiMaoLi.do"; --%>
// 							$('#queryForm').attr('action',action);
// 							$('#queryForm').submit();
// 						}
// 					},{
// 						text: '<fmt:message key="column_selection" />',
// 						title: '<fmt:message key="column_selection" />',
// 						useable:true,
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-100px','-60px']
// 						},
// 						handler: function(){
// 							toColsChoose();
// 						}
// 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		
  	 		$('#MdChengbenDuibiFrame').height($(document).height()-105);
  	 		
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
  	 		
  	 		$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $("#sp_code").val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
  	 		$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positn").val(),
					single:false,
					tagName:'positn_name',
					tagId:'positn',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	//列选择
  	 	function toColsChoose(){
  	 		$('body').window({
				title: '<fmt:message key="column_selection"/>',
				content: '<iframe frameborder="0" src="<%=path%>/caiPinShiJiMaoLi/toColChooseCaiPinShiJiMaoLi.do"></iframe>',
				width: '460px',
				height: '430px',
				draggable: true,
				isModal: true
			});
  	 	}
  	 </script>
  </body>
</html>
