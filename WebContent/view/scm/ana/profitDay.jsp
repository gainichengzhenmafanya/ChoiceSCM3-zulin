<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>毛利率日分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	<style type="text/css">
		.text{
			font-size:12px,border:0px,line-height:20px,height:20px,padding:0px,*height:18px,*line-height:18px,_height:18px,_line-height:18px;
		}
		.search{
			margin-top:3px;
			cursor: pointer;
		}
		form .form-line .form-label{
			width: 7%;
		}
		form .form-line .form-input{
			width: 16%;
		}
		.form-line .form-input input[type=text] , .form-line .form-input select{
			width:80%;
		}
		.page{
			margin-bottom: 25px;
		}
	</style>
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/profitAna/profitDay.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${profit.bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label" style="margin-left: 31px;width: 80px;"><fmt:message key ="Actual_cost_calculation_method" />:</div>
				<div class="form-input" style="margin-left: 18px;">
					<input type="radio" id="hy" name="costby" value="HY" <c:if test="${profit.costby == 'HY'||profit.costby == null}"> checked="checked" </c:if>  /><fmt:message key ="According_to_the_cost_calculation" />
					<input type="radio" id="ly" name="costby" value="LY" <c:if test="${profit.costby == 'LY'}"> checked="checked" </c:if> /><fmt:message key ="Calculation_of_the_collar" />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${profit.edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label" style="margin-left: 50px;"><fmt:message key="branche" />:</div>
				<div class="form-input">
					<input type="text"  id="firmdes" class="text" style="margin-bottom: 4px;" name="firmdes" value="${profit.firmdes }" readonly="readonly"/>
					<input type="hidden" id="firmcode" name="firmcode" value="${profit.firmcode}"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="branches_selection"/>' />
				</div>
			</div>
			<table id="test" style="width:'100%';height:200px" title="<fmt:message key ="Gross_profit_margin_analysis" />" singleSelect="true" rownumbers="true" remoteSort="true" showFooter=true
					sortName="${profit.sortName}" sortOrder="${profit.sortOrder}" idField="itemid" >
				<thead>
					<tr>
						<c:forEach var="dictColumns" items="${dictColumnsListByAccount}">
							<th field="${dictColumns.columnName}" width="${dictColumns.columnWidth}"><span id="_${dictColumns.columnName}"/>${dictColumns.zhColumnName}</span></th>
						</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
 	 <div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
								return;
							}
							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('action','<%=path%>/profitAna/exportProfitDay.do');
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/profitAna/profitDay.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
								$("#wait2").val('NO');//不用等待加载
								$('#listForm').attr('target','report');
								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
								var action="<%=path%>/profitAna/printProfitDay.do";
								$('#listForm').attr('action',action);
								$('#listForm').submit();	
								$('#listForm').attr('target','');
								$('#listForm').attr('action','<%=path%>/profitAna/profitDay.do');
								$("#wait2").val('');//等待加载还原
							}else{
								alert('<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！');
								/* showMessage({
									type: 'error',
									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
									speed: 1000
									}); */
								return ;
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							//window.location.replace("<%=path%>/prdPrcCostManage/reportList.do");
							$(window.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').click();
						}
					}
				]
			});
  	 		$('#test').datagrid({
					rowStyler:function(){
						return 'line-height:11px';
					}
				});
  	 		$('#test').datagrid({});
  	 		var obj={};
			var rows = ${ListBean};
			var footer = ${ListBean_total};
			obj['rows']=rows;
			obj['footer']=footer;
			$('#test').datagrid('loadData',obj);
			setElementHeight('.panel-body',['.tool'],$(document.body),125);
			var numCols = ['costdiff',					
  	 		           	   'theorymaorate',				
		  	 		       'theorycost',			
		  	 		       'maorate',						
		  	 		       'cost',						
		  	 		       'amt',					
		  	 		       'costdiffrate'];
            var numColsPer = ['theorymaorate','costdiffrate','maorate'];
            $('.datagrid-body').find('table').find('tbody').find('tr').find('td').each(function(){
                if ($(this).attr('field')!=null) {
                    if($.inArray($(this).attr('field'),numCols) >=0){
                        if($.inArray($(this).attr('field'),numColsPer)>=0) {
                            $(this).find('div').text($(this).find('div').text()+"%");
                        }
                        $(this).find('div').css("text-align","right");
                    }
                }
            })
            $('.datagrid-footer').find('table').find('tbody').find('tr').find('td').each(function(){
                if ($(this).attr('field')!=null) {
                    if($.inArray($(this).attr('field'),numCols) >=0){
                        if($.inArray($(this).attr('field'),numColsPer)>=0) {
                            $(this).find('div').text($(this).find('div').text()+"%");
                        }
                        $(this).find('div').css("text-align","right");
                    }
                }
            })
			
			//排序开始
			$('.datagrid-header').find(".datagrid-cell").click(function (){
                $('#sortName').val($(this).parent("td").attr("field"));
				if($('#sortOrder').val()=='asc'){
					$('#sortOrder').val('desc');
				}else{
					$('#sortOrder').val('asc');
				};
				$('#listForm').submit();
			})
			$('#_'+$('#sortName').val()).parents('.datagrid-cell').addClass('datagrid-sort-'+$('#sortOrder').val());
			//排序结束
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmcode").val(),
					single:false,
					tagName:'firmdes',
					typn:'1203',
					tagId:'firmcode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
  	 	});
  	 	
  	 </script>
  </body>
</html>
