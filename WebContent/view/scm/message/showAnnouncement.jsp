<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查看公告详情</title>
		<script type="text/javascript">
			var path = "<%=path%>";
		</script>
	</head>
	<body style="text-align: center;">
		<p align="center" style="font-weight:bold;"><c:out value="${announcement.title}"></c:out></p>
		
		<textarea wrap="physical" name="content" id="content" style="width: 100%;height: 250px;text-align: left;"><c:out value="${announcement.content}"></c:out></textarea>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var option = {
						initialFrameHeight:230,
				        focus:false,
				        wordCount:false,
				        scaleEnabled:false,
				        toolbars:[["fullscreen"]]
					};
				var ue = UE.getEditor('content',option);
				ue.addListener("ready", function () {
					ue.setDisabled('fullscreen');
			        ue.focus(false);
			    });
			});
		</script>
	</body>
</html>