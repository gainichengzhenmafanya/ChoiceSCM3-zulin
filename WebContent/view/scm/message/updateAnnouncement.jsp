<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="modify_the_information_notice"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
			<script type="text/javascript">
				var path = "<%=path%>";
			</script>
	</head>
	<body>
	<div class="form" style="text-align: center;width: 98%;">
			<form id="announcementForm" method="post" action="<%=path %>/announcement/saveByUpdate.do">
				<input type="hidden" name="id" value="<c:out value='${announcement.id }'></c:out>"></input>
				<input type="hidden" name="create_date" value="<fmt:formatDate value="${announcement.create_date}" pattern="yyyy-MM-dd"/>"></input>
				<input type="hidden" name="create_time" value="<c:out value='${announcement.create_time }'></c:out>"></input>
				<div class="form-label" style="text-align: center;width: 100%;"><fmt:message key="title"/></div>
				<div class="form-input">
					<input type="text" id="title" name="title" value="<c:out value="${announcement.title}"></c:out>" class="text" style="width:90%;"/>
				</div>
				<br />
<%-- 				<div class="form-label" style="text-align: center;width: 100%;"><span style="margin-left: 60px;"><fmt:message key="content"/></span><span id="count" style="float:right;margin-right:15px;"></span></div> --%>
					<div class="form-label" style="text-align: center;width: 100%;"><span ><fmt:message key="content"/></span></div>
				<div class="form-input" align="center">
<%-- 				<textarea name="content" id="content" onpropertychange="checkLength()" style="width: 450px;height: 180px;"><c:out value="${announcement.content}"></c:out></textarea> --%>
					<textarea name="content" id="content" style="width: 90%;height: 180px;text-align: left;"><c:out value="${announcement.content}"></c:out></textarea>
				</div>
			</form>
			</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		
		<script type="text/javascript">
			var editor;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'title',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="title"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="title_too_long"/>！']
					}]
				});
				editor = UE.getEditor('content');
				//修改时，页面加载后计算内容文本长度
// 				var curLength = $("#content").val().length;
// 				$("#count").html(curLength+'/2000');
	 	 		var startStr = $("#content").val().replace(/[^\x00-\xff]/g, "**"); 
				var curLength = startStr.length; 
				$("#count").html(curLength+'/2000');
				
				//限制文本框的字符长度（汉字算两个字符）
				$("#content").bind('blur keyup',function(){
		 	 		$(this).limitLength(2000);
		 	 		var startStr = $("#content").val().replace(/[^\x00-\xff]/g, "**"); 
					var curLength = startStr.length; 
					$("#count").html(curLength+'/2000');
		 		});
			});
			
			function checkLength(){
				var curLength = $("#content").val().length;
				$("#count").html(curLength+'/2000');
				if(curLength>2000){
					$("#content").val($("#content").val().substring(0,2000));
					
				}
			}
			//将编辑框内容同步内容到content容器
			function getText(){
				var result = true;
				editor.sync(); 
				if(!$("#content").val()){
					result = false;
					alert("<fmt:message key="content"/><fmt:message key="cannot_be_empty"/>！");
				}else if(editor.getPlainTxt().replace(/[^\x00-\xff]/g, "**").length>2000){
					result = false;
					alert("<fmt:message key="content"/><fmt:message key="length_too_long"/>！");
				}
				return result;
			}
		</script>
	</body>
</html>