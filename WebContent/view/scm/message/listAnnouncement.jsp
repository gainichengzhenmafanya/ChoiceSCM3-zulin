<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>公告管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
			<script type="text/javascript">
				var path="<%=path%>";
			</script>								
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/announcement/list.do" id="listForm" name="listForm" method="post">
			<div class="search-div" style="position: absolute;z-index: 99">
				<div class="form-line">
					<div class="form-label"><fmt:message key="title"/>：</div>
					<div class="form-input"><input type="text" id="title" name="title" value="${announcement.title }" onkeyup="ajaxSearch()" class="text" /></div>
					<div class="form-label"><fmt:message key="content"/>：</div>
					<div class="form-input"><input type="text" id="content" name="content" value="${announcement.content }" onkeyup="ajaxSearch()"  class="text" /></div>
					<div class="form-label"><fmt:message key ="creation_time" />：</div>
					<div class="form-input" style="width:300px">
						<span><input type="text" name="bdate" id="bdate" class="Wdate text" value="<fmt:formatDate value="${announcement.bdate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});"/></span>
						<span><fmt:message key ="to2" /></span>
						<span><input type="text" name="edate" id="edate" class="Wdate text" value="<fmt:formatDate value="${announcement.edate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});"/></span>
					</div>
				</div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select"/>'/>
		       		<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty"/>'/>
				</div>
			</div>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:30px;">
									<input type="checkbox" id="chkAll"/>
								</td>
								<td style="width:200px;"><fmt:message key="title"/></td>
								<td style="width:300px;"><fmt:message key="content"/></td>
								<td style="width:150px;"><fmt:message key="creation_time"/></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="announcement" items="${announcementList}" varStatus="status">
								<tr ondblclick="show(${announcement.id})">
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${announcement.id}' />" value="<c:out value='${announcement.id}' />"/>
									</td>
									<td><span style="width:200px;"><c:out value="${announcement.title}" />&nbsp;</span></td>
									<td><span style="width:300px;"><c:out value="${announcement.content}" />&nbsp;</span></td>
									<td><span style="width:150px;"><c:out value="${announcement.create_time}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">		
			function ajaxSearch(key){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#listForm').submit();
				} 
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearlistForm();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select"/>',
							title: '<fmt:message key="select_the_information_notice"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								var t = $('#title').val();
								 $('#title').focus().val(t);
							}
						},"-",{
							text: '<fmt:message key="insert"/>',
							title: '<fmt:message key="increase_the_information_notice"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveAnnouncement();
							}
						},{
							text: '<fmt:message key="update"/>',
							title: '<fmt:message key="modify_the_information_notice"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateAnnouncement();
							}
						},{
							text: '<fmt:message key="delete"/>',
							title: '<fmt:message key="delete_the_information_notice"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteAnnouncement();
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});

				$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
				                if (!!$("#chkAll").attr("checked")) {
				                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
				                }else{
				                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
			                	      }
				            });
					//单击每行选中前面的checkbox
					$('.grid').find('.table-body').find('tr').live("click", function () {
						if($(this).find(':checkbox')[0].checked){
							$(":checkbox").attr("checked", false);
						}else{
							$(":checkbox").attr("checked", false);
							$(this).find(':checkbox').attr("checked", true);
						}
					 });
					//禁用checkbox本身的事件
					$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
						event.stopPropagation();
						if(this.checked){
							$(this).attr("checked",false);	
						}else{
							$(this).attr("checked",true);
						}
						$(this).closest('tr').click();
					});
				function saveAnnouncement(){
					$('body').window({
						id: 'window_addAnnouncement',
						title: '<fmt:message key="announce_the_information_notice"/>',
						content: '<iframe id="addAnnouncementFrame" frameborder="0" src="<%=path%>/announcement/add.do"></iframe>',
						width: '650px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="announce_the_information"/>',
									title: '<fmt:message key="announce_the_information_notice"/>',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('addAnnouncementFrame') && getFrame('addAnnouncementFrame').validate._submitValidate()){
											if(getFrame('addAnnouncementFrame').getText()){
												submitFrameForm('addAnnouncementFrame','announceMentForm');
											}
										}
									}
								},{
									text: '<fmt:message key ="cancel" />',
									title: '<fmt:message key ="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				
				function updateAnnouncement(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						var aim = checkboxList.filter(':checked').eq(0);
						var chkValue =aim.val();
						
						$('body').window({
							title: '<fmt:message key="modify_the_information_notice"/>',
							content: '<iframe id="updateAnnouncementFrame" frameborder="0" src="<%=path%>/announcement/update.do?id='+chkValue+'"></iframe>',
							width: '650px',
							height: '500px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save"/>',
										title: '<fmt:message key="save_the_information_notice"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updateAnnouncementFrame')&& getFrame('updateAnnouncementFrame').validate._submitValidate()){
												if(getFrame('updateAnnouncementFrame').getText()){
													submitFrameForm('updateAnnouncementFrame','announcementForm');
												}
											}
										}
									},{
										text: '<fmt:message key ="cancel" />',
										title: '<fmt:message key ="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
					
				}
				
				function deleteAnnouncement(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="delete_data_confirm"/>？')){
							var codeValue=[];
							checkboxList.filter(':checked').each(function(){
								codeValue.push($(this).val());
							});
							var action = '<%=path%>/announcement/delete.do?ids='+codeValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete_the_information_notice"/>',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 538,
								height: '215px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
						return ;
					}
				}
			});
			
			function date()
			{
				new WdatePicker();
			}
			//双击展示
			function show(chkValue){
				$('body').window({
					title: '<fmt:message key="Detailed_information" />',
					content: '<iframe id="showAnnouncement" frameborder="0" src="<%=path%>/announcement/show.do?id='+chkValue+'"></iframe>',
					width: '650px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key ="close" />',
								title: '<fmt:message key ="close" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
		</script>
	</body>
</html>