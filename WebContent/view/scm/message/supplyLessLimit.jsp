<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>student Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
			</style>
		</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/mainInfo/lessLimit.do" id="listForm" name="listForm" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td><span style="width:100px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:120px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:50px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:80px;"><fmt:message key="current_inventory"/></span></td>
								<td><span style="width:80px;"><fmt:message key="minimum_volume"/></span></td>
								<td><span style="width:80px;"><fmt:message key="insufficient_limited"/></span></td>
								<td><span style="width:100px;"><fmt:message key="Lower_bound_ratio_" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" items="${supplyList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td><span style="width:100px;"><c:out value="${supply.sp_code}" />&nbsp;</span></td>
									<td><span style="width:120px;"><c:out value="${supply.sp_name}" />&nbsp;</span></td>
									<td><span style="width:50px;"><c:out value="${supply.sp_desc}" />&nbsp;</span></td>
									<td><span style="width:50px;"><c:out value="${supply.unit}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${supply.cnt}" />&nbsp;</span></td>
									<td><span style="width:80px;"><c:out value="${supply.sp_min1}" />&nbsp;</span></td>
									<td><span style="width:80px;"><fmt:formatNumber value="${supply.sp_min1-supply.cnt}"  pattern="00"/>&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${supply.tax}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
			});
			
		</script>
	</body>
</html>