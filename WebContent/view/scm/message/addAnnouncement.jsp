<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="new_information_notice"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
			<script type="text/javascript">
				var path = "<%=path%>";
			</script>
	</head>
	<body>
		<div class="form" style="text-align: center;width: 98%;">
			<form id="announceMentForm" method="post" action="<%=path %>/announcement/saveByAdd.do">
				<div class="form-label" style="text-align: center;width: 100%;"><fmt:message key="title"/></div>
				<div class="form-input">
					<input type="text" id="title" name="title" class="text" style="width:90%;"/>
				</div>
				<br />
				<div class="form-label" style="text-align: center;width: 100%;"><span><fmt:message key="content"/></span></div>
				<div class="form-input" align="center">
					<textarea wrap="physical" id="content"  name="content" style="width: 90%;height: 180px;text-align: left;"></textarea>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/dialogs/image/image.js"></script>
		
		<script type="text/javascript">
			var editor;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'title',
						validateType:['canNull','maxLength'],
						param:['F','100'],
						error:['<fmt:message key="title"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="title_too_long"/>！']
					}]
				});
				editor = UE.getEditor('content');
				//限制文本框的字符长度（汉字算两个字符）
				$("#content").bind('blur keyup',function(){
		 	 		$(this).limitLength(2000);
		 	 		var startStr = $("#content").val().replace(/[^\x00-\xff]/g, "**"); 
					var curLength = startStr.length; 
					$("#count").html(curLength+'/2000');
		 		});
			});
			//将编辑框内容同步内容到content容器
			function getText(){
				var result = true;
				editor.sync(); 
				if(!$("#content").val()){
					result = false;
					alert("<fmt:message key="content"/><fmt:message key="cannot_be_empty"/>！");
				}else if(editor.getPlainTxt().replace(/[^\x00-\xff]/g, "**").length>2000){
					result = false;
					alert("<fmt:message key="content"/><fmt:message key="length_too_long"/>！");
				}
				return result;
			}
		</script>
	</body>
</html>