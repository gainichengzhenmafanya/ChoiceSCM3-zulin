<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>acct Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/acct/list.do" id="listForm" name="listForm" method="post">
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:30px;"></span>
								</td>
								<td><span style="width:50px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:180px;"><fmt:message key="set_of_books_describe"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="acct" items="${acctList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:30px; text-align: center;">
										<input type="checkbox"  name="idList" id="chk_<c:out value='${acct.code}' />" value="<c:out value='${acct.code}' />"/></span>
									</td>
									<td><span style="width:50px;text-align: center;"><c:out value="${acct.code}" />&nbsp;</span></td>
									<td><span style="width:180px;text-align: center;"><c:out value="${acct.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="modify_the_set_of_books_information"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateAcct();
							}
						},{
							text: '<fmt:message key="fiscal_month" />',
							title: '<fmt:message key="modify_the_accounting_monhth"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								editMonth();
							}
						},{
							text: '<fmt:message key ="quit" />',
							title: '<fmt:message key ="quit" />',
							icon: {
								url: '/Choice/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
				setElementHeight('.grid',['.tool'],$(document.body),29);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
					$("#chkAll").click(function() {
		                if (!!$("#chkAll").attr("checked")) {
		                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
		                }
		                else
		                {
		                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
	                	}
		            });
					//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
					$('.grid').find('.table-body').find('tr').live("click", function () {
					     if ($(this).hasClass("bgBlue")) {
					         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
					     }
					     else
					     {
					         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
					     }
					 });
				function updateAcct(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList
							&& checkboxList.filter(':checked').size() > 0){
						var chkValue = checkboxList.filter(':checked').eq(0).val();
						$('body').window({
							title: '<fmt:message key="modify_the_set_of_books_information"/>',
							content: '<iframe id="updateAcctFrame" name="updateAcctFrame" frameborder="0" src="<%=path%>/acct/update.do?code='+chkValue+'"></iframe>',
							width: '550px',
							height: '400px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="save_the_set_of_books_information"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updateAcctFrame') && getFrame('updateAcctFrame').validate._submitValidate()){
												submitFrameForm('updateAcctFrame','acctForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel"/>',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_modify"/>！');
						return ;
					}
					
				}
				
			});
			//修改会计月
			function editMonth(){
				monthEdit = $('body').window({
					title: '<fmt:message key="fiscal_month"/>',
					content: '<iframe id="monthEdit" frameborder="0" src="<%=path%>/mainInfo/mainList.do"></iframe>',
					width: '550px',
					height: '400px',
					draggable: true,
					isModal: true
				});
	  	 	}
		</script>
	</body>
</html>