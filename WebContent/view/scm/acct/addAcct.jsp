<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="set_of_books_information"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
				.userInfo,.accountInfo {
					position: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				
			</style>
	</head>
	<body>
	<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:160px;margin-top:90px;">
			<form id="acctForm" method="post" action="<%=path %>/acct/saveByAdd.do">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding"/>：</div>
					<div class="form-input">
						<input type="text" id="code" name="code" class="text"/>
					</div>
				</div>
					<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="set_of_books_describe"/>：</div>
					<div class="form-input">
						<input type="text" id="des" name="des" class="text"/>
					</div>
				</div>	
				<!-- <div class="form-line">
					<div class="form-label">用户名</div>
					<div class="form-input">
						<input type="text" id="usernam" name="usernam" class="text"/>
					</div>
					<div class="form-label">密码</div>
					<div class="form-input">
						<input type="text" id="pass" name="pass" class="text"/>
					</div>
				</div> -->	
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="amount"/>：</div>
					<div class="form-input">
						<input type="text" id="amount" name="amount" class="text"/>
					</div>
				</div>
					<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="show_the_amount_of"/>：</div>
					<div class="form-input">
						<input type="text" id="viewamount" name="viewamount" class="text"/>
					</div>
				</div>	
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="serial_number"/>：</div>
					<div class="form-input">
						<input type="text" id="serial" name="serial" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="being_used"/>：</div>
					<div class="form-input">
						<input type="radio" name="warn" value="N" checked="checked"><fmt:message key="no1"/></input>
						<input type="radio" name="warn" value="Y"><fmt:message key="be"/></input>
					</div>
				</div>
					<div class="form-line">
					<div class="form-label"><fmt:message key="forecast_the_way"/>：</div>
					<div class="form-input">
						<select id="plantyp" name="plantyp" class="select" style="width:133px;">
						    	<option value="营业额"><fmt:message key="turnover"/></option>
						    	<option value="人数"><fmt:message key="number_of_people"/></option>
						    	<option value="单数"><fmt:message key="singular"/></option>
						 </select>
					</div>
				</div>	
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'code',
						validateType:['canNull','num','maxLength'],
						param:['F','F',10],
						error:['<fmt:message key="number"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="coding"/><fmt:message key="not_valid_number"/>！','<fmt:message key="numbers_over_10"/>！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="description"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'usernam',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="user_name"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'pass',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="password"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'amount',
						validateType:['canNull','num'],
						param:['F'],
						error:['<fmt:message key="amount"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="only_digits"/>！']
					},{
						type:'text',
						validateObj:'viewamount',
						validateType:['canNull','num'],
						param:['F'],
						error:['<fmt:message key="show_the_amount_of"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="only_digits"/>！']
					},{
						type:'text',
						validateObj:'serial',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="serial_number"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				
				$('#code').bind('focusout',function(){
					var code = $(this).val();
					var data = {};
					data['code']=code;
					$.post("<%=path%>/acct/checkId.do",data,function(data){
						var rs = eval('('+data+')');
						if(null!=rs.acct){
							alert('<fmt:message key="the_number_exist"/>');
							$("#code").focus();
						}
						
					});	
				});
				
			});
			
		</script>
	</body>
</html>