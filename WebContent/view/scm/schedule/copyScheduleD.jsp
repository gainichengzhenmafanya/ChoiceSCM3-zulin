<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>复制配送班表</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<div class="tool"> </div>
<form id="updateScheduleForm" action="<%=path%>/schedule/copySchedule.do" method="post">
	<div align="center">
		<div class="form-line">
			<div class="form-label"><fmt:message key="name"/>:</div>
			<div class="form-input"><input type="hidden" name="scheduleID" value="${Schedule.scheduleID}">
				<input type="text" id="scheduleName" name="scheduleName" value="" maxlength="10"></div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="starttime"/>:</div>
			<div class="form-input"><input type="text" id="beginDate" name="beginDate" class="Wdate text" value="<fmt:formatDate value="${Schedule.beginDate}" pattern="yyyy-MM-dd" type="date"/>" pattern="yyyy-MM-dd" type="date" onclick="new WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}'});"/></div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="endtime"/>:</div>
			<div class="form-input"><input type="text" id="endDate" name="endDate" class="Wdate text" value="<fmt:formatDate value="${Schedule.endDate}" pattern="yyyy-MM-dd" type="date"/>" pattern="yyyy-MM-dd" type="date" onclick="new WdatePicker({minDate:'#F{$dp.$D(\'beginDate\')}'});"/></div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="positn_state"/>:</div>
			<div class="form-input"><fmt:message key="enable" /><input type="radio" id="status" name="status" class="text" value="Y"
						<c:if test="${Schedule.status=='Y'}"> checked="checked"</c:if> />
					<fmt:message key="disabled" /><input type="radio" id="status" name="status" class="text" value="N"
						<c:if test="${Schedule.status=='N'}"> checked="checked"</c:if> />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="explain"/>:</div>
			<div class="form-input"><textarea rows="5" cols="20" id="comments" name="comments" maxlength="100">${Schedule.comments}</textarea></div>
		</div>
	</div>
</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			var validate;
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key="save"/>',
						title: '<fmt:message key="save"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							updateSchedule();
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(".close",parent.document).click();
						}
					}
				]
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'scheduleName',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key ="cannot_be_empty" />！']
					}]
				});
				
				//--------------------增加 删除---------------------------------
				function updateSchedule(){
					if(validate._submitValidate()){
						$("form").submit();
					}
				}
				//-----------------------------------------------------
			})
		</script>
</body>
</html>