<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			li{float: left;width: 100px;}
			.chooseSchedu{
				width: 540px;
				white-space: normal;
				height:auto !important;
				min-height:63px;
				max-height:109px;
				overflow-y:auto; 
				border-bottom: 1px solid #f2f2f2;
			}
		</style>
	</head>
	<body>
		<div class="tool"> </div>
		<div class="form">
			<form id="scheduleForm" method="post" action="<%=path %>/schedule/saveDetailesByCopy.do">
				<div style="height:420px;width:595px;z-index:88;margin:0px auto;">
					<input type="hidden" name="scheduleID" value="${ScheduleD.scheduleID}"/>
					<input type="hidden" name="category_Code" value="${ScheduleD.category_Code}"/>
					<input type="hidden" id="ids" name="ids"/>
					<input type="hidden" id="schedules" name="schedules"/>
					<div class="chooseSchedu">
						<input type="checkbox" name="checkall"/><font color="red"><fmt:message key ="future_generations" /></font><br/><hr size="1" style="color: #f2f2f2;"/>
						<ul>
							<c:forEach items="${scheList }" var="sche">
								<li><input type="checkbox" name="schedule" value="${sche.scheduleID }" <c:if test="${sche.scheduleID == ScheduleD.scheduleID }">checked="checked"</c:if>/>${sche.scheduleName }</li>
							</c:forEach>
						</ul>
					</div>
					<div id="txt">
						<c:forEach var="grptyp" items="${grptypList}" varStatus="operateNum">
							<span><br /><br />
								<input class="chk_<c:out value="${grptyp.code}" />" type="checkbox" 
							value="${grptyp.code}" name="chk"/><c:out value="${grptyp.des}"/></span>
						</c:forEach>
					</div>
				</div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//$(".chooseSchedu").hide();
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
							handler: function(){
								saveScheduleD();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-20px']
							},
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
								
				//全选/反选
				$("input[name=checkall]").click(function(){
					if($(this).attr("checked")){
						$("input[name=schedule]").attr("checked",true);
					}else{
						$("input[name=schedule]").attr("checked",false);
					}
				});
			});
			function saveScheduleD(){
				var chkValue = [];//报货类别集合
				var scheduleValue = [];//班表集合
				
				$('input[name=chk]').filter(':checked').each(function(i){
					chkValue.push($(this).val());
				});
				$('input[name=schedule]').filter(':checked').each(function(){
					scheduleValue.push($(this).val());
				});
				if ($('input[name=chk]').filter(':checked').size()==0) {
					alert("please_select_daily_goods_categories_to_be_added！");
					return;
				}
				if(scheduleValue.length==0){
					alert('<fmt:message key="Choose_a_copy_of_the_distribution_class" />！');
					return;
				}
// 				alert(scheduleValue.join(","));
// 				alert(chkValue.join(","));
				$('#ids').val(chkValue.join(','));
				$('#schedules').val(scheduleValue.join(","));
				$("form").submit();
			}
		</script>
	</body>
</html>