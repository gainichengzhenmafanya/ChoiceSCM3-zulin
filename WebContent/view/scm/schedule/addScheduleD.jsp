<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加配送班表 明细</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-label{
				width: 80%;
			}
		</style>
	</head>
	<body>
		<div class="tool"> </div>
		<div class="form">
			<form id="scheduleForm" method="post" action="<%=path %>/schedule/addScheduleDetails.do">
				<input type="hidden" id="scheduleID" name="scheduleID"value="${scheduleD.scheduleID}"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="category" />：</div>
					<div class="form-input">
						<select id="category_Code" name="category_Code" class="select" >
						  	<option selected="selected" disabled="disabled"></option>
						    	<c:forEach var="grpTyp" items="${listGrpTyp}" varStatus="status">
						    		<option value="${grpTyp.code}">${grpTyp.des}</option>
						    	</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Order_date" />：</div>
					<div class="form-input">
						<input type="text" id="orderDate" name=orderDate class="Wdate text" pattern="yyyy-MM-dd" type="date" onclick="new WdatePicker();"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key ="order_time" />：</div>
					<div class="form-input">
						<input type="text" id="orderTime" name="orderTime" pattern="hh:mm:ss" type="date" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
						
					</div>
				</div>
				<div class="form-line">
				    <div class="form-label"><fmt:message key ="arrival_date" />：</div>
					<div class="form-input">
						<input type="text" id="receiveDate" name=receiveDate class="Wdate text" pattern="yyyy-MM-dd" type="date" onclick="new WdatePicker();"/>
					</div>
				</div>
				<div class="form-line">
				    <div class="form-label"><fmt:message key="Arrival_time" />：</div>
					<div class="form-input">
						<input type="text" id="receiveTime" name="receiveTime" pattern="hh:mm:ss" type="date" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
							handler: function(){
								saveScheduleD();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'category_Code',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'orderDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'orderTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					}]
				});
			});
			function saveScheduleD(){
				if(validate._submitValidate()){
					$("form").submit();
				}
			}
		</script>
	</body>
</html>