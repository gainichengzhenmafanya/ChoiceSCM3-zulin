<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加配送班表 明细</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.window.css" />	
		<style type="text/css">
			.form-label{
				width: 80%;
			}
			ul{text-decoration: none;list-style: none;width: 100%;}
			li{width: 100%;text-align: left;height: 30px;line-height: 30px;padding-left: 60px;}
		</style>
	</head>
	<body>
		<div class="tool"> </div>
		<div class="form">
			<form id="scheduleForm" method="post" action="<%=path %>/schedule/addScheduleDetails.do">
				<input type="hidden" id="scheduleID" name="scheduleID" value="${scheduleD.scheduleID}"/>
				<input type="hidden" id="category_Code" name="category_Code"value="${scheduleD.category_Code}"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="date"/>：</div>
					<div class="form-input">
						<input type="text" id="orderDate" name=orderDate class="Wdate text" pattern="yyyy-MM-dd" type="date" value="<fmt:formatDate value="${orderDate}" pattern="yyyy-MM-dd" type="date"/>"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="time"/>：</div>
					
					<div class="form-input">
						<input type="text" id="orderTime" name="orderTime" pattern="hh:mm:ss" type="date" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
						
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="color"/>：</div>
					<div class="form-input">
						<input type="text" class="color {pickerClosable:true} text" id="orderColor" name="orderColor" />
						
					</div>
				</div>
				<div class="form-line">
				    <div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="date"/>：</div>
					<div class="form-input">
						<input type="text" id="receiveDate" name=receiveDate class="Wdate text" pattern="yyyy-MM-dd" type="date" onclick="new WdatePicker();"/>
					</div>
				</div>
				<div class="form-line">
				    <div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="time"/>：</div>
					<div class="form-input">
						<input type="text" id="receiveTime" name="receiveTime" pattern="hh:mm:ss" type="date" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="color"/>：</div>
					<div class="form-input">
						<input type="text" class="color {pickerClosable:true} text" id="receiveColor" name="receiveColor" />
						
					</div>
				</div>
				<div class="form-line">
					<c:forEach var="operate" items="${roleOperate.childOperate}" varStatus="operateNum">
						<c:if test="${operateNum.index !=0 && operateNum.index%7==0}">
						<br />
						</c:if>
						&nbsp;&nbsp;<input class="chk_"+i type="checkbox" 
						value="<c:out value="${operate.id}" />" /><c:out value="${operate.name}" />
					</c:forEach>
				</div>
				<div style="text-align: left;width: 100%;" class="choose">
					<ul>
						<li style="padding-left: 23px;"><fmt:message key="please_select_daily_goods_categories_to_be_added"/></li>
						<li style="border-bottom: 1px solid #f2f2f2;"><input type="checkbox" value="all" id="all"/>全&nbsp;&nbsp;&nbsp;部</li>
						<c:forEach var="grptyp" items="${grptypList}" varStatus="operateNum">
							<li><input type="checkbox" value="${grptyp.code }" name="category_code" class="category_code"/>${grptyp.des }</li>
						</c:forEach>
						<li style="height: 10px;">&nbsp;</li>
					</ul>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/jscolor.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".form-line").hide();
// 				$(".tool").hide();
				//$(".tool").show();
				loadToolBar([false,false,true,true]);
				function loadToolBar(use){
					$('.tool').html('');
					$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="save"/>',
							title: '<fmt:message key="save"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
								handler: function(){
									if(validate._submitValidate()){
										//表单提交后 保存按钮变灰色，不可用   防止网络卡时 重复点击 造成数据重复提交
										loadToolBar([false,false,false,true]);
										$("form").submit();
									}
								}
							},{
								text: '<fmt:message key="the_last_step"/>',
								title: '<fmt:message key="the_last_step"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[1],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									//$(".close",parent.document).click();
									$(".form-line").hide();
									$(".choose").show();
									loadToolBar([false,false,true,true]);
								}
							},{
								text: '<fmt:message key="the_next_step"/>',
								title: '<fmt:message key="the_next_step"/>',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[2],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									var code = [];
									$(".choose").find(":checked").each(function(){
										if($(this).attr("checked") =='checked' && $(this).attr("id")!='all'){
											code.push($(this).val());
										}
									});
									if(code.length==0){
										alert("<fmt:message key='please_select_daily_goods_categories_to_be_added'/>");
										return;
									}
									$(".form-line").show();
									$(".choose").hide();
									$("#category_Code").val(code.join(","));
									loadToolBar([true,true,false,true]);
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[3],
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$(".close",parent.document).click();
								}
							}
						]
					});
				} 
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'orderDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="order_goods"/><fmt:message key="date"/><fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'orderTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="order_goods"/><fmt:message key="time"/><fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="receive_goods"/><fmt:message key="date"/><fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="receive_goods"/><fmt:message key="time"/><fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'orderColor',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="order_goods"/><fmt:message key="color"/><fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveColor',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="receive_goods"/><fmt:message key="color"/><fmt:message key="cannot_be_empty" />！']
					
					}]
				});
				
				$("#all").click(function(){
					$(".tool").show();
					if($(this).attr("checked")=='checked'){
						$(".choose").find(".category_code").attr("checked",true);
					}else{
						$(".choose").find(".category_code").attr("checked",false);
					}
				});
			});
			function saveScheduleD(){
				if(validate._submitValidate()){
					//表单提交后 保存按钮变灰色，不可用   防止网络卡时 重复点击 造成数据重复提交
					loadToolBar([false,false,false,true]);
// 					alert("======");
					$("form").submit();
					
				}
			}
		</script>
	</body>
</html>