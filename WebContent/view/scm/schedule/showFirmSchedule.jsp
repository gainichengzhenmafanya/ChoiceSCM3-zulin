<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>分店班表</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<form action="<%=path%>/schedule/toListFirmSchedule.do" id="leftForm" method="post">
<div class="leftFrame" style="width: 100%;">
	<input type="hidden" name="sCode" id="sCode" value="${schedule.sCode }"/>
	<div class="tool"> </div>
		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:13px;text-align: center;"></span></td>
								<td><span style="width: 130px;"><fmt:message key="name"/></span></td>
<!-- 								<td><span style="width: 70px;">开始时间</span></td> -->
<!-- 								<td><span style="width: 70px;">结束时间</span></td> -->
								<td><span style="width: 170px;"><fmt:message key="explain"/></span></td>
								<td><span style="width: 60px;"><fmt:message key="positn_state"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="schedule" varStatus="step" items="${scheduleList}">
								<tr>
									<td class="num"><span  style="width:25px;">${step.count}</span></td>
									<td><input style="width: 20px;" type="checkbox" name="scheduleID" id="lines_${schedule.scheduleID}" value="${schedule.scheduleID}"/></td>
									<td><span style="width: 130px;" title="${schedule.scheduleName}">${schedule.scheduleName}</span></td>
<%-- 									<td><span style="width: 70px;" title="${schedule.beginDate}"><fmt:formatDate value="${schedule.beginDate}" type="date" /></span></td> --%>
<%-- 									<td><span style="width: 70px;" title="${schedule.endDate}"><fmt:formatDate value="${schedule.endDate}" type="date" /></span></td> --%>
									<td><span style="width: 170px;" title="${schedule.comments}">${schedule.comments}</span></td>
									<td><span style="width: 60px;" title="${schedule.status}"><c:if test="${schedule.status=='N'}"><fmt:message key="disable" /></c:if>
									<c:if test="${schedule.status=='Y'}"><fmt:message key="enable" /></c:if></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</div>
		</form>
<!-- 		<div style="display: inline-block;float: left;width: 50%;" class="mainFrame"> -->
<!--     		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe> -->
<!--     	</div> -->
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.moduleInfo'],$(document.body),25);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		});
		
		$('.tool').toolbar({
			items: [{
					text: '<fmt:message key="delete" />',
					title: '<fmt:message key="delete" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						//parent.parent.$('.close').click();
						deleteSchedule();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.parent.$('.close').click();								
					}
				}
			]
		});
		
		//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
		$('.grid').find('.table-body').find('tr').live("click", function () {
			$('.grid').find('.table-body').find(":checkbox").removeAttr("checked");
	        $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
		 });
 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		$('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		function deleteSchedule(){
			if($('.grid').find('.table-body').find(":checked").length==0){
				alert('<fmt:message key="Please_choose_a_class" />！');
				return;
			}
			var scheduleID = [];
			$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
				scheduleID.push($(this).val());
			});
			var sCode = $('#sCode').val();
			if(scheduleID.length!=0){
				if(confirm('<fmt:message key="delete_data_confirm" />？')){
					var action = '<%=path%>/schedule/deleteFirmSchedule.do?scheduleID='+scheduleID.join(",")+'&sCode='+sCode;
					$('body').window({
						title: '<fmt:message key="Delete_distribution_class" />',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: '340px',
						height: '255px',
						draggable: true,
						isModal: true
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
				return ;
			}
		};
		</script>
</body>
</html>