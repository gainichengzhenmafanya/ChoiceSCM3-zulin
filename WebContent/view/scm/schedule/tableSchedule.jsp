<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送线路明细管理</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
	<div class="tool"> </div>
		<div class="grid">
			<input type="hidden" id="scheduleId" value="${scheduleId}"/>
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span class="num" style="width: 25px;">&nbsp;</span></td>
							<td>
								<span style="width: 30px;"> 
									<input type="checkbox" id="chkAll" />
								</span>
							</td>
							<td><span style="width: 65px;"><fmt:message key ="category" /></span>
							</td>
							<td><span style="width: 80px;"><fmt:message key="Order_date" /></span>
							</td>
							<td><span style="width: 80px;"><fmt:message key ="order_time" /></span>
							</td>
							<td><span style="width: 80px;"><fmt:message key ="arrival_date" /></span>
							</td>
							<td><span style="width: 80px;"><fmt:message key="Arrival_time" /></span>
							</td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="scheduleDetails" varStatus="step" items="${scheduleDetailsList}">
							<tr>
								<td><span class="num" style="width: 25px;">${status.index+1}</span>
								</td>
								<td><span style="width: 30px; text-align: center;">
										<input type="checkbox" name="idList" value="${scheduleDetails.scheduleDetailsID}" /> </span>
								</td>
								<td><span style="width: 65px;" title="${scheduleDetails.category}">${scheduleDetails.category}&nbsp;</span>
								</td>
								<td><span style="width: 80px;" title="${scheduleDetails.orderDate}">${scheduleDetails.orderDate}&nbsp;</span>
								</td>
								<td><span style="width: 80px;" title="${scheduleDetails.orderTime}">${scheduleDetails.orderTime}&nbsp;</span>
								</td>
								<td><span style="width: 80px;" title="${scheduleDetails.receiveDate}">${scheduleDetails.receiveDate}&nbsp;</span>
								</td>
								<td><span style="width: 80px;" title="${scheduleDetails.receiveTime}">${scheduleDetails.receiveTime}&nbsp;</span>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							addScheduleDetails();
						}
					},{
						text: '<fmt:message key ="update" />',
						title: '<fmt:message key ="update" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							updateScheduleDetails();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteScheduleDetails();
						}
					}
				]
			});
		
			setElementHeight('.grid',['.moduleInfo'],$(document.body),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid',50);	//计算.table-body的高度
		
 			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
	 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			//---------增、删、改--------------------------------------------------------------------------
			function addScheduleDetails(){
				var scheduleID = $("#scheduleId").val();
				$('body').window({
					id: 'window_Schedule',
					title: '<fmt:message key="insert" /><fmt:message key="Delivery_schedule" />',
					content: '<iframe id="saveScheduleFrame" frameborder="0" src="<%=path%>/schedule/addScheduleDetails.do?scheduleID='+scheduleID+'"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			};
			
			function updateScheduleDetails(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var chkValue = '';
					checkboxList.filter(':checked').each(function(){
						chkValue = $(this).val();
					});
					$('body').window({
						id: 'window_update_surveyd',
						title: '<fmt:message key="update" /><fmt:message key="Delivery_schedule" />',
						content: '<iframe id="updateScheduleFrame" frameborder="0" src="<%=path%>/schedule/updateScheduleDetails.do?scheduleDetailsID='+chkValue+'"></iframe>',
						width: 400,
						height: 360,
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}
			};
			
			function deleteScheduleDetails(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm("<fmt:message key="delete_data_confirm" />?")){
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							codeValue.push($(this).val());
						});
						var action = '<%=path%>/schedule/deleteScheduleDetails.do?scheduleDetailsID='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key ="Delete_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="Select_the_data_you_need_to_delete" />！');
					return ;
				}
			}
			
			//------------------------------------------------------------------------------------
			
		});
		</script>
</body>
</html>