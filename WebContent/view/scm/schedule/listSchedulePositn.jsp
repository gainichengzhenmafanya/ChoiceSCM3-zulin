<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
/* 				.leftFrame{width: 25%;} */
			</style>
		</head>
	<body>
		<div class="leftFrame">
      	<div id="toolbar"></div>
      	<form id="listForm" action="<%=path%>/schedule/findScheduleByPositn.do?typn=4" method="post">
	      	<div class="form-line" style="margin-top: 2px;">
				<div class="form-label" style="width: 45px;"><fmt:message key ="branches_name" />：</div>
				  <div class="form-input" style="width: 80px;">
					<input type="text" name="des" id="des" style="width:70px;" value="${positn.des}" style="width: 80px;" onkeydown="javascript: if(event.keyCode==13){$('#listForm').submit();} "/>
				</div>
				<div class="tool"></div>
			</div>
		</form><hr/>
	    <div class="treePanel1" style="overflow-x:auto;height: 88%;">
	    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
	          var tree = new MzTreeView("tree");
	          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key ="branche" />;method:changeURl("","")';
	        //遍历所有区域
	          <c:forEach var="area" items="${codeDesList}" varStatus="status">
          		tree.nodes['00000000000000000000000000000000_${area.des }'] 
         				= 'text:${area.code},${area.des};';
	          </c:forEach>
	        //遍历所有分店
       		  <c:forEach var="positn" items="${listPositn }">
       			tree.nodes['${positn.area}_${positn.code }'] 
       				= 'text:${positn.code},${positn.des};method:turnTo("${positn.code}")';
       		  </c:forEach>	
	          tree.setIconPath("<%=path%>/image/tree/none/");
	          document.write(tree.toString());
	          //tree.expandAll();//默认展开全部节点
	        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="<%=path%>/schedule/toListFirmSchedule.do?sCode=1" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//实现滚动条
			setElementHeight('.treePanel',['#toolbar'],$(document.body),10);
			
			var offset = $(".tool").prev("div").offset();
			offset.left = $(".tool").prev("div").width() + offset.left + 10;
			$(".tool").button({
				text:'<fmt:message key="select" />',
				container:$(".tool"),
				position: {
					type: 'absolute',
					top: offset.top,	
					left: offset.left
				},
				handler:function(){
					$("#listForm").submit();
				}
			});
			//自动实现滚动条
		//	setElementHeight('.grid',['.tool'],$(document.body),40);	//计算.grid的高度
			//setElementHeight('.table-body',['.table-head'],'.grid',20);	//计算.table-body的高度
		//	loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				// var positnType=$(this).find('td:eq(1)').find('span').text();
				 var descode = $(this).find('td:eq(2)').find('input').val();
				 var descodeName = $(this).find('td:eq(1)').text();
				$("#mainFrame").attr("src","<%=path%>/schedule/toListFirmSchedule.do?sCode="+descode);
			});
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		});// end $(document).ready();
		
		
		function turnTo(descode){
			$("#mainFrame").attr("src","<%=path%>/schedule/toListFirmSchedule.do?sCode="+descode);
		}
		
		//根据区域 获取改区域下的所有分店area
		function findByArea(areacode){
			$("#mainFrame").attr("src","<%=path%>/positn/listDetail.do?area="+areacode+"&typ=firm");
		}
		
	</script>

	</body>
</html>