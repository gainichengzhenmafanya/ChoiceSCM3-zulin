<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">    
			font {font-weight:bold;font-style:italic;}
			#txt{overflow-y: scroll;width: 99%;height: 280px;}
		</style>
	</head>
	<body>
		<div class="tool"> </div>
		<div class="form">
			<form id="scheduleForm" method="post" action="<%=path %>/schedule/saveDetailesByUpdate.do">
				<input type="hidden" name="scheduleDetailsID" value="${ScheduleD.scheduleDetailsID}"/>
				<input type="hidden" id="days1" name="days1" value="${days1}"/>
				<input type="hidden" id="days2" name="days2" value="${days2}"/>
				<input type="hidden" id="days3" name="days3" value="${days3}"/>
				<input type="hidden" id="month1" name="month1" value="${month1}"/>
				<input type="hidden" id="month2" name="month2" value="${month2}"/>
				<input type="hidden" id="month3" name="month3" value="${month3}"/>
				<input type="hidden" id="ids" name="ids"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="date"/>：</div>
					<div class="form-input">
						<input type="text" id="orderDate" name=orderDate class="Wdate text"  value="<fmt:formatDate value="${orderDate}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
					</div>
					 <div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="date"/>：</div>
					<div class="form-input">
						<input type="text" id="receiveDate" name=receiveDate class="Wdate text"  value="<fmt:formatDate value="${receiveDate}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="time"/>：</div>
					<div class="form-input">
						<input type="text" id="orderTime" name="orderTime" class="text Wdate" type="date" value="${ScheduleD.orderTime}" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
					</div>
					<div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="time"/>：</div>
					<div class="form-input">
						<input type="text" id="receiveTime" name="receiveTime" class="text Wdate" type="date" value="${ScheduleD.receiveTime}" onfocus="WdatePicker({dateFmt:'HH:mm'})"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="order_goods"/><fmt:message key="color"/>：</div>
					<div class="form-input">
						<div style="clear: both; height: 23; width: 20; float: left;"
							id="orid"></div>
						<input type="text" class="color {pickerClosable:true} text" id="orderColor" name="orderColor" value="${ScheduleD.orderColor}" style="background-color:${ScheduleD.orderColor};" />
					</div>
					<div class="form-label"><fmt:message key="receive_goods"/><fmt:message key="color"/>：</div>
					<div class="form-input">
						
						<div style="clear: both; height: 23; width: 20; float: left;"
							id="reid"></div>
						<input type="text" class="color {pickerClosable:true} text" id="receiveColor" name="receiveColor" value="${ScheduleD.receiveColor}" style="background-color:${ScheduleD.receiveColor};" />
						
					</div>
				</div>
				<div id="txt"><br/><font size="2" face="Times" color="red"><fmt:message key ="Check_the_date_as_the_date_of_the_order_you_can_copy_and_produce_the_distribution_class_according_to_the_above_cycle" /></font><br/></div>
			</form>
		</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
    	<script type="text/javascript" src="<%=path%>/js/scm/jscolor.js"></script>
		<script type="text/javascript">
			var ColorHex=new Array('00','33','66','99','CC','FF');
			var SpColorHex=new Array('FF0000','00FF00','0000FF','FFFF00','00FFFF','FF00FF');
			var current=null;
			$(document).ready(function(){
				
				//wangjie 2014年10月23日 14:11:54
				$("#txt").on("click","input[name=checkall]",function(){
					var _class = $(this).attr("class");
					if($(this).attr("checked") == 'checked'){//全选
						$("#txt").find("input[name="+_class+"]").map(function(){
							$(this).attr("checked","checked");
						});
					}else{//反选
						$("#txt").find("input[name="+_class+"]").map(function(){
							$(this).removeAttr("checked");
						});
					}
				});
				
				var montharray=[];
				montharray.push($("#month1").val());
				montharray.push($("#month2").val());
				montharray.push($("#month3").val());
				
				var html = $("#txt").html();
				//alert(montharray.join(","));
				for(var month=0;month<montharray.length;month++){
					html+='<b><span>'+montharray[month]+'</span>&nbsp;<input type="checkbox" name="checkall" class="days'+montharray[month]+'"/></b>';
					html+='<table width="97%" cellspace=0>';
					html+='<tr style="background:#f2f2f2;height:20px;text-align:center"><th><fmt:message key ="week7" /></th><th><fmt:message key ="week1" /></th><th><fmt:message key ="week2" /></th><th><fmt:message key ="week3" /></th><th><fmt:message key ="week4" /></th><th><fmt:message key ="week5" /></th><th><fmt:message key ="week6" /></th></tr>';
					var _month = montharray[month];
					var begin_day = new Date (_month.split("-")[0],_month.split("-")[1]-1,1);
					var begin_day_date = begin_day.getDay();
					var end_day = new Date (_month.split("-")[0],_month.split("-")[1],1);
					var count_day = (end_day - begin_day)/1000/60/60/24;
					var count = 0;
					
					if(count_day==31 && (begin_day_date==5 || begin_day_date ==6)){
						count = 6;
					}else if(count_day==31 && begin_day_date!=5 && begin_day_date!=6){
						count = 5;
					}else if(count_day == 30 && (begin_day_date==6)){
						count = 6;
					}else if(count_day == 30 && begin_day_date!=6){
						count = 5;
					}else if(count_day == 28 && begin_day_date ==0){
						count = 4;
					}else{
						count = 5;
					}
					
					
					for(var i=0;i<count;i++){
						html+='<tr style="background:#f2f2f2;height:20px;text-align:center">'
						for(var j=0;j<7;j++){
							html+='<td id="'+montharray[month].split("-")[1]+'d'+j+'j'+i+'">&nbsp;</td>';
						}
						html+='</tr>';
					}
					html+='</table>';
				}
				

				$("#txt").html(html).find("table").map(function(){
					for(var mon=0;mon<montharray.length;mon++){
						init(montharray[mon].split("-")[1]);
						input_calendar(montharray[mon]);
					}
				});
				//自动实现滚动条
				setElementHeight('.form',['.tool'],$(document.body),80);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
							handler: function(){
								saveScheduleD();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'orderDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'orderTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveDate',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveTime',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'orderColor',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					},{
						type:'text',
						validateObj:'receiveColor',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="cannot_be_empty" />！']
					
					}]
				});
			});
			function saveScheduleD(){
				var chkValue = [];
				$(':checkbox').filter(':checked').each(function(i){//wangjie
					if($(this).val()!='on'){
						chkValue.push($(this).val());
					}
				});
				$('#ids').val(chkValue.join(','));
				if(validate._submitValidate()){
					$("form").submit();
				}
			}
			
			function intocolor()
			{
				var p = document.getElementById('colorpanel');
			    p.style.visibility = 'visible';
				var colorTable='';
				for (var i=0;i<2;i++)
				 {
				  for (var j=0;j<6;j++)
				   {
				    colorTable=colorTable+'<tr height=12>';
				    colorTable=colorTable+'<td width=11 style="background-color:#000000">';
				    
				    if (i==0){
				    colorTable=colorTable+'<td width=11 style="background-color:#'+ColorHex[j]+ColorHex[j]+ColorHex[j]+'">';} 
				    else{
				    colorTable=colorTable+'<td width=11 style="background-color:#'+SpColorHex[j]+'">';} 
				 
				    
				    colorTable=colorTable+'<td width=11 style="background-color:#000000">';
				    for (var k=0;k<3;k++)
				     {
				       for (var l=0;l<6;l++)
				       {
				        colorTable=colorTable+'<td width=11 style="background-color:#'+ColorHex[k+i*3]+ColorHex[l]+ColorHex[j]+'">'
				       }
				     }
				  }
				}
				colorTable='<table width=221 border="0" cellspacing="0" cellpadding="0" style="border:1px #000000 solid;border-bottom:none;border-collapse: collapse" bordercolor="000000">'
				           +'<tr height=30><td colspan=21 bgcolor=#cccccc>'
				           +'<table cellpadding="0" cellspacing="1" border="0" style="border-collapse: collapse">'
				           +'<tr><td width="3"><td><input type="text" id="DisColor" name="DisColor" size="6" disabled style="border:solid 1px #000000;background-color:#ffff00"></td>'
				           +'<td width="3"><td><input type="text" id="HexColor" name="HexColor" size="7" style="border:inset 1px;font-family:Arial;" value="#000000"></td></tr></table></td></table>'
				           +'<table border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse" bordercolor="000000" onmouseover="doOver()" onmouseout="doOut()" onclick="doclick()" style="cursor:hand;">'
				           +colorTable+'</table>';          
				colorpanel.innerHTML=colorTable;
			}
			
			function rececolor(){
				var p = document.getElementById('colorpanel2');
			    p.style.visibility = 'visible';
				var colorTable='';
				for (var i=0;i<2;i++)
				 {
				  for (var j=0;j<6;j++)
				   {
				    colorTable=colorTable+'<tr height=12>';
				    colorTable=colorTable+'<td width=11 style="background-color:#000000">';
				    
				    if (i==0){
				    colorTable=colorTable+'<td width=11 style="background-color:#'+ColorHex[j]+ColorHex[j]+ColorHex[j]+'">';} 
				    else{
				    colorTable=colorTable+'<td width=11 style="background-color:#'+SpColorHex[j]+'">';} 
				 
				    
				    colorTable=colorTable+'<td width=11 style="background-color:#000000">';
				    for (var k=0;k<3;k++)
				     {
				       for (var l=0;l<6;l++)
				       {
				        colorTable=colorTable+'<td width=11 style="background-color:#'+ColorHex[k+i*3]+ColorHex[l]+ColorHex[j]+'">';
				       }
				     }
				  }
				}
				colorTable='<table width=221 border="0" cellspacing="0" cellpadding="0" style="border:1px #000000 solid;border-bottom:none;border-collapse: collapse" bordercolor="000000">'
				           +'<tr height=30><td colspan=21 bgcolor=#cccccc>'
				           +'<table cellpadding="0" cellspacing="1" border="0" style="border-collapse: collapse">'
				           +'<tr><td width="3"><td><input type="text" id="DisColor" name="DisColor" size="6" disabled style="border:solid 1px #000000;background-color:#ffff00"></td>'
				           +'<td width="3"><td><input type="text" id="HexColor" name="HexColor" size="7" style="border:inset 1px;font-family:Arial;" value="#000000"></td></tr></table></td></table>'
				           +'<table border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse" bordercolor="000000" onmouseover="doOver()" onmouseout="doOut()" onclick="doclick2()" style="cursor:hand;">'
				           +colorTable+'</table>';          
				colorpanel2.innerHTML=colorTable;
			}
			function doclick(){
				if (event.srcElement.tagName=="TD"){
						document.getElementById("orderColor").value = event.srcElement._background;
						orid.style.backgroundColor = event.srcElement._background;
						$('#orderColor').attr("style","background-color:"+event.srcElement._background+"");						
				}
				return event.srcElement._background;
			}
			function doclick2(){
				if (event.srcElement.tagName=="TD"){
						document.getElementById("receiveColor").value = event.srcElement._background;
						reid.style.backgroundColor = event.srcElement._background;
						$('#receiveColor').attr("style","background-color:"+event.srcElement._background+"");
						
				}
				return event.srcElement._background;
			}
			
			function doOver() {
			      if ((event.srcElement.tagName=="TD") && (current!=event.srcElement)) {
			        if (current!=null){current.style.backgroundColor = current._background;}  
			        event.srcElement._background = event.srcElement.style.backgroundColor;
			        document.getElementById("DisColor").style.backgroundColor = event.srcElement.style.backgroundColor;
			        document.getElementById("HexColor").value = event.srcElement.style.backgroundColor;
			        event.srcElement.style.backgroundColor = "white";
			        current = event.srcElement;
			      }
			}
			 
			function doOut() {
			    if (current!=null) current.style.backgroundColor = current._background;
			}
			 
		
			var isOnDiv = false;
		    function hide(event) {
		      if(!isOnDiv) {
		        var p = document.getElementById('colorpanel');
		        p.style.visibility = 'hidden';
		      }
		    }
			function ok(el) {
		      var isOnDiv=false;
		      var box = document.getElementById('orderColor');
		      box.focus();
		    }
		    function hide2(event) {
		      if(!isOnDiv) {
		        var p = document.getElementById('colorpanel2');
		        p.style.visibility = 'hidden';
		      }
		    }
		    function ok2(el) {
		      var isOnDiv=false;
		      var box = document.getElementById('receiveColor');
		      box.focus();
		    }
		    function inDiv() {
		      isOnDiv = true;
		    }
		    function leftDiv() {
		      isOnDiv = false;
		    }
		    
		    function init(_month){
		    	for(var j=0;j<7;j++){
		    	 for(var i=0;i<6;i++){
			    	$("#"+_month+"d"+j+"j"+i).html("-");
		    	 }
		       }
		    }
		    
		    function input_calendar(_month){
				var begin_day = new Date (_month.split("-")[0],_month.split("-")[1]-1,1);
				var begin_day_date = begin_day.getDay();
				var end_day = new Date (_month.split("-")[0],_month.split("-")[1],1);
				var count_day = (end_day - begin_day)/1000/60/60/24;
		    	var m,n,k;
				m = 0;
				n = 0;
				k = 0;
				if(begin_day_date!=0){
					k = begin_day_date;
				}else{
					k = 0;
				}
				for(n=1;n<count_day+1;n++){
					if(n<10){
						$("#"+_month.split("-")[1]+"d"+k+"j"+m).html("<input type='checkbox' name='days"+_month+"' value='"+_month+"-0"+n+"'/>0"+n);
					}else{
						$("#"+_month.split("-")[1]+"d"+k+"j"+m).html("<input type='checkbox' name='days"+_month+"' value='"+_month+"-"+n+"'/>"+n);
					}
					k++;
					if(k==7){
						k=0;
						m++;
					}
				}
		    }
		</script>
	</body>
</html>