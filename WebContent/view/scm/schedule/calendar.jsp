<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>配送班表</title>
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/Calendar.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/popupUtil.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">   
		var date;
		var month='<c:out value="${month}"/>';
		if (month==''||month==null) {
			date=new Date();
		}else{
			date=new Date(month.substring(0,4),month.split("-")[1]-1,'01');
		}
		$(document).ready(function(){
			createCalendar();
		});
		function pageReload(){ 
			$('#listForm').submit();
		}
		function createCalendar(){ 
			clearTable();
			//var date = new Date(year,month,day);
			//判断1号星期几 
		 	var weekIndex = new Date(date.getFullYear(),date.getMonth(),1).getDay();//星期
			//获取该月天数
			var maxDay = GetMonthDayCount(date);
			var tb = document.getElementById("tb_calendar");
			//alert(tb.rows[1].cells[0].innerText);
			var rowIndex = 1;
			var colIndex = 0;
			for(var i=0;i<weekIndex;i++){
				tb.rows[rowIndex].cells[colIndex].innerText = " ";
				colIndex++;
			} 
			
			//alert(maxDay);
			for(var m=0;m<maxDay;m++){
				var omonth = date.getMonth()+1;
				var oday = m+1;
				if (omonth < 10){
					omonth = "0" + omonth;
				}
				if (oday < 10){
					oday = "0" + oday;
				}
				var odate = date.getFullYear() + "" + omonth + "" + oday;
				var txxodate = date.getFullYear() + "-" + omonth + "-" + oday;
<%-- 				document.getElementById("scode").value = "<%=request.getParameter("scode")%>"; --%>
// 				var scode = document.getElementById("scode").value;
				var code = document.getElementById("code").value;
				tb.rows[rowIndex].cells[colIndex].innerHTML = "<div id='div_"+oday+"' style='height: 80px;width:98%' >"
							+"<div style='height: 20px;'>"
							+"	<div style='float: left;'><span>"+oday+"</span></div>"
							+"	<div style='float: right;'><span style='cursor: hand;' onclick='openwindow(\""+odate+"\",\""+m+"\",\""+txxodate+"\")'><img src=\"<%=path%>/image/themes/icons/jia.jpg\" "+"style=\"cursor: hand\" width=\"16\" height=\"16\" /></span></div>"
							+"</div>"
							+"<div style='width:100%;height:60px;overflow-y: auto;text-align:center;' >"
							+"	<table style='width:100%;height:auto;' id='table_"+oday+"'>"
							+"	</table>"
							+"</div>"
						+"</div>";
				// tb.rows[rowIndex].cells[colIndex].innerHTML =  "<iframe name='frame' id='frame1_"+oday+"'  width='100%' height='80' src='<%=path%>Choice/schedule_findSchedule.java?date=" + odate + "&day=" + (m+1)  +"&code=" + code + "' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' style='border:0px #ffffff solid' ></iframe>"; 
				//tb.rows[rowIndex].cells[colIndex].innerHTML ="<iframe name='frame' id='frame1'  width='100%' height='80'  src='<%=path%>model/schedule/listSchedule.jsp?day="+date.getFullYear()+"-"+(date.getMonth()+1)+(m+1)+"' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' style='border:0px #ffffff solid' ></iframe>";
				colIndex++;
				if(colIndex == 7){
					rowIndex++;
					colIndex = 0;
				}
			}

			var scheduleMonth = date.getFullYear()+"-"+(date.getMonth()+1);
			refresh(scheduleMonth);
			if(date.getMonth()<9){
				document.getElementById("divDate").innerText=date.getFullYear()+"-0"+(date.getMonth()+1);
				$('#month').val(document.getElementById("divDate").innerText);
			}else{
				document.getElementById("divDate").innerText=date.getFullYear()+"-"+(date.getMonth()+1);
				$('#month').val(document.getElementById("divDate").innerText);
			}
		}
		
		function openwindow(date,day,odate){
			var code = getType();
// 			if  (code == "-1"){
// 				alert("请先选择报货类别！");
// 				return;
// 			}
			var scheduleID = document.getElementById("scheduleID").value;
			$('body').window({
				id: 'window_saveSchedule',
				title: '<fmt:message key ="Add_distribution_class_list_details" />',
				content: '<iframe id="saveScheduleFrame" frameborder="0" src="'+"<%=path%>/schedule/addSchedule.do?scheduleID="+scheduleID+"&date="+odate+"&category_Code="+code+'"></iframe>',
				width: '550px',
				height: '380px',
				draggable: true,
				isModal: true
			});
		}
		
		function clearTable(){
			var tb = document.getElementById("tb_calendar");
			for(var m=1;m<tb.rows.length;m++){ 
				for(var i=0;i<tb.rows[0].cells.length;i++){
					tb.rows[m].cells[i].innerHTML="";
				}
			}
		}
		
		function getType(){
			return document.getElementById("code").value;
		}
		
		//上一月
		function goUp(){
			date.setMonth(date.getMonth()-1);  
			createCalendar();
		}
		//下一月
		function goDown(){
			date.setMonth(date.getMonth()+1);
			createCalendar();
		}
		function copyTyp(){
			var code=document.getElementById("code").value;
			if (code=="-1"){
				alert("<fmt:message key ="Please_choose_a_copy_of_the_report_goods_category" />！");
				return;
			}
			var scheduleID = document.getElementById("scheduleID").value;
			$('body').window({
				id: 'window_saveSchedule',
				title: '<fmt:message key ="Copy_and_distribution_list_details" />',
				content: '<iframe id="saveScheduleFrame" frameborder="0" src="'+"<%=path%>/schedule/copyScheduleDetails_c.do?scheduleID="+scheduleID+"&category_Code="+code+'"></iframe>',
				width: '550px',
				height: '380px',
				draggable: true,
				isModal: true
			});
// 			refresh();
		}
		function GetMonthDayCount(newDate){  
			switch(newDate.getMonth()+1)  
			{  
				case   1:case   3:case   5:case   7:case   8:case   10:case   12:  
				return   31;  
				case   4:case   6:case   9:case   11:  
				return   30;  
			}  
			//feb:  
			newDate = new Date(newDate);
			var   lastd=28;  
			newDate.setDate(29);  
			while(newDate.getMonth()==1)  
			{  
				lastd++;  
				newDate.setDate(newDate.getDate()+1);   
			}  
			return   lastd;  
		}
		function ocus(obj){
			$(obj).find("img").css("visibility","visible");
		}
		function unocus(obj){
			$(obj).find("img").css("visibility","hidden");
		}
		function refresh(scheduleMonth) {
			if (scheduleMonth != null || typeof(scheduleMonth) !="undefined"){
				var oday = "";
				if (scheduleMonth.split("-")[1] < 10){
					oday = "0" + scheduleMonth.split("-")[1];
				}else{
					oday=scheduleMonth.split("-")[1];
				}
				scheduleMonth = scheduleMonth.split("-")[0]+"-"+oday;
			} else {
				scheduleMonth=document.getElementById("divDate").innerText;
				
				if(scheduleMonth =='' || scheduleMonth==null){
					var date1 = new Date();
					scheduleMonth = date1.getFullYear()+"-"+(date1.getMonth()+1);
				}
				
				var maxDay = GetMonthDayCount(new Date(scheduleMonth.split("-")[0],scheduleMonth.split("-")[1]-1,1));
				for(var m=1;m<=maxDay;m++){ 
					var oday = "";
					if (m < 10){
						oday = "0" + m;
					}else{
						oday=m;
					}
					var tmptable = document.getElementById("table_"+oday);
					for (var trow=tmptable.rows.length - 1;trow>=0;trow--){
						tmptable.deleteRow(trow);
					}
				}
				var omonth = "";
				if (scheduleMonth.split("-")[1] < 10){
					omonth = scheduleMonth.split("-")[1];
				}else{
					omonth=scheduleMonth.split("-")[1];
				}
				scheduleMonth = scheduleMonth.split("-")[0]+"-"+omonth;
			}
			$.ajax({
		        url:"<%=path%>/schedule/findScheduleByMonth.do",
		        cache:false,
				type:'post',
				async:false,
				data:'scheduleMonth='+scheduleMonth+"&scheduleID="+document.getElementById("scheduleID").value+"&category_Code="+document.getElementById("code").value,
				dataType:'json',
				ifModified:true,
				timeout: 30000,
				error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('Error loading json document:'+XMLHttpRequest.readyState);
				},
				success: function(msg){	
					var scheduleMonth = date.getFullYear()+"-"+(date.getMonth()+1);
					var omonth = "";
					if (scheduleMonth.split("-")[1] < 10){
						omonth = "0" + scheduleMonth.split("-")[1];
					}else{
						omonth=scheduleMonth.split("-")[1];
					}
					scheduleMonth = scheduleMonth.split("-")[0]+"-"+omonth;
					if(msg.data!=null && msg.data.length>0){
						for (var i = 0;i<msg.data.length;i++){
							if (msg.data[i].orderDate.substring(0,7)==scheduleMonth){
								var table = document.getElementById("table_"+msg.data[i].orderDate.substring(8));
								var newRow = table.insertRow(-1);
							 	newCell = newRow.insertCell(0);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "left";
							 	newCell.style.cursor = "hand";
							 	newCell.style.background = msg.data[i].orderColor;
							 	newCell.title = '<fmt:message key ="order_time" />:'+msg.data[i].orderDate+'<fmt:message key ="Delivery_time" />:'+msg.data[i].receiveDate;
							 	newCell.innerHTML = '<span style="color: black"><fmt:message key ="order_goods" />：'+msg.data[i].category_Code+'</span>'; 
							 	var tmpOrdDate= msg.data[i].orderDate;
							 	var tmpid = msg.data[i].scheduleDetailsID;
							 	newCell.onclick = (function(tmpid,tmpOrdDate){
	                                        return function(){findschedulebyid(tmpid,tmpOrdDate);}
	                                })(tmpid,tmpOrdDate);
							 	
							 	newCell = newRow.insertCell(1);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "right";
							 	newCell.width="16";
							 	newCell.valign="middle";
						 		newCell.innerHTML = "<span class='STYLE1' style=\"cursor: hand;border:1px solid white;\" onclick=\"\" onmouseover =\"ocus(this);\"  onmouseout=\"unocus(this);\">  "
											+"<img src=\"<%=path%>/image/scm/del.gif\" "
											+" onclick='delorderschedule(\""+msg.data[i].scheduleDetailsID+"\",\""+msg.data[i].orderDate+"\",\""+msg.data[i].receiveDate+"\")' "
											+"	 style=\"cursor: hand;visibility:hidden;\" width=\"16\" height=\"16\" /> </span>"; 
							}
						}
					}
					if(msg.data2 != null && msg.data2.length>0){
						for (var i = 0;i<msg.data2.length;i++){
							if (msg.data2[i].receiveDate.substring(0,7)==scheduleMonth){
								var table = document.getElementById("table_"+msg.data2[i].receiveDate.substring(8));
								var newRow = table.insertRow(-1);
							 	newCell = newRow.insertCell(0);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "left";
							 	newCell.style.cursor = "hand";
							 	newCell.style.background = msg.data2[i].receiveColor;
							 	newCell.title = ""+msg.data2[i].receiveDate;
							 	newCell.title = ""+msg.data2[i].receiveDate;
							 	newCell.title = '<fmt:message key ="order_time" />:'+msg.data2[i].orderDate+'<fmt:message key ="Delivery_time" />:'+msg.data2[i].receiveDate;
							 	newCell.innerHTML = '<span style="color: black"><fmt:message key ="receive_goods" />：'+msg.data2[i].category_Code+'</span>'; 
							 	var tmpOrdDate= msg.data2[i].receiveDate;
							 	var tmpid = msg.data2[i].scheduleDetailsID;
							 	newCell.onclick = (function(tmpid,tmpOrdDate){
	                                        return function(){findschedulebyid(tmpid,tmpOrdDate);}
	                                })(tmpid,tmpOrdDate);
							 	newCell = newRow.insertCell(1);
							 	newCell.noWrap = "nowrap";
							 	newCell.align = "right";
							 	newCell.width="16";
							 	newCell.innerHTML = "<span class='STYLE1' style=\"cursor: hand;border:1px solid white;\" onclick=\"\"  onmouseover =\"ocus(this);\"  onmouseout=\"unocus(this);\">  "
											+"<img src=\"<%=path%>/image/scm/del.gif\" "
											+" onclick='delorderschedule(\""+msg.data2[i].scheduleDetailsID+"\",\""+msg.data2[i].orderDate+"\",\""+msg.data2[i].receiveDate+"\")' "
											+"	style=\"cursor: hand;visibility:hidden;\" width=\"16\" height=\"16\" /> </span>"; 
							}
						}
					}
				}
			});
		}
		
		//编辑界面，还没写
		function findschedulebyid(scheduleID, date){
			var scheduleMonth=document.getElementById("divDate").innerText;
			//wangjie
			if(scheduleMonth =='' || scheduleMonth==null){
				var date = new Date();
				scheduleMonth = date.getFullYear()+"-"+(date.getMonth()+1);
			}
			var days = 0;
			var tb = document.getElementById("tb_calendar");
			for(var m=1;m<tb.rows.length;m++){ 
				for(var i=0;i<tb.rows[m].cells.length;i++){
					if (tb.rows[m].cells[i].innerHTML!="" && tb.rows[m].cells[i].innerHTML!= '&nbsp;'){
						days++;
					}
				}
			}
			$('body').window({
				id: 'window_saveSchedule',
				title: '<fmt:message key ="Modify_the_delivery_schedule_details" />',
				content: '<iframe id="saveScheduleFrame" frameborder="0" src="'+"<%=path%>/schedule/updateScheduleDetails_c.do?scheduleDetailsID="+scheduleID+"&date="+date+"&days="+days+"&scheduleMonth="+scheduleMonth+'"></iframe>',
				width: '750px',
				height: '420px',
				draggable: true,
				isModal: true
			});
			refresh();
		}
		
		//删除一条详情
		function delorderschedule(scheduleDetailsID,ordate,redate) {
			if(window.confirm('<fmt:message key ="Are_you_sure_you_want_to_delete_it" />？')){
				$.ajax({
			        url:"<%=path%>/schedule/deleteScheduleDetails.do",
			        cache:false,
					type:'post',
					async:false,
					data:'scheduleDetailsID='+scheduleDetailsID+'&orderDate='+ordate+'&category_Code='+document.getElementById("code").value+'&receiveDate='+redate,
					dataType:'json',
					ifModified:true,
					timeout: 30000,
					error: function(XMLHttpRequest, textStatus, errorThrown)
					{
						alert('<fmt:message key="Delete_exception" />:'+XMLHttpRequest.readyState);
					},
					success: function(msg){	
						refresh();
// 						refreshByDay(ordate,redate);
// 						alert("操作成功！");
					}
			   });
			}
		}
	</script>
	</head>
	<body>
		<form id="listForm" action="<%=path%>/schedule/listCalendar.do" method="post">
		<input type="hidden" name="scheduleID" id="scheduleID" value="${schedule.scheduleID}" />
		<input type="hidden" id="month" name="month" value="${month}"/>
		<fieldset style="overflow:auto;height:450px;">
		<legend class="STYLE1"><fmt:message key ="The_shipping_schedule" />&nbsp;&nbsp;&nbsp;${schedule.scheduleName }</legend>
			<table width="100%">
				<tr>
					<td width="190">
<!-- 						<input type="button" value="关闭" onclick="window.close()" /> -->
						<input type="button" value="<fmt:message key="scm_pre_month"/>" onclick="goUp()" />
						<input type="button" value="<fmt:message key="scm_next_motn"/>" onclick="goDown()" />
					</td>
					<td>
						<div id="divDate">
<%-- 							<input id = "month" name= "month" value="${month}"/> --%>
						</div>
					</td>
					
					<td>
						<div align="right">
							<fmt:message key="supplyclassification"/>：
						</div>
					</td>
					<td width="150">
						<div align="left">
							<select name="category_Code" onchange="refresh()" style="width: 200px" 
								id="code">
								<option value="-1"><fmt:message key ="all" /> </option>
								<c:forEach items="${grptypList}" var="grptyp">
									<option
										<c:if test="${category_Code == grptyp.code}"> selected="selected" </c:if>
										value="${grptyp.code }">
										${grptyp.des }
									</option>
								</c:forEach>
							</select>
						</div>
					</td>
					<td width="190">
						<input type="button" value="<fmt:message key="scm_copy"/>" onclick="copyTyp()" />
					</td>
				</tr>
			</table>
			<div class="dayView">
				<table width="100%" cellspacing="0" cellpadding="0"
					class="monthGrid" id="tb_calendar" onclick="">
					<thead>
						<tr class="weekdayRow">
							<td class="weekday firstCol" style="width: 14.28%;">
								<fmt:message key ="week7" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week1" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week2" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week3" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week4" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week5" />
							</td>
							<td class="weekday" style="width: 14.28%;">
								<fmt:message key ="week6" />
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
						<tr>
							<td class="contentdayweek firstCol"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentday"></td>
							<td class="contentdayweek"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</fieldset>
		</form>
	</body>
</html>
