<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送班表管理</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
</head>
<body>
<form action="<%=path%>/schedule/list.do" id="leftForm" method="post">
<div class="leftFrame" style="width: 100%;">
	<div class="tool"> </div>
	<div class="search-div" style="position: absolute;z-index: 99">
			<div class="form-line">
				<div class="form-label"><fmt:message key="branche"/>：</div>
				<div class="form-input">
					<select class="select" id="sCode" name="sCode" style="width:133px">
							<option value=""/><fmt:message key ="all" /></option>
							<c:forEach var="positn" items="${positnList}" varStatus="status">
								<option
									<c:if test="${schedule.sCode==positn.code}"> 
								  	 	selected="selected"
									</c:if>  
								id="${positn.code}" value="${positn.code}">${positn.des}</option>
							</c:forEach>
						</select>
				</div>
			</div>
			<div class="search-commit">
	       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
	       		<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
			</div>
		</div>
		<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:13px;text-align: center;"></span></td>
								<td><span style="width: 130px;"><fmt:message key="name"/></span></td>
<!-- 								<td><span style="width: 70px;">开始时间</span></td> -->
<!-- 								<td><span style="width: 70px;">结束时间</span></td> -->
								<td><span style="width: 170px;"><fmt:message key="explain"/></span></td>
								<td><span style="width: 60px;"><fmt:message key="positn_state"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="schedule" varStatus="step" items="${scheduleList}">
								<tr>
									<td class="num"><span  style="width:25px;">${step.count}</span></td>
									<td><input style="width: 20px;" type="checkbox" name="scheduleID" id="lines_${schedule.scheduleID}" value="${schedule.scheduleID}"/></td>
									<td><span style="width: 130px;" title="${schedule.scheduleName}">${schedule.scheduleName}</span></td>
<%-- 									<td><span style="width: 70px;" title="${schedule.beginDate}"><fmt:formatDate value="${schedule.beginDate}" type="date" /></span></td> --%>
<%-- 									<td><span style="width: 70px;" title="${schedule.endDate}"><fmt:formatDate value="${schedule.endDate}" type="date" /></span></td> --%>
									<td><span style="width: 170px;" title="${schedule.comments}">${schedule.comments}</span></td>
									<td><span style="width: 60px;" title="${schedule.status}"><c:if test="${schedule.status=='N'}"><fmt:message key="disabled" /></c:if>
									<c:if test="${schedule.status=='Y'}"><fmt:message key="enable" /></c:if></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</form>
<!-- 		<div style="display: inline-block;float: left;width: 50%;" class="mainFrame"> -->
<!--     		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe> -->
<!--     	</div> -->
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.moduleInfo'],$(document.body),55);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			/* 模糊查询提交 */
			$("#search").bind('click', function() {
				$('.search-div').hide();
				$('#leftForm').submit();
			});
			/* 模糊查询清空 */
			$("#resetSearch").bind('click', function() {
				$('#leftForm').find(".select").find('option:first').attr('selected','selected');
			});
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />',
					title: '<fmt:message key="select" /><fmt:message key="branches_and_positions_information" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$('.search-div').slideToggle(100);
					}
				},"-",{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							saveSchedule();
						}
					},{
						text: '<fmt:message key="update" />',
						title: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							
							position: ['-18px','0px']
						},
						handler: function(){
							updateSchedule();
						}
					},{
						text: '<fmt:message key="scm_copy"/>',
						title: '<fmt:message key="scm_copy"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							
							position: ['-18px','0px']
						},
						handler: function(){
							copySchedule();
						}
					},{
						text: '<fmt:message key="delete" />',
						title: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteSchedule();
						}
					},{
						text: '<fmt:message key="scm_association_branch"/>',//YUKU-127
						title: '<fmt:message key="scm_association_branch"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							toAccountPositn();
						}
					},{
						text: '<fmt:message key="scm_detailed_settings"/>',
						title: '<fmt:message key="scm_detailed_settings"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							toDetail();
						}
					},{
						text: '<fmt:message key="the_branch_class_table"/>',
						title: '<fmt:message key="the_branch_class_table"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							tocheckFirm();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}
				]
			});
			
			
 			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$('.grid').find('.table-body').find(":checkbox").removeAttr("checked");
		        $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			 });
			//列表双击查看
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
				toDetail();
			})
// 			$('.grid').find('.table-body').find('tr:eq(0)').click();
	 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//-添加、修改、删除--------------------------------------------------------------------
			saveSchedule=function(){
				$('body').window({
					id: 'window_Schedule',
					title: '<fmt:message key="insert" /><fmt:message key ="The_shipping_schedule" />',
					content: '<iframe id="saveScheduleFrame" frameborder="0" src="<%=path%>/schedule/saveSchedule.do"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			};
			
			//复制配送班表
			function copySchedule(){
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert('<fmt:message key="Please_choose_a_class" />！');
					return;
				}
				var scheduleID = $('.grid').find('.table-body').find(':checkbox:checked').val();
				$('body').window({
					id: 'window_update_surveyd',
					title: '<fmt:message key ="copy" /><fmt:message key ="The_shipping_schedule" />',
					content: '<iframe id="updateScheduleFrame" frameborder="0" src="<%=path%>/schedule/copy.do?scheduleID='+scheduleID+'"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			}
			
			function deleteSchedule(){
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert('<fmt:message key="Please_choose_a_class" />！');
					return;
				}
				var scheduleID = $('.grid').find('.table-body').find(':checkbox:checked').val();
				if(scheduleID!=null){
					if(confirm('<fmt:message key="delete_data_confirm" />？')){
						if ($('.grid').find('.table-body').find(':radio:checked').parent().parent().find('td:eq(6)').find('span').text()=="Y") {
							alert('<fmt:message key="Enable_status_can_not_be_deleted" />！');
							return;
						}else{
							var action = '<%=path%>/schedule/deleteSchedule.do?scheduleID='+scheduleID;
							$('body').window({
								title: '<fmt:message key ="delete" /><fmt:message key ="The_shipping_schedule" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '340px',
								height: '255px',
								draggable: true,
								isModal: true
							});
						}
						
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			};
			updateSchedule=function(){
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert('<fmt:message key="Please_choose_a_class" />！');
					return;
				}
				var scheduleID = $('.grid').find('.table-body').find(':checkbox:checked').val();
				$('body').window({
					id: 'window_update_surveyd',
					title: '<fmt:message key="update" /><fmt:message key ="The_shipping_schedule" />',
					content: '<iframe id="updateScheduleFrame" frameborder="0" src="<%=path%>/schedule/update.do?scheduleID='+scheduleID+'"></iframe>',
					width: 400,
					height: 360,
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			};
			
			//分配分店范围
			function toAccountPositn() {
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert('<fmt:message key="Please_choose_a_class" />！');
					return;
				}
				var scheduleID=$('.grid').find('.table-body').find(':checkbox:checked').val();
				path="<%=path%>/schedule/findFirmByScheduleId.do?scheduleId="+scheduleID+"&typn=1203"+"&single=false";
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key ="Distribution_branch_range" />',
					content: '<iframe id="accountPositnFrame" frameborder="0" src="'+path+'"></iframe>',
					width: 800,
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="enter" />',
								title: '<fmt:message key="modify_supplier" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('accountPositnFrame')&&window.document.getElementById("accountPositnFrame").contentWindow.savePositnAccount()){
// 										submitFrameForm('accountPositnFrame','listForm');
										window.document.getElementById("accountPositnFrame").contentWindow.$("#listForm").submit();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//配送班表日历
			function toDetail() {
				if($('.grid').find('.table-body').find(":checked").length==0){
					alert('<fmt:message key="Please_choose_a_class" />！');
					return;
				}
				var scheduleID=$('.grid').find('.table-body').find(':checkbox:checked').val();
				path="<%=path%>/schedule/listCalendar.do?scheduleID="+scheduleID;
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key="Distribution_class_calendar" />',
					content: '<iframe id="accountPositnFrame" frameborder="0" src="'+path+'" name="accountPositnFrame"></iframe>',
					width: 900,
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="back"/>',
								title: '<fmt:message key="back"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							},{
								text:'<fmt:message key="bulk"/><fmt:message key="delete"/>',
								title:'<fmt:message key="bulk"/><fmt:message key="delete"/>',
								icon:{
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler:function(){
									//处理业务
									var accountPositnFrame = $(this).contents().find("#accountPositnFrame");
									var scheduleMonth = accountPositnFrame.find("#divDate").text();
									if(scheduleMonth == null || scheduleMonth == ''){
										var date = new Date();
										scheduleMonth = date.getMonth()<9?date.getFullYear()+"-0"+(date.getMonth()+1):date.getFullYear()+"-"+(date.getMonth()+1);
									}
									var scheduleID = window.frames["accountPositnFrame"].document.getElementById('scheduleID').value;
									var category_Code = window.frames["accountPositnFrame"].document.getElementById('code').value;
									//alert(scheduleMonth+":"+scheduleID+":"+category_Code);
									if(confirm("<fmt:message key ="Determine_all_distribution_classes_that_delete_the_report_class" />？")){
										$.ajax({
											url:"<%=path%>/schedule/batchDeleteScheduleD.do",
									        cache:false,
											type:'post',
											async:false,
											data:'scheduleMonth='+scheduleMonth+"&scheduleID="+scheduleID+"&category_Code="+category_Code,
											dataType:'json',
											ifModified:true,
											timeout: 30000,
											error: function(XMLHttpRequest, textStatus, errorThrown){
												alert('Error loading json document:'+XMLHttpRequest.readyState);
											},
											success: function(msg){
												if(msg.state!=null && msg.state =='ok'){
													alert("<fmt:message key ="successful_deleted" />!");
													window.frames["accountPositnFrame"].refresh();
												}
											}
										});
									}
								}
							}
						]
					}
				});
			}
			
			//查看分店下有那些班表
			function tocheckFirm(){
				path = '<%=path%>/schedule/findScheduleByPositn.do?typn=4';
				customWindow = $('body').window({
					id: 'window_selectPositn',
					title: '<fmt:message key="the_branch_class_table"/>',
					content: '<iframe id="accountPositnFrame" frameborder="0" src="'+path+'" name="accountPositnFrame"></iframe>',
					width: 900,
					height: '500px',
					draggable: true,
					isModal: true
				});
			}
			//-------------------------------------------------------------------------------
		})
		</script>
</body>
</html>