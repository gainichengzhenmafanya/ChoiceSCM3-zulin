<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>新增参考类别</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:100px;">
			<form id="TypothForm" method="post" action="<%=path %>/grpTyp/saveTypothByAdd.do">
				<div class="form-line">
					<div class="form-label"><fmt:message key="coding" />：</div>
					<div class="form-input"><input type="text" id="code" name="code" class="text" onblur="test(this)"  onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name" />：</div>
					<div class="form-input"><input type="text" id="des" name="des" class="text"/></div>
				</div>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:[20],
						error:['<fmt:message key="name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/grpTyp/findTypoth.do",{des:$("#des").val()},function(data){
								if(data != "true") {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['maxLength'],
						param:[10],
						error:['<fmt:message key="coding" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/grpTyp/findTypoth.do",{code:$("#code").val()},function(data){
								if(data != "true") {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />！']
					}]
				});
			});
			function test(e) {
				if(e.value!='' && Number(e.value)==0) {
					alert("<fmt:message key="Encoding_can_not_be_0" />！");
					e.focus();//YUKU-92修改物资类别新增时TAB键无法切换文本框光标
				}
			}
			function  valueCode(){
				return $("#code").val();
			}
		</script>
	</body>
</html>