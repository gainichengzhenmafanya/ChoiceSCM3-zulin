<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查询多条类别</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div class="tool"></div>
		
		<ul id="tt" class="easyui-tree" checkbox="true" style="width:310px;height:400px;overflow:auto;">
			<li>   
		        <span><fmt:message key="all"/></span>   
		        <ul>
		        	<c:forEach var="grptyp" items="${grpTypList }">
		        		<li state="closed">   
			                <span>${grptyp.code},${grptyp.des }</span>
			                <ul> 
			                	<c:forEach var="grp" items="${grpList}">
			                		<c:if test="${grp.grptyp == grptyp.code }">
										<li state="closed">
					                        <span>${grp.code},${grp.des}</span>
					                        <ul>
					                        	<c:forEach var="typ" items="${typList }">
					                        		<c:if test="${typ.grp == grp.code }">
					                        			<li id="${typ.code }">
					                        				${typ.code},${typ.des }
					                        			</li>
					                        		</c:if>
					                        	</c:forEach>
					                        </ul>
					                    </li>
				                    </c:if>
								</c:forEach>
			                </ul>   
			            </li>
		        	</c:forEach>
		        </ul>   
		    </li>     
		</ul>
		<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value=""/>
		<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript">
			//展开折叠状态
			var bz = true;
			$(document).ready(function(){ 
				focus() ;//页面获得焦点
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				}); 
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" /><fmt:message key="category_selection" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Typ();
							}
						},{
							text: '<fmt:message key="expandAll" />/<fmt:message key="collapse" />',
							title: '<fmt:message key="expandAll" />/<fmt:message key="collapse" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								if(bz){
									bz = false;
									$('#tt').tree('expandAll');
								}else{
									bz = true;
									$('#tt').tree('collapseAll');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
				
				//---------------------------
				var defaultCode = "${defaultCode}";
				var resulttyp = '${resulttyp}';
				//返回值类型没有传值则去掉包含括号
				if(!resulttyp || resulttyp==''){
					defaultCode = defaultCode.substring(2,defaultCode.length-2);
				}else{
					defaultCode = defaultCode.substring(1,defaultCode.length-1);
				}
				if(defaultCode!=''){
					var codes = defaultCode.split("','");
					for(var i in codes){
						if(codes[i] != '' && codes[i] != null){
							var n = $("#tt").tree('find',codes[i]);
		                    if(n){
		                        $("#tt").tree('check',n.target);
		                    }
						}
					}	
				}
				
			});
			
			function select_Typ(){
				var node = $('#tt').tree('getChecked');
				var resulttyp = '${resulttyp}';
				var typ = "";
				//返回值类型没有传值则返回值编码包含括号
				if(!resulttyp || resulttyp==''){
					typ = "(";
				}
				var typName = "";
				var first=true;
				if(node.length > 0){
					for (var i = 0; i < node.length; i++) {
						if($(node[i]).attr('id')){
							if (!first) {
								typ = typ+",'"+$(node[i]).attr('id')+"'";
								typName = typName+',' + $.trim(node[i].text.split(',')[1]);
							}else {
								typ = typ+"'"+$(node[i]).attr('id')+"'";
								typName = typName + $.trim(node[i].text.split(',')[1]);
								first=false;
							}
						}
					}
					//返回值类型没有传值则返回值编码包含括号
					if(!resulttyp || resulttyp==''){
						typ = typ+")";
					}
					$('#parentId').val(typ);
					$('#parentName').val(typName);
				}else{
					$('#parentId').val('');
					$('#parentName').val('');
				}
				
			 	top.customWindow.afterCloseHandler('Y');
			    top.closeCustom();
			}
		</script>
	</body>
</html>