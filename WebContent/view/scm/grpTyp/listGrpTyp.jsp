<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
	String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="sector_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div class="leftFrame">
      <div id="toolbar"></div>
	    <div class="treePanel">
	      <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
          var tree = new MzTreeView("tree");
          
          tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("0","0")';
          <c:forEach var="grpTyp" items="${grpTypList}" varStatus="status">
	          	tree.nodes['00000000000000000000000000000000_${grpTyp.code}'] 
	          		= 'text:${grpTyp.code},${grpTyp.des}; method:changeUrl("1","${grpTyp.code}","${grpTyp.des}")';
          </c:forEach>
          
          <c:forEach var="grp" items="${grpList}" varStatus="status">
	          	tree.nodes['${grp.grptyp}_${grp.code}'] 
	          		= 'text:${grp.code},${grp.des}; method:changeUrl("2","${grp.code}","${grp.des}")';
          </c:forEach>

          <c:forEach var="typ" items="${typList}" varStatus="status">
	          	tree.nodes['${typ.grp}_${typ.code}'] 
	          		= 'text:${typ.code},${typ.des}; method:clearContent()';
          </c:forEach>
          
          tree.setIconPath("<%=path%>/image/tree/none/");
          document.write(tree.toString());
        </script>
	    </div>
    </div>
    <div class="mainFrame">
      <iframe src="<%=path%>/grpTyp/table.do?level=${level}&code=${code}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    
    <input type="hidden" id="level" name="level" value="${level-1}"/>
    <input type="hidden" id="code" name="code" value="${code}"/>
    <input type="hidden" id="des" name="des" value="${des}"/>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		function changeUrl(level,code,des){
			$("#mainFrame").show();
			$('#level').val(level);
			$('#code').val(code);
			$('#des').val(des);
	        window.mainFrame.location = "<%=path%>/grpTyp/table.do?level="+(Number(level)+1)+"&code="+code+"&des="+des;
	    }
		function clearContent(){
			$("#mainFrame").hide();
		}
	    function refreshTree(){
	    	var level=$('#level').val();
	    	var code=$('#code').val();
	    	var des=$('#des').val();
	    	
			window.location.href = '<%=path%>/grpTyp/list.do?level='+(Number(level)+1)+'&code='+code+'&des='+des;
	    }
	    
	    function pageReload(){
	    	window.location.href = '<%=path%>/grpTyp/list.do?supplyId='+$("#supplyId").val();
	    }
	    
		$(document).ready(function(){
			tree.focus('${code}',false );
			var toolbar = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="expandAll" />',
						title: '<fmt:message key="expandAll" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					},{
						text: '<fmt:message key="refresh" />',
						title: '<fmt:message key="refresh" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							refreshTree();
						}
					}
				]
			});
			
			setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
		});// end $(document).ready();
		</script>

	</body>
</html>