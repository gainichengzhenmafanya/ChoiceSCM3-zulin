<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>supply Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
	
		<div class="tool">
		</div>
		<input type="hidden" id="code" value="${code }"></input>
		<form id="listForm" action="<%=path%>/grpTyp/table.do?level=${level}&&code=${code}" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:140px;"><fmt:message key="name" /></span></td>
								<c:if test="${level == 2 }">
									<td><span style="width:70px;"><fmt:message key="supplyattr"/></span></td>
									<td><span style="width: 50px;"><fmt:message key="planned_amount"/></span></td>
									<td><span style="width:70px;"><fmt:message key="whether_to_participate_in_the_cost_calculation"/></span></td>
								</c:if>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="result" items="${listResult}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;">
										<input type="checkbox" name="idList" name="code" id="chk_${result.code}" value="${result.code}"/>
										<input type="hidden" name="level" value="${level+1}"/></span>
									</td>
									<td><span title="${result.code}" style="width:70px;">${result.code}&nbsp;</span></td>
									<td><span title="${result.des}" style="width:140px;">${result.des}&nbsp;</span></td>
									<c:if test="${level == 2 }">
										<td><span style="width:70px;">${result.typ }&nbsp;</span></td>
										<td><span style="width:50px;text-align: right;">${result.amt }&nbsp;</span></td>
										<td><span style="width:70px;">${result.cost }&nbsp;</span></td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<%-- 
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	  			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	  		--%>
		</form>
	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" /><fmt:message key="supplies_category" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveGrpTyp();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" /><fmt:message key="supplies_category" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateGrpTyp();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" /><fmt:message key="supplies_category" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteGrpTyp();
							
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}
					]
				});

				setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});
				function saveGrpTyp(){
					$('body').window({
						id: 'window_grpTyp',
						title: '<fmt:message key="insert"/><fmt:message key="supplies_category"/>',
						content: '<iframe id="saveGrpTypFrame" name="saveGrpTypFrame" frameborder="0" src="<%=path%>/grpTyp/add.do?level=${level-1}&code=${code}"></iframe>',
						width: '550px',
						height: '380px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" /><fmt:message key="supplies_category" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
// 										var code = document.getElementById('saveGrpTypFrame').contentWindow.valueCode();
// 										var currentCode = $("#code").val();
										if(getFrame('saveGrpTypFrame')&&getFrame('saveGrpTypFrame').validate._submitValidate()){
// 											if(code!=currentCode){
												submitFrameForm('saveGrpTypFrame','GrpTypForm');
// 											}else{
// 												alert("新增物资编码不能与其上级编码相同!");
// 											}
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
				
				function updateGrpTyp(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
						var aim = checkboxList.filter(':checked').eq(0);
						var code = aim.val();
						var level = $(aim).next().val();
						$('body').window({
							title: '<fmt:message key="update" /><fmt:message key="supplies_category" />',
							content: '<iframe id="updateGrpTypFrame" name="updateGrpTypFrame" frameborder="0" src="<%=path%>/grpTyp/update.do?code='+code+'&level='+level+'"></iframe>',
							width: '550px',
							height: '380px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="update" /><fmt:message key="supplies_category" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updateGrpTypFrame')&&getFrame('updateGrpTypFrame').validate._submitValidate()){
												submitFrameForm('updateGrpTypFrame','GrpTypForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else	if(checkboxList 
							&& checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="please_select_data" />！');
						return ;
					}
					else{
						alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						return ;
					}
					
				}
				
				//判断所选择的物资类别有没有被引用，引用状态下的物资类别是不可以修改和删除的
				function checkGrpTyp(){
					var flag = true;
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					var aim = checkboxList.filter(':checked');
					var code = [];
					var level = $(aim.eq(0)).next().val();
					aim.each(function(){
						code.push($(this).val());
					});
					$.ajax({
						type: "POST",
						url: "<%=path%>/grpTyp/checkGrpTyp.do",
						data: "code="+code.join(",")+"&level="+level,
						async:false,
						dataType: "json",
						success:function(result){
							if(result != null && result != ""){
								alert("<fmt:message key ="supplies_category" />【" + result + "】<fmt:message key ="referenced_can_no_be_deleted" />！");
								flag = false;
							} else {
								flag = true;
							}
						}
					});
					return flag;
				}
				
				//删除物资类别
				function deleteGrpTyp(){
					if(!checkGrpTyp()){
						return;
					}
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm('<fmt:message key="This_operation_will_delete_all_the_material_(categories)_under_this_category_determined_to_be_deleted" />?')){
							var aim = checkboxList.filter(':checked'); 
							var code = [];
							var level = $(aim.eq(0)).next().val() ;
							aim.each(function(){
								code.push($(this).val());
							});
							
							$('body').window({
								title: '<fmt:message key="delete" /><fmt:message key="supplies_category" />',
								content: '<iframe id="updateGrpTypFrame" frameborder="0" src="<%=path%>/grpTyp/delete.do?code='+code.join(",")+'&level='+level+'"></iframe>',
								width: 538,
								height: '215px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
				
			
			function pageReload(){
		    	parent.refreshTree();
		    }
			
			//参考类别的维护 2014.11.19 wjf
			function typothSetting(){
				$('body').window({
					title: '<fmt:message key="Reference_class_settings" />',
					content: '<iframe id="typothFrame" frameborder="0" src="<%=path%>/grpTyp/tableTypoth.do"></iframe>',
					width: '600px',
					height: '450px',
					draggable: true,
					isModal: true
				});
			}
		</script>
	</body>
</html>