<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查询单条类别</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<style type="text/css"> 
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/grpTyp/selectOneGrpTyp.do" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:30px;">&nbsp;</td>
								<td style="width:30px;">
									&nbsp;
								</td>
								<td style="width:50px;"><fmt:message key="coding" /></td>
								<td style="width:180px;"><fmt:message key="name" /></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="typ" varStatus="step" items="${listTyp}">
								<tr>
									<td class="num" style="width:30px;">${step.count}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${typ.code}" value="${typ.code}"/>
									</td>
									<td><span title="${typ.code}" style="width:40px;text-align: left;"><c:out value="${typ.code}" />&nbsp;</span></td>
									<td><span title="${typ.des}" style="width:170px;text-align: left;"><c:out value="${typ.des}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="parentId" name="parentId" class="text" readonly="readonly" value=""/>
			<input type="hidden" id="parentName" name="parentName" class="text" readonly="readonly" value=""/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){ 
				focus() ;//页面获得焦点
				$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				}); 
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" /><fmt:message key="category_selection" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								select_Typ();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}]
				});
				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//------------------------------
				//单击每行选中前面的checkbox
				$('.grid').find('.table-body').find('tr').live("click", function () {
					if($(this).find(':checkbox')[0].checked){
						$(":checkbox").attr("checked", false);
					}else{
						$(":checkbox").attr("checked", false);
						$(this).find(':checkbox').attr("checked", true);
					}
				 });
				//禁用checkbox本身的事件
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					event.stopPropagation();
					if(this.checked){
						$(this).attr("checked",false);	
					}else{
						$(this).attr("checked",true);
					}
					$(this).closest('tr').click();
				});
				//---------------------------
				
				var defaultCode = '${defaultCode}';
				var defaultName = '${defaultName}';
				if(defaultCode!=''){
					$('.grid').find('.table-body').find(':checkbox').each(function(){
						if(this.id.substr(4,this.id.length)==defaultCode){
							$(this).attr("checked", true);
							$('#parentId').val(defaultCode);
							$('#parentName').val(defaultName);
						}
					})	
				}
			});
			function select_Typ(){
				if($('.grid').find('.table-body').find(':checkbox:checked').size()>0){
					$('.grid').find('.table-body').find(':checkbox:checked').each(function(){
						$('#parentId').val($(this).closest('tr').find('td:eq(2)').find('span').attr('title'));
						$('#parentName').val($(this).closest('tr').find('td:eq(3)').find('span').attr('title'));
					})
				}else{
					$('#parentId').val('');
					$('#parentName').val('');
				}
				top.customWindow.afterCloseHandler('Y');
				top.closeCustom();
			}
		</script>
	</body>
</html>