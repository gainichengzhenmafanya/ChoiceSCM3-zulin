<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>参考类别设置</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/grpTyp/tableTypoth.do" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span class="num" style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td>
								<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:140px;"><fmt:message key="name" /></span></td>
								<td><span style="width:70px;"><fmt:message key ="if_inuse" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="result" items="${typothList}" varStatus="status">
								<tr>
									<td><span class="num" style="width: 25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;">
										<input type="checkbox" name="idList" name="code" id="chk_${result.code}" value="${result.code}"/>
										</span>
									</td>
									<td><span title="${result.code}" style="width:70px;">${result.code}&nbsp;</span></td>
									<td><span title="${result.des}" style="width:140px;">${result.des}&nbsp;</span></td>
									<td><span style="width:70px;">${result.locked }&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit" />').click();
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				var tool = $('.tool').toolbar({
					items: [{
// 							text: '<fmt:message key="select" />',
// 							title: '<fmt:message key="select" />',
// 							useable: true,
// 							icon: {
<%-- 								url: '<%=path%>/image/Button/op_owner.gif', --%>
// 								position: ['0px','0px']
// 							},
// 							handler: function(){
// 								$('#listForm').submit();
// 							}
// 						},{
							text: '<fmt:message key="insert" />',
							title: '<fmt:message key="insert" />',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								saveTypoth();
							}
						},{
							text: '<fmt:message key="update" />',
							title: '<fmt:message key="update" />',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								updateTypoth();
							}
						},{
							text: '<fmt:message key="delete" />',
							title: '<fmt:message key="delete" />',
							useable: true,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteTypoth();
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.document).find('.close'));
							}
						}
					]
				});

				setElementHeight('.grid',['.tool'],600,25);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
			});
			//新增方法
			function saveTypoth(){
				$('body').window({
					id: 'window_typoth',
					title: '<fmt:message key="insert"/>',
					content: '<iframe id="saveTypothFrame" frameborder="0" src="<%=path%>/grpTyp/addTypoth.do"></iframe>',
					width: '550px',
					height: '380px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('saveTypothFrame')&&getFrame('saveTypothFrame').validate._submitValidate()){
											submitFrameForm('saveTypothFrame','TypothForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
				//修改方法
				function updateTypoth(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList 
							&& checkboxList.filter(':checked').size() ==1){
						var aim = checkboxList.filter(':checked').eq(0);
						var locked = aim.parents('tr').find('td:eq(4) span').text();
						if(locked == 'Y'){//如果被适用了 不能修改
							alert('<fmt:message key="This_class_has_been_used_and_can_not_be_modified" />！');
							return;
						}
						var code = aim.val();
						$('body').window({
							title: '<fmt:message key="update" />',
							content: '<iframe id="updateTypothFrame" frameborder="0" src="<%=path%>/grpTyp/updateTypoth.do?code='+code+'"></iframe>',
							width: '550px',
							height: '380px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										title: '<fmt:message key="update" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('updateTypothFrame')&&getFrame('updateTypothFrame').validate._submitValidate()){
												submitFrameForm('updateTypothFrame','TypothForm');
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										title: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											position: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else	if(checkboxList 
							&& checkboxList.filter(':checked').size() > 1){
						alert('<fmt:message key="please_select_data" />！');
						return ;
					}
					else{
						alert('<fmt:message key="please_select_information_you_need_to_modify" />！');
						return ;
					}
					
				}
				//删除方法
				function deleteTypoth(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					
					if(checkboxList 
							&& checkboxList.filter(':checked').size() > 0){
						if(confirm("<fmt:message key="Are_you_sure_you_want_to_delete_it" />?")){
							var aim = checkboxList.filter(':checked'); 
							var code = [];
							var flag = true;
							aim.each(function(){
								var locked = $(this).parents('tr').find('td:eq(4) span').text();
								if(locked == 'Y'){//如果被适用了 不能修改
									flag = false;
									return;
								}
								code.push($(this).val());
							});
							if(!flag){
								alert('<fmt:message key ="refencenotdel" />！');
								return;
							}
							$('body').window({
								title: '<fmt:message key="delete" />',
								content: '<iframe id="deleteTypothFrame" frameborder="0" src="<%=path%>/grpTyp/deleteTypoth.do?code='+code.join(",")+'"></iframe>',
								width: 538,
								height: '215px',
								draggable: true,
								isModal: true
							});
						}
					}else{
						alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
						return ;
					}
				}
		</script>
	</body>
</html>