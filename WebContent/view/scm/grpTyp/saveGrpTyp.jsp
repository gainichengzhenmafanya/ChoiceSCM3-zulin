<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Classes Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:50px;">
			<form id="GrpTypForm" method="post" action="<%=path %>/grpTyp/saveByAdd.do">
			<input type="hidden" id="level" name="level" value="${level}"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="coding" />：</div>
					<div class="form-input"><input type="text" id="code" name="code" class="text" onblur="test(this)"  onkeyup="value=value.replace(/[^\d]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name" />：</div>
					<div class="form-input"><input type="text" id="des" name="des" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="parent_category" />：</div>
					<div class="form-input">
<%-- 						<input type="text" id="parent_des" name="parent_des" class="selectDepartment text" value="<c:out value="${des }"/>"/> --%>
						<c:if test="${des ==null }">
							<input type="text" id="parent_des" name="parent_des" disabled="disabled" value='<fmt:message key="supplies_category"/>'/>
						</c:if>
						<c:if test="${des !=null }">
							<input type="text" id="parent_des" name="parent_des" disabled="disabled" value="${des}"/>
						</c:if>
						<c:if test="${level == 1 }">
							<input type="hidden" id="vcode" name="grptyp" value="${code}"/>
						</c:if>
						<c:if test="${level == 2 }">
							<input type="hidden" id="vcode" name="grp" value="${code}"/>
						</c:if>
					</div>
				</div>
				<!-- add wang jie  2014年10月28日 10:52:21 -->
				<c:choose>
					<c:when test="${level == 1 }">
						<div class="form-line">
						<div class="form-label"><fmt:message key="planned_amount"/>：</div>
							<div class="form-input">
								<input type="text" name="amt" id="amt" value="" class="text"/>
							</div>
						</div>
						<div class="form-line">
						<div class="form-label"><fmt:message key="whether_to_participate_in_the_cost_calculation"/>：</div>
							<div class="form-input">
								<input type="radio" name="cost" value="Y"/><fmt:message key ="be" />&nbsp;
								<input type="radio" name="cost" value="N" checked="checked"/><fmt:message key ="no1" />
							</div>
						</div>
						<div class="form-line">
						<div class="form-label"><fmt:message key="supplyattr"/>：</div>
							<div class="form-input">
								<select id="typ" name="typ">
									<option value="" selected="selected">--<fmt:message key ="please_select" />--</option>
									<option value="A,原料"><fmt:message key="Raw_materials" /></option>
									<option value="B,调料"><fmt:message key="Seasoning" /></option>
									<option value="C,商品"><fmt:message key ="commodity" /></option>
									<option value="D,低值"><fmt:message key="Low_value" /></option>
									<option value="E,物料"><fmt:message key ="material" /></option>
									<option value="F,水"><fmt:message key ="misboh_baserecord_water" /></option>
									<option value="G,电"><fmt:message key ="misboh_baserecord_electric" /></option>
									<option value="H,气"><fmt:message key ="misboh_baserecord_gas" /></option>
									<option value="I,办公费"><fmt:message key="Office_expenses" /></option>
									<option value="J,维修费"><fmt:message key="Maintenance_cost" /></option>
									<option value="K,特殊"><fmt:message key="Special" /></option>
								</select>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<input type="hidden" value="0" name="typ" id="typ"/>
						<input type="hidden" value="0" name="cost" id="cost"/>
					</c:otherwise>
				</c:choose>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				/*弹出树*/
				$('#parent_des').bind('focus.custom',function(e){
					if(!top.customWindow){
						var offset = getOffset('parent_des');
						top.custom('<fmt:message key="please_select_parent_category" />','<%=path%>/grpTyp/selectGrptyp.do?level=${level+1}',offset,$(this),$('#parent_des').next());
					}
				});
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:[50],
						error:['<fmt:message key="name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['handler'],
						handler:function(){
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/grpTyp/findGrpTypByDes.do",{level:$("#level").val(),des:$("#des").val()},function(data){
								if($.trim(data)) {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="Name_already_exists" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							
							var result = true;
							if($('#code').val()==$('#vcode').val()){
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="New_supplies_encoding_can_not_be_the_same_with_encoding" />！']
					},{
						type:'text',
						validateObj:'code',
						validateType:['handler'],
						handler:function(){
							
							var result = true;
							$.ajaxSetup({async:false});
							$.post("<%=path %>/grpTyp/findGrpTypByCode.do",{level:$("#level").val(),code:$("#code").val()},function(data){
								if($.trim(data)) {
									result = false;
								}
							});
							return result;
						},
						param:['F'],
						error:['<fmt:message key="coding" /><fmt:message key="already_exists" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！']
					}]
				});
				<c:if test="${level == 0 }">
					validate.validateItem.push({
						type:'text',
						validateObj:'code',
						validateType:['maxLength'],
						param:[2],
						error:['<fmt:message key="coding" /><fmt:message key="maximum_length" />2！']
					});
				</c:if>
				<c:if test="${level == 1 }">
				validate.validateItem.push({
						type:'text',
						validateObj:'code',
						validateType:['maxLength'],
						param:[4],
						error:['<fmt:message key="coding" /><fmt:message key="maximum_length" />4！']
					});
				validate.validateItem.push({
					type:'text',
					validateObj:'code',
					validateType:['minLength'],
					param:[3],
					error:['<fmt:message key="coding" /><fmt:message key="length_at_least" />3！']
				});
				validate.validateItem.push({
					type:'text',
					validateObj:'amt',
					validateType:['handler'],
					handler:function(){
						if(isNaN($("#amt").val())){
							return false;
						}else{
							return true;
						}
					},
					param:['F'],
					error:['<fmt:message key ="only_digits" />！']
				});
				</c:if>
				<c:if test="${level == 2 }">
				validate.validateItem.push({
						type:'text',
						validateObj:'code',
						validateType:['maxLength'],
						param:[9],
						error:['<fmt:message key="coding" /><fmt:message key="maximum_length" />9！']
					});
				validate.validateItem.push({
					type:'text',
					validateObj:'code',
					validateType:['minLength'],
					param:[3],
					error:['<fmt:message key="coding" /><fmt:message key="length_at_least" />5！']
				});
				</c:if>
			});
			function test(e) {
				if(e.value!='' && Number(e.value)==0) {
					alert("<fmt:message key="Encoding_can_not_be_0" />！");
					e.focus();//YUKU-92修改物资类别新增时TAB键无法切换文本框光标
				}
			}
			function  valueCode(){
				return $("#code").val();
			}
		</script>
	</body>
</html>