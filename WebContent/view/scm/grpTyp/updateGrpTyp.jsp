<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Classes Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="form">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:50px;">
				<input type="hidden" id="str" name="str" value="${str}" />
			<form id="GrpTypForm" method="post" action="<%=path %>/grpTyp/saveByUpdate.do">
				<input type="hidden" id="acct" name="acct" value="${acct }"/>
				<input type="hidden" id="level" name="level" value="${level }"/>
				<input type="hidden" id="oldCode" name = "oldCode" value="<c:out value="${grpTyp.code}"/>"/>
				<input type="hidden" id="code1" name = "code" value="<c:out value="${grpTyp.code}"/>"/>
				<div class="form-line">
					<div class="form-label"><fmt:message key="coding" />：</div>
					<div class="form-input"><input disabled="disabled" type="text" id="code" value="<c:out value="${grpTyp.code}"/>" class="text"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="name" />：</div>
					<div class="form-input"><input type="text" id="des" name="des" value="<c:out value="${grpTyp.des}"/>" class="text"/></div>
				</div>
				<c:choose>
					<c:when test="${level == 3 }">
						<div class="form-line">
						<div class="form-label"><fmt:message key="planned_amount"/>：</div>
							<div class="form-input">
								<input type="text" name="amt" id="amt" value="${grpTyp.amt }" class="text"/>
							</div>
						</div>
						<div class="form-line">
						<div class="form-label"><fmt:message key="whether_to_participate_in_the_cost_calculation"/>：</div>
							<div class="form-input">
								<input type="radio" name="cost" value="Y" <c:if test="${grpTyp.cost == 'Y' }">checked="checked"</c:if>/><fmt:message key ="be" />&nbsp;
								<input type="radio" name="cost" value="N" <c:if test="${grpTyp.cost == 'N' }">checked="checked"</c:if>/><fmt:message key ="no1" />
							</div>
						</div>
						<div class="form-line">
						<div class="form-label"><fmt:message key="supplyattr"/>：</div>
							<div class="form-input">
								<select id="typ" name="typ">
									<option value="" <c:if test="${grpTyp.typ == '' or grpTyp.typ == null}">selected="selected"</c:if>>--<fmt:message key ="please_select" />--</option> 
									<option value="A,原料" <c:if test="${grpTyp.typ == '原料' }">selected="selected"</c:if>><fmt:message key="Raw_materials" /></option>
									<option value="B,调料" <c:if test="${grpTyp.typ == '调料' }">selected="selected"</c:if>><fmt:message key="Seasoning" /></option>
									<option value="C,商品" <c:if test="${grpTyp.typ == '商品' }">selected="selected"</c:if>><fmt:message key ="commodity" /></option>
									<option value="D,低值" <c:if test="${grpTyp.typ == '低值' }">selected="selected"</c:if>><fmt:message key="Low_value" /></option>
									<option value="E,物料" <c:if test="${grpTyp.typ == '物料' }">selected="selected"</c:if>><fmt:message key ="material" /></option>
									<option value="F,水" <c:if test="${grpTyp.typ == '水' }">selected="selected"</c:if>><fmt:message key ="misboh_baserecord_water" /></option>
									<option value="G,电" <c:if test="${grpTyp.typ == '电' }">selected="selected"</c:if>><fmt:message key ="misboh_baserecord_electric" /></option>
									<option value="H,气" <c:if test="${grpTyp.typ == '气' }">selected="selected"</c:if>><fmt:message key ="misboh_baserecord_gas" /></option>
									<option value="I,办公费" <c:if test="${grpTyp.typ == '办公费' }">selected="selected"</c:if>><fmt:message key="Office_expenses" /></option>
									<option value="J,维修费" <c:if test="${grpTyp.typ == '维修费' }">selected="selected"</c:if>><fmt:message key="Maintenance_cost" /></option>
									<option value="K,特殊" <c:if test="${grpTyp.typ == '特殊' }">selected="selected"</c:if>><fmt:message key="Special" /></option>
								</select>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<input type="hidden" value="0" name="typ" id="typ"/>
						<input type="hidden" value="0" name="cost" id="cost"/>
					</c:otherwise>
				</c:choose>
			</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				
				if("desError"==$("#str").val()){
					alert("<fmt:message key='category'/><fmt:message key='name'/><fmt:message key='already_exists' />！！");
					var t = $("#des").val();
					$("#des").val("").focus().val(t);
				}
				
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'des',
						validateType:['maxLength'],
						param:[10],
						error:['<fmt:message key="name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'des',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="name" /><fmt:message key="cannot_be_empty" />！']
					}]
				});
			});
			<c:if test="${level == 1 }">
			validate.validateItem.push({
					type:'text',
					validateObj:'code',
					validateType:['maxLength'],
					param:[4],
					error:['<fmt:message key="coding" /><fmt:message key="maximum_length" />4！']
				});
			validate.validateItem.push({
				type:'text',
				validateObj:'code',
				validateType:['minLength'],
				param:[3],
				error:['<fmt:message key="coding" /><fmt:message key="length_at_least" />3！']
			});
			validate.validateItem.push({
				type:'text',
				validateObj:'amt',
				validateType:['handler'],
				handler:function(){
					if(isNaN($("#amt").val())){
						return false;
					}else{
						return true;
					}
				},
				param:['F'],
				error:['<fmt:message key ="only_digits" />！']
			});
			</c:if>
		</script>
	</body>
</html>