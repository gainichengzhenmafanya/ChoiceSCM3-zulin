<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="category_information" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		</head>
	<body>
	  <div class="treePanel">
	    <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	     <script type="text/javascript">
	       var tree = new MzTreeView("tree");
	       
	       tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;dbmethod:changeUrl("0","0","0")';
	          <c:forEach var="grpTyp" items="${grpTypList}" varStatus="status">
		          	tree.nodes['00000000000000000000000000000000_${grpTyp.code}'] 
		          		= 'text:${grpTyp.des}; dbmethod:changeUrl("1","${grpTyp.code}","${grpTyp.des}")';
	          </c:forEach>
	          
	          <c:forEach var="grp" items="${grpList}" varStatus="status">
		          	tree.nodes['${grp.grptyp}_${grp.code}'] 
		          		= 'text:${grp.des}; dbmethod:changeUrl("2","${grp.code}","${grp.des}")';
	          </c:forEach>

	          <c:forEach var="typ" items="${typList}" varStatus="status">
		          	tree.nodes['${typ.grp}_${typ.code}'] 
		          		= 'text:${typ.des}; dbmethod:changeUrl("3","${typ.code}","${typ.des}")';
	          </c:forEach>
	          
	          tree.setIconPath("<%=path%>/js/tree/department/");
	          document.write(tree.toString());
	     </script>
	  </div>
    <input type="hidden" id="parentId" name="parentId" />
    <input type="hidden" id="parentName" name="parentName" />
    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript">
			function changeUrl(level,sp_type,typdes)
	    {
		  if(level!= Number("${level}")){
			  alert("<fmt:message key="please_select_the" />${level}<fmt:message key="level_nodes" />！");
			  return ;
		  }
	      $('#parentId').val(sp_type);
	      $('#parentName').val(typdes);
	      top.customWindow.afterCloseHandler('Y');
	      top.closeCustom();
	    }
			
			$(document).ready(function(){
				$('.treePanel').height($(document.body).height());
			});// end $(document).ready();
		</script>

	</body>
</html>