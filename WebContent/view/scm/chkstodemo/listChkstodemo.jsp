<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.page{margin-bottom: 25px;}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/chkstodemo/findChkByPage.do" method="post">
			<input type="hidden" name="title" id="title" value="${title}"/>		
			<div class="form-line">
				<div class="form-label"><fmt:message key ="supplies_code" />：</div>
				<div class="form-input">
					<input type="text" style="width:80px;" class="text" name="supply.sp_code" id="sp_code" value="${chkstodemo.supply.sp_code }"/>
				</div>
				<div class="form-label"><fmt:message key ="supplies_name" />：</div>
				<div class="form-input">
					<input type="text" style="width:80px;" class="text" name="supply.sp_name" id="sp_name" value="${chkstodemo.supply.sp_name }"/>
				</div>
				<div class="form-label"><fmt:message key ="supplies_abbreviations" />：</div>
				<div class="form-input">
					<input type="text" style="width:80px;" class="text" name="supply.sp_init" id="sp_init" style="text-transform:uppercase;" value="${chkstodemo.supply.sp_init }"/>
				</div>
			</div>
			<div class="grid" id="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;">&nbsp;</span></td>
								<td><span style="width:20px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:120px;"><fmt:message key="title" /></span></td>
								<td><span style="width:50px;"><fmt:message key="abbreviation_code" /></span></td>
								<td><span style="width:100px;"><fmt:message key="supplies_name" /></span></td>
								<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
								<td><span style="width:50px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity" /></span></td>
								<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:50px;"><fmt:message key="reference_number" /></span></td>
								<td><span style="width:50px;"><fmt:message key="reference_unit" /></span></td>
								<td><span style="width:50px;"><fmt:message key="remark" /></span></td>
								<td><span style="width:50px;"><fmt:message key="number" /></span></td>
	                           				</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td title="${chkstodemo.rec}" ><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstodemo.rec}" value="${chkstodemo.rec}"/></span></td>
									<td title="${chkstodemo.title}" ><span style="width:120px;text-align: left;">${chkstodemo.title}</span></td>
									<td title="${chkstodemo.supply.sp_init}" ><span style="width:50px;text-align: left;">${chkstodemo.supply.sp_init}</span></td>
									<td title="${chkstodemo.supply.sp_name}" ><span style="width:100px;text-align: left;">${chkstodemo.supply.sp_name}</span></td>
									<td title="${chkstodemo.supply.sp_code}" ><span style="width:80px;text-align: left;">${chkstodemo.supply.sp_code}</span></td>
									<td title="${chkstodemo.supply.sp_desc}" ><span style="width:50px;text-align: left;">${chkstodemo.supply.sp_desc}</span></td>
									<td title="${chkstodemo.cnt}" ><span style="width:50px;text-align: center;">${chkstodemo.cnt}</span></td>
									<td title="${chkstodemo.supply.unit}" ><span style="width:50px;text-align: center;">${chkstodemo.supply.unit}</span></td>
									<td title="${chkstodemo.cnt1}" ><span style="width:50px;text-align: center;">${chkstodemo.cnt1}</span></td>
									<td title="${chkstodemo.supply.unit}" ><span style="width:50px;text-align: center;">${chkstodemo.supply.unit}</span></td>
									<td title="${chkstodemo.memo}" ><span style="width:50px;text-align: center;">${chkstodemo.memo}</span></td>
									<td title="${chkstodemo.rec}" ><span style="width:50px;text-align: center;">${chkstodemo.rec}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>			
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		var t;
		function ajaxSearch(key){
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒  
		}		
		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit" />').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 83: $('#autoId-button-101').click(); break;
	                case 68: $('#autoId-button-102').click(); break;
	            }

		 	});
		   //光标移动
		   new tabTableInput("tblGrid","text"); 
		   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			}); 	
		   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('#sp_code').bind('focus.custom',function(e){
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),handler);	
				}
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'title2',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="title" /><fmt:message key="cannot_be_empty" />！']
				},{
					type:'text',
					validateObj:'title2',
					validateType:['maxLength'],
					param:[20],
					error:['<fmt:message key="title" /><fmt:message key="length_too_long" />！']
				},{
					type:'text',
					validateObj:'sp_code',
					validateType:['canNull','maxLength'],
					param:['F','10'],
					error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
				},{
					type:'text',
					validateObj:'cnt',
					validateType:['num'],
					param:['F'],
					error:['<fmt:message key="Invalid_quantity" />！']
				},{
					type:'text',
					validateObj:'cnt1',
					validateType:['num'],
					param:['F'],
					error:['<fmt:message key="Invalid_quantity2" />！']
				}]
			});						
			$('.tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="select"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$("#listForm").submit();
						}
					},{
						text: '<fmt:message key="insert" />',
						title: '<fmt:message key="insert"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							addChkstodemo();
						}
// 					},{
// 						text: '<fmt:message key="save" />(<u>S</u>)',
// 						title: '<fmt:message key="save" />',
// 						icon: {
// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-80px','-0px']
// 						},
// 						handler: function(){
// 							if(validate._submitValidate()){
// 								$('body').window({
// 									title: '<fmt:message key="successful_added"/>！',
// 									content: '<iframe name="add" frameborder="0" src=""></iframe>',
// 									width: 500,
// 									height: '245px',
// 									draggable: true,
// 									isModal: true
// 								});
							
// 								window.document.getElementById("addchkstodemoForm").submit();//saveByAdd();
// 							}
							
// 						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deletechkstodemo();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
						}
					}]
			});
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),75);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
		});				
		//弹出物资树回调函数
		function handler(sp_code){
			$('#sp_code').val(sp_code); 
			if(sp_code==undefined || ''==sp_code){
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					 $('#sp_init').val(supply.sp_init);
				}
			});
		}
		//删除申购信息
		function deletechkstodemo(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="delete_data_confirm" />？')){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					var action = '<%=path%>/chkstodemo/delete.do?rec='+chkValue.join(",");
					$('body').window({
						title: '<fmt:message key ="successful_deleted" />！',
						content: '<iframe  frameborder="0" src='+action+'></iframe>',
						width: 500,
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}else{
				alert('<fmt:message key="please_select_information_you_need_to_delete" />！');
				return ;
			}
		}
		//左侧列表点击事件
		function textSubmit(title){
			$('#listForm').attr('action','<%=path%>/chkstodemo/findChkstodemo.do');
			$('#title').val(title);
			$('#listForm').submit();
		}
		function pageReload(){
			parent.pageReload();
		}
		function addChkstodemo(){
			$('body').window({
				id: 'window_addChkstodemo',
				title: '<fmt:message key ="insert" /> <fmt:message key ="purchase_template" />',
				content: '<iframe id="addChkstodemoFrame" frameborder="0" src="'+"<%=path%>/chkstodemo/addNewChkstodemo.do?action=init&title="+$('#title').val()+'"></iframe>',
				width: '550px',
				height: '400px',
				confirmClose: false,
				draggable: true,
				isModal: true
			});
		}
		</script>
	</body>
</html>