<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="purchase_the_template" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<style type="text/css">
		.blueBGColor{background-color:	#F1F1F1;}
		.redBGColor{background-color:	#F1F1F1;}
		.onEdit{
			background:lightblue;
			border:1px solid;
			border-bottom-color: blue;
			border-top-color: blue;
			border-left-color: blue;
			border-right-color: blue;
		}
		.input{
			background:transparent;
			border:1px solid;
		}
		</style>		
</head>
<body>
	<div class="tool"></div>
	<input type="hidden" id="subSta" name="subSta"/>
	<form id="titleForm" action="<%=path%>/chkstodemo/findChkstodemoByAddChkstodemo.do" method="post">
		<div class="form-line">	
			<div class="form-label"><fmt:message key="title" /></div>
			<div class="form-input">
				<input type="hidden" id="selectTitle" name="selectTitle" value="${title }"/>
				<select class="select" id="title" name="title" onchange="findByTitle(this);">
					<option value=""><fmt:message key="all" /></option>				
					<c:forEach var="chkstodemo" items="${titleList}" varStatus="status">
						<option
							<c:if test="${chkstodemo.title==title }"> selected="selected" </c:if> value="${chkstodemo.title}">${chkstodemo.title}
						</option>
					</c:forEach>				
				</select>
			</div>
		</div>
	</form>
	<form id="listForm" action="<%=path%>/chkstodemo/findChkByAddChkstodemoPage.do" method="post">
		<div class="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num" style="width: 25px;"></td>
<!-- 							<td style="width:30px;"><input type="checkbox" id="chkAll"/></td> -->
							<td style="width:0px;"><input type="hidden" id="chkAll"/></td>
							<td style="width:100px;"><fmt:message key="title" /></td>
							<td style="width:80px;"><fmt:message key="supplies_code" /></td>
							<td style="width:120px;"><fmt:message key="supplies_name" /></td>
							<td style="width:100px;"><fmt:message key="abbreviation_code" /></td>
							<td style="width:80px;"><fmt:message key="specification" /></td>
							<td style="width:80px;"><fmt:message key ="standard_unit" /></td>
							<td style="width:80px;"><fmt:message key ="scm_standard_quantity" /></td>
							<td style="width:80px;"><fmt:message key ="reference_unit" /></td>
							<td style="width:80px;"><fmt:message key ="scm_the_reference_number" /></td>
							<td style="width:120px;"><fmt:message key="remark" /></td>
<%-- 							<td style="width:80px;"><fmt:message key="order" /></td> --%>
						</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table id="tblGrid" cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstodemo" items="${chkstodemoList}" varStatus="status">
							<tr>
								<td class="num" style="width: 25px;">${status.index+1}</td>
<%-- 								<td style="width:30px; text-align: center;"><input type="checkbox" name="idList" id="chk_${chkstodemo.rec}" value="${chkstodemo.rec}"/></td> --%>
								<td style="width:0px; text-align: center;"><input type="hidden" name="idList" id="chk_${chkstodemo.rec}" value="${chkstodemo.rec}"/></td>
								<td style="width:100px;">${chkstodemo.title}</td>
								<td style="width:80px;">${chkstodemo.supply.sp_code}</td>
								<td style="width:120px;">${chkstodemo.supply.sp_name}</td>
								<td style="width:100px;">${chkstodemo.supply.sp_init}</td>
								<td style="width:80px;">${chkstodemo.supply.sp_desc}</td>
								<td style="width:80px;">${chkstodemo.supply.unit}</td>
<%-- 								<td style="width:80px;color: red">${chkstodemo.cnt}</td> --%>
								<td class="textInput">
									<span title="" style="width:80px;padding: 0px;">
										<input type="text" value="${chkstodemo.cnt}" onfocus="this.select()" onblur="validate(this);"/>									
									</span>
								</td>
								<td style="width:80px;">${chkstodemo.supply.unit1}</td>
								<td style="width:80px;">${chkstodemo.cnt1}</td>
								<td style="width:120px;">${chkstodemo.memo}</td>
<%-- 								<td style="width:80px;">${chkstodemo.rec}</td> --%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>			
		<page:page form="listForm" page="${pageobj}"></page:page>
		<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
		<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
	</form>				
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript">
	var validate;
	//工具栏
	$(document).ready(function(){
		//按钮快捷键
		focus() ;//页面获得焦点
		//屏蔽鼠标右键
		//$(document).bind('contextmenu',function(){return false;});
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
		}); 
	 	//解决谷歌浏览器单击不选中的问题
		$('.textInput').find('input').live('click',function(event){
			var self = this;
			setTimeout(function(){
				self.select();
			},10);
		});
		$('.textInput').live('keydown',function(e){
	 		if(e.keyCode==13){
	 			var lie = $(this).prevAll().length;
				var hang= $(this).parent().prevAll().length + 1;
				$('#tblGrid').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
	 		}
		});
	 	new tabTableInput("tblGrid","text"); //input  上下左右移动
	 	$('#tblGrid').find('tr:eq(0)').find('input').focus();
	 	
	   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	   $('.grid').find('.table-body').find('tr').hover(
			function(){
				$(this).addClass('tr-over');
			},
			function(){
				$(this).removeClass('tr-over');
			}
		);
		
		//自动实现滚动条
		setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法	
		/* //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
		$('.grid').find('.table-body').find('tr').live("click", function () {
		     if ($(this).hasClass("bgBlue")) {
		         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
		     }
		     else
		     {
		         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
		     }
		 }); */
	});
	//双击修改数量
	$('').find('tr').find('td:eq(8)').bind('dblclick',function(){
		if(($(this).children().attr('id')=='editText'))return;
		$(this).html('<input type="text" class="input" id="editText" style="width:'+$(this).width()+'px;" value="'+$(this).text()+'" onblur="onBlurMethod(this)"/>');
		$(this).children().focus().select();
		$(this).find(':input').blur(function(){
			if(isNaN($(this).val())){
				alert('<fmt:message key="Invalid_quantity" />！');
				/* showMessage({
					type: 'error',
					msg: '<fmt:message key="Invalid_quantity" />！',
					speed: 1000
				}); */
				$(this).focus().select();
				$('#subSta').val('error');
				return;
			}else{
				var inputs=$(this).val();
				var id=$(this).parent().closest('tr').find('td:eq(0)').text()-1;
				var sp_code=$(this).parent().closest('tr').find('td:eq(3)').text();
				$.get("<%=path %>/supply/findById.do",
						{sp_code:sp_code},
						function(data){
							$('.table-body').find('tr:eq('+id+')').find('td:eq(10)').html(inputs*(data.unitper));
						});
				$(this).parent().html($(this).val());
			}
		});
	});
	//动态下拉列表框
	function findByTitle(e){
		var action="<%=path%>/chkstodemo/findChkstodemoByAddChkstodemo.do";
		$('#titleForm').attr('action',action);
		$('#titleForm').submit();
	}
	//确认修改
	function enterUpdate() {
		if(null!=$('#subSta').val() && ''!=$('#subSta').val()){
			alert('<fmt:message key="Invalid_quantity" />！');
			/* showMessage({
				type: 'error',
				msg: '<fmt:message key="Invalid_quantity" />！',
				speed: 1000
			}); */
			return;
		}else{
// 			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var checkboxList = $('.grid').find('.table-body').find('tr');
			var chkValue = [];
			var cntValue = [];
			var cnt1Value = [];
			var data = {};
			var msg=0;
			checkboxList.each(function(){
				if($(this).find('td:eq(8)').find('input').val()=='0.0'){
// 					alert("请输入数量");
					msg=1;
				}
			});
			if (msg==1) {
				alert("<fmt:message key='_the_number'/>");
			}else {
				checkboxList.each(function(){
					chkValue.push($(this).find('input').val());
					cntValue.push($(this).find('td:eq(8)').find('input').val());
					cnt1Value.push($(this).find('td:eq(10)').text());
				});
				data['title'] = $('#title').val();
				data['rec2'] = chkValue.join(",");
				data['cnt'] = cntValue.join(",");
				data['cnt1'] = cnt1Value.join(",");
				$.post("<%=path%>/chkstom/enterUpdate.do",data,function(data){
					parent.appendChkstodemo(data);
				});
			}
		}
	}
	
	// 鼠标离开时检查数量输入格式是否合法
	function validate(e){
		if(Number(e.value)<0){
			alert('<fmt:message key="number_cannot_be_negative"/>');
			e.value=e.defaultValue;
			e.focus();
			return;
		}else if(isNaN(e.value)){
			alert('<fmt:message key="number_be_not_valid_number"/>！');
			e.value=e.defaultValue;
			e.focus();
			return;
		};
		var inputs=$(e).val();
		var id=$(e).parent().closest('tr').find('td:eq(0)').text()-1;
		var sp_code=$(e).parent().closest('tr').find('td:eq(3)').text();
		$.get("<%=path %>/supply/findById.do",
				{sp_code:sp_code},
				function(data){
					$('.table-body').find('tr:eq('+id+')').find('td:eq(10)').html(inputs*(data.unitper));
		});
    }
	</script>
</body>
</html>