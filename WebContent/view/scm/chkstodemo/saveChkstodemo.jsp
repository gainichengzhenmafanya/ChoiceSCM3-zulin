<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加申购单模板</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	
</head>
<body>
	<div class="tool"></div>
	<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:60px;">
	<form id="addchkstodemoForm" name="addchkstodemoForm" method="post" action="<%=path%>/chkstodemo/addNewChkstodemo.do">
	<input type="hidden" name="title"  value="${title}"/>
		<div class="form-line">
			<div class="form-label" ><font style="color: red">*</font><fmt:message key="title" />：</div>
			<div class="form-input">
				<input type="text" name="title2" id="title2" value="${chkstodemo.title}" onfocus="this.select()"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><font style="color: red">*</font><fmt:message key="supplies_code" />：</div>
			<div class="form-input">
				<input type="text" name="sp_code" id="sp_code" onfocus="this.select()" readonly="readonly"/>
				<img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies" />' />
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="supplies_name" />：</div>
			<div class="form-input">
				<input type="text" name="sp_name" id="sp_name" onfocus="this.select()" readonly="readonly"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" ><fmt:message key="abbreviation_code" />：</div>
			<div class="form-input">
				<input type="text" name="sp_init" id="sp_init" onfocus="this.select()" readonly="readonly"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label" ><font style="color: red">*</font><fmt:message key ="number_of_standards" />：</div>
			<div class="form-input">
				<input type="text" name="cnt" id="cnt" value=0 onfocus="this.select()" onkeyup="JiSuan(this)"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><font style="color: red">*</font><fmt:message key="reference_number" />：</div>
			<div class="form-input">
				<input type="text" name="cnt1" id="cnt1" value=0 onfocus="this.select()"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key ="standard_unit" />：</div>
			<div class="form-input">
				<input type="text" name="unit" id="unit" readonly="readonly" onfocus="this.select()"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="reference_unit" />：</div>
			<div class="form-input">
				<input type="text" name="unit1" id="unit1" readonly="readonly" onfocus="this.select()"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="specification" />：</div>
			<div class="form-input">
				<input type="text" readonly="readonly" name="sp_desc" id="sp_desc" onfocus="this.select()"/>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="remark" />：</div>
			<div class="form-input">
				<input type="text" name="memo" id="memo" onfocus="this.select()"/>
			</div>
		</div>
	</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript">
	var validate;
	$(document).ready(function(){
		$('#seachSupply').bind('click.custom',function(e){
			if(!!!top.customWindow){
				var defaultCode = $('#sp_code').val();
				top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode),$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),findOneSupply);
			}
		});
		findOneSupply=function(sp_code){
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findTop.do",
				data: "sp_code="+sp_code,
				success:function(supply){
					$("#cnt1").data("unitper",supply[0].unitper);
					$("#cnt").data("mincnt",supply[0].mincnt);
					$("#cnt").val(0);
					$("#cnt1").val(0);
				}
			});
		};
		JiSuan=function(data){
			if(isNaN($("#cnt1").data("unitper"))){
				return;
			}
			if(isNaN(data.value)){
				alert("<fmt:message key='please_enter_positive_integer'/>！");
				$("#cnt1").val(0);
			}else if(Number(data.value)<0){
				alert("<fmt:message key="number_cannot_be_negative"/>！");
				$("#cnt1").val(0);
			}else if(data.value<$("#cnt").data("mincnt")&&data.value!=0){
				alert("<fmt:message key='the_minimum_purchase_quantity_of_material'/>"+$("#cnt").data("mincnt")+"!");
				$("#cnt1").val(0);
			}else{
				$("#cnt1").val(Number(Number(data.value)*Number($("#cnt1").data("unitper"))).toFixed(2));
			}
		}
		//按钮快捷键
		focus() ;//页面获得焦点
	 	$(document).bind('keyup',function(e){
	 		if(e.keyCode==27){
	 			parent.$('.close').click();
	 		}
	 		if(e.altKey ==false)return;
	 		switch (e.keyCode){
	 			case 83: $('#autoId-button-105').click(); break;//保存
            }
		}); 
		/*验证*/
		validate = new Validate({
			validateItem:[{
				type:'text',
				validateObj:'title2',
				validateType:['canNull'],
				param:['F'],
				error:['<fmt:message key="title" /><fmt:message key="cannot_be_empty" />！']
			},{
				type:'text',
				validateObj:'title2',
				validateType:['maxLength'],
				param:[20],
				error:['<fmt:message key="title" /><fmt:message key="length_too_long" />！']
			},{
				type:'text',
				validateObj:'sp_code',
				validateType:['canNull','maxLength'],
				param:['F','10'],
				error:['<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！']
			},{
				type:'text',
				validateObj:'memo',
				validateType:['maxLength'],
				param:['10'],
				error:['<fmt:message key="maximum_length" />10！']
			}]
// 			},{
// 				type:'text',
// 				validateObj:'cnt',
// 				validateType:['num','intege'],
// 				param:['F','F'],
// 				error:['<fmt:message key="Invalid_quantity" />！','请输入数量！']
// 			},{
// 				type:'text',
// 				validateObj:'cnt1',
// 				validateType:['num','intege'],
// 				param:['F','F'],
// 				error:['<fmt:message key="Invalid_quantity2" />！','请输入数量！']
// 			}]
		});	
	});	
	var tool = $('.tool').toolbar({
		items: [{
			text: '<fmt:message key="save" />(<u>S</u>)',
			title: '<fmt:message key="save" />',
			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'save')},
			icon: {
				url: '<%=path%>/image/Button/op_owner.gif',
				position: ['-80px','-0px']
			},
			handler: function(){
				if(validate._submitValidate()){
					$('#addchkstodemoForm').submit();
				}
			}
		},{
			text: '<fmt:message key="quit"/>',
			title: '<fmt:message key="quit"/>',
			icon: {
				url: '<%=path%>/image/Button/op_owner.gif',
				position: ['-160px','-100px']
			},
			handler: function(){
				parent.$('.close').click();
			}
		}]
	});	
	//弹出物资树回调函数
	function handler(sp_code){
		if(sp_code==undefined || ''==sp_code){
			$('#sp_code').val(''); 
			$('#sp_name').val(''); 
			$('#sp_init').val(''); 
			$('#unit').val(''); 
			$('#unit1').val(''); 
			return;
		}
		$('.validateMsg').remove(); 
		$.ajax({
			type: "POST",
			url: "<%=path%>/supply/findById.do",
			data: "sp_code="+sp_code,
			dataType: "json",
			success:function(supply){
				$('#sp_code').val(supply.sp_code); 
				$('#sp_name').val(supply.sp_name); 
				$('#sp_init').val(supply.sp_init); 
				$('#unit').val(supply.unit); 
				$('#unit1').val(supply.unit1); 
			}
		});
	}
	</script>
</body>
</html>