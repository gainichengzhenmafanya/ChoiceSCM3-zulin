<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="subtract_the_theoretical_cost_data"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.table-head td span{
					white-space: normal;
				}
			</style>
	</head>	
	<body>
		<div class="tool">
		</div>
		<input type="hidden" name ="status"  id="status" value="${status}" />
		<input type="hidden" name ="date"  id="date" value="${date}" />
		<input type="hidden" name ="firm"  id="firm" value="${firm}" />
		<form action="" id="listForm" name="listForm" method="post">
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td><span style="width:80px;"><fmt:message key="branche"/></span></td>
								<td><span style="width:80px;"><fmt:message key="sector"/></span></td>
								<td><span style="width:80px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:100px;"><fmt:message key="name"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="standard_price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="quantity"/></span></td>
								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
<%-- 								<td><span style="width:50px;"><fmt:message key="discount"/></span></td> --%>
<%-- 								<td><span style="width:50px;"><fmt:message key="service_charge"/></span></td> --%>
<%-- 								<td><span style="width:50px;"><fmt:message key="taxes"/></span></td> --%>
<%-- 								<td><span style="width:50px;"><fmt:message key="free_entry"/></span></td> --%>
<%-- 								<td><span style="width:50px;"><fmt:message key="package_deals"/></span></td> --%>
<%-- 								<td><span style="width:50px;"><fmt:message key="quantity"/>2</span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="costCut" items="${costcutList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:80px;" title="${costCut.dat}">${costCut.dat}</span></td>
									<td><span style="width:80px;" title="${costCut.firmid}">${costCut.firmdes}</span></td>									
									<td><span style="width:80px;" title="${costCut.dept}">${costCut.deptdes}</span></td>
									<td><span style="width:80px;" title="${costCut.item}">${costCut.item}</span></td>
									<td><span style="width:100px;" title="${costCut.pdes }">${costCut.pdes }</span></td>
									<td><span style="width:40px;" title="${costCut.punit }">${costCut.punit }</span></td>
									<td><span style="width:50px; text-align: right;" title="${costCut.price }"><fmt:formatNumber value="${costCut.price }" pattern="##.##" /></span></td>
									<td><span style="width:50px; text-align: right;" title="${costCut.cnt }"><fmt:formatNumber value="${costCut.cnt }" pattern="##.##" /></span></td>
									<td><span style="width:50px; text-align: right;" title="${costCut.amt }"><fmt:formatNumber value="${costCut.amt }" pattern="##.##" /></span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>	
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</div>
		</form>
			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">

		//工具栏
			$(document).ready(function(){
				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 	});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="select" />(<u>F</u>)',
							title: '<fmt:message key ="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								search();
							}
						},{
							text: '<fmt:message key="subtract_the_theoretical_cost_data"/>(<u>C</u>)',
							title: '<fmt:message key="subtract_the_theoretical_cost_data"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-20px']
							},
							handler: function(){
								auditChkout();
							}
						},{
							text: '<fmt:message key ="Anti_reduction" />(<u>C</u>)',
							title: '<fmt:message key ="Anti_reduction" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-100px','-20px']
							},
							handler: function(){
								auditChkoutAnti();
							}
						},{
							text: '<fmt:message key="subtract_situation_queries"/>(<u>F</u>)',
							title: '<fmt:message key="subtract_situation_queries"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								searchChkout();
								
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}]
					});
					//自动实现滚动条
					setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
					setElementHeight('.table-body',['.table-head'],'.grid',25);	//计算.table-body的高度
					loadGrid();//  自动计算滚动条的js方法
					
					if($('#status').val()=="ok") {
						alert('<fmt:message key="subtract_completed"/>！');
					}else if($('#status').val()=="no"){
						alert('<fmt:message key ="branche" />:['+$('#firm').val()+'] <fmt:message key ="at" /> '+$('#date').val()+' <fmt:message key="subtract_cannot_continue_please_dont_select_this_branch"/>！');
					}else if($('#status').val()=="uncheckOK"){
						alert('<fmt:message key ="Anti_reduction_success" />！');
					}else if($('#status').val()=="nodata"){
						alert('<fmt:message key ="the_current_conditions_no_data_needs_to_be_reduced" />！');
					}
				});
		    //查询
			function search(){
// 				var checkboxList = parent.$('.grid').find('.table-body').find(':checkbox');
				var chkboxArray = window.parent.$(".treePanel1").find(":checked");
				if(chkboxArray 
						&& chkboxArray.length == 0){
					alert('<fmt:message key="please_select_one_branch"/>！');
					return;
				};
				if(!parent.$("#startdate").val()) {
					alert('<fmt:message key="please_select_the_startdate"/>！');
					return;
				};
				if(!parent.$("#enddate").val()) {
					alert('<fmt:message key="please_select_the_enddate"/>！');
					return;
				};
				if(parent.$("#startdate").val() > parent.$("#enddate").val()) {
					alert('<fmt:message key="startdate_littler_than_enddate"/>！');
					return;
				};
				var startDate = parent.$("#startdate").val();
				var endDate = parent.$("#enddate").val();
				var a =  new Date(endDate.replace(/-/g,"/")).getTime()- new Date(startDate.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>=7) {
					alert("<fmt:message key='select'/><fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>7<fmt:message key='day'/>!");
					return;
				}
				var firm = [];
				chkboxArray.map(function(){
					if($(this).attr("name")=='firm'){
						firm.push($(this).val());
					}
				});
				var action = "<%=path%>/costcut/table.do?action=query&firmid="+firm.join(',')+"&bdate="+parent.$("#startdate").val()+"&edate="+parent.$("#enddate").val();
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			function searchChkout(){
				$('body').window({
					id: 'window_searchChkout',
					title: '<fmt:message key="subtract_dates_and_stores"/>',
					content: '<iframe id="searchChkoutFrame" frameborder="0" src="<%=path%>/costcut/search.do"></iframe>',
					width: 550,
					height: '480px',
					confirmClose: false,
					draggable: true,
					isModal: true
				});
			}
			
			function auditChkout(){
// 				var checkboxList = parent.$('.grid').find('.table-body').find(':checkbox');
				var chkboxArray = window.parent.$(".treePanel1").find(":checked");
				if(chkboxArray 
						&& chkboxArray.length == 0){
					alert('<fmt:message key="please_select_one_branch"/>！');
					return;
				};
				if(!parent.$("#startdate").val()) {
					alert('<fmt:message key="please_select_the_startdate"/>！');
					return;
				};
				if(!parent.$("#enddate").val()) {
					alert('<fmt:message key="please_select_the_enddate"/>！');
					return;
				};
				if(parent.$("#startdate").val() > parent.$("#enddate").val()) {
					alert('<fmt:message key="startdate_littler_than_enddate"/>！');
					return;
				}; 
				var startDate = parent.$("#startdate").val();
				var endDate = parent.$("#enddate").val();
				var a =  new Date(endDate.replace(/-/g,"/")).getTime()- new Date(startDate.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>=7) {
					alert("<fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>7<fmt:message key='day'/>!");
					return;
				}
				var firm = [];
				chkboxArray.map(function(){
					if($(this).attr("name")=='firm'){
						firm.push($(this).val());
					}
				});
				var action = "<%=path%>/costcut/excutecostcut.do?firmid="+firm.join(',')+"&bdate="+parent.$("#startdate").val()+"&edate="+parent.$("#enddate").val();
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			
			//反核减
			function auditChkoutAnti(){
// 				var checkboxList = parent.$('.grid').find('.table-body').find(':checkbox');
				var chkboxArray = window.parent.$(".treePanel1").find(":checked");
				if(chkboxArray 
						&& chkboxArray.length == 0){
					alert('<fmt:message key="please_select_one_branch"/>！');
					return;
				};
				if(!parent.$("#startdate").val()) {
					alert('<fmt:message key="please_select_the_startdate"/>！');
					return;
				};
				if(!parent.$("#enddate").val()) {
					alert('<fmt:message key="please_select_the_enddate"/>！');
					return;
				};
				if(parent.$("#startdate").val() > parent.$("#enddate").val()) {
					alert('<fmt:message key="startdate_littler_than_enddate"/>！');
					return;
				}; 
				var startDate = parent.$("#startdate").val();
				var endDate = parent.$("#enddate").val();
				var a =  new Date(endDate.replace(/-/g,"/")).getTime()- new Date(startDate.replace(/-/g,"/")).getTime();
				if(a/(24*60*60*1000)>=7) {
					alert("<fmt:message key='time'/><fmt:message key='can_not_be_greater_than'/>7<fmt:message key='day'/>!");
					return;
				}
				var firm = [];
				chkboxArray.map(function(){
					if($(this).attr("name")=='firm'){
						firm.push($(this).val());
					}
				});
				var action = "<%=path%>/costcut/excutecostcutAnti.do?firmid="+firm.join(',')+"&bdate="+parent.$("#startdate").val()+"&edate="+parent.$("#enddate").val();
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
		</script>
	</body>
</html>