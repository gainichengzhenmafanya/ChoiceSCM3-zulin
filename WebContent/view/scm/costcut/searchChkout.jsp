<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>positn Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
			<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 0px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
			a.l-btn-plain{
				border:1px solid #7eabcd; 
			}				
			</style>
			<script type="text/javascript">
			var path="<%=path%>";
			</script>
		</head>
	<body>
		<form action="" id="queryForm" name="queryForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input">
					<input autocomplete="off" type="text" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input"><input autocomplete="off" type="text" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="search-commit">
		       		<input type="button" class="search-button" id="search" value='<fmt:message key="select"/>'/>
<%-- 		       		<a style="width:40px;text-align: center;" href="#" id="search" class="easyui-linkbutton" plain="true" iconCls=""><fmt:message key="select"/></a>	       		 --%>
				</div>
			</div>

			<div class="grid" style="overflow: auto;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" style="width: 25px;">&nbsp;</td>
								<td style="width:120px;"><fmt:message key="date"/></td>
								<td style="width:80px;"><fmt:message key="branches_encoding"/></td>
								<td style="width:140px;"><fmt:message key="branches_name"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkout" items="${chkoutList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td><span title="${chkout.dat}" style="width:110px;text-align: left;"><fmt:formatDate value="${chkout.dat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${chkout.firm}" style="width:70px;text-align: left;">${chkout.firm}&nbsp;</span></td>
									<td><span title="${chkout.firmdes}" style="width:130px;text-align: left;">${chkout.firmdes}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){	
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					if(!$("#bdat").val()) {
						alert('<fmt:message key="please_select_the_startdate"/>！');
						return;
					}
					if(!$("#edat").val()) {
						alert('<fmt:message key="please_select_the_enddate"/>！');
						return;
					}
					if($("#bdat").val() > $("#edat").val()) {
						alert('<fmt:message key="startdate_littler_than_enddate"/>！');
						return;
					}
					var action = "<%=path%>/costcut/query.do?startdate="+ $('#bdat').val()+"&enddate="+ $('#edat').val();
					$('#queryForm').attr('action',action);
					$('#wait').show();
					$('#wait2').show();
					$('#queryForm').submit();
				});
				
				focus() ;//页面获得焦点 

				setElementHeight('.grid',['.form-line'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
				//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }
	                else
	                {
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	}
	            });
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 });
				$("#bdat").focus(function(){
					new WdatePicker();
				});
				$("#edat").focus(function(){
					new WdatePicker();
				});
			});

			function onEnter(){
				var selected = $('.grid').find('.table-body').find(':checkbox').filter(':checked'); 
				if(selected.size() != 1){
					alert('<fmt:message key="please_select_single_message"/>');
					return;
				}
				selected.trigger("dblclick");
			}
		</script>
	</body>
</html>