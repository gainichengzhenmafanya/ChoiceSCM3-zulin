<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>student Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.tool {
					position: relative;
					height: 27px;
				}
				.page{
					margin-bottom: 25px;
				}
				.condition {
					position: relative;
					top: 1px;
					height: 31px;
					line-height: 31px;
				}
				.grid td span{
					padding:0px;
				}
			</style>
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/mainInfo/saveByUpdate.do" id="listForm" name="listForm" method="post">
			<input type="hidden" id= "acct" name="acct" value="${mainInfo.acct}"></input>
			<div class="form-line">
			<div class="form-label" style="height:28px;"><fmt:message key="years"/>:<c:out value="${yearr}" /></div>
			<div class="form-input" style="height:28px;">
				<input type="text" style="margin-top:3px;" readonly="readonly" id= "yearr" name="yearr" value="${dyear}"></input>
				<%-- <select name="yearr" id="yearr" class="select" onchange="reLoadMainInfo()"> 
				 <input name="yearr" id="yearr" class="text" value="${yearr}"><c:out value="${yearr}" /> 
					<c:forEach items="${yearrList }" var="yearr">
						<c:if test="${yearr == dyear}">
							<option value="<c:out value="${yearr }"/>" selected="selected"><c:out value="${yearr}"/></option>
						</c:if>
						<c:if test="${yearr != dyear}">
							<option value="<c:out value="${yearr }"/>" ><c:out value="${yearr}"/></option>
						</c:if>
					</c:forEach>
				</select> --%>
			</div>
			<div class="form-label" style="height:28px;"><fmt:message key="months"/>:&nbsp;&nbsp;&nbsp; <c:out value="${dmonth}" /><fmt:message key="month"/></div>
			</div>
						
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td style="width:150px;"><fmt:message key="accounting-period"/></td>
								<td style="width:180px;"><fmt:message key="startdate"/></td>
								<td style="width:180px;"><fmt:message key="enddate"/></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period1"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat1" name="bdat1" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat1}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat1" name="edat1" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat1}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period2"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat2" name="bdat2" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat2}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat2" name="edat2" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat2}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period3"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat3" name="bdat3" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat3}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat3" name="edat3" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat3}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period4"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat4" name="bdat4" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat4}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat4" name="edat4" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat4}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period5"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat5" name="bdat5" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat5}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat5" name="edat5" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat5}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period6"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat6" name="bdat6" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat6}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat6" name="edat6" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat6}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period7"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat7" name="bdat7" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat7}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat7" name="edat7" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat7}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period8"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat8" name="bdat8" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat8}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat8" name="edat8" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat8}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period9"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat9" name="bdat9" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat9}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat9" name="edat9" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat9}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period10"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat10" name="bdat10" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat10}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat10" name="edat10" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat10}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period11"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat11" name="bdat11" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat11}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat11" name="edat11" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat11}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
							<tr>
								<td style="width:150px;text-align: center;"><span><label><fmt:message key="accounting_period12"/></label></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="bdat12" name="bdat12" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.bdat12}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
								<td style="width:180px;text-align: center;"><span><input type="text" id="edat12" name="edat12" style="border: 0" class="Wdate text" value="<fmt:formatDate value="${mainInfo.edat12}" pattern="yyyy-MM-dd" type="date"/>" onclick="date();"/></span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{type:'text',validateObj:'bdat1',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat1',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat2',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat2',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat3',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat3',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat4',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat4',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat5',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat5',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat6',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat6',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat7',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat7',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat8',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat8',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat9',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat9',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat10',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat10',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat11',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat11',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'bdat12',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']},
					              {type:'text',validateObj:'edat12',validateType:['canNull'],param:['F'],error:['<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！']}
					              ]
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="modify_the_set_of_books_information"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','0px']
							},
							handler: function(){
								if(validate._submitValidate()){
									saveMainInfo();
								}
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: '<fmt:message key="close_accounting_month_to_modify"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$(window.parent.document).find('.close').click();
							}
						}]
				});


				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					
					//如果全选按钮选中的话，table背景变色
				$("#chkAll").click(function() {
	                if (!!$("#chkAll").attr("checked")) {
	                	$('.grid').find('.table-body').find('tr').addClass("bgBlue");
	                }else{
	                	$('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                	      }
	            });	
				
			});
			function saveMainInfo(){
				if(($("#bdat1").val()>$("#edat1").val()) || ($("#bdat2").val()>$("#edat2").val())
					&& ($("#bdat3").val()>$("#edat3").val()) || ($("#bdat4").val()>$("#edat4").val())
					&& ($("#bdat5").val()>$("#edat5").val()) || ($("#bdat6").val()>$("#edat6").val())
					&& ($("#bdat7").val()>$("#edat7").val()) || ($("#bdat8").val()>$("#edat8").val())
					&& ($("#bdat9").val()>$("#edat9").val()) || ($("#bdat10").val()>$("#edat10").val())
					&& ($("#bdat11").val()>$("#edat11").val()) || ($("#bdat12").val()>$("#edat12").val())){
					alert("<fmt:message key ="startdate_not_littler_than_enddate" />!");
					return;
				}
				window.document.getElementById("listForm").submit();
			}
			
			function reLoadMainInfo(){
				var year = $("#yearr").val();
				var data = {};
				data['yearr']=year;
				$.post("<%=path%>/mainInfo/getMainList.do",data,function(data){
					var rs = eval('('+data+')');
					$("#acct").val(rs.acct);
					$("#bdat1").val(rs.bdat1);
					$("#edat1").val(rs.edat1);
					$("#bdat2").val(rs.bdat2);
					$("#edat2").val(rs.edat2);
					$("#bdat3").val(rs.bdat3);
					$("#edat3").val(rs.edat3);
					$("#bdat4").val(rs.bdat4);
					$("#edat4").val(rs.edat4);
					$("#bdat5").val(rs.bdat5);
					$("#edat5").val(rs.edat5);
					$("#bdat6").val(rs.bdat6);
					$("#edat6").val(rs.edat6);
					$("#bdat7").val(rs.bdat7);
					$("#edat7").val(rs.edat7);
					$("#bdat8").val(rs.bdat8);
					$("#edat8").val(rs.edat8);
					$("#bdat9").val(rs.bdat9);
					$("#edat9").val(rs.edat9);
					$("#bdat10").val(rs.bdat10);
					$("#edat10").val(rs.edat10);
					$("#bdat11").val(rs.bdat11);
					$("#edat11").val(rs.edat11);
					$("#bdat12").val(rs.bdat12);
					$("#edat12").val(rs.edat12);
				});
			}
			
			function date(){
				new WdatePicker();
			}	
		</script>
	</body>
</html>