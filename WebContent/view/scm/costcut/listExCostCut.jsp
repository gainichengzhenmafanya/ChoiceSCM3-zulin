<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>加工间理论成本核减</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>	
	<body>
		<div class="tool"></div>
		<form action="<%=path%>/costcut/excostcut.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key ="months" />:</div>
				<div class="form-input" >
					<select id="month" name="month" class="select" style="width:40px;">
					<option <c:if test="${month=='1' }"> selected="selected" </c:if> value="1">1</option>
					<option <c:if test="${month=='2' }"> selected="selected" </c:if> value="2">2</option>
					<option <c:if test="${month=='3' }"> selected="selected" </c:if> value="3">3</option>
					<option <c:if test="${month=='4' }"> selected="selected" </c:if> value="4">4</option>
					<option <c:if test="${month=='5' }"> selected="selected" </c:if> value="5">5</option>
					<option <c:if test="${month=='6' }"> selected="selected" </c:if> value="6">6</option>
					<option <c:if test="${month=='7' }"> selected="selected" </c:if> value="7">7</option>
					<option <c:if test="${month=='8' }"> selected="selected" </c:if> value="8">8</option>
					<option <c:if test="${month=='9' }"> selected="selected" </c:if> value="9">9</option>
					<option <c:if test="${month=='10' }"> selected="selected" </c:if> value="10">10</option>
					<option <c:if test="${month=='11' }"> selected="selected" </c:if> value="11">11</option>
					<option <c:if test="${month=='12' }"> selected="selected" </c:if> value="12">12</option>
				</select>
				</div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 16px;">&nbsp;</span></td>
								<!-- <td rowspan="2"><span style="width:20px;">
									<input type="checkbox" id="chkAll"/></span>
								</td> -->
								<td><span style="width:100px;"><fmt:message key="workshop"/></span></td>
								<td><span style="width:80px;"><fmt:message key="date"/></span></td>
								<td><span style="width:140px;"><fmt:message key="fill_time"/></span></td>
								<td><span style="width:50px;"><fmt:message key="serial_number"/></span></td>
								<td><span style="width:100px;"><fmt:message key="scm_document_no"/></span></td>
								<td><span style="width:50px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:50px;"><fmt:message key="orders_audit"/></span></td>
								<td><span style="width:80px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:80px;"><fmt:message key="summary"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="chkinm" varStatus="status" items="${chkinmList}">
								<tr>
									<td class="num"><span style="width:16px;">${status.index+1}</span></td>
									<%-- <td><span style="width:20px;text-align: center;">
										<input type="checkbox" name="idList" id="chk_${chkinm.chkinno}" value="${chkinm.chkinno}"/></span>
									</td> --%>
									<td><span title="${chkinm.deliver.des}" style="width:100px;">${chkinm.deliver.des}&nbsp;</span></td>
									<td><span title="<fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>" style="width:80px;"><fmt:formatDate value="${chkinm.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${chkinm.madet}" style="width:140px;">${chkinm.madet}&nbsp;</span></td>
									<td><span title="${chkinm.chkinno}" style="width:50px;">${chkinm.chkinno}&nbsp;</span></td>
									<td><span title="${chkinm.vouno}" style="width:100px;">${chkinm.vouno}&nbsp;</span></td>
									<td><span title="${chkinm.madeby}" style="width:50px;">${chkinm.madeby}&nbsp;</span></td>
									<td><span title="${chkinm.checby}" style="width:50px;">${chkinm.checby}&nbsp;</span></td>
									<td><span title="${chkinm.totalamt}" style="width:80px;text-align:right;">${chkinm.totalamt}&nbsp;</span></td>
									<td><span title="${chkinm.memo}" style="width:80px;">${chkinm.memo}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">

		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 	});
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key="subtract_the_theoretical_cost_data"/>(<u>C</u>)',
						title: '<fmt:message key="subtract_the_theoretical_cost_data"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-100px','-20px']
						},
						handler: function(){
							check();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),75);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();
			});
			
			function check(){
				var r = confirm('<fmt:message key ="To_confirm_the_calculation_processing_of_this_month_between_the_theory_of_consumption" />？');
				if (r==true) {
					$.ajax({
						type: "POST",
						url: '<%=path%>/costcut/excuteexcostcut.do',
						data: "month="+$('#month').val(),
						dataType: "text",
						success:function(data){
							alert("<fmt:message key ="Subtract_the_success" />！");
						},
						error: function(){
							alert('<fmt:message key="server_busy_please_try_again"/>');
						}
					});
				}
			}
		</script>
	</body>
</html>