<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>加工单生成领料单主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/explanM/listExplanM.do" method="post">
				<div class="form-line">
					<div class="form-label"><fmt:message key="processing_room"/>：</div>
					<div class="form-input" style="width:200px;">
<%-- 						<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${explanM.positn.code}"/> --%>
						<input type="hidden" class="text"  id="positnDes" />
						<input type="hidden" class="text"  id="typ" name="typ" value="A"/>
						<select class="select" id="positn" name="positn.code" style="width:180px;" onchange="findPositnByDes(this);">
								<option></option>
							<c:forEach var="positn" items="${listPositn}" varStatus="status">
								<option 
									<c:if test="${positn.code==explanM.positn.code}"> 
											   	selected="selected"
									</c:if> 
									id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-label"><fmt:message key="starttime"/>：</div>
					<div class="form-input">
						<input type="text" id="maded" name="maded" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${explanM.maded}' pattern='yyyy-MM-dd'/>"/>
					</div>
					<div class="form-label"><fmt:message key="endtime"/>：</div>
					<div class="form-input">
						<!-- 默认显示系统当前日期 同开始日期 -->
						<input type="text" id="madedEnd" name="madedEnd" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${madedEnd}' pattern='yyyy-MM-dd'/>"/>
					</div>
				</div>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td><span style="width:100px;"><fmt:message key="orders_num"/></span></td>
									<td><span style="width:100px;"><fmt:message key="fill_time"/></span></td>
									<td><span style="width:120px;"><fmt:message key="time"/></span></td>
									<td><span style="width:100px;"><fmt:message key="orders_maker"/></span></td>
									<td><span style="width:100px;"><fmt:message key="orders_audit"/></span></td>
									<td><span style="width:100px;"><fmt:message key="processing_room"/></span></td>
									<td><span style="width:100px;"><fmt:message key="document_sources"/></span></td>
									<td><span style="width:100px;"><fmt:message key="scm_hope_date"/></span></td>
									<td><span style="width:100px"><fmt:message key="whether_the_material"/></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="explanM" items="${listExplanM}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList"  value="${explanM.explanno}"/>
										</td>
										<td><span style="width:100px;" title="${explanM.explanno}">${explanM.explanno}</span></td>
										<td><span style="width:100px;" title="${explanM.maded}"><fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:120px;" title="${explanM.madet}">${explanM.madet}</span></td>
										<td><span style="width:100px;" title="${explanM.madeby}">${explanM.madeby}</span></td>
										<td><span style="width:100px;" title="${explanM.checby}">${explanM.checby}</span></td>
										<td><span style="width:100px;" title="${explanM.positn.code}">${explanM.positn.des}</span></td>
										<td><span style="width:100px;" title="${explanM.typ}"><c:if test="${explanM.typ==0}"><fmt:message key ="Fill_in" /></c:if>
											<c:if test="${explanM.typ==1}"><fmt:message key ="reports" /></c:if><c:if test="${explanM.typ==2}"><fmt:message key ="scm_safety_stock" /></c:if><c:if test="${explanM.typ==3}"><fmt:message key="The_material" /></c:if></span></td>
										<td><span style="width:100px;" title="${explanM.hoped}"><fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:100px;" title="${explanM.ynll }"><img src="<%=path%>/image/kucun/${explanM.ynll}.png"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
				</div>
			</form>
<!-- 		<div class="mainFrame" style="height:60%;width:100%;margin-top: 13px;"> -->
<!-- 		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe> -->
<!-- 		</div> -->
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select"/>',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},"-",{
						text: '<fmt:message key="generate_material_requisition"/>',
						title: '<fmt:message key="generate_material_requisition"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()<1) {
								alert("<fmt:message key="Please_select_the_processing_operation" />！");
							}else{
// 								window.document.getElementById("mainFrame").contentWindow.chkstom();
								toChkstom();
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
			 
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     } else {
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			 });
			
			
			/* 模糊清空 */
			$("#resetSearch").bind('click', function() {
				$('.search-condition input').val('');
			});
			$('.grid').find('.table-body').find('tr').live("dblclick", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).find(":checkbox").attr("checked", !$(this).find(":checkbox").attr("checked"));
			     }else{
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue");
			         $(this).find(":checkbox").attr("checked", !$(this).find(":checkbox").attr("checked"));
			         var explanno=$(this).find(':checkbox').val();
<%-- 					 //var  url="<%=path%>/explanM/tableExplanM.do?explanno="+explanno; --%>
					 //$('#mainFrame').attr('src',url);
				     //window.mainFrame.location =url;
			     }
			 });
		});
		
		//生成领料单
		function toChkstom(){
			var flag = true;
			var ary = [];
			var idChkValue = [];
			var checkboxList = $('.grid').find('.table-body').find('tr').find(':checkbox');
			checkboxList.filter(':checked').each(function(i){
				idChkValue.push($(this).val());
				ary.push($(this).parents('tr').find('td:eq(7)').find('span').attr('title'));
				if($(this).parents('tr').find('td:eq(10)').find('span').attr('title') == 'Y'){//如果有已经领料的  wjf，则提示
					flag = false;
				}
			});
			if(!flag){//提示一下 wjf
				if(!confirm('<fmt:message key="There_have_been_picking_orders_whether_to_continue_to_advance" />？')){
					return;
				}
			}
			var nary=ary.sort();  
			if (nary[0]!=nary[ary.length-1]){   
				alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
				return;
			}  
			var ids=idChkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key ="generate_material_requisition" />',
				content: '<iframe id="chkstomFrame" name="chkstomFrame" frameborder="0" src="<%=path%>/explanM/toChkoutm.do?positn.code='+nary[0]+'&idList='+ids+'"></iframe>',
				width: '750px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="generate_material_requisition"/>',
							title: '<fmt:message key="generate_material_requisition"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								window.frames["chkstomFrame"].saveChkstom();
							}
						},{
							text: '<fmt:message key="Export_raw_material_documents" />',
							title: '<fmt:message key="Export_raw_material_documents" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								window.frames["chkstomFrame"].getCheckItemCode();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		function pageReload(){
	        window.location.href = '<%=path%>/explanM/listExplanM.do?positn.code=${explanM.positn.code}';
	    }
		//双击事件
		$('.grid .table-body tr').live('dblclick',function(){
			$(":checkbox").attr("checked", false);
			$(this).find(":checkbox").attr("checked", true);
			toSelectDetailed();
		});
		
		//add wangjie 2014年10月27日 09:25:36
		
		//单击事件        不要了  2014.11.6wjf
// 		$(".grid .table-body tr").live("click",function(){
// 			var explanno=$(this).find(':checkbox').val();
<%-- 			var  url="<%=path%>/explanM/tableExplanM.do?explanno="+explanno; --%>
// 			$('#mainFrame').attr('src',url);
// 		    window.mainFrame.location =url;
// 		});
		
		var detailWin;
		function toSelectDetailed(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList&& checkboxList.filter(':checked').size() ==1){
				var chkValue = checkboxList.filter(':checked').val();
				detailWin = $('body').window({
					id: 'checkedChkoutmDetail',
					title: '<fmt:message key="Query_processing_Dan_Mingxi" />',
					content: '<iframe id="checkedChksoutDetailFrame" frameborder="0" src="<%=path%>/explanM/toSelectDetailed.do?explanno='+chkValue+'"></iframe>',
					width: '640px',
					height: '460px',
					draggable: true,
					isModal: true,
					confirmClose:false
				});
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		
		</script>
	</body>
</html>