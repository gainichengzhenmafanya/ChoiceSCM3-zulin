<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>explan Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
				.form-line .form-label{
					width: 5%;
				}
				.form-line .form-input{
					width: 11%;
				}
				.form-line .form-input input[type=text]{
					width: 98%;
				}
			</style>						
		</head>
	<body>
		<div class="tool">
		</div>
		<form action="<%=path%>/explan/list.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input">
				    <input autocomplete="off" type="text" id="dat" name="dat" class="Wdate text" value="<fmt:formatDate value="${explan.dat}" pattern="yyyy-MM-dd"/>" />
				</div>
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input">
				 	  <input autocomplete="off" type="text" id="datEnd" name="datEnd" class="Wdate text" value="<fmt:formatDate value="${explan.datEnd}" pattern="yyyy-MM-dd"/>" />
				</div>
				<div class="form-input">
					<input type="radio" id="" name="curStatus" value="0" checked="checked"
						<c:if test="${curStatus=='0'}"> checked="checked"</c:if> 
					/><fmt:message key="all"/>
					<input type="radio" id="" name="curStatus" value="1" 
						<c:if test="${curStatus=='1'}"> checked="checked"</c:if> 
					/><fmt:message key="not_report_the_goods"/>
					<input type="radio" id="" name="curStatus" value="2" 
						<c:if test="${curStatus=='2'}"> checked="checked"</c:if> 
					/><fmt:message key="undecomposed"/>
					<input type="radio" id="" name="curStatus" value="3" 
						<c:if test="${curStatus=='3'}"> checked="checked"</c:if> 
					/><fmt:message key="completed_but_storage"/>
					<input type="radio" id="" name="curStatus" value="4" 
						<c:if test="${curStatus=='4'}"> checked="checked"</c:if> 
					/><fmt:message key="already_in_storage"/>
				</div>
			</div>
			<div class="form-line">

				<div class="form-label"><fmt:message key="processing_room"/></div>
				<div class="form-input">
		 			<input type="text" name="supply.positnexdes" id="positnexdes" class="selectDepartment text" readonly="readonly" value="${explan.supply.positnexdes}"/>
					<input type="hidden" name="supply.positnex" id="positnex" class="text" value="${explan.supply.positnex}" />
				</div>
				<div class="form-label"><fmt:message key="category"/></div>
				<div class="form-input">
					<input type="text" name="supply.grpdes" id="grpdes" class="selectDepartment text" readonly="readonly" value="${explan.supply.grpdes}"/>
					<input type="hidden" name="supply.grp" id="grp" class="text" value="${explan.supply.grp}" />
				</div>			
				<div class="form-label"><fmt:message key="coding"/></div>
				<div class="form-input">
					<input type="text" class="text" id="sp_code" name="supply.sp_code" value="${explan.supply.sp_code}" /><img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="查询物资" />
				</div>	
				<div class="form-label" style="width: 20px;"></div>		
				<div class="form-input">
					<span style="font-weight: bold;"><fmt:message key="Not_saving_plan" /> <span id="jhChangeNum" style="font-weight: bold;color: red;">0</span> <fmt:message key ="article" /></span>
					<span style="font-weight: bold;margin-left: 20px;"><fmt:message key="Not_saved" /> <span id="djChangeNum" style="font-weight: bold;color: blue;">0</span> <fmt:message key ="article" /></span>
				</div>					
			</div>
			<div class="form-line">
				<div class="form-label" style="width: 200px;"></div>	
				<div class="form-input" style="margin-left: 50px;margin-top:5px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" ><div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div></div><div id="per" style="margin-top: 5px;">0</div>					
				
			</div>
		</form>
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 30px;">&nbsp;</span></td>
								<td rowspan="2">
									<span style="width:20px;text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</span>
								</td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="processing_no"/></span></td>
								<td rowspan="2"><span style="width:40px;"><fmt:message key="status"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="workshop"/></span></td>
								<td colspan="3"><span><fmt:message key="product"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="demand"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="current_inventory"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="has_planned_amount"/></span></td>
								<td rowspan="2"><span style="width:60px;"><fmt:message key="required_processing_capacity"/></span></td> 
								<td colspan="3"><span><fmt:message key="postpartum_registration"/></span></td>
								<td rowspan="2"><span style="width:40px;white-space: normal;"><fmt:message key="whether_biodegradable"/></span></td> 
								<td rowspan="2"><span style="width:40px;white-space: normal;"><fmt:message key="whether_report_goods"/></span></td> 
								<td rowspan="2"><span style="width:40px;white-space: normal;"><fmt:message key="whether_storage"/></span></td> 
								<td rowspan="2"><span style="width:60px;"><fmt:message key="storage_positions"/></span></td> 
								<td rowspan="2"><span style="width:130px;"><fmt:message key="starttime"/></span></td> 
								<td rowspan="2"><span style="width:130px;"><fmt:message key="endtime"/></span></td> 
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:80px;"><fmt:message key="name"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;" ><fmt:message key="number_of_standards"/></span></td> 
								<td><span style="width:60px;"><fmt:message key="reference_number"/></span></td> 
								<td><span style="width:60px;"><fmt:message key="unit"/></span></td> 
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="explan" items="${explanList}" varStatus="status">
								<tr>
									<td class="num" ><span style="width:30px;">${status.index+1}</span></td>
									<td >
										<span style="width:20px;text-align: center;">
											<input type="checkbox" name="idList" id="chk_${explan.id}" value="${explan.id}"/>
										</span>
									</td>
									<td><span title="${explan.exno}" style="width:40px;text-align: right;">${explan.exno}&nbsp;</span></td>
									<td><span title="${explan.sta}" style="width:40px;">${explan.sta}&nbsp;</span></td>
									<td><span title="${explan.supply.positnex}" style="width:90px;">${explan.supply.positnexdes}&nbsp;</span></td>			
									<td><span title="${explan.supply.sp_code}" style="width:70px;">${explan.supply.sp_code}&nbsp;</span></td>
									<td><span title="${explan.supply.sp_name}" style="width:80px;">${explan.supply.sp_name}&nbsp;</span></td>
									<td><span title="${explan.supply.unit}" style="width:30px;">${explan.supply.unit}&nbsp;</span></td>
									<td><span title="${explan.cntplan}" style="width:60px;text-align: right;"><fmt:formatNumber value="${explan.cntplan}" pattern="0"/>&nbsp;</span></td>
									<td><span title="${explan.cntkc}" style="width:60px;text-align: right;"><fmt:formatNumber value="${explan.cntkc}" pattern="0"/>&nbsp;</span></td>
									<td><span title="${explan.cntplan1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${explan.cntplan1}" pattern="0"/>&nbsp;</span></td>
									<td class="textInput">
										<span title="${explan.cnt}" style="width:70px;padding: 0px;text-align: right;">
											<input type="text" value="<fmt:formatNumber value='${explan.cnt}' pattern='0'/>" onfocus="this.select()" onblur="validate(this);addCnt(this,'${explan.id}')" style="width:65px;padding: 0px;text-align: right;"/>
										</span>
									</td>
									<td class="textInput">
										<span title="${explan.cntact}" style="width:70px;padding: 0px;text-align: right;">
											<input type="text" value="<fmt:formatNumber value='${explan.cntact}' pattern='0.00'/>" onfocus="this.select()" onblur="validate(this);addCntact(this,'${explan.id}')" style="width:65px;padding: 0px;text-align: right;"/>
											<input type="hidden" name="unitper1" value="${explan.unitper}"></input>
										</span>
									</td>
									<td class="textInput">
										<span title="${explan.cntact1}" style="width:70px;padding: 0px;text-align: right;">
											<input type="text" value="<fmt:formatNumber value='${explan.cntact1}' pattern='0.00'/>" onfocus="this.select()" onblur="validate(this);addCntact1(this,'${explan.id}')" style="width:65px;padding: 0px;text-align: right;"/>
											<input type="hidden" name="unitper" value="${explan.unitper}"></input>
										</span>
									</td >
									<td><span title="${explan.supply.unit}" style="width:60px;">${explan.supply.unit}&nbsp;</span></td>
									<td><span title="${explan.ynex}" style="width:40px;">${explan.ynex}&nbsp;</span></td>
									<td><span title="${explan.ynsto}" style="width:40px;">${explan.ynsto}&nbsp;</span></td>
									<td><span title="${explan.ynin}" style="width:40px;">${explan.ynin}&nbsp;</span></td>
									<td><span title="${explan.supply.positndes}" style="width:60px;">${explan.supply.positndes}&nbsp;</span></td>
									<td><span title="${explan.btim}" style="width:130px;">${explan.btim}&nbsp;</span></td>
									<td><span title="${explan.etim}" style="width:130px;">${explan.etim}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;//判断当前页面ajax是否已经准备就绪
		var pageSize = 0;
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			new tabTableInput("table-body","text"); //input  上下左右移动
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
			                case 70: $('#autoId-button-101').click(); break;
			                case 65: $('#autoId-button-102').click(); break;
			                case 81: $('#autoId-button-103').click(); break;
			                case 87: $('#autoId-button-104').click(); break;
			                case 69: $('#autoId-button-105').click(); break;
			                case 82: $('#autoId-button-106').click(); break;
			                case 84: $('#autoId-button-107').click(); break;
			                case 68: $('#autoId-button-108').click(); break;
			            }
			 	});
			
		 	//编辑需加工量、标准数量、参考数量，按回车可以跳到下一行的同一列
			$('.textInput').live('keydown',function(e){
				if(parent.ddzxEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).prevAll().length;
						var hang= $(this).parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
			 		}
				}
			});
		 	//解决谷歌浏览器单击不选中的问题
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
		 	
			if('${action}'!='init' && '${currState}'!='1'){
				addTr('first');	
			}else if('${action}'!='init' && '${currState==1}'){
				$("#per").text('100%');
				$("#currState").width(200);
			}
			pageSize = '${pageSize}';
			var ndivHeight = $(".table-body").height();
			$(".table-body").scroll(function(){
		          	nScrollHeight = $(this)[0].scrollHeight;
		          	nScrollTop = $(this)[0].scrollTop;
		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
		          			returnInfo = false;
		          			addTr();	
		          	}
		          });
			
			if('${action}'=='init'){
				parent.ddzxEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.jhTrlist=undefined;//将计划list清空
				parent.djTrlist=undefined;//将登记list清空
				parent.jhChangeNum=0;//将已经修改的条数改为0
				parent.djChangeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#jhChangeNum").text(parent.jhChangeNum);
				$("#djChangeNum").text(parent.djChangeNum);
			}
			
			//判断如果不是编辑状态
			if(parent.ddzxEditState!='edit'){
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			}else{
				$('tbody input[type="text"]').attr('disabled',false);
				if($('tbody tr:first .nextclass').length!=0){
					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
				}
			}
		 	
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			/*弹出树*/
			$('#grpdes').bind('focus.custom',function(e){
				var grp=$('#grp').val();
				if(!!!top.customWindow){
					var offset = getOffset('grpdes');
					top.custom('<fmt:message key="please_select_category"/>','<%=path%>/explan/selectGrp.do?grp='+grp,offset,$(this),$('#grp'),null,null,null,'150','300','isNull');
				}
			});
			$('#positnexdes').bind('focus.custom',function(e){
				var positnex=$('#positnex').val();
				if(!!!top.customWindow){
					var offset = getOffset('positnexdes');
					top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#positnex'),null,null,null,'150','300','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#dat').bind('click',function(){
				new WdatePicker();
			});
			$('#datEnd').bind('click',function(){
				new WdatePicker();
			});
			//默认两个保存按钮是不可用的
			loadToolBar([true,false,false]);
		});
		
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="query_scheduling_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var dat = $('#listForm').find("#dat").val().toString();
							var datEnd= $('#listForm').find("#datEnd").val().toString();
							if(dat<=datEnd)
							$('#listForm').submit();
							else
								alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
						}
					},{
						text: '<fmt:message key="edit" />(<u>F</u>)',
						title: '<fmt:message key="edit" />',
						useable: use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($('.grid').find('.table-body').find("tr").size()<1){
								alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							}else{
								parent.ddzxEditState='edit';
								loadToolBar([false,true,true]);
								//已经入库的就不能修改  wjf
								$('.grid').find('.table-body').find(':checkbox').each(function(){
									var isruku = $(this).parents('tr').find('td:eq(17)').find('span').attr('title');
									if(isruku == 'Y'){
										$(this).parents('tr').find('input[type="text"]').attr('disabled',true);
									}else{
										$(this).parents('tr').find('input[type="text"]').attr('disabled',false);
									}
								});
								new tabTableInput("table-body","text"); //input  上下左右移动
								if($('tbody tr:first').length!=0){
									$('tbody tr:first .textInput span input')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
								}
							}
						}
					},{
						text: '<fmt:message key="daily_delivery_plan"/>',
						title: '<fmt:message key="daily_delivery_plan"/>',
					//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-20px','-80px']
						},
						handler: function(){
							updateExplanByPlan();
						}
					},{
						text: '<fmt:message key="safety_stock_plan"/>',
						title: '<fmt:message key="safety_stock_plan"/>',
					//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							safeCntToPlan();						}
					},{
						text: '<fmt:message key="add_product" />(<u>A</u>)',
						title: '<fmt:message key="add_product"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							supply_select();
						}
					},{
						text: '<fmt:message key="decomposition_products" />(<u>Q</u>)',
						title: '<fmt:message key="decomposition_products"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-80px']
						},
						handler: function(){ 
							resolveExplan();
						}
					},{
						text: '<fmt:message key="preservation_program" />(<u>W</u>)',
						title: '<fmt:message key="preservation_program"/>',
						useable : use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="Just_save_the_amount_of_processing_required_to_continue" />？')){
								saveCnt();//保存计划
							}
						}
					},{
						text: '<fmt:message key="save_registration" />(<u>E</u>)',
						title: '<fmt:message key="save_registration"/>',
						useable : use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="Only_the_number_of_standards_and_the_number_of_references_to_the_post_natal_registration_continue" />？')){
								saveCntact();//保存登记	
							}
						}
					},{
						text: '<fmt:message key="generated_newspaper_manifest" />(<u>R</u>)',
						title: '<fmt:message key="generated_newspaper_manifest"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-20px']
						},
						handler: function(){
							if(parent.ddzxEditState=='edit'){
								if(confirm('<fmt:message key="The_need_to_modify_processing_quantity_standard_number_reference_number_is_not_saved_continue_to_generate_report_list" />？')){
									chkstom();
								}
							}else{
								chkstom();
							}
						}
					},{
						text: '<fmt:message key="registration_storage" />(<u>T</u>)',
						title: '<fmt:message key="registration_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-40px']
						},
						handler: function(){
							if(parent.ddzxEditState=='edit'){
								if(confirm('<fmt:message key="The_need_to_change_the_amount_of_processing_the_number_of_standards_reference_number_is_not_saved_continue_to_register_storage" />？')){
									chkinm();
								}
							}else{
								chkinm();
							}
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="delete_product_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							deleteExplan();
						
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
		}
		
			var sp_code_;
			/*弹出物资树*/
			function supply_select(){
				 sp_code_=$('#sp_code').val(); 
				if(!!!top.customWindow){
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?ex1=Y',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),handler);	
				}
				
			};
			//弹出物资树回调函数
			function handler(sp_code){
				$('#sp_code').val(sp_code_); 
				if(sp_code==undefined || ''==sp_code){
					return;
				}
				$.ajax({
					type: "POST",
					url: "<%=path%>/supply/findById.do",
					data: "sp_code="+sp_code,
					dataType: "json",
					success:function(supply){
						//alert(supply.ynex);
						if('Y'!=supply.ex1){
							alert('<fmt:message key="noprocessing_supplies"/>！');
						}else{
							var action = '<%=path%>/explan/save.do?supply.sp_code='+sp_code;
							$('body').window({
								title: '<fmt:message key="add_supplies_products"/>',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: 500,
								height: '245px',
								draggable: true,
								isModal: true
							});
						}
					}
				});
			}
		    	function onBlurMethod(inputObj){
		    		$(inputObj).parent('span').text($(inputObj).val());
	    		}
			//保存计划
			function saveCnt(){
				$("#dat").focus();
				//如果修改过数据就保存 
				if(parent.jhTrlist){
					var predata = parent.jhTrlist;
					var disList = {};
					var num=0;
					for(var i in predata){
						disList['explanList['+num+'].id']=i;
						undefined==predata[i].cnt?'':disList['explanList['+num+'].cnt']=predata[i].cnt;
						num++;
					}
					$.post('<%=path%>/explan/updateCnt.do',disList,function(data){
						var rs = eval('('+data+')');
						if(rs.pr=="succ"){
							alert('<fmt:message key ="update_successful" />'+rs.updateNum+'<fmt:message key ="article_data" />！');
							var action="<%=path%>/explan/list.do";
							$('#listForm').attr('action',action);
						}
					});	
				}else{
					alert('<fmt:message key="You_do_not_modify_any_data" />！');
				}
				parent.ddzxEditState = '';
				parent.jhChangeNum = 0;
				parent.djChangeNum = 0;
				parent.jhTrlist=undefined;
				parent.djTrlist=undefined;
				loadToolBar([true,false,false]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				$('#listForm').submit();
			}
			//保存登记
			function saveCntact(){
				$("#dat").focus();
				//如果修改过数据就保存 
				if(parent.djTrlist){
					var predata = parent.djTrlist;
					var disList = {};
					var num=0;
					for(var i in predata){
						disList['explanList['+num+'].id']=i;
						undefined==predata[i].cntact?'':disList['explanList['+num+'].cntact']=predata[i].cntact;
						undefined==predata[i].cntact1?'':disList['explanList['+num+'].cntact1']=predata[i].cntact1;
						num++;
					}
					$.post('<%=path%>/explan/updateCntact.do',disList,function(data){
						var rs = eval('('+data+')');
						if(rs.pr=="succ"){
							alert('<fmt:message key ="update_successful" />'+rs.updateNum+'<fmt:message key ="article_data" />！');
							var action="<%=path%>/explan/list.do";
							$('#listForm').attr('action',action);
						}
					});	
				}else{
					alert('<fmt:message key="You_do_not_modify_any_data" />！');
				}
				parent.ddzxEditState = '';
				parent.jhChangeNum = 0;
				parent.djChangeNum = 0;
				parent.jhTrlist=undefined;
				parent.djTrlist=undefined;
				loadToolBar([true,false,false]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				$('#listForm').submit();
			}
			//删除
			function deleteExplan(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm"/>？')){
						var flag = true;//判断加工单是否是已完成
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							var sta=$(this).parents('tr').find('td:eq(3)').find('span').attr('title');
							if (sta=='完成' || sta=='开始'){
								flag=false;
							}else{
								chkValue.push($(this).val());
							}
						});
						if(!flag){
							alert('<fmt:message key="Have_started_or_completed_the_processing_can_not_be_deleted" />！');
							return;
						}
						var action = '<%=path%>/explan/delete.do?ids='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_scheduling_information"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
						
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
					return ;
				}
			}
			//分解产品
			function resolveExplan(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="only_checked_options_broken_down_whether_to_continue"/>!')){
						var chkValue = [];
						var ary = [];
						var sta = 0;
						var sp_code = '';
						checkboxList.filter(':checked').each(function(){
							sp_code = $(this).parents('tr').find('td:eq(5)').find('span').attr('title');
							if('N' == $(this).parents('tr').find('td:eq(15)').find('span').attr('title')){
								sta=1;
								return false;
							}
							ary.push($(this).val());
							chkValue.push(sp_code);
						});
						if(sta==1){
							alert('<fmt:message key ="whether_semi" />：【'+sp_code+'】<fmt:message key="Non_decomposition" />！');
							return;
						}
						var action = '<%=path%>/explan/resolveExplan.do?sp_codeIds='+chkValue.join(",")+'&ids='+ary.join(",");
						$('body').window({
							title: '<fmt:message key="decomposition_products"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="Choose_products_that_need_to_be_broken_down" />！');
					return ;
				}
			}
			//登记入库
			function chkinm(){
				var flag = true;//判断是否有已入库的 有就提示不要选  wjf
				var flag1 = true;//判断入库数量是否为0
				var flag2 = true;//选的加工间必须一致
				var flag3 = true;//入到的基地仓库必须一样
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() <= 0){ 
					alert('<fmt:message key="please_select_the_information_storage"/>！');
					return;
				}
				var positnex=$('.grid').find('.table-body').find(':checkbox').filter(':checked').first().parents('tr').find('td:eq(4)').find('span').attr('title');//加工间
				var positn=$('.grid').find('.table-body').find(':checkbox').filter(':checked').first().parents('tr').find('td:eq(18)').find('span').attr('title');//入库的基地仓库
				var chkValue = [];
				checkboxList.filter(':checked').each(function(i){
					if($(this).parents('tr').find('td:eq(17)').find('span').attr('title') == 'Y'){
						flag = false;
						return false;
					}else if (Number($(this).parents('tr').find('td:eq(12)').find('span').attr('title'))==0 || Number($(this).parents('tr').find('td:eq(13)').find('span').attr('title'))==0) {
						flag1 = false;
						return false;
					}else if(positnex != $(this).parents('tr').find('td:eq(4)').find('span').attr('title')){
						flag2 = false;
						return false;
					}else if(positn != $(this).parents('tr').find('td:eq(18)').find('span').attr('title')){
						flag3 = false;
						return false;
					}
					chkValue.push($(this).val());
				});
				if(!flag){
					alert('<fmt:message key="Has_not_been_put_into_storage" />！');
					return;
				}
				if(!flag1){
					alert('<fmt:message key="The_number_of_storage_standard_or_reference_number_is_not_0" />！');
					return;
				}
				if (!flag2){   
					alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
					return;
				}
				if (!flag3){   
					alert('<fmt:message key="Can_only_choose_a_single_storage_position" />！');
					return;
				}
				
				$.ajax({
					type: "POST",
					url: "<%=path%>/explan/findById.do",
					data: "positn="+positnex,
					dataType: "json",
					success:function(deliver){
						if(null == deliver){
							alert('<fmt:message key="selected_workshop_supplier_not_set"/>！');
						}else{
							if(confirm('<fmt:message key="only_checked_option_be_put_in_storage_whether_to_continue"/>!')){
								var action = '<%=path%>/explan/saveChkinm.do?ids='+chkValue.join(",");
								$('body').window({
									title: '<fmt:message key="warehousing_scheduling_information"/>',
									content: '<iframe frameborder="0" src='+action+'></iframe>',
									width: 500,
									height: '245px',
									draggable: true,
									isModal: true
								});
							}
						}
					}
				});
			}
			
			//报货
			function chkstom(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() <= 0){ 
					alert('<fmt:message key="please_select_materials_to_generate_reports_manifested"/>！');
					return;
				}
				var chkValue = [];
				var idChkValue = [];
				var ary=[];
				var flag=true;
				var flag1 = true;//已经完成的不能再报货了 2014.11.6 wjf
				checkboxList.filter(':checked').each(function(i){
					idChkValue.push($(this).val());
					if($(this).parents('tr').find('td:eq(15)').find('span').attr('title') == "N"){
						chkValue.push($(this).parents('tr').find('td:eq(5)').find('span').attr('title'));
					}
					ary.push($(this).parents('tr').find('td:eq(4)').find('span').attr('title'));
					if($(this).parents('tr').find('td:eq(4)').find('span').attr('title')==''){
						flag=false;
					}
					var sta=$(this).parents('tr').find('td:eq(3)').find('span').attr('title');
					if (sta=='完成'){
						flag1=false;
					}
				});
				if(!flag1){
					alert('<fmt:message key="Can_not_continue_to_report_the_goods" />！');
					return;
				}
				if (!flag) {
					alert('<fmt:message key="Production_workshop_is_not_between_processing" />！');
					return;
				}
				var nary=ary.sort();   
				if (nary[0]!=nary[ary.length-1]){   
					alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
					return;
				}  
				var ids=chkValue.join(",");
				$('body').window({
					id: 'window_chkstomexplan',
					title: '<fmt:message key="processing_between_processing_materials_to_manifest_data"/>',
					content: '<iframe id="chkstomFrame" name="chkstomFrame" frameborder="0" src="<%=path%>/explan/toChkstom.do?code='+nary[0]+'&itemIds='+ids+'&idList='+idChkValue.join(",")+'"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="confirmation_modify_and_generate_reports_manifest" />',
								title: '<fmt:message key="confirmation_modify_and_generate_reports_manifest"/>',
							//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-40px','-40px']
								},
								handler: function(){
									window.frames["chkstomFrame"].saveChkstom();
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel"/>',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									position: ['-60px','0px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}	
			//报货---报货
			function updateExplanByPlan(){
				$('body').window({
					id: 'window_updateExplanByPlan',
					title: '<fmt:message key="Branch_processing_product_purchase_inquiry" />',
					content: '<iframe id="chkstomFrame" frameborder="0" src="<%=path%>/explan/updateExplanByPlan.do?action=init&deal=0"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true
				});
			}
			//安全库存---计划
			function safeCntToPlan(){
				$('body').window({
					id: 'window_safeCntToPlan',
					title: '<fmt:message key="Safety_stock_to_plan" />',
					content: '<iframe id="chkstomFrame" frameborder="0" src="<%=path%>/explan/safeCntToPlan.do?action=init"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true
				});
			}
		    	function pageReload(par){
			    	$('#listForm').submit();
		    	}
		    
		    	// 鼠标离开时检查数量输入格式是否合法
			function validate(e){
				if(Number(e.value)<0){
					alert('<fmt:message key="number_cannot_be_negative"/>');
					e.value=e.defaultValue;
					e.focus();
					return;
				}else if(isNaN(e.value)){
					alert('<fmt:message key="number_be_not_valid_number"/>！');
					e.value=e.defaultValue;
					e.focus();
					return;
				};
		    	}
		    	
		    	//保存需加工量
		    	function addCnt(e,f){
		    		if(e.defaultValue!=e.value){
						if(parent.jhTrlist){
							if(parent.jhTrlist[f]){
								parent.jhTrlist[f]['cnt']=e.value;
							}else{
								parent.jhTrlist[f]={};
								parent.jhChangeNum = parent.jhChangeNum+1;
								$("#jhChangeNum").text(parent.jhChangeNum);
								parent.jhTrlist[f]['cnt']=e.value;
							}
						}else{
							parent.jhTrlist = {};
							parent.jhTrlist[f]={};
							parent.jhChangeNum = 1;
							$("#jhChangeNum").text(1);
							parent.jhTrlist[f]['cnt']=e.value;
						}
		    		}
		    	}
		    	
		    	//保存产后登记 标准数量
		    	function addCntact(e,f){
		    		var unitper = $(e).next().val();
		    		if(e.defaultValue!=e.value){
					if(parent.djTrlist){
						if(parent.djTrlist[f]){
							parent.djTrlist[f]['cntact']=e.value;
							parent.djTrlist[f]['cntact1']=e.value*unitper;
						}else{
							parent.djTrlist[f]={};
							parent.djChangeNum = parent.djChangeNum+1;
							$("#djChangeNum").text(parent.djChangeNum);
							parent.djTrlist[f]['cntact']=e.value;
							parent.djTrlist[f]['cntact1']=e.value*unitper;
						}
					}else{
						parent.djTrlist = {};
						parent.djTrlist[f]={};
						parent.djChangeNum = 1;
						$("#djChangeNum").text(1);
						parent.djTrlist[f]['cntact']=e.value;
						parent.djTrlist[f]['cntact1']=e.value*unitper;
					}
					//alert(e.value+'-----'+unitper);
					$(e).closest("td").next().find('input:eq(0)').val(Number(e.value*unitper).toFixed(2));
		    		}
		    	}
		    	
		    	//保存产后登记 参考数量
		    	function addCntact1(e,f){
		    		var unitper = $(e).next().val();
		    		if(e.defaultValue!=e.value){
						if(parent.djTrlist){
							if(parent.djTrlist[f]){
								parent.djTrlist[f]['cntact1']=e.value;
								if(Number(unitper) != 0){
									parent.djTrlist[f]['cntact']=Number(e.value/unitper).toFixed(2);
								}
							}else{
								parent.djTrlist[f]={};
								parent.djChangeNum = parent.djChangeNum+1;
								$("#djChangeNum").text(parent.djChangeNum);
								parent.djTrlist[f]['cntact1']=e.value;
								if(Number(unitper) != 0){
									parent.djTrlist[f]['cntact']=Number(e.value/unitper).toFixed(2);
								}
							}
						}else{
							parent.djTrlist = {};
							parent.djTrlist[f]={};
							parent.djChangeNum = 1;
							$("#djChangeNum").text(1);
							parent.djTrlist[f]['cntact1']=e.value;
							if(Number(unitper) != 0){
								parent.djTrlist[f]['cntact']=Number(e.value/unitper).toFixed(2);
							}
						}
						//alert(e.value+'-----'+unitper);
						if(Number(unitper) != 0){
							$(e).closest("td").prev().find('input:eq(0)').val(Number(e.value/unitper).toFixed(2));
						}
		    		}
		    	}
		    	
			var totalCount;
			var condition;
			var currPage;
			function addTr(check){
				if(check=='first'){
					totalCount = '${totalCount}';
					condition = ${explanJson};
					var supply = condition.supply;
					condition.supply=undefined;
					//对象套对象得专门处理，否则报错
					for(var i in supply){
						if(supply[i]){
							condition['supply.'+i]=supply[i];	
						}
					}
					currPage= 1;
					
					condition['dat'] = new Date(condition.dat.time).format("yyyy-mm-dd");
					condition['datEnd'] = new Date(condition.datEnd.time).format("yyyy-mm-dd");
					condition['curStatus'] = '${curStatus}';
					condition['totalCount'] = totalCount;
					condition['currPage']=1;
					$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
					$("#currState").width(Number('${currState}')*200);
					return;
				}
				$.post("<%=path%>/explan/listAjax.do",condition,function(data){
					var rs = eval('('+data+')');
					//var rs = data;
					$("#per").text((rs.currState*100).toFixed(0)+'%');
					$("#currState").width(""+rs.currState*200+"px");
					//不是最后一页
					var num = rs.currPage*pageSize;
					var explanList = rs.explanList;
					for(var i in explanList){
						var explan = explanList[i];
						
						var tr = '<tr ';
						tr = tr + '>';
						tr = tr + '<td class="num" ><span style="width:30px;">'+ ++num +'</span></td>';
						tr = tr + '<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_'+explan.id+'" value="'+explan.id+'"/></span></td>';
						tr = tr + '<td><span title="'+explan.exno+'" style="width:40px;text-align: right;">'+explan.exno+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.sta+'" style="width:40px;">'+explan.sta+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.supply.positnex+'" style="width:90px;">'+explan.supply.positnexdes+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.supply.sp_code+'" style="width:70px;">'+explan.supply.sp_code+'&nbsp;</span></td>';		
						tr = tr + '<td><span title="'+explan.supply.sp_name+'" style="width:80px;">'+explan.supply.sp_name+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.supply.unit+'" style="width:30px;">'+explan.supply.unit+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.cntplan+'" style="width:60px;text-align: right;">'+explan.cntplan+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.cntkc+'" style="width:60px;text-align: right;">'+explan.cntkc+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.cntplan1+'" style="width:60px;text-align: right;">'+explan.cntplan1+'&nbsp;</span></td>';
						tr = tr + '<td class="textInput"><span title="'+explan.cnt+'" style="width:70px;padding: 0px;text-align: right;"><input type="text" value="'+explan.cnt+'" onfocus="this.select()" onblur="validate(this);addCnt(this,'+explan.id+')" style="width:65px;padding: 0px;text-align: right;"/></span></td>';
						tr = tr + '<td class="textInput"><span title="'+explan.cntact+'" style="width:70px;padding: 0px;text-align: right;"><input type="text" value="'+explan.cntact+'" onfocus="this.select()" onblur="validate(this);addCntact(this,'+explan.id+')" style="width:65px;padding: 0px;text-align: right;"/></span></td>';
						tr = tr + '<td class="textInput"><span title="'+explan.cntact1+'" style="width:70px;padding: 0px;text-align: right;"><input type="text" value="'+explan.cntact1+'" onfocus="this.select()" onblur="validate(this);addCntact1(this,'+explan.id+')" style="width:65px;padding: 0px;text-align: right;"/></span></td>';	
						tr = tr + '<td><span title="'+explan.supply.unit+'" style="width:60px;">'+explan.supply.unit+'&nbsp;</span></td>';		
						tr = tr + '<td><span title="'+explan.ynex+'" style="width:60px;">'+explan.ynex+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.ynsto+'" style="width:60px;">'+explan.ynsto+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.ynin+'" style="width:60px;">'+explan.ynin+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.supply.positndes+'" style="width:60px;">'+explan.supply.positndes+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.btim+'" style="width:130px;">'+explan.btim+'&nbsp;</span></td>';
						tr = tr + '<td><span title="'+explan.etim+'" style="width:130px;">'+explan.etim+'&nbsp;</span></td>';
						tr = tr + '</tr>';	
						$(".grid .table-body tbody").append($(tr));
					}
					if(rs.over!='over'){
						condition['currPage']=++currPage;
						returnInfo = true;
					}
					if(parent.ddzxEditState!='edit'){
						$('tbody input[type="text"]').attr('disabled',true);//不可编辑
					}else{
						new tabTableInput("table-body","text"); //input  上下左右移动
					}
				});
			}
			
			Date.prototype.format = function(){	
				var yy = String(this.getFullYear());
				var mm = String(this.getMonth() + 1);
				var dd = String(this.getDate());
				if(mm<10){
					mm = ''+0+mm;
				}
				if(dd<10){
					dd = ''+0+dd;
				}
				var str = yy+"-"+mm+"-"+dd;
				return str;
			};
		</script>
	</body>
</html>