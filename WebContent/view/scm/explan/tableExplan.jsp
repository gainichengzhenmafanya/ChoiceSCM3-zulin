<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Explan Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>		
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/explanM/saveExplan.do" method="post">
		<div class="bj_head">			 	
		<!-- 加入隐藏内容 -->
		<%-- curStatus 0:init;1:edit;2:add;3:show --%>
		<input type="hidden" id="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		
		<div class="form-line">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/>：</div>
			<div class="form-input" style="width:220px;">
				<input autocomplete="off" type="text" id="maded" name="maded" style="width:224px;" class="Wdate text" value="<fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/>"/>
			</div>
			<div class="form-label" ><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${explanM.explanno!=null}"><c:out value="${explanM.explanno}"></c:out></c:if>
				<input type="hidden" name="explanno" id="explanno" value="${explanM.explanno }"/>				
			</div>												
		</div>
		<div class="form-line" style="z-index:12">	
			<div class="form-label"><fmt:message key="Finish_date" />：</div>
			<div class="form-input" style="width:230px;">
				<input autocomplete="off" type="text" id="hoped" name="hoped" style="width:224px;" class="Wdate text" value="<fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd"/>"/>
			</div>
			<div class="form-label"><fmt:message key="orders_maker"/>:</div>
			<div class="form-input" style="width:220px;">
				<c:if test="${explanM.madeby!=null}"><c:out value="${explanM.madeby}"></c:out></c:if>
				<input type="hidden" name="madeby" id="madeby" class="text" value="${explanM.madeby}" />			
			</div>		
		</div>
		<div class="form-line" style="z-index:12">	
			<div class="form-label"><fmt:message key="processing_room"/>：</div>
			<div class="form-input" style="width:240px;">
				<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${explanM.positn.code}"/>
				<input type="hidden" class="text"  id="positnDes" />
				<input type="hidden" class="text"  id="typ" name="typ" value="A"/>
				<select class="select" id="positn" name="positn.code" style="width:180px;" onchange="findPositnByDes(this);">
					<c:forEach var="positn" items="${listPositn}" varStatus="status">
						<option 
							<c:if test="${positn.code==explanM.positn.code}"> 
									   	selected="selected"
							</c:if> 
							id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
						</option>
					</c:forEach>
				</select>
			</div>
<!-- 			<div class="form-label" style="width:100px;">审核人:</div> -->
<!-- 			<div class="form-input"> -->
<%-- 				<c:if test="${explanM.checby!=null}"><c:out value="${explanM.checby}"></c:out></c:if> --%>
<%-- 				<input type="hidden" name="checby" id="checby" class="text" value="${explanM.checby}" />			 --%>
<!-- 			</div>						 -->
				
				<div class="form-label"><fmt:message key="please_select_materials"/>:</div>
				<div class="form-input" style="width:240px;">
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
		</div>
		</div>
			<div class="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td colspan="1">&nbsp;</td>
 								<td colspan="3"><fmt:message key="supplies"/></td>
								<td colspan="2"><fmt:message key="standard_unit"/></td>
<%-- 								<td colspan="2"><fmt:message key="reference_unit"/></td> --%>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>
								<td class="num"><span style="width: 16px;">&nbsp;</span></td>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:190px;"><fmt:message key="name"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
 								<td><span style="width:60px;"><fmt:message key="quantity"/></span></td>
<%-- 								<td><span style="width:40px;"><fmt:message key="unit"/></span></td> --%>
<%--  								<td><span style="width:80px;"><fmt:message key="quantity"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<c:forEach var="explanD"  items="${explanM.explanDList}" varStatus="status" >
								<tr>
									<td class="num"><span style="width: 16px;">${status.index+1}</span></td>
									<td><span style="width:70px;">${explanD.supply.sp_code}</span></td>
									<td><span style="width:190px;">${explanD.supply.sp_name}</span></td>
	 								<td><span style="width:60px;">${explanD.supply.sp_desc}</span></td>
									<td><span style="width:40px;">${explanD.supply.unit}</span></td>
	 								<td><span style="width:60px;text-align:right;">${explanD.amount}</span></td>
	 								<td><span style="width:90px;">${explanD.memo}</span></td>
								</tr>
								<c:set var="sum_num" value="${status.index+1}"/>  
								<c:set var="sum_amount" value="${sum_amount + explanD.amount}"/>  
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div style="height: 10px">	
				<table cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height: 10px">
					<thead>
						<tr>
							<td style="width: 25px;">&nbsp;</td>
							<td style="width:80px;"><fmt:message key="total"/>:</td>
							<td style="width:170px;"><fmt:message key="material_number"/>：<u>&nbsp;&nbsp;<label id="sum_num">${sum_num}</label>&nbsp;&nbsp;</u></td>
							<td style="width:180px;"><fmt:message key="total_number"/>：<u>&nbsp;&nbsp;<label id="sum_amount">${sum_amount}</label>&nbsp;&nbsp;</u></td>
						</tr>
					</thead>
				</table>
		   </div>	
		</form>
		<form id="reportForm" method="post">
		<input type="hidden" id="explanno" name="explanno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/Chkin.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
			var validate;
			var first=false;
			$(document).ready(function(){
				focus() ;//页面获得焦点
				$("#positn_select").val($("#positn").val());
				$('#positn_select').bind('keyup',function(){
			          $("#positn").find('option')
			                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
			                    .attr('selected','selected');
			       });
			 	$(document).bind('keyup',function(e){
			 		if($(e.srcElement).is("input")){//对表格内的输入框进行判读，延迟600毫秒
			 			var index=$(e.srcElement).closest('td').index();
			    		if(index=="5"){
			    			$(e.srcElement).unbind('blur').blur(function(e){
				 				validateByMincnt($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
				 			});
				    		validator($(e.srcElement).closest('td').index(),$(e.srcElement).closest('tr'),{value:$(e.srcElement).val(),ovalue:$(e.srcElement).data("ovalue")});
			    		}
			    	}
			 		if(e.keyCode==27){
			 			$('.<fmt:message key="quit"/>').click();
			 		}
			 		if(e.altKey ==false)return;
			 		switch (e.keyCode)
		            {
		                case 70: $('#autoId-button-101').click(); break;
		                case 65: $('#autoId-button-102').click(); break;
		                case 68: $('#autoId-button-103').click(); break;
		                case 69: $('#autoId-button-104').click(); break;
						case 80: $('#autoId-button-105').click(); break;
		                case 83: $('#autoId-button-106').click(); break;
		                case 67: $('#autoId-button-107').click(); break;
		            }
				}); 
			 	
			 	$('#maded').bind('click',function(){
					new WdatePicker();
				});
			 	$('#hoped').bind('click',function(){
					new WdatePicker();
				});
			 	
			 	//回车换焦点start
				    var array = new Array();        
			 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
			 	    array = ['maded','positn_select', 'positn'];        
			 		//定义加载后定位在第一个输入框上          
			 		$('#'+array[2]).focus();            
			 		$('select,input[type="text"]').keydown(function(e) {                  
				 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
				 		var event = $.event.fix(e);                
				 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
				 		if (event.keyCode == 13) {                
				 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
				 				if(index==6 && $('#'+array[index+1]).attr('disabled')=='disabled'){
				 					++index;
				 				}
				 				$('#'+array[++index]).focus();
				 				if(index==3){
				 					$('#positn_select').val($('#positn').val());
				 				} 
				 				if(index==8){
				 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
				 				} 
				 		}
			 		});    
		 		//回车换焦点end
				//新增
				var status = $("#curStatus").val();
				if(status == 'add')setEditable();
				
				<%-- curStatus 0:init;1:edit;2:add;3:show --%>
				//判断按钮的显示与隐藏
				if(status == 'add'){
					loadToolBar([true,true,false,false,false,true,false,false]);
				}else if(status == 'show'){//查询页面双击返回
					loadToolBar([true,true,true,true,true,false,true,true]);
				}else{//init
					loadToolBar([true,true,false,false,false,false,false,false]);
					$('#positn').attr("disabled","disabled");
					$('#positn_select').attr("disabled","disabled");
					$('#maded').attr("disabled","disabled");
				}
			
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'maded',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="date_of_the_system_alone"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'explanno',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="orders_num"/><fmt:message key="cannot_be_empty"/>！']
					},{
						type:'text',
						validateObj:'positn',
						validateType:['canNull'],
						param:['F'],
						error:['<fmt:message key="purchase_positions"/><fmt:message key="cannot_be_empty"/>！']
					}]
				});
				$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				
				<c:if test="${tableFrom=='table'}">
					$('#autoId-button-101').click();
				</c:if>
			});
			
			function findPositnByDes(inputObj){
		 		$('#positn_select').val($(inputObj).val());
	        }
			
		 	//查找物资 jinshuai 多选
		 	$('#seachSupply').bind('click.custom',function(e){
		 		if($("#curStatus").val() != "edit" && $("#curStatus").val() != "add"){//必须是新增或者编辑的情况下才能选择物资  2015.1.2wjf
		 			alert('<fmt:message key="must_add_or_edit_can_select"/>！');
		 			return;
		 		}
		 		if($('#firmDes').val()==''){
					showMessage({
							type: 'error',
							msg: '<fmt:message key ="please_select" /> <fmt:message key ="daily_cargo_space" />！',
							speed: 1000
						});
				}else{
			 		if(!!!top.customWindow){
			 			top.customSupply('<fmt:message key="please_select_materials" />',encodeURI('<%=path%>/supply/selectSupplyLeft.do?single=false&ex1=Y&positnex='+$('#positn_select').val()+'&sp_position='+$('#firm').val()),$('#sp_code'),null,$('#unit1'),$('.unit'),$('.unit1'),null,handler2);
					}
				}
			});
		 	function handler2(sp_codes){
		 		var codes = '';//存放页面上的编码，用来判断是否存在
		 		$('.grid').find('.table-body').find('tr').each(function (){
		 			codes += $(this).find('td:eq(1)').text()+",";
				});
		 		var sp_code;
				var sp_code_arry = new Array();
		 		sp_code_arry = sp_codes.split(",");
				
				for(var i=0; i<sp_code_arry.length; i++){
					sp_code = sp_code_arry[i];
					if(codes.indexOf(sp_code) != -1 ){//如果已存在，则继续下次循环
						continue;
					}

					$.ajax({
						type: "POST",
						url: "<%=path%>/supply/findById.do",
						data: "sp_code="+sp_code,
						dataType: "json",
						success:function(supply){
							if(!$(".table-body").find("tr:last").find("td:eq(1)").find('span').text()==''){
								$.fn.autoGrid.addRow();
							}
							$(".table-body").find("tr:last").find("td:eq(1)").find('span').text(supply.sp_code).data('sp_name',supply.sp_name);
							$(".table-body").find("tr:last").find("td:eq(2)").find('span').text(supply.sp_name);
							$(".table-body").find("tr:last").find("td:eq(3)").find('span').text(supply.sp_desc);
							$(".table-body").find("tr:last").find("td:eq(4)").find('span').text(supply.unit);
							$(".table-body").find("tr:last").find("td:eq(5)").find('span').text(0).css("text-align","right");
							$(".table-body").find("tr:last").find("td:eq(6)").find('span').text('');
						}
					});		
				}
		 	}
		 	
			function loadToolBar(use){
				$('.tool').html('');
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />(<u>F</u>)',
							title: '<fmt:message key="select"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','-40px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								searchExplan();
							}
						},{
							text: '<fmt:message key="insert" />(<u>A</u>)',
							title: '<fmt:message key="insert"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['0px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								window.location.replace("<%=path%>/explanM/add.do");
							}
						},{
							text: '<fmt:message key="delete" />(<u>D</u>)',
							title: '<fmt:message key="delete"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								deleteExplanM();
							}
						},{
							text: '<fmt:message key="edit" />(<u>E</u>)',
							title: '<fmt:message key="update"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-18px','0px']
							},
							handler: function(){
								var status = $('#curStatus').val();
								if(status == 'init')return;
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？'))return;
								$("#curStatus").val("edit");
								loadToolBar([true,true,true,false,true,true,false]);
								setEditable();
								$('#positn_select').focus();    
							}
						},{
							text: '<fmt:message key="print" />(<u>P</u>)',
							title: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[4],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-140px','-100px']
							},
							handler: function(){
								printChkinm();
							}
						},{
							text: '<fmt:message key="save" />(<u>S</u>)',
							title: '<fmt:message key="save"/>',
							useable: use[5],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if($("#curStatus").val()=='add' || $("#curStatus").val()=='edit'){
									if(validate._submitValidate()){
										saveOrUpdateExplan("0");
									}
								}else{
									alert('<fmt:message key="no_edited_documents_to_be_saved"/>！');
								}
								
							}
						},{
							text: '<fmt:message key="check"/>',
							title: '<fmt:message key="check"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')}&&use[6],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','-240px']
							},
							handler: function(){
								saveOrUpdateExplan("1");
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								var status = $("#curStatus").val();
								if(status == 'add' || status == 'edit')
									if(!confirm('<fmt:message key="data_unsaved_whether_to_exit"/>？'))return;
								invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
							}
						}
					]
				});
			}	
			
			function openExplanM(explanno){
				window.location.replace("<%=path%>/explanM/update.do?explanno="+explanno);
			}
			
			function setEditable(){
				if($('#curStatus').val()=='add'){
					$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
				}
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:7,
					VerifyEdit:{verify:true,enable:function(cell,row){
						return row.find('td').index(cell) == 2 || (row.find("td:eq(2)").text() == null || $.trim(row.find("td:eq(2)").text()) != '' ) ? true : false;
					}},
					widths:[26,80,200,70,50,70,100],
					colStyle:['','','','','',{background:"#F1F1F1"},''],
					editable:[2,5,6],
					onLastClick:function(row){
						$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
						$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(5)').text());//总数量
					},
					onEnter:function(data){
						if(data.curobj.closest('tr').find('td').index(data.curobj.closest('td')) == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.closest('tr'),2);
								return;
							}
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.ovalue) ;
						function getName(){
							var name;
							$.ajaxSetup({ 
								  async: false
								  });
							$.get("<%=path %>/supply/findById.do",
									{sp_code:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										name =  data.sp_name;
									});
							return name;
						};
					},
					cellAction:[{
						index:2,
						action:function(row){
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							var sp_position = $("#positn_select").val();
							data['url'] = '<%=path%>/supply/findExTop10.do?ex1=Y';
							if(!isNaN(data.value))
								data['key'] = 'sp_code';
							else
								data['key'] = 'sp_init';
							data.positn = $('#positn').val();
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.sp_code+'-'+data.sp_init+'-'+data.sp_name;
						},
						afterEnter:function(data,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="added_supplies_remind"/>！',
	 								speed: 1000
	 							});
							}
							
							row.find("td:eq(1) span").text(data.sp_code);
							row.find("td:eq(2) span input").val(data.sp_name).focus();
							row.find("td:eq(3)").find('span').text(data.sp_desc);
							row.find("td:eq(4)").find('span').text(data.unit);
							row.find("td:eq(5)").find('span').text(0).css("text-align","right");
							row.find("td:eq(6)").find('span').text('');
						}
					},{
						index:5,
						action:function(row,data2){
							if(isNaN(data2.value)||Number(data2.value) <= 0){
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								alert('<fmt:message key ="_the_number" />！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								$.fn.autoGrid.setCellEditable(row,6);
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(!row.next().html())
								$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text($('.table-body').find('tr').size());//总行数
						}
					}]
				});
			}
			function searchExplan(){
				var curwindow = $('body').window({
					id: 'window_searchChkinm',
					title: '<fmt:message key="Query_processing" />',
					content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/explanM/list.do?action=init"></iframe>',
					width: 780,
					height: 480,
					draggable: true,
					isModal: true
				});
				curwindow.max();
			}
									
			//增加 或者 修改
			function saveOrUpdateExplan(typ){
				var selected = {};
				selected['status'] = typ;
				selected['maded'] = $('#maded').val();
				selected['hoped'] = $('#hoped').val();
				selected['madeby'] = $('#madeby').val();
				selected['explanno'] = $('#explanno').val();
				selected['positn.code'] = $('#positn').val();
				selected['checby'] = $('#checby').val();
// 				selected['memo'] = $('#memo').val();
				//table 数组
				var numNull=0;//默认0代表成功，1代表0值，2代表非数字
				var isNull=0;//空单据				
				$('.table-body').find('tr').each(function(i){
					if($(this).find('td:eq(1)').find('span').html()!=''){
						isNull=1;
						var amount=$(this).find('td:eq(5) span input').val() ? $(this).find('td:eq(5) span input').val() : $(this).find('td:eq(5)').text();
						if(amount===0 || amount==0.0 ||amount==0.00 ){
							numNull=1;
						}
						if(amount=="" || amount==null || isNaN(amount)){
							numNull=2;//2代表数量为空或者非数字
						}
						selected['explanDList['+i+'].supply.sp_code'] = $(this).find('td:eq(1) span input').val() ? $(this).find('td:eq(1) span input').val() : $(this).find('td:eq(1)').text();
						selected['explanDList['+i+'].supply.sp_name'] = $(this).find('td:eq(2) span input').val() ? $(this).find('td:eq(2) span input').val() : $(this).find('td:eq(2)').text();
						selected['explanDList['+i+'].supply.sp_desc'] = $(this).find('td:eq(3) span input').val() ? $(this).find('td:eq(3) span input').val() : $(this).find('td:eq(3)').text();
						selected['explanDList['+i+'].supply.unit'] = $(this).find('td:eq(4) span input').text() ? $(this).find('td:eq(4) span input').text() : $(this).find('td:eq(4)').text();//单位
						selected['explanDList['+i+'].amount'] = amount;//数量
						//wangjie 2014年11月4日 09:51:56
						var memo=$(this).find('td:eq(6) span input').val() ? $(this).find('td:eq(6) span input').val() : $(this).find('td:eq(6)').text();
						selected['explanDList['+i+'].memo'] = memo;//备注
					}
				});
				if(Number(isNull)==0){
					alert('<fmt:message key="empty_document_unallowed_please_select_supplies"/>！');
					return;
				}
				if(Number(numNull)==1){//数量不为0
					alert('<fmt:message key="number_cannot_be_zero"/>！');
					return;
				}
				if(Number(numNull)==2){//数量为空或字母
					alert('<fmt:message key ="number_be_not_valid_number" />！');
					return;
				}
				$.post('<%=path%>/explanM/updateExplanM.do',selected,function(data){
					var rs = eval('('+data+')');
					//判断状态
					var status = $("#curStatus").val();
					var state='<fmt:message key="Modification_processing" />';
					if(status=="add"){
						state='<fmt:message key="Add_processing" />';
// 						loadToolBar([true,true,true,true,true,false,true,true]);//点保存后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
					}
					if(rs=="1"){
						var type= typ=='1'?'<fmt:message key ="check" />':'<fmt:message key ="save" />';
						showMessage({
							type: 'success',
							msg: type+' <fmt:message key="successful"/>！',//修改提示为成功， wjf 
							speed: 3000
						});	
// 						loadToolBar([true,true,true,true,true,false,true,true]);//点保存成功后把按钮状态改为 show
						$("#curStatus").val('show');    //修改状态
						if(typ=='1'){
							loadToolBar([true,true,false,false,true,false,false,false]);
						}else{
							loadToolBar([true,true,true,true,true,false,true,true]);
						}
					}else{
						showMessage({
							type: 'error',
							msg: state+'<fmt:message key="failure"/>！',
							speed: 1000
						});	
					}
				});
// 				if(typ=='1'){
// 					loadToolBar([true,true,false,false,false,false,false,false]);
// 				}else{
// 					loadToolBar([true,true,false,false,false,false,true,true]);
// 				}
			}
			//删除
			function deleteExplanM(){
				if(!confirm('<fmt:message key="Confirm_deletion_processing_Dan_Ma" />？'))return;
				//检测要保存的单据是否已被审核
				var action = '<%=path%>/explanM/delete.do?action=init&&explannoids='+$('#explanno').val();
				$('#listForm').attr('action',action);
				$('#listForm').submit();
			}
			//打印
			function printChkinm(){
				var explanno = $('#listForm').find('#explanno').val();
				//$('#reportForm').find('#chkinno').attr('value',$('#listForm').find('#explanno').val());
				$('#reportForm').attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0'); 
				var action = '<%=path%>/explanM/viewExplanM.do?checkno='+explanno;	
				$('#reportForm').attr('action',action);
				$('#reportForm').submit();
				$('#reportForm').attr('target','');
			}
			function pageReload(par){
		    	$('#listForm').submit();
	    	}
			
			function validator(index,row,data2){//输入框验证
				if(index=="5"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						data2.value=0;
					}
					$('#sum_amount').text((Number($('#sum_amount').text())+Number(data2.value)-data2.ovalue).toFixed(2));//总数量
					row.find("input").data("ovalue",data2.value);
				
				}
			}
			
			function validateByMincnt(index,row,data2){//最小申购量判断
				if(index=="5"){
					if(isNaN(data2.value)||Number(data2.value) <= 0){
						alert('<fmt:message key="please_enter_positive_integer"/>！');
						row.find("input").focus();
					}
				}
			}
		</script>
	</body>
</html>
