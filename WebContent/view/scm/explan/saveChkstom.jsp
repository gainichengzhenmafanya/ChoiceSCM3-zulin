<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="generated_newspaper_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css"> 	
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
		</style>
	</head>
	<body>  
		<form id="listForm" method="post" action="<%=path %>/explan/saveChkstom.do">
			<input type="hidden" class="text" id="idList" name="idList" value="${idList}" />
<%-- 		<div class="form-label"><fmt:message key="reported_that_trucks_between"/>：${positn.des}</div> --%>
		<div class="form-input">
			<input type="hidden" class="text" id="positn" name="positn" value="${positn.code}" />
		</div>		
 			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td rowspan="2" style="width:30px;">
									<input type="checkbox" id="chkAll" checked="checked" />
								</td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="name"/></span></td>
								<td rowspan="2"><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td colspan="3"><fmt:message key ="standard_unit" /></td>
								<td colspan="2"><fmt:message key ="procurement_unit" /></td>
								<td rowspan="2" style="width:70px;"><fmt:message key="remark"/></td>
							</tr>
							<tr>
								<td style="width:40px;"><fmt:message key="unit"/></td>
								<td style="width:70px;"><fmt:message key="computation"/></td>
								<td style="width:70px;"><fmt:message key="adjustment_amount"/></td>
								<td style="width:40px;"><fmt:message key="unit"/></td>
								<td style="width:70px;"><fmt:message key="adjustment_amount"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="product" items="${productList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 25px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" checked="checked" name="idList" id="chk_${product.item}" value="${product.item}"/>
									</td>
									<td><span title="${product.supply.sp_code}" style="width:70px;text-align: left;" sp_price="${product.supply.sp_price }">${product.supply.sp_code}&nbsp;</span></td>
									<td><span title="${product.supply.sp_name}" style="width:90px;text-align: left;">${product.supply.sp_name}&nbsp;</span></td>
									<td><span title="${product.supply.sp_desc}" style="width:70px;text-align: right;">${product.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${product.supply.unit}" style="width:30px;text-align: left;">${product.supply.unit}&nbsp;</span></td>
									<td class="textInput">
										<span title="" style="width:70px;padding: 0px;text-align: right;">
											<fmt:formatNumber value="${product.cnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>
										</span>
									</td>
									<td class="textInput">
										<span title="" style="width:70px;padding: 0px;text-align: right;">
											<c:set var="cnt" value="${0}"/>
											<fmt:formatNumber var="cnt" value="${product.cnt}" pattern="##" minFractionDigits="0" />
											<input type="text" value="${cnt + 1 }" onfocus="this.select()" onblur="getAmt3(this,'${product.supply.unitper3}')" style="width:65px;text-align:right;"/>
										</span>
									</td>
									<td><span title="${product.supply.unit3}" style="width:30px;text-align: left;">${product.supply.unit3}&nbsp;</span></td>
									<td class="textInput">
										<span title="" style="width:70px;padding: 0px;text-align: right;">
											<c:set var="cnt3" value="${0}"/>
											<fmt:formatNumber var="cnt3" value="${product.cnt * product.supply.unitper3}" pattern="##" minFractionDigits="0" />
											<input type="text" value="${cnt3 + 1 }" onfocus="this.select()" onblur="getAmt(this,'${product.supply.unitper3}')" style="width:65px;text-align:right;"/>
										</span>
									</td>
									<td class="textInput">
										<span title="" style="width:70px;padding: 0px;">
											<input type="text" value="" onfocus="this.select()"/>
										</span>
									</td>
									<td style="display:none;"><span title="${product.supply.sp_price}" style="width:70px;text-align: left;">${product.supply.sp_price}&nbsp;</span></td>
									<td style="display:none;"><span title="${product.cnt1}">${product.cnt1}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),27);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			new tabTableInput("table-body","text"); //input  上下左右移动
		});
		//确定修改并生成报货单
		function saveChkstom(){ 
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
				if(confirm('<fmt:message key="only_checked_option_to_generate_reports_of_invoices_whether_to_continue"/>!')){
					var chkValue = [];
					selected['idList']=$('#idList').val();
					selected['firm']=$('#positn').val();
					var  sta=1;
					checkboxList.filter(':checked').each(function(i){
						if(0==$(this).parents('tr').find('td:eq(7)').find('input').val() || 0 == $(this).parents('tr').find('td:eq(9)').find('input').val()){
							return;
						}
						sta=0;
						chkValue.push($(this).val());
						selected['ProductList['+i+'].supply.sp_code'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
						selected['ProductList['+i+'].supply.sp_name'] = $(this).parents('tr').find('td:eq(3)').find('span').attr('title');
						selected['ProductList['+i+'].supply.sp_desc'] = $(this).parents('tr').find('td:eq(4)').find('span').attr('title');
						selected['ProductList['+i+'].supply.sp_price'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('sp_price');//取标准价，用来计算总价 wjf
						selected['ProductList['+i+'].sale'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('sp_price')*$(this).parents('tr').find('td:eq(7)').find('input').val();
						selected['ProductList['+i+'].cnt'] = $(this).parents('tr').find('td:eq(7)').find('input').val();
						selected['ProductList['+i+'].memo'] = $(this).parents('tr').find('td:eq(10)').find('input').val();//备注
						selected['ProductList['+i+'].cnt1'] = $(this).parents('tr').find('td:eq(9)').find('input').val();
					});
					if(sta==1){
						alert('<fmt:message key="Adjust_the_amount_can_not_be_0" />！');
						/* showMessage({
							type: 'error',
							msg: '<fmt:message key="adjustment_amount_be_zero_the_operation_failed"/>！',
							speed: 1000
						}); */	
						return;
					}
					selected['item'] = chkValue.join(",");
					var result='parent';
					$.post('<%=path%>/explan/saveChkstom.do',selected,function(data){
					 	//弹出提示信息
					 	if ("OK"!=data) {
					 		alert("<fmt:message key="The_operation_failed_please_check_the_default_vendor_settings" />！");
					 	}else{
					 		alert('<fmt:message key="operation_successful"/>！');
				    	}
						parent.pageReload(result);
					});
				}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return ;
			}
		}
		
		//计算数量1的值
		function getAmt3(e,f) {
			if(Number(f) == 0){
				return;
			}
			$(e).val(Math.ceil(e.value));
			$(e).parents('tr').find('td:eq(9)').find('input').val(Math.ceil(f*e.value));
		}
 		
		//计算数量的值
		function getAmt(e,f) {
			if(f==0 || f==0.0){
				return;
			}
			$(e).val(Math.ceil(e.value));
			$(e).parents('tr').find('td:eq(7)').find('input').val(Math.ceil(e.value/f));
		}
		</script>
	</body>
</html>