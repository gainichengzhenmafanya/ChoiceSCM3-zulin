<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>领料单列表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="listForm" name="listForm" action="<%=path%>/explanM/listLL.do" method="post">	
			<input  type="hidden" name="status" id="status" value="0" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input" ><input autocomplete="off" style="width:85px;" type="text" id="madedStart" name="madedStart" class="Wdate text" value="<fmt:formatDate value="${madedStart}" pattern="yyyy-MM-dd"/>" /></div>			
					<div class="form-label" ><fmt:message key="orders_num"/></div>
					<div class="form-input" ><input type="text" id="no" name="no" class="text" value="${explanSpcode.no}"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${explanSpcode.madeby}"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd"/>"/></div>
					<%-- <div class="form-label">审核人</div>
					<div class="form-input"><input type="text" id="checkby" name="checkby" class="text" value="<c:out value="${explanSpcode.checkby}" />"/></div> --%>
					<div class="form-label"><fmt:message key ="processing_room" /></div>
					<div class="form-input">
						<input type="text"  id="positndes"  name="positndes" readonly="readonly" value="${explanSpcode.positndes}"/>
						<input type="hidden" id="positn" name="positn" value="${explanSpcode.positn}"/>
						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="select"/><fmt:message key="processing_room"/>' />
					</div>
				</div>
			</form>
			<!-- wangjie  2014年10月27日 09:17:43 -->
<!-- 			<div id="div1" style="height:40%;overflow:auto;"> -->
				<div id="div1" style="height:40%;">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td><span style="width:100px;"><fmt:message key ="orders_num" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="Filling_in_the_date" /></span></td>
<!-- 									<td><span style="width:120px;">时间</span></td> -->
									<td><span style="width:100px;"><fmt:message key ="orders_maker" /></span></td>
<!-- 									<td><span style="width:100px;">是否审核</span></td> -->
<!-- 									<td><span style="width:100px;">审核人</span></td> -->
									<td><span style="width:100px;"><fmt:message key="The_supply_room" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="explanspcode" items="${explanSpcodeList}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${explanspcode.no}' />" value="${explanspcode.no}"/>
										</td>
										<td><span style="width:100px;" title="${explanspcode.no}">${explanspcode.no}</span></td>
										<td><span style="width:100px;" title="${explanspcode.dat}"><fmt:formatDate value="${explanspcode.dat}" pattern="yyyy-MM-dd"/></span></td>
<%-- 										<td><span style="width:120px;" title="${explanM.madet}">${explanM.madet}</span></td> --%>
										<td><span style="width:100px;" title="${explanspcode.madeby}">${explanspcode.madeby}</span></td>
										<%-- <td><span style="width:100px;" title="${explanspcode.chk}">
											<c:choose>
												<c:when test="${explanspcode.chk == 'Y'}">${explanspcode.chk}</c:when>
												<c:otherwise>N</c:otherwise>
											</c:choose>
										</span></td>
										<td><span style="width:100px;" title="${explanspcode.checkby}">${explanspcode.checkby}</span></td> --%>
										<td><span style="width:100px;" title="${explanspcode.positn}">${explanspcode.positndes}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<%-- <page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" /> --%>
		</div>
		<div class="mainFrame" style="height:50%;width:100%;overflow:auto;">
		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$('#div1'),0);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var maded = $('#listForm').find("#madedStart").val().toString();
						var madedEnd= $('#listForm').find("#madedEnd").val().toString(); 
							if(maded<=madedEnd)
								$('#listForm').submit();
							else
								alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});	
			//查询领料单详情
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			    	 return;
			         //$(this).find(":checkbox").attr("checked", false);
			     }else{
			         $('.bgBlue').removeClass("bgBlue");
			         $('.grid').find('.table-body').find('tr').find(":checkbox").attr("checked", false);
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			         var no=$(this).find(':checkbox').val();
					 var  url="<%=path%>/explanM/tableExplanSpcode.do?no="+no;
					 $('#mainFrame').attr('src',url);
				     window.mainFrame.location =url;
			     }
			 });
			//日期选择
			$('#madedStart').bind('click',function(){
				new WdatePicker();
			});
			$('#madedEnd').bind('click',function(){
				new WdatePicker();
			});
			//选择加工间
			$('#seachOnePositn').bind('click.custom',function(e){
				var positnCode=$('#positn').val();
				if(!!!top.customWindow){
					var offset = getOffset('seachOnePositn');
					top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnCode,offset,$('#positndes'),$('#positn'),null,null,null,'150','300','isNull');
				}
			});
		});
		</script>
	</body>
</html>