<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>加工单生成领料单主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<div style="height:30%;">
			<form id="listForm" action="<%=path%>/explanM/listExplanM.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td><span style="width:100px;"><fmt:message key ="orders_num" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="Filling_in_the_date" /></span></td>
									<td><span style="width:120px;"><fmt:message key ="time" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="orders_maker" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="orders_audit" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="processing_room" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="documenttype" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="scm_hope_date" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="explanM" items="${listExplanM}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_<c:out value='${explanM.explanno}' />" value="${explanM.explanno}"/>
										</td>
										<td><span style="width:100px;" title="${explanM.explanno}">${explanM.explanno}</span></td>
										<td><span style="width:100px;" title="${explanM.maded}"><fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/></span></td>
										<td><span style="width:120px;" title="${explanM.madet}">${explanM.madet}</span></td>
										<td><span style="width:100px;" title="${explanM.madeby}">${explanM.madeby}</span></td>
										<td><span style="width:100px;" title="${explanM.checby}">${explanM.checby}</span></td>
										<td><span style="width:100px;" title="${explanM.positn.code}">${explanM.positn.des}</span></td>
										<td><span style="width:100px;" title="${explanM.typ}"><c:if test="${explanM.typ==0}"><fmt:message key ="Fill_in" /></c:if>
											<c:if test="${explanM.typ==1}"><fmt:message key ="reports" /></c:if><c:if test="${explanM.typ==2}"><fmt:message key ="scm_safety_stock" /></c:if><c:if test="${explanM.typ==3}">领料</c:if></span></td>
										<td><span style="width:100px;" title="${explanM.hoped}"><fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			<div class="search-div" style="margin-left: 0px;">
			<div class="search-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left"><fmt:message key ="startdate" />：</td>
						<td>
							<input type="text" id="maded" name="maded" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${explanM.maded}' pattern='yyyy-MM-dd'/>"/>
						</td>
						<td class="c-left"><fmt:message key ="enddate" />：</td>
						<td>
							<input type="text" id="madedEnd" name="madedEnd" class="Wdate text" onfocus="WdatePicker();" value="<fmt:formatDate value='${madedEnd}' pattern='yyyy-MM-dd'/>"/>
						</td>
					</tr>
				</table>
			</div>
			<div class="search-commit">
				<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />' />
				<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />' />
			</div>
		</div>
			</form>
		</div>
		<div class="mainFrame" style="height:60%;width:100%">
		    <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),390);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key ="select" />',
					title: '<fmt:message key ="select" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$('.search-div').slideToggle(100);
					}
				},"-",{
						text: '<fmt:message key ="generate_material_requisition" />',
						title: '<fmt:message key ="generate_material_requisition" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()<1) {
								alert("<fmt:message key="Please_select_the_processing_operation" />！");
							}else{
								window.document.getElementById("mainFrame").contentWindow.chkstom();
								toChkstom();
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
			var win;
			function saveChkstoDemod(){
				win=$('body').window({
					id: 'window_saverole',
					title: '<fmt:message key ="insert" /><fmt:message key ="report_form_template" />',
					content: '<iframe id="saveChkstoDemodFrame" frameborder="0" src="<%=path%>/chkstodemom/addChkstoDemod.do"></iframe>',
					width: $(document.body).width()-20/* '1100px' */,
					height: '500px',
					draggable: true,
					isModal: true
					
				});
			}
		
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).find(":checkbox").attr("checked", false);
			     }
			     else{
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			         var explanno=$(this).find(':checkbox').val();
					 var  url="<%=path%>/explanM/tableExplanM.do?explanno="+explanno;
					 $('#mainFrame').attr('src',url);
				     window.mainFrame.location =url;
			     }
			 });
			
			$("#search").bind('click', function() {
				$('.search-div').hide();
				$("#listForm").submit();
			});
			
			/* 模糊清空 */
			$("#resetSearch").bind('click', function() {
				$('.search-condition input').val('');
			});
		});
		
		//生成领料单
		function toChkstom(){
			var ary = [];
			var idChkValue = [];
			var checkboxList = $('.grid').find('.table-body').find('tr').find(':checkbox');
			checkboxList.filter(':checked').each(function(i){
				idChkValue.push($(this).val());
				ary.push($(this).parents('tr').find('td:eq(7)').find('span').attr('title'));
			});
			var nary=ary.sort();  
			if (nary[0]!=nary[ary.length-1]){   
				alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
				return;
			}  
			var ids=idChkValue.join(",");
			$('body').window({
				id: 'window_chkstomexplan',
				title: '<fmt:message key ="generate_material_requisition" />',
				content: '<iframe id="chkstomFrame" frameborder="0" src="<%=path%>/explanM/toChkoutm.do?positn.code='+nary[0]+'&idList='+ids+'"></iframe>',
				width: '650px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key ="generate_material_requisition" />',
							title: '<fmt:message key ="generate_material_requisition" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-40px','-40px']
							},
							handler: function(){
								window.frames["chkstomFrame"].saveChkstom();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-60px','0px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		function pageReload(){
	        window.location.href = '<%=path%>/explanM/listExplanM.do';
	    }
		</script>
	</body>
</html>