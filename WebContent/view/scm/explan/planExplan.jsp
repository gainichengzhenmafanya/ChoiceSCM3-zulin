<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>计划--计划</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/explan/updateExplanByPlan.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label"><fmt:message key ="processing_room" />：</div>
				<div class="form-input">
					<select class="select" id="positn" name="positn" style="width:180px;">
						<c:forEach var="positn" items="${listPositn}" varStatus="status">
							<option 
								<c:if test="${positn.code==supplyAcct.positn}"> 
										   	selected="selected"
								</c:if> 
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label" style="width:80px;margin-left:30px;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${supplyAcct.sp_code}"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
				<div class="form-label"><fmt:message key="smallClass"/>：</div>
				<div class="form-input">
					<select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"   class="select"></select>
					<input type="hidden" id="typdes" name="typdes" value="${supplyAcct.typdes }"/>
				</div>	
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label"><fmt:message key ="branche" />：</div>
				<div class="form-input">
					<input type="text"  id="firmDes"  name="firmDes" value="${firmDes }" readonly="readonly"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${firmCode }"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="Whether_or_not" />：</div>
				<div class="form-input" style="width:180px;">
					<input type="radio" name="deal" id="undealed" <c:if test="${supplyAcct.deal=='0'}" >checked="checked" </c:if> value="0"/><fmt:message key ="scm_un_done" />
					<input type="radio" name="deal" id="dealed" <c:if test="${supplyAcct.deal=='1'}" >checked="checked"</c:if> value="1"/><fmt:message key ="scm_have_done" />
				</div>			
			</div>
			<table id="test" style="width:'100%';" title="<fmt:message key ="Branch_processing_product_purchase_inquiry" />"  rownumbers="true" remoteSort="true"
				idField="SP_CODE">
				<thead>
					<tr>
					<th field="ck" checkbox="true"></th>
					<th field="POSITNEXDES" width="100px"><span id="POSITNEXDES"><fmt:message key ="production_workshop" /></span></th>
					<th field="SP_CODE" width="80px"><span id="SP_CODE"><fmt:message key ="supplies_code" /></span></th>
					<th field="SP_NAME" width="100px"><span id="SP_NAME"><fmt:message key ="supplies_name" /></span></th>
					<th field="SP_DESC" width="60px"><span id="SP_DESC"><fmt:message key ="supplies_specifications" /></span></th>
					<th field="UNIT" width="50px"><span id="UNIT"><fmt:message key ="unit" /></span></th>
					<th field="AMOUNT" width="60px" align="right"><span id="UNIT"><fmt:message key ="aggregate_demand" /></span></th>
					<th field="CNT" width="60px" align="right"><span id="UNIT"><fmt:message key ="current_inventory" /></span></th>
					<th field="AMOUNT_NOW" width="80px"  align="right"><span id="AMOUNT_NOW"><fmt:message key="Processing_capacity" /></span></th>
<!-- 			        <th field="" width="100px"><span id="UNIT">已计划量</span></th> -->
					<th field="AMOUNT_XU" width="60px" align="right"><span id="UNIT"><fmt:message key ="required_processing_capacity" /></span></th>
					<c:forEach var="positn" items="${columns}">
						<th field="F_${positn.code}" width="60px;" align="right"><span id="_${positn.code}"/>${positn.des}</th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#grptyp").htmlUtils("select",[]);
			$("#grp").htmlUtils("select",[]);
			$("#typ").htmlUtils("select",[]);
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
								alert('<fmt:message key ="Date_can_not_be_empty" />！');
								return;
							}
							$('#listForm').submit();
						}
// 					},{
// 						text: 'Excel',
// 						title: 'Excel',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
// 						icon: {
/// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-40px','-20px']
// 						},
// 						handler: function(){
// 							$('#listForm').attr('action','<%=path%>/useFirmCompare/exportGrossProfit.do');
// 							$('#listForm').submit();
// 						}
// 					},{
// 						text: '<fmt:message key="print" />',
// 						title: '<fmt:message key="print" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 						icon: {
// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
// 								$('#queryForm').attr('target','report');
// 								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
// 								var action="<%=path%>/useFirmCompare/printGrossProfit.do?checkMis="+checkMis;
// 								$('#queryForm').attr('action',action);
// 								$('#queryForm').submit();								
// 							}else{
// 								showMessage({
// 									type: 'error',
// 									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
// 									speed: 1000
// 									});
// 								return ;
// 							}
// 						}
					},{
						text: '<fmt:message key ="added_to_the_production_arrangement" />',
						title: '<fmt:message key ="added_to_the_production_arrangement" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							 joinPlan();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),100);
  	 		$('#test').datagrid({});
  	 		var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));
				}
			});	
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
  	 	});
  	 	function joinPlan(){
			var rows = $('#test').datagrid('getSelections');
			if(rows==0){
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return;
			}
			var sp_positn = rows[0].POSITNEXDES;
			var flag = true;
  	 		if(confirm('<fmt:message key="only_checked_saved_whether_continue"/>!')){
	  	 		var selected = {}; 
	  	 		selected['dat'] = $('#bdat').val();
	  	 		selected['datEnd'] = $('#edat').val();
				for(var i=0;i<rows.length;i++){
					if(rows[i].AMOUNT_XU<=0){
						alert("<fmt:message key="Do_not_check_the_need_to_process_the_amount_of_less_than_half_of_the_finished_product" />！");
						return;
					}
					if(sp_positn != rows[i].POSITNEXDES){
						flag = false;
					}
					selected['explanList['+i+'].supply.sp_code'] = rows[i].SP_CODE;
					selected['explanList['+i+'].cnt'] = rows[i].AMOUNT_XU;
					selected['explanList['+i+'].cntplan'] = rows[i].AMOUNT_XU;
					selected['explanList['+i+'].supply.positnex'] = rows[i].POSITNEX;
				}
				if (!flag){   
					alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
					return;
				}
				$.post('<%=path%>/explan/saveExplanByPlan.do',selected,function(data){
					parent.pageReload('parent');
				});
  	 		}
//   	 		if(confirm('是否确定将所选择项加入到生产安排？')){
// 	  	 		var selected = {}; 
// 	  	 		selected['dat'] = $('#bdat').val();
// 	  	 		selected['datEnd'] = $('#edat').val();
// 	  	 		selected['datEnd'] = $('#edat').val();
// 				for(var i=0;i<rows.length;i++){
// 					selected['explanList['+i+'].supply.sp_code'] = rows[i].SP_CODE;
// 					selected['explanList['+i+'].cnt'] = rows[i].AMOUNT_XU;
// 					selected['explanList['+i+'].cntplan'] = rows[i].AMOUNT_XU;
// 				}
<%-- 				$.post('<%=path%>/explan/saveExplanByPlan.do',selected,function(data){ --%>
// 					parent.pageReload('parent');
// 				});
				
//   	 		}

  	 	}
  	 </script>
  </body>
</html>
