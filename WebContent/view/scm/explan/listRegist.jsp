<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>explan Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
				.seachSupply{
					margin-top:3px;
					cursor: pointer;
				}
				.form-line .form-label{
					width: 5%;
				}
				.form-line .form-input{
					width: 11%;
				}
				.form-line .form-input input[type=text]{
					width: 98%;
				}
			</style>						
		</head>
	<body>
		<div class="tool">
		</div>
		<input type="hidden" id="selectIndSta" name="selectIndSta" value="NO"/>	
		<form action="<%=path%>/explanM/regist.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input">
				    <input autocomplete="off" type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${explanD.maded}" pattern="yyyy-MM-dd"/>" />
				</div>
				<div class="form-label"><fmt:message key="processing_room"/></div>
				<div class="form-input">
		 			<input type="text" name="supply.positnexdes" id="positnexdes" class="selectDepartment text" readonly="readonly" value="${explanD.supply.positnexdes}"/>
					<input type="hidden" name="supply.positnex" id="positnex" class="text" value="${explanD.supply.positnex}" />
				</div>
				<div class="form-label"><fmt:message key="coding"/></div>
				<div class="form-input">
					<input type="text" class="text" id="sp_code" name="supply.sp_code" value="${explanD.supply.sp_code}" /><img id="seachSupply" class="seachSupply" src="<%=path%>/image/themes/icons/search.png" alt="查询物资" />
				</div>	
				<div class="form-label" style="width: 20px;"></div>		
				<div class="form-input">
					<span style="font-weight: bold;"><fmt:message key="Not_saving_plan" /> <span id="jhChangeNum" style="font-weight: bold;color: red;">0</span> <fmt:message key ="article" /></span>
					<span style="font-weight: bold;margin-left: 20px;"><fmt:message key="Not_saved" /> <span id="djChangeNum" style="font-weight: bold;color: blue;">0</span> <fmt:message key ="article" /></span>
				</div>					
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input">
				 	  <input autocomplete="off" type="text" id="datEnd" name="datEnd" class="Wdate text" value="<fmt:formatDate value="${explanD.datEnd}" pattern="yyyy-MM-dd"/>" />
				</div>
				<div class="form-input">
					<input type="radio" id="" name="ynrk" value="" checked="checked"/><fmt:message key ="all" />
					<input type="radio" id="" name="ynrk" value="Y"
						<c:if test="${explanD.ynrk=='Y'}"> checked="checked"</c:if> 
					/><fmt:message key ="The_storage" />
					<input type="radio" id="" name="ynrk" value="N" 
						<c:if test="${explanD.ynrk=='N'}"> checked="checked"</c:if> 
					/><fmt:message key="Not_in_storage" />
				</div>
<!-- 				<div class="form-label" style="width: 200px;"></div>	 -->
<!-- 				<div class="form-input" style="margin-left: 50px;margin-top:5px;padding-left:0px; width: 200px;height:15px;background-color: #F0F0F0;" ><div id="currState" style="background-color: #28FF28;height:15px;width:0px;" ></div></div><div id="per" style="margin-top: 5px;">0</div>					 -->
			</div>
		
			<div class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num" rowspan="2"><span style="width: 30px;">&nbsp;</span></td>
								<td rowspan="2">
									<span style="width:20px;text-align: center;">
										<input type="checkbox" id="chkAll"/>
									</span>
								</td>
								<td><span style="width:40px;"><fmt:message key="processing_no"/></span></td>
								<td><span style="width:80px;"><fmt:message key="shouyedingdanrq"/></span></td>
								<td><span style="width:90px;"><fmt:message key="workshop"/></span></td>
								<td><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:80px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:60px;"><fmt:message key="demand"/></span></td>
								<td><span style="width:70px;"><fmt:message key="registration_number"/></span></td>
								<td><span style="width:30px;"><fmt:message key="the_reference_unit_br"/></span></td>
								<td><span style="width:70px;"><fmt:message key="reference_number"/></span></td>
								<td><span style="width:80px;"><fmt:message key="storage_positions"/></span></td>
								<td><span style="width:60px;"><fmt:message key="whether_storage"/></span></td>
								<td><span style="width:70px;"><fmt:message key="processing_time"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
<!-- 								<td><span style="width:70px;">生产日期</span></td> -->
<!-- 								<td><span style="width:50px;">批次号</span></td> -->
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="explanD" items="${explanDList}" varStatus="status">
								<tr data-sp_per1="${chkind.supply.sp_per1}" data-ynbatch="${chkind.supply.ynbatch }">
									<td class="num" ><span style="width:30px;">${status.index+1}</span></td>
									<td >
										<span style="width:20px;text-align: center;">
											<input type="checkbox" name="idList" id="chk_${explanD.id}" value="${explanD.id}"/>
										</span>
									</td>
									<td><span title="${explanD.explanno}" style="width:40px;text-align: right;">${explanD.explanno}&nbsp;</span></td>
									<td><span title="${explanD.maded}" style="width:80px;text-align: right;"><fmt:formatDate value="${explanD.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${explanD.supply.positnex}" style="width:90px;">${explanD.supply.positnexdes}&nbsp;</span></td>			
									<td><span title="${explanD.supply.sp_code}" style="width:70px;">${explanD.supply.sp_code}&nbsp;</span></td>
									<td><span title="${explanD.supply.sp_name}" style="width:80px;">${explanD.supply.sp_name}&nbsp;</span></td>
									<td><span title="${explanD.supply.unit}" style="width:30px;text-align: right;">${explanD.supply.unit}&nbsp;</span></td>
									<td><span title="${explanD.supply.last_price}" style="width:50px;text-align: right;">${explanD.supply.last_price}&nbsp;</span></td>
									<td><span title="${explanD.amount}" style="width:60px;text-align:right;">${explanD.amount}&nbsp;</span></td>
									<td class="textInput">
										<span title="<fmt:formatNumber value="${explanD.amountin}" type="currency" pattern="0.00"/>" style="width:70px;">
											<input type="text" value="<fmt:formatNumber value="${explanD.amountin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="addCntact(this,'${explanD.id}','${explanD.supply.extim }','${explanD.supply.unitper }')" onkeyup="validate(this);" disabled="disabled" style="text-align:right;width: 70px;"/>
										</span>
									</td>
									<td><span title="${explanD.supply.unit1}" style="width:30px;text-align: right;">${explanD.supply.unit1}&nbsp;</span></td>
									<td class="textInput">
										<span title="<fmt:formatNumber value="${explanD.amount1in }" type="currency" pattern="0.00"/>" style="width:70px;">
											<input type="text" value="<fmt:formatNumber value="${explanD.amount1in }" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="addCntact1(this,'${explanD.id}','${explanD.supply.extim }','${explanD.supply.unitper }')" onkeyup="validate(this);" disabled="disabled" style="text-align:right;width: 70px;"/>
										</span>
									</td>
									<td><span title="${explanD.supply.sp_position}" style="width:80px;">${explanD.supply.positndes}&nbsp;</span></td>
									<td><span title="${explanD.ynrk}" style="width:60px;">
										<c:choose>
											<c:when test="${explanD.ynrk=='Y'}">${explanD.ynrk}</c:when>
											<c:otherwise>N</c:otherwise>
										</c:choose>
									</span></td>
									<td class="textInput">
										<c:choose>
											<c:when test="${explanD.extim==0.0}">
												<c:set var='extim' value="${explanD.supply.extim*explanD.amount }"/>
											</c:when>
											<c:otherwise>
												<c:set var='extim' value="${explanD.extim }"/>
											</c:otherwise>
										</c:choose>
										<span title="<fmt:formatNumber value="${extim}" type="currency" pattern="0.00"/>" style="width:70px;">
											<input type="text" value="<fmt:formatNumber value="${extim}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="addExtim(this,'${explanD.id}')" onkeyup="validate(this);" disabled="disabled" style="text-align:right;width: 70px;"/>&nbsp;
										</span>
									</td>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${explanD.supply.sp_per1 != 0}"><!-- 必须是设置有效期的才输有效期 --> --%>
<!-- 											<td> -->
<!-- 												<span style="width:70px;"> -->
<!-- 													<input type="text" onfocus="this.select();" onkeypress="selectDued(this)" style="width: 70px;" disabled="disabled"/> -->
<!-- 												</span> -->
<!-- 											</td> -->
<%-- 										</c:when> --%>
<%-- 										<c:otherwise> --%>
<!-- 											<td><span style="width:70px;"></span></td> -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
<!-- 									<td class="textInput"> -->
<!-- 										<span style="width:50px;"> -->
<!-- 											<input title="" type="text" value="" onfocus="this.select()" disabled="disabled"/> -->
<!-- 										</span> -->
<!-- 									</td> -->
									<td style="display:none"><span style="width:90px;">${explanD.supply.positnex}</span></td>
									<td><span title="${explanD.supply.sp_desc}" style="width:70px;">${explanD.supply.sp_desc}&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
// 		var nScrollHeight=0;
// 		var nScrollTop=0;
// 		var returnInfo = true;//判断当前页面ajax是否已经准备就绪
// 		var pageSize = 0;
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			new tabTableInput("table-body","text"); //input  上下左右移动
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
			                case 70: $('#autoId-button-101').click(); break;
			                case 65: $('#autoId-button-102').click(); break;
			                case 81: $('#autoId-button-103').click(); break;
			                case 87: $('#autoId-button-104').click(); break;
			                case 69: $('#autoId-button-105').click(); break;
			                case 82: $('#autoId-button-106').click(); break;
			                case 84: $('#autoId-button-107').click(); break;
			                case 68: $('#autoId-button-108').click(); break;
			            }
			 	});
			
		 	//编辑需加工量、标准数量、参考数量，按回车可以跳到下一行的同一列
			$('.textInput').live('keydown',function(e){
				if(parent.ddzxEditState=="edit"){//判断如果是编辑状态
			 		if(e.keyCode==13){
			 			var lie = $(this).prevAll().length;
						var hang= $(this).parent().prevAll().length + 1;
						$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
			 		}
				}
			});
		 	//解决谷歌浏览器单击不选中的问题
			$('.textInput').find('input').live('click',function(event){
				var self = this;
				setTimeout(function(){
					self.select();
				},10);
			});
		 	
// 			if('${action}'!='init' && '${currState}'!='1'){
// 				addTr('first');	
// 			}else if('${action}'!='init' && '${currState==1}'){
// 				$("#per").text('100%');
// 				$("#currState").width(200);
// 			}
// 			pageSize = '${pageSize}';
// 			var ndivHeight = $(".table-body").height();
// 			$(".table-body").scroll(function(){
// 		          	nScrollHeight = $(this)[0].scrollHeight;
// 		          	nScrollTop = $(this)[0].scrollTop;
// 		          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
// 		          			returnInfo = false;
// 		          			addTr();	
// 		          	}
// 		          });
			
			if('${action}'=='init'){
				parent.ddzxEditState = '';//页面初始化的时候讲编辑状态改为非
				parent.jhTrlist=undefined;//将计划list清空
				parent.djTrlist=undefined;//将登记list清空
				parent.jhChangeNum=0;//将已经修改的条数改为0
				parent.djChangeNum=0;//将已经修改的条数改为0
				$("#sp_init").focus();
			}else{
				$("#jhChangeNum").text(parent.jhChangeNum);
				$("#djChangeNum").text(parent.djChangeNum);
			}
			
			//判断如果不是编辑状态
// 			if(parent.ddzxEditState!='edit'){
// 				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
// 			}else{
// 				$('tbody input[type="text"]').attr('disabled',false);
// 				if($('tbody tr:first .nextclass').length!=0){
// 					$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
// 				}
// 			}
		 	
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			
			$('#positnexdes').bind('focus.custom',function(e){
				var positnex=$('#positnex').val();
				if(!!!top.customWindow){
					var offset = getOffset('positnexdes');
					top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#positnex'),null,null,null,'150','300','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft_safe.do?defaultCode='+defaultCode+'&is_djrk=1',$('#sp_code'));	
				}
			});
			$('#maded').bind('click',function(){
				new WdatePicker();
			});
			$('#datEnd').bind('click',function(){
				new WdatePicker();
			});
			//默认两个保存按钮是不可用的
			loadToolBar([true,false,false]);
		});
		
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />(<u>F</u>)',
						title: '<fmt:message key="query_scheduling_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							var dat = $('#listForm').find("#maded").val().toString();
							var datEnd= $('#listForm').find("#datEnd").val().toString();
							if(dat<=datEnd)
								$('#listForm').submit();
							else
								alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
						}
					},{
						text: '<fmt:message key="edit" />(<u>F</u>)',
						title: '<fmt:message key="edit" />',
						useable: use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
							if($('.grid').find('.table-body').find("tr").size()<1){
								alert('<fmt:message key="data_empty_edit_invalid"/>！！');
							}else{
								parent.ddzxEditState='edit';
								loadToolBar([false,true,true]);
								//只有未入库的才回编辑  wjf
								$('.grid').find('.table-body').find('tr').each(function(e){
									if($(this).find('td:eq(14)').find('span').attr('title') == 'Y'){//wj 查询的td数有误
										$(this).find('input[type="text"]').attr('disabled',true);
									}else{
										$(this).find('input[type="text"]').attr('disabled',false);
									}
								});
								
								new tabTableInput("table-body","text"); //input  上下左右移动
								if($('tbody tr:first').length!=0){
									$('tbody tr:first .textInput span input')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
								}
							}
						}
					},{
						text: '<fmt:message key="save_registration" />(<u>E</u>)',
						title: '<fmt:message key="save_registration"/>',
						useable : use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','-0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="Are_you_sure_you_have_the_change" />？')){
								saveCntact();//保存登记	
							}
						}
					},{
						text: '<fmt:message key="registration_storage" />(<u>T</u>)',
						title: '<fmt:message key="registration_storage"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-40px']
						},
						handler: function(){
							if(parent.ddzxEditState=='edit'){
								if(confirm('<fmt:message key="Change_the_number_of_registration_processing_time_is_not_saved_continue_to_generate_a_single_store" />？')){
									chkinm();
								}
							}else{
								chkinm();
							}
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
		}
					
	    	function onBlurMethod(inputObj){
	    		$(inputObj).parent('span').text($(inputObj).val());
    		}
	    	
			//保存登记
			function saveCntact(){
				$("#maded").focus();
				//如果修改过数据就保存 
				if(parent.djTrlist){
					var predata = parent.djTrlist;
					var disList = {};
					var num=0;
					for(var i in predata){
						disList['explanDList['+num+'].id']=i;
						undefined==predata[i].amountin?'':disList['explanDList['+num+'].amountin']=predata[i].amountin;
						undefined==predata[i].amountin?'':disList['explanDList['+num+'].amount1in']=predata[i].amount1in;
						undefined==predata[i].extim?'':disList['explanDList['+num+'].extim']=predata[i].extim;
						undefined==predata[i].positn?'':disList['explanDList['+num+'].supply.positnex']=predata[i].positn;
						disList['explanDList['+num+'].supply.sp_code']=$('#chk_'+i).parents('tr').find('td:eq(5)').find('span').attr('title');
						num++;
					}
					$.post('<%=path%>/explanM/updateCntact.do',disList,function(data){
						var rs = eval('('+data+')');
						if(rs.pr=="succ"){
							alert('<fmt:message key ="update_successful" />'+rs.updateNum+'<fmt:message key ="article_data" />！');
							var action="<%=path%>/explanM/regist.do";
							$('#listForm').attr('action',action);
						}
					});	
				}else{
					alert('<fmt:message key="You_do_not_modify_any_data" />！');
				}
				parent.ddzxEditState = '';
				parent.jhChangeNum = 0;
				parent.djChangeNum = 0;
				parent.jhTrlist=undefined;
				parent.djTrlist=undefined;
				loadToolBar([true,false,false]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
				$('#listForm').submit();
			}
			
			//登记入库
			function chkinm(){
				var flag = true;//判断是否有已入库的 有就提示不要选  wjf
				var flag1 = true;//判断入库数量是否为0
				var flag2 = true;//选的加工间必须一致
				var flag3 = true;//入到的基地仓库必须一样
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() <= 0){ 
					alert('<fmt:message key="please_select_the_information_storage"/>！');
					return;
				}
				var positnex=$('.grid').find('.table-body').find(':checkbox').filter(':checked').first().parents('tr').find('td:eq(4)').find('span').attr('title');//加工间
				var positn=$('.grid').find('.table-body').find(':checkbox').filter(':checked').first().parents('tr').find('td:eq(13)').find('span').attr('title');//入库的基地仓库
				var chkValue = [];
				checkboxList.filter(':checked').each(function(i){
					if($(this).parents('tr').find('td:eq(14)').find('span').attr('title') == 'Y'){
						flag = false;
						return false;
					}else if (Number($(this).parents('tr').find('td:eq(10)').find('span').attr('title'))==0 || Number($(this).parents('tr').find('td:eq(12)').find('span').attr('title'))==0) {
						flag1 = false;
						return false;
					}else if(positnex != $(this).parents('tr').find('td:eq(4)').find('span').attr('title')){
						flag2 = false;
						return false;
					}//else if(positn != $(this).parents('tr').find('td:eq(13)').find('span').attr('title')){
					//	flag3 = false;
					//	return false;
					//}
					chkValue.push($(this).val());
				});
				if(!flag){
					alert('<fmt:message key="Has_not_been_put_into_storage" />！');
					return;
				}
				if(!flag1){
					alert('<fmt:message key="The_number_of_storage_standard_or_reference_number_is_not_0" />！');
					return;
				}
				if (!flag2){   
					alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
					return;
				}
				//if (!flag3){   
				//	alert('<fmt:message key="Can_only_choose_a_single_storage_position" />！');
				//	return;
				//}
				$.ajax({
					type: "POST",
					url: "<%=path%>/explan/findById.do",
					data: "positn="+positnex,
					dataType: "json",
					success:function(deliver){
						if(null == deliver){
							alert('<fmt:message key="selected_workshop_supplier_not_set"/>！');
						}else{
							if(confirm('<fmt:message key="only_checked_option_be_put_in_storage_whether_to_continue"/>!')){
								var action = '<%=path%>/explanM/saveChkinm.do?ids='+chkValue.join(",")+'&positn='+positnex;
								$('body').window({
									title: '<fmt:message key="warehousing_scheduling_information"/>',
									content: '<iframe frameborder="0" src='+action+'></iframe>',
									width: 500,
									height: '245px',
									draggable: true,
									isModal: true
								});
							}
						}
					}
				});
			}
			
			function pageReload(par){
		    	$('#listForm').submit();
	    	}
		    
		    // 鼠标离开时检查数量输入格式是否合法
			function validate(e){
				if(Number(e.value)<0){
					alert('<fmt:message key="number_cannot_be_negative"/>');
					e.value=e.defaultValue;
					e.focus();
					return;
				}else if(isNaN(e.value)){
					alert('<fmt:message key="number_be_not_valid_number"/>！');
					e.value=e.defaultValue;
					e.focus();
					return;
				}
	    	}
	    			    	
	    	//保存产后登记 标准数量
	    	function addCntact(e,f,tim,u){
	    		if(e.defaultValue!=e.value){
	    			var positn = $(e).closest('td').closest('tr').find('td:eq(16)').find("span").text();
	    			var extim =  $(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val();
					if(parent.djTrlist){
						if(parent.djTrlist[f]){
							parent.djTrlist[f]['amountin']=e.value;
							parent.djTrlist[f]['amount1in'] = Number(e.value)*Number(u);
							$(e).closest('td').closest('tr').find('td:eq(12)').find("span").find('input').val((Number(e.value)*Number(u)).toFixed(2));
							if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
								$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val(Number(e.value)*Number(tim));
								parent.djTrlist[f]['extim']=Number(e.value)*Number(tim);
								extim = Number(e.value)*Number(tim);
							}
							parent.djTrlist[f]['positn']=positn;
							parent.djTrlist[f]['extim']=extim;
						}else{
							parent.djTrlist[f]={};
							parent.djChangeNum = parent.djChangeNum+1;
							$("#djChangeNum").text(parent.djChangeNum);
							parent.djTrlist[f]['amountin']=e.value;
							parent.djTrlist[f]['amount1in'] = Number(e.value)*Number(u);
							$(e).closest('td').closest('tr').find('td:eq(12)').find("span").find('input').val((Number(e.value)*Number(u)).toFixed(2));
							if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
								$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val(Number(e.value)*Number(tim));
								parent.djTrlist[f]['extim']=Number(e.value)*Number(tim);
								extim = Number(e.value)*Number(tim);
							}
							parent.djTrlist[f]['positn']=positn;
							parent.djTrlist[f]['extim']=extim;
						}
					}else{
						parent.djTrlist = {};
						parent.djTrlist[f]={};
						parent.djChangeNum = 1;
						$("#djChangeNum").text(1);
						parent.djTrlist[f]['amountin']=e.value;
						parent.djTrlist[f]['amount1in'] = Number(e.value)*Number(u);
						$(e).closest('td').closest('tr').find('td:eq(12)').find("span").find('input').val((Number(e.value)*Number(u)).toFixed(2));
						if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
							$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val(Number(e.value)*Number(tim));
							parent.djTrlist[f]['extim']=Number(e.value)*Number(tim);
							extim = Number(e.value)*Number(tim);
						}
						parent.djTrlist[f]['positn']=positn;
						parent.djTrlist[f]['extim']=extim;
					}
	    		}
	    		e.value = Number(e.value).toFixed(2);
	    	}
	    	//保存产后登记 参考数量
	    	function addCntact1(e,f,tim,unitper){
	    		if(e.defaultValue!=e.value){
	    			var positn = $(e).closest('td').closest('tr').find('td:eq(16)').find("span").text();
	    			var extim =  $(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val();
					if(parent.djTrlist){
						if(parent.djTrlist[f]){
							parent.djTrlist[f]['amount1in']=e.value;
							if(Number(unitper) != 0){
								parent.djTrlist[f]['amountin']=(Number(e.value)/Number(unitper)).toFixed(2);
								$(e).closest('td').closest('tr').find('td:eq(10)').find("span").find('input').val((Number(e.value)/Number(unitper)).toFixed(2));
								if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
									$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val((Number(e.value)/Number(unitper)*Number(tim)).toFixed(2));
									parent.djTrlist[f]['extim']=Number(e.value)/Number(unitper)*Number(tim)
									extim = Number(e.value)/Number(unitper)*Number(tim);
								}
							}
							parent.djTrlist[f]['positn']=positn;
							parent.djTrlist[f]['extim']=extim;
						}else{
							parent.djTrlist[f]={};
							parent.djChangeNum = parent.djChangeNum+1;
							$("#djChangeNum").text(parent.djChangeNum);
							parent.djTrlist[f]['amount1in']=e.value;
							if(Number(unitper) != 0){
								parent.djTrlist[f]['amountin']=(Number(e.value)/Number(unitper)).toFixed(2);
								$(e).closest('td').closest('tr').find('td:eq(10)').find("span").find('input').val((Number(e.value)/Number(unitper)).toFixed(2));
								if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
									$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val((Number(e.value)/Number(unitper)*Number(tim)).toFixed(2));
									parent.djTrlist[f]['extim']=Number(e.value)/Number(unitper)*Number(tim)
									extim = Number(e.value)/Number(unitper)*Number(tim);
								}
							}
							parent.djTrlist[f]['positn']=positn;
							parent.djTrlist[f]['extim']=extim;
						}
					}else{
						parent.djTrlist = {};
						parent.djTrlist[f]={};
						parent.djChangeNum = 1;
						$("#djChangeNum").text(1);
						parent.djTrlist[f]['amount1in']=e.value;
						if(Number(unitper) != 0){
							parent.djTrlist[f]['amountin']=(Number(e.value)/Number(unitper)).toFixed(2);
							$(e).closest('td').closest('tr').find('td:eq(10)').find("span").find('input').val((Number(e.value)/Number(unitper)).toFixed(2));
							if(Number(extim) == 0){//如果工时为0，则工时=e.value * tim
								$(e).closest('td').closest('tr').find('td:eq(15)').find("span").find('input').val((Number(e.value)/Number(unitper)*Number(tim)).toFixed(2));
								parent.djTrlist[f]['extim']=Number(e.value)/Number(unitper)*Number(tim)
								extim = Number(e.value)/Number(unitper)*Number(tim);
							}
						}
						parent.djTrlist[f]['positn']=positn;
						parent.djTrlist[f]['extim']=extim;
					}
	    		}
	    	}
	    	//保存产后登记 标准数量
	    	function addExtim(e,f){
	    		if(e.defaultValue!=e.value){
	    			var positn = $(e).closest('td').closest('tr').find('td:eq(16)').find("span").text();
					if(parent.djTrlist){
						if(parent.djTrlist[f]){
							parent.djTrlist[f]['extim']=e.value;
							parent.djTrlist[f]['positn']=positn;
						}else{
							parent.djTrlist[f]={};
							parent.djChangeNum = parent.djChangeNum+1;
							$("#djChangeNum").text(parent.djChangeNum);
							parent.djTrlist[f]['extim']=e.value;
							parent.djTrlist[f]['positn']=positn;
							
						}
					}else{
						parent.djTrlist = {};
						parent.djTrlist[f]={};
						parent.djChangeNum = 1;
						$("#djChangeNum").text(1);
						parent.djTrlist[f]['extim']=e.value;
						parent.djTrlist[f]['positn']=positn;
					}
	    		}
	    		e.value = Number(e.value).toFixed(2);
	    	}
	    	//保存生产日期
// 			function saveDued(e,f){
// 				var date = e.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig);
// 				if(null!=date && ""!=date){
// 					if(e.defaultValue!=e.value){
// 						if(parent.trlist){
// 							if(parent.trlist[f]){
// 								parent.trlist[f]['dued']=e.value;
// 							}else{
// 								parent.trlist[f]={};
// 								parent.changeNum = parent.changeNum+1;
// 								$("#changeNum").text(parent.changeNum);
// 								parent.trlist[f]['dued']=e.value;
// 							}
// 						}else{
// 							parent.trlist = {};
// 							parent.trlist[f]={};
// 							parent.changeNum = 1;
// 							$("#changeNum").text(1);
// 							parent.trlist[f]['dued']=e.value;
// 						}
// 					}
// 				}
// 			}
			//输入日期回车
			function selectDued(obj){
				if(window.event.keyCode==13){
					var date = obj.value.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/ig); 
					if(null!=date && ""!=date){
						if($("#selectIndSta").val()=="YES"){
							$("#selectIndSta").val("NO");
							new WdatePicker();
							$(obj).focus();
						}else{
							$("#selectIndSta").val("YES");
							new WdatePicker();
							$(obj).focus();
						}
					}else{
						if(null!=obj.defaultValue && ""!=obj.defaultValue){
							obj.value=obj.defaultValue;
						}else{
							obj.value=getInd();
						}
						$(obj).focus();
						return;
					}
				}
			}
			//获取系统时间
			function getInd(){
				var myDate=new Date();  
				var yy=myDate.getFullYear();
				var MM=myDate.getMonth()+1;
				var dd=myDate.getDate();
				if(MM<10)
					MM="0"+MM;
				if(dd<10)
					dd="0"+dd;
				return fullDate=yy+"-"+MM+"-"+dd;
			}
			//保存批次号
// 			function savePcno(e,f){
// 				if(e.defaultValue!=e.value){
// 					if(parent.trlist){
// 						if(parent.trlist[f]){
// 							parent.trlist[f]['pcno']=e.value;
// 						}else{
// 							parent.trlist[f]={};
// 							parent.changeNum = parent.changeNum+1;
// 							$("#changeNum").text(parent.changeNum);
// 							parent.trlist[f]['pcno']=e.value;
// 						}
// 					}else{
// 						parent.trlist = {};
// 						parent.trlist[f]={};
// 						parent.changeNum = 1;
// 						$("#changeNum").text(1);
// 						parent.trlist[f]['pcno']=e.value;
// 					}
// 				}
// 			}
	    	//保存产后登记 参考数量
// 			var totalCount;
// 			var condition;
// 			var currPage;
// 			function addTr(check){
// 				if(check=='first'){
// 					totalCount = '${totalCount}';
// 					condition = ${explanJson};
// 					var supply = condition.supply;
// 					condition.supply=undefined;
					//对象套对象得专门处理，否则报错
// 					for(var i in supply){
// 						if(supply[i]){
// 							condition['supply.'+i]=supply[i];	
// 						}
// 					}
// 					currPage= 1;
					
// 					condition['maded'] = new Date(condition.dat.time).format("yyyy-mm-dd");
// 					condition['datEnd'] = new Date(condition.datEnd.time).format("yyyy-mm-dd");
// 					condition['curStatus'] = '${curStatus}';
// 					condition['totalCount'] = totalCount;
// 					condition['currPage']=1;
// 					$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
// 					$("#currState").width(Number('${currState}')*200);
// 					return;
// 				}
<%-- 				$.post("<%=path%>/explan/listAjax.do",condition,function(data){ --%>
// 					var rs = eval('('+data+')');
					//var rs = data;
// 					$("#per").text((rs.currState*100).toFixed(0)+'%');
// 					$("#currState").width(""+rs.currState*200+"px");
					//不是最后一页
// 					var num = rs.currPage*pageSize;
// 					var explanList = rs.explanList;
// 					for(var i in explanList){
// 						var explanD = explanList[i];
						
// 						var tr = '<tr ';
// 						tr = tr + '>';
// 						tr = tr + '<td class="num" ><span style="width:30px;">'+ ++num +'</span></td>';
// 						tr = tr + '<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_'+explanD.id+'" value="'+explanD.id+'"/></span></td>';
// 						tr = tr + '<td><span title="'+explanD.exno+'" style="width:40px;text-align: right;">'+explanD.exno+'&nbsp;</span></td>';
// 						tr = tr + '<td><span title="'+explanD.supply.positnex+'" style="width:90px;">'+explanD.supply.positnexdes+'&nbsp;</span></td>';
// 						tr = tr + '<td><span title="'+explanD.supply.sp_code+'" style="width:70px;">'+explanD.supply.sp_code+'&nbsp;</span></td>';		
// 						tr = tr + '<td><span title="'+explanD.supply.sp_name+'" style="width:80px;">'+explanD.supply.sp_name+'&nbsp;</span></td>';
// 						tr = tr + '<td><span title="'+explanD.supply.unit+'" style="width:30px;text-align: right;">'+explanD.supply.unit+'&nbsp;</span></td>';
// 						tr = tr + '<td><span title="'+explanD.amount+'" style="width:60px;">'+explanD.amount+'&nbsp;</span></td>';
// 						tr = tr + '<td class="textInput"><span title="'+explanD.amountin+'" style="width:70px;padding: 0px;"><input type="text" value="'+explanD.amountin+'" onfocus="this.select()" onblur="validate(this);addCntact(this,'+explanD.id+')" disabled="disabled"/></span></td>';
// 						tr = tr + '<td><span title="'+explanD.supply.sp_positn+'" style="width:60px;">'+explanD.supply.positndes+'&nbsp;</span></td>';
// 						tr = tr + '<td><span title="'+explanD.ynrk+'" style="width:60px;">'+explanD.ynrk+'&nbsp;</span></td>';
// 						tr = tr + '</tr>';	
// 						$(".grid .table-body tbody").append($(tr));
// 					}
// 					if(rs.over!='over'){
// 						condition['currPage']=++currPage;
// 						returnInfo = true;
// 					}
// 					if(parent.ddzxEditState!='edit'){
// 						$('tbody input[type="text"]').attr('disabled',true);//不可编辑
// 					}else{
// 						new tabTableInput("table-body","text"); //input  上下左右移动
// 					}
// 				});
// 			}
			
			Date.prototype.format = function(){
				var yy = String(this.getFullYear());
				var mm = String(this.getMonth() + 1);
				var dd = String(this.getDate());
				if(mm<10){
					mm = ''+0+mm;
				}
				if(dd<10){
					dd = ''+0+dd;
				}
				var str = yy+"-"+mm+"-"+dd;
				return str;
			};
		</script>
	</body>
</html>