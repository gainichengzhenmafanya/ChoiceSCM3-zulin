<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>领料单</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css"> 	
				.textInput span{
					padding:0px;
				}
				.textInput input{
					border:0px;
					background: #F1F1F1;
				}
		</style>
	</head>
	<body>  
		<form id="listForm" method="post" action="<%=path %>/explanM/saveChkoutm.do">
		<input type="hidden" class="text" id="positn" name="positn" value="${explanM.positn.code}" />
		<input type="hidden" class="text" id="idList" name="idList" value="${idList}" />
 			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num" style="width: 25px;">&nbsp;</td>
								<td rowspan="2" style="width:30px;">
									<input type="checkbox" id="chkAll" checked="checked" />
								</td>
								<td style="width:80px;"><fmt:message key="coding"/></td>
								<td style="width:100px;"><fmt:message key="name"/></td>
								<td style="width:80px;"><fmt:message key="specification"/></td>
								<td style="width:40px;"><fmt:message key="unit"/></td>
								<td style="width:80px;"><fmt:message key="inventory"/></td>
								<td style="width:80px;"><fmt:message key="computation"/></td>
								<td style="width:80px;"><fmt:message key="adjustment_amount"/></td>
<!-- 								<td style="width:70px;">成本单位</td> -->
<!-- 								<td style="width:70px;">成本数量</td> -->
								<td style="width:100px;"><fmt:message key="remark"/></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="product" items="${productList}" varStatus="status">
								<tr>
									<td class="num" style="width: 25px;">${status.index+1}</td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" checked="checked" name="idList" id="chk_${product.item}" value="${product.item}"/>
									</td>
									<td><span title="${product.supply.sp_code}" style="width:70px;text-align: left;">${product.supply.sp_code}</span></td>
									<td><span title="${product.supply.sp_name}" style="width:90px;text-align: left;">${product.supply.sp_name}&nbsp;</span></td>
									<td><span title="${product.supply.sp_desc}" style="width:70px;text-align: left;">${product.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${product.supply.unit}" style="width:30px;text-align: left;">${product.supply.unit}&nbsp;</span></td>
									<td><span title="${product.kc}" style="width:70px;text-align: right;">${product.kc}&nbsp;</span></td>
									<td class="textInput">
										<span title="" style="width:80px;padding: 0px;text-align: right;">
											<fmt:formatNumber value="${product.cnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>
										</span>
									</td>
									<td class="textInput">
										<span title="" style="width:80px;padding: 0px;">
											<input type="text" value="<fmt:formatNumber value="${product.cnt-product.kc<0?0:product.cnt-product.kc}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>" onkeyup="changeNNN(this);" onfocus="this.select()" style="text-align:right;width:80px;"/>
										</span>
									</td>
<!-- 									<td style="width:70px;"> -->
<%-- 										<span title="${product.supply.unit2}" style="width:70px;padding: 0px;">${product.supply.unit2}</span> --%>
<!-- 									</td> -->
<!-- 									<td style="width:70px;"> -->
<%-- 										<span title="${product.cnt2}" style="width:70px;padding: 0px;"> --%>
<%-- 											<fmt:formatNumber value="${product.cnt2}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber> --%>
<!-- 										</span> -->
<!-- 									</td> -->
									<td class="textInput">
										<span title="" style="width:100px;padding: 0px;">
											<input type="text" value="" onfocus="this.select()"/>
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',0,$(document.body),20);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			new tabTableInput("table-body","text"); //input  上下左右移动
		});
		//确定修改并生成报货单
		function saveChkstom(){ 
			//判断有无盘点
		  	if('${tag}'=='1'){
				if(!confirm('建议生成领料单前，先盘点，是否继续?'))
					return;
			}
			var selected = {};
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList 
					&& checkboxList.filter(':checked').size() > 0){
// 				if(confirm('<fmt:message key="only_checked_option_to_generate_reports_of_invoices_whether_to_continue"/>!')){
				if(confirm("<fmt:message key="Only_check_option_will_be_reported_to_generate_orders_and_requisitions_whether_to_continue" />!")){
					var chkValue = [];
					selected['idList']=$('#idList').val();
					selected['firm']=$('#positn').val();
					var sta=-1;
					checkboxList.filter(':checked').each(function(i){
						if(0==$(this).parents('tr').find('td:eq(8)').find('input').val()||$(this).parents('tr').find('td:eq(8)').find('input').val()<=0){
							return;
						}
						sta++;
						chkValue.push($(this).val());
						selected['positn'] = $('#positn').val();
						selected['explanSpcodeList['+sta+'].sp_code'] = $(this).parents('tr').find('td:eq(2)').find('span').attr('title');
						selected['explanSpcodeList['+sta+'].positn'] = $('#positn').val();
						selected['explanSpcodeList['+sta+'].cnt'] = $(this).parents('tr').find('td:eq(8)').find('input').val();
						selected['explanSpcodeList['+sta+'].memo'] = $(this).parents('tr').find('td:eq(9)').find('input').val();//备注
					});
					if(sta==-1){
						alert('<fmt:message key="adjustment_amount_be_zero_the_operation_failed"/>！');
						return;
					}
					selected['item'] = chkValue.join(",");
					var result='parent';
					$.post('<%=path%>/explanM/saveChkoutm.do',selected,function(data){
						var rs = eval('('+data+')');
// 						var msg = rs.msg;
						switch(Number(rs.pr)){
						case 1:
							//alert('<fmt:message key="operation_successful"/>！');
							alert(rs.msg);
// 							showMessage({
// 								type: 'success',
// 								msg: msg,
// 								speed: 3000
// 							});
// 							setTimeout(showChkout,1000);
							break;
						default:
							alert(rs.msg);
							break;
						}
// 					 	弹出提示信息
// 					 	if ("1"==data) {
// 					 		alert("操作失败，请检查默认供应商是否设置！");
// 					 	}else{
// 				    		alert('<fmt:message key="operation_successful"/>！');
// 				    	}
						//parent.pageReload(result);
						window.parent.location.reload();
					});
				}
			}else{
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return ;
			}
		}
			
		// add wangjie 2014年10月29日 10:44:01   导出物资原材料单据
		function getCheckItemCode(){
			var uncheckeditem = [];//选中的原材料
			var checkeditem = [];//未选中的
			
			var produnctcnt = [];//调整量
			var productmemo = [];//备注
			
			 $('.grid').find('.table-body').find(':checkbox').each(function(i){
				if($(this).attr("checked") != 'checked'){
					uncheckeditem.push($(this).parent().parent().index());
				}else{
					checkeditem.push($(this).parent().parent().index());
					produnctcnt.push($(this).parent().parent().find("td:eq(8)").find("input").val());
					productmemo.push($(this).parent().parent().find("td:eq(9)").find("input").val());
				}
			});
			 
			var positn = $("#positn").val();
			var idList = $("#idList").val();
			if(checkeditem.length != 0){
				$("#wait2").val('NO');//不用等待加载
				$("#listForm").attr("action","<%=path%>/explanM/exportSupplyRequest.do?code="+positn+"&idList="+idList+"&uncheckeditem="+uncheckeditem.join(",")+"&produnctcnt="+produnctcnt.join(",")+"&productmemo="+productmemo.join(","));
				$("#listForm").submit();
				$("#wait2 span").html("Loading...");
				$("#listForm").attr("action","<%=path %>/explanM/saveChkoutm.do");
				$("#wait2").val('');
				
			}else{
				alert("<fmt:message key="Choose_the_raw_materials_for_export" />");
			}
		}
		
		function changeNNN(e){
			var va = $(e).val();
			if(isNaN(va)){
				$(e).val(0);
			}
		}
		</script>
	</body>
</html>