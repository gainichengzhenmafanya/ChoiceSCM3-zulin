<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select_category"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			.table-body{
				overflow-y:auto
			}
			</style>
		</head>
	<body>
     	<div id="toolbar"></div>
   		<div class="grid">
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="grp" items="${grpList}" varStatus="status">
							<tr>
								<td style="width:30px; text-align: center;">
									<input type="checkbox" name="idList" id="chk_${grp.code}" value="${grp.code}" />
								</td>
							<td><span title="${grp.code}" style="width:60px;">${grp.code}&nbsp;</span></td>
							<td><span title="${grp.des}" style="width:60px;">${grp.des}&nbsp;</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
  
    <input type="hidden" id="grp" name="grp" value="${grp}"/>
    <input type="hidden" id="parentId" name="parentId" />
    <input type="hidden" id="parentName" name="parentName" />
    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//按钮快捷键
				focus() ;//页面获得焦点
			 	$(document).bind('keyup',function(e){
			 		if(e.keyCode==27){
			 			parent.$('.close').click();
			 		}
				}); 

				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="confirmation_category_of_select"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-120px','0px']
							},
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var idValue = [];
								var nameValue = [];
								checkboxList.filter(':checked').each(function(){
									idValue.push($(this).val());
									nameValue.push($(this).parents('tr').find('td:eq(2)').find('span').attr('title'))
								});
								$('#parentId').val(idValue.join(","));
								$('#parentName').val(nameValue.join(","));
								top.customWindow.afterCloseHandler('Y');
								top.closeCustom();
								parent.$('.close').click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				var str=$('#grp').val();
				var strArry = str.split(",");
				for(var i=0;i<strArry.length;i++)
				{ 
					$('#chk_'+strArry[i]).attr('checked',true);
				};
				/* $('.grid').find('.table-body').find('tr').live("click",function(){
				     if ($(this).hasClass("bgBlue")){
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     } else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				 }); */
			});// end $(document).ready();
		</script>

	</body>
</html>