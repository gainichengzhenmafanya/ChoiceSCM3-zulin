<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>chkinm Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.search{
				margin-top:-2px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 60px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
		<form id="listForm" action="<%=path%>/explanM/list.do" method="post">
			<input  type="hidden" name="orderBy" id="orderBy" value="<c:out value="${explanM.orderBy}" default="explanno"/>" />
			<input  type="hidden" name="orderDes" id="orderDes" value="<c:out value="${explanM.orderDes}" default="0000000000"/>" />
			<input  type="hidden" name="status" id="status" value="0" />
			<input  type="hidden" name="ynll" id="ynll" value="N" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="startdate"/></div>
					<div class="form-input" ><input autocomplete="off" style="width:85px;" type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/>" /></div>			
					<div class="form-label" ><fmt:message key="orders_num"/></div>
					<div class="form-input" ><input type="text" id="explanno" name="explanno" class="text" value="${explanM.explanno}"/></div>
					<div class="form-label"><fmt:message key="orders_maker"/></div>
					<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${explanM.madeby}"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enddate"/></div>
					<div class="form-input"><input autocomplete="off" style="width:85px;" type="text" id="madedEnd" name="madedEnd" class="Wdate text" value="<fmt:formatDate value="${madedEnd}" pattern="yyyy-MM-dd"/>"/></div>
<!-- 					<div class="form-label">审核人</div> -->
<%-- 					<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="<c:out value="${explanM.checby}" />"/></div> --%>
					<div class="form-label"><fmt:message key ="processing_room" /></div>
					<div class="form-input">
<%-- 						<input type="text"  id="positn"  name="positn.des" readonly="readonly" value="${explanM.positn.des}"/> --%>
<%-- 						<input type="hidden" id="positnCode" name="positn.code" value="${explanM.positn.code}"/> --%>
<%-- 						<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' /> --%>
						<input type="text" name="positn.des" id="positnexdes" class="selectDepartment text" readonly="readonly" value="${explanM.positn.des}"/>
						<input type="hidden" name="positn.code" id="positnex" class="text" value="${explanM.positn.code}" />
					</div>
				</div>
		
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:16px;">&nbsp;</span></td>
								<td>
									<span style="width:20px;"><!-- <input type="checkbox" id="chkAll"/> --></span>
								</td>
								<td><span style="width:60px;"><fmt:message key="Order_processing" /></span></td>
								<td><span style="width:70px;"><fmt:message key="date_of_the_system_alone"/></span></td>
								<td><span style="width:150px;"><fmt:message key="time_of_the_system_alone"/></span></td>
								<td><span style="width:80px;"><fmt:message key ="processing_room" /></span></td>
								<td><span style="width:60px;"><fmt:message key="orders_maker"/></span></td>
<%-- 								<td><span style="width:60px;"><fmt:message key="orders_audit"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div> 
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="explanM" varStatus="step" items="${explanMList}">
								<tr>
									<td class="num" ><span style="width:16px;">${step.count}</span></td>
									<td style="width:25px; text-align: center;">
										<span style="width:20px;"><input type="checkbox" name="idList" id="chk_${explanM.explanno}" value="${explanM.explanno}"/></span>
									</td>
									<td><span title="${explanM.explanno}" style="width:60px;text-align: right;">${explanM.explanno}&nbsp;</span></td>
									<td><span title="<fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/>" style="width:70px;"><fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
									<td><span title="${explanM.madet}" style="width:150px;">${explanM.madet}&nbsp;</span></td>
									<td><span title="${explanM.positn.des}" style="width:80px;">${explanM.positn.des}&nbsp;</span></td>
									<td><span title="${explanM.madeby}" style="width:60px;">${explanM.madeby}&nbsp;</span></td>
<%-- 									<td><span title="${explanM.checby}" style="width:60px;">${explanM.checby}&nbsp;</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<form id="reportForm" method="post">
			<input type="hidden" id="chkinno" name="chkinno"/>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		var validate;
			$(document).ready(function(){
				//排序start
				var array = new Array();      
				array = ['explanno','maded', 'madet', 'positn','madeby','checby'];    	 
				$('.grid').find('.table-head').find('td:gt(1)').each(function(i){
					$(this).bind('click',function(){
						var orderDes=$('#orderDes').val();
						var  a=orderDes.charAt(i);
						var b='';
						a==1?b=array[i]+' asc':b=array[i]+' desc';//0降序 desc  1  升序asc
						a==1?a=0:a=1;
						$('#orderDes').val(""+orderDes.substring(0,i)+""+a+orderDes.substring(i+1,orderDes.length));
						$('#orderBy').val(b+','+$('#orderBy').val());
						$('#listForm').submit();
					});
				});
				var order=$('#orderDes').val();
				for(var i=0; i<order.length; i++){
					if(order.charAt(i)==1)
						$('.grid').find('.table-head').find('td:eq('+(i+2)+')').find('span').addClass('datagrid-sort-icon');
				}
				//排序结束
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'explanno',
						validateType:['canNull','intege'],
						/* handler:function(){
							return $("#code").val().replace(/[^\x00-\xff]/g, "**").length<=10; 
						}, */
						param:['T','T','F'],
						error:['','<fmt:message key="single_number_is_digital_or_empty"/>','<fmt:message key="name" /><fmt:message key="length_too_long" />！']
					}]
				});
				//按钮快捷键
				focus() ;//页面获得焦点
				$('#maded').bind('click',function(){
					new WdatePicker();
				});
				$('#madedEnd').bind('click',function(){
					new WdatePicker();
				});
				//双击事件
				$('.grid .table-body tr').live('dblclick',function(){
					var status = $(window.parent.document).find("#curStatus").val();
					if(status == 'add' || status == 'edit'){
						if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？')){
							return;
						}
					}
					var explanno = $(this).find("input:checkbox").val();
					explanno ? window.parent.openExplanM(explanno) && window.close() : alert("error!");
					
				});
				setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#seachOnePositn').bind('focus.custom',function(e){
					var positnCode=$('#positnCode').val();
// 					var positnDes=$('#positn').val();
					if(!!!top.customWindow){
						var offset = getOffset('maded');
						top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnCode,offset,$('#positn'),$('#positnCode'),null,null,null,'150','300','isNull');
					}
				});
				$('#positnexdes').bind('focus.custom',function(e){
					var positnex=$('#positnex').val();
					if(!!!top.customWindow){
						var offset = getOffset('positnexdes');
						top.custom('<fmt:message key="please_select_processing_room"/>','<%=path%>/explan/selectPositnex.do?positnex='+positnex,offset,$(this),$('#positnex'),null,null,null,'150','300','isNull');
					}
				});
			});	
			var tool = $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						var maded = $('#listForm').find("#maded").val().toString();
						var madedEnd= $('#listForm').find("#madedEnd").val().toString(); 
						if(validate._submitValidate()){
							if(maded<=madedEnd)
							$('#listForm').submit();
							else
								alert("<fmt:message key ="Start_time_not_later_than_the_end_of_time" />");
						}		
					}
				},{
					text: '<fmt:message key="enter" />(Enter)',
					title: '<fmt:message key="enter"/>',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-20px']
					},
					handler: function(){
						enterExplanM();
					}
				},{
					text: '<fmt:message key="check"/>(<u>C</u>)',
					title: '<fmt:message key="check"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-60px','-240px']
					},
					handler: function(){
						saveOrUpdateExplan("1");
					}
				},{
					text: '<fmt:message key="delete" />(<u>D</u>)',
					title: '<fmt:message key="delete"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-38px','0px']
					},
					handler: function(){
						deleteExplanM();
					}
				},{
					text: '<fmt:message key="quit"/>',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			});								
			function enterExplanM(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
						var explanno = checkboxList.filter(':checked').val();
						if(status == 'add' || status == 'edit'){
							if(!confirm('<fmt:message key="data_unsaved_whether_to_continue"/>？')){
								return;
							}
						}
						explanno ? window.parent.openExplanM(explanno) && window.close() : alert("error!");
				}else{
					alert('<fmt:message key="please_select_single_message"/>！');
					return ;
				}
			}
			
			//加入计划
			function saveOrUpdateExplan(typ){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="To_review_these_processes" />？')){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						
						//提交并返回值，判断执行状态
						$.post("<%=path%>/explanM/updateExplanM.do?status="+typ+"&explannoids="+chkValue.join(","),function(data){
// 							var rs = eval('('+data+')');
							switch(data){
							case -1:
								alert('<fmt:message key="data_unsaved"/>！');
								break;
							case 1:
								alert('<fmt:message key ="check_successful" />！');
								$('#listForm').submit();
								break;
							case 0:
								alert('<fmt:message key="cannot_repeat_audit"/>！');
								break;
							}
						});
					}
				}else{
					alert('<fmt:message key="please_select_reviewed_information"/>！');
					return ;
				}				
			}
			
			//删除加工单
			function deleteExplanM(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(confirm('<fmt:message key="delete_data_confirm"/>？')){
						var chkValue = [];
						
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						
						var action = '<%=path%>/explanM/delete.do?explannoids='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_storage_storage"/>',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: 500,
							height: '245px',
							draggable: true,
							isModal: true
						});
					}
				}else{
					alert('<fmt:message key="please_select_information_you_need_to_delete"/>！');
					return ;
				}
			}
			function pageReload(){
			    	$('#listForm').submit();
			}
		</script>
	</body>
</html>