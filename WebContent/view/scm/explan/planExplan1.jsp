<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>报货计划--计划</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/explanM/updateExplanByPlan.do" id="listForm" name="listForm" method="post">
  			<div id="warnings" class="form-line" style='${(error=="" or empty error)?"display:none":""}'>
				<div class="form-label" style="color:red;"><input style="color:red;margin-left:10px;margin-top:2px;width:1140px;" type="text" value="${error}:出品加工间未设置！" /></div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="startdate"/>：</div>
				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div>
				<div class="form-label"><fmt:message key="processing_room"/>：</div>
				<div class="form-input">
					<select class="select" id="positn" name="positn" style="width:180px;">
						<c:forEach var="positn" items="${listPositn}" varStatus="status">
							<option 
								<c:if test="${positn.code==supplyAcct.positn}"> 
										   	selected="selected"
								</c:if> 
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-label" style="width:80px;margin-left:30px;"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${supplyAcct.sp_code}"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="enddate"/>：</div>
				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${supplyAcct.edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div>
				<div class="form-label"><fmt:message key ="branche" />：</div>
				<div class="form-input">
					<input type="text" style="width:176px;" id="firmDes"  name="firmDes" value="${firmDes }" readonly="readonly"/>
					<input type="hidden" id="firmCode" name="firmCode" value="${firmCode }"/>
					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				</div>
				<div class="form-label"><fmt:message key="smallClass"/>：</div>
				<div class="form-input">
					<select id="typ" style="width:145px;" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"   class="select"></select>
					<input type="hidden" id="typdes" name="typdes" value="${supplyAcct.typdes }"/>
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"></div>
				<div class="form-input"></div>
				<div class="form-label"></div>
				<div class="form-input"></div>
				<div class="form-label"><fmt:message key="if_processing"/>：</div>
				<div class="form-input" style="width:180px;">
					<input type="radio" name="deal" id="undealed" <c:if test="${supplyAcct.deal=='0'}" >checked="checked" </c:if> value="0"/><fmt:message key ="scm_un_done" />
					<input type="radio" name="deal" id="dealed" <c:if test="${supplyAcct.deal=='1'}" >checked="checked"</c:if> value="1"/><fmt:message key ="scm_have_done" />
				</div>
			</div>
			<table id="test" style="width:'100%';" title="<fmt:message key="daily_goods_to_the_processing_of_a_single_query"/>"  rownumbers="true" remoteSort="true"
				idField="SP_CODE">
				<thead>
					<tr>
					<th field="ck" checkbox="true"></th>
					<th field="SP_CODE" width="80px"><span id="SP_CODE"><fmt:message key="supplies_code"/></span></th>
					<th field="SP_NAME" width="100px"><span id="SP_NAME"><fmt:message key="supplies_name"/></span></th>
					<th field="SP_DESC" width="80px"><span id="SP_DESC"><fmt:message key="supplies_specifications"/></span></th>
					<th field="UNIT" width="50px"><span id="UNIT"><fmt:message key="unit"/></span></th>
					<th field="POSITNEXDES" width="100px"><span id="POSITNEXDES"><fmt:message key="production_workshop"/></span></th>
					<th field="AMOUNT" width="60px" align="right"><span id="AMOUNT"><fmt:message key="aggregate_demand"/></span></th>
					<th field="CNT" width="60px"  align="right"><span id="CNT"><fmt:message key="current_inventory"/></span></th>
					<th field="SAFECNT" width="60px"  align="right"><span id="SAFECNT"><fmt:message key="scm_safety_stock"/></span></th>
<!-- 			        <th field="" width="100px"><span id="UNIT">已计划量</span></th> -->
					<th field="AMOUNT_NOW" width="70px"  align="right"><span id="AMOUNT_NOW"><fmt:message key="Processing_capacity" /></span></th>
					<th field="AMOUNT_XU" width="60px"  align="right"><span id="AMOUNT_XU"><fmt:message key ="required_processing_capacity" /></span></th>
					<c:forEach var="positn" items="${columns}">
						<th field="F_${positn.code}" width="60px;" align="right"><span id="_${positn.code}"/>${positn.des}</th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	<input type="hidden" id="hiddentyp" value="${supplyAcct.typ }"></input>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
// 			$("#grptyp").htmlUtils("select",[]);
// 			$("#grp").htmlUtils("select",[]);
			$("#typ").htmlUtils("select",[]);
			//为了查询之后select继续能附上值  wjf
			var typ = $("#hiddentyp").val();
			typ = typ.substring(1,typ.length-1);
			$("#typ").next('span').find('input').first().val($('#typdes').val());
			$("#typ").next('span').find('input').last().val(typ);
			//按钮快捷键
			focus() ;//页面获得焦点			
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
// 							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
// 								alert('日期不能为空！');
// 								return;
// 							}
							var typdes = $("#typ").next('span').find('input').val();
							$('#typdes').val(typdes);
							$('#listForm').submit();
						}
					},{
						text: 'Excel',
						title: 'Excel',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
						icon: {
 							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-40px','-20px']
						},
						handler: function(){
							var typdes = $("#typ").next('span').find('input').val();
							$('#typdes').val(typdes);
							$("#wait2").val('NO');//不用等待加载
 							$('#listForm').attr('action','<%=path%>/explanM/exportExplanM.do');
							$('#listForm').submit();
							$("#wait2 span").html("Loading...");
							$("#listForm").attr("action","<%=path%>/explanM/updateExplanByPlan.do");
							$("#wait2").val('');//等待加载还原
						}
// 					},{
// 						text: '<fmt:message key="print" />',
// 						title: '<fmt:message key="print" />',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
// 						icon: {
// 							url: '<%=path%>/image/Button/op_owner.gif',
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							if($("#bdat").val()!=null&&$("#bdat").val()!='' && $("#edat").val()!=null&&$("#edat").val()!=''){
// 								$('#queryForm').attr('target','report');
// 								window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
// 								var action="<%=path%>/useFirmCompare/printGrossProfit.do?checkMis="+checkMis;
// 								$('#queryForm').attr('action',action);
// 								$('#queryForm').submit();								
// 							}else{
// 								showMessage({
// 									type: 'error',
// 									msg: '<fmt:message key="date" /><fmt:message key="cannot_be_empty" />！',
// 									speed: 1000
// 									});
// 								return ;
// 							}
// 						}
					},{
						text: '<fmt:message key="added_to_the_production_arrangement"/>',
						title: '<fmt:message key="added_to_the_production_arrangement"/>',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							 joinPlan();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),125);
  	 		$('#test').datagrid({});
 			var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));
				}
			});	
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			//加工间改变，查询未设置出品加工间的物资
			$("#positn").change(function(){
				$.ajax({
					url:'<%=path%>/explanM/getSupplysNoSetExPositn.do',
					type:"POST",
					data:{code:$("#positn").val()},
		            dataType: "json",
		            success: function(data){
		            	if(data!=null&&data!=''){
		            		$("#warnings").css("display","block");
		            		$("#warnings input").val(data+":出品加工间未设置！");
		            	}
		            	else{
		            		$("#warnings").css("display","none");
		            		$("#warnings input").val("");
		            	}
		            }
				});
			});
  	 	});
  	 	function joinPlan(){
  	 		if($('#positn').val() == ''){
  	 			alert('<fmt:message key="Choose_a_processing_to_query" />！');
  	 			return;
  	 		}
			var rows = $('#test').datagrid('getSelections');
			if(rows==0){
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return;
			}
			var sp_positn = rows[0].SP_POSITION;
			var flag = true;
  	 		if(confirm('<fmt:message key="only_checked_saved_whether_continue"/>!')){
	  	 		var selected = {}; 
	  	 		selected['dat'] = $('#bdat').val();
	  	 		selected['datEnd'] = $('#edat').val();
				for(var i=0;i<rows.length;i++){
					if(rows[i].AMOUNT_XU<=0){
						alert("<fmt:message key="Do_not_check_the_need_to_process_the_amount_of_less_than_half_of_the_finished_product" />！");
						return;
					}
					//if(sp_positn != rows[i].SP_POSITION){
					//	flag = false;
					//	return;
					//}
					selected['explanDList['+i+'].supply.sp_code'] = rows[i].SP_CODE;
					selected['explanDList['+i+'].supply.positnex'] = rows[i].POSITNEX;
					selected['explanDList['+i+'].amount'] = rows[i].AMOUNT_XU;
				}
				if (!flag){   
					alert('<fmt:message key="cannot_choose_two_or_more_processing_supplies_reported_cargo"/>！');
					return;
				}
				$.post('<%=path%>/explanM/saveExplanByPlan.do',selected,function(data){
					var rs = eval('('+data+')');
					if(rs.pr=="succ"){
						alert('<fmt:message key="Successfully_joined_the_production_plan" />！');
						var action="<%=path%>/explanM/updateExplanByPlan.do";
						$('#listForm').attr('action',action);
						$('#listForm').submit();
					}
				});
  	 		}

  	 	}
  	 </script>
  </body>
</html>
