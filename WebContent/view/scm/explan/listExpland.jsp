<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>加工单生成领料单主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<style type="text/css">
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool"></div>
			<form id="listForm" action="<%=path%>/explanM/listExplanM.do" method="post">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 25px;"></span></td>
									<td><span style="width:100px;"><fmt:message key ="orders_num" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="supplies_code" /></span></td>
									<td><span style="width:120px;"><fmt:message key ="supplies_name" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="quantity" /></span></td>
									<td><span style="width:100px;"><fmt:message key ="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="expland" items="${explandList}" varStatus="status">
									<tr>
										<td class="num" ><span style="width: 25px;">${status.index+1}</span></td>
										<td><span style="width:100px;" title="${expland.explanno}">${expland.explanno}</span></td>
										<td><span style="width:100px;" title="${expland.supply.sp_code}">${expland.supply.sp_code}</span></td>
										<td><span style="width:120px;" title="${expland.supply.sp_name}">${expland.supply.sp_name}</span></td>
										<td><span style="width:100px;text-align:right;" title="${expland.amount}">${expland.amount}</span></td>
										<td><span style="width:100px;" title="${expland.memo}">${expland.memo}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),35);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
// 							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
						}
					}
				]
			});
		});
		</script>
	</body>
</html>