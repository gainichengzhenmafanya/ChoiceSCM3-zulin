<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>安全库存到计划</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	     	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
			<style type="text/css">
				.page{
					margin-bottom: 25px;
				}
				form .form-line .form-label{
					width: 5%;
				}
				form .form-line .form-input{
					width: 12%;
				}
				form .form-line .form-input input[type=text]{
					width: 85%;
				}
				form .form-line .form-input select{
					width: 95%;
				}
				.datagrid-sort-icon{
					background:none; 
				}
			</style>						
  </head>	
  <body>
  	<div class="tool"></div>
  		<form action="<%=path%>/explan/safeCntToPlan.do" id="listForm" name="listForm" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key ="processing_room" />：</div>
				<div class="form-input" style="width:200px;">
					<select class="select" id="positn" name="positn" style="width:180px;">
						<c:forEach var="positn" items="${listPositn}" varStatus="status">
							<option 
								<c:if test="${positn.code==supplyAcct.positn}"> 
										   	selected="selected"
								</c:if> 
								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
							</option>
						</c:forEach>
					</select>
				</div>
<%-- 				<div class="form-label"><fmt:message key="startdate"/></div> --%>
<%-- 				<div class="form-input"><input type="text" autocomplete="off" id="bdat" name="bdat" class="Wdate text" value="<fmt:formatDate value="${bdat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});"/></div> --%>
<!-- 				<div class="form-label"><fmt:message key ="branche" /></div> -->
<!-- 				<div class="form-input"> -->
<%-- 					<input type="text"  id="firmDes"  name="firmDes" value="${firmDes }" readonly="readonly"/> --%>
<%-- 					<input type="hidden" id="firmCode" name="firmCode" value="${firmCode }"/> --%>
<%-- 					<img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' /> --%>
<!-- 				</div> -->
				<div class="form-label"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${supplyAcct.sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>				
<!-- 			</div> -->
<!-- 			<div class="form-line"> -->
<%-- 				<div class="form-label"><fmt:message key="enddate"/></div> --%>
<%-- 				<div class="form-input"><input type="text" autocomplete="off" id="edat" name="edat" class="Wdate text" value="<fmt:formatDate value="${edat}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});"/></div> --%>
				<div class="form-label"><fmt:message key="smallClass"/>：</div>
				<div class="form-input">
					<select id="typ" name="typ" url="<%=path %>/grpTyp/findAllTyp.do"   class="select"></select>
				</div>			
			</div>
			<table id="test" style="width:'100%';" title="<fmt:message key="Safety_stock_to_plan" />" rownumbers="true" remoteSort="true"
				idField="SP_CODE" >
				<thead>
					<tr>
					<th field="ck" checkbox="true"></th>
					<th field="SP_CODE" width="80px;"><span id="SP_CODE"/><fmt:message key ="supplies_code" /></span></th>
					<th field="SP_NAME" width="80px;"><span id="SP_NAME"/><fmt:message key ="supplies_name" /></span></th>
					<th field="SP_DESC" width="80px;"><span id="SP_DESC"/><fmt:message key ="supplies_specifications" /></span></th>
					<th field="UNIT" width="50px;"><span id="UNIT"/><fmt:message key ="unit" /></span></th>
					<th field="TOTALBH" width="80px;"><span id="TOTALBH"/><fmt:message key ="Total_newspaper" /></span></th>
					<th field="CNT" width="80px;" align="right"><span id="CNT"/><fmt:message key ="current_inventory" /></span></th>
					<th field="SAFECNT" width="80px;" align="right"><span id="SAFECNT"/><fmt:message key ="scm_safety_stock" /></span></th>
					<th field="PLANCNT" width="80px;" align="right"><span id="PLANCNT"/><fmt:message key ="has_planned_amount" /></span></th>
					<th field="NEEDCNT" width="80px;" align="right"><span id="NEEDCNT"/><fmt:message key ="required_processing_capacity" /></span></th>
					<c:forEach var="positn" items="${columns}">
						<th field="F_${positn.code}" width="100px;" align="right"><span id="_${positn.code}"/>${positn.des}</span></th>
					</c:forEach>
					</tr>
				</thead>
			</table>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	 
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
			$("#typ").htmlUtils("select",[]);	
			focus() ;//页面获得焦点	
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 	});
  	 		$('.tool').toolbar({
				items: [{
						text: '<fmt:message key="select" />',
						title: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','-40px']
						},
						handler: function(){
// 							if(null==$('#bdat').val() || ""==$('#bdat').val() || null==$('#edat').val() || ""==$('#edat').val()){
// 								alert('日期不能为空！');
// 								return;
// 							}
							$('#listForm').submit();
						}
					},{
						text: '<fmt:message key ="added_to_the_production_arrangement" />',
						title: '<fmt:message key ="added_to_the_production_arrangement" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							 joinPlan();
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit" />',
						useable:true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}
				]
			});
  	 		setElementHeight('#test',['.tool'],$(document.body),100);
  	 		$('#test').datagrid({});
	  	 	var obj={};
			var rows = ${ListBean};
			obj['rows']=rows;
			$('#test').datagrid('loadData',obj);
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					typn:'1203',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
  	 	});
  	 	
  	 	//加入到生产计划
  	 	function joinPlan(){
			var rows = $('#test').datagrid('getSelections');
			if(rows==0){
				alert('<fmt:message key="please_select_options_you_need_save"/>！');
				return;
			}
  	 		if(confirm('<fmt:message key="only_checked_saved_whether_continue"/>!')){
	  	 		var selected = {}; 
				for(var i=0;i<rows.length;i++){
					selected['explanList['+i+'].supply.sp_code'] = rows[i].SP_CODE;
					selected['explanList['+i+'].cnt'] = rows[i].NEEDCNT;
				}
				$.post('<%=path%>/explan/saveExplanBySafeCnt.do',selected,function(data){
					parent.pageReload('parent');
				});
				
  	 		}

  	 	}
  	 	
  	 </script>
  </body>
</html>
