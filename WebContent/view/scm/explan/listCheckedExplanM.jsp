<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="query_messages_manifest"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{margin-bottom: 25px;}
			.search{
				margin-top:3px;
				cursor: pointer;
			}
			form .form-line:first-child{
				margin-left: 0px;
			}
			form .form-line .form-label{
				width: 90px;
			}
			form .form-line .form-input , form .form-line .form-input input[type=text]{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			} 
			.bgBlue{
				background: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<form id="listForm" action="<%=path%>/explanM/listCheckedexplanM.do" method="post">
			<div class="form-line">
				<div class="form-label"><fmt:message key="date_of_the_system_alone"/>：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd" type="date"/>"/></div>						
				<div class="form-label"><fmt:message key="orders_num"/>：</div>
				<div class="form-input"><input type="text" id="explanno" name="explanno" class="text" value="${explanM.explanno}" onkeyup="value=value.replace(/[^\d]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))"/></div>
				<div class="form-label"><fmt:message key="supplies_code"/>：</div>
				<div class="form-input">
					<input type="text" id="sp_code" name="sp_code" class="text" onkeyup="ajaxSearch('sp_code')" value="${sp_code }"/>
					<img id="seachSupply" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
 				<div class="form-label" style="width:80px;margin-left:30px;"><fmt:message key="processing_room"/>：</div>
				<div class="form-input">
					<input type="text"  id="positnDes" name="positn.des" readonly="readonly" value="${explanM.positn.des}"/>
					<input type="hidden" id="positn" name="positn.code" value="${explanM.positn.code}"/>
					<img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
					
<%-- 					<input type="text" class="text"  id="positn_select" onfocus="this.select()" style="width:40px;margin-top:4px;vertical-align:top" value="${explanM.positn.code}"/> --%>
<!-- 					<input type="hidden" class="text"  id="positnDes" /> -->
<!-- 					<input type="hidden" class="text"  id="typ" name="typ" value="A"/> -->
<!-- 					<select class="select" id="positn" name="positn.code" style="width:180px;" onchange="findPositnByDes(this);"> -->
<%-- 						<c:forEach var="positn" items="${listPositn}" varStatus="status"> --%>
<!-- 							<option  -->
<%-- 								<c:if test="${positn.code==explanM.positn.code}">  --%>
<!-- 										   	selected="selected" -->
<%-- 								</c:if>  --%>
<%-- 								id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des} --%>
<!-- 							</option> -->
<%-- 						</c:forEach> --%>
<!-- 					</select> -->
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="Finish_date" />：</div>
				<div class="form-input"><input autocomplete="off" type="text" id="hoped" name="hoped" class="Wdate text" value="<fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd" type="date"/>"/></div>
				<div class="form-label"><fmt:message key="orders_maker"/>：</div>
				<div class="form-input"><input type="text" id="madeby" name="madeby" class="text" value="${explanM.madeby}"/></div>
				<div class="form-label"><fmt:message key="orders_audit"/>：</div>
				<div class="form-input"><input type="text" id="checby" name="checby" class="text" value="${explanM.checby}"/></div>
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:25px;"></span></td>
								<td><span style="width:20px;"></span></td>
								<td><span style="width:70px;"><fmt:message key="orders_num"/></span></td>
								<td><span style="width:100px;"><fmt:message key="date_of_the_system_alone"/></span></td>
								<td><span style="width:130px;"><fmt:message key="Finish_date" /></span></td>
								<td><span style="width:100px;"><fmt:message key="processing_room"/></span></td>
<%-- 								<td><span style="width:80px;"><fmt:message key="total_amount1"/></span></td> --%>
								<td><span style="width:100px;"><fmt:message key="orders_maker"/></span></td>
								<td><span style="width:80px;"><fmt:message key="orders_audit"/></span></td>
<%-- 								<td><span style="width:80px;"><fmt:message key="whether_upload"/></span></td> --%>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="explanM" items="${explanMList }" varStatus="status">
								<tr>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;"><input type="checkbox" name="idList" id="chk_${explanM.explanno }" value="${explanM.explanno }"/></span></td>									
									<td><span style="width:70px;text-align: right;">${explanM.explanno }</span></td>
									<td><span style="width:100px;"><fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:130px;"><fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd" type="date"/></span></td>
									<td><span style="width:100px;">${explanM.positn.des }</span></td>
<%-- 									<td><span style="width:80px;text-align: right;"><fmt:formatNumber value="${explanM.totalAmt }" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td> --%>
									<td><span style="width:100px;">${explanM.madeby }</span></td>
									<td><span style="width:80px;">${explanM.checby }</span></td>
<%-- 									<td><span style="width:80px;">${explanM.bak1}</span></td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>				
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		var validate;
		$(document).ready(function(){
			$('#maded').bind('click',function(){
				new WdatePicker();
			});
			$('#hoped').bind('click',function(){
				new WdatePicker();
			});
			if($('#explanno').val()==0)$('#explanno').val('');
			/*过滤*/
			$('#positnDes').bind('keyup',function(){
		          $("#positn").find('option')
		                    .filter(":contains('"+($(this).val().toUpperCase())+"')")
		                    .attr('selected','selected');
		    });
			//双击
			$('.grid .table-body tr').live('dblclick',function(){
				$(":checkbox").attr("checked", false);
				$(this).find(":checkbox").attr("checked", true);
				searchCheckedexplanM();
			});
			//按钮快捷键
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode){
	                case 70: $('.<fmt:message key="select"/>').click(); break;
	                case 80: $('.<fmt:message key="print"/>').click(); break;
	            }
			}); 
			   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
			     }
			     else
			     {
			    	 $('.grid').find(":checkbox").attr("checked", false);
			         $('.bgBlue').removeClass("bgBlue");
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
			     }
			});
			
		 	$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="select" />(<u>F</u>)',
					title: '<fmt:message key="select"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['0px','-40px']
					},
					handler: function(){
						$("#listForm").submit();
					}
				},{
					text: '<fmt:message key="view" />(<u>V</u>)',
					title: '<fmt:message key="view"/>',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-140px','-100px']
					},
					handler: function(){
						searchCheckedexplanM();
					}
				},{
					text: '<fmt:message key="uncheck" />',
					title: '<fmt:message key="uncheck" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-58px','-240px']
					},
					handler: function(){
						uncheckexplanM();
					}
				},{
					text: '<fmt:message key="quit" />',
					title: '<fmt:message key="quit"/>',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						position: ['-160px','-100px']
					},
					handler: function(){
						invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
					}
				}]
			});
		 	/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'explanno2',
					validateType:['canNull','intege'],
					param:['T'],
					error:['','<fmt:message key="single_number_is_digital_or_empty"/>！']
				}]
			});
		    $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法			
			changeTh();
			$('#seachOnePositn').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positn').val();;
					var defaultName = $('#positnDes').val();
					var offset = getOffset('explanno');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold=one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positn'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();;
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
		});
		var detailWin;
		//查看已审核单据的详细信息
		function searchCheckedexplanM(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
					var chkValue = checkboxList.filter(':checked').val();
					detailWin = $('body').window({
						id: 'checkedexplanMDetail',
						title: '<fmt:message key ="The_query_has_audit_jiagong_note_details" />',
						content: '<iframe id="checkedexplanMDetailFrame" frameborder="0" src="<%=path%>/explanM/searchCheckedexplanM.do?explanno='+chkValue+'"></iframe>',
						width: $(document.body).width()-20/* '1050px' */,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
			}else{
				alert('<fmt:message key="please_select_data_to_view"/>！');
				return ;
			}
		}
		//反审核
		function uncheckexplanM(){
			if(!confirm('<fmt:message key="Determine_the_reverse_audit"/>？'))return;
			var data = {};
	        data['explanno'] = $('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
	        
	        //wangjie 2014年11月5日 13:54:40  （报货单填制提交，未选择数据，点反审核 无反应，不提示。）
	        if(data['explanno'] == 'undefined' || data['explanno'] == undefined){
	        	alert('<fmt:message key="please_select"/>1<fmt:message key="article"/><fmt:message key="data"/><fmt:message key="uncheck"/>!');
	        }else{
	        	//判断是否已经采购确认，采购审核，是否已经出库或入库
		    	$.post("<%=path%>/explanM/checkYnUnChk.do",data,function(data){
					if(data<0){
						msg="no";
						alert('<fmt:message key="Anti-picking-has-not-reviewed-the-documents"/>！');
						return;
					}else if (data>0){
						msg="no";
						alert('<fmt:message key="there_has_been_the_purchase_confirmation_or_procurement_audit_materials"/>,<fmt:message key="can_not"/><fmt:message key="uncheck"/>！');
						return;
					}else{
						var explanno=$('.grid').find('.table-body').find(':checkbox').filter(':checked').val();
						if(explanno!=null && explanno!=""){
							unchkeck(explanno);
						}
					}
				});
	        }
		}
		
		function unchkeck(explanno){
			//提交并返回值，判断执行状态
			$.post("<%=path%>/explanM/uncheckexplanM.do?explanno="+explanno,function(data){
				switch(data){
				case 1:
					alert('<fmt:message key="uncheck"/><fmt:message key="successful"/>！');
					$('#listForm').submit();
					break;
				case 0:
					alert('<fmt:message key="uncheck"/><fmt:message key="failure"/>！');
					break;
				}
			});
		}
		
		//打印单条
		function printexplanM(){
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			if(checkboxList && checkboxList.filter(':checked').size() ==1){
				var chkValue = [];
				checkboxList.filter(':checked').each(function(){
					chkValue.push($(this).val());
				});
				window.open ("<%=path%>/explanM/printexplanM.do?explanno="+chkValue.join(","),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}else{
				alert('<fmt:message key="please_select_single_message_to_print"/>！');
				return ;
			}
		}
		</script>				
	</body>
</html>