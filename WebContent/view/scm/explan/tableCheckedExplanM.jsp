<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="audited_reported_manifest"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			
		<style type="text/css">
		.page{
			margin-bottom: 25px;
		}
		.test{
			color:blue;
		}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<%--存放一个状态 判断是何种操作类型 --%>
		<input type="hidden" id="sta" name="sta" value="${sta }"/>
		<form id="listForm" action="<%=path%>/chkstom/saveByAdd.do" method="post">
			<div class="bj_head">
				<div class="form-line">
			<div class="form-label"><fmt:message key="date_of_the_system_alone"/>：</div>
			<div class="form-input" style="width:220px;">
				<input autocomplete="off" type="text" id="maded" name="maded" style="width:224px;" disabled="disabled" class="Wdate text" value="<fmt:formatDate value="${explanM.maded}" pattern="yyyy-MM-dd"/>"/>
			</div>
			<div class="form-label" ><fmt:message key="orders_num"/>:</div>
			<div class="form-input">
				<c:if test="${explanM.explanno!=null}"><c:out value="${explanM.explanno}"></c:out></c:if>
				<input type="hidden" name="explanno" id="explanno" value="${explanM.explanno }"/>				
			</div>												
		</div>
		<div class="form-line" style="z-index:12">	
			<div class="form-label"><fmt:message key="Finish_date" />：</div>
			<div class="form-input" style="width:230px;">
				<input autocomplete="off" type="text" id="hoped" name="hoped" style="width:224px;" disabled="disabled" class="Wdate text" value="<fmt:formatDate value="${explanM.hoped}" pattern="yyyy-MM-dd"/>"/>
			</div>
			<div class="form-label"><fmt:message key="orders_maker"/>:</div>
			<div class="form-input" style="width:220px;">
				<c:if test="${explanM.madeby!=null}"><c:out value="${explanM.madeby}"></c:out></c:if>
				<input type="hidden" name="madeby" id="madeby" class="text" value="${explanM.madeby}" />			
			</div>		
		</div>
		<div class="form-line" style="z-index:12">	
			<div class="form-label"><fmt:message key="processing_room"/>：</div>
			<div class="form-input" style="width:240px;">
<%-- 				<input type="text" class="text" style="width:40px;margin-top:4px;vertical-align:top" value="${explanM.positn}"/> --%>
				<input type="hidden" class="text"  id="positnDes" />
				<input type="hidden" class="text"  id="typ" name="typ" value="A"/>
				<select class="select" id="positn" name="positn.code" style="width:180px;" disabled="disabled">
					<c:forEach var="positn" items="${positnList}" varStatus="status">
						<option 
							<c:if test="${positn.code==explanM.positn}"> 
									   	selected="selected"
							</c:if> 
							id="${positn.code}" value="${positn.code}">${positn.code}-${positn.init}-${positn.des}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-label" style="width:100px;">审核人:</div>
			<div class="form-input">
				<c:if test="${explanM.checby!=null}"><c:out value="${explanM.checby}"></c:out></c:if>
				<input type="hidden" name="checby" id="checby" class="text" value="${explanM.checby}" />			
			</div>						
		</div>			
			</div>
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td colspan="3"><span><fmt:message key="supplies"/></span></td>
								<td colspan="2"><span><fmt:message key="standard_unit"/></span></td>
<%-- 								<td rowspan="2"><span style="width:110px;"><fmt:message key="arrival_date"/></span></td> --%>
								<td rowspan="2"><span style="width:90px;"><fmt:message key="remark"/></span></td>
							</tr>
							<tr>									
								<td class="num"><span style="width: 25px;">&nbsp;</span></td>
								<td><span style="width:90px;"><fmt:message key="supplies_code"/></span></td>
								<td><span style="width:160px;"><fmt:message key="supplies_name"/></span></td>
								<td><span style="width:90px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:75px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:90px;"><fmt:message key="quantity"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
				<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
				<div class="table-body" style="height: 100%">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach var="expland" items="${explandList}" varStatus="status">
							<tr>
								<td class="num"><span style="width:25px;">${status.index+1}</span></td>
								<td ><span style="width:90px;">${expland.supply.sp_code }</span></td>
								<td ><span style="width:160px;">${expland.supply.sp_name }</span></td>
								<td ><span style="width:90px;">${expland.supply.sp_desc }</span></td>
								<td ><span style="width:75px;">${expland.supply.unit }</span></td>
								<td ><span style="width:90px;text-align: right;">${expland.amount }</span></td>
<%-- 								<td ><span style="width:110px;">${expland.hoped}</span></td> --%>
								<td ><span style="width:90px;">${expland.memo }</span></td>								
							</tr>
							<c:set var="sum_num" value="${status.index+1}"/>  
							<c:set var="sum_amount" value="${sum_amount + expland.amount}"/>   
						</c:forEach>
						</tbody>
					</table>					
				</div>
			</div>
			<div style="height:10px;">	
				<table border="0" cellspacing="0" cellpadding="0" style="margin-top:0;z-index:0;height:10px">
					<thead>
						<tr>
							<td style="width:26px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;background:#F1F1F1;"><fmt:message key="total"/><u>&nbsp;&nbsp;<label id="sum_num"><fmt:formatNumber value="${sum_num}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:80px;"></td>
							<td style="width:65px;"></td>
							<td style="width:80px;background:#F1F1F1;"><fmt:message key="quantity"/><u>&nbsp;&nbsp;<label id="sum_amount"><fmt:formatNumber value="${sum_amount}" type="currency" pattern="0.00"/></label>&nbsp;&nbsp;</u></td>
							<td style="width:65px;"></td>
							<td style="width:80px;"></td>
							<td style="width:100px;"></td>
							<td style="width:80px;"></td>
						</tr>
					</thead>
				</table>
		   </div>
		</form>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript">		
		//工具栏
		$(document).ready(function(){
			//屏蔽鼠标右键
			//$(document).bind('contextmenu',function(){return false;});
			//键盘事件绑定
			focus() ;//页面获得焦点
		 	$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			parent.$('.close').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 80: $('#autoId-button-101').click(); break;//打印
	            }
			}); 
			//判断按钮的显示与隐藏
			loadToolBar([true]);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		    $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),121);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
		});
		//控制按钮显示
		function loadToolBar(use){
			$('.tool').html('');
			$('.tool').toolbar({
				items: [{
// 						text: '<fmt:message key="print" />(<u>P</u>)',
// 						title: '<fmt:message key="print"/>',
// 						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')}&&use[0],
// 						icon: {
<%-- 							url: '<%=path%>/image/Button/op_owner.gif', --%>
// 							position: ['-140px','-100px']
// 						},
// 						handler: function(){
// 							printChkstom();
// 						}
// 					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();						
						}
					}
				]
			});
		}			
		//打印单据
		function printChkstom(){ 
			if(null!=$("#chkstoNo").val() && ""!=$("#chkstoNo").val())
			{
				window.open ("<%=path%>/chkstom/printChkstom.do?chkstoNo="+$("#chkstoNo").val(),'newwindow','height='+window.screen.height+',width='+window.screen.width+',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
			}
		}
		</script>			
	</body>
</html>