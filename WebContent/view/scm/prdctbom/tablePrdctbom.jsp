<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="semi_finished_products_cost_card"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.textDisable{
					border: 0;
					background: #FFF;
				}
				form .form-line .form-label{
					width: 9%;
				}
				form .form-line .form-input{
					width: 14%;
				}
				form .form-line .form-input input[type=text]{
					width:80%;
				}
			</style>
	</head>	
	<body>
		<div class="tool">
		</div>
		<form id="tableForm" name="tableForm" method="post" action="<%=path%>/prdctbom/saveOrUpdateOrDelPrdctbom.do?code=${item}" >
		<div class="bj_head">
			<input type="hidden" id="item" name="item" value="${item}"/>
			<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
			<div class="form-line">
				<div class="form-label"><fmt:message key="coding"/>:</div>
				<div class="form-input"><input type="text" id="sp_code" name="supply.sp_code" readonly="readonly"  value="${prdctbom.supply.sp_code}" class="text" /></div>	
				<div class="form-label"><fmt:message key="standard_unit"/>:</div>
				<div class="form-input"><input type="text" id="unit" name="supply.unit" readonly="readonly"  value="${prdctbom.supply.unit}" class="text" />
				</div>
				<div  class="form-label"><fmt:message key="reference_unit"/>:</div>
				<div  class="form-input"><input  type="text" id="unit1" name="supply.unit1" readonly="readonly"  value="${prdctbom.supply.unit1}" class="text"  /></div>
				<div class="form-label"><fmt:message key="reclaimer_rate"/>:</div>
				<div class="form-input"><input type="text" id="exrate" name="exrate" readonly="readonly" value="<c:out value="${prdctbom.exrate}" default="1.0"/>" class="text" />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><fmt:message key="name"/>:</div>
				<div  class="form-input"><input type="text" id="sp_name" name="supply.sp_name" readonly="readonly"  value="${prdctbom.supply.sp_name}" class="text" />
				</div>
				<div class="form-label"><fmt:message key="cost_quantity"/>:</div>
				<div class="form-input"><input type="text" id="excnt" name="excnt" readonly="readonly" class="text" value="${prdctbom.excnt}" maxlength='11'/><label class="unit2"></label>
				</div>	
				<div  class="form-label"><fmt:message key="reference_number"/>:</div>
				<div class="form-input"><input type="text" id="cnt" name="cnt" readonly="readonly" class="text" value="${prdctbom.cnt}" /><label class="unit1"></label>
				</div>		
			</div>
		</div>
		</form>

		<form id="listForm" method="post" action="<%=path%>/prdctbom/save.do">
		<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
		<%-- update by jinshuai at 20160406 存放左侧条件 在导出excel的时候使用 --%>
		<input type="hidden" name="lefttj" id="lefttj" value="" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 20px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:20px;"></span>
<!-- 									<input type="checkbox" id="chkAll"/></span> -->
								</td>
								<td colspan="4"><span style="width:180px;"><fmt:message key="supplies"/></span></td>
								<td colspan="3"><span style="width:140px;"><fmt:message key="cost_unit"/></span></td>
								<td colspan="4"><span style="width:180px;"><fmt:message key="standard_unit"/></span></td>
								<td colspan="2"><span style="width:90px;"><fmt:message key="reference_unit"/></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><fmt:message key="coding"/></span></td>
								<td><span style="width:70px;"><fmt:message key="name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:40px;"><fmt:message key="reclaimer_rate"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="net_usage"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						<c:set value="0" var="totalSum"></c:set>
							<c:forEach var="product" items="${productList}" varStatus="status">
								<input type="hidden" class="ex" value="${product.ex }"/> <!-- 判断报表导出的是门店bom还是总部bom wjf -->
								<input type="hidden" class="ex1" value="${product.ex1 }"/>
								<tr>
									<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
									<td><span style="width:20px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${product.supply.sp_code}" value="${product.supply.sp_code}"/></span>
									</td>
									<td id="sp_code"><span id="sp_code" title="${product.supply.sp_code}" style="width:70px;text-align: left;">${product.supply.sp_code}&nbsp;</span></td>
									<td><span title="${product.supply.sp_name}" style="width:70px;text-align: left;"><a href="javascript:newWindow('${product.supply.sp_code}');"  style="text-decoration: none;" >${product.supply.sp_name}</a></span></td>
									<td><span title="${product.supply.sp_desc}" style="width:70px;text-align: right;">${product.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${product.exrate}" style="width:40px;text-align: right;"><fmt:formatNumber value="${product.exrate}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit2}" style="width:30px;text-align: left;">${product.supply.unit2}&nbsp;</span></td>
									<td><span title="${product.excnt}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.excnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td><span title="${product.cnt2}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt2}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit}" style="width:40px;text-align: left;">${product.supply.unit}&nbsp;</span></td>
									<td><span title="${product.cnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.pricesale}" style="width:40px;text-align: right;"><fmt:formatNumber value="${product.supply.pricesale}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></span></td>
									<td><span title="${product.sale}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.sale}"  pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit1}" style="width:30px;text-align: left;">${product.supply.unit1}&nbsp;</span></td>
									<td><span title="${product.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt1}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
								</tr>
								<c:set var="totalSum" value="${product.sale+totalSum}"></c:set>
							</c:forEach>
						</tbody>
						<c:if test="${totalSum!=0 }">
						  <tfoot>
							<tr>
								<td colspan="2"><span></span></td>
								<td style="width:80px;"><span><fmt:message key="total"/></span></td>
								<td colspan="9"><span></span></td>
								<td style="width:60px;"><span style="width:50px;text-align: right;"><fmt:formatNumber value="${totalSum}" pattern="##.##" minFractionDigits="2"></fmt:formatNumber></span></td>
								<td colspan="2"><span></span></td>
							</tr>							
						  </tfoot>
						</c:if>
					</table>
				</div>
			</div>
		</form>
			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
			var status = $("#curStatus").val();
			<%-- curStatus 0:init;1:edit(E);2:add(A);3:show 4,delete(D)--%>
			//判断按钮的显示与隐藏
			if(status == 'A'){
				loadToolBar([false,false,false,false,false,true,false]);
			}else if(status == 'show'){//查询页面双击返回
				loadToolBar([false,false,false,true,true,false,true]);
				//$('#chk1memo').attr('disabled',false);
			}else{//init
				loadToolBar([true,false,false,false,false,true,true]);
			}
			
			//按钮快捷键
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 65: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 68: $('#autoId-button-103').click(); break;
	                case 83: $('#autoId-button-104').click(); break;
					case 80: $('#autoId-button-106').click(); break;
				}
	 		}); 
			//双单 击事件
			$('.grid .table-body tr').live('dblclick click',function(){
				 $('#sp_code').val($(this).find('td:eq(2)').find('span').attr('title'));
				 $('#sp_name').val($(this).find('td:eq(3)').find('span').attr('title'));
				 $('#unit').val($(this).find('td:eq(9)').find('span').attr('title'));
				 $('#unit1').val($(this).find('td:eq(13)').find('span').attr('title'));
				 $('#excnt').val($(this).find('td:eq(7)').find('span').attr('title'));
				 $('#exrate').val($(this).find('td:eq(5)').find('span').attr('title'));
				 $('#cnt').val($(this).find('td:eq(14)').find('span').attr('title'));
				 $('.unit2').html($(this).find('td:eq(6)').find('span').attr('title'));
				 $('.unit1').html($(this).find('td:eq(13)').find('span').attr('title'));
				 $(":checkbox").attr("checked", false);
				 $(this).find(":checkbox").attr("checked", true);
				 loadToolBar([true,true,true,false,false,true,true]);
			});

			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'excnt',
					validateType:['canNull','num2'],
					param:['F','F'],
					error:['<fmt:message key="cannot_be_empty"/>！','<fmt:message key="Please_enter_a_number_greater_than_0" />！']
				},{
					type:'text',
					validateObj:'cnt',
					validateType:['num2'],
					param:['T'],
					error:['<fmt:message key="please_enter_the_number"/>！']
				},{
					type:'text',
					validateObj:'exrate',
					validateType:['canNull','num2','minValue'],
					param:['F','F','0'],
					error:['<fmt:message key="reclaimer_rate"/><fmt:message key="cannot_be_empty"/>！','请输入大于0的数字！','请输入大于0的数字！']
				}]
			});
			
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),58);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('input:text[readonly]').not("#cnt,#excnt,#exrate").addClass('textDisable');		//不可编辑颜色
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   	$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		});
		/*弹出物资树*/
		function supply_select(){
			if(!!!top.customWindow){
				//修改成本单位不显示问题，但是此处显示的只是标准单位，还不是成本单位，原因：custom.js有问题，根本没有返回成本单位 wjf
				top.customSupplyUnit2('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?ex=Y',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit2'),$('.unit1'),$('.unit2'),handler);	
				//top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?ex=Y',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),handler);	
			}
			if($("#exrate").val()==""){
				$("#exrate").val("1.0");
			}
		};
		
		{
			
		}
		//弹出物资树回调函数
		function handler(sp_code){
// 			$('#sp_code').val(sp_code_); 
			if(sp_code==undefined || ''==sp_code){
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/supply/findById.do",
				data: "sp_code="+sp_code,
				dataType: "json",
				success:function(supply){
					//alert(supply.ynex);
					if('Y'==supply.ex1||'Y'==supply.ex){
						//update by js at 20160322 去掉半成品BOM不能加半成品
						//alert('<fmt:message key="Please_choose_a_non_processed_product" />！');
						loadToolBar([false,false,false,true,true,true,true]);
						$("#curStatus").val("A");
						$("#cnt,#excnt,#exrate").removeAttr('readonly');
					}else{
						loadToolBar([false,false,false,true,true,true,true]);
						$("#curStatus").val("A");
						$("#cnt,#excnt,#exrate").removeAttr('readonly');
					}
				}
			});
		}
		function loadToolBar(use){
			$('.tool').html('');
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="insert" />(<u>A</u>)',
						title: '<fmt:message key="new_cost_card_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							$('#tableForm input[type=text]').val('');
							$('#excnt').siblings('.validateMsg').remove();
							$('#exrate').siblings('.validateMsg').remove();
							supply_select();
						}
					},{
						text: '<fmt:message key ="copy" />',
						title: '<fmt:message key ="copy" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[5],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							copyCostbom();
						}
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="editing_materials_with_material_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							$('#tableForm #excnt').focus();
							$("#curStatus").val("E");
							loadToolBar([false,false,false,true,true,true,true]);
							$("#cnt,#excnt,#exrate").removeAttr('readonly');
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="deleted_material_material_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="delete_data_confirm"/>？')){
// 								if(validate._submitValidate()){
									$("#curStatus").val("D");
									$('#tableForm').submit();
// 								}
							}					
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save_cost_card_information"/>',
						useable: use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var curStatus=$("#curStatus").val();
							if(curStatus=='A'){
								var cur_a='1';
								$('.grid').find('.table-body').find('tr').each(function(i){
									if($(this).find('td:eq(2)').find('span').attr('title')==$('#sp_code').val()){
										cur_a='0';
										return;
									}
								});
								if(cur_a!='1'){
									alert('<fmt:message key="added_supplies_remind"/>！');
									return;
								}
							}
							if(validate._submitValidate()){
								$('#tableForm').submit();
							}
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel_cost_card_information"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							$("#curStatus").val("init");
							$('#tableForm input[type=text]').val('');
							$('#excnt').siblings('.validateMsg').remove();
							$('#exrate').siblings('.validateMsg').remove();
							$(":checkbox").attr("checked", false);
							loadToolBar([true,false,false,false,false,true,true]);
						}
					},{
						  text: 'Excel',
						  title: 'Excel',
						  useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
					      icon: {
						  		url: '<%=path%>/image/Button/op_owner.gif',
						  		position: ['-40px','-20px']
					      		},
							handler: function(){
								
								//update by jinshuai at 20160406
								//增加左侧条件导出
								//编码
								var parent = window.parent.document;
								var jiansuo = parent.getElementById("jiansuo").value;
								//全部 已做 未做
								var is_qb = parent.getElementById("is_qb").checked;
								var is_qr = parent.getElementById("is_qr").checked;
								var is_wz = parent.getElementById("is_wz").checked;
								//判断选的是哪一个
								var qyw = "QB";
								if(is_qb){
									qyw = "QB";
								}else if(is_qr){
									qyw = "QR";
								}else if(is_wz){
									qyw = "WZ";
								}
								var allstr = jiansuo+":"+qyw;
								//把数据放到隐藏域
								$("#lefttj").val(allstr);
								
								
									$("#wait2").val('NO');//不用等待加载
									var ex = $('.ex').first().val();
									var ex1 = $('.ex1').first().val();
									$('#listForm').attr('action','<%=path%>/prdctbom/export.do?ex='+ex+'&ex1='+ex1);
									$('#listForm').submit();
									$("#wait2 span").html("loading...");
									$('#listForm').attr('action','<%=path%>/prdctbom/table.do');
									$("#wait2").val('');//等待加载还原
					 
							}
						},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/prdctbom/print.do?item=${item}";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/prdctbom/tableBycode.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						useable: use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
			});
		};
		
		/**成本数量，参考数量 小数位数限制 wjf**/
		$("#excnt,#cnt").bind('blur',function(e){
			$(this).val(Number($(this).val()).toFixed(2));
		});
		function newWindow(sp_code){ 
			var url = "<%=path%>/prdctbom/select.do?supply.sp_code=" + sp_code; 
			parent.$('body').window({
				id: 'window_forPlan',
					title: '使用该物资的半成品',
					content: '<iframe id="saveColumnFrame" frameborder="0" src=' + url + '></iframe>',
					width: 1000,
					height: 400,
					draggable: true,
					isModal: true
			});
		}
		
		//复制bom
		function copyCostbom(){
			var item = "";
// 			var mods = "";
// 			$('#item').val(parent.$('#item').val());
			if ($('#item').val()==null || $('#item').val()=='') {
				alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
				return;
			}
			item = $('#item').val();//菜品编码
			$('body').window({
				id: 'window_supply',
				title: '<fmt:message key ="Copy_cost_card" />',
				content: '<iframe id="saveCostbom" frameborder="0" src="<%=path%>/prdctbom/toCopy.do?item='+item+'"></iframe>',
				width: '550px',
				height: '400px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key ="enter" />',
							title: '<fmt:message key="save" /><fmt:message key="supplies_code" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if ($('#item').val()==null || $('#item').val()=='') {
									alert('<fmt:message key="the_lack_of_food_data_code"/>,<fmt:message key="operation_failed"/>！');
								}else if(getFrame('saveCostbom') && window.document.getElementById("saveCostbom").contentWindow.checkValue()){
									parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
								}
								parent.$('.grid').find('.table-body').find('tr:eq(0)').click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}
		</script>
	</body>
</html>