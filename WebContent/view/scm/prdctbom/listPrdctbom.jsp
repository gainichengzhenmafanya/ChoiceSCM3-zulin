<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>prdctbom Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.leftFrame{
/* 				width:23.5%; */
				width:30%;
			}
			.mainFrame{
/* 				width:76.5%; */
				width:70%;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			#modDiv{
				border: 1px solid #5A7581;
				margin: 1px 1px 1px 1px;
			 	padding: 1px;
			 	padding-left: -8px;
			 	background-color:#CDDCEE;
			}
		</style>
	</head>
	<body>
		<div class="leftFrame">
      		<div id="modDiv">
	      		<form id="conditionForm" method="post">
					<div class="table-head_" style="background-color:#CDDCEE;">
						<div class="form-line">
							<div class="form-input">
								<fmt:message key="retrieval"/>：
								<input type="text" id="jiansuo" style="width:80%;" onkeyup="changeSelect(this.value);" onkeydown="javascript: if(event.keyCode==32){return false;}"/>
							</div>
						</div>
					   <div class="form-line">		
							<div class="form-input">
								<input type="radio" id="is_qb" name="prdctcard" value="QB" checked="checked" onclick="changeSelect(null);"/><fmt:message key="all"/>
								<input type="radio" id="is_qr" name="prdctcard" value="QR" onclick="changeSelect(null);"/><fmt:message key="been_made_cost_card"/>
						    	<input type="radio" id="is_wz" name="prdctcard" value="WZ" onclick="changeSelect(null);"/><fmt:message key="not_been_made_cost_card"/>
					    	</div>	
				    	</div>
					</div>
				</form>
			</div>
			<form id="listForm" method="post" action="">
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num" ><span style="width: 20px;">&nbsp;</span></td>
									<td><span style="width:65px;"><fmt:message key="coding"/>&nbsp;</span></td>
									<td><span name="name"><fmt:message key="name"/>&nbsp;</span></td>
									<td><span style="width:30px;"><fmt:message key="unit"/>&nbsp;</span></td>
									<td><span style="width:60px;"><fmt:message key="specifications"/>&nbsp;</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="supply" items="${supplyList}" varStatus="status">
									<tr>
										<td class="num" ><span style="width:20px;">${status.index+1}</span></td>
										<td><span title="${supply.sp_code}" style="width:65px;" >${supply.sp_code}</span></td>
										<td><span name="name" title="${supply.sp_name}">${supply.sp_name}&nbsp;</span></td>
										<td><span title="${supply.unit}" style="width:30px;">${supply.unit}&nbsp;</span></td>
										<td><span title="${supply.sp_desc}" style="width:60px;">${supply.sp_desc}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
    	<div class="mainFrame">
      		<iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    	</div>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript">
		var spcode=null;
		var h = $(".main",top.document).parent("div[region='center']").height() - $(".tab-control",top.document).height();
		$(document).ready(function(){
			$('body').height(h);
			//自动实现滚动条
			setElementHeight('.grid',['.moduleInfo'],$(document.body),60);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);

			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
				$(this).addClass('tr-select');
				$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-select');
				spcode=$(this).find('td:eq(1)').find('span').attr('title');
		        window.mainFrame.location = "<%=path%>/prdctbom/tableBycode.do?item="+spcode;
			 });
			$('.grid').find('.table-body').find('tr:eq(0)').click();
			var widthds = $('.grid').width()/4;
			$('span[name=name]').width(widthds);
		});
				
		//当缩写码的值发生改变时执行
		function changeSelect(e)
		{
			var key='';
			var code='';
			if (null!=e) {
				if(isNaN(e)){
					key=e;
				}else{
					code=e;
				}
			}
			var prdctcard = $('input[name="prdctcard"]:checked').val();
			$.ajax({
				type: "POST",
				url: '<%=path%>/prdctbom/findByKeyAjax.do',
				data: "key="+key+"&prdctcard="+prdctcard+"&code="+code,
				dataType: 'json',
				success:function(supplyList){
					$('.table-body').find('table').find('tbody').empty();
					for(var i=0;i<supplyList.length;i++){
						$('.table-body').find('table').find('tbody').append('<tr><td class="num" ><span style="width: 20px;">'
							+(i+1)+'</span></td><td><span title="'+supplyList[i].sp_code+'" style="width:65px;">'
							+supplyList[i].sp_code+'</span></td><td><span title="'+supplyList[i].sp_name+'" style="width:'+($('.grid').width()/4-0.5)+'px;">'
							+supplyList[i].sp_name+'&nbsp;</span></td><td><span title="'+supplyList[i].unit+'" style="width:30px;">'
							+supplyList[i].unit+'&nbsp;</span></td></tr>');
					}
				},
				error: function(){
					alert('<fmt:message key="server_busy_please_try_again"/>');
					}
				});
			  }
		</script>
	</body>
</html>