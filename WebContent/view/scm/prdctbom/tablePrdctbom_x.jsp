<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="semi_finished_products_cost_card"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<style type="text/css">
				.textDisable{
					border: 0;
					background: #FFF;
				}
				form .form-line .form-label{
					width: 9%;
				}
				form .form-line .form-input{
					width: 14%;
				}
				form .form-line .form-input input[type=text]{
					width:80%;
				}
			</style>
	</head>	
	<body>
		<div class="tool">
		</div>
		<form id="tableForm" name="tableForm" method="post" action="<%=path%>/prdctbom/saveOrUpdateOrDelPrdctbom.do?code=${item}" >
		<div class="bj_head">
			<input type="hidden" id="item" name="item" value="${item}"/>
			<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
			<div class="form-line">
				<div class="form-label"><c:if test="${is_supply_x=='Y'}"><fmt:message key ="virtual" /></c:if><fmt:message key="coding"/>:</div>
				<div class="form-input"><input type="text" id="sp_code" name="supply.sp_code" readonly="readonly"  value="${prdctbom.supply.sp_code}" class="text" /></div>	
				<div class="form-label"><fmt:message key="standard_unit"/>:</div>
				<div class="form-input"><input type="text" id="unit" name="supply.unit" readonly="readonly"  value="${prdctbom.supply.unit}" class="text" />
				</div>
				<div  class="form-label"><fmt:message key ="the_reference_unit" />:</div>
				<div  class="form-input"><input  type="text" id="unit1" name="supply.unit1" readonly="readonly"  value="${prdctbom.supply.unit1}" class="text"  /></div>
				<div class="form-label"><fmt:message key="reclaimer_rate"/>:</div>
				<div class="form-input"><input type="text" id="exrate" name="exrate" readonly="readonly" value="<c:out value="${prdctbom.exrate}" default="1.0"/>" class="text" />
				</div>
			</div>
			<div class="form-line">
				<div class="form-label"><c:if test="${is_supply_x=='Y'}"><fmt:message key ="virtual" /></c:if><fmt:message key="name"/>:</div>
				<div  class="form-input"><input type="text" id="sp_name" name="supply.sp_name" readonly="readonly"  value="${prdctbom.supply.sp_name}" class="text" />
				</div>
				<div class="form-label"><fmt:message key="cost_quantity"/>:</div>
				<div class="form-input"><input type="text" id="excnt" name="excnt" readonly="readonly" class="text" value="${prdctbom.excnt}" maxlength='11'/><label class="unit2"></label>
				</div>	
				<div  class="form-label"><fmt:message key ="scm_the_reference_number" />:</div>
				<div class="form-input"><input type="text" id="cnt" name="cnt" readonly="readonly" class="text" value="${prdctbom.cnt}" /><label class="unit1"></label>
				</div>		
			</div>
		</div>
		</form>

		<form id="listForm" method="post" action="<%=path%>/prdctbom/save.do">
		<input type="hidden" id="curStatus" name="curStatus" value="<c:out value="${curStatus}" default="init"/>" />
			<div class="grid">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2"><span class="num" style="width: 20px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:10px;">
									<!-- <input type="checkbox" id="chkAll"/></span> -->
								</td>
								<td colspan="4"><span style="width:180px;"><fmt:message key="supplies"/></span></td>
								<td colspan="3"><span style="width:140px;"><fmt:message key="cost_unit"/></span></td>
								<td colspan="4"><span style="width:180px;"><fmt:message key="standard_unit"/></span></td>
								<td colspan="2"><span style="width:90px;"><fmt:message key="reference_unit"/></span></td>
							</tr>
							<tr>
								<td><span style="width:70px;"><c:if test="${is_supply_x=='Y'}"><fmt:message key ="virtual" /></c:if><fmt:message key="coding"/></span></td>
								<td><span style="width:70px;"><c:if test="${is_supply_x=='Y'}"><fmt:message key ="virtual" /></c:if><fmt:message key="name"/></span></td>
								<td><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td><span style="width:40px;"><fmt:message key="reclaimer_rate"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:50px;"><fmt:message key="net_usage"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
								<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
								<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
								<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
								<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
						<c:set value="0" var="totalSum"></c:set>
							<c:forEach var="product" items="${productList}" varStatus="status">
								<tr>
									<td class="num"><span style="width: 20px;">${status.index+1}</span></td>
									<td style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="chk_${product.supply.sp_code}" value="${product.supply.sp_code}"/>
									</td>
									<td id="sp_code"><span id="sp_code" title="${product.supply.sp_code}" style="width:70px;text-align: left;">${product.supply.sp_code}&nbsp;</span></td>
									<td><span title="${product.supply.sp_name}" style="width:70px;text-align: left;">${product.supply.sp_name}&nbsp;</span></td>
									<td><span title="${product.supply.sp_desc}" style="width:70px;text-align: right;">${product.supply.sp_desc}&nbsp;</span></td>
									<td><span title="${product.exrate}" style="width:40px;text-align: right;"><fmt:formatNumber value="${product.exrate}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit2}" style="width:30px;text-align: left;">${product.supply.unit2}&nbsp;</span></td>
									<td><span title="${product.excnt}" style="width:50px;text-align: right;">${product.excnt}&nbsp;</span></td>
									<td><span title="${product.cnt2}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt2}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit}" style="width:40px;text-align: left;">${product.supply.unit}&nbsp;</span></td>
									<td><span title="${product.cnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.pricesale}" style="width:40px;text-align: right;">${product.supply.pricesale}&nbsp;</span></td>
									<td><span title="${product.sale}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.sale}"  pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
									<td><span title="${product.supply.unit1}" style="width:30px;text-align: left;">${product.supply.unit1}&nbsp;</span></td>
									<td><span title="${product.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt1}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
								</tr>
								<c:set var="totalSum" value="${product.sale+totalSum}"></c:set>
							</c:forEach>
						</tbody>
						<c:if test="${totalSum!=0 }">
						  <tfoot>
							<tr>
								<td colspan="2"><span></span></td>
								<td style="width:80px;"><span><fmt:message key="total"/></span></td>
								<td colspan="9"><span></span></td>
								<td style="width:60px;"><span style="width:50px;text-align: right;"><fmt:formatNumber value="${totalSum}" pattern="##.##" minFractionDigits="2"></fmt:formatNumber></span></td>
								<td colspan="2"><span></span></td>
							</tr>							
						  </tfoot>
						</c:if>
					</table>
				</div>
			</div>
		</form>
			    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/orderTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript">
		//工具栏
		$(document).ready(function(){
			focus() ;//页面获得焦点
			var status = $("#curStatus").val();
			<%-- curStatus 0:init;1:edit(E);2:add(A);3:show 4,delete(D)--%>
			//判断按钮的显示与隐藏
			if(status == 'A'){
				loadToolBar([false,false,false,false,false,true,false]);
			}else if(status == 'show'){//查询页面双击返回
				loadToolBar([false,false,false,true,true,false,true]);
				//$('#chk1memo').attr('disabled',false);
			}else{//init
				loadToolBar([true,false,false,false,false,true,true]);
			}
			
			//按钮快捷键
		 	$(document).bind('keyup',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.altKey ==false)return;
		 		switch (e.keyCode)
	            {
	                case 65: $('#autoId-button-101').click(); break;
	                case 69: $('#autoId-button-102').click(); break;
	                case 68: $('#autoId-button-103').click(); break;
	                case 83: $('#autoId-button-104').click(); break;
					case 80: $('#autoId-button-106').click(); break;
				}
	 		}); 
			//双单 击事件
			$('.grid .table-body tr').live('dblclick click',function(){
				 $('#sp_code').val($(this).find('td:eq(2)').find('span').attr('title'));
				 $('#sp_name').val($(this).find('td:eq(3)').find('span').attr('title'));
				 $('#unit').val($(this).find('td:eq(9)').find('span').attr('title'));
				 $('#unit1').val($(this).find('td:eq(13)').find('span').attr('title'));
				 $('#excnt').val($(this).find('td:eq(7)').find('span').attr('title'));
				 $('#exrate').val($(this).find('td:eq(5)').find('span').attr('title'));
				 $('#cnt').val($(this).find('td:eq(14)').find('span').attr('title'));
				 $('.unit2').html($(this).find('td:eq(6)').find('span').attr('title'));
				 $('.unit1').html($(this).find('td:eq(13)').find('span').attr('title'));
				 $(":checkbox").attr("checked", false);
				 $(this).find(":checkbox").attr("checked", true);
				 loadToolBar([true,true,true,false,false,true,true]);
			});

			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'excnt',
					validateType:['canNull','num2'],
					param:['F','F'],
					error:['<fmt:message key="cannot_be_empty"/>！','<fmt:message key="Please_enter_a_number_greater_than_0" />！']
				},{
					type:'text',
					validateObj:'exrate',
					validateType:['canNull','num2','maxValue','minValue'],
					param:['F','F','1','0'],
					error:['<fmt:message key="reclaimer_rate"/><fmt:message key="cannot_be_empty"/>！','请输入大于0的数字！',
					       '<fmt:message key="reclaimer_rate"/><fmt:message key="should_be_between_0_and_1"/>！','<fmt:message key="reclaimer_rate"/><fmt:message key="should_be_between_0_and_1"/>！']
				}]
			});
			
			//自动实现滚动条
			setElementHeight('.grid',['.tool'],$(document.body),58);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法
			$('input:text[readonly]').not("#cnt,#excnt,#exrate").addClass('textDisable');		//不可编辑颜色
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		   	$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
		});
		/*弹出物资树*/
		function supply_select(){
			if(!!!top.customWindow){
				top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?is_supply_x=${is_supply_x_group}',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),function(){
					loadToolBar([false,false,false,true,true,true,true]);
					$("#curStatus").val("A");
					$("#cnt,#excnt,#exrate").removeAttr('readonly');
				});	
			}
			if($("#exrate").val()==""){
				$("#exrate").val("1.0");
			}
		};
		function loadToolBar(use){
			$('.tool').html('');
			var tool = $('.tool').toolbar({
				items: [{
						text: '<fmt:message key="insert" />(<u>A</u>)',
						title: '<fmt:message key="new_cost_card_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}&&use[0],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['0px','0px']
						},
						handler: function(){
							$('#tableForm input[type=text]').val('');
							$('#excnt').siblings('.validateMsg').remove();
							$('#exrate').siblings('.validateMsg').remove();
							supply_select();
						}
					},{
						text: '<fmt:message key="edit" />(<u>E</u>)',
						title: '<fmt:message key="editing_materials_with_material_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-18px','0px']
						},
						handler: function(){
							$('#tableForm #excnt').focus();
							$("#curStatus").val("E");
							loadToolBar([false,false,false,true,true,true,true]);
							$("#cnt,#excnt,#exrate").removeAttr('readonly');
						}
					},{
						text: '<fmt:message key="delete" />(<u>D</u>)',
						title: '<fmt:message key="deleted_material_material_information"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}&&use[2],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-38px','0px']
						},
						handler: function(){
							if(confirm('<fmt:message key="delete_data_confirm"/>？')){
// 								if(validate._submitValidate()){
									$("#curStatus").val("D");
									$('#tableForm').submit();
// 								}
							}					
						}
					},{
						text: '<fmt:message key="save" />(<u>S</u>)',
						title: '<fmt:message key="save_cost_card_information"/>',
						useable: use[3],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-80px','0px']
						},
						handler: function(){
							var curStatus=$("#curStatus").val();
							if(curStatus=='A'){
								var cur_a='1';
								$('.grid').find('.table-body').find('tr').each(function(i){
									if($(this).find('td:eq(2)').find('span').attr('title')==$('#sp_code').val()){
										cur_a='0';
										return;
									}
								});
								if(cur_a!='1'){
									alert('<fmt:message key="added_supplies_remind"/>！');
									return;
								}
							}
							if(validate._submitValidate()){
								$('#tableForm').submit();
							}
						}
					},{
						text: '<fmt:message key="cancel" />',
						title: '<fmt:message key="cancel_cost_card_information"/>',
						useable: use[4],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-60px','0px']
						},
						handler: function(){
							$("#curStatus").val("init");
							$('#tableForm input[type=text]').val('');
							$('#excnt').siblings('.validateMsg').remove();
							$('#exrate').siblings('.validateMsg').remove();
							$(":checkbox").attr("checked", false);
							loadToolBar([true,false,false,false,false,true,true]);
						}
					},{
						text: '<fmt:message key="print" />',
						title: '<fmt:message key="print"/>',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-140px','-100px']
						},
						handler: function(){
							$("#wait2").val('NO');//不用等待加载
							$('#listForm').attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/prdctbom/print.do?item=${item}";
							$('#listForm').attr('action',action);
							$('#listForm').submit();
							$('#listForm').attr('target','');
							$('#listForm').attr('action','<%=path%>/prdctbom/tableBycode.do');
							$("#wait2").val('');//等待加载还原
						}
					},{
						text: '<fmt:message key="quit" />',
						title: '<fmt:message key="quit"/>',
						useable: use[6],
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));
						}
					}]
			});
		};
			

		</script>
	</body>
</html>