<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>查看项目进度</title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
		<style type="text/css">
			.page{
				margin-bottom: 25px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<div class="grid">
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							
							<td colspan="4"><span style="width:250px;">半成品</span></td>
							<td colspan="3"><span style="width:140px;"><fmt:message key="cost_unit"/></span></td>
							<td colspan="4"><span style="width:180px;"><fmt:message key="standard_unit"/></span></td>
							<td colspan="2"><span style="width:90px;"><fmt:message key="reference_unit"/></span></td>
						</tr>
						<tr>
							<td><span style="width:70px;">编码</span></td>
							<td><span style="width:70px;">名称</span></td>
							<td><span style="width:70px;">规格</span></td>
							<td><span style="width:40px;">取料率</span></td>
							<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
							<td><span style="width:50px;"><fmt:message key="net_usage"/></span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
							<td><span style="width:40px;"><fmt:message key="unit"/></span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
							<td><span style="width:40px;"><fmt:message key="unit_price"/></span></td>
							<td><span style="width:50px;"><fmt:message key="amount"/></span></td>
							<td><span style="width:30px;"><fmt:message key="unit"/></span></td>
							<td><span style="width:60px;"><fmt:message key="hair_dosage"/></span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="product" items="${resultList}" varStatus="status">
							<tr><td><span title="${product.supply.sp_code}" style="width:70px;text-align: left;">${product.supply.sp_code}&nbsp;</span></td>
							<td><span title="${product.supply.sp_name}" style="width:70px;text-align: left;">${product.supply.sp_name}</span></td>
							<td><span title="${product.supply.sp_desc}" style="width:70px;text-align: right;">${product.supply.sp_desc}&nbsp;</span></td>
							<td><span title="${product.exrate}" style="width:40px;text-align: right;"><fmt:formatNumber value="${product.exrate}" pattern="##.#" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
							<td><span title="${product.supply.unit2}" style="width:30px;text-align: left;">${product.supply.unit2}&nbsp;</span></td>
							<td><span title="${product.excnt}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.excnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></span></td>
							<td><span title="${product.cnt2}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt2}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
							<td><span title="${product.supply.unit}" style="width:40px;text-align: left;">${product.supply.unit}&nbsp;</span></td>
							<td><span title="${product.cnt}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
							<td><span title="${product.supply.pricesale}" style="width:40px;text-align: right;"><fmt:formatNumber value="${product.supply.pricesale}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber></span></td>
							<td><span title="${product.sale}" style="width:50px;text-align: right;"><fmt:formatNumber value="${product.sale}"  pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
							<td><span title="${product.supply.unit1}" style="width:30px;text-align: left;">${product.supply.unit1}&nbsp;</span></td>
							<td><span title="${product.cnt1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${product.cnt1}" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>&nbsp;</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>	
		<script type="text/javascript">
			$(document).ready(function(){

				focus() ;//页面获得焦点
			 	$(document).bind('keydown',function(e){
			 		if(e.keyCode==27){
			 			$('.退出').click();
			 		}
			 	});
			 	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			 	
				var tool = $('.tool').toolbar({
					items: [{
						text: '返回',
						title: '退出',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							position: ['-160px','-100px']
						},
						handler: function(){
							$(".close",parent.document).click();									
						}
					}]
				});
				
				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }
				     else
				     {
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				         
				     }
				 });
			});
		</script>
	</body>
</html>