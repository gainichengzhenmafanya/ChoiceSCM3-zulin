<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>student Info</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
			<link type="text/css" rel="stylesheet" href="<%=path%>/view/tableMain.css" />
			<style type="text/css">
				.table-body{
					height:190px;
    					overflow-x: hidden;
    					overflow-y: auto;
				}
				
			</style>
		</head>
		
	<body>
		<div id="message" ><a href="#" onclick="showInfo('62d1a702b589499abd370d98a9643caa','我的工作台','/mainInfo/open.do')"></a></div>
		<div id="m_1_1" class="module" style="height: 150px;display: none;">
			<h4 class="moduleHeader">
			    <span class="title" >我的提醒</span>
			</h4>
			<div>
				<ul style="height:100px;">
					<c:choose>
						<c:when test="${unCheckedList== null || fn:length(unCheckedList) == 0}">
							<li>您没有未审核的单据</li>
						</c:when>
						<c:otherwise>
							<c:forEach var="map" items="${unCheckedList}" varStatus="status">
								<li>近三天  您有 <a href="#" onclick="openUnChecked('<c:out value='${map["nickname"]}'></c:out>',null)">
								<span style="color: red;"><c:out value="${map['count'] }"></c:out></span></a> 条 
								<c:if test="${map['nickname']=='in' }">入库单单据</c:if>
								<c:if test="${map['nickname']=='out' }">出库单单据</c:if>
								<c:if test="${map['nickname']=='sto' }">报货单单据</c:if> 未审核</li>
							</c:forEach>
							<li>昨天 有<a href="#" onclick="openUnChecked('unUpload','<c:out value='${positnMapYesterday["nickname"]}'></c:out>')">
								<span style="color: red;"><c:out value="${positnMapYesterday['count']}"></c:out></span></a> 个分店未上传,
							今天 有<a href="#" onclick="openUnChecked('unUpload','<c:out value='${positnMapToday["nickname"]}'></c:out>')">
								<span style="color: red;"><c:out value="${positnMapToday['count']}"></c:out></span></a> 个分店未上传</li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
		<div id="m_1_2" class="module" style="height: 150px;display: none;">
		<h4 class="moduleHeader">
		    <span class="title" >公告通知</span>
		  </h4>
			<ul>
				<marquee  direction=up height=120px id=m onmouseout=m.start() onMouseOver=m.stop() scrollamount=2 >
					<c:forEach var="announce" items="${announcementList}" varStatus="status">
						<li> <a href="#" onclick="showAnnounce(<c:out value="${announce.id}" />)"><c:out value="${announce.title}" /></a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${announce.create_time}" /></li>
					</c:forEach>
				</marquee>
			</ul>
		</div>
		
		<div id="m_2_1" class="module" style="display: none;">
		<h4 class="moduleHeader">
		    <span class="title">库存超过上限</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_1_more')" onmouseout="hide('m_2_1_more')"><a id="m_2_1_more" href="#" style="visibility: hidden;color: blue;" onclick="openUnChecked('overLimit',0)">更多>>></a></span>
		  </h4>
			<div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <td><span style="width:80px;">物资编码</span></td> -->
								<td><span style="width:110px;">物资名称</span></td>
								<td><span style="width:30px;">单位</span></td>
								<td><span style="width:60px;">当前库存</span></td>
								<td><span style="width:60px;">最大限量</span></td>
								<td><span style="width:60px;">超限量</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" items="${supplyListOver}" varStatus="status">
								<tr>
									<%-- <td><span style="width:80px;"><c:out value="${supply.sp_code}" />&nbsp;</span></td> --%>
									<td><span style="width:110px;"><c:out value="${supply.sp_name}" />&nbsp;</span></td>
									<td><span style="width:30px;"><c:out value="${supply.unit}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.cnt}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.sp_max1}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.sp_max2}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div id="m_2_2" class="module" style="display: none;" >
		<h4 class="moduleHeader">
		    <span class="title" >库存低于下限</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_2_more')" onmouseout="hide('m_2_2_more')"><a id="m_2_2_more" href="#" style="visibility: hidden;color: blue;" onclick="openUnChecked('lessLimit',0)">更多>>></a></span>
		  </h4>
			<div id="supplyLow" class="grid" style="width: 48%;">
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <td><span style="width:80px;">物资编码</span></td> -->
								<td><span style="width:110px;">物资名称</span></td>
								<td><span style="width:30px;">单位</span></td>
								<td><span style="width:60px;">当前库存</span></td>
								<td><span style="width:60px;">最低限量</span></td>
								<td><span style="width:60px;">不足限量</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="supply" items="${supplyListLow}" varStatus="status">
								<tr>
									<%-- <td><span style="width:80px;"><c:out value="${supply.sp_code}" />&nbsp;</span></td> --%>
									<td><span style="width:110px;"><c:out value="${supply.sp_name}" />&nbsp;</span></td>
									<td><span style="width:30px;"><c:out value="${supply.unit}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.cnt}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.sp_min1}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${supply.sp_max2}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div id="m_2_3" class="module" style="display: none;" >
		<h4 class="moduleHeader">
		    <span class="title" >进货单据汇总</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_3_more')" onmouseout="hide('m_2_3_more')"><a id="m_2_3_more" href="#" style="visibility: hidden;color: blue;" onclick="showMore('m_2_3')">更多>>></a></span>
		  </h4>
			<div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <td><span style="width:60px;">供应商编码</span></td> -->
								<td><span style="width:150px;">供应商名称</span></td>
								<td><span style="width:50px;">合计金额</span></td>
								<td><span style="width:50px;">入库金额</span></td>
								<!-- <td><span style="width:90px;">入库凭证号</span></td> -->
								<td><span style="width:70px;">单据类型</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="stackBillSum" items="${stackBillSumList}" varStatus="status">
								<tr>
									<%-- <td><span style="width:60px;"><c:out value="${stackBillSum['DELIVER']}" />&nbsp;</span></td> --%>
									<td><span style="width:150px;"><c:out value="${stackBillSum['DES']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${stackBillSum['TOTALAMT']}" />&nbsp;</span></td>
									<td><span style="width:50px;text-align: right;"><c:out value="${stackBillSum['AMT']}" />&nbsp;</span></td>
									<%-- <td><span style="width:90px;"><c:out value="${stackBillSum['VOUNO']}" />&nbsp;</span></td> --%>
									<td><span style="width:70px;"><c:out value="${stackBillSum['TYP']}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		
		<div id="m_2_4" class="module" style="display: none;" >
		<h4 class="moduleHeader">
		    <span class="title">出库单据汇总</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_4_more')" onmouseout="hide('m_2_4_more')"><a id="m_2_4_more" href="#" style="visibility: hidden;color: blue;" onclick="showMore('m_2_4')">更多>>></a></span>
		  </h4>
			<div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td><span style="width:60px;">仓位</span></td>
								<td><span style="width:60px;">仓位金额</span></td>
								<td><span style="width:60px;">领用仓</span></td>
								<!-- <td><span style="width:50px;">分店金额</span></td> -->
								<td><span style="width:50px;">本单金额</span></td>
								<td><span style="width:100px;">单号</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="outBillSum" items="${outBillSumList}" varStatus="status">
								<tr>
									<td><span style="width:60px;"><c:out value="${outBillSum['POSITNDES']}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${outBillSum['POSITNAMT']}" />&nbsp;</span></td>
									<td><span style="width:60px;"><c:out value="${outBillSum['FIRMDES']}" />&nbsp;</span></td>
									<%-- <td><span style="width:50px;text-align: right;"><c:out value="${outBillSum['FIRMAMT']}" />&nbsp;</span></td> --%>
									<td><span style="width:50px;text-align: right;"><c:out value="${outBillSum['AMT']}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${outBillSum['VOUNO']}" />&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div id="m_2_5" class="module" style="display: none;" >
		<h4 class="moduleHeader">
		    <span class="title" >将到期物资报价</span>
		    <span style="margin-right: 0px;float: right;" onmouseover="show('m_2_5_more')" onmouseout="hide('m_2_5_more')"><a id="m_2_5_more" href="#" style="visibility: hidden;color: blue;" onclick="showMore('m_2_5')">更多>>></a></span>
		  </h4>
			<div id="" class="grid" >
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<!-- <td><span style="width:80px;">物资编码</span></td> -->
								<td><span style="width:100px;">物资名称</span></td>
								<td><span style="width:100px;">分店</span></td>
								<td><span style="width:60px;">报价</span></td>
								<!-- <td><span style="width:80px;">开始日期</span></td> -->
								<td><span style="width:80px;">结束日期</span></td>
							</tr>
						</thead>
					</table>
				</div>
				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="spprice" items="${sppriceList}" varStatus="status">
								<tr>
									<%-- <td><span style="width:80px;"><c:out value="${spprice.sp_code}" />&nbsp;</span></td> --%>
									<td><span style="width:100px;"><c:out value="${spprice.sp_name}" />&nbsp;</span></td>
									<td><span style="width:100px;"><c:out value="${spprice.areaDes}" />&nbsp;</span></td>
									<td><span style="width:60px;text-align: right;"><c:out value="${spprice.price}" />&nbsp;</span></td>
									<%-- <td><span style="width:80px;"><fmt:formatDate value="${spprice.bdat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td> --%>
									<td><span style="width:80px;"><fmt:formatDate value="${spprice.edat}" pattern="yyyy-MM-dd"/>&nbsp;</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- <div id="m_2_6" class="module" style="display: none;text-align: center;width: 50%;height: 50%" >
			<div id="chartdiv" ></div>
		</div> -->
<!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script language="JavaScript" src="<%=path%>/js/FusionCharts.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				//显示菜单
				var menu = '${menu}';
				if(menu==''){
					$("#message").find("a").html("您还没有设置我的桌面，请到 系统初始>>我的工作台   进行设置！");
				}
				var menuArray = menu.split(',');
				for(var i in menuArray){
					$("#"+menuArray[i]).show();
					
				}
				//table行加斑马色
				$('.grid').find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
				);
				<%-- if($("#m_2_6")[0].style.display!="none"){
				 	//图表
					var w = $("#chartdiv").width();
					var h = $("#chartdiv").height();
					var myChart = new FusionCharts("<%=path%>/Charts/FCF_Column3D.swf", "myChartId", w, h);
			  	          myChart.setDataURL("<%=path%>/mainInfo/getXml.do");
			  	          myChart.render("chartdiv");
				} --%>
				//setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				//loadGrid();//  自动计算滚动条的js方法
			});
			
			function show(id){
				$("#"+id)[0].style.visibility='visible';
			}
			
			function hide(id){
				$("#"+id)[0].style.visibility='hidden';
			}
			
			//查看公告
			function showAnnounce(announcementId){
				$('body').window({
					id: 'window_addAnnouncement',
					//title: '查看公告',
					content: '<iframe id="showAnnouncementFrame" frameborder="0" src="<%=path%>/announcement/show.do?id='+announcementId+'"></iframe>',
					width: 500,
					height: '350px',
					confirmClose:false,
					draggable: true,
					isModal: true
				});
				
			};
			//打开提醒里边的信息
			function openUnChecked(chkstono,type){
				var startDate = '${startDate}';
				var currDate = '${currDate}';
				var yesterDay = '${yesterDay}';
				if(chkstono=='in'){
					$('body').window({
						id: 'window_searchChkinm',
						title: '入库单未审核数据',
						content: '<iframe id="searchChkinmFrame" frameborder="0" src="<%=path%>/chkinm/list.do?checkOrNot=unCheck&action=init&startDate='+startDate+'"></iframe>',
						width: 1010,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '查询(<u>F</u>)',
									title: '查询入库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										if(getFrame('searchChkinmFrame')&&window.document.getElementById("searchChkinmFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('searchChkinmFrame','listForm');
										}
									}
								},{
									text: '<fmt:message key ="enter" />(Enter)',
									title: '<fmt:message key ="enter" />',
									useable: true,
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-20px']
									},
									handler: function(){
										window.frames["searchChkinmFrame"].enterChkinm();
									}
								},{
									text: '审核(<u>C</u>)',
									title: '审核入库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-60px','-240px']
									},
									handler: function(){
										window.frames["searchChkinmFrame"].checkChkinm();
									}
								},{
									text: '删除(<u>D</u>)',
									title: '删除入库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-38px','0px']
									},
									handler: function(){
										window.frames["searchChkinmFrame"].deleteChkinm();
									}
								},{
									text: '<fmt:message key ="quit" />',
									title: '查询退出',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(chkstono=='out'){
					$('body').window({
						id: 'window_searchChkout',
						title: '出库单未审核数据',
						content: '<iframe id="listChkoutFrame" frameborder="0" src="<%=path%>/chkout/list.do?startDate='+startDate+'"></iframe>',
						width: 1010,
						height: '460px',
						confirmClose: false,
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '查询(<u>F</u>)',
									title: '查询出库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										if(getFrame('listChkoutFrame'))
											getFrame('listChkoutFrame').execQuery();;
									}
								},{
									text: '确定(Enter)',
									title: '<fmt:message key ="enter" />',
									useable: true,
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-20px']
									},
									handler: function(){
										if(getFrame('listChkoutFrame'))
											getFrame('listChkoutFrame').onEnter();
									}
								},{
									text: '审核(<u>C</u>)',
									title: '审核出库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-58px','-240px']
									},
									handler: function(){
										if(getFrame('listChkoutFrame'))
											getFrame('listChkoutFrame').auditChkout();
									}
								},{
									text: '删除(<u>D</u>)',
									title: '删除出库单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-38px','0px']
									},
									handler: function(){
										if(getFrame('listChkoutFrame'))
											getFrame('listChkoutFrame').deleteChkout();
									}
								},{
									text: '<fmt:message key ="quit" />',
									title: '查询退出',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(chkstono=='sto'){
					$('body').window({
						id: 'window_searchChkstom',
						title: '报货单未审核数据',
						content: '<iframe id="searchChkstomFrame" frameborder="0" src="<%=path%>/chkstom/searchByKey.do?init=init&startDate='+startDate+'"></iframe>',
						width: 1010,
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '查询(<u>F</u>)',
									title: '查询报货单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										if(getFrame('searchChkstomFrame')&&window.document.getElementById("searchChkstomFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('searchChkstomFrame','SearchForm');
										}
									}
								},{
									text: '审核(<u>C</u>)',
									title: '审核报货单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'check')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-58px','-240px']
									},
									handler: function(){
										window.frames["searchChkstomFrame"].checkMoreChkstom();
									}
								},{
									text: '删除(<u>D</u>)',
									title: '删除报货单',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-38px','0px']
									},
									handler: function(){
										window.frames["searchChkstomFrame"].deleteChkstom();
									}
								},{
									text: '<fmt:message key ="quit" />',
									title: '查询退出',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(chkstono=='unUpload'){
					var datetime;
					if(type=='unUploadYesterday'){
						datetime = yesterDay;
					}else if(type=='unUploadToday'){
						datetime = currDate;
					}
					$('body').window({
						id: 'window_searchUpload',
						title: '查看上传',
						content: '<iframe id="searchUploadFrame" frameborder="0" src="<%=path%>/chkstom/searchUploadByDate.do?startDate='+datetime+'"></iframe>',
						width: '1010px',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '查看(<u>F</u>)',
									title: '查看分店上传',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['0px','-40px']
									},
									handler: function(){
										if(getFrame('searchUploadFrame')){
											submitFrameForm('searchUploadFrame','SearchForm');
										}
									}
								},{
									text: '<fmt:message key ="quit" />',
									title: '查看上传退出',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										position: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}]
						}
					});
				}else if(chkstono=='overLimit'){
					$('body').window({
						id: 'window_overLimit',
						title: '物资超过上限',
						content: '<iframe id="searchOverLimitFrame" frameborder="0" src="<%=path%>/mainInfo/overLimit.do"></iframe>',
						width: '1010px',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}else if(chkstono=='lessLimit'){
					$('body').window({
						id: 'window_lessLimit',
						title: '物资低于下限',
						content: '<iframe id="searchLessLimitFrame" frameborder="0" src="<%=path%>/mainInfo/lessLimit.do"></iframe>',
						width: '1010px',
						height: '460px',
						draggable: true,
						isModal: true,
						confirmClose:false
					});
				}
			};
			//showInfo('b114a52b856449b6b3dbd46a3fe4130c','入库单填制审核','/chkinm/add.do?action=init')
			function showMore(id){
				if(id=='m_2_1'){
					
				}else if(id=='m_2_2'){
					
				}else if(id=='m_2_3'){
					showInfo('a934b1d602ff4f6b830a55593c0be3e1','仓储报表','/JhDanjuHuizong/toStockBillSum.do');
				}else if(id=='m_2_4'){
					showInfo('a934b1d602ff4f6b830a55593c0be3e1','仓储报表','/CkDanjuHuizong/toChkoutOrderSum.do');
				}else if(id=='m_2_5'){
					showInfo('b2905d7ced0d4b9eb20d55832ac7abac','将到期物资报价','/spprice/findSppriceByEdat.do');
				}else if(id=='m_2_6'){
					
				}
				
			}
	  	 	function showInfo(moduleId,moduleName,moduleUrl){
	  	 		window.parent.tabMain.addItem([{
		  				id: 'tab_'+moduleId,
		  				text: moduleName,
		  				title: moduleName,
		  				closable: true,
		  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
		  			}
		  		]);
	  	 		window.parent.tabMain.show('tab_'+moduleId);
	  	 	}
	  	 	
		</script>
	</body>
</html>