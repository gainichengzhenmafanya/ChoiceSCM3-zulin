<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
	</head>
	<body>
		<div class="easyui-tabs" fit="false" plain="true" id="tabs">
				<div title="统配验货">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:80px;text-align:right;">验货条数</span></td>
										<td><span style="width:100px;text-align:right;">物资总数</span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${outList}" varStatus="status">
										<tr>
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.cntout }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="直配验货">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:120px;">供应商</span></td>
										<td><span style="width:80px;">验货条数</span></td>
										<td><span style="width:100px;">物资总数</span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${direList}" varStatus="status">
										<tr>
											<td><span style="width:120px;">${dire.deliverDes }</span></td>
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.amountin }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div title="调拨验货">
					<div class="grid" style="width:99% !important;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0">
								<thead>
									<tr>
										<td><span style="width:120px;">调出仓位</span></td>
										<td><span style="width:80px;">验货条数</span></td>
										<td><span style="width:100px;">物资总数</span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body" style="height:170px;">
							<table cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="dire" items="${dbList}" varStatus="status">
										<tr>
											<td><span style="width:120px;">${dire.positndes }</span></td>
											<td><span style="width:80px;text-align:right;">${dire.id }</span></td>
											<td><span style="width:100px;text-align:right;"><fmt:formatNumber value="${dire.cntout }" type="currency" pattern="0.00"/></span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
// 		setElementHeight('.grid',0,200,50);	//计算.grid的高度
// 		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
// 		loadGrid();//  自动计算滚动条的js方法
		</script>
	</body>
</html>