<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>	
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div class="leftFrame">
	      	<div id="toolbar"></div>
		    <div class="treePanel">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
		        <script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("00000000000000000000000000000000","","")';
	          		<c:forEach var="materialType" items="${materialTypeList}" varStatus="status">
	          			<c:if test="${materialType.enablestate == 2}">
		          			tree.nodes['${materialType.parentMaterialtype.pk_materialtype}_${materialType.pk_materialtype}'] 
			          		= 'text:${materialType.vcode}--${materialType.vname}; method:changeUrl("${materialType.pk_materialtype}","${materialType.vcode}","${materialType.pk_father}")';
		          		</c:if>
		      			<c:if test="${materialType.enablestate != 2}">
			      			tree.nodes['${materialType.parentMaterialtype.pk_materialtype}_${materialType.pk_materialtype}'] 
			          		= 'text:<span title="${materialType.vcode}--${materialType.vname}" style="color: crimson">${materialType.vcode}--${materialType.vname}</span>; method:changeUrl("${materialType.pk_materialtype}","${materialType.vcode}","${materialType.pk_father}")';
			      		</c:if>
		          	</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
		    </div>
	    </div>
	    <div class="mainFrame">
	      <iframe src="<%=path%>/materialType/tableMaterialType.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
		<input type="hidden" id="vcode" name="vcode" />
	    <input type="hidden" id="vname" name="vname" />
	    <input type="hidden" id="pk_father" name="pk_father" />
	    <input type="hidden" id="pk_materialtype" name="pk_materialtype" />
	    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		function changeUrl(pk_materialtype,vcode,pk_father){
			$("#wait2").css("display","block");
			$("#wait").css("display","block");
			$('#pk_materialtype').val(pk_materialtype);
			$('#vcode').val(vcode);
			$('#pk_father').val(pk_father);
			var paths=tree.getPath(pk_materialtype).length;
	      	window.mainFrame.location = "<%=path%>/materialType/tableMaterialType.do?pk_materialtype="+pk_materialtype+"&vcode="+vcode+"&lvl="+(paths-1);
	    }
			
		function refreshTree(pk_materialtype,vcode){
			$("#wait2").css("display","block");
			$("#wait").css("display","block");
	    	if(pk_materialtype == null){
	    		window.location.href = '<%=path%>/materialType/materialTypeList.do';
	    	}else{
				window.location.href = '<%=path%>/materialType/materialTypeList.do?pk_materialtype='+pk_materialtype+'&vcode='+vcode;
	    	}
	    }
		$(document).ready(function(){
			var toolbar = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="expandAll" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					},{
						text: '<fmt:message key="refresh" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-60px','0px']
						},
						handler: function(){
							refreshTree();
						}
					}]
			});
			setElementHeight('.treePanel',['#toolbar']);
			tree.focus('${materialType.pk_materialtype}');
			changeUrl('${materialType.pk_materialtype}','${materialType.vcode}','${materialType.vname}');
// 			$("#wait2").css("display","none");
// 			$("#wait").css("display","none");
		});// end $(document).ready();
		//清空页面
		function clearValue(){
			window.frames["mainFrame"].clearValue();
		}
	</script>

	</body>
</html>