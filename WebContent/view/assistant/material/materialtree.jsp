<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>	
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div>
	      	<div id="toolbar"></div>
		    <div class="treePanel">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
		        <script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("00000000000000000000000000000000","<fmt:message key="supplies_category" />","")';
	          		<c:forEach var="materialType" items="${materialTypeList}" varStatus="status">
	          			tree.nodes['${materialType.parentMaterialtype.pk_materialtype}_${materialType.pk_materialtype}'] 
		          		= 'text:${materialType.vcode}--${materialType.vname}; method:changeUrl("${materialType.pk_materialtype}","${materialType.vcode}","${materialType.vname}","${materialType.pk_father}")';
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
		    </div>
	    </div>
		<input type="hidden" id=callBack name="callBack" value="${callBack}"/>
		<input type="hidden" id="vcode" name="vcode" value="${vcode}"/>
	    <input type="hidden" id="vname" name="vname" value="${vname}"/>
	    <input type="hidden" id="pk_father" name="pk_father" value="${pk_father}"/>
	    <input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
	    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		function changeUrl(pk_materialtype,vcode,vname,pk_father){
			$('#pk_materialtype').val(pk_materialtype);
			$('#vcode').val(vcode);
			$('#vname').val(vname);
			$('#pk_father').val(pk_father);
	    }
			
	    function refreshTree(pk_materialtype){
	    	if(pk_materialtype == null){
	    		window.location.href = '<%=path%>/material/selectMaterialType.do?callBack='+$('#callBack').val();
	    	}
	    }
		$(document).ready(function(){
			var toolbar = $('#toolbar').toolbar({
				items: [{
						text: '<fmt:message key="enter" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-80px']
						},
						handler: function(){
							setData();
						}
					},{
						text: '<fmt:message key="expandAll" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-80px']
						},
						handler: function(){
							tree.expandAll();
						}
					},{
						text: '<fmt:message key="refresh" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-60px','0px']
						},
						handler: function(){
							refreshTree();
						}
					}
				]
			});
			setElementHeight('.treePanel',['#toolbar']);
			tree.focus('${materialType.pk_materialtype}');
			changeUrl('${materialType.pk_materialtype}','${materialType.vname}','${materialType.vcode}');
		});// end $(document).ready();
		//清空页面
		function clearValue(){
			window.frames["mainFrame"].clearValue();
		}
		//判断当前选中的分类是不是最小类别
		function checkMaterialType(){
			var pk_materialtype = $('#pk_materialtype').val();
			var flag = false;
			$.ajax({
				type: "POST",
				url: "<%=path%>/material/checkMaterialType.do",
				data: "pk_materialtype="+pk_materialtype,
				async:false,
				dataType: "json",
				success:function(result){
					if(result > 0){
						alerterror('<fmt:message key="First_select_a_final_category" />');
						flag = false;
					} else {
						flag = true;
					}
				}
			});
			return flag;
		}
		function setData(){
			if(checkMaterialType()){
				var data = {};
				data.pk_materialtype = $('#pk_materialtype').val();//主键
				data.vcode = $('#vcode').val();//编码
				data.vname = $('#vname').val();//名称
				parent['${callBack}'](data);
				$(".close",parent.document).click();
			}
		}
	</script>

	</body>
</html>