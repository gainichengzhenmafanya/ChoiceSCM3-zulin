<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.divgrid {
				positn: relative;
				overflow: auto;
				border-top: 1px solid #999999;
				border-bottom: 1px solid #999999;
				border-right: 1px solid #999999;
				width: 90%;
				height: 85%;
			}
			
			.divgrid td {
				white-space:nowrap;
				overflow: hidden;
				border-right: 1px solid #999999;
				border-bottom: 1px solid #999999;
				height: 18px;
				line-height: 18px;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="updateMaterial" method="post" action="<%=path %>/material/updateMaterial.do">
			<input type="hidden" style="width: 240px;" id="pk_materialtype" name="pk_materialtype" value="${material.pk_materialtype}"/>
			<input type="hidden" style="width: 240px;" id="pk_material" name="pk_material" value="${material.pk_material}"/>
			<input type="hidden" style="width: 240px;" id="pk_vunit" name="pk_vunit" value="${material.pk_unit}"/>
			<div class="easyui-tabs" fit="false" plain="true" style="height:300px;width:600px;z-index:88;margin:0px auto;">
				<div title='<fmt:message key="basic_information" />' style="padding-top:50px;">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="supplies_category" />:</div>
						<div class="form-input"><input type="text" id=name name="name" class="text" readonly="readonly" value="${materialType.vname}"/></div>
						<div class="form-label"><fmt:message key="supplies_code" />:</div>
						<div class="form-input"><input type="text" id="vcode" name="vcode" class="text" readonly="readonly" value="${material.vcode}" maxlength="25"/></div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
					    <div class="form-label"><span class="red">*</span><fmt:message key="supplies_name" />:</div>
						<div class="form-input"><input type="text" id="vname" name="vname" class="text" onblur="getSpInit(this,'vinit');" value="${material.vname}" maxlength="25"/></div>
						<div class="form-label"><span class="red">*</span><fmt:message key="Mnemonic" />:</div>
						<div class="form-input"><input type="text" id="vinit" name="vinit" class="text" value="${material.vinit}" maxlength="20"/></div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
					    <div class="form-label"><fmt:message key="supplies_specifications" />:</div>
						<div class="form-input"><input type="text" id="vspecfication" name="vspecfication" class="text" value="${material.vspecfication}"/></div>
					<div class="form-label"><fmt:message key="enable_state" />:</div>
						<div class="form-input">
							<select id="enablestate" name="enablestate"  class="select" style="width:133px;">
								<option value="1"  <c:if test="${materialType.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
								<option value="2"  <c:if test="${materialType.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
								<option value="3"  <c:if test="${materialType.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
							</select>
						</div>
					</div>
				</div>
				<div title='<fmt:message key="convert_units" />' style="width: 100%;">
					<div id="unitForm" class="unitForm" style="width: 100%;margin: auto;margin-top: 10px;">
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="standard_unit" />:</div>
							<div class="form-input">
								<select class="select" id="pk_unit" name="pk_unit" style="width:130px" onchange="setvalue()">
									<c:forEach var="unit" items="${unitlist}" varStatus="status" >
										<option 
										<c:if test="${unit.pk_unit == material.pk_unit}"> selected="selected" </c:if>
										id="${unit.pk_unit}" value="${unit.pk_unit}">${unit.vname}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<input type="hidden" id="unitvname" name="unitvname" value="${unitvname}"/>
							</div>
						</div>
						<div id="divgrid" class="grid">
							<div class="table-head">
								<table>
									<thead>
										<tr style="height: 25px;">
											<td style="width:100px;"><fmt:message key="unit"/></td>
											<td style="width:100px;"><fmt:message key="unit_conversion_rate"/></td>
											<td style="width:80px;"><fmt:message key="Purchase_unit" /></td>
											<td style="width:80px;"><fmt:message key="Sale_unit" /></td>
											<td style="width:80px;"><fmt:message key="Stock_unit" /></td>
											<td style="width:60px;"><input type="button" id="addrow" class="addrow" style="width: 50px;" value="添加"/></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table id="tblGrid">
									<tbody>
										<c:forEach var="materialunit" items="${listMaterialUnit}" varStatus="status" >
											<tr style="height: 35px;">
												<td style="width:100px;text-align: center;">
													<select class="select" id="pk_unit1" name="pk_unit1" style="width:80px;margin-top: 1px;">
														<c:forEach var="unit" items="${unitlist}" varStatus="statsus" >
															<option vcode="${unit.vcode }"
															<c:if test="${unit.pk_unit == materialunit.pk_unit}">selected="selected"</c:if>
															value="${unit.pk_unit}">${unit.vname}</option>
														</c:forEach>
													</select>
												</td>
												<td style="width:100px;text-align: center;">
													<input type="text" id="nrate" name="nrate" style="width:80px;text-align: left;" value="${materialunit.nrate}"/>
												</td>
												<td style="width:80px;text-align: center;">
													<input name="cgbox" type="checkbox"
														<c:if test='${materialunit.bispurchase == 1}'>checked="checked"</c:if>/>
												</td>
												<td style="width:80px;text-align: center;">
													<input name="xsbox" type="checkbox"
														<c:if test='${materialunit.bissale == 1}'>checked="checked"</c:if>/></td>
												<td style="width:80px;text-align: center;">
													<input name="kcbox" type="checkbox"
														<c:if test='${materialunit.bisstore == 1}'>checked="checked"</c:if>/></td>
												<td style="width:60px;">
													<c:if test='${status.index>0}'>
														<img src="../image/scm/move.gif" onclick="deleteRow(this)"/>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div title='<fmt:message key="auxiliary_information" />' style="padding-top:30px;">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="Reference_resources_price" />:</div>
						<div class="form-input"><input type="text" id=nsaleprice name="nsaleprice" class="text" value="${material.nsaleprice}"/></div>
						<div class="form-label"><span class="red">*</span><fmt:message key="min_chkstom_cnt" />:</div>
						<div class="form-input"><input type="text" id="minbhcnt" name="minbhcnt" class="text" value="${material.minbhcnt}"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="tax_rate" />:</div>
						<div class="form-input">
							<select class="select" id="pk_tax" name="pk_tax" style="width:133px">
								<c:forEach var="tax" items="${taxList}" varStatus="status">
									<option
										<c:if test="${material.pk_tax==tax.pk_tax}"> 
									  	 	selected="selected"
										</c:if>  
									id="${tax.pk_tax}" vcode="${tax.vcode}" value="${tax.pk_tax}">${tax.vname}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><span class="red">*</span><fmt:message key="Minimum_Purchase_Amount" />:</div>
						<div class="form-input"><input type="text" id="nminamt" name="nminamt" class="text" value="${material.nminamt}"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="supplies_brands" />:</div>
						<div class="form-input"><input type="text" id="vbrand" name="vbrand" class="text" value="${material.vbrand}"/></div>
					    <div class="form-label"><fmt:message key="supplies_origin" />:</div>
						<div class="form-input"><input type="text" id="vaddr" name="vaddr" class="text" value="${material.vaddr}"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="inspection_difference_lv" />:</div>
						<div class="form-input"><input type="number" id="ncheckdiffrate" name="ncheckdiffrate" value="<fmt:formatNumber value='${material.ncheckdiffrate}' pattern='##.##' />" style="margin-top: 3px;width: 130px;"/><span>%</span></div>
				    </div>
					<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="remark" /><fmt:message key="explain" />:</div>
						<div class="form-input">
							<textarea rows="5"   id="vmemo" name="vmemo"  style="resize:none; width: 403px;">${material.vmemo}</textarea>
						</div>
					</div>
				</div>
			</div>
			</form>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript">
			var savelock = false;
			var validate;
			var checkunit = true;//验证转换率
			var unitarray = [];
			$(document).ready(function(){
				$('#unitvname').val($('#pk_unit').find("option:selected").text());
				$("input[name='cgbox']").live('click',function(event){
					$("input[name='cgbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("input[name='xsbox']").live('click',function(event){
					$("input[name='xsbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("input[name='kcbox']").live('click',function(event){
					$("input[name='kcbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("#vname").focus();
				$(".table-body").height(190);
				//--------------------------输入框支持回车start---------------------------------
				var inputarray = $("input[type='text']");
				$("input[type='text']").bind('keyup',function(){
					if(event.keyCode==13){
						var index = inputarray.index($(this)[0]);
						if(index!=inputarray.length-1){
							$(inputarray[index+1]).focus();
						}
					}
				});
				//--------------------------输入框支持回车end----------------------------------
			});
			function saveCheck(){
				if(savelock){
					return false;
				}
				var pk_material = $.trim($('#pk_material').val());
				var vcode = $.trim($('#vcode').val());
				$('#vcode').val($.trim($('#vcode').val()));
				if (vcode == '' || vcode == null) {
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(vcode.lengh > 25){
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！');
					return false;
				} else if(!checkvcode(vcode,pk_material)){
					alerterror('<fmt:message key="coding" /><fmt:message key="already_exists" />！');
					return false;
				} else if(/[?:"{},\/;'[\]]/im.test(vcode)){
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="cannot_contain_special_characters" />!');
					return false;
				}
				var vname = $.trim($('#vname').val()).replace(' ','');
				$('#vname').val($.trim($('#vname').val()));
				if (vname == '' || vname == null) {
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(vname.lengh > 25){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！');
					return false;
				} else if(!checkvname(vname,pk_material)){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="already_exists" />！');
					return false;
				} else if(/[?:"{},\/;'[\]]/im.test(vname)){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="cannot_contain_special_characters" />!');
					return false;
				}
				var vinit = $.trim($('#vinit').val());
				if (vinit == '' || vinit == null) {
					alerterror('<fmt:message key="supplies_abbreviations" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var pk_unit = $.trim($('#pk_unit').val());
				if(pk_unit == null || pk_unit == ''){
					alerterror('<fmt:message key="standard_unit" /><fmt:message key="cannot_be_empty" />');
					return false;
				}
				var pk_tax = $.trim($('#pk_tax').val());
				if(pk_tax == null || pk_tax == ''){
					$("#pk_tax").find("option[vcode=1]").attr("selected",true);
				}
				var nminamt = $('#nminamt').val();
				if (nminamt == '' || nminamt == null) {
					alerterror('<fmt:message key="Minimum_Purchase_Amount" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(isNaN(nminamt)){
					alerterror('<fmt:message key="Minimum_Purchase_Amount" /><fmt:message key="must_be_numeric" />！');
					return false;
				}
				var nsaleprice = $.trim($('#nsaleprice').val());
				if (nsaleprice == '' || nsaleprice == null) {
					alerterror('<fmt:message key="Reference_resources_price" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(isNaN(nsaleprice)){
					alerterror('<fmt:message key="Reference_resources_price" /><fmt:message key="must_be_numeric" />！');
					return false;
				}
				var ncheckdiffrate = $.trim($('#ncheckdiffrate').val());
				if(isNaN(ncheckdiffrate)){
					alerterror('<fmt:message key="inspection_difference_lv" /><fmt:message key="must_be_numeric" />！');
					return false;
				} else if(ncheckdiffrate == null || ncheckdiffrate == ''){
					alerterror('<fmt:message key="inspection_difference_lv" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var vbrand = $.trim($('#vbrand').val());
				if(vbrand.lengh > 50){
					alerterror('<fmt:message key="supplies_brands" /><fmt:message key="the_maximum_length_not" />50！');
					return false;
				}
				var vaddr = $.trim($('#vaddr').val());
				if(vaddr.lengh > 50){
					alerterror('<fmt:message key="supplies_origin" /><fmt:message key="the_maximum_length_not" />50！');
					return false;
				}
				var vmemo = $.trim($('#vmemo').val());
				if(vmemo.lengh > 100){
					alerterror('<fmt:message key="scscbzbz" /><fmt:message key="the_maximum_length_not" />100！');
					return false;
				}
				var minbhcnt = $.trim($('#minbhcnt').val());
				if(minbhcnt == null || minbhcnt == ''){
					alerterror('<fmt:message key="min_chkstom_cnt" /><fmt:message key="cannot_be_empty" />');
					return false;
				}else if(Number(minbhcnt) < 0){
					alerterror('<fmt:message key="min_chkstom_cnt" /><fmt:message key="Negative" />');
					return false;
				}
				return true;
			}
			function checkvcode(vcode,pk_material){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/findMaterialByCodeUp.do",{vcode:$('#vcode').val(),pk_material:$('#pk_material').val()},function(data){
					if(!data)result = false;
					savelock = false;
				});
				return result;
			}
			function checkvname(vname,pk_material){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/findMaterialByNameUp.do",{vname:$("#vname").val(),pk_material:$('#pk_material').val()},function(data){
					if(!data)result = false;
					savelock = false;
				});
				return result;
			}
			

			function checkUnit(){
				checkunit = true;//验证转换率
				if($('#tblGrid').find('tr').length==0){
					alerterror('<fmt:message key="please_set" /><fmt:message key="unit_conversion_rate" />！');
					checkunit = false;
					return;
				}
				var checkarray = [];
				var cgdw = false;
				$('#tblGrid').find('tr').each(function(){
					var pk_unit = $(this).find('td:first').find('select option:selected').val();
					var vunitcode = $(this).find('td:first').find('select option:selected').attr('vcode');
					var vunitname = $(this).find('td:first').find('select option:selected').text();
					var nrate = $(this).find('td:eq(1)').find('input').val();
					var bispurchase = $(this).find('td:eq(2)').find('input')[0].checked;
					var bissale = $(this).find('td:eq(3)').find('input')[0].checked;
					var bisstore = $(this).find('td:eq(4)').find('input')[0].checked;
					if(isNaN(nrate) || Number(nrate)<0){
						alerterror('<fmt:message key="unit_conversion_rate" /><fmt:message key="no_right" />！');
						savelock = false;
						checkunit = false;
						return;
					}
					if(bispurchase){
						cgdw = true;
					}
					if(bispurchase || bissale || bisstore){
						if($.inArray(pk_unit,checkarray)==-1){
							var unit = {};
							unit.pk_unit = pk_unit;
							unit.vunitcode = vunitcode;
							unit.vunitname = vunitname;
							unit.nrate = nrate;
							unit.bispurchase = bispurchase?1:0;
							unit.bissale = bissale?1:0;
							unit.bisstore = bisstore?1:0;
							unitarray.push(unit);
							checkarray.push(pk_unit);
						}else{
							alerterror('<fmt:message key="unit" /><fmt:message key="duplicate" />！');
							unitarray=[];
							savelock = false;
							checkunit = false;
							return;
						}
					}
				});
				if(!cgdw){
					checkunit = false;
					savelock = false;
					alerterror('采购单位必须选择！');	
				}
			}
			
			//单位换算添加行
			$('#addrow').click(function (){
				var trtd = '<tr style="height: 35px;">'+
								'<td style="width:100px;text-align: center;">'+
								'<select class="select" id="pk_unit" name="pk_unit" style="width:80px;margin-top: 1px;">'+
									<c:forEach var="unit" items="${unitlist}" varStatus="status" >
										'<option id="${unit.pk_unit}" vcode="${unit.vcode}" value="${unit.pk_unit}">${unit.vname}</option>'+
									</c:forEach>
								'</select>'+
							'</td>'+
							'<td style="width:100px;text-align: center;">'+
								'<input type="text" id="nrate" name="nrate" style="width:80px;text-align: left;" value="0.0"/>'+
							'</td>'+
							'<td style="width:80px;text-align: center;"><input name="cgbox" type="checkbox"/></td>'+
							'<td style="width:80px;text-align: center;"><input name="xsbox" type="checkbox"/></td>'+
							'<td style="width:80px;text-align: center;"><input name="kcbox" type="checkbox"/></td>'+
							'<td style="width:60px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></td>'+
						'</tr>';
				$('#tblGrid').append(trtd);
			});

			//循环遍历单位转换率
			function getUnitValues(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				checkUnit();//验证单位转换率
				if(!checkunit){
					return;	
				} 
				if($("#pk_material").val()!=""){
					if($("#enablestate").val()!="2"){
						var result = true;
						$.ajaxSetup({async:false});
						$.post("/JMUASST/material/checkMaterialQuote.do",{pk_material:$("#pk_material").val()},function(data){
							if(data=="false")
								savelock = false;
								result = false;
						});
						if(result){ //如果有被引用
							alerterror('<fmt:message key="refencenotdisable"/>');
							savelock = false;
							return;
						}
					}
				}
				var data = {};
				data["pk_material"] = $.trim($("#pk_material").val());
				data["pk_materialtype"] = $.trim($("#pk_materialtype").val());
				data["vcode"] = $.trim($("#vcode").val());
				data["vname"] = $.trim($("#vname").val());
				data["vinit"] = $.trim($("#vinit").val());
				data["vspecfication"] = $.trim($("#vspecfication").val());
				data["enablestate"] = $.trim($("#enablestate").val());
				data["nsaleprice"] = $.trim($("#nsaleprice").val());
				data["ninprice"] = $.trim($("#nsaleprice").val());
				data["pk_tax"] = $.trim($("#pk_tax").val());
				data["nminamt"] = $.trim($("#nminamt").val());
				data["vbrand"] = $.trim($("#vbrand").val());
				data["vaddr"] = $.trim($("#vaddr").val());
				data["unitvname"] = $.trim($("#unitvname").val());
				data["pk_unit"] = $.trim($("#pk_unit").val());
				data["ncheckdiffrate"] = $.trim($("#ncheckdiffrate").val());
				data["minbhcnt"] = $.trim($("#minbhcnt").val());
				data["vmemo"] = $.trim($("#vmemo").val());

				for(var i=0;i<unitarray.length;i++){
					data["listMaterialUnit["+i+"].pk_unit"] = unitarray[i].pk_unit;
					data["listMaterialUnit["+i+"].vunitcode"] = unitarray[i].vunitcode;
					data["listMaterialUnit["+i+"].vunitname"] = unitarray[i].vunitname;
					data["listMaterialUnit["+i+"].nrate"] = unitarray[i].nrate;
					data["listMaterialUnit["+i+"].bispurchase"] = unitarray[i].bispurchase;
					data["listMaterialUnit["+i+"].bissale"] = unitarray[i].bissale;
					data["listMaterialUnit["+i+"].bisstore"] = unitarray[i].bisstore;
				}
				
				$.ajaxSetup({async:false});
				$.post("<%=path%>/material/updateMaterial.do",data,function(data){
					var rs = data;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
// 										parent.parent.refreshTree($("#pk_materialtype").val());
										parent.$('.close').click();}
									});
						break;
					}
				});
			}
			//选择标准单位给第一行单位转换率的单位赋值
			function setvalue(){
				var pk_vunit = $.trim($('#pk_vunit').val());
				var vcode = $.trim($('#vcode').val());
				if(!checkmaterialunit(vcode)){
					alerterror('<fmt:message key="supplies"/><fmt:message key="heaven_used"/>，<fmt:message key="no_update_unit"/>！');
					$('#pk_unit').val(pk_vunit);
					return;
				}else{
					var v = $('#pk_unit').find("option:selected").val();
					$('#unitvname').val($('#pk_unit').find("option:selected").text());
					$('#pk_unit1').val(v);
				}
			}
			//校验物资是否被引用
			function checkmaterialunit(vcode){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/checkMaterial.do",{vcode:$("#vcode").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			var inputarray = $(".form-input").children().not("input[type='hidden']").not("div");
            inputarray.bind('keyup',function(){
                if(!event.ctrlKey&&event.keyCode==13){
                    var index = inputarray.index($(this)[0]);
                    if(index!=inputarray.length-1){
                        $(inputarray[index+1]).focus();
                    }
                }
            });
            function deleteRow(obj){
            	$(obj).closest("tr").find('td:first').html('');	
				$(obj).closest("tr").remove();
		};
		</script>
	</body>
</html>