<%--组织物资新增--%>
<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<style type="text/css">
				.page{ 
					margin-bottom:26px; 
 				} 
 				.treePanels{
 					width: 100%;
 				}
			</style>
		</head>
	<body>
	  	<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			<span style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
      	<div id="toolbar" class="toolbar"></div>
		<div class="leftFrame" style="overflow: auto;">
            <input type="hidden" value="${material}" id="material"/>
		    <div class="treePanels">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
		        <script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("00000000000000000000000000000000","<fmt:message key="supplies_category" />","")';
	          		<c:forEach var="materialType" items="${materialTypeList}" varStatus="status">
	          			tree.nodes['${materialType.parentMaterialtype.pk_materialtype}_${materialType.pk_materialtype}'] 
		          		= 'text:${materialType.vcode}--${materialType.vname}; method:changeUrl("${materialType.pk_materialtype}","${materialType.vcode}","${materialType.pk_father}")';
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
		    </div>
	    </div>
	    <div class="mainFrame">
           <div style="height: 80px;" id="supplyForm">
               <fmt:message key="supplies"/>:<br/>
               <input type="hidden" id="pk_material" name="pk_material" class="text" readonly="readonly"/>
               <textarea style="width:98%;margin:5px;height:50px; boder:0px;resize: none;" id="vname" name="vname" class="text" readonly="readonly" value="defaultName">${defaultName}</textarea>
           </div>
          <iframe src="<%=path%>/material/selectMaterialByPageList.do" frameborder="0" name="mainFrame" id="mainMyframe"></iframe>
	    </div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		function changeUrl(pk_materialtype,vcode,pk_father){
  			$("#wait2").css("display","block");
  			$("#wait").css("display","block");
			$('#pk_materialtype').val(pk_materialtype);
			$('#vcode').val(vcode);
			$('#pk_father').val(pk_father);
            window.mainFrame.location = "<%=path%>/material/selectMaterialByPageList.do?pk_materialtype="+pk_materialtype;
        }
		$(document).ready(function(){
// 			setElementHeight('.treePanel',['#toolbar']);
			setElementHeight('.leftFrame',['.toolbar'],$(document.body),35);
			setElementHeight('.mainFrame',['.toolbar'],$(document.body),35);
// 			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			$('.toolbar').toolbar({
	            items: [{
	                text: '<fmt:message key ="save" />',
	                icon: {
	                    url: '<%=path%>/image/Button/op_owner.gif',
	                    positn: ['-80px','-0px']
	                },
	                handler: function(){
	                    saveSelectMaterial();
	                }
	            },{
	                text: '取消 ',
	                icon: {
	                    url: '<%=path%>/image/Button/op_owner.gif',
	                    positn: ['-160px','-100px']
	                },
	                handler: function(){
	                    parent.$('.close').click();
	                }
	            }
	            ]
			});
		});
        var defaultCode = $('#pk_material').val();
        var defaultName = $('#vname').val();
        if(defaultCode!='' && defaultName!=''){
            $('#supplyBatchForm').find('div').find('input').val(defaultCode);
            $('#supplyBatchForm').find('div').find('textarea').val(defaultName);
        }
        var chkCode = defaultCode==''?[]:defaultCode.split(',');
        var chkName = defaultName==''?[]:defaultName.split(',');
        function selectSupply(code, name){
            var m = jQuery.inArray(code, chkCode);
            if (m>=0) {
                chkCode.splice(m,1);
                chkName.splice(m,1);
            }else {
                chkCode.push(code);
                chkName.push(name);
            }
            $('#pk_material').val(chkCode);
            $('#vname').val(chkName);
        }
        function selectAllSupply(code, name){
            var m = jQuery.inArray(code, chkCode);
            if (m>=0) {
                return;
            }else {
                chkCode.push(code);
                chkName.push(name);
            }
            $('#pk_material').val(chkCode);
            $('#vname').val(chkName);
        }
        function selectZeroSupply(code, name){
            var m = jQuery.inArray(code, chkCode);
            if (m>=0) {
                chkCode.splice(m,1);
                chkName.splice(m,1);
            }else {
                return;
            }
            $('#pk_material').val(chkCode);
            $('#vname').val(chkName);
        }
		//清空页面
		function clearValue(){
			window.frames["mainFrame"].clearValue();
		}
        var reload=function(){
            if(typeof(eval(parent.pageReload))=='function') {
                parent.pageReload();
            }else {
                parent.location.href = parent.location.href;
            }
            $(".close",parent.document).click();
        };
        var saveSelectMaterial=function(){
//             var checkboxList=$('.grid',window.frames['mainFrame'].document).find('.table-body').find(':checkbox');
            if($('#pk_material').val()!=""){
           		//functions是父页面的方法，固定写死的，需要在弹出新增、修改窗口的父页面加这个方法，在这个方法里调用新增、修改窗口的回调方法
           		parent.frames["${frameId}"].functions(chkCode,"${childFrameId}");
				$(".close",parent.document).click();
            }else{
                alerterror('<fmt:message key="please_select_information_you_need_to_save"/>！');
                return ;
            }
        };
        var reload=function(){
            if(typeof(eval(parent.pageReload))=='function') {
                parent.pageReload();
            }else {
                parent.location.href = parent.location.href;
            }
            $(".close",parent.document).click();
        };
	</script>

	</body>
</html>