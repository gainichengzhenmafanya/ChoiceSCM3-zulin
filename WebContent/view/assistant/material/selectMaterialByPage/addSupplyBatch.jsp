<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="supplies_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<style type="text/css">
			.textDisable{
				border: 0;
				background: #FFF;
			}
			.topFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.bottomFrame { 
				background-color:#FFF; 
				width:100%;
			}
			.check{
				float: left;
				vertical-align: middle;
				margin-right: 0px;
				height: 30px;
				line-height: 30px;
				text-align: right;
				font-weight: bold;
				padding-right: 5px;
				background-color: #E1E1E1;
			}
			.formInput {
				height: 30px;
				vertical-align: middle;
				line-height: 30px;
				text-align: left;
				padding-left: 5px;
				background-color: #E1E1E1;
				white-space: normal;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<div class="topFrame">
		<form id="supplyBatchForm" method="post" action="">
			<div class="form-line" style="background-color: #E1E1E1;height: 80px;;margin-right:0px;" id="supplyForm">
				<fmt:message key="supplies"/>:<br/>
				<!-- <div style="overflow: auto;height: 60px;background-color: #E1E1E1;" id="addPoEle"></div> -->
				<input type="hidden" id="parentId" name="code" class="text" readonly="readonly" value="${defaultCode}"/>
				<textarea style="width:780px; height:50px; boder:0px;background-color: #E1E1E1;" id="parentName" name="name" class="text" readonly="readonly"></textarea>
			</div>
		</form>
	</div>
	<div class="bottomFrame" style="height:85%">
		<iframe src="<%=path%>/supply/selectNSupply.do" frameborder="0" name="choiceFrame" id="choiceFrame"></iframe>
	</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter"/>',
							title: '<fmt:message key="determine_materials_selection"/>',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-120px','0px']
							},
							handler: function(){
								select_Supply();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						},{
							text: '<fmt:message key ="empty" />',
							title: '<fmt:message key ="empty" />',
							//useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-20px']
							},
							handler: function(){
								$('#parentId').val('');
								$('#parentName').val('');
								window.frames["choiceFrame"].clearValue();
							}
						}]
				});
				$('input:text[readonly]').addClass('textDisable');		//不可编辑颜色
			});
			function select_Supply(){
				var code = $('#parentId').val();
				if(code != null){
					parent['${callBack}'](code);
					$(".close",parent.document).click();
				}else{
					alert('<fmt:message key="please_select_materials"/>！');
				}
			}
			
			var defaultCode = '${defaultCode}';
			var defaultName = '${defaultName}';
			if(defaultCode!='' && defaultName!=''){
				$('#supplyBatchForm').find('div').find('input').val(defaultCode);
				$('#supplyBatchForm').find('div').find('textarea').val(defaultName);
			}
			var chkCode = defaultCode==''?[]:defaultCode.split(',');
			var chkName = defaultName==''?[]:defaultName.split(',');
			function selectSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectAllSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					return;
				}else {
					chkCode.push(code);
					chkName.push(name);
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			function selectZeroSupply(code, name){
				var m = jQuery.inArray(code, chkCode);
				if (m>=0) {
					chkCode.splice(m,1);
					chkName.splice(m,1);
				}else {
					return;
				}
				$('#supplyBatchForm').find('div').find('input').val(chkCode);
				$('#supplyBatchForm').find('div').find('textarea').val(chkName);
			}
			
		</script>
	</body>
</html>