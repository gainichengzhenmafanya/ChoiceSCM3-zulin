<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
/* 			.page{ */
/* 					margin-bottom:20px; */
/* 				} */
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<div >
			<form id="listForm" action="<%=path%>/material/materiallist.do?domId=${domId}&delivercode=${delivercode}&irateflag=${irateflag}" method="post">
				<input type="hidden" id="callBack" name="callBack" value="${callBack}"/>
				<input type="hidden" id="parentId" name="parentId" value=""/>
				<input type="hidden" id="parentName" name="parentName" value=""/>
				<input type="hidden" id="positncode" name="positncode" value="${positncode}"/>
				<table cellspacing="0" cellpadding="0">
					<tr >
						<td class="c-left"><fmt:message key="supplies_code" />:</td>
						<td><input type="text" style="width: 100px;" id="vcode" name="vcode" class="text" value="${material.vcode}" autocomplete="off"/></td>
						<td class="c-left"><fmt:message key="supplies_name" />:</td>
						<td><input type="text" style="width: 100px;" id="vname" name="vname" style="text-transform:uppercase;" class="text" value="${material.vname}"/></td>
						<td class="c-left"><fmt:message key="supplies_abbreviations" />:</td>
						<td><input type="text" style="width: 100px;" id="vinit" name="vinit" style="text-transform:uppercase;" class="text" value="${material.vinit}"/></td>
				        <td width="200">&nbsp;
				        	<input type="button" style="width:60px" id="search" name="search" value='<fmt:message key="select" />' onkeyup="ajaxSearch()"/>
				        </td>
				    </tr>
				</table>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span style="width:31px;"></span></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:80px;"><fmt:message key="supplies_code" /></span></td>
									<td><span style="width:100px;"></span><fmt:message key="supplies_name" /></td>
									<td><span style="width:70px;"></span><fmt:message key="supplies_abbreviations" /></td>
									<td><span style="width:70px;"></span><fmt:message key="supplies_specifications" /></td>
									<td><span style="width:60px;"></span><fmt:message key="standard_unit" /></td>
									<td><span style="width:70px;"></span><fmt:message key="Reference_resources_price" /></td>
									<td><span style="width:90px;"></span><fmt:message key="supplies_brands" /></td>
									<td><span style="width:60px;"></span><fmt:message key="tax_rate" /></td>
									<td><span style="width:60px;"></span><fmt:message key="Shelf_life" /></td>
									<td><span style="width:100px;"></span><fmt:message key="The_difference_quantity_inspection" /></td>
									<td><span style="width:100px;"></span><fmt:message key="Whether_can_return" /></td>
									<td><span style="width:100px;"></span><fmt:message key="Minimum_Purchase_Amount" /></td>
									<td><span style="width:150px;"></span><fmt:message key="supplies_origin" /></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" >
							<tbody>
								<c:forEach var="material" varStatus="step" items="${materialList}">
									<tr>
										<td class="num"><span title="" style="width:30px;">${step.count}</span></td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${material.pk_material}" value="${material.pk_material}"/>
										</span></td>
										<td><span title="${material.vcode}" style="width:80px;text-align: left;">${material.vcode}</span></td>
										<td><span title="${material.vname}" style="width:100px;text-align: left;">${material.vname}</span></td>
										<td><span title="${material.vinit}" style="width:70px;text-align: left;">${material.vinit}</span></td>
										<td><span title="${material.vspecfication}" style="width:70px;text-align: left;">${material.vspecfication}</span></td>
										<td><span title="${material.unitvname}" style="width:60px;text-align: left;">${material.unitvname}</span></td>
										<td><span title="${material.ninprice}" style="width:70px;text-align: right;"><fmt:formatNumber value="${material.nsaleprice}" pattern="##.##"/></span></td>
										<td><span title="${material.vbrand}" style="width:90px;text-align: left;">${material.vbrand}</span></td>
										<td><span title="${material.ntax}" style="width:60px;text-align: right;">${material.ntax}</span></td>
										<td><span title="${material.ishelflife}" style="width:60px;text-align: left;">${material.ishelflife}</span></td>
										<td><span title="${material.bisback}" style="width:100px;text-align: right;">${material.bisback}</span></td>
										<td><span title="${material.ncheckdiffrate}" style="width:100px;text-align: right;"><fmt:formatNumber value="${material.ncheckdiffrate}" pattern="##.##"/></span></td>
										<td><span title="${material.nminamt}" style="width:100px;text-align: right;"><fmt:formatNumber value="${material.nminamt}" pattern="##.##"/></span></td>
										<td><span title="${material.vaddr}" style="width:150px;text-align: left;">${material.vaddr}</span></td>
										<td style="display: none;"><span title="${material.pk_unit}" style="width:50px;text-align: left;display: none;">${material.pk_unit}</span></td>
										<td style="display: none;"><span title="${material.nminamt}" style="width:90px;text-align: left;display: none;">${material.nminamt}</span></td>
										<td style="display: none;"><span title="${material.ncheckdiffrate}" style="width:90px;text-align: left;display: none;">${material.ncheckdiffrate}</span></td>
										<td style="display: none;"><span title="${material.unitvcode}" style="width:90px;text-align: left;display: none;">${material.unitvcode}</span></td>
										<td style="display: none;"><span title="${material.pk_materialtype}" style="width:50px;text-align: left;display: none;">${material.pk_materialtype}</span></td>
										<td style="display: none;"><span title="${material.pk_materialtype}" style="width:50px;text-align: left;display: none;">${material.minbhcnt}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#vcode").focus();
				selected = '${domId}' == 'selected' ? parent.selected : (typeof(parent['${domId}']) == 'function' ? parent['${domId}']() : $.trim($('#${domId}',parent.document).val()).split(","));
				if(selected){
					$(".table-body").find('tr td input').each(function(){
						if($.inArray($(this).val(),selected) >= 0){//若大于等于零，则这个id已存在父页面中
							$(this).attr('checked','checked');
						}
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
// 				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+750));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+750));
// 				loadGrid();//  自动计算滚动条的js
				
// 				loadGrid();//  自动计算滚动条的js方法
// 				changeTh();//拖动 改变table 中的td宽度 
// 				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
// 				$('.grid').find('.table-body').find('tr').hover(
// 					function(){
// 						$(this).addClass('tr-over');
// 					},
// 					function(){
// 						$(this).removeClass('tr-over');
// 					}
// 				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								setValue();
							}
						},{
							text: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//------------------------------
				var mod = ${single};
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$("#chkAll").click(function(){
						if($(this)[0].checked){
							$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
						}else{
							$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
						}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = ${single};
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						//$(this).attr('checked','checked');
					}
					event.stopPropagation();
				});
				$('body').bind('keydown',fc = function(event){
					switch(event.keyCode){
					case 38:	
						var trs = $('.table-body').find('table').find('tr');
						if(trs.length==0){
							return;
						}
						var len = trs.find(':checkbox:checked').length;
						if(len==0){
							$(trs[trs.length-1]).find(':checkbox').attr('checked',true);
							$(trs[trs.length-1]).find(':checkbox').focus();
						}else{
							var tr = trs.find(':checkbox:checked').closest('tr');
							if(tr.prev()){
								tr.prev().find(':checkbox').attr('checked',true);
								tr.prev().find(':checkbox').focus();
								tr.find(':checkbox').attr('checked',false);
							}
						}
						break;
					
					case 40:	
						var trs = $('.table-body').find('table').find('tr');
						if(trs.length==0){
							return;
						}
						var len = trs.find(':checkbox:checked').length;
						if(len==0){
							$(trs[0]).find(':checkbox').attr('checked',true);
							$(trs[0]).find(':checkbox').focus();
						}else{
							var tr = trs.find(':checkbox:checked').closest('tr');
							if(tr.next()){
								tr.next().find(':checkbox').attr('checked',true);
								tr.next().find(':checkbox').focus();
								tr.find(':checkbox').attr('checked',false);
							}
						}
						break;
					case 13:	
						if((($('#vcode').val() != null && $('#vcode').val() != "") || 
								($('#vname').val() != null && $('#vname').val() != "") || 
								($('#vinit').val() != null && $('#vinit').val() != "")) && 
								$('.grid').find('.table-body').find(':checkbox').filter(':checked').size() == 0){
							$('#listForm').submit();
						}else{
							setValue();
						}
						break;
					case 27:	
						parent.$('.close').click();
						break;
					}
					
				})
			});
			
			function setValue(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var data = {show:[],code:[],mod:[],entity:[]};
				checkboxList.filter(':checked').each(function(){
					var entity = {};
					var row = $(this).closest('tr');
					data.code.push($(this).val());
					data.show.push($.trim(row.children('td:eq(3)').text()));
					entity.pk_material = $.trim($(this).val());
					entity.vcode = $.trim(row.children('td:eq(2)').text());
					entity.vname = $.trim(row.children('td:eq(3)').text());
					entity.vspecfication = $.trim(row.children('td:eq(5)').text());
					entity.vunitname = $.trim(row.children('td:eq(6)').text());
					entity.pk_unit = $.trim(row.children('td:eq(15)').text());
					entity.ninprice = $.trim(row.children('td:eq(7)').text());
					entity.nminamt = $.trim(row.children('td:eq(16)').text());
					entity.ncheckdiffrate = $.trim(row.children('td:eq(17)').text());
					entity.vunitcode = $.trim(row.children('td:eq(18)').text());
					entity.pk_materialtype = $.trim(row.children('td:eq(19)').text());
					entity.minbhcnt = $.trim(row.children('td:eq(20)').text());
					entity.pk_id = $.trim($(this).val());
					data.entity.push(entity);
				});
				if(data.entity[0] == undefined){
					var entity = {};
					entity.pk_material = '';
					entity.vcode = '';
					entity.vname = '';
					entity.vspecfication = '';
					entity.vunitname = '';
					entity.pk_unit = '';
					entity.ninprice = '';
					entity.nminamt = '';
					entity.ncheckdiffrate = '';
					entity.vunitcode = '';
					entity.pk_materialtype = '';
					entity.pk_id = '';
					data.entity.push(entity);
				}
				parent['${callBack}'](data);
				$(".close",parent.document).click();
			}
		</script>
	</body>
</html>