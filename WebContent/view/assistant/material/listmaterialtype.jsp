<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	</head>
	<body>
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="<%=path%>/materialType/tableMaterialType.do" method="post">
			<div class="grid" class="grid" style="overflow: auto;">
		    	<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
		    	<input type="hidden" id="lvl" name="lvl" value="${lvl}"/>
		    	<input type="hidden" id="vcode" name="vcode" value="${vcode}"/>
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0" id="thGrid">
						<thead>
							<tr>
								<td class="num"><span style="width:30px;"></span></td>
								<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
								<td><span style="width:120px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:160px;"><fmt:message key="name" /></span></td>
								<td><span style="width:90px;"><fmt:message key="enable_state" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table id="tblGrid" cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="materialType" varStatus="step" items="${listMaterialType}">
								<tr>
									<td class="num"><span title="" style="width:30px;">${step.count}</span></td>
									<td style="text-align: center;"><span style="width:30px;"></span>
										<input type="checkbox" name="idList" id="chk_${materialType.pk_materialtype}" value="${materialType.pk_materialtype}"/>
									</td>
									<td><span title="${materialType.vcode}" style="width:120px;text-align: left;">${materialType.vcode}</span></td>
									<td><span title="${materialType.vname}" style="width:160px;text-align: left;">${materialType.vname}</span></td>
									<td><span title="${materialType.enablestate}" style="width:90px;">
											<c:if test="${materialType.enablestate==1}"><fmt:message key="not_enabled" /></c:if>
											<c:if test="${materialType.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${materialType.enablestate==3}"><fmt:message key="stop_enabled" /></c:if>
									</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							if($("#pk_materialtype").val() != ""){
								$.ajaxSetup({async:false});
								$.post('<%=path%>/coderule/getFormat.do',{"pk_id":"00000000000000000000000000000012"},function(data){
									var rs = data;
									if(rs != ""){
										if("${lvl}">rs.split("-").length){
											alerterror( '<fmt:message key="cantaddchildtype" />');
											return;
										}else{
											save();
										}
									}else{
										save();
									}
								});	
							}else{
								alerterror('<fmt:message key="please_select_supplies_category" />');
							}
						}
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							update();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							deletematerialtype();
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				$("#wait2",parent.window.document).css("display","none");
				$("#wait",parent.window.document).css("display","none");
			});
			//新增物资类别
			function save(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="supplies_category" />',
					content: '<iframe id="saveMaterialType" frameborder="0" src="<%=path%>/materialType/toAddMaterialType.do?pk_materialtype=${pk_materialtype}&vcode=${vcode}&lvl=${lvl}"></iframe>',
					width: '550px',
					height: '350px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveMaterialType')&&window.document.getElementById("saveMaterialType").contentWindow.check()){
										window.document.getElementById("saveMaterialType").contentWindow.checkvcode();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改物资类别信息
			function update(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="supplies_category" />',
						content: '<iframe id="updateMaterialType" frameborder="0" src="<%=path%>/materialType/toUpdateMaterialType.do?lvl=${lvl}&vcode=${vcode}&pk_materialtype='+chkValue+'"></iframe>',
						width: '550px',
						height: '350px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateMaterialType')&&window.document.getElementById("updateMaterialType").contentWindow.check()){
											window.document.getElementById("updateMaterialType").contentWindow.checkvcode();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}

			//删除
			function deletematerialtype(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var pkss = chkValue.join(",");
						if(checkmaterialtype(pkss) && findMaterialTypeBytypepk(pkss)){
							$.ajaxSetup({async:false});
							$.post('<%=path%>/materialType/deleteMaterialType.do',{"pkss":pkss},function(data){
								var rs = data;
								switch(Number(rs)){
								case -1:
									alerterror('<fmt:message key="delete_fail"/>！');
									break;
								case 1:
									alerttipsbreak('删除成功！',function(){
										reloadPage();
									});
								}
							});	
						}else{
							alerterror('<fmt:message key="supplies_category" /><fmt:message key="refencenotdisable" />！');
						}
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			//校验物资类别是否被引用
			function checkmaterialtype(pkss){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/materialType/findMaterialTypeByName.do?pks="+pkss,'',function(data){
					if(!data)result = false;
				});
				return result;
			}
			//校验物资类别是否被引用
			function findMaterialTypeBytypepk(pkss){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/materialType/findMaterialTypeBytypepk.do?pks="+pkss,'',function(data){
					if(!data)result = false;
				});
				return result;
			}
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			//------------------------------
			//点击checkbox改变
			$('.grid .table-body tr').live('click',function(){
					 $('#pk_puprorderm').val($(this).find('td:eq(0)').find('span').attr('title'));
					 var pk_puprorderm=$(this).find('td:eq(0)').find('span').attr('title');
					 var istate = $(this).find('td:eq(6)').find('span').attr('title');
					 var url="<%=path%>/puprorder/queryAllPuprOrderds.do?pk_puprorder="+pk_puprorderm+"&istate="+istate;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
// 					 var $tmp=$('[name=idList]:checkbox');
// 					 $('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
			//---------------------------
			function reloadPage(){
				parent.refreshTree($("#pk_materialtype").val(),$("#vcode").val());
			}
			document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
		</script>
	</body>
</html>