<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			/* ====================== grid的样式 ====================== */
			#grid {
				positn: relative;
				overflow: auto;
				border-top: 1px solid #999999;
/* 				border-bottom: 1px solid #999999; */
				border-right: 1px solid #999999;
				border-left:1px solid #999999;
				/* width: 100% !important; */
			 /*	width: 99999999px;*/
				height: 100%;
			}
			
			#grid td {
				font-size:12px;
				white-space:nowrap;
				overflow: hidden;
				border-right: 1px solid #999999;
				border-bottom: 1px solid #999999;
				height: 25px;
				line-height: 25px;
				cursor: pointer;
			}
			
			#grid td span {
				display: block;
				overflow: hidden;
				white-space: nowrap;
				text-overflow: ellipsis;
				padding: 0px 5px;
			}
			
			#grid tr td.num {
			  width: 25px;
			/*  background-color: #D8DADC;*/
				
			/*  border-left: solid 1px #F0F0F0;*/
			  text-align: center;
			}
			#grid tr td.chk {
			  width: 25px; 
			  text-align: center;
			}
			
			#grid tr td.center {
			  text-align: center;
			}
			#grid tr td.left {
			  text-align: left;
			}
			#grid tr td.right {
			  text-align: right;
			}
			
			/* ====================== grid表头的样式 ====================== */
			
			.table-head { 
				overflow: hidden;
				background-color: #F9F9F9;
				/* background: url('../../image/Button_new/table_head2.png') repeat-x center center; */
				cursor:pointer;
			}
			
			.table-head td {
				font-size:14px;
				/*font-weight: bold;*/
				text-align: center;
				border-right: 1px solid #7B96B1;
				border-top: 0px solid #A4AEB5;
				border-bottom: 1px solid #92A0AA;
			}
			
			/* ====================== grid内容的样式 ====================== */
			
			.table-body {
				overflow: auto;
				overflow-x: hidden;
				background-color: #FFFFFF;
			}
			
			.table-body .even {
				 background-color: #E1E1E1;
			}
			
			.table-body .tr-toggle {
				background-color:#F1F4F9;
			}
			
			.table-body .tr-over {
				cursor: pointer;
				/*color: #FFFFFF;*/
				background-color: #D2E9FF;
			}
			
			.table-body .tr-over td {
			/*	color: #FFFFFF; */
			}
			.datagrid-sort-icon {
				padding: 2px 13px 3px 0px;
				background: url('../../image/themes/default/images/datagrid_sort_desc.gif') no-repeat center right;
			}
		</style>
	</head>
	<body>
		<div class="form" style="padding: 0px;">
			<form id="mallMaterial" method="post" action="<%=path %>/material/mallMaterial.do">
				<input type="hidden" style="width: 240px;" id="pk_material" name="pk_material" value="${material.pk_material}"/>
				<input type="hidden" style="width: 100px;" id="length" name="length" value="${length}"/>
				<div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_code" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="vcode" id="vcode" class="text" readonly="readonly" value="${material.vcode}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="specification" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" class="text" id="vspecfication" name="vspecfication"readonly="readonly"  value="${material.vspecfication}"/>
						</div>	
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_name" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="vname" id="vname" class="text" readonly="readonly" value="${material.vname}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="unit" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;margin-top: 4px;" name="unitvname" id="unitvname" class="text" readonly="readonly" value="${material.unitvname}"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Reference_resources_price" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;margin-top: 4px;" name="vcode" id="nsaleprice" class="nsaleprice" readonly="readonly" value="${material.nsaleprice}"/>
						</div>	
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="supplies_brands" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" class="text" id="vbrand" name="vbrand"readonly="readonly"  value="${material.vbrand}"/>
						</div>
<!-- 						<div class="form-input" style="width:100px;margin-left: 70px;"> -->
<%-- 							<input type="button" style="height: 30px;width: 100px;" id="insertmallmaterial" name="insertmallmaterial" value='<fmt:message key="Relation_mallmaterial" />'/> --%>
<!-- 							<input type="button" style="height: 30px;width: 100px;" id="deletemallmaterial" name="deletemallmaterial" value='删除关联物资' onclick="deleterows()"/> -->
<!-- 						</div> -->
					</div>
				</div>
				<hr />
				<div>
					<div class="easyui-tabs" fit="false" plain="true">
						<div title='商城商品信息'>
							<div id="grid">
								<div class="table-head">
									<table cellspacing="0" cellpadding="0">
										<thead>
											<tr>
												<td class="num"><span style="width: 16px;">&nbsp;</span></td>
				                                <td><span style="width: 160px;"><fmt:message key="supplies_name" /></span></td>
				                                <td><span style="width: 180px;"><fmt:message key="suppliers_name" /></span></td>
				                                <td><span style="width: 60px;"><fmt:message key="supplies" /><fmt:message key="offer" /></span></td>
											</tr>
										</thead>
									</table>
								</div>
								<div class="table-body" id="table-body">
									<table cellspacing="0" cellpadding="0" id="tab">
										<tbody>
											<c:forEach var="materialmall" items="${listMaterialMall}" varStatus="status">
												<tr>
													<td class="num"><span style="width: 16px;">${status.index+1}</span></td>
													<td><span style="width:160px;"title="${materialmall.vjmuname}">${materialmall.vjmuname}</span></td>
													<td><span style="width:180px;"> ${materialmall.vjmudelivername }</span></td>
													<td><span style="width:60px;text-align: right;">${materialmall.nprice}</span></td>
													<td style="display: none;"><span style="width:170px;"> ${materialmall.vjmusupplierpk}</span></td>
													<td style="display: none;"><span style="width:170px;"> ${materialmall.vhstore}</span></td>
													<td style="display: none;"><span style="width:170px;"> ${materialmall.vjmustorename}</span></td>
													<td style="display: none;"><span style="width:170px;"> ${materialmall.vhspec}</span></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div> 
						</div>
<!-- 						<div title='询价'> -->
						
<!-- 						</div> -->
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$('#insertmallmaterial').click(function(){
				selectJmuMaterialV2({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setJmuMaterial',
					domId:'delivercode',
					vname:$('#vname').val(),
					single:true
				});
			});
			function setJmuMaterial(data){
				var m = $('#grid').find('.table-body').find("table").find('tr').length + 1;
				for(var i in data.entity){
					var tr = '<tr>';
					tr += '<td class="num"><span style="width:16px;text-align: center;">';
					tr += m;
					tr += '</span></td>';
					tr += '<td><span style="width:20px; text-align: center;">';
					tr += '<input title="" type="checkbox" name="idList" value="00"/>';//
					tr += '</span></td>';
					tr += '<td><span style="width:100px;">';
					tr += data.entity[i].id;//商城物资编码
					tr += '</span></td>';
					tr += '<td><span style="width:160px;">';
					tr += data.entity[i].goodsname;//商城物资名字
					tr += '</span></td>';
					tr += '<td><span style="width:100px;">';
					tr += data.entity[i].sellerid;//商城店铺编码
					tr += '</span></td>';
					tr += '<td><span style="width:180px;">';
					tr += data.entity[i].sellername;//商城店铺名称
					tr += '</span></td>';
					tr += '<td><span style="width:60px;text-align: right;">';
					tr += data.entity[i].price;//报价
					tr += '</span></td>';
					tr += '<td><span style="width:100px;display: none;">';
					tr += data.entity[i].sellerid;//商城供应商主键
					tr += '</span></td>';
					tr += '<td><span style="width:100px;display: none;">';
					tr += data.entity[i].storeid;//商城供应商编码
					tr += '</span></td>';
					tr += '<td><span style="width:100px;display: none;">';
					tr += data.entity[i].storename;//商城供应商名称
					tr += '</span></td>';
					tr += '<td><span style="width:100px;display: none;">';
					tr += data.entity[i].id;//商城物资规格主键
					tr += '</span></td>';
					tr += '</tr>';
					$("#table-body").find("table").append(tr);
					m ++;
				}
			}
			function savemallmaterial(){
				var keys = ["vhspec","vjmucode","vjmuname","vjmudelivercode","vjmudelivername","nprice","vjmusupplierpk","vhstore","vjmustorename","vhspec"];
				var data = {};
				var i = 0;
				data["pk_material"] = $("#pk_material").val();
				var rows = $('#grid').find('.table-body').find('table').find('tr');
				for(i=0;i<rows.length;i++){
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value)
							data["listMaterialMall["+i+"]."+keys[j-1]] = value;
					}
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/material/mallMaterial.do",data,function(data){
					var rs = data;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			function deleterows(){
				var checkboxList = $('#grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />?",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							if($(this).attr("checked")){
								$('#grid').find('.table-body').find("table").find('tr:eq('+ (Number($(this).closest('tr').find('td:eq(0)').text())-1) +')').remove();
							}
						});
					});
						
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				}
			}
		</script>
	</body>
</html>