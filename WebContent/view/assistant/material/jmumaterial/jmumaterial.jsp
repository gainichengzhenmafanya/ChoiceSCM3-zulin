<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
/* 					margin-bottom:26px; */
				}
			.search-div .form-line .form-label{
					width: 10%;
				}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="<%=path%>/materialReview/jmuMaterial.do" method="post">
				<div class="grid" class="grid">
		    		<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td style="width:31px;"></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td style="display: none;"><span style="width:100px;">商城<fmt:message key="supplies_code" /></span></td>
									<td><span style="width:200px;">商城<fmt:message key="supplies_name" /></span></td>
									<td><span style="width:100px;">商城<fmt:message key="supplies_specifications" /></span></td>
									<td><span style="width:80px;"><fmt:message key="unit" /></span></td>
									<td style="display: none;"><span style="width:120px;">商城供应商编码</span></td>
									<td><span style="width:240px;">商城供应商名称</span></td>
									<td><span style="width:240px;">备注</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="revimaterial" varStatus="step" items="${listReviMaterial}">
									<tr>
										<td class="num"><span title="" style="width:20px;">${step.count}</span></td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${revimaterial.pk_revimaterial}" value="${revimaterial.pk_revimaterial}"/>
										</span></td>
										<td style="display: none;"><span title="${revimaterial.vhspec}" style="width:100px;text-align: left;">${revimaterial.vhspec}</span></td>
										<td><span title="${revimaterial.vname}" style="width:200px;text-align: left;">${revimaterial.vname}</span></td>
										<td><span title="${revimaterial.vspecfication}" style="width:100px;text-align: left;">${revimaterial.vspecfication}</span></td>
										<td><span title="${revimaterial.vunit}" style="width:80px;text-align: left;">${revimaterial.vunit}</span></td>
										<td style="display: none;"><span title="${revimaterial.delivercode}" style="width:120px;text-align: left;">${revimaterial.delivercode}</span></td>
										<td><span title="${revimaterial.delivername}" style="width:240px;text-align: left;">${revimaterial.delivername}</span></td>
										<td><span title="${revimaterial.vapplydesc}" style="width:240px;text-align: left;">${revimaterial.vapplydesc}</span></td>
										<td style="display: none;"><span style="width:240px;text-align: left;display: none;">${revimaterial.vhspec}</span></td><!--//商城物资规格主键-->
										<td style="display: none;"><span style="width:240px;text-align: left;display: none;">${revimaterial.vhstore}</span></td><!--店铺主键-->
										<td style="display: none;"><span style="width:240px;text-align: left;display: none;">${revimaterial.vjmustorename}</span></td><!--//店铺名称-->
										<td style="display: none;"><span style="width:240px;text-align: left;display: none;">${revimaterial.vjmusupplierpk}</span></td><!--/商城供应商主键-->
										<td style="display: none;"><span style="width:240px;text-align: left;display: none;">${revimaterial.nprice}</span></td><!--/商城供应商主键-->
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+200));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+200));
				loadGrid();//  自动计算滚动条的js
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
// 				$('.grid').find('.table-body').find('tr').live("click", function () {
// 					$(this).find(':checkbox').trigger('click');
// 				 });
// 				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
// 						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
// 						$(this).attr('checked','checked');
// 					event.stopPropagation();
// 				});
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '刷新',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							location.href=location.href;
						}
					},{
						text: '加入物资列表',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							savetomaterial();
						}
					},{
						text: '关联物资',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'link')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
                            link();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							deleteReviMaterial();
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			//加入到物资表
			function savetomaterial(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '添加到<fmt:message key="supplies" />',
						content: '<iframe id="updateMaterial" frameborder="0" src="<%=path%>/materialReview/toUpdateReviMaterial.do?pk_revimaterial='+chkValue+'"></iframe>',
						width: '650px',
						height: '400px',
						draggable: true,
						isModal: true
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('请选择需要添加的数据！');
					return ;
				}
			}
			//删除物资
			function deleteReviMaterial(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/materialReview/deleteReviMaterial.do?vcodes='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_systemcoding_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
				
			}
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':checkbox')[0].checked) {
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			});
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
        var link=function(){
            var checkboxList = $('.grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var action = '<%=path%>/materialReview/linkmaterialType.do?delivercodejum=' + encodeURI(checkboxList.filter(':checked').val());
                $('body').window({
                    title: '<fmt:message key="related"/><fmt:message key="supplies"/>',
                    content: '<iframe id="skipMaterialFrame" style="width: 100%;" frameborder="0" src="' + encodeURI(action) + '"></iframe>',
                    width: '850px',
                    height: '550px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [
                            {
                                text: '<fmt:message key="save"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-80px', '-0px']
                                },
                                handler: function () {
                                    var data={};
                                    var $tr=checkboxList.filter(':checked').parents("tr");
                                    data.pk_materialmall=$.trim(checkboxList.filter(':checked').val());
                                    data.vhspec=$.trim($tr.find("td:eq(9)").find("span").text());
                                    data.pk_material="";
                                    data.vjmuname= $.trim($tr.find("td:eq(3)").find("span").text());
                                    data.vjmucode=$.trim($tr.find("td:eq(2)").find("span").text());
                                    data.vhstore=$.trim($tr.find("td:eq(10)").find("span").text());
                                    data.nprice=$.trim($tr.find("td:eq(13)").find("span").text());
                                    data.vjmustorename=$.trim($tr.find("td:eq(11)").find("span").text());
                                    data.vjmudelivername=$.trim($tr.find("td:eq(7)").find("span").text());
                                    data.vjmusupplierpk=$.trim($tr.find("td:eq(12)").find("span").text());
                                    data.vjmudelivercode=$.trim($tr.find("td:eq(6)").find("span").text());
                                    getFrame('skipMaterialFrame').linkMaterial(data);
                                }
                            },
                            {
                                text: '<fmt:message key="cancel"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-160px', '-100px']
                                },
                                handler: function () {
                                    $('.close').click();
                                }
                            }
                        ]
                    }
                });
            }else if(checkboxList&&checkboxList.filter(':checked').size()>1){
                alerterror('<fmt:message key="a_data_can_be_set_at_a_time"/>');
            }else{
                alerterror('<fmt:message key="please_select_a_data"/>');
            }
        };
        function reload(){
        	$('#queryForm').submit();
        }
		</script>
	</body>
</html>