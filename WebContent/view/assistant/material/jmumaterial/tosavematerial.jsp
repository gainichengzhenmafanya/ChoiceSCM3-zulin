<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.divgrid {
				positn: relative;
				overflow: auto;
				border-top: 1px solid #999999;
				border-bottom: 1px solid #999999;
				border-right: 1px solid #999999;
				width: 90%;
				height: 85%;
			}
			
			.divgrid tr {
				white-space:nowrap;
				overflow: hidden;
				border-bottom: 1px solid #999999;
				height: 18px;
				line-height: 18px;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div id="tool"></div>
		<div class="form">
			<form id="saveMaterial" method="post" action="<%=path %>/material/updateMaterial.do">
			<input type="hidden" style="width: 240px;" id="pk_materialtype" name="pk_materialtype" value="${material.pk_materialtype}"/>
			<input type="hidden" style="width: 240px;" id="pk_material" name="pk_material" value="${material.pk_material}"/>
			<input type="hidden" style="width: 240px;" id="pk_vunit" name="pk_vunit" value="${material.pk_unit}"/>
			<input type="hidden" style="width: 240px;" id="pk_revimaterial" name="pk_revimaterial" value="${pk_revimaterial}"/>
			<div class="easyui-tabs" fit="false" plain="true" style="height:300px;width:600px;z-index:88;margin:0px auto;">
				<div title='<fmt:message key="basic_information" />' style="padding-top:30px;">
					<div class="form-line">
						<div class="form-label" style="width:150px;"><span class="red">*</span><fmt:message key="supplies_category" />:</div>
						<div class="form-input">
							<input type="hidden" id="code" name="code" class="text" readonly="readonly" value="${materialType.vcode}"/>
							<input type="text" id="name" name="name" class="text" readonly="readonly" value="${materialType.vname}" style="margin-top: -1px;width: 260px;"/>
							<img id="materialtypebutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width:150px;"><span class="red">*</span><fmt:message key="supplies_code" />:</div>
						<div class="form-input"><input type="text" id="vcode" name="vcode" class="text" value="${material.vcode }" maxlength="25"  style="width: 260px;"/></div>
					</div>
					<div class="form-line" >
					    <div class="form-label" style="width:150px;"><span class="red">*</span><fmt:message key="supplies_name" />:</div>
						<div class="form-input"><input type="text" id="vname" name="vname" class="text" onblur="getSpInit(this,'vinit');" value="${material.vname}" maxlength="25" style="width: 260px;"/></div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width:150px;"><span class="red">*</span><fmt:message key="Mnemonic" />:</div>
						<div class="form-input"><input type="text" id="vinit" name="vinit" class="text"  value="${material.vinit}" maxlength="20" style="width: 260px;"/></div>
					</div>
					<div class="form-line" >
					    <div class="form-label" style="width:150px;"><fmt:message key="supplies_specifications" />:</div>
						<div class="form-input"><input type="text" id="vspecfication" name="vspecfication" class="text" value="${material.vspecfication}" style="width: 260px;"/></div>
					</div>
					<div class="form-line">
						<div class="form-label" style="width:150px;"><fmt:message key="enable_state" />:</div>
						<div class="form-input">
							<select class="select" id="enablestate" name="enablestate" style="width:262px;">
								<option value="2"><fmt:message key="have_enabled" /></option>
								<option value="1"><fmt:message key="not_enabled" /></option>
								<option value="3"><fmt:message key="stop_enabled" /></option>
							</select>
						</div>
					</div>
				</div>
				<div title='<fmt:message key="convert_units" />' style="width: 100%;">
					<div id="unitForm" class="unitForm" style="width: 100%;margin: auto;margin-top: 10px;">
						<div class="form-line">
						    <div class="form-label"><span class="red">*</span><fmt:message key="standard_unit" />:</div>
							<div class="form-input">
								<select class="select" id="pk_unit" name="pk_unit" style="margin-top: 3px;" onchange="setunit(this)">
									<option></option>
									<c:forEach var="unit" items="${unitlist}" varStatus="status" >
										<option 
											<c:if test="${unit.pk_unit == material.pk_unit}"> selected="selected" </c:if>
											id="${unit.pk_unit}" value="${unit.pk_unit}">${unit.vname}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<input type="hidden" id="unitvname" name="unitvname" value="${unitvname}"/>
							</div>
						</div>
						<div id="divgrid" class="grid">
							<div class="table-head">
								<table>
									<thead>
										<tr style="height: 25px;">
											<td style="width:100px;"><fmt:message key="unit"/></td>
											<td style="width:100px;"><fmt:message key="unit_conversion_rate"/></td>
											<td style="width:80px;"><fmt:message key="Purchase_unit" /></td>
											<td style="width:80px;"><fmt:message key="Sale_unit" /></td>
											<td style="width:80px;"><fmt:message key="Stock_unit" /></td>
											<td style="width:60px;"><input type="button" id="addrow" class="addrow" style="width: 50px;" value="添加"/></td>
										</tr>
									</thead>
								</table>
							</div>
							<div class="table-body">
								<table id="tblGrid">
									<tbody>
										<tr style="height: 35px;">
											<td style="width:100px;">
												<select class="select" id="pk_unit1" name="pk_unit1" style="width:80px;margin-top: 1px;">
													<c:forEach var="unit" items="${unitlist}" varStatus="status" >
														<option 
																<c:if test="${unit.pk_unit == material.pk_unit}"> selected="selected" </c:if>
																id="${unit.pk_unit}" value="${unit.pk_unit}">${unit.vname}</option>
													</c:forEach>
												</select>
											</td>
											<td style="width:100px;">
												<input type="text" id="nrate" name="nrate" style="width:80px;text-align: left;" value="1.0"/>
											</td>
											<td style="width:80px;text-align: center;"><input id="firstbox1" name="cgbox" type="checkbox" checked="checked"/></td>
											<td style="width:80px;text-align: center;"><input id="secondbox1" name="xsbox" type="checkbox" checked="checked" /></td>
											<td style="width:80px;text-align: center;"><input id="thirdbox1" name="kcbox" type="checkbox" checked="checked" /></td>
											<td style="width:60px;"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div title='<fmt:message key="auxiliary_information" />' style="padding-top:20px;">
					<div class="form-line">
						<div class="form-label"><span class="red">*</span><fmt:message key="Reference_resources_price" />:</div>
						<div class="form-input"><input type="text" id=nsaleprice name="nsaleprice" class="text" value="${material.nsaleprice }"/></div>
						<div class="form-label"><span class="red">*</span><fmt:message key="min_chkstom_cnt" />:</div>
						<div class="form-input"><input type="text" id="minbhcnt" name="minbhcnt" class="text" value="0"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="tax_rate" />:</div>
						<div class="form-input">
							<select class="select" id="pk_tax" name="pk_tax" style="width:130px">
								<c:forEach var="tax" items="${taxList}" varStatus="status" >
									<option id="${tax.vcode}" 
									<c:if test="${tax.vcode==1 }">selected=selected</c:if>
									value="${tax.pk_tax}">${tax.vname}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-label"><span class="red">*</span><fmt:message key="Minimum_Purchase_Amount" />:</div>
						<div class="form-input"><input type="text" id="nminamt" name="nminamt" class="text" value="${material.nminamt }"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><fmt:message key="supplies_brands" />:</div>
						<div class="form-input"><input type="text" id="vbrand" name="vbrand" class="text"/></div>
					    <div class="form-label"><fmt:message key="supplies_origin" />:</div>
						<div class="form-input"><input type="text" id="vaddr" name="vaddr" class="text"/></div>
					</div>
					<div class="form-line">
					    <div class="form-label"><span class="red">*</span><fmt:message key="inspection_difference_lv" />:</div>
						<div class="form-input"><input type="number" id="ncheckdiffrate" name="ncheckdiffrate" class="text" value="0" style="width: 130px;"/><span>%</span></div>
				    </div>
					<div class="form-line" style="margin-top: 5px;">
					<div class="form-label"><fmt:message key="remark" /><fmt:message key="explain" />:</div>
						<div class="form-input">
							<textarea rows="5"  id="vmemo" name="vmemo" style="resize:none; width: 403px;"></textarea>
						</div>
					</div>
				</div>
			</div>
			</form>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
		<script type="text/javascript">
			var savelock = false;
			function setMaterialType(data){
				$('#pk_materialtype').val(data.pk_materialtype);
				$('#code').val(data.vcode);
				$('#name').val(data.vname);
				
				var param = {};
				param['materialtypevcode']=data.vcode;
				$.post("<%=path%>/material/findMaterialVcode.do",param,function(re){
					$('#vcode').val(re);
				});
			}
			var validate;
			var checkunit = true;//验证转换率
			var unitarray = [];
			
			$(document).ready(function(){
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							if(saveCheck()){
								checkvcoderole();
							}else{
								$("#wait2").css("display","none");
								$("#wait").css("display","none");
							}
						}
					},{
						text: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}]
				});
				$('#unitvname').val($('#pk_unit').find("option:selected").text());
				$("input[name='cgbox']").live('click',function(event){
					$("input[name='cgbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("input[name='xsbox']").live('click',function(event){
					$("input[name='xsbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("input[name='kcbox']").live('click',function(event){
					$("input[name='kcbox']").not($(this)).removeAttr("checked");
					event.stopPropagation();
				});
				$("#vname").focus();
				$(".table-body").height(190);
				//--------------------------输入框支持回车start---------------------------------
				var inputarray = $("input[type='text']");
				$("input[type='text']").bind('keyup',function(){
					if(event.keyCode==13){
						var index = inputarray.index($(this)[0]);
						if(index!=inputarray.length-1){
							$(inputarray[index+1]).focus();
						}
					}
				});
				$('#materialtypebutton').click(function(){
					selectMaterialType({
						basePath:'<%=path%>',
						title:'123',
						height:300,
						width:360,
						callBack:'setMaterialType',
						single:true
					});
				});
				//--------------------------输入框支持回车end----------------------------------
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
				//验证编码规则
			function checkvcoderole(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				codeval({
				    action:"<%=path%>",
				    callback:"getUnitValues"
			    },
			    "00000000000000000000000000000011",
			    $("#vcode").val(),
			    $("#code").val(),
			    0);
			}
			//新增验证
			function saveCheck(){
				var pk_materialtype = $.trim($('#pk_materialtype').val());
				var pk_material = $.trim($('#pk_material').val());
				var vcode = $.trim($('#vcode').val());
				$('#vcode').val($.trim($('#vcode').val()));
				if (pk_materialtype == '' || pk_materialtype == null) {
					alerterror('<fmt:message key="please_select_supplies_category" />！');
					return false;
				}
				if (vcode == '' || vcode == null) {
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(vcode.length > 25){
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="length_too_long" />！');
					return false;
				} else if(!checkvcode(vcode,pk_material)){
					alerterror('<fmt:message key="coding" /><fmt:message key="already_exists" />！');
					return false;
				} else if(/[?:"{},\/;'[\]]/im.test(vcode)){
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="cannot_contain_special_characters" />!');
					return false;
				} else if(/[\u4E00-\u9FA5]/im.test(vcode)){
					alerterror('编码不能为汉字!');
					return false;
				}
				var vname = $.trim($('#vname').val()).replace(' ','');
				$('#vname').val($.trim($('#vname').val()));
				if (vname == '' || vname == null) {
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(vname.length > 25){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！');
					return false;
				} else if(!checkvname(vname,pk_material)){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="already_exists" />！');
					return false;
				} else if(/[?:"{},\/;'[\]]/im.test(vname)){
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="cannot_contain_special_characters" />!');
					return false;
				}
				var vinit = $.trim($('#vinit').val());
				if (vinit == '' || vinit == null) {
					alerterror('<fmt:message key="Mnemonic" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(vinit.length > 20){
					alerterror('<fmt:message key="Mnemonic" /><fmt:message key="the_maximum_length_not" />20！');
					return false;
				}
				var pk_unit = $.trim($('#pk_unit').val());
				if(pk_unit == null || pk_unit == ''){
					alerterror('<fmt:message key="standard_unit" /><fmt:message key="cannot_be_empty" />');
					return false;
				}
				var pk_tax = $.trim($('#pk_tax').val());
				if(pk_tax == null || pk_tax == ''){
					alerterror('<fmt:message key="tax_rate" /><fmt:message key="cannot_be_empty" />');
					return false;
				}
				var nminamt = $.trim($('#nminamt').val());
				if (nminamt == '' || nminamt == null) {
					alerterror('<fmt:message key="Minimum_Purchase_Amount" /><fmt:message key="cannot_be_empty" />！');
					return;
				} else if(isNaN(nminamt)){
					alerterror('<fmt:message key="Minimum_Purchase_Amount" /><fmt:message key="must_be_numeric" />！');
					return;
				}
				var nsaleprice = $.trim($('#nsaleprice').val());
				if (nsaleprice == '' || nsaleprice == null) {
					alerterror('<fmt:message key="Reference_resources_price" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(isNaN(nsaleprice)){
					alerterror('<fmt:message key="Reference_resources_price" /><fmt:message key="must_be_numeric" />！');
					return false;
				}
				var ncheckdiffrate = $.trim($('#ncheckdiffrate').val());
				if(isNaN(ncheckdiffrate)){
					alerterror('<fmt:message key="inspection_difference_lv" /><fmt:message key="must_be_numeric" />！');
					return false;
				} else if(ncheckdiffrate == null || ncheckdiffrate == ''){
					alerterror('<fmt:message key="inspection_difference_lv" /><fmt:message key="cannot_be_empty" />！');
					return false;
				} else if(Number(ncheckdiffrate) < -100 || Number(ncheckdiffrate) > 100){
					alerterror('验货差异率的浮动范围是：-100% < 差异率 < 100%,请正确输入！');
					return false;
				}
				var vbrand = $.trim($('#vbrand').val());
				if(vbrand.length > 50){
					alerterror('<fmt:message key="supplies_brands" /><fmt:message key="the_maximum_length_not" />50！');
					return false;
				}
				var vaddr = $.trim($('#vaddr').val());
				if(vaddr.length > 50){
					alerterror('<fmt:message key="supplies_origin" /><fmt:message key="the_maximum_length_not" />50！');
					return false;
				}
				var vmemo = $.trim($('#vmemo').val());
				if(vmemo.length > 100){
					alerterror('<fmt:message key="scscbzbz" /><fmt:message key="the_maximum_length_not" />100！');
					return false;
				}
				var minbhcnt = $.trim($('#minbhcnt').val());
				if(minbhcnt == null || minbhcnt == ''){
					alerterror('<fmt:message key="min_chkstom_cnt" /><fmt:message key="cannot_be_empty" />');
					return false;
				}else if(Number(minbhcnt) < 0){
					alerterror('<fmt:message key="min_chkstom_cnt" /><fmt:message key="Negative" />');
					return false;
				}
				return true;
			}
			//检测编码是否存在
			function checkvcode(vcode,pk_material){;
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/findMaterialByCodeUp.do",{vcode:$('#vcode').val(),pk_material:$('#pk_material').val()},function(data){
					//如果已经存在
					if(!data)result = false;
					savelock = false;
				});
				return result;
			}
			//检测名称是否存在
			function checkvname(vname,pk_material){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/findMaterialByNameUp.do",{vname:$("#vname").val(),pk_material:$('#pk_material').val()},function(data){
					//如果已经存在
					if(!data)result = false;
					savelock = false;
				});
				return result;
			}
			//验证单位是否符合要求
			function checkUnit(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				checkunit = true;//验证转换率
				if($('#tblGrid').find('tr').length==0){
					alerterror('<fmt:message key="please_set" /><fmt:message key="unit_conversion_rate" />！');
					savelock = false;
					checkunit = false;
					return;
				}
				var checkarray = [];
				var cgdw = false;
				$('#tblGrid').find('tr').each(function(){
					var pk_unit = $(this).find('td:first').find('select option:selected').val();
					var vunitcode = $(this).find('td:first').find('select option:selected').attr('vcode');
					var vunitname = $(this).find('td:first').find('select option:selected').text();
					var nrate = $(this).find('td:eq(1)').find('input').val();
					var bispurchase = $(this).find('td:eq(2)').find('input')[0].checked;
					var bissale = $(this).find('td:eq(3)').find('input')[0].checked;
					var bisstore = $(this).find('td:eq(4)').find('input')[0].checked;
					if(isNaN(nrate) || Number(nrate)<0){
						alerterror('<fmt:message key="unit_conversion_rate" /><fmt:message key="no_right" />！');
						savelock = false;
						checkunit = false;
						return;
					}
					if(bispurchase){
						cgdw = true;
					}
					if(bispurchase || bissale || bisstore){
						if($.inArray(pk_unit,checkarray)==-1){
							var unit = {};
							unit.pk_unit = pk_unit;
							unit.vunitcode = vunitcode;
							unit.vunitname = vunitname;
							unit.nrate = nrate;
							unit.bispurchase = bispurchase?1:0;
							unit.bissale = bissale?1:0;
							unit.bisstore = bisstore?1:0;
							unitarray.push(unit);
							checkarray.push(pk_unit);
						}else{
							alerterror('<fmt:message key="unit" /><fmt:message key="duplicate" />！');
							savelock = false;
							checkunit = false;
							return;
						}
					}
				});
				if(!cgdw){
					checkunit = false;
					alerterror('采购单位必须选择！');	
					savelock = false;
				}
				
			}
			function setunit(e){
				var pk_unit = $(e).val();
				$("#pk_unit1").find("option[value='"+pk_unit+"']").attr("selected",true);
			}
			//单位换算添加行
			$('#addrow').click(function (){
				var trtd = '<tr style="height: 35px;">'+
								'<td style="width:100px;">'+
								'<select class="select" id="pk_unit1" name="pk_unit1" style="width:80px;margin-top: 1px;">'+
									<c:forEach var="unit" items="${unitlist}" varStatus="status" >
										'<option id="${unit.pk_unit}" vcode="${unit.vcode}" value="${unit.pk_unit}">${unit.vname}</option>'+
									</c:forEach>
								'</select>'+
							'</td>'+
							'<td style="width:100px;">'+
								'<input type="text" id="nrate" name="nrate" style="width:80px;text-align: left;" value="0.0"/>'+
							'</td>'+
							'<td style="width:80px;text-align: center;"><input name="cgbox" type="checkbox"/></td>'+
							'<td style="width:80px;text-align: center;"><input name="xsbox" type="checkbox"/></td>'+
							'<td style="width:80px;text-align: center;"><input name="kcbox" type="checkbox"/></td>'+
							'<td style="width:60px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></td>'+
						'</tr>';
				$('#tblGrid').append(trtd);
			});
			
			//循环遍历单位转换率。保存数据
			function getUnitValues(message){
				if(message != 'ok'){
					alerterror('<fmt:message key="code_error"/>');
					savelock = false;
					return;
				}
				savelock = false;//检测单位需要重置这个状态，单位检测完毕后需要再置为true
				checkUnit();//验f证单位转换率
				if(!checkunit){
					savelock = false;
					return;	
				} 
				savelock = true;//检测单位需要重置这个状态，单位检测完毕后需要再置为true
				var data = {};
				data["pk_material"] = $("#pk_material").val();
				data["pk_materialtype"] = $.trim($("#pk_materialtype").val());
				data["vcode"] = $.trim($("#vcode").val());
				data["vname"] = $.trim($("#vname").val());
				data["vinit"] = $.trim($("#vinit").val());
				data["vspecfication"] = $.trim($("#vspecfication").val());
				data["enablestate"] = $.trim($("#enablestate").val());
				data["nsaleprice"] = $.trim($("#nsaleprice").val());
				data["ninprice"] = $.trim($("#nsaleprice").val());
				data["pk_tax"] = $.trim($("#pk_tax").val());
				data["nminamt"] = $.trim($("#nminamt").val());
				data["vbrand"] = $.trim($("#vbrand").val());
				data["vaddr"] = $.trim($("#vaddr").val());
				data["pk_unit"] = $.trim($("#pk_unit").val());
				data["unitvname"] = $.trim($("#unitvname").val());
				data["ncheckdiffrate"] = $.trim($("#ncheckdiffrate").val());
				data["minbhcnt"] = $.trim($("#minbhcnt").val());
				data["pk_revimaterial"] = $("#pk_revimaterial").val();
				data["vmemo"] = $.trim($("#vmemo").val());
				
				for(var i=0;i<unitarray.length;i++){
					data["listMaterialUnit["+i+"].pk_unit"] = unitarray[i].pk_unit;
					data["listMaterialUnit["+i+"].vunitcode"] = unitarray[i].vunitcode;
					data["listMaterialUnit["+i+"].vunitname"] = unitarray[i].vunitname;
					data["listMaterialUnit["+i+"].nrate"] = unitarray[i].nrate;
					data["listMaterialUnit["+i+"].bispurchase"] = unitarray[i].bispurchase;
					data["listMaterialUnit["+i+"].bissale"] = unitarray[i].bissale;
					data["listMaterialUnit["+i+"].bisstore"] = unitarray[i].bisstore;
				}
						
				$.ajaxSetup({async:false});
				$.post("<%=path%>/material/addMallMaterial.do",data,function(data){
					savelock = true;//加锁
					var rs = data;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						$("#wait2").css("display","none");
						$("#wait").css("display","none");
						break;
					case 1:
						$("#wait").css("display","none");
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reload();
										parent.$('.close').click();
									}
								});
						break;
					case 2:
						alerterror('尚未关联商城供应商，请先关联供应商再加入物资！');
						savelock = false;
						$("#wait2").css("display","none");
						$("#wait").css("display","none");
						break;
					case 3:
						alerterror('物资已经关联当前供应商的物资，不可重复关联！');
						savelock = false;
						$("#wait2").css("display","none");
						$("#wait").css("display","none");
						break;
					}
				});	
			}
			var inputarray = $(".form-input").children().not("input[type='hidden']").not("div");
            inputarray.bind('keyup',function(){
                if(!event.ctrlKey&&event.keyCode==13){
                    var index = inputarray.index($(this)[0]);
                    if(index!=inputarray.length-1){
                        $(inputarray[index+1]).focus();
                    }
                }
            });
            function deleteRow(obj){
            	$(obj).closest("tr").find('td:first').html('');	
				$(obj).closest("tr").remove();
		};
		</script>
	</body>
</html>