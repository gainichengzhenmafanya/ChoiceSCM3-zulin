<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
					margin-bottom:2px;
				}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div>
	    	<%--<div id="tool"></div>--%>
	    	<form id="queryForm" action="<%=path%>/material/tableLinkMaterial.do" method="post">
	    		<div class="condition">
					<div class="form-line">
						<div class="form-label" style="width: 60px;"><fmt:message key="supplies_code" />:</div>
						<div class="form-input" style="width: 100px;">
							<input type="text" style="width: 100px;" id="vcode" name="vcode" style="text-transform:uppercase;" value="${material.vcode}" onkeyup="ajaxSearch()" class="text"/>
						</div>
						<div class="form-label" style="width: 60px;"><fmt:message key="supplies_name" />:</div>
						<div class="form-input" style="width: 100px;">
							<input type="text" style="width: 100px;" id="vname" name="vname" style="text-transform:uppercase;" value="${material.vname}" onkeyup="ajaxSearch()" class="text"/>
						</div>
						<div class="form-label" style="width: 60px;"><fmt:message key="Mnemonic" />:</div>
						<div class="form-input" style="width: 100px;">
							<input type="text" style="width: 100px;" id="vinit" name="vinit" style="text-transform:uppercase;" value="${material.vinit}" onkeyup="ajaxSearch()" class="text"/>
						</div>
                        <div class="form-input" style="width:40px;">
                            <input type="submit" style="margin-top: 2px;" class="search-button" id="search" value='<fmt:message key="select" />'/>
                        </div>
					</div>
				</div>
				<div class="grid" class="grid">
		    		<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td><span style="width:21px;"></span></td>
									<td><span style="width:20px;"></span></td>
									<td><span style="width:160px;"><fmt:message key="supplies_code" /></span></td>
									<td><span style="width:160px;"><fmt:message key="supplies_name" /></span></td>
									<td><span style="width:60px;"><fmt:message key="Mnemonic" /></span></td>
									<td><span style="width:140px;"><fmt:message key="supplies_category" /></span></td>
									<td><span style="width:80px;"><fmt:message key="supplies_specifications" /></span></td>
									<td><span style="width:80px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:80px;"><fmt:message key="Reference_resources_price" /></span></td>
									<td><span style="width:90px;"><fmt:message key="enable_state" /></span></td>
									<td><span style="width:120px;"><fmt:message key="supplies_brands" /></span></td>
									<td><span style="width:200px;"><fmt:message key="supplies_origin" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="overflow-x: hidden;">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="material" varStatus="step" items="${materialList}">
									<tr>
										<td class="num" style="border-left: 0px;"><span title="" style="width:21px;">${step.count}</span></td>
										<td><span style="width:20px; text-align: center;">
											<input type="radio" name="idList" id="chk_${material.pk_material}" value="${material.pk_material}"/>
										</span></td>
										<td><span title="${material.vcode}" style="width:160px;text-align: left;">${material.vcode}</span></td>
										<td><span title="${material.vname}" style="width:160px;text-align: left;">${material.vname}</span></td>
										<td><span title="${material.vinit}" style="width:60px;text-align: left;">${material.vinit}</span></td>
										<td><span title="${material.materialtype.vname}" style="width:140px;text-align: left;">${material.materialtype.vname}</span></td>
										<td><span title="${material.vspecfication}" style="width:80px;text-align: left;">${material.vspecfication}</span></td>
										<td><span title="${material.unitvname}" style="width:80px;text-align: left;">${material.unitvname}</span></td>
										<td><span title="${material.ninprice}" style="width:80px;text-align: right;">${material.nsaleprice}</span></td>
										<td><span title="${material.enablestate}" style="width:90px;">
											<c:if test="${material.enablestate==1}"><fmt:message key="not_enabled" /></c:if>
											<c:if test="${material.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${material.enablestate==3}"><fmt:message key="stop_enabled" /></c:if>
										</span></td>
										<td><span title="${material.vbrand}" style="width:120px;text-align: left;">${material.vbrand}</span></td>
										<td><span title="${material.vaddr}" style="width:200px;text-align: left;">${material.vaddr}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
			<div class="print-div">
				<form id="printForm" action="<%=path%>/supply/toReport.do" method="post">
				<input type="hidden" name="level" value="${level}"/>
				<input type="hidden" name="code" value="${code}"/>
				<input type="hidden" name="type" value="excel"/>
				<input  type="hidden" name="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
				<input  type="hidden" name="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
				<input type="hidden" name ="pageSize" value="${pageobj.pageSize }" />
				<div class="print-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="export_range" />：</td>
							<td><input type="text" id="reportFrom" name="reportFrom"   value="${pageobj.nowPage}" size="4" /></td>
							<td class="c-left"><fmt:message key="to" />：</td>
							<td><input type="text" id="reportTo" name="reportTo"  value="${pageobj.pageCount}" size="4"  /><fmt:message key="page" /></td>
						</tr>
					</table>
				</div>
				<div class="print-commit">
		       		<input type="button" class="print-button" id="print" value='<fmt:message key="export" />'/>
				</div>
				</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
				$("#vcode").focus();
				/* 导出提交 */
				$("#print").bind('click', function() {
					var reportFrom=$('#reportFrom').val();
					var reportTo=$('#reportTo').val();
					var ex = /^[0-9]*[1-9][0-9]*$/;
					if (ex.test(reportFrom) && ex.test(reportTo)) {
						$('.print-div').hide();
						$('#printForm').submit();
						
					}else{
						alerterror('<fmt:message key="export_range_please_enter_positive_integer" />！');
					}
				});
				setElementHeight('.grid',['.condition'],$(document.body),26);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()) + 200);
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()) + 200);
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
                var $grid=$(".grid");
                var headWidth=$grid.find(".table-head").find("tr").width();
                var gridWidth=$grid.width();
                if(headWidth>=gridWidth){
                    $grid.find(".table-body").width(headWidth);
                    $grid.find(".table-head").width(headWidth);
                }else{
                    $grid.find(".table-body").width(gridWidth);
                    $grid.find(".table-head").width(gridWidth);
                }
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':radio').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':radio')[0].checked) {
					$(this).find(':radio').attr("checked", false);
				}else{
					$(this).find(':radio').attr("checked", true);
				}
			});
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			//倒入
			function importSupply(){
		    	$('body').window({
					id: 'window_importSupply',
					title: '<fmt:message key="import" /><fmt:message key="supplies_information" />',
					content: '<iframe id="importSupply" frameborder="0" src="<%=path%>/material/importMaterial.do"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		    }
			function reloadPage(){
				$('#queryForm').submit();
			}
		    //上传物资
		    function uploadMaterial(){
		    	 var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		         if(checkboxList&&checkboxList.filter(':checked').size() > 0){
		        	 alertconfirm('<fmt:message key="whether_upload"/><fmt:message key="supplies"/>?',function(){
		                 var codeValue=[];
		                 checkboxList.filter(':checked').each(function(){
		                     codeValue.push($(this).val());
		                 });
		                 $.ajaxSetup({async:false});
							var datas = {};
							datas["pk_material"] = codeValue.join(",");
							$.post('<%=path%>/assitWebservice/uploadMaterial.do',datas,function(data){
								if(data=="1"){
									showMessage({
										type: 'success',
										msg: "操作成功",
										speed: 1500
									});
								}else{
									showMessage({
										type: 'error',
										msg: data,
										speed: 1500
									});
								}
							});
		             });
		         }else{
		             alerterror('<fmt:message key="pleaseselectuploaddata"/>！');
		             return ;
		         }
		    }
		    //导出物资
		    function exportSupply(){
		    	$("#wait2").val('NO');//不用等待加载
		    	$("#queryForm").attr("action","<%=path%>/material/exportExcel.do");
		    	$("#queryForm").submit();
			$("#wait2 span").html("数据导出中，请稍后...");
			$('#queryForm').attr("action","<%=path%>/material/tableMaterial.do");	
			$("#wait2").val('');//等待加载还原
		    }
		</script>
	</body>
</html>