<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="module_information"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		</head>
	<body>
		<div class="leftFrame">
	      	<%--<div id="toolbar"></div>--%>
            <input type="hidden" value="${jmuMaterial}" id="jmuMaterial"/>
		    <div class="treePanel">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
		        <script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category" />;method:changeUrl("00000000000000000000000000000000","<fmt:message key="supplies_category" />","")';
	          		<c:forEach var="materialType" items="${materialTypeList}" varStatus="status">
	          			tree.nodes['${materialType.parentMaterialtype.pk_materialtype}_${materialType.pk_materialtype}'] 
		          		= 'text:${materialType.vcode}--${materialType.vname}; method:changeUrl("${materialType.pk_materialtype}","${materialType.vcode}","${materialType.pk_father}")';
	          		</c:forEach>
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
		    </div>
	    </div>
	    <div class="mainFrame">
          <iframe src="<%=path%>/material/tableLinkMaterial.do" frameborder="0" name="mainFrame" id="mainMyframe"></iframe>
	    </div>
		<input type="hidden" id="vcode" name="vcode" value="${vcode}"/>
	    <input type="hidden" id="vname" name="vname" value="${vname}"/>
	    <input type="hidden" id="pk_father" name="pk_father" value="${pk_father}"/>
	    <input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
	    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript">
		function changeUrl(pk_materialtype,vcode,pk_father){
			$('#pk_materialtype').val(pk_materialtype);
			$('#vcode').val(vcode);
			$('#pk_father').val(pk_father);
	      	window.mainFrame.location = "<%=path%>/material/tableLinkMaterial.do?pk_materialtype="+pk_materialtype;
	    }
			
	    function refreshTree(pk_materialtype){
	    	if(pk_materialtype == null){
	    		window.location.href = '<%=path%>/material/materialType.do';
	    	}else{
				window.location.href = '<%=path%>/material/materialType.do?pk_materialtype='+pk_materialtype;
	    	}
	    }
		$(document).ready(function(){
			setElementHeight('.treePanel',['#toolbar']);
			tree.focus('${materialType.pk_materialtype}');
			changeUrl('${materialType.pk_materialtype}','${materialType.vname}','${materialType.vcode}');
		});// end $(document).ready();
		//清空页面
		function clearValue(){
			window.frames["mainFrame"].clearValue();
		}
        var reload=function(){
            if(typeof(eval(parent.pageReload))=='function') {
                parent.pageReload();
            }else {
                parent.location.href = parent.location.href;
            }
            $(".close",parent.document).click();
        };
        var linkMaterial=function(data){
            var cbox=$('.grid',window.frames['mainFrame'].document).find('.table-body').find(':checked');
            if($.trim(cbox.val())==""){
                alerterror('<fmt:message key="please_select_a_data"/>');
                return ;
            }
            data.pk_material=cbox.val();
			$.ajaxSetup({ 
				  async: false 
			});
            $.post("<%=path%>/material/linkMaterial.do", data, function (msg) {
               if (msg == 1||msg=="1") {
//                     alerttipsbreak('<fmt:message key="related"/><fmt:message key="successful"/>',function(){
//                     });
                    alerttips('<fmt:message key="related"/><fmt:message key="successful"/>');
                   	parent.location.href = parent.location.href;
                    $(".close",parent.document).click();
                } else if(msg==2||msg=='2'){
                    alerttips('<fmt:message key="link_materia"/>');
                }else if(msg==3||msg=='3'){
                    alerterror('<fmt:message key="link_material_error"/>');
                }else{
                    alerterror('<fmt:message key="related"/><fmt:message key="failure"/>');
                }
            });
        };
		//关闭提示框
		function  closemsgwin(){
			top.$("#messagewin").hide();
			top.$("#messagewin").remove();
			top.$("#wait2").hide();
		}
	</script>

	</body>
</html>