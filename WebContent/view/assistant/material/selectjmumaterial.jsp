<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
/* 			.page{ */
/* 					margin-bottom:26px; */
/* 				} */
		</style>
	</head>
	<body>
		<div class="tool">
		</div>
		<div >
			<form id="listForm" action="<%=path%>/material/jmumateriallist.do" method="post">
				<input type="hidden" id="callBack" name="callBack" value="${callBack}"/>
				<input type="hidden" id="parentId" name="parentId" value=""/>
				<input type="hidden" id="parentName" name="parentName" value=""/>
				<div>
					<div class="form-line">
						<div class="form-label" style="width: 100px;"><fmt:message key="supplies_code" />:</div>
						<div class="form-input">
							<input type="text" id="vcode" name="vcode" style="text-transform:uppercase;" value="${material.vcode}" class="text"/>
						</div>
						<div class="form-label"><fmt:message key="supplies_name" />:</div>
						<div class="form-input">
							<input type="text" id="vname" name="vname" style="text-transform:uppercase;" value="${material.vname}" class="text"/>
						</div>
						<div class="form-label"><fmt:message key="Mnemonic" />:</div>
						<div class="form-input">
							<input type="text" id="vinit" name="vinit" style="text-transform:uppercase;" value="${material.vinit}" class="text"/>
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       			<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
					</div>
				</div>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 20px;"></span></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll" />
									</span></td>
									<td><span style="width:120px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:200px;"><fmt:message key="name" /></span></td>
									<td><span style="width:70px;"><fmt:message key="specification" /></span></td>
									<td><span style="width:70px;"><fmt:message key="unit_of_measure" /></span></td>
									<td><span style="width:60px;"><fmt:message key="scscshichangjia" /></span></td>
									<td><span style="width:60px;"><fmt:message key="scscshangchengjia" /></span></td>
									<td><span style="width:50px;"><fmt:message key="scscqidingliang" /></span></td>
									<td><span style="width:170px;"><fmt:message key="suppliers" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0" >
							<tbody>
								<c:forEach var="commodity" items="${sellerList}"	varStatus="status">
									<tr>
										<td class="num"><span style="width: 20px;">${status.index+1}</span>
										</td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="<c:out value='${commodity.id}' />"
												value="<c:out value='${commodity.id}' />" />
										</span></td>
										<td><span style="width:120px;" title="${commodity.goods.sn}">${commodity.goods.sn}</span></td>
										<td>
											<span style="width:200px;" onclick="openSpInfo('${commodity.id}')" title="${commodity.goods.name}">
												${commodity.goods.name}
											</span>
										</td>
										<td>
											<span style="width:70px;text-align: right;"> ${commodity.price }</span>
										</td>
										<td>
											<span style="width:70px;text-align: left;"> ${commodity.unit }</span>
										</td>
										<td>
											<span style="width:60px;text-align: right;"> ${commodity.goods.price }</span>
										</td>
										<td>
											<span style="width:60px;text-align: right;"> ${commodity.price }</span>
										</td>
										<td>
											<span style="width:50px;text-align: right;"> ${commodity.goods.least }</span>
										</td>
										<td>
											<span style="width:170px;text-align: left;"> ${commodity.store.name }</span>
										</td>
										<td style="display: none;">
											<span style="width:170px;text-align: left;"> ${commodity.store.id }</span>
										</td>
										<td style="display: none;">
											<span style="width:170px;text-align: left;"> ${commodity.seller.id}</span>
										</td>
										<td style="display: none;">
											<span style="width:170px;text-align: left;"> ${commodity.seller.name}</span>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
			<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
				 	$('#listForm').submit();
				 	pageFunReload();
				});
				//重写翻页方法
				function pageFunReload(){
					var pageSize = content.find('#pageSize').val();
					pageAction = function(nowPage){
						nowPage ? loadGrid(nowPage,pageSize) : loadGrid(content.find('#gotoPage').val(),pageSize);
					};
					perPageColSizeAction = function(count){
						var val = content.find("#viewPageColSize").val();
						if(isNaN(val) || val == 0){
							alerterror("请输入有效数字");
							$("#viewPageColSize").val(pageSize);
							return ;
						}else if(val>150){
							alerterror("最大条数不能超过150条");
							$("#viewPageColSize").val(pageSize);
							return ;
						}else{
							loadGrid($('#nowPage').val(),val);
						}
					};
				}
				var tool = $('.tool').toolbar({
					items: [{
							text: 'enter',
							title: 'enter',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {entity:[]};
								checkboxList.filter(':checked').each(function(){
									var entity = {};
									var row = $(this).closest('tr');
									entity.id = $.trim($(this).val());//主键
									entity.sn = $.trim(row.children('td:eq(2)').text());//名称
									entity.goodsname = $.trim(row.children('td:eq(3)').text());//联系电话
									entity.price = $.trim(row.children('td:eq(7)').text());//地区、区域
									entity.storeid = $.trim(row.children('td:eq(10)').text());//地址
									entity.storename = $.trim(row.children('td:eq(9)').text());//地址
									entity.sellerid = $.trim(row.children('td:eq(11)').text());//地址
									entity.sellername = $.trim(row.children('td:eq(12)').text());//地址
									data.entity.push(entity);
								});
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: 'cancel',
							title: 'cancel',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//------------------------------
				//点击checkbox改变
				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				     if ($(this)[0].checked) {
				    	 $(this).attr("checked", false);
				     }else{
				    	 $(this).attr("checked", true);
				     }
				 });
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').bind("click", function () {
				     if ($(this).find(':checkbox')[0].checked) {
				    	 $(this).find(':checkbox').attr("checked", false);
				     }else{
				    	 $(this).find(':checkbox').attr("checked", true);
				     }
				 });
				//---------------------------
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
			});
			function pageReload(){
				$('#listForm').submit();
			} 
		</script>
	</body>
</html>