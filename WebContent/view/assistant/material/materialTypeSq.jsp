<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
	String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>物资类别授权</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/js/boh/omui/themes/apusic/operamasks-ui.min.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<div>
        	<div id="toolbar"></div>
	    	<div class="treePanel" id="treePanel">
	    	</div>
    	</div>
		<div id="wait2" style="visibility: hidden;"></div>
    	<div id="wait" style="visibility: hidden;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="data_dealing_send" />,<fmt:message key="please_wait" />...</span>
		</div>

		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/department.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.js"></script>
		<script type="text/javascript">
		    var storeData=${materialList };
			$(document).ready(function(){
		     	 $("#treePanel").omTree({
		         	 dataSource : storeData,//数据源
		         	 showCheckbox:true,		//<fmt:message key="whether" />带有多选看
		         	 simpleDataModel:true	//数据模型
		      	});
		     	 //将授权的节点选中
		     	var checkTree = $('#treePanel').omTree('findNodes', "checked", 'true', "",true);
		     	if(checkTree){
		     		$.each(checkTree,function(i,leaf){
		     			debugger;
			     		$('#treePanel').omTree('check',leaf);
			     	});
		     	}
		     	//将未选择的节点选择框取消选中
		     	var uncheckTree = $('#treePanel').omTree('findNodes', "checked", 'false', "",true);
		     	if(uncheckTree){
		     		$.each(uncheckTree,function(i,leaf){
		     			debugger;
			     		$('#treePanel').omTree('uncheck',leaf);
			     	});
		     	}
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-80px']
							},
							handler: function(){
								var chkValue=[],len=0; 
						    	var ss = getStoreTree();
						    	$.each(ss,function(key,val){
									chkValue.push(val.id);
									len++;
								});
								parent['${callBack}'](chkValue);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="expandAll" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-80px']
							},
							handler: function(){
								$('#treePanel').omTree('expandAll');
							}
						},{
							text: '<fmt:message key="refresh" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','0px']
							},
							handler: function(){
								window.location.href=window.location.href;
							}
						},{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','0px']
							},
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});//end toolbar
				setElementHeight('.treePanel',['#toolbar'],$(document.body),5);//计算.treePanel的高度
			});
			//获取选择的数据
		    function getStoreTree(){
		    	var checkedNode=$('#treePanel').omTree('getAllChecked',true);
		    	var storeNode = [];
		    	storeNode = $.grep(checkedNode, function(val, key) {
// 		    	    if(val.hasChildren==null || val.hasChildren==""){ //只获取最后一级节点的数据
		    	        return true;
// 		    	    }
		    	}, false);
				return storeNode;
		    }
		</script>

	</body>
</html>