<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.page{
/* 					margin-bottom:26px; */
				}
			.search-div .form-line .form-label{
					width: 10%;
				}
		</style>
	</head>
	<body>
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="<%=path%>/material/tableMaterial.do" method="post">
	    		<div class="search-div" style="positn: absolute;z-index: 99">
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_code" />:</div>
						<div class="form-input">
							<input type="text" id="vcode" name="vcode" value="${material.vcode}" onkeyup="ajaxSearch()" class="text"/>
						</div>
						<div class="form-label"><fmt:message key="supplies_name" />:</div>
						<div class="form-input">
							<input type="text" id="vname" name="vname" value="${material.vname}" onkeyup="ajaxSearch()" class="text"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="Mnemonic" />:</div>
						<div class="form-input">
							<input type="text" id="vinit" name="vinit" style="text-transform:uppercase;" value="${material.vinit}" onkeyup="ajaxSearch()" class="text"/>
						</div>
						<div class="form-label"><fmt:message key="enable_state" />:</div>
						<div class="form-input">
							<div>
								<select class="select" id="enablestate" name="enablestate" style="width:133px;">
									<option value="0"></option>
									<option value="1" <c:if test="${material.enablestate==1}">selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
									<option value="2" <c:if test="${material.enablestate==2}">selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
									<option value="3" <c:if test="${material.enablestate==3}">selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
								</select>
							</div>
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       			<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
					</div>
				</div>
				<div class="grid" class="grid" style="overflow: auto;">
		    		<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
		    		<input type="hidden" id="materialtypevcode" name="materialtypevcode" value="${materialtypevcode}"/>
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td class="num" style="width:31px;"></td>
									<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
									<td style="width:120px;"><fmt:message key="supplies_code" /></td>
									<td style="width:160px;"><fmt:message key="supplies_name" /></td>
									<td style="width:90px;"><fmt:message key="Mnemonic" /></td>
									<td style="width:140px;"><fmt:message key="supplies_category" /></td>
									<td style="width:80px;"><fmt:message key="supplies_specifications" /></td>
									<td style="width:80px;"><fmt:message key="unit" /></td>
									<td style="width:80px;"><fmt:message key="Reference_resources_price" /></td>
									<td style="width:70px;"><fmt:message key="enable_state" /></td>
									<td style="width:120px;"><fmt:message key="supplies_brands" /></td>
									<td style="width:200px;"><fmt:message key="supplies_origin" /></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="material" varStatus="step" items="${materialList}">
									<tr>
										<td class="num"><span style="width:21px;">${step.count}</span></td>
										<td><span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${material.pk_material}" value="${material.pk_material}"/>
										</span></td>
										<td><span title="${material.vcode}" style="width:110px;text-align: left;">${material.vcode}</span></td>
										<td><span title="${material.vname}" style="width:150px;text-align: left;">${material.vname}</span></td>
										<td><span title="${material.vinit}" style="width:80px;text-align: left;">${material.vinit}</span></td>
										<td><span title="${material.materialtype.vname}" style="width:130px;text-align: left;">${material.materialtype.vname}</span></td>
										<td><span title="${material.vspecfication}" style="width:70px;text-align: left;">${material.vspecfication}</span></td>
										<td><span title="${material.unitvname}" style="width:70px;text-align: left;">${material.unitvname}</span></td>
										<td><span title="${material.ninprice}" style="width:70px;text-align: right;">${material.nsaleprice}</span></td>
										<td><span title="${material.enablestate}" style="width:60px;text-align: center;">
											<c:if test="${material.enablestate==1}"><fmt:message key="not_enabled" /></c:if>
											<c:if test="${material.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${material.enablestate==3}"><fmt:message key="stop_enabled" /></c:if>
										</span></td>
										<td><span title="${material.vbrand}" style="width:110px;text-align: left;">${material.vbrand}</span></td>
										<td><span title="${material.vaddr}" style="width:190px;text-align: left;">${material.vaddr}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
			<div class="print-div">
				<form id="printForm" action="<%=path%>/supply/toReport.do" method="post">
				<input type="hidden" name="level" value="${level}"/>
				<input type="hidden" name="code" value="${code}"/>
				<input type="hidden" name="type" value="excel"/>
				<input  type="hidden" name="orderBy" value="<c:out value="${supply.orderBy}" default="sp_code"/>" />
				<input  type="hidden" name="orderDes" value="<c:out value="${supply.orderDes}" default="00000000000000000000000000000000000000000000000000000000000000000000000000000000"/>" />
				<input type="hidden" name ="pageSize" value="${pageobj.pageSize }" />
				<div class="print-condition">
					<table class="search-table" cellspacing="0" cellpadding="0">
						<tr>
							<td class="c-left"><fmt:message key="export_range" />：</td>
							<td><input type="text" id="reportFrom" name="reportFrom"   value="${pageobj.nowPage}" size="4" /></td>
							<td class="c-left"><fmt:message key="to" />：</td>
							<td><input type="text" id="reportTo" name="reportTo"  value="${pageobj.pageCount}" size="4"  /><fmt:message key="page" /></td>
						</tr>
					</table>
				</div>
				<div class="print-commit">
		       		<input type="button" class="print-button" id="print" value='<fmt:message key="export" />'/>
				</div>
				</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$("#wait2",parent.window.document).css("display","block");
					$("#wait",parent.window.document).css("display","block");
		            $("#vcode").val(stripscript($("#vcode").val()));
		            $("#vname").val(stripscript($("#vname").val()));
		            $("#vinit").val(stripscript($("#vinit").val()));
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
// 					clearQueryForm();
// 					$('.search-condition input').val('');
					$('#vname').val('');
					$('#vinit').val('');
					$('#vcode').val('');
					$('#enablestate').val(0);
				});
				/* 导出提交 */
				$("#print").bind('click', function() {
					var reportFrom=$('#reportFrom').val();
					var reportTo=$('#reportTo').val();
					var ex = /^[0-9]*[1-9][0-9]*$/;
					if (ex.test(reportFrom) && ex.test(reportTo)) {
						$('.print-div').hide();
						$('#printForm').submit();
						
					}else{
						alerterror('<fmt:message key="export_range_please_enter_positive_integer" />！');
					}
				});
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
// 				$('.grid').find('.table-body').find('tr').live("click", function () {
// 					$(this).find(':checkbox').trigger('click');
// 				 });
// 				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
// 						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
// 						$(this).attr('checked','checked');
// 					event.stopPropagation();
// 				});
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
							$("#vcode").focus();
						}
					},{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							if ($("#pk_materialtype").val() != "") {
								if(checkMaterialType()){
									savematerial();
								}
							} else {
								alerterror('<fmt:message key="First_select_a_final_category" />!');
							}
						}
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updatematerial();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deleteSupply();
						}
					},{
						text: '<fmt:message key="suppliership" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'view')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							mallMaterial();
						}
					},{
						text: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							var form = $("#queryForm");
							var oldAction = form.attr("action");
							form.attr('target','report');
							window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
							var action="<%=path%>/material/printMaterial.do";
							form.attr('action',action);
							form.submit();
							form.removeAttr("target");
							form.attr('action',oldAction);
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				$("#wait2",parent.window.document).css("display","none");
				$("#wait",parent.window.document).css("display","none");
			});
			//判断当前选中的分类是不是最小类别
			function checkMaterialType(){
				var pk_materialtype = $('#pk_materialtype').val();
				var flag = false;
				$.ajax({
					type: "POST",
					url: "<%=path%>/material/checkMaterialType.do",
					data: "pk_materialtype="+pk_materialtype,
					async:false,
					dataType: "json",
					success:function(result){
						if(result > 0){
							alerterror('<fmt:message key="First_select_a_final_category" />');
							flag = false;
						} else {
							flag = true;
						}
					}
				});
				return flag;
			}
			//新增物资supplies
			function savematerial(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="supplies" />',
					content: '<iframe id="saveMaterial" frameborder="0" src="<%=path%>/material/toAddMaterial.do?pk_materialtype=${pk_materialtype}&materialtypevcode=${materialtypevcode}"></iframe>',
					width: '650px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveMaterial') && window.document.getElementById("saveMaterial").contentWindow.saveCheck()){
										window.document.getElementById("saveMaterial").contentWindow.checkvcoderole();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改物资信息
			function updatematerial(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="supplies" />',
						content: '<iframe id="updateMaterial" frameborder="0" src="<%=path%>/material/toUpdateMaterial.do?pk_material='+chkValue+'"></iframe>',
						width: '650px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateMaterial')&&window.document.getElementById("updateMaterial").contentWindow.saveCheck()){
											window.document.getElementById("updateMaterial").contentWindow.getUnitValues();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除物资
			function deleteSupply(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var vcodes = chkValue.join(",");
						if(check(vcodes)){
							$.ajaxSetup({async:false});
							$.post('<%=path%>/material/deleteMaterial.do',{"vcodes":vcodes},function(data){
								var rs = data;
								switch(Number(rs)){
								case -1:
									alerterror('<fmt:message key="delete_fail"/>！');
									break;
								case 1:
									showMessage({
										type: 'success',
										msg: '<fmt:message key="successful_deleted"/>！',
										speed: 3000,
										handler:function(){
												reloadPage();
// 												parent.refreshTree($("#pk_materialtype").val());
											}
										});
									break;
								}
							});	
						}else{
							alerterror('<fmt:message key="supplies" /><fmt:message key="refencenotdisable" />！');
							return ;
						}
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};		
				
			}
			
			//关联商城物资
			function mallMaterial(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '商城供应商',
						content: '<iframe id="mallMaterial" frameborder="0" src="<%=path%>/material/toMallMaterial.do?pk_material='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key ="quit" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_data" />！');
					return ;
				}
			}
			//椒盐物资是否被引用
			function check(vcodes){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/material/checkMaterials.do",{vcodes:vcodes},function(data){
					if(!data)result = false;
				});
				return result;
			}
			//------------------------------
			$('.grid .table-body tr').live('click',function(){
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
// 					 var $tmp=$('[name=idList]:checkbox');
// 					 $('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			//倒入
			function importSupply(){
		    	$('body').window({
					id: 'window_importSupply',
					title: '<fmt:message key="import" /><fmt:message key="supplies_information" />',
					content: '<iframe id="importSupply" frameborder="0" src="<%=path%>/material/importMaterial.do"></iframe>',
					width: '400px',
					height: '200px',
					draggable: true,
					isModal: true
				});
		    }
			function reloadPage(){
				$('#queryForm').submit();
			}
		    //导出物资
		    function exportSupply(){
		    	$("#wait2").val('NO');//不用等待加载
		    	$("#queryForm").attr("action","<%=path%>/material/exportExcel.do");
		    	$("#queryForm").submit();
				$("#wait2 span").html('<fmt:message key="data_import" /><fmt:message key="please_wait" />...');
				$('#queryForm').attr("action","<%=path%>/material/tableMaterial.do");	
				$("#wait2").val('');//等待加载还原
		    }
		    document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
		</script>
	</body>
</html>