<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
		<div class="form">
			<form id="updateMaterialType" method="post" action="<%=path %>/materialType/updateMaterialType.do">
	    	<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${materialType.pk_materialtype}"/>
	    	<input type="hidden" id="pk_father" name="pk_father" value="${materialType.pk_father}"/>
	    	<input type="hidden" id="lvl" name="lvl" value="${lvl}"/>
			<div style="height:200px;width:400px;z-index:88px;margin:50px auto;">
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="category" /><fmt:message key="coding" />:</div>
					<div class="form-input"><input type="text" id="vcode" name="vcode" class="text" readonly="readonly" value="${materialType.vcode}"/></div>
				</div>
				<div class="form-line">
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="category" /><fmt:message key="name" />:</div>
					<div class="form-input"><input type="text" id="vname" name="vname" class="text" value="${materialType.vname}"/></div>
				</div>
				<div class="form-line">
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" name="enablestate"  class="select" style="width:133px;">
							<option value="1"  <c:if test="${materialType.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
							<option value="2"  <c:if test="${materialType.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
							<option value="3"  <c:if test="${materialType.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div>
			</div>
		</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
		<script type="text/javascript">
			$("#vname").focus();
			function checkvcode(){
				var data = {};
				data["pk_materialtype"] = $.trim($("#pk_materialtype").val());
				data["vcode"] = $.trim($("#vcode").val());
				data["vname"] = $.trim($("#vname").val());
				data["enablestate"] = $("#enablestate").val();
				if(check()){
						$.ajaxSetup({async:false});
						$.post("<%=path%>/materialType/updateMaterialType.do",data,function(data){
							var rs = data;
							switch(Number(rs)){
							case -1:
								alerterror('<fmt:message key="save_fail"/>！');
								break;
							case 1:
								showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.parent.refreshTree($("#pk_father").val(),null);
										parent.$('.close').click();}
									});
								break;
							}
						});	
				}
			}
			//验证编码规则
// 			function checkvcode(){
// 		        codeval({
<%-- 		            action:"<%=path%>", --%>
// 		            callback:"submitType"
// 		        },
// 		        "00000000000000000000000000000012",
// 		        $("#vcode").val(),
// 		        $("#pk_materialtype").val(),
// 		        $("#lvl").val())
// 		    }
			//校验编码、名称是否为空，当前类别是否已经被引用
			function check(){
				var vcode = stripscript($.trim($('#vcode').val()));
				var vname = stripscript($.trim($('#vname').val()));
				var pk_materialtype = $.trim($('#pk_materialtype').val());
				if(/[?:"{},\/;'[\]]/im.test(vcode)){
					alerterror('<fmt:message key="coding" /><fmt:message key="haven_Special_Character" />!');
					return false;
				}
				if(!checktype(pk_materialtype)){
					alerterror('<fmt:message key="supplies_category" /><fmt:message key="heaven_used" />，<fmt:message key="dont_update" />！');
					return false;
				}
				if(vcode == null || vcode == ''){
					alerterror('<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				if(/[?:"{},\/;'[\]]/im.test(vname)){
					alerterror('<fmt:message key="name" /><fmt:message key="haven_Special_Character" />!');
					return false;
				}
				if(vname == null || vname == ''){
					alerterror('<fmt:message key="coding" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				if($('#enablestate').val() == 3 && !checktype(pk_materialtype)){
					alerterror('<fmt:message key="supplies_category" /><fmt:message key="heaven_used" />，<fmt:message key="dont_update" />！');
					return false;
				}
				if(!checktypevcode(vcode,pk_materialtype)){
					alerterror('<fmt:message key="coding" /><fmt:message key="already_exists" />！');
					return false;
				}
				if(!checktypename(vname)){
					alerterror('<fmt:message key="name" /><fmt:message key="already_exists" />！');
					return false;
				}
				return true;
			}
			//校验编码是否被引用
			function checktype(pk_materialtype){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/materialType/findMaterialTypeBypk.do",{pk_materialtype:$("#pk_materialtype").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			//校验编码、名称是否重复
			function checktypevcode(vcode,pk_materialtype){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/materialType/ajaxIsSameCode.do",{vcode:$("#vcode").val(),pk_materialtype:$("#pk_materialtype").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			function checktypename(vname){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/materialType/ajaxIsSameName.do",{vname:$("#vname").val(),pk_materialtype:$("#pk_materialtype").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			var inputarray = $(".form-input").children().not("input[type='hidden']").not("div");
            inputarray.bind('keyup',function(){
                if(!event.ctrlKey&&event.keyCode==13){
                    var index = inputarray.index($(this)[0]);
                    if(index!=inputarray.length-1){
                        $(inputarray[index+1]).focus();
                    }
                }
            });

		</script>
	</body>
</html>