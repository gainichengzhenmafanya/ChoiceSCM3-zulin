<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加应标供应商</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
	</head>
	<body>
	<div class="form"><div>
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:120px;margin-top:60px;">
			<form id="addCgTenderDForm" method="post" action="<%=path %>/cgtender/saveCgtenderDByAdd.do">
			<input type="hidden" value="${cgTenderd.pk_cgtender}" name="pk_cgtender" id="pk_cgtender"/>
				<div class="form-line">
					<div class="form-label">供应商名称：</div>
					<div class="form-input">
						<input type="text" id="delivername" name="delivername"/>
						<input type="hidden" id="delivercode" name="delivercode"/>
						<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">联系人：</div>
					<div class="form-input">
						<input type="text" id="vcontract" name="vcontract" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">联系电话：</div>
					<div class="form-input">
						<input type="text" id="vphone" name="vphone" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">地址：</div>
					<div class="form-input">
						<input type="text" id="vaddr" name="vaddr" class="text" autocomplete="off"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">经营范围：</div>
					<div class="form-input">
						<input type="text" id="varear" name="varear" class="text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">所属区域：</div>
					<div class="form-input">
						<select name="vregion" class="select" id="vregion" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${area}">
								<option value="${cur.des}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">配送范围：</div>
					<div class="form-input">
						<select name="vdeliveryarea" class="select" id="vdeliveryarea" style="width:134px;">
							<option value=""></option>
							<c:forEach var="cur" items="${psarea}">
								<option value="${cur.des}"><c:out value="${cur.des}"/></option>
							</c:forEach>
						</select>
					</div>
				</div>
			</form>
			</div>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript">
		var t;
		function ajaxSearch(key){
			return;
			if (event.keyCode == 13 ||event.keyCode == 38 ||event.keyCode == 40){
				return; //回车 ，上下 时不执行
			}
			   window.clearTimeout(t); 
			   t=window.setTimeout("ajaxSupply(\'"+key+"\',\'<%=path%>\')",200);//延迟0.2秒
		}
		
		function pageReload(){
			$('#listForm').submit();
		}
		function clearQueryForm(){
			$('#listForm select option').removeAttr("selected");
			$('#listForm select option[value=""]').attr("selected","selected");
		}
		//弹出物资树回调函数
		function handler(delivercode){
			$('#delivercode').val(delivercode); 
			if(delivercode==undefined || ''==delivercode){
				 $('#delivername').val('');
				 $('#vcontract').val('');
				 $('#vphone').val('');
				 $('#vaddr').val('');
				return;
			}
			$.ajax({
				type: "POST",
				url: "<%=path%>/deliver/findDeliverByCode.do",
				data: "code="+delivercode,
				dataType: "json",
				success:function(deliver){
					
// 					$('#delivercode').val(deliver.code);
					$('#delivername').val(deliver.des);
					$('#vcontract').val(deliver.person1);
					$('#vphone').val(deliver.tel1);
					$('#vaddr').val(deliver.addr);
					
				}
			});
			$('#delivername').attr("readonly","readonly");
			$('#vcontract').attr("readonly","readonly");
			$('#vphone').attr("readonly","readonly");
			$('#vaddr').attr("readonly","readonly");
			$('#varear').attr("readonly","readonly");
			$('#vregion').attr("readonly","readonly");
			$('#vdeliveryarea').attr("readonly","readonly");
		}
		$(document).ready(function(){
			$('#seachDeliver').bind('click.custom',function(e){
					var defaultCode = $('#delivercode').val();
					var defaultName = $('#delivername').val();
					var offset = getOffset('delivername');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#delivername'),$('#delivercode'),'900','500','isNull',handler);
			});
			/*验证*/
			validate = new Validate({
				validateItem:[{
					type:'text',
					validateObj:'delivername',
					validateType:['canNull'],
					param:['F'],
					error:['<fmt:message key="suppliers" /><fmt:message key="cannot_be_empty" />！']
				}]
			});
		});
		</script>
	</body>
</html>