<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
				.userInfo,.accountInfo {
					positn: relative;
					top: 1px;
					background-color: #E1E1E1;
				}
				
				.userInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo {
					height: 91px;
					line-height: 91px;
				}
				
				.accountInfo .form-label{
					width: 40%;
				}
				.bgDiv{
					z-index: 901;
				}
			</style>
			<script type="text/javascript">
				var path = "<%=path%>";
			</script>
	</head>
	<body>
	<div class="tool"></div>
		<div class="form" style="overflow: auto;width: 98.5%;height: 98%;" align="center">
				<input type="hidden" id="pk_cgtender" name="pk_cgtender" class="text" value="${cgtender.pk_cgtender}"/>
				<div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label"><span class="red">*</span>招标书名称:</div>
						<div class="form-input">
							<input type="text" id="vcgtendername" name="vcgtendername" class="text" maxlength="50" value="${cgtender.vcgtendername}"/>
						</div>
						<div class="form-label"><span class="red">*</span>招标类型:</div>
						<div class="form-input">
							<select class="select" id="pk_tender" name="pk_tender" style="width:133px">
								<option></option>
									<c:forEach var="tender" items="${listTender}" varStatus="status" >
										<option 
										<c:if test="${tender.pk_tender == cgtender.pk_tender}"> selected="selected" </c:if>
										id="${tender.pk_tender}" value="${tender.pk_tender}">${tender.vname}</option>
									</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div>
					<div class="form-label" style="width: 80%;margin-top: 10px;">招标书内容</div>
					<div class="form-input" style="margin-top: 10px;">
						<textarea wrap="physical" id="content"  name="content" style="width: 90%;height: 180px;text-align: left;">${cgtender.vtendermemo}</textarea>
					</div>
				</div>
				<div style="margin-top: 200px;">
<!-- 					<div class="form-line"> -->
<!-- 						<div class="form-label">上传附件:</div> -->
<!-- 						<div class="form-input"> -->
<!-- 							<input type="button" style="width: 80px;height: 25px;" value="选择文件"/><span style="color: gray;margin-left: 10px;">最多上传10个，总大小不超过50M，支持PDF，EXCEL，WORD，图片格式</span> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<hr />
					<h5>商务条款</h5>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">付款方式:</div>
						<div class="form-input">
							<select class="select" id="pk_paymethod" name="pk_paymethod" style="width:133px">
								<option></option>
									<c:forEach var="paymethod" items="${listPayMethod}" varStatus="status" >
										<option 
										<c:if test="${paymethod.pk_paymethod == cgtender.pk_paymethod}"> selected="selected" </c:if>
										id="${paymethod.pk_paymethod}" value="${paymethod.pk_paymethod}">${paymethod.vname}</option>
									</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">发票类型:</div>
						<div class="form-input">
							<input type="radio" id="bxyfp" name="radio" <c:if test="${cgtender.vnvoicetype == 1}">checked="checked"</c:if>/>不需要发票
							<input type="radio" id="zzfp" name="radio" <c:if test="${cgtender.vnvoicetype == 2}">checked="checked"</c:if>/>增值发票
							<input type="radio" id="ptfp" name="radio" <c:if test="${cgtender.vnvoicetype == 3}">checked="checked"</c:if>/>普通发票
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">交货时间:</div>
						<div class="form-input">
							<span>下单后<span class="red">*</span><input type="text" id="ndelivery" name="ndelivery" value="${cgtender.ndelivery}"/>天内交货到制定地点</span>
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">收货地址:</div>
						<div class="form-input">
							<select name="country" id="country" onChange="goprovince('','','');"> </select>
							<select id="province" name="province" onChange="gocity('','');"></select>
							<select id="city" name="city"></select>
							<input type="text" id="xxdzaddr" name="xxdzaddr" maxlength="50" value="${cgtender.xxdzaddr}"/>
						</div>
					</div>
					<hr />
					<h5>投标要求</h5>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">报名截止时间:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="dregistrationedat" name="dregistrationedat" style="text-transform:uppercase;" class="Wdate text" value="${cgtender.dregistrationedat}"/>
						</div>
						<div class="form-label">投标截止时间:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="dbidedat" name="dbidedat" style="text-transform:uppercase;" class="Wdate text" value="${cgtender.dbidedat}"/>
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">注册资金:</div>
						<div class="form-input">
							<span><input type="text" id="nregistermoney" name="nregistermoney" value="${cgtender.nregistermoney}"/>万元以上</span>
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">证件要求:</div>
						<div class="form-input">
							<input type="checkbox" id="vyyzz" name="vyyzz" <c:if test="${cgtender.vyyzz == '1'}">checked="checked"</c:if>/>营业执照
							<input type="checkbox" id="vswdjz" name="vswdjz" <c:if test="${cgtender.vswdjz == '1'}">checked="checked"</c:if>/>税务登记证
							<input type="checkbox" id="vzzjgdm" name="vzzjgdm" <c:if test="${cgtender.vzzjgdm == '1'}">checked="checked"</c:if>/>组织机构代码
							<input type="checkbox" id="vhjgltxrzs" name="vhjgltxrzs" <c:if test="${cgtender.vhjgltxrzs == '1'}">checked="checked"</c:if>/>环境管理体系认证书
							<input type="checkbox" id="vqyzlgl" name="vqyzlgl" <c:if test="${cgtender.vqyzlgl == '1'}">checked="checked"</c:if>/>企业质量管理体系认证书
						</div>
					</div>
					<hr />
					<h5>联系方式</h5>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label"><span class="red">*</span>联系人:</div>
						<div class="form-input">
							<input style="width: 200px;" type="text" id="vcontact" name="vcontact" maxlength="20" value="${cgtender.vcontact}"/>
						</div>
						<div class="form-label" style="margin-left: 20px;">手机:</div>
						<div class="form-input">
							<input style="width: 200px;" type="text" id="vtelephone" name="vtelephone" maxlength="20" value="${cgtender.vtelephone}"/>
						</div>
					</div>
					<div class="form-line" style="margin-top: 10px;">
						<div class="form-label">固定电话:</div>
						<div class="form-input">
							<input style="width: 200px;" type="text" id="vmobilphone" name="vmobilphone" maxlength="20" value="${cgtender.vmobilphone}"/>
						</div>
						<div class="form-label" style="margin-left: 20px;">电子邮箱:</div>
						<div class="form-input">
							<input style="width: 200px;" type="text" id="vemial" name="vemial" maxlength="20" value="${cgtender.vemial}"/>
						</div>
					</div>
					<hr />
				</div>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_config.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/dialogs/image/image.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/selectcity/loadprovince.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/selectcity/province.js"></script>
		<script type="text/javascript">
			var editor;
			$(document).ready(function(){
		        $("#vcgtendername").focus();
				editor = UE.getEditor('content');
				//限制文本框的字符长度（汉字算两个字符）
				$("#content").bind('blur keyup',function(){
		 	 		$(this).limitLength(2000);
		 	 		var startStr = $("#content").val().replace(/[^\x00-\xff]/g, "**"); 
					var curLength = startStr.length; 
					$("#count").html(curLength+'/2000');
		 		});
				gocountry("country");
				SelectList("country","1");
				goprovince("country","province","city");
				$('#country').find("option[value='${cgtender.country}']").attr("selected",true);
				$('#province').find("option[value='${cgtender.province}']").attr("selected",true);
				gocity('province','city');
				$('#city').find("option[value='${cgtender.city}']").attr("selected",true);
				
				$("#dregistrationedat").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dbidedat\')}'});
				});
				$("#dbidedat").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dregistrationedat\')}'});
				});
				

				$(".form").height($(document.body).height()-65);
				
				var tool = $('.tool').toolbar({
					items: [{
						text: '<fmt:message key ="save" />',
						title: '<fmt:message key ="save" />',
						useable: true,
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							savetender();
						}
					},{
						text: '<fmt:message key ="cancel" />',
						title: '<fmt:message key ="cancel" />',
						icon: {
							url: '/Choice/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							closeFrame();
						}
					}]
				})
			});
			function selected(){
				if($('#isnotagree').attr("checked")=="checked"){
					$('#savebtn').removeAttr("disabled");
				}else{
					$('#savebtn').attr("disabled","disabled");
				}
			}
			//将编辑框内容同步内容到content容器
			function getText(){
				var result = true;
				editor.sync(); 
				$('#content').val(editor.getContent());
				if(!$("#content").val()){
					result = false;
					alerterror("<fmt:message key="content"/><fmt:message key="cannot_be_empty"/>！");
				}else if(editor.getPlainTxt().replace(/[^\x00-\xff]/g, "**").length>2000){
					result = false;
					alerterror("<fmt:message key="content"/><fmt:message key="length_too_long"/>！");
				}
				return result;
			}
			//保存检查
			function check(){
				var vcgtendername = $("#vcgtendername").val();
				if(vcgtendername == "" || vcgtendername == null){
					alerterror('招标书名称不能为空！');
					return false;
				} else if(vcgtendername.lengh > 25){
					alerterror('招标书名称名称过长，不可超过25！');
					return false;
				} else if(/[?:"{},\/;'[\]]/im.test(vcgtendername)){
					alerterror('招标书名称不能含有特殊字符!');
					return false;
				}
				var pk_tender = $('#pk_tender').val();
				if(pk_tender == null || pk_tender == ''){
					alerterror('招标类型不能为空！');
					return false;
				}
				var ndelivery = $('#ndelivery').val();
				if(ndelivery == null || ndelivery == ''){
					alerterror('交货时间不能为空！');
					return false;
				}else if(!ndelivery.match("^[0-9]*[1-9][0-9]*$")){
					alerterror('交货时间必须为正整数！');
					return false;
				}
				var nregistermoney = $('#nregistermoney').val();
				if(isNaN(nregistermoney)){
					alerterror('注册资金必须为数字！');
					return false;
				}else if(!nregistermoney.match("^([+]?)\\d*\\.?\\d+$")){
					alerterror('注册资金必须为数字！');
					return false;
				}
				var vcontact = $('#vcontact').val();
				if(vcontact == null || vcontact == ''){
					alerterror('联系人不能为空！');
					return false;
				}else if(vcontact == null || vcontact == ''){
					alerterror('联系人不能为空！');
					return false;
				}
				var vemial = $.trim($('#vemial').val());
				if(!vemial.match("^([\\w-.]+)@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([\\w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$")){
					alerterror('邮箱格式不正确！');
					return false;
				}
				var vtelephone = $.trim($('#vtelephone').val());
				if(vtelephone != null && vtelephone != ''){
					if(!vtelephone.match("^(13|14|15|17|18)[0-9]{9}$")){
						alerterror('手机号码格式不正确！');
						return false;
					}
				}
				var vmobilphone = $.trim($('#vmobilphone').val());
				if(vmobilphone != null && vmobilphone != ''){
					if(!/^((\+?86)|(\(\+86\)))?\d{3,4}-\d{7,8}(-\d{3,4})?$/.exec(vmobilphone)){
						alerterror('固定电话格式不正确！');
						return false;
					}
				}
				return true;
			}
			//发票
			function getvnvoicetype(){
				if($('input:radio[id="bxyfp"]:checked').val() == 'on'){
					return 1;
				}else if($('input:radio[id="zzfp"]:checked').val() == 'on'){
					return 2;
				}else if($('input:radio[id="ptfp"]:checked').val() == 'on'){
					return 3;
				}else{
					return 1;
				}
			}
			//*********保存招标内容START**********************************
			var savelock = false;
			function savetender(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var data = {};
				data["pk_cgtender"] = $("#pk_cgtender").val();
				data["vcgtendername"] = $("#vcgtendername").val();
				data["pk_tender"] = $("#pk_tender").val();
				if(getText()){
					data["vtendermemo"] = editor.getContent();
				}else{
					savelock = false;
					return;
				}
				data["pk_paymethod"] = $("#pk_paymethod").val();
				data["ndelivery"] = $("#ndelivery").val();
				data["country"] = $('#country').find("option:selected").val();
				data["province"] = $('#province').find("option:selected").val();
				data["city"] = $('#city').find("option:selected").val();
				data["xxdzaddr"] = $('#xxdzaddr').val();
				data['vtotaladdr']= $('#country').find("option:selected").text()+$('#province').find("option:selected").text()+$('#city').find("option:selected").text()+$('#xxdzaddr').val();
				data["vnvoicetype"] = getvnvoicetype();
				data["dregistrationedat"] = $("#dregistrationedat").val();
				data["dbidedat"] = $("#dbidedat").val();
				data["nregistermoney"] = $("#nregistermoney").val();
				if($('#vyyzz').attr("checked")=="checked"){
					data["vyyzz"] = 1;
				}else{
					data["vyyzz"] = 0;
				}
				if($('#vswdjz').attr("checked")=="checked"){
					data["vswdjz"] = 1;
				}else{
					data["vswdjz"] = 0;
				}
				if($('#vzzjgdm').attr("checked")=="checked"){
					data["vzzjgdm"] = 1;
				}else{
					data["vzzjgdm"] = 0;
				}
				if($('#vhjgltxrzs').attr("checked")=="checked"){
					data["vhjgltxrzs"] = 1;
				}else{
					data["vhjgltxrzs"] = 0;
				}
				if($('#vqyzlgl').attr("checked")=="checked"){
					data["vqyzlgl"] = 1;
				}else{
					data["vqyzlgl"] = 0;
				}
				data["vcontact"] = $("#vcontact").val();
				data["vtelephone"] = $("#vtelephone").val();
				data["vmobilphone"] = $("#vmobilphone").val();
				data["vemial"] = $("#vemial").val();
				if(check()){
					$.ajaxSetup({async:false});
					$.post("<%=path%>/cgtender/updateCgTender.do",data,function(data){
						var rs = data;
						switch(Number(rs)){
						case -1:
							alerterror('<fmt:message key="save_fail"/>！');
							break;
						case 1:
							alerttipsbreak('<fmt:message key="save_successful"/>！',function(){
									parent.reloadPage();
									parent.$('.close').click();
								
							});
// 							showMessage({
// 										type: 'success',
// 										msg: '<fmt:message key="save_successful"/>！',
// 										speed: 3000,
// 										handler:function(){
// 											parent.reloadPage();
// 											parent.$('.close').click();
// 											}
// 										});
							break;
						}
					});	
				}else{
					savelock = false;
				}
			}
			//*********END********************************************
			//关闭当前窗口
			function closeFrame(){
				parent.$('.close').click();
			}
		</script>
	</body>
</html>