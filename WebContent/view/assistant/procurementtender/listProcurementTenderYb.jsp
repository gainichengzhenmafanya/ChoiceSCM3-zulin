<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.page{
/* 					margin-bottom:20px; */
				}
			.search-div .form-line .form-label{
					width: 10%;
				}
		</style>
	</head>
	<body>
	<div id="wait2" style="display:block;"></div>
	<div id="wait" style="display:block;"></div>
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="" method="post">
				<div class="grid" class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td ><span style="width:26px;">&nbsp;</span></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:100px;">编码</span></td>
									<td><span style="width:160px;">名称</span></td>
									<td><span style="width:70px;">联系人</span></td>
									<td><span style="width:80px;">联系电话</span></td>
									<td><span style="width:160px;">经营范围</span></td>
									<td><span style="width:160px;">所属区域</span></td>
									<td><span style="width:180px;">配送范围</span></td>
									<td><span style="width:280px;">地址</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="cgTenderd" varStatus="step" items="${listCgTenderd}">
									<tr>
										<td class="num"><span style="width:25px;">${step.count}</span></td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${cgTenderd.pk_cgtenderd}" value="${cgTenderd.pk_cgtenderd}"/>
										</span></td>
										<td><span title="${cgTenderd.delivercode}" style="width:100px;text-align: left;">${cgTenderd.delivercode}</span></td>
										<td><span title="${cgTenderd.delivername}" style="width:160px;text-align: left;">${cgTenderd.delivername}</span></td>
										<td><span title="${cgTenderd.vcontract}" style="width:70px;text-align: left;">${cgTenderd.vcontract}</span></td>
										<td><span title="${cgTenderd.vphone}" style="width:80px;text-align: left;">${cgTenderd.vphone}</span></td>
										<td><span title="${cgTenderd.varear}" style="width:160px;text-align: left;">${cgTenderd.varear}</span></td>
										<td><span title="${cgTenderd.vregion}" style="width:160px;text-align: left;">${cgTenderd.vregion}</span></td>
										<td><span title="${cgTenderd.vdeliveryarea}" style="width:180px;text-align: left;">${cgTenderd.vdeliveryarea}</span></td>
										<td><span title="${cgTenderd.vaddr}" style="width:280px;text-align: left;">${cgTenderd.vaddr}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				/* 模糊查询提交 */
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updateCgTenderD();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deleteCgTenderD();
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							parent.$(".close").click();;	
							
						}
					}]
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			
			
			$('.grid .table-body tr').live('click',function(){
					 $('#pk_puprorderm').val($(this).find('td:eq(0)').find('span').attr('title'));
					 var pk_puprorderm=$(this).find('td:eq(0)').find('span').attr('title');
					 var istate = $(this).find('td:eq(6)').find('span').attr('title');
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			
			//修改应标信息
			function updateCgTenderD(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" />采购招标',
						content: '<iframe id="updateCgTenderFrame" frameborder="0" src="<%=path%>/cgtender/toUpdateCgTenderD.do?pk_cgtenderd='+chkValue+'"></iframe>',
						width: '750px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />应标供应商',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateCgTenderFrame') && getFrame('updateCgTenderFrame').validate._submitValidate()){
											submitFrameForm('updateCgTenderFrame','updateCgTenderDForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			
			//删除应标信息
			function deleteCgTenderD(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var vcodes = chkValue.join(",");
						$.ajaxSetup({async:false});
						$.post('<%=path%>/cgtender/deleteCgTenderD.do',{"vcodes":vcodes},function(data){
							var rs = data;
							switch(Number(rs)){
							case -1:
								alerterror('<fmt:message key="delete_fail"/>！');
								break;
							case 1:
								showMessage({
									type: 'success',
									msg: '<fmt:message key="successful_deleted"/>！',
									speed: 3000,
									handler:function(){
										reloadPage();}
									});
								break;
							}
						});	
					})
				}else{
					alerttips('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};		
				
			}
			function reloadPage(){
				$('#queryForm').submit();
			}
		</script>
	</body>
</html>