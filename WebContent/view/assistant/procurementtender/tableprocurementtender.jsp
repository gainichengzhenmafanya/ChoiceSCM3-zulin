<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.page{
/* 					margin-bottom:20px; */
				}
			.search-div .form-line .form-label{
					width: 10%;
				}
		</style>
	</head>
	<body>
	<div id="wait2" style="display:block;"></div>
	<div id="wait" style="display:block;"></div>
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="<%=path%>/cgtender/queryAllCgTender.do" method="post">
	    		<div class="search-div" style="positn: absolute;z-index: 99">
					<div class="form-line">
						<div class="form-label">招标书名称:</div>
						<div class="form-input">
							<input type="text" id="vcgtendername" name="vcgtendername" value="${cgtender.vcgtendername}" onkeyup="ajaxSearch()" class="text"/>
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       			<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
					</div>
				</div>
				<div class="grid" class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td style="width:31px;"></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:120px;">招标书名称</span></td>
									<td><span style="width:160px;">招标类型</span></td>
<!-- 									<td><span style="width:80px;">招标书状态</span></td> -->
									<td><span style="width:160px;">创建日期</span></td>
									<td><span style="width:160px;">发布日期</span></td>
									<td><span style="width:160px;">停止日期</span></td>
									<td><span style="width:80px;">应标供应商</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="cgtender" varStatus="step" items="${listCgTender}">
									<tr>
										<td class="num"><span title="" style="width:21px;">${step.count}</span></td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${cgtender.pk_cgtender}" value="${cgtender.pk_cgtender}"/>
										</span></td>
										<td><span title="${cgtender.vcgtendername}" style="width:120px;text-align: left;">${cgtender.vcgtendername}</span></td>
										<td><span title="${cgtender.vtendername}" style="width:160px;text-align: left;">${cgtender.vtendername}</span></td>
<%-- 										<td><span title="${cgtender.isstat}" style="width:80px;text-align: center;"> --%>
<%-- 											<c:if test="${cgtender.isstat==1}">未发布</c:if> --%>
<%-- 											<c:if test="${cgtender.isstat==2}">已发布</c:if> --%>
<%-- 											<c:if test="${cgtender.isstat==3}">已终止</c:if> --%>
<%-- 											<c:if test="${cgtender.isstat==4}">已删除</c:if> --%>
<!-- 										</span></td> -->
										<td><span title="${cgtender.vcreatetime}" style="width:160px;text-align: center;">${cgtender.vcreatetime}</span></td>
										<td><span title="${cgtender.vreleasetime}" style="width:160px;text-align: center;">${cgtender.vreleasetime}</span></td>
										<td><span title="${cgtender.vstoptime}" style="width:160px;text-align: center;">${cgtender.vstoptime}</span></td>
										<td><span style="width:80px;text-align: left;color: blue;text-align: center;" onclick="showdetail('${cgtender.pk_cgtender}')">详情</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		
		<script type="text/javascript">
			function setSupplier(data){
				$("#delivercode").val(data.entity[0].delivercode);//主键
				$("#delivercode").val(data.entity[0].vcode);//编码
				$("#delivername").val(data.entity[0].vname);//名称
			}
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setSupplier',
					domId:'delivercode',
					single:false
				});
			});
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$("#vcgtendername").val(stripscript($("#vcgtendername").val()));
					$('#queryForm').submit();
				} 
			}
			var trrows = $(".table-body").find('tr');
			if(trrows.length!=0){
				$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$("#vcgtendername").val(stripscript($("#vcgtendername").val()));
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('#vcgtendername').val('');
				});
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
								$("#vcgtendername").focus();
						}
					},{
						text: '<fmt:message key="view" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							viewCgTender();
						}
					},{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							savevCgTender();
						}
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updateCgTender();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deleteCgTender();
						}
// 					},{
// 	                    text: '发布',
// 	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release')},
// 	                    icon: {
<%-- 	                        url: '<%=path%>/image/Button/op_owner.gif', --%>
// 	                        positn: ['0px','-40px']
// 	                    },
// 	                    handler: function(){
// 	                    	releaseCgTender();
// 	                    }
	                },{
	                    text: '应标',
	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release')},
	                    icon: {
	                        url: '<%=path%>/image/Button/op_owner.gif',
	                        positn: ['0px','-40px']
	                    },
	                    handler: function(){
	                    	addCgTenderD();
	                    }
	                },{
// 	                    text: '终止',
// 	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'stop')},
// 	                    icon: {
<%-- 	                        url: '<%=path%>/image/Button/op_owner.gif', --%>
// 	                        positn: ['0px','-40px']
// 	                    },
// 	                    handler: function(){
// 	                    	stopCgTender();
// 	                    }
// 	                },{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			function savevCgTender(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" />采购招标',
					content: '<iframe id="saveCgTenderFrame" frameborder="0" src="<%=path%>/cgtender/toSaveCgTender.do"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true
				});
			}
			
			//修改物资信息
			function updateCgTender(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
// 					var stat;
// 					checkboxList.filter(':checked').each(function(){
// 						stat = $.trim($(this).closest('tr').find('td').eq(4).find('span').attr('title'));
// 					});
// 					if(stat != 1){
// 						alerterror('当前招标书已经被处理，不能修改！');
// 						return;
// 					}
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" />采购招标',
						content: '<iframe id="updateCgTenderFrame" frameborder="0" src="<%=path%>/cgtender/toUpdateCgTender.do?pk_cgtender='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			
			//删除物资
			function deleteCgTender(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
// 						var stat;
// 						checkboxList.filter(':checked').each(function(){
// 							stat = $.trim($(this).closest('tr').find('td').eq(4).find('span').attr('title'));
// 							chkValue.push($(this).val());
// 						});
// 						if(stat != 1){
// 							alerterror('有已经被处理的招标书，不能删除！');
// 							return;
// 						}
						var vcodes = chkValue.join(",");
						$.ajaxSetup({async:false});
						$.post('<%=path%>/cgtender/deleteCgTender.do',{"vcodes":vcodes},function(data){
							var rs = data;
							switch(Number(rs)){
							case -1:
								alerterror('<fmt:message key="delete_fail"/>！');
								break;
							case 1:
								showMessage({
									type: 'success',
									msg: '<fmt:message key="successful_deleted"/>！',
									speed: 3000,
									handler:function(){
										reloadPage();}
									});
								break;
							}
						});	
					})
				}else{
					alerttips('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};		
			}
			
			//新增应标信息
			function addCgTenderD(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '添加应标供应商',
						content: '<iframe id="addCgTenderDFrame" name="addCgTenderDFrame" frameborder="0" src="<%=path%>/cgtender/toAddCgTenderD.do?pk_cgtender='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />应标供应商',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('addCgTenderDFrame') && getFrame('addCgTenderDFrame').validate._submitValidate()){
											submitFrameForm('addCgTenderDFrame','addCgTenderDForm');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('只能选择一条招标信息进行应标！');
					return ;
				}
			}
			
			//终止采购招标书
			function stopCgTender(){
// 				var flag=false;
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var codeValue=[];
		            checkboxList.filter(':checked').each(function(){
		            	//判断是否是正在进行的数据，不是的话就返回提示不能设置状态为完成
// 						if($.trim($(this).closest('tr').find('td').eq(4).find('span').attr('title'))==3){
// 							flag = true;
// 						}
		                codeValue.push($(this).val());
		            });
// 		            if(flag){
// 						alerterror('有已经终止的招标书不能再执行终止操作!');
// 						$("#wait2").css("display","none");
// 						$("#wait").css("display","none");
// 						return false;
// 		            }
					$.ajaxSetup({async:false});
					var datas = {};
					datas["chkValue"] = codeValue.join(",");
					$.post('<%=path%>/cgtender/stopCgTender.do',datas,function(data){
						if(data!='1'){
							alerterror(data);
							$("#wait2").css("display","none");
							$("#wait").css("display","none");
							return;
						}else{
							alerttipsbreak('<fmt:message key="operation_successful"/>',function(){
								$("#queryForm").submit();
							});
						}
					});
				}else{
					alerttips('<fmt:message key="please_select_at_least_one_data" />!');
					$("#wait2").css("display","none");
					$("#wait").css("display","none");
				}
			}
			
			//查看招标信息
			function viewCgTender(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '查看采购招标',
						content: '<iframe id="updateCgTenderFrame" frameborder="0" src="<%=path%>/cgtender/toUpdateCgTender.do?status=0&pk_cgtender='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('只能选择一条数据进行查看！');
					return ;
				}
			}
			
			//发布采购招标书
			function releaseCgTender(){
// 				var flag=false;
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var codeValue=[];
		            checkboxList.filter(':checked').each(function(){
		            	//判断是否是已完成的数据，有的话就返回提示不能发布
// 						if($.trim($(this).closest('tr').find('td').eq(4).find('span').attr('title'))>1){
// 							flag = true;
// 						}
		                codeValue.push($(this).val());
		            });
		            if(flag){
						alerterror('<fmt:message key="cgzbpublish"/>');
						$("#wait2").css("display","none");
						$("#wait").css("display","none");
						return false;
		            }
					$.ajaxSetup({async:false});
					var datas = {};
					datas["chkValue"] = codeValue.join(",");
					$.post('<%=path%>/cgtender/releaseCgTender.do',datas,function(data){
						if(data!='1'){
							alerterror(data);
							$("#wait2").css("display","none");
							$("#wait").css("display","none");
							return;
						}else{
							alerttipsbreak('<fmt:message key="publish_sussess"/>',function(){
								$("#queryForm").submit();
							});
						}
					});
				}else{
					alerttips('<fmt:message key="please_select_at_least_one_data"/>！');
					$("#wait2").css("display","none");
					$("#wait").css("display","none");
					return ;
				}
			}
			
			$('.grid .table-body tr').live('click',function(){
					 $('#pk_puprorderm').val($(this).find('td:eq(0)').find('span').attr('title'));
					 var pk_puprorderm=$(this).find('td:eq(0)').find('span').attr('title');
// 					 var istate = $(this).find('td:eq(6)').find('span').attr('title');
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$('#queryForm').submit();
			}
			//显示详情
			function showdetail(pk_cgtender){
				var url = '<%=path%>/cgtender/toTenderYbinfo.do?pk_cgtender='+pk_cgtender;
				$('body').window({
					id: 'window_showYbInfo',
					title: '供应商应标详情',
					content: '<iframe id="listShowYbInfoFrame" frameborder="0" src='+url+'></iframe>',
					width: 900,
					height: 500,
					confirmClose: false,
					draggable: true,
					isModal: true
				});
			}
		</script>
	</body>
</html>