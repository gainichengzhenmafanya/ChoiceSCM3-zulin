<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/puprorder/addPuprorder.do" id="updatepuprorder" name="updatepuprorder" method="post">
			<input type="hidden" id="sta" name="sta" value="${sta}"/>
			<input type="hidden" id="pk_backbill" name="pk_backbill" value="${backbillm.pk_backbill}"/>
			<div class="bj_head" style="height: 50px;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="returns" /><fmt:message key="orders_num" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${backbillm.vbillno}"/>
					</div>
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="document_date" />:</div>
					<div class="form-input" style="width:100px;">
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate"  readonly="readonly" style="text-transform:uppercase;" value="${backbillm.dbilldate}" class="Wdate text"/>
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;">
<%-- 						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${backbillm.delivercode}"/> --%>
						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${backbillm.delivercode}"/>
						<input type="text" name="delivername" id="delivername" readonly="readonly" class="text" value="${backbillm.delivername}"/>
<%-- 						<img id="supplierbutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' /> --%>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" style="width: 355px;" name="vmemo" id="vmemo" class="text" value="${backbillm.vmemo}"/>
					</div>
				</div>
			</div>
			<div class="grid" style="height:330px;">
					<div class="table-head" style="width:100%;">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="inspection" /><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 60px;display:none;"><fmt:message key="returns" /><fmt:message key="batch" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="width:77%;">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
								<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
								<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
								<c:set var="sum_priceamt" value="${0}"/>  <!-- 总陈本金额 -->
								<c:forEach var="backbilld" items="${backbilldList}" varStatus="status">
									<c:set var="sum_num" value="${status.index+1}"/>  
									<c:set var="sum_amount" value="${sum_amount + backbilld.nbacknum}"/>  
									<c:set var="sum_totalamt" value="${sum_totalamt + backbilld.nbacknum}"/>  
									<c:set var="sum_priceamt" value="${sum_priceamt + backbilld.nbacknum*backbilld.nprice}"/>  
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${backbilld.sp_code}"/></span></td>
										<td><span style="width:140px;"><c:out value="${backbilld.sp_name}"/></span></td>
										<td><span style="width:80px;"><c:out value="${backbilld.sp_desc}"/></span></td>
										<td><span style="width:80px;"><c:out value="${backbilld.unit3}"/></span></td>
										<td><span style="width:80px;">${backbilld.ninsnum}</span></td>
										<td><span style="width:60px;text-align: right;" ninsnum="${backbilld.ninsnum}"><fmt:formatNumber value="${backbilld.nbacknum*-1}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;"><input type="hidden" value="${backbilld.nprice}"/><fmt:formatNumber value="${backbilld.nprice}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${backbilld.nmoney*-1}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;display:none;"><c:out value="${backbilld.ibackbatch}"/></span></td>
										<td><span style="width:100px;"><c:out value="${backbilld.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${backbilld.pk_unit}"/></span></td>
										<td style="display: none;"><span><c:out value="${backbilld.ninsnum}"/></span></td>
										<td style="display: none;"><span><c:out value="${backbilld.pk_inspect}"/></span></td>
										<td style="display: none;"><span><c:out value="${backbilld.tax}"/></span></td>
										<td style="display: none;"><span><c:out value="${backbilld.taxdes}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${backbillm.creator}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${backbillm.creationtime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="dmaketime" id="dmaketime" readonly="readonly" class="text" value="${backbillm.nmoney}" />
						</div>	
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="modifier" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifier" id="modifier" class="text" readonly="readonly" value="${backbillm.modifier}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="modifedtime" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifedtime" id="modifedtime" readonly="readonly" class="text" value="${backbillm.modifedtime}" />
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="vbacker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="vbacker" id="vbacker" class="text" readonly="readonly" value="${backbillm.vbacker}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="vbacktime" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="vbacktime" id="vbacktime" readonly="readonly" class="text" value="${backbillm.vbacktime}" />
						</div>
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
<%-- 		var selectrow = null;
			function setSupplier(data1,data2){
				$("#delivercode").val(data2);
				$("#delivername").val(data1);
			}
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			function setPuprOrder(data1,data2){
				$("#pk_puprorderList").val(data1);//主键
				$("#vpuproedername").val(data2);
				selectPuprOrderByPK();
			}
			$(document).ready(function(){
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:600,
						width:800,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:600,
						width:800,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				$('#puproederbutton').click(function(){
					if($("#delivercode").val() == null || $("#delivercode").val() == ""){
						alerterror('<fmt:message key="please_select_suppliers" />！');
						return;
					}
					selectPuprOrder({
						basePath:'<%=path%>',
						title:"123",
						height:600,
						width:800,
						callBack:'setPuprOrder',
						delivercode:$("#delivercode").val(),
						type:1,
						single:true
					});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
// 				$("#dbilldate").click(function(){
// 					new WdatePicker({minDate:'#F{$dp.$D(\'creationtime\')}'});
// 				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+125));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+125));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "enter") {
// 					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:12,
					widths:[16,110,150,90,90,70,70,70,70,70,70,110],
					colStyle:['','',{background:"#F1F1F1"},'','','','','','','','',''],
					onEdit:$.noop,
					isAdd:false,
					editable:[5,8,9],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),3);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
						function getName(){
							var vname='';
							$.ajaxSetup({ 
								  async: false 
							});
							$.get("<%=path %>/material/findMaterialTop.do",
									{vname:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										vname =  data.vname;
									});
							return vname;
						};
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								selectrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />！');
								return;
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?delivercode='+$('#delivercode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + sp_desc;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span input").val(data2.vname).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.unitvname);
							row.find("td:eq(5) span").text('0.00').css("text-align","right");
							row.find("td:eq(6) span").text('').css("text-align","right");
							row.find("td:eq(7) span").text('0.00').css("text-align","right");
							row.find("td:eq(8) span").text('0.00').css("text-align","right");
							row.find("td:eq(9) span").text(data2.vmemo);
							row.find("td:eq(10) span").text(data2.pk_unit);
							row.find("td:eq(11) span").text(data2.sp_code);
							$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do",
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										row.find("td:eq(6) span").text('0.00').css("text-align","right");
									}else{
										row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
									}
								}
							});
							
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else {
								if(Number(data2.value) > 0){
									row.find("td:eq(5) span").text(-data2.value);
								}
								if(isNaN(row.find('td:eq(6)').text())){
									row.find("td:eq(6) span").text(0.0);
									row.find("td:eq(7) span").text(0.0);
								} else {
									row.find("td:eq(7) span").text((Number(row.find('td:eq(5)').text()) * Number(row.find("td:eq(6)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(row,6);
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else {
								if(isNaN(row.find('td:eq(6)').text())){
									row.find("td:eq(6) span").text(0.0);
									row.find("td:eq(7) span").text(0.0);
								} else {
									row.find("td:eq(7) span").text((Number(row.find('td:eq(5)').text()) * Number(row.find("td:eq(6)").text())).toFixed(2));
								}
// 								row.find("td:eq(7) span").text((Number(row.find('td:eq(5)').text()) * Number(row.find("td:eq(6)").text())).toFixed(2));
// 								Sprice=Sprice+Number(row.find('td:eq(7)').text());//总金额
								$.fn.autoGrid.setCellEditable(row,7);
							}
						}
					},{
						index:7,
						action:function(row,data){
							if(Number(data.value) > 0){
								row.find("td:eq(7) span").text(-data.value);
							}
							row.find("td:eq(6) span").text((Number(row.find("td:eq(7) span").text())/Number(row.find("td:eq(5)").text())).toFixed(2));
							$.fn.autoGrid.setCellEditable(row,8);
						}
					},{
						index:8,
						action:function(row,data2){
							$.fn.autoGrid.setCellEditable(row,9);
						}
					},{
						index:9,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			} --%>
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			//确认退货
			function enterBackbillm(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				var data = {};
				data["pk_backbill"] = $("#pk_backbill").val();

				$.ajaxSetup({async:false});
				$.post("<%=path%>/backbill/updateBackbill.do?sta="+$('#sta').val(),data,function(data){
					data=eval('('+data+')').pr;
					if(data=="-3"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="can_not_find_the_corresponding_materials"/>!<fmt:message key="repeat_return_record"/>',
							speed: 3000
						});	
						return;
					}else if(data=="-1"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="data_in_material_code_is_not_correct"/>,<fmt:message key="please_check"/>？',
							speed: 3000
						});	
						return;
					}else if(data=="0"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="te_data_have_been_audited"/>,<fmt:message key="no_need_to_re_submit"/>！',
							speed: 3000
						});	
						return;
					}else if(data=="-9"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="can_only_operate_the_accounting_year_of_the_bill"/>!',
							speed: 6000
						});	
						return;
					}else if(data=="-4"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot"/><fmt:message key="operation"/><fmt:message key="MonthClose_from"/><fmt:message key="accounting-period"/><fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/><fmt:message key="de"/><fmt:message key="data"/>!',
							speed: 3000
						});
						return;
					}else if(data=="-11"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="In_orders"/><fmt:message key="create_failer"/>!',
							speed: 3000
						});
						return;
					}
					else{
						showMessage({
							type: 'success',
							msg: '<fmt:message key="returns"/><fmt:message key="successful"/>！',
							speed: 3000,
							handler:function(){
								parent.reloadPage();
								parent.$('.close').click();}
							});
					};
				});	
			}
<%-- 		function selectPuprOrderByPK(){
				var pk_puprorder = $("#pk_puprorderList").val();
				var vpuproedername = $("#vpuproedername").val();
				$('#savepuprorder').attr('action','<%=path%>/inspect/toAddInspectmOrder.do');
				$('#savepuprorder').submit();
			}
			function setMaterial(data2){
				var flag = true;
				if(data2=='undefined'){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data2.sp_code){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				selectrow.find("td:eq(1) span").text(data2.sp_code).css("text-align","right");
				selectrow.find("td:eq(2) span input").val(data2.sp_name).focus();
				selectrow.find("td:eq(3) span").text(data2.sp_desc);
				selectrow.find("td:eq(4) span").text(data2.entity[0].unit3);
				selectrow.find("td:eq(5) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(6) span").text('').css("text-align","right");
				selectrow.find("td:eq(7) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(8) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(9) span").text(data2.vmemo);
				selectrow.find("td:eq(10) span").text(data2.unit3);
				selectrow.find("td:eq(11) span").text(data2.sp_code);
				$.ajax({//查询物资
					type: "POST",
					url: "<%=path%>/material/findMaterialNprice.do",
					data: "sp_code="+data2.sp_code,
					dataType: "json",
					success:function(data3){
						if(data3.sp_price == 0){
                            selectrow.find("td:eq(6) span").text('0.00').css("text-align","right");
						}else{
                            selectrow.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
						}
					}
				});
			}
			
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			}; --%>
		</script>
	</body>
</html>
