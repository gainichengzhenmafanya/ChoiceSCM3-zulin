<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="sta" name="sta" value="${sta}"/>
		<form action="<%=path%>/puprorder/addPuprorder.do" id="savebackbill" name="savebackbill" method="post">
			<div class="bj_head" style="height: 50px;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="returns" /><fmt:message key="orders_num" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${backbillm.vbillno}"/>
					</div>
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="document_date" />:</div>
					<div class="form-input" style="width:100px;">
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate" style="text-transform:uppercase;" value="${backbillm.dbilldate}" class="Wdate text"/>
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;">
<!-- 						<input type="hidden" name="delivercode" id="delivercode" class="text" value=""/> -->
						<input type="hidden" name="delivercode" id="delivercode" class="text" value=""/>
						<input type="text" name="delivername" style="margin-bottom: 6px;" id="delivername" readonly="readonly" class="text" value=""/>
						<img id="supplierbutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label" style="width: 5%;margin-left: 70px;"><fmt:message key="remark" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" style="width: 355px;" name="vmemo" id="vmemo" class="text" value="${backbillm.vmemo}"/>
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 15px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="returns" /><fmt:message key="batch" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${backbillm.creator}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${backbillm.creationtime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="0.00" />
						</div>	
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
        	$("#dbilldate").focus();
			var selectrow = null;
			function setSupplier(data){
// 				$("#delivercode").val(data.entity[0].delivercode);//主键
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			$(document).ready(function(){
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
				$("#dbilldate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'creationtime\')}'});
				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+140));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+140));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "add") {
					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:13,
					widths:[16,110,150,70,70,70,70,70,70,110],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6,8,9],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
						
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}
						 	else if(!data.actionobj){
								return;
							} 
							$.fn.autoGrid.setCellEditable(rownum,5);
						}
						function getName(){
							var vname='';
							$.ajaxSetup({ 
								  async: false 
							});
							$.get("<%=path %>/material/findMaterialTop.do",
									{vname:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										vname =  data.vname;
									});
							return vname;
						};
						if(pos == 5){
							var thnum = $.trim(rownum.find("td:eq(5) span").text());
							if(isNaN(thnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
							}else if(Number(thnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
							}else if(Number(thnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
							}else{
								var nmoney = Number($("#nmoney").val());
								var prenrowmoney = Number($.trim(rownum.find("td:eq(7) span").text())).toFixed(2);
								
								var price = Number($.trim(rownum.find("td:eq(6) span").text()));
								rownum.find("td:eq(5) span").text(Number(thnum).toFixed(2));
								rownum.find("td:eq(7) span").text(Number(price*thnum).toFixed(2));
								
								var diffmoney = Number($.trim(rownum.find("td:eq(7) span").text()))-prenrowmoney;
								$("#nmoney").val(Number(Number($("#nmoney").val())+Number(diffmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(rownum,6);
							}
						}
						if(pos == 6){
							var thnum = $.trim(rownum.find("td:eq(5) span").text());
							if(isNaN(rownum.find("td:eq(6) span").text())){
								alerterror('<fmt:message key="please_enter_positive_integer"/>！');
							}else if(Number(rownum.find("td:eq(6) span").text()) < 0){
								alerterror('<fmt:message key="unitprice_cannot_be_negative"/>');
								rownum.find("td:eq(6) span").text(0.00);
							}else{
								var nmoney = Number($("#nmoney").val());
								var prenrowmoney = Number($.trim(rownum.find("td:eq(7) span").text())).toFixed(2);
								
								var price = Number($.trim(rownum.find("td:eq(6) span").text()));
								rownum.find("td:eq(5) span").text(Number(thnum).toFixed(2));
								rownum.find("td:eq(7) span").text(Number(price*thnum).toFixed(2));
								
								var diffmoney = Number($.trim(rownum.find("td:eq(7) span").text()))-prenrowmoney;
								$("#nmoney").val(Number(Number($("#nmoney").val())+Number(diffmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(rownum,8);
							}
							$.fn.autoGrid.setCellEditable(rownum,8);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								selectrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                delivercode:$('#delivercode').val(),
							                irateflag:0,
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									var prerowmoney = row.find("td:eq(7) span").text();
									if(prerowmoney!=''){
										prerowmoney = Number(prerowmoney);
									}else{
										prerowmoney = 0 ;
									}
									
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(7) span").text('');
									row.find("td:eq(8) span").text('');
									row.find("td:eq(11) span").text('');
									row.find("td:eq(12) span").text('');
									
									$("#mMenu").remove();
									$("#nmoney").val(Number(Number($("#nmoney").val())-Number(prerowmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(row,2);
									return;	
								}
							}
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />！');
								return;
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?delivercode='+$('#delivercode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + sp_desc;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span").text(data2.vname);
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.unitvname);
							row.find("td:eq(5) span").text('0.00').css("text-align","right");
							row.find("td:eq(6) span").text('').css("text-align","right");
							row.find("td:eq(7) span").text('0.00').css("text-align","right");
							row.find("td:eq(8) span").text(1).css("text-align","right");
							row.find("td:eq(9) span").text(data2.vmemo);
							row.find("td:eq(10) span").text(data2.pk_unit);
							row.find("td:eq(11) span").text(data2.sp_code);
							row.find("td:eq(12) span").text(data2.vname);
							$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val(),
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										$.ajax({
											type: "POST",
											url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
											data: "sp_code="+data2.sp_code,
											dataType: "json",
											success:function(data4){
												if(data4.sp_price== 0){
													row.find("td:eq(6) span").text('0.00').css("text-align","right");
													$.fn.autoGrid.setCellEditable(row,5);
												}else{
													row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
													$.fn.autoGrid.setCellEditable(row,5);
												}
											}
										})
									}else{
										row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
										$.fn.autoGrid.setCellEditable(row,5);
									}
								}
							});
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else {
								$.fn.autoGrid.setCellEditable(row,6);
							}
						},
						onCellEdit:function(event,data,row){
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />！');
								return;
							}
							if (row.find("td:eq(2) span").text() == '') {
								alerterror('<fmt:message key="please_select_materials" />！');
								row.find("td:eq(5) span").text('');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5) span").text('0.00');
								row.find('td:eq(7) span').text('0.00');
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								var prenrowmoney = Number($.trim(row.find("td:eq(7) span").text())).toFixed(2);
								cnt = Number($.trim(cnt));
								var price = row.find("td:eq(6) span").text();
								if(isNaN($.trim(price))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									price = Number($.trim(price));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(7) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prenrowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(Number(data2.value) < 0){
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else {
								if(isNaN(row.find('td:eq(6) span').text())){
									row.find("td:eq(7) span").text(0.0);
								}else{
									row.find("td:eq(7) span").text((Number(row.find("td:eq(5) span").text())*Number(row.find("td:eq(6) span").text())).toFixed(2));
								}
							}
							$.fn.autoGrid.setCellEditable(row,8);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />！');
								return;
							}
							if (row.find("td:eq(2) span").text() == '') {
								alerterror('<fmt:message key="please_select_materials" />！');
								row.find("td:eq(6) span").text('');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							var nmoney = Number($("#nmoney").val());
							var prenrowmoney = Number($.trim(row.find("td:eq(7) span").text())).toFixed(2);
							var price = data.value;
							if(isNaN($.trim(price))){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6) span").text('0.00');
								row.find('td:eq(7) span').text('0.00');
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								price = Number($.trim(price));
								var cnt = row.find("td:eq(5) span").text();
								if(isNaN($.trim(cnt))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									cnt = Number($.trim(cnt));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(7) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prenrowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:7,
						action:function(row,data){
							if(Number(data.value) > 0){
								row.find("td:eq(7) span").text(-data.value);
							}
							row.find("td:eq(6) span").text((Number(row.find("td:eq(7) span").text())/Number(row.find("td:eq(5) span").text())).toFixed(2));
							$.fn.autoGrid.setCellEditable(row,8);
						}
					},{
						index:8,
						action:function(row,data2){
							$.fn.autoGrid.setCellEditable(row,9);
						}
					},{
						index:9,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function savepuprorder(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var sp_code=$.trim($(this).find("td:eq(11) span").text());
					if(sp_code==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
					}
				});
				$('.grid').find('.table-body').find('table').find('td').find('input').each(function(){
					$(this).trigger('onEnter');
				});
				
				var keys = ["sp_code","sp_name","sp_desc","unit3","nbacknum","nprice","nmoney","ibackbatch","vmemo","pk_unit","sp_code","sp_name"];
				var data = {};
				var i = 0;
				data["vbillno"] = $("#vbillno").val();
				data["dbilldate"] = $("#dbilldate").val();
				data["delivercode"] = $("#delivercode").val();
				data["creator"] = $("#creator").val();
				data["nmoney"] = Number($("#nmoney").val()*-1).toFixed(2);
				data["creationtime"] = $("#creationtime").val();
				data["delivername"] = $("#delivername").val();
				data["delivercode"] = $("#delivercode").val();
				data["istate"] = $("#istate").val();
				data["vmemo"] = $("#vmemo").val();
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var sp_code=$.trim($(rows[0]).find("td:eq(11) span").text());
					if(sp_code==''){
						alerterror('至少选择一条物资！');
						savelock = false;
						return;
					}
				}
				for(i=0;i<rows.length;i++){
					var vcode = $.trim($(rows[i]).find("td:eq(1) span").text());
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					if(vcode == '' || vcode == null){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />!');
						savelock = false;
						return;
					}else if(vname == '' || vname == null || vname != $(rows[i]).find("td:eq(12) span").text()){
						$(rows[i]).find("td:eq(2) span input").val($.trim($(rows[i]).find("td:eq(12) span").text()));
					}else if(name == '' || name == null || name != $(rows[i]).find("td:eq(12) span").text()){
						$(rows[i]).find("td:eq(2) span").text($.trim($(rows[i]).find("td:eq(12) span").text()));
					}
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					var ncnt = Number($.trim($(rows[i]).find("td:eq(5) span input").val()));
					var cnt = Number($.trim($(rows[i]).find("td:eq(5) span").text()));
					ncnt == 0.0?cnt*-1:ncnt*-1;
					if((vname == '' || vname == null) && (name == '' || name == null)){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
						savelock = false;
						return;
					}
					if(ncnt == 0.0 && cnt == 0.0){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="number_cannot_be_zero" />！');
						savelock = false;
						return;
					}
					var status=$(rows[i]).find('td:eq(0)').text();
					data["backbilldList["+i+"]."+"nprice"] = $(rows[i]).data('nprice') ? $(rows[i]).data('nprice'):$('#nprice').val();
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value){
							if(j==5 || j==7){
								value = Number(value)*-1;
							}
							data["backbilldList["+i+"]."+keys[j-1]] = value;
						}
							
					}
				}
				$.ajaxSetup({async:false});
				if(checkvcode()){
					$.post("<%=path%>/backbill/saveBackbill.do?sta="+$('#sta').val(),data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs)){
						case -1:
							alerterror('<fmt:message key="save_fail"/>！');
							savelock = false;
							break;
						case 1:
							showMessage({
										type: 'success',
										msg: '<fmt:message key="save_successful"/>！',
										speed: 3000,
										handler:function(){
											parent.reloadPage();
											parent.$('.close').click();}
										});
							break;
						}
					});	
				}else{
					alerterror('<fmt:message key="document_no"/><fmt:message key="duplicate"/>，<fmt:message key="please_check_number_rule"/>！');
				}
			}
			//*****************验证单据号是否重复，校验编码规则是否设置正确**************************************
			function checkvcode(){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/backbill/findBackbillByCode.do",{vbillno:$("#vbillno").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			//*****************验证单据号是否重复，校验编码规则是否设置正确**************************************
			function setMaterial(data2){
				var flag = true;
				if(data2.entity[0].sp_code==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data2.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				selectrow.find("td:eq(1) span").text(data2.entity[0].vcode).css("text-align","right");
				selectrow.find("td:eq(2) span input").val(data2.entity[0].vname).focus();
				selectrow.find("td:eq(3) span").text(data2.entity[0].sp_desc);
				selectrow.find("td:eq(4) span").text(data2.entity[0].unit3);
				selectrow.find("td:eq(5) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(6) span").text('').css("text-align","right");
				selectrow.find("td:eq(7) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(8) span").text(1).css("text-align","right");
				selectrow.find("td:eq(9) span").text(data2.entity[0].vmemo);
				selectrow.find("td:eq(10) span").text(data2.entity[0].pk_unit);
				selectrow.find("td:eq(11) span").text(data2.entity[0].sp_code);
				selectrow.find("td:eq(12) span").text(data2.entity[0].vname);
				$.ajax({//查询物资
					type: "POST",
					url: "<%=path%>/material/findMaterialNprice.do?positoncode="+$('#positncode').val(),
					data: "sp_code="+data2.sp_code,
					dataType: "json",
					success:function(data3){
						if(data3.sp_price == 0){
							$.ajax({
								type: "POST",
								url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data4){
									if(data4.sp_price == 0){
										selectrow.find("td:eq(6) span").text('0.00').css("text-align","right");
									}else{
										selectrow.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
									}
								}
							})
						}else{
							selectrow.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
						}
					}
				});
			}
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").html('');
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
		</script>
	</body>
</html>
