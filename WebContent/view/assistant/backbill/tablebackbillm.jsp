<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.search-div .form-line .form-label{
				width: 10%;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div style="height:50%;">
	    	<div id="tool"></div>
	    	<div>
			    <input type="hidden" id="pk_backbill" name="pk_backbill" value="${pk_backbill}"/>
		    	<form id="queryForm" action="<%=path%>/backbill/queryAllBackbillm.do" method="post">
		    		<div style="height: 50px;border: solid 1px gray;">
						<div class="form-line">
							<div class="form-label"><fmt:message key="startdate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="bdate" name="bdate" style="text-transform:uppercase;" value="${backbillm.bdate}" onkeyup="ajaxSearch()" class="Wdate text"/>
							</div>
							<div class="form-label" >退货单号:</div>
							<div class="form-input" >
								<input type="text" name="vbillno" id="vbillno" class="text" value="${backbillm.vbillno}"/>
							</div>
							<div class="form-label"><fmt:message key="suppliers" />:</div>
							<div class="form-input">
<%-- 								<input type="hidden" id="delivercode" name="delivercode" style="width:133px" class="text" value="${backbillm.delivercode}"/> --%>
								<input type="hidden" id="delivercode" name="delivercode" style="width:133px" class="text" value="${backbillm.delivercode}"/>
								<input type="text" id="delivername" style="margin-bottom: 6px;" name="delivername" style="width:133px" class="text" value="${backbillm.delivername}"/>
								<img id="supplierbutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
							</div>
						</div>
						<div class="form-line">
							<div class="form-label"><fmt:message key="enddate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${backbillm.edate}" onkeyup="ajaxSearch()" class="Wdate text"/>
							</div>
							<div class="form-label"><fmt:message key="Document_status" />:</div>
							<div class="form-input">
								<select id="istate" name="istate" style="width:133px" onkeyup="ajaxSearch()" class="select">
									<option value="0" <c:if test="${backbillm.istate==0}">selected=selected</c:if> ><fmt:message key="all" /></option>
									<option value="2" <c:if test="${backbillm.istate==2}">selected=selected</c:if> >未退货</option>
									<option value="1" <c:if test="${backbillm.istate==1}">selected=selected</c:if> >已退货</option>
<%-- 									<option value="3" <c:if test="${backbillm.istate==3}">selected=selected</c:if> >已结算</option> --%>
								</select>
							</div>
						</div>
					</div>
					<div class="grid" class="grid" style="overflow: hidden;positn:static;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td  class="num" style="width:31px;"></td>
										<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
										<td style="width:145px;"><fmt:message key="returns" /><fmt:message key="orders_num" /></td>
										<td style="width:110px;"><fmt:message key="returns" /><fmt:message key="document_number" /></td>
										<td style="width:150px;"><fmt:message key="shouyedingdanbh" /></td>
										<td style="width:160px;"><fmt:message key="suppliers" /></td>
										<td style="width:70px;"><fmt:message key="total_amount1" /></td>
										<td style="width:90px;"><fmt:message key="document_date" /></td>
										<td style="width:60px;"><fmt:message key="Document_status" /></td>
										<td style="width:160px;"><fmt:message key="last_time" /></td>
										<td style="width:240px;"><fmt:message key="summary" /></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="backbillm" varStatus="step" items="${backbillmList}">
										<tr>
											<td class="num"><span id="pk_backbill" style="width:21px;">${step.count}</span></td>
											<td><span style="width:20px; text-align: center;">
												<input type="checkbox" name="idList" id="chk_${backbillm.pk_backbill}" value="${backbillm.pk_backbill}"/>
											</span></td>
											<td><span title="${backbillm.vbillno}" style="width:135px;text-align: left;">${backbillm.vbillno}</span></td>
											<td><span title="${backbillm.vouno}" style="width:100px;text-align: left;">${backbillm.vouno}</span></td>
											<td><span title="${backbillm.vinspectbillno}" style="width:140px;text-align: center;">${backbillm.vinspectbillno}</span></td>
											<td><span title="${backbillm.delivername}" style="width:150px;text-align: left;">${backbillm.delivername}</span></td>
											<td><span title="${backbillm.nmoney}" style="width:60px;text-align: right;">${backbillm.nmoney}</span></td>
											<td><span title="${backbillm.dbilldate}" style="width:80px;text-align: center;">${backbillm.dbilldate}</span></td>
											<td><span title="${backbillm.istate}" style="width:50px;text-align: center;">
												<c:if test="${backbillm.istate==1}">已退货</c:if>
												<c:if test="${backbillm.istate==2}">未退货</c:if>
												<c:if test="${backbillm.istate==3}">已结算</c:if>
											</span></td>
											<td><span title="${backbillm.vmemo}" style="width:150px;text-align: center;">${backbillm.ts}</span></td>
											<td><span title="${backbillm.vmemo}" style="width:230px;text-align: left;">${backbillm.vmemo}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<page:page form="queryForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
				</form>
			</div>
		</div>
		<form id="printForm" action="<%=path%>/backbill/printBackbill.do" method="post">
				<input type="hidden" id="printpk" name="pk_backbill" value="${pk_backbill}"/>
			</form>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/backbill/queryAllBackbilld.do?pk_backbill=${pk_backbill}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
        $("#bdate").focus();
			//快捷键操作（Alt+*）
			$(document).keydown(function (e) {
			    var doPrevent;
			    if (event.keyCode==78 && event.altKey) {//alt+N自制
			    	if(!$('#window_supply').html()){
// 			    		savebackbill();
			    	}
			    }else if(event.keyCode==77 && event.altKey){//alt+M来自验货单
			    	if(!$('#window_supply').html()){
			    		savebackbillbyyh();
			    	}
			    }else if(event.keyCode==85 && event.altKey){//alt+U
			    	if(!$('#window_supply').html()){
			    		updatebackbill();
			    	}
			    }else{
			        doPrevent = false;
			    }
			    if (doPrevent)
			        e.preventDefault();
			}); 
			function setSupplier(data){
// 				$("#delivercode").val(data.entity[0].delivercode);//主键
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setSupplier',
					single:false
				});
			});
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
					parent.$("#mainFrame").submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});

				$("#bdate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});
				});
				$("#edate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});
				});
				/* 导出提交 */
				$("#print").bind('click', function() {
					var reportFrom=$('#reportFrom').val();
					var reportTo=$('#reportTo').val();
					var ex = /^[0-9]*[1-9][0-9]*$/;
					if (ex.test(reportFrom) && ex.test(reportTo)) {
						$('.print-div').hide();
						$('#printForm').submit();
						
					}else{
						alerterror('<fmt:message key="export_range_please_enter_positive_integer" />！');
					}

				});
				setElementHeight('.grid',['#tool','.mainFrame'],$(document.body),115);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
// 				$(".page").css("margin-bottom",$("body").height()*0.45+$(".page").height());
// 				$(".page").css("margin-bottom",$(".mainFrame").height()+40);
				$(".page").css("margin-bottom",$(".mainFrame").height());
				$('.grid .table-body tr').live('click',function(){
					 $('#pk_backbill').val($(this).find('td:eq(1)').find('input').val());
					 var pk_backbill=$(this).find('td:eq(1)').find('input').val();
					 $("#printpk").val(pk_backbill);
					 var  url="<%=path%>/backbill/queryAllBackbilld.do?pk_backbill="+pk_backbill;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							$("#vbillno").val(stripscript($("#vbillno").val()));
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							savebackbillbyyh();
						}/*,
						items:[{
								text: '<fmt:message key="Self_add" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['0px','0px']
								},
								handler: function(){
									savebackbill();
								}
							},{
								text: '来自验货单',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['0px','0px']
								},
								handler: function(){
									savebackbillbyyh();
								}
							}]*/
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updatebackbill();
						}
					},{
						text: '<fmt:message key="confirm" /><fmt:message key="returns" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'confirm')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							enterbackbillm();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deletebackbillm();
						
						}
					},{
						text: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							printbackbill();
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			//新增退货单======来自验货单
			function savebackbillbyyh(){
				$('body').window({
					id: 'window_supply',
					title: '新增退货单',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/backbill/toAddBackBillByYH.do?sta=add"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增退货单
			function savebackbill(){
				$('body').window({
					id: 'window_supply',
					title: '新增退货单',
					content: '<iframe id="savebackbill" frameborder="0" src="<%=path%>/backbill/toAddBackBillm.do?sta=add"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savebackbill')&&window.document.getElementById("savebackbill").contentWindow.validate._submitValidate()){
										window.document.getElementById("savebackbill").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改验货单
			function updatebackbill(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var stat = '';
					checkboxList.filter(':checked').each(function(){
						stat = $.trim($(this).closest('tr').find('td').eq(8).find('span').text());
					});
					if('未退货' != stat){
						alerterror("当前退货单已经确认退货，不可修改！");
						return;
					}
					 //后台得到是否已经审核过
					 var htstate = getBackBillmIstate(chkValue);
					 if(htstate!="2"){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					 }
					$('body').window({
						id: 'window_supply',
						title: '修改退货单',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/backbill/toUpdateBackbill.do?sta=add&pk_backbill='+chkValue+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepuprorder").contentWindow.updateInspectm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//确认退货单
			function enterbackbillm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var stat = '';
					checkboxList.filter(':checked').each(function(){
						stat = $.trim($(this).closest('tr').find('td').eq(8).find('span').text());
					});
					if('未退货' != stat){
						alerterror("当前验货单已经确认退货，不需要确认！");
						return;
					}
					 //后台得到是否已经审核过
					 var htstate = getBackBillmIstate(chkValue);
					 if(htstate!="2"){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					 }
					$('body').window({
						id: 'window_supply',
						title: '确认退货',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/backbill/toUpdateBackbill.do?sta=enter&pk_backbill='+chkValue+'"></iframe>',
						/*content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/backbill/uploadBackBilToMall.do?sta=enter&pk_backbill='+chkValue+'"></iframe>',*/
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="enter" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepuprorder").contentWindow.enterBackbillm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('请选择要确认的数据！');
					return ;
				}
			}
			//删除
			function deletebackbillm(){
				var flag = true;
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							var row = $(this).closest('tr');
							if($.trim(row.children('td:eq(8)').text()) == "已退货" ||
									$.trim(row.children('td:eq(8)').text()) == "已结算"){
								flag = false;
								alerterror('单据已处理，不能删除！');
								return;
							}
						});
						if(flag){
							alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
								var action = '<%=path%>/backbill/deleteBackBillm.do?pks='+chkValue.join(",");
								$('body').window({
									title: '<fmt:message key="delete_systemcoding_information" />',
									content: '<iframe frameborder="0" src='+action+'></iframe>',
									width: '500px',
									height: '245px',
									draggable: true,
									isModal: true
								});
							});
						}
					
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
				
			}
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':checkbox')[0].checked) {
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			});
			//---------------------------
// 			全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$("#queryForm").submit();
			}
			document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
	        
	        function printbackbill(){
	        	var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					$("#printForm").attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					$("#printForm").submit();
				}else{
					alerterror('请选择一条退货单！');
				}
	        }
	        
	        //根据退货单得到状态
	        function getBackBillmIstate(pk_backbill){
				var res = 1;
				$.ajax({
					type: "POST",
					url: "<%=path%>/backbill/queryBackBillmIstate.do",
					data: {'pk_backbill':pk_backbill},
					dataType: "json",
					async:false,
					success: function(data){
						res = data;
						}
					});
				return res;
	        }
		</script>
	</body>
</html>