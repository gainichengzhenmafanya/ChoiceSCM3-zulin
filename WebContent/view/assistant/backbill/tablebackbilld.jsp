<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
    	.linebottom{
    		positn: fixed; 
		bottom: 17px; 
    	}
    	.childgrid{
    		width: 99.8%;
		border:solid 1px #8DB2E3;
		margin-top:6px;
	}
		</style>
	</head>
	<body>
		<div class="childgrid">
            <div class="grid">
                <div class="table-head" >
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td><span style="width: 120px;"><fmt:message key="supplies_code" /></span></td>
	                            <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                            <td><span style="width: 100px;"><fmt:message key="specification" /></span></td>
	                            <td><span style="width: 70px;"><fmt:message key="unit" /></span></td>
	                            <td><span style="width: 100px;"><fmt:message key="quantity" /></span></td>
	                            <td><span style="width: 100px;"><fmt:message key="unit_price" /></span></td>
	                            <td><span style="width: 100px;"><fmt:message key="amount" /></span></td>
	                            <td><span style="width: 100px;display:none;"><fmt:message key="returns" /><fmt:message key="batch" /></span></td>
	                            <td><span style="width: 200px;"><fmt:message key="remark" /></span></td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                        	<c:forEach var="backbilld" varStatus="step" items="${backbilldList}">
	                            <tr>
	                                <td><span style="width: 120px;">${backbilld.sp_code}</span></td>
	                                <td><span style="width: 140px;">${backbilld.sp_name}</span></td>
	                                <td><span style="width: 100px;">${backbilld.sp_desc}</span></td>
	                                <td><span style="width: 70px;">${backbilld.unit3}</span></td>
	                                <td><span style="width: 100px;text-align: right;"><fmt:formatNumber value="${backbilld.nbacknum}" pattern="0.00"/></span></td>
	                                <td><span style="width: 100px;text-align: right;"><fmt:formatNumber value="${backbilld.nprice}" pattern="0.00"/></span></td>
	                                <td><span style="width: 100px;text-align: right;"><fmt:formatNumber value="${backbilld.nmoney}" pattern="0.00"/></span></td>
	                                <td><span style="width: 100px;text-align: right;display:none;">${backbilld.ibackbatch}</span></td>
	                                <td><span style="width: 200px;">${backbilld.vmemo}</span></td>
	                            </tr>
	                    	</c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
			    <div class="form-line linebottom">
					<div class="form-label"><fmt:message key="total" /><fmt:message key="supplies" />:</div>
					<div class="form-input">
						<span id="countmaterial">${length}</span>
						<span><fmt:message key="article" /></span>
					</div>
					<div class="form-label"><fmt:message key="total_amount1" />:</div>
					<div class="form-input">
						<span>￥</span>
						<span id="countmoney"><fmt:formatNumber value="${summoney}" pattern="0.00"/></span>
					</div>
				</div>
			</div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    	var height = $(parent.document.body).height()*0.5-12;
		$(".childgrid").css('height',height-17);
		//自动实现滚动条
		$(".grid").css('height',height-$(".form-line").height());
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())*0.998);
		$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())*0.998);
		loadGrid();//  自动计算滚动条的js方法		
		changeTh();
		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	})
	    
    </script>
</body>
</html>
