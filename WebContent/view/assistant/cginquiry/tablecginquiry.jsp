<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.page{
/* 					margin-bottom:20px; */
				}
			.search-div .form-line .form-label{
					width: 10%;
				}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div> 
		<div>
	    	<div id="tool"></div>
	    	<form id="queryForm" action="<%=path%>/cginquiry/tableCgInquiry.do" method="post">
	    		<input type="hidden" id="pk_material" name="pk_material" value="${cgInquiry.pk_material}"/>
	    		<div class="search-div" style="positn: absolute;z-index:20">
					<div class="form-line">
						<div class="form-label"><fmt:message key="supplies_code" />:</div>
						<div class="form-input">
							<input type="text" id="sp_code" name="sp_code" value="${cgInquiry.sp_code}" onkeyup="ajaxSearch()" class="text"/>
						</div>
						<div class="form-label"><fmt:message key="supplies_name" />:</div>
						<div class="form-input">
							<input type="text" id="sp_name" name="sp_name" value="${cgInquiry.sp_name}" onkeyup="ajaxSearch()" class="text"/>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="suppliers" />:</div>
						<div class="form-input">
							<input type="hidden" id="delivercode" name="delivercode"  class="text" value="${cgInquiry.delivercode}"/>
							<input type="hidden" id="delivercode" name="delivercode"  class="text" value="${cgInquiry.delivercode}"/>
							<input type="text" id="delivername" style="margin-bottom: 6px;" name="delivername"  class="text" value="${cgInquiry.delivername}" onkeyup="ajaxSearch()"/>
							<img id="supplierbutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
		       			<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
					</div>
				</div>
				<div class="grid" class="grid">
					<div class="table-head" style="magin-left:-2px;">
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td class="num" style="width:30px;"></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:80px;">询价类型</span></td>
									<td><span style="width:120px;"><fmt:message key="supplies_code" /></span></td>
									<td><span style="width:160px;"><fmt:message key="supplies_name" /></span></td>
									<td><span style="width:120px;">供应商编码</span></td>
									<td><span style="width:160px;">供应商名称</span></td>
									<td><span style="width:80px;">询价价格</span></td>
									<td><span style="width:80px;">联系人</span></td>
									<td><span style="width:80px;">联系电话</span></td>
									<td><span style="width:80px;">询价人</span></td>
									<td><span style="width:80px;">询价时间</span></td>
									<td><span style="width:80px;">到期时间</span></td>
									<td><span style="width:120px;">备注</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="cgInquiry" varStatus="step" items="${listCgInquiry}">
									<tr>
										<td class="num"><span title="" style="width:20px;">${step.count}</span></td>
										<td><span style="width:30px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${cgInquiry.pk_cginquiry}" value="${cgInquiry.pk_cginquiry}"/>
										</span></td>
										<td><span title="${cgInquiry.vinquiryname}" style="width:80px;text-align: left;">${cgInquiry.vinquiryname}</span></td>
										<td><span title="${cgInquiry.sp_code}" style="width:120px;text-align: left;">${cgInquiry.sp_code}</span></td>
										<td><span title="${cgInquiry.sp_name}" style="width:160px;text-align: left;">${cgInquiry.sp_name}</span></td>
										<td><span title="${cgInquiry.delivercode}" style="width:120px;text-align: left;">${cgInquiry.delivercode}</span></td>
										<td><span title="${cgInquiry.delivername}" style="width:160px;text-align: left;">${cgInquiry.delivername}</span></td>
										<td><span title="${cgInquiry.ninquiryprice}" style="width:80px;text-align: right;"><fmt:formatNumber value="${cgInquiry.ninquiryprice}" pattern="0.00"/></span></td>
										<td><span title="${cgInquiry.vsuppliercontact}" style="width:80px;text-align: left;">${cgInquiry.vsuppliercontact}</span></td>
										<td><span title="${cgInquiry.vsupplierphone}" style="width:80px;text-align: left;">${cgInquiry.vsupplierphone}</span></td>
										<td><span title="${cgInquiry.vinquirycontact}" style="width:80px;text-align: left;">${cgInquiry.vinquirycontact}</span></td>
										<td><span title="${cgInquiry.dinquirybdat}" style="width:80px;text-align: center;">${cgInquiry.dinquirybdat}</span></td>
										<td><span title="${cgInquiry.dinquiryedat}" style="width:80px;text-align: center;">${cgInquiry.dinquiryedat}</span></td>
										<td><span title="${cgInquiry.vmemo}" style="width:120px;text-align: left;">${cgInquiry.vmemo}</span></td>
<%-- 										<td><span title="${cgInquiry.pk_inquiry}" style="width:190px;text-align: left;display: none;">${cgInquiry.pk_inquiry}</span></td> --%>
<%-- 										<td><span title="${cgInquiry.vinquirycode}" style="width:190px;text-align: left;display: none;">${cgInquiry.vinquirycode}</span></td> --%>
<%-- 										<td><span title="${cgInquiry.pk_material}" style="width:190px;text-align: left;display: none;">${cgInquiry.pk_material}</span></td> --%>
<%-- 										<td><span title="${cgInquiry.delivercode}" style="width:190px;text-align: left;display: none;">${cgInquiry.delivercode}</span></td> --%>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		
		<script type="text/javascript">
			function setSupplier(data){
				$("#delivercode").val(data.delivercode);//主键
				$("#delivername").val(data.delivername);//名称
			}
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:700,
					callBack:'setSupplier',
					domId:'delivercode',
					single:false
				});
			});
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$("#sp_code").val(stripscript($("#sp_code").val()));
					$("#sp_name").val(stripscript($("#sp_name").val()));
					$('#queryForm').submit();
				} 
			}
			var trrows = $(".table-body").find('tr');
			if(trrows.length!=0){
				$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$('.search-div').hide();
					$("#sp_code").val(stripscript($("#sp_code").val()));
					$("#sp_name").val(stripscript($("#sp_name").val()));
					$('#queryForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					$('#pk_material').val('');
					$('#sp_code').val('');
					$('#sp_name').val('');
					$('#delivercode').val('');
					$('#delivercode').val('');
					$('#delivername').val('');
				});
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$('.search-div').slideToggle(100);
			 				$("#sp_code").focus();
						}
					},{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							savecgInquiry();
						}
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updatecgInquiry();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deletecgInquiry();
						}
					},{
	                    text: '价格分析',
	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'view')},
	                    icon: {
	                        url: '<%=path%>/image/Button/op_owner.gif',
	                        positn: ['0px','-40px']
	                    },
	                    handler: function(){
                            priceAnalysis();
	                    }
	                },{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			//新增物资supplies
			function savecgInquiry(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="CDJ" />',
					content: '<iframe id="saveCgInquiryFrame" frameborder="0" src="<%=path%>/cginquiry/toSaveCgInquiry.do"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame("saveCgInquiryFrame") && window.document.getElementById("saveCgInquiryFrame").contentWindow.saveCheck()){
										submitFrameForm('saveCgInquiryFrame','saveCgInquiry');
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改物资信息
			function updatecgInquiry(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" />采购询价',
						content: '<iframe id="updateCgInquiryFrame" frameborder="0" src="<%=path%>/cginquiry/toUpdateCginquiry.do?pk_cginquiry='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateCgInquiryFrame')&&window.document.getElementById("updateCgInquiryFrame").contentWindow.saveCheck()){
											submitFrameForm('updateCgInquiryFrame','updateCgInquiry');
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除物资
			function deletecgInquiry(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var vcodes = chkValue.join(",");
						$.ajaxSetup({async:false});
						$.post('<%=path%>/cginquiry/deleteCginquiry.do',{"vcodes":vcodes},function(data){
							var rs = data;
							switch(Number(rs)){
							case -1:
								alerterror('<fmt:message key="delete_fail"/>！');
								break;
							case 1:
								showMessage({
									type: 'success',
									msg: '<fmt:message key="successful_deleted"/>！',
									speed: 3000,
									handler:function(){
										reloadPage();}
									});
								break;
							}
						});
					}));
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};		
				
			}
			
			$('.grid .table-body tr').live('click',function(){
					 $('#pk_puprorderm').val($(this).find('td:eq(0)').find('span').attr('title'));
					 var pk_puprorderm=$(this).find('td:eq(0)').find('span').attr('title');
					 var istate = $(this).find('td:eq(6)').find('span').attr('title');
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
			//---------------------------
			//全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$("#sp_code").val(stripscript($("#sp_code").val()));
				$("#sp_name").val(stripscript($("#sp_name").val()));
				$('#queryForm').submit();
			}
            var priceAnalysis=function(){
                var checkboxList = $('.grid').find('.table-body').find(':checkbox');
                if(checkboxList && checkboxList.filter(':checked').size() == 1){
                    var aim = checkboxList.filter(':checked').eq(0);
                    var chkValue = aim.val();
                    $('body').window({
                        id: 'window_supply',
                        title: '价格分析',
                        content: '<iframe id="priceAnalysisFrame" frameborder="0" src="<%=path%>/cginquiry/priceAnalysis.do?pk_cginquiry='+chkValue+'"></iframe>',
                        width: '750px',
                        height: '400px',
                        draggable: true,
                        isModal: true
                    });
                }else{
                    alerterror('<fmt:message key="please_select_data" />！');
                    return ;
                }
            }
		</script>
	</body>
</html>