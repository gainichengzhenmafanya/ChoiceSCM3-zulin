<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="materials_list" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.divgrid {
				positn: relative;
				overflow: auto;
				border-top: 1px solid #999999;
				border-bottom: 1px solid #999999;
				border-right: 1px solid #999999;
				width: 90%;
				height: 85%;
			}
			
			.divgrid td {
				white-space:nowrap;
				overflow: hidden;
				border-right: 1px solid #999999;
				border-bottom: 1px solid #999999;
				height: 18px;
				line-height: 18px;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<div class="form" style="margin-top: 50px;margin-left: 50px;">
			<form id="updateCgInquiry" method="post" action="<%=path %>/cginquiry/updateCgInquiry.do">
				<input type="hidden" id="pk_cginquiry" name="pk_cginquiry" value="${cginquiry.pk_cginquiry}"/>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label">供应商编码:</div>
					<div class="form-input">
						<input type="hidden" id="delivercode" name="delivercode" class="text" maxlength="50" value="${cginquiry.delivercode}"/>
						<input type="text" style="margin-bottom: 6px;" id="delivercode" name="delivercode" readonly="readonly" class="text" maxlength="50" value="${cginquiry.delivercode}"/>
						<img style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" class="search" onclick="showSupplier()"/>
					</div>
					<div class="form-label">供应商名称:</div>
					<div class="form-input"><input type="text" id="delivername" name="delivername"  class="text" maxlength="50" value="${cginquiry.delivername}"/></div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label"><span class="red">*</span><fmt:message key="supplies_code" />:</div>
					<div class="form-input">
						<input type="hidden" id=pk_material name="pk_material" class="text" value="${cginquiry.pk_material}"/>
						<input type="text" style="margin-bottom: 6px;" id="sp_code" name="sp_code" readonly="readonly" class="text" value="${cginquiry.sp_code}"/>
						<img style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" class="search" onclick="selectmaterial()"/>
					</div>
					<div class="form-label"><span class="red">*</span><fmt:message key="supplies_name" />:</div>
					<div class="form-input"><input type="text" id="sp_name" name="sp_name" readonly="readonly" class="text" value="${cginquiry.sp_name}"/></div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label"><span class="red">*</span>联系人:</div>
					<div class="form-input"><input type="text" id="vsuppliercontact" name="vsuppliercontact" class="text" maxlength="10" value="${cginquiry.vsuppliercontact}"/></div>
					<div class="form-label"><span class="red">*</span>联系电话:</div>
					<div class="form-input"><input type="text" id="vsupplierphone" name="vsupplierphone" class="text" maxlength="20" value="${cginquiry.vsupplierphone}"/></div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label"><span class="red">*</span>询价时间:</div>
					<div class="form-input"><input autocomplete="off" type="text" id="dinquirybdat" name="dinquirybdat" style="text-transform:uppercase;" value="${cginquiry.dinquirybdat}" class="Wdate text"/></div>
					<div class="form-label"><span class="red">*</span>询价结束时间:</div>
					<div class="form-input"><input autocomplete="off" type="text" id="dinquiryedat" name="dinquiryedat" style="text-transform:uppercase;" value="${cginquiry.dinquiryedat}" class="Wdate text"/></div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label"><span class="red">*</span>询价人:</div>
					<div class="form-input"><input type="text" id="vinquirycontact" name="vinquirycontact" class="text" maxlength="10" value="${cginquiry.vinquirycontact}"/></div>
					<div class="form-label"><span class="red">*</span>询价价格:</div>
					<div class="form-input"><input type="text" id="ninquiryprice" name="ninquiryprice" class="text" maxlength="20" value="${cginquiry.ninquiryprice}"/></div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label"><span class="red">*</span>询价类型:</div>
					<div class="form-input">
						<select class="select" id="pk_inquiry" name="pk_inquiry" style="width:133px">
							<option></option>
								<c:forEach var="inquiry" items="${inquiryList}" varStatus="status" >
									<option 
									<c:if test="${inquiry.pk_inquiry == cginquiry.pk_inquiry}"> selected="selected" </c:if>
									id="${inquiry.pk_inquiry}" value="${inquiry.pk_inquiry}">${inquiry.vname}</option>
								</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-line" style="margin-top: 10px;">
					<div class="form-label">备注:</div>
					<div class="form-input">
						<textarea rows="5" cols="50" id="vmemo" name="vmemo">${cginquiry.vmemo}</textarea>
					</div>
				</div>
			</form>
			</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
		        $("#vsuppliercontact").focus();
				$("#dinquirybdat").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dinquiryedat\')}'});
				});
				$("#dinquiryedat").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dinquirybdat\')}'});
				});
				
			});
			var selectmaterial=function() {
				selectSupplyLeftAsst({
	                basePath:'<%=path%>',
	                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
	                height:420,
	                width:700,
	                callBack:'setMaterial',
	                domId:'pk_material',
	                irateflag:1,//取采购单位
	                single:true
	            });
			};
			
			var showSupplier=function() {
			    selectSupplier({
			        basePath: '<%=path%>',
			        title: "请选择供应商",
			        height: 500,
			        width: 720,
			        callBack: 'setSupplier',
			        single: false
			    });
			};
			function setMaterial(data){
				$("#pk_material").val(data.sp_code);//主键
				$("#sp_code").val(data.sp_code);//编码
				$("#sp_name").val(data.sp_name);//名称
			}
			function setSupplier(data){
			    $("#delivercode").val(data.delivercode);
			    $("#delivercode").val(data.delivercode);
			    $("#delivername").val(data.delivername);
			}
			//新增验证
			function saveCheck(){
				var sp_code = $.trim($('#sp_code').val());
				if (sp_code == '' || sp_code == null) {
					alerterror('<fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var sp_name = $.trim($('#sp_name').val());
				if (sp_name == '' || sp_name == null) {
					alerterror('<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var vsuppliercontact = $.trim($('#vsuppliercontact').val());
				if (vsuppliercontact == '' || vsuppliercontact == null) {
					alerterror('联系人<fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var vsupplierphone = $.trim($('#vsupplierphone').val());
				if (vsupplierphone == '' || vsupplierphone == null) {
					alerterror('联系电话<fmt:message key="cannot_be_empty" />！');
					return false;
				}
// 				else if (/^(\\d{3}-|\\d{4}-)?(\\d{8}|\\d{7})$/.test(vsupplierphone) || /^(13|15|17|18)[0-9]{9}$/.test(vsupplierphone)){
// 					alerterror('联系方式输入有误！');
// 					return false;
// 				}
				var vinquirycontact = $.trim($('#vinquirycontact').val());
				if (vinquirycontact == '' || vinquirycontact == null) {
					alerterror('询价人<fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var pk_inquiry = $('#pk_inquiry').val();
				if (pk_inquiry == '' || pk_inquiry == null) {
					alerterror('询价类型<fmt:message key="cannot_be_empty" />！');
					return false;
				}
				var ninquiryprice = $('#ninquiryprice').val();
				if (ninquiryprice == '' || ninquiryprice == null) {
					alerterror('询价价格<fmt:message key="cannot_be_empty" />！');
					return false;
				} else if (isNaN(ninquiryprice)){
					alerterror('询价价格必须为数字！');
					return false;
				} else if (Number(ninquiryprice) < 0){
					alerterror('询价价格不能为负数！');
					return false;
				}
				return true;
			}
		</script>
	</body>
</html>