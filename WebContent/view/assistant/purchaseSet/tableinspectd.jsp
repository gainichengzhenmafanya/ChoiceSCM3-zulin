<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>验货子表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
    <style type="text/css">
    	.linebottom{
    		positn: fixed; 
 		bottom: 17px;
    	}
    	.childgrid{
    		width: 99.8%;	
		border:solid 1px #8DB2E3;
		margin-top:5px;
	}
		</style>
	</head>
	<body>
		<div class="childgrid">
		<div class="grid">
                <div class="table-head" style="width:70%;">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                            <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                            <td><span style="width: 100px;"><fmt:message key="specification" /></span></td>
	                            <td><span style="width: 60px;">批次号</span></td>
	                            <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="purchase_quantity" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="inspection" /><fmt:message key="quantity" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="tax_rate" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="The_difference_quantity_inspection" /></span></td>
	                            <td><span style="width: 60px;"><fmt:message key="difference" /><fmt:message key="amount" /></span></td>
	                            <td><span style="width: 80px;display:none;">合格状态</span></td>
	                            <td><span style="width: 80px;display:none;">不合格原因</span></td>
	                            <td><span style="width: 80px;display:none;">不合格数量</span></td>
	                            <td><span style="width: 200px;"><fmt:message key="remark" /></span></td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body"  style="width:70%;">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                        	<c:forEach var="inspectd" varStatus="step" items="${inspectdList}">
	                            <tr>
	                                <td><span style="width: 100px;">${inspectd.sp_code}</span></td>
	                                <td><span style="width: 140px;">${inspectd.sp_name}</span></td>
	                                <td><span style="width: 100px;">${inspectd.sp_desc}</span></td>
	                                <td><span style="width: 60px;">${inspectd.pcno}</span></td>
	                                <td><span style="width: 60px;">${inspectd.unit3}</span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.npurnum}" pattern="0.00"/></span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.ninsnum}" pattern="0.00"/></span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.nprice}" pattern="0.00"/></span></td>
	                                <td><span style="width: 60px;">${inspectd.taxdes}</span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.nmoney}" pattern="0.00"/></span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.ndiffnum}" pattern="0.00"/></span></td>
	                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.ndiffmoney}" pattern="0.00"/></span></td>
<%-- 	                                <td><span style="width: 80px;">${inspectd.qstate}</span></td> --%>
	                                <td><span style="width:80px;display:none;">
										<c:if test="${inspectd.qstate==''}"></c:if>
										<c:if test="${inspectd.qstate==1}">不合格</c:if>
										<c:if test="${inspectd.qstate==2}">合格</c:if>
									</span></td>
	                                <td><span style="width: 80px;display:none;">${inspectd.vinspectname}</span></td>
	                                <td><span style="width: 80px;text-align: right;display:none;"><fmt:formatNumber value="${inspectd.vinspectcnt}" pattern="0.00"/></span></td>
	                                <td><span style="width: 200px;">${inspectd.vmemo}</span></td>
	                            </tr>
	                    	</c:forEach>
                        </tbody>
                    </table>
                </div>
			</div>
		    <div class="form-line" style="height:40px;">
				<div class="form-label"><fmt:message key="total" /><fmt:message key="supplies" />:</div>
				<div class="form-input">
					<span id="countmaterial">${length}</span>
					<span><fmt:message key="article" /></span>
				</div>
				<div class="form-label"><fmt:message key="total_amount1" />:</div>
				<div class="form-input">
					<span>￥</span>
					<span id="countmoney"><fmt:formatNumber value="${summoney}" pattern="0.00"/></span>
				</div>
			</div>
        </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    	var height = $(parent.document.body).height()*0.5-8;
		$(".childgrid").css('height',height-20);
		//自动实现滚动条
		$(".grid").css('height',height-$(".form-line").height()-10);
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())*0.998+350);
		$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())*0.998+350);
// 		loadGrid();//  自动计算滚动条的js方法		
		changeTh();
		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	})
    </script>
</body>
</html>
