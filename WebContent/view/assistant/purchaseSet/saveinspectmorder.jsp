<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
				.num{
					border-left:0px;
				}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/puprorder/addPuprorder.do" id="savepuprorder" name="savepuprorder" method="post">
			<input type="hidden" id="sta" name="sta" value="${sta}"/>
			<div class="bj_head" style="height: 75px;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="inspection" /><fmt:message key="orders_num" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${inspectm.vbillno}"/>
					</div>
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="document_date" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="dbilldate" id="dbilldate" class="text" readonly="readonly" value="${inspectm.dbilldate}" />
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${inspectm.delivercode}"/>
						<input type="text" name="delivername" style="margin-bottom: 6px;" id="delivername" readonly="readonly" class="text" value="${inspectm.delivername}"/>
						<img id="supplierbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 						<input type="button" id="supplierbutton" name="supplierbutton" value="查询" style="width:30px;height: 20px;"/> -->
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="select1" /><fmt:message key="PuprOrder" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="pk_puprorderList" id="pk_puprorderList" class="text" value="${inspectm.pk_puprorderList}"/>
						<input type="text" name="vpuproedername" style="margin-bottom: 6px;" id="vpuproedername" readonly="readonly" class="text" value="${inspectm.vpuproedername}"/>
						<img id="puproederbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 						<input type="button" id="puproederbutton" name="puproederbutton" value="查询" style="width:30px;height: 20px;"/> -->
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" style="width: 355px;" name="vmemo" id="vmemo" class="text" value="${inspectm.vmemo}"/>
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 15px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 80px;">批次号</span></td>
	                                <td><span style="width: 80px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 120px;"><fmt:message key="orders_code" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="purchase_quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="inspection" /><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="The_difference_quantity_inspection" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="difference" /><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 80px;">合格状态</span></td>
	                                <td><span style="width: 90px;"><fmt:message key="connot_pass_Reason" /></span></td>
	                                <td><span style="width: 90px;"><fmt:message key="connot_pass_cnt" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
								<c:forEach var="puprorderd" items="${listPuprOrderd}" varStatus="status">
									<c:set var="sum_totalamt" value="${sum_totalamt + puprorderd.nmoney}"/>  
									<tr>
										<td><span style="width:15px;text-align:center;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${puprorderd.sp_code}"/></span></td>
										<td><span style="width:100px;"><c:out value="${puprorderd.sp_name}"/></span></td>
										<td><span style="width:80px;"><c:out value="${puprorderd.sp_desc}"/></span></td>
										<td><span style="width:80px;text-align: center;background:#F1F1F1;"></span></td>
										<td><span style="width:80px;"><c:out value="${puprorderd.unit}"/></span></td>
										<td><span style="width:120px;"><c:out value="${puprorderd.vbillno}"/></span></td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${puprorderd.ncnt}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;background:#F1F1F1;"><fmt:formatNumber value="${puprorderd.ncnt}" pattern="0.00"/></span></td>
										<td><span id = "price" style="width:60px;text-align: right;"><fmt:formatNumber value="${puprorderd.sp_price}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${puprorderd.nmoney}" pattern="0.00"/></span></td>
										<td><span style="width:80px;text-align: right;">0</span></td>
										<td><span style="width:60px;text-align: right;">0</span></td>
										<td><span style="width:80px;">
											<select style="width:80px" id="select1" class="select">
												<option value="2">合格</option>
												<option value="1">不合格</option>    
											</select>
										</span></td>
										<td><span style="width:90px;">
											<select  id="select2" class="select" style="width:90px">
												<c:forEach var="inspection" items="${inspectionlist }">
													<option value="${inspection.pk_inspection }"
													<c:if test="${inspectd.vinspectname==inspection.vname}">selected=selected</c:if>
													>${inspection.vname}</option>
												</c:forEach>
											</select>
										</span></td>
										<td><span style="width:90px;text-align: right;background:#F1F1F1;">0</span></td>
										<td><span style="width:100px;"><c:out value="${puprorderd.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${puprorderd.pk_unit}"/></span></td>
										<td style="display: none;"><span><c:out value="${puprorderd.ncheckdiffrate}"/></span></td>
										<td style="display: none;"><span><c:out value="${puprorderd.sp_name}"/></span></td>
										<td style="display: none;"><span><c:out value="${puprorderd.sp_code}"/></span></td>
										<td style="display: none;"><span><c:out value="${puprorderd.pk_puprorder}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${inspectm.creator}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${inspectm.creationtime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="${sum_totalamt}" />
						</div>	
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="modifier" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifier" id="modifier" class="text" readonly="readonly" value="${inspectm.modifier}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="modifedtime" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifedtime" id="modifedtime" readonly="readonly" class="text" value="${inspectm.modifedtime}" />
						</div>
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_pk_supplier" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable_inspect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
			var inspectionurl = '<%=path%>/inspection/findAllInspection.do';
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			var selectrow = null;
			function setSupplier(data){
				//$("#pk_supplier").val(data.entity[0].pk_supplier);//主键
// 				$("#delivercode").val(data.entity[0].vcode);//编码
// 				$("#delivername").val(data.entity[0].vname);//名称
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#vorgname").val(data.entity[0].vname);//名称
				$("#vorgcode").val(data.entity[0].vcode);//编码
			}
			function setPuprOrder(data1,data2){
				var param = {};
				param['pk_puprorders'] = "'"+data1.replace(",","','")+"'";
				param['istate'] = "4";
				$.post("<%=path%>/inspect/queryInspectmPuprorder.do",param,function(re){
					if(re==''){
						$("#pk_puprorderList").val(data1);//主键
						$("#vpuproedername").val(data2);
						selectPuprOrderByPK();
					}else{
						alerterror('所选订单在验货单【'+re+'】中已存在！');
					}
				})
			}
			$(document).ready(function(){
				
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:720,
						callBack:'setSupplier',
						domId:'delivercode',
						isReal:'isReal',
						single:true
					});
				});
				$('#pk_orgbutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				$('#puproederbutton').click(function(){
					if ($('#delivername').val() == null || $('#delivername').val() == '') {
						alerterror('<fmt:message key="please_select_suppliers" />');
						return;
					}
					selectPuprOrder({
						basePath:'<%=path%>',
						title:"选择采购订单",
						height:400,
						width:600,
						callBack:'setPuprOrder',
						delivercode:$("#delivercode").val(),
						istate:4,
						type:1,
						//bdate:$("#dbilldate").val(),
						//edate:$("#dbilldate").val(),
						single:true
					});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 15) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+820));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+820));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "add") {
					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:0,
					colPerRow:24,
					widths:[16,110,110,90,90,90,130,70,70,70,70,90,70,90,100,100,110],
					colStyle:['','',{background:"#F1F1F1"},'',{'text-align':"center",background:"#F1F1F1"},'','',{'text-align':"right",background:"#F1F1F1"},{'text-align':"right",background:"#F1F1F1"},'','','','','','',{'text-align':"right",background:"#F1F1F1"},'',{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[4,8,9,15,16],
					isAdd:false,
					//表格的编辑事件
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),3);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
						function getName(){
							var vname='';
							$.ajaxSetup({ 
								  async: false 
							});
							$.get('<%=path %>/material/findMaterialTop.do?irateflag=0&delivercode='+$('#delivercode').val(),
									{vname:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										vname =  data.vname;
									});
							return vname;
						};
						if(pos == 7){
							rownum.find("td:eq(8) span").text(Number(rownum.find("td:eq(7) span").text()));
						}
						if(pos == 8){
							var prenmoney = $("#nmoney").val();
							var prenrowmoney =  Number($.trim(rownum.find("td:eq(10) span").text()));
							var cgcnt = Number($.trim(rownum.find("td:eq(7) span").text()));
							var yhcnt = $.trim(rownum.find("td:eq(8) span").text());
							if(yhcnt==''){
								yhcnt = Number($.trim(rownum.find("td:eq(8) input").val()));
								rownum.find("td:eq(8) span").text(Number(yhcnt).toFixed(2));
							}
							
							var ncheckdiffrate;
							var param = {};
							param['sp_code'] = $.trim(rownum.find("td:eq(20) span").text());
							$.post('<%=path%>/material/findMaterialByPk.do',param,function(data){
								ncheckdiffrate = Number(data.ncheckdiffrate);
							})
							
							
							if(Number(ncheckdiffrate)!=0){
								var nmincnt = ncheckdiffrate<0?Number(cgcnt*(1+ncheckdiffrate)).toFixed(2):Number(cgcnt).toFixed(2);//验货最小值
								var nmaxcnt = ncheckdiffrate>0?Number(cgcnt*(1+ncheckdiffrate)).toFixed(2):Number(cgcnt).toFixed(2);//验货最大值
								var diffcnt = Number(rownum.find("td:eq(8)").text()) - Number(rownum.find("td:eq(6)").text());//8
								if(Number(yhcnt)<Number(nmincnt) || Number(yhcnt)>Number(nmaxcnt)){
									alerterror("该物资验货差异率为 "+Number(ncheckdiffrate*100)+"%,验货数量只能在"+nmincnt+"和"+nmaxcnt+"之间！");
									rownum.find("td:eq(8) span").text('0.00');
									rownum.find("td:eq(10) span").text('0.00');
									rownum.find("td:eq(11) span").text('0.00');
									rownum.find("td:eq(12) span").text('0.00');
									
									var diffnmoney = prenrowmoney;
									$("#nmoney").val(Number(Number(prenmoney)-Number(diffnmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(rownum,8);
									return;
								}
							}
							var diffcnt = Number(cgcnt)-Number(yhcnt);
							var price = Number($.trim(rownum.find("td:eq(9) span").text()));
							var diffnmoney = Number(Number(yhcnt*price)-Number(prenrowmoney)).toFixed(2);
							
							rownum.find("td:eq(10) span").text(Number(yhcnt*price).toFixed(2));
							rownum.find("td:eq(11) span").text(Number(diffcnt).toFixed(2));
							rownum.find("td:eq(12) span").text(Number(Number(diffcnt)*Number(price)).toFixed(2));
							
							$("#nmoney").val(Number(Number(prenmoney)+Number(diffnmoney)).toFixed(2));
							$.fn.autoGrid.setCellEditable(rownum,15);
						}
						if(pos == 9){
							var pricejiu = Number(rownum.find("td:eq(9)").text());
							var priceorder;
							$.post('<%=path%>/inspect/queryOrderPrice.do?vpuproedername='+$('#vpuproedername').val()+'&sp_code='+rownum.find("td:eq(1)").text(),function(datas){
								priceorder = eval('('+datas+')');
							});
							var selectshisan = rownum.find("td:eq(13) span").find('select').find('option:selected').val();
							if(selectshisan=='2' || selectshisan=='' || selectshisan==null){
								if(pricejiu!=priceorder){
									alerterror('因为是合格状态，单价应该与采购订单的单价相等，当前采购订单的单价是：'+priceorder);
									return;
								}
							}
							if(selectshisan=='1'){
								if(pricejiu>priceorder){
									alerterror('单价不能大于采购订单的单价，当前采购订单的单价是：'+priceorder);
									return;
								}
							}
							
							rownum.find("td:eq(9) span").text(Number(rownum.find("td:eq(9) span").text()));
							var num = Number(rownum.find("td:eq(8)").text());
							if(num != 0){
								var ncheckdiffrate = Number(rownum.find("td:eq(18) span").text());
								var diffcnt = Number(rownum.find("td:eq(8)").text()) - Number(rownum.find("td:eq(7)").text());
								if(ncheckdiffrate > 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) > ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) < 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="up_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								}  else if(ncheckdiffrate < 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) < ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) > 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="dowm_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								}
							}
						}
						if(pos == 10){
							var num = Number(rownum.find("td:eq(9)").text());
							if(num != 0){
								var ncheckdiffrate = Number(rownum.find("td:eq(18) span").text());
								var diffcnt = Number(rownum.find("td:eq(8)").text()) - Number(rownum.find("td:eq(7)").text());
								if(ncheckdiffrate > 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) > ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) < 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="up_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								} else if(ncheckdiffrate < 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) < ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) > 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="dowm_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								}
							}
							var money = rownum.find("td:eq(10) span").text();
							if(isNaN(money)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(10)").find('span').text('0.00');
							}else if(Number(rownum.find("td:eq(10) span").text()) < 0){
								alerterror('<fmt:message key="nmoney_not_less_than_zero"/>0！');
								rownum.find("td:eq(10)").find('span').text('0.00');
							}else{
								if(!isNaN(rownum.find("td:eq(8)").text()) && Number(rownum.find("td:eq(8)").text()) > 0){
									rownum.find("td:eq(10) span").text(Number(rownum.find("td:eq(10) span").text()));
									rownum.find("td:eq(9) span").text((Number(rownum.find("td:eq(10)").text()) / Number(rownum.find("td:eq(8)").text())).toFixed(2));
									rownum.find("td:eq(12) span").text((Number(rownum.find("td:eq(9)").text()) * Number(rownum.find("td:eq(11)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(rownum,12);
							}
						}
						if(pos == 15){
							var num = Number(rownum.find("td:eq(8)").text());
							if(num != 0){
								var ncheckdiffrate = Number(rownum.find("td:eq(18) span").text());
								var diffcnt = Number(rownum.find("td:eq(8)").text()) - Number(rownum.find("td:eq(7)").text());
								if(ncheckdiffrate > 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) > ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) < 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="up_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								}  else if(ncheckdiffrate < 0){
									if((diffcnt / Number(rownum.find("td:eq(7)").text())) < ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(7)").text())) > 0){
										alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="dowm_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
										return;
									}
								}
							}
							var selectshisan = rownum.find("td:eq(13) span").find('select').find('option:selected').val();
							var selectvalue = rownum.find("td:eq(14) span").find('select').find('option:selected').val();
							var num = rownum.find("td:eq(15) span").text();
							if(selectshisan=='2' || selectshisan=='' || selectshisan==null){
								if(selectvalue!=''){
									alerterror('因为状态为合格，请去掉不合格原因！');
									return;
								}
								if(num!='' && num!=0){
									alerterror('因为状态为合格，不合格数量只能为0或空！');
									return;
								}
							}
							if(selectshisan=='1'){
								if(selectvalue==''){
									alerterror('<fmt:message key="please_select"/><fmt:message key="connot_pass_Reason"/>！');
								}
								if(num==''){
									alerterror('<fmt:message key="connot_pass_cnt"/><fmt:message key="cannot_be_empty"/>！');
								}
								if(isNaN(num)){
									alerterror('<fmt:message key="number_be_not_number"/>！');
								}else if(Number(num) == 0){
									alerterror('<fmt:message key="connot_pass_cnt"/><fmt:message key="cannot_be"/>0');
								}else if(Number(num) < 0){
									alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								}
								rownum.find("td:eq(15) span").text(Number(rownum.find("td:eq(15) span").text()));
							}
						}
					},
					//表格的回车事件
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								//当输入框中为空的时候，弹出物资的选择框
								selectrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                pk_supplier:$('#delivercode').val(),
							                irateflag:0,
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,8);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />');
								return;
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?irateflag=0&delivercode='+$('#delivercode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + sp_desc;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span input").val(data2.vname).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text('').css("text-align","center");
							row.find("td:eq(5) span").text(data2.unit3);
							row.find("td:eq(7) span").text('0.00').css("text-align","right");
							row.find("td:eq(8) span").text('0.00').css("text-align","right");
							row.find("td:eq(9) span").text('').css("text-align","right");
							row.find("td:eq(10) span").text('0.00').css("text-align","right");
							row.find("td:eq(11) span").text('0.00').css("text-align","right");
							row.find("td:eq(12) span").text('0.00').css("text-align","right");
							row.find("td:eq(13) span").text('');
							row.find("td:eq(14) span").text('');
							row.find("td:eq(15) span").text('');
							row.find("td:eq(16) span").text(data2.vmemo);
							row.find("td:eq(17) span").text(data2.pk_unit);
							row.find("td:eq(18) span").text(data2.ncheckdiffrate);
							row.find("td:eq(19) span").text(data2.vname);
							row.find("td:eq(20) span").text(data2.sp_code);
							row.find("td:eq(21) span").text('');
							row.find("td:eq(22) span").text('');
							row.find("td:eq(23) span").text('');
							$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do?positincode="+$('#positncode').val(),
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										$.ajax({
											type: "POST",
											url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
											data: "sp_code="+data2.sp_code,
											dataType: "json",
											success:function(data4){
												if(data4.sp_price == 0){
													row.find("td:eq(8) span").text('0.00').css("text-align","right");
												}else{
													row.find("td:eq(8) span").text(data4.sp_price).css("text-align","right");
												}
											}
										})
									}else{
										row.find("td:eq(8) span").text(data3.sp_price).css("text-align","right");
									}
								}
							});
							$.fn.autoGrid.setCellEditable(row,7);
						}
					},{
						index:7,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else {
								row.find("td:eq(7)").find('span').text(Number(row.find("td:eq(7)").find('span').text()).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,8);
							}
						},
						onCellEdit:function(event,data,row){
							if(isNaN(row.find('td:eq(9)').text())){
								row.find("td:eq(9) span").text(0.0);
							}
						}
					},{
						index:8,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(8)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,8);
							}
						},
						onCellEdit:function(event,data,row){
							var prenmoney = $("#nmoney").val();
							var prenrowmoney =  Number($.trim(row.find("td:eq(9) span").text()));
							
							if(isNaN(data.value)){
								alerterrorbreak('<fmt:message key="number_be_not_number"/>！',function(){
									row.find("td:eq(8) span").text('0.00');
									$.fn.autoGrid.setCellEditable(row,8);
									row.find("td:eq(10) span").text('0.00');
									row.find("td:eq(11) span").text('0.00');
									row.find("td:eq(12) span").text('0.00');
									var diffnmoney = prenrowmoney;
									$("#nmoney").val(Number(Number(prenmoney)-Number(diffnmoney)).toFixed(2));
								});
							}else if(Number(data.value)==0){
								alerterrorbreak('<fmt:message key="number_cannot_be_zero"/>！',function(){
									row.find("td:eq(8) span").text('0.00');
									$.fn.autoGrid.setCellEditable(row,8);
									row.find("td:eq(10) span").text('0.00');
									row.find("td:eq(11) span").text('0.00');
									row.find("td:eq(12) span").text('0.00');
									var diffnmoney = prenrowmoney;
									$("#nmoney").val(Number(Number(prenmoney)-Number(diffnmoney)).toFixed(2));
								});
							}else if(Number(data.value)<0){
								alerterrorbreak('<fmt:message key="number_cannot_be_negative"/>！',function(){
									row.find("td:eq(8) span").text('0.00');
									$.fn.autoGrid.setCellEditable(row,8);
									row.find("td:eq(10) span").text('0.00');
									row.find("td:eq(11) span").text('0.00');
									row.find("td:eq(12) span").text('0.00');
									var diffnmoney = prenrowmoney;
									$("#nmoney").val(Number(Number(prenmoney)-Number(diffnmoney)).toFixed(2));
								});
							}else{
								var cgcnt = Number($.trim(row.find("td:eq(7) span").text()));
								var yhcnt = Number(data.value);
								var diffcnt = Number(cgcnt)-Number(yhcnt);
								var price = Number($.trim(row.find("td:eq(9) span").text()));
								var diffnmoney = Number(Number(yhcnt*price)-Number(prenrowmoney)).toFixed(2);
								
								row.find("td:eq(10) span").text(Number(yhcnt*price).toFixed(2));
								row.find("td:eq(11) span").text(Number(diffcnt).toFixed(2));
								row.find("td:eq(12) span").text(Number(Number(diffcnt)*Number(price)).toFixed(2));
								
								$("#nmoney").val(Number(Number(prenmoney)+Number(diffnmoney)).toFixed(2));
							}
						}
					},{
						index:9,
						action:function(row,data){
							var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(10)').text());
							if(isNaN(row.find("td:eq(9)").text())){
								row.find("td:eq(9) span").text(0.0);
								row.find("td:eq(10) span").text(0.0);
							}else{
								row.find("td:eq(9)").find('span').text(Number(row.find("td:eq(9)").find('span').text()));
								row.find("td:eq(10) span").text((Number(row.find("td:eq(8)").text())*Number(row.find("td:eq(9)").text())).toFixed(2));
								row.find("td:eq(12) span").text((Number(row.find("td:eq(11)").text())*Number(row.find("td:eq(9)").text())).toFixed(2));
							}
							Sprice=Sprice+Number(row.find('td:eq(10)').text());//总金额
							$('#sum_price').text(Sprice.toFixed(2));//总金额
							$.fn.autoGrid.setCellEditable(row,10);
						}
					},{
						index:10,
						action:function(row,data2){
							if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(10)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,10);
							}else {
								if(!isNaN(row.find("td:eq(8)").text()) && Number(row.find("td:eq(8)").text()) > 0){
									row.find("td:eq(10)").find('span').text(Number(row.find("td:eq(10)").find('span').text()));
									row.find("td:eq(9) span").text((Number(row.find("td:eq(10)").text()) / Number(row.find("td:eq(8)").text())).toFixed(2));
									row.find("td:eq(12) span").text((Number(row.find("td:eq(9)").text()) * Number(row.find("td:eq(11)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(row,15);
							}
						}
					},{
						index:15,
						action:function(row,data){
							var selectvalue = row.find('select').find('option:selected').val();
							if(selectvalue!=''){
								if(isNaN(data.value)){
									row.find("td:eq(15)").find('span').text(data.ovalue);
									$.fn.autoGrid.setCellEditable(row,15);
								}else if(Number(data.value) < 0){
									row.find("td:eq(15)").find('span').text(data.ovalue);
									$.fn.autoGrid.setCellEditable(row,15);
								}else if(Number(data.value) == 0){
									$.fn.autoGrid.setCellEditable(row,15);
								}else if(data.value == ''){
									$.fn.autoGrid.setCellEditable(row,15);
								}else{
									$.fn.autoGrid.setCellEditable(row,16);
								}
							}
						},
						onCellEdit:function(event,data,row){
							if(isNaN(data.value)){
								row.find("td:eq(15)").find('span').text('');
								$.fn.autoGrid.setCellEditable(row,15);
							}else if(Number(data.value) < 0){
								row.find("td:eq(15)").find('span').text('');
								$.fn.autoGrid.setCellEditable(row,15);
							}
						}
					},{
						index:16,
						action:function(row,data){
							if(row.next().html())$.fn.autoGrid.setCellEditable(row.next(),8);
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function savepuprorder(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var delivercode = $("#delivercode").val();
				var positncode = $("#positncode").val();
				if(delivercode==''){
					alerterror('<fmt:message key="please_select" /><fmt:message key="suppliers" />');
					savelock = false;
					return;
				}
				if(positncode==''){
					alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
					savelock = false;
					return;
				}
				$('.grid').find('.table-body').find('table').find('td').find('input').each(function(){
					$(this).trigger('onEnter');
				});
				
				var checkinfo = true;
				
				var keys = ["sp_code","sp_name","sp_desc","pcno","unit3","vpuprorderbillno","npurnum","ninsnum","nprice","nmoney","ndiffnum","ndiffmoney","qstate","vinspectname","vinspectcnt","vmemo","pk_unit","ncheckdiffrate","sp_name","sp_code","pk_puprorder"];
				var data = {};
				var i = 0;
				data["vbillno"] = $("#vbillno").val();
				data["dbilldate"] = $("#dbilldate").val();
				data["creator"] = $("#creator").val();
				data["creationtime"] = $("#creationtime").val();
				data["delivername"] = $("#delivername").val();
				data["delivercode"] = $("#delivercode").val();
				data["istate"] = $("#istate").val();
				data["nmoney"] = $("#nmoney").val();
				data["vmemo"] = $("#vmemo").val();
 				data["vpuproedername"] = $("#vpuproedername").val();
				var rows = $(".grid .table-body table tr");
				for(i=0;i<rows.length;i++){
					var vcode = $.trim($(rows[i]).find("td:eq(1) span").text());
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					if(vcode == '' || vcode == null){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />!');
						savelock = false;
						checkinfo = false;
						return;
					}else if(vname == '' || vname == null){
						$(rows[i]).find("td:eq(2) span input").val($.trim($(rows[i]).find("td:eq(18) span").text()));
					}else if(name == '' || name == null){
						$(rows[i]).find("td:eq(2) span").text($.trim($(rows[i]).find("td:eq(18) span").text()));
					}else if(vname != $.trim($(rows[i]).find("td:eq(18) span").text())){
						$(rows[i]).find("td:eq(2) span input").val($.trim($(rows[i]).find("td:eq(18) span").text()));
					}else if(name != $.trim($(rows[i]).find("td:eq(18) span").text())){
						$(rows[i]).find("td:eq(2) span").text($.trim($(rows[i]).find("td:eq(18) span").text()));
					}
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					if((vname == '' || vname == null) && (name == '' || name == null)){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
						savelock = false;
						checkinfo = false;
						return;
					}
					
					var ncnt = $.trim($(rows[i]).find("td:eq(8) span").text());
					if(ncnt==''){
						ncnt = $.trim($(rows[i]).find("td:eq(8) span input").val());
					}
					if(Number(ncnt)==0){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="inspection" /><fmt:message key="quantity" /><fmt:message key="cannot_be" />0！');
						savelock = false;
						checkinfo = false;
						return;
					}
					
					var cgcnt = Number($.trim($(rows[i]).find("td:eq(7) span").text()));
					var ncntyh = Number($.trim($(rows[i]).find("td:eq(8) span input").val()));
					var cntyh = Number($.trim($(rows[i]).find("td:eq(8) span").text()));
					var yhcnt = (ncntyh == 0.0?cntyh:ncntyh);
					var dicnt = Number($.trim($(rows[i]).find("td:eq(17) span").text()));
					var diffcnt = yhcnt - cgcnt;
					if(dicnt > 0){
						if((diffcnt / cgcnt) > dicnt || (diffcnt / cgcnt) < 0){
							alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="up_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
							savelock = false;
							checkinfo = false;
							return;
						}
					} else if(dicnt < 0){
						if((diffcnt / cgcnt) < dicnt || (diffcnt / cgcnt) > 0){
							alerterror('<fmt:message key="max_inspection_cnt"/><fmt:message key="dowm_float"/>'+(ncheckdiffrate * Number(rownum.find("td:eq(7)").text()))+'，<fmt:message key="please_enter_inspection_cnt"/>！');
							savelock = false;
							checkinfo = false;
							return;
						}
					}
					
					
					var buhegenum = $(rows[i]).find("td:eq(15) span").text();
					if(buhegenum==''){
						buhegenum = $(rows[i]).find("td:eq(15) input").val();
					}
					var qstates = $(rows[i]).find("td:eq(13) span").find('select').find('option:selected').val();
					var buhegeyuanyin = $(rows[i]).find("td:eq(14) span").find('select').find('option:selected').val();
					if(qstates=='2' || qstates=='' || qstates==null){
						if(buhegeyuanyin!=''){
							alerterror('第'+(i+1)+'行因为是合格状态，请去掉不合格原因！');
							checkinfo = false;
							savelock = false;
							return;
						}
						if(buhegenum!=0 && buhegenum!=''){
							alerterror('第'+(i+1)+'行因为是合格状态，不合格数量应该为0或空！');
							checkinfo = false;
							savelock = false;
							return;
						}
					}
					if(qstates=='1'){
// 						if(buhegenum!='' && Number(buhegenum)>0){
							if(buhegeyuanyin==''){
								alerterror('第'+(i+1)+'行请选择不合格原因！');
								checkinfo = false;
								savelock = false;
								return;
							}
//	 						else{
//	 							if(Number(buhegenum)>Number(cgcnt)){
//	 								alerterror('第'+(i+1)+'行不合格数量不能大于采购数量！');
//	 								checkinfo = false;
//	 								savelock = false;
//	 								return;
//	 							}else if(Number(buhegenum)+Number(yhcnt)>Number(cgcnt)){
//	 								alerterror('第'+(i+1)+'行不合格数量+验货数量不能大于采购数量！');
//	 								checkinfo = false;
//	 								savelock = false;
//	 								return;
//	 							}
//	 						}
// 						}
						if(buhegenum=='' || Number(buhegenum)==0){
							alerterror('第'+(i+1)+'行请输入不合格数量！');
							checkinfo = false;
							savelock = false;
							return;
						}
					}
					//价格判断
					var pricejiu = Number($(rows[i]).find("td:eq(9)").text());
					var priceorder;
					$.post('<%=path%>/inspect/queryOrderPrice.do?vpuproedername='+$('#vpuproedername').val()+'&sp_code='+$(rows[i]).find("td:eq(1)").text(),function(datass){
						priceorder = eval('('+datass+')');
					});
					if(qstates=='2' || qstates=='' || qstates==null){
						if(pricejiu!=priceorder){
							alerterror('第'+(i+1)+'行,因为是非不合格状态，单价应该与采购订单的单价相等，当前采购订单的单价是：'+priceorder);
							checkinfo = false;
							savelock = false;
							return;
						}
					}
					if(qstates=='1'){
						if(pricejiu>priceorder){
							alerterror('第'+(i+1)+'行单价不能大于采购订单的单价，当前采购订单的单价是：'+priceorder);
							checkinfo = false;
							savelock = false;
							return;
						}
					}
					data["inspectdList["+i+"]."+"nprice"] = Number($(rows[i]).find('td:eq(8) span').text());
					var j = 0;
					for(j=1;j <= keys.length;j++){
						if(keys[j]=='qstate'){
							continue;
						}
						if(keys[j]=='vinspectname'){
							continue;
						}
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value)
							data["inspectdList["+i+"]."+keys[j-1]] = value;
					}
					data["inspectdList["+i+"].pk_inspection"] = $(rows[i]).find('select').find('option:selected').val();
					if($(rows[i]).find("td:eq(13) span").find('select').find('option:selected').text()=='不合格'){
						data["inspectdList["+i+"].qstate"] = '1';
					}
					if($(rows[i]).find("td:eq(13) span").find('select').find('option:selected').text()=='合格'){
						data["inspectdList["+i+"].qstate"] = '2';
					}
					data["inspectdList["+i+"].vinspectname"] = $(rows[i]).find("td:eq(14) span").find('select').find('option:selected').text();
				}
				
				if(!checkinfo){
					return;
				}
				
				
				$.ajaxSetup({async:false});
				if(checkvcode()){
					$.post("<%=path%>/inspect/saveInspect.do?sta="+$('#sta').val(),data,function(data){
						var rs = eval('('+data+')');
						switch(Number(rs)){
						case -1:
							alerterror('<fmt:message key="save_fail"/>！');
							savelock = false;
							break;
						case 1:
							showMessage({
										type: 'success',
										msg: '<fmt:message key="save_successful"/>！',
										speed: 3000,
										handler:function(){
											parent.reloadPage();
											parent.$('.close').click();}
										});
							break;
						}
					});
				}else{
					alerterror('<fmt:message key="document_no"/><fmt:message key="duplicate"/>，<fmt:message key="please_check_number_rule"/>！');
				}
			}
			//*****************验证单据号是否重复，校验编码规则是否设置正确**************************************
			function checkvcode(){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/inspect/findInspectByCode.do",{vbillno:$("#vbillno").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}

			function setMaterial(data2){
				var flag = true;
				if(data2.entity[0].pk_material==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data2.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				selectrow.find("td:eq(1) span").text(data2.entity[0].vcode).css("text-align","right");
				selectrow.find("td:eq(2) span input").val(data2.entity[0].vname).focus();
				selectrow.find("td:eq(3) span").text(data2.entity[0].sp_desc);
				selectrow.find("td:eq(4) span").text('').css("text-align","center");
				selectrow.find("td:eq(5) span").text(data2.entity[0].unit3);
				selectrow.find("td:eq(7) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(8) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(9) span").text('').css("text-align","right");
				selectrow.find("td:eq(10) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(11) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(12) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(13) span").text('');
				selectrow.find("td:eq(14) span").text('');
				selectrow.find("td:eq(15) span").text('');
				selectrow.find("td:eq(16) span").text(data2.entity[0].vmemo);
				selectrow.find("td:eq(17) span").text(data2.entity[0].pk_unit);
				selectrow.find("td:eq(18) span").text(data2.entity[0].ncheckdiffrate);
				selectrow.find("td:eq(19) span").text(data2.entity[0].vname);
				selectrow.find("td:eq(20) span").text(data2.entity[0].sp_code);
				selectrow.find("td:eq(21) span").text('');
				selectrow.find("td:eq(22) span").text('');
				selectrow.find("td:eq(23) span").text('');
				$.ajax({//查询物资
					type: "POST",
					url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val(),
					data: "sp_code="+data2.sp_code,
					dataType: "json",
					success:function(data3){
						if(data3.sp_price == 0){
							$.ajax({
								type: "POST",
								url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data4){
									if(data4.sp_price == 0){
										selectrow.find("td:eq(9) span").text('0.00').css("text-align","right");
									}else{
										selectrow.find("td:eq(9) span").text(data4.sp_price).css("text-align","right");
									}
								}
							})
						}else{
							selectrow.find("td:eq(9) span").text(data3.sp_price).css("text-align","right");
						}
					}
				});
				$.fn.autoGrid.setCellEditable(selectrow,8);
			}
			function setInspection(data){
				if(data.entity[0]=='undefined'){
					return;
				}
				selectrow.find("td:eq(14) span").text(data.entity[0].vname);//验货类别名称
				selectrow.find("td:eq(21) span").text(data.entity[0].pk_id);//验货类别主键
				selectrow.find("td:eq(22) span").text(data.entity[0].vcode);//验货类别编码
			}
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			function selectPuprOrderByPK(){
				var pk_puprorder = $("#pk_puprorderList").val();
				var vpuproedername = $("#vpuproedername").val();
				$('#savepuprorder').attr('action','<%=path%>/inspect/toAddInspectmOrder.do?istate=4');
				$('#savepuprorder').submit();
			}
		</script>
	</body>
</html>
