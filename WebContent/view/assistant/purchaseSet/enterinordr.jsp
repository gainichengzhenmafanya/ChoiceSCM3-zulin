<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/puprorder/addPuprorder.do" id="savepuprorder" name="savepuprorder" method="post">
			<input type="hidden" id="sta" name="sta" value="${sta}"/>
			<input type="hidden" id="pk_inspect" name="pk_inspect" value="${inspectm.pk_inspect}"/>
			<div class="bj_head" style="height: 190px;">
				<div class="form-line">
					<div class="form-label" style="display:none;"><fmt:message key="inspection" /><fmt:message key="orders_num" />:</div>
					<div class="form-input" style="width:100px;display:none;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${inspectm.vbillno}"/>
					</div>
					<div class="form-label" style="width: 30%;margin-left: 60px;margin-top:50px;"><span style="color:red;font-weight:bold;font-size:18px;"><fmt:message key="Please_select_a_warehouse_date" />:</span></div>
					<div class="form-input" style="width:100px;margin-top:50px;">
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate" readonly="readonly" onfocus="WdatePicker({minDate:'%y-%M-{%d}'});" style="text-transform:uppercase;" value="${inspectm.dbilldate}" class="Wdate text"/>
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;display:none;"><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;display:none;">
<%-- 						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${inspectm.delivercode}"/> --%>
						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${inspectm.delivercode}"/>
						<input type="text" name="delivername" id="delivername" readonly="readonly" class="text" value="${inspectm.delivername}"/>
					</div>
				</div>
				<div class="form-line" style="display:none;">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vmemo" id="vmemo" class="text" value="${inspectm.vmemo}"/>
					</div>
				</div>
			</div>
				<!--
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 80px;">批次号</span></td>
	                                <td><span style="width: 80px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 130px;"><fmt:message key="orders_code" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="purchase_quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="inspection" /><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="The_difference_quantity_inspection" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="difference" /><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 80px;">合格状态</span></td>
	                                <td><span style="width: 90px;"><fmt:message key="connot_pass_Reason" /></span></td>
	                                <td><span style="width: 90px;"><fmt:message key="connot_pass_cnt" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="inspectd" items="${listInspectd}" varStatus="status">
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${inspectd.sp_code}"/></span></td>
										<td><span style="width:140px;"><c:out value="${inspectd.sp_name}"/></span></td>
										<td><span style="width:80px;"><c:out value="${inspectd.sp_desc}"/></span></td>
										<td><span style="width:80px;"><c:out value="${inspectd.pcno}"/></span></td>
										<td><span style="width:80px;"><c:out value="${inspectd.unit3}"/></span></td>
										<td><span style="width:130px;"><c:out value="${inspectd.vpuprorderbillno}"/></span></td>
										<td><span style="width:60px;text-align: center;"><fmt:formatNumber value="${inspectd.npurnum}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: center;"><fmt:formatNumber value="${inspectd.ninsnum}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: center;"><input type="hidden" value="${inspectd.nprice}"/><fmt:formatNumber value="${inspectd.nprice}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: center;"><fmt:formatNumber value="${inspectd.nmoney}" pattern="0.00"/></span></td>
										<td><span style="width:80px;text-align: center;"><fmt:formatNumber value="${inspectd.ndiffnum}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: center;"><fmt:formatNumber value="${inspectd.ndiffmoney}" pattern="0.00"/></span></td>
										<td><span style="width:80px;">	
											<select id="qstate" name="qstate" style="width:80px" class="select" disabled="disabled">
												<option value=""<c:if test="${inspectd.qstate==1}">selected=selected</c:if>></option>
												<option value="1"<c:if test="${inspectd.qstate==1}">selected=selected</c:if>>不合格</option>
												<option value="2"<c:if test="${inspectd.qstate==2}">selected=selected</c:if>>合格</option>
											</select>
										</span></td>
										<td><span style="width:90px;">
											<select class="select" style="width:90px;" disabled="disabled">
												<c:forEach var="inspection" items="${inspectionlist }">
													<option value="${inspection.pk_inspection }"
													<c:if test="${inspectd.vinspectname==inspection.vname}">selected=selected</c:if>
													>${inspection.vname}</option>
												</c:forEach>
											</select>
										</span></td>	
										<td><span style="width:90px;"><c:out value="${inspectd.vinspectcnt}"/></span></td>
										<td><span style="width:100px;"><c:out value="${inspectd.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.pk_unit}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.ncheckdiffrate}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.sp_name}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.sp_code}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.pk_inspection}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.vinspectcode}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.pk_puprorder}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.tax}"/></span></td>
										<td style="display: none;"><span><c:out value="${inspectd.taxdes}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				-->
				<!--
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${inspectm.creator}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${inspectm.creationtime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="${inspectm.nmoney}" />
						</div>	
					</div>
					<div class="form-line">
						<div class="form-label"><fmt:message key="modifier" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifier" id="modifier" class="text" readonly="readonly" value="${inspectm.modifier}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="modifedtime" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="modifedtime" id="modifedtime" readonly="readonly" class="text" value="${inspectm.modifedtime}" />
						</div>
					</div>
				</div>
				-->
			</form>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
<%-- 		var selectrow = null;
			function setSupplier(data){
// 				$("#delivercode").val(data.entity[0].delivercode);//主键
				$("#delivercode").val(data.entity[0].vcode);//编码
				$("#delivername").val(data.entity[0].vname);//名称
			}
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			function setPuprOrder(data1,data2){
				$("#pk_puprorderList").val(data1);//主键
				$("#vpuproedername").val(data2);
				selectPuprOrderByPK();
			}
			$(document).ready(function(){
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				$('#puproederbutton').click(function(){
					if($("#delivercode").val() == null || $("#delivercode").val() == ""){
						alerterror('<fmt:message key="please_select_suppliers" />！');
						return;
					}
					selectPuprOrder({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPuprOrder',
						delivercode:$("#delivercode").val(),
						type:1,
						single:true
					});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 15) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		});  
// 				$("#dbilldate").click(function(){
// 					new WdatePicker();
// 				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+840));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+840));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "add") {
// 					editCells();
				}
			}); --%>
<%-- 		//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:21,
					widths:[16,110,150,90,90,90,130,70,70,70,70,90,70,90,100,100,110],
					colStyle:['','',{background:"#F1F1F1"},'','','','','','','','','','','','','','',{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,4,6,7,8,9,12,14,15,16],
					//表格的编辑事件
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						if(pos == 2){
							if($.trim(data.curobj.closest('td').prev().text())){
								data.curobj.find('span').html(getName());
								return;
							}
						 	else if(!data.actionobj){
								$.fn.autoGrid.setCellEditable(data.curobj.find('span').closest('tr'),3);
								return;
							} 
						}
						$.trim(data.value) ? data.curobj.find('span').html(data.value) : data.curobj.find('span').html(data.value) ;
						function getName(){
							var vname='';
							$.ajaxSetup({ 
								  async: false 
							});
							$.get('<%=path %>/material/findMaterialTop.do?irateflag=0&delivercode='+$('#delivercode').val(),
									{vname:$.trim(data.curobj.closest('td').prev().text())},
									function(data){
										vname =  data.vname;
									});
							return vname;
						};
						if(pos == 7){
							var num = Number(rownum.find("td:eq(7)").text());
							if(num != 0){
								var ncheckdiffrate = Number(rownum.find("td:eq(17) span").text());
								var diffcnt = Number(rownum.find("td:eq(7)").text()) - Number(rownum.find("td:eq(6)").text());
								if(ncheckdiffrate > 0){
									if((diffcnt / Number(rownum.find("td:eq(6)").text())) > ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(6)").text())) < 0){
										alerterror('验货最大差异量为向上浮动'+(ncheckdiffrate * Number(rownum.find("td:eq(6)").text()))+'，请确认验货数量！');
										return;
									}
								} else if(ncheckdiffrate == 0.0){
									if(diffcnt != 0.0){
										alerterror('验货最大差异量为'+(ncheckdiffrate * Number(rownum.find("td:eq(6)").text()))+'，请确认验货数量！！');
										return;
									}
								} else if(ncheckdiffrate < 0){
									if((diffcnt / Number(rownum.find("td:eq(6)").text())) < ncheckdiffrate ||
											(diffcnt / Number(rownum.find("td:eq(6)").text())) > 0){
										alerterror('验货最大差异量为向下浮动'+(ncheckdiffrate * Number(rownum.find("td:eq(6)").text()))+'，请确认验货数量！！');
										return;
									}
								}
							}
						}
						if(pos == 9){
							var money = rownum.find("td:eq(9) span").text();
							if(isNaN(money)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(9)").find('span').text('0.00');
							}else if(Number(rownum.find("td:eq(9) span").text()) < 0){
								alerterror('验货金额不能小于0！');
								rownum.find("td:eq(9)").find('span').text('0.00');
							}else{
								if(!isNaN(rownum.find("td:eq(7)").text()) && Number(rownum.find("td:eq(7)").text()) > 0){
									rownum.find("td:eq(8) span").text((Number(rownum.find("td:eq(9)").text()) / Number(rownum.find("td:eq(7)").text())).toFixed(2));
									rownum.find("td:eq(11) span").text((Number(rownum.find("td:eq(8)").text()) * Number(rownum.find("td:eq(10)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(rownum,12);
							}
						}
					},
					//表格的回车事件
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								//当输入框中为空的时候，弹出物资的选择框
								selectrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                delivercode:$('#delivercode').val(),
							                irateflag:0,
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,6);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />');
								return;
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?irateflag=0&delivercode='+$('#delivercode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + sp_desc;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span input").val(data2.vname).focus();
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.pcno);
							row.find("td:eq(5) span").text(data2.unit3);
							row.find("td:eq(6) span").text('0.00').css("text-align","right");
							row.find("td:eq(7) span").text('0.00').css("text-align","right");
							row.find("td:eq(8) span").text('').css("text-align","right");
							row.find("td:eq(9) span").text('0.00').css("text-align","right");
							row.find("td:eq(10) span").text('0.00').css("text-align","right");
							row.find("td:eq(11) span").text('0.00').css("text-align","right");
							row.find("td:eq(12) span").text('');
							row.find("td:eq(13) span").text('');
							row.find("td:eq(14) span").text('');
							row.find("td:eq(15) span").text(data2.vmemo);
							row.find("td:eq(16) span").text(data2.pk_unit);
							row.find("td:eq(17) span").text(data2.ncheckdiffrate);
							row.find("td:eq(18) span").text(data2.vname);
							row.find("td:eq(19) span").text(data2.sp_code);
							row.find("td:eq(20) span").text('');
							row.find("td:eq(21) span").text('');
							row.find("td:eq(22) span").text('');
							$.ajax({//查询物资 
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do",
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										row.find("td:eq(8) span").text('0.00').css("text-align","right");
									}else{
										row.find("td:eq(8) span").text(data3.sp_price).css("text-align","right");
									}
								}
							});
						}
					},{
						index:6,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(6)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,6);
							}else {
								$.fn.autoGrid.setCellEditable(row,7);
							}
						},
						onCellEdit:function(event,data,row){
							if(isNaN(row.find('td:eq(8)').text())){
								row.find("td:eq(8) span").text(0.0);
							}
						}
					},{
						index:7,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(Number(data2.value) < 0){
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(7)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,7);
							}else {
								var num = Number(row.find("td:eq(7)").text());
								if(num != 0){
									var ncheckdiffrate = Number(row.find("td:eq(17) span").text());
									var diffcnt = Number(row.find("td:eq(7)").text()) - Number(row.find("td:eq(6)").text());
									if(ncheckdiffrate > 0){
										if((diffcnt / Number(row.find("td:eq(6)").text())) > ncheckdiffrate ||
												(diffcnt / Number(row.find("td:eq(6)").text())) < 0){
											$.fn.autoGrid.setCellEditable(row,7);
											return;
										}
									} else if(ncheckdiffrate == 0.0){
										if(diffcnt != 0.0){
											$.fn.autoGrid.setCellEditable(row,7);
											return;
										}
									} else if(ncheckdiffrate < 0){
										if((diffcnt / Number(row.find("td:eq(6)").text())) < ncheckdiffrate ||
												(diffcnt / Number(row.find("td:eq(6)").text())) > 0){
											$.fn.autoGrid.setCellEditable(row,7);
											return;
										}
									}
								}
								var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(9)').text());
								if(isNaN(row.find("td:eq(8)").text())){
									row.find("td:eq(9) span").text(0.0);
									row.find("td:eq(10) span").text(diffcnt);
									row.find("td:eq(11) span").text(0.0);
								} else {
									row.find("td:eq(9) span").text((Number(row.find("td:eq(7)").text())*Number(row.find("td:eq(8)").text())).toFixed(2));
									row.find("td:eq(10) span").text(diffcnt);
									row.find("td:eq(11) span").text((Number(row.find("td:eq(10)").text())*Number(row.find("td:eq(8)").text())).toFixed(2));
								}
								Sprice=Sprice+Number(row.find('td:eq(9)').text());//总金额
								$('#sum_price').text(Sprice.toFixed(2));//总金额
								$.fn.autoGrid.setCellEditable(row,8);
							}
						}
					},{
						index:8,
						action:function(row,data){
							var Sprice=Number($('#sum_price').text())-Number(row.find('td:eq(9)').text());
							if(isNaN(row.find("td:eq(8)").text())){
								row.find("td:eq(8) span").text(0.0);
								row.find("td:eq(9) span").text(0.0);
							}else{
								row.find("td:eq(9) span").text((Number(row.find("td:eq(7)").text())*Number(row.find("td:eq(8)").text())).toFixed(2));
								row.find("td:eq(11) span").text((Number(row.find("td:eq(10)").text())*Number(row.find("td:eq(8)").text())).toFixed(2));
							}
							Sprice=Sprice+Number(row.find('td:eq(9)').text());//总金额
							$('#sum_price').text(Sprice.toFixed(2));//总金额
							$.fn.autoGrid.setCellEditable(row,9);
						}
					},{
						index:9,
						action:function(row,data2){
							if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(9)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,9);
							}else {
								if(!isNaN(row.find("td:eq(7)").text()) && Number(row.find("td:eq(7)").text()) > 0){
									row.find("td:eq(8) span").text((Number(row.find("td:eq(9)").text()) / Number(row.find("td:eq(7)").text())).toFixed(2));
									row.find("td:eq(11) span").text((Number(row.find("td:eq(8)").text()) * Number(row.find("td:eq(10)").text())).toFixed(2));
								}
								$.fn.autoGrid.setCellEditable(row,12);
							}
						}
					},{
						index:12,
						action:function(row,data){
							$.fn.autoGrid.setCellEditable(row,14);
						}
					},{
						index:14,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								selectrow = row;
								chooseInspection({
					                basePath:'<%=path%>',
					                title:'<fmt:message key="please_enter" />验货类别',
					                height:420,
					                width:650,
					                callBack:'setInspection',
					                domId:'domId',
					                single:true
					            });
							$.fn.autoGrid.setCellEditable(row,15);
							}
						}
					},{
						index:15,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			} --%>
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			//只能点击一次
			var savelock = false;
			//验收入库
			function enterInOrdr(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				
/* 				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var pk_material=$.trim($(this).find("td:eq(19) span").text());
					if(pk_material==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
					}
				});
				
				$('.grid').find('.table-body').find('table').find('td').find('input').each(function(){
					$(this).trigger('onEnter');
				});
				
				var keys = ["sp_code","sp_name","sp_desc","pcno","unit3","vpuprorderbillno","npurnum","ninsnum","nprice","nmoney","ndiffnum","ndiffmoney","qstate","vinspectname","vinspectcnt","vmemo","pk_unit","ncheckdiffrate","sp_name","sp_code","pk_inspection","vinspectcode","pk_puprorder","tax","taxdes"];
				 */
				var data = {};
				var i = 0;
				data["pk_inspect"] = $("#pk_inspect").val();
				data["dbilldate"] = $("#dbilldate").val();
/* 				data["vbillno"] = $("#vbillno").val();
				data["creator"] = $("#creator").val();
				data["creationtime"] = $("#creationtime").val();
				data["delivername"] = $("#delivername").val();
				data["delivercode"] = $("#delivercode").val();
				data["pk_puprorderList"] = $("#pk_puprorderList").val();
				data["istate"] = $("#istate").val();
				data["modifier"] = $("#modifier").val();
				data["modifedtime"] = $("#modifedtime").val();
				data["vmemo"] = $("#vmemo").val(); */
				/**
				var rows = $(".grid .table-body table tr");
				for(i=0;i<rows.length;i++){
					var vcode = $.trim($(rows[i]).find("td:eq(1) span").text());
					if(vcode == '' || vcode == null){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_code" /><fmt:message key="cannot_be_empty" />!');
						savelock = false;
						return;
					}
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					var ncnt = Number($.trim($(rows[i]).find("td:eq(7) span input").val()));
					var cnt = Number($.trim($(rows[i]).find("td:eq(7) span").text()));
					ncnt == 0.0?cnt:ncnt;
					if((vname == '' || vname == null) && (name == '' || name == null)){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！');
						savelock = false;
						return;
					}
					if(ncnt == 0.0 && cnt == 0.0){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="number_cannot_be_zero" />！');
						savelock = false;
						return;
					}
					var status=$(rows[i]).find('td:eq(0)').text();
					data["inspectdList["+i+"]."+"nprice"] = $(rows[i]).data('nprice') ? $(rows[i]).data('nprice'):$('#nprice').val();
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						if(keys[j]=='qstate'){
							continue;
						}
						if(keys[j]=='vinspectname'){
							continue;
						}
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value)
							data["inspectdList["+i+"]."+keys[j-1]] = value;
					}
					data["inspectdList["+i+"].pk_inspection"] = $(rows[i]).find('select').find('option:selected').val();
					if($(rows[i]).find("td:eq(13) span").find('select').find('option:selected').text()=='不合格'){
						data["inspectdList["+i+"].qstate"] = '1';
					}
					if($(rows[i]).find("td:eq(13) span").find('select').find('option:selected').text()=='合格'){
						data["inspectdList["+i+"].qstate"] = '2';
					}
					data["inspectdList["+i+"].vinspectname"] = $(rows[i]).find("td:eq(14) span").find('select').find('option:selected').text();
				}
				**/
				$.ajaxSetup({async:false});
				$.post("<%=path%>/inspect/enterInOrdr.do?sta="+$('#sta').val(),data,function(data){
					data=eval('('+data+')').pr;
					if(data=="-3"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="can_not_find_the_corresponding_materials"/>!<fmt:message key="repeat_return_record"/>',
							speed: 3000
						});	
						return;
					}else if(data=="-1"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="data_in_material_code_is_not_correct"/>,<fmt:message key="please_check"/>？',
							speed: 3000
						});	
						return;
					}else if(data=="0"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="te_data_have_been_audited"/>,<fmt:message key="no_need_to_re_submit"/>！',
							speed: 3000
						});	
						return;
					}else if(data=="-9"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="can_only_operate_the_accounting_year_of_the_bill"/>!',
							speed: 6000
						});	
						return;
					}else if(data=="-4"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="cannot"/><fmt:message key="operation"/><fmt:message key="MonthClose_from"/><fmt:message key="accounting-period"/><fmt:message key="misboh_DeclareGoodsGuide_orderTimeError1"/><fmt:message key="de"/><fmt:message key="data"/>!',
							speed: 3000
						});
						return;
					}else if(data=="-11"){
						showMessage({
							type: 'error',
							msg: '<fmt:message key="In_orders"/><fmt:message key="create_failer"/>!',
							speed: 3000
						});
						return;
					}
					else{
						showMessage({
							type: 'success',
							msg: '<fmt:message key="successful"/><fmt:message key="storage"/>！',
							speed: 3000,
							handler:function(){
								parent.reloadPage();
								parent.$('.close').click();}
							});
					};
				});	
			}
<%-- 		function selectPuprOrderByPK(){
				var pk_puprorder = $("#pk_puprorderList").val();
				var vpuproedername = $("#vpuproedername").val();
				$('#savepuprorder').attr('action','<%=path%>/inspect/toAddInspectmOrder.do');
				$('#savepuprorder').submit();
			}
			function setMaterial(data2){
				var flag = true;
				if(data2.entity[0]=='undefined'){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data2.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				selectrow.find("td:eq(1) span").text(data2.entity[0].vcode).css("text-align","right");
				selectrow.find("td:eq(2) span input").val(data2.entity[0].vname).focus();
				selectrow.find("td:eq(3) span").text(data2.entity[0].sp_desc);
				selectrow.find("td:eq(4) span").text(data2.entity[0].pcno);
				selectrow.find("td:eq(5) span").text(data2.entity[0].unit3);
				selectrow.find("td:eq(6) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(7) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(8) span").text('').css("text-align","right");
				selectrow.find("td:eq(9) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(10) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(11) span").text('0.00').css("text-align","right");
				selectrow.find("td:eq(12) span").text('');
				selectrow.find("td:eq(13) span").text('');
				selectrow.find("td:eq(14) span").text('');
				selectrow.find("td:eq(15) span").text(data2.entity[0].vmemo);
				selectrow.find("td:eq(16) span").text(data2.entity[0].pk_unit);
				selectrow.find("td:eq(17) span").text(data2.entity[0].ncheckdiffrate);
				selectrow.find("td:eq(18) span").text(data2.entity[0].vname);
				selectrow.find("td:eq(19) span").text(data2.entity[0].sp_code);
				selectrow.find("td:eq(20) span").text('');
				selectrow.find("td:eq(21) span").text('');
				selectrow.find("td:eq(22) span").text('');
				$.ajax({//查询物资 
					type: "POST",
					url: "<%=path%>/material/findMaterialNprice.do",
					data: "sp_code="+data2.sp_code,
					dataType: "json",
					success:function(data3){
						if(data3.sp_price == 0){
                            selectrow.find("td:eq(8) span").text('0.00').css("text-align","right");
						}else{
                            selectrow.find("td:eq(8) span").text(data3.sp_price).css("text-align","right");
						}
					}
				});
			}
			function setInspection(data){
				if(data.entity[0]=='undefined'){
					return;
				}
				selectrow.find("td:eq(12) span").text(data.entity[0].vname);//验货类别名称
				selectrow.find("td:eq(20) span").text(data.entity[0].pk_id);//验货类别主键
				selectrow.find("td:eq(21) span").text(data.entity[0].vcode);//验货类别编码
			}
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			}; --%>
		</script>
	</body>
</html>
