<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>验货主表</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div style="height:50%;">
	    	<div id="tool"></div>
	    	<div>
		    	<input type="hidden" id="pk_inspect" name="pk_inspect" value="${pk_inspect}"/>
		    	<form id="queryForm" action="<%=path%>/inspect/queryAllInspectm.do" method="post">
		    		<div style="height: 50px;border: solid 1px gray;">
						<div class="form-line">
							<div class="form-label"><fmt:message key="startdate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="bdate" name="bdate" style="text-transform:uppercase;" value="${inspectm.bdate}" onkeyup="ajaxSearch()" class="Wdate text"/>
							</div>
							<div class="form-label" ><fmt:message key="orders_code" />:</div>
							<div class="form-input" >
								<input type="text" name="vbillno" id="vbillno" class="text" value="${inspectm.vbillno}"/>
							</div>
							<div class="form-label"><fmt:message key="suppliers" />:</div>
							<div class="form-input">
							<input type="hidden" name="delivercode" id="delivercode" class="text" value="${inspectm.delivercode}"/>
							<input type="text" name="delivername" id="delivername" style="margin-bottom: 6px;" class="text" value="${inspectm.delivername}"/>
							<img id="supplierbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 								<input type="button" id="supplierbutton" style="width: 50px;" value="查询"/> -->
							</div>
						</div>
						<div class="form-line">
							<div class="form-label"><fmt:message key="enddate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${inspectm.edate}" onkeyup="ajaxSearch()" class="Wdate text"/>
							</div>
							<div class="form-label" ><fmt:message key="Document_status" />:</div>
							<div class="form-input">
								<select id="istate" name="istate" style="width:133px" onkeyup="ajaxSearch()" class="select">
									<option value="0"><fmt:message key="all" /></option>
									<option value="2"<c:if test="${inspectm.istate==2}">selected=selected</c:if>><fmt:message key="Not_in_storage" /></option>
									<!-- <option value="1"<c:if test="${inspectm.istate==1}">selected=selected</c:if>><fmt:message key="Have_the_inspection" /></option> -->
									<option value="4"<c:if test="${inspectm.istate==4}">selected=selected</c:if>><fmt:message key="The_storage" /></option>
									<option value="3"<c:if test="${inspectm.istate==3}">selected=selected</c:if>><fmt:message key="Settled" /></option>
								</select>
							</div>
						</div>
					</div>
					<div class="grid" class="grid" style="overflow: auto;positn:static;">
						<div class="table-head" style="width:95%;">
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td style="width:32px;"></td>
										<td style="width:29px;"><input type="checkbox" id="chkAll"/></td>
										<td style="width:140px;"><fmt:message key="orders_code" /></td>
										<td style="width:110px;"><fmt:message key="storage" /><fmt:message key="document_number" /></td>
										<!-- <td style="width:80px;"><fmt:message key="storage" /><fmt:message key="orders_num" /></td> -->
										<td style="width:160px;"><fmt:message key="suppliers" /></td>
										<td style="width:80px;"><fmt:message key="total_amount1" /></td>
										<td style="width:90px;"><fmt:message key="document_date" /></td>
										<td style="width:70px;"><fmt:message key="Document_status" /></td>
										<td style="width:150px;"><fmt:message key="last_time" /></td>
										<td style="width:170px;"><fmt:message key="summary" /></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="inspectd" varStatus="step" items="${inspectmList}">
										<tr>
											<td class="num"><span style="width:22px;">${step.count}</span></td>
											<td style="width:29px; text-align: center;">
												<span style="width:19px;"><input type="checkbox" name="idList" id="chk_${inspectd.pk_inspect}" value="${inspectd.pk_inspect}"/></span>
											</td>
											<td><span title="${inspectd.vbillno}" style="width:130px;text-align: left;">${inspectd.vbillno}</span></td>
											<td><span title="${inspectd.vouno}" style="width:100px;text-align: left;">${inspectd.vouno}</span></td>
											<!-- <td><span title="${inspectd.chkno}" style="width:70px;text-align: left;">${inspectd.chkno==0?"":inspectd.chkno}</span></td> -->
											<td><span title="${inspectd.delivername}" style="width:150px;text-align: left;">${inspectd.delivername}</span></td>
											<td><span title="${inspectd.nmoney}" style="width:70px;text-align: right;"><fmt:formatNumber value="${inspectd.nmoney}" pattern="0.00"/></span></td>
											<td><span title="${inspectd.dbilldate}" style="width:80px;text-align: center;">${inspectd.dbilldate}</span></td>
											<td><span style="width:60px;text-align: center;">
												<c:if test="${inspectd.istate==1}"><fmt:message key="Have_the_inspection" /></c:if>
												<c:if test="${inspectd.istate==2}"><fmt:message key="Not_in_storage" /></c:if>
												<c:if test="${inspectd.istate==3}"><fmt:message key="Settled" /></c:if>
												<c:if test="${inspectd.istate==4}"><fmt:message key="The_storage" /></c:if>
											</span></td>
											<td><span title="${inspectd.ts}" style="width:140px;text-align: center;">${inspectd.ts}</span></td>
											<td><span title="${inspectd.vmemo}" style="width:160px;text-align: left;">${inspectd.vmemo}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<page:page form="queryForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
				</form>
			</div>
		</div>
		<form id="printForm" action="<%=path%>/inspect/printInspectbill.do" method="post">
				<input type="hidden" id="printpk" name="pk_inspect" value="${pk_inspect}"/>
		</form>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/inspect/queryAllInspectd.do?pk_inspect=${pk_inspect}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
        $("#bdate").focus();
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:800,
					isReal:'isReal',
					callBack:'setSupplier',
					single:false
				});
			});
			function setSupplier(data){
// 				$("#delivercode").val(data.entity[0].vcode);//主键
// 				$("#vsupplierna").val(data.entity[0].vcode);//编码
// 				$("#delivername").val(data.entity[0].vname);//名称
				$("#delivercode").val(data.delivercode);//主键
// 				$("#vsupplierna").val(data.entity[0].vcode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$("#vbillno").val(stripscript($("#vbillno").val()));
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
				//快捷键操作（Alt+*）
				$(document).keydown(function (e) {
				    var doPrevent;
				    if (event.keyCode==78 && event.altKey) {//alt+N
				    	if(!$('#window_supply').html()){
				    		savepuprorderFromInvoice();
				    	}
				    }else if(event.keyCode==77 && event.altKey){//alt+M
				    	if(!$('#window_supply').html()){
				    		savepuprorder();
				    	}
				    }else if(event.keyCode==85 && event.altKey){//alt+U
				    	if(!$('#window_supply').html()){
				    		updateInspectm();
				    	}
				    }else{
				        doPrevent = false;
				    }
				    if (doPrevent)
				        e.preventDefault();
				});
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$("#vbillno").val(stripscript($("#vbillno").val()));
					$('#queryForm').submit();
					parent.$("#mainFrame").submit();
				});
				$("#bdate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});
				});
				$("#edate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});
				});
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),120);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
// 				$(".page").css("margin-bottom",$("body").height()*0.45+$(".page").height());
				$(".page").css("margin-bottom",$(".mainFrame").height());
				$('.grid .table-body tr').live('click',function(){
					 $('#pk_inspect').val($(this).find('td:eq(1)').find('input').val());
					 var pk_inspect=$(this).find('td:eq(1)').find('input').val();
					 $("#printpk").val(pk_inspect);
					 var  url="<%=path%>/inspect/queryAllInspectd.do?pk_inspect="+pk_inspect;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							$("#vbillno").val(stripscript($("#vbillno").val()));
							$('#queryForm').submit();
						}
					}
					/*,{
						text: '<fmt:message key="ORDR" /><fmt:message key="to" /><fmt:message key="The_inspection_sheet" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							savepuprorder();
						}
						,
						items:[{
								text: '<fmt:message key="Self_add" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['0px','0px']
								},
								handler: function(){
									saveInspectm();
								}
							},{
								text: '<fmt:message key="from" /><fmt:message key="fahuodan" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['0px','0px']
								},
								handler: function(){
									savepuprorderFromInvoice();
								}
							},{
								text: '<fmt:message key="from" /><fmt:message key="PuprOrder" />',
								useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['0px','0px']
								},
								handler: function(){
									savepuprorder();
								}
							}]
					}*/,{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updateInspectm();
						}
					}
					/*,{
						text: '<fmt:message key="confirm" /><fmt:message key="inspection" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'confirm')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							enterInspectm();
						}
					}
					*/,{
						text: '<fmt:message key="acceptance_to_storage" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'confirm')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							enterInOrdr();
						}
					}
					/*,{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deleteInspectm();
						
						}
					}
					*/,{
						text: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							printinspectbill();
						}
					},{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			//新增验货单=====来自采购订单
			function savepuprorder(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="The_inspection_sheet" />来自采购订单',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/inspect/toAddInspectmOrder.do?sta=add&istate=0"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增验货单来自发货单
			function savepuprorderFromInvoice(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="The_inspection_sheet" /><fmt:message key="from" /><fmt:message key="fahuodan" />',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/inspect/toAddInspectmOrderFromInvoice.do?sta=add&istate=0"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增验货单
			function saveInspectm(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="The_inspection_sheet" />',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/inspect/toAddInspectm.do?sta=add"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//修改验货单
			function updateInspectm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var stat = '';
					checkboxList.filter(':checked').each(function(){
						stat = $.trim($(this).closest('tr').find('td').eq(7).find('span').text());
					});
					if('<fmt:message key="Not_in_storage" />' != stat){
						alerterror('<fmt:message key="The_inspection_sheet" />'+stat+'，<fmt:message key="dont_update" />！');
						return;
					}
					 //后台得到是否已经审核过
					 var htstate = getInspectmIstate(chkValue);
					 if(htstate!="2"){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					 }
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="The_inspection_sheet" />',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/inspect/toUpdateInspectm.do?sta=add&pk_inspect='+chkValue+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepuprorder").contentWindow.updateInspectm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//确认验货单
			function enterInspectm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var stat = '';
					checkboxList.filter(':checked').each(function(){
						stat = $.trim($(this).closest('tr').find('td').eq(6).find('span').text());
					});
					if('已验货' == stat){
						alerterror('<fmt:message key="enter_inspection" />，<fmt:message key="connotenter" />！');
						return;
					}
					if('已入库' == stat){
						alerterror('此单据已入库，<fmt:message key="connotenter" />！');
						return;
					}
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="confirm" /><fmt:message key="inspection" />',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/inspect/toEnterInspectm.do?sta=enter&pk_inspect='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="enter" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepuprorder").contentWindow.enterInspectm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_bill_to_enter" />！');
					return ;
				}
			}
			//确认入库单
			function enterInOrdr(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var stat = '';
					checkboxList.filter(':checked').each(function(){
						stat = $.trim($(this).closest('tr').find('td').eq(7).find('span').text());
					});
					//if('未验货' == stat){
					//	alerterror('此单据未验货，请先验货再入库！');
					//	return;
					//}
					if('<fmt:message key="already_in_storage" />' == stat){
						alerterror('此单据<fmt:message key="already_in_storage" />！');
						return;
					}
					if('<fmt:message key="Settled" />' == stat){
						alerterror('此单据<fmt:message key="Settled" />！');
						return;
					}
					 //后台得到是否已经审核过
					 var htstate = getInspectmIstate(chkValue);
					 if(htstate!="2"){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					 }
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="Storage_date" /><fmt:message key="confirm" />',
						content: '<iframe id="enterinordr" frameborder="0" src="<%=path%>/inspect/toEnterInOrdr.do?sta=enterinordr&pk_inspect='+chkValue+'"></iframe>',
						width: '500px',
						height: '250px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="enter" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('enterinordr')&&window.document.getElementById("enterinordr").contentWindow.validate._submitValidate()){
											window.document.getElementById("enterinordr").contentWindow.enterInOrdr();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_bill_to_enter" />！');
					return ;
				}
			}
			//删除
			function deleteInspectm(){
				var flag = true;
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							var row = $(this).closest('tr');
							if($.trim(row.children('td:eq(6)').text()) == '<fmt:message key="Have_the_inspection" />' ||
									$.trim(row.children('td:eq(6)').text()) == '<fmt:message key="Settled" />'){
								flag = false;
								alerterror('<fmt:message key="bill_haven_enter" />，<fmt:message key="cannotdelete" />！');
								return;
							}
						});
						if(flag){
							alertconfirm('<fmt:message key="delete_data_confirm" />？',function(){
								var action = '<%=path%>/inspect/deleteInspectm.do?pks='+chkValue.join(",");
								$('body').window({
									title: '<fmt:message key="delete_systemcoding_information" />',
									content: '<iframe frameborder="0" src='+action+'></iframe>',
									width: '500px',
									height: '245px',
									draggable: true,
									isModal: true
								});
							});
						}
					
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
				
			}
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':checkbox')[0].checked) {
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			});
			//---------------------------
// 			全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$("#queryForm").submit();
			}
			document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
	        
	        function printinspectbill(){
	        	var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					$("#printForm").attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					$("#printForm").submit();
				}else{
					alerterror('请选择一条验货单！');
				}
	        }
	        
	        //根据订单得到状态
	        function getInspectmIstate(pk_inspect){
				var res = 1;
				$.ajax({
					type: "POST",
					url: "<%=path%>/inspect/queryInspectmIstate.do",
					data: {'pk_inspect':pk_inspect},
					dataType: "json",
					async:false,
					success: function(data){
						res = data;
						}
					});
				return res;
	        }
		</script>
	</body>
</html>