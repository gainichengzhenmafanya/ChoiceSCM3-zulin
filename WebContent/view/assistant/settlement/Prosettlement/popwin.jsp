<%--
  User: mc
  Date: 14-10-29
  Time: 下午4:22
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>生成付款单</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
    	.form-line{
    		margin-top:15px;
    	}
    </style>
</head>
<body>
<div id="popWin" style="margin-left:20%;">
    <div class="form-line">
        <div class="form-label"><fmt:message key="suppliers"/>：</div>
        <div class="form-input">
            <input type="hidden" value="${codes}" id="codes"/>
            <input type="hidden" value="${type}" id="type"/>
            <input type="hidden" value="${deliver.code}" id="delivercode"/>
            <input type="text" readonly="readonly" value="${deliver.des}" id="vname"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="document_date"/>：</div>
        <div class="form-input">
            <input type="text" id="billdat" name="billdate" value="${date}" class="Wdate text" onfocus="WdatePicker({maxDate:'%y-%M-%d',minDate:'%y-%M-{%d-30}'})"/>
        </div>
    </div>
    <!-- 
    <div class="form-line">
        <div class="form-label"><fmt:message key="document_no"/>：</div>
        <div class="form-input">
            <input type="text" id="billnumber" value="${billnumber}"/>
        </div>
    </div>
    -->
    <div class="form-line">
        <div class="form-label"><fmt:message key="The_payment_amount"/>：</div>
        <div class="form-input">
            <input type="text" id="nmoney" readonly="readonly" value="<fmt:formatNumber value="${money}" pattern="0.00"/>"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="The_invoice_amount"/>：</div>
        <div class="form-input">
            <input type="text" id="imoney" value="<fmt:formatNumber value="${money}" pattern="0.00"/>"/>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
<script type="text/javascript">
	var savelock = false;
    var proval;
    $(document).ready(function(){
    	$("#billnumber").focus();
        setElementHeight('grid1',['.tool'],$(document.body),360);	//计算.grid的高度
        setElementHeight('grid1 .table-body',['grid1 .table-head'],'grid1'); //计算.table-body的高度
        setElementHeight('grid2',['.tool'],$(document.body),360);	//计算.grid的高度
        setElementHeight('grid2 .table-body',['grid2 .table-head'],'grid2'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.easyui-tabs').css("width",$(document.body).width()*0.99);
        proval= new Validate({
            validateItem:[{
                type:'text',
                validateObj:'billdat',
                validateType:['canNull','maxLength'],
                param:['F','20'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!']
            }/*,{
                type:'text',
                validateObj:'billnumber',
                validateType:['canNull','maxLength'],
                param:['F','50'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!']
            }*/,{
                type:'text',
                validateObj:'nmoney',
                validateType:['canNull','num'],
                param:['F','F'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="must_be_numeric"/>!']
            },{
                type:'text',
                validateObj:'imoney',
                validateType:['num'],
//                 handler:function(){
//                     var money=$("#imoney").val();
//                     if(money.match("^([+]?)\\d*\\.?\\d+$")){
//                         if(Number(money)>=0){
//                             return true;
//                         }
//                     }
//                     return false;
//                 },
                param:['F'],
                error:['<fmt:message key="not_valid_number"/>!']
            }]
        });
    });
    var showStore=function(pro,item){
        window.parent.showStore(pro,item);
    };
    var showSupplier=function() {
        selectSupplier({
            basePath: '<%=path%>',
            title: "123",
            height: 500,
            width: 700,
            callBack: 'setSupplier',
            single: false
        });
    };
    function setSupplier(data){
        $("#vname").val(data.entity[0].vname);
        $("#delivercode").val(data.entity[0].delivercode);
    }
    var mark=true;
    var addPro=function(){
		if(savelock){
			return;
		}else{
			savelock = true;
		}
        if (proval._submitValidate()&&mark) {
            var data={};
            data.codes=$("#codes").val();
            data.type=$("#type").val();
            data["supplier.delivercode"]=$("#delivercode").val();
            data.billdate=$("#billdat").val();
            data.nmoney=$("#nmoney").val();
            data.billnumber=$("#billnumber").val();
            var im= $.trim($("#imoney").val());
            im=im?im:0;

            if(im.indexOf(".")==-1||im.indexOf(".")>10){
                if(im.length>10) {
                    errorbox($("#imoney"), '<fmt:message key="the_maximum_length_not"/>10');
					savelock = false;
                    return;
                }
            }
            data.imoney=im;
            $.post('<%=path%>/prosettlement/addPro.do', data, function (msg) {
                if (msg == 'success') {
                	alerttips('<fmt:message key="successful_added"/>！');
    					savelock = false;
                		reload();
                } else {
                    alerterror('<fmt:message key="save_fail"/>！');
					savelock = false;
                }
            });
            mark=false;
			savelock = false;
            setTimeout(function(){mark=true;},5000);
        }else{
			savelock = false;
        }
    };
    //刷新父级页面
    var reload=function(){
        if(typeof(eval(parent.pageReload))=='function') {
            parent.pageReload();
        }else {
            parent.location.href = parent.location.href;
            $("#listForm",parent.document).submit();
        }
        $(".close",parent.document).click();
    };
</script>
</body>
</html>
