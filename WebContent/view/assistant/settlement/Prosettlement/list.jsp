<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>付款单</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 17px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div> 
<div style="height:48%;">
    <div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/prosettlement/list.do" method="post">
        <div class="bj_head" style="margin-top: 10px;">
            <div class="form-line">
                <div class="form-label" style="width: 50px;"><fmt:message key="date"/>：</div>
                <div class="form-input">
<%--                     <input type="hidden" value="${findPro.bdat}" id="bdat"/> --%>
                    <input type="text" value="${findPro.bdat}" id="bdat" name="bdat" class="Wdate text"  onfocus="WdatePicker({isShowClear:false,maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
                <div class="form-label" style="width: 15px;"><fmt:message key="to2"/></div>
                <div class="form-input">
<%--                     <input type="hidden" value="${findPro.edat}" id="edat"/> --%>
                    <input type="text" value="${findPro.edat}" id="edat" name="edat" class="Wdate text"  onfocus="WdatePicker({isShowClear:false,minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
                <div class="form-label"><fmt:message key="suppliers"/>：</div>
                <div class="form-input">
                    <input type="hidden" name="delivercode" value="${findPro.delivercode}" id="delivercode" style="margin:2px;" class="text"/>
                    <input type="text" name="delivername" style="margin-bottom: 6px;" id="delivername" value="${findPro.delivername}" readonly="readonly" style="margin:2px;" class="text"/>
                    <img style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" class="search" onclick="showSupplier()"/>
                </div>
            </div>
        </div>
        <div class="grid" id="grid">
            <div class="table-head" >
                <table cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                        	<td class="num"><span style="width:30px;">&nbsp;</span></td>
                            <td><span style="width: 150px;"><fmt:message key="suppliers"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="beginning_of_period"/><fmt:message key="Not_paying"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="The_current_stock"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="This_issue"/><fmt:message key="Payment_has_been"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="This_issue"/><fmt:message key="Not_paying"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="Have_invoice"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="Not_invoice"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="This_period_began"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="This_period_end"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellspacing="0" cellpadding="0">
                    <tbody>
                        <c:forEach items="${prosettlement}" var="pro" varStatus="status">
                            <tr>
                            	<td class="num"><span style="width:30px;">${status.index+1}</span></td>
                                <td>
                                    <input type="hidden" value="${pro.delivercode}"/>
                                    <span style="width: 150px;">${pro.delivername}</span>
                                </td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.startUnset}" pattern="0.00"/>">
                                    <c:if test="${pro.startUnset==''||pro.startUnset==null}">0.00</c:if>
                                    <c:if test="${pro.startUnset!=''&&pro.startUnset!=null}">
                                        <fmt:formatNumber value="${pro.startUnset}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.nowPro}" pattern="0.00"/>">
                                    <c:if test="${pro.nowPro==''||pro.nowPro==null}">0.00</c:if>
                                    <c:if test="${pro.nowPro!=''&&pro.nowPro!=null}">
                                        <fmt:formatNumber value="${pro.nowPro}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.nowSet}" pattern="0.00"/>">
                                    <c:if test="${pro.nowSet==''||pro.nowSet==null}">0.00</c:if>
                                    <c:if test="${pro.nowSet!=''&&pro.nowSet!=null}">
                                        <fmt:formatNumber value="${pro.nowSet}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.endUnset}" pattern="0.00"/>">
                                    <c:if test="${pro.endUnset==''||pro.endUnset==null}">0.00</c:if>
                                    <c:if test="${pro.endUnset!=''&&pro.endUnset!=null}">
                                        <fmt:formatNumber value="${pro.endUnset}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.invoice}" pattern="0.00"/>">
                                    <c:if test="${pro.invoice==''||pro.invoice==null}">0.00</c:if>
                                    <c:if test="${pro.invoice!=''&&pro.invoice!=null}">
                                        <fmt:formatNumber value="${pro.invoice}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: right;" title="<fmt:formatNumber value="${pro.nInvoice}" pattern="0.00"/>">
                                    <c:if test="${pro.nInvoice==''||pro.nInvoice==null}">0.00</c:if>
                                    <c:if test="${pro.nInvoice!=''&&pro.nInvoice!=null}">
                                        <fmt:formatNumber value="${pro.nInvoice}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 100px;text-align: center;">${findPro.bdat}</span></td>
                                <td><span style="width: 100px;text-align: center;">${findPro.edat}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <page:page form="listForm" page="${pageobj}"></page:page>
            <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
            <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
        </div>
    </form>
</div>
<form id="printForm" action="<%=path%>/prosettlement/printProsettlement.do" method="post">
	<input type="hidden" id="printpk" name="supplier.delivercode" value="${delivercode}"/>
	<input type="hidden" id="billtype" name="billtype" />
	<input type="hidden" id="pbdat" name="bdat"/>
	<input type="hidden" id="pedat" name="edat"/>
</form>
<div class="mainFrame" style=" height:55%;width:100%;">
    <iframe src="<%=path%>/prosettlement/listbottom.do" frameborder="0" name="mainFrame" id="mainFrame">
    </iframe>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
<script type="text/javascript">
    var vname;
    var delivercode;
$(document).ready(function(){
	$("#vname").focus();

    $('.tool').toolbar({
        items: [{
            text: '<fmt:message key="select"/>',
			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px', '-40px']
            },
            handler: function () {	
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $('#listForm').submit();
            }
        },{
            text: '<fmt:message key="Generate_the_payment_order"/>',
			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'creat')},
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                popwin();
            }
        },{
			text: '<fmt:message key="print" />',
			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
			icon: {
				url: '<%=path%>/image/Button/op_owner.gif',
				positn: ['0px','0px']
			},
			items:[{
					text: '打印未结账单',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['0px','0px']
					},
					handler: function(){
						printprosettlementn('N');
					}
				},{
					text: '打印已结账单',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['0px','0px']
					},
					handler: function(){
						printprosettlementn('Y');
					}
				}]
		},{
            text: '<fmt:message key="quit"/>',
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
            }
        }]
    });
	setElementHeight('.grid',['.tool'],$(document.body),$(document.body).height()/2+85);	//计算.grid的高度
//     setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),135);	//计算.grid的高度
    setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
    loadGrid();//  自动计算滚动条的js方法		
     $(".page").css("margin-bottom",$(".mainFrame").height()-10);
/* $('.grid').find('.table-body').find('tr').hover(
            function(){
                $(this).addClass('tr-over');
            },
            function(){
                $(this).removeClass('tr-over');
            }
    );*/
    //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
    $('.grid').find('.table-body').find('tr').live("click", function () {
        $('.grid').find('.table-body').find('tr').removeClass("tr-over");
        if($(this).hasClass("tr-over")) {
            $(this).removeClass("tr-over");
        }else{
            $(this).addClass("tr-over");
        }
        delivercode=$(this).find("input").val();
        $("#printpk").val(delivercode);
        vname=$(this).find("td:eq(0)").find("span").text();
        var  url="<%=path%>/prosettlement/listbottom.do?supplier.delivercode="+delivercode+"&supplier.vname="+vname
                +"&bdat="+$("#bdat").val()+"&edat="+$("#edat").val();
        $('#mainFrame').attr('src',encodeURI(url));
    });
    if($(".table-body").find("tr").size()>0){
        delivercode=$(".table-body").find("tr:eq(0)").find("input").val();
        $("#printpk").val(delivercode);
        vname=$(".table-body").find("tr:eq(0)").find("td:eq(0)").find("span").text();
        var  url="<%=path%>/prosettlement/listbottom.do?supplier.delivercode="+delivercode+"&supplier.vname="+vname
                +"&bdat="+$("#bdat").val()+"&edat="+$("#edat").val();
        $('#mainFrame').attr('src',encodeURI(url));
    }
    var trrows = $(".table-body").find('tr');
    if(trrows.length!=0){
        $(trrows[0]).addClass('tr-over');
    }
	$("#wait2").css("display","none");
	$("#wait").css("display","none");
});
var showSupplier=function() {
    selectSupplier({
        basePath: '<%=path%>',
        title: "123",
        height: 500,
        width: 850,
        callBack: 'setSupplier',
        single: false
    });
};
function setSupplier(data){
    $("#delivercode").val(data.delivercode);
    $("#delivername").val(data.delivername);
}
//生成付款单
var popwin=function(){
    var checkboxList = $('#grid1',window.frames['mainFrame'].document).find('.table-body').find(':checkbox');
    if(checkboxList&&checkboxList.filter(':checked').size() > 0) {
        var codeValue=[];
        var typeValue=[];
        var moneyValue=0;
        checkboxList.filter(':checked').each(function(){
            codeValue.push($(this).val());
            typeValue.push($(this).parents("tr").find("input[name='billType']").val());
            var money=$.trim(($(this).parents("tr").find("td:eq(3)").find("span").text()));
            moneyValue+=Number(money)?Number(money):0;
        });
        var action = '<%=path%>/prosettlement/popwin.do?supplier.delivercode=' + delivercode + '&codes='+codeValue.join(",")+'&type='+typeValue.join(",")+'&money='+moneyValue;
        $('body').window({
            title: '<fmt:message key="Generate_the_payment_order"/>',
            content: '<iframe id="popwinFrame" frameborder="0" src="' + action + '"></iframe>',
            width: '500px',
            height: '350px',
            draggable: true,
            isModal: true,
            topBar: {
                items: [
                    {
                        text: '<fmt:message key="save"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-80px', '-0px']
                        },
                        handler: function () {
                            getFrame('popwinFrame').addPro();
                        }
                    },
                    {
                        text: '<fmt:message key="cancel"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px', '-100px']
                        },
                        handler: function () {
                            $('.close').click();
                        }
                    }
                ]
            }
        });
    }else{
        alerterror('<fmt:message key="Please_select_a_outstanding_bills"/>！');
        return ;
    }
}

function printprosettlementn(type){
	$("#pbdat").val($("#bdat").val());
	$("#pedat").val($("#edat").val());
	$("#billtype").val(type);
	$("#printForm").attr('target','report');
	window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
	$("#printForm").submit();
}
</script>
</body>
</html>
