<%--
  User: mc
  Date: 14-10-29
  Time: 下午4:22
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
<div class="easyui-tabs" fit="false" plain="true" style="height:270px;z-index:88;margin:0px auto;">
    <div title='未结算账单' id="y_1" style="overflow: hidden;">
        <div class="grid" id="grid1">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td>
                                <span style="width: 30px;">
                                    <input type="checkbox" id="chkAll"/>
                                </span>
                            </td>
                            <td><span style="width: 90px;"><fmt:message key="document_date"/></span></td>
                            <td><span style="width: 120px;"><fmt:message key="storage"/><fmt:message key="document_no"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="amount"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="document_types"/> </span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${proBill}" var="pro">
                            <c:if test="${pro.istate!=3&&pro.istate!=4}">
                                <tr>
                                    <td style="width:30px; text-align: center;">
                                        <span style="width: 30px;">
                                            <input type="checkbox"  name="idList" id="chk_<c:out value='${pro.pk_bill}' />" value="<c:out value='${pro.pk_bill}' />"/>
                                        </span>
                                    </td>
                                    <td><span style="width: 90px;text-align: center;" title="${pro.dbilldate}">${pro.dbilldate}</span></td>
                                    <td><span style="width: 120px;" title="${pro.vbillno}">${pro.vbillno}</span></td>
                                    <td><span style="width: 90px;text-align: right;" title="<fmt:formatNumber value="${pro.nmoney}" pattern="0.00"/>">
                                        <c:if test="${pro.nmoney==''||pro.nmoney==null}">0.0</c:if>
                                        <c:if test="${pro.nmoney!=''&&pro.nmoney!=null}">
                                            <fmt:formatNumber value="${pro.nmoney}" pattern="0.00"/>
                                        </c:if>
                                    </span></td>
                                    <td><span style="width: 90px;">
                                        <input type="hidden" name="billType" value="${pro.billType}"/>
                                        <c:if test="${pro.billType==1}"><fmt:message key="Refund_single"/></c:if>
                                        <c:if test="${pro.billType==2}"><fmt:message key="The_inspection_sheet"/></c:if>
                                        <c:if test="${pro.billType==3}"><fmt:message key="Other_payment"/></c:if>
                                    </span></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='已结算账单' id="y_2" style="overflow: hidden;">
        <div class="grid" id="grid2">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td>
                                <span style="width: 30px;">
                                    <input type="checkbox" id="chkAll2"/>
                                </span>
                            </td>
                            <td><span style="width: 90px;"><fmt:message key="document_date"/></span></td>
                            <td><span style="width: 120px;"><fmt:message key="storage"/><fmt:message key="document_no"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="amount"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="document_types"/></span></td>
                            <td><span style="width: 150px;"><fmt:message key="Payment_is_only_no"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="payment_order_date"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${proBill}" var="pro">
                            <c:if test="${pro.istate==3||pro.istate==4}">
                                <tr>
                                    <td style="width:30px; text-align: center;">
                                            <span style="width: 30px;">
                                                <input type="checkbox"  name="idList2" id="chk_<c:out value='${pro.pk_bill}' />" value="<c:out value='${pro.pk_bill}' />"/>
                                            </span>
                                    </td>
                                    <td><span style="width: 90px;text-align: center;" title="${pro.dbilldate}">${pro.dbilldate}</span></td>
                                    <td><span style="width: 120px;" title="${pro.vbillno}">${pro.vbillno}</span></td>
                                    <td><span style="width: 90px;text-align: right;" title="<fmt:formatNumber value="${pro.nmoney}" pattern="0.00"/>">
                                        <c:if test="${pro.nmoney==''||pro.nmoney==null}">0.0</c:if>
                                        <c:if test="${pro.nmoney!=''&&pro.nmoney!=null}">
                                            <fmt:formatNumber value="${pro.nmoney}" pattern="0.00"/>
                                        </c:if>
                                    </span></td>
                                    <td><span style="width: 90px;">
                                        <input type="hidden" name="billType" title="${pro.billType}" value="${pro.billType}"/>
                                        <c:if test="${pro.billType==1}"><fmt:message key="Refund_single"/></c:if>
                                        <c:if test="${pro.billType==2}"><fmt:message key="The_inspection_sheet"/></c:if>
                                        <c:if test="${pro.billType==3}"><fmt:message key="Other_payment"/></c:if>
                                    </span></td>
                                    <td><span style="width: 150px;text-align: left;" title="${pro.proNumber}">${pro.proNumber}</span></td>
                                    <td><span style="width: 90px;text-align: right;" title="${pro.dpaydate}">${pro.dpaydate}</span></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        setElementHeight('grid1',['.tool'],$(document.body),360);	//计算.grid的高度
        setElementHeight('grid1 .table-body',['grid1 .table-head'],'grid1'); //计算.table-body的高度
        setElementHeight('grid2',['.tool'],$(document.body),360);	//计算.grid的高度
        setElementHeight('grid2 .table-body',['grid2 .table-head'],'grid2'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.easyui-tabs').tabs({
            width:$(document.body).width()*0.998,
            height:$(document.body).height()-51
        })
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        //如果全选按钮选中的话，table背景变色
        $("#chkAll").click(function() {
            if (!!$("#chkAll").attr("checked")) {
                $('#grid1').find('.table-body').find('tr').addClass("bgBlue");
            }else{
                $('#grid1').find('.table-body').find('tr').removeClass("bgBlue");
            }
        });
        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            if ($(this).hasClass("bgBlue")) {
                $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
            }
            else
            {
                $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
            }
        });
    });
    var showStore=function(pro,item){
        window.parent.showStore(pro,item);
    }
</script>
</body>
</html>
