<%--
  申请开发票
  User: mc
  Date: 14-10-31
  Time: 下午7:55
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>申请开发票</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<div class="grid" id="grid">
    <input type="hidden" id="codes" value="${codes}"/>
    <input type="hidden" id="moneys" value="${moneys}"/>
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <td class="num"><span style="width: 26px;">&nbsp;</span></td>
                    <td><span style="width: 200px;"><fmt:message key="The_invoice_looked_up"/></span></td>
                    <td><span style="width: 90px;"><fmt:message key="invoice"/><fmt:message key="amount"/></span></td>
                    <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="confirm"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    add();
                }
            },{
                text: '<fmt:message key="cancel"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $(".close",parent.document).click();
                }
            }]
        });
        setElementHeight('.grid',['.tool'],$(document.body),8);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        editCells();
    });
    //========================================================
    //编辑表格
    function editCells() {
        //====================合约基本============================
        $(".table-body").autoGrid({
            initRow: 1,
            colPerRow: 4,
            widths: [26,210,100,210],
            onEdit: $.noop,
            colStyle:['','',{'text-align':"right"},''],
            editable: [1,2,3],
            onEnter: function (data) {//默认回车后动作    data中curobj表示当前td对象，value保存输入值，ovalue保存输入之前的值
            	var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
            	var rownum = data.curobj.closest('tr');	
            	data.curobj.find('span').text(rownum.find('td:eq('+pos+')').find('input').val());
            },
            cellAction: [
                {
                    index: 1,
                    action: function (row, data2) {
                        if ($.trim(data2.value)=='') {
                            alerterror('<fmt:message key="cannot_be_empty"/>！');
                            row.find("td:eq(1)").find('span').text("");
                            $.fn.autoGrid.setCellEditable(row, 1);
                        } else {
                            $.fn.autoGrid.setCellEditable(row, 2);
                        }
                    }
                },{
                    index:2,
                    action:function(row,data2){
                        if(Number(data2.value) == 0){
                            alerterror('<fmt:message key="number_cannot_be_zero"/>！');
                            row.find("td:eq(2)").find('span').text(data2.ovalue);
                            $.fn.autoGrid.setCellEditable(row,2);
                        }else if(0>Number(data2.value)){
                            alerterror('<fmt:message key="amount"/><fmt:message key="can_not_be_less_than"/>0！');
                            row.find("td:eq(2)").find('span').text(data2.ovalue);
                            $.fn.autoGrid.setCellEditable(row,2);
                        }else if(isNaN(data2.value)){
                            alerterror('<fmt:message key="number_be_not_number"/>！');
                            row.find("td:eq(2)").find('span').text(data2.ovalue);
                            $.fn.autoGrid.setCellEditable(row,2);
                        }else {
                            $.fn.autoGrid.setCellEditable(row,3);
                        }
                    }
                },{
                    index: 3,
                    action: function (row, data) {
                        if(!row.next().html())$.fn.autoGrid.addRow();
                        row.find("td:eq(3)").find('span').text($.trim(data.value));
                        $.fn.autoGrid.setCellEditable(row.next(), 1);
                    }
                }
            ]
        });
    }
    //申请发票保存
	var savelock = false;
    var add=function(){
		if(savelock){
			return;
		}else{
			savelock = true;
		}
        var data={};
        var mark=false;
        $(".table-body").find("tr").each(function(i){
            var head=$.trim($(this).find("td:eq(1)").find("input").val());
            head=head?head:$.trim($(this).find("td:eq(1)").find("span").text());
            var money=$(this).find("td:eq(2)").find("input").val();
            money=money?money:$(this).find("td:eq(2)").find("span").text();
            var memo= $.trim($(this).find("td:eq(3)").find("input").val());
            memo=memo?memo:$.trim($(this).find("td:eq(3)").find("span").text());
            if(head==''|| money==''){
                mark=true;
                alerterror('<fmt:message key="Looked_up_and_amount_cant_be_empty"/>');
				savelock = false;
                return;
            }
            if(0>=Number(money)){
                mark=true;
                alerterror('<fmt:message key="Invoice_amount_not_less_than_zero"/>');
				savelock = false;
                return;
            }
            if(isNaN(money)){
                mark=true;
                alerterror('<fmt:message key="amount"/><fmt:message key="not_valid_number"/>');
				savelock = false;
                return;
            }
            if(head.length>=50){
                mark=true;
                alerterror('<fmt:message key="The_invoice_looked_up"/><fmt:message key="the_maximum_length_not"/>50');
				savelock = false;
                return;
            }
            if(memo.length>=50){
                mark=true;
                alerterror('<fmt:message key="remark"/><fmt:message key="the_maximum_length_not"/>200');
				savelock = false;
                return;
            }
            data["applyinvoices["+i+"].vinvoicehead"]=head;
            data["applyinvoices["+i+"].ninvoicemoney"]=money;
            data["applyinvoices["+i+"].vmemo"]=memo;
            data.codes=$("#codes").val();
        });
        if(mark){
			savelock = false;
            return;
        }
        $.post('<%=path%>/payment/addApp.do',data,function(msg){
            if(msg=="1"){
                alerterror('<fmt:message key="Invoice_amount_is_not_greater_than_the_order_amount"/>');
				savelock = false;
            }else if(msg=="2"){
                alerterror("<fmt:message key="save_fail"/>,2");
				savelock = false;
            }else if(msg=="3"){
            	alerttipsbreak('<fmt:message key="save_successful"/>',function(){
                        $("#listForm",parent.document).submit();
            	});
            }else if(msg=="4"){
                alerterror("<fmt:message key="save_fail"/>,4");
				savelock = false;
            }else{
                alerterror("<fmt:message key="save_fail"/>");
				savelock = false;
            }
        });
    };

	function deleteRow(obj){
		
		var tb = $(obj).closest('table');
		var rowH = $(obj).parent("tr").height();
		var tbH = tb.height();
		$(obj).parent("tr").nextAll("tr").each(function(){
			var curNum = Number($.trim($(this).children("td:first").text()));
			$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
		});
		
		if($(obj).next().length!=0){
			var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
			tb.find('tr:last').prev().append(addCell);
		}
		
		var tr = $(obj).closest('tr');
		if(tr.prev().length==0 ){//删除第一行
			if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
				tr.next().find('td[name="deleCell"]').remove();
			}
		}else if(tr.prev().prev().length==0){//点击第二行的删除
			if(tr.find('td:last').attr('name')=='addCell'){
				tr.prev().find('td[name="deleCell"]').remove();
			}
		}
		
		$(obj).parent("tr").remove();
	};
</script>
</body>
</html>

