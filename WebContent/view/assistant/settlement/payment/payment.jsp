<%--
  付款
  User: mc
  Date: 14-10-31
  Time: 下午7:55
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>申请开发票</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
        .text{
        	width:200px;
        }
        .select{
        	width: 202px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<div class="form-line" style="margin-top: 10px;">
    <input type="hidden" id="codes" value="${codes}"/>
    <div class="form-label" style="width: 120px;"><fmt:message key="The_payment_amount"/>：</div>
    <div class="form-input">
        <input type="hidden" value="${moneys}" id="moneys"/>
        <input type="text" name="npaymoney" class="text" value="<fmt:formatNumber value="${moneys}" pattern="0.00"/>" id="npaymoney"/>
    </div>
</div>
<div class="form-line">
    <div class="form-label" style="width: 120px;"><fmt:message key="payments"/><fmt:message key="way"/>：</div>
    <div class="form-input">
        <select id="paymodel" class="select">
            <c:forEach items="${mhs}" var="mh">
                <option value="${mh.code}">${mh.des}</option>
            </c:forEach>
        </select>
    </div>
</div>
<div class="form-line">
    <div class="form-label" style="width: 120px;"><span style="color:red">*</span><fmt:message key="The_drawee"/>：</div>
    <div class="form-input">
        <input id="vpayaccount" name="vpayaccount" class="text" type="text"/>
    </div>
</div>
<div class="form-line">
    <div class="form-label" style="width: 120px;"><fmt:message key="pay"/><fmt:message key="date"/>：</div>
    <div class="form-input">
        <input id="dpaydat" type="text" class="Wdate text" onfocus="WdatePicker({maxDate:'%y-%M-%d',minDate:'{%y-1}-%M-%d'})"/>
    </div>
</div>
<!-- 
<div class="form-line">
    <div class="form-label" style="width: 120px;"><fmt:message key="Whether_the_end_of_this_document"/>：</div>
    <div class="form-input">
        <select id="ipaymentstate"  class="select">
            <option value="2"><fmt:message key="be"/></option>
            <option value="3"><fmt:message key="no1"/></option>
        </select>
    </div>
</div>
-->
<div class="form-line">
    <div class="form-label" id="creator" style="width: 120px;"><fmt:message key="remark"/>：</div>
    <div class="form-input">
        <textarea rows="5" cols="25" id="vmemo"  style="width: 200px;resize: none;margin-top: 5px;"></textarea>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
	var savelock = false;
    var payval;
    $(document).ready(function(){
        $("#npaymoney").focus();
        $("#dpaydat").htmlUtils("setDate","now");
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="confirm"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
    				if(savelock){
    					return;
    				}else{
    					savelock = true;
    				}
                    if(payval._submitValidate()) {
                        var data = {};
                        data.codes = $("#codes").val();
                        data.npaymoney = $("#npaymoney").val();
                        data.vpayaccount = $("#vpayaccount").val();
                        data.dpaydat = $("#dpaydat").val();
                        //data.ipaymentstate = $("#ipaymentstate").val();
                        data.ipaymentstate = 1;
                        data.vmemo = $("#vmemo").val();
                        data.pk_paymodel = $("#paymodel").val();
                        data.vpaymodelname = $("#paymodel").find(":selected").text();
                        $.post("<%=path%>/payment/addPaydetails.do", data, function (msg) {
                            if (msg == "1") {
                            	alerttips("<fmt:message key="pay"/><fmt:message key="successful"/>！");
                                parent.$("#listForm").submit();
                                $(".close", parent.document).click();
                            } else if (msg == "2") {
                                alerterror('<fmt:message key="pay"/><fmt:message key="failure"/>,2！');
            					savelock = false;
                            }else{
                                alerterror('<fmt:message key="pay"/><fmt:message key="failure"/>！');
            					savelock = false;
                            }
                        });
                    }else{
    					savelock = false;
                    }
                }
            },{
                text: '<fmt:message key="cancel"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $(".close",parent.document).click();
                }
            }]
        });
        payval= new Validate({
            validateItem:[{
                type:'text',
                validateObj:'vpayaccount',
                validateType:['canNull','maxLength'],
                param:['F','48'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>48!']
            },{
                type:'text',
                validateObj:'npaymoney',
                validateType:['canNull','num','handler'],
                handler:function(){
                    var money=$("#npaymoney").val();
                    if(money.indexOf(".")==-1||money.indexOf(".")>10){
                        if(money.length>10) {
                            return false;
                        }
                    }
                    return true;
                },
                param:['F','F','F'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="please_enter_positive_integer"/> ','<fmt:message key="maximum_length"/>10!']
            },{
                type:'text',
                validateObj:'dpaydat',
                validateType:['canNull','date'],
                param:['F','10','T'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="incorrect_format_input"/>!']
            }]
        });
    });
    //========================================================
</script>
</body>
</html>