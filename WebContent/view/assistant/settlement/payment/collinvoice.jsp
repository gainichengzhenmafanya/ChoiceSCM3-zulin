<%--
  发票录入
  User: mc
  Date: 14-10-31
  Time: 下午7:55
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <title>发票录入</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<div class="panel" style="margin-top: 10px;">
    <div class="form-line">
        <input type="hidden" value="${codes}" id="codes"/>
        <div class="form-label"><fmt:message key="invoice_number"/>：</div>
        <div class="form-input">
            <input type="text" name="vinvoicenum" id="vinvoicenum" class="text"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="The_invoice_amount"/>：</div>
        <div class="form-input">
            <input type="text" name="ninvoicemoney" id="ninvoicemoney" value="${xkfp}" class="text"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="invoice"/><fmt:message key="date"/>：</div>
        <div class="form-input">
            <input type="text" id="ddat" class="Wdate text" onfocus="WdatePicker({maxDate:'%y-%M-%d',minDate:'{%y-1}-%M-%d'})"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="operation"/><fmt:message key="people"/>：</div>
        <div class="form-input">
            <input type="text" id="creator" class="text"/>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript">
//ajax同步设置
$.ajaxSetup({
	async: false
});
	var savelock = false;
    var colval;
    $(document).ready(function(){
        $("#vinvoicenum").focus();
        $("#ddat").htmlUtils("setDate","now");
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="confirm"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
    				if(savelock){
    					return;
    				}else{
    					savelock = true;
    				}
                    if(colval._submitValidate()) {
                    	var param = {};
                    	param['codes'] = $('#codes').val();
                    	param['vinvoicenum'] = $('#vinvoicenum').val();
                    	param['ddat'] = $("#ddat").val();
                    	param['creator'] = $("#creator").val();
                    	param['ninvoicemoney'] = $("#ninvoicemoney").val();
                    	$.post("<%=path%>/payment/addColl.do",param,function(re){
                    		if(re=='ok'){
                    			alerttips("发票录入成功！");
                    			parent.$("#listForm").submit();
                    		}else{
                    			alerterror(re);
                    		}
                    	})
                    }else{
    					savelock = false;
                    }
                }
            },{
                text: '<fmt:message key="cancel"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $(".close",parent.document).click();
                }
            }]
        });
        colval= new Validate({
            validateItem:[{
                type:'text',
                validateObj:'vinvoicenum',
                validateType:['canNull','maxLength'],
                param:['F','48'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!']
            },{
                type:'text',
                validateObj:'ninvoicemoney',
                validateType:['canNull','maxLength','num'],
                param:['F','10','F'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!','<fmt:message key="_number_greater_than_zero"/> ']
            }]
        });
    });
    //========================================================
</script>
</body>
</html>
