<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>付款管理</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 17px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div style="height:48%;">
    <div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/payment/list.do" method="post">
        <div class="bj_head" style="margin-top: 10px;">
            <div class="form-line">
                <div class="form-label" style="width: 50px;"><fmt:message key="date"/>：</div>
                <div class="form-input">
                    <input type="text" value="${findPay.bdat}" name="bdat" id="bdat" class="Wdate text"  onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
                <div class="form-label" style="width: 15px;"><fmt:message key="to2"/></div>
                <div class="form-input">
                    <input type="text" value="${findPay.edat}" name="edat" id="edat" class="Wdate text"  onfocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
                <div class="form-label"><fmt:message key="suppliers"/>：</div>
                <div class="form-input" style="margin-right: 20px;">
                    <input type="hidden" name="delivercode.delivercode" id="delivercode" value="${findPay.delivercode.delivercode}" style="margin:2px;" class="text"/>
                    <input type="text" name="delivercode.vname" id="vname" style="margin-bottom: 6px;" value="${findPay.delivercode.vname}" style="margin:2px;" class="text"/>
                    <img src="<%=path%>/image/themes/icons/search.png" style="margin-bottom: 6px;" class="search" onclick="showSupplier()"/>
                </div>
                <div class="form-label" style="width: 70px;"><fmt:message key="Document_status"/>：</div>
                <div class="form-input">
                    <select name="ipaymentstate" class="select" style="margin: 0px;margin-top:3px;">
                        <option value=""><fmt:message key="all"/></option>
                        <option <c:if test="${findPay.ipaymentstate==1}">selected="selected" </c:if> value="1"><fmt:message key="outstanding"/></option>
                        <option <c:if test="${findPay.ipaymentstate==2}">selected="selected" </c:if> value="2"><fmt:message key="closed"/></option>
                        <!-- <option <c:if test="${findPay.ipaymentstate==3}">selected="selected" </c:if> value="3"><fmt:message key="Pay_not_over"/></option> -->
                    </select>
                </div>
            </div>
        </div>
        <div class="grid" id="grid">
            <div class="table-head" >
                <table cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <td style="width:30px;">
                                <input type="checkbox" id="chkAll"/>
                            </td>
                            <td><span style="width: 150px;"><fmt:message key="Payment_is_only_no"/></span></td>
                            <td><span style="width: 150px;"><fmt:message key="suppliers"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="document_date"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="The_accounts_payable_amount"/></span></td>
                            <td><span style="width: 90px;display:none;"><fmt:message key="Need_the_amount_of_the_payment"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="To_make_out_an_invoice_amount"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="The_amount_has_been_paid"/> </span></td>
                            <td><span style="width: 90px;"><fmt:message key="Not_paying_money"/> </span></td>
                            <td><span style="width: 90px;"><fmt:message key="Have_opened_the_invoice"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="status"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellspacing="0" cellpadding="0">
                    <tbody>
                    <c:forEach items="${payment}" var="pay">
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox" name="idList" id="chk_<c:out value='${pay.pk_payment}' />" value="<c:out value='${pay.pk_payment}' />"/>
                            </td>
                            <td><span style="width: 150px;">${pay.vpaymentcode}</span></td>
                            <td>
                                <input type="hidden" value="${pay.delivercode.delivercode}"/>
                                <span style="width: 150px;">${pay.delivercode.vname}</span>
                            </td>
                            <td><span style="width: 90px;">${pay.dbilldat}</span></td>
                            <td>
                                <input type="hidden" value="${pay.nypaymoney}"/>
                                <span style="width: 90px;text-align: right;">
                                    <c:if test="${pay.nypaymoney==''||pay.nypaymoney==null}">0.00</c:if>
                                    <c:if test="${pay.nypaymoney!=''&&pay.nypaymoney!=null}">
                                        <fmt:formatNumber value="${pay.nypaymoney}" pattern="0.00"/>
                                    </c:if>
                                </span>
                            </td>
                            <td><span style="width: 90px;text-align: right;display:none;">
                                <c:if test="${pay.nxpaymoney==''||pay.nxpaymoney==null}">0.00</c:if>
                                <c:if test="${pay.nxpaymoney!=''&&pay.nxpaymoney!=null}">
                                    <fmt:formatNumber value="${pay.nxpaymoney}" pattern="0.00"/>
                                </c:if>
                                </span>
                            </td>
                            <td><span style="width: 100px;text-align: right;">
                                <c:if test="${pay.ninvoicemoney==''||pay.ninvoicemoney==null}">0.00</c:if>
                                <c:if test="${pay.ninvoicemoney!=''&&pay.ninvoicemoney!=null}">
                                    <fmt:formatNumber value="${pay.ninvoicemoney}" pattern="0.00"/>
                                </c:if>
                            </span></td>
                            <td>
                                <input type="hidden" value="${pay.nwpaymoney}"/>
                                <span style="width: 90px;text-align: right;">
                                    <c:if test="${pay.nwpaymoney==''||pay.nwpaymoney==null}">0.00</c:if>
                                    <c:if test="${pay.nwpaymoney!=''&&pay.nwpaymoney!=null}">
                                        <fmt:formatNumber value="${pay.nwpaymoney}" pattern="0.00"/>
                                    </c:if>
                                </span>
                            </td>
                            <td>
                                <span style="width: 90px;text-align: right;">
                                    <fmt:formatNumber value="${pay.nxpaymoney-pay.nwpaymoney}" pattern="0.00"/>
                                </span>
                            </td>
                            <td><span style="width: 90px;">
                                <c:if test="${pay.iisorinvoive==2}"><fmt:message key="Have_invoice"/></c:if>
                                <c:if test="${pay.iisorinvoive==1}"><fmt:message key="Not_invoice"/></c:if>
                            </span></td>
                            <td><span style="width: 90px;">
                                <input type="hidden" name="paystate" value="${pay.ipaymentstate}"/>
                                <c:if test="${pay.ipaymentstate==2}"><fmt:message key="closed"/></c:if>
                                <c:if test="${pay.ipaymentstate==1}"><fmt:message key="outstanding"/></c:if>
                                <!-- <c:if test="${pay.ipaymentstate==1}"><fmt:message key="Not_paying"/></c:if> -->
                            </span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <page:page form="listForm" page="${pageobj}"></page:page>
            <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
            <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
        </div>
    </form>
</div>
<form id="printForm" action="<%=path%>/payment/print.do" method="post">
	<input type="hidden" id="pk_payment" name="pk_payment"/>
	<input type="hidden" id="printtype" name="printtype" />
</form>
<div class="mainFrame" style=" height:55%;width:100%;">
    <iframe src="<%=path%>/payment/bottom.do" frameborder="0" name="mainFrame" id="mainFrame">
    </iframe>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    var pk_payment;
    var delivercode;
    $(document).ready(function(){
    	$("#vname").focus();
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="select"/>',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
                    $('#listForm').submit();
                }
            },
            /*{
                text: '<fmt:message key="Apply_for_invoice"/>',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    popwin("<fmt:message key="Apply_for_invoice"/>",false,"<%=path%>/payment/applyinvoice.do",600,400);
                }
            },*/{
                text: '<fmt:message key="Invoice_entry"/>',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    popwin("<fmt:message key="Invoice_entry"/>",false,"<%=path%>/payment/collinvoice.do",350,270);
                }
            },{
                text: '<fmt:message key="payments"/>',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    var checkboxList = $('.grid').find('.table-body').find(':checkbox').filter(":checked");
                    if(checkboxList.size()>1){
                        alerttips('<fmt:message key="please_select_a_data"/>');
                        return;
                    }
                    popwin("<fmt:message key="payments"/>",true,"<%=path%>/payment/paymentd.do",400,320);
                }
            },{
                text: '<fmt:message key="audit_check"/>',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    var checkboxList = $('.grid').find('.table-body').find(':checkbox').filter(":checked");
                    if(checkboxList.size()==0||checkboxList.size()>1){
                        alerttips('<fmt:message key="please_select_a_data"/>');
                        return;
                    }
                    //popwin("<fmt:message key="payments"/>",true,"<%=path%>/payment/paymentd.do",400,320);
                    var codes = [];
                    var isConf = true;
                    //审核结账
                    checkboxList.each(function(){
                        if(2==$.trim($(this).parents("tr").find("input[name='paystate']").val())){
                            alerterror("<fmt:message key='bill'/><fmt:message key='closed'/>！");
                            isConf=false;
                            return;
                        }
                    	codes.push($(this).val());
                    });
                    if(!isConf){
                    	return;
                    }
                    alertconfirm('<fmt:message key="To_determine_the_audit_check"/>?',function(){
						//改状态js20161215
						$.ajax({
						type: "POST",
						url: "<%=path%>/payment/checkEnd.do",
						data: {"pk_payment":codes.join(",")},
						dataType: "json",
						async:false,
						success: function(data){
							if("OK"==data){
								alerttips('<fmt:message key="operation_successful"/>');
								//刷新页面
								$("#wait2").css("display","block");
								$("#wait").css("display","block");
			                    $('#listForm').submit();
							}else{
								alerttips(data);
							}
						}
						});
                    });
                }
            },{
    			text: '<fmt:message key="print" />',
    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
    			icon: {
    				url: '<%=path%>/image/Button/op_owner.gif',
    				positn: ['0px','0px']
    			},
    			items:[{
    					text: '打印账单详情',
    	    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
    					icon: {
    						url: '<%=path%>/image/Button/op_owner.gif',
    						positn: ['0px','0px']
    					},
    					handler: function(){
    						print('bill');
    					}
    				},{
    					text: '打印发票详情',
    	    			useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
    					icon: {
    						url: '<%=path%>/image/Button/op_owner.gif',
    						positn: ['0px','0px']
    					},
    					handler: function(){
    						print('tax');
    					}
    				}]
    		},{
                text: '<fmt:message key="quit"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                }
            }]
        });
        setElementHeight('.grid',['.tool'],$(document.body),$(document.body).height()/2+85);	//计算.grid的高度
//         setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),45);	//计算.grid的高度
		setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
		loadGrid();//  自动计算滚动条的js方法		
		 $(".page").css("margin-bottom",$(".mainFrame").height()-10);
        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            delivercode=$(this).find("td:eq(1)").find("input").val();
            pk_payment=$(this).find(":checkbox").val();
            $("#pk_payment").val(pk_payment);
            var  url="<%=path%>/payment/bottom.do?delivercode.delivercode="+delivercode+"&pk_payment="+pk_payment;
            $('#mainFrame').attr('src',encodeURI(url));
            $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
            $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
        });
        $('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
            event.stopPropagation();
        });
        if($(".table-body").find("tr").size()>0){
            delivercode=$(".table-body").find("tr:eq(0)").find("td:eq(1)").find("input").val();
            pk_payment=$(".table-body").find("tr:eq(0)").find(":checkbox").val();
            $("#pk_payment").val(pk_payment);
            var  url="<%=path%>/payment/bottom.do?delivercode.delivercode="+delivercode+"&pk_payment="+pk_payment;
            $('#mainFrame').attr('src',encodeURI(url));
        }
        var trrows = $(".table-body").find('tr');
        if(trrows.length!=0){
            $(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
        }
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
    });
    var showSupplier=function() {
        selectSupplier({
            basePath: '<%=path%>',
            title: "123",
            height: 500,
            width: 850,
            callBack: 'setSupplier',
            single: false
        });
    };
    function setSupplier(data){
        $("#delivercode").val(data.delivercode);
        $("#vname").val(data.delivername);
    }
    var popwin=function(title,isPay,action,w,h){
        var checkboxList = $('.grid').find('.table-body').find(':checkbox').filter(":checked");
        var isConf=true;
        if(checkboxList.size() == 1) {
            var codeValue=[];
            var MoneyValue=[];
            var xkfp=[];
            checkboxList.filter(':checked').each(function(){
                var onepk=$(checkboxList[0]).parents("tr").find("td:eq(1)").find("input").val();
                if(onepk!=$(this).parents("tr").find("td:eq(1)").find("input").val()){
                    alerttips('<fmt:message key="Can_only_choose_the_same_supplier"/>!');
                    isConf=false;
                    return;
                }
                if(isPay&&2== $.trim($(this).parents("tr").find("input[name='paystate']").val())){
                    alerterror("<fmt:message key="Paid_over_bill_couldnt_pay"/>！");
                    isConf=false;
                    return;
                }
                codeValue.push($.trim($(this).val()));
                MoneyValue.push(Number($.trim($(this).parents("tr").find("td:eq(4)").find("input").val()))-Number($.trim($(this).parents("tr").find("td:eq(7)").find("input").val())));
                xkfp.push(Number($.trim($(this).parents("tr").find("td:eq(6) span").text())));
            });
//            var code=$(checkboxList[0]).parents("tr").find("td:eq(1)").find("input").val();
//            var money=$(checkboxList[0]).parents("tr").find("td:eq(4)").find("input").val();
            if(isConf) {
                $('body').window({
                    title: title,
                    content: '<iframe id="proPopwinFrame" frameborder="0" src="' + action + '?codes=' + codeValue.join(",")+'&moneys='+MoneyValue.join(",")+'&xkfp='+xkfp.join(",")+'"></iframe>',
                    width: w,
                    height: h,
                    draggable: true,
                    isModal: true
                });
            }
        }else{
            alerttips('<fmt:message key="a_data_can_be_set_at_a_time"/>！');
            return ;
        }
    };
    function print(type){
    	var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		if(checkboxList && checkboxList.filter(':checked').size() == 1){
			$("#printtype").val(type);
			$("#printForm").attr('target','report');
			window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
			$("#printForm").submit();
		}else{
			alerterror('请选择一条付款单！');
		}
    }
</script>
</body>
</html>
