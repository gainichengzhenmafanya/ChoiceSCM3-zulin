<%--
  User: mc
  Date: 14-10-29
  Time: 下午4:22
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>付款單</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
<div class="easyui-tabs" fit="false" plain="true" style="height:270px;z-index:88;">
    <div title='<fmt:message key="billdatelias"/>' id="y_2" style="overflow: hidden;">
        <div class="grid" id="grid2">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 110px;"><fmt:message key="document_no"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="document_types"/></span></td>
                            <td><span style="width: 90px;text-align: right;"><fmt:message key="amount"/></span></td>
                            <%--<td><span style="width: 90px;"><fmt:message key="The_payment_amount"/></span></td>--%>
                            <td><span style="width: 90px;"><fmt:message key="Fill_the_people"/></span></td>
                            <td><span style="width: 120px;"><fmt:message key="Filling_in_the_date"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${payment.paymentDList}" var="pay">
                            <tr>
                                <td><span style="width: 110px;" title="${pay.vbillnumber}">${pay.vbillnumber}</span></td>
                                <td><span style="width: 90px;">
                                    <c:if test="${pay.vbilltype==1}"><fmt:message key="Refund_single"/></c:if>
                                    <c:if test="${pay.vbilltype==2}"><fmt:message key="The_inspection_sheet"/></c:if>
                                    <c:if test="${pay.vbilltype==3}"><fmt:message key="Other_payment"/></c:if>
                                </span></td>
                                <td><span style="width: 90px;text-align: right;" title="${pay.nypaymoney}">
                                        <c:if test="${pay.nypaymoney==''||pay.nypaymoney==null}">0.00</c:if>
                                        <c:if test="${pay.nypaymoney!=''&&pay.nypaymoney!=null}">
                                            <fmt:formatNumber value="${pay.nypaymoney}" pattern="0.00"/>
                                        </c:if>
                                </span></td>
                                <td><span style="width: 90px;" title="${pay.creator}">${pay.creator}</span></td>
                                <td><span style="width: 120px;" title="${pay.creationtime}">${pay.creationtime}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='<fmt:message key="Payment_list"/>' id="y_4" style="overflow: hidden;">
        <div class="grid" id="grid4">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 110px;"><fmt:message key="The_drawee"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="The_payment_amount"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="payment"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="payments"/><fmt:message key="date"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="remark"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <c:forEach items="${payment.paydateiles}" var="pay">
                        <tr>
                            <td><span style="width: 110px;" title="${pay.vpayaccount}">${pay.vpayaccount}</span></td>
                            <td><span style="width: 90px;text-align: right;" title="${pay.npaymoney}">
                                <c:if test="${pay.npaymoney==''||pay.npaymoney==null}">0.00</c:if>
                                <c:if test="${pay.npaymoney!=''&&pay.npaymoney!=null}">
                                    <fmt:formatNumber value="${pay.npaymoney}" pattern="0.00"/>
                                </c:if>
                            </span></td>
                            <td><span style="width: 90px;" title="${pay.npaymoney}">
                                ${pay.vpaymodelname}
                            </span></td>
                            <td><span style="width: 90px;" title="${pay.dpaydat}">${pay.dpaydat}</span></td>
                            <td><span style="width: 120px;" title="${pay.vmemo}">${pay.vmemo}</span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='<fmt:message key="manage"/><fmt:message key="invoice"/>' id="y_1" style="overflow: hidden;">
        <div class="grid" id="grid1">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 110px;"><fmt:message key="invoice_number"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="The_invoice_amount"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="operation"/><fmt:message key="people"/></span></td>
                            <td><span style="width: 120px;"><fmt:message key="Filling_in_the_date"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${payment.collinvoices}" var="coll">
                            <tr>
                                <td><span style="width: 110px;" title="${coll.vinvoicenum}">${coll.vinvoicenum}</span></td>
                                <td><span style="width: 90px;text-align: right;" title="${coll.ninvoicemoney}">
                                    <c:if test="${coll.ninvoicemoney==''||coll.ninvoicemoney==null}">0.00</c:if>
                                    <c:if test="${coll.ninvoicemoney!=''&&coll.ninvoicemoney!=null}">
                                        <fmt:formatNumber value="${coll.ninvoicemoney}" pattern="0.00"/>
                                    </c:if>
                                </span></td>
                                <td><span style="width: 90px;" title="${coll.creator}">${coll.creator}</span></td>
                                <td><span style="width: 120px;" title="${coll.creationtime}">${coll.creationtime}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- 
    <div title='<fmt:message key="Invoice_to_apply_for"/>' id="y_3" style="overflow: hidden;">
        <div class="grid" id="grid3">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 110px;"><fmt:message key="The_invoice_looked_up"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="To_apply_for_the_invoice_amount"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="operation"/><fmt:message key="people"/></span></td>
                            <td><span style="width: 120px;"><fmt:message key="Filling_in_the_date"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <c:forEach items="${payment.applyinvoices}" var="app">
                        <tr>
                            <td><span style="width: 110px;" title="${app.vinvoicehead}">${app.vinvoicehead}</span></td>
                            <td><span style="width: 100px;text-align: right;" title="${app.ninvoicemoney}">
                                <c:if test="${app.ninvoicemoney==''||app.ninvoicemoney==null}">0.00</c:if>
                                <c:if test="${app.ninvoicemoney!=''&&app.ninvoicemoney!=null}">
                                    <fmt:formatNumber value="${app.ninvoicemoney}" pattern="0.00"/>
                                </c:if>
                            </span></td>
                            <td><span style="width: 90px;" title="${app.vapplyaccount}">${app.vapplyaccount}</span></td>
                            <td><span style="width: 120px;" title="${app.creationtime}">${app.creationtime}</span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    -->
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
//        setElementHeight('grid1',['.tool'],$(document.body),360);	//计算.grid的高度
//        setElementHeight('grid1 .table-body',['grid1 .table-head'],'grid1'); //计算.table-body的高度
//        setElementHeight('grid2',['.tool'],$(document.body),360);	//计算.grid的高度
//        setElementHeight('grid2 .table-body',['grid2 .table-head'],'grid2'); //计算.table-body的高度
//        loadGrid();//  自动计算滚动条的js方法
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth);
                $grid.find(".table-head").width(headWidth);
            }else{
                $grid.find(".table-body").width(gridWidth);
                $grid.find(".table-head").width(gridWidth);
            }
        };
        //有可能是这里出错了
        $('.easyui-tabs').tabs({
            width:$(document.body).width()*0.99,
            height:$(document.body).height()-51,
            onSelect:function(){
                var id=$(this).tabs('getSelected')[0].id;
                setElementHeight('#'+id+' .grid',['.tool'],$(document.body),360);	//计算.grid的高度
                setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
                loadGrid('#'+id+' .grid');
                setWidth('#'+id+' .grid');
            }
        });
    });
    var showStore=function(pro,item){
        window.parent.showStore(pro,item);
    };
</script>
</body>
</html>
