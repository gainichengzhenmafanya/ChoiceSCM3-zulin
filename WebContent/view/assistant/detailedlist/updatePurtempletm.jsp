<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="sta" name="sta" value="${sta}"/>
		<input type="hidden" id="pk_purtemplet" name="pk_purtemplet" value="${purtempletm.pk_purtemplet}"/>
		<input type="hidden" id="acct" name="acct" value="${purtempletm.acct}"/>
		<form action="<%=path%>/purtemplet/updatePurtempletm.do" id="savePurtempletm" name="savePurtempletm" method="post">
			<div class="bj_head" style="height: 60px;">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="module" /><fmt:message key="gyszmmcs" />:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="vname" id="vname" class="text" value="${purtempletm.vname}"/>
					</div>
					<div class="form-label"><fmt:message key="creation_date" />:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="ddate" id="ddate" class="text" readonly="readonly" value="${purtempletm.ddate}" />
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="summary" />:</div>
					<div class="form-input" >
						<input type="text" name="vmemo" id="vmemo" class="text" style="width: 413px;" value="${purtempletm.vmemo}" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 80px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 40px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 180px;"><fmt:message key="suppliers" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="Reference_resources_price" /></span></td>
	                                <td><span style="width: 120px;"><fmt:message key="remark" /></span></td>
	                                <td><span style="width: 40px;"><fmt:message key="no" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="purtempletd" items="${listPurtempletm}" varStatus="status">
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:80px;"><c:out value="${purtempletd.sp_code}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:140px;" vname="${purtempletd.sp_name}"><c:out value="${purtempletd.sp_name}"/></span></td>
										<td><span style="width:60px;"><c:out value="${purtempletd.sp_desc}"/></span></td>
										<td><span style="width:40px;"><c:out value="${purtempletd.unit}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:180px;" vcode="${purtempletd.delivercode}" vname="${purtempletd.delivername}"><c:out value="${purtempletd.delivername}"/></span></td>
										<td style="background:#F1F1F1;text-align: right;"><span style="width:60px;"><fmt:formatNumber value="${purtempletd.sp_price}" pattern="##.##"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:120px;"><c:out value="${purtempletd.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.sp_code}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.delivercode}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.delivercode}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.nminamt}"/></span></td>
										<td><span style="height:24px;width:40px;"><img style="height:21px;width:15px;" title="上移一行" src="../image/up.png" onclick="up(this)"/>&nbsp;&nbsp;<img style="height:21px;width:15px;" title="下移一行" src="../image/down.png" onclick="down(this)"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 5px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:50px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${purtempletm.creator}"/>
						</div>
						<div class="form-label" style="width: 60px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:80px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${purtempletm.creationtime}" />
						</div>
						<div class="form-label">修改人:</div>
						<div class="form-input" style="width:60px;">
							<input type="text" style="border: 0px;" name="modifier" id="modifier" class="text" readonly="readonly" value="${purtempletm.modifier}"/>
						</div>
						<div class="form-label" style="width: 60px;">修改时间:</div>
						<div class="form-input" style="width:80px;">
							<input type="text" style="border: 0px;" name="modifedtime" id="modifedtime" readonly="readonly" class="text" value="${purtempletm.modifedtime}" />
						</div>
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_sp_code" />
			<input type="hidden" id="selected_code" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTableWithSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
		var validate;
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var selectedrow;
			$(document).ready(function(){
				$("#vname").focus();
				$(document).keydown(function (e) {
				    var doPrevent;
				    if(event.keyCode==83 && event.altKey){//alt+S
				    	if(validate._submitValidate())
				    		updatePurtempletm();
				    }else{
				        doPrevent = false;
				    }

				    if (doPrevent)
				        e.preventDefault();
				}); 
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
		 		//回车换焦点end************************************************************************************************
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()-1));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()-1));
				
				if ($("#sta").val() == "update") {
					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:12,
					widths:[30,90,150,70,50,180,70,130,100,100,100],
					onEdit:$.noop,
					editable:[2,5,6,7],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos == 2 || pos == 5){
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
						if(pos==6){
							var pprice = $.trim(rownum.find('td:eq(6) span').text());
							if(pprice==''){
								pprice=$.trim(rownum.find('td:eq(6) input').val());
							}
							postd.find('span').text(pprice);
						}
						if(pos==7){
							var vmemo = $.trim(rownum.find('td:eq(7) span').text());
							if(vmemo==''){
								vmemo=$.trim(rownum.find('td:eq(7) input').val());
							}
							postd.find('span').text(vmemo);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								  selectedrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="material" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_sp_code',
							                irateflag:1,//取采购单位
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(8) span").text('');
									
									row.find("td:eq(5) span").text('');
									row.find("td:eq(5) span").attr('vname','');
									row.find("td:eq(10) span").text('');
									row.find("td:eq(2) input").focus();
									$("#mMenu").remove();
									return;	
								}
							}
							data['url'] = '<%=path%>/supply/findTop1.do';
							if (data.value.split(".").length>2) {
								data['key'] = 'sp_code';
							}else if(!isNaN(data.value)){
								data['key'] = 'sp_code';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'sp_name';
							}else{
								data['key'] = 'sp_init';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name + sp_desc;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '物资重复添加！',
	 								speed: 1000
	 							});
								row.find("td:eq(2) span input").val('').focus();
								return;
							}
							
							var checksupplier = true;
							var code;
							$.ajax({//查询供应商
								type: "POST",
								url: "<%=path%>/material/findDeliverByMaterial.do",
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(res){
									if(res==''){
										checksupplier = false;
										alerterror('物资无供应商！');
										row.find("td:eq(2) span input").val('').focus();
									}else{
										var qrysp = res[0];
										row.find("td:eq(5) span").attr('vname',qrysp.des);
										row.find("td:eq(9) span").text(qrysp.code);
										row.find("td:eq(10) span").text(qrysp.code);
										code=qrysp.code;
										//供应商
										var sele = $("<select style='width:190px;'></select>");
										for(var i=0,len=res.length;i<len;i++){
											var obj = res[i];
											opt = $("<option value='"+obj.code+"'>"+obj.des+"</option>");
											sele.append(opt);
										}
										row.find("td:eq(5)").html(sele);
									}
								}
							});
							
							if(checksupplier){
								row.find("td:eq(1) span").text(data2.sp_code).css("text-align","left");
								row.find("td:eq(2) span").text(data2.sp_name);
								row.find("td:eq(2) span").attr('vname',data2.sp_name);
								row.find("td:eq(3) span").text(data2.sp_desc);
								row.find("td:eq(4) span").text(data2.unit);
								row.find("td:eq(6) span").text('').css("text-align","right");
								row.find("td:eq(7) span").text(data2.vmemo);
								row.find("td:eq(8) span").text(data2.sp_code);
								row.find("td:eq(11) span").text(data2.nminamt);
							<%-- 	$.ajax({
									type: "POST",
									url: "<%=path%>/material/findMaterialNpricesupplier.do?code="+code,
									data: "sp_code="+data2.sp_code,
									dataType: "json",
									success:function(data4){
										if(data4.sp_price == 0){
											row.find("td:eq(6) span").text(0.00).css("text-align","right");
										}else{
											row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
										}
									}
								}); --%>
								$.fn.autoGrid.setCellEditable(row,6);
							}else{
								$.fn.autoGrid.setCellEditable(row,2);
							}
						}
					},{
						index:5,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								var sp_code=$.trim(row.find('td:eq(8) span').text());
								  selectedrow = row;
								  selectSupplier({
									basePath:'<%=path%>',
									title:"123",
									height:400,
									width:600,
									callBack:'setSupplierByMaterial',
									domId:'code',
									sp_code:sp_code,
									single:true
								});
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,6);
						},
						onCellEdit:function(event,data,row){
							var sp_code = row.find("td:eq(8) span").text();
							if(sp_code==''){
								alerterror('<fmt:message key="please_enter" /><fmt:message key="material" />');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(5) span").attr('vname','');
									row.find("td:eq(9) span").text('');
									row.find("td:eq(10) span").text('');
									row.find("td:eq(5) input").focus();
									$("#mMenu").remove();
									return;	
								}
							}
							data['url'] = '<%=path%>/material/findDeliverByMaterial.do?sp_code='+sp_code;
							if (data.value.split(".").length>2) {
								data['key'] = 'delivercode';
							}else if(!isNaN(data.value)){
								data['key'] = 'delivercode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'delivername';
							}else{
								data['key'] = 'deliverinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.code+'-'+data.des;
						},
						afterEnter:function(data2,row){
							row.find("td:eq(5) span").text(data2.des);
							row.find("td:eq(5) span").attr('vname',data2.des);
							row.find("td:eq(9) span").text(data2.code);
							row.find("td:eq(10) span").text(data2.code);
							var sp_code = $.trim(row.find("td:eq(8) span").text());
// 							$.ajax({
// 								type: "POST",
// 								url: "/material/findMaterialNpricesupplier.do?code="+data2.code,
// 								data: "sp_code="+sp_code,
// 								dataType: "json",
// 								success:function(data4){
// 									if(data4.sp_price == 0){
// 										row.find("td:eq(6) span").text(0.00).css("text-align","right");
// 									}else{
// 										row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
// 									}
// 								}
// 							});
							$.fn.autoGrid.setCellEditable(row,6);
						}
					},{
						index:6,
						action:function(row,data){
							row.find("td:eq(6) span").text(data.value);
							$.fn.autoGrid.setCellEditable(row,7);
						},
						onCellEdit:function(event,data,row){
							if(isNaN(data.value)){
								row.find("td:eq(6) span input").val("").focus();
								alerterror('<fmt:message key="price_be_not_number" />');
							}
						}
					},{
						index:7,
						action:function(row,data){
							row.find("td:eq(7) span").text(data.value);
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			}
			
			//重新修改供应商  并查找新的价格
			function setSupplierByMaterial(data){
				var delivercode = data.delivercode;
				var delivername = data.delivername;
				if(delivercode==''){
					return;
				}
				var sp_code = $.trim(selectedrow.find("td:eq(8) span").text());
				selectedrow.find("td:eq(5) span").attr('vname',delivername);
				selectedrow.find("td:eq(5) span").text(delivername);
				selectedrow.find("td:eq(9) span").text(delivercode);
				selectedrow.find("td:eq(10) span").text(delivercode);
				$.fn.autoGrid.setCellEditable(selectedrow,6);
			}
			
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="module" /><fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="module" /><fmt:message key="name" /><fmt:message key="name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="remark" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function updatePurtempletm(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2 && pos!=7){
						$(this).find('input').trigger('onEnter');
					}
				});
				var checkcnt = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var sp_code=$.trim($(this).find("td:eq(8) span").text());
					if(sp_code==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
					}
				});
				if(!checkcnt){
					savelock = false;
					return;
				}
				
				var keys = ["sp_code","sp_name","sp_desc","unit","delivername","sp_price","vmemo","sp_code","delivercode","delivercode","nminamt"];
				var data = {};
				var i = 0;
				data["vname"] = $.trim(stripscript($("#vname").val()));
				data["ddate"] = $("#ddate").val();
				data["vmemo"] = $("#vmemo").val();
				data["pk_purtemplet"] = $("#pk_purtemplet").val();
				data["acct"] = $("#acct").val();
				
				if(data.sp_name==''){
					alerterror('请输入模板名称！');
					savelock = false;
					return;
				}
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var sp_code=$.trim($(rows[i]).find('td:eq(8)').text());
					if(sp_code==''){
						alerterror('请选择物资！');
						savelock = false;
						return;
					}
				}
				var checkdata = true;
				for(i=0;i<rows.length;i++){
					var status=$.trim($(rows[i]).find('td:eq(6)').text());
					if(isNaN(status)){
						alerterror('第'+(i+1)+'行单价未定义');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						checkdata = false;
						break;
					}
					data["listPurtempletd["+i+"]."+"sp_price"] = $(rows[i]).data('sp_price') ? $(rows[i]).data('sp_price'):$('#sp_price').val();
					cols = $(rows[i]).find("td");
					var vmname = $.trim($(rows[i]).find("td:eq(2)").text());
					if(vmname==''){
						alerterror('第'+(i+1)+'行未选择物资！');
						checkdata = false;
						break;
					}
					var code=$.trim($(rows[i]).find("td:eq(5) select option:selected").text())||$.trim($(rows[i]).find("td:eq(5) span").text());
					if(code==''){
						alerterror('第'+(i+1)+'未选择供应商！');
						savelock = false;
						return;
					}
					var j = 0;
					data["listPurtempletd["+i+"].isort"] = $(rows[i]).find("td:first").find('span').text();
					//判断备注的长度
					var vmemocontent=$.trim($(rows[i]).find('td:eq(7) span').text());
					vmemocontent = vmemocontent ? vmemocontent : $.trim($(rows[i]).find("td:eq(7) input").val());	
					if(vmemocontent.length>100){
						alerterror('第'+(i+1)+'行备注长度不能大于100');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						checkdata = false;
						savelock = false;
						break;
						return;
					}
					for(j=1;j <= keys.length;j++){
						if(keys[j]=="delivercode"){
							var dname = $.trim($(rows[i]).find("td:eq(5) select option:selected").text())||$.trim($(rows[i]).find("td:eq(5) span").attr('vname'));
							var dcode = $.trim($(rows[i]).find("td:eq(5) select").val())||$.trim($(rows[i]).find("td:eq(5) span").attr('vcode'));
							data["listPurtempletd["+i+"].delivercode"] = dcode;
							data["listPurtempletd["+i+"].delivername"] = dname;
							continue;
						}
						if(keys[j]=="delivername"){
							continue;
						}
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value)
							data["listPurtempletd["+i+"]."+keys[j-1]] = value;
					}
				}
				
				if(!checkdata){
					savelock = false;
					return;
				}
				if(!checkName()){
					alerterror('模板名称已存在！');
					savelock = false;
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/purtemplet/updatePurtempletm.do?sta="+$('#sta').val(),data,function(data){
					var rs = data;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			function up(obj) {
				var objParentTR = $(obj).closest('tr');
				var prevTR = objParentTR.prev();
				var nextTR = objParentTR.next();
				if (prevTR.length > 0) {
					var curNum = $.trim(objParentTR.find('td:first').find('span').html());
					curNum = Number(curNum);
					prevTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
					objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum-1)+'</span>');
					
					if(nextTR.length==0){//最后一行
						var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
						objParentTR.find('td:last').remove();
						if(curNum==2){//第二条和第一条互换
							$(addCell).appendTo(prevTR);
						}else{
							prevTR.append(addCell);
						}
					}
					prevTR.insertAfter(objParentTR);
					var table = $(obj).closest('table');
					table.closest('div').height(table.height());
				}
				
			}
			function down(obj) {
				var objParentTR = $(obj).closest('tr');
				var nextTR = objParentTR.next();
				var nextnextTR = objParentTR.next().next();
				if (nextTR.length > 0) {
					var curNum = $.trim(objParentTR.find('td:first').find('span').html());
					curNum = Number(curNum);
					nextTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
					objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum+1)+'</span>');
					
					if(nextnextTR.length==0){//和最后一行互换
						var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
						$(addCell).appendTo(objParentTR);
						nextTR.find('td:last').remove();
					}
					nextTR.insertBefore(objParentTR);
					var table = $(obj).closest('table');
					table.closest('div').height(table.height());
				}
			}
			
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			
			function setMaterial(data){
				var flag = true;
				if(data.sp_code==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data.sp_code){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				var checksupplier = true;
				var code;
				$.ajax({//查询供应商
					type: "POST",
					url: "<%=path%>/material/findDeliverByMaterial.do",
					data: "sp_code="+data.sp_code,
					dataType: "json",
					success:function(res){
						if(res==''){
							checksupplier = false;
							alerterror('物资无供应商！');
							selectedrow.find("td:eq(2) span input").val('').focus();
						}else{
							var qrysp = res[0];
							selectedrow.find("td:eq(5) span").text(qrysp.des);
							selectedrow.find("td:eq(5) span").attr('vname',qrysp.des);
							selectedrow.find("td:eq(10) span").text(qrysp.code);
							code = qrysp.code;
						}
					}
				});
				
				if(checksupplier){
					selectedrow.find("td:eq(1) span").text(data.sp_code).css("text-align","right");
					selectedrow.find("td:eq(2) span").text(data.sp_name);
					selectedrow.find("td:eq(2) span").attr('vname',data.sp_name);
					selectedrow.find("td:eq(3) span").text(data.sp_desc);
					selectedrow.find("td:eq(4) span").text(data.unit3);
					selectedrow.find("td:eq(6) span").text('').css("text-align","right");
					selectedrow.find("td:eq(7) span").text(data.memo);
					selectedrow.find("td:eq(8) span").text(data.sp_code);
					selectedrow.find("td:eq(11) span").text(data.nminamt);
					$.fn.autoGrid.setCellEditable(selectedrow,5);
				}else{
					$.fn.autoGrid.setCellEditable(selectedrow,2);
				}
			}
			
			function setSupplier(data){
				if(data==''){
					return;
				}
				selectedrow.find("td:eq(5) span").text(data.vname);
				selectedrow.find("td:eq(10) span").text(data.vcode);
				$.fn.autoGrid.setCellEditable(selectedrow,6);
			}
			function resetName(e){
				if($('#mMenu').length!=0){
					return;
				}
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==2){//物资
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}else if(curindex==5){//供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}else if(curindex==7){//当为备注时不进入方法
					
				}else{
					var value = $(e).val();
					$(e).closest('span').text(value);
				}
			}
			
			
			//验证模板名称重复
			function checkName(){
				var flag = true;
				var vname = $.trim(stripscript($("#vname").val()));
				var pk_purtemplet = $("#pk_purtemplet").val();
				var param = {};
				param['vname'] = vname;
				param['pk_purtemplet']=pk_purtemplet;
				$.post("<%=path%>/purtemplet/checkName.do",param,function(re){
					if(re=='NOOK'){
						flag = false;
					}
				})
				return flag;
			}

		</script>
	</body>
</html>
