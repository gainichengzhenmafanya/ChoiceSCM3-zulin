<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div style="height:100%;">
			<div class="tool"></div>
			<div>
				<form id="listForm" action="<%=path%>/purtemplet/queryAllPurtempletm.do" method="post">
					<input type="hidden" id="pk_purtemplet" name="pk_purtemplet" value="${pk_purtemplet}"/>
					<div class="search-div" style="positn: absolute;z-index: 99">
						<div class="form-line">
							<div class="form-label">清单名称：</div>
							<div class="form-input">
								<input type="text" id="vname" name="vname" style="text-transform:uppercase;" value="${purtempletm.vname}" onkeyup="ajaxSearch()" class="text"/>
							</div>
						</div>
						<div class="search-commit">
				       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
				       		<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
						</div>
					</div>
					<div class="grid" id="grid">		
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td class="num"><span style="width:30px;">&nbsp;</span></td>
										<td><span style="width:35px;"><input type="checkbox" id="chkAll"/></span></td>
										<td><span style="width:150px;">清单名称</span></td>
										<td><span style="width:300px;">摘要</span></td>
										<td><span style="width:150px;">创建日期</span></td>
			                     	</tr>
								</thead>
							</table>
						</div>				
						<div class="table-body">
							<table cellspacing="0" cellpadding="0" id="tblGrid">
								<tbody>
									<c:forEach var="purtempletm" items="${listPurtempletm}" varStatus="status">
										<tr>
											<td class="num"><span style="width:30px;">${status.index+1}</span></td>
											<td title="${purtempletm.pk_purtemplet}" ><span style="width:35px;text-align: center;"><input type="checkbox" name="idList" id="chk_${purtempletm.pk_purtemplet}" value="${purtempletm.pk_purtemplet}"/></span></td>
											<td title="${purtempletm.vname}" ><span style="width:150px;text-align: left;">${purtempletm.vname}</span></td>
											<td title="${purtempletm.vmemo}" ><span style="width:300px;text-align: left;">${purtempletm.vmemo}</span></td>
											<td title="${purtempletm.ddate}" ><span style="width:150px;text-align: left;">${purtempletm.ddate}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>			
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
				</form>
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
		//防止没有焦点时按后退键退回上个页面
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#listForm').submit();
					parent.$("#mainFrame").submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),26);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				//分页
// 			 	$(".page").css("margin-bottom",16);
			   //光标移动
			   new tabTableInput("tblGrid","text"); 
			   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').live("click", function () {
				     if ($(this).hasClass("bgBlue")) {
				         $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
				     }else{
				         $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
				     }
				}); 	
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			   $('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#sp_code').bind('focus.custom',function(e){
					if(!!!top.customWindow){
						top.customSupply('<fmt:message key="please_select_materials" />','<%=path%>/supply/selectSupplyLeft.do',$('#sp_code'),$('#sp_name'),$('#unit'),$('#unit1'),$('.unit'),$('.unit1'),handler);	
					}
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="enter" />',
							title: 'enter',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {entity:[]};
								checkboxList.filter(':checked').each(function(){
									var entity = {};
									var row = $(this).closest('tr');
									entity.vname = $.trim(row.children('td:eq(2)').text());
									entity.pk_purtemplet = $.trim($(this).val());
									data.entity.push(entity);
								});
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: 'cancel',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
			});	
		</script>
	</body>
</html>