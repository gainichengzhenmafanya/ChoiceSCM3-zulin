<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  		
		<div style="height:50%;">
			<div class="tool"></div>
			<div>
				<input type="hidden" id="pk_purtemplet" name="pk_purtemplet" value="${pk_purtemplet}"/>
				<form id="listForm" action="<%=path%>/purtemplet/queryAllPurtempletm.do" method="post">
					<div class="search-div" style="positn: absolute;z-index: 99">
						<div class="form-line">
							<div class="form-label"><fmt:message key="gyszmmcs" />:</div>
							<div class="form-input">
								<input type="text" id="vname" name="vname" value="${purtempletm.vname}" class="text"/>
							</div>
						</div>
						<div class="search-commit">
				       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
						<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
						</div>
					</div>
					<div class="grid" id="grid" style="overflow: hidden;">		
						<div class="table-head" style="width: 100%;">
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td class="num"><span style="width:30px;">&nbsp;</span></td>
										<td><span style="width:35px;"><input type="checkbox" id="chkAll"/></span></td>
										<td><span style="width:150px;"><fmt:message key="module" /><fmt:message key="gyszmmcs" /></span></td>
										<td><span style="width:300px;"><fmt:message key="summary" /></span></td>
										<td><span style="width:150px;"><fmt:message key="creation_date" /></span></td>
			                     	</tr>
								</thead>
							</table>
						</div>				
						<div class="table-body" style="width: 100%;">
							<table cellspacing="0" cellpadding="0" id="tblGrid">
								<tbody>
									<c:forEach var="purtempletm" items="${listPurtempletm}" varStatus="status">
										<tr>
											<td class="num"><span style="width:30px;">${status.index+1}</span></td>
											<td><span style="width:35px;text-align: center;"><input type="checkbox" name="idList" id="chk_${purtempletm.pk_purtemplet}" value="${purtempletm.pk_purtemplet}"/></span></td>
											<td title="${purtempletm.vname}" ><span style="width:150px;">${purtempletm.vname}</span></td>
											<td title="${purtempletm.vmemo}" ><span style="width:300px;">${purtempletm.vmemo}</span></td>
											<td title="${purtempletm.ddate}" ><span style="width:150px;text-align: center;">${purtempletm.ddate}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>			
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
				</form>
			</div>
		</div>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/purtemplet/queryAllPurtempletd.do?pk_purtempletm=${pk_purtemplet}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//快捷键操作（Alt+*）
				$(document).keydown(function (e) {
				    var doPrevent;
				    if (event.keyCode==78 && event.altKey) {//alt+N
				    	if(!$('#window_supply').html()){
				    		addpurtempletm();
				    	}
				    }else if(event.keyCode==83 && event.altKey){//alt+S
				    	
				    }else if(event.keyCode==85 && event.altKey){//alt+U
				    	if(!$('#window_supply').html()){
				    		updatepurtempletm();
				    	}
				    }else{
				        doPrevent = false;
				    }
				    if (doPrevent)
				        e.preventDefault();
				}); 
				$("#vname").focus();
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$("#vname").val(stripscript($("#vname").val()));
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),75);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				//分页
			 	$(".page").css("margin-bottom",$("body").height()*0.45+$(".page").height()+10);
				//刷新字表
				$('.grid .table-body tr').live('click',function(){
					 $('#pk_purtemplet').val($(this).find('td:eq(1)').find('input').val());
					 var pk_purtemplet=$(this).find('td:eq(1)').find('input').val();
					 var url="<%=path%>/purtemplet/queryAllPurtempletd.do?pk_purtempletm="+pk_purtemplet;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
			   //光标移动
			   new tabTableInput("tblGrid","text"); 
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$("#vname").focus();
							}
						},{
							text: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								addpurtempletm();
							}
						},{
							text: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								updatepurtempletm();
							}
						},{
							text: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								deletepurtempletm();
							}
						},
						//{
						//	text: '<fmt:message key="to_save" /><fmt:message key="PuprOrder" />',
						//	useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'creat')},
						//	icon: {
						//		url: '<%=path%>/image/Button/op_owner.gif',
						//		positn: ['-38px','0px']
						//	},
						//	handler: function(){
						//		savepuprorder();
						//	}
						//},
						{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}]
				});
				
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});	
			//新增采购清单
			function addpurtempletm(){
				var savePurtempletm = $('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="Order_list" />',
					content: '<iframe id="savePurtempletm" frameborder="0" src="<%=path%>/purtemplet/toAddPurtempletm.do?sta=add"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savePurtempletm')&&window.document.getElementById("savePurtempletm").contentWindow.validate._submitValidate()){
// 										$("#wait2").css("display","block");
// 										$("#wait2").css("display","block");
										window.document.getElementById("savePurtempletm").contentWindow.savePurtempletm();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//修改采购清单
			function updatepurtempletm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var updatePurtempletm = $('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="Order_list" />',
						content: '<iframe id="updatePurtempletm" frameborder="0" src="<%=path%>/purtemplet/toUpdatePurtempletm.do?sta=update&pk_purtemplet='+chkValue+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatePurtempletm')&&window.document.getElementById("updatePurtempletm").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatePurtempletm").contentWindow.updatePurtempletm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除
			function deletepurtempletm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/purtemplet/deletePurtempletm.do?pks='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" /><fmt:message key="Order_list" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			function reloadPage(){
				$("#listForm").submit();
			}
			//生成采购订单
			function savepuprorder(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="to_save" /><fmt:message key="PuprOrder" />',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/puprorder/toSavePuprorder.do?pks='+chkValue.join(",")+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										window.document.getElementById("updatepuprorder").contentWindow.savepuprorder();
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else{
					alerterror('<fmt:message key="please_select" /><fmt:message key="Order_list" />！');
					return ;
				};
			}
		    document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
		</script>
	</body>
</html>