<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/puprorder/toSavePuprorder.do" id="listForm" name="listForm" method="post">
		<input type="hidden" id="sta" name="sta" value="add"/>
		<input type="hidden" id="listSupplierCode" name="listSupplierCode" value="${listSupplierCode}"/>
		<input type="hidden" id="listSupplierName" name="listSupplierName" value="${listSupplierName}"/>
		<input type="hidden" id="ibillfrom" name="ibillfrom" value="${puprOrder.ibillfrom}"/>
		<input type="hidden" id="pk_puprorder" name="pk_puprorder" value="${puprOrder.pk_puprorder}"/>
		<input type="hidden" id="vpurchaser" name="vpurchaser" value="${puprOrder.vpurchaser}"/>
			<input type="hidden" id="pks" name="pks" value="${pks}"/>
			<div class="bj_head">
					<div class="form-label" style="display: none;">订单单号:</div>
					<div class="form-input" style="width:100px;display: none;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${puprOrder.vbillno}"/>
					</div>
					<div class="form-label" style="width: 10%;margin-left: 35px;display: none;">单据日期:</div>
					<div class="form-input" style="width:100px;display: none;">
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate" style="text-transform:uppercase;" value="${puprOrder.dbilldate}"  class="Wdate text"/>
					</div>	
				<div class="form-line">
					<div class="form-label" style="display: none;">制&nbsp;单&nbsp;人:</div>
					<div class="form-input" style="width:100px;display: none;">
						<input type="hidden" name="pk_account" id="pk_account" class="text" value="${puprOrder.pk_account}" />
						<input type="text" name="accountname" id="accountname" class="text" readonly="readonly" value="${puprOrder.accountname}"/>
					</div>
					<div class="form-label" style="width: 10%;margin-left: 35px;display: none;">制单时间:</div>
					<div class="form-input" style="width:100px;display: none;">
						<input type="text" name="dmaketime" id="dmaketime" readonly="readonly" class="text" value="${puprOrder.dmaketime}" />
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;"><span class="red">*</span>采购组织:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="positncode" id="positncode" class="text" value="${puprOrder.positncode}"/>
						<input type="text" name="positnname" id="positnname" class="text" value="${puprOrder.positnname}"/>
						<img id="positncodebutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 						<input type="button" id="positncodebutton" name="positncodebutton" value="查询" style="width:30px;height: 20px;"/> -->
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span>要求到货日:</div>
					<div class="form-input" style="width:100px;">
						<input autocomplete="off" type="text" id="dhopedate" name="dhopedate" style="text-transform:uppercase;" value="${puprOrder.dhopedate}"  class="Wdate text"/>
					</div>	
					<div class="form-label" style="width: 10%;margin-left: 35px;">备&nbsp;&nbsp;&nbsp;&nbsp;注:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" style="width: 355px;" name="vmemo" id="vmemo" class="text" value="${puprOrder.vmemo}"/>
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 80px;">物资编码</span></td>
	                                <td><span style="width: 140px;">物资名称</span></td>
	                                <td><span style="width: 60px;">规格</span></td>
	                                <td><span style="width: 50px;">单位</span></td>
	                                <td><span style="width: 60px;">数量</span></td>
	                                <td><span style="width: 60px;">单价</span></td>
	                                <td><span style="width: 60px;">金额</span></td>
	                                <td><span style="width: 140px;">供应商</span></td>
	                                <td><span style="width: 100px;">备注</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
								<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
								<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
								<c:set var="sum_priceamt" value="${0}"/>  <!-- 总陈本金额 -->
								<c:forEach var="purtempletd" items="${listPurtempletd}" varStatus="status">
									<c:set var="sum_num" value="${status.index+1}"/>  
									<c:set var="sum_amount" value="${sum_amount + purtempletd.cnt}"/>  
									<c:set var="sum_totalamt" value="${sum_totalamt + purtempletd.cnt}"/>  
									<c:set var="sum_priceamt" value="${sum_priceamt + purtempletd.cnt*purtempletd.sp_price}"/>  
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:80px;"><c:out value="${purtempletd.sp_code}"/></span></td>
										<td><span style="width:140px;background:#F1F1F1;" vname="${purtempletd.sp_name}"><c:out value="${purtempletd.sp_name}"/></span></td>
										<td><span style="width:60px;"><c:out value="${purtempletd.sp_desc}"/></span></td>
										<td><span style="width:50px;"><c:out value="${purtempletd.unit}"/></span></td>
										<td><span style="width:60px;text-align: right;background:#F1F1F1;">0</span></td>
										<td 
										<c:if test="${purtempletd.beditprice==1}">edit='false'</c:if>
										<c:if test="${purtempletd.beditprice==0}">style="background:#F1F1F1;"</c:if>
										><span style="width:60px;text-align: right;"><fmt:formatNumber value="${purtempletd.sp_price}" pattern="0.00"/></span></td>
										<td><span style="width:60px;text-align: right;">0</span></td>
										<td><span style="width:140px;background:#F1F1F1;" vname="${purtempletd.delivername}"><c:out value="${purtempletd.delivername}"/></span></td>
										<td><span style="width:100px;"><c:out value="${purtempletd.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.sp_code}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.unit}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.delivercode}"/></span></td>
										<td style="display: none;"><span><c:out value="${purtempletd.delivercode}"/></span></td>
										<td style="text-align: right;display: none;"><span><c:out value="${purtempletd.nminamt}"/></span></td>
										<td style="text-align: right;display: none;"><span><fmt:formatNumber value="${purtempletd.sp_price}" pattern="0.00"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="hidden" name="pk_account" id="pk_account" class="text" value="${puprOrder.pk_account }" />
							<input type="text" style="border: 0px;" name="accountname" id="accountname" class="text" readonly="readonly" value="${puprOrder.accountname}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="dmaketime" id="dmaketime" readonly="readonly" class="text" value="${puprOrder.dmaketime }" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="0.0" />
						</div>	
					</div>
				</div>	
			</form>
			<input type="hidden" id="nmoney" name="nmoney" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$("#delivername").focus();
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			function setPk_org(data){
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
				
				$("#listForm").submit();
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
				
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				$("#dbilldate").click(function(){
					new WdatePicker();
				});
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dbilldate\')}'});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
			    	setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+200));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+200));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "add") {
					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:18,
					widths:[16,90,150,70,60,70,70,70,150,110,110,110,110,110,110,110,110,110,110],
					colStyle:['','',{background:"#F1F1F1"},'','','',{background:"#F1F1F1"},'','',{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6,8,9],
// 					onLastClick:function(row){
// 						$('#sum_num').text(Number($('#sum_num').text())-1);//总行数
// 						$('#sum_amount').text(Number($('#sum_amount').text())-row.find('td:eq(5)').text());//总数量
// 						$('#sum_price').text(Number(Number($('#sum_price').text())-Number(row.find('td:eq(7)').text())).toFixed(2));//总金额
// 					},
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos == 2){
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
						if(pos == 5){
							var cgnum = $.trim(rownum.find("td:eq(5) span").text());
							if(cgnum==''){
								cgnum = $.trim(rownum.find("td:eq(5) input").val());
								rownum.find("td:eq(5) span").text(cgnum);
							}
							var mincnt = findNmincntByRate();
							if(isNaN(cgnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
							}else if(Number(cgnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
							}else if(Number(cgnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
							}else{
								var nmoney = Number($("#nmoney").val());
								var prerowmoney = Number($.trim(rownum.find("td:eq(7) span").text())).toFixed(2);
								
								if(Number(cgnum)<Number(mincnt)){
									var price = Number($.trim(rownum.find("td:eq(6) span").text()));
									alerterror('第'+rownum.find('td:first').text()+'行<fmt:message key="purchase_quantity"/><fmt:message key="can_not_be_less_than"/><fmt:message key="Minimum_Purchase_Amount"/>'+Number(mincnt));
									rownum.find("td:eq(5) span").text(Number(mincnt).toFixed(2));
									rownum.find("td:eq(7) span").text(Number(price*mincnt).toFixed(2));
									$.fn.autoGrid.setCellEditable(rownum,5);
									
									var diffmoney = Number(price*mincnt)-prerowmoney;
									
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(rownum,6);
								}else{
									$.fn.autoGrid.setCellEditable(rownum,6);
								}
							}
							var num = $.trim(rownum.find("td:eq(6) span").text());
							if(isNaN(num)){
								rownum.find("td:eq(6) span").text(0.0);
							} else if(Number(num)<0){
								alerterror('<fmt:message key="unitprice_cannot_be_negative"/>');
							}
						}
						function findNmincntByRate(){
							var cnt = 0;
							$.ajaxSetup({ 
								  async: false 
							});
							var sp_code = $.trim(rownum.find("td:eq(10) span").text());
							var param = {};
							param['sp_code'] = sp_code;
							$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
								cnt = Number(re);
							})
							return cnt;
						}
						if(pos == 6){
							var num = rownum.find("td:eq(6) span").text();
							if(num==''){
								num = $.trim(rownum.find("td:eq(6) input").val());
								rownum.find("td:eq(6) span").text(num);
							}
							if(isNaN(num)){
								alerterror('<fmt:message key="unitprice_be_not_valid_number" />！');
							} else if(isNaN(rownum.find('td:eq(6) span').text())){
								rownum.find("td:eq(7) span").text(0.0);
							}else{
								rownum.find("td:eq(7) span").text(Number(Number(rownum.find("td:eq(5) span").text())*Number(rownum.find('td:eq(6) span').text())));
							}
							return;
						}
						if(pos == 7){
							if(isNaN(rownum.find("td:eq(7) span").text())){
								alerterror('<fmt:message key="price_be_not_number" />！');
							} else if(Number(rownum.find("td:eq(5) span").text()) != 0.0){
								rownum.find("td:eq(6) span").text(Number(rownum.find("td:eq(7) span").text()) / Number(rownum.find("td:eq(5) span").text()));
							}
						}
						if(pos == 8){
							var postd = data.curobj.closest('td');
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								  selectedrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_sp_code',
							                positncode:$('#positncode').val(),//采购组织
							                irateflag:1,//取采购单位
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							var prerowmoney = row.find("td:eq(7) span").text();
							if(prerowmoney!=''){
								prerowmoney = Number(prerowmoney);
							}else{
								prerowmoney = 0 ;
							}
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(2) span").text('');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(5) span").text('');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(7) span").text('');
									row.find("td:eq(8) span").text('');
									row.find("td:eq(9) span").text('');
									$("#mMenu").remove();
									$("#nmoney").val(Number(Number($("#nmoney").val())-Number(prerowmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(row,2);
									return;	
								}
							}
							
							if($('#positnname').val() == null || $('#positnname').val() == ''){
								row.find("td:eq(2) span input").val('').focus();
								alerterror('请先选择采购组织！');
								return;
							}
							data['url'] = '<%=path%>/material/findTop1.do?sp_position='+$("#positncode").val();
							if (data.value.split(".").length>2) {
								data['key'] = 'sp_code';
							}else if(!isNaN(data.value)){
								data['key'] = 'sp_code';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'sp_name';
							}else{
								data['key'] = 'sp_init';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name + sp_desc;
						},
						afterEnter:function(data2,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(7) span").text())).toFixed(2);
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '物资重复添加！',
	 								speed: 1000
	 							});
								return;
							}
							
							var checksupplier = true;
							$.ajax({//查询供应商
								type: "POST",
								url: "<%=path%>/material/findDeliverByMaterial.do",
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(res){
									if(res==''){
										checksupplier = false;
										alerterror('物资无供应商！');
										row.find("td:eq(2) span input").val('').focus();
									}else{
										var qrysp = res[0];
										row.find("td:eq(8) span").text(qrysp.des);
										row.find("td:eq(12) span").text(qrysp.code);
										row.find("td:eq(13) span").text(qrysp.code);
									}
								}
							});
							
							if(checksupplier){
								row.find("td:eq(1) span").text(data2.sp_code);
								row.find("td:eq(2) span").text(data2.sp_name);
								row.find("td:eq(2) span").attr('vname',data2.sp_name);
								row.find("td:eq(3) span").text(data2.sp_desc);
								row.find("td:eq(4) span").text(data2.unitvname);
								
								row.find("td:eq(6) span").text('').css("text-align","right");
								row.find("td:eq(7) span").text('0.00').css("text-align","right");
								row.find("td:eq(8) span").text(data2.delivername);
								row.find("td:eq(8) span").attr('vname',data2.delivername);
								row.find("td:eq(9) span").text('');
								row.find("td:eq(10) span").text(data2.sp_code);
								row.find("td:eq(11) span").text(data2.unit);
								row.find("td:eq(12) span").text(data2.delivercode);
								row.find("td:eq(13) span").text(data2.delivercode);
								row.find("td:eq(14) span").text(data2.nminamt);
								row.find("td:eq(15) span").text(data2.sp_price);
								var cnt = 0;
								$.ajaxSetup({ 
									  async: false 
								});
								var param = {};
								param['sp_code'] = data2.sp_code;
								$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
									cnt = Number(re);
									row.find("td:eq(5) span").text(Number(cnt).toFixed(2)).css("text-align","right");
									row.find("td:eq(14) span").text(Number(cnt).toFixed(2));
								})
								var delivercode = row.find("td:eq(13) span").text();
								$.ajax({//查询物资
									type: "POST",
									url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val()+"&delivercode="+delivercode,
									data: "sp_code="+data2.sp_code,
									dataType: "json",
									success:function(data3){
										if(data3.sp_price == 0){
											row.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
											row.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
											$.ajax({
												type: "POST",
												url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+delivercode,
												data: "sp_code="+data2.sp_code,
												dataType: "json",
												success:function(data4){
													if(data4.sp_price == 0){
														row.find("td:eq(6) span").text('0.00').css("text-align","right");
														var nrowmoney = 0;
														var diffmoney = Number(nrowmoney)-prerowmoney;
														row.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
														$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
													}else{
														row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
														
														var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
														row.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
														$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
													}
												}
											})
										}else{
											row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
											//-----------
											row.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
											row.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
											//-----------
											var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
											row.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
											$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
										}
									}
								});
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								$.fn.autoGrid.setCellEditable(row,2);
							}
						}
					},{
						index:5,
						action:function(row,data2){
							if(Number(data2.value) == 0){
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								alerterror('数量不能为负数！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(isNaN(data2.value)){
								alerterror('<fmt:message key="number_be_not_number"/>！');
								row.find("td:eq(5)").find('span').text(data2.ovalue);
								$.fn.autoGrid.setCellEditable(row,5);
							}else {
								var price = row.find('td:eq(6)').find('span').text();
								if(price==''){
									price = row.find('td:eq(6)').find('input').val();
								}
								row.find("td:eq(7)").find('span').text(Number(Number(data2.value)*Number(price)).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,6);
							}
						},
						onCellEdit:function(event,data,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = row.find("td:eq(7) span").text();
							if(prerowmoney!=''){
								prerowmoney = Number(prerowmoney);
							}else{
								prerowmoney = 0 ;
							}
							if($('#positnname').val() == null || $('#positnname').val() == ''){
								alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
								row.find("td:eq(5) span").text('');
								return;
							}
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								row.find('td:eq(7) span').text('0.00');
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								cnt = Number($.trim(cnt));
								var price = row.find("td:eq(6) span").text();
								if(isNaN($.trim(price))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									price = Number($.trim(price));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(7) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(isNaN(row.find('td:eq(6) span').text())){
								row.find("td:eq(7) span").text(0.0);
							}else{
								row.find("td:eq(7) span").text(Number(Number(row.find("td:eq(5) span").text())*Number(row.find('td:eq(6) span').text())).toFixed(2));
							}
							$.fn.autoGrid.setCellEditable(row,8);
						},
						onCellEdit:function(event,data,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(7) span").text())).toFixed(2);
							var price = data.value;
							if(isNaN($.trim(price))){
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								row.find("td:eq(6) span").text('0.00');
								row.find("td:eq(7) span").text('0.00');
							}else{
								price = Number($.trim(price));
								var cnt = row.find("td:eq(5) span").text();
								if(isNaN($.trim(cnt))){
									$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
									row.find('td:eq(6) span').text('0.00');
								}else{
									cnt = Number($.trim(cnt));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(7) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:8,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								var sp_code = row.find("td:eq(10) span").text();
								if(sp_code==''){
									alerterror('<fmt:message key="please_enter" /><fmt:message key="material" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								  selectedrow = row;
								  selectSupplier({
									basePath:'<%=path%>',
									title:"123",
									height:400,
									width:600,
									callBack:'setSupplierByMaterial',
									domId:'delivercode',
									sp_code:sp_code,
									single:true
								});
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,9);
						},
						onCellEdit:function(event,data,row){
							var sp_code = row.find("td:eq(10) span").text();
							if(sp_code==''){
								alerterror('<fmt:message key="please_enter" /><fmt:message key="material" />');
								return;
							}
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(8) span").text('');
									row.find("td:eq(8) span").attr('vname','');
									row.find("td:eq(12) span").text('');
									row.find("td:eq(13) span").text('');
									$("#mMenu").remove();
									$.fn.autoGrid.setCellEditable(row,8);
									return;	
								}
							}
							data['url'] = '<%=path%>/material/findDeliverByMaterial.do?sp_code='+sp_code;
							if (data.value.split(".").length>2) {
								data['key'] = 'delivercode';
							}else if(!isNaN(data.value)){
								data['key'] = 'delivercode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'delivername';
							}else{
								data['key'] = 'deliverinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.code+'-'+data.des;
						},
						afterEnter:function(data2,row){
							row.find("td:eq(8) span").text(data2.des);
							row.find("td:eq(8) span").attr('vname',data2.des);
							row.find("td:eq(12) span").text(data2.code);
							row.find("td:eq(13) span").text(data2.code);
							var sp_code = row.find("td:eq(10) span").text();
							var cnt = row.find("td:eq(5) span").text();
							$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val()+"&delivercode="+data2.code,
								data: "sp_code="+sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										row.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
										row.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
										$.ajax({
											type: "POST",
											url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+data2.delivercode,
											data: "sp_code="+sp_code,
											dataType: "json",
											success:function(data4){
												if(data4.sp_price == 0){
													row.find("td:eq(6) span").text('0.00').css("text-align","right");
												}else{
													row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
													
													var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
													row.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
													$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
												}
											}
										})
									}else{
										row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
										//-----------
										row.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
										row.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
										//-----------
										var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
										row.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
										$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
									}
								}
							});
							$.fn.autoGrid.setCellEditable(row,9);
						}
					},{
						index:9,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
						}
					}]
				});
			}
			var savelock = false;
			function savepuprorder(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				var positncode = $("#positncode").val();
				if(positncode==''){
					alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
					savelock = false;
					return;
				}
				if($('#vmemo').val().length>100){
					alerterror('备注长度不能大于100！');
					savelock = false;
					return;
				}
				
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var sp_code=$.trim($(this).find("td:eq(10) span").text());
					if(sp_code==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
					}
				});
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2 && pos!=9){
						$(this).find('input').trigger('onEnter');
					}
				});
				var keys = ["sp_code","sp_name","sp_desc","unit","ncnt","nprice","nmoney","delivername","vmemo","pk_material","unit","delivercode","delivercode","nminamt"];
				var data = {};
				var i = 0;
				data["vbillno"] = $("#vbillno").val();
				data["dbilldate"] = $("#dbilldate").val();
				data["delivercode"] = $("#delivercode").val();
				data["pk_account"] = $("#pk_account").val();
				data["dmaketime"] = $("#dmaketime").val();
				data["istate"] = 1;
				data["positncode"] = $("#positncode").val();
				data["positnname"] = $("#positnname").val();
				data["listcode"] = $("#listcode").val();
				data["listSupplierCode"] = $("#listSupplierCode").val();
				data["listSupplierName"] = $("#listSupplierName").val();
				data["dhopedate"] = $("#dhopedate").val();
				data["vmemo"] = $("#vmemo").val();
				data["vpurchaser"] = $("#vpurchaser").val();
				data["pks"] = $("#pks").val();
				var rows = $(".grid .table-body table tr");
				var checknum = true;
				if(rows.length==1){
					var sp_code=$.trim($(rows[0]).find("td:eq(10) span").text());
					if(sp_code==''){
						alerterror('至少选择一条物资！');
						savelock = false;
						return;
					}
				}
				if($("#dhopedate").val() == null || $("#dhopedate").val() == ''){
					alerterror('请选择到货日期!');
					savelock = false;
					return;
				}
				for(i=0;i<rows.length;i++){
					var vmname = $.trim($(rows[i]).find("td:eq(1)").text());
					if(vmname==''){
						alerterror('第【'+(i+1)+'】行未选择物资！');
						checknum = false;
						savelock = false;
						return;
					}
					var delivercode = $.trim($(rows[i]).find("td:eq(13) span").text());
					if(delivercode==''){
						alerterror('第【'+(i+1)+'】行未选择供应商！');
						checknum = false;
						savelock = false;
						return;
					}
					//判断备注的长度
					var vmemocontent=$.trim($(rows[i]).find('td:eq(9) span').text());
					vmemocontent = vmemocontent ? vmemocontent : $.trim($(rows[i]).find("td:eq(9) input").val());	
					if(vmemocontent.length>100){
						alerterror('第'+(i+1)+'行备注长度不能大于100');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						checkdata = false;
						savelock = false;
						break;
						return;
					}
					var number = $.trim($(rows[i]).find('td:eq(5) span').text());
					if(number==''){
						number = $.trim($(rows[i]).find('td:eq(5) input').val());
					}
					if(number=='' || isNaN(number)){
						alerterror('第【'+(i+1)+'】行数量不正确！');
						checknum = false;
						savelock = false;
						return;
					}else if(Number(number)==0){
						alerterror('第【'+(i+1)+'】行数量不能为0！');
						checknum = false;
						savelock = false;
						return;
					}
					var status=$(rows[i]).find('td:eq(0)').text();
					data["puprOrderdList["+i+"]."+"nprice"] = $(rows[i]).data('nprice') ? $(rows[i]).data('nprice'):$('#nprice').val();
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value){
							data["puprOrderdList["+i+"]."+keys[j-1]] = value;
							if(j==6){//保存价格是否为合约价
								var beditprice = $(rows[i]).find("td:eq(6)").attr('edit');
								data["puprOrderdList["+i+"].beditprice"] = (beditprice=='false'?1:0);
							}
						}
					}
				}
				if(!checknum){
					savelock = false;
					return;	
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/puprorder/addPuprorder.do?sta="+$('#sta').val(),data,function(data){
					var rs = eval('('+data+')');
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			function setMaterial(data){
				var flag = true;
				if(data.sp_code==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data.sp_code){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				
							
				var checksupplier = true;
				$.ajax({//查询供应商
					type: "POST",
					url: "<%=path%>/material/findDeliverByMaterial.do",
					data: "sp_code="+data.sp_code,
					dataType: "json",
					success:function(res){
						if(res==''){
							checksupplier = false;
							alerterror('物资无供应商！');
							selectedrow.find("td:eq(2) span input").val('').focus();
						}else{
							var qrysp = res[0];
							selectedrow.find("td:eq(8) span").text(qrysp.des);
							selectedrow.find("td:eq(8) span").attr('vname',qrysp.des);
							selectedrow.find("td:eq(13) span").text(qrysp.code);
						}
					}
				});
				
				if(checksupplier){
					selectedrow.find("td:eq(1) span").text(data.sp_code);
					selectedrow.find("td:eq(2) span").text(data.sp_name);
					selectedrow.find("td:eq(2) span").attr('vname',data.sp_name);
					selectedrow.find("td:eq(3) span").text(data.sp_desc);
					selectedrow.find("td:eq(4) span").text(data.unit);
					
					selectedrow.find("td:eq(6) span").text('').css("text-align","right");
					selectedrow.find("td:eq(7) span").text('0.00').css("text-align","right");
					selectedrow.find("td:eq(9) span").text('');
					selectedrow.find("td:eq(10) span").text(data.sp_code);
					selectedrow.find("td:eq(11) span").text(data.unit3);
					selectedrow.find("td:eq(12) span").text(data.delivercode);
					
					selectedrow.find("td:eq(15) span").text(data.sp_price);
					var cnt = 0;
					$.ajaxSetup({ 
						  async: false
					});
					var param = {};
					param['sp_code'] = data.sp_code;
					$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
						cnt = Number(re);
						selectedrow.find("td:eq(5) span").text(Number(cnt).toFixed(2)).css("text-align","right");
						selectedrow.find("td:eq(14) span").text(Number(cnt).toFixed(2));
					})
					var delivercode = selectedrow.find("td:eq(13) span").text();
					$.ajax({//查询物资
						type: "POST",
						url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val()+"&delivercode="+delivercode,
						data: "sp_code="+data.sp_code,
						dataType: "json",
						success:function(data3){
							if(data3.sp_price == 0){
								selectedrow.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
								selectedrow.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
								$.ajax({
									type: "POST",
									url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+delivercode,
									data: "sp_code="+data.sp_code,
									dataType: "json",
									success:function(data4){
										if(data4.sp_price == 0){
											selectedrow.find("td:eq(6) span").text('0.00').css("text-align","right");
										}else{
											selectedrow.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
											
											var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
											selectedrow.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
											$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
										}
									}
								})
							}else{
								selectedrow.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
								//-----------
								selectedrow.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
								selectedrow.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
								//-----------
								var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
								selectedrow.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
								$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
							}
						}
					});
					$.fn.autoGrid.setCellEditable(selectedrow,5);
				}else{
					$.fn.autoGrid.setCellEditable(selectedrow,2);
				}
			}
			
			//重新修改供应商  并查找新的价格
			function setSupplierByMaterial(data){
				var positncode = $("#positncode").val();
				var delivercode = data.delivercode;
				var delivername = data.delivername;
				if(delivercode==''){
					return;
				}
				var sp_code = $.trim(selectedrow.find("td:eq(10) span").text());
				var recnt = selectedrow.find("td:eq(5) span").text();
				selectedrow.find("td:eq(8) span").attr('vname',delivername);
				selectedrow.find("td:eq(8) span").text(delivername);
				selectedrow.find("td:eq(12) span").text(delivercode);
				selectedrow.find("td:eq(13) span").text(delivercode);
				var param = {};
				param['positncode'] = positncode;
				param['sp_code'] = sp_code;
				param['delivercode'] = delivercode;
				$.post('<%=path%>/material/findMaterialNprice.do',param,function(re){
					var reninprice = re.sp_price;
					if(reninprice!=0){
						selectedrow.find("td:eq(6) span").text(reninprice);
						selectedrow.find("td:eq(6)").attr('edit','false');
						selectedrow.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
						selectedrow.find("td:eq(7) span").text(Number(recnt*reninprice).toFixed(2));
					}else{
						selectedrow.find("td:eq(6) span").text(0);
						selectedrow.find("td:eq(7) span").text(0);
						selectedrow.find("td:eq(6)").removeAttr('edit');
						selectedrow.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
						
						$.ajax({
							type: "POST",
							url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+positncode+"&delivercode="+delivercode,
							data: "sp_code="+sp_code,
							dataType: "json",
							success:function(data4){
								if(data4.sp_price == 0){
									selectedrow.find("td:eq(6) span").text('0.00').css("text-align","right");
								}else{
									selectedrow.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
									var nrowmoney = Number(Number(data4.sp_price)*Number(recnt)).toFixed(2);
									selectedrow.find("td:eq(7) span").text(nrowmoney).css("text-align","right");
									$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
								}
							}
						})
					}
					
				})
				$.fn.autoGrid.setCellEditable(selectedrow,5);
			}
			
			
			function resetName(e){
				if($('#mMenu').length!=0){
					return;
				}
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==2 || curindex==5){//物资 供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}else if(curindex==7){//当为备注时不进入方法
					
				}else{
					var value = $(e).val();
					$(e).closest('span').text(value);
				}
			}
			function deleteRow(obj){
				var nmoney = $("#nmoney").val();
				var delmoney = Number($(obj).parent("tr").find('td:eq(7) span').text());
				
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
				
				$("#nmoney").val(Number(nmoney-delmoney).toFixed(2));
			};
		</script>
	</body>
</html>
