<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.table-head td span{
				white-space: normal;
			}
			.childgrid{
				width: 99.8%;
				border:solid 1px #8DB2E3;
				margin-top:6px;
			}
		</style>
	</head>
	<body>
		<div class="childgrid">
			<div class="grid" id="grid">		
				<div class="table-head" >
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width:30px;">&nbsp;</span></td>
								<td><span style="width:120px;"><fmt:message key="shouyewuzibm" /></span></td>
								<td><span style="width:140px;"><fmt:message key="shouyewuzimc" /></span></td>
								<td><span style="width:100px;"><fmt:message key="specification" /></span></td>
								<td><span style="width:100px;"><fmt:message key="unit" /></span></td>
								<td><span style="width:100px;"><fmt:message key="Reference_resources_price" /></span></td>
								<td><span style="width:200px;"><fmt:message key="suppliers" /></span></td>
								<td><span style="width:50px;"><fmt:message key="tax_rate" /></span></td>
								<td><span style="width:200px;"><fmt:message key="remark" /></span></td>
	                    	</tr>
						</thead>
					</table>
				</div>				
				<div class="table-body">
					<table cellspacing="0" cellpadding="0">
						<tbody>
							<c:forEach var="purtempletd" items="${listPurtempletd}" varStatus="status">
								<tr>
									<td class="num"><span style="width:30px;">${status.index+1}</span></td>
									<td title="${purtempletd.sp_code}" ><span style="width:120px;text-align: left;">${purtempletd.sp_code}</span></td>
									<td title="${purtempletd.sp_name}" ><span style="width:140px;text-align: left;">${purtempletd.sp_name}</span></td>
									<td title="${purtempletd.sp_desc}" ><span style="width:100px;text-align: left;">${purtempletd.sp_desc}</span></td>
									<td title="${purtempletd.unit}" ><span style="width:100px;text-align: left;">${purtempletd.unit}</span></td>
									<td title="${purtempletd.sp_price}" ><span style="width:100px;text-align: right;"><fmt:formatNumber value="${purtempletd.sp_price}" pattern="0.00"/></span></td>
									<td title="${purtempletd.delivername}" ><span style="width:200px;">${purtempletd.delivername}</span></td>
									<td title="${purtempletd.taxdes}" ><span style="width:50px;">${purtempletd.taxdes}</span></td>
									<td title="${purtempletd.vmemo}" ><span style="width:200px;">${purtempletd.vmemo}</span></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
		</div>	
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var height = $(parent.document.body).height()*0.5-12;
				$(".childgrid").css('height',height);
				//自动实现滚动条
				$(".grid").css('height',height-$(".form-line").height()-1);
				setElementHeight('.table-body',['.table-head'],'.grid',25);	//计算.table-body的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())*0.998);
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())*0.998);
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			})
		</script>
	</body>
</html>