<%--
  User: mc
  Date: 14-10-21
  Time: 下午5:43
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>
<div class="leftFrame">
    <div id="toolbar"></div>
    <div class="treePanel" >
        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
        <script type="text/javascript">
            var tree = new MzTreeView("tree");
            tree.nodes['0_00000000000000000000000000000000'] = 'text:供应商类别; method:changeUrl("00000000000000000000000000000000","","模块","")';
            <c:forEach var="typeList" items="${typeList}" varStatus="status">
                <c:if test="${typeList.enablestate==2}">
                    tree.nodes['${typeList.pk_father}_${typeList.delivercodetype}'] = 'text:${lo:show(typeList.vcode)}-${lo:show(typeList.vname)}; method:changeUrl(\'${typeList.delivercodetype}\',\'${typeList.vcode}\',\'${lo:show(typeList.vname)}\',\'${typeList.pk_father}\')';
                </c:if>
                <c:if test="${typeList.enablestate!=2}">
                    tree.nodes['${typeList.pk_father}_${typeList.delivercodetype}'] = 'text:<span title="${lo:show(typeList.vname)}" style="color: crimson">${lo:show(typeList.vcode)}-${lo:show(typeList.vname)}</span>; method:changeUrl(\'${typeList.delivercodetype}\',\'${typeList.vcode}\',\'${lo:show(typeList.vname)}\',\'${typeList.pk_father}\')';
                </c:if>
            </c:forEach>
            tree.setIconPath("<%=path%>/image/tree/none/");
            document.write(tree.toString());
        </script>
    </div>
</div>
<div class="mainFrame">
    <iframe src="<%=path%>/supplierType/showType.do?pk_father=00000000000000000000000000000000&parentcode=&lvl=1" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript">
    function changeUrl(moduleId,moduleCode,moduleName,parentid){
		$("#wait2").css("display","block");
		$("#wait").css("display","block");
        var paths=tree.getPath(moduleId).length;
        window.mainFrame.location="<%=path%>/supplierType/showType.do?pk_father="+moduleId+"&parentcode="+moduleCode+"&lvl="+(paths-1);
    }
    function refreshTree(){
		$("#wait2").css("display","block");
		$("#wait").css("display","block");
        window.location.href = '<%=path%>/supplierType/typeList.do';
    }
    function refreshNode(delivercodetype,vcode){
        window.location.href = '<%=path%>/supplierType/typeList.do?delivercodetype='+delivercodetype+"&vcode="+vcode;
    }
    $(document).ready(function(){
        var toolbar = $('#toolbar').toolbar({
            items: [{
                text: '<fmt:message key="expandAll" />',
                title: '<fmt:message key="expandAll" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-160px','-80px']
                },
                handler: function(){
                    tree.expandAll();
                }
            },{
                text: '<fmt:message key="refresh" />',
                title: '<fmt:message key="refresh" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-60px','0px']
                },
                handler: function(){
                    refreshTree();
                }
            }]
        });
        setElementHeight('.treePanel',['#toolbar']);
        if('${type.delivercodetype}'!='') {
            tree.focus('${type.delivercodetype}');
            changeUrl('${type.delivercodetype}', '${type.vcode}', '${type.vname}', '${type.pk_father}')
        }
// 		$("#wait2").css("display","none");
// 		$("#wait").css("display","none");
    });
</script>
</body>
</html>
