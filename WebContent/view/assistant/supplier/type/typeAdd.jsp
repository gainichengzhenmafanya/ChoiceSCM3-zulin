<%--
  User: mc
  Date: 14-10-21
  Time: 下午10:32
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>采购合约</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
</head>
<body>
<form id="mainForm" action="<%=path%>/supplierType/addType.do" method="post">
    <div class="condition" style="margin-top:35px;">
        <div class="form-line">
            <div class="form-label"><fmt:message key="category"/><fmt:message key="coding"/>：</div>
            <input type="hidden" id="pk_father" name="pk_father" value="${type.pk_father}"/>
            <input type="hidden" id="parentcode" name="parentcode" value="${type.parentcode}"/>
            <input type="hidden" id="lvl" name="lvl" value="${type.lvl}"/>
            <div class="form-input">
                <input type="text" id="vcode" title="${codeFormat}" value="${type.vcode}" name="vcode" style="margin:2px;" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="category"/><fmt:message key="name"/>：</div>
            <div class="form-input">
                <input type="text" id="vname" name="vname" style="margin:2px;" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="category"/><fmt:message key="status"/>：</div>
            <div class="form-input">
                <select class="select" id="enablestate" name="enablestate">
                    <option value="1"><fmt:message key="not_enabled"/></option>
                    <option selected="selected" value="2"><fmt:message key="have_enabled"/></option>
                    <option value="3"><fmt:message key="stop_enabled"/></option>
                </select>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    var vald;
    $(document).ready(function(){
        document.onkeydown=function(){
            if(event.keyCode==27){//ESC 后关闭窗口
                $(".close",parent.document).click();
            }
            if(event.ctrlKey&& event.keyCode==13){
                $("#save",parent.document).click();
            }
        };
        var inputarray = $(".form-input").children().not("input[type='hidden']").not("div");
        inputarray.bind('keyup',function(){
            if(!event.ctrlKey&&event.keyCode==13){
                var index = inputarray.index($(this)[0]);
                if(index!=inputarray.length-1){
                    $(inputarray[index+1]).focus();
                }else{
                    $("#save",parent.document).click();
                }
            }
        });
        $("#vcode").focus();
        vald = new Validate({
            validateItem:[{
                type:'text',
                validateObj:'vcode',
                validateType:['canNull','maxLength','withOutSpecialChar'],
                param:['F','50'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
            },{
                type:'text',
                validateObj:'vname',
                validateType:['canNull','maxLength','withOutSpecialChar'],
                param:['F','50'],
                error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
            }]
        });
    });
	var savelock = false;
    var add=function(){
		if(savelock){
			return;
		}else{
			savelock = true;
		}
        if($.trim($("#vcode").val())==''){
            valerrorbox("vcode",'<fmt:message key="cannot_be_empty"/>');
            $("#vcode").val($.trim($("#vcode").val()));
			savelock = false;
            return;
        }
        if($.trim($("#vname").val())==''){
            valerrorbox("vname",'<fmt:message key="cannot_be_empty"/>');
            $("#vname").val($.trim($("#vname").val()));
			savelock = false;
            return;
        }
        if(vald._submitValidate()) {
            codeval({
                action:"<%=path%>",
                callback:"ajax"
            },
            "00000000000000000000000000000010",
            $("#vcode").val(),
//             $("#parentcode").val(),
			"",
            $("#lvl").val());
        }else{
			savelock = false;
		}
    };
    var mark=true;
    var ajax=function(msg){
        if(msg=="ok"){
            if(mark) {
                $("form").submit();
                mark=false;
                setTimeout(function(){mark=true;},5000);
            }
        }else {
            valerrorbox("vcode",'<fmt:message key="code_error"/>');
			savelock = false;
        }
    };
</script>
</body>
</html>
