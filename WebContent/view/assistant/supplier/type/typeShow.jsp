<%--
  User: mc
  Date: 14-10-21
  Time: 下午10:32
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <title>采购合约</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
</head>
<body>
<div class="tool"></div>
<input type="hidden" id="father" value="${type.pk_father}"/>
<input type="hidden" id="lvl" value="${type.lvl}"/>
<input type="hidden" id="parentcode" value="${type.parentcode}"/>
<div class="grid" id="grid">
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td><span style="width: 30px;text-align: center;"><fmt:message key="the_serial_number"/></span></td>
                <td style="width:30px;text-align: center;">
                    <input type="checkbox" id="chkAll"/>
                </td>
                <td><span style="width: 80px;text-align: center;"><fmt:message key="coding"/></span></td>
                <td><span style="width: 100px;text-align: center;"><fmt:message key="name"/></span></td>
                <td><span style="width: 50px;text-align: center;"><fmt:message key="status"/></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <c:forEach items="${supplierType}" var="type" varStatus="status">
                <tr>
                    <td><span style="width: 30px;text-align: center;">${status.index+1}</span></td>
                    <td style="width:30px; text-align: center;">
                        <input type="checkbox"  name="idList" id="chk_<c:out value='${type.delivercodetype}' />" value="<c:out value='${type.delivercodetype}' />"/>
                    </td>
                    <td><span style="width: 80px;" title="${type.vcode}">${type.vcode}</span></td>
                    <td><span style="width: 100px;" title="${type.vname}">${type.vname}</span></td>
                    <td><span style="width: 50px;text-align: center;">
                        <c:if test="${type.enablestate==1}"><fmt:message key="not_enabled"/></c:if>
                        <c:if test="${type.enablestate==2}"><fmt:message key="have_enabled"/></c:if>
                        <c:if test="${type.enablestate==3}"><fmt:message key="stop_enabled"/></c:if>
                    </span></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),35);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        //如果全选按钮选中的话，table背景变色
        $("#chkAll").click(function() {
            if (!!$("#chkAll").attr("checked")) {
                $('.grid').find('.table-body').find('tr').addClass("bgBlue");
            }else{
                $('.grid').find('.table-body').find('tr').removeClass("bgBlue");
            }
        });
        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            if ($(this).hasClass("bgBlue")) {
                $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
            }
            else
            {
                $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
            }
        });
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="insert"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                	$.ajaxSetup({async:false});
					$.post('<%=path%>/coderule/getFormat.do',{"pk_id":"00000000000000000000000000000010"},function(data){
						var rs = data;
						if(rs != ""){
							if("${type.lvl}">rs.split("-").length){
								alerterror( '<fmt:message key="cantaddchildtype" />');
								return;
							}else{
								  insert();
							}
						}else{
							insert();
						}
					});	
                }
            }, {
                text: '<fmt:message key="update"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    update();
                }
            }, {
                text: '<fmt:message key="delete"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    del();
                }
            },{
                text: '<fmt:message key="quit" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-60px','0px']
                },
                handler: function(){
                    invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                }
            }]
        });
		$("#wait2",parent.window.document).css("display","none");
		$("#wait",parent.window.document).css("display","none");
    });
    var insert=function(){
        var action="<%=path%>/supplierType/showAdd.do?pk_father="+$("#father").val()+"&parentcode="+$("#parentcode").val()+"&lvl="+$("#lvl").val();
        $('body').window({
            title: '<fmt:message key="insert"/>',
            content: '<iframe id="skipSupplierFrame" frameborder="0" src="'+action+'"></iframe>',
            width: '350px',
            height: '250px',
            draggable: true,
            isModal: true,
            topBar: {
                items: [{
                    text: '<fmt:message key="save"/>',
                    id:"save",
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-80px','-0px']
                    },
                    handler: function(){
                        getFrame('skipSupplierFrame').add();
                    }
                },{
                    text: '<fmt:message key="cancel" /> ',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-160px','-100px']
                    },
                    handler: function(){
                        $('.close').click();
                    }
                }
                ]
            }
        });
    }
    var update=function(){
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size()==1) {
            var action="<%=path%>/supplierType/showEdit.do?delivercodetype="+checkboxList.filter(':checked').val()+"&parentcode="+$("#parentcode").val()+"&lvl="+$("#lvl").val();
            $('body').window({
                title: '<fmt:message key="update"/>',
                content: '<iframe id="skipSupplierFrame" frameborder="0" src="'+action+'"></iframe>',
                width: '350px',
                height: '250px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            title: '<fmt:message key="save"/>',
                            id:"save",
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                getFrame('skipSupplierFrame').update();
                            }
                        },
                        {
                            text: '<fmt:message key="cancel"/>',
                            title: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
        }else if(!checkboxList||0>=checkboxList.filter(':checked').size()){
            alerterror('<fmt:message key="please_select_information_you_need_to_modify"/>！');
            return ;
        }else{
            alerterror('<fmt:message key="you_can_only_modify_a_data"/>！');
            return ;
        }
    }
    var del=function(){
        var checkboxList = $('.grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
        	alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
                var codeValue=[];
                checkboxList.filter(':checked').each(function(){
                    codeValue.push($(this).val());
                });
                var action = '<%=path%>/supplierType/delType.do?ids='+codeValue.join(",");
                $('body').window({
                    title: '<fmt:message key="data"/><fmt:message key="delete"/>',
                    content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                    width: 250,
                    height: 150,
                    draggable: true,
                    isModal: true
                });
            });
        }else{
            alerterror('<fmt:message key="please_select_information_you_need_to_delete"/>！');
            return ;
        }
    }
    var pageReload=function(){
        window.parent.refreshNode($("#father").val(),$("#parentcode").val());
    }
</script>
</body>
</html>
