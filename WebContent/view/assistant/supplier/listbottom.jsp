<%--
  User: mc
  Date: 14-10-24
  Time: 下午12:15
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div class="easyui-tabs" fit="false" plain="true" style="z-index:88;">
    <div title='<fmt:message key="basic_information"/>' id="y_1">
        <div class="grid" id="grid1">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 90px;"><fmt:message key="suppliers_type"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="suppliers_coding"/></span></td>
                        <td><span style="width: 180px;"><fmt:message key="suppliers_name"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="Mnemonic"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="whether_invoicing"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="person_in_charge"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="contact"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="address"/></span></td>
                        <td><span style="width: 130px;"><fmt:message key="email"/></span></td>
                        <td><span style="width: 130px;"><fmt:message key="www"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="fax"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="tel"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="registered_capital"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td><span style="width: 90px;text-align: left;" title="${supplier.delivercodetype.vname}" id="j_supplierType">${supplier.delivercodetype.vname}</span></td>
                        <td><span style="width: 120px;text-align: left;" title="${supplier.vcode}" id="j_vcode">${supplier.vcode}</span></td>
                        <td><span style="width: 180px;text-align: left;" title="${supplier.vname}" id="j_vname">${supplier.vname}</span></td>
                        <td><span style="width: 50px;text-align: left;" title="${supplier.vinit}" id="j_vinit">${supplier.vinit}</span></td>
                        <td><span style="width: 50px;text-align: center;" id="j_enablestate">
                            <c:if test="${supplier.enablestate==1}"><fmt:message key="not_enabled"/></c:if>
                            <c:if test="${supplier.enablestate==2}"><fmt:message key="have_enabled"/></c:if>
                            <c:if test="${supplier.enablestate==3}"><fmt:message key="stop_enabled"/></c:if>
                        </span></td>
                        <td><span style="width: 80px;text-align: center;" id="j_bistax">
                            <input disabled="disabled" <c:if test="${supplier.bistax==0}">checked="checked" </c:if> type="checkbox" id="isbill"/>
                        </span></td>
                        <td><span style="width: 50px;text-align: left;" title="${supplier.vlegal}" id="j_vlegal">${supplier.vlegal}</span></td>
                        <td><span style="width: 50px;text-align: left;" title="${supplier.vcontact}" id="j_vcontact">${supplier.vcontact}</span></td>
                        <td><span style="width: 200px;text-align: left;" title="${supplier.vaddr}" id="j_vaddr">${supplier.vaddr}</span></td>
                        <td><span style="width: 130px;text-align: left;" title="${supplier.vmailaddr}" id="j_vmailaddr">${supplier.vmailaddr}</span></td>
                        <td><span style="width: 130px;text-align: left;" title="${supplier.vwebsite}" id="j_vwebsite">${supplier.vwebsite}</span></td>
                        <td><span style="width: 90px;text-align: left;" title="${supplier.vfax}" id="j_vfax">${supplier.vfax}</span></td>
                        <td><span style="width: 90px;text-align: left;" title="${supplier.vtele}" id="j_vtele">${supplier.vtele}</span></td>
                        <td><span style="width: 90px;text-align: right;" title="${supplier.nsignmonty}" id="j_nsignmonty">${supplier.nsignmonty}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='<fmt:message key="payment_days_set"/>' id="y_2">
        <div class="panel-body">
            <div class="condition">
                <div class="form-line">
                    <div class="form-label"><fmt:message key="bill_frequency"/> ：</div>
                    <div class="form-input">
<%--                         <input name="iaccttype" type="radio" disabled="disabled" value="0"><fmt:message key="immediately"/></input> --%>
                        <input name="iaccttype" type="radio" disabled="disabled" value="1"  <c:if test="${supplier.iaccttype==1 }">checked="checked"</c:if>/>30天
                        <input name="iaccttype" type="radio" disabled="disabled" value="2"  <c:if test="${supplier.iaccttype==2 }">checked="checked"</c:if>/>60天
                        <input name="iaccttype" type="radio" disabled="disabled" value="3"  <c:if test="${supplier.iaccttype==3 }">checked="checked"</c:if> />90天
<%--                         <input name="iaccttype" type="radio" disabled="disabled" value="4"><fmt:message key="half_a_year"/></input> --%>
<%--                         <input name="iaccttype" type="radio" disabled="readonly" value="5"><fmt:message key="year"/></input> --%>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="bill_date"/>：</div>
                    <div class="form-input">
                        <select class="select" disabled="disabled" id="iacctdate" name="iacctdate">
                            <c:forEach begin="1" end="31" step="1" var="index">
                                <option value="${index}">${index}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--     <div title='信用管理' id="y_3"> -->
<!--         <div class="grid" id="grid2"> -->
<!--             <div class="table-head"> -->
<!--                 <table cellpadding="0" cellspacing="0"> -->
<!--                     <thead> -->
<!--                     <tr> -->
<!--                         <td><span style="width: 100px;">打分日期</span></td> -->
<!--                         <td><span style="width: 40px;">序号</span></td> -->
<!--                         <td><span style="width: 200px;">评价指标</span></td> -->
<!--                         <td><span style="width: 50px;">权重</span></td> -->
<!--                         <td><span style="width: 50px;">得分</span></td> -->
<!--                         <td><span style="width: 250px;">备注</span></td> -->
<!--                     </tr> -->
<!--                     </thead> -->
<!--                 </table> -->
<!--             </div> -->
<!--             <div class="table-body"> -->
<!--                 <table cellpadding="0" cellspacing="0"> -->
<!--                     <tbody> -->
<%--                     <c:forEach items="${supplier.suppliereval}" var="eval"> --%>
<%--                         <c:forEach items="${eval.supplierevalds}" var="evald" varStatus="vs"> --%>
<!--                             <tr> -->
<%--                                 <c:if test="${vs.index==0}"> --%>
<%--                                     <td rowspan="${fn:length(eval.supplierevalds)+1}"><span style="width: 100px;">${eval.devaldate}</span></td> --%>
<%--                                 </c:if> --%>
<%--                                 <td><span style="width: 40px;">${vs.index+1}</span></td> --%>
<%--                                 <td><span style="width: 200px;">${evald.vstandard}</span></td> --%>
<%--                                 <td><span style="width: 50px;text-align: right;">${evald.vweight}</span></td> --%>
<%--                                 <td><span style="width: 50px;text-align: right;">${evald.nscore}</span></td> --%>
<%--                                 <td><span style="width: 250px;">${evald.vmemo}</span></td> --%>
<!--                             </tr> -->
<%--                         </c:forEach> --%>
<!--                         <tr> -->
<!--                             <td><span style="width: 40px;"></span></td> -->
<!--                             <td><span style="width: 200px;">最终得分</span></td> -->
<!--                             <td><span style="width: 50px;"></span></td> -->
<%--                             <td><span style="width: 50px;text-align: right;">${eval.nscore}</span></td> --%>
<!--                             <td><span style="width: 250px;"></span></td> -->
<!--                         </tr> -->
<%--                     </c:forEach> --%>
<!--                     </tbody> -->
<!--                 </table> -->
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->
    <div title='<fmt:message key="fit_store"/>' id="y_4">
        <div class="grid" id="grid3">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 40px;"><fmt:message key="the_serial_number"/></span></td>
                        <td><span style="width: 40px;"><fmt:message key="coding"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="store"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${supplier.supplierOrg}" var="org" varStatus="i">
                            <tr>
                                <td><span style="width: 40px;text-align: center;">${i.index+1}</span></td>
                                <td><span style="width: 40px;" title="${org.positncode.code}">${org.positncode.code}</span></td>
                                <td><span style="width: 100px;" title="${org.positncode.name}">${org.positncode.name}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='<fmt:message key="Scope_of_supplies"/>' id="y_5">
        <div class="grid" id="grid4">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 40px;"><fmt:message key="the_serial_number"/></span></td>
                        <td><span style="width: 70px;"><fmt:message key="supplies_code"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="supplies_name"/></span></td>
                        <td><span style="width: 70px;"><fmt:message key="specification"/></span></td>
                        <td><span style="width: 70px;"><fmt:message key="unit"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="Mall_goods_code"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="Mall_goods_name"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${supplier.materialScope}" var="scope" varStatus="i">
                            <tr>
                                <td><span style="width: 40px;text-align: center;">${i.index+1}</span></td>
                                <td><span style="width: 70px;" title="${scope.vcode}">${scope.vcode}</span></td>
                                <td><span style="width: 100px;" title="${scope.vname}">${scope.vname}</span></td>
                                <td><span style="width: 70px;" title="${scope.vstandard}">${scope.vstandard}</span></td>
                                <td><span style="width: 70px;" title="${scope.pk_unit.vname}">${scope.pk_unit.vname}</span></td>
                                <td><span style="width: 100px;" title="${scope.materialMall.vjmucode}">${scope.materialMall.vjmucode}</span></td>
                                <td><span style="width: 200px;" title="${scope.materialMall.vjmuname}">${scope.materialMall.vjmuname}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='<fmt:message key="bank_account"/>' id="y_6">
        <div class="grid" id="grid5">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 40px;"><fmt:message key="the_serial_number"/></span></td>
                            <td><span style="width: 150px;"><fmt:message key="bank_account"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="bank"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${supplier.supplierBank}" var="bank" varStatus="i">
                            <tr>
                                <td><span style="width: 40px;">${i.index+1}</span></td>
                                <td><span style="width: 150px;" title="${bank.vbanknumber}">${bank.vbanknumber}</span></td>
                                <td><span style="width: 100px;" title="${bank.vbankname}">${bank.vbankname}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#y_2").find("input[value='"+<c:out value="${supplier.iaccttype}" default="0"/>+"']").attr("checked","checked");
        $("#y_2").find("#iacctdate").find("option[value='"+<c:out value="${supplier.iacctdate}" default="1"/>+"']").attr("selected","selected");
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth+1);
                $grid.find(".table-head").width(headWidth+1);
            }else{
                $grid.find(".table-body").width(gridWidth+1);
                $grid.find(".table-head").width(gridWidth+1);
            }
        };
        $('.easyui-tabs').tabs({
            width:$(document.body).width()-2,
            height:$(document.body).height()-34,
            onSelect:function(){
                var id=$(this).tabs('getSelected')[0].id;
                setElementHeight('#'+id+' .grid',['.tool'],$(document.body),90);	//计算.grid的高度
                setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
                loadGrid('#'+id+' .grid');
                setWidth('#'+id+' .grid');
            }
        });
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
    });
</script>
</body>
</html>
