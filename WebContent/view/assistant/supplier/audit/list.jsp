<%--
  User: mc
  Date: 14-10-29
  Time: 下午10:01
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div class="tool">
</div>
<div class="grid" id="grid" style="margin-top: 5px;padding-top: 5px;">
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <td>
                        <span style="width: 30px;">
                            <input type="checkbox" id="chkAll"/>
                        </span>
                    </td>
                    <td><span style="width: 150px;"><fmt:message key="suppliers_name"/></span></td>
                    <td><span style="width: 90px;"><fmt:message key="rank"/></span></td>
                    <td><span style="width: 90px;"><fmt:message key="contact"/></span></td>
                    <td><span style="width: 90px;"><fmt:message key="tel"/></span></td>
                    <td><span style="width: 250px;"><fmt:message key="address"/></span></td>
                    <td><span style="width: 300px;"><fmt:message key="remark"/></span></td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <c:forEach items="${audit}" var="a">
                <tr>
                    <td style="width:30px; text-align: center;">
                        <span style="width: 30px;">
                            <input type="checkbox"  name="idList" id="chk_<c:out value='${a.delivercode}' />" value="<c:out value='${a.delivercode}' />"/>
                            <input type="hidden" value="${a.pk_auditsupplier}"/>
                        </span>
                    </td>
                    <td><span style="width: 150px;" title="${a.vname}">${a.vname}</span></td>
                    <td><span style="width: 90px;" title="<c:if test="${a.vlevel==1}">子供应链</c:if> 
																				<c:if test="${a.vlevel==2}">品牌供应商</c:if>
																				<c:if test="${a.vlevel==3}">普通供应商</c:if>">
						<c:if test="${a.vlevel==1}">子供应链</c:if> 
						<c:if test="${a.vlevel==2}">品牌供应商</c:if>
						<c:if test="${a.vlevel==3}">普通供应商</c:if>
						 </span>
					</td>
                    <td><span style="width: 90px;" title="${a.vcontact}">${a.vcontact}</span></td>
                    <td><span style="width: 90px;" title="${a.vsuppliertele}">${a.vsuppliertele}</span></td>
                    <td><span style="width: 250px;" title="${a.vsupplieraddress}">${a.vsupplieraddress}</span></td>
                    <td><span style="width: 300px;" title="${a.vmemo}">${a.vmemo}</span></td>
					<td style="display: none;">${a.vcompanyfax}</td>
					<td style="display: none;">${a.vcompanyemail}</td>
					<td style="display: none;">${a.vcompanyurl}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <input type="hidden" id="moduleId" name="moduleId" value="${moduleId }" />
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.tool').toolbar({
            items: [{
        		text: '刷新',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
        		icon: {
        			url: '<%=path%>/image/Button/op_owner.gif',
        			positn: ['0px','0px']
        		},
        		handler: function(){
        			location.href=location.href;
        		}
        	},{
                text: '<fmt:message key="join"/><fmt:message key="suppliers"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    islink();
                }
            },{
                text: '<fmt:message key="related"/><fmt:message key="suppliers"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'link')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    link();
                }
            },{
                text: '<fmt:message key="delete"/>',
                useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    del();
                }
            },{
                text: '<fmt:message key="quit"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                }
            }]
        });
        setElementHeight('.grid',['.tool'],$(document.body),7);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        //如果全选按钮选中的话，table背景变色
        $("#chkAll").click(function() {
            if (!!$("#chkAll").attr("checked")) {
                $('#grid').find('.table-body').find('tr').addClass("bgBlue");
            }else{
                $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
            }
        });
        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            if ($(this).hasClass("bgBlue")) {
                $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
            }
            else
            {
                $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
            }
            var  url="<%=path%>/supplier/listbottom.do?delivercode="+$(this).find(":checkbox").val();
            $('#mainFrame').attr('src',encodeURI(url));
        });
        var trrows = $(".table-body").find('tr');
        if(trrows.length!=0){
            $(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
        }
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
    });
    //========================================================
    var link=function(){
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size()==1) {
            var action = '<%=path%>/audit/link.do?delivercodejum=' + encodeURI(checkboxList.filter(':checked').val())
                    +'&vcodejmu='+ encodeURI(checkboxList.filter(':checked').val())
                    +'&vnamejmu='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(1)").find("span").text()))
                    +'&vaddr='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(5)").text()))
                    +'&vtele='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(4)").text()))
                    +'&vcontact='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(3)").text()))
                    +'&vmailaddr='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(8)").text()))
                    +'&vwebsite='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(9)").text()))
                    +'&vfax='+ encodeURI($.trim(checkboxList.filter(':checked').parents("tr").find("td:eq(7)").text()));
            $('body').window({
                title: '<fmt:message key="related"/><fmt:message key="suppliers"/>',
                content: '<iframe id="skipSupplierFrame" frameborder="0" src="' + encodeURI(action) + '"></iframe>',
                width: '850px',
                height: '560px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                getFrame('skipSupplierFrame').linkSupplier();
                            }
                        },
                        {
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
        }else if(checkboxList&&checkboxList.filter(':checked').size()>1){
            alerttips('<fmt:message key="a_data_can_be_set_at_a_time"/>');
        }else{
            alerttips('<fmt:message key="please_select_a_data"/>');
        }
    };
    var islink=function(){
        $.ajax({
            url:"<%=path%>/audit/islink.do?auditSupplier="+$('#grid').find('.table-body').find(':checkbox').filter(':checked').val(),
            success:function(msg){
                if(msg>0){
                	alertconfirm('<fmt:message key="relevance_supplier_is_have"/>',function(){
                		add();
                    });
                }else {
                    add();
                }
            }
        });
    };
    var add=function(){
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size()==1) {
            var action = '<%=path%>/audit/showAddSupplier.do?delivercodejum=' + checkboxList.filter(':checked').val()+'&moduleId='+$("#moduleId").val();
            $('body').window({
                title: '<fmt:message key="join"/><fmt:message key="suppliers"/>',
                content: '<iframe id="skipSupplierFrame" frameborder="0" src="' + encodeURI(action) + '"></iframe>',
                width: '700px',
                height: '500px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                getFrame('skipSupplierFrame').insertAuditSupplier();
                            }
                        },
                        {
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
        }else{
            alerttips('<fmt:message key="please_select_a_data"/>');
        }
    };
    del=function(){
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size()>0) {
        	alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
	            var dataValue=[];
	            checkboxList.filter(":checked").each(function(){
	                dataValue.push($(this).next().val());
	            });
	            var action = '<%=path%>/audit/delSupplier.do?ids='+dataValue.join(",");
	            $('body').window({
	                title: '<fmt:message key="delete"/><fmt:message key="suppliers"/>',
	                content: '<iframe id="skipSupplierFrame" frameborder="0" src="' + action + '"></iframe>',
	                width: '250px',
	                height: '150px',
	                draggable: true,
	                isModal: true
	            });
        	});
        }else{
            alerttips('<fmt:message key="please_select_at_least_one_data"/>');
        }
    };
    //回调方法-调用弹出窗口的物资新增方法
    function functions(chkCode,childFrameId){
    	document.getElementById(childFrameId).contentWindow.setMaterial(chkCode);
    }
</script>
</body>
</html>
