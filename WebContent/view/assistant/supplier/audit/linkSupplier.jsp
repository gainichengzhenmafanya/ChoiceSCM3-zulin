<%--
  User: mc
  Date: 14-10-30
  Time: 上午10:36
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商审核</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
        .page{
            margin-bottom:27%;
        }
    </style>
</head>
<body>
<div style="height:55%;">
    <form id="listForm" action="<%=path%>/audit/link.do" method="post">
        <input type="hidden" id="delivercodejum" name="delivercodejum" value="${supplier.delivercodejum}"/>
        <input type="hidden" id="vcodejmu" name="vcodejmu" value="${supplier.vcodejmu}"/>
        <input type="hidden" id="vnamejmu" name="vnamejmu" value="${supplier.vnamejmu}"/>
        <input type="hidden" id="vaddr" name="vaddr" value="${supplier.vaddr}"/>
        <input type="hidden" id="vtele" name="vtele" value="${supplier.vtele}"/>
        <input type="hidden" id="vcontact" name="vcontact" value="${supplier.vcontact}"/>
        <input type="hidden" id="vmailaddr" name="vmailaddr" value="${supplier.vmailaddr}"/>
        <input type="hidden" id="vwebsite" name="vwebsite" value="${supplier.vwebsite}"/>
        <input type="hidden" id="vfax" name="vfax" value="${supplier.vfax}"/>
        <div class="condition" id="storetool">
            <div class="form-line" style="z-index:5;">
                <div class="form-label" style="width: 60px;"><fmt:message key="coding"/>：</div>
                <div class="form-input"  style="width: 100px;">
                    <input type="text" id="vcode" name="vcode" style="width: 100px;" value="${supplier.vcode}" class="text"/>
                </div>
                <div class="form-label" style="width: 60px;"><fmt:message key="name"/>：</div>
                <div class="form-input"  style="width: 100px;">
                    <input type="text" style="width: 100px;" id="vname" name="vname" value="${supplier.vname}" class="text"/>
                </div>
                <div class="form-label" style="width: 60px;"><fmt:message key="Mnemonic"/>：</div>
                <div class="form-input"  style="width: 100px;">
                    <input type="text" id="vinit" name="vinit" style="width: 100px;" value="${supplier.vinit}" class="text"/>
                </div>
                <div class="form-input" style="padding-top: 3px;">
                    <input type="button"  id="select"  name="select"  value="<fmt:message key='select'/>"/>
                </div>
            </div>
        </div>
        <div class="grid" id="grid">
            <div class="table-head" >
                <table cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <td>
                            <span style="width: 20px;">&nbsp;</span>
                        </td>
                        <td><span style="width: 80px;"><fmt:message key="suppliers_coding"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="suppliers_name"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="jmu_supplier"/></span></td>
                        <td><span style="width: 60px;"><fmt:message key="contact"/></span></td>
                        <td><span style="width: 180px;"><fmt:message key="address"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
                        <td><span style="width: 150px;"><fmt:message key="remark"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellspacing="0" cellpadding="0">
                    <tbody>
                    <c:forEach items="${supplierList}" var="supp" >
                        <tr>
                            <td >
                          		<span style="width: 20px;text-align: center;">
	                                <input type="radio"  name="idList" id="chk_<c:out value='${supp.delivercode}' />" value="<c:out value='${supp.delivercode}' />"/>
	                                <input type="hidden" id="jmu_pk" value="${supp.delivercodejum}"/>
                                </span>
                            </td>
                            <td><span style="width: 80px;" title="${supp.vcode}">${supp.vcode}</span></td>
                            <td><span style="width: 200px;" title="${supp.vname}">${supp.vname}</span></td>
                            <td><span style="width: 200px;" title="${supp.vnamejmu}">${supp.vnamejmu}</span></td>
                            <td><span style="width: 60px;" title="${supp.vcontact}">${supp.vcontact}</span></td>
                            <td><span style="width: 180px;" title="${supp.vaddr}">${supp.vaddr}</span></td>
                            <td><span style="width: 50px;">
                                <c:if test="${supp.enablestate==1}"><fmt:message key="not_enabled"/></c:if>
                                <c:if test="${supp.enablestate==2}"><fmt:message key="have_enabled"/></c:if>
                                <c:if test="${supp.enablestate==3}"><fmt:message key="stop_enabled"/></c:if>
                            </span></td>
                            <td><span style="width: 150px;" title="${supp.vmemo}">${supp.vmemo}</span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <page:page form="listForm" page="${pageobj}"></page:page>
            <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
            <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
        </div>
    </form>
</div>
<div class="mainFrame" style=" height:50%;width:100%;">
    <iframe src="<%=path%>/audit/listbottom.do" frameborder="0" name="mainFrame" id="mainFrame">
    </iframe>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
var savelock = false;
$(document).ready(function(){
	$("#vcode").focus();
    setElementHeight('.grid',['.mainFrame','.condition'],$(document.body),40);	//计算.grid的高度
    setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
    loadGrid();//  自动计算滚动条的js方法
    $('.grid').find('.table-body').find('tr').hover(
            function(){
                $(this).addClass('tr-over');
            },
            function(){
                $(this).removeClass('tr-over');
            }
    );
    //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
    $('.grid').find('.table-body').find('tr').live("click", function () {
        $(this).addClass("bgBlue").find(":radio").attr("checked", true);
        var  url="<%=path%>/audit/listbottom.do?delivercode="+$(this).find(":radio").val();
        $('#mainFrame').attr('src',encodeURI(url));
    });

    if($(".table-body").find("tr").size()>0){
        var  url="<%=path%>/audit/listbottom.do?delivercode="+$(".table-body").find("tr:eq(0)").find(":radio").val();
        $(".table-body").find("tr:eq(0)").find(":radio").attr('checked','checked');
        $('#mainFrame').attr('src',encodeURI(url));
    }
    //查询按钮查询事件
    $("#select").click(function(){
    	$("#vcode").val(stripscript($("#vcode").val()));
    	$("#vname").val(stripscript($("#vname").val()));
    	$("#vinit").val(stripscript($("#vinit").val()));
		$('#listForm').submit();
    });
});
//====================================================
var reload=function(){
    if(typeof(eval(parent.pageReload))=='function') {
        parent.pageReload();
    }else {
        parent.location.href = parent.location.href;
    }
    $(".close",parent.document).click();
};
var linkSupplier=function(){
	if(savelock){
		return;
	}else{
		savelock = true;
	}
    var pk=$(".table-body").find(":checked").next().val();
    if($.trim(pk)==""){
		savelock = false;
        confirmLink();
    }else {
    	alertconfirm('<fmt:message key="relevance_supplier_is_have"/>',function(){
            confirmLink();
        });
    	savelock = false;
    }
};
var confirmLink=function(){
    var data = {};
    data.delivercodejum = $.trim($("#delivercodejum").val());
    data.vcodejmu = $.trim($("#vcodejmu").val());
    data.vnamejmu = $.trim($("#vnamejmu").val());
    data.vaddr = $.trim($("#vaddr").val());
    data.vtele = $.trim($("#vtele").val());
    data.vcontact = $.trim($("#vcontact").val());
    data.vmailaddr = $.trim($("#vmailaddr").val());
    data.vwebsite = $.trim($("#vwebsite").val());
    data.vfax = $.trim($("#vfax").val());
    data.delivercode = $(".table-body").find(":checked").val();
    $.post("<%=path%>/audit/linkSupplier.do", data, function (msg) {
        if (msg == "ok") {
        	alerttipsbreak('<fmt:message key="related"/><fmt:message key="successful"/>',function(){
				savelock = false;
		        parent.location.href = parent.location.href;
        	});
        } else {
            alerterror('<fmt:message key="related"/><fmt:message key="failure"/>');
			savelock = false;
        }
    });
};
</script>
</body>
</html>
