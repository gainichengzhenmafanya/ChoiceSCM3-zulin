<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
/* 			.page{ */
/* 					margin-bottom:26px; */
/* 				} */
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div class="tool">
		</div>
		<div >
			<form id="queryForm" action="<%=path%>/supplier/supplierList.do?domId=${domId}&callBack=${callBack}" method="post">
				<input type="hidden" id="parentId" name="parentId" value=""/>
				<input type="hidden" id="parentName" name="parentName" value=""/>
				<input type="hidden" name="pk_material" value="${pk_material }"  />
				<input type="hidden" id="single" name="single" value="${single}"/>
				<input type="hidden" id="pk_purtemplet" name="pk_purtemplet" value="${pk_purtemplet}"/>
				<table cellspacing="0" cellpadding="0">
					<tr >
						<td class="c-left" style="width: 100px;text-align: right;">编码：</td>
						<td><input type="text" style="width: 100px;" id="vcode" name="vcode" class="text" value="${supplier.vcode}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
						<td class="c-left" style="width: 100px;text-align: right;">名称：</td>
						<td><input type="text" style="width: 100px;" id="vname" name="vname" class="text" value="${supplier.vname}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
						<td class="c-left" style="width: 100px;text-align: right;">缩写：</td>
						<td><input type="text" style="width: 100px;" id="vinit" name="vinit" style="text-transform:uppercase;" class="text" value="${supplier.vinit}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
				        <td width="200">&nbsp;
				        	<input type="button" style="width:60px" id="search" name="search" value='查询'/>
				        </td>
				    </tr>
				</table>
				<div class="grid">
					<div class="table-head" >
						<table>
							<thead>
								<tr>
									<td style="width:31px;"></td>
									<td style="width:30px;">
										<input type="checkbox" id="chkAll"/>
									</td>
									<td style="width:100px;">编码</td>
									<td style="width:220px;">名称</td>
									<td style="width:60px;">缩写</td>
									<td style="width:100px;">联系人</td>
									<td style="width:100px;">法人</td>
									<td style="width:100px;">联系电话</td>
									<td style="width:100px;">区域</td>
									<td style="width:140px;">地址</td>
									<td style="width:140px;display: none;">主键</td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid">
							<tbody>
								<c:forEach var="supplier" varStatus="step" items="${supplierList}">
									<tr>
										<td class="num"><span style="width:21px;" title="${supplier.delivercode}">${step.count}</span></td>
										<td><span style="width:20px; text-align: center;">
											<input type="checkbox" name="idList" id="chk_${supplier.delivercode}" value="${supplier.delivercode}"/>
										</span></td>
										<td><span title="${supplier.vcode}" style="width:90px;text-align: left;">${supplier.vcode}</span></td>
										<td><span title="${supplier.vname}" style="width:210px;text-align: left;">${supplier.vname}</span></td>
										<td><span title="${supplier.vinit}" style="width:50px;text-align: left;">${supplier.vinit}</span></td>
										<td><span title="${supplier.vcontact}" style="width:90px;text-align: left;">${supplier.vcontact}</span></td>
										<td><span title="${supplier.vlegal}" style="width:90px;text-align: left;">${supplier.vlegal}</span></td>
										<td><span title="${supplier.vtele}" style="width:90px;text-align: left;">${supplier.vtele}</span></td>
										<td><span title="${supplier.varea}" style="width:90px;text-align: left;">${supplier.varea}</span></td>
										<td><span title="${supplier.vaddr}" style="width:130px;text-align: left;">${supplier.vaddr}</span></td>
										<td style="display: none;"><span title="${supplier.delivercode}" style="width:130px;text-align: left;display: none;">${supplier.delivercode}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var delivercode='${delivercode}';
				$("#vcode").focus();
				selected = '${domId}' == 'selected' ? parent.selected : (typeof(parent['${domId}']) == 'function' ? parent['${domId}']() : $.trim($('#${domId}',parent.document).val()).split(","));
				if(selected){
					$(".table-body").find('tr td input').each(function(){
						if($.inArray($(this).val(),selected) >= 0){//若大于等于零，则这个id已存在父页面中
							$(this).attr('checked','checked');
						}
						if($(this).val()==delivercode){
							$(this).attr('checked','checked');
						}
					});
				}
				setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
	                $("#vcode").val(stripscript($("#vcode").val()));
	                $("#vname").val(stripscript($("#vname").val()));
	                $("#vinit").val(stripscript($("#vinit").val()));
				 	$('#queryForm').submit();
				});
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="enter" />',
							title: 'enter',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								setValue();
							}
						},{
							text: '<fmt:message key ="cancel" />',
							title: 'cancel',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//------------------------------
				var mod = ${single};
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$("#chkAll").click(function(){
						if($(this)[0].checked){
							$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
						}else{
							$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
						}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = ${single};
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						//$(this).attr('checked','checked');
					}
					event.stopPropagation();
				});
				//---------------------------
				var t=$("#init").val();
				$("#init").val("").focus().val(t);
				
				//---------------------------
				$('body').bind('keydown',fc = function(event){
					switch(event.keyCode){
					case 38:	
						var trs = $('.table-body').find('table').find('tr');
						if(trs.length==0){
							return;
						}
						var len = trs.find(':checkbox:checked').length;
						if(len==0){
							$(trs[trs.length-1]).find(':checkbox').attr('checked',true);
							$(trs[trs.length-1]).find(':checkbox').focus();
						}else{
							var tr = trs.find(':checkbox:checked').closest('tr');
							if(tr.prev()){
								tr.prev().find(':checkbox').attr('checked',true);
								tr.prev().find(':checkbox').focus();
								tr.find(':checkbox').attr('checked',false);
							}
						}
						break;
					
					case 40:	
						var trs = $('.table-body').find('table').find('tr');
						if(trs.length==0){
							return;
						}
						var len = trs.find(':checkbox:checked').length;
						if(len==0){
							$(trs[0]).find(':checkbox').attr('checked',true);
							$(trs[0]).find(':checkbox').focus();
						}else{
							var tr = trs.find(':checkbox:checked').closest('tr');
							if(tr.next()){
								tr.next().find(':checkbox').attr('checked',true);
								tr.next().find(':checkbox').focus();
								tr.find(':checkbox').attr('checked',false);
							}
						}
						break;
					case 13:	
// 						setValue();
						break;
					case 27:	
						parent.$('.close').click();
						break;
					}
					
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			
			function setValue(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				var data = {show:[],code:[],mod:[],entity:[]};
				checkboxList.filter(':checked').each(function(){
					var entity = {};
					var row = $(this).closest('tr');
					data.code.push($(this).val());
					data.show.push($.trim(row.children('td:eq(3)').text()));
					entity.delivercode = $.trim($(this).val());
					entity.vcode = $.trim(row.children('td:eq(2)').text());
					entity.vname = $.trim(row.children('td:eq(3)').text());
					data.entity.push(entity);
				});
				if(data.entity[0] == undefined){
					var entity = {};
					entity.delivercode = '';
					entity.vcode = '';
					entity.vname = '';
					data.entity.push(entity);
				}
				parent['${callBack}'](data);
				$('.close',parent.document).click();
			}
		</script>
	</body>
</html>