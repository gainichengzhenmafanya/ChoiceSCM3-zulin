<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title><fmt:message key="gyszm"/></title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div> 
<div style="height:55%;">
    <div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/supplierrm/srmlist.do" method="post">
    <div class="search-div" style="positn: absolute;z-index: 99">
        <div class="form-line">
            <div class="form-label"><fmt:message key="gyszmbh"/>：</div>
            <div class="form-input">
                <input type="text" name="vcode" id="vcode" style="margin:2px;" value="${vcode}" class="text"/>
            </div>
            <div class="form-label"><fmt:message key="gyszmmc"/>：</div>
            <div class="form-input">
                <input type="text" name="vname" id="vname" style="margin:2px;" value="${vname}" class="text"/>
            </div>
            <div class="form-label"><fmt:message key="gyszmzt"/>：</div>
            <div class="form-input">
                <select name="istate" id="istate" class="select" >
                    <option <c:if test="${istate==0}"> selected="selected" </c:if> value="0"><fmt:message key="all"/></option>
                    <option <c:if test="${istate==3}"> selected="selected" </c:if> value="3"><fmt:message key="gyszmwks"/></option>
                    <option <c:if test="${istate==1}"> selected="selected" </c:if> value="1"><fmt:message key="gyszmzjx"/></option>
                    <option <c:if test="${istate==2}"> selected="selected" </c:if> value="2"><fmt:message key="gyszmywc"/></option>
                </select>
            </div>
        </div>
        <div class="search-commit">
            <input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
            <input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
        </div>
    </div>
    <div class="grid" id="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:30px;">
                            <input type="checkbox" id="chkAll"/>
                        </td>
                        <td><span class="num" style="width: 50px;">&nbsp;</span></td>
                        <td><span style="width: 110px;">招募书编号</span></td>
                        <td><span style="width: 220px;">招募书名称</span></td>
                        <td><span style="width: 120px;">招募发布日期</span></td>
                        <td><span style="width: 120px;"><fmt:message key="gyszmbmjzrq"/></span></td>
<!--                         <td><span style="width: 50px;">报名</span></td> -->
                        <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
                        <td><span style="width:80px;">应募供应商</span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${supplierRMList}" var="supplierRM"   varStatus="status">
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${supplierRM.pk_recruit}' />" value="<c:out value='${supplierRM.pk_recruit}' />"/>
                            </td>
                            <td><span class="num" style="width: 50px;text-align: center;">${status.index+1}</span></td>
                            <td><span style="width: 110px;">${supplierRM.vcode}</span></td>
                            <td><span style="width: 220px;">${supplierRM.vname}</span></td>
                            <td><span style="width: 120px;">${supplierRM.dreleasedat}</span></td>
                            <td><span style="width: 120px;">${supplierRM.denddat}</span></td>
<!--                             <td><span style="width: 50px;"></span></td> -->
                            <td><span style="width: 50px;">
                                <c:if test="${supplierRM.istate==3}"><fmt:message key="gyszmwks"/></c:if>
                                <c:if test="${supplierRM.istate==1}"><fmt:message key="gyszmzjx"/></c:if>
                                <c:if test="${supplierRM.istate==2}"><fmt:message key="gyszmywc"/></c:if>
                            </span></td>
                            <td><span style="width:80px;text-align: left;color: blue;text-align: center;" onclick="showdetail('${supplierRM.pk_recruit}')">详情</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${pageobj}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
    </div>
    </form>
    </div>
    <div class="mainFrame" style=" height:8%;width:100%;">
        <iframe src="<%=path%>/supplierrm/srmchildlist.do" frameborder="0" name="mainFrame" id="mainFrame">
        </iframe>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript">
    $.ajaxSetup({
		async: false
	});
        $(document).ready(function(){
            $("#search").bind('click', function() {
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $("#vcode").val(stripscript($("#vcode").val()));
                $("#vname").val(stripscript($("#vname").val()));
                $('.search-div').hide();
                $('#listForm').submit();
            });
            /* 模糊查询清空 */
            $("#resetSearch").bind('click', function() {
                $("#vcode").val("");
                $("#vname").val("");
                $("#istate").val("0");
            });
            $('.tool').toolbar({
                items: [{
                    text: '<fmt:message key="select"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px', '-40px']
                    },
                    handler: function () {
                        $('.search-div').slideToggle(100);
                    	$("#vcode").focus();
                    }
                },{
                    text: '<fmt:message key="insert"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        add();
                    }
                },{
                    text: '<fmt:message key="update"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                       	update();
                    }
                },{
                    text: '<fmt:message key="delete"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        del();
                    }
                }/*,{
                    text: '<fmt:message key="gyszmzjx"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'alone')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                       uEna(1);
                    },
                    items:[{
                        text: '<fmt:message key="gyszmywc"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'alone')},
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px','-40px']
                        },
                        handler: function(){
                            uEna(2);
                        }
                    },{
	                    text: '<fmt:message key="gyszmwks"/>',
	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'alone')},
	                    icon: {
	                        url: '<%=path%>/image/Button/op_owner.gif',
	                        positn: ['0px','-40px']
	                    },
	                    handler: function(){
	                        uEna(3);
	                    }
               		 }]
                	}*/,{
                    text: '应募',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        position: ['0px','-40px']
                    },
                    handler: function(){
                    	addSupplierTO();
                    }
                },{
                    text: '<fmt:message key="quit"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                    }
                }]
            });
            setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),15);	//计算.grid的高度
            setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            $(".page").css("margin-bottom",$(".mainFrame").height()-20);
            $('.grid').find('.table-body').find('tr').hover(
                    function(){
                        $(this).addClass('tr-over');
                    },
                    function(){
                        $(this).removeClass('tr-over');
                    }
            );
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                if ($(this).hasClass("bgBlue")) {
                    $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
                }
                else
                {
                    $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
                }
<%--                 var  url="<%=path%>/supplierrm/srmchildlist.do?pk_recruit="+$(this).find(":checkbox").val(); --%>
//                 $('#mainFrame').attr('src',encodeURI(url));
            });
//             if($(".table-body").find("tr").size()>0){
<%--                 var  url="<%=path%>/supplierrm/srmchildlist.do?pk_recruit="+$(".table-body").find("tr:eq(0)").find(":checkbox").val(); --%>
//                 $('#mainFrame').attr('src',encodeURI(url));
//             }
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
        });
        //====================================================
        //新增方法
        var add=function(){
            $('body').window({
                title: '<fmt:message key="insert"/>',
                content: '<iframe id="addSupplierRMFrame" frameborder="0" src="<%=path%>/supplierrm/rminsertorupdate.do"></iframe>',
                width: '700px',
                height: '500px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [{
                        text: '<fmt:message key="save"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-80px','-0px']
                        },
                        handler: function(){
                        	if(getFrame('addSupplierRMFrame').checkvcoderole()){
								$(".ui-draggable").css("z-index","101");
								getFrame('addSupplierRMFrame').showWait();
                        		getFrame('addSupplierRMFrame').insertSupplierRM();
                        	}
                        }
                             
//                             if(getFrame('skipSupplierRMFrame')&&window.document.getElementById("skipSupplierRMFrame").contentWindow.validate._submitValidate()){
// 								submitFrameForm('skipSupplierRMFrame','skipSupplierRMFrame');
 								//window.document.getElementById("addSupplierRMFrame").contentWindow.insertSupplierRM();
                    },{
                        text: '<fmt:message key="cancel" /> ',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px','-100px']
                        },
                        handler: function(){
                            $('.close').click();
                        }
                    }
                    ]
                }
            });
        };
        //修改方法
        var update=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var flag=false;
				if($.trim(checkboxList.filter(':checked').closest("tr").find("td").eq(6).text())!='<fmt:message key="gyszmwks"/>'){
					flag = true;
				}               
                if(flag){
					alerterror('<fmt:message key="gyszmupdatemsg"/>');
					return false;
                } 	
                var action='<%=path%>/supplierrm/rminsertorupdate.do?pk_recruit='+checkboxList.filter(':checked').val();
                $('body').window({
                    title: '<fmt:message key="update"/>',
                    content: '<iframe id="updateSupplierRMFrame" frameborder="0" src="'+action+'"></iframe>',
                    width: '700px',
                    height: '500px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [
                            {
                                text: '<fmt:message key="save"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-80px', '-0px']
                                },
                                handler: function () {
    								$(".ui-draggable").css("z-index","101");
                                    getFrame('updateSupplierRMFrame').updateSupplierRM();
                                }
                            },
                            {
                                text: '<fmt:message key="cancel"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-160px', '-100px']
                                },
                                handler: function () {
                                    $('.close').click();
                                }
                            }
                        ]
                    }
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerterror('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerterror('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        //删除方法
    var del=function(){
        var flag=false;
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
                var codeValue=[];
                checkboxList.filter(':checked').each(function(){
                	//判断是否是进行中、已完成的数据，有的话就返回提示不能删除
					if($.trim($(this).closest("tr").find("td").eq(6).text())!='<fmt:message key="gyszmwks"/>'){
						flag = true;
					}                	
                    codeValue.push($(this).val());
                });
                if(flag){
					alerterror('<fmt:message key="gyszmscmsg"/>');
					return false;
                }
        	alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
                var action = '<%=path%>/supplierrm/deleteSupplierRM.do?ids='+codeValue.join(",");
                $('body').window({
                    title: '<fmt:message key="delete_the_information_notice"/>',
                    content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                    width: 538,
                    height: '215px',
                    draggable: true,
                    isModal: true
                });
            });
        }else{
            alerterror('<fmt:message key="please_select_information_you_need_to_delete"/>！');
            return ;
        }
    };
    var uEna=function(et){
        var flag=false;
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            var codeValue=[];
            checkboxList.filter(':checked').each(function(){
            	//判断是否是进行中、已完成的数据，有的话就返回提示不能删除
				if($.trim($(this).closest("tr").find("td").eq(6).text())!='<fmt:message key="gyszmwks"/>'){
					flag = true;
				}
                codeValue.push($(this).val());
            });
            if(flag){
				alerterror('<fmt:message key="gyszmscmsg"/>');
				return false;
            }
            var action = '<%=path%>/supplierrm/uEnablestate.do?enablestate='+et+'&ids='+codeValue.join(",");
            $('body').window({
                title: '<fmt:message key="status"/><fmt:message key="update"/>',
                content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                width: '200px',
                height: '200px',
                draggable: true,
                isModal: true
            });
        }else{
            alerterror('<fmt:message key="please_select_at_least_one_data"/>！');
            return ;
        }
    };
  //新增应标信息
	function addSupplierTO(){
		var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		if(checkboxList 
				&& checkboxList.filter(':checked').size() == 1){
			var aim = checkboxList.filter(':checked').eq(0);
			var chkValue = aim.val();
			$('body').window({
				id: 'window_supply',
				title: '添加应募供应商',
				content: '<iframe id="addSupplierTOFrame" frameborder="0" src="<%=path%>/supplierrm/toAddSupplierTO.do?pk_recruit='+chkValue+'"></iframe>',
				width: '750px',
				height: '500px',
				draggable: true,
				isModal: true,
				topBar: {
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save" />应标供应商',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-80px','-0px']
							},
							handler: function(){
								if(getFrame('addSupplierTOFrame') && getFrame('addSupplierTOFrame').validate._submitValidate()){
									submitFrameForm('addSupplierTOFrame','addSupplierTOForm');
								}
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								position: ['-160px','-100px']
							},
							handler: function(){
								$('.close').click();
							}
						}
					]
				}
			});
		}else if(checkboxList 
				&& checkboxList.filter(':checked').size()>1){
			alerterror('<fmt:message key="please_select_data" />!');
		}else{
			alerterror('只能选择一条招募信息进行应募！');
			return ;
		}
	}
    //供应商招募发布
	function publishSupplierRM(){
		var flag=false;
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            var codeValue=[];
            checkboxList.filter(':checked').each(function(){
            	//判断是否是已完成的数据，有的话就返回提示不能发布
				if($.trim($(this).closest("tr").find("td").eq(6).text())!='<fmt:message key="gyszmwks"/>'){
					flag = true;
				}
                codeValue.push($(this).val());
            });
            if(flag){
				alerterror('<fmt:message key="gyszmpublish"/>');
				return false;
            }
			$.ajaxSetup({async:false});
			var datas = {};
			datas["pk_recruit"] = codeValue.join(",");
			$.post('<%=path%>/supplierrm/releaseSupplierRM.do',datas,function(data){
				if(data!='1'){
					alerterror(data);
					return;
				}else{
					alerttipsbreak('<fmt:message key="publish_sussess"/>',function(){
						$("#listForm").submit();
					});
				}
			});
        }else{
            alerterror('<fmt:message key="please_select_at_least_one_data"/>！');
            return ;
        }
	}
    //设置供应商招募状态为已完成
	function updateSupplierRMComplete(){
		var flag=false;
        var checkboxList = $('#grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            var codeValue=[];
            checkboxList.filter(':checked').each(function(){
            	//判断是否是正在进行的数据，不是的话就返回提示不能设置状态为完成
				if($.trim($(this).closest("tr").find("td").eq(6).text())!='<fmt:message key="gyszmzjx"/>'){
					flag = true;
				}
                codeValue.push($(this).val());
            });
            if(flag){
				alerterror('<fmt:message key="gyszmcomplete"/>');
				return false;
            }
			$.ajaxSetup({async:false});
			var datas = {};
			datas["pk_recruit"] = codeValue.join(",");
			$.post('<%=path%>/supplierrm/updateEnlist.do',datas,function(data){
				if(data!='1'){
					alerterror(data);
					return;
				}else{
					alerttipsbreak('<fmt:message key="update_successful"/>',function(){
						$("#listForm").submit();
					});
				}
			});
        }else{
            alerterror('<fmt:message key="please_select_at_least_one_data"/>！');
            return ;
        }
	}
	//显示详情
	function showdetail(pk_recruit){
		var url = '<%=path%>/supplierrm/toSupplierTOInfo.do?pk_recruit='+pk_recruit;
		$('body').window({
			id: 'window_showYbInfo',
			title: '供应商应募详情',
			content: '<iframe id="listShowYbInfoFrame" frameborder="0" src='+url+'></iframe>',
			width: 900,
			height: 500,
			confirmClose: false,
			draggable: true,
			isModal: true
		});
	}
    </script>
</body>
</html>
