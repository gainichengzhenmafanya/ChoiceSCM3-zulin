<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title><fmt:message key="gyszmnewgys"/></title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
	<form action="<%=path %>/ftputil/download.do"  id="downloadFile" method="post" >
		<input type="hidden"  id="filePaths" name="filePaths"  value="${supplierRM.vupfile}" />
	</form>
    <div class="easyui-tabs" fit="false" plain="true" id="tabs">
        <input type="hidden" id="pk_material"/>
        <div title='<fmt:message key="basic_information"/>' id="tab1" style="padding:10px;">
            <div class="condition">
                <div class="form-line">
                    <div class="form-label"><span style="color: red;">*</span>招募书编号  </div>
                    <div class="form-input">
                    	 <input type="hidden" value="${supplierRM.pk_recruit}" id="pk_recruit"/>
                         <input type="text" id="vcode" value="${supplierRM.vcode}" <c:if test="${quote}">readonly="readonly"</c:if> name="vcode" class="text"/>
                    </div>
                    <div class="form-label"><span style="color: red;">*</span>交易方式    </div>
                    <div class="form-input">
                        <input style="margin-top:3px;" id="ijyfsyi" name="ijyfs" type="radio" <c:if test="${supplierRM.ijyfs==1}"> checked="checked" </c:if> value="1" checked="checked" />支付宝
                        <input style="margin-top:3px;" id="ijyfser" name="ijyfs" type="radio" <c:if test="${supplierRM.ijyfs==2}"> checked="checked" </c:if> value="2" />其他交易方式
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span style="color: red;">*</span>招募书名称  </div>
                    <div class="form-input">
                        <input type="text" id="vname" value="${supplierRM.vname}" name="vname" class="text"/>
                    </div>
                    <div class="form-label"><span style="color: red;">*</span>发票要求    </div>
                    <div class="form-input">
                    	<select class="select" id="ifpyq" name="ifpyq" style="width: 130px;">
                            <option <c:if test="${supplierRM.ifpyq==1}"> selected="selected" </c:if> value="1">无需发票</option>
                            <option <c:if test="${supplierRM.ifpyq==2}"> selected="selected" </c:if> value="2">普通发票</option>
                            <option <c:if test="${supplierRM.ifpyq==3}"> selected="selected" </c:if> value="3">增值税发票</option>
                        </select>
<%--                         <input  style="margin-top:3px;" id="ifpyqyi" name="ifpyq" type="radio" <c:if test="${supplierRM.ifpyq==1}"> checked="checked" </c:if> value="1" checked="checked" />无需发票 --%>
<%--                         <input  style="margin-top:3px;" id="ifpyqer" name="ifpyq" type="radio" <c:if test="${supplierRM.ifpyq==2}"> checked="checked" </c:if> value="2"  />普通发票 --%>
<%--                         <input  style="margin-top:3px;" id="ifpyqsan" name="ifpyq" type="radio" <c:if test="${supplierRM.ifpyq==3}"> checked="checked" </c:if> value="3" />增值税发票 --%>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label">招募品类    </div>
                    <div class="form-input">
                        <input type="text" id="vrmcategory" value="${supplierRM.vrmcategory}" name="vrmcategory" class="text"/>
                    </div>
                    <div class="form-label">供应商注册资金    </div>
                    <div class="form-input">
                        <input type="text" id="vsregmoney" value="${supplierRM.vsregmoney}" name="vsregmoney" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label">所属行业    </div>
                    <div class="form-input">
                        <input type="text" id="vindustry" value="${supplierRM.vindustry}" name="vindustry" class="text"/>
                    </div>
                    <div class="form-label">需求描述    </div>
                    <div class="form-input">
                        <input type="text" id="vreqdescription" value="${supplierRM.vreqdescription}" name="vreqdescription" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label">年计划采购金额    </div>
                    <div class="form-input">
                        <input type="text" id="vyearordrmoney" value="${supplierRM.vyearordrmoney}" name="vyearordrmoney" class="text"/>
                    </div>
                    <div class="form-label">上传附件    </div>
                    <div class="form-input">
                        <input type="hidden" id="vupfile" value="${supplierRM.vupfile}" name="vupfile"/>
                        <input type="text" id="filepath" name="filepath" readonly="readonly"/>
                        <input type="button" id="browsebutton" name="browsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
						<input type="button" id="clearbutton" name="clearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
						<input type="hidden" id="importbutton" name="importbutton"  value="上传"/>
						<c:if test="${not empty supplierRM.vupfile}">
							<input type="button" id="downFileNow" value="下载"  onclick="downloadFile()"/>
						</c:if>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span style="color: red;">*</span>招募发布日期    </div>
                    <div class="form-input">
                       <input type="text" id="dreleasedat" name="dreleasedat" value="${supplierRM.dreleasedat}" class="Wdate text"  />
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span style="color: red;">*</span>报名截止日期    </div>
                    <div class="form-input">
                      <input  style="margin-top:3px;" id="iregyi" name="ireg" type="radio" <c:if test="${supplierRM.ireg==1}"> checked="checked" </c:if> value="1" onclick="zdyrqs()" checked="checked" />长期有效
                      <input  style="margin-top:3px;" id="ireger" name="ireg" type="radio" <c:if test="${supplierRM.ireg==2}"> checked="checked" </c:if> value="2" onclick="zdyrq()" />自定义
                    </div>
                </div>
                <div id="zdy" class="form-line" <c:if test="${supplierRM.ireg!=2}">style="display: none"</c:if>>
                	<div class="form-label">报名截止日期   </div>
                	<div class="form-input">
                      <input type="text" id="denddat" name="denddat" value="${supplierRM.denddat}" class="Wdate text"  />
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span style="color: red;">*</span><fmt:message key="gyszmzt"/>：</div>
                    <div class="form-input">
                        <select class="select" id="istate" name="istate" style="width: 130px;">
                            <option <c:if test="${supplierRM.istate==3}"> selected="selected" </c:if> value="3"><fmt:message key="gyszmwks"/></option>
                            <option <c:if test="${supplierRM.istate==1}"> selected="selected" </c:if> value="1"><fmt:message key="gyszmzjx"/></option>
                            <option <c:if test="${supplierRM.istate==2}"> selected="selected" </c:if> value="2"><fmt:message key="gyszmywc"/></option>
                        </select>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label">备注    </div>
                    <div class="form-input">
                       <textarea id="vmemo" name="textarea"  rows="6" style="resize:none; width: 440px;margin-top: 5px;">${supplierRM.vmemo}</textarea>
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/validate.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/upload/plupload.full.min.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
    <script type="text/javascript">
    var uploader = "";
    var actionUrl="<%=path%>/supplierrm/addSupplierRM.do";
    $.ajaxSetup({
		async: false
	});
        $(document).ready(function(){
        	$("#dreleasedat").click(function(){
				new WdatePicker({maxDate:'#F{$dp.$D(\'denddat\')}'});
			});
			$("#denddat").click(function(){
				new WdatePicker({minDate:'#F{$dp.$D(\'dreleasedat\')}'});
			});

        	if($("#pk_recruit").val()==""){
        		$("#vcode").focus();
        	}else{
        		$("#vname").focus();
        	}
            $('.easyui-tabs').tabs({
                width:$(document.body).width()*0.99,
                height:$(document.body).height()*0.99,
            });
            /*供应商招募主表基本验证*/
            supplierval = new Validate({
                validateItem:[{
                    type:'text',
                    validateObj:'vcode',
                    validateType:['canNull','maxLength','alphcomb'],
                    param:['F','50','F'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="the_maximum_length"/>50!','<fmt:message key="onlynumletter" />']
                },{
                    type:'text',
                    validateObj:'vname',
                    validateType:['canNull','maxLength'],
                    param:['F','50'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="the_maximum_length"/>50!']
                },{
                    type:'text',
                    validateObj:'dreleasedat',
                    validateType:['canNull'],
                    param:['F'],
                    error:['<fmt:message key="cannot_be_empty"/>!']
                },{
                    type:'text',
                    validateObj:'vyearordrmoney',
                    validateType:['num2'],
                    param:['T'],
                    error:['<fmt:message key="please_enter_a_number_greater"/>!']
                },{
                    type:'text',
                    validateObj:'vsregmoney',
                    validateType:['num2'],
                    param:['T'],
                    error:['<fmt:message key="please_enter_a_number_greater"/>!']
                },{
                    type:'text',
                    validateObj:'vrmcategory',
                    validateType:['maxLength'],
                    param:['50'],
                    error:['<fmt:message key="the_maximum_length"/>50!']
                },{
                    type:'text',
                    validateObj:'vindustry',
                    validateType:['maxLength'],
                    param:['50'],
                    error:['<fmt:message key="the_maximum_length"/>50!']
                },{
                    type:'text',
                    validateObj:'vreqdescription',
                    validateType:['maxLength'],
                    param:['50'],
                    error:['<fmt:message key="the_maximum_length"/>50!']
                },{
                    type:'text',
                    validateObj:'vmemo',
                    validateType:['maxLength'],
                    param:['300'],
                    error:['<fmt:message key="the_maximum_length"/>300!']
                }]
            });
			$("#clearbutton").click(function(){
				$(this).prev().prev().val('');	
			});
        	//导入控件=======================================================================
       		var filepathid = "filepath";//文件目录名id
			var browsebuttonid = "browsebutton";//选择文件按钮id
			var importbutton = "importbutton";//导入文件按钮id
			uploader = new plupload.Uploader({
			    browse_button : browsebuttonid, //触发文件选择对话框的按钮，为那个元素id
			    url : "<%=path%>/ftputil/uploadFile.do", //服务器端的上传页面地址
			    flash_swf_url : '<%=path%>/js/assistant/upload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
			    silverlight_xap_url : '<%=path%>/js/assistant/upload/Moxie.xap',//silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
			    multipart_params:dh(),//传入参数
			    multi_selection:false,
			    runtimes:'html5,html4,silverlight,flash',
			    filters: {
			        mime_types : [ { title : "word", extensions : "doc,docx" }, 
								    { title : "pdf", extensions : "pdf" }],
			        max_file_size : '30mb' //最大只能上传30mb的文件
			    },
			    init : {
			        PostInit: function() {
			            //最后给"开始上传"按钮注册事件
			            $("#"+importbutton).click(function(){
			            	if($("#filepath").val()){
				                uploader.start(); //调用实例对象的start()方法开始上传文件，可以在其他地方调用该方法
			            	}else{
			            		var inf ='{"flag":"ok"}';
			            		analyseErrorInfo(inf);
			            	}
			            });
			        },
			        FileUploaded:function(uploader,file,responseObject){
			        	var preinfo = responseObject.response;
			        	var errorinfo = '';
			        	if(preinfo.indexOf("</pre>")>0||preinfo.indexOf("</PRE>")>0){
			        		errorinfo = preinfo.substring(preinfo.indexOf(">")+1,preinfo.lastIndexOf("<")); 
			        	}else{
			        		errorinfo = preinfo;
			        	}
			        	$("#"+filepathid).val('');
			        	parent.$("#wait").hide();
			        	parent.$("#wait2").hide();
			        	analyseErrorInfo(errorinfo);
			        },
			        FilesAdded:function(uploader,files){
			            if(uploader.files.length>1){
			                uploader.removeFile(uploader.files[0]);
			            }
			            for(var i = 0, len = files.length; i<len; i++){
			                var file_name = files[i].name; //文件名
			                $("#"+filepathid).val(file_name);
			            }
			        },
			        Error:function(uploader,file){
			        	parent.$("#wait").hide();
			        	parent.$("#wait2").hide();
			            switch (file.code){
			                case -601:
			                    alerterror("<fmt:message key='typ_file_docpdf' />");
			                    break;
			                case -600:
			                	alerttips("<fmt:message key='file_maxsize' />30M！");
			                    break;			                
			                default :
			                    alerterror("<fmt:message key='upload' /><fmt:message key='failure' />！");
			                    break;
			            }
			        },
			        UploadProgress:function(uploader,file){
			        }
			    }
			});
			//==============================================================================
			uploader.init();
        });
        //===============================新增操作==================================
        var sspdrq;
        var data = {};
        var dh=function() {
            data.pk_recruit=$("#pk_recruit").val();
            data.vcode = $("#vcode").val();
            data.vname = $("#vname").val();
            data.vrmcategory = $("#vrmcategory").val();
            data.vsregmoney = $("#vsregmoney").val();
            data.vindustry = $("#vindustry").val();
            data.vreqdescription = $("#vreqdescription").val();
            data.vyearordrmoney = $("#vyearordrmoney").val();
            data.vupfile = $("#vupfile").val();
            data.vmemo = $("#vmemo").val();
            data.ijyfs = !!$("input[name='ijyfs']").attr("checked") ? 1 : 2;
            data.ifpyq = $("#ifpyq").val();
            //data.ifpyq = !!$("input[name='ifpyq']").attr("checked") ? 1 : 2;
            data.dreleasedat = $("#dreleasedat").val();
            data.denddat = $("#denddat").val();
            data.istate = $("#istate").val();
            data.ireg = !!$("input[name='ireg']").attr("checked") ? 1 : 2;
            var sd = new Date((data.dreleasedat).replace("-","/"));
            var ed = new Date((data.denddat).replace("-","/"));
            if(Date.parse(sd)-Date.parse(ed)>=0){
            	sspdrq = "1";
            }
            data.inputName="file";
            return data;
        };
		var savelock = false;
        var insertSupplierRM=function(){
			if(savelock){
				return;
			}else{
				savelock = true;
			}
        	dh();
        	if(sspdrq !="1"){
        		if (supplierval._submitValidate()) {
	            	actionUrl="<%=path%>/supplierrm/addSupplierRM.do";
                	$("#importbutton").click();
					savelock = false;
                }else{
					savelock = false;
				}
        	}else{
        		alerterror('自定义报名截止日期要大于招募发布日期！');
				savelock = false;
        	}
            
        };
        //===============================修改操作==================================
        var updateSupplierRM=function(){
			if(savelock){
				return;
			}else{
				savelock = true;
			}
        	dh();
        	if(sspdrq !="1"){
	            if (supplierval._submitValidate()) {
	            	actionUrl="<%=path%>/supplierrm/updateSupplierRM.do";
                	$("#importbutton").click();
					savelock = false;
	            }else{
					savelock = false;
				}
        	}else{
        		alerterror('自定义报名截止日期要大于招募发布日期！');
				savelock = false;
        	}
        };

        function zdyrq() {
        	document.getElementById('zdy').style.display = "block";
        }
        function zdyrqs() {
        	document.getElementById('zdy').style.display = "none";
        }
    	//验证编码规则
	function checkvcoderole(){
    		var check = false;
    		var param = {};
    		param['code'] = $("#vcode").val();
    		param['codeMark'] = "00000000000000000000000000000001";
    		$.post('<%=path%>/coderule/codeformat.do',param,function(re){
    			if(re=='ok'){
    				check = true;
    			}
    		});
    		if(!check){
    			alerterror('编码不符合编码规则！');
    			return false;
    		}
    		return true;
	}
    	//	数据保存方法
    	function analyseErrorInfo(msg){
    		var result = eval("("+msg+")");//转换为json对象 
    		debugger;
    		 var flag = result.flag;
    		 var res = result.res;
    		  if (flag == 'ok') {
    			  if(res=="" || res == undefined ){
    				  data.vupfile = $("#vupfile").val();
    			  }else{
	    			  data.vupfile = res;
    			  }
    			  if($("#pk_recruit").val()){
    				  $.post('<%=path%>/supplierrm/updateSupplierRM.do', data, function (msg) {
	                    if (msg == 'ok') {
	                    	alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
                                parent.$("#listForm").submit();
	                    	});
	                    } else {
	                       alerterror(msg);
	                    }
	                });
    			  }else{
    				  $.post('<%=path%>/supplierrm/addSupplierRM.do', data, function (msg) {
                        if (msg == 'ok') {
                        	alerttipsbreak('<fmt:message key="successful_added"/>！',function(){
                                parent.$("#listForm").submit();
	                    	});
                        } else {
                           alerterror(msg);
                        }
                    });
    			  }
              } else {
                 alerterror(flag);
              }
    	}
    	//下载文件
    	function downloadFile(){
    		$("#filePaths").val($("#vupfile").val());
			$("#downloadFile").attr("action","<%=path %>/ftputil/download.do");
			$("#downloadFile").submit();
    	}
    	
    	function showWait(){
    		parent.$("#wait").show();
    		parent.$("#wait2").show();
    	}
    </script>
</body>
</html>
