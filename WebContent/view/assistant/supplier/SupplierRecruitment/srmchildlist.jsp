<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title><fmt:message key="gyszmym"/></title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
<div class="easyui-tabs" fit="false" plain="true" style="z-index:88;">
    <div title='<fmt:message key="gyszmymsqgys"/>' id="y_1">
<!--     	<input type="hidden" id="showId" name="showId" /> -->
        <div class="grid">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 80px;"><fmt:message key="gyszmymgysbh"/></span></td>
                        <td><span style="width: 150px;"><fmt:message key="gyszmymgysmc"/></span></td>
<!--                         <td><span style="width: 120px;">所属组织</span></td> -->
<!--                         <td><span style="width: 120px;">供应商财务组织</span></td> -->
                        <td><span style="width: 80px;"><fmt:message key="gyszmymlxr"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="gyszmymlxrdh"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="gyszmymdz"/></span></td>
                        <td><span style="width: 161px;"><fmt:message key="gyszmymcz"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td><span style="width: 80px;" id="j_vsuppliercode">${supplierTO.delivercode}</span></td>
                        <td><span style="width: 150px;" id="j_vsuppliername">${supplierTO.delivername}</span></td>
<%--                         <td><span style="width: 120px;" id="j_pk_org">${supplierTO.pk_org}</span></td> --%>
<%--                         <td><span style="width: 120px;" id="j_pk_org_f">${supplierTO.pk_org_f}</span></td> --%>
                        <td><span style="width: 80px;" id="j_vsuppliercontact">${supplierTO.vsuppliercontact}</span></td>
                        <td><span style="width: 80px;" id="j_vsupplierphone">${supplierTO.vsupplierphone}</span></td>
                        <td><span style="width: 120px;" id="j_vsupplieraddress">${supplierTO.vsupplieraddress}</span></td>
                      	<td>
                        	<span style="width:100px;text-align:center;">
                        		<a onclick="addGysList('${supplierTO.delivercode}')">
									<input type="button" value='<fmt:message key="gyszmymjrgysdsh" />'></input>
								</a>
							</span>
						</td>
						<td>
							<span style="width:50px;text-align: center;">
								<a onclick="deleteyminfo('${supplierTO.delivercode}')">
										<input type="button" value='<fmt:message key="gyszmymscsc" />'></input>
								</a>
							</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
     <div title='<fmt:message key="gyszmymyscgys"/>' id="y_2" >
        <div class="grid" id="grid2">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 80px;"><fmt:message key="gyszmymgysbh"/></span></td>
                        <td><span style="width: 150px;"><fmt:message key="gyszmymgysmc"/></span></td>
<!--                         <td><span style="width: 120px;">所属组织</span></td> -->
<!--                         <td><span style="width: 120px;">供应商财务组织</span></td> -->
                        <td><span style="width: 80px;"><fmt:message key="gyszmymlxr"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="gyszmymlxrdh"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="gyszmymdz"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="gyszmymcz"/></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td><span style="width: 80px;" id="j_vsuppliercode">${supplierTOS.delivercode}</span></td>
                        <td><span style="width: 150px;" id="j_vsuppliername">${supplierTOS.delivername}</span></td>
<%--                         <td><span style="width: 120px;" id="j_pk_org">${supplierTO.pk_org}</span></td> --%>
<%--                         <td><span style="width: 120px;" id="j_pk_org_f">${supplierTO.pk_org_f}</span></td> --%>
                        <td><span style="width: 80px;" id="j_vsuppliercontact">${supplierTOS.vsuppliercontact}</span></td>
                        <td><span style="width: 80px;" id="j_vsupplierphone">${supplierTOS.vsupplierphone}</span></td>
                        <td><span style="width: 120px;" id="j_vsupplieraddress">${supplierTOS.vsupplieraddress}</span></td>
                        <td>
                        	<span style="width:50px;text-align: center;">
									<a onclick="hfyminfo('${supplierTOS.delivercode}')">
										<input type="button" value='<fmt:message key="gyszmymhfhf" />'></input>
									</a>
							</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
	var savelock = false;
    $(document).ready(function(){
//         $("#y_2").find("input[value='"+${supplier.iaccttype}+"']").attr("checked","checked");
//         $("#y_2").find("#iacctdate").find("option[value='"+${supplier.iacctdate}+"']").attr("selected","selected");
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth);
                $grid.find(".table-head").width(headWidth);
            }else{
                $grid.find(".table-body").width(gridWidth);
                $grid.find(".table-head").width(gridWidth);
            }
        };
        $('.easyui-tabs').tabs({
            width:$(document.body).width()-2,
            height:$(document.body).height()-34,
            onSelect:function(){
                var id=$(this).tabs('getSelected')[0].id;
                setElementHeight('#'+id+' .grid',['.tool'],$(document.body),90);	//计算.grid的高度
                setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
                loadGrid('#'+id+' .grid');
                setWidth('#'+id+' .grid');
            }
        });
    });
   	function addGysList(code){
   		if(code == ""){
   			alerterror('<fmt:message key="please_select_data"/>！');
   			return;
   		}
		if(savelock){
			return;
		}else{
			savelock = true;
		}
   		$.ajaxSetup({async:true});
		var datas = {},pk_check;
		datas["code"] = code;
		alertconfirm('<fmt:message key="quereningysshenhe"/>？',function(){
            $.post('<%=path%>/supplierrm/addymgyslb.do', datas, function (msg) {
                if (msg == 'ok') {
                	alerttipsbreak('<fmt:message key="successful"/>！',function(){
                		reload();
    					savelock = false;
                	});
                } else {
                    alerterror('<fmt:message key="already_exists"/>！');
					savelock = false;
                }
            });
			savelock = false;
       });
    }
   	function deleteyminfo(code){
   		if(code == ""){
   			alerterror('<fmt:message key="please_select_information_you_need_to_delete"/>！');
   			return;
   		}
   		$.ajaxSetup({async:true});
		var datas = {},pk_check;
		datas["code"] = code;
		alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
            $.post('<%=path%>/supplierrm/updateyminfo.do', datas, function (msg) {
                if (msg == 'ok') {
                	alerttips('<fmt:message key="successful_deleted"/>！',function(){
                		reload();
                	});
                } else {
                    alerterror('<fmt:message key="delete_fail"/>！');
                }
            });
       });
    }
   	function hfyminfo(code){
   		if(code == ""){
   			alerterror('<fmt:message key="please_select_data"/>！');
   			return;
   		}
   		$.ajaxSetup({async:true});
		var datas = {},pk_check;
		datas["code"] = code;
		alertconfirm('<fmt:message key="querenhfcitiaodelete"/>？',function(){
            $.post('<%=path%>/supplierrm/hfupdateyminfo.do', datas, function (msg) {
                if (msg == 'ok') {
                	alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
                		reload();
                	});
                } else {
                    alerterror('<fmt:message key="update_fail"/>！');
                }
            });
       });
    }
   	var reload=function(){
        if(typeof(eval(parent.pageReload))=='function') {
            pageReload();
        }else {
            location.href = location.href;
        }
    };
</script>
</body>
</html>
