<%--
  User: mc
  Date: 14-12-4
  Time: 下午10:01
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商评价</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<input type="hidden" value="${delivercode}" id="delivercode"/>
<div class="grid" id="grid">
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td><span style="width: 90px;"></span></td>
                <td><span style="width: 200px;">评价指标</span></td>
                <td><span style="width: 60px;">权重(%)</span></td>
                <td><span style="width: 60px;">得分</span></td>
                <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <c:forEach items="${evalitemList}" var="a" varStatus="st">
                    <tr>
                        <td><span style="width: 90px;text-align: center;" title="${st.index+1}">${st.index+1}</span></td>
                        <td><span style="width: 200px;" title="${a.vstandard}">${a.vstandard}</span></td>
                        <td><span style="width: 60px;text-align: right;" title="${a.vweight}">${a.vweight}</span></td>
                        <td><span style="width: 60px;"><input style="width: 56px;" onkeyup="isNumber(this)" onblur="isNumber(this)" type="text" maxlength="3"/>0</span></td>
                        <td><span style="width: 200px;" title="${a.vmemo}">${a.vmemo}</span></td>
                    </tr>
                </c:forEach>
                <tr id="nscore">
                    <td style="width: 90px;text-align:center;">最总得分</td>
                    <td><span style="width: 200px;">&nbsp;</span></td>
                    <td><span style="width: 60px;">&nbsp;</span></td>
                    <td name="nscore"><span style="width: 60px;text-align: right;">0</span></td>
                    <td><span style="width: 200px;">&nbsp;</span></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="save"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    saveEval();
                }
            },{
                text: '<fmt:message key="back"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $('.close',parent.document).click();
                }
            }]
        });
        setElementHeight('.grid',['.tool'],$(document.body),20);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
    });
    //========================================================
	var savelock = false;
    var saveEval=function(){
		if(savelock){
			return;
		}else{
			savelock = true;
		}
        var data={};
        data.pk_supplier=$("#delivercode").val();
        data.nscore=$("td[name='nscore']").find("span").text();
        var checkdata = true;
        var evalContent = 0;
        var obj = $(".table-body").find("tr");
        var length = obj.length;
        $(".table-body").find("tr").not("#nscore").each(function(i){
        	if($.trim($(this).find("td:eq(3)").find("input").val())==''){
        		alerterror('第'+$.trim($(this).find("td:eq(0)").find("span").text())+'行请输入得分！');
        		checkdata = false;
        		return false;
        	}
        	evalContent++;
            data["supplierevalds["+i+"].vstandard"]=$.trim($(this).find("td:eq(1)").find("span").text());
            data["supplierevalds["+i+"].vweight"]=$.trim($(this).find("td:eq(2)").find("span").text());
            data["supplierevalds["+i+"].nscore"]=$.trim($(this).find("td:eq(3)").find("input").val());
            data["supplierevalds["+i+"].vmemo"]=$.trim($(this).find("td:eq(4)").find("span").text());
        });
        //判断评价项是否为空
        if(evalContent==0 && length==0){
        	alerterror('请先到评价项设置中添加评价项。');
        	savelock = false;
        	return;
        }
        if(!checkdata){
        	savelock = false;
        	return;
        }
        
        $.post('<%=path%>/evaluate/evaluation.do', data, function (msg) {
            if (msg == 'success') {
                alerttipsbreak('<fmt:message key="save_successful"/>！',function(){
					savelock = false;
		            parent.$("#listForm").submit();
                });
            } else {
                alerterror(msg);
				savelock = false;
            }
        });
    };
    var isNumber=function(v){
        var val=$(v).val();
        if(val!=""){
            if(isNaN(val)){
                if(val.length>1) {
                    $(v).val(val.substring(0, val.length - 1));
                }else{
                    $(v).val(0);
                }
            }else if(0>Number(val)){
                alerterror('<fmt:message key="please_enter_a_number_greater"/>');
            }else{
                if(Number(val)>100){
                    $(v).val(100);
                }
            }
        }
        var nscoreAll=0;
        $(".table-body").find("tr").not("#nscore").each(function(i){
            var vweight=$.trim($(this).find("td:eq(2)").find("span").text());
            var nscore=$.trim($(this).find("td:eq(3)").find("input").val());
            if(isNaN(nscore)){
                nscore=0;
            }
            nscoreAll+=(vweight/100)*nscore;
        });
        $("td[name='nscore']").find("span").text(Number(nscoreAll).toFixed(2));
    };
</script>
</body>
</html>
