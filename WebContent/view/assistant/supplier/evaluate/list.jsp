<%--
  User: mc
  Date: 14-10-21
  Time: 上午10:14
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商评价</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
.tool {
	positn: relative;
	height: 27px;
}

.grid td span {
	padding: 0px;
}

.button :hover {
	background-image: url('<%=path%>/image/Button_new/newbtns.png');
	background-positn: 0px 0px;
	width: 80px;
	height: 35px;
	color: #FFFFFF;
}

.button input {
	width: 80px;
	height: 35px;
}
</style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div style="height:55%;">
    <form id="listForm" action="<%=path%>/evaluate/list.do" method="post">
    <div class="condition" style="margin-top:1px;height: 33px;border: solid 1px gray">
        <div class="form-line" style="margin-top: 3px;">
            <div class="form-label" style="width: 70px;margin-top: 2px;"><fmt:message key="coding"/>：</div>
            <div class="form-input" style="padding-top: 2px;">
                <input type="text" name="code" id="code" style="margin:2px;" value="${deliver.code}" class="text"/>
            </div>
            <div class="form-label" style="width: 70px;margin-top: 2px;"><fmt:message key="name"/>：</div>
            <div class="form-input" style="padding-top: 2px;">
                <input type="text" name="des" id="des" style="margin:2px;" value="${deliver.des}" class="text"/>
            </div>

            <div class="form-label" style="width: 70px;margin-top: 2px;"><fmt:message key="abbreviation_code"/>：</div>
            <div class="form-input" style="padding-top: 2px;">
                <input type="text" name="init" id="init" style="margin:2px;" value="${deliver.init}" class="text"/>
            </div>
<%--             <div class="form-label" style="width: 70px;margin-top: 2px;"><fmt:message key="enable_state"/>：</div> --%>
<!--             <div class="form-input"> -->
<!--                 <select name="enablestate" class="select" id="enablestate" style="margin-top: 4px;"> -->
<%--                     <option <c:if test="${deliver.enablestate==0}"> selected="selected" </c:if> value="0"><fmt:message key="all"/></option> --%>
<%--                     <option <c:if test="${deliver.enablestate==1}"> selected="selected" </c:if> value="1"><fmt:message key="not_enabled"/></option> --%>
<%--                     <option <c:if test="${deliver.enablestate==2}"> selected="selected" </c:if> value="2"><fmt:message key="have_enabled"/></option> --%>
<%--                     <option <c:if test="${deliver.enablestate==3}"> selected="selected" </c:if> value="3"><fmt:message key="stop_enabled"/></option> --%>
<!--                 </select> -->
<!--             </div> -->
            <div class="form-input">
<!--                 <span style="text-align: center;margin-top: -2px;" class="button"> -->
                    <input type="button" style="width:60px;height:100%;border: 2px;background-image: url('<%=path%>/image/Button_new/dsbtn.png');cursor: pointer;"  id="search" value='<fmt:message key="select" />'  <c:if test="${!elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}"> disabled="disabled"</c:if>/>
<%--                     <input type="button" style="width:60px" id="calculate2" name="calculate2" value='<fmt:message key="calculate" />2'/> --%>
<!--                 </span> -->
            </div>
        </div>
    </div>
    <div class="grid" id="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:30px;">
                            <input type="checkbox" id="chkAll"/>
                        </td>
                        <td><span style="width: 100px;"><fmt:message key="suppliers_type"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="suppliers_coding"/></span></td>
                        <td><span style="width: 180px;"><fmt:message key="suppliers_name"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="contact"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="address"/></span></td>
<%--                         <td><span style="width: 50px;"><fmt:message key="status"/></span></td> --%>
                        <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${supplierList}" var="supplier" >
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${supplier.code}' />" value="<c:out value='${supplier.code}' />"/>
                            </td>
                            <td><span style="width: 100px;text-align: left;" title="${supplier.typ}">${supplier.typ}</span></td>
                            <td><span style="width: 120px;text-align: left;" title="${supplier.code}">${supplier.code}</span></td>
                            <td><span style="width: 180px;text-align: left;" title="${supplier.des}">${supplier.des}</span></td>
                            <td><span style="width: 80px;text-align: left;">${supplier.person1}</span></td>
                            <td><span style="width: 200px;text-align: left;" title="${supplier.addr}">${supplier.addr}</span></td>
<!--                             <td><span style="width: 50px;text-align: center;"> -->
<%--                                 <c:if test="${supplier.enablestate==1}"><fmt:message key="not_enabled"/></c:if> --%>
<%--                                 <c:if test="${supplier.enablestate==2}"><fmt:message key="have_enabled"/></c:if> --%>
<%--                                 <c:if test="${supplier.enablestate==3}"><fmt:message key="stop_enabled"/></c:if> --%>
<!--                             </span> -->
<%--                              <input type="hidden" value="${supplier.enablestate}"/> --%>
<!--                             </td> -->
                            <td><span style="width: 200px;text-align: left;" title="${supplier.memo}">${supplier.memo}</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${pageobj}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
    </div>
    </form>
    </div>
    <div class="mainFrame" style=" height:50%;width:100%;">
        <span class="button" id="evaluate" style="z-index:10;text-align: center;">
            <input type="button" style="width:60px;height:20px;border: 2px;background-image: url('<%=path%>/image/Button_new/dsbtn.png');cursor: pointer;" <c:if test="${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}">onclick="addevaluate()"</c:if>  value="评分" <c:if test="${!elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}"> disabled="disabled"</c:if>/>
<%--             <input type="button" style="width:60px;height:100%;border: 2px;background-image: url('<%=path%>/image/Button_new/newbtns.png');cursor: pointer;"  id="search" value='<fmt:message key="select" />'  <c:if test="${!elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}"> disabled="disabled"</c:if>/> --%>
        </span>
        <span class="button" id="editEvaluate" style="z-index:10;text-align: center;">
            <input style="width:60px;height:20px;border: 2px;background-image: url('<%=path%>/image/Button_new/dsbtn.png');cursor:pointer;" <c:if test="${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}">onclick="editEvaluate()"</c:if>  type="button" value="评价项设置" <c:if test="${!elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')}"> disabled="disabled"</c:if>/>
        </span>
        <iframe src="<%=path%>/evaluate/listbottom.do" frameborder="0" name="mainFrame" id="mainFrame">
        </iframe>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        	$("#code").focus();
            $("#search").bind('click', function() {
            	if("${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}"!="true"){
	            	alerterror( '<fmt:message key="hasnoselectpernission"/>');
            		return;
            	}
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $("#code").val(stripscript($("#code").val()));
                $("#des").val(stripscript($("#des").val()));
                $("#init").val(stripscript($("#init").val()));
                $('.search-div').hide();
                $('#listForm').submit();
            });
            $("#evaluate").css("margin-left",$(document.body).width()-230);
            $("#editEvaluate").css("margin-left",$(document.body).width()-120);
            setElementHeight('.grid',['.condition','.mainFrame'],$(document.body),0);	//计算.grid的高度
            setElementHeight('.grid .table-body',['.grid .table-head'],'.grid',18); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            $(".page").css("margin-bottom",$(".mainFrame").height()-17);
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
                $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
                var  url="<%=path%>/evaluate/listbottom.do?delivercode="+$(this).find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            });
            $('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
                event.stopPropagation();
            });
            if($(".table-body").find("tr").size()>0){
                var  url="<%=path%>/evaluate/listbottom.do?delivercode="+$(".table-body").find("tr:eq(0)").find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            }
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
// 			$('.grid').find('.table-body').find('tr:eq(0)').click();
        });
        //====================================================
        //导出供应商
        function exportSupplier(){
            $("#wait2").val('NO');//不用等待加载
            $("#listForm").attr("action","<%=path%>/supplier/exportExcel.do");
            $("#listForm").submit();
            $("#wait2 span").html("数据导出中，请稍后...");
            $('#listForm').attr("action","<%=path%>/supplier/list.do");
            $("#wait2").val('');//等待加载还原
        }
        /**
         * 评价
         */
        var addevaluate=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                $('body').window({
                    title: '<fmt:message key="evaluateSupplier"/>',
                    content: '<iframe id="evaluateSupplierFrame" frameborder="0" src="<%=path%>/evaluate/showEvaluate.do?m=0&delivercode='+checkboxList.filter(':checked').val()+'"></iframe>',
                    width: '700px',
                    height: '350px',
                    draggable: true,
                    isModal: true
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        /**
         * 评价项设置
         */
        var editEvaluate=function(){
            $('body').window({
                title: '<fmt:message key="evaluateSupplier"/>',
                content: '<iframe id="addEvaluateSupplierFrame" frameborder="0" src="<%=path%>/evaluate/showEvaluate.do?m=1"></iframe>',
                width: '700px',
                height: '350px',
                draggable: true,
                isModal: true
            });
        };

    </script>
</body>
</html>
