<%--
  User: mc
  Date: 14-12-4
  Time: 下午10:01
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商评价体系</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<div class="grid" id="grid">
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td>
                    <span style="width: 30px;">
                        <input type="checkbox" id="chkAll"/>
                    </span>
                </td>
                <td><span style="width: 90px;">&nbsp;</span></td>
                <td><span style="width: 200px;">评价指标</span></td>
                <td><span style="width: 60px;">权重</span></td>
                <td><span style="width: 250px;"><fmt:message key="remark"/></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <c:forEach items="${evalitemList}" var="a" varStatus="st">
                    <tr>
                        <td style="width:30px; text-align: center;">
                            <span style="width: 30px;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${a.pk_evalitem}' />" value="<c:out value='${a.pk_evalitem}' />"/>
                            </span>
                        </td>
                        <td><span style="width: 90px;text-align: center;" title="${st.index+1}">${st.index+1}</span></td>
                        <td><span style="width: 200px;" title="${a.vstandard}"><input  style="width: 196px;" type="text" value="${a.vstandard}"/></span></td>
                        <td><span style="width: 60px;" title="${a.vweight}">
                            <select style="width:56px" >
                                <c:forEach begin="1" end="100" step="1" varStatus="i">
                                    <option value="${i.index}" <c:if test="${a.vweight==i.index}">selected="selected"</c:if>>${i.index}</option>
                                </c:forEach>
                            </select>
                        </span></td>
                        <td><span style="width: 250px;" title="${a.vmemo}"><input  style="width: 246px;" value="${a.vmemo}" type="text"/></span></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<span style="color: red;margin: 5px;">备注：启用的评价指标权重合计必须为100%</span>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="insert"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    var size=$(".table-body").find("tbody").find("tr").size();
                    $(".table-body").find("tbody").append('<tr name="new">'+
                            '<td style="width:30px; text-align: center;">'+
                            '<span style="width: 30px;">'+
                            '<input type="checkbox"  name="idList" id="chk_'+(Number(size)+1)+'" value="'+(Number(size)+1)+'"/>'+
                            '<input type="hidden" value="${a.delivercode}"/>'+
                            '</span>'+
                            '</td>'+
                            '<td><span style="width: 90px;text-align: center;">'+(Number(size)+1)+'</span></td>'+
                            '<td><span style="width: 200px;text-align: center;"><input style="width: 196px;" maxlength="50" type="text"/></span></td>'+
                            '<td><span style="width: 60px;text-align: center;"><select style="width:56px">'+
                            '<c:forEach begin="1" end="100" step="1" varStatus="i">'+
                                '<option value="${i.index}">${i.index}</option>'+
                            '</c:forEach>'+
                            '</select></span></td>'+
                            '<td><span style="width: 250px;text-align: center;"><input style="width: 246px;" maxlength="500" type="text"/></span></td>'+
                            '</tr>');
                }
            },{
                text: '<fmt:message key="delete"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                	if($('#grid').find('.table-body').find(':checkbox').filter(':checked').size()<1){
                		 alerterror('<fmt:message key="please_select_information_you_need_to_delete"/>！');
                         return ;
                	}
                    $('#grid').find('.table-body').find("tbody").find(':checkbox').filter(':checked').each(function(){
                    	var thisTr = $(this).parents("tr");
                    	thisTr.find("td").eq(3).html('');
                    	thisTr.remove();
//                         $(this).parent().closest('tr').remove();
                    });
                    $('#grid').find('.table-body').find(':checkbox').each(function(i){
                        $(this).parents("tr").find("td:eq(1)").find("span").text(i+1);
                    });
                }
            },'-',{
                text: '<fmt:message key="save"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    saveEval();
                }
            },{
                text: '<fmt:message key="back"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $('.close',parent.document).click();
                }
            }]
        });
        setElementHeight('.grid',['.tool'],$(document.body),30);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        //如果全选按钮选中的话，table背景变色
        $("#chkAll").click(function() {
            if (!!$("#chkAll").attr("checked")) {
                $('#grid').find('.table-body').find('tr').addClass("bgBlue");
            }else{
                $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
            }
        });
        //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').live("click", function () {
            if ($(this).hasClass("bgBlue")) {
                $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
            }
            else
            {
                $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
            }
        });
    });
    //========================================================
    var data={};
	var savelock = false;
    var saveEval=function(){
		if(savelock){
			return;
		}else{
			savelock = true;
		}
        var weight=0;
        var mark=true;
        $(".table-body").find("tr").each(function(i){
            var vstandard= $.trim($(this).find("td:eq(2)").find("input").val());
            if(vstandard==""){
            	alerterror('<fmt:message key="the"/>'+(Number(i)+1)+'<fmt:message key="line"/><fmt:message key="cannot_be_empty"/>');
				savelock = false;
                mark=false;
                return false;
            }
            var vweight= $.trim($(this).find("td:eq(3)").find("select").val());
            var vmemo=$.trim($(this).find("td:eq(4)").find("input").val());
            weight+=Number(vweight);
            data["evalitems["+i+"].vstandard"]=vstandard;
            data["evalitems["+i+"].vweight"]=vweight;
            data["evalitems["+i+"].vmemo"]=vmemo;
        });
        if(weight!=100){
            alerterror('启用的评价指标权重合计必须等于100');
			savelock = false;
            return;
        }
        if(mark) {
            $.post('<%=path%>/evaluate/saveEval.do', data, function (msg) {
                if (msg == 'success') {
                    alerttipsbreak('<fmt:message key="save_successful"/>！',function(){
    					savelock = false;
    		            parent.$("#listForm").submit();
                    });
                } else {
                    alerterror(msg);
					savelock = false;
                }
            });
        }
    };
</script>
</body>
</html>
