<%--
  User: mc
  Date: 14-10-24
  Time: 下午12:15
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div class="easyui-tabs" fit="false" plain="true" style="z-index:88;">
    <div title='当前评价' id="y_1">
        <div class="grid" id="grid1">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 100px;">打分日期</span></td>
                            <td><span style="width: 40px;">序号</span></td>
                            <td><span style="width: 200px;">评价指标</span></td>
                            <td><span style="width: 50px;">权重</span></td>
                            <td><span style="width: 50px;">得分</span></td>
                            <td><span style="width: 250px;">备注</span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${supplierevalList}" end="0" var="eval">
                            <c:forEach items="${eval.supplierevalds}" var="evald" varStatus="vs">
                                <tr>
                                    <c:if test="${vs.index==0}">
                                        <td rowspan="${fn:length(eval.supplierevalds)+1}"><span style="width: 100px;">${eval.devaldate}</span></td>
                                    </c:if>
                                    <td><span style="width: 40px;">${vs.index+1}</span></td>
                                    <td><span style="width: 200px;">${evald.vstandard}</span></td>
                                    <td><span style="width: 50px;text-align: right;">${evald.vweight}</span></td>
                                    <td><span style="width: 50px;text-align: right;">${evald.nscore}</span></td>
                                    <td><span style="width: 250px;">${evald.vmemo}</span></td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td><span style="width: 40px;"></span></td>
                                <td><span style="width: 200px;">最终得分</span></td>
                                <td><span style="width: 50px;"></span></td>
                                <td><span style="width: 50px;text-align: right;">${eval.nscore}</span></td>
                                <td><span style="width: 250px;"></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='历史评价' id="y_2">
        <div class="grid" id="grid2">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td><span style="width: 100px;">打分日期</span></td>
                        <td><span style="width: 40px;">序号</span></td>
                        <td><span style="width: 200px;">评价指标</span></td>
                        <td><span style="width: 50px;">权重</span></td>
                        <td><span style="width: 50px;">得分</span></td>
                        <td><span style="width: 250px;">备注</span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                        <tbody>
                        <c:forEach items="${supplierevalList}" var="eval">
                            <c:forEach items="${eval.supplierevalds}" var="evald" varStatus="vs">
                                <tr>
                                    <c:if test="${vs.index==0}">
                                        <td rowspan="${fn:length(eval.supplierevalds)+1}"><span style="width: 100px;">${eval.devaldate}</span></td>
                                    </c:if>
                                    <td><span style="width: 40px;">${vs.index+1}</span></td>
                                    <td><span style="width: 200px;">${evald.vstandard}</span></td>
                                    <td><span style="width: 50px;text-align: right;">${evald.vweight}</span></td>
                                    <td><span style="width: 50px;text-align: right;">${evald.nscore}</span></td>
                                    <td><span style="width: 250px;">${evald.vmemo}</span></td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td><span style="width: 40px;"></span></td>
                                <td><span style="width: 200px;">最终得分</span></td>
                                <td><span style="width: 50px;"></span></td>
                                <td><span style="width: 50px;text-align: right;">${eval.nscore}</span></td>
                                <td><span style="width: 250px;"></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth);
                $grid.find(".table-head").width(headWidth);
            }else{
                $grid.find(".table-body").width(gridWidth);
                $grid.find(".table-head").width(gridWidth);
            }
        };
        $('.easyui-tabs').tabs({
            width:$(document.body).width()-2,
            height:$(document.body).height()-34,
            onSelect:function(){
                var id=$(this).tabs('getSelected')[0].id;
                setElementHeight('#'+id+' .grid',['.tool'],$(document.body),90);	//计算.grid的高度
                setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
                loadGrid('#'+id+' .grid');
                setWidth('#'+id+' .grid');
            }
        });
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
    });
</script>
</body>
</html>
