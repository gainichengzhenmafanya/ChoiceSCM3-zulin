<%--
  User: 供应商列表 修改新增
  Date: 14-10-21
  Time: 下午3:09
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>新增供应商</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
    <div class="easyui-tabs" fit="false" plain="true" id="tabs">
        <input type="hidden" id="pk_material"/>
        <input type="hidden" id="operate" value="${operate}"/>
        <input type="hidden" id="selectpks" value=""/>
        <div title='<fmt:message key="basic_information"/>' id="tab1" style="padding:10px;">
            <div class="condition">
                <div class="form-line">
                    <div class="form-label"><fmt:message key="suppliers_type"/>：</div>
                    <div class="form-input">
                        <input type="hidden" value="${supplier.delivercode}" id="delivercode"/>
                        <select id="delivercodetype" class="select" onchange="getSupplierCode()" name="delivercodetype">
                            <c:forEach items="${supplierType}" var="type">
                                <option <c:if test="${type.delivercodetype==supplier.delivercodetype.delivercodetype}">selected="selected"</c:if> data="${type.vcode}" value="${type.delivercodetype}">${type.vcode}-${type.vname}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="relevance_supplier"/> ：</div>
                    <div class="form-input">
                        <input type="hidden" id="delivercodejum" value="${supplier.delivercodejum}"/>
                        <input type="text" id="supplierjum_name" <c:if test="${join!=0}">style="margin-bottom: 6px;" </c:if> value="${supplier.vnamejmu}" readonly="readonly" class="text"/>
                        <c:if test="${join!=0}">
                            <img onclick="showJmu()" src="<%=path%>/image/themes/icons/search.png" style="cursor: pointer;margin-bottom: 6px;"/>
                        </c:if>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span class="red">*</span><fmt:message key="suppliers"/><fmt:message key="coding"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vcode" maxlength="50" value="${supplier.vcode}" <c:if test="${quote}">readonly="readonly"</c:if> data="${supplier.vcode}" name="vcode" class="text"/>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="registered_capital"/>：</div>
                    <div class="form-input">
                        <input type="text" id="nsignmonty" maxlength="9" value="<fmt:formatNumber value="${supplier.nsignmonty}" pattern="#.##"/>" name="nsignmonty" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><span class="red">*</span><fmt:message key="suppliers"/><fmt:message key="name"/>：</div>
                    <div class="form-input">
                        <c:if test="${join==0}">
                            <input type="text" id="vname" maxlength="40" value="${supplier.vnamejmu}" name="vname" onblur="getSpInit(this,'vinit');" class="text"/>
                        </c:if>
                        <c:if test="${join!=0}">
                            <input type="text" id="vname" maxlength="40" value="${supplier.vname}" name="vname" onblur="getSpInit(this,'vinit');" class="text"/>
                        </c:if>

                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="Mnemonic"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vinit" maxlength="40" value="${supplier.vinit}" name="vinit" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="contact"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vcontact" maxlength="19" value="${supplier.vcontact}" name="vcontact" class="text"/>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="person_in_charge"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vlegal" maxlength="19" value="${supplier.vlegal}" name="vlegal" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="email"/>：</div>
                    <div class="form-input">
                        <input type="text" name="vmailaddr" maxlength="30" value="${supplier.vmailaddr}" id="vmailaddr" class="text"/>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="zip_code"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vzipcode" maxlength="6" value="${supplier.vzipcode}" name="vzipcode" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="www"/>：</div>
                    <div class="form-input">
                        <input type="text" name="vwebsite" maxlength="30" value="${supplier.vwebsite}" id="vwebsite" class="text"/>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="fax"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vfax" maxlength="30" value="${supplier.vfax}" name="vfax" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="tel"/>：</div>
                    <div class="form-input">
                        <input type="text" id="vtele" maxlength="14" value="${supplier.vtele}" name="vtele" class="text"/>
                    </div>
                    <div class="form-label" style="width: 120px;"><fmt:message key="whether_invoicing"/>：</div>
                    <div class="form-input" style="margin: auto;">
                        <input id="bistax" name="bistax" <c:if test="${supplier.bistax==0}">checked="checked" </c:if> type="checkbox" style="margin-top: 3px;"/>&nbsp;
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="address"/>：</div>
                    <div class="form-input">
                        <input type="text" name="vaddr" style="width: 413px;" maxlength="190" value="${supplier.vaddr}" id="vaddr" class="text"/>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="status"/>：</div>
                    <div class="form-input">
                        <select id="enablestate" class="select" name="enablestate">
                            <option <c:if test="${supplier.enablestate==2}"> selected="selected" </c:if> value="2"><fmt:message key="have_enabled"/></option>
                            <option <c:if test="${supplier.enablestate==1}"> selected="selected" </c:if> value="1"><fmt:message key="not_enabled"/></option>
                            <c:if test="${operate!=0}">
                                <option <c:if test="${supplier.enablestate==3}"> selected="selected" </c:if> value="3"><fmt:message key="stop_enabled"/></option>
                            </c:if>
                        </select>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="remark"/>：</div>
                    <div class="form-input">
                        <textarea id="vmemo" name="vmemo"  rows="5" style="resize:none; width: 413px;margin-top: 5px;">${supplier.vmemo}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div title='<fmt:message key="payment_days_set"/>' id="tab2" style="padding:10px;">
            <div class="condition">
                <div class="form-line">
                    <div class="form-label"><fmt:message key="bill_frequency"/>：</div>
                    <div class="form-input">
<%--                         <input name="iaccttype" type="radio" checked="checked" value="0" /><fmt:message key="immediately"/> --%>
                        <input name="iaccttype" type="radio" value="1" checked="checked" />30天
                        <input name="iaccttype" type="radio" value="2" />60天
                        <input name="iaccttype" type="radio" value="3" />90天
<%--                         <input name="iaccttype" type="radio" value="4" /><fmt:message key="half_a_year"/> --%>
<%--                         <input name="iaccttype" type="radio" value="5" /><fmt:message key="year"/> --%>
                    </div>
                </div>
                <div class="form-line">
                    <div class="form-label"><fmt:message key="bill_date"/>：</div>
                    <div class="form-input">
<%--                         <select id="iacctdate" <c:if test="${supplier.iaccttype==0||supplier.iaccttype==null||supplier.iaccttype==''}">disabled="disabled"</c:if> class="select" name="iacctdate"> --%>
                        <select id="iacctdate"  class="select" name="iacctdate">
                            <c:forEach begin="1" end="31" step="1" var="index">
                                <option <c:if test="${iacctdate==index}">selected="selected"</c:if> value="${index}">${index}<fmt:message key="code"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div title='<fmt:message key="fit_store"/>' id="tab3" style="padding:10px;">
            <div class="condition" id="storetool">
                <div class="form-line">
                    <div class="form-input" style="width: 40px;">
                        <input type="hidden" id="positncode"  />
                        <input type="button"  onclick="showOrg()" value="<fmt:message key="add"/>"/>
                    </div>
                    <div class="form-input" >
                        <input type="button" onclick="del('store-body')" value="<fmt:message key="delete"/>"/>
                    </div>
                </div>
            </div>
            <div class="grid" id="grid0">
                <div class="table-head" >
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <td>
                                <span style="width: 40px;">
                                    <input type="checkbox" id="chkAll"/>
                                </span>
                            </td>
                            <td><span style="width: 70px;"><fmt:message key="coding_stores_javapro"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="store"/><fmt:message key="name"/></span></td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body">
                    <table cellpadding="0" cellspacing="0">
                        <tbody id="store-body">
                            <c:forEach items="${supplier.supplierOrg}" var="store">
                                <tr>
                                    <td>
                                        <span style="width: 40px;text-align: center;">
                                            <input type="checkbox"  name="idList" id="chk_<c:out value='${store.positncode.id}' />" value="<c:out value='${store.positncode.id}' />"/>
                                        </span>
                                    </td>
                                    <td><span style="width: 70px;">${store.positncode.code}</span></td>
                                    <td><span style="width: 100px;">${store.positncode.name}</span></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div title='<fmt:message key="Scope_of_supplies"/>' id="tab4" style="padding:10px;">
            <div id="supptool" class="condition">
                <div style="float:left;">
                        <input type="button" onclick="malSelect()" value="<fmt:message key="add"/>"/>
                        <input type="button" onclick="del('material-body')" value="<fmt:message key="delete"/>"/>
                        <input type="text" id="searchmname" value="请输入物资名称" onfocus="clearsearchmname(this)"/> <input type="button" onclick="searchmaterial()" value="检索"/>
                        <input type="button" id="insertmallmaterial" name="insertmallmaterial" value="关联商城物资"/>
                </div>
            </div>
            <div class="grid" id="grid1" style="border: solid 1px #8DB2E3;">
                <div class="table-head" >
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <td>
                                <span style="width: 40px;">
                                    <input type="checkbox" id="chkAll2"/>
                                </span>
                            </td>
                            <td><span style="width: 70px;"><fmt:message key="supplies_code"/></span></td>
                            <td><span style="width: 140px;"><fmt:message key="supplies_name"/></span></td>
                            <td><span style="width: 70px;"><fmt:message key="specification"/></span></td>
                            <td><span style="width: 70px;"><fmt:message key="suppliers"/><fmt:message key="price"/></span></td>
                            <td><span style="width: 70px;"><fmt:message key="unit"/></span></td>
                            <td><span style="width: 90px;"><fmt:message key="Mall_goods_code"/></span></td>
                            <td><span style="width: 200px;"><fmt:message key="Mall_goods_name"/></span></td>
                            <td style="width:60px;"></td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body">
                    <table cellpadding="0" cellspacing="0">
                        <tbody id="material-body">
                            <c:forEach items="${supplier.materialScope}" var="materialScope">
                                <tr>
                                    <td>
                                        <span style="width: 40px;text-align: center;">
                                            <input type="checkbox"  name="idList2" id="chk_<c:out value='${materialScope.pk_material}'/>" value="<c:out value='${materialScope.pk_material}'/>"/>
                                        </span>
                                    </td>
                                    <td><span style="width: 70px;" title="${materialScope.vcode}">${materialScope.vcode}</span></td>
                                    <td><span style="width: 140px;" title="${materialScope.vname}">${materialScope.vname}</span></td>
                                    <td><span style="width: 70px;" title="${materialScope.vstandard}">${materialScope.vstandard}</span></td>
                                    <td><span style="width: 70px;" title="${materialScope.nprice}"><input type="text" maxlength="13" style="width: 63px;" value="${materialScope.nprice}"/></span></td>
                                    <td><span style="width: 70px;" title="${materialScope.pk_unit.vname}"><input type="hidden" value="${materialScope.pk_unit.pk_unit }"/>${materialScope.pk_unit.vname}</span></td>
                                    <td><span style="width: 90px;" title="${materialScope.materialMall.vjmucode}">${materialScope.materialMall.vjmucode}</span></td>
                                    <td><span style="width: 200px;" title="${materialScope.materialMall.vjmuname}">${materialScope.materialMall.vjmuname}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vhstore}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vjmustorename}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.nprice}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vjmusupplierpk}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vjmudelivercode}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vjmudelivername}</span></td>
                                    <td style="display: none;"><span style="width: 70px;display: none;">${materialScope.materialMall.vhspec}</span></td>
                                    <td style="width:60px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div title='<fmt:message key="bank_account"/>' id="tab5" style="padding:10px;">
            <div class="condition" id="banktool">
                <div class="form-line">
                    <div class="form-input" style="width: 40px;">
                        <input type="button"  onclick="bankAdd()"  value="<fmt:message key="add"/>"/>
                    </div>
                    <div class="form-input" style="width: 40px;">
                        <input type="button" onclick="del('bankBody')" value="<fmt:message key="delete"/>"/>
                    </div>
                </div>
            </div>
            <div class="grid" id="grid2">
                <div class="table-head" >
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <td>
                                <span style="width: 40px;">
                                    <input type="checkbox" id="chkAll3"/>
                                </span>
                            </td>
                            <td><span style="width: 150px;"><fmt:message key="bank_account"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="bank"/></span></td>
                            <td><span style="width: 80px;"><fmt:message key="account_holder"/></span></td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body">
                    <table cellpadding="0" cellspacing="0">
                        <tbody id="bankBody">
                            <c:forEach items="${supplier.supplierBank }" var="supplierBank">
                                <tr>
                                    <td>
                                        <span style="width: 40px;text-align: center;">
                                            <input type="checkbox"  name="idList3" id="chk_<c:out value='${supplierBank.delivercodebank}' />" value="<c:out value='${supplierBank.delivercodebank}' />"/>
                                        </span>
                                    </td>
                                    <td><span style="width: 150px;" title="${supplierBank.vbanknumber}">${supplierBank.vbanknumber}</span></td>
                                    <td><span style="width: 100px;" title="${supplierBank.vbankname}">${supplierBank.vbankname}</span></td>
                                    <td><span style="width: 80px;" title="${supplierBank.vaccountname}">${supplierBank.vaccountname}</span></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="bankselect" style="text-align: center;visibility: hidden;">
        <div class="form-line" style="margin-top: 10px;">
            <div class="form-label"><fmt:message key="bank"/>：</div>
            <div class="form-input">
                <input type="text" id="bankName" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="bank_account"/>：</div>
            <div class="form-input">
                <input type="text" id="bankNumber" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="account_holder"/>：</div>
            <div class="form-input">
                <input type="text" id="bankAcct" class="text"/>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/validate.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
    <script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
    	//关联商城物资按钮
	    $('#insertmallmaterial').click(function(){
	    	var supplierjum_name = $('#supplierjum_name').val();
	    	if(supplierjum_name != ""){
		    	var checkboxList = $('#grid1').find('.table-body').find(':checkbox');
		    	if(checkboxList  && checkboxList.filter(':checked').size() == 1){
		    		var name = '';
		    		var pk_material = '';
		    		checkboxList.filter(':checked').each(function(){
			    		var row = $(this).closest('tr');
			    		name = $.trim(row.children('td:eq(2)').text());
			    		pk_material = $.trim($(this).val());
		    		});
// 		    		if(checkMaterialmall($('#delivercodejum').val(),pk_material)){
						selectJmuMaterialV2({
							basePath:'<%=path%>',
							title:"123",
							height:400,
							width:600,
							callBack:'setJmuMaterial',
							domId:'delivercode',
							vname:name,
							delivername:supplierjum_name,
							single:true
						});
// 		    		}else{
//                         alerttips('物资已经关联当前供应商的物资，不可重复关联！');
// 		    			return;
// 		    		}
		    	}else{
                    alerttips('请选择一条数据！');
		    		return;
		    	}
	    	}else{
                alerttips('请先关联商城供应商！');
	    		return;
	    	}
		});
	    //校验是否重复关联
	    function checkMaterialmall(delivercodejum,pk_material){
			var result = true;
			$.ajaxSetup({async:false});
			$.post("<%=path %>/material/checkMaterialmall.do",{delivercodejum:delivercodejum,pk_material:pk_material},function(data){
				if(!data)result = false;
			});
			return result;
		}
	    //将商城的数据填充到表格中
	    function setJmuMaterial(data){
	    	var flag = false,materialName="";
	    	//检测一个商城物资是否重复关联本地物资
	    	$('#grid1').find('.table-body').find('tr').each(function(){
	    		if($.trim($(this).find("td").eq(6).text())==data.entity[0].id){
	    			flag = true;
	    			materialName = $.trim($(this).find("td").eq(2).text());
	    			return false;
	    		}
	    	});
	    	if(flag){
	    		alerterror('物资-'+materialName+'已经关联商城物资-'+data.entity[0].goodsname+',不能重复关联。');
	    		return;
	    	}
	    	var checkboxList = $('#grid1').find('.table-body').find(':checkbox');
	    	checkboxList.filter(':checked').each(function(){
	    		var row = $(this).closest('tr');
	    		row.children('td:eq(6)').find("span").text(data.entity[0].id);//商城物资编码
	    		row.children('td:eq(7)').find("span").text(data.entity[0].goodsname);//商城物资名字
	    		row.children('td:eq(8)').text(data.entity[0].storeid).css("display","none");//商城店铺编码
	    		row.children('td:eq(9)').text(data.entity[0].storename).css("display","none");//商城店铺名称
	    		row.children('td:eq(10)').text(data.entity[0].price).css("display","none");//报价
	    		row.children('td:eq(11)').text(data.entity[0].sellerid).css("display","none");//商城供应商主键
	    		row.children('td:eq(12)').text(data.entity[0].sellerid).css("display","none");//商城供应商编码
	    		row.children('td:eq(13)').text(data.entity[0].sellername).css("display","none");//商城供应商名称
	    		row.children('td:eq(14)').text(data.entity[0].id).css("display","none");//商城物资规格主键
    		});
	    }
	    
        var editor;
        var single='${single}';
        var bankvalidate;
        var supplierval;
		var deleteFlagVal = $("#enablestate").val();
        $(document).ready(function(){
			$("#enablestate").change(function(){
				if($("#delivercode").val()!=""){
					if($("#enablestate").val()!="2"){
						var result = true;
						$.ajaxSetup({async:false});
						$.post("<%=path%>/supplier/checkSupplierQuote.do",{delivercode:$("#delivercode").val()},function(data){
							if(data=="false")
								result = false;
						});
						if(result){ //如果有被引用
							alerterror('<fmt:message key="refencenotdisable"/>');
							$("#enablestate").val(deleteFlagVal);
						}
					}
				}
			});
            document.onkeydown=function(){
                if(event.keyCode==27){//ESC 后关闭窗口
                    debugger;
                    if($('.close').length>0){
                        $('.close').click();
                    }else {
                        $(".close", parent.document).click();
                    }
                }
                if(event.ctrlKey&& event.keyCode==13){
                    insertSupplier();
                }
            };
            var inputarray = $(".form-input").children().not("input[type='hidden']").not("div");
            inputarray.bind('keyup',function(){
                if(!event.ctrlKey&&event.keyCode==13){
                    var index = inputarray.index($(this)[0]);
                    if(index!=inputarray.length-1){
                        $(inputarray[index+1]).focus();
                    }
                }
            });
            $("input[name='iaccttype']").click(function(i,name){
                if($(this).val()=='0'){
                    $("#iacctdate").attr("disabled","disabled").find("option[value='1']").attr("selected","selected");
                }else{
                    $("#iacctdate").removeAttr("disabled");
                }
            });
            /*获取编码规则*/
            getSupplierCode();
            //账期默认选中
            $("input[value='<c:out value='${supplier.iaccttype}'/>']").attr("checked","checked");
            $("#iacctdate").find("option[value='<c:out value='${supplier.iacctdate}'/>']").attr("selected","selected");
            var setWidth=function(id){
                var $grid=$(id);
                var headWidth=$grid.find(".table-head").find("tr").width();
                var gridWidth=$grid.width();
                if(headWidth>=gridWidth){
                    $grid.find(".table-body").width(headWidth+30);
                    $grid.find(".table-head").width(headWidth+30);
                }else{
                    $grid.find(".table-body").width(gridWidth+30);
                    $grid.find(".table-head").width(gridWidth+30);
                }
            };
            $('.easyui-tabs').tabs({
                width:$(document.body).width()-2,
                height:$(document.body).height()*0.99,
                onSelect:function(){
                    var id=$(this).tabs('getSelected')[0].id;
                    setElementHeight('#'+id+' .grid',['#'+id+' .condition'],$(document.body),80);	//计算.grid的高度
                    setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
                    loadGrid('#'+id+' .grid'); //自动计算滚动条的js方法
                    setWidth('#'+id+' .grid');
                }
            });
            $('.grid').find('.table-body').find('tr').hover(
                    function(){
                        $(this).addClass('tr-over');
                    },
                    function(){
                        $(this).removeClass('tr-over');
                    }
            );

            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#grid0').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid0').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //如果全选按钮选中的话，table背景变色
            $("#chkAll2").click(function() {
                if (!!$("#chkAll2").attr("checked")) {
                    $('#grid1').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid1').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });//如果全选按钮选中的话，table背景变色
            $("#chkAll3").click(function() {
                if (!!$("#chkAll3").attr("checked")) {
                    $('#grid2').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid2').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                if ($(this).hasClass("bgBlue")) {
                    $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
                }
                else
                {
                    $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
                }
            });
            var codes=[];
            $("#grid0").find('.table-body').find('tr').each(function(){
                codes.push($(this).find(":checkbox").val());
            });
            $("#positncode").val(codes.join(","));
            //================================================================

            /*银行信息验证*/
            bankvalidate = new Validate({
                validateItem:[{
                    type:'text',
                    validateObj:'bankName',
                    validateType:['canNull','maxLength'],
                    param:['F','20'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>20!']
                },{
                    type:'text',
                    validateObj:'bankNumber',
                    validateType:['canNull','maxLength'],
                    param:['F','50'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>50!']
                },{
                    type:'text',
                    validateObj:'bankAcct',
                    validateType:['canNull','maxLength'],
                    param:['F','10'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>10!']
                }]
            });
            /*供应商基本验证*/
            supplierval = new Validate({
                validateItem:[{
                    type:'text',
                    validateObj:'vcode',
                    validateType:['canNull','maxLength','zimuint'],
                    param:['F','50','F'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>50<fmt:message key="character"/>!','<fmt:message key="onlynumletter"/>!']
                },{
                    type:'text',
                    validateObj:'vname',
                    validateType:['canNull','maxLength','withOutSpecialChar'],
                    param:['F','50','F'],
                    error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="maximum_length"/>50<fmt:message key="character"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
                },{
                    type:'text',
                    validateObj:'nsignmonty',//注册资金
                    validateType:['num2','handler'],
                    handler:function(){
                      var money=$("#nsignmonty").val();
                        if(999999999>=Number(money)){
                            return true;
                        }
                        return false;
                    },
                    param:['T','T'],
                    error:['<fmt:message key="please_enter_a_number_greater"/>','<fmt:message key="can_not_be_greater_than"/>999999999!']
                },{
                    type:'text',
                    validateObj:'vinit',//简写
                    validateType:['maxLength','withOutSpecialChar'],
                    param:['40'],
                    error:['<fmt:message key="maximum_length"/>40<fmt:message key="character"/>!<fmt:message key="The_letter"/>','<fmt:message key="cannot_contain_special_characters"/>!']
                },{
                    type:'text',
                    validateObj:'vcontact',//联系人
                    validateType:['maxLength','withOutSpecialChar'],
                    param:['19'],
                    error:['<fmt:message key="maximum_length"/>19<fmt:message key="character"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
                },{
                    type:'text',
                    validateObj:'vlegal',//负责人
                    validateType:['maxLength','withOutSpecialChar'],
                    param:['19'],
                    error:['<fmt:message key="maximum_length"/>19<fmt:message key="character"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
                },{
                    type:'text',
                    validateObj:'vmailaddr',//邮箱
                    validateType:['maxLength','email'],
                    param:['30','T'],
                    error:['<fmt:message key="maximum_length"/>30<fmt:message key="character"/>!','<fmt:message key="e-mail_can_not_be_empty_mailbox_format_javapro"/>!']
                },{
                    type:'text',
                    validateObj:'vaddr',//地址
                    validateType:['maxLength'],
                    param:['200'],
                    error:['<fmt:message key="maximum_length"/>200<fmt:message key="character"/>!']
                },{
                    type:'text',
                    validateObj:'vwebsite',//网址
                    validateType:['maxLength'],
                    param:['30'],
                    error:['<fmt:message key="maximum_length"/>30<fmt:message key="character"/>!']
                },{
                    type:'text',
                    validateObj:'vfax',
                    validateType:['maxLength','telepOrphone'],//传真
                    param:['30','T'],
                    error:['<fmt:message key="maximum_length"/>30<fmt:message key="character"/>!','<fmt:message key="incorrect_format"/>!']
                },{
                    type:'text',
                    validateObj:'vtele',
                    validateType:['maxLength','telepOrphone'],//电话
                    param:['14','T'],
                    error:['<fmt:message key="maximum_length"/>14<fmt:message key="character"/>!','<fmt:message key="phone_calls_can_not_be_empty_format_javapro"/>']
                },{
                    type:'text',
                    validateObj:'vmemo',
                    validateType:['maxLength'],//
                    param:['200'],
                    error:['<fmt:message key="maximum_length"/>200<fmt:message key="character"/>!']
                },{
                    type:'text',
                    validateObj:'vzipcode',
                    validateType:['maxLength','zipCode'],//
                    param:['6','T'],
                    error:['<fmt:message key="maximum_length"/>6<fmt:message key="digital"/>!','<fmt:message key="zip_zip_format_can_not_be_empty_javapro"/>!']
                }]
            });
            //========end===================================
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
            /*定位光标*/
            if($("#operate").val()=="1") {//1 修改
                $("#vname").focus();
            }else{
                $("#vcode").focus();
            }
        });
        //===============================适用门店==================================
        var showOrg=function(){
            chooseDepartMentList({
                basePath:'<%=path%>',
                title:'选择适用门店',
                height:370,
                width:600,
                single:false,
                domId:'positncode',
                type:'1'
            });
        };
        function setDepartmentList(data){
            $("#positncode").val(data.code.join(","));
            $("#store-body").find("tr").remove();
            for(var i=0;data.entity.length>i;i++){
                if($.trim(data.entity[i].pk_id)==""){
                    return;
                }
                $("#store-body").append('<tr>' +
                        '<td>' +
                        '<span style="width: 40px;text-align: center;">' +
                        '<input type="checkbox"  name="idList" id="chk_'+data.entity[i].pk_id+'" value="'+data.entity[i].pk_id+'"/>' +
                        '</span>' +
                        '</td>' +
                        '<td><span style="width: 70px;">'+data.entity[i].vcode+'</span></td>' +
                        '<td><span style="width: 100px;">'+data.entity[i].vname+'</span></td>'+
                        '<td style="width:10px;border:0;cursor: pointer;" onclick="reTr(this)"><img src="../image/scm/move.gif"/></td>'+
                    '</tr>');
            }
        }
        //==========================物资选择=======================================
        var malSelect=function(){
        	top.selectMaterialByPage({
                basePath:'<%=path%>',
                width:1000,
                height:600,
		positn : {
			type : 'absolute',
			top: $(topWin).height()*0.5-300,
			left : $(topWin).width()*0.5-500
		},
                callBack:'setMaterial',
                domId:'pk_material',
                irateflag:1,//取采购单位
                single:false,
                frameId:'iframe_'+"${moduleId}",
                childFrameId:'skipSupplierFrame',
                container:$(parent.parent.document.body)
            });
        };
        function setMaterial(data){
            var pk=[];
            var pks = data.toString();
            $("#material-body").find("tr").each(function(){
                var value=$.trim($(this).find("td:eq(0)").find("input").val());
                if(pks.indexOf(value)>=0){
                	pks=pks.replace(new RegExp(value,"gm"),"");
                }
            });
            //将空值去掉
            var lis = pks.split(",");
            for(var i in lis){
            	if(lis[i]!=""){
            		pk.push(lis[i]);
            	}
            }
            if(pk.length>0){
	            //查询选择的物资
	            $.ajaxSetup({
					  async: false 
				});
				$.post("<%=path%>/material/queryMaterialByPk.do?irateflag=1&pk_material="+pk.toString(),data,function(data){
					var rs = data;
					for(var i in data){
		                $("#material-body").append('<tr>'+
		                        '<td>'+
		                        '<span style="width: 40px;text-align: center;">'+
		                        '<input type="checkbox"  name="idList2" id="chk_'+data[i].pk_material+'" value="'+data[i].pk_material+'"/>'+
		                        '</span>'+
		                        '</td>'+
		                        '<td><span style="width: 70px;" id="materialScopeVcode">'+data[i].vcode+'</span></td>'+
		                        '<td><span style="width: 140px;" id="materialScopeVname">'+data[i].vname+'</span></td>'+
		                        '<td><span style="width: 70px;" id="materialScopeVstandard">'+data[i].vspecfication+'</span></td>'+
		                        '<td><span style="width: 70px;" id="materialScopeNprice"><input maxlength="13" type="text" style="width: 63px" value="'+data[i].nsaleprice+'"/></span></td>'+
		                        '<td><span style="width: 70px;" id="materialScopePk_unit"><input type="hidden" value="'+data[i].pk_unit+'"/>'+data[i].unitvname+'</span></td>'+
		                        '<td><span style="width: 90px;"></span></td>'+
		                        '<td><span style="width: 200px;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display:none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
		                        '<td style="width:60px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></td>'+
		                        '</tr>');
		                pk.push(data[i].pk_material);
					}
				});	
	            $("#pk_material").val(pk.join(","));
            }
        }
        var reTr=function(v,m){
            $(v).parent("tr").remove();
            if(m==1){
                var pk=[];
                $("#material-body").find("tr").each(function(){
                    pk.push($(this).find("td:eq(0)").find("input").val());
                });
                $("#pk_material").val(pk.join(","));
            }
        };
        //===============================银行卡号==================================
        var bankSave=function(){
            var num= $.trim(stripscript($("#bankAdd").find("#bankNumber").val()));
            var name= $.trim(stripscript($("#bankAdd").find("#bankName").val()));
            var acct= $.trim(stripscript($("#bankAdd").find("#bankAcct").val()));
            var ok=true;
            if(num==''){
                errorbox($("#bankAdd").find("#bankNumber"),'<fmt:message key="cannot_be_empty"/>');
                ok=false;
            }else if(num.length>50){
            	alerterror('银行账号不能超过50个数字！');
            	ok=false;
            }else if(!/^\d+$/.test(num)){
            	alerterror('银行账号只能为数字！');
            	ok=false;
            }
            if(name==''){
                errorbox($("#bankAdd").find("#bankName"),'<fmt:message key="cannot_be_empty"/>');
                ok=false;
            }else if(name.length>100){
            	alerterror('银行名称不能超过100个汉字！');
            	ok=false;
            }
            if(acct==''){
                errorbox($("#bankAdd").find("#bankAcct"),'<fmt:message key="cannot_be_empty"/>');
                ok=false;
            }else if(acct.length>25){
            	alerterror('开户人姓名不能超过25个汉字');
            	ok=false;
            }
            var re = /[`~!@#\$%\^\&\*\(\)_\+<>\?:"\{\},\.\\\/;'\[\]]/im;
            if(re.test(name)){
                errorbox($("#bankAdd").find("#bankName"),'<fmt:message key="cannot_contain_special_characters"/>');
                ok=false;
            }
            if(re.test(acct)){
                errorbox($("#bankAdd").find("#bankAcct"),'<fmt:message key="cannot_contain_special_characters"/>');
                ok=false;
            }
            if(!ok) {
                return;
            }
                $("#bankBody").append('<tr>'+
                        '<td>'+
                        '    <span style="width: 40px;text-align: center;">'+
                        '   <input type="checkbox"  name="idList3" id="chk_'+stripscript($("#bankAdd").find("#bankNumber").val())+'" value="'+$("#bankAdd").find("#bankNumber").val()+'"/>'+
                        '    </span>'+
                        '</td>'+
                        '<td><span style="width: 150px;">'+stripscript($("#bankAdd").find("#bankNumber").val())+'</span></td>'+
                        '<td><span style="width: 100px;">'+stripscript($("#bankAdd").find("#bankName").val())+'</span></td>'+
                        '<td><span style="width: 80px;">'+stripscript($("#bankAdd").find("#bankAcct").val())+'</span></td>'+
                        '<td style="width:10px;border:0;cursor: pointer;" onclick="reTr(this)"><img src="../image/scm/move.gif"/></td>'+
                        '</tr>');
            $('.close').click();
        };
        var del=function(id){
            if($("#"+id).find("input[checked='checked']").size()<=0){
                alerttips("<fmt:message key="please_select_at_least_one_data"/>");
                return;
            }
            $("#"+id).find("input[checked='checked']").each(function(){
               $(this).parents("tr").remove();
            });
        };
        //===============================新增操作==================================
        var isRight;
        var dh=function() {
            isRight=true;
            var data = {};
            data.delivercode=$("#delivercode").val();
            data.vcode = $.trim($("#vcode").val());
            data.vname = $.trim($("#vname").val());
            data["delivercodetype.delivercodetype"] = $("#delivercodetype").val();
            data.vcontact = $("#vcontact").val();
            data.vtele = $.trim($("#vtele").val());
            data.enablestate = $("#enablestate").val();
            data.delivercodejum = $("#delivercodejum").val();
            data.vcodejmu = $("#vcodejmu").val();
            data.vnamejmu = $("#vnamejmu").val();
            data.vinit = $("#vinit").val();
            data.iaccttype = $("input[name='iaccttype']:checked").val();
            data.iacctdate = $("#iacctdate").val();
            data.vlegal = $("#vlegal").val();
            data.vaddr = $.trim($("#vaddr").val());
            data.vmailaddr = $.trim($("#vmailaddr").val());
            data.vwebsite = $("#vwebsite").val();
            data.vfax = $("#vfax").val();
            data.vzipcode = $("#vzipcode").val();
            data.nsignmonty = $("#nsignmonty").val();
            data.bistax = !!$("#bistax").attr("checked") ? 0 : 1;
            data.vmemo = $.trim($("#vmemo").val());
            data.delivercodejum = $.trim($("#delivercodejum").val());
            data.vcodejmu = $.trim($("#delivercodejum").val());
            data.vnamejmu = $.trim($("#supplierjum_name").val());
            $("#bankBody").find("tr").each(function (i) {
                data["supplierBank[" + i + "].vaccountname"] = $.trim($(this).find("td:eq(3)").find("span").text());
                data["supplierBank[" + i + "].vbankname"] = $.trim($(this).find("td:eq(2)").find("span").text());
                data["supplierBank[" + i + "].vbanknumber"] = $.trim($(this).find("td:eq(1)").find("span").text());
            });
            $("#material-body").find("tr").each(function(i){
               data["materialScope["+i+"].vcode"]=$(this).find("td:eq(1)").find("span").text();
               data["materialScope["+i+"].vname"]=$.trim($(this).find("td:eq(2)").find("span").text());
               data["materialScope["+i+"].pk_unit.pk_unit"]=$(this).find("td:eq(5)").find("input").val();
               data["materialScope["+i+"].vstandard"]=$(this).find("td:eq(3)").find("span").text();
               data["materialScope["+i+"].pk_material"]=$(this).find("td:eq(0)").find("input").val();
                var nprice=$.trim($(this).find("td:eq(4)").find("input").val());
                if(nprice!=""&&Number(nprice)>=0){
                    if(nprice.indexOf(".")==-1||nprice.indexOf(".")>10){
                        if(nprice.length>10) {
                            alerterror('<fmt:message key="maximum_length"/>10!');
                            isRight=false;
                        }
                    }
                }else{
                    alerterror('<fmt:message key="price_must_be_greater_than"/>');
                    isRight=false;
                }
               data["materialScope["+i+"].nprice"]= nprice;
            });
            //**********************关联商城物资********************************************************************
            $("#material-body").find("tr").each(function(i){
            	if($.trim($(this).find("td:eq(14)").text()) != "" && $.trim($(this).find("td:eq(14)").text()) != null){
	                data["listMaterialMall["+i+"].pk_material"]=$(this).find("td:eq(0)").find("input").val();//物资主键
	                data["listMaterialMall["+i+"].vhspec"]=$.trim($(this).find("td:eq(14)").text());//规格主键
	                data["listMaterialMall["+i+"].vjmuname"]=$(this).find("td:eq(7)").text();//商城物资名称
	                data["listMaterialMall["+i+"].vjmucode"]=$(this).find("td:eq(6)").text();//商城物资编码
	                data["listMaterialMall["+i+"].vhstore"]=$(this).find("td:eq(8)").text();//店铺主键
	                data["listMaterialMall["+i+"].nprice"]= Number($.trim($(this).find("td:eq(10)").text()));//报价
	                data["listMaterialMall["+i+"].vjmustorename"]= $.trim($(this).find("td:eq(9)").text());//店铺名称
	                data["listMaterialMall["+i+"].vjmudelivername"]= $.trim($(this).find("td:eq(13)").text());//商城供应商名称
	                data["listMaterialMall["+i+"].vjmusupplierpk"]= $.trim($(this).find("td:eq(11)").text());//商城供应商主键
	                data["listMaterialMall["+i+"].vjmudelivercode"]= $.trim($(this).find("td:eq(12)").text());//商城供应商编码
            	}
             });
            //**********************关联商城物资********************************************************************
            $("#store-body").find("tr").each(function(i){
                data["supplierOrg["+i+"].positncode.id"]=$(this).find("td:eq(0)").find("input").val();
            });
            //供应物资范围
            //data.materialScope;
            //data.supplierOrgs;
            return data;
        };
		var savelock = false;
        var insertSupplier=function(){
			if(savelock){
				return;
			}else{
				savelock = true;
			}
            codeval({//编码规则验证
                    action:"<%=path%>",
                    callback:"saveSupplier"
                },
            "00000000000000000000000000000009",
            $("#vcode").val(),
            $("#delivercodetype").find(":selected").attr("data"),0);
// 			setTimeout(function(){savelock=false;},5000);
        };
        var mark=true;        
        var saveSupplier=function(msg){
        	var resFlag = true;
            if('ok'==msg) {
                if (supplierval._submitValidate()) {
                    var data=dh();
                    if(mark&&isRight) {
                    	$.ajaxSetup({async:false});
                        $.post('<%=path%>/supplier/addSupplier.do',data, function (msg) {
                            if (msg == 'ok') {
                            	alerttipsbreak('<fmt:message key="successful_added"/>！',function(){
                					savelock = false;
                                	parent.$("#listForm").submit();
                            	});
                            } else {
                                alerterror(msg);
                                mark=true;
            					savelock = false;
                            }
                        });
                        mark=false;
//                         setTimeout(function(){mark=true;},5000);
                    }
                }else{
//                 	$(".tabs").find('li:eq(0)').trigger('click');//显示第一个页签
					savelock = false;
                }
            }else{
                valerrorbox("vcode",'<fmt:message key="code_error"/>');
                savelock=false;
            }
			savelock = false;
            mark=true;
        };
        //===============================供应商审核界面新增==================================
        var insertAuditSupplier=function(){
			if(savelock){
				return;
			}else{
				savelock = true;
			}
            codeval({//编码规则验证
                        action:"<%=path%>",
                        callback:"saveAuditSupplier"
                    },
                    "00000000000000000000000000000009",
                    $("#vcode").val(),
                    $("#delivercodetype").find(":selected").attr("data"),0);
        };
        var auditMark=true;
        var saveAuditSupplier=function(msg){
            if('ok'==msg) {
                if (supplierval._submitValidate()) {
                    var data=dh();
                    if(mark&&isRight) {
                        $.post('<%=path%>/audit/addSupplier.do', data, function (msg) {
                            if (msg == 'ok') {
                            	alerttipsbreak('<fmt:message key="successful_added"/>！',function(){
                					savelock = false;
                                	parent.location.href = parent.location.href;
                            	});
                            } else {
            					savelock = false;
                                alerterror(msg);
                            }
                        });
                        auditMark=false;
                        setTimeout(function(){auditMark=true;},5000);
                    }else{
    					savelock = false;
    				}
                }else{
					savelock = false;
                }
            }else{
				savelock = false;
                valerrorbox("vcode",'<fmt:message key="code_error"/>');
            }
        };
        //===============================修改操作==================================
        var update=function(msg){
			if(savelock){
				return;
			}else{
				savelock = true;
			}
            if('ok'==msg) {
                if (supplierval._submitValidate()) {
                    var data=dh();
                    if(isRight) {
                        $.post('<%=path%>/supplier/update.do', data, function (msg) {
                            if (msg == 'ok') {
                                alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
                					savelock = false;
                                	parent.$("#listForm").submit();
                                });
                            } else {
            					savelock = false;
                                alerterror(msg);
                            }
                        });
                    }else{
    					savelock = false;
    				}
                }else{
//                 	$(".tabs").find('li:eq(0)').trigger('click');//显示第一个页签
		savelock = false;
		}
            }else{
				savelock = false;
                valerrorbox("vcode",'<fmt:message key="code_error"/>');
            }
        };
        var updateSupplier=function(){
            /*var code=$.trim($("#vcode").val());
            if(code!= $.trim($("#vcode").attr("data"))) {
                codeval({//编码规则验证
                            action:,
                            callback: "update"
                        },
                        "00000000000000000000000000000009",
                        $("#vcode").val(),
                        $("#delivercodetype").find(":selected").attr("data"), 0)
            }else{*/
                update("ok");
           /* }*/
        };
        var reload=function(){
            if(typeof(eval(parent.pageReload))=='function') {
                parent.pageReload();
            }else {
                parent.location.href = parent.location.href;
            }
            $(".close",parent.document).click();
        };

        //=================================================================
        var showJmu=function(){
            if($.trim($("#delivercodejum").val())!=""){
                alertconfirm('<fmt:message key="update_jmu_supplier_alert"/>',function(){
                    chooceJmu();
                });
            }else{
                chooceJmu();
            }
        };
        var chooceJmu=function(){
            selectjmuSupplier({
                basePath:'<%=path%>',
                title:"123",
                height:400,
                width:600,
                callBack:'setSupplier',
                domId:'delivercode',
                supplierName:$("#vname").val(),
                single:true
            });
        };
        var setSupplier=function(data){
        	//删除供应物资范围
            if($.trim($("#delivercodejum").val())!=""){
                $("#material-body").find("tr").each(function(){
                	$(this).find("td").eq(6).find("span").empty();
                	$(this).find("td").eq(7).find("span").empty();
                	$(this).find("td").eq(8).html("");
                	$(this).find("td").eq(9).html("");
                	$(this).find("td").eq(10).html("");
                	$(this).find("td").eq(11).html("");
                	$(this).find("td").eq(12).html("");
                	$(this).find("td").eq(13).html("");
                	$(this).find("td").eq(14).html("");
                });
            }
            if($("#operate").val()==0) {
                if($.trim($("#vname").val())=="") {
                    $("#vname").val(data.entity[0].name);
                }
//                 if($.trim($("#vaddr").val())=="") {
//                     $("#vaddr").val(data.entity[0].companyAddress);
//                 }
//                 if($.trim($("#vtele").val())=="") {
//                     $("#vtele").val(data.entity[0].companyPhone);
//                 }
            }
            $("#delivercodejum").val(data.entity[0].code);
            $("#supplierjum_name").val(data.entity[0].name);
            $("#vaddr").val(data.entity[0].companyAddress);
            $("#vtele").val(data.entity[0].companyPhone);
            $("#vcontact").val(data.entity[0].companyContact);
            $("#vmailaddr").val(data.entity[0].companyFax);
            $("#vwebsite").val(data.entity[0].companyUrl);
            $("#vfax").val(data.entity[0].companyEmail);
        };
        //=================================================================
        var bankAdd=function(){
            $('body').window({
                title: '<fmt:message key="bank"/><fmt:message key="insert"/>',
                content: $("#bankselect").html(),
                id:"bankAdd",
                width: '320px',
                height: '200px',
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                bankSave();
                            }
                        },{
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
            $("#bankAdd").find("#bankName").focus();
            var inputarray = $("#bankAdd").find("input").not("input[type='hidden']").not("div");
            inputarray.bind('keyup',function(){
                if(!event.ctrlKey&&event.keyCode==13){
                    var index = inputarray.index($(this)[0]);
                    if(index!=inputarray.length-1){
                        $(inputarray[index+1]).focus();
                    }else{
                        bankSave();
                    }
                }
            });
        };
        /*自动生成编码*/
        var getSupplierCode=function(){
            if($.trim($("#vcode").val())==""||$("#operate").val()==0) {
                getCode({//编码规则验证
                            action: "<%=path%>",
                            callback: "supplierCode"
                        },
                        "00000000000000000000000000000009",
                        $("#delivercodetype").find(":selected").attr("data"),
                        null,
                        0);
            }
        };
        var supplierCode=function(msg){
            var obj= eval('(' + msg + ')');
//             var obj=JSON.parse(msg);
            if(obj.msg=="ok"){
                $("#vcode").val(obj.code);
            }else{
                alerterror(obj.msg);
            }
        };
        
        function clearsearchmname(e){
        	$(e).val('');
        }
        //检索物资
        function searchmaterial(){
        	var vmname=$("#searchmname").val();
        	var scrollheight=0;
        	var index = 0;
        	if(vmname==''){
        		return;
        	}
        	$("#material-body").find("tr").each(function(){
        		$(this).find("td:eq(2)").find("span").css('color','black');
                index++;	
        	      var materialname=$.trim($(this).find("td:eq(2)").find("span").text());
                if(materialname.indexOf(vmname)!=-1){
                	if(scrollheight==0){
                		scrollheight=index*32;
                	}
                	$(this).find("td:eq(2)").find("span").css('color','red');
                }
            });
        	$("#grid1").find('.table-body').animate({scrollTop:scrollheight-150},1000);//
        }
        function deleteRow(obj){
        	$(obj).closest("tr").remove();
        };
    </script>
</body>
</html>
