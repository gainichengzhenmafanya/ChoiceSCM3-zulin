<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
/* 			.page{ */
/* 					margin-bottom:26px; */
/* 				} */
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div class="tool">
		</div>
		<div >
			<form id="queryForm" action="<%=path%>/supplier/supplierJmuList.do" method="post">
				<input type="hidden" id="parentId" name="parentId" value=""/>
				<input type="hidden" id="parentName" name="parentName" value=""/>
				<input type="hidden" id="callBack" name="callBack" value="${callBack}"/>
		    	<input type="hidden" id="single" name="single" value="${single}"/>
		    	<input type="hidden" id="domId" name="domId" value="${domId}"/>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left">名称:</td>
						<td><input type="text" id="vname" name="des" class="text" value="${supplier.vname}" onkeydown="javascript: if(event.keyCode==13){$('#search').click();} "/></td>
				        <td width="200">&nbsp;
				        	<input type="button" style="width:60px" id="search" name="search" value='查询'/>
				        </td>
				    </tr>
				</table>
				<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 30px;"></span></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll" /></span>
									</td>
									<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:150px;"><fmt:message key="name" /></span></td>
									<td><span style="width:70px;">等级</span></td>
									<td><span style="width:110px;"><fmt:message key="phone" /></span></td>
									<td><span style="width:150px;">经营范围</span></td>
									<td><span style="width:150px;">地区</span></td>
									<td><span style="width:150px;">配送范围</span></td>
									<td><span style="width:200px;"><fmt:message key="address" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid">
							<tbody>
								<c:forEach var="seller" varStatus="status" items="${sellerList}">
									<tr>
										<td class="num"><span style="width: 27px;">${status.index+1}</span>
										</td>
										<td><span style="width:30px; text-align: center;">
										<input type="checkbox" name="idList" id="<c:out value='${seller.id}' />"
											value="<c:out value='${seller.id}' />" />
										</span></td>
										<td><span style="width:67px;">${seller.id}</span></td>
										<td><span style="width:147px;" onclick="openGysInfo('${seller.id}')">${seller.company.companyName}</span></td>
										<td>
											<span style="width:67px;"> 
												<c:if test="${seller.grade==1}">子供应链</c:if> 
												<c:if test="${seller.grade==2}">品牌供应商</c:if>
												<c:if test="${seller.grade==3}">普通供应商</c:if> 
											</span>
										</td>
										<td><span style="width:108px;">${seller.company.companyPhone}</span></td>
										<td><span style="width:148px;"></span></td>
										<td><span style="width:148px;">${seller.company.companyArea}</span></td>
										<td><span style="width:148px;"></span></td>
										<td><span style="width:200px;">${seller.company.companyAddress}</span></td>
										<td style="display: none;">${seller.company.companyContact}</td>
										<td style="display: none;">${seller.company.companyFax}</td>
										<td style="display: none;">${seller.company.companyUrl}</td>
										<td style="display: none;">${seller.company.companyEmail}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#vname").focus();
				setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				$('#search').bind("click",function search(){
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
				 	$('#queryForm').submit();
				});
				//重写翻页方法
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key ="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {entity:[]};
								checkboxList.filter(':checked').each(function(){
									var entity = {};
									var row = $(this).closest('tr');
									entity.code = $.trim(row.children('td:eq(2)').text());//主键
									entity.name = $.trim(row.children('td:eq(3)').text());//名称
									entity.companyPhone = $.trim(row.children('td:eq(5)').text());//联系电话
									entity.companyArea = $.trim(row.children('td:eq(7)').text());//地区、区域
									entity.companyAddress = $.trim(row.children('td:eq(9)').text());//地址
									entity.companyContact = $.trim(row.children('td:eq(10)').text());//联系人
									entity.companyFax = $.trim(row.children('td:eq(11)').text());//传真
									entity.companyUrl = $.trim(row.children('td:eq(12)').text());//网址
									entity.companyEmail = $.trim(row.children('td:eq(13)').text());//邮箱
									data.entity.push(entity);
								});
								if(data.entity.length==0){
									var entity = {};
									entity.code = "";//主键
									entity.name ="";//名称
									entity.companyPhone ="";//联系电话
									entity.companyArea = "";//地区、区域
									entity.companyAddress = "";//地址
									entity.companyContact = "";//联系人
									entity.companyFax = "";//传真
									entity.companyUrl = "";//网址
									entity.companyEmail = "";//邮箱
									data.entity.push(entity);
								}
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key ="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
				//------------------------------
				var mod = ${single};
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$("#chkAll").click(function(){
						if($(this)[0].checked){
							$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
						}else{
							$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
						}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = ${single};
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
					}
					event.stopPropagation();
				});
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			function pageReload(){
				$('#queryForm').submit();
			} 
		</script>
	</body>
</html>