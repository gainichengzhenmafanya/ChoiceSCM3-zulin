<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<!DOCTYPE HTML>
<%
	String path = request.getContextPath();
%>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<head>
<title>HTML5的标题</title>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
</head>
<body>
	<div style="margin-top:50px;margin-left:60px;">
<!-- 		<button type="button" id="button" onclick="eject()">Click Me!</button> -->
		<button type="button" id="button">Click Me!</button>
		<input type="text" id="in"/>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript">
		$('#button').click(function(){
			selectSupplier({
				basePath:'<%=path%>',
				title:"123",
				height:600,
				width:800,
				callBack:'selectSupplier',
				single:false
			});
		});
		function setMaterial(data,name){
			$("#in").val(data);
		}
	</script>
</body>
</html>