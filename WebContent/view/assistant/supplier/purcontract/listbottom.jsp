<%--
  User: mc
  Date: 14-10-24
  Time: 下午12:15
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div class="easyui-tabs" fit="false" plain="true" style="height:100%;z-index:88;">
    <input type="hidden" id="pk_purcontract" value="${pur.pk_purcontract}"/>
    <div title='<fmt:message key="The_basic_contract"/>' id="y_1" style="overflow-y:hidden; ">
        <iframe src="<%=path%>/purcontract/getContractQuote.do?pk_purcontract=${pur.pk_purcontract}" id="tabForm">
        </iframe>
    </div>
    <div title='合约条款' id="y_2" style="overflow: auto;">
        <div class="grid" id="grid2">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td style="width:30px; text-align: center;">
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll2"/>
                            </span>
                        </td>
                        <td><span style="width: 90px;">条款名称</span></td>
                        <td><span style="width: 150px;">条款内容</span></td>
                        <td><span style="width: 300px;">其他信息</span></td>
                        <td><span style="width: 120px;">操作时间</span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body" id="table2">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="grid2-body" >
                    <c:forEach items="${pur.contractclauList}" var="clau" varStatus="status">
                        <tr>
                            <td style="width:30px; text-align: center;">
                                    <span style="width: 30px;">
                                        <input type="checkbox"  name="idList2" id="chk_<c:out value='${clau.pk_contractclau}' />" value="<c:out value='${tta.pk_contractclau}' />"/>
                                    </span>
                            </td>
                            <td><span style="width: 90px;" title="${clau.vname}"><c:out value='${clau.vname}' /></span></td>
                            <td><span style="width: 150px;" title="${clau.vcontent}"><c:out value='${clau.vcontent}' /></span></td>
                            <td><span style="width: 300px;" title="${clau.vmemo}"><c:out value='${clau.vmemo}' /></span></td>
                            <td><span style="width: 120px;" title="${clau.ts}"><c:out value='${clau.ts}' /></span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='合约大事记' id="y_3" style="overflow: auto;">
        <div class="grid" id="grid3">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td style="width:30px; text-align: center;">
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll4"/>
                            </span>
                        </td>
                        <td><span style="width: 90px;">大事记编码</span></td>
                        <td><span style="width: 150px;">大事记名称</span></td>
                        <td><span style="width: 300px;">备注</span></td>
                        <td><span style="width: 120px;">操作时间</span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body" id="table3">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="grid3-body">
                    <c:forEach items="${pur.contractevenList}" var="even" varStatus="status">
                        <tr>
                            <td style="width:30px; text-align: center;">
                                        <span style="width: 30px;">
                                            <input type="checkbox"  name="idList4" id="chk_<c:out value='${even.pk_contracteven}' />" value="<c:out value='${even.pk_contracteven}' />"/>
                                        </span>
                            </td>
                            <td><span style="width: 90px;" title="${even.vcode}"><c:out value='${even.vcode}' /></span></td>
                            <td><span style="width: 150px;" title="${even.vcontent}"><c:out value='${even.vcontent}' /></span></td>
                            <td><span style="width: 300px;" title="${even.vmemo}"><c:out value='${even.vmemo}' /></span></td>
                            <td><span style="width: 120px;" title="${even.ts}"><c:out value='${even.ts}' /></span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='变更历史' id="y_4" style="overflow: auto;">
        <div class="grid">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 60px;"><fmt:message key="the_version_number"/></span></td>
                            <td><span style="width: 80px;">变更人</span></td>
                            <td><span style="width: 120px;">变更日期</span></td>
                            <td><span style="width: 400px;">变更信息</span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${pur.changeLogList}" var="log" varStatus="status">
                            <tr>
                                <td><span style="width: 60px;" title="<c:out value='${log.version}' />"><c:out value='${log.version}' /></span></td>
                                <td><span style="width: 80px;" title="<c:out value='${log.vaccountname}' />"><c:out value='${log.vaccountname}' /></span></td>
                                <td><span style="width: 120px;" title="<c:out value='${log.vchangedate}' />"><c:out value='${log.vchangedate}' /></span></td>
                                <td><span style="width: 400px;" title="${log.vchangecontent}"><c:out value='${log.vchangecontent}' /></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='　<fmt:message key="The_attachment"/> 　' id="y_6" style="overflow: auto;">
        <div class="grid" id="grid6">
            <div class="table-head">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 40px;"><fmt:message key="the_serial_number"/></span></td>
                            <td><span style="width: 150px;"><fmt:message key="name"/></span></td>
                            <td><span style="width: 120px;">操作时间</span></td>
                            <td><span style="width: 100px;"><fmt:message key="download"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <c:forEach items="${pur.transationattaList}" var="tta" varStatus="i">
                            <tr>
                                <td><span style="width: 40px;text-align: center;">${i.index+1}</span></td>
                                <td><span style="width: 150px;">${tta.vattaname}</span></td>
                                <td><span style="width: 120px;">${tta.ts}</span></td>
                                <td><span style="width: 100px;text-align: center;"><a href="<%=path%>/purcontract/download.do?transationatta=${tta.pk_transationatta}"><fmt:message key="download"/></a></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth);
                $grid.find(".table-head").width(headWidth);
            }else{
                $grid.find(".table-body").width(gridWidth);
                $grid.find(".table-head").width(gridWidth);
            }
        };
        $('.easyui-tabs').tabs({
            width:$(document.body).width()-2,
            height:$(document.body).height()-35,
            onSelect:function() {
                var id = $(this).tabs('getSelected')[0].id;
                if(id!='y_1') {
                    setElementHeight('#' + id + ' .grid', ['.tool'], $(document.body), 90);	//计算.grid的高度
                    setElementHeight('#' + id + ' .table-body', ['#' + id + ' .table-head'], '#' + id + ' .grid'); //计算.table-body的高度
                    loadGrid('#' + id + ' .grid');//  自动计算滚动条的js方法
                    setWidth('#' + id + ' .grid');
                }
            }
        });
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
    });
    var showStore=function(pro,sp_code,iquotebatch){
        window.parent.showStore(pro,sp_code,iquotebatch);
    };
</script>
</body>
</html>
