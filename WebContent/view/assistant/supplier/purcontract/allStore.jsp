<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>supply Info</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .page{
            margin-bottom: 25px;
        }
    </style>
</head>
<body>
    <div class="grid" >
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <td><span style="width:30px;">
									<input type="checkbox" id="chkAll"/></span>
                    </td>
                    <td><span style="width:50px;"><fmt:message key="coding" /></span></td>
                    <td><span style="width:120px;"><fmt:message key="name" /></span></td>
                </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach var="firmDeliver" items="${firmDeliverList}" varStatus="status">
                        <tr>
                            <td><span style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${firmDeliver.firm}' />" value="<c:out value='${firmDeliver.firm}' />"/></span>
                            </td>
                            <td><span style="width:50px;" title="${firmDeliver.firm}"><c:out value="${firmDeliver.firm}" />&nbsp;</span></td>
                            <td><span style="width:120px;" title="${firmDeliver.firmdes}"><c:out value="${firmDeliver.firmdes}" />&nbsp;</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <input type="hidden" value="${stores}" id="stores"/>
    <input type="hidden" value="${iquotebatch}" id="iquotebatch"/>
    <input type="hidden" value="${pk_purcontract}" id="pk_purcontract"/>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var selected=$("#stores").val().split(",");
        if(selected!=null&&selected!='') {
            $(".table-body").find('tr td input').each(function () {
                if ($.inArray($(this).val(), selected) >= 0) {//若大于等于零，则这个id已存在父页面中
                    $(this).attr('checked', 'checked');
                }
            });
        }
        setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
        setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        changeTh();//拖动 改变table 中的td宽度
        $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        $('#search').bind("click",function search(){
            $('#listForm').submit();
        });
        //点击checkbox改变
        $('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
            if ($(this)[0].checked) {
                $(this).attr("checked", false);
            }else{
                $(this).attr("checked", true);
            }
        });
        // 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        $('.grid').find('.table-body').find('tr').bind("click", function () {
            if ($(this).find(':checkbox')[0].checked) {
                $(this).find(':checkbox').attr("checked", false);
            }else{
                $(this).find(':checkbox').attr("checked", true);
            }
        });

    });
    var allStore=function(){
        var list=$('.table-body :checkbox');
        var data=[];
        list.filter(':checked').each(function(index){
            var par=$(this).parent().parent().parent();
            var s={};
            s.id=$(this).val();
            s.code=par.find("td:eq(1) span").text();
            s.name=par.find("td:eq(2) span").text();
            data.push(s);
        });
        return data;
    }
    var updateStore=function(sp_code){
        var checkboxList = $('.grid').find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            var codeValue=[];
            checkboxList.filter(':checked').each(function(){
                codeValue.push($(this).val());
            });
            var action = '<%=path%>/purcontract/updateStore.do?ids='+codeValue.join(",")+'&pk_purcontract='+$("#pk_purcontract").val()+'&iquotebatch='+$("#iquotebatch").val()+'&sp_code='+sp_code;
            $.post(action, function (msg) {
                if (msg == 'ok') {
                	alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
                		$(".close",parent.parent.document).click();
                	});
                } else if(msg=='error'){
                    alerterror('<fmt:message key="update_fail"/>！');
                }else{
//                    alerterror(msg);
                    jNotify(msg,{
                        autoHide:false,
                        HorizontalPositn:'center',
                        VerticalPositn:'top'
                    })
                }
            });
        }else{
            alerterror('<fmt:message key="please_select_one_store"/>！');
            return ;
        }
    }
</script>
</body>
</html>