<%--
  User: mc
  Date: 14-10-27
  Time: 下午3:37
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>显示门店</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
</head>
<body>
<div class="grid" >
    <div class="table-head" >
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td><span style="width:50px;"><fmt:message key="coding" /></span></td>
                <td><span style="width:120px;"><fmt:message key="name" /></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <c:forEach var="dep" items="${store}">
                <tr>
                    <td><span style="width:50px;" title="${dep.positn.code}"><c:out value="${dep.positn.code}" />&nbsp;</span></td>
                    <td><span style="width:120px;" title="${dep.positn.des}"><c:out value="${dep.positn.des}" />&nbsp;</span></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
        setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
    });
</script>
</body>
</html>
