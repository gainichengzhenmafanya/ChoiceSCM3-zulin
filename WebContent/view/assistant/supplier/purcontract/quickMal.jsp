<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>报价快速加入</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .line{
            BORDER-LEFT-STYLE: none;
            BORDER-RIGHT-STYLE: none;
            BORDER-TOP-STYLE: none;
            width: 70px;
        }
    </style>
</head>
<body>
<div class="leftFrame" style="width: 69.5%;margin-right: 1px;">
    <iframe src="<%=path%>/purcontract/listMaterial.do?sp_code=${sp_code}&iquotebatch=${iquotebatch}&delivercode=${delivercode}&canupdate=${canupdate}" frameborder="0" name="leftFrame" id="leftFrame"></iframe>
</div>
<div style="display: inline-block;float: left;width: 30%;" class="mainFrame">
    <iframe src="<%=path%>/purcontract/findStore.do?pk_purcontract=${pk_purcontract}&iquotebatch=${iquotebatch}&ids=${ids}&deliver=${delivercode}&sp_code=${sp_code}" style="overflow-x:scroll;" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
</div>
<div id="batchDate" style="width: 0px;height: 0px;visibility: hidden;">
    <div class="form-line" style="margin-top: 35px;">
        <div class="form-label"><fmt:message key="offer"/><fmt:message key="startdate"/></div>
        <div class="form-input">
            <input type="text" id="bdat" name="bdat" class="Wdate text"  onclick="WdatePicker()"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="offer"/><fmt:message key="enddate"/></div>
        <div class="form-input">
            <input type="text" id="edat" name="edat" class="Wdate text"  onclick="WdatePicker()"/>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    var update=function(){
        var checkboxList =$(".grid", getFrame("leftFrame").document).find('.table-body').find(':checkbox');
        if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            $('body').window({
                title: '<fmt:message key="bulk_update"/>',
                content: $("#batchDate").html(),
                width: '300px',
                height: '200px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                var bdat=$(".window-content").find("#bdat").val();
                                var edat=$(".window-content").find("#edat").val();
                                if(bdat>edat){
                                    alerterror('<fmt:message key="Purcontract_price_date_error"/>')
                                    return;
                                }
                                getFrame('leftFrame').upMalDate(bdat,edat);
                                $('.close').click();
                            }
                        },{
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
        }else{
            alerterror('<fmt:message key="please_select_one_material"/>！');
            return ;
        }
    }
    var getMal=function(){
        return getFrame("leftFrame").allMal();
    }
    var getStore=function(){
        return getFrame("mainFrame").allStore();
    }
    var updateStore=function(){
        return getFrame("mainFrame").updateStore('${sp_code}');
    }
</script>
</body>
</html>