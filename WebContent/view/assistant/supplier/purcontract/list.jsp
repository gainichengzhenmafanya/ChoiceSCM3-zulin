<%--
  User: mc
  Date: 14-10-21
  Time: 上午10:14
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>采购合约</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .grid td span{
            padding:0px;
        }
        .search-div .form-line .form-label{
            width: 10%;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div style="height:55%;">
<!--     <div class="toolbar"></div> -->
    <div class="tool"></div>
    <form id="listForm" action="<%=path%>/purcontract/list.do" method="post">
        <div class="search-div" style="positn: absolute;z-index: 19">
        <div class="form-line">
            <div class="form-label" style="width: 70px;"><fmt:message key="suppliers"/>：</div>
            <div class="form-input" style="width: 135px;">
                <input type="hidden" value="${delivercode}" name="delivercode.code" id="delivercode"/>
                <input type="text" value="${delivername}" style="width: 100px;margin-bottom:6px;" id="supplier" name="delivercode.des" readonly="readonly" style="margin:2px;" class="text"/>
                <img src="<%=path%>/image/themes/icons/search.png" style="margin-bottom: 6px;" onclick="showSupplier()"/>
            </div>
            <div class="form-label" style="width: 70px;margin-left: 16px;"><fmt:message key="date_of_signing"/>：</div>
            <div class="form-input" style="width: 104px;">
                <input type="text" value="${dsigndat}" name="dsigndat" style="width: 100px;" id="dsigndat" class="Wdate text"  onclick="WdatePicker()"/>
            </div>
            <div class="form-label" style="width: 70px;"><fmt:message key="Contract_code"/>：</div>
            <div class="form-input" style="width: 104px;">
                <input type="text" value="${vcontractcode}" id="vcontractcode" name="vcontractcode" style="width: 100px;" class="text"/>
            </div>
            <div class="form-label" style="width: 70px;"><fmt:message key="status"/>：</div>
            <div class="form-input" style="width: 104px;">
                <select name="istate" id="istate" class="select" style="width: 100px;">
                    <option <c:if test="${istate==0}"> selected="selected" </c:if> value="0"><fmt:message key="all"/></option>
                    <option <c:if test="${istate==1}"> selected="selected" </c:if> value="1"><fmt:message key="To_audit"/></option>
                    <option <c:if test="${istate==3}"> selected="selected" </c:if> value="3"><fmt:message key="take_effect"/></option>
                    <option <c:if test="${istate==4}"> selected="selected" </c:if> value="4"><fmt:message key="Termination_of"/></option>
                </select>
            </div>
        </div>
        <div class="search-commit">
            <input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
            <input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
        </div>
    </div>
    <div class="grid" id="list_grid">
        <div class="table-head">
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td>
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll"/>
                            </span>
                        </td>
                        <td><span style="width: 100px;"><fmt:message key="Purchasing_organization" /></span></td>
                        <td><span style="width: 150px;"><fmt:message key="suppliers" /></span></td>
                        <td><span style="width: 150px;"><fmt:message key="Contract_code" /></span></td>
                        <td><span style="width: 90px;"><fmt:message key="Contract" /><fmt:message key="name" /></span></td>
                        <td><span style="width: 80px;"><fmt:message key="Contract" /><fmt:message key="type" /></span></td>
                        <td><span style="width: 50px;"><fmt:message key="the_version_number"/> </span></td>
                        <td><span style="width: 100px;"><fmt:message key="Contract"/><fmt:message key="date_of_signing"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="Plan_effective_date"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="Plan_an_end_date"/></span></td>
                        <td><span style="width: 90px;"><fmt:message key="hintday"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="buyer"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="The_other_instructions"/></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${purct}" var="pur" >
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <span style="width: 30px;">
                                    <input type="checkbox"  name="idList" id="chk_<c:out value='${pur.pk_purcontract}' />" value="<c:out value='${pur.pk_purcontract}' />"/>
                                </span>
                            </td>
                            <td><span style="width: 100px;" title="${pur.positncode.des}">${pur.positncode.des}</span></td>
                            <td><span style="width: 150px;" title="${pur.delivercode.des}">${pur.delivercode.des}</span></td>
                            <td><span style="width: 150px;" title="${pur.vcontractcode}">${pur.vcontractcode}</span></td>
                            <td><span style="width: 90px;" title="${pur.vcontractname}">${pur.vcontractname}</span></td>
                            <td><span style="width: 80px;" title="${pur.vcontracttyp}">${pur.vcontracttyp}</span></td>
                            <td><span style="width: 50px;" title="${pur.vcontractversion}">${pur.vcontractversion}</span></td>
                            <td><span style="width: 100px;">${pur.dsigndat}</span></td>
                            <td><span style="width: 100px;">${pur.dstartdat}</span></td>
                            <td><span style="width: 100px;">${pur.denddat}</span></td>
                            <td><span style="width: 90px;text-align: right;">${pur.ihintday}</span></td>
                            <td><span style="width: 80px;">${pur.vpurcaccount}</span></td>
                            <td><span style="width: 50px;">
                                <input type="hidden" id="puristate" value="${pur.istate}"/>
                                <c:if test="${pur.istate==1}"><fmt:message key="To_audit"/></c:if>
                                <c:if test="${pur.istate==2}"><fmt:message key="checked"/></c:if>
                                <c:if test="${pur.istate==3}"><fmt:message key="take_effect"/></c:if>
                                <c:if test="${pur.istate==4}"><fmt:message key="Termination_of"/></c:if>
                            </span></td>
                            <td><span style="width: 200px;" title="${pur.vmemo}">${pur.vmemo}</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${page}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${page.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${page.pageSize }" />
    </div>
    </form>
    </div>
    <div class="mainFrame" style=" height:50%;width:100%;">
        <iframe src="<%=path%>/purcontract/listbottom.do" frameborder="0" name="mainFrame" id="mainFrame">
        </iframe>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var audit=${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')};
            $("#search").bind('click', function() {
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $("#vcontractcode").val(stripscript($("#vcontractcode").val()));
                $('.search-div').hide();
                $('#listForm').submit();
            });
            /* 模糊查询清空 */
            $("#resetSearch").bind('click', function() {
                $("#pk_supplier").val("");
                $("#supplier").val("");
                $("#dsigndat").val("");
                $("#vcontractcode").val("");
                $("#istate").val("0");
            });
            $('.tool').toolbar({
                items: [{
                        text: '<fmt:message key="select"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px', '-40px']
                        },
                        handler: function () {
                            $('.search-div').slideToggle(100);
                            $("#dsigndat").focus();
                        }
                    }, {
                        text: '<fmt:message key="insert"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px', '-40px']
                        },
                        handler: function () {
                            location.href="<%=path%>/purcontract/skip.do?m=1";
                        }
                    },
                    {
                        text: '<fmt:message key="update"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px', '-40px']
                        },
                        handler: function () {
                            update();
                        }
                    },
                    {
                        text: '<fmt:message key="delete"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')} ,
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px', '-40px']
                        },
                        handler: function () {
                            del();
                        }
                    },
                    "-",
                    {
                        text: '<fmt:message key="handle"/>',
//                         useable: (${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release']}||${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'stop']}),
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px', '-40px']
                        },
                        handler: function () {
                        },
                        items:[{
                            text: '<fmt:message key="take_effect"/>',
//                             useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release')},
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['0px', '-40px']
                            },
                            handler: function () {
                                updateState(3);
                            }
                        },{
                            text: '失效',
//                             useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'release')},
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['0px', '-40px']
                            },
                            handler: function () {
                                updateState(11);
                            }
                        },{
                            text: '<fmt:message key="Termination_of"/>',
//                             useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'stop')},
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['0px', '-40px']
                            },
                            handler: function () {
                                updateState(4);
                            }
                        } 
                        ]
                    },{
                        text: '<fmt:message key="quit"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px','-40px']
                        },
                        handler: function(){
                            invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                        }
                    }]
            });
            setElementHeight('#list_grid',['.toolbar','.mainFrame'],$(document.body),15);	//计算.grid的高度
            setElementHeight('#list_grid .table-body',['#list_grid .table-head'],'#list_grid'); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            $(".page").css("margin-bottom",$(".mainFrame").height()-15);
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#list_grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#list_grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('#list_grid').find('.table-body').find('tr').live("click", function () {
                $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
                $('#list_grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);

                var  url="<%=path%>/purcontract/listbottom.do?pk_purcontract="+$(this).find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            });
            $('#list_grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
                event.stopPropagation();
            });
            if($(".table-body").find("tr").size()>0){
                var  url="<%=path%>/purcontract/listbottom.do?pk_purcontract="+$(".table-body").find("tr:eq(0)").find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            }
            /*列表双击事件*/
            $('#list_grid').find('.table-body').find('tr').dblclick(function(event){
                update();
            });
            var trrows = $('#list_grid').find('.table-body').find('tr');
            if(trrows.length!=0){
                $(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
            }
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
        });
        //====================================================
        var update=function(){
            var checkboxList = $('#list_grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var state=checkboxList.filter(':checked').parents("tr").find("input[type='hidden']").val();
                if(state==1) {
                    location.href = "<%=path%>/purcontract/skip.do?m=0&pk_purcontract=" + checkboxList.filter(':checked').val();
                }else if(state==3){
                    alerterror('<fmt:message key="The_approved_and_have_effective_contract_cannot"/><fmt:message key="update"/>');
                }else if(state==4){
                	alerterror('<fmt:message key="Contract"/><fmt:message key="already"/><fmt:message key="Termination_of"/><fmt:message key="dont_update"/>');
                }
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return;
            }
        };
        //====================================================
		//删除采购合约
        var del=function(){
            var checkboxList = $('.grid').find('.table-body').find(':checkbox');
            var flag = false;
            var codeValue=[];
            var verValue=[];
            var mark=true;
            checkboxList.filter(':checked').each(function(){
                var state=$(this).parents("tr").find("#puristate").val();
                if(state==2||state==3){
                    mark=false;
                    flag = true;
                }else{
	                codeValue.push($(this).val());
	                verValue.push($.trim($(this).parents("tr").find("td:eq(6)").find("span").text()));
                }
            });
            //如果采购合约已经审核生效、终止 不能删除
            if(flag){
                mark=false;
                alerterror('<fmt:message key="The_approved_and_have_effective_contract_cannot"/><fmt:message key="delete"/>');
                return false;
            }
            if(checkboxList&&checkboxList.filter(':checked').size() > 0){
            	alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
                    if(mark) {
                        var action = '<%=path%>/purcontract/delPur.do?ids=' + codeValue.join(",")+'&versions='+verValue.join(",");
                        $('body').window({
                            title: '<fmt:message key="delete"/>',
                            content: '<iframe frameborder="0" src="' + action + '"></iframe>',
                            width: 538,
                            height: '215px',
                            draggable: true,
                            isModal: true
                        });
                    }
                });
            }else{
                alerttips('<fmt:message key="please_select_information_you_need_to_delete"/>！');
                return ;
            }
        };
        //修改采购合约
        var updateState=function(v){
            var checkboxList = $('.grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size() > 0){
                var codeValue=[];
                var verValue=[];
                var istateValue=[];
                var vbillno = [];
                var errMsg="";
                checkboxList.filter(':checked').each(function(){
                    codeValue.push($(this).val());
                    verValue.push($.trim($(this).parents("tr").find("td:eq(6)").find("span").text()));
                    istateValue.push($.trim($(this).parents("tr").find("td:eq(12)").find("input").val()));
                    vbillno.push($.trim($(this).closest("tr").find("td:eq(3)").find("span").text()));
                    if(v=="11" && '<fmt:message key="take_effect"/>'!=$.trim($(this).closest("tr").find("td:eq(12)").find("span").text())){
                    	errMsg = '<fmt:message key="cannotupdatenotsx"/>';
                    }else if(v=="12" && '<fmt:message key="Termination_of"/>'!=$.trim($(this).closest("tr").find("td:eq(12)").find("span").text())){
                    	errMsg = '<fmt:message key="cannotupdatenotzz"/>';
                    }
                });
                if(errMsg!=''){
                	alerterror(errMsg);
                	return;
                }
                var param = {};
                param['istate']=v;
                param['ids']=codeValue.join(",");
                param['versions']=verValue.join(",");
                param['oldistate']=istateValue.join(",");
                param['vbillno']=vbillno.join(",");
                
                $.post('<%=path%>/purcontract/updatePurState.do',param,function(re){
                	if(re=='OK'){
                		alerttipsbreak('<fmt:message key="status"/><fmt:message key="update"/><fmt:message key="successful"/>',function(){
                			$("#listForm").submit();
                		});
                	}else{
                		alerterror(re);
                	}
                });
            }else{
                alerttips('<fmt:message key="please_select_at_least_one_data"/>！');
                return ;
            }
        };
        var showSupplier=function() {
            selectSupplier({
                basePath: '<%=path%>',
                title: "123",
                height: 350,
                width: 650,
                domId:'pk_supplier',
                callBack: 'setSupplier',
                single: true
            });
        };
        function setSupplier(data){
            $("#supplier").val(data.delivername);
            $("#pk_supplier").val(data.delivercode);
        }
    var showStore=function(pro,sp_code,iquotebatch){
    	
        var action = '<%=path%>/purcontract/findSuitstore.do?pk_purcontract='+pro+'&sp_code='+sp_code+'&iquotebatch='+iquotebatch;
        $("body").window({
            title: '<fmt:message key="fit_store"/>' ,
            content: '<iframe frameborder="0" src="'+action+'"></iframe>',
            width: '300px',
            height: '400px',
            draggable: true,
            isModal: true
        });
    };
    </script>
</body>
</html>
