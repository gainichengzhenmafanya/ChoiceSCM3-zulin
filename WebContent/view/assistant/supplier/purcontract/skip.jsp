<%--
  User: mc
  Date: 14-10-21
  Time: 上午10:14
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>采购合约</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/assistant/ajaxfileupload.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
/*             height: 27px; */
        }
        .grid td span{
            padding:0px;
        }
        .page{
            margin-bottom:290px;
        }
        .select{
            width:133px;
        }
    </style>
</head>
<body>
<div id="wait2" style="display:block;"></div>
<div id="wait" style="display:block;">
    <img src="<%=path%>/image/loading_detail.gif" />
    &nbsp;
    <span id="UploadWaiting" style="color: #F0F0EB;"></span>
</div>
<div class="tool"></div>
<div id="div">
    <div class="condition">
        <div class="form-line">
            <input type="hidden" value="${pur.pk_purcontract}" id="pk_purcontract"/>
            <input type="hidden" value="${batch}" id="batch"/>
            <input type="hidden" value="${operate}" id="operate"/>
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="Purchasing_organization"/>：</div>
            <div class="form-input">
                <input type="text" id="org" readonly="readonly" value="${pur.positncode.des}" class="text" style="margin-bottom: 6px;"/>
                <input type="hidden" id="pk_org" value="${pur.positncode.code}" style="margin:2px;" class="text"/>
                <img src="<%=path%>/image/themes/icons/search.png" onclick="showOrg()" style="cursor: pointer;"/>
            </div>
            <div class="form-label" style="width: 94px;margin-left: 16px;"><span style="color: red;">*</span><fmt:message key="Contract_code"/>：</div>
            <div class="form-input">
                <input type="text" id="vcontractcode" maxlength="40" value="${pur.vcontractcode}" style="margin:2px;" class="text"/>
            </div>
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="Contract"/><fmt:message key="name"/>：</div>
            <div class="form-input">
                <input type="text" id="vcontractname" maxlength="40" value="${pur.vcontractname}" style="margin:2px;" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="Contract"/><fmt:message key="type"/>：</div>
            <div class="form-input">
                <select id="vcontracttyp" class="select">
                    <c:forEach items="${items}"  var="item">
                        <option value="${item.pk_contractterms}" name="${item.vname}" code="${item.vcode}">${item.vname}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="suppliers"/>：</div>
            <div class="form-input">
                <input type="hidden" id="pk_supplier" value="${pur.delivercode.code}"/>
                <input type="text" id="supplier" value="${pur.delivercode.des}" readonly="readonly" style="margin:2px 0px 6px 2px;" class="text"/>
                <c:if test="${pur.delivercode.code==null||pur.delivercode.code==''}">
                    <img src="<%=path%>/image/themes/icons/search.png" onclick="showSupplier()" style="cursor: pointer;"/>
                </c:if>
            </div>
            <div class="form-label" style="margin-left: 16px;width:94px; "><fmt:message key="buyer"/> ：</div>
            <div class="form-input">
                <input type="text" id="vpurcaccount" maxlength="30" value="${pur.vpurcaccount}" style="margin:2px;" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="date_of_signing"/>：</div>
            <div class="form-input">
                <input type="text" id="dsigndat" name="dsigndat" value="${pur.dsigndat}" class="Wdate text"  onclick="WdatePicker()"/>
            </div>
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="Plan_effective_date"/>：</div>
            <div class="form-input" style="margin-left: 2px;">
                <input type="text" id="dstartdat" name="dstartdat" value="${pur.dstartdat}" class="Wdate text"  onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'denddat\')}',dateFmt:'yyyy-MM-dd'})"/>
            </div>
            <div class="form-label"><span style="color: red;">*</span><fmt:message key="Plan_an_end_date"/>：</div>
            <div class="form-input">
                <input type="text" id="denddat" name="denddat" value="${pur.denddat}" class="Wdate text"  onfocus="WdatePicker({minDate:'#F{$dp.$D(\'dstartdat\')}',dateFmt:'yyyy-MM-dd'})"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="Contract"/><fmt:message key="status"/>：</div>
            <div class="form-input">
                <select id="istate" class="select">
                    <option <c:if test="${pur.istate==1}">selected="selected" </c:if> value="1"><fmt:message key="To_audit"/></option>
                    <option <c:if test="${pur.istate==3}">selected="selected" </c:if> value="3"><fmt:message key="take_effect"/></option>
                    <option <c:if test="${pur.istate==4}">selected="selected" </c:if> value="4"><fmt:message key="Termination_of"/></option>
                </select>
            </div>
            <div class="form-label"><fmt:message key="the_version_number"/>：</div>
            <div class="form-input">
                <input type="text" id="vcontractversion" maxlength="40" value="${pur.vcontractversion}" name="name" style="margin:2px;" class="text"/>
            </div>
            <div class="form-label"><fmt:message key="hintday"/>：</div>
            <div class="form-input">
                <input type="text" id="ihintday" onkeyup="inputNum(this)" maxlength="4" value="${pur.ihintday}" name="ihintday" style="margin:2px;" class="text"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="The_other_instructions"/>：</div>
            <div class="form-input">
                <textarea id="vmemo" style="width:677px;resize:none;margin-top: 5px;" rows="3">${pur.vmemo}</textarea>
            </div>
        </div>
    </div>
</div>
<span class="button" id="auditbut" style="z-index:10;text-align: center;">
	<input onclick="auditbut()" type="button" value="<fmt:message key="check_offer"/>"/>
</span>
<span class="button" id="addbut" style="z-index:10;text-align: center;">
	<input onclick="selectMal()"  type="button" value="<fmt:message key="add_to_the_new_offer"/> "/>
</span>
<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="margin:auto;">
    <div title='<fmt:message key="The_basic_contract"/>' id="y_1" style="padding:10px;">
        <div class="grid" id="grid1">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td>
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll"/>
                            </span>
                        </td>
                        <td><span style="width: 80px;"><fmt:message key="Quotation_for_batch"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="supplies_code"/></span></td>
                        <td><span style="width: 150px;"><fmt:message key="supplies_name"/></span></td>
                        <td><span style="width: 70px;"><fmt:message key="specification"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="supplies"/><fmt:message key="price"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="unit"/></span></td>
                        <td><span style="width: 70px;"><fmt:message key="status"/></span></td>
                        <td><span style="width: 130px;"><fmt:message key="offer"/><fmt:message key="startdate"/></span></td>
                        <td><span style="width: 130px;"><fmt:message key="offer"/><fmt:message key="enddate"/></span></td>
                        <td><span style="width: 100px;"><fmt:message key="Apply_to_stores"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
                        <td style="width:20px;border:0;cursor: pointer;"><span style="width:10px;"></span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body" id="table1">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="table1Body">
                        <c:forEach items="${pur.sppriceList}" var="ote">
                            <tr>
                                <td style="width:30px; text-align: center;">
                                    <span style="width: 30px;">
                                        <input type="checkbox"  name="idList" id="chk_<c:out value='${ote.sp_code}' />" value="<c:out value='${ote.sp_code}' />"/>
                                    </span>
                                </td>
                                <td>
                                    <input type="hidden" value="${ote.sp_code}"/>
                                    <span style="width: 80px;text-align: center;">${ote.iquotebatch}</span>
                                </td>
                                <td><span style="width: 100px;" title="${ote.sp_code}">${ote.sp_code}</span></td>
                                <td><span style="width: 150px;" title="${ote.sp_name}">${ote.sp_name}</span></td>
                                <td><span style="width: 70px;" title="${ote.sp_desc}">${ote.sp_desc}</span></td>
                                <td edit='false'><span style="width: 80px;text-align: right;" title="${ote.price}">${ote.price}</span></td>
                                <td><span style="width: 50px;" title="${ote.unit}">${ote.unit}</span></td>
                                <td><span style="width: 70px;">${ote.sta}</span></td>
                                <td><span style="width: 130px;"><input type="text" <c:if test="${ote.sta != '已审核'}">onclick="WdatePicker()"</c:if> style="width: 100%;" id="bdat" name="dstartdat" readonly="readonly" class="Wdate text" value="<fmt:formatDate value="${ote.bdat}" type="date" pattern="yyyy-MM-dd"/>" /></span></td>
                                <td><span style="width: 130px;"><input type="text" <c:if test="${ote.sta != '已审核'}">onclick="WdatePicker()"</c:if> style="width: 100%;" id="edat" name="denddat" readonly="readonly" class="Wdate text" value="<fmt:formatDate value="${ote.edat}" type="date" pattern="yyyy-MM-dd"/>" /></span></td>
                                <td><span style="width: 100px;text-align: center;color: #0000ff;" onclick="showStoreOl('${pur.pk_purcontract}','${ote.pk_spprice}','${ote.sp_code}',this)">适用分店</span></td>
<!--                                 <td><span style="width: 100px;text-align: center;color: #0000ff;">适用分店</span></td> -->
                                <td><span style="width: 200px;">${ote.memo}</span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='合约条款' id="y_2" style="padding:10px;overflow: auto;">
        <div class="condition" style="height: 33px;">
            <div class="form-line">
                <div class="form-input" style="width: 100px;">
                    <span class="button" style="text-align: center;">
                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" onclick="showEdit('1')" value="<fmt:message key="add"/>"/>
                    </span>
                </div>
                <div class="form-input" >
                   <span class="button" style="text-align: center;">
                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" onclick="del('table2')" value="<fmt:message key="delete"/>"/>
                   </span>
                </div>
            </div>
        </div>
        <div class="grid" id="grid2">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td style="width:30px; text-align: center;">
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll2"/>
                            </span>
                        </td>
                        <td><span style="width: 90px;">条款名称</span></td>
                        <td><span style="width: 150px;">条款内容</span></td>
                        <td><span style="width: 300px;">其他信息</span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body" id="table2">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="grid2-body">
                        <c:forEach items="${pur.contractclauList}" var="clau" varStatus="status">
                            <tr>
                                <td style="width:30px; text-align: center;">
                                    <span style="width: 30px;">
                                        <input type="checkbox"  name="idList2" id="chk_<c:out value='${clau.pk_contractclau}' />" value="<c:out value='${tta.pk_contractclau}' />"/>
                                    </span>
                                </td>
                                <td><span style="width: 90px;"><c:out value='${clau.vname}' /></span></td>
                                <td><span style="width: 150px;"><c:out value='${clau.vcontent}' /></span></td>
                                <td><span style="width: 300px;"><c:out value='${clau.vmemo}' /></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='合约大事记' id="y_3" style="padding:10px;overflow: auto;">
        <div class="condition" style="height: 33px;">
            <div class="form-line">
                <div class="form-input" style="width: 100px;">
                    <span class="button" style="text-align: center;">
                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;"  onclick="showEdit('2')" value="<fmt:message key="add"/>"/>
                    </span>
                </div>
                <div class="form-input" >
                    <span class="button" style="text-align: center;">
                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" onclick="del('table3')" value="<fmt:message key="delete"/>"/>
                    </span>
                </div>
            </div>
        </div>
        <div class="grid" id="grid3">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <td style="width:30px; text-align: center;">
                            <span style="width: 30px;">
                                <input type="checkbox" id="chkAll4"/>
                            </span>
                        </td>
                        <td><span style="width: 90px;">大事记编码</span></td>
                        <td><span style="width: 150px;">大事记名称</span></td>
                        <td><span style="width: 300px;">备注</span></td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body" id="table3">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="grid3-body">
                        <c:forEach items="${pur.contractevenList}" var="even" varStatus="status">
                            <tr>
                                <td style="width:30px; text-align: center;">
                                        <span style="width: 30px;">
                                            <input type="checkbox"  name="idList4" id="chk_<c:out value='${even.pk_contracteven}' />" value="<c:out value='${even.pk_contracteven}' />"/>
                                        </span>
                                </td>
                                <td><span style="width: 90px;"><c:out value='${even.vcode}' /></span></td>
                                <td><span style="width: 150px;"><c:out value='${even.vcontent}' /></span></td>
                                <td><span style="width: 300px;"><c:out value='${even.vmemo}' /></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div title='　<fmt:message key="The_attachment"/> 　' id="y_4" style="padding:10px;overflow: auto;">
        <div class="condition" style="height: 33px;">
            <form action="<%=path%>/purcontract/upload.do" method="post">
                <div class="form-line">
                    <div class="form-label">选择文件：</div>
                    <div class="form-input">
                        <input type="text" name="filetext" readonly="readonly" id="filetext" class="text"/>
                        <span class="button" style="text-align: center;margin-left:5px;">
	                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" name="fileButton" id="fileButton" value="<fmt:message key="file"/><fmt:message key="select1"/>"/>
                        </span>
                        <span class="button" style="text-align: center;margin-left:105px;">
	                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" name="start_upload" id="start_upload" value="<fmt:message key="upload"/>" disabled="disabled"/>
                        </span>
                        <span class="button" style="text-align: center;margin-left:205px;">
	                        <input type="button" style="width:100%;height:100%;border: 0px;background-color: transparent;" onclick="deltta()" name="name" value="<fmt:message key="delete"/>"/>
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <div class="grid" id="grid4" style="height: 80%;">
            <div class="table-head" >
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td><span style="width: 30px;"><fmt:message key="the_serial_number"/></span></td>
                            <td style="width:30px; text-align: center;">
                                <span style="width: 30px;">
                                    <input type="checkbox" id="chkAll3"/>
                                </span>
                            </td>
                            <td><span style="width: 200px;"><fmt:message key="name"/></span></td>
                            <td><span style="width: 100px;"><fmt:message key="download"/></span></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="table-body">
                <table cellpadding="0" cellspacing="0">
                    <tbody id="grid4-body">
                        <c:forEach items="${pur.transationattaList}" var="tta" varStatus="status">
                            <tr>
                                <td><span style="width: 30px;text-align: center;">${status.index+1}</span></td>
                                <td style="width:30px; text-align: center;">
                                    <span style="width: 30px;">
                                        <input type="checkbox"  name="idList3" id="chk_<c:out value='${tta.pk_transationatta}' />" value="<c:out value='${tta.pk_transationatta}' />"/>
                                    </span>
                                </td>
                                <td><span style="width: 200px;">${tta.vattaname}</span></td>
                                <td><span style="width: 100px;text-align: center;"><a href="<%=path%>/purcontract/download.do?transationatta=${tta.pk_transationatta}">下载</a></span></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="stroe" style="width: 0px;height: 0px;">
</div>
<div id="terms" style="visibility: hidden;">
    <div class="form-line" style="margin-top: 35px;">
        <div class="form-label"><fmt:message key="Terms"/><fmt:message key="name"/>:</div>
        <div class="form-input">
            <input type="text" maxlength="50" class="text" id="tsname" name="tsname"/>
        </div>
    </div>
    <div class="form-line" style="height: 110px;">
        <div class="form-label"><fmt:message key="Terms"/><fmt:message key="content"/>:</div>
        <div class="form-input">
            <textarea class="textarea" id="tscontent" name="tscontent" style="width: 450px;height: 100px;"></textarea>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="Terms"/><fmt:message key="other_information"/>:</div>
        <div class="form-input">
            <textarea class="textarea" id="tsother" name="tsother" style="width: 450px;height: 100px;"></textarea>
        </div>
    </div>
</div>
<div id="memorabilia" style="visibility: hidden;">
    <div class="form-line" style="margin-top: 35px;">
        <div class="form-label"><fmt:message key="event"/><fmt:message key="coding"/>:</div>
        <div class="form-input">
            <input type="text"  maxlength="50" class="text" id="mbacoding" name="mbacoding"/>
        </div>
        <div class="form-label"><fmt:message key="event"/><fmt:message key="name"/>:</div>
        <div class="form-input">
            <input type="text"  maxlength="500" class="text" id="mbaname" name="mbaname"/>
        </div>
    </div>
    <div class="form-line">
        <div class="form-label"><fmt:message key="event"/><fmt:message key="remark"/>:</div>
        <div class="form-input">
            <textarea class="textarea" id="mbamark" name="mbamark" style="width:450px;height: 200px;"></textarea>
        </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/upload/plupload.full.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
<script type="text/javascript">
var purValidate;
//=================文件上传=======================================
var uploader = new plupload.Uploader({
    browse_button : 'fileButton', //触发文件选择对话框的按钮，为那个元素id
    url : "<%=path%>/purcontract/upload.do", //服务器端的上传页面地址
    flash_swf_url : '<%=path%>/js/assistant/upload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
    silverlight_xap_url : '<%=path%>/js/assistant/upload/Moxie.xap',//silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
    multipart_params:{pk_purcontract:$("#pk_purcontract").val()},
    multi_selection:false,
    runtimes:'html5,html4,silverlight,flash',
    filters: {
        mime_types : [ { title : "word", extensions : "doc,docx" }, 
					    { title : "excel", extensions : "xls,xlsx" },
					    { title : "pdf", extensions : "pdf" }],
        max_file_size : '10mb', //最大只能上传400kb的文件
        prevent_duplicates : false //不允许选取重复文件
    },
    init : {
        PostInit: function() {
            //最后给"开始上传"按钮注册事件
            $('#start_upload').click(function(){
            	if($("#filetext").val()){
	                $("#wait").show();
	                $("#wait2").show();
	                uploader.start(); //调用实例对象的start()方法开始上传文件，当然你也可以在其他地方调用该方法
            	}else{
            		alerterror("请先选择文件。");
            		return;
            	}
            });
        },
        FileUploaded:function(uploader,file,responseObject){
            $("#wait").hide();
            $("#wait2").hide();
            var obj;
            if($.trim(responseObject.response)!=''){
                if(responseObject.response.indexOf("</pre>")>0||responseObject.response.indexOf("</PRE>")>0){
                    var pre=responseObject.response;
                    obj = jQuery.parseJSON(pre.substring(pre.indexOf(">")+1,pre.lastIndexOf("<")));
                }else{
                    obj=JSON.parse(responseObject.response);
                }
            }
            if($.trim(obj)!=''||obj.msg=='ok'){
                $("#grid4").find("tbody").append('<tr>'+
                        '<td><span style="width: 30px;text-align: center;">'+(Number($("#grid4").find(".table-body").find("tr").size())+1)+'</span></td>'+
                        '<td style="width:30px; text-align: center;">'+
                        '<span style="width: 30px;">'+
                        '<input type="checkbox"  name="idList3" id="chk_'+obj.file.pk_transationatta+'" value="'+obj.file.pk_transationatta+'"/>'+
                        '</span>'+
                        '</td>'+
                        '<td><span style="width: 200px;">'+obj.file.vattaname+'</span></td>'+
                        '<td><span style="width: 100px;text-align: center;"><a href="<%=path%>/purcontract/download.do?transationatta='+obj.file.pk_transationatta+'">下载</a></span></td>'+
                        '</tr>');
                $("#filetext").val("");
                $("#start_upload").attr("disabled","disabled");
            }else{
                alerttips('<fmt:message key="upload"/><fmt:message key="failure"/>');
            }
        },
        FilesAdded:function(uploader,files){
            if(uploader.files.length>1){
                uploader.removeFile(uploader.files[0]);
            }
            for(var i = 0, len = files.length; len>i; i++){
                var file_name = files[i].name; //文件名
                $("#filetext").val(file_name);
                $("#start_upload").removeAttr("disabled");
            }
        },
        Error:function(uploader,file){
            $("#wait").hide();
            $("#wait2").hide();
            switch (file.code){
                case -601:
                    alerterror("文件格式不匹配！");
                    break;
                case -600:
                    alerterror("文件过大！");
                    break;
                case -200:
                    alerterror("服务器错误！");
                default :
                    alerterror("上传错误，错误编码："+file.code);
                    break;
            }
        },
        UploadProgress:function(uploader,file){
            $("#UploadWaiting").text(file.percent + '%');//控制进度条
        }
    }
});
uploader.init();
//========================================================
$(document).ready(function(){
	var h = $(document.body).height();
	$("#div").height(h*0.3);
    document.onkeydown=function(){
        if(event.keyCode==27){//ESC 后关闭窗口
            debugger;
            if($('.close').length>0){
                $('.close').click();
            }else {
                reload();
            }
        }
        if(event.ctrlKey&& event.keyCode==13){
            if(duplicate()) {
                if ($.trim($("#operate").val()) == '0') {
                    save();
                } else {
                    update();
                }
            }
        }
    };
    var inputarray = $(".form-input").children().not("input[type='hidden']").not("input[readonly='readonly']").not("div").not("img");
    inputarray.bind('keyup',function(){
        if(!event.ctrlKey&&event.keyCode==13){
            var index = inputarray.index($(this)[0]);
            if(index!=inputarray.length-1){
                $(inputarray[index+1]).focus();
            }else{
                if(duplicate()) {
                    if ($.trim($("#operate").val()) == '0') {
                        save();
                    } else {
                        update();
                    }
                }
            }
        }
    });
    /*定位光标*/
    if($("#operate").val()==1) {//1 修改
        $("#vcontractname").focus();
    }else{
        $("#vcontractcode").focus();
    }
    $('.tool').toolbar({
        items: [{
            text: '<fmt:message key="save"/>',
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                if(duplicate()) {
                    if ($.trim($("#operate").val()) == '0') {
                        save();
                    } else {
                        update();
                    }
                }
            }
        },{
            text: '<fmt:message key="cancel"/>',
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                reload();
            }
        }]
    });
    var setWidth=function(id){
        var $grid=$(id);
        var headWidth=$grid.find(".table-head").find("tr").width();
        var gridWidth=$grid.width();
        if(headWidth>=gridWidth){
            $grid.find(".table-body").width(headWidth+30);
            $grid.find(".table-head").width(headWidth+40);
        }else{
            $grid.find(".table-body").width(gridWidth);
            $grid.find(".table-head").width(gridWidth);
        }
    };
//     $("#addbut").css("margin-left",$(document.body).width()-80);
//     $("#auditbut").css("margin-left",$(document.body).width()-160);
//     $("#stopbut").css("margin-left",$(document.body).width()-240);
if('${operate}'==0){
	$("#addbut").css("margin-left",$(document.body).width()-230);
    $("#auditbut").css("margin-left",$(document.body).width()-120);
}else{
	$("#addbut").css("margin-left",$(document.body).width()-450);
    $("#auditbut").css("margin-left",$(document.body).width()-340);
    $("#stopbut").css("margin-left",$(document.body).width()-230);
    $("#enablebut").css("margin-left",$(document.body).width()-120);	
}
    $('.easyui-tabs').tabs({
        width:$(document.body).width()*0.99,
        height:$(document.body).height()*0.6,
        onSelect:function(){
            var id=$(this).tabs('getSelected')[0].id;
            setElementHeight('#'+id+' .grid',['#tool','#div','#'+id+' .condition'],$(document.body),130);	//计算.grid的高度
            setElementHeight('#'+id+' .table-body',['#'+id+' .table-head'],'#'+id+' .grid'); //计算.table-body的高度
            loadGrid('#'+id+' .grid');//  自动计算滚动条的js方法
            setWidth('#'+id+' .grid');
            uploader.refresh();
        }
    });
    $('.grid').find('.table-body').find('tr').hover(
            function(){
                $(this).addClass('tr-over');
            },
            function(){
                $(this).removeClass('tr-over');
            }
    );
    //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
    $('.grid').find('.table-body').find('tr').live("click", function () {
        if ($(this).hasClass("bgBlue")) {
            $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
        }
        else
        {
            $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
        }
    });
    //=========================回车换焦点=====================
    var array = new Array();
    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击
    array = ['firmDes','firm'];
    //定义加载后定位在第一个输入框上
    $('#'+array[0]).focus();
    $("#vcontractcode").focus();
    $('select,input[type="text"]').keydown(function(e) {
        //使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target
        var event = $.event.fix(e);
        //判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点
        if (event.keyCode == 13) {
            var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
            $('#'+array[++index]).focus();
            if(index==2){
                $.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
            }
        }
    });
    //======================回车换焦点================================
    editCells();
    //==========================输入验证=============================
    purValidate = new Validate({
        validateItem:[{
            type:'text',
            validateObj:'org',
            validateType:['canNull'],
            param:['F'],
            error:['<fmt:message key="cannot_be_empty"/>!']
        },{
            type:'text',
            validateObj:'vcontractcode',
            validateType:['canNull','maxLength','zimuint'],
            param:['F','40','F'],
            error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!','<fmt:message key="onlynumletter"/>']
        },{
            type:'text',
            validateObj:'vcontractname',
            validateType:['canNull','maxLength','withOutSpecialChar'],
            param:['F','40'],
            error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="length_too_long"/>!','<fmt:message key="cannot_contain_special_characters"/>!']
        },{
            type:'text',
            validateObj:'supplier',
            validateType:['canNull'],
            param:['F','10'],
            error:['<fmt:message key="cannot_be_empty"/>!']
        },{
            type:'text',
            validateObj:'dsigndat',
            validateType:['canNull','date'],
            param:['F','10'],
            error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="incorrect_format"/>']
        },{
            type:'text',
            validateObj:'dstartdat',
            validateType:['canNull','date'],
            param:['F','10'],
            error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="incorrect_format"/>']
        },{
            type:'text',
            validateObj:'denddat',
            validateType:['canNull','date'],
            param:['F','10'],
            error:['<fmt:message key="cannot_be_empty"/>!','<fmt:message key="incorrect_format"/>']
        },{
            type:'text',
            validateObj:'vcontractversion',
            validateType:["num2"],
            param:['T'],
            error:['请输入数字!']
        },{
            type:'text',
            validateObj:'ihintday',
            validateType:["num2"],
            param:['T'],
            error:['<fmt:message key="please_input_nozero_begin"/>!']
        }]
    });
    $("td[name='deleCell']").remove();
	$("#wait2").css("display","none");
	$("#wait").css("display","none");
});
//===================ready end=================================
var allMaterial=[];
var allStore=[];
var delOte=[];
var isNumber=function(v){
    if(Number(v.value) < 0){
        alerterror('<fmt:message key="number_cannot_be_negative"/> ！');
        $(v).focus();
    }else if(isNaN(v.value)){
        alerterror('<fmt:message key="number_be_not_number"/>！');
        $(v).focus();
    }
};
/*判定数据重复*/
var duplicate=function(){
    var isdup=true;
    $("#table1Body").find("tr[name='new']").each(function(i) {//第一次循环
        if(!isdup){//如果里面有重复的就不在继续
        	isdup = false;
            return isdup;
        }
        var ipk=$(this).find("td:eq(1)").find("input").val();
        var istate=$(this).find("td:eq(7)").find("input").val();
        var ibdat=$(this).find("td:eq(8)").find("input").val();
        var iedat=$(this).find("td:eq(9)").find("input").val();
        var iprice= $.trim($(this).find("td:eq(5)").find("input").val());
        iprice=iprice?iprice:$.trim($(this).find("td:eq(5)").find("span").text());
        var istores=$("#"+$(this).attr("mark")).find("input");
        $("#table1Body").find("tr[name='new']").each(function(j) {//第二次循环
            if(i>=j){
               return false;
            }
            var opk=$(this).find("td:eq(1)").find("input").val();
            var ostate=$(this).find("td:eq(7)").find("input").val();
            var obdat=$(this).find("td:eq(8)").find("input").val();
            var oedat=$(this).find("td:eq(9)").find("input").val();
            var oprice= $.trim($(this).find("td:eq(5)").find("input").val());
            oprice=oprice?oprice:$.trim($(this).find("td:eq(5)").find("span").text());
            var ostores=$("#"+$(this).attr("mark")).find("input");
            if(!(ipk != opk || istate != ostate || ibdat != obdat || iedat != oedat || iprice != oprice)){//如果菜品一样
                if(istores.length==ostores.length){//如果门店数量一样
                    var count=0;
                    istores.each(function(ist){
                        ostores.each(function(ost){
                           if(istores[ist].value==ostores[ost].value){
                               count++;
                           }
                        });
                    });
                    if(count==istores.length){//如果相同数量等于门店数组长度，证明重复
                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="data"/><fmt:message key="with"/><fmt:message key="the"/>'+(j+1)+'<fmt:message key="line"/><fmt:message key="data"/><fmt:message key="duplicate"/>');
                        isdup=false;
                        return isdup;
                    }
                }
            }
        });
    });
    return isdup;
};
/**
 *快速选择物资
 */
var selectMal=function(){
    if($.trim($("#pk_supplier").val())==""){
        alerttips('<fmt:message key="please_select_suppliers"/>');
        return;
    }
    var action='<%=path%>/purcontract/quickMal.do?delivercode='+$("#pk_supplier").val()+'&iquotebatch='+(Number($("#batch").val())+1);
    $('body').window({
        title: '<fmt:message key="add"/><fmt:message key="supplies"/>',
        content: '<iframe id="listMaterialFrame" frameborder="0" src="'+action+'"></iframe>',
        width: '750px',
        height: '500px',
        draggable: true,
        isModal: true,
        topBar: {
            items: [
                {
                    text: '<fmt:message key="save"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-80px', '-0px']
                    },
                    handler: function () {
                        var mal=getFrame('listMaterialFrame').getMal();
                        var sto=getFrame('listMaterialFrame').getStore();
                        if(mal==null||mal==''||0>=mal.length){
                            alerttips('<fmt:message key="please_select_one_material"/>！');
                            return;
                        }
                        if(sto==null||sto==''||0>=sto.length){
                            alerttips('<fmt:message key="please_select_one_store"/>！');
                            return;
                        }
                        var errorinfo = checkdata(mal);//检查所选物资有无重复
                        if(errorinfo==''){
                        		addtable(mal,allMaterial.length);
	                            setStroe(sto,allMaterial.length);
	                            allMaterial.push(mal);
	                            allStore.push(sto);
	                            $('.close').click();
                        }else{
                        	alerterror('物资【'+errorinfo+'】重复添加！');
                        }
                        
                    }
                },
                {
                    text: '<fmt:message key="cancel"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-160px', '-100px']
                    },
                    handler: function () {
                        $('.close').click();
                    }
                },{
                    text: '<fmt:message key="bulk_update"/><fmt:message key="date"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-160px', '-100px']
                    },
                    handler: function () {
                        getFrame('listMaterialFrame').update();
                    }
                }
            ]
        }
    });
};
//===========================已保存数据门店修改==================================
var showStoreOl=function(pk_purcontract,pk_spprice,sp_code,t){
        var state = $(t).parents("tr").find("td:eq(7)").find("span").text();
        var iquotebatch = $.trim($(t).parents("tr").find("td:eq(1)").find("span").text());
        if(state=='未审核'){
        	var action = "<%=path%>/purcontract/quickMal.do?pk_purcontract="+pk_purcontract+"&iquotebatch="+iquotebatch+"&sp_code=" + sp_code + "&delivercode=" + $("#pk_supplier").val();
            $('body').window({
                title: '<fmt:message key="supplies"/><fmt:message key="update"/>',
                content: '<iframe id="listMaterialFrame" frameborder="0" src="' + action + '"></iframe>',
                width: '750px',
                height: '500px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                getFrame('listMaterialFrame').updateStore();
                            }
                        },
                        {
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        },
                        "-",
                        {
                            text: '<fmt:message key="bulk_update"/><fmt:message key="date"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                getFrame('listMaterialFrame').update();
                            }
                        }
                    ]
                }
            });
        }else{
        	var action = "<%=path%>/purcontract/quickMal.do?pk_purcontract=" + pk_purcontract + "&iquotebatch="+iquotebatch+"&sp_code=" + sp_code + "&delivercode=" + $("#pk_supplier").val()+"&canupdate=1";
            $('body').window({
                title: '<fmt:message key="supplies"/><fmt:message key="update"/>',
                content: '<iframe id="listMaterialFrame" frameborder="0" src="' + action + '"></iframe>',
                width: '750px',
                height: '500px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [
                        {
                            text: '<fmt:message key="save"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-80px', '-0px']
                            },
                            handler: function () {
                                getFrame('listMaterialFrame').updateStore();
                            }
                        },
                        {
                            text: '<fmt:message key="cancel"/>',
                            icon: {
                                url: '<%=path%>/image/Button/op_owner.gif',
                                positn: ['-160px', '-100px']
                            },
                            handler: function () {
                                $('.close').click();
                            }
                        }
                    ]
                }
            });
        }
};
//=============================================================
var showSupplier=function() {
    selectSupplier({
        basePath: '<%=path%>',
        title: "123",
        height: 400,
        width: 750,
        callBack: 'setSupplier',
        single: true
    });
};
function setSupplier(data){
    if($.trim($("#pk_supplier").val())!=data.delivercode) {
        $("#table1Body").find("tr").each(function () {
            reTr($(this).find("td:eq(0)"), 1);
        });
    }
    $("#supplier").val(data.delivername);
    $("#supplier").next(".validateMsg").remove();
    $("#pk_supplier").val(data.delivercode);
    allMaterial=[];
    allStore=[];
}

var showOrg=function(){
        chooseDepartMentList({
            basePath:'<%=path%>',
            title:'<fmt:message key="select1"/><fmt:message key="Purchasing_organization"/>',
            height:400,
            width:600,
            single:true,
            domId:'pk_org',
            type:'1'
        });
};
function setDepartmentList(data){
    $('#pk_org').val(data.code);
    $('#org').val(data.show);
    $("#org").next(".validateMsg").remove();

}
//============================把物质数据添加至表格=================================
//验证物资是否重复选择
var checkdata=function(data){
	var errorinfo = '';
	$("#table1Body").find("tr[name='new']").each(function(){
		var pk_material = $(this).find('td:eq(1)').find('input').val();
		var vmaterialname = $(this).find('td:eq(3) span').text();
		$(data).each(function(m){
			if(data[m].pk_material==pk_material){
				errorinfo = vmaterialname;
				return;
			}	
		});
	});
	return errorinfo;
};	
var addtable=function(data,mark){
    if($("#table1Body")){
        $("#table1").find("table").append("<tbody id='table1Body'></tbody>");
    }
    $(data).each(function(m){
        var tr='<tr name="new" mark="newStore'+mark+'">';
        tr+='<td style="width:30px; text-align: center;">';
        tr+='<span style="width: 30px;">';
        tr+='<input type="checkbox"  name="idList"/>';
        tr+='</span>';
        tr+='</td>';
        tr+='<td>' ;
        tr+='<input type="hidden" value="'+data[m].pk_material+'"/>';
        tr+='<span style="width: 80px;text-align: center;">'+(Number($("#batch").val())+1)+'</span></td>';
        tr+='<td><span style="width: 100px;" title="'+data[m].vcode+'">'+data[m].vcode+'</span></td>';
        tr+='<td><span style="width: 150px;" title="'+data[m].vname+'">'+data[m].vname+'</span></td>';
        tr+='<td><span style="width: 70px;">'+data[m].vspecfication+'</span></td>';
        tr+='<td><span style="width: 80px;text-align: right;" title="'+data[m].nsaleprice+'">'+data[m].nsaleprice+'</span></td>';
        tr+='<td>';
        tr+='<input type="hidden" value="'+data[m].pk_unit+'"/>';
        tr+='<span style="width: 50px;">'+data[m].unitvname+'</span></td>';
        tr+='<td><input type="hidden" value="1"/><span style="width: 70px;">未审核</span></td>';
        tr+='<td><span style="width: 130px;"><input type="text" style="width: 100%;"  name="denddat" value="'+data[m].bdat+'" class="Wdate text"  onclick="WdatePicker()"/></span></td>';
        tr+='<td><span style="width: 130px;"><input type="text" style="width: 100%;"  name="denddat" value="'+data[m].edat+'" class="Wdate text"  onclick="WdatePicker()"/></span></td>';
        tr+='<td><span style="width: 100px;text-align: center;color: #0000ff;" onclick="showStore(\'newStore'+mark+'\',\''+data[m].vcode+'\',this)">适用分店</span></td>';
        tr+='<td><span style="width: 200px;"></span></td>';
        tr+='<td style="width:20px;border:0;cursor: pointer;" onclick="reTr(this)"><img src="../image/scm/move.gif"/></td>';
        tr+='</tr>';
        $("#table1Body").append(tr);
    });
};
//报价删除
var reTr=function(v,dr){
    if(dr==1){
        var data={};
        data.pk_purcontract=$("#pk_purcontract").val();
        data.pk_spprice=$(v).parent("tr").find("td:eq(1)").find("input").val();
        data.dr=1;
        delOte.push(data);
    }
    $(v).parent("tr").remove();
};
//适用门店
var setStroe=function(data,mark){
    $("#stroe").append('<div id="newStore'+mark+'" style="overflow: auto;">' +
            '<fmt:message key="Apply_to_stores"/>：<br/>' +
            '</div>');
    $(data).each(function (m) {
        $("#newStore"+mark).append('<input type="hidden" value="' + data[m].id + '"><span name="name">'+data[m].name+'</span>,<br/>');
    });
};
//显示适用门店
var showStore=function(v,t,s){
    var codeValue=[];
    $("#"+v).find("input").each(function(){
        codeValue.push($(this).val());
    });
    //update by lq 2015-02-03 20:55:25
    var iquotebatch = $.trim($(s).closest('tr').find('td:eq(1) span').text());
    var canupdate = $(s).closest('tr').find('td:eq(7) span').text()=='未审核'?0:1;
    var action="<%=path%>/purcontract/quickMalUnSave.do?ids="+codeValue.join(",")+"&pk_material="+t+"&pk_supplier="+$("#pk_supplier").val()+"&canupdate="+canupdate+"&iquotebatch="+iquotebatch;
    if(canupdate==1){//无法修改
    	$('body').window({
            title: '<fmt:message key="supplies"/><fmt:message key="update"/>',
            content: '<iframe id="listMaterialFrame" frameborder="0" src="'+action+'"></iframe>',
            width: '750px',
            height: '500px',
            draggable: true,
            isModal: true,
            topBar: {
                items: [
                    {
                        text: '<fmt:message key="save"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-80px', '-0px']
                        },
                        handler: function () {
                            var mal=getFrame('listMaterialFrame').getMal();
                            var sto=getFrame('listMaterialFrame').getStore();
                            if(mal==null||mal==''||0>=mal.length){
                                alerttips('<fmt:message key="please_select_one_material"/>！');
                                return;
                            }
                            if(sto==null||sto==''||0>=sto.length){
                                alerttips('<fmt:message key="please_select_one_store"/>！');
                                return;
                            }
                            $(s).parents("tr").remove();
                            addtable(mal,allMaterial.length);
                            setStroe(sto,allMaterial.length);
                            allMaterial.push(mal);
                            allStore.push(sto);
                            $('.close').click();
                        }
                    },
                    {
                        text: '<fmt:message key="cancel"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px', '-100px']
                        },
                        handler: function () {
                            $('.close').click();
                        }
                    }
                ]
            }
        });
    }else{
    	$('body').window({
            title: '<fmt:message key="supplies"/><fmt:message key="update"/>',
            content: '<iframe id="listMaterialFrame" frameborder="0" src="'+action+'"></iframe>',
            width: '750px',
            height: '500px',
            draggable: true,
            isModal: true,
            topBar: {
                items: [
                    {
                        text: '<fmt:message key="save"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-80px', '-0px']
                        },
                        handler: function () {
                            var mal=getFrame('listMaterialFrame').getMal();
                            var sto=getFrame('listMaterialFrame').getStore();
                            if(mal==null||mal==''||0>=mal.length){
                                alerttips('<fmt:message key="please_select_one_material"/>！');
                                return;
                            }
                            if(sto==null||sto==''||0>=sto.length){
                                alerttips('<fmt:message key="please_select_one_store"/>！');
                                return;
                            }
                            $(s).parents("tr").remove();
                            addtable(mal,allMaterial.length);
                            setStroe(sto,allMaterial.length);
                            allMaterial.push(mal);
                            allStore.push(sto);
                            $('.close').click();
                        }
                    },
                    {
                        text: '<fmt:message key="cancel"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px', '-100px']
                        },
                        handler: function () {
                            $('.close').click();
                        }
                    },{
                        text: '<fmt:message key="bulk_update"/><fmt:message key="date"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px', '-100px']
                        },
                        handler: function () {
                            getFrame('listMaterialFrame').update();
                        }
                    }
                ]
            }
        });
    }
};
//=============================================================
//编辑表格
function editCells(){
    //====================合约基本============================
    $("#table1").autoGrid({
        isAdd:false,
        initRow:0,
        colPerRow:10,
        widths:[50,80,90,70,50,50,100,130,130,200],
        colStyle:['','','',{background:"#F1F1F1",'text-align':"right"},{background:"#F1F1F1",'text-align':"right"},{background:"#F1F1F1",'text-align':"right"}],
        onEdit:$.noop,
        editable:[5,11],
        onEnter:function(data){//默认回车后动作    data中curobj表示当前td对象，value保存输入值，ovalue保存输入之前的值
        	var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
	var rownum = data.curobj.closest('tr');
	if(pos == 5){
		var postd = data.curobj.closest('td');
		var vname = postd.find('span').attr('vname');
		postd.find('span').text(vname);
	}
        },
        cellAction:[{
            index:5,
            action:function(row,data2){
                if($.trim(row.find("td:eq(7)").find("input").val())==1) {
                    if (Number(data2.value) == 0) {
                        alerterror('<fmt:message key="amount_cannot_be_zero"/>！');
                        row.find("td:eq(5)").find('span').text(data2.ovalue);
                        $.fn.autoGrid.setCellEditable(row, 5);
                    } else if (0>Number(data2.value) ) {
                        alerterror('<fmt:message key="price_must_be_greater_than"/>');
                        row.find("td:eq(5)").find('span').text(data2.ovalue);
                        $.fn.autoGrid.setCellEditable(row, 5);
                    } else if (isNaN(data2.value)) {
                        alerterror('<fmt:message key="unitprice_be_not_valid_number"/>！');
                        row.find("td:eq(5)").find('span').text(data2.ovalue);
                        $.fn.autoGrid.setCellEditable(row, 5);
                    } else {
                        $.fn.autoGrid.setCellEditable(row, 11);
                    }
                    row.find("td:eq(5)").find('span').text(data2.value);
                }else{
                    row.find("td:eq(5)").find('span').text(data2.ovalue);
                    alerterror('<fmt:message key="offer"/>'+$.trim(row.find("td:eq(7)").find("span").text())+'<fmt:message key="dont_update"/>');
                }
            },
            onCellEdit:function(event,data2,row){
                if($.trim(row.find("td:eq(7)").find("input").val())==1) {
                    if (0>=Number(data2.value)) {
                        row.find("td:eq(5)").find('span').text(data2.valBefore);
                        alerterror('<fmt:message key="price_must_be_greater_than"/>');
                    } else if (isNaN(data2.value)) {
                        row.find("td:eq(5)").find('span').text(data2.valBefore);
                        alerterror('<fmt:message key="unitprice_be_not_valid_number"/>！');
                    }
                }else{
                    row.find("td:eq(5)").find('span').text(data2.valBefore);
                    alerterror('<fmt:message key="offer"/>'+$.trim(row.find("td:eq(7)").find("span").text())+'<fmt:message key="dont_update"/>');
                }
            }
        },{
            index:11,
            action:function(row,data){
                $.fn.autoGrid.setCellEditable(row.next(),5);
            }
        }]
    });
}
//====================================================

function checkAll(tbl,chk){
    var chkState = chk.checked;
    if(chkState){
        $("#"+tbl).find('input:checkbox').attr('checked','checked');
    }else{
        $("#"+tbl).find('input:checkbox').removeAttr('checked');
    }
}
//合约保存
var savelock = false;
var save=function(){
	if(savelock){
		return;
	}else{
		savelock = true;
	}
    var data = {};
    /*dstartdat,dsigndat,denddat*/
    if($("#dstartdat").val()>$("#denddat").val()){
        alerterror('<fmt:message key="Purcontract_date_error"/>');
		savelock = false;
        return;
    }
    if($.trim($("#vcontracttyp").val())==''){
        alerterror('<fmt:message key="Contract"/><fmt:message key="type"/><fmt:message key="cannot_be_empty"/>');
		savelock = false;
        return;
    }
    var next=true;
    if (purValidate._submitValidate()) {
        if(0>=$("#table1Body").find("tr[name='new']").size()){
            alerterror('<fmt:message key="Quotation_for_null"/>');
			savelock = false;
            return;
        }
        data["positncode.code"]=$("#pk_org").val();
        data["delivercode.code"]=$("#pk_supplier").val();
        data["positncode.des"]=$("#org").val();
        data["delivercode.des"]=$("#supplier").val();
        data.pk_purcontract=$.trim($("#pk_purcontract").val());
        data.vcontractcode= $.trim($("#vcontractcode").val());
        data.vcontractname= $.trim($("#vcontractname").val());
        data.vcontracttyp= $.trim($("#vcontracttyp").find(":selected").attr("name"));
        data.vcontracttypcode= $.trim($("#vcontracttyp").find(":selected").attr("code"));
        data.pk_contractterms= $.trim($("#vcontracttyp").val());
        data.vcontractversion= $.trim($("#vcontractversion").val());
        data.dsigndat= $.trim($("#dsigndat").val());
        data.dstartdat=$.trim($("#dstartdat").val());
        data.denddat=$.trim($("#denddat").val());
        data.vpurcaccount=$.trim($("#vpurcaccount").val());
        data.istate=$("#istate").val();
        data.ihintday=$("#ihintday").val();
        data.vmemo=$.trim($("#vmemo").val());
        //检测一个采购组织、一个供应商 采购合约是否时间重叠
        $.ajaxSetup({async:false});
		$.post("<%=path%>/purcontract/checkPurcontract.do",data,function(res){
			if(res!="1"){//如果有重叠的
				alerterror(res);
  				savelock = false;
				return;
			}else{
		        $("#table1Body").find("tr[name='new']").each(function(i)
                {
                    if(!next){  /*alerterror 阻塞代码 但是循环无法阻塞 所以适用该判定进行循环停止*/
                        return false;
                    }
                    data["sppriceList["+i+"].iquotebatch"]=$.trim($(this).find("td:eq(1)").find("span").text());
                    data["sppriceList["+i+"].sp_code"]=$(this).find("td:eq(2)").find("span").text();
                    data["sppriceList["+i+"].sp_name"]=$.trim($(this).find("td:eq(3)").find("span").text());
                    data["sppriceList["+i+"].unit"]=$.trim($(this).find("td:eq(6)").find("input").text());
                    data["sppriceList["+i+"].sp_desc"]=$.trim($(this).find("td:eq(4)").find("span").text());
                    data["sppriceList["+i+"].sta"]=$.trim($(this).find("td:eq(7)").find("span").text());
                    
//                     data["sppriceList["+i+"].area"]=$("#pk_org").val();
                    data["sppriceList["+i+"].deliver"]=$("#pk_supplier").val();
                    var price= $.trim($(this).find("td:eq(5)").find("input").val());
                    price=price?price:$.trim($(this).find("td:eq(5)").find("span").text());
                    if(!price.match("^([+]?)\\d*\\.?\\d+$")||0>=price){
                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="price_must_be_greater_than"/>');
                        next=false;
        				savelock = false;
                        return false;
                    }
                    data["sppriceList["+i+"].price"]=price;
                    var bdat=$(this).find("td:eq(8)").find("input").val();
                    var edat=$(this).find("td:eq(9)").find("input").val();
                    if(!bdat.match("^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$")||!edat.match("^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$")){
                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="time_format_of_objects"/>');
                        next=false;
        				savelock = false;
                        return false;
                    }
                    if(bdat>edat){
                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Purcontract_price_date_error"/>');
                        next=false;
        				savelock = false;
                        return false;
                    }
                    if(edat> $.trim($("#denddat").val())){
                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Offer_the_end_date_error"/>');
                        next=false;
        				savelock = false;
                        return false;
                    }
                    data["sppriceList["+i+"].bdat"]=bdat;
                    data["sppriceList["+i+"].edat"]=edat;
//                     data["sppriceList["+i+"].sta"]=$(this).find("td:eq(7)").find("input").val();
                    var vmemo=$.trim($(this).find("td:eq(11)").find("span").text());
                    vmemo=vmemo?vmemo:$.trim($(this).find("td:eq(11)").find("input").val());
//                     data["sppriceList["+i+"].vmemo"]= $.trim(vmemo);
                    $("#"+$(this).attr("mark")).find("input").each(function(j){
                        data["sppriceList["+i+"].suitstoreList["+j+"].positn.code"]=$(this).val();
//                         data["sppriceList["+i+"].suitstoreList["+j+"].pk_position.des"]=$(this).next("span[name='name']").text();
                    });
                });
                $("#grid2-body").find("tr").each(function(i){
                    data["contractclauList["+i+"].vname"]=$.trim($(this).find("td:eq(1)").find("span").text());
                    data["contractclauList["+i+"].vcontent"]=$.trim($(this).find("td:eq(2)").find("span").text());
                    data["contractclauList["+i+"].vmemo"]=$.trim($(this).find("td:eq(3)").find("span").text());
                });
                $("#grid3-body").find("tr").each(function(i){
                    data["contractevenList["+i+"].vcode"]=$.trim($(this).find("td:eq(1)").find("span").text());
                    data["contractevenList["+i+"].vcontent"]=$.trim($(this).find("td:eq(2)").find("span").text());
                    data["contractevenList["+i+"].vmemo"]=$.trim($(this).find("td:eq(3)").find("span").text());
                });
                $("#grid4-body").find("tr").each(function(i){
                    data["transationattaList["+i+"].pk_transationatta"]=$.trim($(this).find(":checkbox").val());
                });
                if(next) {
                    next=false;
        			savelock = false;
                    setTimeout(function(){next=true;},5000);
                    $.post('<%=path%>/purcontract/savePur.do', data, function (msg) {
                        if (msg == 'success') {
                        	alerttipsbreak('<fmt:message key="successful_added"/>！',function(){
            					savelock = false;
                        		reload();
                        	});
                        }else{
                            alerterror(msg);
        					savelock = false;
                        }
                    });
                }else{
        			savelock = false;
                }
			}
		});
    }else{
    	savelock = false;
    }
};
//合约修改
var update=function(){
	if(savelock){
		return;
	}else{
		savelock = true;
	}
    var data = {};
    if($("#dstartdat").val()>$("#denddat").val()){
        alerterror('<fmt:message key="Purcontract_date_error"/>');
		savelock = false;
        return false;
    }
    if($.trim($("#vcontracttyp").val())==''){
        alerterror('<fmt:message key="Contract"/><fmt:message key="type"/><fmt:message key="cannot_be_empty"/>');
		savelock = false;
        return false;
    }
    var next=true;
    if (purValidate._submitValidate()) {
        data["positncode.code"]=$("#pk_org").val();
        data["positncode.des"]=$("#org").val();
        data["delivercode.des"]=$("#supplier").val();
        data["delivercode.code"]=$("#pk_supplier").val();
        data.pk_purcontract=$.trim($("#pk_purcontract").val());
        data.vcontractcode=$.trim($("#vcontractcode").val());
        data.vcontractname=$.trim($("#vcontractname").val());
        data.vcontracttyp= $.trim($("#vcontracttyp").find(":selected").attr("name"));
        data.vcontracttypcode= $.trim($("#vcontracttyp").find(":selected").attr("code"));
        data.pk_contractterms= $.trim($("#vcontracttyp").val());
        data.vcontractversion=$.trim($("#vcontractversion").val());
        data.dsigndat=$.trim($("#dsigndat").val());
        data.dstartdat=$.trim($("#dstartdat").val());
        data.denddat=$.trim($("#denddat").val());
        data.vpurcaccount=$.trim($("#vpurcaccount").val());
        data.istate=$("#istate").val();
        data.ihintday=$("#ihintday").val();
        data.vmemo=$.trim($("#vmemo").val());
        //检测一个采购组织、一个供应商 采购合约是否时间重叠
        $.ajaxSetup({async:false});
		$.post("<%=path%>/purcontract/checkPurcontract.do",data,function(res){
			if(res!="1"){//如果有重叠的
				alerterror(res);
  				savelock = false;
				return;
			}else{
		        $("#table1Body").find("tr").each(function(i){
                    if(!next){  /*alerterror 阻塞代码 但是循环无法阻塞 所以适用该判定进行循环停止*/
                        return false;
                    }
                        data["sppriceList["+i+"].iquotebatch"]=$.trim($(this).find("td:eq(1)").find("span").text());
                        data["sppriceList["+i+"].sp_code"]=$(this).find("td:eq(2)").find("span").text();
                        data["sppriceList["+i+"].sp_name"]=$.trim($(this).find("td:eq(3)").find("span").text());
                        data["sppriceList["+i+"].unit"]=$.trim($(this).find("td:eq(6)").find("input").text());
                        data["sppriceList["+i+"].sp_desc"]=$.trim($(this).find("td:eq(4)").find("span").text());
                        data["sppriceList["+i+"].sta"]=$.trim($(this).find("td:eq(7)").find("span").text());
                        
//                         data["sppriceList["+i+"].area"]=$("#pk_org").val();
                        data["sppriceList["+i+"].deliver"]=$("#pk_supplier").val();
                        var price= $.trim($(this).find("td:eq(5)").find("input").val());
                        price=price?price:$.trim($(this).find("td:eq(5)").find("span").text());
                        if(!price.match("^([+]?)\\d*\\.?\\d+$")||0>=price){
                            next=false;
                            alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="price_must_be_greater_than"/>');
            				savelock = false;
                            return false;
                        }
                        data["sppriceList["+i+"].price"]=price;
                        data["sppriceList["+i+"].priceold"]=price;
                        var bdat=$(this).find("td:eq(8)").find("input").val();
                        var edat=$(this).find("td:eq(9)").find("input").val();
                        if(!bdat.match("^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$")||!edat.match("^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$")){
                            next=false;
                            alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="time_format_of_objects"/>');
            				savelock = false;
                            return false;
                        }
                        if(bdat>edat){
                            next=false;
                            alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Purcontract_price_date_error"/>');
            				savelock = false;
                            return false;
                        }
                        if(edat> $.trim($("#denddat").val())){
                            next=false;
                            alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Offer_the_end_date_error"/>');
            				savelock = false;
                            return false;
                        }
                        data["sppriceList["+i+"].bdat"]=bdat;
                        data["sppriceList["+i+"].edat"]=edat;
//                         data["sppriceList["+i+"].sta"]=$(this).find("td:eq(7)").find("input").val();
                        data["sppriceList["+i+"].pk_purcontract"]=$.trim($('#pk_purcontract').val());
                        var vmemo=$(this).find("td:eq(11)").find("span").text();
                        vmemo=vmemo?vmemo:$(this).find("td:eq(11)").find("input").val();
//                         data["contractQuoteList["+i+"].vmemo"]= $.trim(vmemo);
                        $("#"+$(this).attr("mark")).find("input").each(function(j){
                        	data["sppriceList["+i+"].suitstoreList["+j+"].positn.code"]=$(this).val();
                        });
                });
//                 for(var i=0;delOte.length>i;i++){
//                     data["delContractQuoteList["+i+"].pk_purcontract"]=delOte[i].pk_purcontract;
//                     data["delContractQuoteList["+i+"].pk_spprice"]=delOte[i].pk_spprice;
//                 };
                $("#grid2-body").find("tr").each(function(i){
                    data["contractclauList["+i+"].vname"]=$.trim($(this).find("td:eq(1)").find("span").text());
                    data["contractclauList["+i+"].vcontent"]=$.trim($(this).find("td:eq(2)").find("span").text());
                    data["contractclauList["+i+"].vmemo"]=$.trim($(this).find("td:eq(3)").find("span").text());
                });
                $("#grid3-body").find("tr").each(function(i){
                    data["contractevenList["+i+"].vcode"]=$.trim($(this).find("td:eq(1)").find("span").text());
                    data["contractevenList["+i+"].vcontent"]=$.trim($(this).find("td:eq(2)").find("span").text());
                    data["contractevenList["+i+"].vmemo"]=$.trim($(this).find("td:eq(3)").find("span").text());
                });
                if(next) {
                    $.post('<%=path%>/purcontract/updatePur.do', data, function (msg) {
                        if (msg == 'success') {
                        	alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
            					savelock = false;
                        		reload();
                        	});
                        } else {
                            alerterror(msg);
        					savelock = false;
                        }
                    });
                }else{
        			savelock = false;
                }
			}
		});
    }else{
		savelock = false;
    }
};
//报价审核
var auditbut=function(){
    var checkboxList=$("#grid1").find(".table-body").find(":checkbox");
    if(checkboxList&&checkboxList.filter(':checked').size() > 0){
        var next=true;
        checkboxList.filter(':checked').each(function(i){
            var price= $.trim($(this).parents("tr").find("td:eq(5)").find("input").val());
            price=price?price:$.trim($(this).parents("tr").find("td:eq(5)").find("span").text());
            if(!price.match("^([+]?)\\d*\\.?\\d+$")||price<=0){
                next=false;
                alerterror('<fmt:message key="the_selected"/><fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="price_must_be_greater_than"/>');
                return false;
            }
            var bdat= $.trim($(this).parents("tr").find("td:eq(8)").find("input").val());
            var edat= $.trim($(this).parents("tr").find("td:eq(9)").find("input").val());
            if(bdat>edat){
                next=false;
                alerterror('<fmt:message key="check_offer"/><fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Purcontract_price_date_error"/>');
                return false;
            }
            if(edat> $.trim($("#denddat").val())){
                next=false;
                alerterror('<fmt:message key="check_offer"/><fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Offer_the_end_date_error"/>');
                return false;
            }
        });
        if(next) {
            checkboxList.filter(':checked').each(function (i) {
                if ($(this).parents("tr").find("td:eq(7)").find("span").text() == '未审核' && next) {
                    $(this).parents("tr").find("td:eq(7)").find("span").text('已审核');
                    
                    $(this).parents("tr").find("td:eq(8)").find("input").removeAttr("onclick");
                    $(this).parents("tr").find("td:eq(8)").find("input").attr("readonly","readonly");
                    $(this).parents("tr").find("td:eq(9)").find("input").attr("readonly","readonly");
                    $(this).parents("tr").find("td:eq(9)").find("input").removeAttr("onclick");
                    
                    //update by lq 2015-02-03 20:46:23  增加不可编辑属性
                    $(this).parents("tr").find("td:eq(5) span").text( $(this).parents("tr").find("td:eq(5) input").val());
                    $(this).parents("tr").find("td:eq(5)").attr('edit','false');
                    
                } else if ($(this).parents("tr").find("td:eq(7)").find("input").val() == 3 && next) {
                    next=false;
                    alerterror('<fmt:message key="stop_enabled_cannot_audited"/>');
                    return  false;
                }
            });
            if(next){
            	alerttips("<fmt:message key="check_successful"/>,<fmt:message key="Save_the_record"/>");
            }
        }
    }else{
        alerterror('<fmt:message key="please_select"/><fmt:message key="check"/><fmt:message key="data"/>');
    }
};
//启用报价
var enablebut=function(){
    var checkboxList=$("#grid1").find(".table-body").find(":checkbox");
    if(checkboxList&&checkboxList.filter(':checked').size() > 0){
    	var checkstate = true;//选中的报价状态
    	checkboxList.filter(':checked').each(function(){
    		var state = $(this).parents("tr").find("td:eq(7)").find("input").val();
    		if(state==1 || state==2){
        			alerterror('只能启用已停用的报价！');
    			checkstate = false;
        			return;
    		}
    	});
    	if(checkstate){
    		checkboxList.filter(':checked').each(function(){
	            	$(this).parents("tr").find("td:eq(7)").find("input").val(2);
	              	$(this).parents("tr").find("td:eq(7)").find("span").text('<fmt:message key="checked"/>');
	            });
    		alerttips('<fmt:message key="enable"/><fmt:message key="successful"/>');
    	}
    }else{
        alerterror('<fmt:message key="please_select"/><fmt:message key="enable"/><fmt:message key="data"/>');
    }
};
//停用报价
var stopbut=function(){
    var checkboxList=$("#grid1").find(".table-body").find(":checkbox");
    if(checkboxList&&checkboxList.filter(':checked').size() > 0){
        checkboxList.filter(':checked').each(function(){
            $(this).parents("tr").find("td:eq(7)").find("input").val(3);
            $(this).parents("tr").find("td:eq(7)").find("span").text('<fmt:message key="disable1"/>');
        });
        alerttips('<fmt:message key="disable1"/><fmt:message key="successful"/>');
    }else{
        alerterror('<fmt:message key="please_select"/><fmt:message key="disable1"/><fmt:message key="data"/>');
    }
};
var reload=function(){
    location.href="<%=path%>/purcontract/list.do";
};
/*附件上传*/
var upload=function(){
    $("#wait").toggle();
    $("#wait2").toggle();
    var file= $.trim($("#file").val());
    if(file==''){
        alerttips('<fmt:message key="Please_select_a_file_to_upload"/>');
    }
    $.ajaxFileUpload({
        fileElementId:'file',
        secureuri:false,//一般设置为false
        dateType:'json',
        url:"<%=path%>/purcontract/upload.do?pk_purcontract="+$("#pk_purcontract").val(),
        success:function(data, status){
            $("#wait").toggle();
            $("#wait2").toggle();
            var obj = jQuery.parseJSON($(data).find("pre").html());
            if(obj.msg=='ok'){
                $("#grid4").find("tbody").append('<tr>'+
                    '<td><span style="width: 30px;text-align: center;">'+(Number($("#grid4").find(".table-body").find("tr").size())+1)+'</span></td>'+
                    '<td style="width:30px; text-align: center;">'+
                        '<span style="width: 30px;">'+
                        '<input type="checkbox"  name="idList3" id="chk_'+obj.file.pk_transationatta+'" value="'+obj.file.pk_transationatta+'"/>'+
                        '</span>'+
                    '</td>'+
                    '<td><span style="width: 200px;">'+obj.file.vattaname+'</span></td>'+
                    '<td><span style="width: 100px;text-align: center;"><a href="<%=path%>/purcontract/download.do?transationatta='+obj.file.pk_transationatta+'">下载</a></span></td>'+
                '</tr>');
                alerttips('<fmt:message key="upload"/><fmt:message key="successful"/>');
            }else{
                alerterror('<fmt:message key="upload"/><fmt:message key="failure"/>');
            }
        },
        error:function(s,xml,status,e){
            alerterror(s);
        }
    });
};

/*附件删除*/
var deltta=function(){
    var checkboxList = $('#grid4').find('.table-body').find(':checkbox');
    if(checkboxList&&checkboxList.filter(':checked').size() > 0){
    	alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
            var codeValue=[];
            checkboxList.filter(':checked').each(function(){
                codeValue.push($(this).val());
            });
            var action = '<%=path%>/purcontract/deltta.do';
            $.post(action,{ids:codeValue.join(",")},function(msg){
                if(msg=="ok"){
                    checkboxList.filter(':checked').parents("tr").remove();
                }else{
                    alerterror('<fmt:message key="delete_fail"/>');
                }
            });
        });
    }else{
        alerttips('<fmt:message key="please_select_information_you_need_to_delete"/>！');
        return ;
    }
};
/*判定是否输入是数字，如果不是则删除输入的数据*/
var inputNum=function(v){
    var value= $.trim($(v).val());
    if(!value.match("^([+]?)\\d*\\.?\\d+$")){
        if(value.length>0){
            $(v).val(Number(value.substring(0,Number(value.length)-1)));
        }
    }else{
        $(v).val(Number(value));
    }
};
var showEdit=function(index){
    var id;
    if(index=='1'){
        id='terms';
    }else{
        id='memorabilia';
    }
    $('body').window({
        title: '<fmt:message key="insert"/>',
        content: $("#"+id).html(),
        id:"insertWin",
        width: '630px',
        height: '400px',
        topBar: {
            items: [
                {
                    text: '<fmt:message key="save"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-80px', '-0px']
                    },
                    handler: function () {
                        var data={};
                        if(index=='1'){
                            data.name= $.trim($("#insertWin").find("#tsname").val());
                            data.content=$.trim($("#insertWin").find("#tscontent").val());
                            data.other=$.trim($("#insertWin").find("#tsother").val());
                            if(data.name==""){
                                errorbox($("#insertWin").find("#tsname"),'<fmt:message key="cannot_be_empty"/>');
                                return;
                            }
                            if(data.content==""){
                                errorbox($("#insertWin").find("#tscontent"),'<fmt:message key="cannot_be_empty"/>');
                                return;
                            }
                            if(data.name.replace(/[^\x00-\xff]/g,"**").length >50){
                                alerterror('<fmt:message key="Terms"/><fmt:message key="name"/><fmt:message key="the_maximum_length_not"/>50<fmt:message key="character"/>');
                                return;
                            }else if(data.content.replace(/[^\x00-\xff]/g,"**").length >300){
                                alerterror('<fmt:message key="Terms"/><fmt:message key="content"/><fmt:message key="the_maximum_length_not"/>300<fmt:message key="character"/>');
                                return;
                            }else if(data.other.replace(/[^\x00-\xff]/g,"**").length>200){
                                alerterror('<fmt:message key="Terms"/><fmt:message key="other_information"/><fmt:message key="the_maximum_length_not"/>200<fmt:message key="character"/>');
                                return;
                            }

                            addTerme(data);
                        }else{
                            data.code=$.trim($("#insertWin").find("#mbacoding").val());
                            data.name=$.trim($("#insertWin").find("#mbaname").val());
                            data.remark=$.trim($("#insertWin").find("#mbamark").val());
                            if(data.code==""){
                                errorbox($("#insertWin").find("#mbacoding"),'<fmt:message key="cannot_be_empty"/>');
                                return;
                            }
                            if(data.name==""){
                                errorbox($("#insertWin").find("#mbaname"),'<fmt:message key="cannot_be_empty"/>');
                                return;
                            }
                            if(data.code.replace(/[^\x00-\xff]/g,"**").length >50){
                                alerterror('<fmt:message key="event"/><fmt:message key="coding"/><fmt:message key="the_maximum_length_not"/>50<fmt:message key="character"/>');
                                return;
                            }else if(data.name.replace(/[^\x00-\xff]/g,"**").length >500){
                                alerterror('<fmt:message key="event"/><fmt:message key="name"/><fmt:message key="the_maximum_length_not"/>500<fmt:message key="character"/>');
                                return;
                            }else if(data.remark.replace(/[^\x00-\xff]/g,"**").length>500){
                                alerterror('<fmt:message key="event"/><fmt:message key="remark"/><fmt:message key="the_maximum_length_not"/>500<fmt:message key="character"/>');
                                return;
                            }
                            addMemorabilia(data);
                        }
                        $('.close').click();
                    }
                },{
                    text: '<fmt:message key="cancel"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['-160px', '-100px']
                    },
                    handler: function () {
                        $('.close').click();
                    }
                }
            ]
        }
    });
};
/*tsname
tscontent
tsother
mbacoding
mbaname
mbamark*/
var addTerme=function(data){
    if($.trim(data.name)==null||$.trim(data.content)==null){
        return false;
    }
    $("#grid2-body").append('<tr>'+
            '<td style="width:30px; text-align: center;">'+
            '<span style="width: 30px;">'+
            '<input type="checkbox"  name="idList2" />'+
            '</span>'+
            '</td>'+
            '<td><span style="width: 90px;" title="'+data.name+'">'+data.name+'</span></td>'+
            '<td><span style="width: 150px;" title="'+data.content+'">'+data.content+'</span></td>'+
            '<td><span style="width: 300px;" title="'+data.other+'">'+data.other+'</span></td>'+
            '</tr>');
};
var addMemorabilia=function(data){
    $("#grid3-body").append('<tr>'+
            '<td style="width:30px; text-align: center;">'+
            '<span style="width: 30px;">'+
            '<input type="checkbox"  name="idList4"/>'+
            '</span>'+
            '</td>'+
            '<td><span style="width: 90px;" title="'+data.code+'">'+data.code+'</span></td>'+
            '<td><span style="width: 150px;" title="'+data.name+'">'+data.name+'</span></td>'+
            '<td><span style="width: 300px;" title="'+data.remark+'">'+data.remark+'</span></td>'+
            '</tr>');
};
var del=function(id){
    if($("#"+id).find("input[checked='checked']").size()<=0){
        alerttips("<fmt:message key="please_select_at_least_one_data"/>");
        return;
    }
    $("#"+id).find("input[checked='checked']").each(function(){
        $(this).parents("tr").remove();
    });
};
</script>
</body>
</html>
