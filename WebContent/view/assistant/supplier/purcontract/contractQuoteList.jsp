<%--
  获取合约明细
  User: mc
  Date: 14-12-13
  Time: 下午3:31
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
  String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
  <title>获取合约明细</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
</head>
<body>
<form action="<%=path%>/purcontract/getContractQuote.do" id="tabForm">
    <input type="hidden" id="pk_purcontract" name="pk_purcontract" value="${pk_purcontract}"/>
    <div class="grid" id="grid1">
      <div class="table-head">
        <table cellpadding="0" cellspacing="0">
          <thead>
          <tr>
            <td><span style="width: 80px;"><fmt:message key="Quotation_for_batch"/></span></td>
            <td><span style="width: 100px;"><fmt:message key="supplies_code"/></span></td>
            <td><span style="width: 150px;"><fmt:message key="supplies_name"/></span></td>
            <td><span style="width: 50px;"><fmt:message key="specification"/></span></td>
            <td><span style="width: 80px;"><fmt:message key="supplies"/><fmt:message key="price"/></span></td>
            <td><span style="width: 50px;"><fmt:message key="unit"/></span></td>
            <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
            <td><span style="width: 100px;"><fmt:message key="offer"/><fmt:message key="startdate"/></span></td>
            <td><span style="width: 100px;"><fmt:message key="offer"/><fmt:message key="enddate"/></span></td>
            <td><span style="width: 70px;"><fmt:message key="Apply_to_stores"/></span></td>
            <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
          </tr>
          </thead>
        </table>
      </div>
      <div class="table-body">
        <table cellpadding="0" cellspacing="0">
          <tbody>
          <c:forEach items="${sppriceList}" var="spprice">
            <tr>
              <td><span style="width: 80px;text-align: center;">${spprice.iquotebatch}</span></td>
              <td><span style="width: 100px;">${spprice.sp_code}</span></td>
              <td><span style="width: 150px;">${spprice.sp_name}</span></td>
              <td><span style="width: 50px;">${spprice.sp_desc}</span></td>
              <td><span style="width: 80px;text-align: right;">
                                        <c:if test="${spprice.price==''||spprice.price==null}">0.00</c:if>
                                        <c:if test="${spprice.price!=''&&spprice.price!=null}">
                                          <fmt:formatNumber value="${spprice.price}" pattern="0.00"/>
                                        </c:if>
                                    </span></td>
              <td><span style="width: 50px;">${spprice.unit}</span></td>
              <td><span style="width: 50px;">${spprice.sta}</span></td>
              <td><span style="width: 100px;"><fmt:formatDate value="${spprice.bdat}" type="date" pattern="yyyy-MM-dd"/></span></td>
              <td><span style="width: 100px;"><fmt:formatDate value="${spprice.edat}" type="date" pattern="yyyy-MM-dd"/></span></td>
              <td><span style="width: 70px;text-align: center;color: #0000ff;" onclick="showStore('${pk_purcontract}','${spprice.sp_code}','${spprice.iquotebatch}')">适用门店</span></td>
<%--               <td><span style="width: 70px;text-align: center;color: #0000ff;">${spprice.pk_spprice}</span></td> --%>
              <td><span style="width: 200px;">${spprice.memo}</span></td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
    <div>
        <page:page form="tabForm" page="${page}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${page.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${page.pageSize }" />
    </div>
</form>

<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    var setWidth=function(id){
      var $grid=$(id);
      var headWidth=$grid.find(".table-head").find("tr").width();
      var gridWidth=$grid.width();
      if(headWidth>=gridWidth){
        $grid.find(".table-body").width(headWidth);
        $grid.find(".table-head").width(headWidth);
      }else{
        $grid.find(".table-body").width(gridWidth);
        $grid.find(".table-head").width(gridWidth);
      }
    };
    setElementHeight('.grid',['.tool'],$(document.body),25);	//计算.grid的高度
    setElementHeight('.table-body',['.table-head'],'.grid'); //计算.table-body的高度
    loadGrid('.grid');//  自动计算滚动条的js方法
    setWidth('.grid');
    $(".page").css("margin-bottom",0);
  });
  var showStore=function(pro,sp_code,iquotebatch){
    window.parent.showStore(pro,sp_code,iquotebatch);
  };
</script>
</body>
</html>
