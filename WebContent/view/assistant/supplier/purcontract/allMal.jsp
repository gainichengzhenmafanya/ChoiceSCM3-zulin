<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>supply Info</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
</head>
<body>
<form id="listForm" action="<%=path%>/purcontract/listMaterial.do">
    <div class="grid" >
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <td><span style="width:30px;">
                        <input type="checkbox" id="chkAll"/></span>
                    </td>
                    <td><span style="width:80px;"><fmt:message key="supplies_code"/></span></td>
                    <td><span style="width:80px;"><fmt:message key="supplies_name"/></span></td>
                    <td><span style="width:50px;"><fmt:message key="unit"/></span></td>
                    <td><span style="width:50px;"><fmt:message key="price"/></span></td>
                    <td><span style="width:50px;"><fmt:message key="specification"/></span></td>
                    <td><span style="width:100px;"><fmt:message key="offer"/><fmt:message key="startdate"/></span></td>
                    <td><span style="width:100px;"><fmt:message key="offer"/><fmt:message key="enddate"/></span></td>
                </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                <c:forEach var="mt" items="${mat}" varStatus="status">
                    <tr>
                        <td>
                            <span style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_${mt.pk_material}" value="${mt.pk_material}"/>
                            </span>
                        </td>
                        <td><span style="width:80px;" title="${mt.sp_code}">${mt.sp_code}</span></td>
                        <td><span style="width:80px;" title="${mt.sp_name}">${mt.sp_name}</span></td>
                        <td>
                            <span style="width:50px;">${mt.unit}</span></td>
                        <td><span style="width:50px;text-align: right;" title="${mt.sp_price}">${mt.sp_price}</span></td>
                        <td><span style="width:50px;">${mt.sp_desc}</span></td>
                        <td><span style="width:100px;">
                            <input type="text" name="bdat" id="bdat"  style="width: 98px; margin: auto;" value="<fmt:formatDate value="${dat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text"  onclick="WdatePicker()"/>
                        </span></td>
                        <td><span style="width:100px;">
                            <input type="text" name="edat" id="edat"  style="width: 98px; margin: auto;" value="<fmt:formatDate value="${dat}" pattern="yyyy-MM-dd" type="date"/>" class="Wdate text"  onclick="WdatePicker()"/>
                        </span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <%--<div>
        <page:page form="listForm" page="${page}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${page.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${page.pageSize }" />
    </div>--%>
</form>
<input value="${pk_material}" type="hidden" id="pk_material"/>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript">
var canupdate = '${canupdate}';
    $(document).ready(function(){
        var selected=$("#pk_material").val();
        if(selected!=null&&selected!='') {
            $(".table-body").find('tr td input').each(function () {
                if ($(this).val()==selected) {//若大于等于零，则这个id已存在父页面中
                    $(this).attr('checked', 'checked');
                }
                if(canupdate==1){
                	$(this).prop({
                        disabled: true
                    });
                }
            });
        }
        setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
        setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        changeTh();//拖动 改变table 中的td宽度
        $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        $('#search').bind("click",function search(){
            $('#listForm').submit();
        });
        //点击checkbox改变
        $('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
            if ($(this)[0].checked) {
                $(this).attr("checked", false);
            }else{
                $(this).attr("checked", true);
            }
        });
        // 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
        if(canupdate!=1){
        	$('.grid').find('.table-body').find('tr').bind("click", function () {
                if ($(this).find(':checkbox')[0].checked) {
                    $(this).find(':checkbox').attr("checked", false);
                }else{
                    $(this).find(':checkbox').attr("checked", true);
                }
            });
        }
        var defaultCode = '${ids}';
        var codes = defaultCode.split(',');
        if(defaultCode!=''){
            $('.grid').find('.table-body').find(':checkbox').each(function(){
                for(var i in codes){
                    if(this.id.substr(4,this.id.length)==codes[i]){
                        $(this).attr("checked", true);
                    }
                }
            })
        }

    });
    var upMalDate=function(bdat,edat){
        var list=$('.table-body :checkbox');
        list.filter(':checked').each(function(){
            if(bdat!=''){
                $(this).parents("tr").find("td:eq(6) input").val(bdat);
            }
            if(edat!=''){
                $(this).parents("tr").find("td:eq(7) input").val(edat);
            }
        });
    }
    var allMal=function(){
        var list=$('.table-body :checkbox');
        var data=[];
        list.filter(':checked').each(function(){
            var par=$(this).parent().parent().parent();
            var m={};
            m.pk_material=$(this).val();
            m.vcode=par.find("td:eq(1) span").text();
            m.vname=par.find("td:eq(2) span").text();
            m.unitvname=par.find("td:eq(3) span").text();
            m.pk_unit=par.find("td:eq(3) input").val();
            m.nsaleprice=par.find("td:eq(4) span").text();
            m.vspecfication=par.find("td:eq(5) span").text();
            m.bdat=par.find("td:eq(6) input").val();
            m.edat=par.find("td:eq(7) input").val();
            data.push(m);
        });
        return data;
    }
</script>
</body>
</html>