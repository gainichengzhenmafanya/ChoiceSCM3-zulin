<%--
  User: mc
  Date: 14-10-21
  Time: 上午10:14
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>供应商列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div style="height:55%;">
	<div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/supplier/list.do" method="post">
    	<input type="hidden" id="moduleId" name="moduleId" value="${moduleId }" />
    <div class="search-div" style="positn: absolute;z-index: 19;">
        <div class="form-line">
            <div class="form-label" style="width: 100px;"><fmt:message key="coding"/>：</div>
            <div class="form-input">
                <input type="text" name="vcode" id="vcode"  value="${supplier.vcode}" class="text"/>
            </div>
            <div class="form-label" style="width: 100px;"><fmt:message key="name"/>：</div>
            <div class="form-input">
                <input type="text" name="vname" id="vname"  value="${supplier.vname}" class="text"/>
            </div>
            <div class="form-label" style="width: 100px;"><fmt:message key="Mnemonic"/>：</div>
            <div class="form-input">
                <input type="text" name="vinit" id="vinit"  value="${supplier.vinit}" class="text"/>
            </div>
		</div>
        <div class="form-line">
            <div class="form-label" style="width: 100px;"><fmt:message key="suppliers"/><fmt:message key="category"/>：</div>
            <div class="form-input">
				<input type="hidden" name="suppliertype" id="delivercodetype" class="text" value="${supplier.suppliertype }"/>
				<input type="text" name="suppliertypename" id="suppliertypename" class="text" style="margin-bottom: 6px;"   style="vertical-align: initial;" value="${supplier.suppliertypename }"   readonly="readonly"/>
				<img id="suppliertypebutton" class="search" style="margin-bottom: 6px;"  src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
			</div>
            <div class="form-label" style="width: 100px;"><fmt:message key="enable_state"/>：</div>
            <div class="form-input">
                <select class="select" name="enablestate" id="enablestate" style="margin-top: 3px;">
                    <option <c:if test="${supplier.enablestate==0}"> selected="selected" </c:if> value="0"><fmt:message key="all"/></option>
                    <option <c:if test="${supplier.enablestate==1}"> selected="selected" </c:if> value="1"><fmt:message key="not_enabled"/></option>
                    <option <c:if test="${supplier.enablestate==2}"> selected="selected" </c:if> value="2"><fmt:message key="have_enabled"/></option>
                    <option <c:if test="${supplier.enablestate==3}"> selected="selected" </c:if> value="3"><fmt:message key="stop_enabled"/></option>
                </select>
            </div>
        </div>
        <div class="search-commit">
            <input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
            <input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
        </div>
    </div>
    <div class="grid" id="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:30px;">
                            <input type="checkbox" id="chkAll"/>
                        </td>
                        <td><span style="width: 100px;"><fmt:message key="suppliers_type"/></span></td>
                        <td><span style="width: 120px;"><fmt:message key="suppliers_coding"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="suppliers_name"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="jmu_supplier"/></span></td>
                        <td><span style="width: 80px;"><fmt:message key="contact"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="address"/></span></td>
                        <td><span style="width: 50px;"><fmt:message key="status"/></span></td>
                        <td><span style="width: 200px;"><fmt:message key="remark"/></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${supplierList}" var="supplier" >
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${supplier.delivercode}' />" value="<c:out value='${supplier.delivercode}' />"/>
                            </td>
                            <td><span style="width: 100px;text-align: left;" title="${supplier.delivercodetype.vname}">${supplier.delivercodetype.vname}</span></td>
                            <td><span style="width: 120px;text-align: left;" title="${supplier.vcode}">${supplier.vcode}</span></td>
                            <td><span style="width: 200px;text-align: left;" title="${supplier.vname}">${supplier.vname}</span></td>
                            <td><span style="width: 200px;text-align: left;" title="${supplier.vnamejmu}">${supplier.vnamejmu}</span></td>
                            <td><span style="width: 80px;text-align: left;">${supplier.vcontact}</span></td>
                            <td><span style="width: 200px;text-align: left;" title="${supplier.vaddr}">${supplier.vaddr}</span></td>
                            <td><span style="width: 50px;text-align: center;">
                                <c:if test="${supplier.enablestate==1}"><fmt:message key="not_enabled"/></c:if>
                                <c:if test="${supplier.enablestate==2}"><fmt:message key="have_enabled"/></c:if>
                                <c:if test="${supplier.enablestate==3}"><fmt:message key="stop_enabled"/></c:if>
                            </span>
                             <input type="hidden" value="${supplier.enablestate}"/>
                            </td>
                            <td><span style="width: 200px;text-align: left;" title="${supplier.vmemo}">${supplier.vmemo}</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${pageobj}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
    </div>
    </form>
    </div>
    <div class="mainFrame" style=" height:50%;width:100%;">
        <iframe src="<%=path%>/supplier/listbottom.do" frameborder="0" name="mainFrame" id="mainFrame">
        </iframe>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#search").bind('click', function() {
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $("#vcode").val(stripscript($("#vcode").val()));
                $("#vname").val(stripscript($("#vname").val()));
                $("#vinit").val(stripscript($("#vinit").val()));
                $('.search-div').hide();
                $('#listForm').submit();
            });
            /* 模糊查询清空 */
            $("#resetSearch").bind('click', function() {
                $("#vcode").val("");
                $("#enablestate").val("0");
                $("#vname").val("");
                $("#vinit").val("");
            });
//             $('.toolbar').toolbar({
            $('.tool').toolbar({
                items: [{
                    text: '<fmt:message key="select"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px', '-40px']
                    },
                    handler: function () {
                        $('.search-div').slideToggle(100);
                        $("#vcode").focus();
                    }
                },{
                    text: '<fmt:message key="insert"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        add();
                    }
                },{
                    text: '<fmt:message key="update"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        update();
                    }
                },{
                    text: '<fmt:message key="delete"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        del();
                    }
                },{
                    text: '<fmt:message key="relevance_supplier"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'link')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        relevanceSupplier();
                    }
                },{
                    text: '<fmt:message key="Relation_mallmaterial"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'view')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        relevanceMaterial();
                    }
                },{
                    text: '<fmt:message key="enable"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'enable')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        uEna(2);
                    },
                    items:[{
                        text: '<fmt:message key="disabled"/>',
                        useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'disable')},
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['0px','-40px']
                        },
                        handler: function(){
                            uEna(3);
                        }
                    }]
                },{
                    text: '<fmt:message key="export"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                    	exportSupplier();
                    }
                },{
                    text: '<fmt:message key="print"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                    	var form = $("#listForm");
						var oldAction = form.attr("action");
						form.attr('target','report');
						window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
						var action="<%=path%>/supplier/printSupplier.do";
		                $("#vcode").val(stripscript($("#vcode").val()));
		                $("#vname").val(stripscript($("#vname").val()));
		                $("#vinit").val(stripscript($("#vinit").val()));
						form.attr('action',action);
						form.submit();
						form.removeAttr("target");
						form.attr('action',oldAction);
						$('#wait2,#wait').css("visibility","hidden");
                    }
                },{
                    text: '<fmt:message key="quit"/>',
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
                    }
                }]
            });
            setElementHeight('.grid',['.toolbar','.mainFrame'],$(document.body),15);	//计算.grid的高度
            setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            $(".page").css("margin-bottom",$(".mainFrame").height()-15);
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
                $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
                var  url="<%=path%>/supplier/listbottom.do?delivercode="+$(this).find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            });
            $('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
                event.stopPropagation();
            });
            if($(".table-body").find("tr").size()>0){
                var  url="<%=path%>/supplier/listbottom.do?delivercode="+$(".table-body").find("tr:eq(0)").find(":checkbox").val();
                $('#mainFrame').attr('src',encodeURI(url));
            }
            /*列表双击事件*/
            $('.grid').find('.table-body').find('tr').dblclick(function(event){
                update();
            });
            var trrows = $(".table-body").find('tr');
            if(trrows.length!=0){
                $(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
            }
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
        });
        //====================================================
        var add=function(){
            $('body').window({
                title: '<fmt:message key="suppliers"/><fmt:message key="insert"/>',
                content: '<iframe id="skipSupplierFrame" frameborder="0" src="<%=path%>/supplier/skip.do?moduleId=${moduleId}"></iframe>',
                width: '700px',
                height: '500px',
                draggable: true,
                isModal: true,
                topBar: {
                    items: [{
                        text: '<fmt:message key="save"/>',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-80px','-0px']
                        },
                        handler: function(){
                            getFrame('skipSupplierFrame').insertSupplier();
                        }
                    },{
                        text: '<fmt:message key="cancel" /> ',
                        icon: {
                            url: '<%=path%>/image/Button/op_owner.gif',
                            positn: ['-160px','-100px']
                        },
                        handler: function(){
                            $('.close').click();
                        }
                    }
                    ]
                }
            });
        };
        var update=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var action='<%=path%>/supplier/skip.do?delivercode='+checkboxList.filter(':checked').val()+'&moduleId=${moduleId}';
                $('body').window({
                    title: '<fmt:message key="suppliers"/><fmt:message key="update"/>',
                    content: '<iframe id="skipSupplierFrame" frameborder="0" src="'+action+'"></iframe>',
                    width: '700px',
                    height: '500px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [
                            {
                                text: '<fmt:message key="save"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-80px', '-0px']
                                },
                                handler: function () {
                                    getFrame('skipSupplierFrame').updateSupplier();
                                }
                            },
                            {
                                text: '<fmt:message key="cancel"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-160px', '-100px']
                                },
                                handler: function () {
                                    $('.close').click();
                                }
                            }
                        ]
                    }
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        var del=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size() > 0){
                alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
                    var codeValue=[];
                    checkboxList.filter(':checked').each(function(){
                        codeValue.push($(this).val());
                    });
                    var action = '<%=path%>/supplier/delete.do?ids='+codeValue.join(",");
                    $('body').window({
                        title: '<fmt:message key="data"/><fmt:message key="delete"/>',
                        content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                        width: 250,
                        height: 150,
                        draggable: true,
                        isModal: true
                    });
                });
            }else{
                alerttips('<fmt:message key="please_select_information_you_need_to_delete"/>！');
                return ;
            }
        };
        var uEna=function(et){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size() > 0){
                var codeValue=[];
                checkboxList.filter(':checked').each(function(){
                    codeValue.push($(this).val());
                });
                if(et=="3"){
                	var result = true;
					$.ajaxSetup({async:false});
					$.post("<%=path%>/supplier/checkSupplierQuote.do",{delivercode:codeValue.join(",")},function(data){
						if(data=="false")
							result = false;
					});
					if(result){ //如果有被引用
						alerterror('<fmt:message key="refencenotdisable"/>');
						return false;
					}
                }
                var action = '<%=path%>/supplier/uEnablestate.do?enablestate='+et+'&ids='+codeValue.join(",");
                $('body').window({
                    title: '<fmt:message key="status"/><fmt:message key="update"/>',
                    content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                    width: '200px',
                    height: '200px',
                    draggable: true,
                    isModal: true
                });
            }else{
                alerttips('<fmt:message key="please_select_at_least_one_data"/>！');
                return ;
            }
        };
        //导出供应商
        function exportSupplier(){
            $("#wait2").val('NO');//不用等待加载
            $("#vcode").val(stripscript($("#vcode").val()));
            $("#vname").val(stripscript($("#vname").val()));
            $("#vinit").val(stripscript($("#vinit").val()));
            $("#listForm").attr("action","<%=path%>/supplier/exportExcel.do");
            $("#listForm").submit();
	        $("#wait2 span").html("数据导出中，请稍后...");
	        $('#listForm').attr("action","<%=path%>/supplier/list.do");
	        $("#wait2").val('');//等待加载还原
        }
        var relevanceSupplier=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var jmu= $.trim($('#grid').find('.table-body').find(':checkbox').filter(':checked').parents("tr").find("td:eq(4)").find("span").text());
                var supplierName= $.trim($('#grid').find('.table-body').find(':checkbox').filter(':checked').parents("tr").find("td:eq(3)").find("span").text());
                if(jmu!=""){
                    alertconfirm('<fmt:message key="relevance_supplier_is_have"/>，<fmt:message key="update_jmu_supplier_alert"/>',function(){
                        selectjmuSupplier({
                            basePath:'<%=path%>',
                            title:"123",
                            height:400,
                            width:600,
                            callBack:'setSupplier',
                            domId:'delivercode',
                            single:true,
                            supplierName:supplierName
                        });
                    });
                }else{
                    selectjmuSupplier({
                        basePath:'<%=path%>',
                        title:"123",
                        height:400,
                        width:600,
                        callBack:'setSupplier',
                        domId:'delivercode',
                        single:true,
                        supplierName:supplierName
                    });
                }
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        var setSupplier=function(data){
            data.delivercodejum = $.trim(data.entity[0].code);
            data.vcodejmu = $.trim(data.entity[0].code);
            data.vnamejmu = $.trim(data.entity[0].name);
            data.vaddr = $.trim(data.entity[0].companyAddress);
            data.vtele = $.trim(data.entity[0].companyPhone);
            data.vcontact = $.trim(data.entity[0].companyContact);
            data.vmailaddr = $.trim(data.entity[0].companyFax);
            data.vwebsite = $.trim(data.entity[0].companyUrl);
            data.vfax = $.trim(data.entity[0].companyEmail);
            data.delivercode=$('#grid').find('.table-body').find(':checkbox').filter(':checked').val();
            $.post('<%=path%>/supplier/updatajmu.do',data, function (msg) {
                if (msg == 'ok') {
                    alerttipsbreak('<fmt:message key="update_successful"/>！',function(){
                    		$("#listForm").submit();
                        	$(".close").click();
                    });
                } else {
                    alerterror('<fmt:message key="update_fail"/>');
                }
            });
        };
        var relevanceMaterial=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var jmu= $.trim($('#grid').find('.table-body').find(':checkbox').filter(':checked').parents("tr").find("td:eq(4)").find("span").text());
                if(jmu==""){
                    alerttips('<fmt:message key="Please_link_mall_supplier_again"/>');
                    return;
                }
                var action='<%=path%>/supplier/getMaterial.do?delivercode='+checkboxList.filter(':checked').val();
                $('body').window({
                    title: '关联商城物资',
                    content: '<iframe id="materialSupplierFrame" frameborder="0" src="'+action+'"></iframe>',
                    width: '700px',
                    height: '500px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [
                            {
                                text: '<fmt:message key="save"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-80px', '-0px']
                                },
                                handler: function () {
                                    getFrame('materialSupplierFrame').updateMaterial();
                                }
                            },
                            {
                                text: '<fmt:message key="cancel"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-160px', '-100px']
                                },
                                handler: function () {
                                    $('.close').click();
                                }
                            }
                        ]
                    }
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        //回调方法-调用弹出窗口的物资新增方法
        function functions(chkCode,childFrameId){
        	document.getElementById(childFrameId).contentWindow.setMaterial(chkCode);
        }
        //供应商类别选择
        $("#suppliertypebutton").click(function(){
        	chooseSupplierTypeTree({
				basePath:'<%=path%>',
				title:'选择供应商类别',
				height:500,
				width:430,
				callBack:'setSupplierType',
				pk_check:$("#delivercodetype").val()
			});
        });
        //回调函数
        function setSupplierType(chkValue,textValue){
				$("#delivercodetype").val(chkValue);        	
				$("#suppliertypename").val(textValue);        	
        }
    </script>
</body>
</html>
