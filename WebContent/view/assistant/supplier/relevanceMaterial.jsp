<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
  String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
  <title>供应商列表</title>
  <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
  <style type="text/css">
    .grid td span{
      padding:0px;
    }
    .bj_head{
    	height:30px;
    }
  </style>
</head>
<body>
<div class="bj_head">
  <input type="hidden" id="pk_material"/>
  <input type="hidden" value="${supplier.delivercode}" id="delivercode"/>
  <input type="hidden" value="${supplier.delivercodejum}" id="delivercodejum"/>
  <input type="hidden" value="${supplier.vnamejmu}" id="supplierjum_name"/>
  <div style="float:left;margin-top:3px;">
           <input type="button" onclick="malSelect()" value="<fmt:message key="add"/>"/>
           <input type="button" onclick="del('material-body')" value="<fmt:message key="delete"/>"/>
           <input type="text" id="searchmname" value="请输入物资名称" onfocus="clearsearchmname(this)"/> <input type="button" onclick="searchmaterial()" value="检索"/>
           <input type="button" id="insertmallmaterial" name="insertmallmaterial" value="关联商城物资"/>
   </div>
</div>
  <div class="grid" id="grid">
    <div class="table-head" >
      <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
          <td>
              <span style="width: 40px;">
                  <input type="checkbox" id="chkAll2"/>
              </span>
          </td>
          <td><span style="width: 70px;"><fmt:message key="supplies_code"/></span></td>
          <td><span style="width: 100px;"><fmt:message key="supplies_name"/></span></td>
          <td><span style="width: 70px;"><fmt:message key="specification"/></span></td>
          <td><span style="width: 80px;"><fmt:message key="suppliers"/><fmt:message key="price"/></span></td>
          <td><span style="width: 70px;"><fmt:message key="unit"/></span></td>
            <td><span style="width: 90px;"><fmt:message key="Mall_goods_code"/></span></td>
            <td><span style="width: 200px;"><fmt:message key="Mall_goods_name"/></span></td>
            <td><span style="width: 50px;"></span></td>
        </tr>
        </thead>
      </table>
    </div>
    <div class="table-body">
      <table cellpadding="0" cellspacing="0">
        <tbody id="material-body">
        <c:forEach items="${materiallist}" var="materialScope">
          <tr>
            <td>
                <span style="width: 40px;text-align: center;">
                    <input type="checkbox"  name="idList2" id="chk_<c:out value='${materialScope.pk_material}'/>" value="<c:out value='${materialScope.pk_material}'/>"/>
                </span>
            </td>
            <td><span style="width: 70px;" title="${materialScope.vcode}">${materialScope.vcode}</span></td>
            <td><span style="width: 100px;" title="${materialScope.vname}">${materialScope.vname}</span></td>
            <td><span style="width: 70px;" title="${materialScope.vstandard}">${materialScope.vstandard}</span></td>
            <td><span style="width: 80px;" title="${materialScope.nprice}"><input type="text" maxlength="13" style="width: 68px;" value="${materialScope.nprice}"></input></span></td>
            <td><span style="width: 70px;" title="${materialScope.pk_unit.vname}"><input type="hidden" value="${materialScope.pk_unit.pk_unit }"/>${materialScope.pk_unit.vname}</span></td>
            <td><span style="width: 90px;" title="${materialScope.materialMall.vjmucode}">${materialScope.materialMall.vjmucode}</span></td>
            <td><span style="width: 200px;" title="${materialScope.materialMall.vjmuname}">${materialScope.materialMall.vjmuname}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vhstore}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vjmustorename}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.nprice}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vjmusupplierpk}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vjmudelivercode}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vjmudelivername}</span></td>
            <td style="display: none;"><span style="width: 70px;">${materialScope.materialMall.vhspec}</span></td>
            <td><span style="width: 50px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></span></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/validate.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
    <script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/codeCommon.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var setWidth=function(id){
      var $grid=$(id);
      var headWidth=$grid.find(".table-head").find("tr").width();
      var gridWidth=$grid.width();
      if(headWidth>=gridWidth){
        $grid.find(".table-body").width(headWidth+25);
        $grid.find(".table-head").width(headWidth+25);
      }else{
        $grid.find(".table-body").width(gridWidth);
        $grid.find(".table-head").width(gridWidth);
      }
    };
    setElementHeight('.grid',['.bj_head'],$(document.body),5);	//计算.grid的高度
    setElementHeight('.table-body',['.table-head'],'.grid'); //计算.table-body的高度
    loadGrid();//  自动计算滚动条的js方法
    setWidth(".grid");
    $('.grid').find('.table-body').find('tr').hover(
            function(){
              $(this).addClass('tr-over');
            },
            function(){
              $(this).removeClass('tr-over');
            }
    );
    //如果全选按钮选中的话，table背景变色
    $("#chkAll").click(function() {
      if (!!$("#chkAll").attr("checked")) {
        $('.grid').find('.table-body').find('tr').addClass("bgBlue");
      }else{
        $('.grid').find('.table-body').find('tr').removeClass("bgBlue");
      }
    });
    //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
    $('.grid').find('.table-body').find('tr').live("click", function () {
      if ($(this).hasClass("bgBlue")) {
        $(this).removeClass("bgBlue").find(":checkbox").attr("checked", false);
      }
      else
      {
        $(this).addClass("bgBlue").find(":checkbox").attr("checked", true);
      }
    });
  });
  $('#insertmallmaterial').click(function(){
    var supplierjum_name = $('#supplierjum_name').val();
    if(supplierjum_name != ""){
      var checkboxList = $('.grid').find('.table-body').find(':checkbox');
      if(checkboxList&& checkboxList.filter(':checked').size() == 1){
        var name = '';
        var pk_material = '';
        checkboxList.filter(':checked').each(function(){
          var row = $(this).closest('tr');
          name = $.trim(row.children('td:eq(2)').text());
          pk_material = $.trim($(this).val());
        });
//         if(checkMaterialmall($('#delivercodejum').val(),pk_material)){
          selectJmuMaterialV2({
            basePath:'<%=path%>',
            title:"123",
            height:400,
            width:600,
            callBack:'setJmuMaterial',
            domId:'delivercode',
            vname:name,
            delivername:supplierjum_name,
            single:true
          });
//         }else{
//           alerterror('物资已经关联当前供应商的物资，不可重复关联！');
//           return;
//         }
      }else{
        alerterror('请选择一条数据！');
        return;
      }
    }else{
      alerterror('请先关联商城供应商！');
      return;
    }
  });
  //校验是否重复关联
  function checkMaterialmall(delivercodejum,pk_material){
    var result = true;
    $.ajaxSetup({async:false});
    $.post("<%=path %>/material/checkMaterialmall.do",{delivercodejum:delivercodejum,pk_material:pk_material},function(data){
      if(!data)result = false;
    });
    return result;
  }
  //将商城的数据填充到表格中
  function setJmuMaterial(data){
  	var flag = false,materialName="";
	//检测一个商城物资是否重复关联本地物资
	$('#grid').find('.table-body').find('tr').each(function(){
		if($.trim($(this).find("td").eq(6).text())==data.entity[0].id){
			flag = true;
			materialName = $.trim($(this).find("td").eq(2).text());
			return false;
		}
	});
	if(flag){
		alerterror('物资-'+materialName+'已经关联商城物资-'+data.entity[0].goodsname+',不能重复关联。');
		return;
	}
    var checkboxList = $('.grid').find('.table-body').find(':checkbox');
    checkboxList.filter(':checked').each(function(){
      var row = $(this).closest('tr');
      row.children('td:eq(6)').find("span").text(data.entity[0].id);//商城物资编码
      row.children('td:eq(7)').find("span").text(data.entity[0].goodsname);//商城物资名字
      row.children('td:eq(8)').text(data.entity[0].storeid).css("display","none");//商城店铺编码
      row.children('td:eq(9)').text(data.entity[0].storename).css("display","none");//商城店铺名称
      row.children('td:eq(10)').text(data.entity[0].price).css("display","none");//报价
      row.children('td:eq(11)').text(data.entity[0].sellerid).css("display","none");//商城供应商主键
      row.children('td:eq(12)').text(data.entity[0].sellerid).css("display","none");//商城供应商编码
      row.children('td:eq(13)').text(data.entity[0].sellername).css("display","none");//商城供应商名称
      row.children('td:eq(14)').text(data.entity[0].id).css("display","none");//商城物资规格主键
    });
  }
  var del=function(id){
    if($("#"+id).find("input[checked='checked']").size()<=0){
      alerterror("<fmt:message key="please_select_at_least_one_data"/>");
      return;
    }
    $("#"+id).find("input[checked='checked']").each(function(){
      $(this).parents("tr").remove();
    });
  };
  //==========================物资选择=======================================
  var malSelect=function(){
  	top.selectMaterialByPage({
          basePath:'<%=path%>',
          width:1000,
          height:600,
          positn : {
  			type : 'absolute',
  			top: $(topWin).height()*0.5-300,
  			left : $(topWin).width()*0.5-500
  		},
          callBack:'setMaterial',
          domId:'pk_material',
          irateflag:1,//取采购单位
          single:false,
          frameId:'iframe_d2fd8bd5459a4a598ded902800bc9dd5',
          childFrameId:'materialSupplierFrame'
      });
  };
  function setMaterial(data){
      var pk=[];
      var pks = data.toString();
      $("#material-body").find("tr").each(function(){
          var value=$.trim($(this).find("td:eq(0)").find("input").val());
          if(pks.indexOf(value)>=0){
          	pks=pks.replace(new RegExp(value,"gm"),"");
          }
      });
      //将空值去掉
      var lis = pks.split(",");
      for(var i in lis){
      	if(lis[i]!=""){
      		pk.push(lis[i]);
      	}
      }
      if(pk.length>0){
	      //查询选择的物资
	      $.ajaxSetup({
				  async: false 
			});
			$.post("<%=path%>/material/queryMaterialByPk.do?irateflag=1&pk_material="+pk.toString(),data,function(data){
				var rs = data;
				for(var i in data){
	              $("#material-body").append('<tr>'+
	                      '<td>'+
	                      '<span style="width: 40px;text-align: center;">'+
	                      '<input type="checkbox"  name="idList2" id="chk_'+data[i].pk_material+'" value="'+data[i].pk_material+'"/>'+
	                      '</span>'+
	                      '</td>'+
	                      '<td><span style="width: 70px;" id="materialScopeVcode">'+data[i].vcode+'</span></td>'+
	                      '<td><span style="width: 100px;" id="materialScopeVname">'+data[i].vname+'</span></td>'+
	                      '<td><span style="width: 70px;" id="materialScopeVstandard">'+data[i].vspecfication+'</span></td>'+
	                      '<td><span style="width: 80px;" id="materialScopeNprice"><input maxlength="13" type="text" style="width: 63px" value="'+data[i].nsaleprice+'"/></span></td>'+
	                      '<td><span style="width: 70px;" id="materialScopePk_unit"><input type="hidden" value="'+data[i].pk_unit+'"/>'+data[i].unitvname+'</span></td>'+
	                      '<td><span style="width: 90px;"></span></td>'+
	                      '<td><span style="width: 200px;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display:none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td style="display: none;"><span style="width: 70px;display: none;"></span></td>'+
	                      '<td><span style="width: 50px;"><img src="../image/scm/move.gif" onclick="deleteRow(this)"/></span></td>'+
	                      '</tr>');
	              pk.push(data[i].pk_material);
				}
			});	
	    $("#pk_material").val(pk.join(","));
      }
  }
  var isRight;
  var dh=function() {
    isRight=true;
    var data = {};
    data.delivercode=$("#delivercode").val();
    data.delivercodejum=$("#delivercodejum").val();
    $("#material-body").find("tr").each(function(i){
      data["materialScope["+i+"].vcode"]=$(this).find("td:eq(1)").find("span").text();
      data["materialScope["+i+"].vname"]=$.trim($(this).find("td:eq(2)").find("span").text());
      data["materialScope["+i+"].pk_unit.pk_unit"]=$(this).find("td:eq(5)").find("input").val();
      data["materialScope["+i+"].vstandard"]=$(this).find("td:eq(3)").find("span").text();
      data["materialScope["+i+"].pk_material"]=$(this).find("td:eq(0)").find("input").val();
      var nprice=$.trim($(this).find("td:eq(4)").find("input").val());
      if(nprice!=""&&Number(nprice)>=0){
        if(nprice.indexOf(".")==-1||nprice.indexOf(".")>10){
          if(nprice.length>10) {
            alerterror('<fmt:message key="maximum_length"/>10!');
            isRight=false;
          }
        }
      }else{
        alerterror('<fmt:message key="price_must_be_greater_than"/>');
        isRight=false;
      }
      data["materialScope["+i+"].nprice"]= nprice;
    });
    //**********************关联商城物资********************************************************************
    $("#material-body").find("tr").each(function(i){
      if($.trim($(this).find("td:eq(14)").text()) != "" && $.trim($(this).find("td:eq(14)").text()) != null){
        data["listMaterialMall["+i+"].pk_material"]=$(this).find("td:eq(0)").find("input").val();//物资主键
        data["listMaterialMall["+i+"].vjmuname"]=$(this).find("td:eq(7)").text();//商城物资名称
        data["listMaterialMall["+i+"].vjmucode"]=$(this).find("td:eq(6)").text();//商城物资编码
        data["listMaterialMall["+i+"].vhstore"]=$(this).find("td:eq(8)").text();//店铺主键
        data["listMaterialMall["+i+"].vjmustorename"]= $.trim($(this).find("td:eq(9)").text());//店铺名称
        data["listMaterialMall["+i+"].nprice"]= Number($.trim($(this).find("td:eq(10)").text()));//报价
        data["listMaterialMall["+i+"].vjmusupplierpk"]= $.trim($(this).find("td:eq(11)").text());//商城供应商主键
        data["listMaterialMall["+i+"].vjmudelivercode"]= $.trim($(this).find("td:eq(12)").text());//商城供应商编码
        data["listMaterialMall["+i+"].vjmudelivername"]= $.trim($(this).find("td:eq(13)").text());//商城供应商名称
        data["listMaterialMall["+i+"].vhspec"]=$.trim($(this).find("td:eq(14)").text());//规格主键
      }
    });
    //**********************关联商城物资********************************************************************
    return data;
  };
  var mark=true;
  var updateMaterial=function(){
      var data=dh();
      if(mark&&isRight) {
        $.post('<%=path%>/supplier/updataMaterial.do',data, function (msg) {
          if (msg == 'ok') {
            alerttipsbreak('<fmt:message key="save_successful"/>！',function(){
            	parent.$("#listForm").submit();
            });
          } else {
            alerterror(msg);
          }
        });
        mark=false;
        setTimeout(function(){mark=true;},5000);
      }
  };
  var reload=function(){
    if(typeof(eval(parent.pageReload))=='function') {
      parent.pageReload();
    }else {
      parent.location.href = parent.location.href;
    }
    $(".close",parent.document).click();
  };
  
  function clearsearchmname(e){
  	$(e).val('');
  }
  //检索物资
  function searchmaterial(){
  	var vmname=$("#searchmname").val();
  	var scrollheight=0;
  	var index = 0;
  	if(vmname==''){
  		return;
  	}
  	$("#material-body").find("tr").each(function(){
  		$(this).find("td:eq(2)").find("span").css('color','black');
          index++;	
  	      var materialname=$.trim($(this).find("td:eq(2)").find("span").text());
          if(materialname.indexOf(vmname)!=-1){
          	if(scrollheight==0){
          		scrollheight=index*32;
          	}
          	$(this).find("td:eq(2)").find("span").css('color','red');
          }
      });
  	$("#grid1").find('.table-body').animate({scrollTop:scrollheight-150},1000);//
  }
  function deleteRow(obj){
	$(obj).closest("tr").remove();
};
</script>
</body>
</html>
