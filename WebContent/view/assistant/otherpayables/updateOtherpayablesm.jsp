<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<form action="<%=path%>/otherpayables/updateOtherPayablesm.do" method="post">
			<input type="hidden" name="pk_otherpayables" id="pk_otherpayables" value="${otherpayablesm.pk_otherpayables }"/>
			<input type="hidden" name="istate" id="istate" value="${otherpayablesm.istate }"/>
			<div class="bj_head" style="height: 75px;">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="document_no" />:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="vbillno" id="vbillno" class="text" value="${otherpayablesm.vbillno}" readonly="readonly"/>
					</div>
					<div class="form-label"><fmt:message key="document_date" />:</div>
					<div class="form-input" >
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate" value="${otherpayablesm.dbilldate}" class="Wdate text"/>
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label" ><span style="color:red;">*</span><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="delivercode" id="delivercode" class="text" value="${otherpayablesm.delivercode }"/>
						<input type="text" name="delivername" style="margin-bottom: 6px;" id="delivername" class="text" value="${otherpayablesm.delivername }"  readonly="readonly"/>
						<img id="supplierbutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input" >
						<input type="text" name="vmemo" id="vmemo" class="text" value="${otherpayablesm.vmemo }" style="width: 413px;" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width:100px;"><fmt:message key="payables" /><fmt:message key="type" /></span></td>
                                    <td><span style="width:150px;"><fmt:message key="related" />入库单</span></td>
                                    <td><span style="width:100px;"><fmt:message key="amount" /></span></td>
                                    <td><span style="width:120px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="height: 296px;">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="otherpayablesd" items="${otherpayablesdlist}" varStatus="status">
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td style="background:#F1F1F1;">
											<select  class="select" style="width:110px;height:28px;">
												<c:forEach var="otherpay" items="${otherpaylist }">
													<option value="${otherpay.pk_otherpay }"
													<c:if test="${otherpay.pk_otherpay==otherpayablesd.pk_otherpay}">selected=selected</c:if>
													>${otherpay.vname}</option>
												</c:forEach>
											</select>
											</td>
										<td style="background:#F1F1F1;"><span style="width:150px;" vbillno="${otherpayablesd.vinspectno}"  pk_inspect="${otherpayablesd.pk_inspect}"><c:out value="${otherpayablesd.vinspectno}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:100px;text-align: right;"><c:out value="${otherpayablesd.nmoney}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:120px;"><c:out value="${otherpayablesd.vmemo}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable_otherpay.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
        $("#dbilldate").focus();
		var otherpaytypeurl = '<%=path%>/otherpay/findOtherpayList.do';
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
			var selectedrow;
			function setSupplier(data){
				if($("#delivercode").val()!=data.delivercode){//之前的供应商和这次选择的不一致
					$(".table-body table").find('tr').each(function(){
						$(this).find("td:eq(2) span").text('');
						$(this).find("td:eq(2) span").attr('vbillno','');
						$(this).find("td:eq(2) span").attr('pk_inspect','');
					});
				}
				$("#delivercode").val(data.delivercode);
				$("#delivername").val(data.delivername);
			}
			$(document).ready(function(){
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
				$(".table-body").height($(".grid").height()-$(".table-head").height()-20);
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()));
				
				editCells();
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:5,
					widths:[30,70,130,70,130],
					colStyle:['',{background:"#F1F1F1"},{background:"#F1F1F1"},{background:"#F1F1F1",'text-align':"right"},{background:"#F1F1F1"}],
					onEdit:$.noop,
					editable:[2,3,4],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos==2){
							var vbillno = postd.find('span').attr('vbillno');
							postd.find('span').text(vbillno);
						}
						if(pos == 3){
							var nmoney = $.trim(rownum.find("td:eq(3) span").text());
							if(nmoney==''){
								nmoney = $.trim(rownum.find("td:eq(3) input").val());
								rownum.find("td:eq(3) span").text(nmoney);
							}
							if(isNaN(nmoney)){
								rownum.find("td:eq(3) span").text(0);
								alerterror('<fmt:message key="price_be_not_number"/>！');
							}else if(Number(nmoney) == 0){
								rownum.find("td:eq(3) span").text(0);
								alerterror('<fmt:message key="amount_cannot_be_zero"/>！');
							}							
							return;
						}
						if(pos == 4){
							var memo = rownum.find("td:eq(4) input").val();
							postd.find('span').text(memo);
							return;
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if ($('#delivercode').val() == null || $('#delivercode').val() == '') {
								alerterror('<fmt:message key="please_select" /><fmt:message key="suppliers" />');
								return;
							}
							if(data.value==''){
								selectedrow = row;
								selectInOrder({
									basePath:'<%=path%>',
									title:"123",
									height:400,
									width:600,
									callBack:'setInspect',
									delivercode:$("#delivercode").val(),
									type:1,
									istate:1,
									single:true
								});
							}
							$.fn.autoGrid.setCellEditable(row,3);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivercode').val() == null || $('#delivercode').val() == '') {
								alerterror('<fmt:message key="please_select" /><fmt:message key="suppliers" />');
								return;
							}
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(2) span").attr('vbillno','');	
									row.find("td:eq(2) span").attr('pk_inspect','');	
									row.find("td:eq(2) input").focus();
									$("#mMenu").remove();
									return;	
								}
							}
							data['url'] = '<%=path%>/inspect/findInspectmTop.do?delivercode='+$("#delivercode").val();
							data['key'] = 'vbillno';
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.vbillno;
						},
						afterEnter:function(data2,row){
							row.find("td:eq(2) span").text(data2.vbillno);
							row.find("td:eq(2) span").attr('vbillno',data2.vbillno);
							row.find("td:eq(2) span").attr('pk_inspect',data2.pk_inspect);
							$.fn.autoGrid.setCellEditable(row,3);
						}
					},{
						index:3,
						action:function(row,data){
							if(Number(data.value) == 0){
								row.find("td:eq(5)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,3);
							}else if(isNaN(data.value)){
								row.find("td:eq(5)").find('span').text(data.ovalue);
								$.fn.autoGrid.setCellEditable(row,3);
							}
							$.fn.autoGrid.setCellEditable(row,4);
						},
						onCellEdit:function(event,data,row){
							if ($('#delivercode').val() == null || $('#delivercode').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								row.find('td:eq(7) span').text('0.00');
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								cnt = Number($.trim(cnt));
								var price = row.find("td:eq(6) span").text();
								if(isNaN($.trim(price))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									price = Number($.trim(price));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(7) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:4,
						action:function(row,data){
							row.find("td:eq(4) span").text(data.value);
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="remark" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function updateOtherpayablesm(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2 && pos!=4){
						$(this).find('input').trigger('onEnter');
					}
				});
				
				$("#vmemo").focus();
				var data = {};
				var i = 0;
				data["pk_otherpayables"] = $("#pk_otherpayables").val();
				data["istate"] = $("#istate").val();
				data["dbilldate"] = $.trim($("#dbilldate").val());
				data["vbillno"] = $.trim($("#vbillno").val());
				data["delivercode"] = $("#delivercode").val();
				data["delivername"] = $("#delivername").val();
				data["vmemo"] = $("#vmemo").val();
				
				if(data.vbillno==''){
					alerterror('<fmt:message key="please_enter" /><fmt:message key="document_no" />！');
					savelock = false;
					return;
				}
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var otherpay=$(rows[0]).find('td:eq(1)').find('select').val();
					if(otherpay==''){
						alerterror('请选择应付款类型！');
						savelock = false;
						return;
					}
				}
				var checkdata = true;
				var ntotalmoney=0;
				var index = 0;
				for(i=0;i<rows.length;i++){
					var pk_otherpay = $(rows[i]).find('td:eq(1)').find('select').val();
					var otherpayname = $(rows[i]).find('td:eq(1)').find('select').find('option:selected').text();
					if(pk_otherpay==''){
						continue;
					}
					var nmoney=$.trim($(rows[i]).find('td:eq(3)').text());
					if(nmoney=='' || isNaN(nmoney)){
						alerterror('<fmt:message key="the" />'+(i+1)+'<fmt:message key="line" /><fmt:message key="amount" /><fmt:message key="incorrect_format" />');
						checkdata = false;
						break;
					}
					
					data["otherpayablesdList["+index+"].pk_otherpay"] = pk_otherpay;
					data["otherpayablesdList["+index+"].votherpayname"] = otherpayname;
					data["otherpayablesdList["+index+"].pk_inspact"] = $.trim($(rows[i]).find('td:eq(2)').find('span').attr('pk_inspact'));
					data["otherpayablesdList["+index+"].vinspectno"] = $.trim($(rows[i]).find('td:eq(2)').find('span').attr('vbillno'));
					data["otherpayablesdList["+index+"].nmoney"] = $.trim($(rows[i]).find('td:eq(3)').find('span').text());
					data["otherpayablesdList["+index+"].vmemo"] = $.trim($(rows[i]).find('td:eq(4)').find('span').text());
					
					ntotalmoney = ntotalmoney+Number($.trim($(rows[i]).find('td:eq(3)').find('span').text()));
					index++;
				}
				data["ntotalmoney"] = ntotalmoney;
				if(!checkdata){
					savelock = false;
					return;	
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/otherpayables/updateOtherPayablesm.do",data,function(data){
					var rs = data;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").find('td:eq(1)').html('<span></span>');
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			
			function setInspect(pk_inspect,vbillno){
				selectedrow.find("td:eq(2) span").text(vbillno);
				selectedrow.find("td:eq(2) span").attr('vbillno',vbillno);
				selectedrow.find("td:eq(2) span").attr('pk_inspect',pk_inspect);
				$.fn.autoGrid.setCellEditable(selectedrow,3);
			}
			function resetName(e){
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==1 || curindex==2){//物资 供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname==undefined){
						$(e).closest('span').text('');
					}else{
						$(e).closest('span').text(vname);
					}
				}else if(curindex==3){
					var value = $(e).val();
					if(isNaN(value)){
						$(e).closest('span').text('');	
					}else{
						$(e).closest('span').text(value);
					}
				}
			}
		</script>
	</body>
</html>
