<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div style="height:50%;">
			<div class="tool"></div>
			<div>
				<input type="hidden" id="pk_otherpayables" name="pk_otherpayables" value="${pk_otherpayables}"/>
				<form id="listForm" action="<%=path%>/otherpayables/queryAllOtherPayablesm.do" method="post">
					<div class="search-div" style="positn: absolute;z-index:20">
						<div class="form-line">
							<div class="form-label"><fmt:message key="startdate"/></div>
							<div class="form-input">
								<input type="text" id="bdat" style="width: 90px;" name="bdat" class="Wdate text" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'}) " value="${otherPayablesm.bdat}" />
							</div>
							<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="suppliers" />:</div>
							<div class="form-input" style="width:100px;">
								<input type="hidden" name="delivercode" id="delivercode" class="text" value="${otherPayablesm.delivercode}"/>
								<input type="text" style="margin-bottom: 6px;" name="delivername" id="delivername" class="text" value="${otherPayablesm.delivername}"   readonly="readonly"/>
								<img id="supplierbutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
							</div>
						</div>
						<div class="form-line">
							<div class="form-label"><fmt:message key="enddate"/></div>
							<div class="form-input">
								<input type="text" id="edat" style="width: 90px;" name="edat" class="Wdate text" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})"  value="${otherPayablesm.edat}"/>
							</div>
						</div>
						<div class="search-commit">
				       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
						<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />'/>
						</div>
					</div>
					<div class="grid" id="grid">		
						<div class="table-head" style="width: 100%;">
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td class="num"><span style="width:30px;">&nbsp;</span></td>
										<td><span style="width:35px;"><input type="checkbox" id="chkAll"/></span></td>
										<td><span style="width:120px;"><fmt:message key="document_no" /></span></td>
										<td><span style="width:80px;"><fmt:message key="Document_status" /></span></td>
										<td><span style="width:150px;"><fmt:message key="scscgongyingshang" /></span></td>
										<td><span style="width:120px;"><fmt:message key="document_date" /></span></td>
										<td><span style="width:80px;"><fmt:message key="amount" /></span></td>
										<td><span style="width:150px;"><fmt:message key="remark" /></span></td>
			                     	</tr>
								</thead>
							</table>
						</div>				
						<div class="table-body" style="width: 100%;">
							<table cellspacing="0" cellpadding="0" id="tblGrid">
								<tbody>
									<c:forEach var="otherpayablesm" items="${listotherpayablesm}" varStatus="status">
										<tr>
											<td class="num"><span style="width:30px;">${status.index+1}</span></td>
											<td><span style="width:35px;text-align: center;"><input type="checkbox" name="idList" id="chk_${otherpayablesm.pk_otherpayables}" value="${otherpayablesm.pk_otherpayables}"/></span></td>
											<td><span style="width:120px;text-align: left;" title="${otherpayablesm.vbillno}">${otherpayablesm.vbillno}</span></td>
											<td><span style="width:80px;text-align: left;" istate="${otherpayablesm.istate}">
												<c:if test="${otherpayablesm.istate==1}"><fmt:message key="Uncommitted" /></c:if>
												<c:if test="${otherpayablesm.istate==2}"><fmt:message key="Unsettled" /></c:if>
												<c:if test="${otherpayablesm.istate==3}"><fmt:message key="Settled" /></c:if>
											</span></td>
											<td><span style="width:150px;text-align: left;">${otherpayablesm.delivername}</span></td>
											<td><span style="width:120px;text-align: left;">${otherpayablesm.dbilldate}</span></td>
											<td><span style="width:80px;text-align: right;">${otherpayablesm.ntotalmoney}</span></td>
											<td><span style="width:150px;text-align: left;">${otherpayablesm.vmemo}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>			
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
				</form>
			</div>
		</div>
		<form id="printForm" action="<%=path%>/otherpayables/printOtherPayables.do" method="post">
				<input type="hidden" id="printpk" name="pk_otherpayables" value="${pk_otherpayables}"/>
			</form>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/otherpayables/queryAllOtherPayablesd.do?pk_otherpayablesm=${pk_otherpayables}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			function setSupplier(data){
				$("#delivercode").val(data.delivercode);//主键
				$("#delivername").val(data.delivername);//名称
			}
			$(document).ready(function(){
				//快捷键操作（Alt+*）
				$(document).keydown(function (e) {
				    var doPrevent;
				    if (event.keyCode==78 && event.altKey) {//alt+N
				    	if(!$('#window_supply').html()){
				    		addotherpayablesm();
				    	}
				    }else if(event.keyCode==85 && event.altKey){//alt+U
				    	if(!$('#window_supply').html()){
				    		updateotherpayablesm();
				    	}
				    }else{
				        doPrevent = false;
				    }
				    if (doPrevent)
				        e.preventDefault();
				}); 
				
				if($("#bdat").val()=='' && $("#edat").val()==''){
					$("#bdat,#edat").htmlUtils("setDate","now");
				}
				
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$('.search-div').hide();
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),70);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				//分页
			 	$(".page").css("margin-bottom",$("body").height()*0.45+$(".page").height()+10);
				//刷新字表
				$('.grid .table-body tr').live('click',function(){
					 $('#pk_otherpayables').val($(this).find('td:eq(1)').find('input').val());
					 var pk_otherpayables=$(this).find('td:eq(1)').find('input').val();
					 $("#printpk").val(pk_otherpayables);
					 
					 var url="<%=path%>/otherpayables/queryAllOtherPayablesd.do?pk_otherpayablesm="+pk_otherpayables;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
			   //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:350,
						width:700,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
				$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
// 								$('#bdat').focus();
							}
						},{
							text: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								addotherpayablesm();
							}
						},{
							text: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								updateotherpayablesm();
							}
						},{
							text: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								deleteotherpayablesm();
							}
						},{
							text: '提交',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'upload')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								commitpayablesm();
							}
						},{
							text: '打印',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								printpayablesm();
							}
						},{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}]
				});
				
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});	
			//新增其他应付款
			function addotherpayablesm(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="other" /><fmt:message key="payables" />',
					content: '<iframe id="saveOtherpayablesm" frameborder="0" src="<%=path%>/otherpayables/toAddOtherPayablesm.do?sta=add"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveOtherpayablesm')&&window.document.getElementById("saveOtherpayablesm").contentWindow.validate._submitValidate()){
										window.document.getElementById("saveOtherpayablesm").contentWindow.saveOtherpayablesm();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//修改其他应付款
			function updateotherpayablesm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					
					var state = checkboxList.filter(':checked').closest('tr').find('td:eq(3)').find('span').attr('istate');
					if(state==2){
						alerterror('<fmt:message key="bill" />已提交无法修改！');
						return;
					}
					if(state==3){
						alerterror('<fmt:message key="bill" /><fmt:message key="Settled" />,<fmt:message key="dont_update" />');
						return;
					}
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="other" /><fmt:message key="payables" />',
						content: '<iframe id="updateOtherpayablesm" frameborder="0" src="<%=path%>/otherpayables/toUpdateOtherPayablesm.do?pk_otherpayables='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateOtherpayablesm')&&window.document.getElementById("updateOtherpayablesm").contentWindow.validate._submitValidate()){
											window.document.getElementById("updateOtherpayablesm").contentWindow.updateOtherpayablesm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除
			function deleteotherpayablesm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/otherpayables/deleteOtherPayablesm.do?pks='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete_systemcoding_information" />',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			
			//报货单提交
			function commitpayablesm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox:checked');
				var checkdata = true;
				if(checkboxList.length!=0){
					var chkValue = [];
					checkboxList.each(function(){
						var tr = $(this).closest('tr');
						var num = tr.find('td:first').text();
						if(tr.find('td:eq(3)').find('span').attr('istate')==2){
							checkdata = false;
							alerterror('第'+num+'条数据已提交！');
							return false;
						}else if(tr.find('td:eq(3)').find('span').attr('istate')==3){
							checkdata = false;
							alerterror('第'+num+'条数据已结算！');
							return false;
						}
						chkValue.push($(this).val());
					});
				}else{
					alerterror('未选择任何数据！');
					checkdata = false;
				}
				if(checkdata){
					var action = '<%=path%>/otherpayables/commitPayablesm.do?pks='+chkValue.join(",");
					$('body').window({
						title: '其他付款单提交',
						content: '<iframe frameborder="0" src='+action+'></iframe>',
						width: '500px',
						height: '245px',
						draggable: true,
						isModal: true
					});
				}
			}
			
			function printpayablesm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					$("#printForm").attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					$("#printForm").submit();
				}else{
					alerterror('请选择一条应付款！');
				}
			}
			
			function reloadPage(){
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
				$("#listForm").submit();
			}
		</script>
	</body>
</html>