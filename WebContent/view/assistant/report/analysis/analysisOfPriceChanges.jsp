<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>采购价格异动分析</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
	form .form-line .form-label{
		width: 80px;
	}
	form .form-line .form-input{
		width: 100px;
	}
	form .form-line .form-input input[type='text']{
		width: 100px;
	}
	form .form-line .form-input select{
		width: 100px;
	}
	</style>
	<%@ include file="../../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
	  	<div class="bj_head">
			<div class="form-line" style="z-index:5;">
                <div class="form-label"><fmt:message key="startdate"/>：</div>
                <div class="form-input">
                    <input type="text" id="bdat" readonly="readonly" name="bdat" value="${bdat}" class="Wdate text"  onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
				<div class="form-label" style="width: 140px;"><fmt:message key="supplies_category" />:</div>
				<div class="form-input" style="width:100px;">
					<input type="hidden" name="pk_materialtype" id="pk_materialtype" class="text"/>
					<input type="text" readonly="readonly" style="margin-bottom: 6px;" name="materialtypeName" id="materialtypeName" class="text"/>
					<img id="materialTypebutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				</div>
                <div class="form-label" style="width:114px;margin-left: 26px;"><fmt:message key="supplies" />:</div>
                <div class="form-input" style="width:100px;">
                    <input type="hidden" name="pk_material" id="pk_material" class="text"/>
                    <input type="text" readonly="readonly" style="margin-bottom: 6px;" name="materialName" id="materialName" class="text"/>
                    <img onclick="malSelect()" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
                </div>
			</div>
            <div class="form-line" style="z-index:5;">
                <div class="form-label"><fmt:message key="enddate"/>：</div>
                <div class="form-input">
                    <input type="text" id="edat" readonly="readonly" name="edat" value="${edat}" class="Wdate text"  onFocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})"/>
                </div>
                <div class="form-label" style="width: 140px;"><fmt:message key="Price_volatility_is_greater_than_this_month" />:</div>
                <div class="form-input" style="width:100px;">
                    <input type="text" name="big" id="big" maxlength="9" onkeyup="inputNum(this)" value="0" class="text"/>
                </div>
                <div class="form-label" style="width: 140px;"><fmt:message key="This_month_is_less_than_the_price_fluctuations" />:</div>
                <div class="form-input" style="width:100px;">
                    <input type="text" name="small" maxlength="9" id="small" onkeyup="inputNum(this)" value="0" class="text"/>
                </div>
                <div class="form-label" style="width:114px;margin-left: 26px;"><fmt:message key="New_materials_bought_this_month" />:</div>
                <div class="form-input" style="width:30px;">
                    <input type="checkbox" name="thisMonth"  value="1" id="thisMonth" class="text" style="margin-top: 3px;"/>&nbsp;
                </div>
            </div>
		</div>
		<input type="hidden" id="serviceName" name="serviceName" value="com.choice.assistant.service.report.AnalysisService"/>
		<input type="hidden" id="decimal" name="decimal" value="2" />
		<input type="hidden" id="numCols" name="numCols" value="inputPrice,inputNumber" />
	</form>
	<input type="hidden" id="reportName" value="analysisOfPriceChanges"/>
	<input type="hidden" id="excelUrl" value="<%=path%>/asstReport/exportReport.do"/>
	<input type="hidden" id="printUrl" value="<%=path%>/asstReport/printReport.do"/>
	<input type="hidden" id="dataUrl" value="<%=path%>/asstReport/findAnalysisOfPriceChanges.do"/>
	<%--<input type="hidden" id="title" value="采购价格异动分析"/>--%>
	<div id="datagrid"></div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认时间
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
			initTab(false);
			/*物资类别选择*/
			$('#materialTypebutton').click(function(){
                var action = '<%=path%>/asstReport/findMSelectTree.do?callBack=getTreeValue&ids='+$("#pk_materialtype").val();
                $('body').window({
                    title: '<fmt:message key="supplies_category"/>',
                    content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                    width: '450px',
                    height: '500px',
                    draggable: true,
                    isModal: true
                });
			});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp : true,//导出时从前台取列名
  	 			verifyFun:function(){
  	 				var bdat = $("#bdat").val();
  	 				var edat = $("#edat").val();
  	 				if(bdat=='' || edat==''){
  	 					alerterror('日期区间必须选择！');
  	 					return false;
  	 				}
	 				return true;
  	 			},
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/asstReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','print','option','exit']
  	 		});
  	 		
  	 		if(rebuild || grid.css('display') != 'none'){
 	  			firstLoad = true;//重置第一次加载标志
	  	 		builtTable({
	  	 			headUrl:"<%=path%>/asstReport/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 		  	hiddenCols:[{field:'CARDID'}],
	  	 			numCols:['inputPrice','inputNumber'],//右对齐
	  	 			decimalDigitF:2,//几位小数
	  	 			onDblClickRow:function(index,data){
//	  	 				alert("还没加");
// 	  	 				showInfo('inOutRecord','充值消费明细','/asstReport/toInOutRecord.do?cardId='+data['CARDID']);
	  	 			}
	  	 		});
 	  		}
  	 		//计算表格高度
  	 		$(".datagrid-wrap").css("height",document.body.clientHeight-5-$("#tool").height()-$("#queryForm").height());
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
        var malSelect=function(){
            selectSupplyLeftAsst({
                basePath:'<%=path%>',
                title:"123",
                height:420,
                width:650,
                callBack:'setMaterial',
                domId:'pk_material',
				irateflag:1,//取采购单位
                single:false
            });
        }
        function setMaterial(data) {
            var pk=[];
            var name=[]
            for(var i=0;data.entity.length>i;i++){
                name.push(data.entity[i].vname);
                pk.push(data.entity[i].pk_material);
            }
            $("#pk_material").val(pk.join(","));
            $("#materialName").val(name.join(","));
        }
        /*获取物资数据*/
         var getTreeValue=function(pk,name){
             $("#pk_materialtype").val(pk);
             $("#materialtypeName").val(name);
         }
        /*判定是否输入是数字，如果不是则删除输入的数据*/
         var inputNum=function(v){
            var value= $.trim($(v).val());
            if(!value.match("^([+]?)\\d*\\.?\\d+$")){
                if(value.length>0){
                    $(v).val(Number(value.substring(0,Number(value.length)-1)));
                }
            }else{
				$(v).val(Number(value));
			}
         }
  	 </script>
  </body>
</html>