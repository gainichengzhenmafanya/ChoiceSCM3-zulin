<%--
  合约价格趋势分析
  User: mc
  Date: 14-11-14
  Time: 下午1:31
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>合约价格趋势分析</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
        .leftMyFrame {
            background-color:#FFF;
            float:left;
            width:220px;
            height:100%;
        }
        .mainMyframe {
            background-color:#FFF;
            float:left;
            height:100%;
            border-left:solid 1px #B4B4B4;
            margin-left:-1px;
        }
	.datagrid-wrap{
		height:455px; 
	}
    </style>
</head>
<body>
<div class="leftMyFrame">
    <div style="height: 50%;">
        <div id="toolbar"></div>
        <div class="treePanel">
            <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
            <script type="text/javascript">
                var tree = new MzTreeView("tree");
                tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="supplies_category"/>; method:changeUrl("00000000000000000000000000000000","","模块","")';
                <c:forEach var="typeList" items="${materialType}" varStatus="status">
                        <c:if test="${typeList.enablestate==2}">
                        tree.nodes['${typeList.parentMaterialtype.pk_materialtype}_${typeList.pk_materialtype}'] = 'text:${lo:show(typeList.vname)}; method:changeUrl(\'${typeList.pk_materialtype}\',\'${typeList.vcode}\',\'${lo:show(typeList.vname)}\',\'${typeList.parentMaterialtype.pk_materialtype}\')';
                </c:if>
                </c:forEach>
                tree.setIconPath("<%=path%>/image/tree/none/");
                document.write(tree.toString());
            </script>
        </div>
    </div>
    <div id="supplierList" style="height: 50%;">
        <iframe src="<%=path%>/asstReport/findSupplier.do" frameborder="0" name="supplierFrame" id="supplierFrame"></iframe>
    </div>
</div>
<div class="mainMyframe">
    <div style="height: 50%;width: 100%;">
        <iframe src="<%=path%>/asstReport/findMaterial.do" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
    </div>
    <div style="height: 50%;text-align: center;" id="mainBottom">
        <div id="chartTool"></div>
        <div id="chartdiv" align="center"> FusionCharts. </div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/Charts/FusionCharts.js"></script>
<script type="text/javascript">
    var yeardate;
    var chartMatl;
    var chartSupplier;
    var chart = new FusionCharts("<%=path%>/Charts/Line.swf", "ChartId", "100%", ($("#mainBottom").height()-35), "0", "0");
    chart.setDataURL("<%=path%>/asstReport/findContractPriceTrendData.do");
    chart.render("chartdiv");
    function changeUrl(moduleId,moduleCode,moduleName,parentid){
        $('#pk_materialtype').val(moduleId);
        $('#vcode').val(moduleCode);
        $('#pk_father').val(parentid);
        window.mainFrame.location = "<%=path%>/asstReport/findMaterial.do?pk_materialtype="+moduleId;
    }
    function refreshTree(){
        window.location.href = '<%=path%>/asstReport/findContractPriceTrend.do';
    }
    var getSupplier=function(matl){
        var action='<%=path%>/asstReport/findSupplier.do?pk_material='+matl;
        $('#supplierFrame').attr('src',encodeURI(action));
    };
    var findChartData=function(matl,supplier,date){
        if(!(matl == null || matl == "" || supplier == null || supplier == "")){
            yeardate=date;
            chartMatl=matl;
            chartSupplier=supplier;
            var charturl='<%=path%>/asstReport/findContractPriceTrendData.do?material='+matl+'&supplier='+supplier+"&date="+date;
            chart.setDataURL(charturl);
        }
    };
    $(document).ready(function(){
        var toolbar = $('#toolbar').toolbar({
            items: [{
                text: '<fmt:message key="expandAll" />',
                title: '<fmt:message key="expandAll" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-160px','-80px']
                },
                handler: function(){
                    tree.expandAll();
                }
            },{
                text: '<fmt:message key="refresh" />',
                title: '<fmt:message key="refresh" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-60px','0px']
                },
                handler: function(){
                    refreshTree();
                }
            }
            ]
        });
        $('#chartTool').toolbar({
            items: [{
                text: '<fmt:message key="The_previous_year" />',
                title: '<fmt:message key="The_previous_year" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-160px','-80px']
                },
                handler: function(){
                    findChartData(chartMatl,chartSupplier,Number(yeardate)-1);
                }
            },{
                text: '<fmt:message key="Next_year" />',
                title: '<fmt:message key="Next_year" />',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['-60px','0px']
                },
                handler: function(){
                    findChartData(chartMatl,chartSupplier,Number(yeardate)+1);
                }
            }
            ]
        });

        setElementHeight('.treePanel',['#toolbar','#supplierList']);
        setElementHeight('#supplierList',['#toolbar','.treePanel'],$(document.body),6);
//         var h=$("#mainBottom").height()-$("#chartTool").height()-300;
        var w=$(document.body).width()-222;
        $(".mainMyframe").css("width",w);
    });
</script>
</body>
</html>
