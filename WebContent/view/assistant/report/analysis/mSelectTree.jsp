<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>供应物资多选树</title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/js/boh/omui/themes/apusic/operamasks-ui.min.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
			<style type="text/css">
					
				.datagrid-wrap{
					height:455px; 
				}
			</style>
		</head>
	<body>
		<div>
        	<div id="toolbar"></div>
	    	<div class="treePanel" id="treePanel">
	    	</div>
    	</div>
		<div id="wait2" style="visibility: hidden;"></div>
    	<div id="wait" style="visibility: hidden;">
			<img src="<%=path%>/image/loading_detail.gif" />&nbsp;
			<span style="color:white;font-size:15px;vertical-align: middle;"><fmt:message key="data_dealing_send" />,<fmt:message key="please_wait" />...</span>
		</div>

		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/department.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
		    var storeData=${materialType };
			$(document).ready(function(){
		     	 $("#treePanel").omTree({
		         	 dataSource : storeData,//数据源
		         	 showCheckbox:true,		//带有多选看
		         	 simpleDataModel:true	//数据模型
		      	});
		     	var checkTree = $('#treePanel').omTree('findNodes', "checked", 'true', "",true);
		     	if(checkTree){
		     		$.each(checkTree,function(i,leaf){
			     		$('#treePanel').omTree('check',leaf);
			     	});
		     	}
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-80px']
							},
							handler: function(){
								var chkValue=[]; 
								var textValue=[];
						    	var ss = getStoreTree();
						    	$.each(ss,function(key,val){
									chkValue.push(val.id);
                                    textValue.push(val.text);
								});
								parent['${callBack}'](chkValue,textValue);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-80px']
							},
							handler: function(){
								$('#treePanel').omTree('expandAll');
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','0px']
							},
							handler: function(){
								window.location.href=window.location.href;
							}
						},{
							text: '<fmt:message key="quit" />',
							title: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','0px']
							},
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});//end toolbar
				setElementHeight('.treePanel',['#toolbar'],$(document.body),27);//计算.treePanel的高度
			});
		    function refreshTree(){
				window.location.href = '<%=path%>/department/chooseDepartment.do';
	    	}
		    function getStoreTree(){
		    	var checkedNode=$('#treePanel').omTree('getChecked',true);
		    	var storeNode = [];
		    	debugger;
		    	storeNode = $.grep(checkedNode,function(val, key) {
 		    	    if(val.children==null) { //只获取最后一级节点的数据
                        return true;
                    }
		    	}, false);
				return storeNode;
		    }
		</script>

	</body>
</html>