<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.search-div .form-line .form-label{
					width: 10%;
				}
			.datagrid-wrap{
				height:455px; 
			}
		</style>
	</head>
	<body>
		<div>
            <input type="hidden" value="${pk_material}" id="pk_material"/>
            <div class="grid" id="gridsupplier">
                <div class="toolbar">
                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                        <thead>
                            <tr style="height: 25px;">
                                <td style="border:0px;"><span style="width: 80px;color:#000000;"><fmt:message key="suppliers_coding"/></span></td>
                                <td style="border:0px;border-left: 1px;"><span style="width: 80px;color:#000000;"><fmt:message key="suppliers_name"/></span></td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body">
                    <table cellspacing="0" cellpadding="0" style="width: 100%;">
                        <tbody>
                        <c:forEach items="${supplierList}" var="supplier" >
                            <tr>
                                <td>
                                    <input type="hidden" value="${supplier.delivercode}"/>
                                    <span style="width: 80px;" title="${supplier.vcode}">${supplier.vcode}</span>
                                </td>
                                <td style="border-right: 0px;"><span style="width: 80px;" title="${supplier.vname}">${supplier.vname}</span></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),0);	//计算.grid的高度
				setElementHeight('.table-body',['.toolbar'],'.grid');	//计算.table-body的高度
//				loadGrid();//  自动计算滚动条的js方法
//				changeTh();//拖动 改变table 中的td宽度
                var $grid=$(".grid");
                var headWidth=$grid.find(".toolbar").find("tr").width();
                var gridWidth=$grid.width();
                if(headWidth>=gridWidth){
                    $grid.find(".table-body").width(headWidth);
                    $grid.find(".table-head").width(headWidth);
                }else{
                    $grid.find(".table-body").width(gridWidth);
                    $grid.find(".table-head").width(gridWidth);
                }
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			});
			//------------------------------
			$('.grid').find('.table-body').find('tr').bind("click", function () {
                $('.grid').find('.table-body').find('tr').removeClass("tr-over");
                if($(this).hasClass("tr-over")) {
                    $(this).removeClass("tr-over");
                }else{
                    $(this).addClass("tr-over");
                }
                /*获取物料表的物料PK*/
                var supplier= $(this).find("td:eq(0)").find("input").val();
                var matl=$("#pk_material").val();
                parent.window.findChartData(matl,supplier,0);
			});
			//---------------------------
		</script>
	</body>
</html>