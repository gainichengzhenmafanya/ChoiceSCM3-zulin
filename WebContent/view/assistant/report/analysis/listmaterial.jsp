<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
        <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.search-div .form-line .form-label{
					width: 10%;
				}
			.datagrid-wrap{
				height:455px; 
			}
		</style>
	</head>
	<body>
		<div>
	    	<form id="queryForm" action="<%=path%>/asstReport/findMaterial.do" method="post">
                <div class="condition" id="storetool">
                    <div class="form-line" style="z-index:5;">
                        <div class="form-label" style="width: 70px;"><fmt:message key="coding"/>：</div>
                        <div class="form-input">
                            <input type="text" name="vcode" value="${material.vcode}" class="text"/>
                        </div>
                        <div class="form-label" style="width: 70px;"><fmt:message key="Mnemonic"/>：</div>
                        <div class="form-input">
                            <input type="text" name="vinit" value="${material.vinit}" class="text"/>
                        </div>
                        <div class="form-label" style="width: 70px;"><fmt:message key="name"/>：</div>
                        <div class="form-input">
                            <input type="text" name="vname" value="${material.vname}" class="text"/>
                        </div>
                        <div class="form-input" style="padding-top: 3px;">
                            <input type="submit" value="<fmt:message key='select'/>"/>
                        </div>
                    </div>
                </div>
				<div class="grid" class="grid" style="overflow: auto;">
		    	<input type="hidden" id="pk_materialtype" name="pk_materialtype" value="${pk_materialtype}"/>
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td><span style="width:22px;"></span></td>
									<td style="width:160px;"><fmt:message key="supplies_code" /></td>
									<td style="width:160px;"><fmt:message key="supplies_name" /></td>
									<td style="width:60px;"><fmt:message key="supplies_abbreviations" /></td>
									<td style="width:140px;"><fmt:message key="supplies_category" /></td>
									<td style="width:80px;"><fmt:message key="supplies_specifications" /></td>
									<td style="width:80px;"><fmt:message key="unit" /></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table id="tblGrid" cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="material" varStatus="step" items="${materialList}">
									<tr>
										<td class="num">
                                            <input type="hidden" value="${material.pk_material}"/>
                                            <span title="" style="width:21px;">${step.count}</span>
                                        </td>
										<td><span title="${material.vcode}" style="width:150px;text-align: left;">${material.vcode}</span></td>
										<td><span title="${material.vname}" style="width:150px;text-align: left;">${material.vname}</span></td>
										<td><span title="${material.vinit}" style="width:50px;text-align: left;">${material.vinit}</span></td>
										<td><span title="${material.materialtype.vname}" style="width:130px;text-align: left;">${material.materialtype.vname}</span></td>
										<td><span title="${material.vspecfication}" style="width:70px;text-align: left;">${material.vspecfication}</span></td>
										<td><span title="${material.unitvname}" style="width:70px;text-align: left;">${material.unitvname}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
        <script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				setElementHeight('.grid',['.condition'],$(document.body),38);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
                var $grid=$(".grid");
                var headWidth=$grid.find(".table-head").find("tr").width();
                var gridWidth=$grid.width();
                if(headWidth>=gridWidth){
                    $grid.find(".table-body").width(headWidth);
                    $grid.find(".table-head").width(headWidth);
                }else{
                    $grid.find(".table-body").width(gridWidth);
                    $grid.find(".table-head").width(gridWidth);
                }
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			});
			//判断当前选中的分类是不是最小类别
			function checkMaterialType(){
				var pk_materialtype = $('#pk_materialtype').val();
				var flag = false;
				$.ajax({
					type: "POST",
					url: "<%=path%>/material/checkMaterialType.do",
					data: "pk_materialtype="+pk_materialtype,
					async:false,
					dataType: "json",
					success:function(result){
						if(result > 0){
                            alerterror('<fmt:message key="First_select_a_final_category" />');
							flag = false;
						} else {
							flag = true;
						}
					}
				});
				return flag;
			}
			//------------------------------
			$('.grid').find('.table-body').find('tr').bind("click", function () {
                $('.grid').find('.table-body').find('tr').removeClass("tr-over");
                if($(this).hasClass("tr-over")) {
                    $(this).removeClass("tr-over");
                }else{
                    $(this).addClass("tr-over");
                }
                /*获取物料表的物料PK*/
                var matl= $(this).find("td:eq(0)").find("input").val();
                parent.window.getSupplier(matl);
			});
			//---------------------------
		</script>
	</body>
</html>