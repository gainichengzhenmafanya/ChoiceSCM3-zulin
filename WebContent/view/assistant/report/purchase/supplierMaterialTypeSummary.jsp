<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>供应商类别汇总</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
		form .form-line .form-label{
			width: 50px;
		}
		form .form-line .form-input{
			width: 120px;
		}
		form .form-line .form-input input[type='text']{
			width: 120px;
		}
		form .form-line .form-input select{
			width: 105px;
		}
	</style>
	<%@ include file="../../share/permission.jsp"%>
  </head>	
  <body>
   <div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
	  	<div class="bj_head">
			<div class="form-line" style="z-index:5;">
		                <div class="form-label" style="width:50px;"><fmt:message key="from"/>：</div>
		                <div class="form-input">
		                    <input type="text" id="bdat" name="bdat" value="${bdat}" class="Wdate text"  onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'})"/>
		                </div>
		                <div class="form-label" style="width:50px;"><fmt:message key="to2"/>：</div>
		                <div class="form-input">
		                    <input type="text" id="edat" name="edat" value="${edat}" class="Wdate text"  onFocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})"/>
		                </div>
		                <div class="form-label" ><fmt:message key="suppliers" />:</div>
		                <div class="form-input" style="width:100px;">
		                    <input type="hidden" name="delivercode" id="delivercode" class="text" value=""/>
		                    <input type="text" name="delivername" id="delivername" class="text" value=""/>
		                    <img id="supplierbutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
		                </div>
			</div>
		</div>
		<input type="hidden" name="serviceName" value="com.choice.assistant.service.report.PurchaseService"/>
	</form>
		<input type="hidden" id="reportName" value="supplierMaterialTypeSummary"/>
		<input type="hidden" id="excelUrl" value="<%=path%>/asstReport/exportReport.do"/>
		<input type="hidden" id="printUrl" value="<%=path%>/asstReport/printReport.do"/>
		<input type="hidden" id="dataUrl" value="<%=path%>/asstReport/findSupplierMaterialTypeSummary.do"/>
		<input type="hidden" id="decimal" value="2" />
		<input type="hidden" id="title" value="供应商物资类别汇总"/>
			<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 	//默认时间
			$("#bdat,#edat").htmlUtils("setDate","now");
			initTab(false);
			
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setSupplier',
					domId:'delivercode',
					single:true
				});
			});
			$('#positncodebutton').click(function(){
				if ($('#delivername').val() == null || $('#delivername').val() == '') {
                    alerterror('<fmt:message key="please_select_suppliers" />');
					return;
				}
				chooseDepartMentList({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setPk_org',
					domId:'positncode',
					type:1,
					single:true
				});
			});
  	 	});
  	 	function initTable(rebuild){
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title =$("#title").val();
  	 		var grid = $("#datagrid");
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp : true,//导出时从前台取列名
  	 			verifyFun:function(){
  	 				//验证查询条件 不满足return false;
	 				return true;
  	 			},
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			toolbar:['search','excel','print','exit']
  	 		});
  	 		if(rebuild || grid.css('display') != 'none'){
  	 			firstLoad = true;
				builtTable({
	  	 			headUrl:"<%=path%>/asstReport/findMaterialType.do",
	  	 			dataUrl:dataUrl,
	  	 			title:title,	
	  	 			grid:grid,
	  	 			decimalDigitF:$('#decimal').val(),//几位小数
	  	 			dateCols:dateCols,
	  	 			createHeader:function(data,head,frozenHead){
	  	 				var typearray = eval('('+data+')');
	  	 				var colFirst = [];
	  	 				var colSecond = [];
	  	 				
	  	 				frozenHead.push([{field:'VSUPPLIERCODE',width:120,rowspan:2,title:'<fmt:message key="suppliers_coding>"/>',align:'left'},{field:'VSUPPLIERNAME',width:120,rowspan:2,title:'<fmt:message key="suppliers_name>"/>',align:'left'}]);
	  	 				for(var i=0;i<typearray.length;i++){
	  	 					var parenttype = typearray[i];
	  	 					var childtypearray = parenttype.childMaterialtype;
	  	 					colFirst.push({colspan:childtypearray.length,title:parenttype[i].vname});
	  	 					for(var j=0;j<childtypearray.length;j++){
	  	 						colSecond.push({field:childtypearray[j].pk_materialtype.toUpperCase(),width:90,title:childtypearray[j].vname,align:'right'});
	  	 					}
	  	 				}
	  	 				
	  	 				colFirst.push({field:'TOTALAMT',width:100,rowspan:2,title:'<fmt:message key="total>"/>',align:'right'});
	  	 				head.push(colFirst);
	  	 				head.push(colSecond);
	  	 			}
	  	 		});
			}	
  	 		//计算表格高度
  	 		$(".datagrid-wrap").css("height",document.body.clientHeight-5-$("#tool").height()-$("#queryForm").height());
			
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  	 </script>
  </body>
</html>