<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<title>物资类别汇总</title>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<style type="text/css">
	form .form-line .form-label{
		width: 80px;
	}
	form .form-line .form-input{
		width: 100px;
	}
	form .form-line .form-input input[type='text']{
		width: 100px;
	}
	form .form-line .form-input select{
		width: 100px;
	}
	</style>
	<%@ include file="../../share/permission.jsp"%>
  </head>	
  <body>
  	<div id="tool"></div>
  	<form id="queryForm" name="queryForm" method="post">
	  	<div class="bj_head">
			<div class="form-line" style="z-index:5;">
				<div class="form-label"><fmt:message key="startdate"/></div>
				<div class="form-input">
					<input type="text" id="bdat" style="width: 90px;" name="bdat" class="Wdate text" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}',dateFmt:'yyyy-MM-dd'}) "/>
				</div>
				<div class="form-label"><fmt:message key="suppliers" />:</div>
				<div class="form-input" style="width:100px;">
					<input type="hidden" name="delivercode" id="delivercode" class="text" value=""/>
					<input type="text" readonly="readonly" name="delivername" id="delivername" class="text" value=""/>
					<img id="supplierbutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
				<div class="form-label" style="margin-left: 26px;"><fmt:message key="ORDR"/><fmt:message key="code"/></div>
				<div class="form-input">
					<input type="text" id="billno" name="billno" />
				</div>
			</div>
			<div class="form-line" style="z-index:5;">
				<div class="form-label"><fmt:message key="enddate"/></div>
				<div class="form-input">
					<input type="text" id="edat" style="width: 90px;" name="edat" class="Wdate text" onFocus="WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}',dateFmt:'yyyy-MM-dd'})" />
				</div>
				<div class="form-label"><fmt:message key="Theprocuringentity" />:</div>
				<div class="form-input" style="width:100px;">
					<input type="hidden" name="positncode" id="positncode" class="text" value=""/>
					<input type="text" readonly="readonly" name="positnname" id="positnname" class="text" value=""/>
					<img id="positncodebutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				</div>
			</div>
		</div>
		<input type="hidden" id="serviceName" name="serviceName" value="com.choice.assistant.service.report.PurchaseService"/>
	</form>
		<input type="hidden" id="reportName" value="materialTypeSummary"/>
		<input type="hidden" id="excelUrl" value="<%=path%>/asstReport/exportReport.do"/>
		<input type="hidden" id="printUrl" value="<%=path%>/asstReport/printReport.do"/>
		<input type="hidden" id="dataUrl" value="<%=path%>/asstReport/findMaterialTypeSummary.do"/>
		<input type="hidden" id="decimal" value="2" />
		<%--<input type="hidden" id="title" value="物资类别汇总"/>--%>
			<div id="datagrid"></div>
	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/util.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/plugins/jquery.datagrid.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/locale/easyui-lang-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 <script language="JavaScript" src="<%=path%>/Charts/FusionCharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
  	 <script type="text/javascript">
  	 	$(document).ready(function(){
  	 		//默认时间
  	 		$("#bdat,#edat").htmlUtils("setDate","now");
			initTab(false);
			
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setSupplier',
					domId:'delivercode',
                    single:false
				});
			});
			$('#positncodebutton').click(function(){
				chooseDepartMentList({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setPk_org',
					domId:'positncode',
					type:1,
                    single:false
				});
			});
  	 	});
  	 	
  		//初始化选中标签内容
  	 	function initTab(rebuild){
  	 		var excelUrl = $("#excelUrl").val();
  	 		var printUrl = $("#printUrl").val();
  	 		var reportName = $("#reportName").val();
  	 		var dataUrl = $("#dataUrl").val();
  	 		var title = $("#title").val();
  	 		var grid = $("#datagrid");
  	 		
  	 		builtToolBar({
  	 			basePath:"<%=path%>",
  	 			toolbarId:'tool',
  	 			formId:'queryForm',
  	 			grid:grid,
  	 			exportTyp : true,//导出时从前台取列名
  	 			verifyFun:function(){
  	 				//验证查询条件 不满足return false;
	 				return true;
  	 			},
  	 			excelUrl:excelUrl+'?reportName='+reportName,
  	 			printUrl:printUrl+'?reportName='+reportName,
  	 			colsChooseUrl:'<%=path%>/asstReport/toColumnsChoose.do?reportName='+reportName,
  	 			toolbar:['search','excel','option','exit']
  	 		});
  	 		
  	 		if(rebuild || grid.css('display') != 'none'){
 	  			firstLoad = true;//重置第一次加载标志
	  	 		builtTable({
	  	 			headUrl:"<%=path%>/asstReport/findHeaders.do?reportName="+reportName,
	  	 			dataUrl:dataUrl,
	  	 			title:title,
	  	 			grid:grid,
	  	 		  	hiddenCols:[{field:'CARDID'}],
	  	 			alignCols:['nnum','nprice','namt'],//右对齐
	  	 			numCols:['nnum','nprice','namt'],//右对齐
	  	 			decimalDigitF:$('#decimal').val(),//几位小数
	  	 			onDblClickRow:function(index,data){
//	  	 				alert("还没加");
// 	  	 				showInfo('inOutRecord','充值消费明细','/asstReport/toInOutRecord.do?cardId='+data['CARDID']);
	  	 			}
	  	 		});
 	  		}
  	 		//计算表格高度
  	 		$(".datagrid-wrap").css("height",document.body.clientHeight-5-$("#tool").height()-$("#queryForm").height());
  	 	}
  		//列选择后页面重新加载
  		function pageReload(){
  			closeColChooseWin();
  			initTab(true);
  		}
  		function showInfo(moduleId,moduleName,moduleUrl){
  	 		window.parent.tabMain.addItem([{
	  				id: 'tab_'+moduleId,
	  				text: moduleName,
	  				title: moduleName,
	  				closable: true,
	  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
	  			}
	  		]);
  	 		window.parent.tabMain.show('tab_'+moduleId);
  	 	}
  		function setSupplier(data){
            var pk=[];
            var name=[]
            for(var i=0;data.entity.length>i;i++){
                name.push(data.entity[i].vname);
                pk.push(data.entity[i].delivercode);
            }
            $("#delivercode").val(pk.join(","));
            $("#delivername").val(name.join(","));
		}
		function setPk_org(data){
            var pk=[];
            var name=[]
            for(var i=0;data.entity.length>i;i++){
                name.push(data.entity[i].vname);
                pk.push(data.entity[i].pk_id);
            }
            $("#positncode").val(pk.join(","));
            $("#positnname").val(name.join(","));
		}
  	 </script>
  </body>
</html>