<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<style type="text/css">
			.linebottom{
		    		positn: fixed; 
				bottom: 0px; 
		    	}
		    	.childgrid{
		    		width: 99.8%;
				border:solid 1px #8DB2E3;
				margin-top:6px;
			}
		</style>
	</head>
	<body>
		<div class="childgrid" >
		<div class="grid" id="grid">		
			<div class="table-head" >
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td><span style="width:120px;"><fmt:message key="shouyewuzibm" /></span></td>
							<td><span style="width:140px;"><fmt:message key="shouyewuzimc" /></span></td>
							<td><span style="width:100px;"><fmt:message key="specification" /></span></td>
							<td><span style="width:60px;"><fmt:message key="unit" /></span></td>
							<td><span style="width:80px;">报货数量</span></td>
							<td><span style="width:80px;">调整后数量</span></td>
							<td><span style="width:60px;">是否调整</span></td>
							<td><span style="width:50px;">状态</span></td>
							<td><span style="width:150px;"><fmt:message key="remark" /></span></td>
                    	</tr>
					</thead>
				</table>
			</div>				
			<div class="table-body">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<c:forEach var="chkstod" items="${chkstodlist}" varStatus="status">
							<tr>
								<td title="${chkstod.vmcode}" ><span style="width:120px;text-align: left;">${chkstod.vmcode}</span></td>
								<td title="${chkstod.vmname}" ><span style="width:140px;text-align: left;">${chkstod.vmname}</span></td>
								<td title="${chkstod.vspecfication}" ><span style="width:100px;text-align: left;">${chkstod.vspecfication}</span></td>
								<td title="${chkstod.vunitname}" ><span style="width:60px;text-align: left;">${chkstod.vunitname}</span></td>
								<td title="${chkstod.nneedcnt}" ><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstod.nneedcnt}" pattern="0.00"/></span></td>
								<td title="${chkstod.nupdatecnt}" ><span style="width:80px;text-align: right;"><fmt:formatNumber value="${chkstod.nupdatecnt}" pattern="0.00"/></span></td>
								<td>
									<span style="width:60px;">
										<c:if test="${chkstod.isupdate==0}">否</c:if>
										<c:if test="${chkstod.isupdate==1}">是</c:if>
									</span>
								</td>
								<td>
									<span style="width:50px;text-align: center;">
										<c:if test="${chkstod.istate==0}">未提交</c:if>
										<c:if test="${chkstod.istate==1}">已提交</c:if>
										<c:if test="${chkstod.istate==2}">已分拨</c:if>
									</span>	
								</td>
								<td title="${chkstod.vmemo}" ><span style="width:150px;">${chkstod.vmemo}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="form-line">
			<div class="form-label"><fmt:message key="total" /><fmt:message key="supplies" />:</div>
			<div class="form-input">
				<span id="countmaterial">${length}</span>
				<span><fmt:message key="article" /></span>
			</div>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			var height = $(parent.document.body).height()*0.5-12;
			$(".childgrid").css('height',height);
			//自动实现滚动条
			$(".grid").css('height',height-$(".form-line").height()-1);
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())*0.998);
			$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())*0.998);
			loadGrid();//  自动计算滚动条的js方法		
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
		})
		</script>
	</body>
</html>