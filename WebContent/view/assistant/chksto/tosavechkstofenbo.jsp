<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="pk_chksto" name="pk_chksto" value="${chkstom.pk_chksto}"/>
		<input type="hidden" id="acct" name="acct" value="${chkstom.acct}"/>
		<form action="<%=path%>/chksto/updateChkstom.do" id="updateChkstom" name="updateChkstom" method="post">
			<div class="bj_head" style="height: 55px;">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="Theprocuringentity" />:</div>
					<div class="form-input" >
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="text" name="positnname" id="positnname" class="text" readonly="readonly" value="${chkstom.positnname}"/>
						<img id="positncodebutton" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input">
						<input type="text" style="width: 400px;" name="vmemo" id="vmemo" class="text"  value="${chkstom.vmemo}" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
		                                				<td><span style="width:80px;"><fmt:message key="shouyewuzibm" /></span></td>
									<td><span style="width:140px;"><fmt:message key="shouyewuzimc" /></span></td>
									<td><span style="width:70px;"><fmt:message key="specification" /></span></td>
									<td><span style="width:40px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:120px;">报货组织</span></td>
									<td><span style="width:60px;">采购数量</span></td>
									<td><span style="width:120px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTableNoSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		
		function setPk_org(data){
			$("#positncode").val(data.code);//主键
			$("#positnname").val(data.entity[0].vname);//名称
			$("#positncode").val(data.entity[0].vcode);//编码
			$($(".grid .table-body tbody").find('tr:first').find('input')[0]).focus();
		}
		
		function movefocus(e){
			var tr = $(e).closest('tr');
			if(event.keyCode==13 || event.keyCode==40){//向下
				if(tr.next().length>0){
					$(tr.next().find('input')[0]).focus();
				}
			}else if(event.keyCode==38){//向下
				if(tr.prev().length>0){
					$(tr.prev().find('input')[0]).focus();
				}
			}
			
			var val = $(e).val();
			if(isNaN(val)){
				alerterror('数量只能为数字！');
				$(e).val($(e).closest('span').attr('cnt'));
				$(e).focus();
			}else if(Number(val)<=0){
				alerterror('数量不能小于或等于0！');
				$(e).val($(e).closest('span').attr('cnt'));
				$(e).focus();
			}
		}
		
		$(document).ready(function(){
			setElementHeight('.grid',['.tool'],$(document.body),65);	//计算.grid的高度
			$(".table-body").height($(".grid").height()-$(".table-head").height()-20);
			$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()));
			$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()));
			
			$('#positncodebutton').click(function(){
				chooseDepartMentList({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:600,
					callBack:'setPk_org',
					domId:'positncode',
					type:1,
					single:true
				});
			});
			//----------------------------------------------------------------
			
			
			//---------------------加载表格start--------------------------------
			var chkstom = parent.chkstom;
			var totalnum = parent.totalnum;
			var num = 0;
			for(var i=0;i<totalnum;i++){
				var cnt = chkstom['chkstodlist['+ i +'].nupdatecnt'];
				var tr = '<tr index='+num+'>';
				tr = tr+ '<td class="num"><span style="width:16px;">'+ ++num +'</span></td>';
				tr = tr+ '<td><span style="width:80px;">'+chkstom['chkstodlist['+ i +'].vmcode']+'</span></td>';
				tr = tr+ '<td><span style="width:140px;">'+chkstom['chkstodlist['+ i +'].vmname']+'</span></td>';
				tr = tr+ '<td><span style="width:70px;">'+chkstom['chkstodlist['+ i +'].vspecfication']+'</span></td>';
				tr = tr+ '<td><span style="width:40px;">'+chkstom['chkstodlist['+ i +'].vunitname']+'</span></td>';
				tr = tr+ '<td><span style="width:120px;">'+chkstom['chkstodlist['+ i +'].positnname']+'</span></td>';
				tr = tr+ '<td><span style="width:60px;text-align:right;" cnt='+cnt+'><input type="text" style="width: 55px;text-align:right;" value="'+cnt+'" onkeyup="movefocus(this)"></span></td>';
				tr = tr+ '<td><span style="width:120px;text-align:right;"><input type="text" style="width: 115px;"></span></td>';
				tr = tr + '</tr>';	
				
				$(".grid .table-body tbody").append($(tr));
			}
		});
		
		function updateChkstom(){
			var positncode = $("#positncode").val();
			if(positncode==''){
				alerterror('请选择采购组织！');
				return false;
			}else{
				parent.chkstom.positncode=positncode;
			}
			$(".grid .table-body tbody").find('tr').each(function(){
				var npurcnt = $(this).find('td:eq(6)').find('input').val();
				var index = $(this).attr('index');
				parent.chkstom['chkstodlist['+ index +'].npurcnt']=npurcnt;
				
				var vmemo = $(this).find('td:eq(7)').find('input').val();
				parent.chkstom['chkstodlist['+ index +'].vmemo']=vmemo;
			})
			return true;
		}
		</script>
	</body>
</html>
