<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
				.form-line .form-label{
					width:100px;
				}
				.form-line .form-input {
					width:120px;
				}
				.form-line .form-input input{
					width:120px;
				}
			</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
	<div class="tool"></div>
		<form action="<%=path%>/chkstoFendan/chkstofenbo.do" id="listForm" name="listForm" method="post">
			<input type="hidden" id="acct" name="acct" value="${chkstom.acct}"></input>
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label" ><fmt:message key="startdate" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="bdat" name="bdat" style="text-transform:uppercase;" value="${chkstom.bdat}" class="Wdate text" />
					</div>
					<div class="form-label"><fmt:message key="suppliers" />:</div>
					<div class="form-input">
						<input type="hidden" id="delivercode" name="delivercode"  class="text" value="${chkstom.delivercode}"/>
						<input type="text" id="delivername" style="margin-bottom: 6px;" name="delivername"  class="text" value="${chkstom.delivername}"/>
						<img id="supplierbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label" style="margin-left:20px;"><fmt:message key="Document_status" />:</div>
					<div class="form-input">
						<select id="istate" name="istate" class="select" style="width: 120px;">
							<option value="-1" 
							<c:if test="${chkstom.istate==-1}">selected=selected</c:if>
							><fmt:message key="all" /></option>
							<option value="1"
							<c:if test="${chkstom.istate==1}">selected=selected</c:if>
							>未分单</option>
							<option value="2"
							<c:if test="${chkstom.istate==2}">selected=selected</c:if>
							>已分单</option>
						</select>
					</div>
					<div class="form-label" >报货单号:</div>
					<div class="form-input">
						<input type="text" id="vbillno" name="vbillno" value="${chkstom.vbillno}" class="text"/>
					</div>
				</div>
				<div class="form-line">
				<div class="form-label" ><fmt:message key="enddate" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="edat" name="edat" style="text-transform:uppercase;" value="${chkstom.edat}" class="Wdate text"  />
					</div>
					<div class="form-label"><fmt:message key="Theprocuringentity" />:</div>
					<div class="form-input">
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="text" name="positnname" style="margin-bottom: 6px;" id="positnname" class="text" value="${chkstom.positnname}"/>
						<img id="positncodebutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='选择采购组织' />
					</div>
					<div style="display: inline-block;margin:5px 0 0 65px;width: 160px;height:15px;background-color: #F0F0F0;" >
						<div id="currState" style="background-color: #28FF28;height:15px;width:0px;float: left;" ></div>
					</div>
					<div id="per" style="display: inline-block;" style="width:50px;">0</div>
					<div style="font-weight: bold;display: inline;">&nbsp;&nbsp;&nbsp;您有     <span id="changeNum" style="color: red;width:50px;text-align:center;">0</span>    条数据未保存,共有     <span id="changeNum" style="color: green;width:50px;text-align:center;">${totalCount}</span>    条数据</div>
				</div>
				
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
									<td><span style="width:20px;text-align: center;"><input type="checkbox" id="chkAll"/></span></td>	
									<td><span style="width:80px;">单据日期</span></td>
									<td><span style="width:40px;">状态</span></td>
		                                				<td><span style="width:80px;"><fmt:message key="shouyewuzibm" /></span></td>
									<td><span style="width:120px;"><fmt:message key="shouyewuzimc" /></span></td>
									<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
									<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:70px;">报货数量</span></td>
									<td><span style="width:70px;">调整后数量</span></td>
									<td><span style="width:70px;">单价</span></td>
									<td><span style="width:70px;">金额</span></td>
									<td><span style="width:90px;">报货组织</span></td>
									<td><span style="width:120px;">供应商</span></td>
									<td><span style="width:120px;">报货单号</span></td>
									<td><span style="width:120px;">订单号</span></td>
									<td><span style="width:90px;">到货日期</span></td>
									<td><span style="width:100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" id="table-body">
							<tbody>
								<c:forEach var="dis" items="${chkstodlist}" varStatus="status">
									<tr 
										<c:if test="${dis.nneedcnt!=dis.nupdatecnt}">style="color:red;"</c:if>
									>
										<td class="num" positncode="${dis.positncode}" positncode="${dis.positncode}" positnname="${dis.positnname}"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.pk_chkstod}" value="${dis.pk_chkstod}"/></span></td>
										<td><span style="width:80px;">${dis.dmakedate}</span></td>
										<td>
											<span style="width:40px;text-align:center;" istate="${dis.istate}">
												<c:if test="${dis.istate==1}"><img src="<%=path%>/image/assis/image/N.png" style="height:25px;width:25px;" title="未分单"/></c:if>
												<c:if test="${dis.istate==2}"><img src="<%=path%>/image/assis/image/Y.png" style="height:25px;width:25px;" title="已分单"/></c:if>
											</span>	
										
										</td>
			                                				<td><span style="width:80px;" pk_material="${dis.pk_material}">${dis.vmcode}</span></td>
										<td><span style="width:120px;">${dis.vmname}</span></td>
										<td><span style="width:90px;">${dis.vspecfication}</span></td>
										<td><span style="width:50px;" pk_unit="${dis.pk_unit}">${dis.vunitname}</span></td>
										<td><span style="width: 70px;text-align: right;"><fmt:formatNumber value="${dis.nneedcnt}" type="currency" pattern="0.00"/></span></td>
										<td class="textInput">
											<span style="width:70px;text-align:right;" nmincnt="${dis.nmincnt}">
												<input class="nextclass" type="text" style="width: 65px;text-align: right;" value="<fmt:formatNumber value="${dis.nupdatecnt}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveNupdatecnt(this,'${dis.pk_chkstod}')" onkeyup="validate(this)"/>
											</span>
										</td>
										<td>
											<span style="width:70px;text-align:right;" beditprice="${dis.beditprice}">
												<input class="nextclass" type="text" style="width: 65px;text-align: right;" value="<fmt:formatNumber value="${dis.nprice}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveNprice(this,'${dis.pk_chkstod}')" onkeyup="validate(this)"/>
											</span>
										</td>
										<td>
											<span style="width:70px;text-align:right;" beditprice="${dis.beditprice}">
												<input class="nextclass" type="text" style="width: 65px;text-align: right;" value="<fmt:formatNumber value="${dis.nmoney}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveNmoney(this,'${dis.pk_chkstod}')" onkeyup="validate(this)"/>
											</span>
										</td>
										<td><span style="width:90px;">${dis.positnname}</span></td>
										<td class="textInput">
											<span style="width:120px;" delivercode="${dis.delivercode}">
												<input type="text" style="width:100px;float:left;" readonly="readonly" value="${dis.delivername}"></input><img class="search" src="<%=path%>/image/themes/icons/search.png" onclick="selectSupplierByMaterial(this)" />
											</span>
										</td>
										<td><span style="width:120px;">${dis.vbillno}</span></td>
										<td><span style="width:120px;">${dis.vpurbillno}</span></td>
										<td><span style="width:90px;">${dis.dhopedate}</span></td>
										<td istate="${dis.istate}"><span style="width:100px;">${dis.vmemo}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
			<input type="hidden" id="pk_material" name="pk_material"/>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTableNoSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
        $("#bdat").focus();
		//快捷键操作（Alt+*）
		$(document).keydown(function (e) {
		    var doPrevent;
		     if(event.keyCode==83 && event.altKey){//alt+M来自报货模版
		    	if(!$('#window_supply').html()){
		    		updateChksto();
		    	}
		    }else if(event.keyCode==85 && event.altKey){//alt+U
		    	if(!$('#window_supply').html()){
		    		editTables();
		    	}
		    }else{
		        doPrevent = false;
		    }
		    if (doPrevent)
		        e.preventDefault();
		}); 
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		function setPk_org(data){
			$("#positncode").val(data.code);//主键
			$("#positnname").val(data.entity[0].vname);//名称
			$("#positncode").val(data.entity[0].vcode);//编码
		}
		function setSupplier(data){
			$("#delivercode").val(data.entity[0].delivercode);//主键
			$("#delivercode").val(data.entity[0].vcode);//编码
			$("#delivername").val(data.entity[0].vname);//名称
		}
		
			$(document).ready(function(){
				$("#bdat").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
				});
				$("#edat").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
				});
				//回车换焦点start************************************************************************************************
		 		$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			   
			    	$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
			    	new tabTableInput("table-body","text"); //input  上下左右移动
			    	setElementHeight('.grid',['.tool'],$(document.body),100);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
			    	
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
				//全选
				$("#chkAll").click(function() {
				    	if (!!$("#chkAll").attr("checked")) {
				    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
				    	}else{
				            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
			            	}
			    	});
				
				//编辑采购数量和单价时，按回车可以跳到下一行的同一列
				$('tbody .nextclass').live('keydown',function(e){
					if(parent.bhfbEditState=="edit"){//判断如果是编辑状态
				 		if(e.keyCode==13){
				 			var lie = $(this).parent().parent().prevAll().length;
							var hang= $(this).parent().parent().parent().prevAll().length + 1;
							$('tbody').find('tr:eq('+hang+')').find('td:eq('+lie+')').find('span').find('input').focus();
				 		}
					}
				});
				
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setSupplier',
						domId:'delivercode',
						single:true
					});
				});
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"选择组织",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				
				$('.textInput').find('input').live('click',function(event){
					var self = this;
					setTimeout(function(){
						self.select();
					},10);
				});
				
				if('${action}'!='init' && '${currState}'!='1'){
					addTr('first');	
				}else if('${action}'!='init' && '${currState==1}'){
					$("#per").text('100%');
					$("#currState").width(160);
				}
				pageSize = '${pageSize}';
				var ndivHeight = $(".table-body").height();
				$(".table-body").scroll(function(){
			          	nScrollHeight = $(this)[0].scrollHeight;
			          	nScrollTop = $(this)[0].scrollTop;
			          	if((ndivHeight+nScrollTop)/nScrollHeight>0.66 && $("#per").text()!='100%' && returnInfo){
			          			returnInfo = false;
			          			addTr();	
			          	}
			          });
				
				if('${action}'=='init'){
					parent.bhfbEditState = '';//页面初始化的时候讲编辑状态改为非
					parent.trlist=undefined;//将trlist清空
					parent.changeNum=0;//将已经修改的条数改为0
				}else{
					$("#changeNum").text(parent.changeNum);
				}
				
				//判断如果不是编辑状态
				if(parent.bhfbEditState!='edit'){
					$('tbody input[type="text"]').attr('disabled',true);//不可编辑
					loadToolBar([true,true,false,true]);
				}else{
					loadToolBar([true,false,true,false]);
					$('tbody input[type="text"]').attr('disabled',false);
					$('#table-body').find('tr').each(function(){
						var beditpric = $(this).find('td:eq(10)').find('span').attr('beditprice');
						if(beditpric==1){
							$(this).find('td:eq(10)').find('input').attr('disabled',true);
							$(this).find('td:eq(11)').find('input').attr('disabled',true);
						}
						var istate = $(this).find('td:eq(3)').find('span').attr('istate');
						if(istate==2){//已分拨不能编辑
							$(this).find('input').attr('disabled',true);
							$(this).find('.search').remove();
						}
					});
					if($('tbody tr:first .nextclass').length!=0){
						$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
					}
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			
			//控制按钮显示
			function loadToolBar(use){
				$('.tool').html('');
				$('.tool').toolbar({
					items: [{
							text: '<fmt:message key="select" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')}&&use[0],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								if($('#bdat').val()=='' || $('#edat').val()==''){
									alerterror('日期区间不能为空！');
	 							}else{
	 								$("#wait2").css("display","block");
	 								$("#wait").css("display","block");
	 								$("#listForm").submit();
	 							}
							}
						},{
							text: '<fmt:message key="edit" />',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')}&&use[1],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-18px','0px']
							},
							handler: function(){
								editTables();
							}
						},{
							text: '<fmt:message key="save" />(S)',
							useable: use[2],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-80px','-0px']
							},
							handler: function(){
								updateChksto();
							}
						},{
							text: '生成采购订单',
							useable:${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'creat')}&&use[3],
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-80px','-0px']
							},
							handler: function(){
								chkstofenbo();
							}
						},{
							text: '<fmt:message key="print"/>',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								printchkstofenbo();
							}
						},{
							text: '<fmt:message key="quit"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
//	 							invokeClick($(window.parent.parent.document).find('.main').find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
								
								if(parent.bhfbEditState=="edit"){
									alertconfirm('<fmt:message key="data_unsaved_whether_to_exit"/>？',function(){
										invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));
									});
								}else{
									invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
								}
							}
						}]
				});
			}
			function editTables(){
				if($('.grid').find('.table-body').find("tr").size()<1){
					alerterror('<fmt:message key="data_empty_edit_invalid"/>！！');
				}else{
					parent.bhfbEditState='edit';
					loadToolBar([true,false,true,false,false,false]);
					$('tbody input[type="text"]').attr('disabled',false);
					$('#table-body').find('tr').each(function(){
						var beditpric = $(this).find('td:eq(10)').find('span').attr('beditprice');
						if(beditpric==1){
							$(this).find('td:eq(10)').find('input').attr('disabled',true);
							$(this).find('td:eq(11)').find('input').attr('disabled',true);
						}
						var istate = $(this).find('td:eq(3)').find('span').attr('istate');
						if(istate==2){//已分拨不能编辑
							$(this).find('input').attr('disabled',true);
							$(this).find('.search').remove();
						}
					});
					if($('tbody tr:first .nextclass').length!=0){
						$('tbody tr:first .nextclass')[0].focus();//如果是编辑状态下查询，将焦点定位到第一行的采购数量列
					}
				}
			}
			//保存数量
			function saveNupdatecnt(e,f){
				if(e.defaultValue!=e.value){
					var materialname = $(e).closest('tr').find('td:eq(5)').text();
					var nmincnt= $(e).closest('td').find('span').attr('nmincnt');
					
					if(Number(e.value)<nmincnt){
						alerterror(materialname+'最小报货量为'+nmincnt);
						$(e).val(0.0);
					}
					
					var nprice = $(e).closest('td').next().find('input').val();
					var nmoney = (Number(e.value)*Number(nprice)).toFixed(2);
					$(e).closest('td').next().next().find('input').val(nmoney);
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['nupdatecnt']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['nupdatecnt']=e.value;
						}
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nmoney']=nmoney;
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['nupdatecnt']=e.value;
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nmoney']=nmoney;
					}
				}
			}
			//保存价格
			function saveNprice(e,f){
				if(e.defaultValue!=e.value){
					var nupdatecnt = $(e).closest('td').prev().find('input').val();
					var nmoney = (Number(e.value)*Number(nupdatecnt)).toFixed(2);
					$(e).closest('td').next().find('input').val(nmoney);
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['nprice']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['nprice']=e.value;
						}
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nmoney']=nmoney;
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['nprice']=e.value;
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nmoney']=nmoney;
					}
				}
			}
			//保存金额
			function saveNmoney(e,f){
				var nupdatecnt = $(e).closest('td').prev().prev().find('input').val();
				var nprice = Number(nupdatecnt)==0?0:(Number(e.value)/Number(nupdatecnt)).toFixed(2);
				$(e).closest('td').prev().find('input').val(nprice);
				if(e.defaultValue!=e.value){
					if(parent.trlist){
						if(parent.trlist[f]){
							parent.trlist[f]['nmoney']=e.value;
						}else{
							parent.trlist[f]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[f]['nmoney']=e.value;
						}
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nprice']=nprice;
					}else{
						parent.trlist = {};
						parent.trlist[f]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[f]['nmoney']=e.value;
						parent.trlist[f]['isupdate']=1;
						parent.trlist[f]['nprice']=nprice;
					}
				}
			}
			
			var selectobj;
			function selectSupplierByMaterial(e){
				if(parent.bhfbEditState!="edit"){
					return;
				}
				selectobj = e;
				var pk_material = $(selectobj).closest('tr').find('td:eq(4)').find('span').attr('pk_material');
				
				var checksupplier = true;
				var param = {};
				param['pk_material'] = pk_material;
				$.post("<%=path%>/material/findDeliverByMaterial.do",param,function(re){
					if(re==''){
						checksupplier = false;
					}
				});
				
				if(checksupplier){
					var delivercode = $(e).closest('span').attr('delivercode');
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setSupplierByMaterial',
						domId:'delivercode',
						pk_material:pk_material,
						delivercode:delivercode,
						single:true
					});
				}else{
					alerttips('该物资没有供应商，请到供应商列表增加【自购】供应商<br/>或者在已有供应商中分配添加该物资！');
				}
			}
			//重新修改供应商  并查找新的价格
			function setSupplierByMaterial(data){
				var pk_chkstod = $(selectobj).closest('tr').find('td:eq(1)').find('input').val();
				var olddelivercode = $(selectobj).closest('td').find('span').attr('delivercode');
				
				var delivercode = data.entity[0].delivercode;
				var delivercode = data.entity[0].vcode;
				var delivername = data.entity[0].vname;
				if(delivercode==''){
					$(selectobj).closest('td').find('input').val('');//名称
					$(selectobj).closest('td').find('span').attr('delivercode',delivercode);//主键
					$(selectobj).closest('tr').find('td:eq(10)').find('input').val('0.00');
					$(selectobj).closest('tr').find('td:eq(11)').find('input').val('0.00');
					$(selectobj).closest('tr').find('td:eq(10)').attr('beditprice',0);
					$(selectobj).closest('tr').find('td:eq(11)').attr('beditprice',0);
					if(olddelivercode!=''){
						if(parent.trlist){
							if(parent.trlist[pk_chkstod]){
								parent.trlist[pk_chkstod]['delivercode']='';
								parent.trlist[pk_chkstod]['delivercode']='';
								parent.trlist[pk_chkstod]['delivername']='';
							}else{
								parent.trlist[pk_chkstod]={};
								parent.changeNum = parent.changeNum+1;
								$("#changeNum").text(parent.changeNum);
								parent.trlist[pk_chkstod]['delivercode']='';
								parent.trlist[pk_chkstod]['delivercode']='';
								parent.trlist[pk_chkstod]['delivername']='';
							}
							parent.trlist[pk_chkstod]['nprice']=$(selectobj).closest('tr').find('td:eq(10)').find('input').val();
							parent.trlist[pk_chkstod]['nmoney']=$(selectobj).closest('tr').find('td:eq(11)').find('input').val();
						}else{
							parent.trlist = {};
							parent.trlist[pk_chkstod]={};
							parent.changeNum = 1;
							$("#changeNum").text(1);
							parent.trlist[pk_chkstod]['delivercode']='';
							parent.trlist[pk_chkstod]['nprice']=$(selectobj).closest('tr').find('td:eq(10)').find('input').val();
							parent.trlist[pk_chkstod]['nmoney']=$(selectobj).closest('tr').find('td:eq(11)').find('input').val();
							parent.trlist[pk_chkstod]['delivercode']='';
							parent.trlist[pk_chkstod]['delivername']='';
						}
					}
					
					return;
				}
				$(selectobj).closest('td').find('input').val(delivername);//名称
				$(selectobj).closest('td').find('span').attr('delivercode',delivercode);//主键
				
				var positncode = $(selectobj).closest('tr').find('td:first').attr('positncode');
				var pk_material = $(selectobj).closest('tr').find('td:eq(4)').find('span').attr('pk_material');
				var param = {};
				param['positncode'] = positncode;
				param['pk_material'] = pk_material;
				param['delivercode'] = delivercode;
				$.post('<%=path%>/material/findMaterialNprice.do',param,function(re){
					var reninprice = re.ninprice;
					//没有合约价  可以编辑价格
					if(Number(reninprice)==0){
						$(selectobj).closest('tr').find('td:eq(10)').find('input').attr('disabled',false);
						$(selectobj).closest('tr').find('td:eq(11)').find('input').attr('disabled',false);
						$(selectobj).closest('tr').find('td:eq(10)').attr('beditprice',0);
						$(selectobj).closest('tr').find('td:eq(11)').attr('beditprice',0);
					}else{
						$(selectobj).closest('tr').find('td:eq(10)').find('input').attr('disabled',true);
						$(selectobj).closest('tr').find('td:eq(11)').find('input').attr('disabled',true);
						$(selectobj).closest('tr').find('td:eq(10)').attr('beditprice',1);
						$(selectobj).closest('tr').find('td:eq(11)').attr('beditprice',1);
					}
					if(Number(reninprice) == 0.0){
						$.post('<%=path%>/material/findMaterialNpricesupplier.do',param,function(materialobj){
							reninprice = materialobj.ninprice;
						});
					}
					$(selectobj).closest('tr').find('td:eq(10)').find('input').val(Number(reninprice).toFixed(2));
					var updcnt = $(selectobj).closest('tr').find('td:eq(9)').find('input').val();
					$(selectobj).closest('tr').find('td:eq(11)').find('input').val((Number(updcnt)*Number(reninprice)).toFixed(2));
					
					if(parent.trlist){
						if(parent.trlist[pk_chkstod]){
							parent.trlist[pk_chkstod]['delivercode']=delivercode;
							parent.trlist[pk_chkstod]['delivercode']=delivercode;
							parent.trlist[pk_chkstod]['delivername']=delivername;
						}else{
							parent.trlist[pk_chkstod]={};
							parent.changeNum = parent.changeNum+1;
							$("#changeNum").text(parent.changeNum);
							parent.trlist[pk_chkstod]['delivercode']=delivercode;
							parent.trlist[pk_chkstod]['delivercode']=delivercode;
							parent.trlist[pk_chkstod]['delivername']=delivername;
						}
						parent.trlist[pk_chkstod]['nprice']=$(selectobj).closest('tr').find('td:eq(10)').find('input').val();
						parent.trlist[pk_chkstod]['nmoney']=$(selectobj).closest('tr').find('td:eq(11)').find('input').val();
					}else{
						parent.trlist = {};
						parent.trlist[pk_chkstod]={};
						parent.changeNum = 1;
						$("#changeNum").text(1);
						parent.trlist[pk_chkstod]['delivercode']=data.entity[0].delivercode;
						parent.trlist[pk_chkstod]['nprice']=$(selectobj).closest('tr').find('td:eq(10)').find('input').val();
						parent.trlist[pk_chkstod]['nmoney']=$(selectobj).closest('tr').find('td:eq(11)').find('input').val();
						parent.trlist[pk_chkstod]['delivercode']=delivercode;
						parent.trlist[pk_chkstod]['delivername']=delivername;
					}
				});
			}
			//焦点离开检查输入是否合法
			function validate(e){
				if(null==e.value || ""==e.value){
					e.value=e.defaultValue;
					showMessage({
								type: 'error',
								msg: '<fmt:message key="cannot_be_empty"/>！',
								speed: 1000
								});
					$(e).focus();
					return;
				}
				if(isNaN(e.value)||Number(e.value)<0 ){
					e.value=e.defaultValue;
					showMessage({
								type: 'error',
								msg: '不是有效数字！',
								speed: 1000
								});
					$(e).focus();
					return;
				}
			}
			
			var totalCount;
			var condition;
			var currPage;
			function addTr(check){
				if(check=='first'){
					totalCount = '${totalCount}';
					condition = ${chkstomjson};
					currPage= 1;
					condition['totalCount'] = totalCount;
					condition['currPage']=1;
					$("#per").text((Number('${currState}')*100).toFixed(0)+'%');
					$("#currState").width(Number('${currState}')*160);
					return;
				}
				$.post("<%=path%>/chkstoFendan/listAjax.do",condition,function(data){
					var rs = eval('('+data+')');
					//var rs = data;
					$("#per").text((rs.currState*100).toFixed(0)+'%');
					$("#currState").width(""+rs.currState*160+"px");
					//不是最后一页
					var num = rs.currPage*pageSize;
					var chkstodlist = rs.chkstodlist;
					for(var i in chkstodlist){
						var dis = chkstodlist[i];
						
						var tr = '<tr ';
						if(dis.nneedcnt!=dis.nupdatecnt){
							tr = tr+'style="color:red;"';
						}
						tr = tr + '>';
						tr = tr+ '<td class="num" positncode="'+dis.positncode+'" positncode="'+dis.positncode+'" positnname="'+dis.positnname+'"><span style="width:16px;">'+ ++num +'</span></td>';
						tr = tr+ '<td><span style="width:20px;text-align: center;"><input type="checkbox" name="idList" id="chk_'+dis.pk_chkstod+'" value="'+dis.pk_chkstod+'"/></span></td>';
						tr = tr+ '<td><span style="width:80px;">'+dis.dmakedate+'</span></td>';
						var staname = '';
						var img = '';
						if(dis.istate==1){
							staname=='未分单';
							img = '<img src="<%=path%>/image/assis/image/N.png" style="height:25px;width:25px;"/>';
						}else if(dis.istate==2){
							staname=='已分单';
							img = '<img src="<%=path%>/image/assis/image/Y.png" style="height:25px;width:25px;"/>';
						}
						tr = tr+ '<td><span style="width:40px;text-align:center;"  istate="'+dis.istate+'" title="'+staname+'">'+img+'</span></td>';
						tr = tr+ '<td><span style="width:80px;" pk_material="'+dis.pk_material+'">'+dis.vmcode+'</span></td>';
						tr = tr+ '<td><span style="width:120px;">'+dis.vmname+'</span></td>';
						tr = tr+ '<td><span style="width:90px;">'+dis.vspecfication+'</span></td>';
						tr = tr+ '<td><span style="width:50px;" pk_unit="'+dis.pk_unit+'">'+dis.vunitname+'</span></td>';
						tr = tr+ '<td><span style="width: 70px;text-align: right;">'+numberformat(dis.nneedcnt)+'</span></td>';
						tr = tr+ '<td class="textInput"><span style="width:70px;text-align:right;" nmincnt="'+dis.nmincnt+'"><input class="nextclass" type="text" style="width: 70px;text-align: right;" '+(dis.istate==2?"disabled=disabled":"")+' value=\"'+numberformat(dis.nupdatecnt)+'\" onfocus="this.select()" onblur="saveNupdatecnt(this,\"'+dis.pk_chkstod+'\")" onkeyup="validate(this)"/></span></td>';
						tr = tr+ '<td><span style="width:70px;text-align:right;" beditprice="'+dis.beditprice+'"><input class="nextclass" type="text" style="width: 70px;text-align: right;" '+(dis.istate==2?"disabled=disabled":"")+' value=\"'+numberformat(dis.nprice)+'\" onfocus="this.select()" onblur="saveNprice(this,\"'+dis.pk_chkstod+'\")" onkeyup="validate(this)"/></span></td>';
						tr = tr+ '<td><span style="width:70px;text-align:right;" beditprice="'+dis.beditprice+'"><input class="nextclass" type="text" style="width: 70px;text-align: right;" '+(dis.istate==2?"disabled=disabled":"")+' value=\"'+numberformat(dis.nmoney)+'\" onfocus="this.select()" onblur="saveNmoney(this,\"'+dis.pk_chkstod+'\")" onkeyup="validate(this)"/></span></td>';
						tr = tr+ '<td><span style="width:90px;">'+dis.positnname+'</span></td>';
						tr = tr+ '<td class="textInput"><span style="width:120px;" delivercode="'+dis.delivercode+'"><input type="text" style="width:100px;float:left;" readonly="readonly" value=\"'+dis.delivername+'\" ></input>'+(dis.istate==1||parent.bhfbEditState!="edit"?"<img class=\"search\" src=\"<%=path%>/image/themes/icons/search.png\" onclick=\"selectSupplierByMaterial(this)\" />":"")+'</span></td>';
						tr = tr+ '<td><span style="width:120px;">'+dis.vbillno+'</span></td>';
						tr = tr+ '<td><span style="width:120px;">'+dis.vpurbillno+'</span></td>';
						tr = tr+ '<td><span style="width:90px;">'+dis.dhopedate+'</span></td>';
						tr = tr+ '<td istate="'+dis.istate+'"><span style="width:100px;">'+dis.vmemo+'</span></td>';
						tr = tr + '</tr>';	
						
						$(".grid .table-body tbody").append($(tr));
					}
					$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
					if(rs.over!='over'){
						condition['currPage']=++currPage;
						returnInfo = true;
					}
					if(parent.bhfbEditState!='edit'){
						$('tbody input[type="text"]').attr('disabled',true);//不可编辑
					}else{
						$('#table-body').find('tr').each(function(){
							var beditpric = $(this).find('td:eq(10)').find('span').attr('beditprice');
							if(beditpric==1){
								$(this).find('td:eq(10)').find('input').attr('disabled',true);
								$(this).find('td:eq(11)').find('input').attr('disabled',true);
							}
							var istate = $(this).find('td:eq(3)').find('span').attr('istate');
							if(istate==2){//已分拨不能编辑
								$(this).find('input').attr('disabled',true);
								$(this).find('.search').remove();
							}
						})
						new tabTableInput("table-body","text"); //input  上下左右移动	
					}
				});
			}
			
			
			//修改报货------------		
			function updateChksto(){
				$("#positncode").focus();
				//如果修改过数据就保存 
				if(parent.trlist){
					var predata = parent.trlist;
					var chkstodlist = {};
					var num=0;
					for(var i in predata){
						chkstodlist['chkstodlist['+num+'].pk_chkstod']=i;
						chkstodlist['chkstodlist['+num+'].acct']=$("#acct").val();
						undefined==predata[i].nprice?'':chkstodlist['chkstodlist['+num+'].nprice']=predata[i].nprice;
						undefined==predata[i].nmoney?'':chkstodlist['chkstodlist['+num+'].nmoney']=predata[i].nmoney;
						undefined==predata[i].nupdatecnt?'':chkstodlist['chkstodlist['+num+'].nupdatecnt']=predata[i].nupdatecnt;
						undefined==predata[i].isupdate?'':chkstodlist['chkstodlist['+num+'].isupdate']=predata[i].isupdate;
						undefined==predata[i].delivercode?'':chkstodlist['chkstodlist['+num+'].delivercode']=predata[i].delivercode;
						undefined==predata[i].delivercode?'':chkstodlist['chkstodlist['+num+'].delivercode']=predata[i].delivercode;
						undefined==predata[i].delivername?'':chkstodlist['chkstodlist['+num+'].delivername']=predata[i].delivername;
						num++;
					}
					$.post("<%=path%>/chkstoFendan/updateChkstod.do",chkstodlist,function(re){
						if(isNaN(re)){
							alerterror('修改失败！');
						}else{
							alerttipsbreak('成功修改'+re+'条数据！',function(){
								var action="<%=path%>/chkstoFendan/chkstofenbo.do";
								$('#listForm').attr('action',action);
				 				$('#listForm').submit();
							});
						}
					});	
				}else{
					alerttipsbreak('您没有修改任何数据！',function(){
		 				$('#listForm').submit();
					});
				}
				parent.bhfbEditState = '';
				parent.changeNum = 0;
				parent.trlist=undefined;
				loadToolBar([true,true,false,true,true,true]);
				$('tbody input[type="text"]').attr('disabled',true);//不可编辑
			}
			//分单
			var chkstom = {};
			var totalnum = 0;
			function chkstofenbo(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox:checked');
				if(checkboxList.size()>0){
					var checkdata = true;
					var num = 0;
					checkboxList.each(function(){
						var tr = $(this).closest('tr');
						var index = tr.find('td:first').text();
						if(tr.find('td:last').attr('istate')==2){
							alerterror('第'+index+'条数据已分单！');
							checkdata = false;
							return false;
						}
						var positncode = tr.find('td:first').attr('positncode');
						var positncode = tr.find('td:first').attr('positncode');
						var positnname = tr.find('td:first').attr('positnname');
						var pk_material = tr.find('td:eq(4)').find('span').attr('pk_material');
						var vmaterialcode = tr.find('td:eq(4)').find('span').text();
						var vmaterialname = tr.find('td:eq(5)').find('span').text();
						var vspecfication = tr.find('td:eq(6)').find('span').text();
						var pk_unit = tr.find('td:eq(7)').find('span').attr('pk_unit');
						var vunitname = tr.find('td:eq(7)').find('span').text();
						var nupdatecnt = tr.find('td:eq(9)').find('input').val();
						var nprice  = tr.find('td:eq(10)').find('input').val();
						var beditprice  = tr.find('td:eq(10) span').attr('beditprice');
						var nmoney = tr.find('td:eq(11)').find('input').val();
						var delivercode = tr.find('td:eq(13)').find('span').attr('delivercode');
						var delivername = tr.find('td:eq(13)').find('span').find('input').val();
						var vmemo = tr.find('td:last').find('span').text();
						var dhopedate = tr.find('td:last').prev().find('span').text();
						
						if(nupdatecnt=='' || nupdatecnt==0){
							alerterror('第'+index+'条数据调整后数量为0，无法分单！');
							checkdata = false;
							return false;
						}
						if(nprice=='' || nprice==0){
							alerterror('第'+index+'条数据单价为0，无法分单！');
							checkdata = false;
							return false;
						}
						if(nmoney=='' || nmoney==0){
							alerterror('第'+index+'条数据金额为0，无法分单！');
							checkdata = false;
							return false;
						}
						if(delivercode=='' || delivercode==0){
							alerterror('第'+index+'条数据未选择供应商，无法分单！');
							checkdata = false;
							return false;
						}
						chkstom['chkstodlist['+ num +'].positncode']=positncode;
						chkstom['chkstodlist['+ num +'].positncode']=positncode;
						chkstom['chkstodlist['+ num +'].positnname']=positnname;
						chkstom['chkstodlist['+ num +'].pk_material']=pk_material;
						chkstom['chkstodlist['+ num +'].vmcode']=vmaterialcode;
						chkstom['chkstodlist['+ num +'].vmname']=vmaterialname;
						chkstom['chkstodlist['+ num +'].vspecfication']=vspecfication;
						chkstom['chkstodlist['+ num +'].pk_unit']=pk_unit;
						chkstom['chkstodlist['+ num +'].vunitname']=vunitname;
						chkstom['chkstodlist['+ num +'].pk_chkstod']=$(this).val();
						chkstom['chkstodlist['+ num +'].nupdatecnt']=nupdatecnt;
						chkstom['chkstodlist['+ num +'].nprice']=nprice;
						chkstom['chkstodlist['+ num +'].nmoney']=nmoney;
						chkstom['chkstodlist['+ num +'].delivercode']=delivercode;
						chkstom['chkstodlist['+ num +'].delivername']=delivername;
						chkstom['chkstodlist['+ num +'].dhopedate']=dhopedate;
						chkstom['chkstodlist['+ num +'].vmemo']=vmemo;
						chkstom['chkstodlist['+ num +'].beditprice']=beditprice;
						
						num++;
					});
					if(checkdata){
						totalnum = num;
						$('body').window({
							id: 'window_savechksto',
							title: '报货分单',
							content: '<iframe id="savechksto" frameborder="0" src="<%=path%>/chkstoFendan/tosavechkstofenbo.do"></iframe>',
							width: '750px',
							height: '500px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											positn: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('savechksto')){
												if(window.document.getElementById("savechksto").contentWindow.updateChkstom()){
													$.post('<%=path%>/chkstoFendan/savePuprorderByChksto.do',chkstom,function(reinfo){
														if(reinfo=='OK'){
															alerttipsbreak('分单成功！',function(){
																parent.bhfbEditState = '';
																parent.changeNum = 0;
																parent.trlist=undefined;
																loadToolBar([true,true,false,true,true,true]);
																$('tbody input[type="text"]').attr('disabled',true);//不可编辑
																$('#listForm').submit();
															});
														}else{
															alerterror('分单失败！');
														}
													});
												}
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											positn: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}
					
					
				}else{
					alerterror('请选择要分单的数据！');
				}
			}
			
			//打印报货分单
			function printchkstofenbo(){
				$("#listForm").attr('action','<%=path%>/chkstoFendan/printChkstofenbo.do');
				$("#listForm").attr('target','report');
				window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
				$("#listForm").submit();
				$("#listForm").attr('target','');
				$("#listForm").attr('action','<%=path%>/chkstoFendan/chkstofenbo.do');
				$('#wait2,#wait').css("visibility","hidden");
			}
			
			function numberformat(prenumber){
				return prenumber.toFixed(2);
			}
			
			
		</script>
	</body>
</html>
