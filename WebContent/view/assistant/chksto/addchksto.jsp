<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
				.form-line .form-label{
					margin-left:10px;
				}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="sta" name="sta" value="${sta}"/>
		<form action="<%=path%>/chksto/addChkstom.do" id="saveChkstom" name="saveChkstom" method="post">
			<div class="bj_head" style="height: 75px;">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span>单据号:</div>
					<div class="form-input">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${chkstom.vbillno}"/>
					</div>
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="document_date" />:</div>
					<div class="form-input" >
						<input autocomplete="off" type="text" id="dmakedate" name="dmakedate" value="${chkstom.dmakedate}" class="Wdate text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="Theprocuringentity" />:</div>
					<div class="form-input" >
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
						<input type="text" name="positnname" id="positnname" style="margin-bottom: 6px;" class="text" readonly="readonly" value="${chkstom.positnname}"/>
						<img id="positncodebutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label"><fmt:message key="Hope_date" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="dhopedate" name="dhopedate" value="${chkstom.dhopedate}" class="Wdate text" />
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input">
						<input type="text" style="width: 415px;" name="vmemo" id="vmemo" class="text" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 15px;">&nbsp;</span></td>
		                            					<td><span style="width:80px;"><fmt:message key="shouyewuzibm" /></span></td>
									<td><span style="width:140px;"><fmt:message key="shouyewuzimc" /></span></td>
									<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
									<td><span style="width:90px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:90px;">报货数量</span></td>
									<td><span style="width:100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</form>
			<input type="hidden" id="pk_material" name="pk_material"/>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
        $("#dmakedate").focus();
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
			var selectedrow;
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			var validate;
			$(document).ready(function(){
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull'],
						param:['F'],
						error:['单据号<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'dmakedate',
						validateType:['canNull'],
						param:['F'],
						error:['单据日期<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'positnname',
						validateType:['canNull'],
						param:['F'],
						error:['采购组织<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'dhopedate',
						validateType:['canNull'],
						param:['F'],
						error:['要求到货日<fmt:message key="cannot_be_empty" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="remark" /><fmt:message key="length_too_long" />！']
					}]
				});
				
			    	$('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),60);	//计算.grid的高度
// 				$(".table-body").height($(".grid").height()-$(".table-head").height()-20);
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()));
				
				editCells();
				
				$("#dmakedate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dhopedate\')}'});
				});
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dmakedate\')}',maxDate:'%y-%M-{%d+'+180+'}'});
				});
				
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"选择组织",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:10,
					widths:[30,90,150,100,100,100,110],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1",'text-align':'right'},{background:"#F1F1F1"},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos == 2){
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
						if(pos == 5){
							var bhnum = $.trim(rownum.find("td:eq(5) span").text());
							if(bhnum==''){
								bhnum=$.trim(rownum.find("td:eq(5) input").val());
							}
							var mincnt = Number(rownum.find("td:eq(5) span").attr('minbhcnt'));
							if(isNaN(bhnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) < mincnt){
								rownum.find("td:eq(5) span").text(mincnt);
								alerterror('第'+rownum.find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+mincnt);
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else{
								rownum.find("td:eq(5) span").text(bhnum);
								$.fn.autoGrid.setCellEditable(rownum,6);
							}
						}
						if(pos == 6){
							var memo = rownum.find("td:eq(6) input").val();
							postd.find('span').text(memo);
							return;
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								  selectedrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                positncode:$('#positncode').val(),//采购组织
							                irateflag:1,//取采购单位
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(5) span").text('');
									row.find("td:eq(5) span").attr('minbhcnt','');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(7) span").text('');
									row.find("td:eq(8) span").text('');
									row.find("td:eq(9) span").text('');
									
									$("#mMenu").remove();
									$.fn.autoGrid.setCellEditable(row,2);
									return;	
								}
							}
							if($('#positnname').val() == null || $('#positnname').val() == ''){
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
								return;
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?irateflag=1&positncode='+$('#positncode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var vspecfication ='';
							if(data.vspecfication !='' && data.vspecfication !=null){
								vspecfication = '-'+data.vspecfication ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + vspecfication;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '物资重复添加！',
	 								speed: 1000
	 							});
                                					$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span").text(data2.vname);
							row.find("td:eq(2) span").attr('vname',data2.vname);
							row.find("td:eq(3) span").text(data2.vspecfication);
							row.find("td:eq(4) span").text(data2.unitvname);
							row.find("td:eq(5) span").attr('minbhcnt',data2.minbhcnt);
							row.find("td:eq(7) span").text(data2.pk_material);
							row.find("td:eq(8) span").text(data2.pk_unit);
							row.find("td:eq(9) span").text(data2.pk_materialtype);
							$.fn.autoGrid.setCellEditable(row,5);
						}
					},{
						index:5,
						action:function(row,data2){
							var mincnt = Number(row.find("td:eq(5) span").attr('minbhcnt'));
							
							if(isNaN(data2.value)){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) == 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < mincnt){
								row.find("td:eq(5) span").text(mincnt);
								alerterror('第'+row.find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+mincnt);
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								$.fn.autoGrid.setCellEditable(row,6);
							}
						},
						onCellEdit:function(event,data,row){
							var mincnt = Number(row.find("td:eq(5) span").attr('minbhcnt'));
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								$.fn.autoGrid.setCellEditable(row,5);
							}
						}
					},{
						index:6,
						action:function(row,data){
							row.find("td:eq(6) span").text(data.value);
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
						}
					}]
				});
			}
			var savelock = false;
			function saveChkstom(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2){
						$(this).find('input').trigger('onEnter');
					}
				});
				var checkcnt = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var pk_material=$.trim($(this).find("td:eq(7) span").text());
					if(pk_material==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
						return;
					}
					
					var bhnum = $.trim($(this).find("td:eq(5) span").text());
					var mincnt = Number($(this).find("td:eq(5) span").attr('minbhcnt'));
					if(Number(bhnum)<Number(mincnt)){
						alerterror('第'+$(this).find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="Minimum_Purchase_Amount"/>'+mincnt);
						checkcnt = false;
						return false;
					}
				});
				if(!checkcnt){
					savelock = false;
					return;
				}
				
				
				$("#vmemo").focus();
				var keys = ["vmcode","vmname","vspecfication","vunitname","nneedcnt","vmemo","pk_material","pk_unit","pk_materialtype"];
				var data = {};
				var i = 0;
				data["vbillno"] = $.trim(stripscript($("#vbillno").val()));
				data["positncode"] = $("#positncode").val();
				data["positnname"] = $("#positnname").val();
				data["positncode"] = $("#positncode").val();
				data["dmakedate"] = $("#dmakedate").val();
				data["dhopedate"] = $("#dhopedate").val();
				data["vmemo"] = $("#vmemo").val();
				
				if(data.positncode==''){
					alerterror('请选择采购组织！');
					$("#wait2",parent.document).css("display","none");
					$("#wait",parent.document).css("display","none");
					savelock = false;
					return;
				}
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var pk_material=$.trim($(rows[i]).find('td:eq(2)').text());
					if(pk_material==''){
						alerterror('请选择物资！');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						savelock = false;
						return;
					}
				}
				var checkdata = true;
				var rowindex = 0;
				for(i=0;i<rows.length;i++){
					var vmname = $.trim($(rows[i]).find("td:eq(2)").text());
					if(vmname==''){
						continue;
					}
					var preneedcnt = $(rows[i]).find("td:eq(5)").text();
					if(isNaN(preneedcnt)){
						alerterror('第'+(i+1)+'行报货数量不正确！');
						$("#wait2",parent.document).css("display","none");
				                    $("#wait",parent.document).css("display","none");
				                    checkdata = false;
						break;
					}
					var vneedcnt = Number(preneedcnt);
					var minbhcnt = $(rows[i]).find("td:eq(5)").find('span').attr('minbhcnt');
					if(vneedcnt<=0){
						alerterror('第'+(i+1)+'行报货数量不正确！');
						$("#wait2",parent.document).css("display","none");
				                    $("#wait",parent.document).css("display","none");
				                    checkdata = false;
						break;
					}else if(vneedcnt<Number(minbhcnt)){
						alerterror('第'+(i+1)+'行最小报货数量为'+minbhcnt);
						$("#wait2",parent.document).css("display","none");
				                    $("#wait",parent.document).css("display","none");
				                    checkdata = false;
						break;
					}
					data["chkstodlist["+rowindex+"].nmincnt"] = minbhcnt;
					data["chkstodlist["+rowindex+"].dhopedate"] = $("#dhopedate").val();	
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
						if(value){
							data["chkstodlist["+rowindex+"]."+keys[j-1]] = value;	
						}
					}
					rowindex++;
				}
				if(!checkdata){
					savelock = false;
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/chksto/addChkstom.do",data,function(re){
					var rs = re;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
				$(".validateMsg").css('display','none');
				$('.table-body').find('table').empty();
				editCells();
			}
			
			function setMaterial(data){
				var flag = true;
				if(data.entity[0].pk_material==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				
				selectedrow.find("td:eq(1) span").text(data.entity[0].vcode).css("text-align","right");
				selectedrow.find("td:eq(2) span").text(data.entity[0].vname);
				selectedrow.find("td:eq(2) span").attr('vname',data.entity[0].vname);
				selectedrow.find("td:eq(3) span").text(data.entity[0].vspecfication);
				selectedrow.find("td:eq(4) span").text(data.entity[0].vunitname);
				selectedrow.find("td:eq(5) span").attr('minbhcnt',data.entity[0].minbhcnt);
				selectedrow.find("td:eq(6) span").text(data.entity[0].vmemo);
				selectedrow.find("td:eq(7) span").text(data.entity[0].pk_material);
				selectedrow.find("td:eq(8) span").text(data.entity[0].pk_unit);
				selectedrow.find("td:eq(9) span").text(data.entity[0].pk_materialtype);
				$.fn.autoGrid.setCellEditable(selectedrow,5);
			}
			
			function resetName(e){
				if($('#mMenu').length!=0){
					return;
				}
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==2){//物资 供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}else if(curindex!=6){
					$(e).closest('span').text($(e).val());
				}
			}
		</script>
	</body>
</html>
