<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
				margin-left:20px;
			}
			.form-line .form-input{
				width: 100px;
			}
			.form-line .form-input input{
				width: 100px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
			<div class="tool"></div>
			<div>
			<input type="hidden" id="pk_chksto" name="pk_chksto" value="${pk_chksto}"/>
			<form id="listForm" action="<%=path%>/chksto/queryAllChkstom.do" method="post">
				<div class="bj_head">
					<div class="form-line">
						<div class="form-label"><fmt:message key="startdate" />:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="bdat" name="bdat" style="text-transform:uppercase;" value="${chkstom.bdat}" class="Wdate text"/>
						</div>
						<div class="form-label"><fmt:message key="orders_code" />:</div>
						<div class="form-input">
							<input type="text" name="vbillno" id="vbillno" class="text" value="${chkstom.vbillno}"/>
						</div>
						<div class="form-label" style="margin-left:20px;"><fmt:message key="Document_status" />:</div>
						<div class="form-input">
							<select id="istate" name="istate" class="select">
								<option value="-1" 
								<c:if test="${chkstom.istate==-1}">selected=selected</c:if>
								><fmt:message key="all" /></option>
								<option value="0"
								<c:if test="${chkstom.istate==0}">selected=selected</c:if>
								>未提交</option>
								<option value="1"
								<c:if test="${chkstom.istate==1}">selected=selected</c:if>
								>已提交</option>
							</select>
						</div>
					</div>
					<div class="form-line">
						<div class="form-label" ><fmt:message key="enddate" />:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="edat" name="edat" style="text-transform:uppercase;" value="${chkstom.edat}" class="Wdate text"/>
						</div>
						<div class="form-label"><fmt:message key="Theprocuringentity" />:</div>
						<div class="form-input">
							<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
							<input type="hidden" name="positncode" id="positncode" class="text" value="${chkstom.positncode}"/>
							<input type="text" name="positnname" id="positnname" style="margin-bottom: 6px;" readonly="readonly" class="text" value="${chkstom.positnname}"/>
							<img id="positncodebutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
						</div>
					</div>
				</div>
				<div class="grid" id="grid" style="overflow: hidden;positn: static;">		
					<div class="table-head" style="width: 100%;">
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td><span style="width:30px;"></span></td>
									<td><span style="width:30px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:150px;">报货单号</span></td>
									<td><span style="width:150px;">采购组织</span></td>
									<td><span style="width:100px;">单据状态</span></td>
									<td><span style="width:100px;">制单日期</span></td>
									<td><span style="width:100px;">到货日</span></td>
									<td><span style="width:150px;">备注</span></td>
		                     				</tr>
							</thead>
						</table>
					</div>				
					<div class="table-body" style="width: 100%;">
						<table cellspacing="0" cellpadding="0" id="tblGrid">
							<tbody>
								<c:forEach var="chkstom" items="${chkstomList}" varStatus="status">
									<tr>
										<td class="num"><span style="width:30px;">${status.index+1}</span></td>
										<td title="${chkstom.pk_chksto}" ><span style="text-align: center;width:30px;"><input type="checkbox" name="idList" id="chk_${chkstom.pk_chksto}" value="${chkstom.pk_chksto}"/></span></td>
										<td title="${chkstom.vbillno}" ><span style="width:150px;">${chkstom.vbillno}</span></td>
										<td title="${chkstom.positnname}" ><span style="width:150px;">${chkstom.positnname}</span></td>
										<td>
											<span style="width:100px;text-align: center;<c:if test="${chkstom.istate==1}">color:red;</c:if>" istate="${chkstom.istate}" >
												<c:if test="${chkstom.istate==0}">未提交</c:if>
												<c:if test="${chkstom.istate==1}">已提交</c:if>
											</span>
										</td>
										<td title="${chkstom.dmakedate}" ><span style="width:100px;text-align: center;">${chkstom.dmakedate}</span></td>
										<td title="${chkstom.dhopedate}" ><span style="width:100px;text-align: center;">${chkstom.dhopedate}</span></td>
										<td title="${chkstom.vmemo}" ><span style="width:150px;">${chkstom.vmemo}</span></td>
										
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>			
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
			</form>
			<form id="printForm" action="<%=path%>/chksto/printChkstom.do" method="post">
				<input type="hidden" id="printpk" name="pk_chksto" value="${pk_chksto}"/>
				<input type="hidden" id="istate" name="istate" value="-1"/>
			</form>
		</div>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/chksto/queryAllChkstods.do?pk_chksto=${pk_chksto}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
		        $("#vbillno").focus();
				//快捷键操作（Alt+*）
				$(document).keydown(function (e) {
				    var doPrevent;
				    if (event.keyCode==78 && event.altKey) {//alt+N自制
				    	if(!$('#window_supply').html()){
				    		addchkstom();
				    	}
				    }else if(event.keyCode==77 && event.altKey){//alt+M来自报货模版
				    	if(!$('#window_supply').html()){
				    		addchkstomtemplet();
				    	}
				    }else if(event.keyCode==85 && event.altKey){//alt+U
				    	if(!$('#window_supply').html()){
				    		updatechkstom();
				    	}
				    }else{
				        doPrevent = false;
				    }
				    if (doPrevent)
				        e.preventDefault();
				}); 
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				
				$("#bdat").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edat\')}'});
				});
				$("#edat").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdat\')}'});
				});
				
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"选择组织",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						single:true
					});
				});
				
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),105);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				//分页
			 	$(".page").css("margin-bottom",$(".mainFrame").height()-5);
				//刷新字表
				$('.grid .table-body tr').live('click',function(){
					var pk_chksto=$(this).find('td:eq(1)').find('input').attr('value');
					 $('#pk_chksto').val(pk_chksto);
					 $("#printpk").val(pk_chksto);
					 var url="<%=path%>/chksto/queryAllChkstods.do?pk_chksto="+pk_chksto;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
			   //光标移动
			   new tabTableInput("tblGrid","text"); 
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								$("#wait2").css("display","block");
								$("#wait").css("display","block");
								$("#vbillno").val(stripscript($("#vbillno").val()));
								$('#listForm').submit();
							}
						},{
							text: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							items:[{
									text: '<fmt:message key="Self_add" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['0px','0px']
									},
									handler: function(){
										addchkstom();
									}
								},{
									text: '报货模板',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['0px','0px']
									},
									handler: function(){
										addchkstomtemplet();
									}
								}]
						},{
							text: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								updatechkstom();
							}
						},{
							text: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								deletechkstom();
							}
						},{
							text: '提交',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'upload')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								commitchkstom();
							}
						},{
							text: '<fmt:message key="print" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								printchkstom();
							}
						},{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});	
			
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			//新增报货单
			function addchkstom(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" />报货单',
					content: '<iframe id="saveChkstom" frameborder="0" src="<%=path%>/chksto/toAddChkstom.do"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveChkstom')&&window.document.getElementById("saveChkstom").contentWindow.validate._submitValidate()){
										window.document.getElementById("saveChkstom").contentWindow.saveChkstom();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增采购订单==来自清单
			function addchkstomtemplet(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" />报货单',
					content: '<iframe id="addchkstomtemplet" frameborder="0" src="<%=path%>/chksto/toAddChkstomTemplete.do"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('addchkstomtemplet')&&window.document.getElementById("addchkstomtemplet").contentWindow.validate._submitValidate()){
										window.document.getElementById("addchkstomtemplet").contentWindow.saveChkstom();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//修改报货单
			function updatechkstom(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var istate = checkboxList.filter(':checked').closest('tr').find('td:eq(4)').find('span').attr('istate');
					if(istate==1){
						alerterror('该报货已提交，无法修改！');
						return;
					}
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" />报货单',
						content: '<iframe id="updateChkstom" frameborder="0" src="<%=path%>/chksto/toUpdateChkstom.do?pk_chksto='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateChkstom')&&window.document.getElementById("updateChkstom").contentWindow.validate._submitValidate()){
											window.document.getElementById("updateChkstom").contentWindow.updateChkstom();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除
			function deletechkstom(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					var checkdata = true;
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						var tr = $(this).closest('tr');
						var num = tr.find('td:first').text();
						if(tr.find('td:eq(4)').find('span').attr('istate')==1){
							checkdata = false;
							alerterror('第'+num+'条数据已提交，无法删除！');
							return false;
						}
						chkValue.push($(this).val());
					});
					if(checkdata){
						alertconfirm('<fmt:message key="delete_data_confirm" />？',function(){
							var action = '<%=path%>/chksto/deleteChkstom.do?pks='+chkValue.join(",");
							$('body').window({
								title: '<fmt:message key="delete" /><fmt:message key="Order_list" />',
								content: '<iframe frameborder="0" src='+action+'></iframe>',
								width: '500px',
								height: '245px',
								draggable: true,
								isModal: true
							});
						});
					};
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			
			//报货单提交
			function commitchkstom(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox:checked');
				var checkdata = true;
				var checkedarray = [];
				if(checkboxList.length!=0){
					var chkValue = [];
					checkboxList.each(function(){
						var tr = $(this).closest('tr');
						var num = tr.find('td:first').text();
						if(tr.find('td:eq(4)').find('span').attr('istate')==1){
							checkdata = false;
							alerterror('第'+num+'条数据已提交！');
							return false;
						}
						chkValue.push($(this).val());
						checkedarray.push(num);
					});
				}else{
					alerterror('未选择任何数据！');
					checkdata = false;
				}
				if(checkdata){
					var param = {};
					param['pks'] = chkValue.join(",");
					$.ajaxSetup({ 
						  async: false 
					});
					$.post('<%=path%>/chksto/commitChkstom.do',param,function(re){
						if(re==true){
							alerttipsbreak('提交成功！',function(){
								$("#listForm").submit();
							});
						}else{
							var index = re.split(':')[0];
							var info = re.split(':')[1];
							var mname = re.split(':')[2];
							if(info=='supplier'){
								alerterror('第'+checkedarray[index]+'条报货单物资【'+mname+'】无供应商！');
							}
							if(info=='price'){
								alerterror('第'+checkedarray[index]+'条报货单物资【'+mname+'】无单价！');
							}
						}
					});
				}
			}
			
			//打印报货单
			function printchkstom(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					$("#printForm").attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					$("#printForm").submit();
				}else{
					alerterror('请选择一条报货单！');
				}
				
			}
			
			function reloadPage(){
				$("#listForm").submit();
			}
		</script>
	</body>
</html>