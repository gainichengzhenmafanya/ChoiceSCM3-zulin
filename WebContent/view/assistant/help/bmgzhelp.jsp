<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <title>采购订单帮助样例</title><!--[if gte mso 9]>
    <xml>
        <w:WordDocument>
            <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
            <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
            <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
            <w:DocumentKind>DocumentNotSpecified</w:DocumentKind>
            <w:DrawingGridVerticalSpacing>7.8</w:DrawingGridVerticalSpacing>
            <w:View>Web</w:View>
            <w:Compatibility>
                <w:DontGrowAutofit/>
                <w:BalanceSingleByteDoubleByteWidth/>
                <w:DoNotExpandShiftReturn/>
                <w:UseFELayout/>
            </w:Compatibility>
            <w:Zoom>0</w:Zoom>
        </w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true" DefSemiHidden="true" DefQFormat="false"
                        DefPriority="99" LatentStyleCount="260">
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Normal"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="heading 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="footnote text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="annotation text"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="header"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="footer"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="35" SemiHidden="false" QFormat="true"
                            Name="caption"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="table of figures"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="envelope address"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="envelope return"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="footnote reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="annotation reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="line number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="page number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="endnote reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="endnote text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="table of authorities"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="macro"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="toa heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="10" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Title"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Closing"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Signature"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Default Paragraph Font"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Message Header"></w:LsdException>
            <w:LsdException Locked="false" Priority="11" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Subtitle"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Salutation"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Date"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="Body Text First Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="Body Text First Indent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Note Heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Block Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Hyperlink"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="FollowedHyperlink"></w:LsdException>
            <w:LsdException Locked="false" Priority="22" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Strong"></w:LsdException>
            <w:LsdException Locked="false" Priority="20" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Emphasis"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Document Map"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Plain Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="E-mail Signature"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal (Web)"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Acronym"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Address"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Cite"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Code"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Definition"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Keyboard"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Preformatted"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Sample"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Typewriter"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Variable"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal Table"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="annotation subject"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="No List"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Contemporary"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Elegant"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Professional"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Subtle 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Subtle 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Balloon Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Theme"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 6"></w:LsdException>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        @font-face {
            font-family: "Times New Roman";
        }

        @font-face {
            font-family: "宋体";
        }

        @font-face {
            font-family: "Wingdings";
        }

        @font-face {
            font-family: "Arial";
        }

        @font-face {
            font-family: "黑体";
        }

        @font-face {
            font-family: "微软雅黑";
        }

        @list l0:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F0B2;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l1:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l2:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l3:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F0B2;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l4:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F076;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l5:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F076;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l6:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 39.0000pt;
            mso-level-number-positn: left;
            margin-left: 39.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l7:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 81.0000pt;
            mso-level-number-positn: left;
            margin-left: 81.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l8:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 18.0000pt;
            mso-level-number-positn: left;
            margin-left: 18.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l9:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 18.0000pt;
            mso-level-number-positn: left;
            margin-left: 18.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l10:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 60.0000pt;
            mso-level-number-positn: left;
            margin-left: 60.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l11:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 60.0000pt;
            mso-level-number-positn: left;
            margin-left: 60.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l12:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 39.0000pt;
            mso-level-number-positn: left;
            margin-left: 39.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l13:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 102.0000pt;
            mso-level-number-positn: left;
            margin-left: 102.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l14:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 81.0000pt;
            mso-level-number-positn: left;
            margin-left: 81.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l15:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 102.0000pt;
            mso-level-number-positn: left;
            margin-left: 102.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        p.p0 {
            margin: 0pt;
            margin-bottom: 0.0001pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            text-align: justify;
            font-size: 10.5000pt;
            font-family: 'Times New Roman';
        }

        h2 {
            mso-para-margin-top: 0.0000gd;
            margin-top: 13.0000pt;
            mso-para-margin-bottom: 0.0000gd;
            margin-bottom: 13.0000pt;
            page-break-after: void;
            text-align: justify;
            line-height: 172%;
            font-weight: bold;
            font-size: 16.0000pt;
            font-family: 'Arial';
        }

        h3 {
            mso-para-margin-top: 0.0000gd;
            margin-top: 13.0000pt;
            mso-para-margin-bottom: 0.0000gd;
            margin-bottom: 13.0000pt;
            page-break-after: void;
            text-align: justify;
            line-height: 172%;
            font-weight: bold;
            font-size: 16.0000pt;
            font-family: 'Times New Roman';
        }

        span

        .10
        {
            font-family: 'Times New Roman'
        ;
        }

        table.MsoNormalTable {
            mso-style-name: 普通表格;
            mso-style-parent: "";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-padding-alt: 0.0000pt 5.4000pt 0.0000pt 5.4000pt;
        }

        span

        .15
        {
            font-weight: bold
        ;
            font-size: 16.0000pt
        ;
            font-family: 'Times New Roman'
        ;
        }

        span

        .16
        {
            font-family: 'Times New Roman'
        ;
        }

        span

        .17
        {
            font-weight: bold
        ;
            font-size: 16.0000pt
        ;
            font-family: 'Arial'
        ;
        }

        p.p18 {
            margin-top: 0pt;
            margin-bottom: 0pt;
            border-top: none;;
            mso-border-top-alt: none;;
            border-right: none;;
            mso-border-right-alt: none;;
            border-bottom: none;;
            mso-border-bottom-alt: none;;
            border-left: none;;
            mso-border-left-alt: none;;
            padding: 1pt 4pt 1pt 4pt;
            layout-grid-mode: char;
            text-align: justify;
            font-size: 9.0000pt;
            font-family: 'Times New Roman';
        }

        p.p19 {
            margin-top: 0pt;
            margin-bottom: 0pt;
            layout-grid-mode: char;
            text-align: left;
            font-size: 9.0000pt;
            font-family: 'Times New Roman';
        }

        p.p20 {
            margin-top: 0pt;
            margin-bottom: 6.0000pt;
            text-align: justify;
            font-size: 10.5000pt;
            font-family: 'Times New Roman';
        }

        p.p21 {
            text-indent: 10.5000pt;
            margin-top: 0pt;
            margin-bottom: 6.0000pt;
            text-align: justify;
            font-size: 10.5000pt;
            font-family: 'Times New Roman';
        }

        @page {
            mso-page-border-surround-header: no;
            mso-page-border-surround-footer: no;
        }

        @page Section0 {
            margin-top: 72.0000pt;
            margin-bottom: 72.0000pt;
            margin-left: 59.0000pt;
            margin-right: 84.3000pt;
            size: 595.3000pt 841.9000pt;
            layout-grid: 15.6000pt;
        }

        div.Section0 {
            page: Section0;
        }</style>
</head>
<body style="tab-interval:21pt; text-justify-trim:punctuation; "><!--StartFragment-->
<div class="Section0" style="layout-grid:15.6000pt; ">
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:center; "><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:22.0000pt; font-family:'微软雅黑'; ">编码规则帮助样例</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:22.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; ">概述</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">此功能主要用于规范及约束基础数据及业务单据编码规则，通过全面的编码预置定义将企业信息进行有效、合理地统一。通过编码规则定义可以达到系统业务规范化、基础数据合理化、系统统一性的作用。</span><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="z-index:1; left:0px; margin-left:-2.1333px; margin-top:18.4000px; width:642.0000px; height:2.0000px; "><img
            width="642" height="2" src="<%=path%>/view/assistant/help/img/41.png"></span><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'微软雅黑'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; ">产品界面</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><img width="630" height="308"
                                                                                  src="<%=path%>/view/assistant/help/img/113.png"><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'微软雅黑'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;进入节点：系统设置------------------编码规则。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; ">产品功能</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.0000pt; text-indent:-21.0000pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l5 level1 lfo1; ">
        <![if !supportLists]><span
            style="color:rgb(0,0,255); font-weight:bold; font-size:12.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#118;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:12.0000pt; font-family:'微软雅黑'; ">功能元素</span><span
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:9.8700pt; text-indent:10.5000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#178;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; ">业务单据编码规则：</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">招募书单据号：预设为---------ZM20141203001</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">付款单据号：预设为-----FK20141203001</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">验货单单据号：预设为-----YH20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">退货单单据号：预设为-----TH20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">采购订单单据号：预设为-----CG20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">采购合约单据号:：预设为-----HY20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">到货单单据号：预设为-----DH20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">其他应付单单据号：预设为-----QT20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">报货单单号：预设为-----BH20141203</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:0pt; "><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-size:10.0000pt; font-family:'微软雅黑'; ">说明：业务单据编码规则关键要素包含编码级数、长度、类型、值。&nbsp;业务单据编码级数本质为单据规则组成方式。长度代表每种组成方式的可输入位数，类型代表输入值的类型，值代表具体的输入数值。</span><span
            class="15"
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:9.8700pt; text-indent:10.5000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#178;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; ">基础数据编码规则：</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">组织编码：预设为-------X--XX--XX</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">供应商编码：预设为--X---XX--XX--XXX</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">供应商类别：预设为---X--XX--XX</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">物资编码：预设为---X---XX--XX---XXX</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="text-indent:21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l1 level1 lfo3; "><![if
        !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">物资分类：预设为---X--XX--XX</span><span
                style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; ">注意事项</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:10.8150pt; text-indent:0.1000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l3 level1 lfo4; ">
        <![if !supportLists]><span style="color:rgb(0,0,255); font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#178;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-size:10.0000pt; font-family:'微软雅黑'; ">编码定义</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:20.5800pt; text-indent:0.2000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-weight:normal; font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; ">编码可以分为基础数据编码与业务单据编码</span><span
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">编码定义规则可以根据企业实际需求进行系统编码规则定义设置。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">编码规则定义生效后，系统按预置规则进行。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">编码关键要素分为级别、类型、长度、值，综合组成整个编码样例。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21
       style="margin-left:10.8150pt; text-indent:0.1000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l3 level1 lfo4; ">
        <![if !supportLists]><span style="color:rgb(0,0,255); font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#178;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-size:10.0000pt; font-family:'微软雅黑'; ">规范说明</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:20.5800pt; text-indent:0.2000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-weight:normal; font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; ">编码规则定义单据号及基础数据定义时必须符合系统预设规则。</span><span
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">如将正在使用的编码规则修改或是停用，请先咨询系统管理员。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">如基础数据为外部导入，请预先设置与模板信息对应的编码规则。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21
       style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; mso-list:l2 level1 lfo5; ">
        <![if !supportLists]><span style="font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#108;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">编码规则一旦预设，单据号及基础数据严格按照编码规则进行。</span><span
            class="15" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'微软雅黑'; ">案例说明</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21
       style="margin-left:21.0000pt; text-indent:-21.0000pt; margin-top:0pt; margin-bottom:0pt; mso-list:l4 level1 lfo6; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#118;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; ">编码规则案例</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">例如：&nbsp;组织机构：37--01--01&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;规则为：XX---XX--XX</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;编码级别：三级</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一级类别：国家、地区。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;二级类别：区域、品牌。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三级类别：公司。&nbsp;&nbsp;</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">例如：&nbsp;采购订单单据号：&nbsp;&nbsp;CG20141204001&nbsp;&nbsp;&nbsp;规则为：XX---XXXXXXXX---XXX</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;编码级别：三级</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一级类别：单据类型标记。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;二级类别：年月日。</span><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span></p>
    <p class=p21 style="margin-left:21.0000pt; margin-top:0pt; margin-bottom:6.0000pt; text-align:justify; "><span
            style="mso-spacerun:'yes'; font-size:10.0000pt; font-family:'微软雅黑'; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;三级类别：流水号。&nbsp;</span><span
            class="15" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.0000pt; font-family:'微软雅黑'; "><o:p></o:p></span>
    </p>
</div>
<!--EndFragment--></body>
</html>