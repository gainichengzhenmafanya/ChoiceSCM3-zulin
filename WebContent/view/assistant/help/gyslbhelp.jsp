<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <title>采购合约帮助样例</title><!--[if gte mso 9]>
    <xml>
        <w:WordDocument>
            <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
            <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
            <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
            <w:DocumentKind>DocumentNotSpecified</w:DocumentKind>
            <w:DrawingGridVerticalSpacing>7.8</w:DrawingGridVerticalSpacing>
            <w:View>Web</w:View>
            <w:Compatibility>
                <w:DontGrowAutofit/>
                <w:BalanceSingleByteDoubleByteWidth/>
                <w:DoNotExpandShiftReturn/>
                <w:UseFELayout/>
            </w:Compatibility>
            <w:Zoom>0</w:Zoom>
        </w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true" DefSemiHidden="true" DefQFormat="false"
                        DefPriority="99" LatentStyleCount="260">
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Normal"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="heading 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false" QFormat="true"
                            Name="heading 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="39" SemiHidden="false" Name="toc 9"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="footnote text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="annotation text"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="header"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="footer"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="index heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="35" SemiHidden="false" QFormat="true"
                            Name="caption"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="table of figures"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="envelope address"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="envelope return"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="footnote reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="annotation reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="line number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="page number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="endnote reference"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="endnote text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="table of authorities"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="macro"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="toa heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Bullet 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Number 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="10" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Title"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Closing"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Signature"></w:LsdException>
            <w:LsdException Locked="false" Priority="0" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Default Paragraph Font"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="List Continue 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Message Header"></w:LsdException>
            <w:LsdException Locked="false" Priority="11" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Subtitle"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Salutation"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Date"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="Body Text First Indent"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false"
                            Name="Body Text First Indent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Note Heading"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Body Text Indent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Block Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Hyperlink"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="FollowedHyperlink"></w:LsdException>
            <w:LsdException Locked="false" Priority="22" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Strong"></w:LsdException>
            <w:LsdException Locked="false" Priority="20" SemiHidden="false" UnhideWhenUsed="false" QFormat="true"
                            Name="Emphasis"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Document Map"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Plain Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="E-mail Signature"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal (Web)"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Acronym"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Address"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Cite"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Code"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Definition"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Keyboard"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Preformatted"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Sample"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Typewriter"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="HTML Variable"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Normal Table"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="annotation subject"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="No List"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Simple 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Classic 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Colorful 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Columns 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 7"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table List 8"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table 3D effects 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Contemporary"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Elegant"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Professional"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Subtle 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Subtle 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Web 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Balloon Text"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="99" SemiHidden="false" Name="Table Theme"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 1"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 2"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 3"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 4"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 5"></w:LsdException>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Shading Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Light Grid Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Shading 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium List 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 1 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 2 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Medium Grid 3 Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Dark List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Shading Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful List Accent 6"></w:LsdException>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false" UnhideWhenUsed="false"
                            Name="Colorful Grid Accent 6"></w:LsdException>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        @font-face {
            font-family: "Times New Roman";
        }

        @font-face {
            font-family: "宋体";
        }

        @font-face {
            font-family: "Wingdings";
        }

        @font-face {
            font-family: "Arial";
        }

        @font-face {
            font-family: "黑体";
        }

        @font-face {
            font-family: "Helvetica";
        }

        @font-face {
            font-family: "Arial Unicode MS";
        }

        @list l0:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F0D8;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l1:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F076;
            mso-level-tab-stop: 21.0000pt;
            mso-level-number-positn: left;
            margin-left: 21.0000pt;
            text-indent: -21.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l2:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 39.0000pt;
            mso-level-number-positn: left;
            margin-left: 39.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l3:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 81.0000pt;
            mso-level-number-positn: left;
            margin-left: 81.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l4:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 18.0000pt;
            mso-level-number-positn: left;
            margin-left: 18.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l5:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 18.0000pt;
            mso-level-number-positn: left;
            margin-left: 18.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l6:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 60.0000pt;
            mso-level-number-positn: left;
            margin-left: 60.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l7:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 60.0000pt;
            mso-level-number-positn: left;
            margin-left: 60.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l8:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 39.0000pt;
            mso-level-number-positn: left;
            margin-left: 39.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l9:level1 {
            mso-level-number-format: bullet;
            mso-level-suffix: tab;
            mso-level-text: \F06C;
            mso-level-tab-stop: 102.0000pt;
            mso-level-number-positn: left;
            margin-left: 102.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Wingdings';
        }

        @list l10:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 81.0000pt;
            mso-level-number-positn: left;
            margin-left: 81.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        @list l11:level1 {
            mso-level-number-format: decimal;
            mso-level-suffix: tab;
            mso-level-text: "%1.";
            mso-level-tab-stop: 102.0000pt;
            mso-level-number-positn: left;
            margin-left: 102.0000pt;
            text-indent: -18.0000pt;
            margin-top: 0pt;
            margin-bottom: 0pt;
            font-family: 'Times New Roman';
        }

        p.p0 {
            text-align: justify;
            font-size: 10.5000pt;
            font-family: 'Times New Roman';
        }

        h2 {
            mso-para-margin-top: 0.0000gd;
            margin-top: 13.0000pt;
            mso-para-margin-bottom: 0.0000gd;
            margin-bottom: 13.0000pt;
            page-break-after: void;
            text-align: justify;
            line-height: 172%;
            font-weight: bold;
            font-size: 16.0000pt;
            font-family: 'Arial';
        }

        h3 {
            mso-para-margin-top: 0.0000gd;
            margin-top: 13.0000pt;
            mso-para-margin-bottom: 0.0000gd;
            margin-bottom: 13.0000pt;
            page-break-after: void;
            text-align: justify;
            line-height: 172%;
            font-weight: bold;
            font-size: 16.0000pt;
            font-family: 'Times New Roman';
        }

        span

        .10
        {
            font-family: 'Times New Roman'
        ;
        }

        table.MsoNormalTable {
            mso-style-name: 普通表格;
            mso-style-parent: "";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-padding-alt: 0.0000pt 5.4000pt 0.0000pt 5.4000pt;
        }

        span

        .15
        {
            font-weight: bold
        ;
            font-size: 16.0000pt
        ;
            font-family: 'Arial'
        ;
        }

        span

        .16
        {
            font-weight: bold
        ;
            font-size: 16.0000pt
        ;
            font-family: 'Times New Roman'
        ;
        }

        p.p17 {
            margin-top: 0.0000pt;
            margin-bottom: 0.0000pt;
            text-align: left;
            color: rgb(0, 0, 0);
            letter-spacing: 0.0000pt;
            font-size: 12.0000pt;
            font-family: 'Helvetica';
        }

        p.p18 {
            margin-top: 0.0000pt;
            margin-bottom: 0.0000pt;
            text-align: justify;
            color: rgb(0, 0, 0);
            letter-spacing: 0.0000pt;
            font-size: 10.5000pt;
            font-family: 'Times New Roman';
        }

        p.p19 {
            margin-top: 0pt;
            margin-bottom: 0pt;
            layout-grid-mode: char;
            text-align: left;
            font-size: 9.0000pt;
            font-family: 'Times New Roman';
        }

        p.p20 {
            margin-top: 0pt;
            margin-bottom: 0pt;
            border-top: none;;
            mso-border-top-alt: none;;
            border-right: none;;
            mso-border-right-alt: none;;
            border-bottom: none;;
            mso-border-bottom-alt: none;;
            border-left: none;;
            mso-border-left-alt: none;;
            padding: 1pt 4pt 1pt 4pt;
            layout-grid-mode: char;
            text-align: justify;
            font-size: 9.0000pt;
            font-family: 'Times New Roman';
        }

        @page {
            mso-page-border-surround-header: no;
            mso-page-border-surround-footer: no;
        }

        @page Section0 {
            margin-top: 72.0000pt;
            margin-bottom: 72.0000pt;
            margin-left: 59.0000pt;
            margin-right: 64.3000pt;
            size: 595.3000pt 841.9000pt;
            layout-grid: 15.6000pt;
        }

        div.Section0 {
            page: Section0;
        }</style>
</head>
<body style="tab-interval:21pt; text-justify-trim:punctuation; "><!--StartFragment-->
<div class="Section0" style="layout-grid:15.6000pt; ">
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:center; "><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:22.0000pt; font-family:'宋体'; ">供应商列表</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:22.0000pt; font-family:'宋体'; ">帮助样例</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:22.0000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; ">概述</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p18
       style="margin-left:10.9000pt; text-indent:-10.9200pt; margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; color:rgb(0,0,0); letter-spacing:0.0000pt; font-size:10.5000pt; font-family:'宋体'; ">&nbsp;&nbsp;</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,0); letter-spacing:0.0000pt; font-size:10.5000pt; font-family:'宋体'; ">供应商列表（供应商管理），可以对供应商进行一个统一的系统化管理。可以一目了然供应商基本心、适用的门店与供应商物资范围。</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,0); letter-spacing:0.0000pt; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="z-index:1; left:0px; margin-left:-2.1333px; margin-top:18.4000px; width:642.0000px; height:2.0000px; "><img
            width="642" height="2" src="<%=path%>/view/assistant/help/img/41.png"></span><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; ">产品界面</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><img width="581" height="338"
                                                                                  src="<%=path%>/view/assistant/help/img/82.png"
                                                                                  align="left" hspace="12"><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p>&nbsp;</o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">&nbsp;&nbsp;注：供应商【适用门店】与【供应物资范围】在创建合约报价的时候会用到！</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">&nbsp;&nbsp;进入节点：供应商管理&#8212;&#8212;供应商列表</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0 style="text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; ">产品功能</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0
       style="margin-left:21.0000pt; text-indent:-21.0000pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l1 level1 lfo1; ">
        <![if !supportLists]><span
            style="color:rgb(0,0,255); font-weight:bold; font-size:12.0000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#118;<span>&nbsp;</span></span></span><![endif]><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:12.0000pt; font-family:'宋体'; ">功能元素</span><span
            style="mso-spacerun:'yes'; color:rgb(0,0,255); font-weight:bold; font-size:12.0000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">查询：</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">根据编码、名称、助记码、启用状态查询供应商信息</span><span
            class="16"
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">。</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">新增：</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">新增供应商，关联商城供应商</span><span
            class="16"
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">。</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">修改：</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">除了供应商编码，供应商其他信息都可以修改</span><span
            class="16"
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">。</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">删除：</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">只能删除未引用的供应商</span><span
            class="16"
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">。</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:bold; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:bold; font-size:10.5000pt; font-family:'宋体'; ">启用：</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">可以对供应商进行启用禁用操作</span><span
            class="16"
            style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">。</span><span
            style="mso-spacerun:'yes'; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0 style="margin-top:0pt; margin-bottom:0pt; text-align:left; "><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; ">注意事项</span><span
            style="mso-spacerun:'yes'; font-weight:bold; font-size:14.0000pt; font-family:'宋体'; "><o:p></o:p></span></p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:normal; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">适用门店与供应物资范围在创建采购合约的时候会用到，具体请查阅采购合约帮助</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
    <p class=p0
       style="margin-left:21.5250pt; text-indent:-10.6050pt; margin-top:0pt; margin-bottom:0pt; text-align:left; mso-list:l0 level1 lfo2; ">
        <![if !supportLists]><span style="font-weight:normal; font-size:10.5000pt; font-family:'Wingdings'; "><span
            style='mso-list:Ignore; '>&#216;<span>&nbsp;</span></span></span><![endif]><span class="16"
                                                                                             style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; ">关联商城供应商可以带出供应商部分供应商基本信息。</span><span
            class="16" style="mso-spacerun:'yes'; font-weight:normal; font-size:10.5000pt; font-family:'宋体'; "><o:p></o:p></span>
    </p>
</div>
<!--EndFragment--></body>
</html>