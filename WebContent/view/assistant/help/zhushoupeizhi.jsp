<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=gb2312">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:宋体;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"\@宋体";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	font-size:10.5pt;
	font-family:"Times New Roman",serif;}
h3
	{mso-style-link:"标题 3 Char";
	margin-top:13.0pt;
	margin-right:0cm;
	margin-bottom:13.0pt;
	margin-left:0cm;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:172%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Times New Roman",serif;}
span.3Char
	{mso-style-name:"标题 3 Char";
	mso-style-link:"标题 3";
	font-family:"Times New Roman",serif;
	font-weight:bold;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	layout-grid:15.6pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ZH-CN style='text-justify-trim:punctuation'>

<div class=WordSection1 style='layout-grid:15.6pt'>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:22.0pt;font-family:宋体;color:blue'>助手配置帮助</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251663360;margin-left:-8px;margin-top:7px;width:25px;
height:25px'></span><b><span
lang=EN-US style='font-size:14.0pt'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>概述</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp; </span><span
style='font-family:宋体'>此功能主要用于采购助手中企业的基本信息维护、超级关联员的密码修改、商城接口登录的密码修改、企业会计期的修改。</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
relative;z-index:251659264;left:-2px;top:17px;width:641px;height:20px'></span><span lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251661312;margin-left:-7px;margin-top:10px;width:24px;
height:23px'></span><span
style='positn:absolute;z-index:251660288;margin-left:-7px;margin-top:9px;
width:24px;height:23px'><img width=24 height=23 src="助手配置.files/image003.png"></span><b><span
lang=EN-US style='font-size:14.0pt;font-family:宋体'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>产品界面</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US><img
width=630 height=313 id="图片 1" src="<%=path%>/view/assistant/help/img/zhushoupeizhi.jpg"></span></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-family:宋体'>&nbsp; </span></b><b><span style='font-family:宋体'>进入节点：系统设置<span
lang=EN-US>-----------------</span>助手配置。</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251662336;margin-left:-9px;margin-top:8px;width:24px;
height:23px'></span><b><span
lang=EN-US style='font-size:14.0pt'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>产品功能</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span lang=EN-US style='font-size:12.0pt;font-family:Wingdings;
color:blue'>v<span style='font:7.0pt "Times New Roman"'> </span></span><b><span
style='font-size:12.0pt;font-family:宋体;color:blue'>功能元素</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.55pt;text-align:left;
text-indent:-10.65pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体'>基本信息的保存：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>修改企业名称、主卡号、联系人、联系电话、企业地址、注册资金等信息，编码不能修改。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.55pt;text-align:left;
text-indent:-10.65pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体'>超级管理员的保存：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>修改管理员<span lang=EN-US>admin</span>的登录密码。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.55pt;text-align:left;
text-indent:-10.65pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体'>会计期的保存：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>修改企业的会计期的设置。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.55pt;text-align:left;
text-indent:-10.65pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体'>商城登录接口参数修改：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>修改登录商城接口的用户名、密码</span></span></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251664384;margin-left:-5px;margin-top:8px;width:24px;
height:23px'></span><b><span
lang=EN-US style='font-size:14.0pt;font-family:宋体'>&nbsp;&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>注意事项</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.5pt;text-align:left;
text-indent:-10.6pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>企业号不能修改。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.5pt;text-align:left;
text-indent:-10.6pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>超级管理员密码修改的是<span lang=EN-US>admin</span>的密码。</span></span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

</div>

</body>

</html>
