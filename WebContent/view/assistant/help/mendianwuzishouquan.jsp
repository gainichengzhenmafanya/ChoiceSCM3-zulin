<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=gb2312">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:宋体;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"\@宋体";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	font-size:10.5pt;
	font-family:"Times New Roman",serif;}
h3
	{mso-style-link:"标题 3 Char";
	margin-top:13.0pt;
	margin-right:0cm;
	margin-bottom:13.0pt;
	margin-left:0cm;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:172%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	text-indent:21.0pt;
	font-size:10.5pt;
	font-family:"Times New Roman",serif;}
span.3Char
	{mso-style-name:"标题 3 Char";
	mso-style-link:"标题 3";
	font-family:"Times New Roman",serif;
	font-weight:bold;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	layout-grid:15.6pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ZH-CN style='text-justify-trim:punctuation'>

<div class=WordSection1 style='layout-grid:15.6pt'>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:22.0pt;font-family:宋体;color:blue'>门店物资授权帮助</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-size:14.0pt'>&nbsp; </span></b><b><span style='font-size:14.0pt;
font-family:宋体'>概述</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp; </span><span
style='font-family:宋体'>此功能主要用于供应商招募的数据维护，发布到商城后可以对应募的供应商进行管理。</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
relative;z-index:251659264;left:-2px;top:17px;width:641px;height:20px'></span><span lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-size:14.0pt;font-family:宋体'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>产品界面</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US><img
width=647 height=305 id="图片 1" src="<%=path%>/view/assistant/help/img/mendianwuzishouquan.jpg"></span></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-family:宋体'>&nbsp; </span></b><b><span style='font-family:宋体'>进入节点：物资管理<span
lang=EN-US>------------------</span>门店物资授权。</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-size:14.0pt'>&nbsp; </span></b><b><span style='font-size:14.0pt;
font-family:宋体'>产品功能</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-size:12.0pt;
font-family:Wingdings;color:blue;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>
</span></span></span><b><span style='font-size:12.0pt;font-family:宋体;
color:blue'>功能元素</span></b><span class=3Char><span style='font-family:宋体;
font-weight:normal'> </span></span></p>

<p class=MsoListParagraph style='margin-left:21.0pt;text-indent:-21.0pt'><span
class=3Char><span lang=EN-US style='font-family:Wingdings;font-weight:normal'>v<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><span
class=3Char><span style='font-family:宋体'>查询：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>可以根据名称查询组织信息。</span></span></p>

<p class=MsoListParagraph style='margin-left:21.0pt;text-indent:-21.0pt'><span
class=3Char><span lang=EN-US style='font-family:Wingdings;font-weight:normal'>v<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><span
class=3Char><span style='font-family:宋体'>添加：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>新增门店的物资权限，在新增订单时会根据选择的采购组织过滤可以使用的物资。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span lang=EN-US style='font-family:Wingdings'>v<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span class=3Char><span
style='font-family:宋体'>删除：</span></span><span class=3Char><span
style='font-family:宋体;font-weight:normal'>删除门店分配的物资，可多选删除。</span></span></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-size:14.0pt;font-family:宋体'>&nbsp;&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>注意事项</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>&Oslash;<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体;font-weight:
normal'>编码根据编码规则生成，生成规则可以到编码规则中修改。</span></span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

</div>

</body>

</html>
