<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=gb2312">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:宋体;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:宋体;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"\@宋体";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	font-size:10.5pt;
	font-family:"Times New Roman",serif;}
h3
	{mso-style-link:"标题 3 Char";
	margin-top:13.0pt;
	margin-right:0cm;
	margin-bottom:13.0pt;
	margin-left:0cm;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:172%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"页眉 Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	layout-grid-mode:char;
	border:none;
	padding:0cm;
	font-size:9.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"页脚 Char";
	margin:0cm;
	margin-bottom:.0001pt;
	layout-grid-mode:char;
	font-size:9.0pt;
	font-family:"Times New Roman",serif;}
span.3Char
	{mso-style-name:"标题 3 Char";
	mso-style-link:"标题 3";
	font-family:"Times New Roman",serif;
	font-weight:bold;}
span.Char
	{mso-style-name:"页眉 Char";
	mso-style-link:页眉;
	font-family:"Times New Roman",serif;}
span.Char0
	{mso-style-name:"页脚 Char";
	mso-style-link:页脚;
	font-family:"Times New Roman",serif;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	layout-grid:15.6pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=ZH-CN style='text-justify-trim:punctuation'>

<div class=WordSection1 style='layout-grid:15.6pt'>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:22.0pt;font-family:宋体;color:blue'>账号管理帮助</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251663360;margin-left:-8px;margin-top:7px;width:25px;
height:25px'></span><b><span
lang=EN-US style='font-size:14.0pt'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>概述</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp; </span><span
style='font-family:宋体'>此功能主要用于采购助手中登录、操作人的帐号定义，能对该帐号进行角色权限、组织权限、物资类别权限的限制，达成数据的隔离管理。</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
relative;z-index:251659264;left:-2px;top:17px;width:641px;height:20px'></span><span lang=EN-US>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251661312;margin-left:-7px;margin-top:10px;width:24px;
height:23px'></span><span
style='positn:absolute;z-index:251660288;margin-left:-7px;margin-top:9px;
width:24px;height:23px'></span><b><span
lang=EN-US style='font-size:14.0pt;font-family:宋体'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>产品界面</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US><img
width=629 height=312 id="图片 1" src="<%=path%>/view/assistant/help/img/zhanghaoguanli.jpg""></span></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><b><span lang=EN-US
style='font-family:宋体'>&nbsp; </span></b><b><span style='font-family:宋体'>进入节点：系统设置<span
lang=EN-US>------------------</span>帐号管理。</span></b></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251662336;margin-left:-9px;margin-top:8px;width:24px;
height:23px'></span><b><span
lang=EN-US style='font-size:14.0pt'>&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>产品功能</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span lang=EN-US style='font-size:12.0pt;font-family:Wingdings;
color:blue'>v<span style='font:7.0pt "Times New Roman"'> </span></span><b><span
style='font-size:12.0pt;font-family:宋体;color:blue'>功能元素</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>查询：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>根据用户名、账号名称查询账号信息。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>新增：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>新增账号信息，可以新增账号名称、用户名、登录密码、所属组织，是否采购员等信息。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>修改：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>根据实际情况可修改账号信息，账号的用户名不能修改。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>删除：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>通过此功能将账号进行删除，删除后该账号不能登录系统。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>启用<span
lang=EN-US>/</span>禁用：</span></span><span class=3Char><span style='font-family:
宋体;font-weight:normal'>可以修改账号的启用状态，禁用时该账号不能登录系统。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>关联角色：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>对账号进行系统节点授权，账号关联角色后，再次登录系统左侧菜单展示效果会根据关联的角色进行重新组合。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>物资类别授权：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>可对帐号进行物资类别的授权，在订单、物资管理等节点选择物资时会根据物资类别授权过滤没有授权的物资</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>组织授权：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>可对帐号进行组织的授权，根据权限的不同，展示相应的组织结构</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>重置密码：</span></span><span
class=3Char><span style='font-family:宋体;font-weight:normal'>可对选中的帐号进行密码重置，重置密码为<span
lang=EN-US>111111</span></span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体'>退出<span
lang=EN-US> </span>：</span></span><span class=3Char><span style='font-family:
宋体;font-weight:normal'>通过此功能可直接退出该业务节点。<span lang=EN-US>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></p>

<p class=MsoNormal align=left style='text-align:left'><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal align=left style='text-align:left'><span style='positn:
absolute;z-index:251664384;margin-left:-5px;margin-top:8px;width:24px;
height:23px'></span><b><span
lang=EN-US style='font-size:14.0pt;font-family:宋体'>&nbsp;&nbsp; </span></b><b><span
style='font-size:14.0pt;font-family:宋体'>注意事项</span></b></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体;font-weight:
normal'>一个企业下用户名不能重复，</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体;font-weight:
normal'>已经引用的账号不能禁用、删除。</span></span></p>

<p class=MsoNormal align=left style='margin-left:21.0pt;text-align:left;
text-indent:-21.0pt'><span class=3Char><span lang=EN-US style='font-family:
Wingdings;font-weight:normal'>v<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></span><span class=3Char><span style='font-family:宋体;font-weight:
normal'>账号删除、禁用后无法登录。</span></span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

</div>

</body>

</html>
