<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="gyshead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
<!-- 				<td class="num"><span style="width: 20px;"></span></td> -->
<!-- 				<td style="width:30px;"><input type="checkbox" id="chkAll" /> -->
<!-- 				</td> -->
				<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
				<td><span style="width:150px;"><fmt:message key="name" /></span></td>
				<td><span style="width:70px;"><fmt:message key="level" /></span></td>
				<td><span style="width:70px;"><fmt:message key="scsclxdh" /></span></td>
				<td><span style="width:140px;">经营范围</span></td>
				<td><span style="width:140px;">地区</span></td>
				<td><span style="width:140px;">配送范围</span></td>
				<td><span style="width:140px;"><fmt:message key="gyszmymdz" /></span></td>
				<td><span style="width:120px;"><fmt:message key="scscshijian" /></span></td>
				<td><span style="width:50px;"><fmt:message key="gyszmymcz" /></span></td>
			</tr>
		</thead>
	</table>
</div>
<div id="gystable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="collDeliver" items="${collDeliverList}"
				varStatus="status">
				<tr>
					<td><span style="width:70px;text-align: left;">${collDeliver.delivercode}</span></td>
					<td>
						<span style="width:150px;text-align: left;color: blue;" title="${collDeliver.delivername}" onclick="openFavoirtesGysInfo('${collDeliver.delivercode}')">
			 					${collDeliver.delivername}
						</span>
					</td>
					<td><span style="width:70px;text-align: left;">
						<c:if test="${collDeliver.vdeliverlevel==1}">子供应链</c:if> 
						<c:if test="${collDeliver.vdeliverlevel==2}">品牌供应商</c:if>
						<c:if test="${collDeliver.vdeliverlevel==3}">普通供应商</c:if>
						</span>
					</td>
					<td><span style="width:70px;text-align: left;">${collDeliver.vdeliverphone}</span></td>
					<td><span style="width:140px;text-align: left;">${collDeliver.voperatingrange}</span></td>
					<td><span style="width:140px;text-align: left;">${collDeliver.varea}</span></td>
					<td><span style="width:140px;text-align: left;">${collDeliver.vdeliveryarea}</span></td>
					<td><span style="width:140px;text-align: left;">${collDeliver.vdeliveraddress}</span></td>
					<td><span style="width:120px;text-align: left;">${collDeliver.ts}</span></td>
					<td>
						<span style="width:50px;text-align: left;">
							<a style="color:#3D83BA;" onclick="deleteGys('${collDeliver.pk_colldeliver}')">
<%-- 								<input type="button" value='<fmt:message key="gyszmymscsc" />'></input> --%>
								<fmt:message key="gyszmymscsc" />
							</a>
						</span>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<page:page form="queryFrom" page="${pageobj}"></page:page>
<script>

$(".page").css("bottom","3px");
	var x = 22;  
	var y = 20;  
	var MouseEvent = function(e) {
	    this.x = e.pageX;
	    this.y = e.pageY;
	};
	var Mouse = function(e){
	      var kdheight =  $(document).scrollTop();
	        mouse = new MouseEvent(e);
	        leftpos = mouse.x+5;
	        toppos = mouse.y-5;
	};
	$('.hoverDiv').hover(
		function(e){
			Mouse(e);
			$("#spImage").attr("src",$(this).find("input").val());
		    $(".showinfo").css({top:toppos,left:leftpos}).fadeIn(100);
		    widthJudge(e);
		},
		function(){
			$(".showinfo").hide();
		}
	);
	//重新定位图片坐标Y坐标，防止图片溢出浏览器
	function widthJudge(e){  
        var marginHeight = document.documentElement.clientHeight - e.pageY;  
        var imageHeight = $(".showinfo").height(); 
        if(marginHeight < (imageHeight+45)){  
            $(".showinfo").css({top:(e.pageY - (imageHeight-marginHeight)-70 ) + 'px'});  
        }  
    } 
	function openFavoirtesGysInfo(pk){
		if(pk == null || pk == '')
			return;
		$.ajaxSetup({
			async:false
		});
		var buttonTitle="";
		var flag="1";
		var addSpInfo=$('body').window({
			id: 'window_GysInfo',
			title: '<fmt:message key="suppliers" /><fmt:message key="detailInfo" />',
			content: '<iframe id="addGysInfoFrame" frameborder="0" src="<%=path%>/search/toGysInfo.do?vcode='+pk+'&flag=2"></iframe>',
			width: '800px',
			height: '500px',
			draggable: true,
			isModal: true,
			confirmClose:false
		});
	}
</script>
