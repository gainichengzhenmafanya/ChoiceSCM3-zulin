<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="sphead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
				<td><span style="width:220px;"><fmt:message key="name" /></span></td>
				<td><span style="width:70px;"><fmt:message key="unit_of_measure" /></span></td>
<%-- 				<td><span style="width:70px;"><fmt:message key="scscguige" /></span></td> --%>
				<td><span style="width:70px;"><fmt:message key="scscshichangjia" /></span></td>
				<td><span style="width:70px;"><fmt:message key="scscshangchengjia" /></span></td>
				<td><span style="width:70px;"><fmt:message key="scscqidingliang" /></span></td>
				<td><span style="width:150px;"><fmt:message key="scscgongyingshang" /></span></td>
				<td><span style="width:120px;"><fmt:message key="scscshijian" /></span></td>
				<c:if test="${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}">
					<td><span style="width:50px;"><fmt:message key="gyszmymcz" /></span></td>
				</c:if>
			</tr>
		</thead>
	</table>
</div>
<div id="sptable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="collMaterial" items="${collMaterialList}"	varStatus="status">
					<tr>
							<td><span style="width:70px;text-align: left;">${collMaterial.vmaterialcode}</span></td>
							<td>
								<span style="width:220px;text-align: left;color: blue;" title="${collMaterial.vmaterialname}" onclick="openFavoirtesSpInfo('${collMaterial.pk_mallmaterial}')">	
									${collMaterial.vmaterialname}							 	 
								</span>
							</td>
							<td><span style="width:70px;text-align: left;">${collMaterial.vunit}</span></td>
<%-- 							<td><span style="width:70px;text-align: right;">${collMaterial.vmaterialspec}</span></td> --%>
							<td><span style="width:70px;text-align: right;">${collMaterial.nmatprice}</span></td>
							<td><span style="width:70px;text-align: right;">${collMaterial.nmallprice}</span></td>
							<td><span style="width:70px;text-align: right;">${collMaterial.namount}</span></td>
							<td><span style="width:150px;text-align: left;">${collMaterial.vmallsupplier}</span></td>
							<td><span style="width:120px;text-align: left;">${collMaterial.ts}</span></td>
							<c:if test="${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')}">
								<td>
									<span style="width:50px;text-align: center;">
										<a style="color:#3D83BA;" onclick="deleteSp('${collMaterial.pk_collmaterial}')">
	<%-- 										<input type="button" value='<fmt:message key="gyszmymscsc" />'></input> --%>
											<fmt:message key="gyszmymscsc" />
										</a>
									</span>
								</td>
							</c:if>
					</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<page:page form="queryFrom" page="${pageobj}"></page:page>
<script>

$(".page").css("bottom","3px");
	var x = 22;  
	var y = 20;  
	var MouseEvent = function(e) {
	    this.x = e.pageX;
	    this.y = e.pageY;
	};
	var Mouse = function(e){
	      var kdheight =  $(document).scrollTop();
	        mouse = new MouseEvent(e);
	        leftpos = mouse.x+5;
	        toppos = mouse.y-5;
	};
	$('.hoverDiv').hover(
		function(e){
			Mouse(e);
			$("#spImage").attr("src",$(this).find("input").val());
		    $(".showinfo").css({top:toppos,left:leftpos}).fadeIn(100);
		    widthJudge(e);
		},
		function(){
			$(".showinfo").hide();
		}
	);
	//重新定位图片坐标Y坐标，防止图片溢出浏览器
	function widthJudge(e){  
        var marginHeight = document.documentElement.clientHeight - e.pageY;  
        var imageHeight = $(".showinfo").height(); 
        if(marginHeight < (imageHeight+45)){  
            $(".showinfo").css({top:(e.pageY - (imageHeight-marginHeight)-70 ) + 'px'});  
        }  
    } 
	function openFavoirtesSpInfo(pk){
		if(pk == null || pk == '')
			return;
		$.ajaxSetup({
			async:false
		});
		var buttonTitle="";
		var addSpInfo=$('body').window({
			id: 'window_SpInfo',
			title: '<fmt:message key="scscjiashangpininfo" />',
			content: '<iframe id="addSpInfoFrame" frameborder="0" src="<%=path%>/search/toSpInfo.do?vcode='+pk+'&flag=2"></iframe>',
			width: '800px',
			height: '500px',
			draggable: true,
			isModal: true,
			confirmClose:false
		});
	}
</script>