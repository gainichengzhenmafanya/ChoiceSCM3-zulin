<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
	<div class="form">
		<form id="inAuditDeliver" method="post" action="<%=path %>/favorites/addInAuditDeliver.do">
			<div id="sptable" class="table-body">
				<table cellspacing="0" cellpadding="0" style="margin: 10px auto;">
					<tbody>
					<input type="hidden" name="pk_colldeliver" id="pk_id" value="${collDeliverListbygysid.pk_colldeliver}" />
						<tr>
							<td>
								<fmt:message key="scscgysmc" />：<span style="width:150px;" title="${collDeliverListbygysid.delivername}">${collDeliverListbygysid.delivername}</span>
							</td>
							<td rowspan="6"><img src="<%=path %>/image/login/scjtz.png" style="width: 240px;height: 240px;" /></td>
						</tr>
						<tr>
							<td style="width:140px;"><fmt:message key="scscgysjb" />：
								<span style="width:150px;" title="${collDeliverListbygysid.vdeliverlevel}">${collDeliverListbygysid.vdeliverlevel}</span>
							</td>
						</tr>
						<tr>
							<td><fmt:message key="scsclxdh" />：
								<span style="width:50px;" title="${collDeliverListbygysid.vdeliverphone}">${collDeliverListbygysid.vdeliverphone}</span>
							</td>
						</tr>
						<tr>
							<td>经营范围：<span style="width:150px;">${collDeliverListbygysid.voperatingrange}</span></td>
						</tr>
						<tr>
							<td>地区：<span style="width:150px;">${collDeliverListbygysid.varea}</span></td>
						</tr>
						<tr>
							<td>配送范围：<span style="width:150px;">${collDeliverListbygysid.vdeliveryarea}</span></td>
						</tr>
						<tr>
							<td><fmt:message key="gyszmymdz" />：<span style="width:150px;">${collDeliverListbygysid.vdeliveraddress}</span></td>
						</tr>
						<tr>
							<td><fmt:message key="scscshijian" />：<span style="width:150px;">${collDeliverListbygysid.ts}</span></td>
						</tr>
<!-- 						<tr> -->
<%-- 							<td><fmt:message key="scscbzbz" />：<span style="width:150px;">${collDeliverListbygysid.vmemo}</span></td> --%>
<!-- 						</tr> -->
					</tbody>
				</table>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript">
			
			function inDeliver(){
				$('#inAuditDeliver').submit();
			}
		</script>
</body>
</html>
