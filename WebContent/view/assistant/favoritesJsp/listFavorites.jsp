<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.separator{
				display:none !important;
			}
/* 			.page{ */
/* 				margin-bottom: 28px; */
/* 			} */
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			.showinfo{
				display:none;
				left:1px;
				top:1px;
				width:"240px";
				height:"240px";
				positn:fixed; 
				z-index:999999; 
				border: 1px solid #5e4e3f;
			}
		</style>
	</head>
	<body>
		<div class="form" style="float: left;height: 100%;">
			<form id="queryFrom" action="<%=path %>/favorites/list.do">
				<input type="hidden" id="showId" name="showId" />
				<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="z-index:88;" >
					<div title='<fmt:message key="materialscsc"/>'>
						<input type="hidden" id="flag" value="sp" />
						<input type="hidden" id="sp_record"/>
						<input type="hidden" id="sp_pkid"/>
						<div id="spdiv" class="grid">
							
					  	</div>
					</div>
					<div title='<fmt:message key="materialgysgys"/>'>
						<input type="hidden" id="flag" value="gys" />
						<input type="hidden" id="gys_record"/>
						<div id="gysdiv" class="grid">
							
					    </div>
					</div>
				</div>
			</form>
		</div>
		<div class="showinfo" style="display: none;text-align: center;">
			<a>
				<img id="spImage" src="" alt="" style="width:240px;height:240px;" />
			</a>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/validate/bohvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#tabs').tabs({onSelect:function(){
					var tab = $(this).tabs('getSelected');//获取选中的面板
					var content = tab.panel("body");
					var flag = content.find('#flag').val();
					$('#showId').val(flag);
					if(flag=='gys'){
						$("#tabName").val('<fmt:message key="suppliers"/>');
					}
					var grid = content.find('#'+flag+'div');
					var modifyId = $.trim($('#modifyId').val()) ? $.trim($('#modifyId').val()) : null;
					//加载当前页签下的页面
					if(grid.get(0) && !$.trim(grid.html())){
						loadGrid();
					}else{
						pageFunReload();
					}
					function loadGrid(nowPage,pageSize){
						grid.load("<%=path%>/favorites/loadSubPage.do?flag="+flag+"&vname="+$('#vname').val(),{nowPage:nowPage ? nowPage : 1,pageSize:pageSize ? pageSize : 20},function(){
							var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
							grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
							if(grid.find('.table-body').width() > grid.width()){
								grid.scroll(function(){
									content.find('.page').css('margin-left',grid.scrollLeft());
								});
							}
							grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
							grid.find('.table-body').find('tr').hover(
								function(){
									$(this).addClass('tr-over');
								},
								function(){
									$(this).removeClass('tr-over');
								}
							);
						});
					}
					//重写翻页方法
					function pageFunReload(){
						var pageSize = content.find('#pageSize').val();
						pageAction = function(nowPage){
							nowPage ? loadGrid(nowPage,pageSize) : loadGrid(content.find('#gotoPage').val(),pageSize);
						};
						perPageColSizeAction = function(count){
							var val = content.find("#viewPageColSize").val();
							if(isNaN(val) || val == 0){
								alerterror("请输入有效数字");
								$("#viewPageColSize").val(pageSize);
								return ;
							}else if(val>150){
                                alerterror("最大条数不能超过150条");
								$("#viewPageColSize").val(pageSize);
								return ;
							}else{
								loadGrid($('#nowPage').val(),val);
							}
						};
					}
				 }});
				//自动实现滚动条
				setElementHeight('.easyui-tabs',['.form-line'],$(document.body),40);	//计算.easyui-tabs的高度
				setElementHeight('.tabs-panels',['.tabs-header'],$(".easyui-tabs"),1);	//计算tabs-panels的高度
				setElementHeight('.grid',['.form-line'],$(document.body),65);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
// 				$(".grid").css("overflow-y","hidden");

				$('#tabs').css("width",$(document.body).width()*0.99);
// 				$('.tabs-panels').css("height",$(document.body).height()*0.75);
				$('.grid').css("width",$('#tabs').width()*0.50);
				loadGrid();//  自动计算滚动条的js方法
			});
			
			 function pageReload(){
				$('#queryFrom').submit();
			} 
			 $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").removeAttr("checked");
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", "checked");
			     }
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
			 });
			
			$('.showinfo').hover(
					function(){
						$(".showinfo").show();
					},
					function(){
						$(".showinfo").hide();
					}
				);
			function deleteSp(pk){
				$.ajaxSetup({async:true});
				var datas = {},pk_check;
				datas["id"] = pk;
				alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
					$.post('<%=path%>/favorites/deletesp.do',datas,function(data){
						if(data=='T'){
							alerttipsbreak('<fmt:message key="successful_deleted"/>！',function(){
								var flag = $('#showId').val();
								var content = $('#tabs').tabs('getSelected').panel("body");
								var grid =  content.find('#'+flag+'div');
								if(grid.get(0)){
									loadGrid();
									function loadGrid(nowPage,pageSize){
										grid.load("<%=path%>/favorites/loadSubPage.do?flag="+flag,{nowPage:nowPage ? nowPage : 1,pageSize:pageSize ? pageSize : 20},function(){
											var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
											grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
											if(grid.find('.table-body').width() > grid.width()){
												grid.scroll(function(){
													content.find('.page').css('margin-left',grid.scrollLeft());
												});
											}
											grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
											grid.find('.table-body').find('tr').hover(
												function(){
													$(this).addClass('tr-over');
												},
												function(){
													$(this).removeClass('tr-over');
												}
											);
										});
									}
								}
							});
						}
					});
				});
			}
			function deleteGys(pk){
				$.ajaxSetup({async:false});
				var datas = {},pk_check;
				datas["id"] = pk;
				alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
					$.post('<%=path%>/favorites/deletegys.do',datas,function(data){
						if(data=='T'){
							alerttipsbreak('<fmt:message key="successful_deleted"/>！',function(){
								var flag = $('#showId').val();
								var content = $('#tabs').tabs('getSelected').panel("body");
								var grid =  content.find('#'+flag+'div');
								if(grid.get(0)){
									loadGrid();
									function loadGrid(nowPage,pageSize){
										debugger;
										grid.load("<%=path%>/favorites/loadSubPage.do?flag="+flag,{nowPage:nowPage ? nowPage : 1,pageSize:pageSize ? pageSize : 20},function(){
											var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
											grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
											if(grid.find('.table-body').width() > grid.width()){
												grid.scroll(function(){
													content.find('.page').css('margin-left',grid.scrollLeft());
												});
											}
											grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
											grid.find('.table-body').find('tr').hover(
												function(){
													$(this).addClass('tr-over');
												},
												function(){
													$(this).removeClass('tr-over');
												}
											);
										});
									}
								} 
							});
						}
					});
// 					reload();
				});
			}
			var reload=function(){
	            if(typeof(eval(parent.pageReload))=='function') {
	                pageReload();
	            }else {
	                location.href = location.href;
	            }
	        }
		</script>
	</body>
</html>