<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<script type="text/javascript">
	var report_search_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')};
	var report_export_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'export')};
	var report_print_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')};
	var report_option_perm_tele = ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'option')};
</script>