<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>来自报货分拨单</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>		
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="accountName" name="accountName" value="${accountName }"/>
		<input type="hidden" id="selectIndSta" name="selectIndSta" value="NO"/>	
		<input type="hidden" id="selectDelCodeId" name="selectDelCodeId"/>	
		<input type="hidden" id="ind" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" />
		<form action="<%=path%>/puprorder/selectDisfenbo.do" id="disFenbo" name="disFenbo" method="post">
			<div class="bj_head">
				<div class="form-line">
				    <div class="form-label" style="width: 6.1%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key ="date" />：</div>
				    <div class="form-input">
					   <span><input type="text" id="maded" name="maded" class="Wdate text" value="<fmt:formatDate value="${dis.maded}" pattern="yyyy-MM-dd" type="date"/>" onclick="new WdatePicker();"/></span>
		               <span><input type="checkbox" <c:if test="${ind1=='ind' }"> checked="checked"</c:if> id="ind1" name="ind1" value="ind"/></span>
		               <span><font style="color:blue;"><fmt:message key="arrival_date1"/></font></span>
				    </div>
					
					<div class="form-label" style="width:65px;margin-left:55px;"><span style="color:red;">*</span><fmt:message key="Theprocuringentity"/>：</div>
				    <div class="form-input">
					    <input type="text"  id="positnDes"  name="positnDes" readonly="readonly" value="${dis.positnDes}"/>
					    <input type="hidden" id="positnCode" name="positnCode" value="${dis.positnCode}"/>
					    <img id="seachOnePositn" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_position"/>' />
				    </div>
					
					<div class="form-label" style="width:85px;margin-left:20px;"><span style="color:red;">*</span><fmt:message key="scscgongyingshang"/>：</div>
				    <div class="form-input">
					    <input type="text" id="deliverDes" name="deliverDes" readonly="readonly" value="${dis.deliverDes}"/>
					    <input type="hidden" id="deliverCode" name="deliverCode" value="${dis.deliverCode}"/>
					<img id="seachDeliver" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
				    </div>
					
					<div class="form-label" style="width:85px;margin-left:20px;"><fmt:message key="daily_cargo_space"/>：</div>
				    <div class="form-input">
					    <input type="text"  id="firmDes"  name="firmDes" readonly="readonly" value="${dis.firmDes}"/>
					    <input type="hidden" id="firmCode" name="firmCode" value="${dis.firmCode}"/>
					    <img id="seachPositn" class="search" src="<%=path%>/image/themes/icons/searchmul1.png" alt='<fmt:message key="query_position"/>' />
				    </div>	
				</div>
				
				<div class="form-line">	
				    <div class="form-label" style="width: 6.1%;"><fmt:message key="supplies_code"/>：</div>
				    <div class="form-input">
					    <input type="text" style="margin-top:0px;vertical-align:middle;" id="sp_code" name="sp_code" value="${dis.sp_code }" />
					    <img id="seachSupply" class="search" style="margin-left: 5px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_supplies"/>' />
				    </div>
					
				    <div class="form-label" style="width:180px;padding-left:50px;margin-top:0px;vertical-align:middle;text-align: right;margin-left:20px;">
					    <font size="3">[</font><input type="radio" name="yndo" value="" checked="checked"/><fmt:message key="all"/>
					    <input type="radio" <c:if test="${dis.yndo=='YES' }"> checked="checked"</c:if> name="yndo" value="YES"/><fmt:message key="scm_have_done"/>
					    <input type="radio" <c:if test="${dis.yndo=='NO' }"> checked="checked"</c:if> name="yndo" value="NO"/><fmt:message key="scm_un_done"/><font size="3">]</font>
				    </div>
			    </div>
						
			</div>
			</form>
	
	<div id="grid" class="grid" style="overflow: auto;"width="1418">		
				<div class="table-head" id="grid-head-div">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td rowspan="2" class="num"><span style="width:25px;">&nbsp;</span></td>
								<td rowspan="2"><span style="width:25px;text-align: center;"><input type="checkbox" id="chkAll"/>&nbsp;</span></td>								
								<td rowspan="2" title="<fmt:message key="serial_number"/>"><span style="width:60px;"><fmt:message key="serial_number"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_code"/>"><span style="width:70px;"><fmt:message key="supplies_code"/></span></td>
								<td rowspan="2" title="<fmt:message key="supplies_name"/>"><span style="width:70px;"><fmt:message key="supplies_name"/></span></td>
 								<td rowspan="2" title="<fmt:message key="specification"/>"><span style="width:70px;"><fmt:message key="specification"/></span></td>
								<td rowspan="2" title="<fmt:message key="Theprocuringentity"/><fmt:message key="gyszmbm"/>"><span style="width:60px;"><fmt:message key="Theprocuringentity"/><fmt:message key="gyszmbm"/></span></td>
 								<td rowspan="2" title="<fmt:message key="Theprocuringentity"/>"><span style="width:120px;"><fmt:message key="Theprocuringentity"/></span></td>
								<td rowspan="2" title="<fmt:message key="daily_cargo_space"/>"><span style="width:120px;"><fmt:message key="daily_cargo_space"/></span></td>
								<td colspan="3" title="<fmt:message key="procurement_unit"/>"><span style="width:105px;"><fmt:message key="procurement_unit"/></span></td><!-- 采购单位 -->
								<td colspan="5" title="<fmt:message key="standard_unit"/>"><span style="width:185px;"><fmt:message key="standard_unit"/></span></td><!-- 标准单位 -->
								<td colspan="2" title="<fmt:message key="the_reference_unit"/>"><span style="width:85px;"><fmt:message key="the_reference_unit"/></span></td><!-- 参考单位 -->
								<td rowspan="2" title="<fmt:message key="direction"/>"><span style="width:60px;"><fmt:message key="direction"/></span></td>
 								<td rowspan="2" title="<fmt:message key="arrival_date"/>"><span style="width:70px;"><fmt:message key="arrival_date"/></span></td>
 								<td rowspan="2" title="<fmt:message key="inventory"/>"><span style="width:60px;"><fmt:message key="inventory"/></span></td> 								
 								<td rowspan="2" title="<fmt:message key="which_procurement_indicating"/>"><span style="width:60px;"><fmt:message key="which_procurement_indicating"/></span></td>
 								<td rowspan="2" title="<fmt:message key="orderer"/>"><span style="width:60px;"><fmt:message key="orderer"/></span></td>
 								<td rowspan="2" title="<fmt:message key="confirm_the_time"/>"><span style="width:120px;"><fmt:message key="confirm_the_time"/></span></td>
 								<td rowspan="2" title="<fmt:message key="the_unknown_arrives"/>"><span style="width:60px;"><fmt:message key="the_unknown_arrives"/></span></td>
 								<td rowspan="2" title="<fmt:message key="purchase_audit"/>"><span style="width:60px;"><fmt:message key="purchase_audit"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="remark"/>"><span style='width:${scm_project=="dttc"?"300px":"70px"};'><fmt:message key="remark"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="production_date"/>"><span style="width:70px;"><fmt:message key="production_date"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="pc_no"/>"><span style="width:60px;"><fmt:message key="pc_no"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="reported_num"/>"><span style="width:70px;"><fmt:message key="reported_num"/></span></td>	
 								<td rowspan="2"  title="<fmt:message key="additional_items"/>"><span style="width:70px;"><fmt:message key="additional_items"/></span></td>
 								<td rowspan="2"  title="<fmt:message key="suppliers"/><fmt:message key="confirm"/>"><span style="width:60px;"><fmt:message key="suppliers"/><fmt:message key="deliver_goods"/><br/><fmt:message key="confirm"/></span></td>
 								<c:if test="${isOrNOtRelationCG=='1' }">
	 								<td rowspan="2"><span style="width:60px;">订单制单人</span></td>
	 								<td rowspan="2"><span style="width:120px;">订单制单时间</span></td>
	 								<td rowspan="2"><span style="width:50px;">是否已生成</span></td>
 								</c:if>
							</tr>
							<tr>
								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
								<td title="<fmt:message key="the_number_of_the_reported_goods"/>"><span style="width:60px;"><fmt:message key="the_number_of_the_reported_goods"/></span></td>
								<td title="<fmt:message key="adjustment_amount"/>"><span style="width:60px;"><fmt:message key="adjustment_amount"/></span></td>
								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
								<td title="<fmt:message key="number_of_standards"/>"><span style="width:60px;"><fmt:message key="number_of_standards"/></span></td>
 								<td title="<fmt:message key="offer"/><fmt:message key="unit_price"/>"><span style="width:60px;"><fmt:message key="offer"/><fmt:message key="unit_price"/></span></td>
 								<td title="<fmt:message key="scm_sale_price"/><fmt:message key="unit_price"/>"><span style="width:60px;"><fmt:message key="scm_sale_price"/><fmt:message key="unit_price"/></span></td>
 								<td title="<fmt:message key="amount"/>"><span style="width:60px;"><fmt:message key="amount"/></span></td>
 								<td title="<fmt:message key="unit"/>"><span style="width:60px;"><fmt:message key="unit"/></span></td>
 								<td title="<fmt:message key="reference_number"/>"><span style="width:60px;"><fmt:message key="reference_number"/></span></td>
							</tr>							
						</thead>
					</table>
				</div>
				<div class="table-body" id="grid-body-div" style="overflow: auto;">
					<table cellspacing="0" cellpadding="0" id="table-body">
						<tbody>
							<c:forEach var="dis" items="${disesList1}" varStatus="status">
								<tr <c:if test="${dis.amount!=dis.amountin }">style="color:red;"</c:if>>
									<td class="num"><span style="width:25px;">${status.index+1}</span></td>
									<td><span style="width:25px;text-align: center;"><input type="checkbox" name="idList" id="chk_${dis.id}" value="${dis.id}"/></span></td>
									<td><span title="${dis.id }" style="width:60px;">${dis.id }</span></td>
									<td><span title="${dis.sp_code }" style="width:70px;">${dis.sp_code }</span></td>
									<td><span title="${dis.sp_name }" style="width:70px;">${dis.sp_name }</span></td>
									<td><span title="${dis.sp_desc }" style="width:70px;">${dis.sp_desc }</span></td>
									<c:choose>
										<c:when test="${dis.inout == 'out' }">
											<td>
												<span style="width:60px;" title="${dis.deliverCode }">${dis.deliverCode}</span>
											</td>
										</c:when>
										<c:otherwise>
											<td class="textInput">
												<span style="width:60px;">
													<input disabled="disabled" title="${dis.deliverCode }" type="text" id="c_delv_${dis.id}" value="${dis.deliverCode}" onfocus="this.select()" onblur="findByDeliver(this,'${dis.id}')" onkeydown="searchDeliver(this,'${dis.id}')"/>
												</span>
											</td>
										</c:otherwise>
									</c:choose>
									<td><span title="${dis.deliverDes}" id="d_del_${dis.id }" style="width:120px;">${dis.deliverDes }</span></td>
									<td><span title="${dis.firmDes}" style="width:120px;"><input type="hidden" value="${dis.firmCode }"/>${dis.firmDes }</span></td>
									<td>
										<span title="${dis.unit3 }" style="width:60px;">${dis.unit3 }</span>
									</td>
									<td>
										<span title="${dis.amount1}" style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.amount1}" type="currency" pattern="0.00"/></span>
									</td>
									<td>
										<span style="width:60px;">
											<input disabled="disabled"  class="nextclass"  title="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amount1sto}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1sto(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td>
										<span title="${dis.unit }" style="width:60px;">${dis.unit }</span>
									</td>
									<td>
										<span style="width:60px;">
											<input disabled="disabled"  class="nextclass"  title="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amountin}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmountin(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<c:choose>
										<c:when test="${dis.ynprice == 'N'}"><!-- 如果没有取到报价，价格可以修改 -->
											<td class="textInput">
												<span style="width:60px;">
													<input disabled="disabled"  class="nextclass" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onchange="checkPrice(this,'14')" onkeyup="validate(this)"/>
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${!empty accountDis && accountDis.ynzp == 'Y'}"><!-- 如果有权限修改报价 -->
													<td class="textInput">
														<span style="width:60px;">
															<input  disabled="disabled"  class="nextclass" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricein(this,'${dis.id}')" onchange="checkPrice(this,'14')" onkeyup="validate(this)"/>
														</span>
													</td>
												</c:when>
												<c:otherwise>
													<td><span style="width:60px;text-align:right;" title="<fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/>"><fmt:formatNumber value="${dis.pricein}" type="currency" pattern="0.00"/></span></td>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									<td class="textInput">
										<span style="width:60px;">
											<input  disabled="disabled"  class="nextclass" title="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.pricesale}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="savePricesale(this,'${dis.id}')" onkeyup="validate(this)" onchange="checkPrice(this,'15','${dis.id}')"/>
										</span>
									</td>
									<td class="textInput">
										<span style="width:60px;text-align: right;"><fmt:formatNumber value="${dis.pricein * dis.amountin}" type="currency" pattern="0.00"/></span>
									</td>
									<td>
										<span title="${dis.unit1 }" style="width:60px;">${dis.unit1 }</span>
									</td>
									<td>
										<span style="width:60px;">
											<input disabled="disabled"  class="nextclass"  title="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" type="text" style="width: 60px;text-align: right;" value="<fmt:formatNumber value="${dis.amount1in}" type="currency" pattern="0.00"/>" onfocus="this.select()" onblur="saveAmount1in(this,'${dis.id}','${dis.unitper }','${dis.unitper3 }')" onkeyup="validate(this)"/>
										</span>
									</td>
									<td>
										<span title="${dis.inout}" style="width:60px;display: none;">${dis.inout}</span>
										<span title="<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
											<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
											<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
											<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>" style="width:60px;">
											<c:if test="${dis.inout == 'in'}"><fmt:message key="storage"/></c:if>
											<c:if test="${dis.inout == 'out'}"><fmt:message key="library"/></c:if>
											<c:if test="${dis.inout == 'inout'}"><fmt:message key="straight_hair"/></c:if>
											<c:if test="${dis.inout == 'dire'}"><fmt:message key="direct"/></c:if>
										</span>
									</td>
									<td class="textInput">
										<span style="width:70px;">
											<input disabled="disabled"  title="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>" type="text" class="Wdate text" value="<fmt:formatDate value="${dis.ind}" pattern="yyyy-MM-dd" type="date"/>"  onfocus="this.select()" onblur="saveInd(this,'${dis.id}')" onclick="selectInd(this,1)" onkeypress="selectInd(this)"/>
										</span>
									</td>
									<td><span title="${dis.cnt}" style="width:60px;text-align: right;">${dis.cnt}</span></td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.chk1}">
											<input type="hidden" id="qr_${dis.id}" value="${dis.chk1 }"/>
											<img src="<%=path%>/image/kucun/${dis.chk1}.png"/>
										</span>
									</td>
									<td><span title="${dis.chk1emp }" style="width:60px;">${dis.chk1emp }</span></td>
									<td><span title="${dis.chk1tim }" style="width:120px;">${dis.chk1tim }</span></td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.chksend}">
										<img src="<%=path%>/image/kucun/${dis.chksend}.png"/>
										</span>
									</td>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.sta}"><!-- 用来判断是否采购审核 2014.12.5wjf -->
										<img src="<%=path%>/image/kucun/${dis.sta}.png"/>
										</span>
									</td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style='width:${scm_project=="dttc"?"300px":"70px"};' title="${empty memo[1] ? '':memo[0]}">${empty memo[1] ? '':memo[0]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style='width:${scm_project=="dttc"?"300px":"70px"};' title="${dis.memo}">${dis.memo}</span></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${dis.sp_per1 != '0' and (dis.inout == 'in' or dis.inout == 'inout')}"><!-- 必须是直发或者入库方向的，并且设置有效期的才输有效期 -->
											<td class="textInput">
												<span style="width:70px;">
													<input  disabled="disabled"  title="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>" type="text" class="Wdate text" value="<fmt:formatDate value="${dis.dued}" pattern="yyyy-MM-dd" type="date"/>"  onfocus="this.select()" onblur="saveDued(this,'${dis.id}')" onkeypress="selectDued(this)"/>
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${dis.inout == 'in' or dis.inout == 'inout'}">
											<td class="textInput">
												<span style="width:60px;">
													<input disabled="disabled"  title="${dis.pcno}" type="text" value="${dis.pcno}" onfocus="this.select()" onblur="savePcno(this,'${dis.id}')" />
												</span>
											</td>
										</c:when>
										<c:otherwise>
											<td><span style="width:60px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td><span style="width:70px;">${dis.chkstoNo }</span></td>
									<c:choose>
										<c:when test="${fn:contains(dis.memo, '##')}">
											<c:set var="memo" value="${fn:split(dis.memo, '##')}"></c:set>
											<td><span style="width:70px;" title="${empty memo[1] ? memo[0]:memo[1]}">${empty memo[1] ? memo[0]:memo[1]}</span></td>
										</c:when>
										<c:otherwise>
											<td><span style="width:70px;"></span></td>
										</c:otherwise>
									</c:choose>
									<td>
										<span style="width:60px;text-align:center;" title="${dis.deliveryn}">
										<img src="<%=path%>/image/kucun/${dis.deliveryn}.png"/>
										</span>
									</td>
									<c:if test="${isOrNOtRelationCG=='1' }">
										<td><span title="${dis.puprorderemp}" style="width:60px;">${dis.puprorderemp}</span></td>
										<td><span title="${dis.puprordertim}" style="width:120px;">${dis.puprordertim}</span></td>
										<td>
											<span style="width:50px;text-align:center;" title="${dis.ispuprorder}">
											<img src="<%=path%>/image/kucun/${dis.ispuprorder}.png"/>
											</span>
										</td>
									</c:if>
									
									<td style="display:none;">${dis.tax}</td>
							 		<td style="display:none;">${dis.taxdes}</td>
									
								</tr>
							</c:forEach>	
						</tbody>
					</table>
				</div>	
			</div>
	
	
	

			<input type="hidden" id="selected_sp_code" />
			<input type="hidden" id="selected_code" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/ajaxSearchSupply.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	    <script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
		var isCanUse = true;//是否采购确认 那些按钮可用 2014.12.5wjf
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var nScrollHeight=0;
		var nScrollTop=0;
		var returnInfo = true;
		var pageSize = 0;
		
		//点击checkbox改变
		$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
			if ($(this)[0].checked) {
			    $(this).attr("checked", false);
			}else{
				$(this).attr("checked", true);
			}
		});
		// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
		$('.grid').find('.table-body').find('tr').bind("click", function () {
			if ($(this).find(':checkbox')[0].checked) {
				$(this).find(':checkbox').attr("checked", false);
			}else{
				$(this).find(':checkbox').attr("checked", true);
			}
		});
		//---------------------------
//			全选
		$("#chkAll").click(function() {
	    	if (!!$("#chkAll").attr("checked")) {
	    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
	    	}else{
	            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
            }
	    });
		
		$(document).ready(function(){
			//遍历tr，判断如果是直配方向售价显示报价
			$('.grid').find('.table-body').find('tr').each(function(i){
				var fx = $(this).find('td:eq(19)').find('span:eq(0)').attr('title');
				if(fx == 'dire'){
					$(this).find('td:eq(15)').find('input').val($(this).find('td:eq(14)').find('input').val());
				}
			});
			//自动实现滚动条--放到最后加载了
			setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
			setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
			loadGrid();//  自动计算滚动条的js方法		
			changeTh();
			new tabTableInput("table-body","text"); //input  上下左右移动
			$('#bchectim').attr('disabled','disabled');
			$('#echectim').attr('disabled','disabled');
			$(document).bind('keydown',function(e){
		 		if(e.keyCode==27){
		 			$('.<fmt:message key="quit"/>').click();
		 		}
		 		if(e.keyCode==32){return false;}//屏蔽空格
		 		if(e.altKey == false) return;
	 			switch (e.keyCode){
			                case 70: $('#autoId-button-101').click(); break;
			                case 69: $('#autoId-button-102').click(); break;
			                case 83: updateDis(); break;
			                case 65: $('#autoId-button-104').click(); break;
			                case 68: printDis("printInspection.do"); break;
			                case 67: $('#autoId-button-106').click(); break;
			                case 88: changeFocus();break;//Alt+X
		            	}
			});
				
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
				$("#dbilldate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dhopedate\')}'});
				});
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dbilldate\')}',maxDate:'%y-%M-{%d+'+180+'}'});
				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),130);	//计算.grid的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+250));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+250));
				
			});
			
			var validate;
			
			var savelock = false;
			function enterList(){
				if(!savelock){
					savelock=true;
				}else{
					savelock=false;
				}
				
				if($("#positnCode").val()==''){
					alert("请选择采购组织！");
					return;
				}
				
				if($("#deliverCode").val()==''){
					alert("请选择供应商！");
					return;
				}
				
				var data = {};
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
				checkboxList.filter(':checked').each(function(i){
					data["puprOrderdList["+i+"].sp_code"] = $(this).parents('tr').find('td:eq(3)').find("span").attr('title');
					data["puprOrderdList["+i+"].sp_name"] = $(this).parents('tr').find('td:eq(4)').find("span").attr('title');
					data["puprOrderdList["+i+"].sp_desc"] = $(this).parents('tr').find('td:eq(5)').find("span").attr('title');
					data["puprOrderdList["+i+"].positnCode"] = $(this).parents('tr').find('td:eq(6)').find("span").text();
					data["puprOrderdList["+i+"].positnDes"] = $(this).parents('tr').find('td:eq(7)').find("span").text();
					//data["puprOrderdList["+i+"].positncode"] = $(this).parents('tr').find('td:eq(8)').find("input").val();
					//data["puprOrderdList["+i+"].positnname"] = $(this).parents('tr').find('td:eq(8)').find("span").attr('title');
					
					data["puprOrderdList["+i+"].unit"] = $(this).parents('tr').find('td:eq(12)').find("span").attr('title');//标准单位
					data["puprOrderdList["+i+"].ncnt"] = $(this).parents('tr').find('td:eq(13)').find("input").val();//数量
					data["puprOrderdList["+i+"].sp_price"] = $(this).parents('tr').find('td:eq(14)').find("span").attr('title');//单价
				    data["puprOrderdList["+i+"].nmoney"] = $(this).parents('tr').find('td:eq(16)').find("span").text();//金额
					data["puprOrderdList["+i+"].vmemo"] = $(this).parents('tr').find('td:eq(27)').find("span").attr('title');//备注
					data["puprOrderdList["+i+"].dhopedate"] = $("#maded").val();//到货日
					//if(data["puprOrderdList["+i+"].positncode"]!=data["puprOrderdList[0].positncode"]){
					//	aa =1;
					//	alert("请选择同一个供应商的物资！");
					//	return false;
					//}
					
					data["puprOrderdList["+i+"].delivercode"] = $("#deliverCode").val();
					data["puprOrderdList["+i+"].delivername"] = $("#deliverDes").val();
					
					data["puprOrderdList["+i+"].positncode"] = $("#positnCode").val();
					data["puprOrderdList["+i+"].positnname"] = $("#positnDes").val();
					
					data["puprOrderdList["+i+"].tax"] = $(this).parents('tr').find('td:eq(33)').text();
					data["puprOrderdList["+i+"].taxdes"] = $(this).parents('tr').find('td:eq(34)').text();
					
				});
				}else{
					alert("未选择数据！");
					return;
				}
				
				data['positncode'] = $('#firmCode').val();//报货分店、仓位
				data['positnname'] = $('#firmDes').val();//
				data['delivercode'] = $('#deliverCode').val();//报货分店、仓位
				data['delivername'] = $('#deliverDes').val();//报货分店、仓位
				$.post("<%=path%>/puprorder/updateDisfenbo.do",data,function(data){
					var a = eval(data);
					if(a=="1"){
						alert("保存成功！");
						parent.reloadPage();
						parent.$('.close').click()
						window.location.href="<%=path%>/puprorder/queryAllPuprOrders.do";
					}else{
						alert("保存失败");
						return;
					}
					
					
				});
				
			}
			
		    function selectList(){
							if(null!=$('#maded').val() && ""!=$('#maded').val()){
								var deliverCode = $("#deliverCode").val();
								var firmCode = $("#firmCode").val();
								/*  if(deliverCode == null || deliverCode ==""){
								    alert("请先选择供应商！");
								}if(firmCode == null || firmCode ==""){
									alert("请选择报货仓位！");
								}else{ */
									
								if($("#positnCode").val()==''){
									alert("请选择采购组织！");
									return;
								}
								
								if($("#deliverCode").val()==''){
									alert("请选择供应商！");
									return;
								}
									
									$("#disFenbo").submit();
								/* } */
								
 							}else{
 								alert('<fmt:message key="date"/><fmt:message key="cannot_be_empty"/>！');
 							}
			};
			
			$("#seachPositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#firmCode").val(),
					single:false,
					tagName:'firmDes',
					tagId:'firmCode',
					title:'<fmt:message key="please_select_positions"/>'
				});
			});
			
			$('#seachDeliver').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#deliverCode').val();
					var defaultName = $('#deliverDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_suppliers"/>',encodeURI('<%=path%>/deliver/selectOneDeliver.do?defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#deliverDes'),$('#deliverCode'),'900','500','isNull');
				}
			});
			
			$("#seachOnePositn").click(function(){
				chooseStoreSCM({
					basePath:'<%=path%>',
					width:600,
					firmId:$("#positnCode").val(),
					single:true,
					tagName:'positnDes',
					tagId:'positnCode'
				});
			});
			
			$('#seachOnePositn1').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#positnCode').val();
					var defaultName = $('#positnDes').val();
					var offset = getOffset('maded');
					top.cust('<fmt:message key="please_select_positions"/>',encodeURI('<%=path%>/positn/selectPositn.do?mold='+'one&defaultCode='+defaultCode+'&defaultName='+encodeURI(defaultName)),offset,$('#positnDes'),$('#positnCode'),'760','520','isNull');
				}
			});
			$('#seachSupply').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#sp_code').val();
					top.customSupply('<fmt:message key="please_select_materials"/>','<%=path%>/supply/selectSupplyLeft.do?defaultCode='+defaultCode,$('#sp_code'));	
				}
			});
			$('#seachSupply1').bind('click.custom',function(e){
				if ($('.grid').find('.table-body').find(':checkbox').filter(':checked').size()==1){
					if(!!!top.customWindow){
						var defaultCode = $('.grid').find('.table-body').find(':checkbox').filter(':checked').parents('tr').find('td:eq(3)').find('span').attr('title');
						var offset = getOffset('maded');
						top.cust('<fmt:message key="please_select_materials"/>',encodeURI('<%=path%>/supply/findAllSupplySXS.do?defaultCode='+defaultCode),offset,$('#spName'),$('#spCode'),'560','420','isNull');
					}
				}else {
					$('.search-div').hide();
					alert("<fmt:message key='select_a_need_to_adjust_the_material_data'/>！");
				}
			});
			$('#seachTyp').bind('click.custom',function(e){
				if(!!!top.customWindow){
					var defaultCode = $('#typCode').val();
					//var defaultName = $('#typDes').val();
					var offset = getOffset('positnDes');
					top.cust('<fmt:message key="please_select_category"/>',encodeURI('<%=path%>/grpTyp/selectMoreGrpTyp.do?defaultCode='+defaultCode),offset,$('#typDes'),$('#typCode'),'650','500','isNull');
				}
			});
		</script>
	</body>
</html>
