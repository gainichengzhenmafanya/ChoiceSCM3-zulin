<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 70px;
			}
			.form-line .form-input{
				width: 90px;
			}
			.form-line .form-input input[type='text']{
				width: 90px;
			}
		</style>
	</head>
	<body>
		<div style="height:100%;">
		<div class="tool"></div>
	    	<div>
		    	<form id="queryForm" action="<%=path%>/puprorder/queryAllPuprOrders.do" method="post">
		    	<input type="hidden" id="code" name="code" value="${code}"/>
		    	<input type="hidden" id="callBack" name="callBack" value="${callBack}"/>
		    	<input type="hidden" id="delivercode" name="delivercode" value="${delivercode}"/>
		    	<input type="hidden" id="single" name="single" value="${single}"/>
		    	<input type="hidden" id="domId" name="domId" value="${domId}"/>
		    	<input type="hidden" id="istate" name="istate" value="${istate}"/>
		    	<div style="height: 25px;">
		    		<div class="form-line">
		    				<div class="form-label"><fmt:message key="orders_code" />:</div>
						<div class="form-input">
							<input type="text" id="vbillno" name="vbillno" style="margin-top:3px;" value="${puprorder.vbillno}" />
						</div>
						<div class="form-label"><fmt:message key="startdate" />:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="bdate" name="bdate" style="text-transform:uppercase;" value="${puprorder.bdate}" onkeyup="ajaxSearch()" class="Wdate text"/>
						</div>
						<div class="form-label"><fmt:message key="enddate" />:</div>
						<div class="form-input">
							<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${puprorder.edate}" onkeyup="ajaxSearch()" class="Wdate text"/>
						</div>
						<div class="form-label"><input type="button" id="queryPuprOrders" name="queryPuprOrders"  style="margin-top:3px;" value="<fmt:message key="select" />"/></div>
					</div>
				</div>
					<div class="grid" class="grid" style="overflow: auto;">
						<div class="table-head" >
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td style="width:31px;">&nbsp;</td>
										<td style="width:30px;"><input type="checkbox" id="chkAll"/></td>
										<td style="width:160px;"><fmt:message key="orders_code" /></td>
										<td style="width:160px;"><fmt:message key="suppliers" /></td>
										<td style="width:100px;"><fmt:message key="total_amount1" /></td>
										<td style="width:140px;"><fmt:message key="document_date" /></td>
										<td style="width:140px;"><fmt:message key="The_inspection_sheet" /><fmt:message key="gyszmzt" /></td>
										<td style="width:140px;"><fmt:message key="Document_status" /></td>
										<td style="width:240px;"><fmt:message key="summary" /></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="ordermap" varStatus="step" items="${puprorderList}">
										<tr>
											<td><span id="pk_puprorder" title="${ordermap.pk_puprorder}" style="width:21px;">${step.count}</span></td>
											<td style="width:30px; text-align: center;">
												<input title="${ordermap.pk_puprorder}" type="checkbox" name="idList" id="chk_${ordermap.pk_puprorder}" value="${ordermap.pk_puprorder}"/>
											</td>
											<td><span title="${ordermap.vbillno}" style="width:150px;text-align: left;">${ordermap.vbillno}</span></td>
											<td><span title="${ordermap.delivername}" style="width:150px;text-align: left;">${ordermap.delivername}</span></td>
											<td><span title="${ordermap.nmoney}" style="width:90px;text-align: right;">${ordermap.nmoney}</span></td>
											<td><span title="${ordermap.dbilldate}" style="width:130px;text-align: left;">${ordermap.dbilldate}</span></td>
											<td><span title='${ordermap.hasCreate==0?"未生成":"已生成"}' style="width:130px;text-align: left;">${ordermap.hasCreate==0?"未生成":"已生成"}</span></td>
											<td><span title="${ordermap.istate}" style="width:130px;text-align: left;">
												<c:if test="${ordermap.istate==1}"><fmt:message key="unchecked" /></c:if>
												<c:if test="${ordermap.istate==4}"><fmt:message key="checked" /></c:if>
												<c:if test="${ordermap.istate==5}"><fmt:message key="Have_the_inspection" /></c:if>
												<c:if test="${ordermap.istate==6}"><fmt:message key="The_storage" /></c:if>
												<c:if test="${ordermap.istate==7}"><fmt:message key="Settled" /></c:if>
											</span></td>
											<td><span title="${ordermap.vmemo}" style="width:230px;text-align: left;">${ordermap.vmemo}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<page:page form="listForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
				</form>
			</div>
		</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		
		<script type="text/javascript">
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$('#queryForm').submit();
				} 
			}
			$(document).ready(function(){
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
					parent.$("#mainFrame").submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				//查询按钮
				$('#queryPuprOrders').bind('click',function(){
					$('#queryForm').attr('action','<%=path%>/puprorder/selectAllPuprOrders.do');
					$('#queryForm').submit();
				});
				$("#bdate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});
				});
				$("#edate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});
				});
				setElementHeight('.grid',['.tool'],$(document.body),85);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.grid').find('.table-body').find('tr').hover(
					function(){
						$(this).addClass('tr-over');
					},
					function(){
						$(this).removeClass('tr-over');
					}
				);
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="enter" />',
							title: '<fmt:message key="enter" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-20px']
							},
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var chkValue = [];
								var nameValue = [];
								checkboxList.filter(':checked').each(function(){
									var row = $(this).closest('tr');
									if($.trim(row.children('td:eq(7)').text()) != '<fmt:message key="checked" />' ){
										alerterror('<fmt:message key="bill_haven_enter" />！');
										return;
									}
									chkValue.push($(this).val());
									nameValue.push($.trim(row.children('td:eq(2)').text()));
								});
								parent['${callBack}'](chkValue.join(","),nameValue.join(","));
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								parent.$('.close').click();
							}
						}
					]
				});
			});
			//------------------------------
				var mod = ${single};
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$("#chkAll").click(function(){
						if($(this)[0].checked){
							$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
						}else{
							$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
						}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = ${single};
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						//$(this).attr('checked','checked');
					}
					event.stopPropagation();
				});
			//---------------------------
// 			全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
		</script>
	</body>
</html>