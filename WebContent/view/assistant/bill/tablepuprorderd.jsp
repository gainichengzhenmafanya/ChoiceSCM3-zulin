<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" /> 
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" /> 
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
    <style type="text/css">
/*     	.ul li { */
/*     		float: left; */
/*     		list-style-type:none; */
/*     		width: 50px; */
/*     		font-size: 18px; */
/*     		margin-top: 50px; */
/*     		margin-left: 40px; */
/*     	} */
/*     	.ul2 li { */
/*     		float: left; */
/*     		list-style-type:none; */
/*     	} */
		.stat1 tr td{
			font-size: 18px;
			width: 126px;
			text-align: center;
		}
		.stat2 tr td{
			font-size: 18px;
			text-align: center;
		}
    </style>
</head>
<body>
		<div style="width: 99.8%;height:85%" class="bodydiv">
		<input type="hidden" id="pk_puprorder" value="${pk_puprorder}"></input>
		<input type="hidden" id="istate" value="${puprorder.istate}"></input>
			<div class="easyui-tabs" fit="false" plain="true" style="margin-top: 30px;" id="tabs1">
	        <div title='<fmt:message key="ORDR" /><fmt:message key="the_detail" />' style="overflow:hidden;">

	            <div class="grid">
	                <div class="table-head" >
	                    <table cellpadding="0" cellspacing="0">
	                        <thead>
	                            <tr>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 150px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="tax_rate" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="Theprocuringentity" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
	                            </tr>
	                        </thead>
	                    </table>
	                </div>
	                <div class="table-body">
	                    <table cellpadding="0" cellspacing="0">
	                        <tbody>
	                        	<c:forEach var="orderd" varStatus="step" items="${puprOrderdList}">
		                            <tr>
		                                <td><span style="width: 100px;">${orderd.sp_code}</span></td>
		                                <td><span style="width: 150px;">${orderd.sp_name}</span></td>
		                                <td><span style="width: 60px;">${orderd.sp_desc}</span></td>
		                                <td><span style="width: 60px;">${orderd.unit}</span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.ncnt}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.sp_price}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;">${orderd.taxdes}</span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.nmoney}" pattern="0.00"/></span></td>
		                                <td><span style="width: 100px;">${orderd.positnname}</span></td>
		                                <td><span style="width: 100px;">${orderd.vmemo}</span></td>
		                            </tr>
		                    	</c:forEach>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
			    <div>
			    	<div class="form-line">
						<div class="form-label"><fmt:message key="total" /><fmt:message key="supplies" />:</div>
						<div class="form-input">
							<span id="countmaterial">${length}</span>
							<span><fmt:message key="article" /></span>
						</div>
						<div class="form-label"><fmt:message key="total_amount1" />:</div>
						<div class="form-input">
							<span>￥</span>
							<span id="countmoney"><fmt:formatNumber value="${summoney}" pattern="0.00"/></span>
						</div>
					</div>
			    </div>
	        </div>
	        <div title='订单状态' id="billstate">
	            <div style="width: 100%;margin: auto;margin-top: 50px;margin-bottom: 102px;">
				<input type="hidden" id="istate" name="istate" value="${istate}"/>
				<c:choose>
            				<c:when test="${istate == -1}">
            					<table class="stat1" style="margin-left: 140px;margin-top: 20px;">
									<tr><td>
										<span style="color: red;"><fmt:message key="Have_been_revoked" /></span>
									</td></tr>
								</table>
            				</c:when>
            				<c:otherwise>
            					<table class="stat1" id="stattable1" style="margin-left: 40px;margin-top: 20px;">
							<tr>
								<td>
									<c:choose>
			            				<c:when test="${istate == 1}"><span style="color: red;" checked='checked'><fmt:message key="unchecked" /></span></c:when>
			            				<c:otherwise><span><fmt:message key="unchecked" /></span></c:otherwise>
				            			</c:choose>
								</td>
								<td>
									<c:choose>
			            				<c:when test="${istate == 4}"><span style="color: red;" checked='checked'><fmt:message key="checked" /></span></c:when>
			            				<c:otherwise><span><fmt:message key="checked" /></span></c:otherwise>
			            			</c:choose>
								</td>
								<td>
									<c:choose>
			            				<c:when test="${istate == 6}"><span style="color: red;" checked='checked'><fmt:message key="The_storage" /></span></c:when>
			            				<c:otherwise><span><fmt:message key="The_storage" /></span></c:otherwise>
			            			</c:choose>
								</td>
								<td>
									<c:choose>
			            				<c:when test="${istate == 7}"><span style="color: red;" checked='checked'><fmt:message key="Settled" /></span></c:when>
			            				<c:otherwise><span><fmt:message key="Settled" /></span></c:otherwise>
			            			</c:choose>
								</td>
							</tr>
						</table>
						<table class="stat2" id="stattable2" style="margin-left: 80px;">
							<tr>
								<td style="width: 36px;">
									<c:choose>
			            				<c:when test="${istate == 1}"><span style="color: red;" checked='checked'>●</span></c:when>
			            				<c:otherwise><span>●</span></c:otherwise>
			            			</c:choose>
								</td>
								<td style="width: 90px;"><span>-------></span></td>
								<td style="width: 36px;">
									<c:choose>
			            				<c:when test="${istate == 4}"><span style="color: red;" checked='checked'>●</span></c:when>
			            				<c:otherwise><span>●</span></c:otherwise>
			            			</c:choose>
								</td>
								<td style="width: 90px;"><span>-------></span></td>
								<td style="width: 36px;">
									<c:choose>
			            				<c:when test="${istate == 6}"><span style="color: red;" checked='checked'>●</span></c:when>
			            				<c:otherwise><span>●</span></c:otherwise>
			            			</c:choose>
								</td>
								<td style="width: 90px;"><span>-------></span></td>
								<td style="width: 36px;">
									<c:choose>
			            				<c:when test="${istate == 7}"><span style="color: red;" checked='checked'>●</span></c:when>
			            				<c:otherwise><span>●</span></c:otherwise>
			            			</c:choose>
								</td>
							</tr>
						</table>
            				</c:otherwise>
            			</c:choose>
	            </div>
	        </div>
<!-- 	        <div title='物流信息' style="overflow-x:hidden;"> -->
<!-- 	            <div class="grid"> -->
<!-- 	                <div class="table-head" > -->
<!-- 	                    <table cellpadding="0" cellspacing="0"> -->
<!-- 	                        <thead> -->
<!-- 	                            <tr> -->
<!-- 	                                <td><span style="width: 200px;">物资编码</span></td> -->
<!-- 	                                <td><span style="width: 500px;">物资编码</span></td> -->
<!-- 	                            </tr> -->
<!-- 	                        </thead> -->
<!-- 	                    </table> -->
<!-- 	                </div> -->
<!-- 	                <div class="table-body"> -->
<!-- 	                    <table cellpadding="0" cellspacing="0"> -->
<!-- 	                        <tbody> -->
<%-- 	                        	<c:forEach var="orderd" varStatus="step" items="${puprOrderdList}"> --%>
<!-- 		                            <tr> -->
<%-- 		                                <td><span style="width: 200px;">${orderd.sp_code}</span></td> --%>
<%-- 		                                <td><span style="width: 500px;">${orderd.sp_code}</span></td> --%>
<!-- 		                            </tr> -->
<%-- 		                    	</c:forEach> --%>
<!-- 	                        </tbody> -->
<!-- 	                    </table> -->
<!-- 	                </div> -->
<!-- 	            </div> -->
<!-- 	        </div> -->
<!-- 
	        <div title='发货单明细' style="overflow-x:hidden;">
	            <div class="grid">
	                <div class="table-head" >
	                    <table cellpadding="0" cellspacing="0">
	                        <thead>
	                            <tr>
<%-- 	                                <td><span style="width: 160px;"><fmt:message key="shouyefahuodh" /></span></td> --%>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 150px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="purchase_quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="shouyefahuoshuliang" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
	                            </tr>
	                        </thead>
	                    </table>
	                </div>
	                <div class="table-body">
	                    <table cellpadding="0" cellspacing="0">
	                        <tbody>
	                        	<c:forEach var="orderd" varStatus="step" items="${fahuoList}">
		                            <tr>
<%-- 		                                <td><span style="width: 160px;">${orderd.sp_code}</span></td> --%>
		                                <td><span style="width: 100px;">${orderd.sp_code}</span></td>
		                                <td><span style="width: 150px;">${orderd.sp_name}</span></td>
		                                <td><span style="width: 60px;">${orderd.sp_desc}</span></td>
		                                <td><span style="width: 60px;">${orderd.unit}</span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.ncnt}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.ncnt}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.sp_price}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${orderd.nmoney}" pattern="0.00"/></span></td>
		                                <td><span style="width: 100px;">${orderd.vmemo}</span></td>
		                            </tr>
		                    	</c:forEach>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <div>
			    	<div class="form-line">
						<div class="form-label"></div>
						<div class="form-input">
							<span id="countmaterial"></span>
							<span></span>
						</div>
						<div class="form-label"></div>
						<div class="form-input">
							<span></span>
							<span id="countmoney"></span>
						</div>
					</div>
			    </div>
            </div> -->
	        <div title='<fmt:message key="The_inspection_sheet" /><fmt:message key="the_detail" />' style="overflow-x:hidden;">
	            <div class="grid">
	                <div class="table-head" >
	                    <table cellpadding="0" cellspacing="0">
	                        <thead>
	                            <tr>
	                                <td><span style="width: 150px;"><fmt:message key="The_inspection_sheet" /><fmt:message key="code" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 150px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="purchase_quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="inspection" /><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="The_difference_quantity_inspection" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
	                            </tr>
	                        </thead>
	                    </table>
	                </div>
	                <div class="table-body">
	                    <table cellpadding="0" cellspacing="0">
	                        <tbody>
	                        	<c:forEach var="inspectd" varStatus="step" items="${listInspectd}">
		                            <tr>
		                                <td><span style="width: 150px;">${inspectd.vinspectbillno}</span></td>
		                                <td><span style="width: 100px;">${inspectd.sp_code}</span></td>
		                                <td><span style="width: 150px;">${inspectd.sp_name}</span></td>
		                                <td><span style="width: 60px;">${inspectd.sp_desc}</span></td>
		                                <td><span style="width: 60px;">${inspectd.unit3}</span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.npurnum}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.ninsnum}" pattern="0.00"/></span></td>
		                                <td><span style="width: 80px;text-align: right;"><fmt:formatNumber value="${inspectd.ndiffnum}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.nprice}" pattern="0.00"/></span></td>
		                                <td><span style="width: 60px;text-align: right;"><fmt:formatNumber value="${inspectd.nmoney}" pattern="0.00"/></span></td>
		                                <td><span style="width: 100px;">${inspectd.vmemo}</span></td>
		                            </tr>
		                    	</c:forEach>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
			    <div>
			    	<div class="form-line">
						<div class="form-label"><fmt:message key="total" /><fmt:message key="supplies" />:</div>
						<div class="form-input">
							<span id="countmaterial">${lengthinsp}</span>
							<span><fmt:message key="article" /></span>
						</div>
						<div class="form-label"><fmt:message key="total_amount1" />:</div>
						<div class="form-input">
							<span>￥</span>
							<span id="countmoney"><fmt:formatNumber value="${inspnmoney}" pattern="0.00"/></span>
						</div>
					</div>
			    </div>
	        </div>
	        <div title='评价' style="overflow-x:hidden;">
	            <div style="margin-top: 20px;margin-left: 50px;height:188px;">
	            	<div><span style="font-size: 18px;">评价内容：</span></div>
			<div style="margin-top: 20px;">
				<textarea id="evarea" name="textareas" rows="5" cols="100" readonly="readonly">${puprorder.vevaluate}</textarea>
			</div>
	            </div>
	        </div>
	    </div>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
    <script type="text/javascript">
//			loadGrid();//  自动计算滚动条的js
	    $(document).ready(function(){
	    	setElementHeight('.grid',['.tool'],$(document.body),110);	//计算.grid的高度
		    setElementHeight('.table-body',['.table-head'],'.grid'); //计算.table-body的高度
		    $('.easyui-tabs').css("width",$(document.body).width()*0.999);
		$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())*0.998);
		$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())*0.998);
		
		
		if($(".grid .table-body").find('table').find('tr').length==0){
			$("#billstate").html('');
		}else{
			var istate = $('#istate').val();
		    	if(istate != -1){
		    		$($('#stattable1').find('tr')[0]).find('td').each(function(){
		    			$(this).find('span').css('color','red');
		    			if($(this).find('span').attr('checked')=='checked'){
		    				return false;
		    			}
		    		});
		    		$($('#stattable2').find('tr')[0]).find('td').each(function(){
		    			$(this).find('span').css('color','red');
		    			if($(this).find('span').attr('checked')=='checked'){
		    				return false;
		    			}
		    		});
		    	} 
		}
	    	$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
	    });
	    
	  //选择页签
	function evaluatetabs(){
		$(".tabs").find('li:last').trigger('click');
		$("#evarea").removeAttr('readonly');
	  }
	  //保存评价
	function saveevaluate(){
		var param = {};
		param['pk_puprorder'] = $("#pk_puprorder").val();
		param['vevaluate'] = $("#evarea").val();
		$.post('<%=path%>/puprorder/saveEvaluate.do',param,function(re){
			if(re=='OK'){
				alerttips('保存成功！');
				$("#evarea").attr('readonly','readonly');
			}
		})
	  }
    </script>
</body>
</html>
