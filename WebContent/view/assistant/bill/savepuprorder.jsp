<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="ibillfrom" name="ibillfrom" value="${puprOrder.ibillfrom}"/>
		<input type="hidden" id="istate" name="istate" value="${puprOrder.istate}"/>
		<form action="<%=path%>/puprorder/addPuprorder.do" id="savepuprorder" name="savepuprorder" method="post">
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><fmt:message key="orders_code" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${puprOrder.vbillno}"/>
					</div>
					<div class="form-label" style="width: 8%;margin-left: 35px;"><fmt:message key="document_date" />:</div>
					<div class="form-input" style="width:100px;">
						<input autocomplete="off" type="text" id="dbilldate" name="dbilldate" style="text-transform:uppercase;" value="${puprOrder.dbilldate}" class="Wdate text"/>
					</div>	
					<div class="form-label" style="width: 8%;margin-left: 35px;"><fmt:message key="Hope_date" />:</div>
					<div class="form-input" style="width:100px;">
						<input autocomplete="off" type="text" id="dhopedate" name="dhopedate" style="text-transform:uppercase;" value="${puprOrder.dhopedate}" class="Wdate text"/>
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label" style="width: 9.4%;margin-left: 0px;"><span style="color:red;">*</span><fmt:message key="suppliers" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="delivercode" id="delivercode" class="text" value=""/>
						<input type="text" name="delivername" id="delivername" style="margin-bottom: 6px;" class="text" readonly="readonly" value=""/>
						<img id="supplierbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label" style="width: 4.55%;margin-left: 75px;"><span style="color:red;">*</span><fmt:message key="Theprocuringentity" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="hidden" name="positncode" id="positncode" class="text" value="${puprOrder.positncode}"/>
						<input type="hidden" name="vcontact" id="vcontact" class="text" value="${puprOrder.vcontact}"/>
						<input type="hidden" name="vcontactphone" id="vcontactphone" class="text" value="${puprOrder.vcontactphone}"/>
						<input type="hidden" name="vdistributionaddr" id="vdistributionaddr" class="text" value="${puprOrder.vdistributionaddr}"/>
						<input type="text" name="positnname" id="positnname" style="margin-bottom: 6px;" class="text" readonly="readonly" value="${puprOrder.positnname}"/>
						<img id="positncodebutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="buyer" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" name="vpurchaser" id="vpurchaser" class="text" value="${puprOrder.vpurchaser}"/>
					</div>
					<div class="form-label" style="width: 8%;margin-left: 35px;"><fmt:message key="remark" />:</div>
					<div class="form-input" style="width:100px;">
						<input type="text" style="width: 355px;" name="vmemo" id="vmemo" class="text" value="${puprOrder.vmemo}"/>
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" style="width:78.5%;">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="tax_rate" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="Theprocuringentity" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="width:78.5%;">
						<table cellspacing="0" cellpadding="0">
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="hidden" name="pk_account" id="pk_account" class="text" value="${puprOrder.pk_account}" />
							<input type="text" style="border: 0px;" name="accountname" id="accountname" class="text" readonly="readonly" value="${puprOrder.accountname}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="dmaketime" id="dmaketime" readonly="readonly" class="text" value="${puprOrder.dmaketime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="${puprOrder.nmoney}" />
						</div>	
					</div>
					<input type="hidden" name="positncodehidden" id="positncodehidden" class="text" value=""/>
					<input type="hidden" name="positnnamehidden" id="positnnamehidden" class="text" value=""/>
					<input type="hidden" name="vcontacthidden" id="vcontacthidden" class="text" value=""/>
					<input type="hidden" name="vcontactphonehidden" id="vcontactphonehidden" class="text" value=""/>
					<input type="hidden" name="vdistributionaddrhidden" id="vdistributionaddrhidden" class="text" value=""/>
				</div>
			</form>
			<input type="hidden" id="selected_sp_code" />
			<input type="hidden" id="selected_code" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
			$("#dbilldate").focus();
			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			var selectrow = null;
			function setSupplier(data){
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
				$('.table-body').find('table').empty();
				$("#nmoney").val(0.0);
				editCells();
			}
			function setPk_org(data){
				$("#positncode").val(data.entity[0].vcode);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#vcontact").val(data.entity[0].vcontact);//联系人
				$("#vcontactphone").val(data.entity[0].vcontactphone);//联系电话
				$("#vdistributionaddr").val(data.entity[0].vdistributionaddr);//配送地址
				$("#positncodehidden").val(data.code);//主键
				$("#positnnamehidden").val(data.entity[0].vname);//名称
				$("#positncodehidden").val(data.entity[0].vcode);//编码
				$("#vcontacthidden").val(data.entity[0].vcontact);//联系人
				$("#vcontactphonehidden").val(data.entity[0].vcontactphone);//联系电话
				$("#vdistributionaddrhidden").val(data.entity[0].vdistributionaddr);//配送地址
				
				$('.table-body').find('table').empty();
				$("#nmoney").val(0.0);
				editCells();
			}
			$(document).ready(function(){
				$('#supplierbutton').click(function(){
					selectSupplier({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:700,
						callBack:'setSupplier',
						domId:'delivercode',
						isReal:'isReal',
						single:true
					});
				});
				$('#positncodebutton').click(function(){
					if ($('#delivername').val() == null || $('#delivername').val() == '') {
						alerttips('<fmt:message key="please_select_suppliers" />');
						return;
					}
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						typn:'1202',
						single:true
					});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alert(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
				$("#dbilldate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dhopedate\')}'});
				});
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dbilldate\')}',maxDate:'%y-%M-{%d+'+180+'}'});
				});
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+250));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+250));
				editCells();
			});
			//编辑表格
			function editCells(){
				var sumnum = 0;
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:23,
					widths:[25,110,150,110,70,70,70,70,70,150,110,200,200,200,200],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1",'text-align':"right"},{background:"#F1F1F1",'text-align':"right"},{background:"#F1F1F1"},'{"text-align":"right"}',{background:"#F1F1F1"},'',{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6,7,10],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						if(pos == 2){
							var postd = data.curobj.closest('td');
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
						if(pos == 5){
							var cgnum = $.trim(rownum.find("td:eq(5) span").text());
							if(cgnum==''){
								cgnum = $.trim(rownum.find("td:eq(5) input").val());
								rownum.find("td:eq(5) span").text(cgnum);
							}
							var mincnt = findNmincntByRate();
							if(isNaN(cgnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(cgnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(cgnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								var prerowmoney = Number($.trim(rownum.find("td:eq(8) span").text())).toFixed(2);
								
								if(Number(cgnum)<Number(mincnt)){
									var price = Number($.trim(rownum.find("td:eq(6) span").text()));
									alerterror('第'+rownum.find('td:first').text()+'行<fmt:message key="purchase_quantity"/><fmt:message key="can_not_be_less_than"/><fmt:message key="Minimum_Purchase_Amount"/>'+Number(mincnt));
									rownum.find("td:eq(5) span").text(Number(mincnt).toFixed(2));
									rownum.find("td:eq(8) span").text(Number(price*mincnt).toFixed(2));
									$.fn.autoGrid.setCellEditable(rownum,5);
									
									var diffmoney = Number(price*mincnt)-prerowmoney;
									
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(rownum,6);
								}else{
									$.fn.autoGrid.setCellEditable(rownum,6);
								}
							}
							var num = $.trim(rownum.find("td:eq(6) span").text());
							if(isNaN(num)){
								rownum.find("td:eq(6) span").text(0.0);
							} else if(Number(num)<0){
								alerterror('<fmt:message key="unitprice_cannot_be_negative"/>');
							}
						}
						function findNmincntByRate(){
							var cnt = 0;
							$.ajaxSetup({ 
								  async: false 
							});
							var sp_code = $.trim(rownum.find("td:eq(17) span").text());
							var param = {};
							param['sp_code'] = sp_code;
							$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
								cnt = Number(re);
							})
							return cnt;
						}
						if(pos == 6){
							var num = rownum.find("td:eq(6) span").text();
							if(num==''){
								num = $.trim(rownum.find("td:eq(6) input").val());
								rownum.find("td:eq(6) span").text(num);
							}
							
							if(isNaN(num)){
								alerterror('<fmt:message key="unitprice_be_not_valid_number" />！');
							} else if(isNaN(rownum.find('td:eq(6) span').text())){
								rownum.find("td:eq(8) span").text(0.0);
							}else{
								rownum.find("td:eq(8) span").text(Number(Number(rownum.find("td:eq(5) span").text())*Number(rownum.find('td:eq(6) span').text())).toFixed(2));
							}
							return;
						}
						if(pos == 8){
							if(isNaN(rownum.find("td:eq(8) span").text())){
								alerterror('<fmt:message key="price_be_not_number" />！');
							} else if(Number(rownum.find("td:eq(5) span").text()) != 0.0){
								rownum.find("td:eq(6) span").text(Number(rownum.find("td:eq(8) span").text()) / Number(rownum.find("td:eq(5) span").text()));
							}
						}
						if(pos == 9){
							var postd = data.curobj.closest('td');
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								if($('#positnname').val() == null || $('#positnname').val() == ''){
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								if($('#dhopedate').val() == null || $('#dhopedate').val() == ''){
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select" /><fmt:message key="arrival_date" />');
									return;
								}
								selectrow = row;
							            <%-- selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_sp_code',
							                code:$('#delivercode').val(),//供应商
							                positncode:$('#positncode').val(),//采购组织
							                irateflag:1,//取采购单位
							                single:true
							            }); --%>
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							var prerowmoney = row.find("td:eq(8) span").text();
							if(prerowmoney!=''){
								prerowmoney = Number(prerowmoney);
							}else{
								prerowmoney = 0 ;
							}
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(8) span").text('');
									row.find("td:eq(9) span").text('');
									row.find("td:eq(12) span").text('');
									
									row.find("td:eq(5) span").text('');
									row.find("td:eq(9) span").attr('vname','');
									row.find("td:eq(11) span").text('');
									row.find("td:eq(2) input").focus();
									$("#mMenu").remove();
									$("#nmoney").val(Number(Number($("#nmoney").val())-Number(prerowmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(row,2);
									return;	
								}
							}
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />');
								return;
							}
							if($('#positnname').val() == null || $('#positnname').val() == ''){
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
								return;
							}
							if($('#dhopedate').val() == null || $('#dhopedate').val() == ''){
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select" /><fmt:message key="arrival_date" />');
								return;
							}
							data['url'] = '<%=path%>/supply/findTopByGys.do?positnCondition='+$("#positncode").val()+'&deliver='+$("#delivercode").val()+'&dateCondition='+formatDate();
							if (data.value.split(".").length>2) {
								data['key'] = 'sp_code';
							}else if(!isNaN(data.value)){
								data['key'] = 'sp_code';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'sp_name';
							}else{
								data['key'] = 'sp_init';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
							//滚动条到底部
							$(".grid").scrollTop($('.grid')[0].scrollHeight);
						},
						resultFormat:function(data){
							var sp_desc ='';
							if(data.sp_desc !='' && data.sp_desc !=null){
								sp_desc = '-'+data.sp_desc ;
							}
							return data.sp_init+'-'+data.sp_code+'-'+data.sp_name + sp_desc;
						},
						afterEnter:function(data2,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(8) span").text())).toFixed(2);
							
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.sp_code){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />！',
	 								speed: 1000
	 							});
								return;
							}
							row.find("td:eq(1) span").text(data2.sp_code).css("text-align","right");
							row.find("td:eq(2) span").text(data2.sp_name);
							row.find("td:eq(2) span").attr('vname',data2.sp_name);
							row.find("td:eq(3) span").text(data2.sp_desc);
							row.find("td:eq(4) span").text(data2.unit3);
							
							row.find("td:eq(6) span").text(data2.sp_price).css("text-align","right");
							
							//税率
							var obj = eval('${taxList}');
							var select = $("<select style='width:70px;'></select>");
							for(var i=0,len=obj.length;i<len;i++){
								var opt;
								if(data2.tax==obj[i].tax){
									opt = $("<option value='"+obj[i].tax+"' selected>"+obj[i].taxdes+"</option>");
								}else{
									opt = $("<option value='"+obj[i].tax+"'>"+obj[i].taxdes+"</option>");
								}
								select.append(opt);
							}
							row.find("td:eq(7)").html(select);
							
							row.find("td:eq(8) span").text('0.00').css("text-align","right");
							row.find("td:eq(9) span").text($('#positnnamehidden').val());
							row.find("td:eq(9) span").attr('vname',$('#positnnamehidden').val());
							row.find("td:eq(10) span").text(data2.vmemo);
							row.find("td:eq(11) span").text(data2.unit3);
							row.find("td:eq(12) span").text($('#positncodehidden').val());
							row.find("td:eq(13) span").text($('#positncodehidden').val());
							row.find("td:eq(14) span").text(data2.sp_name);
							
							row.find("td:eq(16) span").text(data2.ncheckdiffrate);
							row.find("td:eq(17) span").text(data2.sp_code);
							row.find("td:eq(18) span").text(data2.sp_code);
							row.find("td:eq(19) span").text($('#positnnamehidden').val());
							row.find("td:eq(20) span").text($('#vcontact').val());
							row.find("td:eq(21) span").text($('#vcontactphone').val());
							row.find("td:eq(22) span").text($('#vdistributionaddr').val());
							
							row.find("td:eq(5) span").text("0").css("text-align","right");
							row.find("td:eq(15) span").text("0");
						<%--var cnt = 0;
							$.ajaxSetup({ 
								  async: false 
							});
							var param = {};
							param['sp_code'] = data2.sp_code;
						 	$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
								cnt = Number(re);
								row.find("td:eq(5) span").text(Number(cnt).toFixed(2)).css("text-align","right");
								row.find("td:eq(15) span").text(Number(cnt).toFixed(2));
							}); --%>
							
						<%-- 	$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
								data: "sp_code="+data2.sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										row.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
										row.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
										$.ajax({
											type: "POST",
											url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val(),
											data: "sp_code="+data2.sp_code,
											dataType: "json",
											success:function(data4){
												if(data4.sp_price == 0){
													row.find("td:eq(6) span").text('0.00').css("text-align","right");
													
													var nrowmoney = 0;
													var diffmoney = Number(nrowmoney)-prerowmoney;
													row.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
													$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
												}else{
													row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
													
													var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
													var diffmoney = Number(nrowmoney)-prerowmoney;
													row.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
													$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
												}
											}
										})
									}else{
										row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
										//-----------
										row.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
										row.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
										//-----------
										var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
										var diffmoney = Number(nrowmoney)-prerowmoney;
										row.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
										$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
									}
								}
							}); --%>
							$.fn.autoGrid.setCellEditable(row,5);
						}
					},{
						index:5,
						action:function(row,data2){
						//	if(Number(data2.value) == 0){
						//		row.find("td:eq(5)").find('span').text(data2.ovalue);
						//		$.fn.autoGrid.setCellEditable(row,5);
						//	}else if(Number(data2.value) < 0){
						//		row.find("td:eq(5)").find('span').text(data2.ovalue);
						//		$.fn.autoGrid.setCellEditable(row,5);
						//	}else if(isNaN(data2.value)){
						//		row.find("td:eq(5)").find('span').text(data2.ovalue);
						//		$.fn.autoGrid.setCellEditable(row,5);
						//	}else if(data2.value < Number(row.find("td:eq(15) span").text())){
						//		row.find("td:eq(5)").find('span').text(data2.ovalue);
						//		$.fn.autoGrid.setCellEditable(row,5);
						//	}else{
						//		if(isNaN(row.find('td:eq(6) span').text())){
						//			row.find("td:eq(6) span").text(0.0);
						//			row.find("td:eq(8) span").text(0.0);
						//		}else{
						//			var price = row.find('td:eq(6)').find('span').text();
						//			if(price==''){
						//				price = row.find('td:eq(6)').find('input').val();
						//			}
						//			row.find("td:eq(8) span").text(Number(Number(row.find("td:eq(5) span").text())*Number(price)).toFixed(2));
						//		}
						//		$.fn.autoGrid.setCellEditable(row,6);
						//	}
						},
						onCellEdit:function(event,data,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = row.find("td:eq(8) span").text();
							if(prerowmoney!=''){
								prerowmoney = Number(prerowmoney);
							}else{
								prerowmoney = 0 ;
							}
							if ($('#delivername').val() == null || $('#delivername').val() == '') {
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select_suppliers" />');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							if($('#positnname').val() == null || $('#positnname').val() == ''){
								row.find("td:eq(2) span input").val('').focus();
								alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
								$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								row.find('td:eq(8) span').text('0.00');
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								cnt = Number($.trim(cnt));
								var price = row.find("td:eq(6) span").text();
								if(isNaN($.trim(price))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									price = Number($.trim(price));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(8) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(isNaN(row.find('td:eq(6) span').text())){
								row.find("td:eq(8) span").text(0.0);
							}else{
								row.find("td:eq(8) span").text(Number(Number(row.find("td:eq(5) span").text())*Number(row.find('td:eq(6) span').text())).toFixed(2));
							}
							$.fn.autoGrid.setCellEditable(row,10);
						},
						onCellEdit:function(event,data,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(8) span").text())).toFixed(2);
							var price = data.value;
							if(isNaN($.trim(price))){
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								row.find("td:eq(6) span").text('0.00');
								row.find("td:eq(8) span").text('0.00');
							}else{
								price = Number($.trim(price));
								var cnt = row.find("td:eq(5) span").text();
								if(isNaN($.trim(cnt))){
									$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
									row.find('td:eq(6) span').text('0.00');
									row.find('td:eq(8) span').text('0.00');
								}else{
									cnt = Number($.trim(cnt));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(8) span').text(nrowmoney);
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:8,
						action:function(row,data){
							if(isNaN(data.value)){
								alerterror('<fmt:message key="price_be_not_number" />！');
								row.find("td:eq(8)").find('span').text('0.00');
								$.fn.autoGrid.setCellEditable(row,8);
							} else if(Number(row.find("td:eq(5) span").text()) != 0.0){
								row.find("td:eq(6) span").text(Number(row.find("td:eq(8) span").text()) / Number(row.find("td:eq(5) span").text()));
							}
							$.fn.autoGrid.setCellEditable(row,10);
						}
					},{
						index:9,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								if ($('#delivername').val() == null || $('#delivername').val() == '') {
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select_suppliers" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								if($('#positnname').val() == null || $('#positnname').val() == ''){
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select" /><fmt:message key="Theprocuringentity" />');
									$.fn.autoGrid.setCellEditable(row,2);
									return;
								}
								if($('#dhopedate').val() == null || $('#dhopedate').val() == ''){
									row.find("td:eq(2) span input").val('').focus();
									alerterror('<fmt:message key="please_select" /><fmt:message key="arrival_date" />');
									return;
								}
								selectrow = row;
								var sp_code = $.trim(row.find("td:eq(17) span").text());
								chooseDepartMentList({
									basePath:'<%=path%>',
									title:"123",
									height:400,
									width:600,
									callBack:'setPk_orgMaterial',
									domId:'positncode',
									sp_code:sp_code,
									type:1,
									single:true
								});
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,10);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									var prerowmoney = row.find("td:eq(8) span").text();
									if(prerowmoney!=''){
										prerowmoney = Number(prerowmoney);
									}else{
										prerowmoney = 0 ;
									}
									
									row.find("td:eq(9) span").text('');
									row.find("td:eq(9) span").attr('vname','');
									row.find("td:eq(12) span").text('');
									row.find("td:eq(13) span").text('');
									row.find("td:eq(19) span").text('');
									
									row.find("td:eq(6) span").text(0.0);
									row.find("td:eq(8) span").text(0.0);
									
									$("#mMenu").remove();
									$("#nmoney").val(Number(Number($("#nmoney").val())-Number(prerowmoney)).toFixed(2));
									$.fn.autoGrid.setCellEditable(row,10);
									return;	
								}
							}
							
							var sp_code = $.trim(row.find("td:eq(17) span").text());
							data['url'] = '<%=path%>/material/findPositnBySupply.do?sp_code='+sp_code;
							if (data.value.split(".").length>2) {
								data['key'] = 'positncode';
							}else if(!isNaN(data.value)){
								data['key'] = 'positncode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'positnname';
							}else{
								data['key'] = 'positninit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							return data.code+'-'+data.des;
						},
						afterEnter:function(data2,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(8) span").text())).toFixed(2);
							
							row.find("td:eq(9) span").text(data2.des);
							row.find("td:eq(9) span").attr('vname',data2.des);
							row.find("td:eq(12) span").text(data2.code);
							row.find("td:eq(13) span").text(data2.code);
							row.find("td:eq(19) span").text(data2.des);
							
							var positncode = data2.code;
							var sp_code = $.trim(row.find("td:eq(17) span").text());
							var cnt = row.find("td:eq(5) span").text();
							
							//重新获取合约价
							$.ajax({//查询物资
								type: "POST",
								url: "<%=path%>/material/findMaterialNprice.do?positncode="+positncode+"&delivercode="+$('#delivercode').val(),
								data: "sp_code="+sp_code,
								dataType: "json",
								success:function(data3){
									if(data3.sp_price == 0){
										row.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
										row.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
										$.ajax({
											type: "POST",
											url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+positncode+"&delivercode="+$('#delivercode').val(),
											data: "sp_code="+sp_code,
											dataType: "json",
											success:function(data4){
												if(data4.sp_price == 0){
													row.find("td:eq(6) span").text('0.00').css("text-align","right");
												}else{
													row.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
													
													var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
													var diffmoney = Number(nrowmoney)-prerowmoney;
													row.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
													$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
												}
											}
										})
									}else{
										row.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
										//-----------
										row.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
										row.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
										//-----------
										var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
										var diffmoney = Number(nrowmoney)-prerowmoney;
										row.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
										$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
									}
								}
							});
							
							$("#positncodehidden").val(data2.code);//主键
							$("#positnnamehidden").val(data2.name);//名称
							$("#positncodehidden").val(data2.code);//编码
							$.fn.autoGrid.setCellEditable(row,10);
						}
					},{
						index:10,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
							$('#sum_num').text(Number($('#sum_num').text())+1);//总行数
// 							$.fn.autoGrid.setCellEditable(row,9);
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vbillno',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="supplies_name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="supplies_name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			//保存采购订单
			function savepuprorder(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				var delivercode = $("#delivercode").val();
				var positncode = $("#positncode").val();
				
				if(delivercode==''){
					alerterror('请选择供应商！');
					savelock = false;
					return;
				}
				if(positncode==''){
					alerterror('请选择采购组织！');
					savelock = false;
					return;
				}
				if($("#vmemo").val().length>100){
					alerterror('备注最大长度不能大于100！');
					savelock = false;
					return;
				}
				
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2 && pos!=9){
						$(this).find('input').trigger('onEnter');
					}
				});
				var checkcnt = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var sp_code=$.trim($(this).find("td:eq(17) span").text());
					if(sp_code==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
						return;
					}
					
					var cgnum = $.trim($(this).find("td:eq(5) span").text());
					var mincnt = Number($(this).find("td:eq(15) span").text());
					if(Number(cgnum)<Number(mincnt)){
						alerterror('第'+$(this).find('td:first').text()+'行<fmt:message key="purchase_quantity"/><fmt:message key="can_not_be_less_than"/><fmt:message key="Minimum_Purchase_Amount"/>'+mincnt);
						checkcnt = false;
						savelock = false;
						return false;
					}
					var positncode = $.trim($(this).find("td:eq(13) span").text());
					if(positncode==''){
						alerterror('第【'+$(this).find('td:first').text()+'】行未选择采购组织！');
						checkcnt = false;
						savelock = false;
						return false;
					}
					var vmemocontent = $.trim($(this).find("td:eq(10) span").text());
					vmemocontent = vmemocontent ? vmemocontent : $.trim($(this).find("td:eq(10) input").val());	
					if(vmemocontent.length>100){
						alerterror('第【'+$(this).find('td:first').text()+'】行备注长度最长不能大于100！');
						checkcnt = false;
						savelock = false;
						return false;
					}
				});
				if(!checkcnt){
					savelock = false;
					return;
				}
				var keys = ["sp_code","sp_name","sp_desc","unit","ncnt","sp_price","tax","nmoney","positnname","vmemo","pk_unit","positncode","positncode","sp_name","nminamt","ncheckdiffrate","sp_code","vunitcode","positnname","vcontact","vcontactphone","vdistributionaddr"];
				var data = {};
				var i = 0;
				data["vbillno"] = $("#vbillno").val();
				data["dbilldate"] = $("#dbilldate").val();
				data["pk_account"] = $("#pk_account").val();
				data["dmaketime"] = $("#dmaketime").val();
				data["istate"] = $("#istate").val();
				data["positncode"] = $("#positncode").val();
				data["positncode"] = $("#positncode").val();
				data["positnname"] = $("#positnname").val();
				data["dhopedate"] = $("#dhopedate").val();
				data["nmoney"] = $("#nmoney").val();
				data["delivercode"] = $("#delivercode").val();
				data["delivername"] = $("#delivername").val();
				data["vcontact"] = $("#vcontact").val();
				data["vcontactphone"] = $("#vcontactphone").val();
				data["vdistributionaddr"] = $("#vdistributionaddr").val();
				data["vpurchaser"] = $("#vpurchaser").val();
				data["vmemo"] = $("#vmemo").val();
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var sp_code=$.trim($(rows[0]).find("td:eq(17) span").text());
					if(sp_code==''){
						alerterror('至少选择一条物资！');
						savelock = false;
						return;
					}
				}
				
				var checkinfo = true;
				
				var rowindex = 0;
				for(i=0;i<rows.length;i++){
					var vcode = $.trim($(rows[i]).find("td:eq(1) span").text());
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == null?name:vname;
					if(vcode == '' || vcode == null){
						continue;
					}else if(vname == '' || vname == null){
						$(rows[i]).find("td:eq(2) span input").val($.trim($(rows[i]).find("td:eq(14) span").text()));
					}else if(name == '' || name == null){
						$(rows[i]).find("td:eq(2) span").text($.trim($(rows[i]).find("td:eq(14) span").text()));
					}else if(vname != $.trim($(rows[i]).find("td:eq(14) span").text())){
						$(rows[i]).find("td:eq(2) span input").val($.trim($(rows[i]).find("td:eq(14) span").text()));
					}else if(name != $.trim($(rows[i]).find("td:eq(14) span").text())){
						$(rows[i]).find("td:eq(2) span").text($.trim($(rows[i]).find("td:eq(14) span").text()));
					}
					var vname = $.trim($(rows[i]).find("td:eq(2) span input").val());
					var name = $.trim($(rows[i]).find("td:eq(2) span").text());
					vname == ""?name:vname;
					var vorg = $.trim($(rows[i]).find("td:eq(9) span input").val());
					var org = $.trim($(rows[i]).find("td:eq(9) span").text());
					vorg == ""?org:vorg;
					var ncnt = $.trim($(rows[i]).find("td:eq(5) span input").val());
			        	ncnt=ncnt==""?$.trim($(rows[i]).find("td:eq(5) span").text()):ncnt;
			        	var price = $.trim($(rows[i]).find("td:eq(6) span input").val());
			        	price=price==""?$.trim($(rows[i]).find("td:eq(6) span").text()):price;
			        	var money = $.trim($(rows[i]).find("td:eq(8) span input").val());
			        	money=money==""?$.trim($(rows[i]).find("td:eq(8) span").text()):money;
			       		if((vname == '' || vname == null) && (name == '' || name == null)){
						continue;
					}
			                    if(isNaN(price)){
			                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="unitprice_be_not_valid_number" />！');
			                        checkinfo  =false;
			                        savelock = false;	
			                        return;
			                    }else if(price<0){
			                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="unitprice_be_not_valid_number" />！');
			                        savelock = false;
			                        checkinfo  =false;
			                        return;
			                    }
			                    if(isNaN(money)){
			                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="amount" /><fmt:message key="not_valid_number" />！');
			                        savelock = false;
			                        checkinfo  =false;
			                        return;
			                    }else if(money<0){
			                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="amount" /><fmt:message key="not_valid_number" />！');
			                        savelock = false;
			                        checkinfo  =false;
			                        return;
			                    }
					if((vorg == '' || vorg == null) && (org == '' || org == null)){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="Theprocuringentity" /><fmt:message key="cannot_be_empty" />！');
						savelock = false;
						checkinfo  =false;
						return;
					}
					if(isNaN(ncnt)){
			                        alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="number_be_not_valid_number" />！');
			                        checkinfo  =false;
			                        savelock = false;
			                        return;
			                    }else if(Number(ncnt)==0){
						alerterror('<fmt:message key="the"/>'+(i+1)+'<fmt:message key="line"/><fmt:message key="supplies" /><fmt:message key="number_cannot_be_zero" />！');
						savelock = false;
						checkinfo  =false;
						return;
					}
					
					var status=$(rows[i]).find('td:eq(0)').text();
					data["puprOrderdList["+rowindex+"]."+"sp_price"] = $(rows[i]).data('sp_price') ? $(rows[i]).data('sp_price'):$('#sp_price').val();
					cols = $(rows[i]).find("td");
					var j = 0;
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());	
						if(value){
							data["puprOrderdList["+rowindex+"]."+keys[j-1]] = value;
							if(j==6){//保存价格是否为合约价
								var beditprice = $(rows[i]).find("td:eq(6)").attr('edit');
								data["puprOrderdList["+rowindex+"].beditprice"] = (beditprice=='false'?1:0);
							}
							else if(j==7){
								var tax = $(rows[i]).find("td:eq(7) select").val();
								//税率
								data["puprOrderdList["+rowindex+"].tax"] = tax?tax:0;
								data["puprOrderdList["+rowindex+"].taxdes"] = $(rows[i]).find("td:eq(7) select option:selected").text();
							}
						}
					}
					
					rowindex++;
				}
				
				if(!checkinfo){
					return;
				}
				
				$.ajaxSetup({async:false});
				if(checkvcode()){
					$.post("<%=path%>/puprorder/addPuprorder.do?sta="+$('#sta').val(),data,function(data){
						var rs = data;
                        if(isNaN(rs)){
                            alerterror('<fmt:message key="save_fail"/>！');
                            return;
                        }
						switch(Number(rs)){
						case -1:
							alerterror('<fmt:message key="save_fail"/>！');
							savelock = false;
							break;
						case 1:
							showMessage({
										type: 'success',
										msg: '<fmt:message key="save_successful"/>！',
										speed: 3000,
										handler:function(){
											parent.reloadPage();
											parent.$('.close').click();}
										});
							break;
						}
					});	
				}else{
					alerterror('<fmt:message key="document_no"/><fmt:message key="duplicate"/>，<fmt:message key="please_check_number_rule"/>！');
					savelock = false;
				}
			}
			//*****************验证单据号是否重复，校验编码规则是否设置正确**************************************
			function checkvcode(){
				var result = true;
				$.ajaxSetup({async:false});
				$.post("<%=path %>/puprorder/findPuprorderByCode.do",{vbillno:$("#vbillno").val()},function(data){
					if(!data)result = false;
				});
				return result;
			}
			//*****************验证单据号是否重复，校验编码规则是否设置正确**************************************
			function setMaterial(data2){
				var flag = true;
				if(data2.sp_code==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data2.sp_code){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				
				var checksupplier = true;
				$.ajax({//查询供应商
					type: "POST",
					url: "<%=path%>/material/findDeliverByMaterial.do",
					data: "sp_code="+data2.sp_code,
					dataType: "json",
					success:function(res){
						if(res==''){
							checksupplier = false;
							alerterror('物资无供应商！');
							row.find("td:eq(2) span input").val('').focus();
						}else{
							var qrysp = res[0];
							selectrow.find("td:eq(6) span").text(qrysp.des);
							selectrow.find("td:eq(6) span").attr('vname',qrysp.des);
							selectrow.find("td:eq(12) span").text(qrysp.code);
						}
					}
				});
				
				if(checksupplier){
					selectrow.find("td:eq(1) span").text(data2.sp_code).css("text-align","right");
					selectrow.find("td:eq(2) span").text(data2.sp_name);
					selectrow.find("td:eq(2) span").attr('vname',data2.sp_name);
					selectrow.find("td:eq(3) span").text(data2.sp_desc);
					selectrow.find("td:eq(4) span").text(data2.unit3);
					
					selectrow.find("td:eq(6) span").text(0.0).css("text-align","right");
					selectrow.find("td:eq(8) span").text('0.00').css("text-align","right");
					selectrow.find("td:eq(9) span").text($('#positnnamehidden').val());
					selectrow.find("td:eq(9) span").attr('vname',$('#positnnamehidden').val());
					selectrow.find("td:eq(10) span").text(data2.memo);
					selectrow.find("td:eq(11) span").text(data2.unit3);
					selectrow.find("td:eq(12) span").text($('#positncodehidden').val());
					selectrow.find("td:eq(13) span").text($('#positncodehidden').val());
					selectrow.find("td:eq(14) span").text(data2.sp_name);
					
					selectrow.find("td:eq(16) span").text(data2.ncheckdiffrate);
					selectrow.find("td:eq(17) span").text(data2.sp_code);
					selectrow.find("td:eq(18) span").text(data2.unit3);
					selectrow.find("td:eq(19) span").text($('#positnnamehidden').val());
					selectrow.find("td:eq(20) span").text($('#vcontacthidden').val());
					selectrow.find("td:eq(21) span").text($('#vcontactphonehidden').val());
					selectrow.find("td:eq(22) span").text($('#vdistributionaddrhidden').val());

					var cnt = 0;
					$.ajaxSetup({ 
						  async: false 
					});
					var param = {};
					param['sp_code'] = data2.sp_code;
					$.post('<%=path %>/material/findNmincntByRate.do',param,function(re){
						cnt = Number(re);
						selectrow.find("td:eq(5) span").text(Number(cnt).toFixed(2)).css("text-align","right");
						selectrow.find("td:eq(15) span").text(Number(cnt).toFixed(2));
					})
					$.ajax({//查询物资
						type: "POST",
						url: "<%=path%>/material/findMaterialNprice.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
						data: "sp_code="+data2.sp_code,
						dataType: "json",
						success:function(data3){
							if(data3.sp_price == 0){
								selectrow.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
								selectrow.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
								$.ajax({
									type: "POST",
									url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+$('#positncode').val()+"&delivercode="+$('#delivercode').val(),
									data: "sp_code="+data2.sp_code,
									dataType: "json",
									success:function(data4){
										if(data4.sp_price == 0){
											selectrow.find("td:eq(6) span").text('0.00').css("text-align","right");
										}else{
											selectrow.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
											
											var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
											selectrow.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
											$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
										}
									}
								})
							}else{
								selectrow.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
								//-----------
								selectrow.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
								selectrow.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
								//-----------
								var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
								selectrow.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
								$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
							}
						}
					});
					$.fn.autoGrid.setCellEditable(selectrow,5);
				}else{
					$.fn.autoGrid.setCellEditable(selectrow,2);
				}
			}
			
			function setPk_orgMaterial(data){
				if(data.entity[0].vcode==''){
					return;
				}
				selectrow.find("td:eq(9) span").text(data.entity[0].vname);
				selectrow.find("td:eq(9) span").attr('vname',data.entity[0].vname);
				selectrow.find("td:eq(12) span").text(data.entity[0].vcode);
				selectrow.find("td:eq(13) span").text(data.entity[0].pk_id);
				selectrow.find("td:eq(19) span").text(data.entity[0].vname);
				
				var positncode = data.entity[0].pk_id;
				var sp_code = $.trim(selectrow.find("td:eq(17) span").text());
				
				var cnt = Number(selectrow.find("td:eq(5) span").text()).toFixed(2);
				//重新获取合约价
				$.ajax({//查询物资
					type: "POST",
					url: "<%=path%>/material/findMaterialNprice.do?positncode="+positncode+"&delivercode="+$('#delivercode').val(),
					data: "sp_code="+sp_code,
					dataType: "json",
					success:function(data3){
						if(data3.sp_price == 0){
							selectrow.find("td:eq(6)").removeAttr('edit');//把不可修改属性去除
							selectrow.find("td:eq(6)").css('background','#F1F1F1');//去掉可编辑背景色
							$.ajax({
								type: "POST",
								url: "<%=path%>/material/findMaterialNpricesupplier.do?positncode="+positncode+"&delivercode="+$('#delivercode').val(),
								data: "sp_code="+sp_code,
								dataType: "json",
								success:function(data4){
									if(data4.sp_price == 0){
										selectrow.find("td:eq(6) span").text('0.00').css("text-align","right");
									}else{
										selectrow.find("td:eq(6) span").text(data4.sp_price).css("text-align","right");
										
										var nrowmoney = Number(Number(data4.sp_price)*Number(cnt)).toFixed(2);
										selectrow.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
										$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
									}
								}
							})
						}else{
							selectrow.find("td:eq(6) span").text(data3.sp_price).css("text-align","right");
							//-----------
							selectrow.find("td:eq(6)").attr('edit','false');//合约有价格的不能修改setCellEditable中判断
							selectrow.find("td:eq(6)").css('background-color','');//去掉可编辑背景色
							//-----------
							var nrowmoney = Number(Number(data3.sp_price)*Number(cnt)).toFixed(2);
							selectrow.find("td:eq(8) span").text(nrowmoney).css("text-align","right");
							$("#nmoney").val(Number(Number($("#nmoney").val())+Number(nrowmoney)).toFixed(2));
						}
					}
				});
				$.fn.autoGrid.setCellEditable(selectrow,10);
			}
			
			function deleteRow(obj){
				var nmoney = $("#nmoney").val();
				var delmoney = Number($(obj).parent("tr").find('td:eq(8) span').text());
				
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
				
				$("#nmoney").val(Number(nmoney-delmoney).toFixed(2));
			};
			
			//得到日期
			function formatDate(){
			    var date = new Date();
			    return date.getFullYear()+"-"+((date.getMonth()+1)<10?"0"+(date.getMonth()+1):(date.getMonth()+1))+"-"+(date.getDate()<10?"0"+date.getDate():date.getDate());
			}
		</script>
	</body>
</html>
