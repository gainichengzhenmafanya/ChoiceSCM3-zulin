<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div id="tool"></div>
		<form action="<%=path%>/puprorder/addPuprorder2.do" id="savepreorder" name="savepreorder" method="post">
		<input type="hidden" name="positncode" id="positncode" class="text" value="${preOrder.positncode}"/>
		<input type="hidden" name="delivercode" id="delivercode" class="text" value="${preOrder.delivercode}"/>
		<input type="hidden" name="pk_preorders" id="pk_preorders" class="text" value="${preOrder.pk_preorders}"/>
		<input type="hidden" name="bdate" id="bdate" class="text" value="${preOrder.bdate}"/>
			<div class="bj_head" style="height: 30px;">
				<div class="form-line">
					<!-- 到货日 -->
					<div class="form-label" style="width: 80px;margin-left: 0px;"><fmt:message key="Hope_date" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="dhopedate" name="dhopedate" style="text-transform:uppercase;" value="${preOrder.ddate}"  class="Wdate text"/>
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" style="width:83%;">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit_price" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="tax_rate" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="amount" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="Theprocuringentity" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="suppliers" /></span></td>
	                                <td><span style="width: 100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body" style="width:83%;">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:set var="sum_num" value="${0}"/>  <!-- 物资条数 -->
								<c:set var="sum_amount" value="${0}"/>  <!-- 总数量 -->
								<c:set var="sum_totalamt" value="${0}"/>  <!-- 总金额 -->
								<c:forEach var="preOrderd" items="${preOrderds}" varStatus="status">
									<c:set var="sum_num" value="${status.index+1}"/>  
									<c:set var="sum_amount" value="${sum_amount + preOrderd.cnt}"/>  
									<c:set var="sum_totalamt" value="${sum_totalamt + preOrderd.cnt*preOrderd.sp_price}"/>  
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${preOrderd.sp_code}"/></span></td>
										<td><span style="width:140px;background:#F1F1F1;" vname="${preOrderd.sp_name}"><c:out value="${preOrderd.sp_name}"/></span></td>
										<td><span style="width:60px;"><c:out value="${preOrderd.sp_desc}"/></span></td>
										<td><span style="width:60px;"><c:out value="${preOrderd.unit}"/></span></td>
										<td><span style="width:60px;text-align: right;background:#F1F1F1;"><c:out value="${preOrderd.cnt}"/></span></td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${preOrderd.sp_price}" pattern="0.00"/></span></td>
										<td>
											<!-- 税率 -->
											<select style="width:70px;">
												<c:forEach items="${taxListObj}" var="tax">
													<option <c:if test="${preOrderd.tax==tax.tax}">selected</c:if> value="${tax.tax}">${tax.taxdes}</option>
												</c:forEach>
											</select>
										</td>
										<td><span style="width:60px;text-align: right;"><fmt:formatNumber value="${preOrderd.cnt*preOrderd.sp_price}" pattern="0.00"/></span></td>
										<td><span style="width:140px;background:#F1F1F1;" title="${preOrderd.positncode}"><c:out value="${preOrderd.positnname}"/></span></td>
										<td><span style="width:140px;background:#F1F1F1;" title="${preOrderd.delivercode}"><c:out value="${preOrderd.delivername}"/></span></td>
										<td><span style="width:100px;"><c:out value="${purtempletd.vmemo}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 10px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="hidden" name="pk_account" id="pk_account" class="text" value="${puprOrder.pk_account}" />
							<input type="text" style="border: 0px;" name="accountname" id="accountname" class="text" readonly="readonly" value="${puprOrder.accountname}"/>
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="dmaketime" id="dmaketime" readonly="readonly" class="text" value="${puprOrder.dmaketime}" />
						</div>
						<div class="form-label" style="width: 10%;margin-left: 35px;"><fmt:message key="total_amount1" />:</div>
						<div class="form-input" style="width:100px;">
							<input type="text" style="border: 0px;" name="nmoney" id="nmoney" readonly="readonly" class="text" value="${sum_totalamt}" />
						</div>	
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_sp_code" />
			<input type="hidden" id="selected_code" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//回车换焦点start************************************************************************************************
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}',maxDate:'%y-%M-{%d+'+180+'}'});
				});
			   
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),150);	//计算.grid的高度
			    $('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width())+160);
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width())+160);
				
				//按钮
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="save" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-80px','-0px']
						},
						handler: function(){
							savepuprorder();
						}
					},{
						text: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							window.parent.$(".close").click();
						}
					}
				]});
				
				//可编辑
				editCells();
			});
			
			//点击事件
		 	$('.table-body').bind('mousedown',function(e){//mousedown
		 		if($(e.srcElement).is("input")||$(e.srcElement).is("span")){
		 			var index=$(e.srcElement).closest('td').index();
		    		if(index==10){
		    			var rownum = $(e.srcElement).closest('tr');
						var sp_code = rownum.find("td:eq(1) span").text();
						$.ajax({//查询供应商
							type: "POST",
							url: "<%=path%>/deliver/findGySpGysTop10.do",
							data: "sp_code="+sp_code+"&positncode="+$("#positncode").val(),
							dataType: "json",
							success:function(obj){
								//供应商
								var select = $("<select style='width:150px;' onchange='changeDeliver(this);'></select>");
								for(var i=0,len=obj.length;i<len;i++){
									var opt = $("<option value='"+obj[i].delivercode+"' tax='"+obj[i].tax+"' sp_price='"+obj[i].sp_price+"'>"+obj[i].delivername+"</option>");
									select.append(opt);
								}
								rownum.find("td:eq(10)").html(select);
							}
						});
		    		}
		    	}
		 	});
			
			//改变供应商
			function changeDeliver(ee){
				var sobj = $(ee);
				var trobj = sobj.closest('tr');
				var tax = sobj.find("option:selected").attr('tax');
				var sp_price = sobj.find("option:selected").attr('sp_price');
				var cnt = trobj.find('td:eq(5) span').text()||trobj.find('td:eq(5) input').val();
				var amt = sp_price * cnt;
				
				//原金额
				var yje = trobj.find('td:eq(8) span').text();
				var totalamt = $("#nmoney").val();
				totalamt = Number(totalamt) + Number(amt) - Number(yje);
				$("#nmoney").val(totalamt);
				
				trobj.find('td:eq(6) span').text(Number(sp_price).toFixed(2));
				trobj.find('td:eq(7) select').val(tax);
				trobj.find('td:eq(8) span').text(Number(amt).toFixed(2));
			}
			
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:16,
					widths:[26,110,150,70,70,70,70,70,70,150,110,70,70,70],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1"},{background:"#F1F1F1"},'',{background:"#F1F1F1"},'',{background:"#F1F1F1"},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[5,10],
					onEnter:function(data){
					},
					cellAction:[{
						index:5,
						action:function(rownum,data2){
							var cgnum = $.trim(rownum.find("td:eq(5) span").text());
							if(cgnum==''){
								cgnum = $.trim(rownum.find("td:eq(5) input").val());
								rownum.find("td:eq(5) span").text(cgnum);
							}
							if(isNaN(cgnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}
							else if(Number(cgnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}
							else if(Number(cgnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alert('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else{
								var nmoney = Number($("#nmoney").val());
								var prerowmoney = Number($.trim(rownum.find("td:eq(8) span").text())).toFixed(2);
								if(rownum.next().html()){
									$.fn.autoGrid.setCellEditable(rownum.next(),5);
								}
							}
						},
						onCellEdit:function(event,data,row){
							var nmoney = Number($("#nmoney").val());
							var prerowmoney = Number($.trim(row.find("td:eq(8) span").text())).toFixed(2);
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								row.find('td:eq(8) span').text('0.00');
								$("#nmoney").val(Number(Number(nmoney)-Number(prerowmoney)).toFixed(2));
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								cnt = Number($.trim(cnt));
								var price = row.find("td:eq(6) span").text();
								if(isNaN($.trim(price))){
									row.find('td:eq(6) span').text('0.00');
								}else{
									price = Number($.trim(price));
									var nrowmoney = Number(cnt*price).toFixed(2);
									row.find('td:eq(8) span').text(nrowmoney);
									//计算总金额
									var diffmoney = Number(nrowmoney-prerowmoney).toFixed(2);
									$("#nmoney").val(Number(Number(nmoney)+Number(diffmoney)).toFixed(2));
								}
							}
						}
					},{
						index:10,
						action:function(row,data){
							row.find('td:eq(10) span').text(data.value);
							if(row.next().html()){
								$.fn.autoGrid.setCellEditable(row.next(),5);
							}
						},
						onCellEdit:function(event,data,rownum){
							
						}
					}]
				});
			}

			//保存锁
			var savelock = false;
			
			//保存订单
			function savepuprorder(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				var i = 1;
				var check = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					//判断有无供应商
					var gys = $(this).find("td:eq(10) span").text()||$(this).find("td:eq(10) select").val();
					if(gys==''||gys==undefined){
						check = false;
						return false;
					}
					i++;
				});
				
				//有没有供应商的
				if(!check){
					alerterror("第"+i+"行没有供应商！");
					return;
				}
				//属性
				var keys = ["","sp_code","sp_name","sp_desc","unit","ncnt","sp_price","tax","nmoney","positnname","delivername"];
				var data = {};
				var i = 0;
				//赋值
				data["pk_preorders"] = $("#pk_preorders").val();
				data["dhopedate"] = $("#dhopedate").val();
				var rows = $(".grid .table-body table tr");
				var lineNumber = 0;
				for(i=0;i<rows.length;i++){
					var status=$(rows[i]).find('td:eq(0)').text();
					var j = 0;
					for(j=1;j<=keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());					
						if(value){
							data["puprOrderdList["+lineNumber+"]."+keys[j]] = value;
							if(j==7){
								var tax = $(rows[i]).find("td:eq(7) select").val();
								//税率
								data["puprOrderdList["+lineNumber+"].tax"] = tax?tax:0;
								data["puprOrderdList["+lineNumber+"].taxdes"] = $(rows[i]).find("td:eq(7) select option:selected").text();
							}
							else if(j==9){
								//采购组织编码
								data["puprOrderdList["+lineNumber+"].positncode"] = $(rows[i]).find("td:eq(9) span").attr("title");
							}
						}
						//供应商
						if(j==10){
							var sele = $(rows[i]).find("td:eq(10) select");
							var isHasSelect = false;
							if(sele.length>0){
								isHasSelect = true;
							}
							if(isHasSelect){
								//供应商编码
								data["puprOrderdList["+lineNumber+"].delivercode"] = $(rows[i]).find("td:eq(10) select option:selected").val();
								data["puprOrderdList["+lineNumber+"].delivername"] = $(rows[i]).find("td:eq(10) select option:selected").text();
							}else{
								//供应商编码
								data["puprOrderdList["+lineNumber+"].delivercode"] = $(rows[i]).find("td:eq(10) span").attr("title");
								data["puprOrderdList["+lineNumber+"].delivername"] = $(rows[i]).find("td:eq(10) span").text();
							}
						}
					}
					lineNumber++;
				}
				
				//保存方法
				$.ajaxSetup({async:false});
				$.post("<%=path%>/puprorder/addPuprorder2.do",data,function(data){
					var rs = data;
                    if(isNaN(rs)){
                        alerttips('<fmt:message key="save_fail"/>！');
                        return;
                    }
					switch(Number(rs)){
					case -1:
						alerttips('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
							type: 'success',
							msg: '<fmt:message key="save_successful"/>！',
							speed: 3000,
							handler:function(){
								parent.reloadPage();
								parent.$('.close').click();}
							});
						break;
					}
				});	
			}
		</script>
	</body>
</html>
