<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
	</head>
	<body>
<!-- 	<div class="form"> -->
<%-- 		<form id="inAuditMaterial" method="post" action="<%=path %>/favorites/addInAuditMaterial.do?"> --%>
			<div class="bj_head" style="height: 50px;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="module" /><fmt:message key="gyszmmcs" />:</div>
					<div class="form-input" style="width:130px;">
						<input type="text" readonly="readonly" name="vname" id="vname" class="text" value="${tableMainCgqdList.vname}"/>
					</div>
					<div class="form-label"><fmt:message key="creation_date" />:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="ddate" id="ddate" class="text" readonly="readonly" value="${tableMainCgqdList.ddate}" />
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="summary" /></div>
					<div class="form-input" style="width:130px;">
						<input type="text" readonly="readonly" name="vmemo" id="vmemo" class="text" value="${tableMainCgqdList.vmemo}" style="width: 383px;" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span style="width: 40px;"><fmt:message key="shouyexuhao" /></span></td>
									<td><span style="width: 70px;"><fmt:message key="shouyewuzibm" /></span></td>
	                                <td><span style="width: 70px;"><fmt:message key="shouyewuzimc" /></span></td>
	                                <td><span style="width: 70px;"><fmt:message key="scscguige" /></span></td>
	                                <td><span style="width: 70px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 180px;"><fmt:message key="scscgongyingshang" /></span></td>
	                                <td><span style="width: 70px;"><fmt:message key="Reference_resources_price" /></span></td>
	                                <td><span style="width: 120px;"><fmt:message key="scscbzbz" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="tableMainCgqd" items="${tableMainCgqdLists}" varStatus="status">
									<tr>
										<td><span style="width:40px;">${status.index+1}</span></td>
										<td><span style="width:70px;"><c:out value="${tableMainCgqd.vmcode}"/></span></td>
										<td><span style="width:70px;"><c:out value="${tableMainCgqd.vmname}"/></span></td>
										<td><span style="width:70px;"><c:out value="${tableMainCgqd.vspecfication}"/></span></td>
										<td><span style="width:70px;"><c:out value="${tableMainCgqd.vunitname}"/></span></td>
										<td><span style="width:180px;"><c:out value="${tableMainCgqd.delivername}"/></span></td>
										<td><span style="width:70px;text-align: right;"><c:out value="${tableMainCgqd.ninprice}"/></span></td>
										<td><span style="width:120px;"><c:out value="${tableMainCgqd.vmemo}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
<!-- 		</form> -->
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript">
			
// 			function inMaterial(){
// 				$('#inAuditMaterial').submit();
// 			}
		</script>
</body>
</html>
