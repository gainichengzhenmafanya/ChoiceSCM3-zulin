<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.search-div .form-line .form-label{
				width: 70px;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div style="height:50%;">
	    	<div id="tool"></div>
		    	<form id="queryForm" action="<%=path%>/preorder/listToPuprorder.do" method="post">
		    		<div style="height: 28px;border: solid 1px gray;">
						<div class="form-line">
							<div class="form-label" style="width: 80px;margin-left: 0px;"><span style="color:red;">*</span><fmt:message key="Theprocuringentity" />:</div>
							<div class="form-input">
								<input type="hidden" name="positncode" id="positncode" class="text" value="${preOrder.positncode}"/>
								<input type="text" name="positnname" id="positnname" style="margin-bottom: 6px;" readonly="readonly" class="text" value="${preOrder.positnname}"/>
								<img id="positncodebutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
							</div>
							<div class="form-label"><fmt:message key="startdate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="bdate" name="bdate" style="text-transform:uppercase;" value="${preOrder.bdate}" class="Wdate text"/>
							</div>
							<div class="form-label" ><fmt:message key="enddate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${preOrder.edate}" class="Wdate text"/>
							</div>
						</div>
					</div>
					<div class="grid" class="grid" style="overflow: auto;positn:static;">
						<div class="table-head">
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td><span style="width:30px;"></span></td>
										<td><span style="width:10px;"><input type="checkbox" id="chkAll"/></span></td>
										<td><span style="width:100px;"><fmt:message key="orders_code" /></span></td>
										<td><span style="width:150px;"><fmt:message key="Theprocuringentity" /></span></td>
										<td><span style="width:80px;"><fmt:message key="document_date" /></span></td>
										<td><span style="width:150px;"><fmt:message key="documents_time" /></span></td>
										<td><span style="width:60px;"><fmt:message key="Document_status" /></span></td>
										<td><span style="width:150px;"><fmt:message key="user_name" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="ordermap" varStatus="step" items="${preOrders}">
										<tr>
											<td class="num"><span style="width:30px;">${step.count}</span></td>
											<td><span style="width:10px; text-align: center;">
												<input type="checkbox" name="idList" id="chk_${ordermap.pk_preorder}" value="${ordermap.pk_preorder}"/>
											</span></td>
											<td><span title="${ordermap.pk_preorder}" style="width:100px;text-align: left;">${ordermap.pk_preorder}</span></td>
											<td><span title="${ordermap.positnname}" style="width:150px;text-align: left;">${ordermap.positnname}</span></td>
											<td><span title="${ordermap.ddate}" style="width:80px;text-align: center;">${ordermap.ddate}</span></td>
											<td><span title="${ordermap.dtime}" style='width:150px;text-align: center;'>${ordermap.dtime}</span></td>
											<td><span style="width:60px;text-align: center;">
												<c:if test="${ordermap.dtype==1}">未生成</c:if>
												<c:if test="${ordermap.dtype==2}"><span>已生成</span></c:if>
											</span></td>
											<td><span title="${ordermap.accountId}" style="width:150px;text-align: center;">${ordermap.accountId}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<page:page form="queryForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
				</form>
			</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
 			$("#vbillno").focus();
			//快捷键操作（Alt+*）
/*			$(document).keydown(function (e) {
			    var doPrevent;
			    if (event.keyCode==78 && event.altKey) {//alt+N
			    	if(!$('#window_supply').html()){
			    		savepuprorder();
			    	}
			    }else if(event.keyCode==77 && event.altKey){//alt+M
			    	if(!$('#window_supply').html()){
			    		savepuprorderlist();
			    	}
			    }else if(event.keyCode==66 && event.altKey){//alt+B
			    	if(!$('#window_supply').html()){
			    		savepuprorderlast();
			    	}
			    }else if(event.keyCode==85 && event.altKey){//alt+U
			    	if(!$('#window_supply').html()){
			    		updatepuprorder();
			    	}
			    }else{
			        doPrevent = false;
			    }
			    if (doPrevent)
			        e.preventDefault();
			}); */
			function setSupplier(data){
				$("#code").val(data.delivercode);//主键
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:800,
					isReal:'isReal',
					callBack:'setSupplier',
					domId:'delivercode',
					single:false
				});
			});
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
				//重新加载
				reloadPage();
			}
			$('#positncodebutton').click(function(){
				chooseDepartMentList({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:650,
					callBack:'setPk_org',
					domId:'positncode',
					type:1,
					typn:'1202',
					single:true
				});
			});
			$(document).ready(function(){
				$("#bdate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});
				});
				$("#edate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});
				});
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),105);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度 
				$(".page").css("margin-bottom",$(".mainFrame").height()-15);
				$('.grid .table-body tr').live('click',function(){
					 var checked = $(this).hasClass('tr-over');
					 if(checked){
						 $(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
					 }else{
					 	$(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 }
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							$('#queryForm').submit();
						}
					},{
						text: '确定',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							addtopreorder();
						}
					},{
						text: '<fmt:message key="cancel" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							parent.$('.close').click();
						}
					}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});
			
			//展示生成采购订单的页面
			function addtopreorder(){
				//采购组织
				var positncode = $("#positncode").val();
				if(positncode==''){
					alerterror('选择采购组织！');
					return;
				}
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() >= 1){
					var chkval = new Array();
					checkboxList.filter(':checked').each(function(){
						chkval.push($(this).attr("value"));
					});
					$('body').window({
						id: 'window_updatepreorder',
						title: '<fmt:message key="insert" /><fmt:message key="PuprOrder" />',
						content: '<iframe id="updatepreorder" frameborder="0" src="<%=path%>/puprorder/toAddPreorderList.do?pk_preorders='+chkval.join(',')+'&positncode='+positncode+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true
					});
				}else if(checkboxList && checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="Only_select_one_data" />');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':checkbox')[0].checked) {
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			});
			//---------------------------
// 			全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$("#queryForm").submit();
			}
		    document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
	        //根据订单得到状态
	        function getPuprOrderIstate(pk_puprorder){
				var res = 1;
				$.ajax({
					type: "POST",
					url: "<%=path%>/puprorder/queryPuprorderIstate.do",
					data: {'pk_puprorder':pk_puprorder},
					dataType: "json",
					async:false,
					success: function(data){
						res = data;
						}
					});
				return res;
	        }
		</script>
	</body>
</html>