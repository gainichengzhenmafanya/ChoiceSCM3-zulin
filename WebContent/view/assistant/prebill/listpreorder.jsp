<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Module Info</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
		    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.search-div .form-line .form-label{
				width: 70px;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div style="height:50%;">
	    	<div id="tool"></div>
		    	<form id="queryForm" action="<%=path%>/preorder/list.do" method="post">
		    		<input type="hidden" id="dtype" name="dtype" value="${preOrder.dtype }"/>
		    		<div style="height: 26px;border: solid 1px gray;">
						<div class="form-line">
							<div class="form-label"><fmt:message key="startdate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="bdate" name="bdate" style="text-transform:uppercase;" value="${preOrder.bdate}" class="Wdate text"/>
							</div>
							
							<div class="form-label" ><fmt:message key="enddate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${preOrder.edate}" class="Wdate text"/>
							</div>	
							
							<div class="form-label" ><fmt:message key="status" />:</div>
							<div class="form-input">
								<select id="dtypeselect" style="margin-top:2px;" onchange="changeType();">
									<option value="1" <c:if test="${preOrder.dtype==1}">selected</c:if>>未生成</option>
									<option value="2" <c:if test="${preOrder.dtype==2}">selected</c:if>>已生成</option>
								</select>
							</div>	
							
<%-- 						<div class="form-label"><fmt:message key="orders_code" />:</div>
							<div class="form-input" >
								<input type="text" name="vbillno" id="vbillno" style="width: 130px;" class="text" value="${puprOrder.vbillno}"/>
							</div>
							<div class="form-label" ><fmt:message key="Theprocuringentity" />:</div>
							<div class="form-input">
								<input type="hidden" name="positncode" id="positncode" class="text" value="${puprorder.positncode}"/>
								<input type="text" name="positnname" id="positnname" style="margin-bottom: 6px;width: 130px;" class="text" value="${puprorder.positnname}"/>
								<img id="positncodebutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
							</div> --%>
						</div>
						<div class="form-line">
<%-- 						<div class="form-label" ><fmt:message key="enddate" />:</div>
							<div class="form-input">
								<input autocomplete="off" type="text" id="edate" name="edate" style="text-transform:uppercase;" value="${puprorder.edate}" class="Wdate text"/>
							</div>	
							<div class="form-label"><fmt:message key="Document_status" />:</div>
							<div class="form-input">
								<select id="istate" name="istate" class="select" style="width:132px;margin-bottom: 5px;" onkeyup="ajaxSearch()">
									<option value="0" <c:if test="${puprorder.istate==0}">selected=selected</c:if> ><fmt:message key="all" /></option>
									<option value="1" <c:if test="${puprorder.istate==1}">selected=selected</c:if> ><fmt:message key="unchecked" /></option>
									<option value="4" <c:if test="${puprorder.istate==4}">selected=selected</c:if> ><fmt:message key="checked" /></option>
									<!-- <option value="5" <c:if test="${puprorder.istate==5}">selected=selected</c:if> ><fmt:message key="Have_the_inspection" /></option> -->
									<option value="6" <c:if test="${puprorder.istate==6}">selected=selected</c:if> ><fmt:message key="The_storage" /></option>
									<option value="7" <c:if test="${puprorder.istate==7}">selected=selected</c:if> ><fmt:message key="Settled" /></option>
									<option value="100" <c:if test="${puprorder.istate==100}">selected=selected</c:if> ><fmt:message key="Last_add" /></option>
								</select>
							</div>
							<div class="form-label" ><fmt:message key="suppliers" />:</div>
							<div class="form-input">
								<input type="hidden" id="delivercode" name="delivercode" style="width:130px" class="text" value="${puprorder.delivercode}"/>
								<input type="text" id="delivername"  name="delivername" style="width:130px;margin-bottom: 6px;" class="text" value="${puprorder.delivername}"/>
								<img id="supplierbutton" class="search" style="margin-bottom: 6px;" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
<!-- 								<input type="button" id="supplierbutton" style="width: 30px;" value="查询"/> -->
							</div> --%>
						</div>
					</div>
					<div class="grid" class="grid" style="overflow: auto;positn:static;">
						<div class="table-head">
							<table cellspacing="0" cellpadding="0" id="thGrid">
								<thead>
									<tr>
										<td><span style="width:30px;"></span></td>
										<td><span style="width:10px;"><input type="checkbox" id="chkAll"/></span></td>
										<td><span style="width:100px;"><fmt:message key="orders_code" /></span></td>
										<td><span style="width:150px;"><fmt:message key="Theprocuringentity" /></span></td>
										<td><span style="width:80px;"><fmt:message key="document_date" /></span></td>
										<td><span style="width:150px;"><fmt:message key="documents_time" /></span></td>
										<td><span style="width:60px;"><fmt:message key="Document_status" /></span></td>
										<td><span style="width:150px;"><fmt:message key="user_name" /></span></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class="table-body">
							<table id="tblGrid" cellspacing="0" cellpadding="0">
								<tbody>
									<c:forEach var="ordermap" varStatus="step" items="${preOrders}">
										<tr>
											<td class="num"><span style="width:30px;">${step.count}</span></td>
											<td><span style="width:10px; text-align: center;">
												<input type="checkbox" name="idList" id="chk_${ordermap.pk_preorder}" value="${ordermap.pk_preorder}"/>
											</span></td>
											<td><span title="${ordermap.pk_preorder}" style="width:100px;text-align: left;">${ordermap.pk_preorder}</span></td>
											<td><span title="${ordermap.positnname}" style="width:150px;text-align: left;">${ordermap.positnname}</span></td>
											<td><span title="${ordermap.ddate}" style="width:80px;text-align: center;">${ordermap.ddate}</span></td>
											<td><span title="${ordermap.dtime}" style='width:150px;text-align: center;'>${ordermap.dtime}</span></td>
											<td><span style="width:60px;text-align: center;">
												<c:if test="${ordermap.dtype==1}">未生成</c:if>
												<c:if test="${ordermap.dtype==2}"><span>已生成</span></c:if>
											</span></td>
											<td><span title="${ordermap.accountName}" style="width:150px;text-align: center;">${ordermap.accountName}</span></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<page:page form="queryForm" page="${pageobj}"></page:page>
					<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
					<input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
				</form>
			</div>
			<form id="printForm" action="<%=path%>/puprorder/printPuprorder.do" method="post">
				<input type="hidden" id="printpk" name="pk_puprorder" value="${pk_puprorder}"/>
			</form>
		<div class="mainFrame" style=" height:50%;width:100%;margin-top: 0px;">
<!-- 		<span class="button"   id="evaluate"  style="text-align: center;margin-top: 30px;" >
				<input type="button"  style="width:50px;height:100%;border: 0px;background-color: #BCD7EF;cursor:pointer;"  value="评价"></input>
			</span> -->
			<iframe src="<%=path%>/preorder/queryAllPreOrderds.do?pk_preorder=${pk_preorder}" name="mainFrame" id="mainFrame" frameborder="0" ></iframe>
		</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
	<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
	<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
	<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
	<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
	<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		
		<script type="text/javascript">
/* 			$("#vbillno").focus();
			//快捷键操作（Alt+*）
			$(document).keydown(function (e) {
			    var doPrevent;
			    if (event.keyCode==78 && event.altKey) {//alt+N
			    	if(!$('#window_supply').html()){
			    		savepuprorder();
			    	}
			    }else if(event.keyCode==77 && event.altKey){//alt+M
			    	if(!$('#window_supply').html()){
			    		savepuprorderlist();
			    	}
			    }else if(event.keyCode==66 && event.altKey){//alt+B
			    	if(!$('#window_supply').html()){
			    		savepuprorderlast();
			    	}
			    }else if(event.keyCode==85 && event.altKey){//alt+U
			    	if(!$('#window_supply').html()){
			    		updatepuprorder();
			    	}
			    }else{
			        doPrevent = false;
			    }
			    if (doPrevent)
			        e.preventDefault();
			}); */
<%-- 		function setSupplier(data){
				$("#code").val(data.delivercode);//主键
				$("#delivercode").val(data.delivercode);//编码
				$("#delivername").val(data.delivername);//名称
			}
			$('#supplierbutton').click(function(){
				selectSupplier({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:800,
					isReal:'isReal',
					callBack:'setSupplier',
					domId:'delivercode',
					single:false
				});
			});
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
			}
			$('#positncodebutton').click(function(){
				chooseDepartMentList({
					basePath:'<%=path%>',
					title:"123",
					height:400,
					width:650,
					callBack:'setPk_org',
					domId:'positncode',
					type:1,
					typn:'1202',
					single:true
				});
			});
			function ajaxSearch(){
				if (event.keyCode == 13){	
					$('.search-div').hide();
					$("#vbillno").val(stripscript($("#vbillno").val()));
					$('#queryForm').submit();
				} 
			} --%>
			$(document).ready(function(){
/* 				/* 模糊查询提交 */
/*  			$("#search").bind('click', function() {
					$('.search-div').hide();
					$('#queryForm').submit();
				}); */
				/* 模糊查询清空 */
/* 				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				}); */

				$("#bdate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'edate\')}'});
				});
				$("#edate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'bdate\')}'});
				});
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),105);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				
				loadGrid();//  自动计算滚动条的js方法
				changeTh();//拖动 改变table 中的td宽度
				$(".page").css("margin-bottom",$(".mainFrame").height()-15);
				//点击行事件
				$('.grid .table-body tr').live('click',function(){
					 var pk_preorder=$(this).find('td:eq(1)').find('input').val();
					 var url="<%=path%>/preorder/queryAllPreOrderds.do?pk_preorder="+pk_preorder;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function () {
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				 });
				var toolbar = $('#tool').toolbar({
					items: [{
						text: '<fmt:message key="select" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','-40px']
						},
						handler: function(){
							$("#wait2").css("display","block");
							$("#wait").css("display","block");
							$('#queryForm').submit();
						}
					},{
						text: '<fmt:message key="insert" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							savepreorder();
						}
					},{
						text: '<fmt:message key="update" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							updatepreorder();
						}
					},{
						text: '<fmt:message key="delete" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-38px','0px']
						},
						handler: function(){
							deletepreorder();
						}
					},<%-- {
	                    text: '<fmt:message key="check" />',
	                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'upload')},
	                    icon: {
	                        url: '<%=path%>/image/Button/op_owner.gif',
	                        positn: ['0px','-40px']
	                    },
	                    handler: function(){
	                    	uploadPuprOrdr();
	                    }
	                },{
						text: '<fmt:message key="print" />',
						useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'print')},
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['0px','0px']
						},
						handler: function(){
							printpuprorder();
						}
					}, --%>{
						text: '<fmt:message key="quit" />',
						icon: {
							url: '<%=path%>/image/Button/op_owner.gif',
							positn: ['-160px','-100px']
						},
						handler: function(){
							invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
							
						}
					}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
				
				$("#evaluate").css("margin-left",$(document.body).width()-120);
				
				$("#evaluate").bind('click',function(){
					
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList && checkboxList.filter(':checked').size() == 1){
						var istate = $(checkboxList.filter(':checked')[0]).closest('tr').find('td:eq(7) span').attr('istate');
						if(istate==1){
							  if(Number(istate)==1){
								  alerterror('该订单未提交，不能评价！');
								  return;
							  }
						}
						
						var state = $("#evaluate").find('input').attr('value');
						if(state=='评价'){
							$("#evaluate").find('input').attr('value','保存');
							window.document.getElementById("mainFrame").contentWindow.evaluatetabs();
						}else{
							$("#evaluate").find('input').attr('value','评价');
							window.document.getElementById("mainFrame").contentWindow.saveevaluate();
						}
					}else{
						alerterror('请选择一条订单进行评价！');
					}
				});
			});
			
			
			//新增采购订单==补录单
			function savepuprorderlast(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="Makeup_Orders" />',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/puprorder/toAddPuprorder.do?sta=100"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//新增来自报货分拨
			function savedisFenbo(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="from" /><fmt:message key="reported_cargo_distribution" />',
					content: '<iframe id="disFenbo" frameborder="0" src="<%=path%>/puprorder/selectDisfenbo.do?action=init"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="select" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									
										window.document.getElementById("disFenbo").contentWindow.selectList();
								}
							},{
								text: '<fmt:message key="enter" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
								
										window.document.getElementById("disFenbo").contentWindow.enterList();
									
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							},
						]
					}
				});
			}
			//新增采购订单==来自模板
			function savepuprorderlist(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" /><fmt:message key="PuprOrder" />',
					content: '<iframe id="savepuprorder" frameborder="0" src="<%=path%>/puprorder/toAddPuprorderList.do?sta=1"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('savepuprorder')&&window.document.getElementById("savepuprorder").contentWindow.validate._submitValidate()){
										window.document.getElementById("savepuprorder").contentWindow.savepuprorder();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			//==============================================================================================================
			//==============================================================================================================
			
			
			//新增预订单
			function savepreorder(){
				$('body').window({
					id: 'window_savepreorder',
					title: '<fmt:message key="insert" /><fmt:message key="pre_order" />',
					content: '<iframe id="listFormmm" frameborder="0" src="<%=path%>/preorder/toAddPreorder.do"></iframe>',
					width: '100%',
					height: '100%',
					draggable: true,
					isModal: true<%-- ,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									//if(getFrame('listFormmm')&&window.document.getElementById("listFormmm").contentWindow.validate._submitValidate()){
										window.document.getElementById("listFormmm").contentWindow.savepreorder();
									//}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					} --%>
				});
			}
			
			
			//修改预订单
			function updatepreorder(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var dtype='';
					checkboxList.filter(':checked').each(function(){
						dtype = $.trim($(this).closest('tr').find('td:eq(6)').find('span').text()); 
					});
					if(dtype=='已生成'){
						alerterror('<fmt:message key="This_order_have_overed" />');
						return;
					}
					//后台得到是否已经审核过
/* 					var htstate = getPuprOrderIstate(chkValue);
					if(htstate>1){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					} */
					$('body').window({
						id: 'window_updatepreorder',
						title: '<fmt:message key="update" /><fmt:message key="PuprOrder" />',
						content: '<iframe id="updatepreorder" frameborder="0" src="<%=path%>/preorder/toUpdatePreorder.do?pk_preorder='+chkValue+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true<%-- ,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										//if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepreorder").contentWindow.savepreorder();
										//}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						} --%>
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="Only_select_one_data" />');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			
			//删除预订单
			function deletepreorder(){
				var flag = true;
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		    	 //每次只能删除一条限制
		    	 if(checkboxList&&checkboxList.filter(':checked').size()>1){
		    		 alerterror("<fmt:message key='please_select_data' />！");
		    		 return;
		    	 }
		    	 //每次只能删除一条限制
		    	 if(checkboxList&&checkboxList.filter(':checked').size()==0){
		    		 alerterror("<fmt:message key='please_select_data' />！");
		    		 return;
		    	 }
				if(checkboxList && checkboxList.filter(':checked').size()>0){
				    var chkValue = [];
					var dtype='';
					checkboxList.filter(':checked').each(function(){
						dtype = $.trim($(this).closest('tr').find('td:eq(6)').find('span').text());
						chkValue.push($(this).val());
					});
					if(dtype=='已生成'){
						alerterror('<fmt:message key="This_order_have_overed" />');
						return;
					}
					if(flag){
					    alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var param = {};
						param['pk_preorder'] = chkValue.join(",");
							$.post("<%=path%>/preorder/deletePreorder.do",param,function(data){
								if(data=='OK'){
									alerttips('<fmt:message key="successful_deleted" />');
									$("#queryForm").submit();
								}
							})
						});
					}
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			
			
			//==============================================================================================================
			//==============================================================================================================
			
			
			
			
			//修改采购订单
			function updatepuprorder(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					var istate='';
					checkboxList.filter(':checked').each(function(){
						istate = $.trim($(this).closest('tr').find('td:eq(7)').find('span').attr('istate')); 
					});
					if(istate!=1 && istate!=100){
						alerterror('<fmt:message key="This_order_have_overed" />');
						return;
					}
					//后台得到是否已经审核过
					var htstate = getPuprOrderIstate(chkValue);
					if(htstate>1){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					}
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" /><fmt:message key="PuprOrder" />',
						content: '<iframe id="updatepuprorder" frameborder="0" src="<%=path%>/puprorder/toUpdatePuprorder.do?pk_puprorder='+chkValue+'&istate='+istate+'"></iframe>',
						width: '100%',
						height: '100%',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updatepuprorder')&&window.document.getElementById("updatepuprorder").contentWindow.validate._submitValidate()){
											window.document.getElementById("updatepuprorder").contentWindow.updatepuprorder();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="Only_select_one_data" />');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除
			function deletepuprorder(){
				var flag = true;
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		    	 //每次只能审核一条限制
		    	 if(checkboxList&&checkboxList.filter(':checked').size()>1){
		    		 alerterror("<fmt:message key='please_select_data' />！");
		    		 return;
		    	 }
				if(checkboxList && checkboxList.filter(':checked').size() > 0){
					    var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							var row = $(this).closest('tr');
							if($.trim(row.children('td:eq(7)').text()) != "<fmt:message key='unchecked' />"){
								flag = false;
								alerterror('<fmt:message key="bill_haven_enter" />，<fmt:message key="cannotdelete" />！');
								return;
							}
						});
						if(flag){
							    //后台得到是否已经审核过
							    var htstate = getPuprOrderIstate(chkValue.join(","));
							    if(htstate>1){
								  alerterror('<fmt:message key="This_order_have_overed" />');
								  reloadPage();
								  return;
							    }
							    alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
								var param = {};
								param['pks'] = chkValue.join(",");
								$.post("<%=path%>/puprorder/deletePuprorder.do",param,function(data){
									if(data=='OK'){
										alerttips('<fmt:message key="successful_deleted" />');
										$("#queryForm").submit();
									}
								})
							});
						}
					
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
				
			}
			//------------------------------
			//点击checkbox改变
			$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
				if ($(this)[0].checked) {
				    $(this).attr("checked", false);
				}else{
					$(this).attr("checked", true);
				}
			});
			// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').bind("click", function () {
				if ($(this).find(':checkbox')[0].checked) {
					$(this).find(':checkbox').attr("checked", false);
				}else{
					$(this).find(':checkbox').attr("checked", true);
				}
			});
			//---------------------------
// 			全选
			$("#chkAll").click(function() {
		    	if (!!$("#chkAll").attr("checked")) {
		    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
		    	}else{
		            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
	            }
		    });
			function reloadPage(){
				$("#queryForm").submit();
			}
			//改变状态
			function changeType(){
				$("#dtype").val($("#dtypeselect option:selected").val());
				reloadPage();
			}
			
		    //上传订单
		    function uploadPuprOrdr(){
		    	 var checkboxList = $('.grid').find('.table-body').find(':checkbox');
		    	 //每次只能审核一条限制
		    	 if(checkboxList&&checkboxList.filter(':checked').size()>1){
		    		 alerterror("<fmt:message key='please_select_data' /><fmt:message key='check' />！");
		    		 return;
		    	 }
		         if(checkboxList&&checkboxList.filter(':checked').size() > 0){
		        	 var flag = "0";
		        	 checkboxList.filter(':checked').each(function(){
		        		 if($.trim($(this).closest('tr').find('td').eq(7).find('span').text())!='<fmt:message key="unchecked" />'){
		        			 flag = "1";
		        		 }
		     		 });
		        	 if(flag == "1"){
		        		 alerterror("<fmt:message key='checked' />！");
		        		 return;
		        	 }
	                 var codeValue=[];
	                 checkboxList.filter(':checked').each(function(){
	                     codeValue.push($(this).val());
	                 });
					 //后台得到是否已经审核过
					 var htstate = getPuprOrderIstate(codeValue.join(","));
					 if(htstate>1){
						alerterror('<fmt:message key="This_order_have_overed" />');
						reloadPage();
						return;
					 }
		        	 //判断是否能够提交不是当前用户创建的订单
		        	 if("${vsubmitothorder}"=="N"){
							$.ajaxSetup({async:false});
							var datas = {};
							datas["pk_puprorder"] = codeValue.join(",");
							$.post('<%=path%>/puprorder/checkOrderIsMine.do',datas,function(data){
								if(data!='ok'){
									alerterror('<fmt:message key="ORDR"/>：'+data+'，<fmt:message key="notyoucreatecantsubmit"/>');
									return;
								}
							});
		        	 }
		        	 alertconfirm('<fmt:message key="confirm"/><fmt:message key="check"/><fmt:message key="ORDR"/>?',function(){
					 //改状态js20161215
					 $.ajax({
						type: "POST",
						url: "<%=path%>/puprorder/updatePuprOrdersState.do",
						data: {pk_puprorder:codeValue.join(",")},
						dataType: "json",
						async:false,
						success: function(data){
							if("OK"==data){
								alerttips('<fmt:message key="operation_successful"/>');
								//刷新页面
								$("#wait2").css("display","block");
								$("#wait").css("display","block");
								$("#vbillno").val(stripscript($("#vbillno").val()));
								$('#queryForm').submit();
							}else{
								alerttips(data);
							}
						}
					 });
					 //屏蔽后面的上传
	        		 return;
		        		
						$.ajaxSetup({async:false});
						var datas = {};
						datas["pk_puprorder"] = codeValue.join(",");
						$.post('<%=path%>/assitWebservice/uploadPuprOrdr.do',datas,function(data){
							if(data=="1"){
								showMessage({
									type: 'success',
									msg: '<fmt:message key="operation_successful"/>',
									speed: 1500,
									handler:function(){
										reloadPage();
									}
								});
							}else{
								 alerterror(data); 
							}
						});
		             });
		         }else{
		             alerterror('<fmt:message key="please_select"/><fmt:message key="data"/>！');
		             return ;
		         }
		    }
		    document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
	        
	        function printpuprorder(){
	        	var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					$("#printForm").attr('target','report');
					window.open("about:blank","report",'status=no,toolbar=no,menubar=no,location=no,scrollbars=no,resizable=yes,width='+window.screen.width+',height='+window.screen.height+',top=0,left=0');
					$("#printForm").submit();
				}else{
					alerterror('请选择一条订单！');
				}
	        }
	        //根据订单得到状态
	        function getPuprOrderIstate(pk_puprorder){
				var res = 1;
				$.ajax({
					type: "POST",
					url: "<%=path%>/puprorder/queryPuprorderIstate.do",
					data: {'pk_puprorder':pk_puprorder},
					dataType: "json",
					async:false,
					success: function(data){
						res = data;
						}
					});
				return res;
	        }
		</script>
	</body>
</html>