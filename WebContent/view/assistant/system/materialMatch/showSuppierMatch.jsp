<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="gyshead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<td class="num"><span style="width: 20px;"></span></td>
				<td><span style="width:70px;"><fmt:message key="suppliers_coding" /></span></td>
				<td><span style="width:180px;"><fmt:message key="suppliers_name" /></span></td>
				<td><span style="width:110px;"><fmt:message key="phone" /></span></td>
				<td><span style="width:260px;"><fmt:message key="address" /></span></td>
			</tr>
		</thead>
	</table>
</div>
<div id="gystable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="seller" items="${sellerList}"
				varStatus="status">
				<tr>
					<td class="num"><span style="width: 20px;">${status.index+1}</span>
					</td>
					<td><span style="width:70px;">${seller.delivercode}</span></td>
					<td>
						<span style="width:180px;" title="${seller.delivername}">
							${seller.delivername}
						</span>
					</td>
					<td><span style="width:110px;">${seller.vsuppliertel}</span></td>
					<td><span style="width:260px;">${seller.vsupplieraddr}</span></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<span>
	<page:page form="queryFrom" page="${pageobj}"></page:page>
</span>
<script>
	$(".page").css("bottom","3px");
</script>
