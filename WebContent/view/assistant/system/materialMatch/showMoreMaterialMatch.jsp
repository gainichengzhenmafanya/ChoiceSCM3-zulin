<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/materialMatch.css" />
		<style type="text/css">
			.separator{
				display:none !important;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			.toolbar{
				background:white;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:none;"></div>
		<div id="wait" style="display:none;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div class="form" style="float: left;height: 100%;">
			<input type="hidden" id="vcode" name="vcode" value="${materialMatch.vcode }" />
			<form id="queryFrom" >
				<input type="hidden" id="showId" name="showId" />
				<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="z-index:88;" >
					<div title='<fmt:message key="commodity" /><fmt:message key="the_detail" />' style="padding:10px;overflow: hidden;">
						<input type="hidden" id="flag" value="sp" />
						<input type="hidden" id="sp_record"/>
						<input type="hidden" id="sp_pkid"/>
						<div id="spdiv" class="grid" style="border: 1px solid #999;overflow:auto;">
							
					  	</div>
					</div>
					<div title='<fmt:message key="suppliers" /><fmt:message key="the_detail" />' style="padding:10px;overflow: hidden;">
						<input type="hidden" id="flag" value="gys" />
						<input type="hidden" id="gys_record"/>
						<div id="gysdiv" class="grid" style="border: 1px solid #999;overflow-y: hidden;">
							
					    </div>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
// 				$('.tabs-panels').css("height",$(document.body).height()*0.75);
				$('.grid').css("height",$(document.body).height()*0.7);
				$('.grid').css("width",$('.tabs-panels').width()*0.98);
				
				$('#tabs').tabs({onSelect:function(){
					var tab = $(this).tabs('getSelected');//获取选中的面板
					var content = tab.panel("body");
					var flag = content.find('#flag').val();
					$('#showId').val(flag);
					if(flag=='gys'){
						$("#tabName").val('<fmt:message key="suppliers" />');
					}
					var grid = content.find('#'+flag+'div');
					var modifyId = $.trim($('#modifyId').val()) ? $.trim($('#modifyId').val()) : null;
					//加载当前页签下的页面
					if(grid.get(0) && !$.trim(grid.html())){
						loadGrid();
					}else{
						pageFunReload(content);
					}
					function loadGrid(nowPage,pageSize){
						$("#wait2").css("display","block");
						$("#wait").css("display","block");
						grid.load("<%=path%>/materialMatch/loadSubPage.do?flag="+flag+"&vcode="+$('#vcode').val(),{nowPage:nowPage ? nowPage : 1,pageSize:pageSize ? pageSize : 20},function(){
							var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
							grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
							if(grid.find('.table-body').width() > grid.width()){
								grid.scroll(function(){
									content.find('.page').css('margin-left',grid.scrollLeft());
								});
							}
							grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
							grid.find('.table-body').find('tr').hover(
								function(){
									$(this).addClass('tr-over');
								},
								function(){
									$(this).removeClass('tr-over');
								}
							);
							pageFunReload(content);
							$("#wait2").css("display","none");
							$("#wait").css("display","none");
						});
					}
				 }});
				//自动实现滚动条
				setElementHeight('.easyui-tabs',['.form-line'],$(document.body),40);	//计算.easyui-tabs的高度
				setElementHeight('.tabs-panels',['.tabs-header'],$(".easyui-tabs"),1);	//计算tabs-panels的高度
				setElementHeight('.grid',['.form-line'],$(document.body),85);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
// 				$(".grid").css("overflow-y","hidden");

				$('#tabs').css("width",$(document.body).width()*0.99);
// 				$('.tabs-panels').css("height",$(document.body).height()*0.75);
				$('.grid').css("width",$('#tabs').width()*0.50);
				loadGrid();//  自动计算滚动条的js方法
            	$("#tabs").find('li:eq(1)').trigger('click');//显示第一个页签
            	$("#tabs").find('li:eq(0)').trigger('click');//显示第一个页签
			});
			 function pageReload(){
				$('#listForm').submit();
			} 
			 $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").removeAttr("checked");
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", "checked");
			     }
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
			 });
			
			//查询方法中页签加载方法
			function loadGrid(nowPage,pageSize){
				var flag = $('#showId').val();
				var content = $('#tabs').tabs('getSelected').panel("body");
				var grid =  content.find('#'+flag+'div');
				debugger;
				grid.load("<%=path%>/materialMatch/loadSubPage.do?flag="+flag+"&vcode="+$('#vcode').val(),{nowPage:nowPage ? nowPage : 1,pageSize:pageSize ? pageSize : 20},function(){
					var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
					grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
					if(grid.find('.table-body').width() > grid.width()){
						grid.scroll(function(){
							content.find('.page').css('margin-left',grid.scrollLeft());
						});
					}
					grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
					grid.find('.table-body').find('tr').hover(
						function(){
							$(this).addClass('tr-over');
						},
						function(){
							$(this).removeClass('tr-over');
						}
					);
					$("#wait2").css("display","none");
					$("#wait").css("display","none");
					pageFunReload(content);
				});
			}
			//重写翻页方法
			function pageFunReload(content){
				var pageSize = content.find('#pageSize').val();
				pageAction = function(nowPage){
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					nowPage ? loadGrid(nowPage,pageSize) : loadGrid(content.find('#gotoPage').val(),pageSize);
				};
				perPageColSizeAction = function(count){
					var val = content.find("#viewPageColSize").val();
					if(isNaN(val) || val == 0){
						alerterror("<fmt:message key="please_fill_in_figures" />");
						$("#viewPageColSize").val(pageSize);
						return ;
					}else if(val>150){
						alerterror("<fmt:message key="max_num_150" />");
						$("#viewPageColSize").val(pageSize);
						return ;
					}else{
						loadGrid($('#nowPage').val(),val);
					}
				};
			}
		</script>
	</body>
</html>