<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="sphead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<td class="num"><span style="width: 20px;"></span></td>
				<td><span style="width:70px;"><fmt:message key="supplies_code" /></span></td>
				<td><span style="width:200px;"><fmt:message key="supplies_name" /></span></td>
				<td><span style="width:50px;"><fmt:message key="startbuy" /></span></td>
				<td><span style="width:50px;"><fmt:message key="unit" /></span></td>
				<td><span style="width:60px;"><fmt:message key="unit_price" /></span></td>
				<td><span style="width:80px;"><fmt:message key="suppliers_coding" /></span></td>
				<td><span style="width:170px;"><fmt:message key="suppliers_name" /></span></td>
			</tr>
		</thead>
	</table>
</div>
<div id="sptable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="commodity" items="${commodityList}"	varStatus="status">
				<tr>
					<td class="num"><span style="width: 20px;">${status.index+1}</span>
					</td>
					<td><span style="width:70px;">${commodity.vmallcode}</span></td>
					<td>
						<span style="width:200px;float: left;" title="${commodity.vmallname}">
							${commodity.vmallname}
						</span>
					</td>
					<td>
						<span style="width:50px;text-align: left;"> ${commodity.nleast }</span>
					</td>
					<td>
						<span style="width:50px;text-align: right;"> ${commodity.vunit }</span>
					</td>
					<td>
						<span style="width:60px;text-align: right;"> ${commodity.nprice }</span>
					</td>
					<td>
						<span style="width:80px;text-align: right;"> ${commodity.delivercode }</span>
					</td>
					<td>
						<span style="width:170px;text-align: left;"> ${commodity.delivername}</span>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<page:page form="queryFrom" page="${pageobj}"></page:page>
<script>
	$(".page").css("bottom","3px");
</script>