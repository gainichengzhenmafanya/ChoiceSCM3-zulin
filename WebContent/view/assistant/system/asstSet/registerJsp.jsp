<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>云采购平台注册</title>
	<link rel="stylesheet" href="<%=path%>/css/assistant/style.css" />
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />		
	<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<style  type="text/css">
		.messagewin{
			margin-left:0px;
			margin-top: -220px;
		}
	</style>
</head>
<body>

 <div id="wait2" style="display:none;">
	<div id="wait" style="display:none;">
		<img src="<%=path%>/image/loading_detail.gif" />
		&nbsp;
		<span style="color:white;font-size:15px;">云采购平台注册中...</span>
	</div>  
 </div>
 <div id="contentDiv"  class="form-div">
	  <form id="reg-form"  method="post">
	    <table>
	    	<tr>
	    		<td colspan="2"><div style="text-align: center;font-size: 20px;margin-bottom: 5px;">云采购平台注册</div></td>
	    	</tr>
	      <tr style="display: none;">
	        <td>主卡号</td>
	        <td>
	        	<input type="hidden" id="vmcnumberasd" disabled="disabled" value="${company.vmcnumber}"/>
	        	<input name="vmcnumber" type="hidden" id="vmcnumber" readonly="readonly" value="${company.vmcnumber}"/>
		    	<input type="hidden" id="vaddress" name="vaddress" value="${company.vaddress}" />
				<input type="hidden" id="vphone" name="vphone" value="${company.vphone}" />
				<input type="hidden" id="vname" name="vname" value="${company.vname}" />
				<input type="hidden" id="vemail" name="vemail" value="${company.vemail}" />
				<input type="hidden" id="vcity" name="vcity" value="${company.vcity}" />
				<input type="hidden" id="vcompanylevel" name="vcompanylevel" value="${company.vcompanylevel}" />
			</td>
	      </tr>
	      <tr style="display: none;">
	        <td>企业名称</td>
	        <td>
	        	<input  type="hidden" id="names"  disabled="disabled" value="${company.vcompname}"/>
	        	<input name="vcompname" type="hidden" id="vcompname"  readonly="readonly" value="${company.vcompname}"/>
	        </td>
	      </tr>
	      <tr>
	        <td><span class="red">*</span>商城<fmt:message key="loginName"/></td>
	        <td>
	        	<input name="vitfuserid" type="text" id="vitfuserid"/>
	        </td>
	      </tr>
	      <tr>
	        <td><span class="red">*</span>商城<fmt:message key="password"/></td>
	        <td><input name="vitfpwd" type="password" id="vitfpwd"/></td>
	      </tr>
	      <tr>
	        <td><span class="red">*</span><fmt:message key="email"/></td>
	        <td><input name="email" type="text" id="email" /></td>
	      </tr>
	    </table>
	    <div class="buttons">
	      <input value="注 册" id="tj" type="button" style="cursor: pointer;" onclick="saveAccountInfo()"/>
	    </div>
	    <br class="clear"/>
	  </form>
	</div>	
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/easyform.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#reg-form').easyform();
			 $(document.body).bind('keydown',function(){
				if(event.keyCode==27 || event.keyCode==13){
	 				closemsgwin();
	 			}
	 		});
			 var inputarray = $("input");
			 $("input").bind('keyup',function(){
				 if(event.keyCode==13){
					 var index = inputarray.index($(this)[0]);
					 if(index!=inputarray.length-1){
					 	$(inputarray[index+1]).focus();
					 	if($(inputarray[index]).attr("id")=="email"){
					 		$("#tj").click();
					 	}
					 }
				 }
			 });
		});	
		function saveAccountInfo(){
// 			$("#wait2").css("display","block");
// 			$("#wait").css("display","block");
			if($("#vitfuserid").val()==""){
				alerterror('<fmt:message key="loginName"/><fmt:message key="cannot_be_empty"/>');
				return;
			}
			if($("#vitfpwd").val()==""){
				alerterror('商城<fmt:message key="password"/><fmt:message key="cannot_be_empty"/>');
				return;
			}
			if($("#email").val()==""){
				alerterror('<fmt:message key="email"/><fmt:message key="cannot_be_empty"/>');
				return;
			}
			if(!$("#email").val().match("^([\\w-.]+)@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([\\w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$")){
				alerterror('<fmt:message key="e-mail_can_not_be_empty_mailbox_format_javapro"/>');
				return;
			}
			$("#wait2").css("display","block");
			$("#wait").css("display","block");
			$.ajax({
				type : 'POST',
				url : '<%=path %>/registerMall/saveRegisterInfo.do',
				data : getParam($("#reg-form")),
				success : function(message) {
					$("#wait2").css("display","none");
					$("#wait").css("display","none");
					if(message!=""){
						debugger;
						if(message.indexOf("0-")>=0){
							alerterror(message.substring(2));
						}else if(message.indexOf("1-")>=0){
							alerttips(message.substring(2));
						}
					}else{
						alerterror("注册失败，请联系北京辰森世纪科技有限公司平台处理。");
					}
				}
			});
		}

		function showmask(){
			topWin.$("#wait2").show();
		}
		function closemask(){
			topWin.$("#wait2").hide();
		}
		
		var topWin= (function (p,c){
		    while(p!=c){
		        c = p;        
		        p = p.parent;
		    }
		    return c;
		})(window.parent,window);
		function alerterror(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>错误信息</span></div><div class="messageinfowin"><span style="width: 370px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgwin()">确&nbsp;定</div></div></div>';
			if(top.$("#messagewin").length!=0){
				top.$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo(top.$("#contentDiv"));
			messagewin.css('margin-left','10px');
		}
		function alerttips(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>提示信息</span></div><div class="messageinfowin"><span style="width: 370px;margin-top: -15px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgtipswin()">确&nbsp;定</div></div></div>';
			if(top.$("#messagewin").length!=0){
				top.$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo(top.$("#contentDiv"));
			messagewin.css('height','255px');
			messagewin.css('margin-left','10px');
			$(".messageinfowin").css("height","165px");
			$(".messageinfowin").css("font-size","19px");
			$(".messageinfowin").css("text-align","left");
			$(".messageinfowin span").css("margin-left","20px");
		}
		function  closemsgtipswin(){
			top.$("#messagewin").hide(1,function(){
				window.location.href = window.location.href.substring(0, window.location.href.indexOf("/registerMall"));
			});
			closemask();
		}
	</script>
</body>
</html>
