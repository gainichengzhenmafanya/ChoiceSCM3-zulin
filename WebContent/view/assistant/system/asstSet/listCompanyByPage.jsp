<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>企业信息列表</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <style type="text/css">
        .grid td span{
            padding:0px;
        }
        #contentDiv{
        	positn: absolute;
        	height:100%;
        	width:100%;
        }
    </style>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div style="height:55%;">
	<div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/getCompanyInitParams/listCompanyList.do" method="post">
    	<input type="hidden" id="moduleId" name="moduleId" value="${moduleId }" />
    	<input type="hidden" id="vitfuserid" name="vitfuserid" value="${company.vitfuserid }" />
    	<input type="hidden" id="vitfpwd" name="vitfpwd" value="${company.vitfpwd }" />
    <div class="search-div" style="positn: absolute;z-index: 19;">
        <div class="form-line">
            <div class="form-label" style="width: 100px;"><fmt:message key="coding"/>：</div>
            <div class="form-input">
                <input type="text" name="vcode" id="vcode"  value="${company.vcode}" class="text"/>
            </div>
            <div class="form-label" style="width: 100px;"><fmt:message key="name"/>：</div>
            <div class="form-input">
                <input type="text" name="vcompname" id="vcompname"  value="${company.vcompname}" class="text"/>
            </div>
		</div>
        <div class="search-commit">
            <input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
            <input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
        </div>
    </div>
    <div id="contentDiv">
    
    </div>
    <div class="grid" id="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:30px;">
                            <input type="checkbox" id="chkAll"/>
                        </td>
                        <td><span style="width: 120px;"><fmt:message key="Enterprise"/><fmt:message key="coding"/></span></td>
                        <td><span style="width: 300px;"><fmt:message key="Enterprise"/><fmt:message key="name"/></span></td>
                        <td><span style="width: 260px;"><fmt:message key="aeskey"/></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${list}" var="company" >
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${company.acct}' />" value="<c:out value='${company.acct}' />"/>
                            </td>
                            <td><span style="width: 120px;text-align: left;" title="${company.vcode}">${company.vcode}</span></td>
                            <td><span style="width: 300px;text-align: left;" title="${company.vcompname}">${company.vcompname}</span></td>
                            <td><span style="width: 260px;text-align: left;" title="${company.vauthkey}">${company.vauthkey}</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${pageobj}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
    </div>
    </form>
    </div>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/json2.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript">
    	var errmsg = "${errmsg}";
        $(document).ready(function(){
            $("#search").bind('click', function() {
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
                $("#vcode").val(stripscript($("#vcode").val()));
                $("#vcompname").val(stripscript($("#vcompname").val()));
                $('.search-div').hide();
                $('#listForm').submit();
            });
            /* 模糊查询清空 */
            $("#resetSearch").bind('click', function() {
                $("#vcode").val("");
                $("#vcompname").val("");
            });
            $('.tool').toolbar({
                items: [{
                    text: '<fmt:message key="select"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px', '-40px']
                    },
                    handler: function () {
                        $('.search-div').slideToggle(100);
                        $("#vcode").focus();
                    }
                }]
            });
            setElementHeight('.grid',['.toolbar'],$(document.body),15);	//计算.grid的高度
            setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('.grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('.grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
                $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
            });
            $('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
                event.stopPropagation();
            });
			$("#wait2").css("display","none");
			$("#wait").css("display","none");
			
        	if(errmsg!=""){
       			alerterror(errmsg);
       			window.location.href = "<%=path%>/getCompanyInitParams/toListCompany.do";
        	}
        });
        var update=function(){
            var checkboxList = $('.grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var action='<%=path%>/company/skip.do?acct='+checkboxList.filter(':checked').val()+'&moduleId=${moduleId}';
                $('body').window({
                    title: '<fmt:message key="suppliers"/><fmt:message key="update"/>',
                    content: '<iframe id="skipSupplierFrame" frameborder="0" src="'+action+'"></iframe>',
                    width: '700px',
                    height: '500px',
                    draggable: true,
                    isModal: true,
                    topBar: {
                        items: [
                            {
                                text: '<fmt:message key="save"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-80px', '-0px']
                                },
                                handler: function () {
                                    getFrame('skipSupplierFrame').updateSupplier();
                                }
                            },
                            {
                                text: '<fmt:message key="cancel"/>',
                                icon: {
                                    url: '<%=path%>/image/Button/op_owner.gif',
                                    positn: ['-160px', '-100px']
                                },
                                handler: function () {
                                    $('.close').click();
                                }
                            }
                        ]
                    }
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        
		function showmask(){
			$("#wait2").show();
		}
		function closemask(){
			$("#wait2").hide();
		}
		
		var topWin= (function (p,c){
		    while(p!=c){
		        c = p;        
		        p = p.parent;
		    }
		    return c;
		})(window.parent,window);
		
		function alerterror(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>错误信息</span></div><div class="messageinfowin"><span style="width: 390px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgwin()">确&nbsp;定</div></div></div>';
			if($("#messagewin").length!=0){
				$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo($("#contentDiv"));
			var w = (document.body.clientWidth-400)/2;
			var h = (document.body.clientHeight-260)/2;
			messagewin.css('margin-left',w);
			messagewin.css('margin-top',h);
		}
		function alerttips(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>提示信息</span></div><div class="messageinfowin"><span style="width: 370px;margin-top: -15px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgtipswin()">确&nbsp;定</div></div></div>';
			if($("#messagewin").length!=0){
				$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo($("#contentDiv"));
			messagewin.css('height','255px');
			messagewin.css('margin-left','10px');
			$(".messageinfowin").css("height","165px");
			$(".messageinfowin").css("font-size","19px");
			$(".messageinfowin").css("text-align","left");
			$(".messageinfowin span").css("margin-left","20px");
		}
		function  closemsgtipswin(){
			$("#messagewin").hide(1,function(){
			});
			closemask();
		}
    </script>
</body>
</html>
