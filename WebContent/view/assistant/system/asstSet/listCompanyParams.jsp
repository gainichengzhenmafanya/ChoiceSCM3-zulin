<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>云采购平台获取企业信息初始页面</title>
	<link rel="stylesheet" href="<%=path%>/css/assistant/style.css" />
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />		
	<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
	<style  type="text/css">
		.messagewin{
			margin-left:0px;
			margin-top: -220px;
		}
	</style>
</head>
<body>

 <div id="wait2" style="display:none;">
	<div id="wait" style="display:none;">
		<img src="<%=path%>/image/loading_detail.gif" />
		&nbsp;
		<span style="color:white;font-size:15px;">云采购平台登录中。。。</span>
	</div>  
 </div>
 <div id="contentDiv"  class="form-div">
	  <form id="reg-form"  method="post">
	    <table>
	    	<tr>
	    		<td colspan="2"><div style="text-align: center;font-size: 20px;margin-bottom: 5px;">请登录</div></td>
	    	</tr>
	      <tr>
	        <td><span class="red">*</span><fmt:message key="loginName"/></td>
	        <td>
	        	<input name="vitfuserid" type="text" id="vitfuserid"/>
	        </td>
	      </tr>
	      <tr>
	        <td><span class="red">*</span><fmt:message key="password"/></td>
	        <td><input name="vitfpwd" type="password" id="vitfpwd"/></td>
	      </tr>
	    </table>
	    <div class="buttons">
	      <input value="登  录" id="tj" type="button" style="cursor: pointer;" onclick="saveAccountInfo()"/>
	    </div>
	    <br class="clear"/>
	  </form>
	</div>	
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/easyform.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#reg-form').easyform();
			 $(document.body).bind('keydown',function(){
				if(event.keyCode==27 || event.keyCode==13){
	 				closemsgwin();
	 			}
	 		});
			 var inputarray = $("input");
			 $("input").bind('keyup',function(){
				 if(event.keyCode==13){
					 var index = inputarray.index($(this)[0]);
					 if(index!=inputarray.length-1){
					 	$(inputarray[index+1]).focus();
					 	if($(inputarray[index]).attr("id")=="vitfpwd"){
					 		$("#tj").click();
					 	}
					 }
				 }
			 });
		});	
		function saveAccountInfo(){
			if($("#vitfuserid").val()==""){
				alerterror('<fmt:message key="loginName"/><fmt:message key="cannot_be_empty"/>');
				return;
			}
			if($("#vitfpwd").val()==""){
				alerterror('<fmt:message key="password"/><fmt:message key="cannot_be_empty"/>');
				return;
			}
			$("#wait2").css("display","block");
			$("#wait").css("display","block");
			$.ajax({
				type : 'POST',
				url : '<%=path %>/getCompanyInitParams/loginToPage.do',
				data : getParam($("#reg-form")),
				success : function(message) {
					$("#wait2").css("display","none");
					$("#wait").css("display","none");
					debugger;
					if(message!=""){
						if(message.indexOf("0-")>=0){
							alerterror(message.substring(2));
						}else if(message.indexOf("1-")>=0){
							$("#reg-form").attr("action","<%=path%>"+message.substring(2));
							$("#reg-form").submit();
						}
					}else{
						alerterror("登录失败，请联系北京辰森世纪科技有限公司处理。");
					}
				}
			});
		}

		function showmask(){
			topWin.$("#wait2").show();
		}
		function closemask(){
			topWin.$("#wait2").hide();
		}
		
		var topWin= (function (p,c){
		    while(p!=c){
		        c = p;        
		        p = p.parent;
		    }
		    return c;
		})(window.parent,window);
		function alerterror(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>错误信息</span></div><div class="messageinfowin"><span style="width: 390px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgwin()">确&nbsp;定</div></div></div>';
			if(top.$("#messagewin").length!=0){
				top.$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo(top.$("#contentDiv"));
			messagewin.css('margin-left','10px');
		}
		function alerttips(msg){
			showmask();
			var windiv = '<div id="messagewin" class="messagewin"><div class="messagetitlewin"><span>提示信息</span></div><div class="messageinfowin"><span style="width: 370px;margin-top: -15px;">'+msg+'</span></div><div class="messagebtnwin"><div class="okbtn" onclick="closemsgtipswin()">确&nbsp;定</div></div></div>';
			if(top.$("#messagewin").length!=0){
				top.$("#messagewin").remove();
			}
			messagewin = $(windiv);
			messagewin.appendTo(top.$("#contentDiv"));
			messagewin.css('height','255px');
			messagewin.css('margin-left','10px');
			$(".messageinfowin").css("height","165px");
			$(".messageinfowin").css("font-size","19px");
			$(".messageinfowin").css("text-align","left");
			$(".messageinfowin span").css("margin-left","20px");
		}
		function  closemsgtipswin(){
			top.$("#messagewin").hide(1,function(){
			});
			closemask();
		}
	</script>
</body>
</html>
