<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="basic_information" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			div{
				display:block;
			}
		</style>
	</head>
	<body>
			<form id="companyForm" method="post" action="<%=path %>/company/saveCompany.do">
			<div  style="left:50%;top:50%;margin:90px auto;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="number"/></div>
					<div class="form-input">
						<input type="text" id="vcode" name="vcode" class="text" disabled="disabled" value="${company.vcode }"/>
					</div>
					<div class="form-label">
						<span style="color:red;">*</span><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="vcompname" name="vcompname" class="text" value="${company.vcompname }" />
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="main_card"/></div>
					<div class="form-input">
						<input type="text" id="vmcnumber" name="vmcnumber" class="text" value="${company.vmcnumber }"/>
					</div>
					<div class="form-label"><fmt:message key="storeQuantity"/></div>
					<div class="form-input"><input type="text" maxlength="5" id="nstorequantity" name="nstorequantity" class="text" value="<c:out value='${company.nstorequantity}' />"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="contact"/></div>
					<div class="form-input">
						<input type="text" id="vname" name="vname" class="text" value="${company.vname }"/>
					</div>
					<div class="form-label"><fmt:message key="phone"/></div>
					<div class="form-input">
						<input type="text" id="vphone" name="vphone" class="text" value="${company.vphone }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="zip_code"/></div>
					<div class="form-input">
						<input type="text" id="vzipcode" name="vzipcode" class="text" value="${company.vzipcode }"/>
					</div>
					<div class="form-label"><fmt:message key="email"/></div>
					<div class="form-input">
						<input type="text" id="vemail" name="vemail" class="text" value="${company.vemail }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="www"/></div>
					<div class="form-input">
						<input type="text" id="vweburl" name="vweburl" class="text" value="${company.vweburl }"/>
					</div>
					<div class="form-label"><fmt:message key="registered_capital"/></div>
					<div class="form-input">
						<input type="text" id="nregmoney" name="nregmoney" class="text" value="${company.nregmoney }"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label">可以提交他人订单到商城</div>
					<div class="form-input">
						<input type="checkbox" id="vsubmitothorder" name="vsubmitothorder"  value="Y" <c:if test="${company.vsubmitothorder=='Y' }">checked="checked"</c:if>/>&nbsp;
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="Enterprise"/><fmt:message key="address"/></div>
					<div class="form-input" style="width: 405px;">
						<input type="text" id="vaddress" name="vaddress" class="text" value="${company.vaddress }"  style="width: 403px;"/>
						<input type="hidden" id="vcompanylevel" name="vcompanylevel" class="text" value="${company.vcompanylevel }"  style="width: 403px;"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="text-align: right;margin-top: 10px;margin-left: 50px;">
						<a id="save1" name="save1" class="btn btn-signup"><fmt:message key="save"/></a> &nbsp;&nbsp;&nbsp;
					</div>
				</div>
			</div>
			</form>
		<script type="text/javascript">
			$("#vcompname").focus();
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vcompname',
						validateType:['canNull','maxLength','withOutSpecialChar'],
						param:['F','300','F'],
						error:['<fmt:message key="name"/><fmt:message key="cannot_be_empty"/>！','<fmt:message key="the_maximum_length_not"/>300','<fmt:message key="no_contain_special_char" />\'?']
					},{
						type:'text',
						validateObj:'nstorequantity',
						validateType:['canNull','maxLength','intege2'],
						param:['F','11','F'],
						error:['<fmt:message key="cannot_be_empty"/>！','<fmt:message key="the_maximum_length_not"/>11','<fmt:message key="enter_the_number_of_non_zero" />']
					},{
						type:'text',
						validateObj:'vzipcode',
						validateType:['zipCode'],
						param:['F'],
						error:['<fmt:message key="zip_zip_format_can_not_be_empty_javapro"/>']
					},{
						type:'text',
						validateObj:'vemail',
						validateType:['maxLength','email'],
						param:['60','F'],
						error:['<fmt:message key="the_maximum_length_not"/>60','<fmt:message key="e-mail_can_not_be_empty_mailbox_format_javapro" />']
					},{
						type:'text',
						validateObj:'vweburl',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="the_maximum_length_not"/>100']
					},{
						type:'text',
						validateObj:'nregmoney',
						validateType:['canNull','maxLength','intege2'],
						param:['F','11','F'],
						error:['<fmt:message key="cannot_be_empty"/>！','<fmt:message key="the_maximum_length_not"/>11','<fmt:message key="enter_the_number_of_non_zero" />']
					},{
						type:'text',
						validateObj:'vaddress',
						validateType:['maxLength'],
						param:['300'],
						error:['<fmt:message key="the_maximum_length_not"/>300']
					},{
						type:'text',
						validateObj:'vphone',
						validateType:['phone'],
						param:['F'],
						error:['<fmt:message key="telephone_format_is_not_correct"/>']
					}]
				});
			});
			$("#save1").click(function(){
				if(validate._submitValidate()){
					$.ajax({
						type: 'POST',
						async:false,
						url: '<%=path %>/company/saveCompany.do',
						data: getParam($("#companyForm")),
						dataType: 'html',
						success: function(info){
							if(info=='T'){
								showMessage({
									type: 'success',
									msg: '<fmt:message key="update" /><fmt:message key="successful" />',
									speed: 1500
								});	
							}else{
								showMessage({
									type: 'error',
									msg: '<fmt:message key="update" /><fmt:message key="failure" />',
									speed: 1500
								});	
							}
						}
					});
				}
			});
		</script>
</body>
</html>
