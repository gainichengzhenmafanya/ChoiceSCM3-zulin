<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="accounting-period" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			div{
				display:block;
			}
		</style>
	</head>
	<body>
			<form id="departmentForm">
			<div  style="left:50%;top:50%;margin:50px auto;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="years"/></div>
					<div class="form-input">
						<input type="text" id="vaccountyear" name="vaccountyear" class="text" value="${accountPeri.vaccountyear }" disabled="disabled"/>
						<input type="hidden" id="denddate0" name="denddate0" class="text" value="${accountPeri.vaccountyear-1 }-12-31" disabled="disabled"/>
						<input type="hidden" id="dstartdate13" name="dstartdate13" class="text" value="${accountPeri.vaccountyear+1 }-01-01" disabled="disabled"/>
					</div>
					
				</div>
				<c:forEach varStatus="status" items="${accountPeriList }" var="accountPeri">
					<div class="form-line">
						<div class="form-label"><fmt:message key="accounting-period"/>${accountPeri.vaccountmonth }</div>
						<div class="form-input">
							<input type="text" id="dstartdate${accountPeri.vaccountmonth }" name="dstartdate${accountPeri.vaccountmonth }" value="${accountPeri.dstartdate }" class="text Wdate"/>
							&nbsp;&nbsp;--&nbsp;&nbsp;<input type="text" id="denddate${accountPeri.vaccountmonth }" name="denddate${accountPeri.vaccountmonth }" value="${accountPeri.denddate }" class="text Wdate"/>
						</div>
					</div>
				</c:forEach>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="text-align: right;margin-top: 10px;margin-left: 50px;">
						<a id="saveQjq" name="saveQjq" class="btn btn-signup"> 保存 </a>
					</div>
				</div>
			</div>
			</form>
	 	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript">
		debugger;
			<c:forEach varStatus="status" begin="1" end="12">
				$("#dstartdate${status.index }").click(function(){
		  			new WdatePicker({isShowClear:false,maxDate:'#F{$dp.$D(\'denddate${status.index }\')}',minDate:'#F{$dp.$D(\'denddate${status.index-1 }\',{d:1})}'});
		  		});
		  		$("#denddate${status.index }").click(function(){
		  			new WdatePicker({isShowClear:false,minDate:'#F{$dp.$D(\'dstartdate${status.index }\')}',maxDate:'#F{$dp.$D(\'dstartdate${status.index+1 }\',{d:-1})}'});
		  		});
			</c:forEach>
			$("#saveQjq").click(function(){
				var data = {};
				data["vaccountyear"] = $("#vaccountyear").val();
				for(var i=0;i<=11;i++){
					data["listAccountPeri["+i+"].vaccountyear"]=$("#vaccountyear").val();
					data["listAccountPeri["+i+"].vaccountmonth"]=i+1;
					data["listAccountPeri["+i+"].dstartdate"]=$("#dstartdate"+(i+1)).val();
					data["listAccountPeri["+i+"].denddate"]=$("#denddate"+(i+1)).val();
				}
				$.ajax({
					type: 'POST',
					async:false,
					url: '<%=path %>/company/saveAccountPeri.do',
					data: data,
					dataType: 'html',
					success: function(info){
						if(info=='T'){
							showMessage({
								type: 'success',
								msg: '<fmt:message key="update" /><fmt:message key="successful" />',
								speed: 1500
							});	
						}else{
							showMessage({
								type: 'error',
								msg: '<fmt:message key="update" /><fmt:message key="failure" />',
								speed: 1500
							});	
						}
					}
				});
			});
		</script>
</body>
</html>
