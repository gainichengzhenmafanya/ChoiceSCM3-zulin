<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="superAdminConf" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			div{
				display:block;
			}
		</style>
	</head>
	<body>
			<form id="departmentForm" method="post">
			<div  style="left:50%;top:50%;margin:90px auto;">
				<div class="form-line">
					<div class="form-label"><fmt:message key="original_password"/></div>
					<div class="form-input">
						<input type="password" id="oldPwd" name="oldPwd" class="text"/>
					</div>
					<div id="errMess"  class="form-input" style="display: none;"><span id="msg" style="color: red;"></span></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="new_password"/></div>
					<div class="form-input">
						<input type="password" id="newPwd" name="newPwd" class="text"/>
					</div>
					<div id="errMess1"  class="form-input" style="display: none;"><span id="msg1" style="color: red;"></span></div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="confirm_password"/></div>
					<div class="form-input">
						<input type="password" id="cPwd" name="cPwd" class="text"/>
					</div>
					<div id="errMess2"  class="form-input" style="display: none;"><span id="msg2" style="color: red;"></span></div>
				</div>
				<div class="form-line">
					<div class="form-label"></div>
					<div class="form-input" style="text-align: right;margin-top: 10px;margin-left: 20px;">
						<a id="savePwd" name="savePwd" class="btn btn-signup"> 保存 </a>
					</div>
				</div>
			</div>
			</form>
		<script type="text/javascript">
			$("#oldPwd").focus();
			$("#savePwd").click(function(){
				$("#errMess").css("display","none");
				$("#errMess1").css("display","none");
				$("#errMess2").css("display","none");
				var oldPwd = $("#oldPwd").val();
				var newPwd = $("#newPwd").val();
				var cPwd = $("#cPwd").val();
				var reg = /[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/;
				if(oldPwd==null || oldPwd == ''){
					$("#errMess").css("display","block");
					$("#msg").text('*<fmt:message key="original_password"/><fmt:message key="cannot_be_empty"/>');
					$("#oldPwd").focus();
					return;
				}else{
					$("#errMess").css("display","none");
				}
				if(newPwd==null || newPwd == ''){
					$("#errMess1").css("display","block");
					$("#msg1").text('*<fmt:message key="new_password"/><fmt:message key="cannot_be_empty"/>');
					$("#newPwd").focus();
					return;
				}else{
					if(!reg.test(newPwd)){
						$("#newPwd").focus();
						$("#errMess1").css("display","block");
						$("#msg1").text('*<fmt:message key="pwdmsg"/>');
						return;
					}else{
						if(newPwd.length>16 || newPwd.length<6){
							$("#newPwd").focus();
							$("#errMess1").css("display","block");
							$("#msg1").text('*<fmt:message key="pwdlength"/>');
							return;
						}
					}
					$("#errMess1").css("display","none");
				}
				if(cPwd==null || cPwd == ''){
					$("#errMess2").css("display","block");
					$("#msg2").text('*<fmt:message key="confirm_password"/><fmt:message key="cannot_be_empty"/>');
					$("#cPwd").focus();
					return;
				}else{
					if(!reg.test(cPwd)){
						$("#cPwd").focus();
						$("#errMess2").css("display","block");
						$("#msg2").text('*<fmt:message key="pwdmsg"/>');
						return;
					}
					if(cPwd.length>16 || cPwd.length<6){
						$("#newPwd").focus();
						$("#errMess1").css("display","block");
						$("#msg1").text('*<fmt:message key="pwdlength"/>');
						return;
					}
					$("#errMess2").css("display","none");
				}
				if(newPwd != cPwd){
					$("#errMess2").css("display","block");
					$("#msg2").text('*<fmt:message key="enter_two_passwords_dont_match"/>');
					$("#cPwd").focus();
					return;
				}else{
					$("#errMess2").css("display","none");
				}
				if(oldPwd == newPwd){
					$("#newPwd").focus();
					$("#errMess1").css("display","block");
					$("#msg1").text('*<fmt:message key="pwdmsgconfirm"/>');
					return;
				}
				$.ajax({
					type: 'POST',
					async:false,
					url: '<%=path %>/company/updatePwd.do',
					data: '&oldPwd='+oldPwd+'&newPwd='+newPwd+'&cPwd='+cPwd,
					dataType: 'html',
					success: function(info){
						if(info=='1'){
							$("#msg").text('*<fmt:message key="original_password_input_errors_please_re_enter"></fmt:message>');
							$("#errMess").css("display","block");
							$("#oldPwd").focus();
						}else if(info=='2'){
							$("#errMess2").css("display","block");
							$("#msg2").text('*<fmt:message key="enter_two_passwords_dont_match"/>');
							$("#cPwd").focus();
						}else if(info=='3'){
							$("#oldPwd").val('');
							$("#newPwd").val('');
							$("#cPwd").val('');
							alerttips('<fmt:message key="update_successful"/>');
						}
					}
				});
			});
		</script>
</body>
</html>
