<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>修改接口参数</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>		
		<style type="text/css">
				.accountInfo {
					positn: relative;
					top: 1px;
				}
				.hr{
					border-bottom: 2px solid #677FB2;
				}
				.form-line .form-input{
	 				width: 20%; 
	 			}
	 			.form-line .form-label{
	 				width: 20%; 
	 			}
			</style>
	</head>
	<body>
		<div id="toolbar"></div>
		<div class="accountInfo">
		<div  style="height:500px; width:500px;left:50%;top:50%;margin:0px auto;margin-left:90px;margin-top:20px;">
			<form id="accountForm" name="accountForm" method="post" action="<%=path %>/company/updateIftUserInfo.do">
				<input type="hidden" id="acct" name="acct" value="${company.acct}" />
				<div class="form-line">
					<div class="form-label"><fmt:message key="name"/></div>
					<div class="form-input">
						<input type="text" id="vitfuserid" name="vitfuserid" class="text"  value="${company.vitfuserid}"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="new_password"/></div>
					<div class="form-input">
						<input type="password" id="vitfpwd" name="vitfpwd" class="text" value="${company.vitfpwd}" />
					</div>
				</div>
			</form>
		</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="save" />',
							title: '<fmt:message key="save"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-80px','-0px']
							},
							handler: function(){
								saveInfo();
							}
						},{
							text: '<fmt:message key="cancel" />',
							title: '<fmt:message key="cancel" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								$('.close',parent.document).click();
							}
						}
					]
				});//end toolbar
				
				setElementHeight('.accountInfo',['#toolbar','.form-group']);
				
			});//end $(document).ready()
			
			function saveInfo(){
				var acct = $('#acct').val(),
					vitfuserid = $('#vitfuserid').val(),
					vitfpwd = $('#vitfpwd').val();
				
				$.ajax({
					type: 'POST',
					url: '<%=path %>/company/updateIftUserInfo.do',
					contentType: 'application/json',
					data: '{"acct":"'+acct+'","vitfuserid":"'+vitfuserid+'","vitfpwd":"'+vitfpwd+'"}',
					dataType: 'html',
					success: function(info){
						if(info && info === 'T'){
							
							//弹出提示信息
							showMessage({
								handler: function(){
									var accountTab = parent.window.accountTab,
										updateAccountWin = parent.window.updateAccountWin;
									if(accountTab){
							    	var $tableFrame = accountTab.getItem('tab_tableAccount').div.find('#tableAccountFrame');
							    	$tableFrame.attr('src',$tableFrame.attr('src'));
							    	
							    	accountTab.close('tab_updateAccount');
							    	accountTab.show('tab_tableAccount');
									}else if(updateAccountWin){
										updateAccountWin.close();
									}
								}
							});					
						}else{
							//弹出提示信息
							showMessage({
								type: 'error',
								msg: info,
								speed: 1000
							});	
						}
					}
				});
			}
		</script>
	</body>
</html>