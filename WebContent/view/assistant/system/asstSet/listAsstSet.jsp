<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
		<style type="text/css">
			.separator{
				display:none !important;
			}
/* 			.page{ */
/* 				margin-bottom: 28px; */
/* 			} */
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
			.showinfo{
				display:none;
				left:1px;
				top:1px;
				width:"240px";
				height:"240px";
				positn:fixed; 
				z-index:999999; 
				border: 1px solid #5e4e3f;
			}
			.btn{background-color: #D2340F;color:white;text-decoration: none;font-size: 15px;padding:5px 6px;border-radius: 3px;cursor: pointer;}
			.btn-buynow{background-color: #D2340F;}
			.btn-buynow:hover{background-color: #EF411A;}
			.btn-signup{background-color: #006DCC;}
			.btn-signup:hover{background-color: rgb(0,74,204);}
			.btn-login{background-color: rgb(113,181,27);}
			.btn-login:hover{background-color: rgb(147,199,25);}
		</style>
	</head>
	<body>
		<div class="form" style="float: left;height: 100%;">
				<div class="easyui-tabs" fit="false" plain="true" id="tabs" style="z-index:88;" >
					<div title='<fmt:message key="basic_information" />' style="padding:10px;overflow: hidden;">
						<input type="hidden" id="flag" value="basic" />
						<input type="hidden" id="basic_record"/>
						<input type="hidden" id="basic_pkid"/>
						<div id="basicdiv" class="main" style="border: 1px solid #999;overflow:auto;">
							
						</div>
					</div>
				<div title='<fmt:message key="superAdminConf" />' style="padding:10px;overflow: hidden;" id="pwdDiv">
					<input type="hidden" id="flag" value="adm" />
					<input type="hidden" id="adm_record"/>
					<div id="admdiv" class="main" style="border: 1px solid #999;overflow-y: hidden;">
						
				    </div>
				</div>
<%-- 				<div title='<fmt:message key="accounting-period" />' style="padding:10px;overflow: hidden;"> --%>
<!-- 					<input type="hidden" id="flag" value="kjq" /> -->
<!-- 					<input type="hidden" id="kjq_record"/> -->
<!-- 					<div id="kjqdiv" class="main" style="border: 1px solid #999;overflow-y: hidden;"> -->
						
<!-- 				    </div> -->
<!-- 				</div> -->
			</div>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
  		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-${sessionScope.locale}.js"></script>		
		<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#tabs').tabs({onSelect:function(){
					var tab = $(this).tabs('getSelected');//获取选中的面板
					var content = tab.panel("body");
					var flag = content.find('#flag').val();
					$('#showId').val(flag);
					if(flag=='basic'){
						$("#tabName").val('<fmt:message key="materials_list" />');
					}
					var grid = content.find('#'+flag+'div');
					var modifyId = $.trim($('#modifyId').val()) ? $.trim($('#modifyId').val()) : null;
					//加载当前页签下的页面
					if(grid.get(0) && !$.trim(grid.html())){
						loadGrid();
					}
					function loadGrid(nowPage,pageSize){
						grid.load("<%=path%>/company/loadSubPage.do?flag="+flag,{},function(){
							var fixed = grid.find('.table-body').width() > grid.width() ? 46 : (grid.find(".page").size()>0 ? 26 : 1);
							grid.find('.table-body').height(grid.innerHeight() - grid.find('.table-head').outerHeight() - fixed);
							if(grid.find('.table-body').width() > grid.width()){
								grid.scroll(function(){
									content.find('.page').css('margin-left',grid.scrollLeft());
								});
							}
							grid.find('.table-body').find('tr:odd').toggleClass('tr-toggle');
							grid.find('.table-body').find('tr').hover(
								function(){
									$(this).addClass('tr-over');
								},
								function(){
									$(this).removeClass('tr-over');
								}
							);
						});
					}
				 }});
				if("${pwdShow}"=="Y"){
				}else{
					$("#pwdDiv").remove();
					var f = document.getElementsByTagName("ul");
					var childs=f[0].childNodes;
					 f[0].removeChild(childs[1]); 
					
				}
				//自动实现滚动条
				setElementHeight('.easyui-tabs',['.form-line'],$(document.body),20);	//计算.easyui-tabs的高度
				setElementHeight('.tabs-panels',['.tabs-header'],$(".easyui-tabs"),1);	//计算tabs-panels的高度
				setElementHeight('.grid',['.form-line'],$(document.body),85);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
// 				$(".grid").css("overflow-y","hidden");

				$('#tabs').css("width",$(document.body).width()*0.99);
// 				$('.tabs-panels').css("height",$(document.body).height()*0.75);
				$('.main').css("width",$('#tabs').width()*0.98);
				$('.main').css("height",$('#tabs').height()*0.9);
				loadGrid();//  自动计算滚动条的js方法
			});
			
			function pageReload(){
				window.location.href=window.location.href;
			}
			 $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
			     if ($(this).hasClass("bgBlue")) {
			         $(this).removeClass("bgBlue").find(":checkbox").removeAttr("checked");
			     }else{
			         $(this).addClass("bgBlue").find(":checkbox").attr("checked", "checked");
			     }
					var $tmp=$('[name=idList]:checkbox');
					//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
					$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
			 });
		</script>
	</body>
</html>