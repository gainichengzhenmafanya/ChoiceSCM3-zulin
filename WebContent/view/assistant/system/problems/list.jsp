<%--
  User: mc
  Date: 2015-02-03
  Time: 17:48:56
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>常见问题</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <style type="text/css">
        .grid td span{
            padding:0px;
        }
    </style>
</head>
<body>
	<div class="tool">
    </div>
    <form id="listForm" action="<%=path%>/problems/findProblems.do" method="post">
    <div class="bj_head">
        <div class="form-line">
            <div class="form-label" style="width: 100px;"><fmt:message key="content"/>：</div>
            <div class="form-input">
                <input type="text" name="vanswer" id="vanswer"  value="${problems.vanswer}" class="text"/>
            </div>
		</div>
    </div>
    <div class="grid" id="grid">
        <div class="table-head" >
            <table cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <td style="width:30px;">
                            <input type="checkbox" id="chkAll"/>
                        </td>
                        <td><span style="width: 300px;"><fmt:message key="problems"/></span></td>
                        <td><span style="width: 600px;"><fmt:message key="answer"/></span></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="table-body">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <c:forEach items="${problemsList}" var="problems" >
                        <tr>
                            <td style="width:30px; text-align: center;">
                                <input type="checkbox"  name="idList" id="chk_<c:out value='${problems.pk_problems}' />" value="<c:out value='${problems.pk_problems}' />"/>
                            </td>
                            <td><span style="width: 300px;text-align: left;" title="${problems.vproblems}">${problems.vproblems}</span></td>
                            <td><span style="width: 600px;text-align: left;" title="${problems.vanswer}">${problems.vanswer}</span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <page:page form="listForm" page="${pageobj}"></page:page>
        <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
        <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
    </div>
    </form>
    <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
    <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
    <script type="text/javascript" src="<%=path%>/js/util.js"></script>
    <script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
    <script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tool').toolbar({
                items: [{
                    text: '<fmt:message key="select"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px', '-40px']
                    },
                    handler: function () {
                    	$("#vanswer").val(stripscript($("#vanswer").val()));
                        $("#listForm").submit();
                    }
                },{
                    text: '<fmt:message key="insert"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        add();
                    }
                },{
                    text: '<fmt:message key="update"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        update();
                    }
                },{
                    text: '<fmt:message key="delete"/>',
                    useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
                    icon: {
                        url: '<%=path%>/image/Button/op_owner.gif',
                        positn: ['0px','-40px']
                    },
                    handler: function(){
                        del();
                    }
                }]
            });
            setElementHeight('.grid',['.toolbar','.mainFrame'],$(document.body),25);	//计算.grid的高度
            setElementHeight('.grid .table-body',['.grid .table-head'],'.grid'); //计算.table-body的高度
            loadGrid();//  自动计算滚动条的js方法
            $(".page").css("margin-bottom",$(".mainFrame").height()-25);
            //如果全选按钮选中的话，table背景变色
            $("#chkAll").click(function() {
                if (!!$("#chkAll").attr("checked")) {
                    $('#grid').find('.table-body').find('tr').addClass("bgBlue");
                }else{
                    $('#grid').find('.table-body').find('tr').removeClass("bgBlue");
                }
            });
            //当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
            $('.grid').find('.table-body').find('tr').live("click", function () {
                $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
                $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
                $('#mainFrame').attr('src',encodeURI(url));
            });
            $('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
                event.stopPropagation();
            });
            /*列表双击事件*/
            $('.grid').find('.table-body').find('tr').dblclick(function(event){
                update();
            });
        });
        //====================================================
        var add=function(){
            $('body').window({
                title: '<fmt:message key="problems"/><fmt:message key="insert"/>',
                content: '<iframe id="skipProblemsFrame" frameborder="0" src="<%=path%>/problems/getView.do?mark=add"></iframe>',
                width: '700px',
                height: '500px',
                draggable: true,
                isModal: true
            });
        };
        var update=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size()==1) {
                var action='<%=path%>/problems/getView.do?pk_problems='+checkboxList.filter(':checked').val()+'&mark=update';
                $('body').window({
                    title: '<fmt:message key="problems"/><fmt:message key="update"/>',
                    content: '<iframe id="skipProblemsFrame" frameborder="0" src="'+action+'"></iframe>',
                    width: '700px',
                    height: '500px',
                    draggable: true,
                    isModal: true
                });
            }else if(!checkboxList&&0>=checkboxList.filter(':checked').size()){
                alerttips('<fmt:message key="please_select_information_you_need_to_modify"/>！');
                return ;
            }else{
                alerttips('<fmt:message key="you_can_only_modify_a_data"/>！');
                return ;
            }
        };
        var del=function(){
            var checkboxList = $('#grid').find('.table-body').find(':checkbox');
            if(checkboxList&&checkboxList.filter(':checked').size() > 0){
                alertconfirm('<fmt:message key="delete_data_confirm"/>？',function(){
                    var codeValue=[];
                    checkboxList.filter(':checked').each(function(){
                        codeValue.push($(this).val());
                    });
                    var action = '<%=path%>/problems/delProblems.do?ids='+codeValue.join(",");
                    $('body').window({
                        title: '<fmt:message key="data"/><fmt:message key="delete"/>',
                        content: '<iframe frameborder="0" src="'+action+'"></iframe>',
                        width: 250,
                        height: 150,
                        draggable: true,
                        isModal: true
                    });
                });
            }else{
                alerttips('<fmt:message key="please_select_information_you_need_to_delete"/>！');
                return ;
            }
        };
    </script>
</body>
</html>
