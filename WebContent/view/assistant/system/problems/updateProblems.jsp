<%--
  常见问题 新增、修改
  User: mc
  Date: 14-10-31
  Time: 下午7:55
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>常见问题 新增、修改</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <style type="text/css">
        .tool {
            positn: relative;
            height: 27px;
        }
    </style>
</head>
<body>
<div class="tool">
</div>
<input type="hidden" id="mark" value="${mark}"/>
<div style="margin-top: 10px;">
    <form action="" id="mainForm" name="mainForm" method="post">
        <div class="form-line">
            <input type="hidden" value="${problems.pk_problems}" name="pk_problems" id="pk_problems"/>
            <div class="form-label"><fmt:message key="problems"/>：</div>
            <div class="form-input">
                <input type="text" class="text" style="width: 500px;" maxlength="500" value="${problems.vproblems}" name="vproblems" id="vproblems"/>
            </div>
        </div>
        <div class="form-line">
            <div class="form-label"><fmt:message key="answer"/>：</div>
            <div class="form-input">
                <textarea type="text" class="textarea" style="width: 500px;" id="vanswer" name="vanswer" cols="80" rows="20">${problems.vanswer}</textarea>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
<script type="text/javascript">
    //ajax同步设置
    $.ajaxSetup({
        async: false
    });
    var savelock = false;
    var colval;
    $(document).ready(function(){
        $("#vinvoicenum").focus();
        $("#ddat").htmlUtils("setDate","now");
        $('.tool').toolbar({
            items: [{
                text: '<fmt:message key="confirm"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px', '-40px']
                },
                handler: function () {
                    if(savelock){
                        return;
                    }else{
                        savelock = true;
                    }
                    if(colval._submitValidate()) {
                        if($("#mark").val()=="update") {
                            document.mainForm.action = "<%=path%>/problems/update.do";
                        }else {
                            document.mainForm.action = "<%=path%>/problems/addProblems.do";
                        }
                        document.mainForm.submit();
                    }else{
                        savelock = false;
                    }
                }
            },{
                text: '<fmt:message key="cancel"/>',
                icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
                handler: function(){
                    $(".close",parent.document).click();
                }
            }]
        });
        colval= new Validate({
            validateItem:[{
                type:'text',
                validateObj:'vproblems',
                validateType:['canNull','maxLength'],
                param:['F','500'],
                error:['<fmt:message key="cannot_be_empty"/>!']
            },{
                type:'text',
                validateObj:'vanswer',
                validateType:['canNull'],
                param:['F'],
                error:['<fmt:message key="cannot_be_empty"/>!']
            }]
        });
    });
    //========================================================
</script>
</body>
</html>
