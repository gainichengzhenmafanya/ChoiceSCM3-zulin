<%--
  常见问题
  User: mc
  Date: 15/2/3
  Time: 下午8:34
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <title>常见问题</title>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <style type="">
    	body{
    		overflow: auto;
    	}
    </style>
</head>
<body>
<form action="<%=path%>/problems/findProblems.do?mark=show" method="post" id="listForm" name="listForm">
    <div style="margin: 20px;padding: 10px;border:1px solid #d3d3d3;">
        <h2>常见问题</h2>
        <hr style="color: #d3d3d3;"/>
        <c:forEach items="${problemsList}" varStatus="status" var="problems">
            <span style="background-color: orange;color: white;float: left;margin-left: 10px;margin-right: 10px;padding-left: 2px;padding-right: 2px;padding-top: 2px;"><small>${status.index+1}</small></span>
            <div style="margin-left: 30px;width: 97%;word-wrap:break-word;word-break:break-all;white-space: normal;">
                Q:<span style="margin: 10px;padding-right:40px;font-weight: lighter;WORD-BREAK: break-all; WORD-WRAP: break-word;">${problems.vproblems}</span>
                <br/><br/>
                A:<span style="margin: 10px;padding-right:40px;word-wrap:break-word;word-break:break-all;"><small>${problems.vanswer}</small></span>
            </div>
            <hr style="color: #d3d3d3;" size="1"/>
        </c:forEach>
        <div>
            <page:page form="listForm" page="${pageobj}"></page:page>
            <input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
            <input type="hidden" name ="pageSize" id="pageSize" value="${pageobj.pageSize }" />
        </div>
    </div>
</form>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/util.js"></script>
<script type="text/javascript">
	$("body").css("height",$("body").height()-20);
</script>
</body>
</html>
