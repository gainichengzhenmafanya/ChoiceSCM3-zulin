<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>excel</title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<style type="text/css">
			.main{
				height:400px;
				border: 1px solid #999;
				overflow:auto;
			}
			.btn{background-color: #D2340F;color:white;text-decoration: none;font-size: 15px;padding:5px 6px;border-radius: 3px;cursor: pointer;}
			.btn-buynow{background-color: #D2340F;}
			.btn-buynow:hover{background-color: #EF411A;}
			.btn-signup{background-color: #006DCC;}
			.btn-signup:hover{background-color: rgb(0,74,204);}
			.btn-login{background-color: rgb(113,181,27);}
			.btn-login:hover{background-color: rgb(147,199,25);}
		</style>
	</head>
	<body>
	<div id="wait2" style="display:none;"></div>
	<div id="wait" style="display:none;">
	    <span id="UploadWaiting" style="color: #F0F0EB;"></span>
	</div>
		<div class="tool">
		
		</div>
		
		<form id="excelform" method="post" >
			<input type="hidden" id="filetype" name="filetype"/>
			<div class="easyui-tabs" fit="false" plain="true" id="tabs1">
				<div title='单位' id="unitdiv" style="padding: 10px;overflow: hidden;">
					<div id="1" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="1"><fmt:message key="unit" /><fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								4、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="unitfilepath" name="unitfilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="unitbrowsebutton" name="unitbrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="unitclearbutton" name="unitclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="unitimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
				<div title='物资类别' id="materialtypediv" style="padding:10px;overflow: hidden;">
					<div id="2" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="2"><fmt:message key="supplies_category" /><fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								4、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
								5、<fmt:message key="import_max4" /><br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="materialtypefilepath" name="materialtypefilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="materialtypebrowsebutton" name="materialtypebrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="materialtypeclearbutton" name="materialtypeclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="materialtypeimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
				<div title='物资' id="materialdiv" style=" padding: 10px;height: 450px;">
					<div id="3" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="3"><fmt:message key="supplies" /><fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="before_import_material_class" /><br/>
								4、<fmt:message key="before_import_material_unit" /><br/>
								5、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								6、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="materialfilepath" name="materialfilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="materialbrowsebutton" name="materialbrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="materialclearbutton" name="materialclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="materialimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
				<div title='供应商类别' id="suppliertypediv" style="padding: 10px;overflow: hidden;">
					<div id="4" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="4"><fmt:message key="suppliers" /><fmt:message key="category" /><fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								4、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
								5、<fmt:message key="import_max4" /><br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="suppliertypefilepath" name="suppliertypefilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="suppliertypebrowsebutton" name="suppliertypebrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="suppliertypeclearbutton" name="suppliertypeclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="suppliertypeimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
				<div title='供应商' id="supplierdiv" style="padding: 10px;overflow: hidden;">
					<div id="5" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="5"><fmt:message key="suppliers" /><fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="before_import_supplier_class" /><br/>
								4、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								5、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="supplierfilepath" name="supplierfilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="supplierbrowsebutton" name="supplierbrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="supplierclearbutton" name="supplierclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="supplierimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
				<div title='组织结构' id="orgdiv" style="padding: 10px;overflow: hidden;">
					<div id="6" class="main">
						<div class="form-line">
							<div class="form-input" style="margin-top: 10px;">
								<span onclick="download(this)" class="btn btn-signup" value="6"><fmt:message key="org" />结构<fmt:message key="template_download" /></span> 
							</div>
						</div>
						<br/><hr />
						<div style="color: red;">
							<fieldset  style="width: 450px;height: 150px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="precautions" /></legend>
								1、<fmt:message key="confirm_data_1sheet" /><br/>
								2、<fmt:message key="before_import_delete_example" />。<br/>
								3、<fmt:message key="coding" /><fmt:message key="onlynumletter" /><br/>
								4、<fmt:message key="name" /><fmt:message key="cannot_contain_special_characters" />（<fmt:message key="for_example" />：' "）<br/>
								5、<fmt:message key="import_max4" /><br/>
							</fieldset>
						</div>
						<div>
							<fieldset  style="width: 450px;height: 50px;margin-left: 10px;margin-top: 20px;">
								<legend><fmt:message key="please_select" />Excel</legend>
								<span>
									<input type="text" id="orgfilepath" name="orgfilepath" style="width: 250px;" readonly="readonly"/>
									<input type="button" id="orgbrowsebutton" name="orgbrowsebutton"  value="<fmt:message key='select1' /><fmt:message key='file' />"/>
									<input type="button" id="orgclearbutton" name="orgclearbutton"  value="<fmt:message key='empty' /><fmt:message key='file' />"/>
								</span>
								<span style="vertical-align: bottom;margin-left: 10px;">
									<input type="button" id="orgimportbutton" onclick="importExcel(this)"  style="width: 60px;" value="<fmt:message key="import" />"></input>
								</span>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</form>
			
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/cha/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/upload/plupload.full.min.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		
		var importstate = true;//导入状态
		var tt;//定时器
		
		$(document).ready(function(){
			var bodywidth = $(document.body).width();
			var bodyheight = $(document.body).height();
			
			$("#tabs1").css('width',bodywidth*0.9);
			$("#tabs1").css('height',bodyheight);
			
			$(".panel").css('width',bodywidth*0.9);
			$(".panel").css('height',bodyheight*0.88);
			$(".tabs-panels").css('width',bodywidth*0.88);
			$(".tabs-panels").css('height',bodyheight*0.88);
			
			$(".main").css('width',bodywidth*0.85);
			$(".main").css('height',bodyheight*0.9-50);
			$(".main").parent().css('width',bodywidth*0.85);
			$(".main").parent().css('height',bodyheight);
			
			$(".tabs").css('width',bodywidth*0.88);
			
			//判断导入权限
			var operate = "${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'import')}";
			if(!operate){
				$("input[id$=button]").attr('disabled',true);
			}
			
			$('#tabs1').tabs( {onSelect:function(){
				selecttabs();
			}});
			$("input[id$=clearbutton]").click(function(){
				$(this).prev().prev().val('');	
			});
		});
		//模板下载
		function download(e){
			$("#filetype").val($(e).attr('value'));
			$("#excelform").attr("action","<%=path %>/excel/download.do");
			$("#excelform").submit();
		}
		//excel导入
		function importExcel(e){
			var inputName = $(e).closest('span').prev().find('input[type=text]').val();
			if(inputName==''){
				alerterrorbreak('<fmt:message key="please_select_the_imported" />！',function(){
					clearInterval(tt);//清除定时器
				});
				return;
			}
			$("#wait").show();
             		$("#wait2").show();
		}
		var uploader;
		//选择页签
		function selecttabs(){
			if(uploader!=undefined){
				uploader.destroy();
				clearInterval(tt);//清除定时器
			}
			var tab= $('#tabs1').tabs('getSelected');//获取选中的面板	
			var content = tab.panel("body");
			
			var filepathid = content.find("input[id$=filepath]").attr("id");//文件目录名id
			var browsebuttonid = content.find("input[id$=browsebutton]").attr("id");//选择文件按钮id
			var importbutton = content.find("input[id$=importbutton]").attr("id");//导入文件按钮id
			var importfiletype = content.find(".main").attr("id");//导入文件类型
			
			//导入控件=======================================================================
			uploader = new plupload.Uploader({
			    browse_button : browsebuttonid, //触发文件选择对话框的按钮，为那个元素id
			    url : "<%=path%>/excel/excelImport.do", //服务器端的上传页面地址
			    flash_swf_url : '<%=path%>/js/assistant/upload/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
			    silverlight_xap_url : '<%=path%>/js/assistant/upload/Moxie.xap',//silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
			    multipart_params:{inputName:'file',filetype:importfiletype},//传入参数
			    multi_selection:false,
			    runtimes:'html5,html4,silverlight,flash',
			    filters: {
			        mime_types : [{ title : "excel文件", extensions : "xls,xlsx" }],
			        max_file_size : '512mb', //最大只能上传512mb的文件
			        prevent_duplicates : false //不允许选取重复文件
			    },
			    init : {
			        PostInit: function() {
			            //最后给"开始上传"按钮注册事件
			            $("#"+importbutton).click(function(){
			                uploader.start(); //调用实例对象的start()方法开始上传文件，当然你也可以在其他地方调用该方法
			                tt = setInterval("readImportState()", 500);
			            });
			        },
			        FileUploaded:function(uploader,file,responseObject){
			        	var preinfo = responseObject.response;
			        	var errorinfo = '';
			        	if(preinfo.indexOf("</pre>")>0||preinfo.indexOf("</PRE>")>0){
			        		errorinfo = preinfo.substring(preinfo.indexOf(">")+1,preinfo.lastIndexOf("<")); 
			        	}else{
			        		errorinfo = preinfo;
			        	}
			        	$("#"+filepathid).val('');
			        	$("#wait").hide();
	             		$("#wait2").hide();
			        	analyseErrorInfo(errorinfo);
			        },
			        FilesAdded:function(uploader,files){
			            if(uploader.files.length>1){
			                uploader.removeFile(uploader.files[0]);
			            }
			            for(var i = 0, len = files.length; i<len; i++){
			                var file_name = files[i].name; //文件名
			                $("#"+filepathid).val(file_name);
			            }
			        },
			        Error:function(uploader,file){
			        	clearInterval(tt);//清除定时器
			        	$("#wait").hide();
	             		$("#wait2").hide();
			            switch (file.code){
			                case -601:
			                    alerterror("<fmt:message key='type_of_file' />！");
			                    break;
			                case -600:
			                	alerttips("<fmt:message key='file_maxsize' />500M！");
			                    break;			                
			                default :
			                    alerterror("<fmt:message key='upload' /><fmt:message key='failure' />！");
			                    break;
			            }
			        },
			        UploadProgress:function(uploader,file){
			        }
			    }
			});
			//==============================================================================
			uploader.init();
			
		}
		//解析错误信息
		function analyseErrorInfo(errorinfo){
			clearInterval(tt);//清除定时器
			resetImportState();//重置订单状态
			
			var colarray = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
			if(errorinfo.indexOf('OK')!=-1){
				var infoarray = errorinfo.split(':');
				alerttips('<fmt:message key="import_successful" />！'+infoarray[2]+'<fmt:message key="article" />');     
			}else if(errorinfo=='orgstructure'){
				alerterror('没有组织结构类型！');
			}else if(errorinfo=='database'){
				alerterror('<fmt:message key="import" /><fmt:message key="failure" />！');
			}else{
				var info = eval('('+errorinfo+')');
				var type = info.type;
				var attr = info.attrname;
				if(type=='template'){
					alerterror('<fmt:message key="import" /><fmt:message key="template" /><fmt:message key="error" />！');
				}
				if(type=='null'){
					alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="gyszmbm" /><fmt:message key="cannot_be_empty" />！');
				}
				if(type=='exist'){
					alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="gyszmbm" /><fmt:message key="already_exists" />！');
				}
				if(type=='excelexist'){
					alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" /><fmt:message key="and" /><fmt:message key="the" />'+info.rowexist+'<fmt:message key="line" /><fmt:message key="gyszmbm" /><fmt:message key="duplicate" />！');
				}
				if(type=='notexist'){
					if(attr=='typecode'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="category" /><fmt:message key="gyszmbm" /><fmt:message key="not_exists" />，<fmt:message key="import_category_first" />！');
					}else if(attr=='typename'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="category" /><fmt:message key="name" /><fmt:message key="not_exists" />,<fmt:message key="import_category_first" />！');
					}else if(attr=='unit'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="unit" /><fmt:message key="not_exists" />！');
					}
				}
				if(type=='type'){
					var attr = info.attrname;
					if(attr=='code'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="category" /><fmt:message key="gyszmbm" /><fmt:message key="incorrect_format" />！');	
					}else if(attr=='parentcode'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="The_parent" /><fmt:message key="category" /><fmt:message key="gyszmbm" /><fmt:message key="incorrect_format" />！');
					}else if(attr=='price'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="incorrect_format" />！');//参考单价
					}else if(attr=='inprice'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="incorrect_format" />！');//进货价
					}else if(attr=='minnum'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="incorrect_format" />！');//最小采购量
					}else if(attr=='diffrate'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="incorrect_format" />！');//验货差异率
					}
				}
				if(type=='character'){
					alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="no_contain_special_char" />！');//名称
				}
				if(type=='length'){
					var attr = info.attrname;
					var len = info.length;
					if(attr=='code'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="gyszmbm" /><fmt:message key="the_maximum_length_not" />'+len+'');	
					}else if(attr=='name'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//名称	
					}else if(attr=='spec'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//规格	
					}else if(attr=='brand'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//品牌	
					}else if(attr=='addr'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//地址	
					}else if(attr=='parentcode'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="The_parent" /><fmt:message key="gyszmbm" /><fmt:message key="the_maximum_length_not" />'+len+'');	
					}else if(attr=='contact'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//联系人	
					}else if(attr=='tele'){
						alerterror('<fmt:message key="the" />'+info.row+'<fmt:message key="line" />'+colarray[info.col]+'<fmt:message key="row" /><fmt:message key="the_maximum_length_not" />'+len+'');//电话	
					}
				}
			}
			
		}
		
		function readImportState(){
			var param = {};
			$.post('<%=path%>/excel/readImportState.do',param,function(re){
				var rearray = re.split('-');
				var state = rearray[0];
				var total = rearray[1];
				var cur = rearray[2];
				
				if(state==0){
					$("#UploadWaiting").text('文件上传中...');
				}else if(state==1){
					$("#UploadWaiting").text('文件解析中...');
				}else if(state==2){
					$("#UploadWaiting").text('共'+total+'条数据，当前导入第'+cur+'条！');
				}
			});
		}
		function resetImportState(){
			var param = {};
			$.post('<%=path%>/excel/resetImportState.do',param,function(re){
				$("#UploadWaiting").text('');
			});
			
		}
		
		</script>
	</body>
</html>