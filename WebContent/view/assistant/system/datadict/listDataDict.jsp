<%@ page import="com.choice.orientationSys.constants.StringConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%
	String path = request.getContextPath();
String rootId = StringConstant.ROOT_ID;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="data"/><fmt:message key="Dictionary"/></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<style type="text/css">
			
			</style>
		</head>
	<body>
		<%@ include file="../../../share/useable.jsp"%>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div> 
		<div class="leftFrame">
        	<div id="toolbar"></div>
	    	<div class="treePanel">
		        <script src="<%=path%>/js/tree/MzTreeView10.js" type="text/javascript"></script>
	        	<script type="text/javascript">
	          		var tree = new MzTreeView("tree");
	          
	          		tree.nodes['0_00000000000000000000000000000000'] = 'text:<fmt:message key="all"/>';
	          		tree.nodes['00000000000000000000000000000000_20'] 
	          		= 'text:采购相关';
		          		tree.nodes['20_2001'] 
		          		= 'text:询价类型; method:changeUrl("inquiry/list.do","2001")';
		          		tree.nodes['20_2002'] 
		          		= 'text:合同条款类型; method:changeUrl("contractterms/list.do","2002")';
		          		tree.nodes['20_2003'] 
		          		= 'text:其他应付款; method:changeUrl("otherpay/list.do","2003")';
		          		tree.nodes['20_2004'] 
		          		= 'text:验货类型; method:changeUrl("inspection/list.do","2004")';
		          		tree.nodes['20_2005'] 
		          		= 'text:招标类型; method:changeUrl("tender/list.do","2005")';
// 		          		tree.nodes['20_2006'] 
// 		          		= 'text:税率设置; method:changeUrl("taxRate/list.do","2006")';
		          		tree.nodes['20_2007'] 
		          		= 'text:付款方式; method:changeUrl("payMethod/list.do","2007")';
// 	          		tree.nodes['00000000000000000000000000000000_30'] 
// 	          		= 'text:库存相关';
	          		
	          		 tree.setIconPath("<%=path%>/image/tree/none/");
	          		document.write(tree.toString());
	        	</script>
	    	</div>
    	</div>
	    <div class="mainFrame">
	      <iframe src="" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
	    </div>
    
	    <input type="hidden" id="cid" name="cid" />
	    <input type="hidden" id="url" name="url" />
	    
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/system/department.js"></script>
		<script type="text/javascript">

			//ajax同步设置
			$.ajaxSetup({
				async: false
			});
			function changeUrl(url,id){
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
				$("#cid").val(id);
				$("#url").val(url);
	      		window.mainFrame.location = "<%=path%>/"+url;

			}
			
		    function refreshTree(){
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
					window.location.href = '<%=path%>/orgstructure/toDataDict.do?cid='+$("#cid").val()+'&url='+$("#url").val();
		    }
		    
		    function pageReload(){
				$("#wait2").css("display","block");
				$("#wait").css("display","block");
		    	window.location.href = '<%=path%>/orgstructure/toDataDict.do?cid='+$("#cid").val()+'&url='+$("#url").val();
		    }
	    
			$(document).ready(function(){
				
				var toolbar = $('#toolbar').toolbar({
					items: [{
							text: '<fmt:message key="expandAll" />',
							title: '<fmt:message key="expandAll"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-80px']
							},
							handler: function(){
								tree.expandAll();
							}
						},{
							text: '<fmt:message key="refresh" />',
							title: '<fmt:message key="refresh"/>',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','0px']
							},
							handler: function(){
								refreshTree();
							}
						}
					]
				});//end toolbar
				
				setElementHeight('.treePanel',['#toolbar'],$(document.body),30);
				
				tree.focus('${cid}');
				changeUrl('${url}','${cid}');
				
				
			});// end $(document).ready();
		</script>

	</body>
</html>