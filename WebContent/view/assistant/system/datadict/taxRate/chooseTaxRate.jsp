<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="org" /><fmt:message key="select1" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<form id="queryForm" name="queryForm" action="<%=path %>/taxRate/chooseTaxRate.do" method="post">
				<div class="form-line">
					<div class="form-label" style="width:40px"><fmt:message key="coding" />：</div>
					<div class="form-input">
						<input id="vcode" name="vcode" class="text"/>
					</div>
					<div class="form-label" style="width:40px"><fmt:message key="name" />：</div>
					<div class="form-input">
						<input id="vname" name="vname" class="text"/>
					</div>
					<div class="form-label" style="width:60px"><fmt:message key="abbreviation" />：</div>
					<div class="form-input">
						<input id="vinit" name="vinit" class="text"/>
					</div>
					<input type="button" id="filterquery" value="<fmt:message key="query" />"/>
				</div>
				<div class="grid">
				<div class="table-head">
					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td class="num"><span style="width: 20px;"></span></td>
								<td style="width:30px;"><input type="checkbox" id="chkAll" />
								</td>
								<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
								<td><span style="width:150px;"><fmt:message key="name" /></span></td>
								<td><span style="width:70px;"><fmt:message key="tax_rate" />(%)</span></td>
								<td><span style="width:50px;"><fmt:message key="enable_state" /></span></td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="table-body">
					<table cellspacing="0" cellpadding="0" class="datagrid">
						<tbody>
							<c:forEach var="taxRate" items="${taxList}"
								varStatus="status">
								<tr>
									<td class="num"><span style="width: 20px;">${status.index+1}</span>
									</td>
									<td style="width:30px; text-align: center;">
									<input type="checkbox" name="idList" id="<c:out value='${taxRate.pk_tax}' />"
										value="<c:out value='${taxRate.pk_tax}' />" />
									</td>
									<td><span style="width:70px;">${taxRate.vcode}</span></td>
									<td><span style="width:150px;">${taxRate.vname}</span></td>
									<td><span style="width:70px;text-align: right;">${taxRate.ntaxrate}</span></td>
									<td>
										<span style="width:50px;"> 
											<c:if test="${taxRate.enablestate==1}"><fmt:message key="not_enabled" /></c:if> 
											<c:if test="${taxRate.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
											<c:if test="${taxRate.enablestate==3}"><fmt:message key="stop_enabled" /></c:if> 
										</span>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<page:page form="listForm" page="${pageobj}"></page:page>
			<input type="hidden" name="nowPage" id="nowPage" value="${pageobj.nowPage }" /> 
			<input type="hidden" name="pageSize" id="pageSize" value="${pageobj.pageSize }" />
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		
		<script type="text/javascript">
			var selected;
			$(document).ready(function(){
				$(document).keydown(function(e){
					if(e.keyCode == 13) $("#filterquery").trigger('click');
				});
				selected = '${domId}' == 'selected' ? parent.selected : (typeof(parent['${domId}']) == 'function' ? parent['${domId}']() : $('#${domId}',parent.document).val().split(","));
				if(selected){
					$(".table-body").find('tr td input').each(function(){
						if($.inArray($(this).val(),selected) >= 0){//若大于等于零，则这个id已存在父页面中
							$(this).attr('checked','checked');
						}
					});
				}
				$("#toolbar").toolbar({
						items: [{
							text: '<fmt:message key="enter" />',
							useable:true,
							handler: function(){
								var checkboxList = $('.grid').find('.table-body').find(':checkbox');
								var data = {show:[],code:[],mod:[],entity:[]};
								checkboxList.filter(':checked').each(function(){
									var entity = {};
									var row = $(this).closest('tr');
									data.code.push($(this).val());
									data.show.push($.trim(row.children('td:eq(3)').text()));
									entity.vcode = $.trim(row.children('td:eq(2)').text());
									entity.vname = $.trim(row.children('td:eq(3)').text());
									entity.pk_id = $.trim($(this).val());
									data.entity.push(entity);
								});
								parent['${callBack}'](data);
								$(".close",parent.document).click();
							}
						},{
							text: '<fmt:message key="cancel" />',
							useable: true,
							handler: function(){
								$(".close",parent.document).click();
							}
						}
					]
				});
				$("#filterquery").click(function(){
					var trlist=$(".table-body").find('tbody').find('tr');
					var bcode=$.trim($("#vcode").val());
					var bname=$.trim($("#vname").val());
					if(trlist.length>0){
						trlist.each(function(){
							var vcode=$(this).find('td span').eq(2).text();
							var vname=$(this).find('td span').eq(3).text();
							var vmarket=$(this).find('td span').eq(5).text();
							var bol=false;
							if(bcode!=""){
								if($.trim(vcode).indexOf(bcode)){
									bol=true;
								}
							}
							if(bname!=""){
								if($.trim(vname).indexOf(bname)){
									bol=true;
								}
							}
							if(bol){
								//一旦有不满足<fmt:message key="condition" />，即把这行隐藏,否则现在
								$(this).attr("style","display:none");
								$(this).find('input').attr("checked", false );
							}else{
								$(this).attr("style","display: table-row;");
							}
						});
					}
				});
				setElementHeight('.grid',['.tool'],$(document.body),50);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');				//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$('.grid').find('.table-body').find('tr').live('mouseover',function(){
					$(this).addClass('tr-over');
				});
				$('.grid').find('.table-body').find('tr').live('mouseout',function(){
					$(this).removeClass('tr-over');
				});
				//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				var mod = '<c:out value="${single}"/>';
				if(mod){
					$('#chkAll').unbind('click');
					$('#chkAll').css('display','none');
				}else{
					$("#chkAll").click(function(){
						if($(this)[0].checked){
							$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
						}else{
							$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
						}
					});
				}
				$('.grid').find('.table-body').find('tr').live("click", function () {
					$(this).find(':checkbox').trigger('click');
				 });
				$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
					var mod = '<c:out value="${single}"/>';
					if(mod){
						$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
						//$(this).attr('checked','checked');
					}
					event.stopPropagation();
				});
				if(typeof(parent.editTable) == 'function')
					parent.editTable($('.grid'));
			});
			
		</script>
	</body>
</html>