<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="inspection" /><fmt:message key="type" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<style type="text/css">
			.form-line .form-input{
				margin-right: 0px;
				padding-left: 0px;
			}
		</style>
	</head>
	<body>
		<div class="form">
			<form id="InspectionForm" method="post" action="<%=path %>/inspection/saveInspection.do">
			<input type="hidden" id="pk_inspection" name="pk_inspection" value="${inspection.pk_inspection}"/>
			<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="coding" />:</div>
					<div class="form-input"><input type="text" id="vcode" name="vcode" class="text" value="${inspection.vcode }"/></div>
				</div>
				<div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="name" />:</div>
					<div class="form-input"><input type="text" id="vname" name="vname" class="text" value="${inspection.vname }"  /></div>
				</div>
				 <div class="form-line">
					<div class="form-label"><span class="red">*</span><fmt:message key="enable_state" />:</div>
					<div class="form-input">
						<select id="enablestate" class="select" name="enablestate" style="width: 133px;" >
							<option value="1"  <c:if test="${inspection.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
							<option value="2"  <c:if test="${inspection.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
							<option value="3"  <c:if test="${inspection.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/validate.js"></script>	
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		
		<script type="text/javascript">
		var validate;
		//防止没有焦点时按后退键退回上个页面
		$(document).ready(function(){
			if('${inspection.vcode }'!=null && '${inspection.vcode }'!=''){
				$("#vcode").attr("disabled","disabled");
				$("#enablestate").attr("disabled","disabled");
				$("#vname").focus();
			}
			if('${inspection.enablestate }'==null || '${inspection.enablestate }'==''){
				$("#enablestate").val("2");
				$("#vcode").focus();
			}
			/*验证*/
			validate = new Validate({
				validateItem:[{
						type:'text',
						validateObj:'vcode',
						validateType:['handler'],
						handler:function(){
							var result = true;
							if($("#pk_inspection").val()!=""){
								return true;
							}
							var commonMethod = new CommonMethod();
							commonMethod.vcode = $("#vcode").val();
							commonMethod.vcodename = "vcode";
							commonMethod.vtablename = "cscm_inspection";
							var returnvalue = validateVcode("<%=path %>",commonMethod);
							if (returnvalue == "1") {
								result = false;
							}
							return result;
						},
						param:['F'],
						error:['<fmt:message key="already_exists" />,<fmt:message key="please_re_enter" />！']
				},{
					type:'text',
					validateObj:'vcode',
					validateType:['canNull','maxLength','alphcomb'],
					param:['F',8,'F'],
					error:['<fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />8','<fmt:message key="onlynumletter" />']
				},{
					type:'text',
					validateObj:'vname',
					validateType:['handler'],
					handler:function(){
						var result = true;
						var commonMethod = new CommonMethod();
						commonMethod.vcode = $("#vname").val();
						commonMethod.vcodename = "vname";
						if($("#pk_inspection").val()!=null && $("#pk_inspection").val()!=''){
							commonMethod.pk_id = $("#pk_inspection").val();
							commonMethod.params = "false";
							commonMethod.ids = "pk_inspection";
						}
						commonMethod.vtablename = "cscm_inspection";
						var returnvalue = validateVcode("<%=path %>",commonMethod);
						if (returnvalue == "1") {
							result = false;
						}
						return result;
					},
					param:['F'],
					error:['<fmt:message key="already_exists" />,<fmt:message key="please_re_enter" />！']
				},{
					type:'text',
					validateObj:'vname',
					validateType:['canNull','maxLength','withOutSpecialChar'],
					param:['F',50,'F'],
					error:['<fmt:message key="cannot_be_empty" />！','<fmt:message key="the_maximum_length" />50','<fmt:message key="no_contain_special_char" />\'?']
				}
				]
			});
		});
		</script>
	</body>
</html>