<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="inquiry" /><fmt:message key="type" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<style type="text/css">
			.separator{
				display:none !important;
			}
			.page{
/* 				margin-bottom: 28px; */
			}
			.separator ,div , .pgSearchInfo{
				display: none;
			}
			div[class]{
				display:block;
			}
			.tr-select{
				background-color: #D2E9FF;
			}
		</style>
	</head>
	<body>
		<div class="tool">
		
		</div>
		<form id="queryForm" action="<%=path%>/inquiry/list.do" method="post">

		<div class="grid" style="positn: static;">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td class="num"><span style="width: 20px;"></span></td>
							<td style="width:30px;"><input type="checkbox" id="chkAll" />
							</td>
							<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
							<td><span style="width:150px;"><fmt:message key="name" /></span></td>
<%-- 							<td><span style="width:70px;"><fmt:message key="abbreviation" /></span></td> --%>
							<td><span style="width:60px;"><fmt:message key="enable_state" /></span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" class="datagrid">
					<tbody>
						<c:forEach var="inquiry" items="${inquiryList}"
							varStatus="status">
							<tr>
								<td class="num"><span style="width: 20px;">${status.index+1}</span>
								</td>
								<td style="width:30px; text-align: center;">
								<input type="checkbox" name="idList" id="<c:out value='${inquiry.pk_inquiry}' />"
									value="<c:out value='${inquiry.pk_inquiry}' />" />
								</td>
								<td><span style="width:70px;">${inquiry.vcode}</span></td>
								<td><span style="width:150px;">${inquiry.vname}</span></td>
<%-- 								<td><span style="width:70px;">${inquiry.vInit}</span></td> --%>
								<td>
									<span style="width:60px;"> 
										<c:if test="${inquiry.enablestate==1}"><fmt:message key="not_enabled" /></c:if> 
										<c:if test="${inquiry.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
										<c:if test="${inquiry.enablestate==3}"><fmt:message key="stop_enabled" /></c:if> 
									</span>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<page:page form="queryForm" page="${pageobj}"></page:page>
		<input type="hidden" name="nowPage" id="nowPage" value="${pageobj.nowPage }" /> 
		<input type="hidden" name="pageSize" id="pageSize" value="${pageobj.pageSize }" />

		<div class="search-div" style="margin-left: 0px;">
			<div class="search-condition">
				<table class="search-table" cellspacing="0" cellpadding="0">
					<tr>
						<td class="c-left"><fmt:message key="coding" />：</td>
						<td>
							<input type="text" id="vcode" name="vcode" class="text" onkeypress="IsNum(event)" value="${inquiry.vcode }" />
						</td>
						<td class="c-left"><fmt:message key="name" />：</td>
						<td>
							<input type="text" id="vname" name="vname" class="text" value="${inquiry.vname }" />
						</td>
						<td class="c-left"><fmt:message key="enable_state" />：</td>
						<td>
							<select id="enablestate" name="enablestate" style="width:133px;" class="select">
								<option value=""><fmt:message key="all" /></option>
									<option value="1"  <c:if test="${inquiry.enablestate==1}"> selected="selected" </c:if> ><fmt:message key="not_enabled" /></option>
									<option value="2"  <c:if test="${inquiry.enablestate==2}"> selected="selected" </c:if> ><fmt:message key="have_enabled" /></option>
									<option value="3"  <c:if test="${inquiry.enablestate==3}"> selected="selected" </c:if> ><fmt:message key="stop_enabled" /></option>
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="search-commit">
				<input type="button" class="search-button" id="search" value='<fmt:message key="enter" />' />
				<input type="button" class="search-button" id="resetSearch" value='<fmt:message key="empty" />' />
			</div>
		</div>
	</form>
		
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/validate/assvalidate.js"></script>	
		<script type="text/javascript">

			var savelock = false;
			 function pageReload(){
				$('#queryForm').submit();
			} 
			 $('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			$('.grid').find('.table-body').find('tr').hover(
				function(){
					$(this).addClass('tr-over');
				},
				function(){
					$(this).removeClass('tr-over');
				}
			);
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			$('.grid').find('.table-body').find('tr').live("click", function () {
		         $(this).addClass("bgBlue").find(":checkbox").attr("checked", "checked");
		         $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				var $tmp=$('[name=idList]:checkbox');
				//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
				$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
			 });
			$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
				event.stopPropagation(); 
			});
			
			$(document).ready(function(){
				$('.search-div').hide(); 
				var tool = $('.tool').toolbar({
					items: [{
							text: '<fmt:message key="insert" />',
							useable: parent.pageUseable.insert,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								save();
							}
						},{
							text: '<fmt:message key="update" />',
							useable: parent.pageUseable.update,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-18px','0px']
							},
							handler: function(){
								update();
							}
						},{
							text: '<fmt:message key="enable" />',
							useable: parent.pageUseable.enable,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-60px','-40px']
							},
							handler: function(){
								enableInquiry();
							},
							items:[{
								text: '<fmt:message key="disable1" />',
								useable: parent.pageUseable.disable,
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-40px']
								},
								handler: function(){
									disableInquiry();
								}
							}]
						},{
							text: '<fmt:message key="select" />',
							useable: parent.pageUseable.select,
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$("#vcode").focus();
							}
						},{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));								
							}
						}
					]
				});
				
				$("#search").bind('click', function() {
					$("#wait2",parent.window.document).css("display","block");
					$("#wait",parent.window.document).css("display","block");
					$("#vcode").val(stripscript($("#vcode").val()));
					$("#vname").val(stripscript($("#vname").val()));
					$('.search-div').hide();
					$('#queryForm').submit();
				});
				/* 模糊<fmt:message key="select" />清空 */
				$("#resetSearch").bind('click', function() {
					$('.search-condition input').val('');
				});
				
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),28);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法
				$("#wait2",parent.window.document).css("display","none");
				$("#wait",parent.window.document).css("display","none");
			});
			function save(){
				$('body').window({
					id: 'window_save',
					title: '<fmt:message key="insert" />',
					content: '<iframe id="saveFrame" frameborder="0" src="<%=path%>/inquiry/toEditInquiry.do"></iframe>',
					width: '650px',
					height: '400px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								title: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){;
									if(savelock){
										return;
									}else{
										savelock = true;
									}
									if(getFrame('saveFrame')&&window.document.getElementById("saveFrame").contentWindow.validate._submitValidate()){
										savelock = true;
										submitFrameForm('saveFrame','InquiryForm');
									}else{
										savelock = false;
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								title: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			
			function update(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList && checkboxList.filter(':checked').size() == 1){
					var chkValue = checkboxList.filter(':checked').eq(0).val();
					
					$('body').window({
						title: '<fmt:message key="update" />',
						content: '<iframe id="updateFrame" frameborder="0" src="<%=path%>/inquiry/toEditInquiry.do?pk_inquiry='+chkValue+'"></iframe>',
						width: '650px',
						height: '400px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(savelock){
											return;
										}else{
											savelock = true;
										}
										if(getFrame('updateFrame')&&window.document.getElementById("updateFrame").contentWindow.validate._submitValidate()){
											submitFrameForm('updateFrame','InquiryForm');
										}else{
											savelock = false;
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList && checkboxList.filter(':checked').size() > 1){
					alerterror('<fmt:message key="you_can_only_modify_a_data" />！');
					return;
				}else {
					alerterror('<fmt:message key="please_select_information_you_need_to_modify"/>！');
					return ;
				}
				
			}
			
			
			function enableInquiry(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key='want_to_enable' />？",function(){
						var chkValue = [];
						var codeValue=[];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
							codeValue.push($.trim($(this).closest('td').next().find('span').text()));
						});
						
						var action = '<%=path%>/inquiry/enableInquiry.do?pk_inquiry='+chkValue.join(",")+'&vcode='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key="enable" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror("<fmt:message key='select_you_want_to_enable' />！");
					return ;
				}
			}
			
			
			function disableInquiry(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					var chkValue = [];
					var codeValue=[];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
						codeValue.push($.trim($(this).closest('td').next().find('span').text()));
					});
					//查询市场是否已被运营区引用
					var  cq = 0; //定义检查的返回值，0表示没被引用，1表示被引用，-1表示报错
					var checkcode = [];//定义被检查出来的编码
					for(var i=0;i<chkValue.length;i++){
						cq = checkQuote("<%=path%>","CBOH_BOH_3CH","PK_MARKET",chkValue[i]);
						if(cq>0){ //如果有被引用
							checkcode.push(codeValue[i]);  //将分类编码加入
						}
					}
					if(checkcode.length>0){
						alerterror('<fmt:message key="refencenotdisable"/>');
						return;
					}
					alertconfirm("<fmt:message key='want_to_disable' />？",function(){
						var action = '<%=path%>/inquiry/disableInquiry.do?pk_inquiry='+chkValue.join(",")+'&vcode='+codeValue.join(",");
						$('body').window({
							title: '<fmt:message key="disable1" />',
							content: '<iframe id="updateButtontypeFrame" frameborder="0" src="'+action+'"></iframe>',
							width: '300px',
							height: '200px',
							draggable: true,
							isModal: true
						});
					});
					
				}else{
					alerterror("<fmt:message key='select_you_want_to_disable' />！");
					return ;
				}
			}
			
		</script>
	</body>
</html>