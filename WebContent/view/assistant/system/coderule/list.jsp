<%--
  User: mc
  Date: 14-10-18
  Time: 下午2:32
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <title>编码规则</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
<div class="tool"></div>
<div class="easyui-tabs" fit="false" plain="true" id="tabs">
    <div title='单据号生成规则' id="tab2">
        <iframe src="<%=path%>/coderule/code.do" name="tab2" frameborder="0" width="100%" height="1000px" scrolling="auto"></iframe>
    </div>
    <div title='档案编码约束' id="tab1">
        <iframe src="<%=path%>/coderule/order.do" name="tab1" frameborder="0" width="100%" height="100%" scrolling="auto"></iframe>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.tool').toolbar({
        items: [{
            text: '<fmt:message key="update"/>',
            useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
            id:"update",
            icon: {
                    url: '<%=path%>/image/Button/op_owner.gif',
                    positn: ['0px','-40px']
                },
            handler: function(){
                var id=$('.easyui-tabs').tabs('getSelected')[0].id
                var iframe=$(".grid",window.frames[id].document);
                if($(iframe).find(":radio:checked").length>0) {
                    $(iframe).find(":radio:checked").parents("tr").find("*").not("select[type='5']").removeAttr("disabled");
                    $("#update").parents("li").toggle();
                    $("#save").parents("li").toggle();
                    $("#cancel").parents("li").toggle();
                    $(iframe).find(":radio").attr("disabled", "disabled");
                }else{
                    alerterror("请选中需要修改的数据！");
                }
            }
            },{
            text: '<fmt:message key="save"/>',
            useable: true,
            id:"save",
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                var id=$('.easyui-tabs').tabs('getSelected')[0].id
                window.frames[id].window.getData()
            }
        },{
            text: '<fmt:message key="cancel"/>',
            useable: true,
            id:"cancel",
            icon: {
                url: '<%=path%>/image/Button/op_owner.gif',
                positn: ['0px','-40px']
            },
            handler: function(){
                $("#update").parents("li").toggle();
                $("#save").parents("li").toggle();
                $("#cancel").parents("li").toggle();
                var id=$('.easyui-tabs').tabs('getSelected')[0].id
                $("#"+id).find("iframe").attr('src',encodeURI($("#"+id).find("iframe").attr("src")));
            }
        }]
    });
    //初始化隐藏保存按钮
    $("#save").parents("li").toggle();
    $("#cancel").parents("li").toggle();
    $('.easyui-tabs').tabs({
        width:$(document.body).width()*0.99,
        height:$(document.body).height()-50,
        onSelect:function(){
            if($("#update").parents("li").is(":hidden")) {
                $("#update").parents("li").toggle();
                $("#save").parents("li").toggle();
                $("#cancel").parents("li").toggle();
            }
            var id=$(this).tabs('getSelected')[0].id
            $("#"+id).find("iframe").attr('src',encodeURI($("#"+id).find("iframe").attr("src")));
        }
    });
// 	$("#wait2").css("display","none");
// 	$("#wait").css("display","none");
});
    var upButton=function(){
        var id=$('.easyui-tabs').tabs('getSelected')[0].id
        $("#"+id).find("iframe").attr('src',encodeURI($("#"+id).find("iframe").attr("src")));
        $("#update").parents("li").toggle();
        $("#save").parents("li").toggle();
        $("#cancel").parents("li").toggle();
    }
</script>
</body>
</html>
