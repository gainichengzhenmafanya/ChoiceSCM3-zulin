<%--
  编码规则
  User: mc
  Date: 14-11-8
  Time: 下午4:52
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <title>编码规格</title>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.form.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.css"/>
</head>
<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div> 
<div class="grid">
    <div class="table-head">
        <table cellspacing="0" cellpadding="0">
            <thead>
            <tr>
                <td rowspan="2"><span style="width: 30px;"></span></td>
                <td rowspan="2"><span style="width: 90px;"><fmt:message key="The_project_name"/></span></td>
                <td rowspan="2"><span style="width: 80px;"><fmt:message key="Coding_series"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_of_a"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_2"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_3"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_4"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_5"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Level_6"/></span></td>
                <td colspan="3"><span style="width: 184px;"><fmt:message key="Grade_seven"/></span></td>
                <td rowspan="2"><span style="width: 100px;"><fmt:message key="preview"/></span></td>
            </tr>
            <tr>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
                <td><span style="width: 47px;"><fmt:message key="The_length_of_the"/></span></td>
                <td><span style="width: 61px;"><fmt:message key="type"/></span></td>
                <td><span style="width: 91px;"><fmt:message key="value"/></span></td>
            </tr>
            </thead>
        </table>
    </div>
    <div class="table-body">
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <c:forEach items="${coderuleList}" var="coderule" >
                <tr>
                    <td>
                        <input type="hidden" value="${coderule.pk_coderule}" id="pk_coderule"/>
                        <span style="width: 30px;text-align: center;"><input type="radio" name="selItem"/></span></td>
                    <td><span style="width: 90px;">${coderule.vprojectname}</span></td>
                    <td>
                        <span style="width: 80px;">
                        <select disabled="disabled" id="icodeseries" name="icodeseries" class="select" style="width:79px;">
                            <option selected="selected" value="0">0</option>
                            <option <c:if test="${coderule.icodeseries==1}"> selected="selected"</c:if> value="1">1</option>
                            <option <c:if test="${coderule.icodeseries==2}"> selected="selected"</c:if> value="2">2</option>
                            <option <c:if test="${coderule.icodeseries==3}"> selected="selected"</c:if> value="3">3</option>
                            <option <c:if test="${coderule.icodeseries==4}"> selected="selected"</c:if> value="4">4</option>
                            <option <c:if test="${coderule.icodeseries==5}"> selected="selected"</c:if> value="5">5</option>
                            <option <c:if test="${coderule.icodeseries==6}"> selected="selected"</c:if> value="6">6</option>
                            <option <c:if test="${coderule.icodeseries==7}"> selected="selected"</c:if> value="7">7</option>
                        </select>
                        </span>
                    </td>
                    <td>
                        <span style="width: 47px;" id="ilength1">
                            <c:if test="${coderule.ilength1!=null && coderule.ilength1!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype1==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength1==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength1==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength1==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength1==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength1==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength1==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength1==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength1==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength1==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype1">
                            <c:if test="${coderule.itype1!=null && coderule.itype1!=''}">
                                <select disabled="disabled" name="itype1" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype1==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype1==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype1==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype1==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype1==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype1==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue1">
                            <c:if test="${coderule.vvalue1!=null && coderule.vvalue1!=''}">
                                <c:if test="${coderule.itype1==3}">
                                    <select disabled="disabled" name="vvalue1" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue1=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue1=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue1=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue1=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue1=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue1=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype1==4}">
                                    <select disabled="disabled" name="vvalue1" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue1==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue1==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue1==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue1==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype1!=4&&coderule.itype1!=3&&coderule.itype1!=5&&coderule.itype1!=null && coderule.itype1!=''}">
                                <input type="text" disabled="disabled" name="vvalue1" style="width: 90px;" value="${coderule.vvalue1}"/>
                            </c:if>
                        </span>
                    </td>
                    <%--级别一--%>
                    <td>
                        <span style="width: 47px;" id="ilength2">
                            <c:if test="${coderule.ilength2!=null && coderule.ilength2!=''}">
                                <select disabled="disabled"  <c:if test="${coderule.itype2==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength2==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength2==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength2==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength2==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength2==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength2==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength2==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength2==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength2==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype2">
                            <c:if test="${coderule.itype2!=null && coderule.itype2!=''}">
                                <select disabled="disabled" name="itype2" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype2==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype2==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype2==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype2==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype2==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype2==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue2">
                            <c:if test="${coderule.vvalue2!=null && coderule.vvalue2!=''}">
                                <c:if test="${coderule.itype2==3}">
                                    <select disabled="disabled" name="vvalue2" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue2=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue2=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue2=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue2=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue2=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue2=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype2==4}">
                                    <select disabled="disabled" name="vvalue2" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue2==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue2==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue2==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue2==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype2!=4&&coderule.itype2!=3&&coderule.itype2!=5&&coderule.itype2!=null && coderule.itype2!=''}">
                                <input type="text" disabled="disabled" name="vvalue2" style="width: 90px;" value="${coderule.vvalue2}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别二--%>
                    <td>
                        <span style="width: 47px;" id="ilength3">
                            <c:if test="${coderule.ilength3!=null && coderule.ilength3!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype3==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength3==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength3==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength3==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength3==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength3==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength3==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength3==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength3==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength3==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype3">
                            <c:if test="${coderule.itype3!=null && coderule.itype3!=''}">
                                <select disabled="disabled" name="itype3" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype3==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype3==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype3==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype3==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype3==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype3==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue3">
                            <c:if test="${coderule.vvalue3!=null && coderule.vvalue3!=''}">
                                <c:if test="${coderule.itype3==3}">
                                    <select disabled="disabled" name="vvalue3" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue3=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue3=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue3=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue3=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue3=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue3=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype3==4}">
                                    <select disabled="disabled" name="vvalue3" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue3==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue3==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue3==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue3==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype3!=4&&coderule.itype3!=3&&coderule.itype3!=5&&coderule.itype3!=null && coderule.itype3!=''}">
                                <input type="text" disabled="disabled" name="vvalue3" style="width: 90px;" value="${coderule.vvalue3}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别三--%>
                    <td>
                        <span style="width: 47px;" id="ilength4">
                            <c:if test="${coderule.ilength4!=null && coderule.ilength4!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype4==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength4==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength4==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength4==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength4==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength4==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength4==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength4==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength4==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength4==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype4">
                            <c:if test="${coderule.itype4!=null && coderule.itype4!=''}">
                                <select disabled="disabled" name="itype4" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype4==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype4==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype4==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype4==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype4==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype4==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue4">
                            <c:if test="${coderule.vvalue4!=null && coderule.vvalue4!=''}">
                                <c:if test="${coderule.itype4==3}">
                                    <select disabled="disabled" name="vvalue4" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue4=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue4=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue4=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue4=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue4=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue4=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype4==4}">
                                    <select disabled="disabled" name="vvalue4" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue4==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue4==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue4==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue4==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype4!=4&&coderule.itype4!=3&&coderule.itype4!=5&&coderule.itype4!=null && coderule.itype4!=''}">
                                <input type="text" disabled="disabled" name="vvalue4" style="width: 90px;" value="${coderule.vvalue4}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别四--%>
                    <td>
                        <span style="width: 47px;" id="ilength5">
                            <c:if test="${coderule.ilength5!=null && coderule.ilength5!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype5==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength5==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength5==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength5==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength5==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength5==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength5==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength5==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength5==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength5==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype5">
                            <c:if test="${coderule.itype5!=null && coderule.itype5!=''}">
                                <select disabled="disabled" name="itype5" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype5==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype5==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype5==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype5==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype5==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype5==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue5">
                            <c:if test="${coderule.vvalue5!=null && coderule.vvalue5!=''}">
                                <c:if test="${coderule.itype5==3}">
                                    <select disabled="disabled" name="vvalue5" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue5=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue5=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue5=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue5=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue5=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue5=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype5==4}">
                                    <select disabled="disabled" name="vvalue5" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue5==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue5==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue5==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue5==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype5!=4&&coderule.itype5!=3&&coderule.itype5!=5&&coderule.itype5!=null && coderule.itype5!=''}">
                                <input type="text" disabled="disabled" name="vvalue5" style="width: 90px;" value="${coderule.vvalue5}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别五--%>
                    <td>
                        <span style="width: 47px;" id="ilength6">
                            <c:if test="${coderule.ilength6!=null && coderule.ilength6!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype6==5}">type="5"</c:if> class="select" style="width:46px;">
                                    <option <c:if test="${coderule.ilength6==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength6==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength6==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength6==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength6==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength6==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength6==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength6==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength6==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype6">
                            <c:if test="${coderule.itype6!=null && coderule.itype6!=''}">
                                <select disabled="disabled" name="itype6" onchange="toLive(this)" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype6==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype6==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype6==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype6==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype6==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype6==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue6">
                            <c:if test="${coderule.vvalue6!=null && coderule.vvalue6!=''}">
                                <c:if test="${coderule.itype6==3}">
                                    <select disabled="disabled" name="vvalue6" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue6=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue6=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue6=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue6=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue6=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue6=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype6==4}">
                                    <select disabled="disabled" name="vvalue6" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue6==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue6==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue6==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue6==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype6!=4&&coderule.itype6!=3&&coderule.itype6!=5&&coderule.itype6!=null && coderule.itype6!=''}">
                                <input type="text" disabled="disabled" name="vvalue6" style="width: 90px;" value="${coderule.vvalue6}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别六--%>
                    <td>
                        <span style="width: 47px;" id="ilength7">
                            <c:if test="${coderule.ilength7!=null && coderule.ilength7!=''}">
                                <select disabled="disabled" <c:if test="${coderule.itype7==5}">type="5"</c:if> style="width:46px;">
                                    <option <c:if test="${coderule.ilength7==1}"> selected="selected"</c:if> value="1">1</option>
                                    <option <c:if test="${coderule.ilength7==2}"> selected="selected"</c:if> value="2">2</option>
                                    <option <c:if test="${coderule.ilength7==3}"> selected="selected"</c:if> value="3">3</option>
                                    <option <c:if test="${coderule.ilength7==4}"> selected="selected"</c:if> value="4">4</option>
                                    <option <c:if test="${coderule.ilength7==5}"> selected="selected"</c:if> value="5">5</option>
                                    <option <c:if test="${coderule.ilength7==6}"> selected="selected"</c:if> value="6">6</option>
                                    <option <c:if test="${coderule.ilength7==7}"> selected="selected"</c:if> value="7">7</option>
                                    <option <c:if test="${coderule.ilength7==8}"> selected="selected"</c:if> value="8">8</option>
                                    <option <c:if test="${coderule.ilength7==9}"> selected="selected"</c:if> value="9">9</option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 61px;" id="itype7">
                            <c:if test="${coderule.itype7!=null && coderule.itype7!=''}">
                                <select disabled="disabled" onchange="toLive(this)" name="itype7" id="type" class="select" style="width:60px;">
                                    <option <c:if test="${coderule.itype7==0}"> selected="selected"</c:if> value="0"><fmt:message key="There_is_no"/></option>
                                    <option <c:if test="${coderule.itype7==1}"> selected="selected"</c:if> value="1"><fmt:message key="digital"/></option>
                                    <option <c:if test="${coderule.itype7==2}"> selected="selected"</c:if> value="2"><fmt:message key="The_letter"/></option>
                                    <option <c:if test="${coderule.itype7==3}"> selected="selected"</c:if> value="3"><fmt:message key="date"/></option>
                                    <option <c:if test="${coderule.itype7==4}"> selected="selected"</c:if> value="4"><fmt:message key="Running_water"/></option>
                                    <option <c:if test="${coderule.itype7==5}"> selected="selected"</c:if> value="5"><fmt:message key="category"/></option>
                                </select>
                            </c:if>
                        </span>
                    </td>
                    <td>
                        <span style="width: 91px;" id="vvalue7">
                            <c:if test="${coderule.vvalue7!=null && coderule.vvalue7!=''}">
                                <c:if test="${coderule.itype7==3}">
                                    <select disabled="disabled" name="vvalue7" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue7=='yyyyMMdd'}"> selected="selected"</c:if> value="yyyyMMdd">YYYYMMDD</option>
                                        <option <c:if test="${coderule.vvalue7=='yyyyMM'}"> selected="selected"</c:if> value="yyyyMM">YYYYMM</option>
                                        <option <c:if test="${coderule.vvalue7=='MMdd'}"> selected="selected"</c:if> value="MMdd">MMDD</option>
                                        <option <c:if test="${coderule.vvalue7=='yyyy'}"> selected="selected"</c:if> value="yyyy">YYYY</option>
                                        <option <c:if test="${coderule.vvalue7=='MM'}"> selected="selected"</c:if> value="MM">MM</option>
                                        <option <c:if test="${coderule.vvalue7=='dd'}"> selected="selected"</c:if> value="dd">DD</option>
                                    </select>
                                </c:if>
                                <c:if test="${coderule.itype7==4}">
                                    <select disabled="disabled" name="vvalue7" class="select" style="width:90px;">
                                        <option <c:if test="${coderule.vvalue7==41}"> selected="selected"</c:if> value="41"><fmt:message key="Day_running_water"/></option>
                                        <option <c:if test="${coderule.vvalue7==42}"> selected="selected"</c:if> value="42"><fmt:message key="On_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue7==43}"> selected="selected"</c:if> value="43"><fmt:message key="In_the_water"/></option>
                                        <option <c:if test="${coderule.vvalue7==44}"> selected="selected"</c:if> value="44"><fmt:message key="Running_water"/></option>
                                    </select>
                                </c:if>
                            </c:if>
                            <c:if test="${coderule.itype7!=4&&coderule.itype7!=3&&coderule.itype7!=5&&coderule.itype7!=null && coderule.itype7!=''}">
                                <input type="text" disabled="disabled" name="vvalue7" style="width: 90px;" value="${coderule.vvalue7}"/>
                            </c:if>
                        </span>
                    </td>
                        <%--级别七--%>
                    <td><span style="width: 100px;" id="vpreview">${coderule.vpreview}</span></td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="<%=path%>/js/json2.js"></script>
<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
<script type="text/javascript" src="<%=path%>/js/assistant/alert/jquery/jNotify.jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var setWidth=function(id){
            var $grid=$(id);
            var headWidth=$grid.find(".table-head").find("tr").width();
            var gridWidth=$grid.width();
            if(headWidth>=gridWidth){
                $grid.find(".table-body").width(headWidth+1);
                $grid.find(".table-head").width(headWidth);
            }else{
                $grid.find(".table-body").width(gridWidth+1);
                $grid.find(".table-head").width(gridWidth);
            }
        }
        setElementHeight('.grid', ['.condition'], $(document.body), 0);	//计算.grid的高度
        setElementHeight('.table-body', ['.table-head'],'.grid'); //计算.table-body的高度
        loadGrid();//  自动计算滚动条的js方法
        setWidth(".grid");
        //级别选择
        $("select[name='icodeseries']").change(function(){
            for(var i=1;$(this).val() >= i;i++){
                var l=$(this).parents("tr").find("#ilength"+i);
                var t=$(this).parents("tr").find("#itype"+i);
                var v=$(this).parents("tr").find("#vvalue"+i);
                if(l.html()==null||$.trim(l.html())==''){
                    l.html('<select class="select" style="width:46px;">'+
                            '<option  value="1">1</option>'+
                            '<option  value="2">2</option>'+
                            '<option  value="3">3</option>'+
                            '<option  value="4">4</option>'+
                            '<option  value="5">5</option>'+
                            '<option  value="6">6</option>'+
                            '<option  value="7">7</option>'+
                            '<option  value="8">8</option>'+
                            '<option  value="9">9</option>'+
                            '</select>');
                    t.html('<select class="select" id="type" onchange="toLive(this)" style="width:60px;">'+
                            '<option  value="0"><fmt:message key="There_is_no"/></option>'+
                            '<option  value="1"><fmt:message key="digital"/></option>'+
                            '<option  value="2"><fmt:message key="The_letter"/></option>'+
                            '<option  value="3"><fmt:message key="date"/></option>'+
                            '<option  value="4"><fmt:message key="Running_water"/></option>'+
                            '<option  value="5"><fmt:message key="category"/></option>'+
                            '</select>');
                    v.html('<input type="text" style="width: 90px;"/>');
                }
            }
            for(var i=(parseInt($(this).val())+1);7>=i;i++){
                var l=$(this).parents("tr").find("#ilength"+i).html("");
                var t=$(this).parents("tr").find("#itype"+i).html("");
                var v=$(this).parents("tr").find("#vvalue"+i).html("");
            }
        });
        $('.grid').find('.table-body').find('tr').hover(
                function(){
                    $(this).addClass('tr-over');
                },
                function(){
                    $(this).removeClass('tr-over');
                }
        );
        $('.grid').find('.table-body').find('tr').click(function() {
            $(this).find("input[name='selItem']:enabled").attr("checked","true");
        });
		$("#wait2").css("display","none");
		$("#wait").css("display","none");
		$("#wait2",parent.window.document).css("display","none");
		$("#wait",parent.window.document).css("display","none");
    });
    var toLive=function(v){
        if($(v).val()==3){
            $(v).parents("td").next().find("span").html('<select class="select" style="width:90px;">'+
                    '<option  value="yyyyMMdd">YYYYMMDD</option>'+
                    '<option  value="yyyyMM">YYYYMM</option>'+
                    '<option  value="MMdd">MMDD</option>'+
                    '<option  value="yyyy">YYYY</option>'+
                    '<option  value="MM">MM</option>'+
                    '<option  value="dd">DD</option>'+
                    '</select>');
            $(v).parents("td").prev().find("select").removeAttr("disabled");
        }else if($(v).val()==4){
            $(v).parents("td").next().find("span").html('<select class="select" style="width:90px;">'+
                    '<option  value="41"><fmt:message key="Day_running_water"/></option>'+
                    '<option  value="42"><fmt:message key="On_the_water"/></option>'+
                    '<option  value="43"><fmt:message key="In_the_water"/></option>'+
                    '<option  value="44"><fmt:message key="Running_water"/></option>'+
                    '</select>');
            $(v).parents("td").prev().find("select").removeAttr("disabled");
        }else if($(v).val()==5){
            $(v).parents("td").next().find("span").html('');
            $(v).parents("td").prev().find("select").attr("disabled","disabled");
        }else{
            $(v).parents("td").next().find("span").html('<input type="text" style="width: 90px;"/>');
            $(v).parents("td").prev().find("select").removeAttr("disabled");
        }
    }
    var getData=function(){
        var n=$(":radio:checked").parents("tr").find("#icodeseries").val();
        var t=$(":radio:checked").parents("tr");
        var data={"pk_coderule":$(":radio:checked").parents("tr").find("#pk_coderule").val(),
            "icodeseries":n,"ilength1":"","itype1":"","vvalue1":"","ilength2":"","itype2":"",
            "vvalue2":"","ilength3":"","itype3":"","vvalue3":"","ilength4":"","itype4":"",
            "vvalue4":"","ilength5":"","itype5":"","vvalue5":"","ilength6":"","itype6":"",
            "vvalue6":"","ilength7":"","itype7":"","vvalue7":""}
        for(var i=1;n>=i;i++){
            var l=t.find("#ilength"+i).find("select").val();
            var tp=t.find("#itype"+i).find("select").val();
            data["ilength"+i]=l;
            data["itype"+i]=tp;
            if(!(tp == 3 || tp == 4)) {//如果不等于3和4
                var v = t.find("#vvalue" + i).find("input").val();
                if (length(l, v,i)) {
                    if (tp == 0) {
                        data["vvalue" + i] = v;
                    } else if (tp == 1) {
                        if (v.match("^[0-9]*$")) {
                            data["vvalue" + i] = v;
                        } else {
                            alerterror('<fmt:message key="the"/>' + i + '<fmt:message key="Level"/>,<fmt:message key="Integer_must_be_greater_than_zero"/>');
                            return
                        }
                    } else if (tp == 2) {
                        var reg = /^[A-Za-z]+$/;
                        if (reg.test(v)) {
                            data["vvalue" + i] = v;
                        } else {
                            alerterror('<fmt:message key="the"/>' + i + '<fmt:message key="Level"/>,<fmt:message key="Type_in_conformity_with_the_type_of_input"/>');
                            return;
                        }
                    }
                }else{
                    return;
                }
            }else{
                var v = t.find("#vvalue" + i).find("select").val();
                if(!(tp != 3 || l==v.length)) {
                    alerterror('<fmt:message key="the"/>'+i+'<fmt:message key="Level"/>,<fmt:message key="Length_do_not_tally_with_the_format_does_not_meet_the_length"/>');
                    return;
                }
                if(v==43){
                    if(5>l){
                        alerterror('<fmt:message key="the"/>'+i+'<fmt:message key="Level"/>,<fmt:message key="The_format_must_be_greater_than_the_water"/>5!');
                        return;
                    }
                }else if(v==41||v==42){
                    if(3>l){
                        alerterror('<fmt:message key="the"/>'+i+'<fmt:message key="Level"/>,<fmt:message key="The_format_must_be_greater_than_the_water"/>3!');
                        return;
                    }
                }
                data["vvalue"+i]=v;
            }
        }
        save(data);

    }
    var length=function(l,v,i){
        if($.trim(v)==""||$.trim(v).length==l){
            return true;
        }else{
            alerterror('<fmt:message key="the"/>'+i+'<fmt:message key="Level"/>,<fmt:message key="The_length_of_the_set_does_not_accord_with_the_length_of_the_input_box_values"/>');
            return false;
        }
    }
    var save=function(d){
        $.ajax({
            type : 'POST',
            //contentType : 'application/json;charset=UTF-8',
            url : '<%=path%>/coderule/update.do',
            data : d,
            dataType : 'json',
            success : function(message) {
                if(message=='error'){
                    alerterror('<fmt:message key="Modify_the_error_coding_rules"/>')
                }else {
                    $(":radio:checked").parents("tr").find("*").attr("disabled", "disabled");
                    $(":radio:checked").parents("tr").find("#vpreview").html(message);
                    $(":radio").removeAttr("disabled", "disabled");
                    parent.window.upButton();
                }
            }
        });
    }
</script>
</body>
</html>
