<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="commodity" /><fmt:message key="detailInfo" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<style type="text/css">
			.seasonMsg{
				
				width: 450px;
				height: 230px;
				positn: relative;
				z-index: 1000;
				background-color: white;
			}
			div[class]{
				display:block;
			}
		</style>
	</head>
	<body>
	<div id="sptable" class="table-body" style="text-align: center;">
		<form id="compForm">
			<div id="reseaonDiv" class="seasonMsg">
				<div style="float: left;white-space: normal;text-align: left;margin-left: 20px;margin-top: 10px;">请输入申请原因，以方便审批人员了解相关信息。</div>
				<div style="float: left;">
					<div align="left" style="float: left;margin-left: 20px;margin-top: 10px;"><span><fmt:message key="applyReason" />:</span></div>
					<div style="float: left;margin-left: 10px;margin-top: 10px;">
						<textarea id="reasonMsg" rows="5" cols="40" style="resize: none;"></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>	
	<script type="text/javascript">
		
		$(document).ready(function(){
			document.onkeydown=function(e){
                if(e.keyCode==27 ){//ESC 关闭弹出提示层
                	 $(".close",parent.document).click();
                }
            };
			$("#reasonMsg").focus();
		});
		
		var flag="0";
		//待审核物资申请原因放到父页面
		function applyToSave(){
			var reasonStr = $("#reasonMsg").val();
			if(reasonStr.replace(/[^\x00-\xff]/g,"**").length>300){
				alerterror("申请原因超长。");
				return;
			}
			$("#vapplydesc",$(parent.document)).val(reasonStr);
			parent.saveToRev();
			parent.$('.close').click();
		}
	</script>
</body>
</html>
