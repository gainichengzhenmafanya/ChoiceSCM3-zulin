<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select1" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
				.gridRight{
					width: 24%;
					float: left;
					height: 82%;
					border-left: 1px solid #999999;
				}
				.specDiv{
					height:30px;
					border-bottom: 1px solid #e2e2e2;
					border-right: 1px solid #e2e2e2;
					cursor: pointer;
				}
				.specDiv span{
					margin-left: 10px;
					line-height: 30px;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<form id="queryForm" name="queryForm" method="post" action="<%=path %>/deliver/supplyJmuList.do" style="height:100%;">
			<input type="hidden" id="type" name="type" value="0" />
			<div class="form-line">
				<div class="form-label" style="width:40px"><fmt:message key="name" />：</div>
				<div class="form-input">
					<input type="hidden" id="sp_code" name="sp_code" class="text" value="${supply.sp_code}"/>
					<input type="text" id="sp_name" name="sp_name" class="text" value="${supply.sp_name}"/>
					<input type="hidden" id="delivercode" name="delivercode" class="text" value="${delivercode}"/>
				</div>
				<input type="button" id="filterquery" value="<fmt:message key="query" />"/>
			</div>
			<div  style="width: 75%;float: left; height: 82%;">
				<div class="grid" >
					<div class="table-head">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 35px;"></span></td>
									<td style="width:30px;"><span style="width: 20px;"><input type="checkbox" id="chkAll" /></span>
									</td>
									<td style="display: none;"><span style="width:70px;"><fmt:message key="coding" /></span></td>
									<td><span style="width:200px;"><fmt:message key="name" /></span></td>
									<td><span style="width:70px;"><fmt:message key="unit_of_measure" /></span></td>
									<td><span style="width:60px;">商城价</span></td>
									<td><span style="width:170px;"><fmt:message key="suppliers" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div id="sptable" class="table-body" style="height:90%;">
						<table cellspacing="0" cellpadding="0" class="datagrid">
							<tbody>
								<c:forEach var="commodity" items="${commodityList}"	varStatus="status">
									<tr>
										<td class="num"><span style="width: 35px;">${status.index+1}</span>
										</td>
										<td style="width:30px; text-align: center;">
										<span style="width: 30px;">
											<input type="checkbox" name="idList" id="<c:out value='${commodity.id}' />"
											value="<c:out value='${commodity.id}' />" />
										</span>
										</td>
										<td style="display: none;"><span style="width:70px;">${commodity.SN}</span></td>
										<td>
											<span style="width:200px;" title="${commodity.name}">
												${commodity.name}
											</span>
										</td>
										<td>
											<span style="width:70px;text-align: left;"> ${commodity.unit }</span>
										</td>
										<td>
											<span style="width:60px;text-align: right;"> ${commodity.price }</span>
										</td>
										<td>
											<span style="width:170px;text-align: left;"> ${commodity.store.seller.companyName }</span>
										</td>
										<td style="display: none;">${commodity.store.seller.id }</td><!-- 供应商主键 -->
										<td style="display: none;">${commodity.store.id }</td><!-- 商铺主键 -->
										<td style="display: none;">${commodity.store.name }</td><!-- 商铺名称 -->
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="gridRight">
				<div class="grid" >
					<div class="table-head">
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td><span style="width:100%;">规格列表</span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div id="specListDiv"></div>	
				</div>		
			</div>
				<page:page form="queryForm" page="${pageobj}"></page:page>
				<input type="hidden" name="nowPage" id="nowPage" value="${pageobj.nowPage }" /> 
				<input type="hidden" name="pageSize" id="pageSize" value="${pageobj.pageSize }" />
			</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			loadGrid();//  自动计算滚动条的js方法
			$("#filterquery").click(function(){
				$("#type").val("1");
				$("#queryForm").submit();
			});
// 			$("#toolbar").toolbar({
// 					items: [{
// 						text: '<fmt:message key="enter" />',
// 						useable:true,
// 						handler: function(){
// 							var flag="1";
// 							var checkboxList = $('.grid').find('.table-body').find(':checkbox');
// 							var data = {show:[],code:[],mod:[],entity:[]};
// 							if(checkboxList.filter(':checked').length<=0){
//                                 alerterror('请选择一条商品!');
// 								return;
// 							}
// 							checkboxList.filter(':checked').each(function(){
// 								var valid = $(".gridRight").find("#specListDiv").find('.specListDivSelected').find("input[name='id']").val();
// 								var valprice= $(".gridRight").find("#specListDiv").find('.specListDivSelected').find("input[name='price']").val();
// 								if($.trim(valid)==null || $.trim(valid)==''){
// 									flag="0";
// 									return false;
// 								}
// 								var entity = {};
// 								var row = $(this).closest('tr');
// 								data.code.push($(this).val());
// 								data.show.push($.trim(row.children('td:eq(3)').text()));
// 								entity.id = valid;
// 								entity.sn = $.trim(row.children('td:eq(2)').text());
// 								entity.goodsname = $.trim(row.children('td:eq(3)').text());
// 								entity.price = valprice;
// 								entity.sellerid = $.trim(row.children('td:eq(7)').text());
// 								entity.sellername = $.trim(row.children('td:eq(6)').text());
// 								entity.storeid = $.trim(row.children('td:eq(8)').text());
// 								entity.storename = $.trim(row.children('td:eq(9)').text());
// 								entity.pk_id = $.trim($(this).val());
// 								data.entity.push(entity);
// 							});
// 							if(flag=='0'){
//                                 alerterror("有未选择规格的商品，请选择规格！");
// 								return;
// 							}
// 							parent['${callBack}'](data);
// 							$(".close",parent.document).click();
// 						}
// 					},{
// 						text: '<fmt:message key="cancel" />',
// 						useable: true,
// 						handler: function(){
// 							$(".close",parent.document).click();
// 						}
// 					}
// 				]
// 			});
			$('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
			//当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
			var mod = '<c:out value="${single}"/>';
			if(mod){
				$('#chkAll').unbind('click');
				$('#chkAll').css('display','none');
			}else{
				$("#chkAll").click(function(){
					if($(this)[0].checked){
						$('.grid').find('.table-body').find(':checkbox').attr("checked","checked");
					}else{
						$('.grid').find('.table-body').find(':checkbox').removeAttr("checked");
					}
				});
			}
			//点击行事件
			var savelock = false;
			$('.grid').find('.table-body').find('tr').live("click", function () {
				if(savelock){
					return;
				}else{
					savelock = true;
				}
	  	        $('.grid').find('.table-body').find('tr').removeClass("tr-over");
	  	        if($(this).hasClass("tr-over")) {
	  	            $(this).removeClass("tr-over");
	  	        }else{
	  	            $(this).addClass("tr-over");
	  	        }
				$(this).find(':checkbox').attr('checked','checked');
				 $('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				var $tmp=$('[name=idList]:checkbox');
				//用filter方法筛选出选中的复选框。并直接给chkAll赋值。
				$('#chkAll').attr('checked',$tmp.length==$tmp.filter(':checked').length);
				$.ajaxSetup({ 
					  async: false 
				});
				$.post("<%=path %>/deliver/toMallMaterialDetial.do",
						{vcode:$.trim($(this).find(':checkbox').val())},
						function(data){
							var divD = "";
							$("#specListDiv").empty();
							if(data.spec){
								for(var i in data.spec){
									var title = "";
									if(data.spec[i].product=='')
										title='无规格';
									else{
										title=data.spec[i].product;
									}
									divD += '<div class="specDiv"><span>'+title+
										'</span><input name="id" type="hidden" value="'+data.spec[i].id+'"/>'+
										'<input name="price" type="hidden" value="'+data.spec[i].price+'"/></div>';
								}
							}
							$("#specListDiv").append(divD);
							if($(".specDiv").length==1){
								if($.trim($(".specDiv").text())=='无规格'){
									$(".specDiv").click();
// 									$(".specDiv").hide();
								}else{
									$(".specDiv").click();
								}
							}else{
								$(".specDiv")[0].click();
							}

				  			savelock = false;
						});
			 });
			//判断是否需要单选
			$('.grid').find('.table-body').find('tr').find(':checkbox').live('click',function(event){
				var mod = '<c:out value="${single}"/>';
				if(mod){
					$(this).closest('.table-body').find(':checkbox').not($(this)).removeAttr("checked");
				}
				$(this).closest("tr").click();
				event.stopPropagation();
			});
			
			$("#queryForm").css("height",$("#queryForm").height()-25);
			//重新计算规格列表的宽度
			$(".gridRight .grid").css("width","100%");
			$(".gridRight .table-head").css("width","100%");
			$(".gridRight .table-head table").css("width","100%");
			//判断是否是自动查询
			if("${type}"!="1"){
	  			$("#type").val("1");
				$("#queryForm").submit();
			}
			$(".grid").css("overflow-x","auto");
			$(".grid").css("overflow-y","hidden");
		});
		$('#specListDiv').find(".specDiv").live("click",function(){
			$(this).css("background-color","gainsboro");
			$(this).addClass("specListDivSelected");
			$('#specListDiv').find(".specDiv").not(this).css("background-color","white");
			$('#specListDiv').find(".specDiv").not(this).removeClass("specListDivSelected");
		});
		
		function saveJmuSupply(){
			var flag="1";
			var checkboxList = $('.grid').find('.table-body').find(':checkbox');
			var selectdata = {show:[],code:[],mod:[],entity:[]};
			if(checkboxList.filter(':checked').length!=1){
               alerterror('请选择一条商品!');
				return;
			}
			checkboxList.filter(':checked').each(function(){
				var valid = $(".gridRight").find("#specListDiv").find('.specListDivSelected').find("input[name='id']").val();
				var valprice= $(".gridRight").find("#specListDiv").find('.specListDivSelected').find("input[name='price']").val();
				if($.trim(valid)==null || $.trim(valid)==''){
					flag="0";
					return false;
				}
				var entity = {};
				var row = $(this).closest('tr');
				selectdata.code.push($(this).val());
				selectdata.show.push($.trim(row.children('td:eq(3)').text()));
				entity.id = valid;
				entity.sn = $.trim(row.children('td:eq(2)').text());
				entity.goodsname = $.trim(row.children('td:eq(3)').text());
				entity.price = valprice;
				entity.sellerid = $.trim(row.children('td:eq(7)').text());
				entity.sellername = $.trim(row.children('td:eq(6)').text());
				entity.storeid = $.trim(row.children('td:eq(8)').text());
				entity.storename = $.trim(row.children('td:eq(9)').text());
				entity.pk_id = $.trim($(this).val());
				selectdata.entity.push(entity);
			});
			if(flag=='0'){
               alerterror("有未选择规格的商品，请选择规格！");
				return;
			}
			var data = {};
			data['sp_code'] = $('#sp_code').val();
			data['pk_materialjmu'] = selectdata.entity[0].id;
			data['vcodejmu'] = selectdata.entity[0].id;
			data['vnamejmu'] = selectdata.entity[0].goodsname;
			data['vhspec'] = selectdata.entity[0].sn;
			data['delivercode'] = $('#delivercode').val();
			$.post("<%=path%>/deliver/updateJmuSupply.do?",data,function(data){
				var rs = data;
                if(isNaN(rs)){
                    alert('关联失败！');
                    return;
                }
				switch(Number(rs)){
				case -1:
					alert('关联失败！');
					break;
				case 1:
					showMessage({
						type: 'success',
						msg: '关联成功！！',
						speed: 3000,
						handler:function(){
							parent.pageReload();
							parent.$('.close').click();
						}
					});
					break;
				}
			});
		}
		
		
	</script>
	</body>
</html>