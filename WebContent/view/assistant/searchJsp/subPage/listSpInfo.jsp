<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="commodity" /><fmt:message key="detailInfo" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	
		<style type="text/css">
			body{
/* 				white-space: normal; */
			}
			.textClass{
				word-break:break-all;
				overflow:hidden; 
				width: 350px;
				margin:5px 0px 0px 25px;
			}
			.specDiv{
				cursor: pointer;
				float: left;
				margin: 1px 5px 3px 0px;
				border: 1px solid blue;
				padding: 4px 6px;
			}
			.form-label{
				 float: left;
				 width: 80px;
				 text-align: right;
			}
			.form-input{
				 width: 270px;
				 float: left;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<div id="sptable" class="table-body" style="text-align: center;">
		<form id="compForm">
			<input type="hidden" name="pk_material" id="pk_material" value="${commodity.id}" />
			<input type="hidden" name="vcode" id="vcode" value="${commodity.id}" />
			<input type="hidden" name="vname" id="vname" value="${commodity.name}" />
			<input type="hidden" name="nleast" id="nleast" value="${commodity.least}" />
			<input type="hidden" name=defPrice id="defPrice" value="${commodity.defPrice}" />
			<input type="hidden" name=nprice id="nprice" value="" />
			<input type="hidden" name=vspecfication id="vspecfication" value="" />
			<input type="hidden" name=vhspec id="vhspec" value="" />
			<input type="hidden" name=vhstore id="vhstore" value="${commodity.store.id}" />
			<input type="hidden" name=vjmustorename id="vjmustorename" value="${commodity.store.name}" />
			<input type="hidden" name=vjmusupplierpk id="vjmusupplierpk" value="${commodity.store.seller.id}" />
			<input type="hidden" name=delivercode id="delivercode" value="${commodity.store.seller.id}" />
			<input type="hidden" name=delivername id="delivername" value="${commodity.store.seller.companyName}" />
			<input type="hidden" name=vapplydesc id="vapplydesc" value="" />
			<input type="hidden" name="vunit" id="vunit" value="${commodity.unit}" />
			<div style="margin: 10px auto;width: 400px;float: left;text-align: left;">
				<div title="${commodity.name}" class="textClass" >
					<div class="form-label"><fmt:message key="name"/>：</div>
<%-- 					<div class="form-input"><span style="width: 270px;" title="${commodity.name}">${commodity.name}</span></div> --%>
						<span class="form-input" style="width: 270px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.name}">${commodity.name}</span>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="price"/>：</div>
					<div class="form-input">
						<span id="price" style="width:150px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.minPrice}">${commodity.minPrice}</span>
					</div>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="brands"/>：</div>
					<div class="form-input">
						<span style="width:50px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.brand}">${commodity.brand} </span>
					</div>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="scscguige"/>：</div>
					<div class="form-input">
						<c:if test="${not empty commodity.spec}">
							<c:forEach items="${commodity.spec}" var="spec" varStatus="status">
								<c:choose>
									<c:when test="${spec.product=='[]' }">
										<div class="specDiv" onclick="changePri('${spec.id}','','${spec.price}','${spec.price}')"><fmt:message key="nosoec"/></div>
									</c:when>
									<c:otherwise>
										<div class="specDiv" onclick="changePri('${spec.id}','${spec.product}','${spec.price}')">${spec.product}</div>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</div>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="unit"/>：</div>
					<div class="form-input">
						<span style="width:150px;">${commodity.unit}</span>
					</div>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="startbuy"/>：</div>
					<div class="form-input">
						<span style="width:150px;">${commodity.least}</span>
					</div>
				</div>
				<div class="textClass" >
					<div class="form-label"><fmt:message key="totalsale"/>：</div>
					<div class="form-input">
						<span style="width:150px;">${commodity.volume}</span>
					</div>
				</div>
				<div class="textClass"><hr></hr></div>
				<div  class="textClass">
					<div class="form-label">店铺名称：</div>
					<div class="form-input" style="width: 270px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.store.name}">${commodity.store.name}</div>
				</div>
				<div  class="textClass">
					<div class="form-label"><fmt:message key="suppliers_name" />：</div>
<%-- 					<div class="form-input" style="width: 270px;"><span style="width: 260px;" title="${commodity.store.seller.companyName}"></span>${commodity.store.seller.companyName}</div> --%>
						<div class="form-input" style="width: 270px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.store.seller.companyName}">${commodity.store.seller.companyName}</div>
				</div>
				<div  class="textClass">
					<div class="form-label"><fmt:message key="level" />：</div>
					<div class="form-input" style="width: 270px;">
						<c:if test="${commodity.store.seller.grade==1}">子供应链</c:if> 
						<c:if test="${commodity.store.seller.grade==2}">品牌供应商</c:if>
						<c:if test="${commodity.store.seller.grade==3}">普通供应商</c:if> 
					</div>
				</div>
				<div  class="textClass">
					<div class="form-label"><fmt:message key="address" />：</div>
<%-- 					<div class="form-input" style="width: 270px;"><span style="width: 260px;" title="${commodity.store.seller.companyAddress}"></span>${commodity.store.seller.companyAddress}</div> --%>
					<div class="form-input" style="width: 270px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.store.seller.companyAddress}">${commodity.store.seller.companyAddress}</div>
				</div>
				<div  class="textClass">
					<div class="form-label"><fmt:message key="phone" />：</div>
<%-- 					<div class="form-input" style="width: 270px;"><span style="width: 260px;" title="${commodity.store.seller.companyPhone}"></span>${commodity.store.seller.companyPhone}</div> --%>
					<div class="form-input" style="width: 270px;overflow:hidden;text-overflow:ellipsis;" title="${commodity.store.seller.companyPhone}">${commodity.store.seller.companyPhone}</div>
				</div>
			</div>
			<div style="float: left;">
				<img src="${commodity.image[0].image}" style="width: 240px;height: 240px;" />
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/validate/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/validate/bohvalidate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-${sessionScope.locale}.js"></script>		
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
	<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function(){
			//判断错误提示 如果错误提示不为空，弹出提示信息并关闭本页面
			if("${errormsg}"!=""){
				alerterror("${errormsg}");
				parent.$(".close").click();
			}
			if($(".specDiv").length==1){
				$(".specDiv").click();
				if($.trim($(".specDiv").text())=='<fmt:message key="nosoec"/>'){
					$(".specDiv").hide();
				}
			}else{
				$(".specDiv")[0].click();
			}
			
			
			$('.tool').toolbar({
				items: [{
					text: '<fmt:message key="applyJoinMaterial" />',
					title: '<fmt:message key="applyJoinMaterial" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						applyToMaterial();
					}
				},{
					text: '<fmt:message key="applyJoinFav" />',
					title: '<fmt:message key="applyJoinFav" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						applyToFavorite();
					}
				},{
					text: '<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />',
					title: '<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />',
					useable: true,
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						deleteFromFavorite();
					}
				},{
					text: '<fmt:message key="cancel" />',
					title: '<fmt:message key="cancel" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			})
			
			var flag = '${flag}';
			if(flag=="1"){
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").show();
					}
				});
			}else if(flag=="0"){
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").show();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
				});
			}else{
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
				});
			}
		});
		
		$("#msgShow",$(parent.document)).html('<fmt:message key="being" /><fmt:message key="save" />...');
		var flag="0";
		//加入到待审核物资
		function applyToMaterial(){
			if($("#vhspec").val()==null || $("#vhspec").val()==""){
				alerterror('<fmt:message key="pleaseselectspec"/>！');
				return;
			}
			var data={};
			data = getParam($("#compForm"));
			$.ajaxSetup({
				async:false
			});
			//如果目标窗口已经存在
			if($('#window_ApplySpInfo').html()){
				return;
			}
			$.post("<%=path%>/search/findReviMaterialByPk.do",data,function(data){
				if(data!="1"){
					alerterror('<fmt:message key="materialisaleadyexits"/>！');
					return;
				}else{
// 					$("#wait2",$(parent.document)).css("display","block");
					$('body').window({
						id: 'window_ApplySpInfo',
						title: '<fmt:message key="applyReason" />',
						content: '<iframe id="applySpInfoFrame" frameborder="0" src="<%=path%>/search/toApplySpInfo.do"></iframe>',
						width: '450px',
						height: '260px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('applySpInfoFrame')){
											window.document.getElementById("applySpInfoFrame").contentWindow.applyToSave();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}
			});
		}
		//申请原因按钮点击事件
		function saveToRev(){
			$("#msgShowDiv",$(parent.document)).css("display","none");
			$("#wait",$(parent.document)).css("display","block");
			var data={};
			data = getParam($("#compForm"));
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/search/saveReviMaterial.do",data,function(data){
				var rs = data;
				switch(Number(rs)){
				case -1:
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 1:
					alerttips('<fmt:message key="save_successful"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 2:
					alerterror('<fmt:message key="materialisaleadyexits"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				}
			});
		}
		//从收藏夹中删除物资
		function deleteFromFavorite(){
			$("#wait2",$(parent.document)).css("display","block");
			$("#wait",$(parent.document)).css("display","block");
			$("#wait2",$(parent.document)).css("z-index","125");
			$("#wait",$(parent.document)).css("z-index","125");
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/favorites/deletesp.do",{"pk_mallmaterial":$("#pk_material").val()},function(data){
				if(data=="T"){
					alerttips('取消成功！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					$(".tool").find(".button-main").each(function(){
						if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
							$(this).closest("li").show();
						}
						if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
							$(this).closest("li").hide();
						}
					});
				}else{
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
				}
			});
		}
		//加入到收藏夹
		function applyToFavorite(){
			$("#wait2",$(parent.document)).css("display","block");
			$("#wait",$(parent.document)).css("display","block");
			$("#wait2",$(parent.document)).css("z-index","125");
			$("#wait",$(parent.document)).css("z-index","125");
			var data={};
			data["pk_mallmaterial"]=$("#pk_material").val();
			data["vmaterialcode"]=$("#vcode").val();
			data["vmaterialname"]=$("#vname").val();
			data["vmaterialspec"]=$("#vspecfication").val();
			data["nmatprice"]=$("#defPrice").val();
			data["nmallprice"]=$("#nprice").val();
			data["vmallsupplier"]='${commodity.store.seller.companyName}';
			data["namount"]=$("#least").val();
			data["vunit"]='${commodity.unit}';
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/search/saveFavorite.do",data,function(data){
				var rs = data;
				switch(Number(rs)){
				case -1:
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 1:
					alerttips('<fmt:message key="save_successful"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					$(".tool").find(".button-main").each(function(){
						if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
							$(this).closest("li").hide();
						}
						if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
							$(this).closest("li").show();
						}
					});
					break;
				}
			});	
		}
		//点击规格修改价格事件
		function changePri(vhspec,name,price){
			debugger;
			$("#vhspec").val(vhspec);
			$("#vspecfication").val(name);
			$("#nprice").val(price);
			$("#price").text(price);
		}
		//规格div点击事件
		$(".specDiv").live("click",function(){
			$(this).css("background-color","grey");
			$(".specDiv").not(this).css("background-color","white");
		});
	</script>
</body>
</html>
