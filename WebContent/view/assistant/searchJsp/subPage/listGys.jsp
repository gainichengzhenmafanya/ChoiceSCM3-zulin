<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="gyshead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<td class="num"><span style="width: 20px;"></span></td>
				<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
				<td><span style="width:150px;"><fmt:message key="name" /></span></td>
				<td><span style="width:70px;">等级</span></td>
				<td><span style="width:110px;"><fmt:message key="phone" /></span></td>
				<td><span style="width:150px;">经营范围</span></td>
				<td><span style="width:150px;">地区</span></td>
				<td><span style="width:150px;">配送范围</span></td>
				<td><span style="width:200px;"><fmt:message key="address" /></span></td>
			</tr>
		</thead>
	</table>
</div>
<div id="gystable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="seller" items="${sellerList}"
				varStatus="status">
				<tr>
					<td class="num"><span style="width: 20px;">${status.index+1}</span>
					</td>
					<td><span style="width:70px;">${seller.id}</span></td>
					<td>
						<span style="width:150px;color: blue;" onclick="openGysInfo('${seller.id}')" title="${seller.company.companyName}">
							${seller.company.companyName}
						</span>
					</td>
					<td>
						<span style="width:70px;"> 
							<c:if test="${seller.grade==1}">子供应链</c:if> 
							<c:if test="${seller.grade==2}">品牌供应商</c:if>
							<c:if test="${seller.grade==3}">普通供应商</c:if> 
						</span>
					</td>
					<td><span style="width:110px;">${seller.company.companyPhone}</span></td>
					<td><span style="width:150px;"></span></td>
					<td><span style="width:150px;">${seller.company.companyArea}</span></td>
					<td><span style="width:150px;"></span></td>
					<td><span style="width:200px;" title="${seller.company.companyAddress}">${seller.company.companyAddress}</span></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<span>
	<page:page form="queryFrom" page="${pageobj}"></page:page>
</span>
<script>
	$(".page").css("bottom","3px");
	if("${errmsg}"!=''){
        alerterror("${errmsg}");
	}
</script>
