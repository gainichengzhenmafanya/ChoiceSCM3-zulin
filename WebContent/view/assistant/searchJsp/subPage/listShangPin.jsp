<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<div id="sphead" class="table-head">
	<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
	<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />
	<table cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<td class="num"><span style="width: 20px;"></span></td>
				<td><span style="width:70px;"><fmt:message key="coding" /></span></td>
				<td><span style="width:220px;"><fmt:message key="name" /></span></td>
				<td><span style="width:70px;"><fmt:message key="unit_of_measure" /></span></td>
				<td><span style="width:60px;"><fmt:message key="scscshichangjia" /></span></td>
				<td><span style="width:60px;"><fmt:message key="scscshangchengjia" /></span></td>
				<td><span style="width:50px;"><fmt:message key="scscqidingliang" /></span></td>
				<td><span style="width:170px;"><fmt:message key="suppliers" /></span></td>
			</tr>
		</thead>
	</table>
</div>
<div id="sptable" class="table-body">
	<table cellspacing="0" cellpadding="0" class="datagrid">
		<tbody>
			<c:forEach var="commodity" items="${commodityList}"	varStatus="status">
				<tr>
					<td class="num"><span style="width: 20px;">${status.index+1}</span>
					</td>
					<td><span style="width:70px;">${commodity.SN}</span></td>
					<td>
						<c:if test="${not empty commodity.image}">
							<div style="float: left;display: block;width: 17px;height: 15px;margin:5px 0px 0px 3px;" class="hoverDiv">
								<img alt="image" src="<%=path%>/image/img_icon.png">
								<input type="hidden" id="imageUrl" value="${commodity.image }" />
							</div>
						</c:if>
						<span style="width:200px;color: blue;float: left;" onclick="openSpInfo('${commodity.id}')" title="${commodity.name}">
							${commodity.name}
						</span>
					</td>
					<td>
						<span style="width:70px;text-align: left;"> ${commodity.unit }</span>
					</td>
					<td>
						<span style="width:60px;text-align: right;"> ${commodity.defPrice }</span>
					</td>
					<td>
						<span style="width:60px;text-align: right;"> ${commodity.price }</span>
					</td>
					<td>
						<span style="width:50px;text-align: right;"> ${commodity.least }</span>
					</td>
					<td>
						<span style="width:170px;text-align: left;" title="${commodity.store.seller.companyName }"> ${commodity.store.seller.companyName }</span>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<page:page form="queryFrom" page="${pageobj}"></page:page>
<script>
	$(".page").css("bottom","3px");
if("${errmsg}"!=''){
    alerterror("${errmsg}");
}
	var x = 22;  
	var y = 20;  
	var MouseEvent = function(e) {
	    this.x = e.pageX;
	    this.y = e.pageY;
	};
	var Mouse = function(e){
	      var kdheight =  $(document).scrollTop();
	        mouse = new MouseEvent(e);
	        leftpos = mouse.x+5;
	        toppos = mouse.y-5;
	};
	$('.hoverDiv').hover(
		function(e){
			Mouse(e);
			$("#spImage").attr("src",$(this).find("input").val());
		    $(".showinfo").css({top:toppos,left:leftpos}).fadeIn(100);
		    widthJudge(e);
		},
		function(){
			$(".showinfo").hide();
		}
	);
	//重新定位图片坐标Y坐标，防止图片溢出浏览器
	function widthJudge(e){  
        var marginHeight = document.documentElement.clientHeight - e.pageY;  
        var imageHeight = $(".showinfo").height(); 
        if(marginHeight < (imageHeight+45)){  
            $(".showinfo").css({top:(e.pageY - (imageHeight-marginHeight)-70 ) + 'px'});  
        }  
    }  
</script>