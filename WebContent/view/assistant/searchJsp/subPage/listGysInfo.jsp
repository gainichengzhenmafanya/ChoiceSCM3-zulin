<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="select" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
		<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
	    <link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />	
		<style type="text/css">
			body{
/* 				white-space: normal; */
			}
			.textClass{
				word-break:break-all;
				overflow:hidden; 
				width: 350px;
				margin:5px 0px 0px 25px;
			}
			.form-label{
				 float: left;
				 width: 80px;
				 text-align: right;
			}
			.form-input{
				 width: 270px;
				 float: left;
			}
		</style>
	</head>
	<body>
	<div class="tool"></div>
	<form id="compForm">
		<input type="hidden" name="delivercode" id="delivercode" value="${seller.id}" />
		<input type="hidden" name="vcode" id="vcode" value="${seller.id}" />
		<input type="hidden" name="vname" id="vname" value="${seller.company.companyName}" />
		<input type="hidden" name="vlevel" id="vlevel" value="${seller.grade}" />
		<input type="hidden" name="vsuppliertele" id="vsuppliertele" value="${seller.company.companyPhone}" />
		<input type="hidden" name="vsupplieraddress" id="vsupplieraddress" value="${seller.company.companyAddress}" />
		<input type="hidden" name="voperatingrange" id="voperatingrange" value="" />
		<input type="hidden" name="vdeliveryarea" id="vdeliveryarea" value="${seller.company.companyArea}" />
		<input type="hidden" name="vcontact" id="vcontact" value="${seller.company.companyContact}" />
		<input type="hidden" name="vcompanyfax" id="vcompanyfax" value="${seller.company.companyFax}" />
		<input type="hidden" name="vcompanyurl" id="vcompanyurl" value="${seller.company.companyUrl}" />
		<input type="hidden" name="vcompanyemail" id="vcompanyemail" value="${seller.company.companyEmail}" />
		<input type="hidden" name=vapplydesc id="vapplydesc" value="" />
		<div style="margin: 10px auto;width: 400px;float: left;text-align: left; ">
			<div class="textClass" >
				<div class="form-label"><fmt:message key="scscgysmc" />：</div>
				<div class="form-input">
					<span style="width:150px;overflow:hidden;text-overflow:ellipsis;" title="${seller.company.companyName}">${seller.company.companyName}</span>
				</div>
			</div>
			<div class="textClass" >
				<div class="form-label"><fmt:message key="level" />：</div>
				<div class="form-input">
					<c:if test="${seller.grade==1}">子供应链</c:if> 
					<c:if test="${seller.grade==2}">品牌供应商</c:if>
					<c:if test="${seller.grade==3}">普通供应商</c:if> 
				</div>
			</div>
			<div class="textClass" >
				<div class="form-label"><fmt:message key="phone" />：</div>
				<div class="form-input">
					<span style="width:50px;" >${seller.company.companyPhone} </span>
				</div>
			</div>
			<div class="textClass" >
				<div class="form-label"><fmt:message key="BusinessScope" />：</div>
				<div class="form-input"><span style="width:150px;"></span></div>
			</div>
			<div class="textClass" >
				<div class="form-label"><fmt:message key="region" />：</div>
				<div class="form-input">
					<span style="width:150px;overflow:hidden;text-overflow:ellipsis;" title="${seller.company.companyArea}">${seller.company.companyArea}</span>
				</div>
			</div>
			<div class="textClass" >
				<div class="form-label"><fmt:message key="address" />：</div>
				<div class="form-input">
					<span style="width:150px;overflow:hidden;text-overflow:ellipsis;" title="${seller.company.companyAddress}">${seller.company.companyAddress}</span>
				</div>
			</div>
		</div>
		<div style="float: left;margin-top: 10px;">
				<div><input type="hidden"  id="yyzz"  value="${seller.qualify[0].image}"/>
				<img id="yyzzimg" src="<%=path%>/image/kucun/0.png" alt="营业执照" />
					营业执照
				</div>
				<div><input type="hidden"  id="zzjg"  value="${not empty seller.qualify[1].image}"/>
				<img id="zzjgimg" src="<%=path%>/image/kucun/0.png" alt="组织机构代码证" />
					组织机构代码证
				</div>
				<div><input type="hidden"  id="swdj"  value="${not empty seller.qualify[2].image}"/>
				<img id="swdjimg" src="<%=path%>/image/kucun/0.png" alt="税务登记许可证" />
					税务登记许可证
				</div>
				<div><input type="hidden"  id="sfz"  value="${not empty seller.qualify[3].image}"/>
				<img id="sfzimg" src="<%=path%>/image/kucun/0.png" alt="身份证" />
					身份证
				</div>
		</div>
	</form>
	<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/validate/validate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/boh/validate/bohvalidate.js"></script>
	<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
	<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
	<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-${sessionScope.locale}.js"></script>		
	<script type="text/javascript" src="<%=path%>/js/util.js"></script>	
	<script type="text/javascript" src="<%=path%>/js/boh/autoTable.js"></script>
 	<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>	
	<script type="text/javascript">
		$(document).ready(function(){
			document.onkeydown=function(e){
                if(e.keyCode==27 ){//ESC 关闭弹出提示层
                	 $(".close",parent.document).click();
                }
            };
            if($("#yyzz").val()!=""){
            	$("#yyzzimg").attr("src","<%=path%>/image/kucun/1.png");
            }
            if($("#zzjg").val()!=""){
            	$("#zzjgimg").attr("src","<%=path%>/image/kucun/1.png");
            }
            if($("#swdj").val()!=""){
            	$("#swdjimg").attr("src","<%=path%>/image/kucun/1.png");
            }
            if($("#sfz").val()!=""){
            	$("#sfzimg").attr("src","<%=path%>/image/kucun/1.png");
            }
            
            $('.tool').toolbar({
				items: [{
					text: '<fmt:message key="applyJoinSupplier" />',
					title: '<fmt:message key="applyJoinSupplier" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						applyToSupply();
					}
				},{
					text: '<fmt:message key="applyJoinFav" />',
					title: '<fmt:message key="applyJoinFav" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						applyToFavorite();
					}
				},{
					text: '<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />',
					title: '<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />',
					useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-80px','-0px']
					},
					handler: function(){
						deleteFromFavorite();
					}
				},{
					text: '<fmt:message key="cancel" />',
					title: '<fmt:message key="cancel" />',
					icon: {
						url: '<%=path%>/image/Button/op_owner.gif',
						positn: ['-160px','-100px']
					},
					handler: function(){
						parent.$('.close').click();
					}
				}]
			})
			
			var flag = '${flag}';
			if(flag=="1"){
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").show();
					}
				});
			}else if(flag=="0"){
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").show();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
				});
			}else{
				$(".tool").find(".button-main").each(function(){
					if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
					if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
						$(this).closest("li").hide();
					}
				});
			}
            
		});
		$("#msgShow",$(parent.document)).html('<fmt:message key="being" /><fmt:message key="save" />...');
		//加入到待审核供应商
		function applyToSupply(){
			var data={};
			data = getParam($("#compForm"));
			$.ajaxSetup({
				async:false
			});
			//如果目标窗口已经存在
			if($('#window_GYSInfo').html()){
				return;
			}
			$.post("<%=path%>/search/findAuditMaterialByPk.do",data,function(data){
				if(data!="1"){
					alerterror('<fmt:message key="supplierisaleadyexits"/>！');
					return;
				}else{
// 					$("#wait2",$(parent.document)).css("display","block");
					$('body').window({
						id: 'window_GYSInfo',
						title: '<fmt:message key="applyReason" />',
						content: '<iframe id="applyGYSInfoInfoFrame" frameborder="0" src="<%=path%>/search/toApplySpInfo.do"></iframe>',
						width: '450px',
						height: '260px',
						draggable: true,
						isModal: true,
						confirmClose:false,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									title: '<fmt:message key="save" />',
									useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('applyGYSInfoInfoFrame')){
											window.document.getElementById("applyGYSInfoInfoFrame").contentWindow.applyToSave();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									title: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
					$("#window_GYSInfo").css("z-index","9999");
				}
			});
		}
		//申请原因按钮点击事件
		function saveToRev(){
			$("#wait",$(parent.document)).css("display","block");
			var data={};
			data = getParam($("#compForm"));
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/search/addInAuditMaterial.do",data,function(data){
				var rs = data;
				switch(Number(rs)){
				case -1:
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 1:
					alerttips('<fmt:message key="save_successful"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 2:
					alerterror('<fmt:message key="supplierisaleadyexits"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				}
			});	
		}
		//从收藏夹中删除供应商
		function deleteFromFavorite(){
			$("#wait2",$(parent.document)).css("display","block");
			$("#wait",$(parent.document)).css("display","block");
			$("#wait2",$(parent.document)).css("z-index","125");
			$("#wait",$(parent.document)).css("z-index","125");
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/favorites/deletegys.do",{"pk_deliver":$("#delivercode").val()},function(data){
				if(data=="T"){
					alerttips('取消成功！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					$(".tool").find(".button-main").each(function(){
						if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
							$(this).closest("li").show();
						}
						if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
							$(this).closest("li").hide();
						}
					});
				}else{
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
				}
			});	
		}
		//加入到收藏夹
		function applyToFavorite(){
			$("#wait2",$(parent.document)).css("display","block");
			$("#wait",$(parent.document)).css("display","block");
			$("#wait2",$(parent.document)).css("z-index","125");
			$("#wait",$(parent.document)).css("z-index","125");
			var data={};
			data["pk_deliver"]=$("#delivercode").val();
			data["delivercode"]=$("#vcode").val();
			data["delivername"]=$("#vname").val();
			data["vdeliverlevel"]=$("#vlevel").val()=="0"?"":$("#vlevel").val();
			data["vdeliverphone"]=$("#vsuppliertele").val();
			data["vdeliveraddress"]=$("#vsupplieraddress").val();
			data["vdeliveryarea"]=$("#vdeliveryarea").val();
			$.ajaxSetup({
				async:false
			});
			$.post("<%=path%>/search/saveGysFavorite.do",data,function(data){
				var rs = data;
				switch(Number(rs)){
				case -1:
					alerterror('<fmt:message key="save_fail"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					break;
				case 1:
					alerttips('<fmt:message key="save_successful"/>！');
					$("#wait2",$(parent.document)).css("display","none");
					$("#wait",$(parent.document)).css("display","none");
					$(".tool").find(".button-main").each(function(){
						if($(this).attr("title")=='<fmt:message key="applyJoinFav" />'){
							$(this).closest("li").hide();
						}
						if($(this).attr("title")=='<fmt:message key="cancel" /><fmt:message key="applyJoinFav" />'){
							$(this).closest("li").show();
						}
					});
					break;
				}
			});	
		}
		
		
	</script>
</body>
</html>
