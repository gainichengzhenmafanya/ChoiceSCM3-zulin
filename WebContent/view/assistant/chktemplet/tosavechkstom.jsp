<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="sta" name="sta" value="add"/>
		<form action="<%=path%>/chksto/tosavechkstombytemplet.do" id="listForm" name="listForm" method="post">
			<input type="hidden" name="pk_chktemplet" value="${pk_chktemplet}" />
			<div class="bj_head">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span>单据号:</div>
					<div class="form-input">
						<input type="text" name="vbillno" id="vbillno" class="text" readonly="readonly" value="${chkstom.vbillno}"/>
					</div>
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="document_date" />:</div>
					<div class="form-input" >
						<input autocomplete="off" type="text" id="dmakedate" name="dmakedate" value="${chkstom.dmakedate}" class="Wdate text"/>
					</div>
				</div>
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="org" />:</div>
					<div class="form-input" >
						<input type="hidden" name="positncode" id="positncode"  value="${chkstom.positncode}" class="text"/>
						<input type="hidden" name="positncode" id="positncode"  value="${chkstom.positncode}" class="text"/>
						<input type="text" name="positnname" id="positnname"  value="${chkstom.positnname}" style="margin-bottom: 6px;" class="text" readonly="readonly"/>
						<img id="positncodebutton" style="margin-bottom: 6px;" class="search" src="<%=path%>/image/themes/icons/search.png" alt='<fmt:message key="query_suppliers"/>' />
					</div>
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="Hope_date" />:</div>
					<div class="form-input">
						<input autocomplete="off" type="text" id="dhopedate" name="dhopedate"  value="${chkstom.dhopedate}" class="Wdate text" />
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="remark" />:</div>
					<div class="form-input">
						<input type="text" style="width: 400px;" name="vmemo" id="vmemo" class="text" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
		                            <td><span style="width:100px;"><fmt:message key="shouyewuzibm" /></span></td>
									<td><span style="width:140px;"><fmt:message key="shouyewuzimc" /></span></td>
									<td><span style="width:90px;"><fmt:message key="specification" /></span></td>
									<td><span style="width:70px;"><fmt:message key="unit" /></span></td>
									<td><span style="width:90px;">报货数量</span></td>
									<td><span style="width:100px;"><fmt:message key="remark" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="chkstod" items="${chkstodlist}" varStatus="status">
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${chkstod.vmcode}"/></span></td>
										<td><span style="width:140px;background:#F1F1F1;"><c:out value="${chkstod.vmname}"/></span></td>
										<td><span style="width:90px;"><c:out value="${chkstod.vspecfication}"/></span></td>
										<td><span style="width:70px;"><c:out value="${chkstod.vunitname}"/></span></td>
										<td><span style="width:90px;text-align: right;background:#F1F1F1;" minbhcnt="${chkstod.nmincnt}"><fmt:formatNumber value="${chkstod.nneedcnt}" pattern="##.##"/></span></td>
										<td><span style="width:100px;"><c:out value="${chkstod.vmemo}"/></span></td>
										<td style="display: none;"><span style="width:120px;display: none;"><c:out value="${chkstod.pk_material}"/></span></td>
										<td style="display: none;"><span style="width:120px;display: none;"><c:out value="${chkstod.pk_unit}"/></span></td>
										<td style="display: none;"><span style="width:120px;display: none;"><c:out value="${chkstod.pk_materialtype}"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form>
			<input type="hidden" id="positncodes" name="positncodes" value="${positncodes}" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTable.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript">
		$("#dmakedate").focus();
			function setPk_org(data){
				$("#positncode").val(data.code);//主键
				$("#positnname").val(data.entity[0].vname);//名称
				$("#positncode").val(data.entity[0].vcode);//编码
				$("#listForm").submit();
			}
			$(document).ready(function(){
				$('#positncodebutton').click(function(){
					chooseDepartMentList({
						basePath:'<%=path%>',
						title:"123",
						height:400,
						width:600,
						callBack:'setPk_org',
						domId:'positncode',
						type:1,
						positncodes:$("#positncodes").val(),
						single:true
					});
				});
				$("#dmakedate").click(function(){
					new WdatePicker({maxDate:'#F{$dp.$D(\'dhopedate\')}'});
				});
				$("#dhopedate").click(function(){
					new WdatePicker({minDate:'#F{$dp.$D(\'dmakedate\')}',maxDate:'%y-%M-{%d+'+180+'}'});
				});
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();            
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),80);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()));
// 				loadGrid();//  自动计算滚动条的js
				
				if ($("#sta").val() == "add") {
					editCells();
				}
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:9,
					widths:[30,110,150,100,80,100,110],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1",'text-align':'right'},{background:"#F1F1F1"},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos == 2){
// 							var vname = postd.find('span').attr('vname');
// 							if(vname==undefined){
// 								postd.find('span').text('');
// 							}else{
// 								postd.find('span').text(vname);
// 							}
						}
						if(pos == 5){
							var bhnum = $.trim(rownum.find("td:eq(5) span").text());
							if(bhnum==''){
								bhnum=$.trim(rownum.find("td:eq(5) input").val());
							}
							var mincnt = Number(rownum.find("td:eq(5) span").attr('minbhcnt'));
							if(isNaN(bhnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) < mincnt){
								rownum.find("td:eq(5) span").text(mincnt);
								alerterror('第'+rownum.find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+mincnt);
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else{
								rownum.find("td:eq(5) span").text(bhnum);
								$.fn.autoGrid.setCellEditable(rownum,6);
							}
						}
						if(pos == 6){
							var vmemo = $.trim(rownum.find("td:eq(6) span").text());
							if(vmemo==''){
								vmemo = $.trim(rownum.find("td:eq(6) input").val());
							}
							postd.find('span').text(vmemo);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								  selectedrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                positncode:$('#positncode').val(),//采购组织
							                domId:'selected_pk_material',
							                irateflag:1,//取采购单位
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(5) span").text('');
									row.find("td:eq(5) span").attr('minbhcnt','');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(7) span").text('');
									row.find("td:eq(8) span").text('');
									row.find("td:eq(9) span").text('');
									
									row.find("td:eq(2) input").focus();
									$("#mMenu").remove();
									return;	
								}
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?irateflag=1&positncode='+$('#positncode').val();
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var vspecfication ='';
							if(data.vspecfication !='' && data.vspecfication !=null){
								vspecfication = '-'+data.vspecfication ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + vspecfication;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '物资重复添加！',
	 								speed: 1000
	 							});
								row.find("td:eq(2) span input").val('').focus();
                                					$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode);
							row.find("td:eq(2) span").text(data2.vname);
							row.find("td:eq(2) span").attr('vname',data2.vname);
							row.find("td:eq(3) span").text(data2.vspecfication);
							row.find("td:eq(4) span").text(data2.unitvname);
							row.find("td:eq(5) span").attr('minbhcnt',data2.minbhcnt);
							row.find("td:eq(7) span").text(data2.pk_material);
							row.find("td:eq(8) span").text(data2.pk_unit);
							row.find("td:eq(9) span").text(data2.pk_materialtype);
							$.fn.autoGrid.setCellEditable(row,5);
						}
					},{
						index:5,
						action:function(row,data2){
							var mincnt = Number(row.find("td:eq(5) span").attr('minbhcnt'));
							
							if(isNaN(data2.value)){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) == 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < mincnt){
								row.find("td:eq(5) span").text(mincnt);
								alerterror('第'+row.find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+mincnt);
								$.fn.autoGrid.setCellEditable(row,5);
							}else{
								$.fn.autoGrid.setCellEditable(row,6);
							}
						},
						onCellEdit:function(event,data,row){
							var mincnt = Number(row.find("td:eq(5) span").attr('minbhcnt'));
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								$.fn.autoGrid.setCellEditable(row,5);
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[
					{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="template" /><fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="template" /><fmt:message key="name" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function saveChkstom(){
				if(savelock){
					return;
				}else{
					savelock = true;
					$("#window_chooseMaterial").find('.close').click();
					$("#window_chooseSupplier").find('.close').click();
					$("#window_chooseFirm").find('.close').click();
					$("#mMenu").remove();
				}
				
				$('.grid').find('.table-body').find('table').find('td').each(function(){
					var pos = $(this).closest('tr').find('td').index($(this));
					if(pos!=2){
						$(this).find('input').trigger('onEnter');
					}
				});
				var checkcnt = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var pk_material=$.trim($(this).find("td:eq(7) span").text());
					if(pk_material==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
						return;
					}
					
					var bhnum = $.trim($(this).find("td:eq(5) span").text());
					var mincnt = Number($(this).find("td:eq(5) span").attr('minbhcnt'));
					if(Number(bhnum)<Number(mincnt)){
						alerterror('第'+$(this).find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="Minimum_Purchase_Amount"/>'+mincnt);
						checkcnt = false;
						return false;
					}
				});
				if(!checkcnt){
					savelock = false;
					return;
				}
				
				var keys = ["vmcode","vmname","vspecfication","vunitname","nneedcnt","vmemo","pk_material","pk_unit","pk_materialtype"];
				var data = {};
				var i = 0;
				data["vbillno"] = $.trim(stripscript($("#vbillno").val()));
				data["positncode"] = $("#positncode").val();
				data["positnname"] = $("#positnname").val();
				data["positncode"] = $("#positncode").val();
				data["dmakedate"] = $("#dmakedate").val();
				data["dhopedate"] = $("#dhopedate").val();
				data["vmemo"] = $("#vmemo").val();
				
				if(data.positncode==''){
					alerterror('请选择采购组织！');
					savelock = false;
					return;
				}
				if(data.dhopedate == ''){
					alerterror('要求到货日不能为空！');
					savelock = false;
					return;
				}
				if(data.vmemo.length>100){
					alerterror('备注最大长度不能大于100！');
					savelock = false;
					return;
				}
				var rows = $(".grid .table-body table tr");
				var checkdata = true;
				for(i=0;i<rows.length;i++){
					var vmcode = $.trim($(rows[i]).find("td:eq(1)").text());
					if(vmcode==''){
						alerterror('第'+(i+1)+'行未选择物资！');
				        		checkdata = false;
						break;
					}
					var tcnt = $.trim($(rows[i]).find("td:eq(5) span").text());
					var icnt = $.trim($(rows[i]).find("td:eq(5) input").val());
					var cnt = tcnt ==""?icnt:tcnt;
					if(cnt == ''){
						alerterror('第'+(i+1)+'行报货数量不能为空！');
						checkdata = false;
						break;
					}if(isNaN(cnt)){
						alerterror('第'+(i+1)+'行报货数量必须为数字！');
						checkdata = false;
						break;
					}else if(Number(cnt) < 0){
						alerterror('第'+(i+1)+'行报货数量不能为负数！');
						checkdata = false;
						break;
					}
					var j = 0;
					data["chkstodlist["+i+"].isort"] = $(rows[i]).find("td:first").find('span').text();
					//判断备注的长度
					var vmemocontent=$.trim($(rows[i]).find('td:eq(6) span').text());
					vmemocontent = vmemocontent ? vmemocontent : $.trim($(rows[i]).find("td:eq(6) input").val());	
					if(vmemocontent.length>100){
						alerterror('第'+(i+1)+'行备注长度不能大于100');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						checkdata = false;
						savelock = false;
						break;
						return;
					}
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
						if(value){
							data["chkstodlist["+i+"]."+keys[j-1]] = value;	
						}
					}
				}
				if(!checkdata){
					savelock = false;
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/chksto/addChkstom.do",data,function(re){
					var rs = re;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			
			function setMaterial(data){
				var flag = true;
				if(data.entity[0].pk_material==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
							
				selectedrow.find("td:eq(1) span").text(data.entity[0].vcode).css("text-align","right");
				selectedrow.find("td:eq(2) span").text(data.entity[0].vname);
				selectedrow.find("td:eq(2) span").attr('vname',data.entity[0].vname);
				selectedrow.find("td:eq(3) span").text(data.entity[0].vspecfication);
				selectedrow.find("td:eq(4) span").text(data.entity[0].vunitname);
				selectedrow.find("td:eq(5) span").attr('minbhcnt',data.entity[0].minbhcnt);
				selectedrow.find("td:eq(6) span").text(data.entity[0].vmemo);
				selectedrow.find("td:eq(7) span").text(data.entity[0].pk_material);
				selectedrow.find("td:eq(8) span").text(data.entity[0].pk_unit);
				selectedrow.find("td:eq(9) span").text(data.entity[0].pk_materialtype);
				$.fn.autoGrid.setCellEditable(selectedrow,5);
			}
			function resetName(e){
				if($('#mMenu').length!=0){
					return;
				}
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==2 || curindex==5){//物资 供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}
			}
		</script>
	</body>
</html>
