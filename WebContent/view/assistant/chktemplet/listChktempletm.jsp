<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="purchase_template" /></title>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
			.form-line .form-label{
				width: 80px;
			}
			.form-line .form-input{
				width: 80px;
			}
			.table-head td span{
				white-space: normal;
			}
		</style>
	</head>
	<body>
		<div id="wait2" style="display:block;"></div>
		<div id="wait" style="display:block;">
			<img src="<%=path%>/image/loading_detail.gif" />
			&nbsp;
			<span id="msgShow" style="color:white;font-size:15px;"><fmt:message key="dataLoading" />...</span>
		</div>  
		<div class="tool"></div>
		<div style="height:44%;">
			<input type="hidden" id="pk_chktemplet" name="pk_chktemplet" value="${pk_chktemplet}"/>
			<form id="listForm" action="<%=path%>/chktemplet/queryAllChktempletm.do" method="post">
				<div class="search-div" style="positn: absolute;z-index: 99">
					<div class="form-line">
						<div class="form-label"><fmt:message key="template" /><fmt:message key="gyszmmcs" />:</div>
						<div class="form-input">
							<input type="text" id="vname" name="vname" style="text-transform:uppercase;" value="${chktempletm.vname}" class="text"/>
						</div>
					</div>
					<div class="search-commit">
			       		<input type="button" class="search-button" id="search" value='<fmt:message key="select" />'/>
					<input type="button" class="search-button" id="resetSearch" value="<fmt:message key="empty" />"/>
					</div>
				</div>
				<div class="grid" id="grid">		
					<div class="table-head">
						<table cellspacing="0" cellpadding="0" id="thGrid">
							<thead>
								<tr>
									<td class="num"><span style="width:30px;">&nbsp;</span></td>
									<td><span style="width:35px;"><input type="checkbox" id="chkAll"/></span></td>
									<td><span style="width:150px;"><fmt:message key="template" /><fmt:message key="gyszmmcs" /></span></td>
									<td><span style="width:300px;"><fmt:message key="summary" /></span></td>
									<td><span style="width:150px;"><fmt:message key="creation_date" /></span></td>
		                     	</tr>
							</thead>
						</table>
					</div>				
					<div class="table-body">
						<table cellspacing="0" cellpadding="0" id="tblGrid">
							<tbody>
								<c:forEach var="chktempletm" items="${listChktempletm}" varStatus="status">
									<tr>
										<td class="num"><span style="width:30px;">${status.index+1}</span></td>
										<td><span style="width:35px;text-align: center;"><input type="checkbox" name="idList" id="chk_${chktempletm.pk_chktemplet}" value="${chktempletm.pk_chktemplet}"/></span></td>
										<td title="${chktempletm.vname}" ><span style="width:150px;">${chktempletm.vname}</span></td>
										<td title="${chktempletm.vmemo}" ><span style="width:300px;">${chktempletm.vmemo}</span></td>
										<td title="${chktempletm.dmakedate}" ><span style="width:150px;text-align: center;">${chktempletm.dmakedate}</span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>	
				<page:page form="listForm" page="${pageobj}"></page:page>
				<input type="hidden" name ="nowPage" id="nowPage" value="${pageobj.nowPage }" />
				<input type="hidden" name ="pageSize"  id="pageSize" value="${pageobj.pageSize }" />				
			</form>
		</div>
		<div class="mainFrame" style=" height:50%;width:100%;">
			<iframe src="<%=path%>/chktemplet/queryAllChktempletd.do?pk_chktemplet=${pk_chktemplet}" frameborder="0" name="mainFrame" id="mainFrame"></iframe>
		</div>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/BoxSelect.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/tableInput.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/scm/changeTh.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/common/teleFunc.js"></script>
		<script type="text/javascript" src="<%=path%>/js/ueditor/editor_all_min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.js"></script>
		<script type="text/javascript" src="<%=path%>/js/layer/layer.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/getInitFromName.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//快捷键操作（Alt+*）
				$(document).keydown(function (e) {
				    var doPrevent;
				    if (event.keyCode==78 && event.altKey) {//alt+N
				    	if(!$('#window_supply').html()){
				    		addchktempletm();
				    	}
				    }else if(event.keyCode==85 && event.altKey){//alt+U
				    	if(!$('#window_supply').html()){
				    		updatechktempletm();
				    	}
				    }else{
				        doPrevent = false;
				    }
				    if (doPrevent)
				        e.preventDefault();
				}); 
				/* 模糊查询提交 */
				$("#search").bind('click', function() {
					$("#wait2").css("display","block");
					$("#wait").css("display","block");
					$('.search-div').hide();
					$("#vname").val(stripscript($("#vname").val()));
					$('#listForm').submit();
				});
				/* 模糊查询清空 */
				$("#resetSearch").bind('click', function() {
					clearQueryForm();
				});
				//自动实现滚动条
				setElementHeight('.grid',['.tool','.mainFrame'],$(document.body),70);	//计算.grid的高度
				setElementHeight('.table-body',['.table-head'],'.grid');	//计算.table-body的高度
				loadGrid();//  自动计算滚动条的js方法		
				changeTh();
				//分页
			 	$(".page").css("margin-bottom",$(".mainFrame").height()+7);

				//刷新字表
				$('.grid .table-body tr').live('click',function(){
					 $('#pk_chktemplet').val($(this).find('td:eq(1)').find('input').val());
					 var pk_chktemplet=$(this).find('td:eq(1)').find('input').val();
					 var url="<%=path%>/chktemplet/queryAllChktempletd.do?pk_chktemplet="+pk_chktemplet;
					 $('#mainFrame').attr('src',encodeURI(url));
					 $(this).addClass('tr-over').find(":checkbox").attr("checked", true);
					$('.grid').find('.table-body').find('tr').not(this).removeClass('tr-over').find(":checkbox").attr("checked", false);
				});
				$('.grid').find('.table-body').find('tr').find(":checkbox").live("click", function (event) {
					event.stopPropagation(); 
				}); 
			   //光标移动
			   new tabTableInput("tblGrid","text"); 
			   $('.grid').find('.table-body').find('tr:odd').toggleClass('tr-toggle');
				$('.tool').toolbar({
						items: [{
							text: '<fmt:message key="select" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'select')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','-40px']
							},
							handler: function(){
								$('.search-div').slideToggle(100);
								$('#vname').focus();
							}
						},{
							text: '<fmt:message key="insert" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'insert')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								addchktempletm();
							}
						},{
							text: '<fmt:message key="update" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'update')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['0px','0px']
							},
							handler: function(){
								updatechktempletm();
							}
						},{
							text: '<fmt:message key="delete" />',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'delete')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								deletechktempletm();
							}
						},{
							text: '<fmt:message key="to_save" />报货单',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'creat')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								savechksto();
							}
						},{
							text: '适用采购组织',
							useable: ${elf:isPermitted(requestScope['javax.servlet.forward.request_uri'],'authorize')},
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-38px','0px']
							},
							handler: function(){
								insertorg();
							}
						},{
							text: '<fmt:message key="quit" />',
							icon: {
								url: '<%=path%>/image/Button/op_owner.gif',
								positn: ['-160px','-100px']
							},
							handler: function(){
								invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));									
							}
						}]
				});
				var trrows = $(".table-body").find('tr');
				if(trrows.length!=0){
					$(trrows[0]).addClass('tr-over').find(":checkbox").attr("checked", true);;
				}
				$("#wait2").css("display","none");
				$("#wait").css("display","none");
			});	
			//新增采购清单
			function addchktempletm(){
				$('body').window({
					id: 'window_supply',
					title: '<fmt:message key="insert" />报货模板',
					content: '<iframe id="saveChktempletm" frameborder="0" src="<%=path%>/chktemplet/toAddChktempletm.do"></iframe>',
					width: '750px',
					height: '500px',
					draggable: true,
					isModal: true,
					topBar: {
						items: [{
								text: '<fmt:message key="save" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-80px','-0px']
								},
								handler: function(){
									if(getFrame('saveChktempletm')&&window.document.getElementById("saveChktempletm").contentWindow.validate._submitValidate()){
										window.document.getElementById("saveChktempletm").contentWindow.savePurtempletm();
									}
								}
							},{
								text: '<fmt:message key="cancel" />',
								icon: {
									url: '<%=path%>/image/Button/op_owner.gif',
									positn: ['-160px','-100px']
								},
								handler: function(){
									$('.close').click();
								}
							}
						]
					}
				});
			}
			//修改采购清单
			function updatechktempletm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() == 1){
					var aim = checkboxList.filter(':checked').eq(0);
					var chkValue = aim.val();
					$('body').window({
						id: 'window_supply',
						title: '<fmt:message key="update" />报货模版',
						content: '<iframe id="updateChktempletm" frameborder="0" src="<%=path%>/chktemplet/toUpdateChktempletm.do?pk_chktemplet='+chkValue+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('updateChktempletm')&&window.document.getElementById("updateChktempletm").contentWindow.validate._submitValidate()){
											window.document.getElementById("updateChktempletm").contentWindow.updateChktempletm();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else if(checkboxList 
						&& checkboxList.filter(':checked').size()>1){
					alerterror('<fmt:message key="please_select_data" />!');
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_modify" />！');
					return ;
				}
			}
			//删除
			function deletechktempletm(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					alertconfirm("<fmt:message key="delete_data_confirm" />？",function(){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						var action = '<%=path%>/chktemplet/deleteChktempletm.do?pks='+chkValue.join(",");
						$('body').window({
							title: '<fmt:message key="delete" />报货模板',
							content: '<iframe frameborder="0" src='+action+'></iframe>',
							width: '500px',
							height: '245px',
							draggable: true,
							isModal: true
						});
					});
				}else{
					alerterror('<fmt:message key="please_select_information_you_need_to_delete" />！');
					return ;
				};
			}
			function reloadPage(){
				$("#listForm").submit();
			}
			//生成采购订单
			function savechksto(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					if(checkboxList 
							&& checkboxList.filter(':checked').size() == 1){
						var pk_chktemplet = $(checkboxList.filter(':checked')[0]).val();
						$('body').window({
							id: 'window_supply',
							title: '<fmt:message key="to_save" />报货单',
							content: '<iframe id="savechkstombytemplet" frameborder="0" src="<%=path%>/chksto/tosavechkstombytemplet.do?pk_chktemplet='+pk_chktemplet+'"></iframe>',
							width: '750px',
							height: '500px',
							draggable: true,
							isModal: true,
							topBar: {
								items: [{
										text: '<fmt:message key="save" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											positn: ['-80px','-0px']
										},
										handler: function(){
											if(getFrame('savechkstombytemplet')){
												window.document.getElementById("savechkstombytemplet").contentWindow.saveChkstom();
											}
										}
									},{
										text: '<fmt:message key="cancel" />',
										icon: {
											url: '<%=path%>/image/Button/op_owner.gif',
											positn: ['-160px','-100px']
										},
										handler: function(){
											$('.close').click();
										}
									}
								]
							}
						});
					}else{
						alerterror('请选择一条数据！');
						return;
					}
				}else{
					alerterror('<fmt:message key="please_select" />报货单！');
					return ;
				};
			}
			//适用分店
			function insertorg(){
				var checkboxList = $('.grid').find('.table-body').find(':checkbox');
				if(checkboxList 
						&& checkboxList.filter(':checked').size() > 0){
					var chkValue = [];
					checkboxList.filter(':checked').each(function(){
						chkValue.push($(this).val());
					});
					$('body').window({
						id: 'window_supply',
						title: '适用采购组织',
						content: '<iframe id="savechkstombytemplet" frameborder="0" src="<%=path%>/chktemplet/chooseDepartment.do?chkValue='+chkValue.join(",")+'"></iframe>',
						width: '750px',
						height: '500px',
						draggable: true,
						isModal: true,
						topBar: {
							items: [{
									text: '<fmt:message key="save" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-80px','-0px']
									},
									handler: function(){
										if(getFrame('savechkstombytemplet')){
											window.document.getElementById("savechkstombytemplet").contentWindow.savechktempletogr();
										}
									}
								},{
									text: '<fmt:message key="cancel" />',
									icon: {
										url: '<%=path%>/image/Button/op_owner.gif',
										positn: ['-160px','-100px']
									},
									handler: function(){
										$('.close').click();
									}
								}
							]
						}
					});
				}else{
					alerterror('请至少选中一条数据！');
					return ;
				};
			}
			document.onkeydown=function(){
	            if(event.keyCode==27){//ESC 后关闭窗口
	                if($('.close').length>0){
	                    $('.close').click();
	                }else {
	                	invokeClick($(window.parent.parent.document).find('.tab-item').find('.button-click').find('.button-arrow').get(0));	
	                }
	            }
	        };
		</script>
	</body>
</html>