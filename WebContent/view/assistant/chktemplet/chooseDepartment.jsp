<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/tld/pagetag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="org" /><fmt:message key="select1" /></title>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
			<link type="text/css" rel="stylesheet" href="<%=path%>/css/search.css" />
			<style type="text/css">
				.grid td span{
					padding:0px;
				}
				.form-line .form-input{
					width: 17%;
					margin-right: 0px;
					padding-left: 0px;
				}
				.form-line .form-input input,.form-line .form-input select{
					width:90%;
				}
			</style>
		</head>
	<body>
		<div id='toolbar'></div>
		<input type="hidden" id="chkValue" name="chkValue" value="${chkValue}"/>
		<form id="savechkstombytemplet" name="savechkstombytemplet" method="post">
			<div class="form-line">
				<div class="form-label" style="width:40px"><fmt:message key="coding" />：</div>
				<div class="form-input">
					<input id="vcode" name="vcode" class="text"/>
				</div>
				<div class="form-label" style="width:40px"><fmt:message key="name" />：</div>
				<div class="form-input">
					<input id="vname" name="vname" class="text"/>
				</div>
				<div class="form-label" style="width:60px"><fmt:message key="abbreviation" />：</div>
				<div class="form-input">
					<input id="vinit" name="vinit" class="text"/>
				</div>
				<input type="button" id="filterquery" value="<fmt:message key="query" />"/>
			</div>
			<div class="grid">
			<div class="table-head">
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<td style="text-align: center;"><span style="width: 25px;"></span></td>
							<td style="width:30px;"><input type="checkbox" id="chkAll" />
							</td>
							<td><span style="width:100px;"><fmt:message key="coding" /></span></td>
							<td><span style="width:100px;"><fmt:message key="name" /></span></td>
							<td><span style="width:80px;"><fmt:message key="abbreviation" /></span></td>
							<td><span style="width:80px;"><fmt:message key="enable_state" /></span></td>
							<td><span style="width:80px;"><fmt:message key="contact" /></span></td>
							<td><span style="width:80px;"><fmt:message key="phone" /></span></td>
							<td><span style="width:150px;"><fmt:message key="address" /></span></td>
						</tr>
					</thead>
				</table>
			</div>
			<div class="table-body">
				<table cellspacing="0" cellpadding="0" class="datagrid">
					<tbody>
						<c:forEach var="department" items="${departmentList}"	varStatus="status">
							<tr>
								<td style="text-align: center;"><span style="width: 25px;">${status.index+1}</span>
								</td>
								<td>
									<span style="width:30px; text-align: center;"><input type="checkbox" name="idList" id="<c:out value='${department.id}' />"
									value="<c:out value='${department.id}' />" /></span>
								</td>
								<td><span style="width:100px;">${department.viewCode}</span></td>
								<td><span style="width:100px;">${department.name}</span></td>
								<td><span style="width:80px;">${department.vinit}</span></td>
								<td>
									<span style="width:80px;"> 
										<c:if test="${department.enablestate==1}"><fmt:message key="not_enabled" /></c:if> 
										<c:if test="${department.enablestate==2}"><fmt:message key="have_enabled" /></c:if>
										<c:if test="${department.enablestate==3}"><fmt:message key="stop_enabled" /></c:if> 
									</span>
								</td>
								<td><span style="width:80px;">${department.vcontact}</span></td>
								<td><span style="width:80px;">${department.vtel}</span></td>
								<td><span style="width:150px;">${department.vaddr}</span></td>
								<td style="display: none;"><span style="width:100px;">${cbohBoh.cboh_market.pk_market}</span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		</form>
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/common/teleFunc-${sessionScope.locale}.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/boh/BoxSelect.js"></script>
		
		<script type="text/javascript">
        $("#vcode").focus();
			var selected;
			$(document).ready(function(){
				setElementHeight('.grid',['.tool'],$(document.body),26);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()+30));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()+30));
// 				loadGrid();//  自动计算滚动条的js

				$('.grid').find('.table-body').find('tr').find(':checkbox').bind("change", function () {
					if ($(this)[0].checked) {
					    $(this).attr("checked", false);
					}else{
						$(this).attr("checked", true);
					}
				});
				// 当点击tr行的时候，tr行头的checkbox也能被选中，不用非得点击checkbox才能选中行
				$('.grid').find('.table-body').find('tr').bind("click", function () {
					if ($(this).find(':checkbox')[0].checked) {
						$(this).find(':checkbox').attr("checked", false);
					}else{
						$(this).find(':checkbox').attr("checked", true);
					}
				});
				//---------------------------
	// 			全选
				$("#chkAll").click(function() {
			    	if (!!$("#chkAll").attr("checked")) {
			    		$('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",true);
			    	}else{
			            $('.grid').find('.table-body').find('tr').find(':checkbox').attr("checked",false);
		            }
			    });
				var positncodeList = '${positncodeList}';
				if(positncodeList != null && positncodeList != ""){
					$('.datagrid').find('tr').each(function(){
						var pk = $(this).find('td:eq(1)').find('input').val();
						if(positncodeList.indexOf(pk) != -1){
							$(this).find(':checkbox').attr('checked',true);
						}
					});
				}

			});
				function savechktempletogr(){
					var checkboxList = $('.grid').find('.table-body').find(':checkbox');
					if(checkboxList  
							&& checkboxList.filter(':checked').size() > 0){
						var chkValue = [];
						checkboxList.filter(':checked').each(function(){
							chkValue.push($(this).val());
						});
						$.ajaxSetup({async:false});
						$.post("<%=path%>/chktemplet/addChktempletOrg.do?chkValue="+chkValue.join(",")+"&chktemplets="+$('#chkValue').val(),function(data){
							var rs = data;
		                    if(isNaN(rs)){
		                    	alerterror('<fmt:message key="save_fail"/>！');
		                        	return;
		                    }
							switch(Number(rs)){
								case -1:
									alerterror('<fmt:message key="save_fail"/>！');
									break;
								case 1:
									showMessage({
												type: 'success',
												msg: '<fmt:message key="save_successful"/>！',
												speed: 3000,
												handler:function(){
													parent.reloadPage();
													parent.$('.close').click();}
												});
									break;
							}
						});	
					}else{
						alerterror('请至少选择一条数据！');
						return;
					}
				}
		</script>
	</body>
</html>