<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><fmt:message key="storehouse_fill_in_audit"/></title>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.core.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path %>/css/lib.ui.form.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>	
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.grid.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/validate.css" />
		<link type="text/css" rel="stylesheet" href="<%=path%>/css/scm/ajaxSearch.css" />
		<style type="text/css">
				.memoClass{border:0px;background:none;}
			</style>
	</head>
	<body>
	<div class="tool"></div>
		<input type="hidden" id="acct" name="acct" value="${chktempletm.acct}"/>
		<form action="<%=path%>/chktemplet/updateChktempletm.do" id="saveChktempletm" name="saveChktempletm" method="post">
			<input type="hidden" id="pk_chktemplet" name="pk_chktemplet" value="${chktempletm.pk_chktemplet}"/>
			<div class="bj_head" style="height: 60px;">
				<div class="form-line">
					<div class="form-label"><span style="color:red;">*</span>清单名称:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="vname" id="vname" class="text" value="${chktempletm.vname}"/>
					</div>
					<div class="form-label"><span style="color:red;">*</span><fmt:message key="creation_date" />:</div>
					<div class="form-input" style="width:160px;">
						<input type="text" name="dmakedate" id="dmakedate" class="text" readonly="readonly" value="${chktempletm.dmakedate}" />
					</div>	
				</div>
				<div class="form-line">
					<div class="form-label"><fmt:message key="summary" />:</div>
					<div class="form-input" >
						<input type="text" name="vmemo" id="vmemo" class="text" style="width: 413px;" value="${chktempletm.vmemo}" />
					</div>
				</div>
			</div>
			<div class="grid">
					<div class="table-head" >
						<table cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<td class="num"><span style="width: 16px;">&nbsp;</span></td>
	                                <td><span style="width: 100px;"><fmt:message key="supplies_code" /></span></td>
	                                <td><span style="width: 140px;"><fmt:message key="supplies_name" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="specification" /></span></td>
	                                <td><span style="width: 60px;"><fmt:message key="unit" /></span></td>
	                                <td><span style="width: 80px;"><fmt:message key="quantity" /></span></td>
	                                <td><span style="width: 120px;"><fmt:message key="remark" /></span></td>
	                                <td><span style="width: 40px;"><fmt:message key="no" /></span></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="table-body">
						<table cellspacing="0" cellpadding="0">
							<tbody>
								<c:forEach var="chktempletd" items="${chktempletdlist}" varStatus="status">
									<tr>
										<td class="num"><span style="width:16px;">${status.index+1}</span></td>
										<td><span style="width:100px;"><c:out value="${chktempletd.vmcode}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:140px;" vname="${chktempletd.vmname}"><c:out value="${chktempletd.vmname}"/></span></td>
										<td><span style="width:60px;"><c:out value="${chktempletd.vspecfication}"/></span></td>
										<td><span style="width:60px;"><c:out value="${chktempletd.vunitname}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:80px;text-align: right;"><c:out value="${chktempletd.nneedcnt}"/></span></td>
										<td style="background:#F1F1F1;"><span style="width:120px;"><c:out value="${chktempletd.vmemo}"/></span></td>
										<td style="display: none;"><span><c:out value="${chktempletd.pk_material}"/></span></td>
										<td style="display: none;"><span><c:out value="${chktempletd.pk_unit}"/></span></td>
										<td style="display: none;"><span><c:out value="${chktempletd.pk_materialtype}"/></span></td>
										<td style="display: none;"><span><c:out value="${chktempletd.nmincnt}"/></span></td>
										<td><span style="height:24px;width:40px;"><img style="height:21px;width:15px;" title="上移一行" src="../image/up.png" onclick="up(this)"/>&nbsp;&nbsp;<img style="height:21px;width:15px;" title="下移一行" src="../image/down.png" onclick="down(this)"/></span></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div style="margin-top: 5px;">
					<div class="form-line">
						<div class="form-label"><fmt:message key="orders_maker" />:</div>
						<div class="form-input" style="width:50px;">
							<input type="text" style="border: 0px;" name="creator" id="creator" class="text" readonly="readonly" value="${chktempletm.creator}"/>
						</div>
						<div class="form-label" style="width: 60px;"><fmt:message key="time_of_the_system_alone" />:</div>
						<div class="form-input" style="width:80px;">
							<input type="text" style="border: 0px;" name="creationtime" id="creationtime" readonly="readonly" class="text" value="${chktempletm.creationtime}" />
						</div>
						<div class="form-label">修改人:</div>
						<div class="form-input" style="width:60px;">
							<input type="text" style="border: 0px;" name="modifier" id="modifier" class="text" readonly="readonly" value="${chktempletm.modifier}"/>
						</div>
						<div class="form-label" style="width: 60px;">修改时间:</div>
						<div class="form-input" style="width:80px;">
							<input type="text" style="border: 0px;" name="modifedtime" id="modifedtime" readonly="readonly" class="text" value="${chktempletm.modifedtime}" />
						</div>
					</div>
				</div>
			</form>
			<input type="hidden" id="selected_pk_material" />
			<input type="hidden" id="selected_delivercode" />
		<script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/datePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=path%>/js/jquery.json-2.3.min.js"></script>
		<script type="text/javascript" src="<%=path%>/js/validate.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/autoTableWithSort.js"></script>
		<script type="text/javascript" src="<%=path%>/js/offset.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/common/teleFunc-zh_CN.js"></script>
		<script type="text/javascript" src="<%=path%>/js/assistant/omui/operamasks-ui.min.js"></script>
		<script type="text/javascript">
		//ajax同步设置
		$.ajaxSetup({
			async: false
		});
		var selectedrow;
			$(document).ready(function(){
				//回车换焦点start************************************************************************************************
			    var array = new Array();        
		 	    //定义需要做切换的input输入框，最后可以放一个提交按钮，这样最好一个input点击回车后可以直接触发按钮的点击       
		 	    array = ['firmDes','firm'];        
		 		//定义加载后定位在第一个输入框上          
		 		$('#'+array[0]).focus();          
				$("#vname").focus();  
		 		$('select,input[type="text"]').keydown(function(e) {                  
			 		//使用jquery的这种获取事件方法，不必再指定event.srcElement或者event.target  
			 		var event = $.event.fix(e);                
			 		//判断如果按键事件的按键代码是回车，则从数组中获取下一个元素的ID,并设置焦点                       
			 		if (event.keyCode == 13) {                
			 			var index = $.inArray($.trim($(event.target).attr("id")), array);//alerterror(index)
		 				$('#'+array[++index]).focus();
		 				if(index==2){
// 		 					$('#firmDes').val($('#firm').val());
		 					$.fn.autoGrid.setCellEditable($('.table-body').find('tr:first'),2);
		 				} 
			 		}
		 		}); 
		 		//回车换焦点end************************************************************************************************
			   
			    $('input').filter(':disabled').addClass('textDisable');		//不可编辑颜色
				//自动实现滚动条
				setElementHeight('.grid',['.tool'],$(document.body),120);	//计算.grid的高度
// 				setElementHeight('.table-body',['.table-head'],$(document.body).height()-$('.tool').height()-$('.bj_head').height());	//计算.table-body的高度
// 			    $('.grid').height($(document.body).height()-$('.tool').height()-$('.bj_head').height());
// 			    $('.table-body').height($(document.body).height()-$('.tool').height()-$('.bj_head').height()-$('.table-head').height());
				$('.grid').find('.table-head').css("width",($('.grid').find('.table-head').width()));
				$('.grid').find('.table-body').css("width",($('.grid').find('.table-body').width()));
// 				loadGrid();//  自动计算滚动条的js
				
				editCells();
			});
			//编辑表格
			function editCells(){
				$(".table-body").autoGrid({
					initRow:1,
					colPerRow:13,
					widths:[30,110,150,70,70,90,130,100,100,100,100,100,100],
					colStyle:['','',{background:"#F1F1F1"},'','',{background:"#F1F1F1",'text-align':'right'},'',{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'},{display:'none'}],
					onEdit:$.noop,
					editable:[2,5,6],
					onEnter:function(data){
						var pos = data.curobj.closest('tr').find('td').index(data.curobj.closest('td'));
						var rownum = data.curobj.closest('tr');
						var postd = data.curobj.closest('td');
						if(pos == 2){
							var vname = postd.find('span').attr('vname');
							postd.find('span').text(vname);
						}
						if(pos == 5){
							var bhnum = $.trim(rownum.find("td:eq(5) span").text());
							if(bhnum==''){
								bhnum=$.trim(rownum.find("td:eq(5) input").val());
							}
							if(isNaN(bhnum)){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) == 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else if(Number(bhnum) < 0){
								rownum.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(rownum,5);
							}else{
								var mincnt = 0;
								var param = {};
								param['pk_material'] = $.trim(rownum.find("td:eq(7) span").text());
								$.post('<%=path%>/material/findMaterialByPk.do',param,function(data){
									if(Number(bhnum)<Number(data.minbhcnt)){
										alerterror('第'+rownum.find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+data.minbhcnt);
										rownum.find("td:eq(5) span").text(Number(data.minbhcnt).toFixed(2));
										rownum.find("td:eq(10) span").text(Number(data.minbhcnt).toFixed(2));
										$.fn.autoGrid.setCellEditable(rownum,5);
									}else{
										rownum.find("td:eq(5) span").text(bhnum);
										$.fn.autoGrid.setCellEditable(rownum,6);
									}
								})
							}
						}
						if(pos == 6){
							var vmemo = $.trim(rownum.find("td:eq(6) span").text());
							if(vmemo==''){
								vmemo = $.trim(rownum.find("td:eq(6) input").val());
							}
							postd.find('span').text(vmemo);
						}
					},
					cellAction:[{
						index:2,
						action:function(row,data){
							if(data.value == null || data.value == ''){
								$("input").blur();
								  selectedrow = row;
							            selectSupplyLeftAsst({
							                basePath:'<%=path%>',
							                title:'<fmt:message key="please_enter" /><fmt:message key="supplies" />',
							                height:420,
							                width:650,
							                callBack:'setMaterial',
							                domId:'selected_pk_material',
							                irateflag:1,//取采购单位
							                single:true
							            });
							            return;
							}
							$.fn.autoGrid.setCellEditable(row,5);
						},
						onCellEdit:function(event,data,row){
							if(window.event.keyCode==8||window.event.keyCode==46){
								if(data.value==''){
									row.find("td:eq(1) span").text('');
									row.find("td:eq(2) span").attr('vname','');
									row.find("td:eq(3) span").text('');
									row.find("td:eq(4) span").text('');
									row.find("td:eq(5) span").text('');
									row.find("td:eq(6) span").text('');
									row.find("td:eq(7) span").text('');
									
									row.find("td:eq(2) input").focus();
									$("#mMenu").remove();
									return;	
								}
							}
							data['url'] = '<%=path%>/material/findMaterialTop.do?irateflag=1';
							if (data.value.split(".").length>2) {
								data['key'] = 'vcode';
							}else if(!isNaN(data.value)){
								data['key'] = 'vcode';
							}else if((/[\u4e00-\u9fa5]+/).test(data.value)){
								data['key'] = 'vname';
							}else{
								data['key'] = 'vinit';
							}
							$.fn.autoGrid.ajaxEdit(data,row);
						},
						resultFormat:function(data){
							var vspecfication ='';
							if(data.vspecfication !='' && data.vspecfication !=null){
								vspecfication = '-'+data.vspecfication ;
							}
							return data.vinit+'-'+data.vcode+'-'+data.vname + vspecfication;
						},
						afterEnter:function(data2,row){
							var num=0;
							$('.grid').find('.table-body').find('tr').not(row).each(function (){
								if($(this).find("td:eq(1)").text()==data2.vcode){
									num=1;
								}
							});
							if(num==1){
								showMessage({
	 								type: 'error',
	 								msg: '物资重复添加！',
	 								speed: 1000
	 							});
								row.find("td:eq(2) span input").val('').focus();
                                					$.fn.autoGrid.setCellEditable(row,2);
								return;
							}
							row.find("td:eq(1) span").text(data2.vcode).css("text-align","right");
							row.find("td:eq(2) span").text(data2.vname);
							row.find("td:eq(2) span").attr('vname',data2.vname);
							row.find("td:eq(3) span").text(data2.vspecfication);
							row.find("td:eq(4) span").text(data2.unitvname);
							row.find("td:eq(5) span").text('');
							row.find("td:eq(6) span").text(data2.vmemo);
							row.find("td:eq(7) span").text(data2.pk_material);
							row.find("td:eq(8) span").text(data2.pk_unit);
							row.find("td:eq(9) span").text(data2.pk_materialtype);
							row.find("td:eq(10) span").text(data2.minbhcnt);
							$.fn.autoGrid.setCellEditable(row,5);
						}
					},{
						index:5,
						action:function(row,data2){
							if(isNaN(data2.value)){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_be_not_number"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) == 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_zero"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}else if(Number(data2.value) < 0){
								row.find("td:eq(5) span").text(0.00);
								alerterror('<fmt:message key="number_cannot_be_negative"/>！');
								$.fn.autoGrid.setCellEditable(row,5);
							}
						},
						onCellEdit:function(event,data,row){
							var cnt = data.value;
							if(isNaN($.trim(cnt))){
								row.find("td:eq(5) span").text('0.00');
								$.fn.autoGrid.setCellEditable(row,5);
							}
						}
					},{
						index:6,
						action:function(row,data){
							if(!row.next().html())$.fn.autoGrid.addRow();
							$.fn.autoGrid.setCellEditable(row.next(),2);
						}
					}]
				});
			}
			var validate;
			$(document).ready(function(){
				/*验证*/
				validate = new Validate({
					validateItem:[{
						type:'text',
						validateObj:'vname',
						validateType:['canNull','maxLength'],
						param:['F','25'],
						error:['<fmt:message key="template" /><fmt:message key="name" /><fmt:message key="cannot_be_empty" />！','<fmt:message key="template" /><fmt:message key="name" /><fmt:message key="length_too_long" />！']
					},{
						type:'text',
						validateObj:'vmemo',
						validateType:['maxLength'],
						param:['100'],
						error:['<fmt:message key="remark" /><fmt:message key="length_too_long" />！']
					}]
				});
			});
			var savelock = false;
			function updateChktempletm(){
				if(savelock){
					return;
				}else{
					savelock = true;
				}
				$('.grid').find('.table-body').find('table').find('td').find('input').each(function(){
					$(this).trigger('onEnter');
				});
				
				if(!checkName()){
					alerterror("模板名称已存在！");
					savelock = false;
					return;
				}
				
				var checkcnt = true;
				$('.grid').find('.table-body').find('table').find('tr').each(function(){
					var pk_material=$.trim($(this).find("td:eq(7) span").text());
					if(pk_material==''){
						deleteRow($(this).find("td[name='deleCell']")[0]);
						return;
					}
					
					var cgnum = $.trim($(this).find("td:eq(5) span").text());
					if(cgnum=='' || cgnum==0){
						alerterror('报货数量不能为0！');
						checkcnt = false;
					}
					var mincnt = Number($(this).find("td:eq(10) span").text());
					if(Number(cgnum)<Number(mincnt)){
						alerterror('第'+$(this).find('td:first').text()+'行报货数量<fmt:message key="can_not_be_less_than"/><fmt:message key="min_chkstom_cnt"/>'+mincnt);
						$(this).find("td:eq(5) span").text(mincnt);
						checkcnt = false;
						return false;
					}
				});
				if(!checkcnt){
					savelock = false;
					return;
				}
				
				var keys = ["vmcode","vmname","vspecfication","vunitname","nneedcnt","vmemo","pk_material","pk_unit","pk_materialtype","nmincnt"];
				var data = {};
				var i = 0;
				data["vname"] = $.trim(stripscript($("#vname").val()));
				data["pk_chktemplet"] = $("#pk_chktemplet").val();
				data["dmakedate"] = $("#dmakedate").val();
				data["creator"] = $("#creator").val();
				data["creationtime"] = $("#creationtime").val();
				data["modifier"] = $("#modifier").val();
				data["modifedtime"] = $("#modifedtime").val();
				data["vmemo"] = $("#vmemo").val();
				
				var rows = $(".grid .table-body table tr");
				if(rows.length==1){
					var pk_material=$.trim($(rows[i]).find('td:eq(7)').text());
					if(pk_material==''){
						alerterror('请选择物资！');
						savelock = false;
						return;
					}
				}
				var checkdata = true;
				for(i=0;i<rows.length;i++){
					var vmcode = $.trim($(rows[i]).find("td:eq(1) span").text());
					if(vmcode==''){
						alerterror('第'+(i+1)+'行未选择物资！');
				        		checkdata = false;
						break;
					}
					var cnt1 = $.trim($(rows[i]).find("td:eq(5) span").text());
					var cnt2 = $.trim($(rows[i]).find("td:eq(5) input").val());
					var cnt = cnt1==""?cnt2:cnt1;
					if(isNaN(cnt)){
						alerterror('数量输入不正确！');
						checkdata = false;
						break;
					}
					var j = 0;
					data["chktempletdlist["+i+"].isort"] = $(rows[i]).find("td:first").find('span').text();
					//判断备注的长度
					var vmemocontent=$.trim($(rows[i]).find('td:eq(6) span').text());
					vmemocontent = vmemocontent ? vmemocontent : $.trim($(rows[i]).find("td:eq(6) input").val());	
					if(vmemocontent.length>100){
						alerterror('第'+(i+1)+'行备注长度不能大于100');
						$("#wait2",parent.document).css("display","none");
						$("#wait",parent.document).css("display","none");
						checkdata = false;
						savelock = false;
						break;
						return;
					}
					for(j=1;j <= keys.length;j++){
						var value = $.trim($(rows[i]).find("td:eq("+j+")").text());
						value = value ? value : $.trim($(rows[i]).find("td:eq("+j+") input").val());
						if(value){
							data["chktempletdlist["+i+"]."+keys[j-1]] = value;	
						}
					}
				}
				if(!checkdata){
					savelock = false;
					return;
				}
				$.ajaxSetup({async:false});
				$.post("<%=path%>/chktemplet/updateChktempletm.do",data,function(re){
					var rs = re;
					switch(Number(rs)){
					case -1:
						alerterror('<fmt:message key="save_fail"/>！');
						savelock = false;
						break;
					case 1:
						showMessage({
									type: 'success',
									msg: '<fmt:message key="save_successful"/>！',
									speed: 3000,
									handler:function(){
										parent.reloadPage();
										parent.$('.close').click();}
									});
						break;
					}
				});	
			}
			function up(obj) {
				var objParentTR = $(obj).closest('tr');
				var prevTR = objParentTR.prev();
				var nextTR = objParentTR.next();
				if (prevTR.length > 0) {
					var curNum = $.trim(objParentTR.find('td:first').find('span').html());
					curNum = Number(curNum);
					prevTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
					objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum-1)+'</span>');
					
					if(nextTR.length==0){//最后一行
						var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
						objParentTR.find('td:last').remove();
						if(curNum==2){//第二条和第一条互换
							$(addCell).appendTo(prevTR);
						}else{
							prevTR.append(addCell);
						}
					}
					prevTR.insertAfter(objParentTR);
					var table = $(obj).closest('table');
					table.closest('div').height(table.height());
				}
				
			}
			function down(obj) {
				var objParentTR = $(obj).closest('tr');
				var nextTR = objParentTR.next();
				var nextnextTR = objParentTR.next().next();
				if (nextTR.length > 0) {
					var curNum = $.trim(objParentTR.find('td:first').find('span').html());
					curNum = Number(curNum);
					nextTR.find('td:first').html('<span style="width:24px;padding:1px;">'+curNum+'</span>');
					objParentTR.find('td:first').html('<span style="width:24px;padding:1px;">'+Number(curNum+1)+'</span>');
					
					if(nextnextTR.length==0){//和最后一行互换
						var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
						$(addCell).appendTo(objParentTR);
						nextTR.find('td:last').remove();
					}
					nextTR.insertBefore(objParentTR);
					var table = $(obj).closest('table');
					table.closest('div').height(table.height());
				}
			}
			
			function deleteRow(obj){
				var tb = $(obj).closest('table');
				var rowH = $(obj).parent("tr").height();
				var tbH = tb.height();
				$(obj).parent("tr").nextAll("tr").each(function(){
					var curNum = Number($.trim($(this).children("td:first").text()));
					$(this).children("td:first").html('<span style="width:26px;padding:0px;">'+Number(curNum-1)+'</span>');
				});
				
				if($(obj).next().length!=0){
					var addCell = $('<td name="addCell" style="width:10px;border:0;cursor: pointer;"  onclick="$.fn.autoGrid.addRow(2)"><img src="../image/scm/add.png"/></td>');
					tb.find('tr:last').prev().append(addCell);
				}
				
				var tr = $(obj).closest('tr');
				if(tr.prev().length==0 ){//删除第一行
					if(tr.next().find('td:last').attr('name')=='addCell'){//第二行最后是个+号
						tr.next().find('td[name="deleCell"]').remove();
					}
				}else if(tr.prev().prev().length==0){//点击第二行的删除
					if(tr.find('td:last').attr('name')=='addCell'){
						tr.prev().find('td[name="deleCell"]').remove();
					}
				}
				
				$(obj).parent("tr").remove();
// 				tb.height(tbH-rowH);
// 				tb.closest('div').height(tb.height());
			};
			
			function setMaterial(data){
				var flag = true;
				if(data.entity[0].pk_material==''){
					return;
				}
				$('.grid').find('.table-body').find('tr').each(function (){
					if($(this).find("td:eq(1)").text()==data.entity[0].vcode){
						flag = false;
						return;
					}
				});
				if(!flag){
					alerterror('<fmt:message key="supplies" /><fmt:message key="duplicate" /><fmt:message key="add" />');
					return;
				}
				selectedrow.find("td:eq(1) span").text(data.entity[0].vcode).css("text-align","right");
				selectedrow.find("td:eq(2) span").text(data.entity[0].vname);
				selectedrow.find("td:eq(2) span").attr('vname',data.entity[0].vname);
				selectedrow.find("td:eq(3) span").text(data.entity[0].vspecfication);
				selectedrow.find("td:eq(4) span").text(data.entity[0].vunitname);
				selectedrow.find("td:eq(5) span").text('');
				selectedrow.find("td:eq(6) span").text(data.entity[0].vmemo);
				selectedrow.find("td:eq(7) span").text(data.entity[0].pk_material);
				selectedrow.find("td:eq(8) span").text(data.entity[0].pk_unit);
				selectedrow.find("td:eq(9) span").text(data.entity[0].pk_materialtype);
				$.fn.autoGrid.setCellEditable(selectedrow,5);
			}
			
			function resetName(e){
				if($('#mMenu').length!=0){
					return;
				}
				var curtd = $(e).closest('td');
				var curindex = $(e).closest('tr').find('td').index(curtd[0]);
				if(curindex==2){//物资 供应商
					var vname = $(e).closest('span').attr('vname');
					if(vname!=''){
						$(e).closest('span').text(vname);
					}
				}else if(curindex!=6){
					$(e).closest('span').text($(e).val());
				}
			}
			
			//验证模板名称重复
			function checkName(){
				var flag = true;
				var vname = $.trim(stripscript($("#vname").val()));
				var pk_chktemplet = $.trim(stripscript($("#pk_chktemplet").val()));
				var param = {};
				param['vname'] = vname;
				param['pk_chktemplet'] = pk_chktemplet;
				$.post("<%=path%>/chktemplet/checkName.do",param,function(re){
					if(re=='NOOK'){
						flag = false;
					}
				})
				return flag;
			}
		</script>
	</body>
</html>
