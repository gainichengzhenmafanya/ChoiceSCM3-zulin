<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="elf" uri="/WEB-INF/tld/elfunc.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="lo" uri="/WEB-INF/tld/local.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
response.setHeader("Pragma","No-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.core.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.button.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.tab.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.toolbar.css"/>			
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/widget/lib.ui.window.css"/>
	<link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.menu-v.css"/>
    <link type="text/css" rel="stylesheet" href="<%=path%>/css/lib.ui.module.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/default/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="<%=path%>/image/themes/icon.css"/>
    <title>餐饮管理系统</title>
  	<style type="text/css">
  		.head {
			  position: relative;
			  height:78px;
			  width: 100%;
			  border-bottom: solid 1px #19418A;
			/*   background: #19418A; */
			  background: url('<%=path%>/image/login/panel_title.png') repeat-x center;
	  	}
		* html .logo { 
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale, src="<%=path%>/image/logo.png"); 
			background:none; 
		}
	  	.logo {
				position: absolute;
				width: 275px;
				height: 50px;
				top: 14px;
				left: 30px;
				z-index:200;
				background: url('<%=path%>/image/logo.png') repeat center center;
				float: left;
			}
		.workspace {
	  		position: relative;
	  		width: 20%;
	  		float: left;
	  	}
	  	.info {
	  		position: relative;
	  		width: 100%;
	  		float: left;
	  		height: 22px;
	  		background-color: #E7EEF1;
	  		padding-top:5px;
	  		color:#19418A;
	    	font-size:10px;
	      font-family:微软雅黑;
	  	}
	  	span{
	  		position: relative;
	  		color:#19418A;
	  		padding-top:5px;
	    	font-size:12px;
	      font-family:微软雅黑;
	  	}
	  	.module {
	  		position: relative;
	  		width: 100%;
	  		float: left;
	  		z-index:110;
	  	}
	  	
	  	.main {
	  		position: relative;
	  		width: 100%;
	  		float: left;
	  	}
	  	
	  	.foot {
	  		position: relative;
	  		height: 32px;
			  line-height: 32px;
			  text-align: center;
			  background: #19418A;
			  border-top: solid 1px #B2B2B2;
			  color: #FFFFFF;
			  clear: both;
	  	}
	  	a:link{
				text-decoration: none;
				color: gray;
			}
			
			a:visit{
				text-decoration: none;
			}
			
			.toolSys{
				position:relative;
				float:right;
/* 				width: 100%; */
				text-decoration: none;
			/*  background-color:red;   */
				height: 20px;
				border-color:#322679; 
				top:56px;
				
			}
			.tool_div{
				float:right;
				text-decoration: none;
				margin-right:6px;
			}
			.tool_span{
			 	top:2px;
				color:#FFF;
				text-decoration: none;
				margin-left: 4px;
			}
			#one{
/* 			margin-top:-25px; */
			}
			.toolbar {
				float:right;
				position: absolute;
				border-top: none;
				border-bottom: none;
				border-right: none;
				background-color: transparent ;
				text-align: center;
				margin: 55px 30px;
/* 				width: 100%; */
				filter:alpha(opacity=80);
				-moz-opacity:0.8;
				-khtml-opacity: 0.8;
				opacity: 0.8;
/* 				text-shadow: 0 1px 1px rgba(0,0,0,0.8); */
			}
			.layout-panel-south{
				
			}	
/* 			a.l-btn-plain{ */
/* 				border:1px solid #7eabcd;  */
/* 				text-align: center; */
/* 			} */
					
  	</style>
  	<style type="text/css">
		#winpop { width:200px; height:0px; position:absolute; right:0; bottom:0; border:1px solid #666; margin:0; padding:1px; overflow:hidden; display:none;}
		#winpop .title { width:100%; height:22px; line-height:20px; background:#2828FF; font-weight:bold; text-align:center; font-size:12px;}
		#winpop .con { width:100%; height:400px; line-height:70px; font-weight:bold; font-size:12px; color:#FF0080; text-align:center}
		.close { position:absolute; right:4px; top:-1px; color:#FFF; cursor:pointer}
	</style>
  </head>	
  
  <body class="easyui-layout" onload="myfunction();">
  	<div id="wait2" style="display:block;"></div>
	<div id="wait" style="display:block;">
		<img src="<%=path%>/image/loading_detail.gif" />
		&nbsp;
		<span style="color:white;font-size:15px;">数据加载中...</span>
	</div>  
  
    <div region="north"  style="height:78px;padding:0px;overflow:hidden">
  	   <div class="head">
	  	 	<div class="toolSys">
<%-- 		  	 	<div class="tool_div"><a href="javascript:if(confirm('确定要退出吗?')){window.close();}"><span class="tool_span"><fmt:message key="sign" /></span></a></div> --%>
<%-- 		  	 	<div class="tool_div"><a href="javascript:;" onclick="logout()"><span class="tool_span"><fmt:message key="log-out" /></span></a></div> --%>
<%-- 		  	 	<div class="tool_div"><a href="javascript:;" onclick="updateAccount()"><span class="tool_span"><fmt:message key="change-the-password" /></span></a></div> --%>
<%-- 	 	 		<div class="tool_div"><a href="javascript:;" onclick="getHelp();"><span class="tool_span"><fmt:message key="help-file" /></span></a></div> --%>
	 	 		<div class="tool_div"><a href="javascript:if(confirm('确定要退出吗?')){window.close();}"><img src="<%=path%>/image/login/tcdl.png"></img><span class="tool_span"><fmt:message key="sign" /></span></a></div>
	 	 		<div class="tool_div"><a href="javascript:;" onclick="logout()"><img src="<%=path%>/image/login/zxdl.png"></img><span class="tool_span"><fmt:message key="log-out" /></span></a></div>
	 	 		<div class="tool_div"><a href="javascript:;" onclick="updateAccount()"><img src="<%=path%>/image/login/xgmm.png"></img><span class="tool_span"><fmt:message key="change-the-password" /></span></a></div>
	 	 		<div class="tool_div"><a href="javascript:;" onclick="getHelp();"><img src="<%=path%>/image/login/bzwj.png"></img><span class="tool_span"><fmt:message key="help-file" /></span></a></div>
	  		</div>
	  	   <div class="logo"></div>
			   <div class="toolbar" id="one" style="background-color:none;background-image:none">
			   	<c:forEach var="tagpage" items="${tagList}" varStatus="status">
			   		<a href="#" class="easyui-linkbutton" style="background-color:white;height: 20px;" plain="true" iconCls="" onclick="showNewTag('${tagpage.id}','${lo:show(tagpage.name)}','${tagpage.url}')">
			   			<span title="${lo:show(tagpage.name)}" style="color:black;top:-1px; ">
			   				<c:set var="strName" value="${lo:show(tagpage.name)}"></c:set>${fn:substring(strName,0,2)}
			   			</span>
			   		</a>
			   	</c:forEach>
			   </div>
  	   </div>
    </div>
  	<div region="west" split="false" title="<span><fmt:message key="welcome" />：${account.name }(${account.names })</span>&nbsp;&nbsp;" style="width:190px;padding1:1px;overflow:hidden;">
	  	 <div class="workspace_" >
		  	 <div class="module"></div>
	  	 </div>
   </div>
<!--   	 <div class="main"></div>	 -->
 <!--  	 <div class="foot">Copyright&nbsp;&copy;&nbsp;辰森软件</div> -->
  	 <div region="center" style="overflow:hidden;">
		<div class="main"></div>
	</div>
    <div region="south" split="false" style="height:28px;padding:0px;background: url('<%=path%>/image/login/panel_foot.png') repeat center;">
		 
			<div region="center" style="text-align:left;vertical-align:center;color: #FFF;padding-top: 3px;">
				当前操作站点：【<label id="currFirmDes">${firmDes }</label>】
			</div>
	</div> 
  	 <div id="winpop" style="height: 0px;">
	 <div class="title">您有新的短消息！<span class="close" onclick="closeTips()">X</span></div>
	    <div class="con"><a id="mCount" href="#" style="text-decoration:none;color:#FF0080; " onclick="showMeg()" /></div>
	</div>
	  	 <script type="text/javascript" src="<%=path%>/js/jquery-1.7.1.min.js"></script>
	  	 <script type="text/javascript" src="<%=path%>/js/jquery.easyui.min.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.button.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.tab.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.toolbar.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/widget/lib.ui.window.js"></script>
		<script type="text/javascript" src="<%=path%>/js/widget/lib.ui.drag.js"></script>
		<script type="text/javascript" src="<%=path%>/js/lib.ui.core.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/lib.ui.menu.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/main.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/custom.js"></script>
		 <script type="text/javascript" src="<%=path%>/js/scm/addMul.js"></script>
  	 <script type="text/javascript">
  	 	var tabMain;
  	 	function showInfo(moduleId,moduleName,moduleUrl){
  	 		moduleName=moduleName.replace(/[_]/g, " ");
  	 		//-----------------------------------------
  	 		var tabArray = tabMain.tabArray;
  	 		for(var i in tabArray){
  	 			if(tabArray[i].li[0].innerText=='入库单填制审核'&&moduleName=='出库单填制审核'){
  	 				alert('请先关闭入库单填制审核页面！');
  	 				return;
  	 			}
  	 			if(tabArray[i].li[0].innerText=='出库单填制审核'&&moduleName=='入库单填制审核'){
  	 				alert('请先关闭出库单填制审核页面！');
  	 				return;
  	 			}
  	 		}
  	 		//-----------------------------------------
  	 		tabMain.addItem([{
	  				id: 'tab_'+moduleId,
	  				text: moduleName,
	  				title: moduleName,
	  				closable: true,
	  				
	  				content: '<iframe id="iframe_'+moduleId+'" name="iframe_'+moduleId+'" frameborder="0" src="<%=path%>'+moduleUrl+'"></iframe>'
	  			}
	  		]);
  	 		tabMain.show('tab_'+moduleId);
  	 	}
  	 	
  	 	function updateAccount(){
				updateAccountWin = $('body').window({
					id: 'window_updateAccount',
					title: '修改账号密码',
					content: '<iframe id="updateAccountFrame" frameborder="0" src="<%=path%>/accountPositn/updatePasssWord.do"></iframe>',
					width: 497,
					height: 205,
					draggable: true,
					isModal: true,
					confirmClose: false
				});
			}
			
			function getHelp(){
			//	alert('文档整理中...');return;
			<%-- location.href="<%=path%>/supply/downloadScmTemplate.do"; --%>
				$('body').window({
					id: 'window_showHelpFiles',
					title: '帮助文档',
					content: '<iframe id="showHelpFiles" frameborder="0" src="<%=path%>/supply/showHelpFiles.do"></iframe>',
					width: 650,
					height: '500px',
					confirmClose:false,
					draggable: false,
					isModal: true
				});
			}
			
			function logout(){
				location.href="<%=path%>/login/loginOut.do";	
			}
			
			function toReport(url){
				$('body').window({
					title: '<fmt:message key="print" />',
					content: '<iframe frameborder="0" src='+url+'></iframe>',
					width: '99%',
					height: '99%',
					draggable: true,
					isModal: true
					});
			}
  	 	$(document).ready(function(){
  	 		//调节iframe的大小
  	 		$('.layout-button-left').bind('click', function() {
  	 		 var rframe = $("#mainFrame");
             //ie7默认情况下会有上下滚动条，去掉上下15像素
             var h = $(window).width();
             rframe.width(h);

			});
			//设置toolbar样式居中
			var width = $(document.body).width()-$('#one').width()-60;
			$('#one').css("left",width/2);
  	 		
  	 		
  	 		//获取菜单栏需要显示的菜单项
  	 		var config = {};
 	      config.items = [];
	      <c:forEach var="module" items="${accountCache.moduleList}" varStatus="status">
	       	config.items['${module.parentModule.id}_${module.id}'] 
	       		= {text:'${lo:show(module.name)}',title:'${lo:show(module.name)}',handler:"showInfo('${module.id}',\'${lo:show(module.name)}\',\'${module.url}\')"};
	      </c:forEach>

 	      //创建菜单栏
  	 		new MenuView({
  	 			'container':$('.module'),
  	 			'items': config.items
  	 		});
  	 		
  	 		var height = $(document.body).height() 
					- $('.foot').outerHeight() 
	 				- $('.head').outerHeight();
  	 	
  	 		$('.main').height(height);
  	 		$('.workspace').height(height);
  	 		$('.module').height(height - $('.info').outerHeight()-38);
  	 		$('.module img').css({
  	 			"top": (height - $('.module img').outerHeight()) * 0.5,
  	 			"left": $('.module').outerWidth() - $('.module img').outerWidth() * 0.5
  	 		});
  	 		
  	 		
  	 		tabMain = $('.main').tab({
					width: '100%',
					height: '100%',
					items: [{
							id: 'tab_desktop',
							text: '<fmt:message key="my-desktop" />',
							title: '<fmt:message key="my-desktop" />',
							icon: {
								url: '<%=path%>/image/Button/set.png'
							},
							content: '<iframe id="iframe_account" name="iframe_account" frameborder="0" src="<%=path%>/mainInfo/list.do"></iframe>'
						}
					]
				});
  	 		$('.button_now').bind('click', function() {
  	 		  $(this).addClass("button-click");
  	 		});
  	 		$('.button_now').hover(
				function(){
					$(this).addClass('button-over');
				},
				function(){
					$(this).removeClass('button-over');
				}
			);
  	 		
  	 	});	//end $(document).ready();
  	 </script>
  	 <script>
	  	$(document).ready(function(){
  			//setInterval("openTips()",60000); 
	  	});
  		function openTips(){
	 		
	 		$.post("<%=path%>/message/unRead.do",function(data){
			var rs = data;
			if(rs>0){
				$("#mCount").html(rs+'条未读信息');
				var MsgPop=document.getElementById("winpop");
		  	 	var popH=parseInt(MsgPop.style.height);//将对象的高度转化为数字
		  	 	if(popH==0){
			  	 	MsgPop.style.display="block";//显示隐藏的窗口
			  	 	showMessageUp=setInterval("changeH('up')",2);
		  	 	}
			}
		})
	 		
	 	}
	 	function closeTips(){
  	 	var MsgPop=document.getElementById("winpop");
  	 	var popH=parseInt(MsgPop.style.height);//将对象的高度转化为数字
	 		hideMessageDown=setInterval("changeH('down')",2);
	 	}
	 	function changeH(str) {
  	 	var MsgPop=document.getElementById("winpop");
  	 	var popH=parseInt(MsgPop.style.height);
  	 	if(str=="up"){
	  	 	if (popH<=120){
	  	 		MsgPop.style.height=(popH+5).toString()+"px";
	  	 	}else{  
	  	 		clearInterval(showMessageUp);
	  	 	}
  	 	}
  	 	if(str=="down"){ 
	  	 	if (popH>=4){  
	  	 		MsgPop.style.height=(popH-5).toString()+"px";
	  	 	}else{ 
		  	 	clearInterval(hideMessageDown);   
		  	 	MsgPop.style.display="none";  //隐藏DIV
	  	 	}
  	 	}
	 	}
	 	function showMeg(){
	 		closeTips();
	 		showInfo('0c1a919d9a4b41e188f9b63b31db171c','我的消息','/message/list.do');
	 	}
	 	function myfunction(){
  			$("#wait2").css("display","none");
  			$("#wait").css("display","none");
	 	}
	 	//快捷操作
  		function showNewTag(moduleId,moduleName,moduleUrl){
  			if(moduleUrl){
  				showInfo(moduleId,moduleName,moduleUrl);
  			}else{
  				alert("未配置该模块路径，请在【系统管理】下的【模块管理】中配置路径。");
  			}
  		}
  	 </script>
  </body>
</html>
