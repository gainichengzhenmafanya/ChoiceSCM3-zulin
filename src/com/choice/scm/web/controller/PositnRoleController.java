package com.choice.scm.web.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Role;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PositnRoleConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;


@Controller
@RequestMapping("positnRole")
public class PositnRoleController {

	@Autowired
	PositnRoleService positnRoleService;
	@Autowired
	PositnService positnService;
		
	/**
	 * 根据角色 查询  仓位
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView findPositnByRole(ModelMap modelMap,Role role,Positn positn,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("PositnRoleList", positnRoleService.findRolePositnList(role.getId()));
	//	modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("roleId", role.getId());
		return new ModelAndView(PositnRoleConstants.LIST_POSITN_ROLE,modelMap);
	}
	/**
	 * 保存角色    拥有的仓位
	 * @param modelMap
	 * @param roleId
	 * @param positnIds
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/savePositnRole")
	public ModelAndView savePositnRole(ModelMap modelMap,String roleId,String positnIds) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listPositnId=Arrays.asList(positnIds.split(","));
		positnRoleService.savePositnRole(roleId, listPositnId);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/***  账号  仓位 访问   ***/ 
	/**
	 * 分配账号仓位范围
	 * @throws Exception
	 */
	@RequestMapping("/positnByAccount")
	public ModelAndView PositnByAccount(ModelMap modelMap,String accountId,Positn positn,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("PositnAccountList", positnRoleService.findRolePositnList(accountId));
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("accountId", accountId);
		modelMap.put("positn", positn);
		return new ModelAndView(PositnRoleConstants.LIST_POSITN_ACCOUNT,modelMap);
	}
	
	/**
	 * 保存账号 仓位范围
	 * @throws Exception
	 */
	@RequestMapping("/savePositnAccount")
	public ModelAndView savePositnAccount(ModelMap modelMap,String accountId,String positnIds) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listPositnId=Arrays.asList(positnIds.split(","));
		positnRoleService.savePositnRole(accountId, listPositnId);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 账号   所属分店
	 * @throws Exception
	 */
	@RequestMapping("/firmByAccount")
	public ModelAndView firmByAccount(ModelMap modelMap,String accountId,Positn positn,Page page,String firmCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("accountId", accountId);
		modelMap.put("positn", positn);
		modelMap.put("firmCode", firmCode);
		return new ModelAndView(PositnRoleConstants.LIST_Firm_ACCOUNT,modelMap);
	}
	/**
	 * 账号加入所属分店
	 */
	@RequestMapping("/saveFirmAccount")
	public ModelAndView saveFirmAccount(ModelMap modelMap,String accountId,String positnIds) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnRoleService.saveFrim(accountId, positnIds);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
