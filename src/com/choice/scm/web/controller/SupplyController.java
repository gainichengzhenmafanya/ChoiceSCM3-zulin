package com.choice.scm.web.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.UserService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostbomConstants;
import com.choice.scm.constants.FirmSupplyConstants;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SppriceQuickConstants;
import com.choice.scm.constants.SupplyConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Pubitem;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.domain.Tax;
import com.choice.scm.domain.Typ;
import com.choice.scm.domain.Typoth;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.CostbomService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.FileWorked;
/**
 * 物资编码
 * @author lehui
 *
 */
@Controller
@RequestMapping(value = "supply")
public class SupplyController {

	@Autowired
	private SupplyService supplyService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	PositnService positnService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private ExportExcel<Supply> exportExcel;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private CostbomService costbomService;
	@Autowired
	private UserService userService;
	
	/**
	 * 物资编码列表
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllSupply(ModelMap modelMap, Supply supply, GrpTyp grpTyp, Grp grp, Typ typ,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
	//	modelMap.put("supplyList", supplyService.findAllSupply(supply));
	//	modelMap.put("querySupply",supply);
		return new ModelAndView(SupplyConstants.LIST_SUPPLY, modelMap);
	}
	
	/**
	 * 选择物资编码列表
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chooseSupply")
	public ModelAndView chooseSupply(ModelMap modelMap,String use, Supply supply, GrpTyp grpTyp, Grp grp, Typ typ,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
	//	modelMap.put("supplyList", supplyService.findAllSupply(supply));
	//	modelMap.put("querySupply",supply);
		if("spprice".equals(use))
			return new ModelAndView(SupplyConstants.CHOOSE_SUPPLY_SPPRICE,modelMap);
		return new ModelAndView(SupplyConstants.CHOOSE_SUPPLY, modelMap);
	}
	/**
	 * 根据左侧的树   显示 右侧的物资编码
	 * @param modelMap
	 * @param level
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllSupplyByLeftGrpTyp(ModelMap modelMap, Supply supply, String level,
			String code, Page page, HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.TABLE_NAME_SUPPLY);
		String locale = session.getAttribute("locale").toString();
		dictColumns.setLocale(locale);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		//添加物资权限
//		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
//		supply.setAccountId(session.getAttribute("accountId").toString());
		//查询所有状态的物资（禁用和启用）
		supply.setSta("all");
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(SupplyConstants.TABLE_SUPPLY, modelMap);
	}
	
	/**
	 * 根据左侧的树   显示 右侧的物资编码(物资编码查询选择)
	 * @param modelMap
	 * @param level
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chooseTableSupply")
	public ModelAndView chooseTableSupply(ModelMap modelMap,Supply supply,String level,String use,String code,Page page,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.TABLE_NAME_SUPPLY);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		//添加物资权限
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		//获取所有供应商
		modelMap.put("deliverList", deliverService.findAllDelivers(null));
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(SupplyConstants.CHOOSE_TABLE_SUPPLY_SPPRICE);
	}

	/**
	 * 打开新增页面
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap, Supply supply, String level, String code) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positnList", positnService.findPositnForAddSupply(new Positn()));//仓位
		modelMap.put("positnexList", positnService.findPositnex(new Positn()));//加工间
		modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
		
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
		codeDes.setTyp("15");
		modelMap.put("attributeList", codeDesService.findCodeDes(codeDes));//物料属性
		modelMap.put("typothList", supplyService.findTypoth(null,null));//参考类别
		codeDes.setTyp("11");
		modelMap.put("typEasList", codeDesService.findCodeDes(codeDes));//报货类别
		codeDes.setTyp("17");
		modelMap.put("positn1List", codeDesService.findCodeDes(codeDes));//货架
		modelMap.put("taxList", supplyService.findTax(null));//税率
		if("3".equals(level) && !"".equals(code)){
			modelMap.put("sp_code", supplyService.getMaxSpcode(code));
		}
		
		return new ModelAndView(SupplyConstants.SAVE_SUPPLY_NEW, modelMap);
	}

	/***
	 * 拿到标准产品列表
	 * @param modelMap
	 * @param pubitem
	 * @param page
	 * @param vvcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchPubitemList")
	public ModelAndView searchPubitemList(ModelMap modelMap,Pubitem pubitem,Page page,String vvcode) throws Exception {
		String tele_boh = "choice7";
		if("tele".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			modelMap.put("pubitemList", costbomService.findAllPubitem(new Pubitem()));//标准产品
		}else if("choice3".equals(tele_boh) || "choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			if (pubitem == null) {
				modelMap.put("pubitemList", costbomService.findAllPubitem_boh(new Pubitem(),page));//标准产品
			} else {
				modelMap.put("pubitemList", costbomService.findAllPubitem_boh(pubitem,page));//标准产品
			}
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			modelMap.put("pubitemList", null);//标准产品
		}else if("nothing".equals(tele_boh)){
			modelMap.put("pubitemList", null);//标准产品
		}
		
		//update by jin shuai at 20160311
		modelMap.put("pageobj", page);
		
		//标准产品编码
		modelMap.put("vvcode", vvcode);
		
		return new ModelAndView(SupplyConstants.SEARCH_ALL_PUBITEM, modelMap);
	}
	
	/**
	 *获取所有的虚拟物料 信息
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectAllSupply_x")
	public ModelAndView selectAllSupply_x(ModelMap modelMap, Supply supply, String defaultCode, String defaultName,String defaultUnit,String defaultInit,String bom,Costbom costbom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		System.out.println(supply.getSp_init_x());
		modelMap.put("supplyList", supplyService.selectAllSupply_x(supply));
		modelMap.put("querySupply", supply);
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("defaultName", defaultName);
		modelMap.put("defaultUnit", defaultUnit);
		modelMap.put("defaultInit", defaultInit);
		if("BOM".equals(bom)){
			costbom.setUnit( java.net.URLDecoder.decode(costbom.getUnit(),"UTF-8"));
			modelMap.put("costbom", costbom);
			return new ModelAndView(SupplyConstants.SELECT_SUPPLY_XBOM,modelMap);
		}else{
			modelMap.put("costbom", costbom);
			return new ModelAndView(SupplyConstants.SELECT_SUPPLY_X,modelMap);
		}
	}
	
	
	/**
	 *将选择的虚拟物料返回到添加物资页面 
	 */
	@RequestMapping(value="/returnSupply_x")
	public ModelAndView returnSupply_x(ModelMap modelMap,Supply supply)throws Exception{
		//是否是  虚拟物料
		supply.setSp_name_x(java.net.URLDecoder.decode(supply.getSp_name_x(),"UTF-8"));
		supply.setUnit_x(java.net.URLDecoder.decode(supply.getUnit_x(),"UTF-8"));
		modelMap.put("supplys", supply);
		return new ModelAndView(SupplyConstants.SAVE_SUPPLY, modelMap);
	}
	
	/**
	 * 获取虚拟物料对应的所有实际物料
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toActSupply")
	public ModelAndView toActSupply(ModelMap modelMap,HttpSession session,Supply supply,Costbom costbom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		costbom.setUnit( java.net.URLDecoder.decode(costbom.getUnit(),"UTF-8"));
		modelMap.put("costbom",costbom);
		costbom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if ("E".equals(costbom.getCurStatus())) {//edit
			modelMap.put("supplyList", supplyService.findSupplyBySp_code_x_edit(costbom));
		}else{//add
			modelMap.put("supplyList", supplyService.findSupplyBySp_code_x_add(supply));
		}
		
		return new ModelAndView(SupplyConstants.TABLESUPPLYBYSP_CODE_X,modelMap);
	}
	
	/**
	 * 打开复制（新增）页面
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/copy")
	public ModelAndView copy(ModelMap modelMap, Supply supply, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positnList", positnService.findPositnForAddSupply(new Positn()));//仓位
		modelMap.put("positnexList", positnService.findPositnex(new Positn()));//加工间
		modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
		codeDes.setTyp("15");
		modelMap.put("attributeList", codeDesService.findCodeDes(codeDes));//物料属性
		modelMap.put("typothList", supplyService.findTypoth(null,null));//参考类别
		codeDes.setTyp("11");
		modelMap.put("typEasList", codeDesService.findCodeDes(codeDes));//报货类别
		codeDes.setTyp("17");
		modelMap.put("positn1List", codeDesService.findCodeDes(codeDes));//货架
		modelMap.put("taxList", supplyService.findTax(null));//税率
		//帐套
	    supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		Supply supply_= supplyService.findSupplyById(supply);
		//自动获取第三类下的最大编码
		String code_ = null;
		code_ = supplyService.getMaxSpcode(supply.getSp_code().substring(0, 5));
		supply_.setSp_code(code_);
		supply_.setSp_init(null);
		supply_.setSp_name(null);
		//查询物资的多单位
		SupplyUnit supplyUnit = new SupplyUnit();
		supplyUnit.setSp_code(supply.getSp_code());
		List<SupplyUnit> supplyunitList = supplyService.findAllSupplyUnit(supplyUnit);
		modelMap.put("supplyunitList", supplyunitList);
		modelMap.put("supply", supply_);//仓位
		return new ModelAndView(SupplyConstants.COPY_SUPPLY_NEW, modelMap);
	}
	
	/**
	 * 打开选择物资类别
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/selectGrptyp")
	public ModelAndView selectGrptyp(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.SELECT_SUPPLY, modelMap);
	}
	
	/**
	 * 保存物资编码
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView save(ModelMap modelMap, Supply supply,HttpSession session,String firmCodes,String orderno,String disunit) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String  acct=session.getAttribute("ChoiceAcct").toString();
		String sp_type=supply.getSp_type();//三级 code
		Typ typ=(Typ)grpTypService.findByCode(3, sp_type, acct);
		//中类
		Grp grp=(Grp)grpTypService.findByCode(2, typ.getGrp(), acct);
		//大类
		GrpTyp grptyp=(GrpTyp)grpTypService.findByCode(1, typ.getGrptyp(), acct);
		Tax tax=supplyService.findTax(supply.getTaxId()).get(0);
		BigDecimal bd = new BigDecimal(String.valueOf(tax.getTax()));
		supply.setTax(bd.doubleValue());//税率
		supply.setTaxdes(tax.getTaxdes());//税率名
		if (supply.getTypoth()==null||supply.getTypoth()==""){
			supply.setTypothdes("");
		}else{
			supply.setTypothdes(supplyService.findTypoth(supply.getTypoth(),null).get(0).getDes());//参考类名
		}
		supply.setGrp(grp.getCode());		//加入中类
		supply.setGrpdes(grp.getDes());
		supply.setGrptyp(grptyp.getCode());		//加入大类
		supply.setGrptypdes(grptyp.getDes());
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		supplyService.saveSupply_new(supply, session.getAttribute("accountId").toString(),firmCodes,orderno,disunit);
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 打开修改页面
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, Supply supply,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positnList", positnService.findPositnForAddSupply(new Positn()));//仓位
		modelMap.put("positnexList", positnService.findPositnex(new Positn()));//加工间
		modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
		codeDes.setTyp("11");
		modelMap.put("typEasList", codeDesService.findCodeDes(codeDes));//报货类别
		codeDes.setTyp("15");
		modelMap.put("attributeList", codeDesService.findCodeDes(codeDes));//物料属性
		codeDes.setTyp("17");
		modelMap.put("positn1List", codeDesService.findCodeDes(codeDes));//货架
		modelMap.put("typothList", supplyService.findTypoth(null,null));//参考类别
		modelMap.put("taxList", supplyService.findTax(null));//税率
		//帐套
	    supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		Supply supply_ = supplyService.findSupplyById(supply);
		//查询物资的多单位
		SupplyUnit supplyUnit = new SupplyUnit();
		supplyUnit.setSp_code(supply_.getSp_code());
		List<SupplyUnit> supplyunitList = supplyService.findAllSupplyUnit(supplyUnit);
		//修改的时候要判断配送单位是不是已经引用了 引用的不可以修改也不可以删除
		for(SupplyUnit su : supplyunitList){
			if("Y".equals(su.getYnDisunit())){
				su.setYnDelete(supplyService.findSupplyUnitByPositnspcode(su));
			}
		}
		modelMap.put("supply",supply_);
		modelMap.put("supplyunitList", supplyunitList);
		return new ModelAndView(SupplyConstants.UPDATE_SUPPLY_NEW, modelMap);
		
	}
	
	/**
	 * 修改物资编码信息
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, Supply supply,HttpSession session,String orderno,String disunit)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String  acct=session.getAttribute("ChoiceAcct").toString();//帐套
		String sp_type=supply.getSp_type();//三级 code
		Typ typ=(Typ)grpTypService.findByCode(3, sp_type, acct);
		//中类
		Grp grp=(Grp)grpTypService.findByCode(2, typ.getGrp(), acct);
		//大类
		GrpTyp grptyp=(GrpTyp)grpTypService.findByCode(1, typ.getGrptyp(), acct);
		Tax tax=supplyService.findTax(supply.getTaxId()).get(0);
		BigDecimal bd = new BigDecimal(String.valueOf(tax.getTax()));
		supply.setTax(bd.doubleValue());//税率
		supply.setTaxdes(tax.getTaxdes());//税率名
		supply.setTypothdes(supply.getTypoth() == null?"":supplyService.findTypoth(supply.getTypoth(),null).get(0).getDes());//参考类名   防止为空报错wjf
		supply.setGrp(grp.getCode());		//加入中类
		supply.setGrpdes(grp.getDes());
		supply.setGrptyp(grptyp.getCode());		//加入大类
		supply.setGrptypdes(grptyp.getDes());
		//帐套
	    supply.setAcct(acct);
	    supplyService.updateSupply_new(supply,orderno, disunit);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DictColumns dictColumns = new DictColumns();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.TABLE_NAME_SUPPLY);
		String locale = session.getAttribute("locale").toString();
		dictColumns.setLocale(locale);
		modelMap.put("objBean", "supply");
		modelMap.put("tableName", ScmStringConstant.TABLE_NAME_SUPPLY);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));
		dictColumns.setAccountId("");
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByAccount(dictColumns,null));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 跳转到批量修改物资参考类别 页面 wangjie 2014.12.29
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/toBatchTypoth")
	public ModelAndView toBatchTypoth(ModelMap modelMap,String checkedcode,String menuname) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//查询所有物资参考类别类型
		List<Typoth> typothList = supplyService.findTypoth(null,null);
		modelMap.put("typothList", typothList);
		modelMap.put("checkedcode", checkedcode);
		modelMap.put("menuname", menuname);
		return new ModelAndView(ScmStringConstant.TOBATCHTYPOTH,modelMap);
	}
	/**
	 * 批量修改物资参考类别
	 * @param supply
	 * @param checkedcode 选择的物资
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/batchTypoth")
	public ModelAndView batchTypoth(Supply supply,String checkedcode) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.batchTypoth(supply, checkedcode);
		return new ModelAndView(StringConstant.ACTION_DONE, null);
	}
	
	/**
	 * 根据输入的条件查询 前n条记录
	 */
	
	@RequestMapping(value = "/findTop", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findTop(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		return supplyService.findSupplyListTopN(supply);
	}
	
	/**
	 * 根据输入的条件
	 * 查询供应商供应物资前十条记录
	 */
	@RequestMapping(value = "/findTopByGys", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findTopByGys(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		return supplyService.findGysSpListTopN(supply);
	}
	
	/**
	 * 根据输入的条件查询虚拟物料的前n条记录
	 * @return 虚拟物料
	 * @author wangchao
	 */
	
	@RequestMapping(value = "/findTop_x", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findTop_x(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));//这里貌似没用吧?????wjf
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		List<Supply> list = supplyService.findSupplyListTopN_x(supply);
		return list;
	}
	
	/**
	 * 根据输入的条件查询 前n条记录,用于申购时，可按照分店权限筛选
	 */
	
	@RequestMapping(value = "/findTop1", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findTop1(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		return supplyService.findSupplyListTopN(supply);
	}
	
	/**
	 * 根据条件查询售价
	 */
	@RequestMapping(value = "/findSprice", method=RequestMethod.POST)
	@ResponseBody
	public  SppriceSale findSprice(SppriceSale spprice, String typ, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
		double price = 0;
		SppriceSale sprice = supplyService.findSprice(spprice);
		if (sprice == null|| sprice.getPrice() == null) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			supply.setSp_code(spprice.getSupply().getSp_code());
			Supply sup=supplyService.findSupplyById(supply);
			//wangjie 2014年10月28日 09:20:11  取物资的售价
			if (sup.getPricesale()!=null && sup.getPricesale() != 0) {
				price = sup.getPricesale();
			}else{
				price = sup.getSp_price();
			}
		}else {
			price = sprice.getPrice().doubleValue();
		}
		spprice.setPrice(price);
		return spprice;
	}
	
	
	/**
	 * 根据条件查询售价
	 */
	@RequestMapping(value = "/findSprice_x", method=RequestMethod.POST)
	@ResponseBody
	public  SppriceSale findSprice_x(SppriceSale spprice, String typ, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
		double price = 0;
		SppriceSale sprice = supplyService.findSprice(spprice);
		if (sprice == null|| sprice.getPrice() == null) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			supply.setSp_code_x(spprice.getSupply().getSp_code());
			supply.setIs_supply_x("Y");//使用虚拟物料
			Supply sup=supplyService.findSupplyById_x(supply);
			if (sup.getSp_price()!=null) {
				price = sup.getSp_price();
			}
		}else {
			price = sprice.getPrice().doubleValue();
		}
		spprice.setPrice(price);
		return spprice;
	}
	
	/**
	 * 根据条件查询报价
	 */
	@RequestMapping(value = "/findBprice", method=RequestMethod.POST)
	@ResponseBody
	public Spprice findBprice(Spprice spprice, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		String acct = session.getAttribute("ChoiceAcct").toString();
		spprice.setAcct(acct);
		//1.查报价
		Spprice sp = supplyService.findBprice(spprice);
		spprice.setPrice(new BigDecimal(0));
		spprice.setSta("N");
		if (null == sp||null == sp.getPrice()) {
			Supply supply = new Supply();
			supply.setAcct(acct);
			supply.setSp_code(spprice.getSp_code());
			supply = supplyService.findSupplyById(supply);
			if (supply.getSp_price()!=null) {
				spprice.setPrice(new BigDecimal(supply.getSp_price()));
			}
		} else {
			spprice.setPrice(sp.getPrice());
			spprice.setSta("Y");
		}
		//2.查税率
		if(null != spprice.getDeliver()){
			Supply s = new Supply();
			s.setAcct(acct);
			s.setSp_code(spprice.getSp_code());
			s.setDeliver(spprice.getDeliver());
			s = supplyService.findTaxByDeliverSpcode(s);
			spprice.setTax(s.getTax());
			spprice.setTaxdes(s.getTaxdes());
		}
		return spprice;
	}
	
	/**
	 * 跳转到导入页面
	 */
	@RequestMapping("/importSupply")
	public ModelAndView importSupply(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyConstants.IMPORT_SUPPLY,modelMap);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = supplyService.upload(request, response);  //将文件上传到temp文件夹
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(SupplyConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	@ResponseBody
	public List<String> importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		System.out.println("上传文件路径： "+ realFilePath);
		List<String> listError=supplyService.check(realFilePath, session.getAttribute("accountId").toString());
		return listError;
	}
	
	/**
	 * 下载模板信息
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.downloadTemplate(response, request);
	}
	
	/**
	 * 读取操作文档详细列表
	 * @param response
	 * @param request
	 * @param modelMap
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/showHelpFiles")
	public ModelAndView showHelpFiles(HttpServletResponse response,HttpServletRequest request,ModelMap modelMap) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("files", supplyService.showHelpFiles(response, request));
		return new ModelAndView(LoginConstants.HELP_FILES,modelMap);
	}
	
	/**
	 * 下载操作文档
	 */
	@RequestMapping(value = "/downloadScmTemplate")
	public void downloadScmTemplate(HttpServletResponse response,HttpServletRequest request,String fileName) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		FileWorked.downloadTemplate(response, request, "\\template\\"+fileName);
	}
	
	/**
	 * 打印
	 * @param level
	 * @param code
	 * @param page
	 * @return
	 * @throws CRUDException
	 * @throws IOException 
	 */
	@RequestMapping("/toReport")
	@ResponseBody
	public void toReport(String level,String code,Page page,Supply supply,String type,String reportFrom,String reportTo,HttpSession session,HttpServletRequest request,HttpServletResponse response)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		int fromnum = 0,tonum = 0;//定义开始结束
		//根据传入的页数范围计算数据大小
		if(null!=reportFrom){
			fromnum = (Integer.parseInt(reportFrom)-1)*page.getPageSize();
//			page.setNowPage(Integer.parseInt(reportFrom));
		}
		if(null!=reportFrom && null!=reportTo){
			tonum = (Integer.parseInt(reportTo)-Integer.parseInt(reportFrom)+1)*page.getPageSize() + fromnum;
//			page.setMaxSize((Integer.parseInt(reportTo)-Integer.parseInt(reportFrom)+1)*page.getPageSize());
		}
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		
		//用于mysql向后台传值jinshuai
		supply.setPageSize(page.getPageSize());
		
		String fileName="物资编码";
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		String locale = session.getAttribute("locale").toString();
		System.out.println("=============="+locale);
		dictColumns.setLocale(locale);
		dictColumns.setTableName(ScmStringConstant.TABLE_NAME_SUPPLY);
	//	modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		List<Supply> supplys = supplyService.findAllSupplyExport(supply, level,code,fromnum,tonum);
		exportExcel.creatWorkBook(response.getOutputStream(),supplys, "物资编码", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));

//	 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
//	        String report_name=new String("供应链系统物资编码报表"); 
//	        String report_dw=new String(" "); 
//	        SimpleDateFormat sdt=new SimpleDateFormat("yyyy-MM-dd");
//	        String report_time=sdt.format(new Date());
//	        parameters.put("report_name", report_name); 
//	        parameters.put("report_dw", report_dw); 
//	        parameters.put("report_time", report_time); 
//        modelMap.put("parameters", parameters);
//        modelMap.put("reportUrl", SupplyConstants.REPORT_URL);
//      
//	 	String url=ScmStringConstant.IREPORT_PDF;//跳转页面
//	 	if(type.equals("html")){
//	 		url=ScmStringConstant.IREPORT_HTML;
//	 	}else if(type.equals("excel")){
//	 		url=ScmStringConstant.IREPORT_EXCEL;
//	 	}else if(type.equals("word")){
//	 		url=ScmStringConstant.IREPORT_WORD;
//	 	}
//		return new ModelAndView(url,modelMap);
	}
	
	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 删除物资编码信息
	 * @param modelMap
	 * @param supplyId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String supplyId,Supply supply,String level,String code,Page page,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//先判断是否有使用状态为Y的  ，有的话返回
		String result=supplyService.findAllSupplySfyyByIds(supplyId);
		if("".equals(result)){
			List<String> ids = Arrays.asList(supplyId.split(","));
			supplyService.deleteSupply(ids);
		}else {
			modelMap.put("result", result);//删除提示
		}
		
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.TABLE_NAME_SUPPLY);
		String acct=session.getAttribute("ChoiceAcct").toString();
		supply.setAcct(acct);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_TABLE_DEFAULT_COLUMNS));
	    modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(SupplyConstants.TABLE_SUPPLY, modelMap);
	}
	
	/**
	 * 根据sp_code获取物资信息并返回json对象
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Supply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		return supplyService.findSupplyById(supply);
	}
	
	/**
	 * 安卓手机用   根据条形码     sp_code1获取物资   入库要返回报价  信息并返回json对象     
	 */
	@RequestMapping("/finSupplyBySp_codeWAP")
	@ResponseBody
	public Object finSupplyBySp_codeWAP(Supply supply,HttpSession session,String jsonpcallback,String typ) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
//		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null==supply){
			return null;
		}
		Supply  supply1=supplyService.findSupplyById(supply);
		Map<String, String> map1=new HashMap<String, String>();
		if(null!=supply1){
			map1.put("sp_code", supply1.getSp_code());
			map1.put("sp_name", supply1.getSp_name());
			map1.put("sp_desc", supply1.getSp_desc());
			map1.put("unit", supply1.getUnit());
			map1.put("sp_code1", supply1.getSp_code1());
		}
		if("RK".equals(typ)){//入库需要返回一个price  报价  
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Spprice spprice=new Spprice();
			spprice.setSp_code(supply1.getSp_code());
			spprice.setArea(supply.getSp_position());
			spprice.setDeliver(supply.getDeliver());
			spprice.setMadet(sdf.format(new Date())); 
			//1.查报价
			Spprice sp = supplyService.findBprice(spprice);
			if (null == sp||null == sp.getPrice()) {//没有报价传标准价
				map1.put("price", supply1.getSp_price()+"");
			}else{
				map1.put("price", sp.getPrice()+"");
			}
		}
		return  JSONObject.fromObject(map1).toString();
	}
	
	/**
	 * 根据sp_code_x获取物资信息并返回json对象
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById_x")
	@ResponseBody
	public Object findById_x(Supply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		supply.setSp_code_x(supply.getSp_code());
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		return supplyService.findSupplyById_x2(supply);
	}
	
	/**
	 * 弹出物资选择框                左侧
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeft")
	public ModelAndView selectSupplyLeft(ModelMap modelMap,HttpSession session,String positnex,String defaultCode, String is_supply_x,String single,String ex1,String sp_position)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		modelMap.put("ex1", ex1);//是否是只查询加工品  wjf
		modelMap.put("sp_position", sp_position);//部分需要根据仓位物资属性进行过滤   2015.6.11   xlh 
		modelMap.put("positnex", positnex);
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){//虚拟物料  并且需要分组合并
			modelMap.put("is_supply_x", "Y_group");//虚拟物资编码
			return new ModelAndView(CostbomConstants.SELECT_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(CostbomConstants.SELECT_SUPPLY, modelMap);
		}
	//	modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(null,"1","", new Page()));
	}
	
	/**
	 * 弹出物资选择框                左侧   ----  用于安全库存到加工间和登记入库
	 * @param modelMap
	 * @param is_djrk 是否只显示半成品物资  1表示是 (登记入库需要只显示半成品物资，安全库存到加工间要显示全部的)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeft_safe")
	public ModelAndView selectSupplyLeft_safe(ModelMap modelMap,HttpSession session,String defaultCode, String is_supply_x,String single,String is_djrk)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		if(is_djrk!=null && "1".equals(is_djrk)){
			modelMap.put("grpTypList", grpTypService.findGrpTypByDes("半成品"));//大类
		}else{
			modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		}
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		modelMap.put("is_djrk", is_djrk);
		return new ModelAndView(CostbomConstants.SELECT_SUPPLY_SAFE, modelMap);
	}
	
	/**
	 * 弹出物资选择框                左侧
	 * @param modelMap
	 * @return--用于虚拟模板添加
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeft_x")
	public ModelAndView selectSupplyLeft_x(ModelMap modelMap,HttpSession session,String defaultCode, String is_supply_x,String single)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		return new ModelAndView(CostbomConstants.SELECT_SUPPLY_MODX, modelMap);
	}
	
	/**
	 * 弹出物资选择框                左侧
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllSupplySXS")
	public ModelAndView findAllSupplySXS(ModelMap modelMap, HttpSession session, String defaultCode)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Supply supply = new Supply();
		supply.setSp_code(defaultCode);
		supply.setAcct(acct);
		modelMap.put("supplyList", supplyService.findAllSupplySXS(supply));//大类
		return new ModelAndView(CostbomConstants.SELECT_S_X_S, modelMap);
	}
	
	/**
	 * 弹出物资选择框                右侧
	 * @param modelMap
	 * @return    is_supply_x   虚拟物资编码--用于虚拟模板添加
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply_x")
	public ModelAndView selectSupply_x(ModelMap modelMap, Supply supply, String level, String code,String single ,Page page,
			HttpSession session, String defaultCode, String is_supply_x)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){
			supply.setIs_supply_x("Y_group");//虚拟物料  且是 分组合并
		}
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));//这里貌似也没用吧  wjf
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp_x(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single );//是否单选 默认单选  false多选
		modelMap.put("is_supply_x", "Y_group");
		return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY_MODX, modelMap);
	}
	
	/**
	 * 弹出物资选择框                右侧
	 * @param modelMap
	 * @return    is_supply_x   虚拟物资编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply")
	public ModelAndView selectSupply(ModelMap modelMap, Supply supply, String positnex,String level, String code,String single ,Page page,
			HttpSession session, String defaultCode, String is_supply_x)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setPositnex(positnex);
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){
			supply.setIs_supply_x("Y_group");//虚拟物料  且是 分组合并
		}
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		//wangjie 2014年11月21日 14:04:40  报货单、出库单填制提交 物资支持多选
		modelMap.put("single", single );//是否单选 默认单选  false多选
		if(null!=is_supply_x && ("Y_group").equals(is_supply_x)){//是否虚拟物料
			modelMap.put("is_supply_x", "Y_group");
			return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY, modelMap);
		}
	}
	
	/**
	 * 弹出物资选择框                右侧----  用于安全库存到加工间和登记入库
	 * @param modelMap
	 * @return    is_supply_x   虚拟物资编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply_safe")
	public ModelAndView selectSupply_safe(ModelMap modelMap, Supply supply, String level, String code,String single ,Page page,
			HttpSession session, String defaultCode, String is_supply_x)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setEx1("Y");
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){
			supply.setIs_supply_x("Y_group");//虚拟物料  且是 分组合并
		}
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("single", single );//是否单选 默认单选  false多选
		return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY, modelMap);
	}
	
	/**
	 * 查询多条物资    弹出框 上
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addSupplyBatch")
	public ModelAndView addPositnBatch(ModelMap modelMap,String defaultName,String defaultCode,String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		if(type==null){
			return new ModelAndView(SupplyConstants.ADD_SUPPLYBATCH, modelMap);
		}else if ("quick".equals(type)){
			return new ModelAndView(SppriceQuickConstants.ADD_SUPPLY_BATCH_QUICK, modelMap);
		}else{
			return new ModelAndView(FirmSupplyConstants.ADD_SUPPLY_BATCH_FIRM, modelMap);
		}
	}
	
	/**
	 * 查询多条物资    弹出框 下左
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectNSupply")
	public ModelAndView selectNSupply(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.SELECT_NSUPPLY, modelMap);
	}
	
	/**
	 * 查询多条物资    弹出框 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNSupply")
	public ModelAndView selectTableNSupply(ModelMap modelMap, Supply supply, String level, String code, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
//		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(SupplyConstants.SELECT_TABLENSUPPLY, modelMap);
	}
	
	/**
	 * 获取当前物资列别中编码的最大值+1
	 * @param code
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/getMaxSpcode")
	@ResponseBody
	public Object getMaxSpcode(String code) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return supplyService.getMaxSpcode(code);
	}
	/**
	*获取物资大类 
	*
	*/
	@RequestMapping(value = "/findDivisionSupply")
	public ModelAndView chooseSupply(ModelMap modelMap,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		return new ModelAndView(SupplyConstants.DIVISIONSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资中类
	 */
	@RequestMapping(value="/findGroupTyp")
	public ModelAndView findGroupTyp(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		return new ModelAndView(SupplyConstants.GROUPSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资小
	 */
	@RequestMapping(value="/findSmallClass")
	public ModelAndView findSmallClass(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyConstants.CLASSSUPPLY, modelMap);
	}
	/**
	 * 根据大类查询该大类下的所有物资
	 * 
	 */
	@RequestMapping(value="/findGrp")
	@ResponseBody
	public  String findGrp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Supply> listSupply = grpTypService.findGrp(code,session.getAttribute("ChoiceAcct").toString());
		String codeStr = "";
		String nameStr = "";
		String str = "";
		if(listSupply!=null){
			for(int i=0;i<listSupply.size();i++){
	 			codeStr += listSupply.get(i).getSp_code()+",";
	 			nameStr += listSupply.get(i).getSp_name()+",";
			}
			codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
			nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
			str = codeStr+"_"+nameStr;
		}
		return str;
		 
	}
	
	/**
	 * 根据中类查询该中类下的所有物资
	 * 
	 */
	@RequestMapping(value="/findTyp")
	@ResponseBody
	public  String findTyp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Supply> typList = grpTypService.findTyp(code,session.getAttribute("ChoiceAcct").toString());
		String codeStr = "";
		String nameStr = "";
		String str = "";
		if(typList!=null){
		for(int i=0;i<typList.size();i++){
 			codeStr += typList.get(i).getSp_code()+",";
 			nameStr += typList.get(i).getSp_name()+",";
		}
		codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
		nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
		str = codeStr+"_"+nameStr;
		}
		return str;

	}
	
	/**
	 * 根据小类查询该小类下的所有物资 
	 * 
	 */
	@RequestMapping(value="/findSupplyByTyp")
	@ResponseBody
	public  String findSupplyByTyp(ModelMap modelMap,HttpSession session,String code,String level)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		 List<Supply> supplyList = grpTypService.findSupplyByTyp(code,session.getAttribute("ChoiceAcct").toString());
		 String codeStr = "";
			String nameStr = "";
			String str = "";
		if(supplyList!=null){
			for(int i=0;i<supplyList.size();i++){
	 			codeStr += supplyList.get(i).getSp_code()+",";
	 			nameStr += supplyList.get(i).getSp_name()+",";
			}
			codeStr = codeStr.substring(0,codeStr.lastIndexOf(","));
			nameStr = nameStr.substring(0,nameStr.lastIndexOf(","));
			str = codeStr+"_"+nameStr;
		}
			return str;
	}
	
	/**
	 * 进入 物资图片维护界面
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toUploadSupplyImg")
	public ModelAndView toUploadSupplyImg(ModelMap modelMap,HttpServletRequest request, Supply supply) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supply", supplyService.findSupplyById(supply));
		String filePath = request.getSession().getServletContext().getRealPath("/image/scm/supply")+"\\"+supply.getSp_code()+".jpg";
		File file = new File(filePath);
		if(file.exists()){
			modelMap.put("fileExists", "Y");
		}else{
			modelMap.put("fileExists", "N");
		}
		return new ModelAndView(SupplyConstants.UPLOAD_SUPPLY_IMG, modelMap);
	}
	/***
	 * 物资图片上传
	 * @param modelMap
	 * @param request
	 * @param supply
	 * @return
	 */
	@RequestMapping("/uploadSupplyImg")
	public ModelAndView uploadSupplyImg(ModelMap modelMap,HttpServletRequest request,Supply supply) throws Exception{
		String realFilePath = "";//图片路径
		String fileuploadPath = request.getSession().getServletContext().getRealPath("/image/scm/supply")+"\\";
		//上传新的图片
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("image");
		//大图
		if(file.getSize() != 0){
			if(file.getSize()/1024 > 600){ //如果上传图片大于600K,默认600K,不能上传
				modelMap.put("message", "上传失败！上传文件不得大于600K!");
				String filePath = request.getSession().getServletContext().getRealPath("/image/scm/supply")+"\\"+supply.getSp_code()+".jpg";
				File imgfile = new File(filePath);
				if(imgfile.exists()){
					modelMap.put("fileExists", "Y");
				}else{
					modelMap.put("fileExists", "N");
				}
			}else{
				//删除原来的图片
				File filedel = new File(fileuploadPath + supply.getSp_code()+".jpg");
				filedel.delete();
				String realFileName = file.getOriginalFilename();
				if(realFileName != null && realFileName != ""){
					String strend=realFileName.substring(realFileName.length()-5, realFileName.length());
					String endname=strend.substring(strend.indexOf("."), strend.length());
					realFileName=supply.getSp_code()+endname;
					realFilePath = fileuploadPath + realFileName;
					File uploadFile = new File(realFilePath);
					try {
						file.transferTo(uploadFile);
						modelMap.put("message", "上传成功！");
						modelMap.put("fileExists", "Y");
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		modelMap.put("supply", supply);
		return new ModelAndView(SupplyConstants.UPLOAD_SUPPLY_IMG, modelMap);
	}
	
	/**
	 * 检查一种虚拟物资对应多种实际物资，成本单位是否一样
	 * @param sp_code_x
	 * @return
	 */
	@RequestMapping("/checkUnit")
	@ResponseBody
	public Object checkUnit(String sp_code_x,String sp_code) throws Exception{
		return null;
	}
	
	/**
	 * 检查虚拟物资编码和名称是否重复
	 * @param sp_code_x
	 * @param sp_name_x
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/checkSupply_x")
	@ResponseBody
	public int checkSupply_x(String sp_code_x,String sp_name_x) throws Exception{
		return 0;
	}
	
	/**
	 * 查询所有的虚拟物资信息 
	 * @param supply 虚拟物资查询条件
	 * @return
	 */
	@RequestMapping("/searchSupply_x")
	public ModelAndView searchSupply_x(ModelMap modelMap,Supply supply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listSupply_x", supplyService.selectSupply_x(supply));
		return new ModelAndView(SupplyConstants.SEARCHSUPPLY_X, modelMap);
	}
	
	/**
	 * 跳转到修改虚拟物料的界面
	 * @param modelMap
	 * @param sp_code_x
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toUpdateSupply_x")
	public ModelAndView toUpdateSupply_x(ModelMap modelMap,String sp_code_x) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Supply supply = new Supply();
		supply.setSp_code_x(sp_code_x);
		modelMap.put("supply", supplyService.toUpdateSupply_x(supply));
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
		return new ModelAndView(SupplyConstants.UPDATESUPPLY_X, modelMap);
	}
	
	/**
	 * 修改虚拟物料
	 * @param modelMap
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByUpdateSupply_x")
	public ModelAndView saveByUpdateSupply_x(ModelMap modelMap,Supply supply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.saveByUpdateSupply_x(supply);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/***
	 * 查询虚拟物料下实际物料用量
	 * @param modelMap
	 * @param supply
	 * @param sp_name_x
	 * @param sp_code_x
	 * @param sp_init_x
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/downSupply")
	public ModelAndView downSupply(ModelMap modelMap,Supply supply, String sp_name_x,String sp_code_x,String sp_init_x, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Supply sup = new Supply();
		sup.setSp_code_x(sp_code_x);
//		supply.setSp_name_x(sp_name_x);
		sup.setSp_init_x(sp_init_x);
		modelMap.put("downSupplyList", supplyService.downSupply(sup));
		modelMap.put("pageobj", page);
		return new ModelAndView(SupplyConstants.DOWN_COSTBOM,modelMap);
	}
	
	/**
	 * 批量删除菜品----虚拟物料
	 * @param modelMap
	 * @param session
	 * @param supply
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteSupplyBySp_code_x")
	public ModelAndView deleteSupplyBySp_code_x(ModelMap modelMap,HttpSession session,Supply supply,Costbom costbom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		costbom.setAcct(acct);
		modelMap.put("costbomList", costbomService.findAllCostbombomByLeftId_x(costbom));
		return new ModelAndView(SupplyConstants.TABLESUPPLYLISTBYSP_CODE_X,modelMap);
	}
	
	/**
	 * 批量删除菜品----实际物料
	 * @param modelMap
	 * @param session
	 * @param supply
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteSupplyBySp_code")
	public ModelAndView deleteSupplyBySp_code(ModelMap modelMap,HttpSession session,Supply supply,Costbom costbom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		costbom.setAcct(acct);
		modelMap.put("costbomList", costbomService.findAllCostbombomByLeftId(costbom));
		modelMap.put("costbom", costbom);
		return new ModelAndView(SupplyConstants.TABLESUPPLYLISTBYSP_CODE,modelMap);
	}

	/***
	 * 直接加入分店物资属性
	 * @param modelMap
	 * @param page
	 * @param ids
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toJoinPositn")
	public ModelAndView toJoinPositn(ModelMap modelMap,Page page,String ids,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("10");//10是分店类型
		modelMap.put("codeDess", codeDesService.findCodeDes(codeDes));
		modelMap.put("listPositn", positnService.findPositnBySupply1(positn));
		modelMap.put("queryPositn", positn);
		modelMap.put("ids", ids);
		return new ModelAndView(SupplyConstants.JOINPOSITN,modelMap);
	}

	/***
	 * 直接加入分店物资属性
	 * @param session
	 * @param modelMap
	 * @param ids
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/joinPositn")
	@ResponseBody
	public String joinPositn(HttpSession session,ModelMap modelMap,String ids,String codes) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return supplyService.joinPositn(acct,ids,codes);
	}

	/***
	 * 进入关联用户界面
	 * @param modelMap
	 * @param page
	 * @param ids
	 * @param departmentCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toJoinAccount")
	public ModelAndView toJoinAccount(ModelMap modelMap,Page page,String ids,String departmentCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("departmentCode", departmentCode);
		modelMap.put("userList", userService.findUserAccount(map));
		modelMap.put("ids", ids);
		return new ModelAndView(SupplyConstants.JOINACCOUNT,modelMap);
	}

	/***
	 * 关联用户
	 * @param session
	 * @param modelMap
	 * @param ids
	 * @param accountids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/joinAccount")
	@ResponseBody
	public String joinAccount(HttpSession session,ModelMap modelMap,String ids,String accountids) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return supplyService.joinAccount(acct,ids,accountids);
	}
	
	/**
	 * 维护物资多单位主界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/tableUnit")
	public ModelAndView tableUnit(ModelMap modelMap, SupplyUnit supplyUnit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listSupplyUnit", supplyService.findAllSupplyUnit(supplyUnit));
		modelMap.put("supplyUnit", supplyUnit);
		return new ModelAndView(SupplyConstants.TABLE_SUPPLYUNIT,modelMap);
	}

	/***
	 * 打开新增物资多单位页面
	 * @param modelMap
	 * @param supplyUnit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addUnit")
	public ModelAndView addUnit(ModelMap modelMap, SupplyUnit supplyUnit) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
//		modelMap.put("units", supplyService.findAllSupplyUnit(supplyUnit).size());
		supplyUnit.setUnitper(1);
		List<SupplyUnit> listSupplyUnit = supplyService.findAllSupplyUnit(supplyUnit);
		modelMap.put("units", listSupplyUnit.size());
		if (listSupplyUnit.size()>0) {
			modelMap.put("unit", listSupplyUnit.get(0));
		}
		return new ModelAndView(SupplyConstants.SAVE_SUPPLYUNIT, modelMap);
	}

	/***
	 * 保存物资多单位
	 * @param modelMap
	 * @param supplyUnit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveUnitByAdd")
	public ModelAndView saveUnit(ModelMap modelMap, SupplyUnit supplyUnit) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.saveSupplyUnit(supplyUnit);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/***
	 * 打开修改物资多单位页面
	 * @param modelMap
	 * @param supplyUnit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateUnit")
	public ModelAndView updateUnit(ModelMap modelMap, SupplyUnit supplyUnit) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("0");
		modelMap.put("unitList", codeDesService.findCodeDes(codeDes));//物资单位
		modelMap.put("supplyUnit", supplyService.findSupplyUnitById(supplyUnit));
		return new ModelAndView(SupplyConstants.UPDATE_SUPPLYUNIT, modelMap);
	}
	
	/**
	 * 修改物资编码信息
	 * @param modelMap
	 * @param supplyUnit
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveUnitByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, SupplyUnit supplyUnit)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.updateSupplyUnit(supplyUnit);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/***
	 * 删除物资多单位信息
	 * @param modelMap
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteUnitByIds")
	public ModelAndView deleteUnitByIds(ModelMap modelMap, String ids) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyService.deleteByIds(ids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 获取所有物资中类信息
	 * @param supplyUnit
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findSupplyUnit")
	@ResponseBody
	public Object findSupplyUnit(SupplyUnit supplyUnit) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return supplyService.findAllSupplyUnit(supplyUnit);
	}
	
	/***
	 * 查询半成品前十条
	 * @author wjf
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findExTop10", method=RequestMethod.POST)
	@ResponseBody
	public  List<Supply> findExTop10(Supply supply, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		return supplyService.findSupplyExTop10(supply);
	}
}
