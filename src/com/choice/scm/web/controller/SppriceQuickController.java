package com.choice.scm.web.controller;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SppriceQuickConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceQuickService;

/**
 * 报价模块快速加入
 * @author czh
 *
 */
@Controller
@RequestMapping("sppriceQuick")
public class SppriceQuickController {
	
	/**
	 * 获取分店
	 */
	@Autowired
	PositnService positnService;
	@Autowired
	SppriceQuickService sppriceQuickService;
	@Autowired
	private CodeDesService codeDesService;
	/**
	 * 报价模板添加 跳转
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceQuickConstants.LIST_SPPRICEQUICK,modelMap);
	}
	/**
	 * 根据供货商ID显示 该供货商下模板
	 */
	@RequestMapping("/findSppriceByDeliverCode")
	public ModelAndView findSppriceByDeliverCode(ModelMap modelMap,Deliver deliver) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sppriceList", sppriceQuickService.findSppriceByDeliverCode(deliver));
		return new ModelAndView(SppriceQuickConstants.LIST_SPPRICEBYDELIVERID,modelMap);
	}
	/**
	 * 根据id获取物资信息并返回json对象
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Supply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		return sppriceQuickService.findSupplyById(supply);
	}
	/**
	 * 查询所有的分店和仓位
	 */
	@RequestMapping("/searchAllPositn")
	public ModelAndView searchAllPositn(ModelMap modelMap, Positn positn, String mis,String defaultCode,String defaultName,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
		}
		page.setPageSize(Integer.MAX_VALUE);
		modelMap.put("listPositn", positnService.findPositnForQuickSpprice(positn,page));
		List<Positn> typeList = positnService.findAllPositnTyp();
		List<Positn> list = new ArrayList<Positn>();
		for(Positn positn2:typeList){
			if(positn2.getTyp().indexOf("1207")>-1){
				list.add(positn2);
			}
		}
		typeList.removeAll(list);
		modelMap.put("listPositnTyp", typeList);
		//查询所有分店类型  wjf
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("10");//10是分店类型
		modelMap.put("firmtypList", codeDesService.findCodeDes(codeDes));
		modelMap.put("positn", positn);
		modelMap.put("defaultCode", defaultCode);
		if(positn.getTyp()!=null){
			modelMap.put("typ", positn.getTyp());
		}
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName,"UTF-8"));
		}
		modelMap.put("mis", mis);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceQuickConstants.SEARCH_ALLPOSITN,modelMap);
	}
	/**
	 * 添加报价
	 */
	@RequestMapping("/saveSpprice")
	@ResponseBody
	public String saveSpprice(HttpSession session,Spprice spprice) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return sppriceQuickService.saveSpprice(session,spprice);
	}

	/**********************************************************************************
	 * 以下是快速添加 
	 *********************************************************************************/
	/**
	 * 价格模板 批量添加 跳转
	 */
	@RequestMapping("/quickListSppriceDemo")
	public ModelAndView quickListSppriceDemo(ModelMap modelMap,String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("code", code);
		return new ModelAndView(SppriceQuickConstants.LIST_SPPRICEDEMO_QUICK,modelMap);
	}
	/**
	 * 添加报价
	 */
	@RequestMapping("/saveSppriceDemo")
	@ResponseBody
	public String saveSppriceDemo(HttpSession session,SppriceDemo sppriceDemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return sppriceQuickService.saveSppriceDemo(session,sppriceDemo);
	}
}
