package com.choice.scm.web.controller;

import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.LinesFirm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.service.AccountDisService;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisFenBoService;
import com.choice.scm.service.LinesFirmService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;
import com.choice.wsyd.util.MailUtils;

@Controller
@RequestMapping(value = "disFenBo")
/**
 * 报货分拨
 * @author csb
 *
 */
public class DisFenBoController {

	private final static int PAGE_SIZE = 40;
	@Autowired
	private DisFenBoService disFenBoService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private LinesFirmService linesFirmService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private AccountDisService accountDisService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private AcctService acctService;
	@Autowired
	private LogsMapper logsMapper;
//	@Autowired
//	private NcBasicDataService basicDataService;
	@Autowired
	private PositnService positnService;
	/********************************************报货分拨start******************************************/	
	/**
	 * 报货分拨
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap,HttpSession session,Dis dis,String action,String ind1,String chectim1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"报货分拨查询",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		if(null!=action && "init".equals(action)){//页面初次加载时
			dis.setMaded(new Date());
			modelMap.put("action", "init");
			dis.setYndo("NO");
			
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setInd(dis.getMaded());
				dis.setMaded(null);
			}
			
			dis.setStartNum(0);
			dis.setEndNum(PAGE_SIZE);
			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
			dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
			List<Dis> disesList1 = disFenBoService.findAllDisPage(dis);
			int totalCount = disFenBoService.findAllDisCount(dis);//查总数量  应该是放上所有条件再查  不然和列表里对不起来  2014.11.19wjf
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Dis d:disesList1){
				supplyAcct.setPositn(d.getPositnCode());
				supplyAcct.setSp_code(d.getSp_code());
				d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
				//判断是否生成采购订单是否为空   默认为N
				if (d.getIspuprorder() == null || "".equals(d.getIspuprorder())) {
					d.setIspuprorder("N");
				}
			}
			modelMap.put("disesList1", disesList1);//前20条数据
			dis.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("totalCount", totalCount);
			
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
			}
			
			if(null!=ind1 && !"".equals(ind1)){
				dis.setMaded(dis.getInd());
			}
		}
		
		//查询货架
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("17");
		List<CodeDes> list = codeDesService.findCodeDes(codeDes);
		modelMap.put("positn1s", list);
		
		//配送
		modelMap.put("linesFirmList", linesFirmService.findLinesFirm(new LinesFirm()));//配送片区
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));//配送片区
		modelMap.put("accountName", accountName);
		modelMap.put("ind1", ind1);
		modelMap.put("chectim1", chectim1);
		modelMap.put("dis", dis);
		modelMap.put("currPage", 1);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		String ZHGLTSQX = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "ZHGLTSQX");
		modelMap.put("ZHGLTSQX", ZHGLTSQX);//加判断，是否加特殊权限判断，洞庭需要在报货分拨页面加价格修改限制 wjf 
		if("1".equals(ZHGLTSQX)){//如果是1，则去查询权限表
			modelMap.put("accountDis", accountDisService.getAccountDisByAccountId(session.getAttribute("accountId").toString()));//此账户是否有特殊权限 wjf
		}
		
		return new ModelAndView(DisConstants.LIST_DIS_NEW, modelMap);
	
		
	}
	
	/**
	 * 报货分拨Ajax加载
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session,Dis dis,String ind1,String currPage,String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		dis.setStartNum(intCurrPage*PAGE_SIZE);
		dis.setEndNum((intCurrPage+1)*PAGE_SIZE);
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> disesList1 = disFenBoService.findAllDisPage(dis);
		
		//查询库存量--物资余额表数据
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(Dis d:disesList1){
			supplyAcct.setPositn(d.getPositnCode());
			supplyAcct.setSp_code(d.getSp_code());
			d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
		}
		modelMap.put("disesList1", disesList1);
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		if(null!=ind1 && !"".equals(ind1)){
			dis.setMaded(dis.getInd());
		}
		modelMap.put("ind1", ind1);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
		//return modelMap;
	}
	
	/**
	 * 报货分拨生成采购订单
	 * @param session
	 * @param dis
	 * @return
	 */
	@RequestMapping(value = "/toSavePuprOrder")
	@ResponseBody
	public String toSavePuprOrder(HttpSession session,PuprOrderd puprOrderd)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return disFenBoService.savePuprOrder(session,puprOrderd);
	}
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Double.class, new PropertyEditorSupport() {
            @Override  
            public String getAsText() {
                return getValue().toString();   
            }   
            @Override  
            public void setAsText(String text) throws IllegalArgumentException {
            	if(null != text && !"".equals(text))
            		setValue(Double.parseDouble(text));
            	else
            		setValue(null);
            }   
        });   
    }
	
	/**
	 * 批量修改
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"批量修改报货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
//		//上传数据到NC
//		basicDataService.salepre(dis);
		disFenBoService.updateByIds(dis,accountName);
		return new ModelAndView(DisConstants.LIST_DIS_NEW, modelMap);
	}
	
	/***
	 * 更新最新报价
	 * @author 20151208wjf
	 * @param modelMap
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateLastPrice")
	@ResponseBody
	public int updateLastPrice(ModelMap modelMap,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"报货分拨更新最新价格",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		return disFenBoService.updateLastPrice(dis);
	}
	
	/**
	 * 全部操作（全部采购确认、全部采购审核、全部未知到货、全部取消，采购生成采购订单）
	 */
	@RequestMapping(value = "/updateAll")
	public ModelAndView updateAll(ModelMap modelMap,HttpSession session,Dis dis,String param,String ind1,String chectim1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"报货单全部操作",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		dis.setStartNum(0);
		dis.setEndNum(Integer.MAX_VALUE);
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> disList=disFenBoService.findAllDisPage(dis);	
		
		String[] type = param.split("-");
		if(type[0].equals("chk1")){
			dis.setChk1(type[1]);
		}else if(type[0].equals("chksend")){
			dis.setChksend(type[1]);
		}else if(type[0].equals("sta")){
			dis.setSta(type[1]);
		}else if(type[0].equals("isPuprOrder")){
			dis.setIspuprorder(type[1]);
		}
		StringBuffer idsBuffer = new StringBuffer();
		for(Dis d:disList){
			//如果供应商已经发货的，不能取消确认，取消审核  需要过滤掉
			if(("chk1".equals(type[0]) && "N".equals(type[1])) || ("sta".equals(type[0]) && "N".equals(type[1]))){
				if("Y".equals(d.getDeliveryn())){
					continue;
				}
			}
			idsBuffer.append(d.getId()+",");
		}
		if(idsBuffer.length() != 0){
			dis.setId(idsBuffer.substring(0, idsBuffer.lastIndexOf(",")));
			if("price".equals(type[0])){
				disFenBoService.updateLastPrice(dis);
			}else{
				disFenBoService.updateByIds(dis,accountName);
			}
		}
		int totalCount = disList.size();
		dis.setEndNum(PAGE_SIZE);
		dis.setId(null);
		dis.setChk1(null);
		dis.setSta(null);
		List<Dis> disesList1 = disFenBoService.findAllDisPage(dis);
		
		//全部采购审核
		if(param != null && "sta-Y".equals(param)){
			String id = "";
			for(Dis d:disesList1){
				if("in".equals(d.getInout())){//只有入库的数据 才往NC传
					id+=d.getId()+",";
				}
			}
			Dis dis2 = new Dis();
			dis2.setId(id);
		}
		
		//查询库存量--物资余额表数据
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(Dis d:disesList1){
			supplyAcct.setPositn(d.getPositnCode());
			supplyAcct.setSp_code(d.getSp_code());
			d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
		}
		modelMap.put("disesList1", disesList1);
		modelMap.put("totalCount", totalCount);
		
		if(totalCount<=PAGE_SIZE){
			modelMap.put("currState", 1);
		}else{
			modelMap.put("currState", PAGE_SIZE*1.0/totalCount);
		}
		if(null!=ind1 && !"".equals(ind1)){//选到货日的时候 全部操作有点问题
			dis.setMaded(dis.getInd());
		}
		//配送
		modelMap.put("linesFirmList", linesFirmService.findLinesFirm(new LinesFirm()));//配送片区
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));//配送片区
		modelMap.put("accountName", accountName);
		modelMap.put("ind1", ind1);
		modelMap.put("chectim1", chectim1);
		modelMap.put("dis", dis);
		modelMap.put("currPage", 1);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		modelMap.put("lineCode", dis.getLineCode());
		String ZHGLTSQX = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "ZHGLTSQX");
		modelMap.put("ZHGLTSQX", ZHGLTSQX);//加判断，是否加特殊权限判断，洞庭需要在报货分拨页面加价格修改限制 wjf 
		if("1".equals(ZHGLTSQX)){//如果是1，则去查询权限表
			modelMap.put("accountDis", accountDisService.getAccountDisByAccountId(session.getAttribute("accountId").toString()));//此账户是否有特殊权限 wjf
		}
		return new ModelAndView(DisConstants.LIST_DIS_NEW, modelMap);
	}
	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap,HttpSession session,DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"报货单修改",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
//		String accountName=session.getAttribute("accountName").toString();
		return disFenBoService.updateDis(disList);
//		modelMap.put("accountName", accountName);
//		return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}
	
	/***
	 * 报货分拨供应商修改
	 * @param modelMap
	 * @param deliver
	 * @param oldCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDeliverTyp")
	@ResponseBody
	public Object findDeliverTyp(ModelMap modelMap, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//根据供应商编码查找
		return disFenBoService.findDeliverTyp(deliver);
	}
	
	/**
	 * 报货分拨，采购汇总
	 */
	@RequestMapping(value = "/findCaiGouTotal")
	public ModelAndView findCaiGouTotal(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		modelMap.put("disList", disFenBoService.findCaiGouTotal(dis,page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.CAIGOU_TOTAL_DISTRIBUTION, modelMap);
	}
	
	/**
	 * 报货分拨，配送汇总
	 */
	@RequestMapping(value = "/findPeiSongTotal")
	public ModelAndView findPeiSongTotal(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("disList", disFenBoService.findPeiSongTotal(dis,page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.PEISONG_TOTAL_DISTRIBUTION, modelMap);
	}
	
	/**
	 * 报货分拨，部门明细
	 */
	@RequestMapping(value = "/findDeptDetail")
	public ModelAndView findDeptDetail(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition= new HashMap<String,Object>();	
 		condition.put("maded",dis.getMaded());//制单日期
		condition.put("positnCode", dis.getPositnCode());//仓位名称
		condition.put("deliverCode", dis.getDeliverCode());//供货商名称
		condition.put("typCode", dis.getTypCode());//类别名称
		condition.put("firmCode", dis.getFirmCode());//分店名
		condition.put("sp_code",dis.getSp_code());//物资编码
		condition.put("ex1", dis.getEx1());//是否半成品
		condition.put("sp_init",dis.getSp_init());//缩写码
		condition.put("inout",dis.getInout()); //单据的方向
		condition.put("chksend", dis.getChksend());
		modelMap.put("firmCode", dis.getFirmCode());//分店名
		modelMap.put("columns", disFenBoService.findTableHead(dis.getFirmCode().toString()));//获得表头
		modelMap.put("ListBean", JSONArray.fromObject(disFenBoService.findDeptDetail(condition)).toString());
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.DEPT_DETAIL_DISTRIBUTION, modelMap);
	}
	
	/**
	 *部门明细---Excel 
	 */
	@RequestMapping(value = "/exportDeptDetail")
	@ResponseBody
	public boolean exportCard(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "部门明细";
		Map<String,Object> condition= new HashMap<String,Object>();	
 		condition.put("maded",dis.getMaded());//制单日期
		condition.put("positnCode", dis.getPositnCode());//仓位名称
		condition.put("deliverCode", dis.getDeliverCode());//供货商名称
		condition.put("typCode", dis.getTypCode());//类别名称
		condition.put("firmCode", dis.getFirmCode());//分店名
		condition.put("sp_code",dis.getSp_code());//物资编码
		condition.put("ex1", dis.getEx1());//是否半成品
		condition.put("sp_init",dis.getSp_init());//缩写码
		condition.put("inout",dis.getInout()); //单据的方向
		condition.put("chksend", dis.getChksend());		

		condition.put("wzqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		condition.put("wzzhqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//判断账号关联小类还是关联具体物资  wjf
		condition.put("fxqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		condition.put("gysqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		condition.put("cwqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		condition.put("fbcxqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		condition.put("accountId", session.getAttribute("accountId").toString());
		List<Map<String,Object>> exportList = disFenBoService.findDeptDetail(condition);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		return disFenBoService.exportExcel(response.getOutputStream(), exportList, dis.getFirmCode());
	}
	
	/**
	 * 报货分拨，部门明细2
	 */
	@RequestMapping(value = "/findDeptDetail2")
	public ModelAndView findDeptDetail2(ModelMap modelMap,Page page,HttpSession session,Dis dis,String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		String firmCode=dis.getFirmCode();//记录原始值
		String inout=dis.getInout();
		if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
		if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
		if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
		if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
		dis.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		dis.setSp_codes(CodeHelper.replaceCode(dis.getSp_codes()));
		List<Dis> disList = disFenBoService.findDeptDetail2(dis,page);
		dis.setFirmCode(firmCode);//覆盖处理后的值
		dis.setInout(inout);
		modelMap.put("ind1", ind1);
		if(null!=ind1 && !"".equals(ind1)){
			dis.setMaded(dis.getInd());
		}
		modelMap.put("disList", disList);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.DEPT_DETAIL_DISTRIBUTION2, modelMap);
	}
	
	/**
	 *部门明细2---Excel 
	 */
	@RequestMapping(value = "/exportDeptDetail2")
	@ResponseBody
	public boolean exportDeptDetail2(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis,String ind1) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "部门明细";
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
		if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
		if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
		if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
		dis.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		List<Dis> disList = disFenBoService.findDeptDetail2(dis,null);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		return disFenBoService.exportDeptDetail2(response.getOutputStream(), disList);
	}
	
	/***
	 * 部门明细2---Excel 导出部门分拨单 wjf
	 * @param response
	 * @param session
	 * @param request
	 * @param dis
	 * @param ind1
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exportDeptExcel")
	@ResponseBody
	public boolean exportDeptExcel(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis,String ind1) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "部门报货分拨单";
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			dis.setMaded(null);
		}
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
		if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
		if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
		if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
		dis.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		List<Dis> disList = disFenBoService.findDeptDetail2(dis,null);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		return disFenBoService.exportDeptExcel(response.getOutputStream(), disList, dis);
	}
	
	/**
	 * 部门明细2 打印
	 */
	@RequestMapping(value = "/printDeptDetail2")
	public ModelAndView printDeptDetail2(ModelMap modelMap,HttpSession session,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"部门明细打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printDeptDetail2.do");//传入回调路径	
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
		if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
		if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
		if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
		dis.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		List<Dis> disList = disFenBoService.findDeptDetail2(dis,null);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("部门明细");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
        for(Dis d:disList){
        	Map<String,Object> map = new HashMap<String,Object>();
        	map.put("chkstoNo", d.getChkstoNo());
        	map.put("id", d.getId());
        	map.put("deliverDes", d.getDeliverDes());
        	map.put("firmDes", d.getFirmDes());
        	map.put("sp_code", d.getSp_code());
        	map.put("sp_name", d.getSp_name());
        	map.put("sp_desc", d.getSp_desc());
        	map.put("unit3", d.getUnit3());
        	map.put("amount1", d.getAmount1());
        	list.add(map);
        	for (Chkstod chkstod : d.getDeptList()) {
        		Map<String,Object> m = new HashMap<String,Object>();
            	m.put("chkstoNo", "");
            	m.put("id", "");
            	m.put("deliverDes", "");
            	m.put("firmDes", chkstod.getPosition().getDes());
            	m.put("sp_code", "");
            	m.put("sp_name", "");
            	m.put("sp_desc", "");
            	m.put("unit3", "");
            	m.put("amount1", chkstod.getAmount());
            	list.add(m);
			}
        }
	    modelMap.put("List", list);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/disFenBo/printDeptDetail2.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_DEPTDETAIL2_URL,DisConstants.REPORT_DEPTDETAIL2_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 *导出excel分拨单 
	 */
	@RequestMapping(value = "/excelDis")
	@ResponseBody
	public boolean excelDis(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出excel分拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "分拨单";
		dis.setEndNum(30000);
		//权限
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> exportList = disFenBoService.findAllDisPage(dis);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		
		return disFenBoService.exportFenboExcel_new(response.getOutputStream(), exportList, dis.getFirmCode());
	}
	
	
	/**
	 *导出excel分拨单2
	 *add by jinshuai at 20160331
	 *修改导出excel的格式
	 */
	@RequestMapping(value = "/excelDis2")
	@ResponseBody
	public boolean excelDis2(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出excel分拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "分拨单";
		dis.setEndNum(30000);
		
		//保存到货日
		Date dhd = null;
		//勾选到货日
		String ind1 = dis.getInd1();
		if(ind1!=null&&"ind".equals(ind1)){
			dis.setInd(dis.getMaded());
			dhd = dis.getMaded();
			dis.setMaded(null);
		}
		//权限
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> exportList = disFenBoService.findAllDisPage(dis);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		
		return disFenBoService.exportFenboExcel_newtwo(response.getOutputStream(), exportList, dis.getFirmCode(),dhd);
	}
	
	/***
	 * 导出excel分拨单3
	 * @param response
	 * @param session
	 * @param request
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/excelDis3")
	@ResponseBody
	public boolean excelDis3(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出excel分拨单3",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "分拨单";
		dis.setEndNum(30000);
		
		//保存到货日
		Date dhd = null;
		//勾选到货日
		String ind1 = dis.getInd1();
		if(ind1!=null&&"ind".equals(ind1)){
			dis.setInd(dis.getMaded());
			dhd = dis.getMaded();
			dis.setMaded(null);
		}
		//权限
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> exportList = disFenBoService.findAllDisPage(dis);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		return disFenBoService.exportFenboExcel3(response.getOutputStream(), exportList, dis.getFirmCode(),dhd);
	}
	
	/**
	 *导出excel分拨单4
	 *add by jinshuai at 201601215
	 *修改导出excel的格式
	 */
	@RequestMapping(value = "/excelDis4")
	@ResponseBody
	public boolean excelDis4(HttpServletResponse response, HttpSession session, HttpServletRequest request, Dis dis, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出excel分拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "分拨单";
		dis.setEndNum(30000);
		
		//保存到货日
		Date dhd = null;
		//勾选到货日
		String ind1 = dis.getInd1();
		if(ind1!=null&&"ind".equals(ind1)){
			dis.setInd(dis.getMaded());
			dhd = dis.getMaded();
			dis.setMaded(null);
		}
		//权限
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		List<Dis> exportList = disFenBoService.findAllDisPage(dis);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		return disFenBoService.exportFenboExcel_newfour(response.getOutputStream(), exportList, dis.getFirmCode(),dhd);
	}
	
	
	/**
	 * 打印分拨单
	 */
	@RequestMapping(value = "/printIndent")
	public ModelAndView printIndent(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印分拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询条件
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("indent");
		param.put("danjuTyp", "indent");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		param.put("ind1", ind1);
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printIndent.do");//传入回调路径
		//根据关键字查询
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		//1。看配置是不是需要采购确认，采购审核wjf2014.12.18
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		if("Y".equals(acct.getYncgqr())){//2.需要采购确认，只查采购确认的
			dis.setChk1("Y");
		}else{
			dis.setChk1(null);
		}
		if("Y".equals(acct.getYncgsh())){//3.需要采购审核，只查采购审核的
			dis.setSta("Y");
		}else{
			dis.setSta(null);
		}
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		//查询库存量--物资余额表数据
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(Dis d:disList){
			supplyAcct.setPositn(d.getPositnCode());
			supplyAcct.setSp_code(d.getSp_code());
			d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
		}
		//记录合计数
		Map<String,Object> countMap=new HashMap<String,Object>();
		//所有的数据
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		String deliverDes2="";
		String sp_name2="";
		//计算合计
		double count=0;
		//------------------------------------------------ 
	    int tdnum1=1;//标记列1
	    int tdnum2=1;//标记列2
	    int tdnum3=1;//标记列3
	    //存放数据，往报表页面传
	    Map<String,Object> resultMap=new HashMap<String,Object>();
	  //如果要走新的报货页面 2014.12.26wjf
    	//报表文件同种物资类似行列转换用到的方法
	    for(int i=0;i<disList.size();i++){
	    	String memo0 = null == disList.get(i).getMemo()?"":disList.get(i).getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		String[] testMemo0 = memo0.split("##");
	    		memo0 = testMemo0.length>0?testMemo0[0]:"";
	    		memo1 = testMemo0.length>1?testMemo0[1]:"";
	    	}
	    	if(deliverDes2.equals(disList.get(i).getDeliverDes())){
			     if(sp_name2.equals(disList.get(i).getSp_name())){
					  if(tdnum1==4){
						   tdnum1=1;
						   tdnum2=1;
						   tdnum3=1;
						   list.add(resultMap);
						   resultMap=new HashMap<String,Object>();
						   resultMap.put("STR", "");
						   
						   //wangjie 物资编码
						   resultMap.put("SP_CODE",disList.get(i).getSp_code());
						   resultMap.put("SP_NAME",disList.get(i).getSp_name());
						   //add by jin shuai at 20160308
						   resultMap.put("SP_INIT",disList.get(i).getSp_init());
						   resultMap.put("DELIVER", disList.get(i).getDeliverDes());
						   resultMap.put("UNIT",disList.get(i).getUnit3());
						   resultMap.put("CNT",disList.get(i).getCnt());
						   resultMap.put("SP_DESC",disList.get(i).getSp_desc());
					  }
				      count+=disList.get(i).getAmount1sto();//采购数量
				      countMap.put(disList.get(i).getSp_name(),count);
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				      resultMap.put("MEMO"+tdnum3++, memo0); 
				      resultMap.put("MEMO1"+(tdnum3-1), memo1); 
			     }else{
				      if(resultMap.size()!=0){
				    	  list.add(resultMap);
				      }
				      tdnum1 = 1;
				      tdnum2 = 1;
				      tdnum3 = 1;
				      count = 0;
				      resultMap=new HashMap<String,Object>();
				      resultMap.put("STR", "STR");
				      resultMap.put("SP_CODE",disList.get(i).getSp_code());
				      resultMap.put("SP_NAME",disList.get(i).getSp_name());
				      //add by jin shuai at 20160308
					  resultMap.put("SP_INIT",disList.get(i).getSp_init());
				      resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				      resultMap.put("UNIT",disList.get(i).getUnit3());
				      resultMap.put("CNT",disList.get(i).getCnt());
					  resultMap.put("SP_DESC",disList.get(i).getSp_desc());
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				      resultMap.put("MEMO"+tdnum3++, memo0); 
				      resultMap.put("MEMO1"+(tdnum3-1), memo1);
				      count=disList.get(i).getAmount1sto();
				      countMap.put(disList.get(i).getSp_name(),count);
				      sp_name2=disList.get(i).getSp_name();
			     }
	    	}else{
	    		if(resultMap.size()!=0){
	    			list.add(resultMap);
			    }
				tdnum1 = 1;
				tdnum2 = 1;
				tdnum3 = 1;
				count = 0;
				resultMap=new HashMap<String,Object>();
				resultMap.put("STR", "STR");
				resultMap.put("SP_CODE",disList.get(i).getSp_code());
				resultMap.put("SP_NAME",disList.get(i).getSp_name());
				//add by jin shuai at 20160308
				resultMap.put("SP_INIT",disList.get(i).getSp_init());
				resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				resultMap.put("UNIT",disList.get(i).getUnit3());
				resultMap.put("CNT",disList.get(i).getCnt());
				resultMap.put("SP_DESC",disList.get(i).getSp_desc());
				resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				resultMap.put("MEMO"+tdnum3++, memo0); 
			    resultMap.put("MEMO1"+(tdnum3-1), memo1);
				count=disList.get(i).getAmount1sto();
				countMap.put(disList.get(i).getSp_name(),count);
				sp_name2=disList.get(i).getSp_name();
				deliverDes2=disList.get(i).getDeliverDes();
		     }
	    }
	    list.add(resultMap);//将最后一行加上
		//计算完合计，存放进map
		for (int i = 0; i < list.size(); i++) {
			Map<String,Object> map=list.get(i);
			map.put("TOTALAMT", countMap.get(map.get("SP_NAME")));
			countMap.remove(map.get("SP_NAME"));
		}
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("分拨单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        modelMap.put("List", list);
	    modelMap.put("parameters", parameters);
	    Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INDENT_URL,DisConstants.REPORT_INDENT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
        //return new ModelAndView(rs.get("url"),modelMap);
	}
	
	
	
	
	/**
	 * 打印分拨单 add by jinshuai at 20160415
	 * 去掉物资编码 每行打印5个店
	 */
	@RequestMapping(value = "/printIndent2")
	public ModelAndView printIndent2(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印分拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询条件
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("indent");
		param.put("danjuTyp", "indent");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printIndent2.do");//传入回调路径
		//根据关键字查询
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		//1。看配置是不是需要采购确认，采购审核wjf2014.12.18
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		if("Y".equals(acct.getYncgqr())){//2.需要采购确认，只查采购确认的
			dis.setChk1("Y");
		}else{
			dis.setChk1(null);
		}
		if("Y".equals(acct.getYncgsh())){//3.需要采购审核，只查采购审核的
			dis.setSta("Y");
		}else{
			dis.setSta(null);
		}
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		//查询库存量--物资余额表数据
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(Dis d:disList){
			supplyAcct.setPositn(d.getPositnCode());
			supplyAcct.setSp_code(d.getSp_code());
			d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
		}
//		dis.setStartNum(0);
//		dis.setEndNum(Integer.MAX_VALUE);
//		List<Dis> disList=disFenBoService.findAllDisPage(dis);	
		//记录合计数
		Map<String,Object> countMap=new HashMap<String,Object>();
		//所有的数据
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		String deliverDes2="";
		String sp_name2="";
		//计算合计
		double count=0;
		//------------------------------------------------ 
	    int tdnum1=1;//标记列1
	    int tdnum2=1;//标记列2
	    int tdnum3=1;//标记列3
	    //存放数据，往报表页面传
	    Map<String,Object> resultMap=new HashMap<String,Object>();
	    
	    //update by jinshuai at 20160413
	    //修改打印分拨单
	    String printtj = "1";//去掉物资编码 库存 附加项 每行显示5个分店
	    //显示分店的数量
    	int	firmsize = 5;
	    
    	//如果要走新的报货页面 2014.12.26wjf
    	//报表文件同种物资类似行列转换用到的方法
	    for(int i=0;i<disList.size();i++){
	    	String memo0 = null == disList.get(i).getMemo()?"":disList.get(i).getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		String[] testMemo0 = memo0.split("##");
	    		memo0 = testMemo0.length>0?testMemo0[0]:"";
	    		memo1 = testMemo0.length>1?testMemo0[1]:"";
	    	}
	    	if(deliverDes2.equals(disList.get(i).getDeliverDes())){
			     if(sp_name2.equals(disList.get(i).getSp_name())){
					  if(tdnum1==(firmsize+1)){
						   tdnum1=1;
						   tdnum2=1;
						   tdnum3=1;
						   list.add(resultMap);
						   resultMap=new HashMap<String,Object>();
						   resultMap.put("STR", "");
						   
						   //wangjie 物资编码
						   resultMap.put("SP_CODE",disList.get(i).getSp_code());
						   resultMap.put("SP_NAME",disList.get(i).getSp_name());
						   //add by jin shuai at 20160308
						   resultMap.put("SP_INIT",disList.get(i).getSp_init());
						   resultMap.put("DELIVER", disList.get(i).getDeliverDes());
						   resultMap.put("UNIT",disList.get(i).getUnit3());
						   resultMap.put("CNT",disList.get(i).getCnt());
					  }
				      count+=disList.get(i).getAmount1sto();//采购数量
				      countMap.put(disList.get(i).getSp_name(),count);
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				      resultMap.put("MEMO"+tdnum3++, memo0); 
				      resultMap.put("MEMO1"+(tdnum3-1), memo1); 
			     }else{
				      if(resultMap.size()!=0){
				    	  list.add(resultMap);
				      }
				      tdnum1 = 1;
				      tdnum2 = 1;
				      tdnum3 = 1;
				      count = 0;
				      resultMap=new HashMap<String,Object>();
				      resultMap.put("STR", "STR");
				      resultMap.put("SP_CODE",disList.get(i).getSp_code());
				      resultMap.put("SP_NAME",disList.get(i).getSp_name());
				      //add by jin shuai at 20160308
					  resultMap.put("SP_INIT",disList.get(i).getSp_init());
				      resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				      resultMap.put("UNIT",disList.get(i).getUnit3());
				      resultMap.put("CNT",disList.get(i).getCnt());
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				      resultMap.put("MEMO"+tdnum3++, memo0); 
				      resultMap.put("MEMO1"+(tdnum3-1), memo1);
				      count=disList.get(i).getAmount1sto();
				      countMap.put(disList.get(i).getSp_name(),count);
				      sp_name2=disList.get(i).getSp_name();
			     }
	    	}else{
	    		if(resultMap.size()!=0){
	    			list.add(resultMap);
			    }
				tdnum1 = 1;
				tdnum2 = 1;
				tdnum3 = 1;
				count = 0;
				resultMap=new HashMap<String,Object>();
				resultMap.put("STR", "STR");
				resultMap.put("SP_CODE",disList.get(i).getSp_code());
				resultMap.put("SP_NAME",disList.get(i).getSp_name());
				//add by jin shuai at 20160308
				resultMap.put("SP_INIT",disList.get(i).getSp_init());
				resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				resultMap.put("UNIT",disList.get(i).getUnit3());
				resultMap.put("CNT",disList.get(i).getCnt());
				resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes1());
				resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				resultMap.put("MEMO"+tdnum3++, memo0); 
			    resultMap.put("MEMO1"+(tdnum3-1), memo1);
				count=disList.get(i).getAmount1sto();
				countMap.put(disList.get(i).getSp_name(),count);
				sp_name2=disList.get(i).getSp_name();
				deliverDes2=disList.get(i).getDeliverDes();
		     }
	    }
	    list.add(resultMap);//将最后一行加上
		//计算完合计，存放进map
		for (int i = 0; i < list.size(); i++) {
			Map<String,Object> map=list.get(i);
			map.put("TOTALAMT", countMap.get(map.get("SP_NAME")));
			countMap.remove(map.get("SP_NAME"));
		}
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("分拨单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        modelMap.put("List", list);
	    modelMap.put("parameters", parameters);
	    
	    Map<String,String> rs;
	    //update by jinshuai at 20160413
	    //根据条件得到模板文件
	    if("1".equals(printtj)){//去掉附加项 去掉备注 每行显示5个分店
		    rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INDENT_URL_NEW2,DisConstants.REPORT_INDENT_URL_NEW2);
	    }else{
	    	rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INDENT_URL,DisConstants.REPORT_INDENT_URL);//判断跳转路径
	    }
	    
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
        //return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 打印验货单
	 */
	@RequestMapping(value = "/printInspection")
	public ModelAndView printInspection(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印验货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户前台输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("inspection");
		param.put("danjuTyp", "inspection");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		//保存制单日期
		Date zddate = dis.getMaded();
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printInspection.do");//传入回调路径
		//根据关键字进行查询
		//List<Dis> disList=disFenBoService.findAllDisForReport(dis);	
		dis.setStartNum(0);
		dis.setEndNum(Integer.MAX_VALUE);
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		List<Dis> disList=disFenBoService.findAllDisPage(dis);	
		//修改洞庭提的直配的打印验货单的时候 应该显示报价，不能显示售价  2014.11.20 wjf
		for(Dis di:disList){
			if("dire".equals(di.getInout())){
				di.setPricesale(di.getPricein());
			}
			//调整备注和附加项
			String memo0 = null == di.getMemo()?"":di.getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		String[] testMemo0 = memo0.split("##");
	    		memo0 = testMemo0.length>0?testMemo0[0]:"";
	    		memo1 = testMemo0.length>1?testMemo0[1]:"";
	    	}
	    	di.setMemo(memo0);
	    	di.setMemo1(memo1);
		}
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("验货单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        parameters.put("maded", DateFormat.getStringByDate(zddate,"yyyy-MM-dd"));
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表用输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INSPECTION_URL,DisConstants.REPORT_INSPECTION_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印配送单
	 */
	@RequestMapping(value = "/printDelivery")
	public ModelAndView printDelivery(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印配送单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("delivery");
		param.put("danjuTyp", "delivery");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		param.put("chkstoNo", dis.getChkstoNo());
		param.put("positn1", dis.getPositn1());
		//报错制单日期
		Date zddate = dis.getMaded();
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printDelivery.do");//传入回调路径
		//根据关键字查询
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		//1。看配置是不是需要采购确认，采购审核wjf2014.12.18
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		if("Y".equals(acct.getYncgqr())){//2.需要采购确认，只查采购确认的
			dis.setChk1("Y");
		}else{
			dis.setChk1(null);
		}
		if("Y".equals(acct.getYncgsh())){//3.需要采购审核，只查采购审核的
			dis.setSta("Y");
		}else{
			dis.setSta(null);
		}
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		for(Dis di:disList){
			//调整备注和附加项
			String memo0 = null == di.getMemo()?"":di.getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		String[] testMemo0 = memo0.split("##");
	    		memo0 = testMemo0.length>0?testMemo0[0]:"";
	    		memo1 = testMemo0.length>1?testMemo0[1]:"";
	    	}
	    	di.setMemo(memo0);
	    	di.setMemo1(memo1);
		}
//		dis.setStartNum(0);
//		dis.setEndNum(Integer.MAX_VALUE);
//		List<Dis> disList=disFenBoService.findAllDisPage(dis);	
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("配送清单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        parameters.put("maded", DateFormat.getStringByDate(zddate, "yyyy-MM-dd"));
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_DELIVERY_URL,DisConstants.REPORT_DELIVERY_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 汇总打印
	 */
	@RequestMapping(value = "/printDisTotal")
	public ModelAndView printDisTotal(ModelMap modelMap,HttpSession session,Page page,Dis dis,Date maded,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"汇总打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/printDelivery.do");//传入回调路径	
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		List<Dis> disList=disFenBoService.findCaiGouTotal(dis,null);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("申购汇总");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(maded,"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
	    modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/disFenBo/printDisTotal.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_TOTAL_URL,DisConstants.REPORT_TOTAL_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 *木屋烧烤采购订单导出 
	 * 
	 */
	@RequestMapping(value = "/exportCaiGou")
	@ResponseBody
	public  String exportCaiGou(ModelMap modelMap,HttpSession session,HttpServletRequest request,Page page,HttpServletResponse response,Dis dis,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"采购订单导出 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "申购明细";
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("firmDes",dis.getFirmDes());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		List<Dis> disListt=disFenBoService.findCaiGouTotal(dis,null);
		/*if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}  
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");	*/
		File file = new File(request.getSession().getServletContext().getRealPath("/upload/"+fileName+".xls"));
		if(!file.exists()){
			file.createNewFile();
		}
		OutputStream out = new FileOutputStream(file);
		//disFenBoService.exportDisExcel(out, disList, disListt);
		disFenBoService.exportDisExcel(out, disList, disListt);
		String path = file.getAbsolutePath();
		out.flush();
		out.close();
		//path =  URLEncoder.encode(path, "UTF-8");
		return path;
	}
	
	/**
	 * 导出
	 */
	@RequestMapping(value = "/exportExcel")
	public ModelAndView exportExcel(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		param.put("lineCode", dis.getLineCode()==null?"":dis.getLineCode());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
			param.put("ind1", ind1);
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disFenBo/exportExcel.do");//传入回调路径	
		
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		
		//查询库存量--物资余额表数据--js 20160530
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(Dis d:disList){
			supplyAcct.setPositn(d.getPositnCode());
			supplyAcct.setSp_code(d.getSp_code());
			d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			//判断是否生成采购订单是否为空   默认为N
			if (d.getIspuprorder() == null || "".equals(d.getIspuprorder())) {
				d.setIspuprorder("N");
			}
		}
		
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("申购明细");
        parameters.put("report_name", report_name); 
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
	    modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/disFenBo/printDisTotal.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl("excel",DisConstants.REPORT_EXCEL_URL_NEW,DisConstants.REPORT_EXCEL_URL_NEW);//判断跳转路径
	 	modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 发送邮件 
	 * 
	 */
	@RequestMapping(value = "/sendEmail")
	@ResponseBody
	public String sendEmail(ModelMap modelMap,HttpSession session,HttpServletRequest request,Page page,HttpServletResponse response,Dis dis,String ind1){
		try{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			//首先判断此供应商下当前时间的物资是否已审核wjf
			dis.setSta("Y");//设置为已审核
			List<Dis> disList=disFenBoService.findAllDisForReport(dis);
			if(disList.size() == 0){//如果都未采购审核，则返回
				return "NSH";
			}
			modelMap.put("dis", dis);
			response.setContentType("application/msexcel; charset=UTF-8");
			String deliverCode = dis.getDeliverCode();
			Deliver deliver=disFenBoService.findDeliverOne(deliverCode);
			String mailAddr = deliver.getMail();
			MailUtils mailUtils = new MailUtils();
			//String text = WebConfig.getInstance().getValue("text").toString();
			//String tile = WebConfig.getInstance().getValue("tile").toString();
			String  text="申购汇总和明细表(见附件)";
			String tile="";
			tile = URLEncoder.encode(tile, "UTF-8");
			String filename = exportCaiGou(modelMap, session, request, page, response, dis, ind1).toString();
			mailUtils.go2(tile, text,filename,mailUtils,mailAddr);
			return "OK";
		}catch(Exception e){
			return "fail";
		}
		
	}
	
	/**
	 * 报货分拨拆分
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDispatch")
	@ResponseBody
	public ModelAndView updateDispatch(ModelMap modelMap, Chkstod chkstod) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		disFenBoService.updateDispatch(chkstod);
		return null;
	}
	
	/**
	 * 报货分拨调整
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDispatchX")
	@ResponseBody
	public ModelAndView updateDispatchX(ModelMap modelMap, Chkstod chkstod) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		disFenBoService.updateDispatchX(chkstod);
		return null;
	}
	/********************************************报货分拨end******************************************/	
	
	/********************************************验收直发start******************************************/
	/**
	 * 报货分拨，验货直发
	 */
	@RequestMapping(value = "/findYanHuoZhiFa")
	public ModelAndView findYanHuoZhiFa(ModelMap modelMap,Page page,HttpSession session,Dis dis,String action,Date maded1) throws Exception {
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CREATE,
					"报货分拨，验货直发",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			if(null!=action && !"".equals(action)){
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, +1);    //得到前一天
				Date date = calendar.getTime();
				dis.setBdat(new Date());
				dis.setEdat(date);
				maded1 = new Date();
				Positn p = new Positn();
				p.setTypn("1201");
				List<Positn> positn = positnService.findPositnSuperNOPage(p);
				if(positn.size()>0){
					dis.setPositnCode(positn.get(0).getCode());
					dis.setPositnDes(positn.get(0).getDes());
				}
			}else{
				String firmCode = dis.getFirmCode();
				if(null != dis.getFirmCode() && !"".equals(dis.getFirmCode())){//分店 wjf
					dis.setFirmCode(CodeHelper.replaceCode(firmCode));
				}
				String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
				dis.setAcct(acct);
				
				dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
				dis.setBdat(DateFormat.formatDate(dis.getBdat(), "yyyy-MM-dd"));
				dis.setEdat(DateFormat.formatDate(dis.getEdat(), "yyyy-MM-dd"));
				dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
				dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
				dis.setAccountId(session.getAttribute("accountId").toString());
				dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
				dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
				dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
				modelMap.put("disList", disFenBoService.findYanHuoZhiFa(dis,page));
				dis.setFirmCode(firmCode);
			}
			modelMap.put("maded1", maded1);
			modelMap.put("dis", dis);
			modelMap.put("pageobj", page);
			modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return new ModelAndView(DisConstants.YANHUO_ZHIFA_DISTRIBUTION_NEW, modelMap);
	}
	
	/**
	 * 生成直拨    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addzb")
	@ResponseBody
	public int addzb(ModelMap modelMap, HttpSession session, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CREATE,
				"生成直拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Integer  result=0;
		String accountName=session.getAttribute("accountName").toString();
		chkinm.setMadeby(accountName);
		chkinm.setChecby(accountName);
		chkinm.setMaded(new Date());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
		chkinm.setChkinno(chkinmService.getMaxChkinno());
		String acct=session.getAttribute("ChoiceAcct").toString();
//		String accountId=session.getAttribute("accountId").toString();
		chkinm.setAcct(acct);
		chkinm.setInout(ChkinmConstants.conk);
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB, chkinm.getMaded()));//从新获得单号
		result=disFenBoService.saveChkinm(chkinm);//保存并审核直发单
		return result;		
	}
	
	/***
	 * 新的生成直发单的方法 
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addzb_new")
	public ModelAndView addzb_new(ModelMap modelMap,HttpSession session,Page page,Dis dis,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CREATE,
				"生成直发单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		String firmCode = dis.getFirmCode();
		if(null != dis.getFirmCode() && !"".equals(dis.getFirmCode())){//分店 wjf
			dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		}
		dis.setAcct(acct);
		
		dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
		dis.setBdat(DateFormat.formatDate(dis.getBdat(), "yyyy-MM-dd"));
		dis.setEdat(DateFormat.formatDate(dis.getEdat(), "yyyy-MM-dd"));
		dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
		dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		List<Dis> disList=disFenBoService.findYanHuoZhiFa(dis,null);//如果page为空，则不分页  查询所有的2014.10.24wjf
		//报货单转直发单方法
		String  accountName=session.getAttribute("accountName").toString();//当前用户
		if(disList.size()!=0){
			disFenBoService.saveChkinmzb(disList,acct,accountName,null == maded1?new Date() : maded1);
			str=1;//不为空，成功
		}else{
			str=2;//为空，不能进行入库
		}
		modelMap.put("chkMsg", str);
		//应该再查一遍列表
		dis.setInCondition(null);//将id设为空
		modelMap.put("disList", disFenBoService.findYanHuoZhiFa(dis,page));
		dis.setFirmCode(firmCode);
		modelMap.put("maded1", maded1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		return new ModelAndView(DisConstants.YANHUO_ZHIFA_DISTRIBUTION_NEW, modelMap);
	}
}