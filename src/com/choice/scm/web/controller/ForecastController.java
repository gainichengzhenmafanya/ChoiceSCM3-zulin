package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ExplanConstants;
import com.choice.scm.constants.ForecastConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.ItemUse;
import com.choice.scm.domain.PosItemPlan;
import com.choice.scm.domain.PosSalePlan;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.ForecastService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;

/**
* 菜品点击率,营业预估,预估菜品销售计划,营业预估实际对比,菜品预估实际对比,菜品预估沽清对比
* @author -  css
*/

@Controller
@RequestMapping("forecast")
public class ForecastController {

	@Autowired
	private ForecastService forecastService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private Page pager;
	
	/********************************************************菜品点击率start*************************************************/
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView forecastList(ModelMap modelMap, HttpSession session, Page page, ItemUse itemUse, 
			String firmId, String firmName, Date bdate, Date edate, String type, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			itemUse.setFirm(firmId);
			modelMap.put("itemUseList", forecastService.findAllItemUse(itemUse, page));
		}
		//初次加载页面传值
		if (null==bdate) {
			bdate=new Date();
		}
		if (null==edate) {
			edate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("edate", edate);
		modelMap.put("mis", mis);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_COSTCTR,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
		}
	}
	
	/**
	 * 删除菜品点击率
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap modelMap, HttpSession session, Date bdate, Date edate, 
			String firmId,  String firmName, String msg, String mis, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		ItemUse itemUse = new ItemUse();
		itemUse.setFirm(firmId);
		// 删除所有菜品点击率
		forecastService.deleteAll(itemUse);
		modelMap.put("itemUseList", forecastService.findAllItemUse(itemUse, page));
		
		if (bdate==null) {
			bdate=new Date();
		}
		if (edate==null) {
			edate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_COSTCTR,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
		}
	}
	
	/**
	 * 分店MIS预估系统菜品点击率
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, Page page, 
			String firmId, String firmName, Date bdate, Date edate, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		ItemUse itemUse = new ItemUse();
		itemUse.setFirm(firmId);
		// 重新计算点击率
		forecastService.saveCalculate(firmId, session.getAttribute("ChoiceAcct").toString(), bdate, edate);
		List<ItemUse> itemUseList = forecastService.findAllItemUse(itemUse, page);
		if (itemUseList.size()==0) {
			modelMap.put("dataNull", "dataNull");
		}
		modelMap.put("itemUseList", itemUseList);
		modelMap.put("pageobj", page);
		//初次加载页面传值
		if (bdate==null) {
			bdate=new Date();
		}
		if (edate==null) {
			edate=new Date();
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("msg", "ok");//改变msg的值，使计算等待提示信息不显示
		modelMap.put("mis", mis);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_COSTCTR,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_COSTCTR,modelMap);
		}
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param itemUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public ModelAndView update(ModelMap modelMap, ItemUse itemUse) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		forecastService.update(itemUse);
		return null;
	}
	
	/**
	 * 菜品点击率表打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/print")
	public ModelAndView print(ModelMap modelMap, HttpSession session, String firmId, String type, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		ItemUse itemUse = new ItemUse();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		itemUse.setFirm(firmId);
		List<ItemUse> disList=forecastService.findAllItemUse(itemUse);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "菜品点击率表");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/print.do?firmId="+firmId);//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_FORECAST,ForecastConstants.REPORT_FORECAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出菜品点击率excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/export")
	@ResponseBody
	public void export(HttpServletResponse response, String sort, String firmId, 
			String order, HttpServletRequest request, HttpSession session) throws Exception{
		ItemUse itemUse = new ItemUse();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		itemUse.setFirm(firmId);
		List<ItemUse> disList=forecastService.findAllItemUse(itemUse);
		String fileName = "菜品点击率表";
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("itemUse", itemUse);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcel(response.getOutputStream(), disList);
		
	}
	
	/********************************************************菜品点击率end*************************************************/
	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/castList")
	public ModelAndView castList(ModelMap modelMap, HttpSession session, String type, String firmName, Page page,
			String firmId, PosSalePlan posSalePlan, Date startdate, Date enddate, Date bdate, Date edate, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			posSalePlan.setFirm(firmId);
			posSalePlan.setStartdate(startdate);
			posSalePlan.setEnddate(enddate);
			posSalePlan.setDept(CodeHelper.replaceCode(posSalePlan.getDept()));
			modelMap.put("posSalePlanList", forecastService.findPosSalePlan(posSalePlan, page));
			
		}
		//初次加载页面传值
		if (startdate==null) {
			startdate=new Date();
		}
		if (enddate==null) {
			enddate=new Date();
		}if (bdate==null) {//默认选择近一个月的时间段
	        Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(new Date());  
	        calendar.add(Calendar.DATE, -29);  
	        bdate = calendar.getTime();
		}
		if (edate==null) {
			edate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("startdate", startdate);
		modelMap.put("enddate", enddate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("posSalePlan", posSalePlan);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_FORECAST,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_FORECAST,modelMap);
		}
	}
	
	/**
	 * 分店MIS预估系统营业预估
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPosSalePlan")
	public ModelAndView calPosSalePlan(ModelMap modelMap, HttpSession session, String firmId, String firmName, 
			Date bdate, Date edate, Date startdate, Date enddate, String mis, Page page,PosSalePlan posSalePlan) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		Positn positn = new Positn();
		// 重新计算分店MIS预估系统营业预估
		if(null==mis){//总部
			positn.setArea(CodeHelper.replaceCode(firmId));//总部区域
			List<Positn> ls = positnService.findPositnByArea(positn);//根据区域查找分店
			forecastService.saveCalPosSalePlanByArea(ls,session.getAttribute("ChoiceAcct").toString(), bdate, edate, startdate, enddate,mis);
		}else{//门店
			positn.setCode(firmId);
			positn = positnService.findPositnByCode(positn);
//			forecastService.saveCalPosSalePlan(positn.getFirm(), firmId, session.getAttribute("ChoiceAcct").toString(), bdate, edate, startdate, enddate,mis);
			forecastService.saveCalPosSalePlan(firmId, firmId, session.getAttribute("ChoiceAcct").toString(), bdate, edate, startdate, enddate,mis);//xlh 2015.1.24
		} 
		//List<String> listId = Arrays.asList(firmId.split(","));
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		modelMap.put("posSalePlanList", forecastService.findPosSalePlan(posSalePlan, page));
		//初次加载页面传值
		if (bdate==null) {
			bdate=new Date();
		}
		if (edate==null) {
			edate=new Date();
		}
		if (startdate==null) {
			startdate=new Date();
		}
		if (enddate==null) {
			enddate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("startdate", startdate);
		modelMap.put("enddate", enddate);
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("posSalePlan", posSalePlan);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_FORECAST,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_FORECAST,modelMap);
		}
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateSalePlan")
	@ResponseBody
	public ModelAndView updateSalePlan(ModelMap modelMap, HttpSession session, PosSalePlan posSalePlan) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		forecastService.updateSalePlan(posSalePlan);
		return null;
	}
	
	/**
	 * 营业预估打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCast")
	public ModelAndView printCast(ModelMap modelMap, HttpSession session,String type, 
			String firmId, Date startdate, Date enddate, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosSalePlan posSalePlan = new PosSalePlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		
		//List<String> listId = Arrays.asList(firmId.split(","));
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		List<PosSalePlan> disList =  forecastService.findPosSalePlan(posSalePlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "营业预估");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("firmId", posSalePlan.getFirm());
	    map.put("startdate", DateFormat.getStringByDate(posSalePlan.getStartdate(),"yyyy-MM-dd"));
	    map.put("enddate", DateFormat.getStringByDate(posSalePlan.getEnddate(),"yyyy-MM-dd"));
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printCast.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_CAST,ForecastConstants.REPORT_CAST);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 营业预估excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCast")
	@ResponseBody
	public void exportCast(HttpServletResponse response, String sort, String order, String firmId, 
			HttpServletRequest request, HttpSession session, Date startdate, Date enddate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosSalePlan posSalePlan = new PosSalePlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		
		//List<String> listId = Arrays.asList(firmId.split(","));
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		List<PosSalePlan> disList =  forecastService.findPosSalePlan(posSalePlan);
		String fileName = "营业预估";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posSalePlan", posSalePlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcelcast(response.getOutputStream(), disList);
		
	}
/********************************************************营业预估end*************************************************/
	
/****************************************************预估菜品销售计划start*************************************************/
	
	/**
	 * 预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/planList")
	public ModelAndView planList(ModelMap modelMap, HttpSession session, String type, Page page,
			String firmId, String firmName, PosItemPlan posItemPlan, Date bdate, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			//List<String> listId = Arrays.asList(firmId.split(","));
			posItemPlan.setFirm(firmId);
			posItemPlan.setDat(bdate);
			posItemPlan.setDept(CodeHelper.replaceCode(posItemPlan.getDept()));
			modelMap.put("posItemPlanList", forecastService.findPosItemPlan(posItemPlan, page));
		}
		//初次加载页面传值
		if (null==bdate) {
			bdate=new Date();
		}
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("msg", "a");
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("posItemPlan", posItemPlan);
		modelMap.put("mis", mis);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_PLAN,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
		}
	}
	
	/**
	 * 删除预估菜品销售计划
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/deletePlan")
	public ModelAndView deletePlan(ModelMap modelMap, HttpSession session,
			String firmId, String firmName, Date bdate, String mis,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		
		PosItemPlan posItemPlan = new PosItemPlan();
		//删除预估菜品 改为删除选择的一天 不是删除所有  wjf
		posItemPlan.setDat(bdate);
		//List<String> listId = Arrays.asList(firmId.split(","));
		posItemPlan.setFirm(firmId);
		forecastService.deleteAllPlan(posItemPlan);
		if (bdate==null) {
			bdate=new Date();
		}
		modelMap.put("posItemPlanList", forecastService.findPosItemPlan(posItemPlan,page));
		modelMap.put("bdate", bdate);
		modelMap.put("msg", "a");
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("pageobj", page);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_PLAN,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
		}
	}
	
	/**
	 * 计算预估菜品销售计划
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calPlan")
	public ModelAndView calPlan(ModelMap modelMap, HttpSession session, String firmId, 
			String firmName, Date bdate, String mis,PosItemPlan posItemPlan, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		// 重新计算预估菜品销售计划
		PosSalePlan posSalePlan = new PosSalePlan();
		posSalePlan.setEnddate(bdate);
		posSalePlan.setStartdate(bdate);
		posSalePlan.setFirm(firmId);
		List<PosSalePlan> posSalePlanList = forecastService.findPosSalePlan(posSalePlan);
		ItemUse itemUse = new ItemUse();
		itemUse.setFirm(firmId);
		List<ItemUse> itemUseList = forecastService.findAllItemUse(itemUse);
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		List<PosItemPlan> posItemPlanist = forecastService.findPosItemPlan(posItemPlan, page);
		if (posSalePlanList.size()==0) {
			modelMap.put("msg", "选择的日期内有没有做营业预估，请检查营业预估!");
		}else if (itemUseList.size()==0) {
			modelMap.put("msg", "没有设定点击率，不能自动计算预估!");
		}else if (posItemPlanist.size()>0) {
			modelMap.put("msg", "请先删除数据，然后重新计算!");
		}else{
			modelMap.put("msg", "a");//标记正常计算营业预估
			// 数组存放某日四个班所对应的营业预估值
			double a[] = {posSalePlanList.get(0).getPax1(),posSalePlanList.get(0).getPax2(),posSalePlanList.get(0).getPax3(),posSalePlanList.get(0).getPax4()};
			//加入部门  posSalePlanList.get(0).getDept()
			forecastService.saveCalPlan(firmId, bdate, a,posSalePlanList.get(0).getDept());
		}
		modelMap.put("posItemPlanList", forecastService.findPosItemPlan(posItemPlan, page));
		modelMap.put("pageobj", page);
		modelMap.put("bdate", bdate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("posItemPlan", posItemPlan);
		modelMap.put("mis", mis);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_PLAN,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_PLAN,modelMap);
		}
	}
	
	/**
	 *  保存修改调整量（预估菜品销售计划）
	 * @param modelMap
	 * @param posSalePlan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePlan")
	@ResponseBody
	public ModelAndView updatePlan(ModelMap modelMap, HttpSession session, String firmId, PosItemPlan posItemPlan) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
			forecastService.updateItemPlan(posItemPlan, firmId);
		}else{
			forecastService.updateItemPlan(posItemPlan);
		}
		
		return null;
	}
	
	/**
	 * 预估菜品销售计划打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printPlan")
	public ModelAndView printPlan(ModelMap modelMap, HttpSession session, String type, String firmId, Date bdate, String mis)throws CRUDException{
		PosItemPlan posItemPlan = new PosItemPlan();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		List<PosItemPlan> disList=forecastService.findPosItemPlan(posItemPlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "分店菜品销售预估");//修改名字 wjf
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printPlan.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_PLAN,ForecastConstants.REPORT_PLAN);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("msg", "a");
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 预估菜品销售计划excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportPlan")
	@ResponseBody
	public void exportPlan(HttpServletResponse response, String sort, String order, String mis,
			String firmId, HttpServletRequest request,HttpSession session, Date bdate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosItemPlan posItemPlan = new PosItemPlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(bdate);
		List<PosItemPlan> disList=forecastService.findPosItemPlan(posItemPlan);
		String fileName = "预估菜品销售计划";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posItemPlan", posItemPlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcelPlan(response.getOutputStream(), disList);
		
	}
	
/********************************************************预估菜品销售计划end*************************************************/

/****************************************************营业预估实际对比start*************************************************/
	
	/**
	 * 营业预估实际对比
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saleCmpList")
	public ModelAndView saleCmpList(ModelMap modelMap, HttpSession session, String select, String mis, String type,
			String firmId, String firmName, PosSalePlan posSalePlan, Date startdate, Date enddate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			posSalePlan.setFirm(firmId);
			posSalePlan.setStartdate(startdate);
			posSalePlan.setEnddate(enddate);
			posSalePlan.setDept(CodeHelper.replaceCode(posSalePlan.getDept()));
			List<PosSalePlan> posSalePlanList = forecastService.findPosSalePlan(posSalePlan);
		    long m = (enddate.getTime() - startdate.getTime())/(1000*60*60*24)+1;

			if (posSalePlanList.size() < m) {
				modelMap.put("msg", "选择的日期内没有做营业预估，请检查营业预估!");
			}else {
				modelMap.put("msg", "a");
				modelMap.put("posSalePlanList", forecastService.findPosSalePlan1(posSalePlan, session.getAttribute("ChoiceAcct").toString()));
			}
		}else {
			modelMap.put("msg", "a");
		}
		//初次加载页面传值
		if (startdate==null) {
			startdate=new Date();
		}
		if (enddate==null) {
			enddate=new Date();
		}
		modelMap.put("startdate", startdate);
		modelMap.put("enddate", enddate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("posSalePlan", posSalePlan);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_SALECMP,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_SALECMP,modelMap);
		}
	}
	
	/**
	 * 营业预估实际对比打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printSaleCmp")
	public ModelAndView printSaleCmp(ModelMap modelMap, HttpSession session, String type, 
			String firmId, String firmName, Date startdate, Date enddate, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosSalePlan posSalePlan = new PosSalePlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		List<PosSalePlan> posSalePlanList = forecastService.findPosSalePlan(posSalePlan);
	    long m = (enddate.getTime() - startdate.getTime())/(1000*60*60*24)+1;

		if (posSalePlanList.size() < m) {
			modelMap.put("msg", "选择的日期内没有做营业预估，请检查营业预估!");
			modelMap.put("mis", mis);
			modelMap.put("startdate", startdate);
			modelMap.put("enddate", enddate);
			modelMap.put("firmId", firmId);
			modelMap.put("firmName", firmName);
			if (null==mis) {
				return new ModelAndView(ForecastConstants.LIST_SALECMP,modelMap);
			}else {
				return new ModelAndView(ForecastConstants.MIS_LIST_SALECMP,modelMap);
			}
		}
		List<PosSalePlan> disList = forecastService.findPosSalePlan1(posSalePlan, session.getAttribute("ChoiceAcct").toString());
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList); //list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "营业预估实际对比");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printSaleCmp.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_SALECMP,ForecastConstants.REPORT_SALECMP);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("msg", "a");
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 营业预估实际对比excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportSaleCmp")
	@ResponseBody
	public void exportSaleCmp(HttpServletResponse response, String sort, String order, String firmId, 
			HttpServletRequest request, HttpSession session, Date startdate, Date enddate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosSalePlan posSalePlan = new PosSalePlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posSalePlan.setFirm(firmId);
		posSalePlan.setStartdate(startdate);
		posSalePlan.setEnddate(enddate);
		List<PosSalePlan> disList=forecastService.findPosSalePlan1(posSalePlan, session.getAttribute("ChoiceAcct").toString());
		String fileName = "营业预估实际对比";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posSalePlan", posSalePlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportSaleCmp(response.getOutputStream(), disList);
		
	}
	
	/********************************************************营业预估实际对比end*************************************************/
	

	/****************************************************菜品预估实际对比start*************************************************/
	
	/**
	 * 菜品预估实际对比
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/costCmpList")
	public ModelAndView costCmpList(ModelMap modelMap, HttpSession session, String type, String mis,
			String firmName, String firmId, PosItemPlan posItemPlan, Date startdate, Date enddate) throws Exception{
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {//是否初次加载
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			posItemPlan.setFirm(firmId);
			posItemPlan.setStartdate(startdate);
			posItemPlan.setEnddate(enddate);
			posItemPlan.setDept(CodeHelper. replaceCode(posItemPlan.getDept()));
			List<PosItemPlan> posItemPlanList = forecastService.findPosItemPlan1(posItemPlan);
			if (posItemPlanList.size()==0) { //没有菜品销售计划
				modelMap.put("msg", "选择的日期内没有做菜品销售计划，请确认!");
			}else {
				posItemPlanList.clear();
				modelMap.put("msg", "a");
				List<String> listId = Arrays.asList(firmId.split(","));
				for (int i=0;i<listId.size();i++) {
					posItemPlan.setFirm(listId.get(i));
					posItemPlan.setStartdate(startdate);
					posItemPlan.setEnddate(enddate);
					posItemPlanList.addAll(forecastService.getList(listId.get(i), startdate, enddate, posItemPlan));
				}
				
				modelMap.put("posItemPlanList", posItemPlanList);
			}
		}else{
			modelMap.put("msg", "a");
		}
		//初次加载页面传值
		if (startdate==null) {
			startdate=new Date();
		}
		if (enddate==null) {
			enddate=new Date();
		}
		
		modelMap.put("startdate", startdate);
		modelMap.put("enddate", enddate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("posItemPlan", posItemPlan);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_COSTCMP,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_COSTCMP,modelMap);
		}
	}
	
	/**
	 * 菜品预估实际对比打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCostCmp")
	public ModelAndView printCostCmp(ModelMap modelMap, HttpSession session, String type, 
			String firmId, Date startdate, Date enddate, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosItemPlan posItemPlan = new PosItemPlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		List<PosItemPlan> disList = new ArrayList<PosItemPlan>();
		List<String> listId = Arrays.asList(firmId.split(","));
		for (int i=0;i<listId.size();i++) {
			posItemPlan.setFirm(listId.get(i));
			posItemPlan.setStartdate(startdate);
			posItemPlan.setEnddate(enddate);
			disList.addAll(forecastService.getList(listId.get(i), startdate, enddate, posItemPlan));
		}
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList); //list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>();
        parameters.put("report_name", "菜品预估实际对比");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printCostCmp.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_COSTCMP,ForecastConstants.REPORT_COSTCMP);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 菜品预估实际对比excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCostCmp")
	@ResponseBody
	public void exportCostCmp(HttpServletResponse response, String sort, String order, String firmId, 
			HttpServletRequest request, HttpSession session, Date startdate, Date enddate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosItemPlan posItemPlan = new PosItemPlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		List<PosItemPlan> disList = new ArrayList<PosItemPlan>();
		List<String> listId = Arrays.asList(firmId.split(","));
		for (int i=0;i<listId.size();i++) {
			posItemPlan.setFirm(listId.get(i));
			posItemPlan.setStartdate(startdate);
			posItemPlan.setEnddate(enddate);
			disList.addAll(forecastService.getList(listId.get(i), startdate, enddate, posItemPlan));
		}
		String fileName = "菜品预估实际对比";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posItemPlan", posItemPlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportCostCmp(response.getOutputStream(), disList);
		
	}
	
	/********************************************************菜品预估实际对比end*************************************************/
	

	/****************************************************菜品预估沽清对比start*************************************************/
	
	/**
	 * 菜品预估沽清对比
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cmpList")
	public ModelAndView cmpList(ModelMap modelMap, HttpSession session, String type, String firmId, 
			String firmName, PosItemPlan posItemPlan, Date startdate, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null==type) {
			if (null == firmId) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					firmId=accountPositn.getPositn().getCode();
				}
			}
			posItemPlan.setFirm(firmId);
			posItemPlan.setDat(startdate);
			posItemPlan.setDept(CodeHelper. replaceCode(posItemPlan.getDept()));
			List<PosItemPlan> posItemPlanList = forecastService.getCmpList( posItemPlan);
			modelMap.put("posItemPlanList", posItemPlanList);
		}
		//初次加载页面传值
		if (startdate==null) {
			startdate=new Date();
		}
		
		modelMap.put("startdate", startdate);
		modelMap.put("firmId", firmId);
		modelMap.put("firmName", firmName);
		modelMap.put("mis", mis);
		modelMap.put("posItemPlan", posItemPlan);
		if (null==mis) {
			return new ModelAndView(ForecastConstants.LIST_CMP,modelMap);
		}else {
			return new ModelAndView(ForecastConstants.MIS_LIST_CMP,modelMap);
		}
	}
	
	/**
	 * 菜品预估沽清对比打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printCmp")
	public ModelAndView printCmp(ModelMap modelMap, HttpSession session, String type, PosItemPlan posItemPlan,
			String firmId, Date startdate, Date enddate, String mis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(startdate);
		List<PosItemPlan> disList = forecastService.getCmpList(posItemPlan);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList); //list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "菜品预估沽清对比");  
        parameters.put("report_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/forecast/printCmp.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ForecastConstants.REPORT_CMP,ForecastConstants.REPORT_CMP);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
        modelMap.put("mis", mis);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 菜品预估沽清对比excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCmp")
	@ResponseBody
	public void exportCmp(HttpServletResponse response, String sort, String order, String firmId, PosItemPlan posItemPlan,
			HttpServletRequest request,HttpSession session, Date startdate, Date enddate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosSalePlan posSalePlan = new PosSalePlan();
		if (null == firmId) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				firmId=accountPositn.getPositn().getCode();
			}
		}
		posItemPlan.setFirm(firmId);
		posItemPlan.setDat(startdate);
		List<PosItemPlan> disList = forecastService.getCmpList( posItemPlan);
		String fileName = "菜品预估沽清对比";
		Map<String, Object> condition = new HashMap<String,Object>();
		condition.put("posSalePlan", posSalePlan);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		forecastService.exportExcelcmp(response.getOutputStream(), disList);
		
	}
	
	/********************************************************菜品预估沽清对比end*************************************************/
	/**
	 * 加工间   多选框
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectArea")
	public ModelAndView selectPositnex(ModelMap modelMap, String code) throws Exception { 
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codedes=new CodeDes();
		codedes.setTyp("3");
		modelMap.put("listArea", codeDesService.findCodeDes(codedes));
		modelMap.put("code", code);
		return new ModelAndView(ExplanConstants.SELECT_CODEDES, modelMap);
	}
}
