package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.InventoryDemomContants;
import com.choice.scm.domain.InventoryDemoFirm;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.ExecSql;
import com.choice.scm.service.InventoryDemomService;
import com.choice.scm.service.PositnService;

/**
 * 盘点模板
 * @author 孙胜彬
 */
@Controller
@RequestMapping(value="inventoryDemom")
public class InventoryDemomController {

	@Autowired
	private InventoryDemomService inventoryDemomService;
	@Autowired
	private PositnService positnService;
	@Autowired
	ExecSql execSql;
	
	/**
	 * 盘点模板主表查询
	 * @param modelMap
	 * @param page
	 * @param inventoryDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listInventoryDemom")
	public ModelAndView listInventoryDemom(ModelMap modelMap,Page page,InventoryDemom inventoryDemom) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<InventoryDemom> listInventoryDemom=inventoryDemomService.listInventoryDemom(inventoryDemom, page);
		modelMap.put("listInventoryDemom", listInventoryDemom);
		modelMap.put("pageobj", page);
		modelMap.put("inventoryDemom", inventoryDemom);
		return new ModelAndView(InventoryDemomContants.LIST_LISTINVENTORYDEMOM,modelMap);
	}
	
	/**
	 * 盘点模板子表查询
	 * @param modelMap
	 * @param page
	 * @param inventoryDemod
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listInventoryDemod")
	public ModelAndView listInventoryDemod(ModelMap modelMap, InventoryDemod inventoryDemod) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<InventoryDemod> listInventoryDemod = inventoryDemomService.listInventoryDemod(inventoryDemod);
		modelMap.put("listInventoryDemod", listInventoryDemod);
		return new ModelAndView(InventoryDemomContants.LIST_LISTINVENTORYDEMOD,modelMap);
	}
	
	/**
	 * 删除盘点模板
	 * @param ids
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("deleteInventoryDemom")
	public ModelAndView deleteInventoryDemom(String ids) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventoryDemomService.deleteInventoryDemod(ids);
		inventoryDemomService.deleteInventoryDemom(ids);
		inventoryDemomService.deleteInventoryDemoFirm(ids);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 * 新增盘点模板页面
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("addInventoryDemom")
	public ModelAndView addInventoryDemom(ModelMap modelMap,String chkstodemono,HttpSession session,InventoryDemod inventoryDemod) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sta", "add");
		return new ModelAndView(InventoryDemomContants.ADD_LISTINVENTORYDEMOM,modelMap);
	}
	
	/**
	 * 新增盘点模板
	 * @param inventoryDemom
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("saveInventoryDemom")
	@ResponseBody
	public String saveInventoryDemom(InventoryDemom inventoryDemom,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return inventoryDemomService.saveInventoryDemomm(inventoryDemom,session);
	}
	
	/**
	 * 修改盘点模板页面
	 * @param modelMap
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updInventoryDemom")
	public ModelAndView updInventoryDemom(ModelMap modelMap,InventoryDemom inventoryDemom,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<InventoryDemom> listInventoryDemom=inventoryDemomService.listInventoryDemom(inventoryDemom, page);
		InventoryDemod inventoryDemod=new InventoryDemod();
		inventoryDemod.setChkstodemono(inventoryDemom.getChkstodemono());
		List<InventoryDemod> listInventoryDemod=inventoryDemomService.listInventoryDemod(inventoryDemod);
		modelMap.put("inventoryDemom", listInventoryDemom.get(0));
		modelMap.put("listInventoryDemod", listInventoryDemod);
		modelMap.put("sta", "add");
		return new ModelAndView(InventoryDemomContants.UPDATE_LISTINVENTORYDEMOM,modelMap);
	}
	
	/**
	 * 修改盘点模板
	 * @param inventoryDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updateInventoryDemom")
	@ResponseBody
	public int updateInventoryDemom(InventoryDemom inventoryDemom) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return inventoryDemomService.updInventoryDemom(inventoryDemom);
	}
	
	/**
	 * 盘点模板适用分店查询
	 * @param modelMap
	 * @param page
	 * @param inventoryDemoFirm
	 * @return
	 */
	@RequestMapping("listInventoryDemoFirm")
	public ModelAndView listInventoryDemoFirm(ModelMap modelMap,InventoryDemoFirm inventoryDemoFirm,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("inventoryDemoFirm", inventoryDemomService.listInventoryDemoFirm(inventoryDemoFirm));
		modelMap.put("chksto", inventoryDemoFirm.getChksto());
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("positn", positn);
		return new ModelAndView(InventoryDemomContants.LIST_LISTINVENTORYDEMOFIRM,modelMap);
	}
	
	/**
	 * 新增适用分店
	 * @param inventoryDemoFirm
	 * @return
	 */
	@RequestMapping("saveInventoryDemoFirm")
	public ModelAndView saveInventoryDemoFirm(InventoryDemoFirm inventoryDemoFirm) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventoryDemomService.saveInventoryDemoFirm(inventoryDemoFirm);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
}
