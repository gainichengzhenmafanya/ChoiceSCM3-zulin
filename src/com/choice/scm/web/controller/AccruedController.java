package com.choice.scm.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.cert.CRLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.AccruedConstants;
import com.choice.scm.domain.Accpaysum;
import com.choice.scm.domain.Braset;
import com.choice.scm.domain.BrasetItem;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Recsum;
import com.choice.scm.service.AccruedService;

/**
 * 应收应付
 * Created by mc on 14-12-27.
 */
@Controller
@RequestMapping("accrued")
public class AccruedController {
    @Autowired
    private AccruedService accruedService;
    /*----------------------------应付款汇总 start--------------------------------*/
    /**
     * 应付款汇总-显示
     * The accounts payable summary
     * @param modelMap
     * @return
     */
    @RequestMapping("/accpaysumList")
    public ModelAndView accpaysum(ModelMap modelMap){
        modelMap.put("pageobj",new Page());
        return new ModelAndView(AccruedConstants.ACCPAYSUM_LIST,modelMap);
    }
    /**
     * 获取 应付款汇总
     * @param modelMap
     * @param accpaysum
     * @param page
     * @return
     * @throws CRLException
     */
    @RequestMapping("/findaccpaysum")
    public ModelAndView findAccpaysum(ModelMap modelMap,HttpSession session,Accpaysum accpaysum,Page page) throws CRLException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        accpaysum.setAccountId(session.getAttribute("accountId").toString());
        modelMap.put("accpaysumList",accruedService.findAccpaysum(accpaysum,page));
        modelMap.put("pageobj",page);
        modelMap.put("accpaysum",accpaysum);
        return new ModelAndView(AccruedConstants.ACCPAYSUM_LIST,modelMap);
    }
    /**
     * 导出
     * @return
     * @throws Exception
     */
    @RequestMapping("/exportAccpaysum")
    @ResponseBody
    public boolean exportAccpaysum(HttpServletResponse response,HttpServletRequest request, HttpSession session,Accpaysum accpaysum) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        accpaysum.setAccountId(session.getAttribute("accountId").toString());
        List<Accpaysum> accpaysumList = accruedService.findAccpaysum(accpaysum, new Page() {{
            setMaxSize(Integer.MAX_VALUE);
        }});
        String fileName = "应付款汇总";
        setExpArg(response,request,fileName);
        return accruedService.exportAccpaysum(response.getOutputStream(), accpaysumList);
    }

    /*----------------------------应付款汇总 end--------------------------------*/
    /*----------------------------应收款汇总 start--------------------------------*/
    /**
     * 应收款汇总-显示
     * Receivables summary
     * @return
     */
    @RequestMapping("/recsumList")
    public ModelAndView recsum(ModelMap modelMap){
        modelMap.put("pageobj",new Page());
        return new ModelAndView(AccruedConstants.RECSUM_LIST,modelMap);
    }

    /**
     * 获取 应收款汇总
     * @param modelMap
     * @param recsum
     * @param page
     * @return
     * @throws CRLException
     */
    @RequestMapping("/findrecsum")
    public ModelAndView findRecsum(ModelMap modelMap,HttpSession session,Recsum recsum,Page page) throws CRLException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        recsum.setAccountId(session.getAttribute("accountId").toString());
        modelMap.put("recsumList",accruedService.findRecsum(recsum,page));
        modelMap.put("pageobj",page);
        modelMap.put("recsum",recsum);
        return new ModelAndView(AccruedConstants.RECSUM_LIST,modelMap);
    }
    /**
     * 导出
     * @return
     * @throws Exception
     */
    @RequestMapping("/exportRecsum")
    @ResponseBody
    public boolean exportRecsum(HttpServletResponse response,HttpServletRequest request, HttpSession session,Recsum recsum) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        recsum.setAccountId(session.getAttribute("accountId").toString());
        List<Recsum> recsumList = accruedService.findRecsum(recsum, new Page() {{
            setMaxSize(Integer.MAX_VALUE);
        }});
        String fileName = "应收款汇总";
        setExpArg(response,request,fileName);
        return accruedService.exportRecsum(response.getOutputStream(), recsumList);
    }
    public void setExpArg(HttpServletResponse response,HttpServletRequest request,String fileName) throws UnsupportedEncodingException {
        response.setContentType("application/msexcel; charset=UTF-8");
        if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
            //IE
            fileName = URLEncoder.encode(fileName, "UTF-8");
        }else{
            fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
        }
        response.setHeader("Content-disposition", "attachment; filename="+ fileName + ".xls");
    }
    /*----------------------------应收款汇总 end--------------------------------*/
    /*----------------------------分店结算 start--------------------------------*/

    /**
     * 分店结算-显示
     * Branch settlement
     * @param modelMap
     * @return
     */
    @RequestMapping("/brasetList")
    public ModelAndView braset(ModelMap modelMap){
        modelMap.put("pageobj",new Page());
        return new ModelAndView(AccruedConstants.BRASET_LIST,modelMap);
    }

    /**
     * 分店结算-查询
     * @param modelMap
     * @param page
     * @return
     */
    @RequestMapping("/findbraset")
    public ModelAndView findBraset(ModelMap modelMap,HttpSession session,Braset braset,Page page) throws CRLException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        braset.setAccountId(session.getAttribute("accountId").toString());
        braset.setLocale(session.getAttribute("locale").toString());
        modelMap.put("brasetList",accruedService.findBraset(braset, page));
        modelMap.put("pageobj",page);
        modelMap.put("braset",braset);
        return new ModelAndView(AccruedConstants.BRASET_LIST,modelMap);
    }

    /**
     * 获取分店结算-支付明细
     * @param modelMap
     * @param braset
     * @return
     */
    @RequestMapping("/findbrasetitem")
    public ModelAndView findBrasetItem(ModelMap modelMap,HttpSession session,Braset braset) throws CRLException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(braset.getFirm()!=null&&!"".equals(braset.getFirm())){
            braset.setAccountId(session.getAttribute("accountId").toString());
            modelMap.put("brasetitemList",accruedService.findBrasetItem(braset));
        }
        return new ModelAndView(AccruedConstants.BRASET_ITEM_LIST,modelMap);
    }

    /**
     * 打开付款界面
     * @return
     */
    @RequestMapping("/paymoney")
    public ModelAndView payMoney(ModelMap modelMap,Braset braset){
        modelMap.put("braset",braset);
        return new ModelAndView(AccruedConstants.PAY_MONEY,modelMap);
    }
    /**
     * 付款
     * @return
     */
    @RequestMapping("/topaymoney")
    public ModelAndView toPayMoney(ModelMap modelMap,HttpSession session,BrasetItem brasetItem){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            brasetItem.setAcct(session.getAttribute("ChoiceAcct").toString());
            brasetItem.setMadeby(session.getAttribute("accountName").toString());
            String result=accruedService.toPayMoney(brasetItem);
            if("ok".equals(result)) {
                return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
            }else{
                modelMap.put("msg",result);
                return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
            }
        } catch (CRLException e) {
            modelMap.put("msg",e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
    }

    /**
     * 单据审核并结束
     * @param modelMap
     * @param brasetItem
     * @return
     */
    @RequestMapping("/checkedbill")
    public ModelAndView checkedBill(ModelMap modelMap,BrasetItem brasetItem){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            accruedService.checkedBill(brasetItem);
            return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
    }

    /**
     * 期初界面
     * @param modelMap
     * @param braset
     * @return
     */
    @RequestMapping("/addmoney")
    public ModelAndView addMoney(ModelMap modelMap,Braset braset){
        modelMap.put("braset",braset);
        return new ModelAndView(AccruedConstants.ADD_MONEY,modelMap);
    }
    /**
     * 添加期初
     * @param modelMap
     * @param chkoutm
     * @return
     */
    @RequestMapping("/toaddmoney")
    public ModelAndView toAddMoney(ModelMap modelMap,HttpSession session,Chkoutm chkoutm){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            chkoutm.setChecby(session.getAttribute("accountName").toString());
            chkoutm.setMadeby(session.getAttribute("accountName").toString());
            chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
            accruedService.addChkoutm(chkoutm);
            return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
        } catch (CRLException e) {
            e.printStackTrace();
        }
        modelMap.put("msg","操作失败！");
        return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
    }
    /*----------------------------分店结算 end--------------------------------*/

}
