package com.choice.scm.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.DeliverPlatConstants;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.domain.Arrivald;
import com.choice.scm.domain.Arrivalm;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.DeliverPlatService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.FirmDeliverService;
import com.choice.scm.service.report.RkHuizongChaxunService;
import com.choice.scm.service.report.RkMingxiChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "deliverPlat")
public class DeliverPlatController {

	@Autowired
	private DeliverPlatService deliverPlatService;
	@Autowired
	private FirmDeliverService firmDeliverService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private RkHuizongChaxunService rkHuizongChaxunService;
	@Autowired
	private RkMingxiChaxunService rkMingxiChaxunService;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private Page pager;
	@Autowired
	private ChkinmService chkinmService;
	
	/*****************************************账号管理 --账号关联供应商***********************************************/
	
	/***
	 * 账号关联供应商
	 * @param session
	 * @param accountId
	 * @param deliverCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveAccountSupplier")
	@ResponseBody
	public int saveAccountSupplier(HttpSession session, String accountId, String deliverCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		accountId = CodeHelper.replaceCode(accountId);
		return deliverPlatService.saveAccountSupplier(accountId,deliverCode);
	}
	
	/********************************************1.门店报货查询******************************************/
	
	/***
	 * 查询门店验货数据
	 * @param modelMap
	 * @param page
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listChkstom")
	public ModelAndView listChkstom(ModelMap modelMap,Page page,HttpSession session, Dis dis, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询门店直配报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		if(null != action && "init".equals(action)){
			Date date = new Date();
			dis.setMaded(date);
			//获取当前账号所属供应商
			String account = session.getAttribute("accountId").toString();
			Deliver deliver = deliverPlatService.findDeliverByAccount(account);
			if(null != deliver)
				dis.setDeliverCode(deliver.getCode());
			dis.setDeliveryn("N");//默认未确认
		}else{
			//查询门店报货数据
			dis.setAcct(session.getAttribute("ChoiceAcct").toString());
			dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
			String firmCode = dis.getFirmCode();
			dis.setFirmCode(CodeHelper.replaceCode(firmCode));
			List<Dis> disList = null;
			if("Y".equals(dis.getYndo())){
				disList = deliverPlatService.findChkstomSum(dis, page);
			}else{
				disList = deliverPlatService.findChkstom(dis, page);
			}
			dis.setFirmCode(firmCode);
			modelMap.put("disList", disList);
		}
		modelMap.put("pageobj", page);
		modelMap.put("dis", dis);
		modelMap.put("isReportJmj", 0);
		if("Y".equals(dis.getYndo())){
			return new ModelAndView(DeliverPlatConstants.LIST_CHKSTOMSUM, modelMap);
		}else{
			return new ModelAndView(DeliverPlatConstants.LIST_CHKSTOM, modelMap);
		}
	}
	
	/***
	 * 确认送货
	 * @param ids
	 * @return
	 */
	@RequestMapping("/confirmChkstom")
	@ResponseBody
	public int confirmChkstom(HttpSession session, String ids) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"确认门店直配报货单:"+ids,session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		return deliverPlatService.confirmChkstom(ids);
	}
	
	/**
	 * 导出门店验货
	 * @param response
	 * @param request
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportChkstom")
	@ResponseBody
	public boolean exportChkstom(HttpServletResponse response,HttpServletRequest request,HttpSession session,Dis dis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出门店直配报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		String firmCode = dis.getFirmCode();
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = null;
		String fileName = "门店报货查询";
		if("Y".equals(dis.getYndo())){
			disList = deliverPlatService.findChkstomSum(dis, null);
			fileName = "门店报货汇总";
		}else{
			disList = deliverPlatService.findChkstom(dis, null);
		}
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		if("Y".equals(dis.getYndo())){
			return deliverPlatService.exportChkstomSum(response.getOutputStream(),disList,dis);
		}else{
			return deliverPlatService.exportChkstom(response.getOutputStream(),disList,dis);
		}
	}
	
	/***
	 * 打印分拨单
	 * @param modelMap
	 * @param session
	 * @param dis
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/printIndent")
	public ModelAndView printIndent(ModelMap modelMap,HttpSession session,Dis dis, String type)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("indent");
		param.put("danjuTyp", "indent");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("deliverCode", dis.getDeliverCode());
		param.put("deliveryn", dis.getDeliveryn());
		param.put("ind", DateFormat.getStringByDate(dis.getInd(),"yyyy-MM-dd"));
		param.put("vouno", dis.getVouno());
		String firmCode = dis.getFirmCode();
		param.put("firmCode", firmCode);
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliverPlat/printIndent.do");//传入回调路径
		//根据关键字查询
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = deliverPlatService.findChkstom(dis, null);
		//记录合计数
		Map<String,Object> countMap=new HashMap<String,Object>();
		//所有的数据
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		String sp_code="";
		//计算合计
		double count=0;
		//------------------------------------------------ 
	    int tdnum1=1;//标记列1
	    int tdnum2=1;//标记列2
	    int tdnum3=1;//标记列3
	    //存放数据，往报表页面传
	    Map<String,Object> resultMap=new HashMap<String,Object>();
    	//报表文件同种物资类似行列转换用到的方法
	    for(int i=0;i<disList.size();i++){
	    	String memo0 = null == disList.get(i).getMemo()?"":disList.get(i).getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		memo1 = memo0.split("##")[1];
	    		memo0 = memo0.split("##")[0];
	    	}
		    if(sp_code.equals(disList.get(i).getSp_code())){
				  if(tdnum1==4){
					   tdnum1=1;
					   tdnum2=1;
					   tdnum3=1;
					   list.add(resultMap);
					   resultMap=new HashMap<String,Object>();
					   resultMap.put("STR", "");
					   //wangjie 物资编码
					   resultMap.put("SP_CODE",disList.get(i).getSp_code());
					   resultMap.put("SP_NAME",disList.get(i).getSp_name());
					   resultMap.put("SP_DESC",null == disList.get(i).getSp_desc()?"":disList.get(i).getSp_desc());
					   resultMap.put("UNIT",disList.get(i).getUnit3());
				  }
				  resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				  resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
				  resultMap.put("MEMO"+tdnum3++, memo0); 
				  resultMap.put("MEMO1"+(tdnum3-1), memo1); 
			      count+=disList.get(i).getAmount1sto();//采购数量
			      countMap.put(disList.get(i).getSp_name(),count);
		     }else{
			      if(resultMap.size()!=0){
			    	  list.add(resultMap);
			      }
			      tdnum1 = 1;
			      tdnum2 = 1;
			      tdnum3 = 1;
			      count = 0;
			      resultMap=new HashMap<String,Object>();
			      resultMap.put("STR", "STR");
			      resultMap.put("SP_CODE",disList.get(i).getSp_code());
			      resultMap.put("SP_NAME",disList.get(i).getSp_name());
			      resultMap.put("SP_DESC",null == disList.get(i).getSp_desc()?"":disList.get(i).getSp_desc());
			      resultMap.put("UNIT",disList.get(i).getUnit3());
			      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
			      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount1sto());
			      resultMap.put("MEMO"+tdnum3++, memo0); 
			      resultMap.put("MEMO1"+(tdnum3-1), memo1);
			      count=disList.get(i).getAmount1sto();
			      countMap.put(disList.get(i).getSp_code(),count);
			      sp_code=disList.get(i).getSp_code();
		     }
	    }
	    list.add(resultMap);//将最后一行加上
		//计算完合计，存放进map
		for (int i = 0; i < list.size(); i++) {
			Map<String,Object> map=list.get(i);
			map.put("TOTALAMT", countMap.get(map.get("SP_NAME")));
			countMap.remove(map.get("SP_NAME"));
		}
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        parameters.put("report_name", deliverPlatService.findDeliverByAccount(session.getAttribute("accountId").toString()).getDes()+"分拨单"); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", list);
	    modelMap.put("parameters", parameters);
	    Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverPlatConstants.REPORT_INDENT_URL,DeliverPlatConstants.REPORT_INDENT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 打印配送单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/printDelivery")
	public ModelAndView printDelivery(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("delivery");
		param.put("danjuTyp", "delivery");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("deliverCode", dis.getDeliverCode());
		param.put("ind", DateFormat.getStringByDate(dis.getInd(),"yyyy-MM-dd"));
		param.put("vouno", dis.getVouno());
		String firmCode = dis.getFirmCode();
		param.put("firmCode", firmCode);
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliverPlat/printDelivery.do");//传入回调路径
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = deliverPlatService.findChkstom(dis, null);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        parameters.put("report_name", deliverPlatService.findDeliverByAccount(session.getAttribute("accountId").toString()).getDes()+"配送清单"); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverPlatConstants.REPORT_DELIVERY_URL,DeliverPlatConstants.REPORT_DELIVERY_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/***
	 * 打印验货单
	 * @param modelMap
	 * @param session
	 * @param dis
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/printInspection")
	public ModelAndView printInspection(ModelMap modelMap,HttpSession session,Dis dis,String type)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户前台输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("inspection");
		param.put("danjuTyp", "inspection");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("deliverCode", dis.getDeliverCode());
		param.put("ind", DateFormat.getStringByDate(dis.getInd(),"yyyy-MM-dd"));
		param.put("vouno", dis.getVouno());
		String firmCode = dis.getFirmCode();
		param.put("firmCode", firmCode);
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliverPlat/printInspection.do");//传入回调路径
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = deliverPlatService.findChkstom(dis, null);
		//根据关键字进行查询
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        parameters.put("report_name", deliverPlatService.findDeliverByAccount(session.getAttribute("accountId").toString()).getDes()+"验货单"); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountNames").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表用输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverPlatConstants.REPORT_INSPECTION_URL,DeliverPlatConstants.REPORT_INSPECTION_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/********************************************2.门店验货确认******************************************/
	
	/***
	 * 查询门店验货数据
	 * @param modelMap
	 * @param page
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listInspection")
	public ModelAndView listInspection(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询门店直配验货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		if(null == dis.getBdat()){
			Date date = new Date();
			dis.setBdat(date);
			dis.setEdat(date);
			//获取当前账号所属供应商
			String account = session.getAttribute("accountId").toString();
			Deliver deliver = deliverPlatService.findDeliverByAccount(account);
			if(null != deliver)
				dis.setDeliverCode(deliver.getCode());
			dis.setDeliveryn("0");//默认未确认
		}else{
			//查询门店验货数据(到货单表)
			dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
			String firmCode = dis.getFirmCode();
			dis.setFirmCode(CodeHelper.replaceCode(firmCode));
			List<Dis> disList = null;
			if("Y".equals(dis.getYndo())){
				disList = deliverPlatService.findInspectionSum(dis, page);
			}else{
				disList = deliverPlatService.findInspection(dis, page);
			}
			dis.setFirmCode(firmCode);
			modelMap.put("disList", disList);
		}
		modelMap.put("pageobj", page);
		modelMap.put("dis", dis);
		modelMap.put("isReportJmj", 0);
		if("Y".equals(dis.getYndo())){
			return new ModelAndView(DeliverPlatConstants.LIST_INSPECTIONSUM, modelMap);
		}else{
			return new ModelAndView(DeliverPlatConstants.LIST_INSPECTION, modelMap);
		}
	}
	
	/***
	 * 确认验货
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/confirmInspection")
	@ResponseBody
	public int confirmInspection(HttpSession session, Dis dis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String id = dis.getId();
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"确认门店直配验货单:"+id,session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		dis.setId(CodeHelper.replaceCode(id));
		return deliverPlatService.confirmIns(dis);
	}
	
	/**
	 * 导出门店验货
	 * @param response
	 * @param request
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportInspection")
	@ResponseBody
	public boolean exportInspection(HttpServletResponse response,HttpServletRequest request,HttpSession session,Dis dis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出门店直配验货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		String firmCode = dis.getFirmCode();
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = null;
		String fileName = "门店验货确认";
		if("Y".equals(dis.getYndo())){
			disList = deliverPlatService.findInspectionSum(dis, null);
			fileName = "门店验货汇总";
		}else{
			disList = deliverPlatService.findInspection(dis, null);
		}
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		if("Y".equals(dis.getYndo())){
			return deliverPlatService.exportInsSum(response.getOutputStream(),disList,dis);
		}else{
			return deliverPlatService.exportIns(response.getOutputStream(),disList,dis);
		}
	}
	
	/********************************************************门店验货查询 *******************************************************/
	
	/***
	 * 查询门店验货数据
	 * @param modelMap
	 * @param page
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listInspectionNormal")
	public ModelAndView listInspectionNormal(ModelMap modelMap,Page page,HttpSession session, Dis dis, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询门店直配验货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		
		if(null == dis.getBdat()){
			Date date = new Date();
			dis.setBdat(date);
			dis.setEdat(date);
			//获取当前账号所属供应商
			String account = session.getAttribute("accountId").toString();
			Deliver deliver = deliverPlatService.findDeliverByAccount(account);
			if(null != deliver)
				dis.setDeliverCode(deliver.getCode());
		}else{
			dis.setChkyh("Y");
			dis.setAcct(session.getAttribute("ChoiceAcct").toString());
			dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
			String firmCode = dis.getFirmCode();
			dis.setFirmCode(CodeHelper.replaceCode(firmCode));
			List<Dis> disList = null;
			if("Y".equals(dis.getYndo())){
				disList = deliverPlatService.findChkstomSum(dis, page);
			}else{
				disList = deliverPlatService.findChkstom(dis, page);
			}
			dis.setFirmCode(firmCode);
			modelMap.put("disList", disList);
		}
		modelMap.put("pageobj", page);
		modelMap.put("dis", dis);
		modelMap.put("isReportJmj", 0);
		if("Y".equals(dis.getYndo())){
			return new ModelAndView(DeliverPlatConstants.LIST_INSPECTIONNORMALSUM, modelMap);
		}else{
			return new ModelAndView(DeliverPlatConstants.LIST_INSPECTIONNORMAL, modelMap);
		}
	}
	
	/**
	 * 导出门店验货
	 * @param response
	 * @param request
	 * @param session
	 * @param dis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportInspectionNormal")
	@ResponseBody
	public boolean exportInspectionNormal(HttpServletResponse response,HttpServletRequest request,HttpSession session,Dis dis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.EXPORT,
				"导出门店直配验货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		dis.setChkyh("Y");
		dis.setAcct(session.getAttribute("ChoiceAcct").toString());
		dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		dis.setAccountId(session.getAttribute("accountId").toString());
		String firmCode = dis.getFirmCode();
		dis.setFirmCode(CodeHelper.replaceCode(firmCode));
		List<Dis> disList = null;
		String fileName = "门店验货查询";
		if("Y".equals(dis.getYndo())){
			disList = deliverPlatService.findChkstomSum(dis, null);
			fileName = "门店报货汇总";
		}else{
			disList = deliverPlatService.findChkstom(dis, null);
		}
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		if("Y".equals(dis.getYndo())){
			return deliverPlatService.exportInsSumNormal(response.getOutputStream(),disList,dis);
		}else{
			return deliverPlatService.exportInsNormal(response.getOutputStream(),disList,dis);
		}
	}
	
	/********************************************************3 供应商对账查询 *******************************************************/
	
	/**
	 * 供应商结算
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/listChkinmByDeliver")
	public ModelAndView listChkinmByDeliver(ModelMap modelMap, Chkinm chkinm, Page page, Date madedEnd, String checkOrNot,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String account = session.getAttribute("accountId").toString();
        if (null == chkinm.getMaded() || null == madedEnd) {
            chkinm.setMaded(new Date());
            madedEnd = new Date();
			Deliver deliver = deliverPlatService.findDeliverByAccount(account);
			if(null != deliver)
				chkinm.setDeliver(deliver);
        } else {
        	String cwqx = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX");
        	List<Chkinm> listChkinm = deliverPlatService.findAllChkinmNew(chkinm, page, madedEnd,session.getAttribute("locale").toString(), cwqx, account);
            Chkinm totalChkinm = chkinmService.findAllChkinmTotal("check", chkinm, madedEnd);
            modelMap.put("listChkinm", listChkinm);
            modelMap.put("totalChkinm", totalChkinm);
        }
        modelMap.put("pageobj", page);
        modelMap.put("Chkinm", chkinm);
        modelMap.put("madedEnd", madedEnd);
        modelMap.put("checkOrNot", checkOrNot);
		return new ModelAndView(DeliverPlatConstants.LIST_CHKINMBYDELIVER,modelMap);
	}
	
	/**************************************************供应商入库汇总查询*************************************************************/
	/**
	 * 跳转到入库汇总查询报表页面
	 * @return
	 */
	@RequestMapping("/toChkinDetailSum")
	public ModelAndView toChkinDetailSum(ModelMap modelMap, HttpSession session, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取当前账号所属供应商
		String account = session.getAttribute("accountId").toString();
		Deliver deliver = deliverPlatService.findDeliverByAccount(account);
		if(null != deliver)
			supplyAcct.setDelivercode(deliver.getCode());
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_CHKINCSQ);
		return new ModelAndView(DeliverPlatConstants.REPORT_SHOW_CHKINDETAILSUM,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkinDSHeaders")
	@ResponseBody
	public Object getChkinDSHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINCSQ);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKINCSQ));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_CHKINCSQ_FROZEN);
		return columns;
	}
	/**
	 * 查询入库汇总查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinDetailSum")
	@ResponseBody
	public Object ChkinDetailSum(ModelMap modelMap, HttpSession session, String page,
			String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		// 分店是否使用库存系统
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkHuizongChaxunService.findChkinSumQuery(condition,pager);
	}
	/**
	 * 打印入库汇总查询报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkinDetailSum")
	public ModelAndView viewChkoutDetailSum(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("supplyAcct", supplyAcct);
		String deliverCode = supplyAcct.getDelivercode();
		modelMap.put("List",rkHuizongChaxunService.findChkinSumQuery(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	 	Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",deliverCode);
	    modelMap.put("actionMap", params);
	    parameters.put("report_name", "入库单明细汇总");
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/deliverPlat/printChkinDetailSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverPlatConstants.REPORT_PRINT_URL_CHKINDETAILSUM,
	 			DeliverPlatConstants.REPORT_EXP_URL_CHKINDETAILSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出入库汇总查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinDetailSum")
	@ResponseBody
	public void exportChkinDetailSum(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库汇总查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINCSQ);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), rkHuizongChaxunService.findChkinSumQuery(condition,pager).getRows(), 
				"入库汇总查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKINCSQ));		
	}
	
	/******************************************供应商端 入库明细查询  start*********************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkinDQHeaders")
	@ResponseBody
	public Object getChkinDQHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKIN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.DELIVER_BASICINFO_REPORT_CHKIN));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_CHKIN_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到入库明细查询报表页面
	 * @return
	 * @throws CRUDException 
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/toChkinDetailS")
	public ModelAndView toChkinDetailS(ModelMap modelMap, HttpSession session, SupplyAcct supplyAcct) throws CRUDException, UnsupportedEncodingException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取当前账号所属供应商
		String account = session.getAttribute("accountId").toString();
		Deliver deliver = deliverPlatService.findDeliverByAccount(account);
		if(null != deliver)
			supplyAcct.setDelivercode(deliver.getCode());
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_CHKIN);
		return new ModelAndView(DeliverPlatConstants.REPORT_SHOW_CHKINDETAILS,modelMap);
	}
	
	/**
	 * 查询入库明细查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinDetailS")
	@ResponseBody
	public Object ChkinDetailS(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkMingxiChaxunService.findChkinDetailS(condition,pager);
	}
	/**
	 * 打印入库库明细查询报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkinDetailS")
	public ModelAndView printChkinDetailS(ModelMap modelMap,Page pager,HttpSession session,SupplyAcct supplyAcct,String type)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("des",supplyAcct.getDes());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",rkMingxiChaxunService.findChkinDetailS(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "入库单明细查询");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/deliverPlat/printChkinDetailS.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverPlatConstants.REPORT_PRINT_URL_CHKINDETAILS,
	 			DeliverPlatConstants.REPORT_EXP_URL_CHKINDETAILS);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出入库明细查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinDetailS")
	@ResponseBody
	public void exportChkinDetailS(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库明细查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKIN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), rkMingxiChaxunService.findChkinDetailS(condition,pager).getRows(), 
				"入库明细查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.DELIVER_BASICINFO_REPORT_CHKIN));
		
	}
	
	/*********************************************供应商端 入库明细查询end**********************************************************/
	
	
	/****************************************************九毛九供应商直配验货确认功能 20160816wjf*****************************************************************/
	
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listArrivalm")
	public ModelAndView listArrivalm(ModelMap modelMap, HttpSession session, Arrivalm arrivalmMis,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == arrivalmMis.getBdate()){
			arrivalmMis.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			arrivalmMis.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			String account = session.getAttribute("accountId").toString();
			Deliver deliver = deliverPlatService.findDeliverByAccount(account);
			if(null != deliver)
				arrivalmMis.setPk_supplier(deliver.getCode());
		}
		String url = DeliverPlatConstants.LIST_ARRIVALM;
		if(arrivalmMis.getSelectType() == 0){//普通查询
			List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalmMis,page);
			modelMap.put("arrivalmList", amList);
		}else{//按物资汇总
			url = DeliverPlatConstants.LIST_ARRIVALM_SUM;
			//1.先查当前条件下供应的门店
			String pk_supplier = arrivalmMis.getPk_supplier();
			arrivalmMis.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
			String pk_org = arrivalmMis.getPk_org();
			arrivalmMis.setPk_org(CodeHelper.replaceCode(pk_org));
			List<Positn> listPositn = deliverPlatService.findPositnByArrivalm(arrivalmMis);
			List<Map<String,Object>> listMap = deliverPlatService.findArrivaldListSum(arrivalmMis,listPositn,page);
			arrivalmMis.setPk_supplier(pk_supplier);
			arrivalmMis.setPk_org(pk_org);
			modelMap.put("positnList", listPositn);
			modelMap.put("arrivaldList", listMap);
		}
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("pageobj", page);
		return new ModelAndView(url,modelMap);
	}
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param arrivalmMis
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findArrivald")
	public ModelAndView findArrivald(ModelMap modelMap, HttpSession session, Arrivalm arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		arrivalmMis.setIstate(0);//未验货
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalmMis);
		List<Arrivald> adList = new ArrayList<Arrivald>();
		if(amList.size() != 0){
			arrivalmMis = amList.get(0);
			adList = deliverPlatService.findArrivaldList(arrivalmMis);
			if(null == arrivalmMis.getMaded())//null说明没验货的,入库日期默认等于到货日期
				arrivalmMis.setMaded(DateFormat.getDateByString(arrivalmMis.getDarrbilldate(), "yyyy-MM-dd"));
		}
		modelMap.put("arrivalm", arrivalmMis);
		modelMap.put("arrivaldList", adList);
		return new ModelAndView(DeliverPlatConstants.LIST_ARRIVALD,modelMap);
	}
	
	/***
	 * 九毛九直配验货修改数量
	 * @param modelMap
	 * @param session
	 * @param disList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateArrivald")
	@ResponseBody
	public String updateArrivalm(ModelMap modelMap, HttpSession session,Arrivalm arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"供应商确认九毛九直配到货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		return deliverPlatService.updateArrivalm(arrivalmMis);
	}
	
	/***
	 * 九毛九直配验货修改数量判断
	 * @param modelMap
	 * @param session
	 * @param disList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkState")
	@ResponseBody
	public String checkState(ModelMap modelMap, HttpSession session,Arrivalm arrivalmMis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalmMis);
		if(amList.size() != 0){
			arrivalmMis = amList.get(0);
		}
		return arrivalmMis.getIstate()+"";
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportArrivalm")
	@ResponseBody
	public void exportArrivalm(HttpServletResponse response,HttpServletRequest request,HttpSession session,Arrivalm arrivalmMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "报货汇总报表";
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		//1.先查当前条件下供应的门店
		String pk_supplier = arrivalmMis.getPk_supplier();
		arrivalmMis.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
		String pk_org = arrivalmMis.getPk_org();
		arrivalmMis.setPk_org(CodeHelper.replaceCode(pk_org));
		List<Positn> listPositn = deliverPlatService.findPositnByArrivalm(arrivalmMis);
		List<Map<String,Object>> listMap = deliverPlatService.findArrivaldListSum(arrivalmMis,listPositn,null);
		//exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		deliverPlatService.exportArrivalm(response.getOutputStream(),arrivalmMis, listPositn, listMap);
	}
	
	
	/*************************WAP 调用后台 start******************************/
	/**
	 * 供应商对应分店查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addFirmDeliverWAP")
	@ResponseBody
	public Object addFirmDeliverWAP(String account, Page page, String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//获取当前账号所属供应商
		Deliver deliver = deliverPlatService.findDeliverByAccount(account);
		if(null != deliver){
			//根据供应商账号查询分店供应商里所有配送门店
			map.put("listTitle", firmDeliverService.findPositnByDeliver(deliver.getCode(), page));
		}else{
			map.put("listTitle", null);
		}		
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 报货查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getChkstodWAP")
	@ResponseBody
	public Object getChkstodWAP(Dis dis, String jsonpcallback, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));
		map.put("disList", deliverPlatService.findChkstom(dis, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 确认送货
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/confirmShWAP")
	@ResponseBody
	public Object checkWAP(String ids, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		deliverPlatService.confirmChkstom(ids);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pr", 1);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/***
	 * 确认所有送货
	 * @param ids
	 * @return
	 */
	@RequestMapping("/confirmShAllWAP")
	@ResponseBody
	public String confirmShAllWAP(Dis dis, String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));
		deliverService.confirmShAll(dis);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("pr", 1);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 报货汇总查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getChkstodSumWAP")
	@ResponseBody
	public Object getChkstodSumWAP(Dis dis, String jsonpcallback, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));
		map.put("disList", deliverPlatService.findChkstomSum(dis, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 验货查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getInspectionWAP")
	@ResponseBody
	public Object getInspectionWAP(Dis dis, String jsonpcallback, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("disList", deliverPlatService.findInspectionBZB(dis, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 对账单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/listChkinmByDeliverWAP")
	@ResponseBody
	public Object listChkinmByDeliverWap(String account, Chkinm chkinm, Page page, Date madedEnd, String jsonpcallback)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//获取当前账号所属供应商
		Deliver deliver = deliverPlatService.findDeliverByAccount(account);
		if(null != deliver){
			chkinm.setDeliver(deliver);
		}
        if (null == chkinm.getMaded() || null == madedEnd) {
            chkinm.setMaded(new Date());
            madedEnd = new Date();
        } 
        map.put("listChkinm", chkinmService.findAllChkinmNew("check", chkinm, page, madedEnd,"zh_CN"));
        map.put("totalChkinm", chkinmService.findAllChkinmTotal("check", chkinm, madedEnd));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 对账明细
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getChkindWAP")
	@ResponseBody
	public Object getChkindWAP(Chkind chkind, String jsonpcallback, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("chkindList", chkinmService.findAllChkind(chkind, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	/*************************WAP 调用后台 end******************************/
}
