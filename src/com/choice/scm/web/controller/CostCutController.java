package com.choice.scm.web.controller;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostCutConstants;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.CostCut;
import com.choice.scm.domain.CostItem;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.CostCutService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;

/**
* 菜品理论成本核减，加工间理论成本核减
* @author -  css
*/

@Controller
@RequestMapping("costcut")
public class CostCutController {
	@Autowired
	private CostCutService costcutService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private MainInfoService mainInfoService;
		
/********************************************************菜品理论成本核减start*************************************************/

	/***
	 * 核减左侧界面
	 * @param modelMap
	 * @param session
	 * @param mis
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap, HttpSession session, String mis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//获取所有区域 wangjie 2014年12月24日 12:55:29
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		List<CodeDes> areaList = codeDesService.findCodeDes(codeDes);
		modelMap.put("areaList", areaList);
		
		if ("1".equals(mis)) {
			List<Positn> positnList = new ArrayList<Positn>();
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				Positn positn = accountPositn.getPositn();
				positnList.add(positn);
			}
			modelMap.put("positnList", positnList);
		}else if ("2".equals(mis)) {// mis  多档口核减 功能      
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				String positnCode = accountPositn.getPositn().getCode();
				Positn positn = new Positn();
				positn.setUcode(positnCode);
				modelMap.put("positnList", positnService.findDeptByPositn(positn));
			}
		}else{//总部核减
			modelMap.put("listPositn", positnService.findPositnBySupply1(new Positn()));//查询所有分店
		}
		CostItem costItem = new CostItem();
		costItem.setDat(new Date());
		modelMap.put("mis", mis);
		modelMap.put("queryChkout", costItem);
		if ("1".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_LIST_COSTCUT,modelMap); //   
		}else if ("2".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_LIST_COSTCUT2,modelMap);//多档口核减 页面
		}else {
			return new ModelAndView(CostCutConstants.LIST_COSTCUT,modelMap); // 
		}
		
	}
		
	/***
	 * 核减右侧界面
	 * @param modelMap
	 * @param costCut
	 * @param action
	 * @param firmid
	 * @param mis
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView table(ModelMap modelMap,CostCut costCut, String action,String firmid, String mis, HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<CostCut> costcutList = new ArrayList<CostCut>();
		if(null!=action && "query".equals(action)){
			//加入查询 
			costCut.setFirmid(CodeHelper.replaceCode(costCut.getFirmid()));
			if ("2".equals(mis)) {
				// 获取分店code
				String accountId=session.getAttribute("accountId").toString();
				AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null!=accountPositn.getPositn()){
					String firmId = accountPositn.getPositn().getCode();
					costCut.setFirmid(firmId);
				}
			}
			//这里查询核减表就可以，没必要查销售表 20160421wjf
			costCut.setAcct(session.getAttribute("ChoiceAcct").toString());
			costcutList = costcutService.findCostItemByCostCut(costCut,page);
		}
		modelMap.put("costcutList",costcutList);
		modelMap.put("pageobj",page);
		
		if ("1".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT,modelMap);
		}else if ("2".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT2,modelMap);
		}else {
			return new ModelAndView(CostCutConstants.TABLE_COSTCUT,modelMap);
		}
	}
	
	/**
	 * 查询已核减
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search")
	public ModelAndView search(ModelMap modelMap) throws Exception{
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("pageobj", new Page());
		return new ModelAndView(CostCutConstants.LIST_SEARCH,modelMap);
		
	}
	
	/**
	 * 分店查询已核减
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/search1")
	public ModelAndView search1(ModelMap modelMap) throws Exception{
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("pageobj", new Page());
		return new ModelAndView(CostCutConstants.MIS_LIST_SEARCH,modelMap);
	}
	
	/**
	 * 查询已核减信息
	 * @param modelMap
	 * @param page
	 * @param startdate
	 * @param enddate
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/query")
	public ModelAndView query(ModelMap modelMap, Page page, String startdate, String enddate, String mis, HttpSession session) throws Exception{
		
		CostItem costItem = new CostItem();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (mis!=null) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				String firmId = accountPositn.getPositn().getCode();
				costItem.setFirm(firmId);
			}
		}
		Date bdat = DateFormat.getDateByString(startdate, "yyyy-MM-dd");
		Date edat = DateFormat.getDateByString(enddate, "yyyy-MM-dd");
		List<CostItem> chkoutList = new ArrayList<CostItem>();
		costItem.setDat(bdat);
		chkoutList.addAll(costcutService.queryByDate(costItem));
		for(;bdat.getTime() != edat.getTime();) {
			bdat  =  new Date(bdat.getTime()+24*3600*1000);
	        costItem.setDat(bdat);
			chkoutList.addAll(costcutService.queryByDate(costItem));
		}
		modelMap.put("chkoutList", chkoutList);
		modelMap.put("bdat", DateFormat.getDateByString(startdate, "yyyy-MM-dd"));
		modelMap.put("edat", edat);
		if ("1".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_LIST_SEARCH,modelMap);
		}else {
			return new ModelAndView(CostCutConstants.LIST_SEARCH,modelMap);
		}
	}
	
	/**
	 * 根据缩写码查询分店
	 * @param modelMap
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findByKeyAjax",method=RequestMethod.POST)
	@ResponseBody
	public List<Positn> findByKey(ModelMap modelMap, String key) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return costcutService.findByKey(key.toUpperCase());
	}
	
	/***
	 * 核减理论成本数据
	 * @param modelMap
	 * @param costCut
	 * @param firmid
	 * @param mis
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/excutecostcut")
	public ModelAndView excutecostcut(ModelMap modelMap,CostCut costCut, String firmid, String mis, HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//修改核减不成功问题 wjf
		List<String> codeList = Arrays.asList(firmid.split(","));
		costCut.setCodeList(codeList);
		costCut.setFirmid(CodeHelper.replaceCode(costCut.getFirmid()));
		if ("2".equals(mis)) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				String firmId = accountPositn.getPositn().getCode();
				costCut.setFirmid(firmId);
			}
		}
		String acct= session.getAttribute("ChoiceAcct").toString();
		costCut.setAcct(acct);
		
		HashMap<String, Object> returnMap = costcutService.chkOrNot(costCut);//先查询物流库  是否已经核减
		if (null == returnMap) {//门店没有核减
			modelMap.put("status", "ok");	
			List<CostCut> costcutList = new ArrayList<CostCut>();
			String tele_boh = "choice7";
			if("tele".equals(tele_boh)){   //中餐      老中餐的没改20160421 wjf
				DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
				costcutList = costcutService.getCostcut(costCut, mis);//获取需要核减的决策数据
				if(costcutList.size() == 0){//没有需要核减的数据 wjf
					modelMap.put("status","nodata");
				}else{
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					costcutService.excutecostcut(costcutList, costCut);//执行核减，往costitem插入数据，并调用存储过程COSTITEM_SPCODE进行核减
				}
			} else {
				boolean b = false;//判断是否有要核减的数据
				for(String code : codeList){
					Positn positn = new Positn();
					positn.setCode(code);
					positn.setAcct(acct);
					Positn p = positnService.findPositnByCode(positn);
					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
					//根据门店编码查询门店的类型
					String vfoodsign = costcutService.findVfoodSignByVcode(positn);
					List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
					if(null != vfoodsign && "2".equals(vfoodsign)){ // add by wangkai 2016-1-27快餐的话
						listMap = costcutService.findDatSubtract(code,costCut.getBdate(),costCut.getEdate(),p.getYnUseDept());
					}else{
						listMap = costcutService.findDatSubtract_cn(code,costCut.getBdate(),costCut.getEdate(),p.getYnUseDept());
					}
					if(listMap.size() == 0){
						continue;
					}
					b = true;
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					costcutService.Subtract(costCut.getBdate(),costCut.getEdate(),p,listMap);
				}
				if(!b)
					modelMap.put("status","nodata");
			}
		}else {
			modelMap.put("firm", returnMap.get("firm"));
			modelMap.put("date", returnMap.get("date"));
			modelMap.put("status", "no");
		}
		//这里查询核减表就可以，没必要查销售表 20160421wjf
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("costcutList",costcutService.findCostItemByCostCut(costCut,page));
		modelMap.put("pageobj",page);
		if ("1".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT,modelMap); // mis  非多档口
		}else if ("2".equals(mis)) {// mis  多档口核减 功能      
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT2,modelMap);
		}else{ // mis  总部核减 功能      
			return new ModelAndView(CostCutConstants.TABLE_COSTCUT,modelMap);
		}
	}
	/***
	 * 反核减理论成本数据
	 * @param modelMap
	 * @param costCut
	 * @param firmid
	 * @param mis
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/excutecostcutAnti")
	public ModelAndView excutecostcutAnti(ModelMap modelMap,CostCut costCut, String firmid, String mis, HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//修改核减不成功问题 wjf
		List<String> codeList = Arrays.asList(firmid.split(","));
		costCut.setCodeList(codeList);
		costCut.setFirmid(CodeHelper.replaceCode(costCut.getFirmid()));
		if ("2".equals(mis)) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				String firmId = accountPositn.getPositn().getCode();
				costCut.setFirmid(firmId);
			}
		}
		String acct= session.getAttribute("ChoiceAcct").toString();
		costCut.setAcct(acct);
		for(String code : codeList){
			Positn positn = new Positn();
			positn.setAcct(acct);
			positn.setCode(code);
			costcutService.antiReduction(costCut.getBdate(),costCut.getEdate(), positn);
		}
		modelMap.put("status", "uncheckOK");
		//这里查询核减表就可以，没必要查销售表 20160421wjf
		modelMap.put("costcutList",costcutService.findCostItemByCostCut(costCut,page));
		modelMap.put("pageobj",page);
		if ("1".equals(mis)) {
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT,modelMap); // mis  非多档口
		}else if ("2".equals(mis)) {// mis  多档口核减 功能      
			return new ModelAndView(CostCutConstants.MIS_TABLE_COSTCUT2,modelMap);
		}else{ // mis  总部核减 功能      
			return new ModelAndView(CostCutConstants.TABLE_COSTCUT,modelMap);
		}
	}
	
	
/********************************************************菜品理论成本核减end*************************************************/
/********************************************************加工间理论成本核减start*************************************************/

	/**
	 * 查询分店
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/excostcut")
	public ModelAndView listExCostCut(ModelMap modelMap, HttpSession session, String action, String month, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (!"init".equals(action)) {
			//这里应该是会计月20170607wjf 所以获取当前会计月开始日期和结束日期
			int year = Integer.parseInt(mainInfoService.findYearrList().get(0));
			MainInfo mainInfo= mainInfoService.findMainInfo(year);
			String bdat = "getBdat"+month;
			Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
			Date bdate = (Date)getBdat.invoke(mainInfo);
			String edat = "getEdat"+month;
			Method getEdat = MainInfo.class.getDeclaredMethod(edat);
			Date edate = (Date)getEdat.invoke(mainInfo);
			Chkinm chkinm = new Chkinm();
			chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());
			chkinm.setMaded(bdate);
			chkinm.setMadedto(edate);
			chkinm.setYearr(year+"");
			chkinm.setMonth(Integer.parseInt(month));
			modelMap.put("chkinmList", costcutService.findChkinm(chkinm, page));
			modelMap.put("month", month);
		}else{
			Map<String,Object> map=acctService.findAcctMainInfo();
			modelMap.put("month", map.get("month"));
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(CostCutConstants.LIST_EXCOSTCUT,modelMap);
	}
	
	/**
	 * 加工间理论成本数据核减
	 * @param modelMap
	 * @param checkboxList
	 * @param startdate
	 * @param enddate
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/excuteexcostcut")
	@ResponseBody
	public int excuteexcostcut(HttpSession session, String month) throws Exception{
		int data = 0;
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//这里应该是会计月20170607wjf 所以获取当前会计月开始日期和结束日期
		int year = Integer.parseInt(mainInfoService.findYearrList().get(0));
		MainInfo mainInfo= mainInfoService.findMainInfo(year);
		String bdat = "getBdat"+month;
		Method getBdat = MainInfo.class.getDeclaredMethod(bdat);
		Date bdate = (Date)getBdat.invoke(mainInfo);
		String edat = "getEdat"+month;
		Method getEdat = MainInfo.class.getDeclaredMethod(edat);
		Date edate = (Date)getEdat.invoke(mainInfo);
		Chkinm chkinm = new Chkinm();
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkinm.setMaded(bdate);
		chkinm.setMadedto(edate);
		chkinm.setYearr(year+"");
		chkinm.setMonth(Integer.parseInt(month));
		data = costcutService.excuteExcostcut(chkinm);
		return data;
	}
	
/********************************************************加工间理论成本核减end*************************************************/

}
