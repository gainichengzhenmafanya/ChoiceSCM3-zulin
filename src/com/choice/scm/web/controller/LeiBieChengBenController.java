package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.LeiBieChengBenService;
import com.choice.scm.util.ExportExcel;

/**
 * 类别成本分析报表 json
 * 
 */
@Controller
@RequestMapping("leiBieChengBen")
public class LeiBieChengBenController {

	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String, Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private LeiBieChengBenService leiBieChengBenService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	AcctService acctService;

	/**************************************************** 分店类别成本分析报表 ***************************************************/
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findHeaders")
	@ResponseBody
	public Object findHeaders(HttpSession session) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> columns = new HashMap<String, Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_LBCBBB);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_LBCBBB));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_LBCBBB_FROZEN);
		return columns;
	}

	/**
	 * 跳转到类别成本分析报表页面
	 */
	@RequestMapping("/toLeiBieChengBen")
	public ModelAndView toLeiBieChengBen(ModelMap modelMap, HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_LBCBBB);
		return new ModelAndView(PrdPrcCMConstant.LIST_LBCBBB, modelMap);
	}

	/**
	 * 查询类别成本差异数据
	 * 
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findLeiBieChengBen")
	@ResponseBody
	public Object findLeiBieChengBen(ModelMap modelMap, HttpSession session, String page, String rows, SupplyAcct supplyAcct) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> content = new HashMap<String, Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("supplyAcct", supplyAcct);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 20 : Integer.parseInt(rows));
		return leiBieChengBenService.findLeiBieChengBen(content, pager);
	}

	/**
	 * 导出
	 * 
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportLeiBieChengBen")
	@ResponseBody
	public void exportLeiBieChengBen(HttpServletResponse response, String sort, String order, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String fileName = "类别成本报表";
		Map<String, Object> condition = new HashMap<String, Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_LBCBBB);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
			// IE
			fileName = URLEncoder.encode(fileName, "UTF-8");
		} else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
			// firefox
			fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		} else {
			// other
			fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		List<Map<String,Object>> list = leiBieChengBenService.findLeiBieChengBen(condition, pager).getRows();
		if(list==null||list.size()==0){
			return;
		}
		exportExcelMap.creatWorkBook(response.getOutputStream(), leiBieChengBenService.findLeiBieChengBen(condition, pager).getRows(), "类别成本报表", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_LBCBBB));
	}
}
