package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostbomConstants;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Pubitem;
import com.choice.scm.service.CostbomService;
import com.choice.scm.util.ReadReportUrl;

/**
* 菜品bom            菜品理论利润率
* @author -  css      lehui
*/ 

@Controller
@RequestMapping(value = "costbom")
public class CostbomController {

	@Autowired
	private CostbomService costbomService;
	
	private String tele_boh = "choice7";
	/**
	 * 初始跳转
	 * @param modelMap
	 * @param pubitem
	 * @param pubgrp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllCostbom(ModelMap modelMap, Pubitem pubitem)throws Exception {
		modelMap.put("pubitem", pubitem);
		if("tele".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem(pubitem));
		}else if("choice3".equals(tele_boh)||"choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp_boh());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm_boh());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem_boh(pubitem));
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp_pos());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm_pos());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem_pos(pubitem));
		}
		
		return new ModelAndView(CostbomConstants.LIST_COSTBOM_GZJJ, modelMap);
	}
	
	/**
	 * ajax查询  菜品bom
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllPubitemByAjax",method=RequestMethod.POST)
	@ResponseBody
	public List<Pubitem> findAllPubitemByAjax(ModelMap modelMap,Pubitem pubitem) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Costbom> listCostbom = new ArrayList<Costbom>();
		String pubitemIds="0";
		listCostbom = costbomService.findCostBomList();//查询所有已做成本卡的菜品数据
		if (listCostbom != null) {
			for (int i = 0; i < listCostbom.size(); i++) {
				if(i==0){
					pubitemIds+=listCostbom.get(i).getItem();
				}else{
					pubitemIds+=","+listCostbom.get(i).getItem();
				}
			}
		}
		pubitem.setPubitem(pubitemIds);
		if("tele".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			return costbomService.findAllPubitem(pubitem);
		}else if("choice3".equals(tele_boh)||"choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			pubitem.setPubitem(CodeHelper.replaceCode(pubitemIds));
			return costbomService.findAllPubitem_boh(pubitem);
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			return costbomService.findAllPubitem_pos(pubitem);
		}else{
			return null;
		}
	}
	/**
	 * 主页面
	 * @param modelMap
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView costbomTable(ModelMap modelMap,Costbom costbom)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (costbom != null && costbom.getUnit_sprice() != null) {
			costbom.setUnit_sprice(java.net.URLDecoder.decode(costbom.getUnit_sprice(),"UTF-8"));
		} 
		if (costbom != null && costbom.getItcode() != null) {
			costbom.setItcode(java.net.URLDecoder.decode(costbom.getItcode(),"UTF-8"));
		} 
		modelMap.put("costbom", costbom);
		modelMap.put("modList", costbomService.findMod());//查询菜谱方案 
		modelMap.put("costbomList", costbomService.findAllCostbombomByLeftId(costbom));
		return new ModelAndView(CostbomConstants.TABLE_COSTBOM_GZJJ, modelMap);
		
		
	}
	/**
	 * 增 删 改  方法
	 * @param modelMap
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdateOrDelCostbom")
	public ModelAndView saveOrUpdateOrDelCostbom(ModelMap modelMap,Costbom costbom,HttpSession session)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		costbom.setAcct(session.getAttribute("ChoiceAcct").toString());
		costbomService.saveOrUpdateOrDelCostbom(costbom);
		modelMap.put("costbom", costbom);
		modelMap.put("modList", costbomService.findMod());//查询菜谱方案 
		modelMap.put("costbomList", costbomService.findAllCostbombomByLeftId(costbom));
		return new ModelAndView(CostbomConstants.TABLE_COSTBOM_GZJJ, modelMap);
	}
	/**
	 * 增 删 改  方法     替换物料
	 * @param modelMap
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdateOrDelCostbom_n")
	public ModelAndView saveOrUpdateOrDelCostbom_n(ModelMap modelMap,Costbom costbom,HttpSession session)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		costbom.setAcct(session.getAttribute("ChoiceAcct").toString());
		costbomService.saveOrUpdateOrDelCostbom_n(costbom);
		modelMap.put("costbom", costbom);
		modelMap.put("modList", costbomService.findMod());//查询菜谱方案 
		modelMap.put("listDownCostbom", costbomService.getDownCostbom_n(costbom));
		return new ModelAndView(CostbomConstants.DOWN_COSTBOM_N, modelMap);
		
	}
	/**
	 * 增 删 改  方法(虚拟物料)
	 * @param modelMap
	 * @param costbom
	 * @param cntt4
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdateOrDelCostbom_x")
	public ModelAndView saveOrUpdateOrDelCostbom_x(ModelMap modelMap,Costbom costbom,HttpSession session,String cntt)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		costbom.setAcct(session.getAttribute("ChoiceAcct").toString());
		costbomService.saveOrUpdateOrDelCostbom_x(costbom,cntt);
		modelMap.put("costbom", costbom);
		modelMap.put("modList", costbomService.findMod());//查询菜谱方案 
		modelMap.put("costbomList", costbomService.findAllCostbombomByLeftId_x(costbom));
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 批量删除
	 * @param modelMap
	 * @param costbom
	 * @param cntt4
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteCostbom_x")
	public ModelAndView deleteCostbom_x(ModelMap modelMap,String ids,String item,HttpSession session)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> map = new HashMap<String,String>();
		map.put("acct", session.getAttribute("ChoiceAcct").toString());
		map.put("item", item);
		map.put("ids", ids);
		costbomService.deleteCostbom_x(map);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 批量删除
	 * @param modelMap
	 * @param costbom
	 * @param cntt4
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteCostbom")
	public ModelAndView deleteCostbom(ModelMap modelMap,String ids, String item, String mods, HttpSession session)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> map = new HashMap<String,String>();
		map.put("acct", session.getAttribute("ChoiceAcct").toString());
		map.put("item", item);
		map.put("mods", mods);
		map.put("ids", ids);
		costbomService.deleteCostbom(map);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 菜品理论利润率
	 */
	@RequestMapping(value = "/listCaiPingLiLunLiLunLv")
	public ModelAndView listCaiPingLiLunLiLunLv(ModelMap modelMap,Pubitem pubitem,Page page,String action,HttpSession session)throws Exception {
		modelMap.put("pubitem", pubitem);
		pubitem.setItcodes(pubitem.getItcode());
		DecimalFormat decimal = new DecimalFormat("#.##");
		if("tele".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem(new Pubitem()));//标准产品
		}else if("choice3".equals(tele_boh)||"choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp_boh());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm_boh());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem_boh(new Pubitem()));//标准产品
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			modelMap.put("pubgrpList", costbomService.findAllPubgrp_pos());//查询所有菜品类别
			modelMap.put("itemPrgmList", costbomService.findItemPrgm_pos());//查询菜谱方案 
			modelMap.put("pubitemList", costbomService.findAllPubitem_pos(new Pubitem()));
		}
		if(null!=action&&"init".equals(action)){
			modelMap.put("ListBean", "123");
		}else{
			ArrayList<Pubitem> listPubitem_END = new ArrayList<Pubitem>();
			List<Pubitem> listPubitem = new ArrayList<Pubitem>();
			if("tele".equals(tele_boh)){
				DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
				listPubitem = costbomService.findAllPubitem(pubitem, page);
			}else if("choice3".equals(tele_boh)||"choice7".equals(tele_boh)){
				DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
				listPubitem = costbomService.findAllPubitem_boh(pubitem, page);
			}else if("pos".equals(tele_boh)){
				DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
				listPubitem = costbomService.findAllPubitem_pos(pubitem, page);
			}
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			String acct=session.getAttribute("ChoiceAcct").toString();
			DecimalFormat formater = new DecimalFormat("#0.##");
			for (int i = 0; i <listPubitem.size(); i++) {//查询成本     加入利润  百分比
				
				Pubitem pub=listPubitem.get(i);
				pub.setAcct(acct);
				Pubitem pub_End=costbomService.findPubitemCostByItem(pub);
				if(null==pub_End){
					pub.setCost(0);
					pub.setLiRun(Double.parseDouble(decimal.format(pub.getPrice())));
					pub.setBaiFenBi(100);
				}else{
					pub.setCost(pub_End.getCost());
					if(0.0==pub.getPrice()){
						pub.setLiRun(Double.parseDouble(decimal.format(-pub_End.getCost())));
						pub.setBaiFenBi(100);
					}else{
						pub.setLiRun(Double.parseDouble(decimal.format(pub.getPrice()-pub_End.getCost())));
						pub.setBaiFenBi(Double.parseDouble(formater.format((pub.getLiRun()/pub.getPrice()*100))));
					}
				}
				listPubitem_END.add(pub);
			}
			modelMap.put("ListBean", JSONArray.fromObject(listPubitem_END).toString());
			
		}
		modelMap.put("pageobj", page);
		modelMap.put("pubitem", pubitem);
		return new ModelAndView(CostbomConstants.LIST_CAI_PING_LI_LUNLILUNLV, modelMap);
	}
	
	/**
	 * 菜品bom打印
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/print")
	public ModelAndView printCast(ModelMap modelMap, HttpSession session, String type, Costbom costbom, String itemNm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			
			String unit = costbom.getUnit_sprice().split(",")[0];
			costbom.setUnit_sprice(unit);
		} catch (Exception e) {
			// TODO: handle exception
		}
		List<Costbom> disList = costbomService.findAllCostbombomByLeftId(costbom);
		for (int i = 0; i < disList.size(); i++) {
			System.out.println(disList.get(i).getSp_code());
		}
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "菜品bom");  
        parameters.put("itemNm", itemNm);  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("item", costbom.getItem().toString());
	    map.put("mods", costbom.getMods());
	    map.put("itemNm", itemNm);
	    map.put("acct", session.getAttribute("ChoiceAcct").toString());
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/costbom/print.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CostbomConstants.REPORT,CostbomConstants.REPORT);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 查询虚拟物料下实际物料用量
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/downCostbom")
	public ModelAndView downCostbom(ModelMap modelMap, Costbom costbom, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		costbom.setUnit_sprice(java.net.URLDecoder.decode(costbom.getUnit_sprice(),"UTF-8"));
		List<Costbom> listDownCostbom=costbomService.getDownCostbom(costbom);
		modelMap.put("listDownCostbom", listDownCostbom);
		modelMap.put("costbom", costbom);
		modelMap.put("pageobj", page);
		return new ModelAndView(CostbomConstants.DOWN_COSTBOM,modelMap);
	}
	/**
	 * 查询替换物料子表
	 */
	@RequestMapping("/downCostbom_n")
	public ModelAndView downCostbom_n(ModelMap modelMap, Costbom costbom, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		costbom.setUnit_sprice(java.net.URLDecoder.decode(costbom.getUnit_sprice(),"UTF-8"));
		List<Costbom> listDownCostbom=costbomService.getDownCostbom_n(costbom);
		modelMap.put("listDownCostbom", listDownCostbom);
		modelMap.put("costbom", costbom);
		modelMap.put("pageobj", page);
		return new ModelAndView(CostbomConstants.DOWN_COSTBOM_N,modelMap);
	}
	/**
	 *菜品bom---Excel 
	 *@param lefttj 存放左侧条件
	 */
	@RequestMapping(value = "/export")
	@ResponseBody
	public boolean export(HttpServletResponse response, HttpServletRequest request, Page page,String lefttj) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "菜品项目成本卡明细表";
		Pubitem pubitem = new Pubitem();
		List<Costbom> listCostbom = new ArrayList<Costbom>();
		listCostbom = costbomService.findAllCostdtlmIdList();
		String pubitemIds="";
		for (int i = 0; i < listCostbom.size(); i++) {
			if(i==0){
				pubitemIds+=listCostbom.get(i).getItem();
			}else{
				pubitemIds+=","+listCostbom.get(i).getItem();
			}
		}
		pubitem.setPubitem(pubitemIds);
		pubitem.setPrdctcard("QR");
		List<Pubitem> itemList = new ArrayList<Pubitem>();
		if("tele".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			itemList = costbomService.findAllPubitem(pubitem);
		}else if("choice3".equals(tele_boh)||"choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			
			//update by js at 20160324 导出菜品BOM的时候 根据左侧条件查询
			if(lefttj!=null&&!"".equals(lefttj)){
				String[] alltj = lefttj.split(":");
				//菜谱方案
				String prgid = "";
				//类别
				String pgrp = "";
				//缩写
				String init = "";
				//编码
				String itcode = "";
				//全部 已做 未做
				String prdctcard = "";
				//分割字符串得到相应的条件
				//用try catch 出现异常可以继续向下执行
				try{
					prgid = alltj[0];
				}catch(Exception e){
					prgid = "";
				}
				try{
					pgrp = alltj[1];
				}catch(Exception e){
					pgrp = "";
				}
				try{
					init = alltj[2];
				}catch(Exception e){
					init = "";
				}
				try{
					itcode = alltj[3];
				}catch(Exception e){
					itcode = "";
				}
				try{
					prdctcard = alltj[4];
				}catch(Exception e){
					prdctcard = "";
				}
				//设置属性
				pubitem.setPrgid(prgid);
				pubitem.setPgrp(pgrp);
				pubitem.setInit(init);
				pubitem.setItcode(itcode);
				//全部包括已做成成品卡的和未做成成品卡的
				if("1".equals(prdctcard)){
					//设空
					pubitem.setPrdctcard("");
				}
				//已做成成卡的
				else if("2".equals(prdctcard)){
					pubitem.setPrdctcard("QR");
				}
				//未做成成品卡的
				else if("3".equals(prdctcard)){
					pubitem.setPrdctcard("WZ");
				}
			}
			
			itemList = costbomService.findAllPubitem_boh(pubitem);
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			itemList = costbomService.findAllPubitem_pos(pubitem);
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		return costbomService.exportExcel(response.getOutputStream(), itemList);
	}
	
	/**
	 * 复制成本卡------页面跳转
	 * @param modelMap
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toCopyCostbom")
	public ModelAndView toCopyCostbom(ModelMap modelMap,String item,String mods) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("item", item);
		modelMap.put("mods", mods);
		modelMap.put("codedesList", costbomService.selectCodedes());
		return new ModelAndView(CostbomConstants.COSTBOMTYP,modelMap);
	}
	
	/**
	 * 复制成本卡----虚拟物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/copyCostBom_x")
	public ModelAndView copyCostBom_x(String item,String mods,String ids,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		if (item != "") {
			costbomService.copyCostbom(item, mods, ids,acct);
		}
		if (item == "") {
			costbomService.copyCostbomByTyp(mods, ids, acct);
		}
		return new ModelAndView(CostbomConstants.DONE);
	}
	
	/**
	 * 复制成本卡----实际物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/copyCostBom")
	public ModelAndView copyCostBom(String item,String mods,String ids,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		if (item != "") {
			costbomService.copyCostBom(item, mods, ids,acct);
		}
		if (item == "") {
			costbomService.copyCostBomByTyp(mods, ids, acct);
		}
		return new ModelAndView(CostbomConstants.DONE);
	}
	
	/**
	 * 更新物资编码的单位
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/updateUnit")
	@ResponseBody
	public String updateUnit(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return costbomService.updateUnit(session.getAttribute("ChoiceAcct").toString());
	}
}
