package com.choice.scm.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkoutService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisChuKuService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "disChuKu")
/**
 * 验收出库
 * @author csb
 *
 */
public class DisChuKuController {

	@Autowired
	private DisChuKuService disChuKuService;
	@Autowired
	private ChkoutService chkoutService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private LogsMapper logsMapper;
	/*******************************************验收出库start***********************************************/
	/**
	 * 验收出库
	 */
	@RequestMapping(value = "/listCheckout.do")
	public ModelAndView findAllCheckout(ModelMap modelMap,HttpSession session,Page page,Dis dis,String action,String ind1,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"验收出库查询 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		if(null!=action && !"zfcx".equals(action)&& !"ckcx".equals(action)){
			dis.setInd(new Date());
//			modelMap.put("maded1", new Date());
		}else{
			if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"))){//报货分拨下查询是否需要按照账号物资权限筛选--验收出库
				dis.setAccountId(session.getAttribute("accountId").toString());
			}
				dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
				dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
				dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
				dis.setAccountId(session.getAttribute("accountId").toString());
			
			List<Dis> disList=disChuKuService.findAllDis(dis,ind1,page);
			if(null!=maded1){modelMap.put("maded1", maded1);}
			else{modelMap.put("maded1", new Date());}
			if(null!=dis.getInd()){}
			else{dis.setInd(new Date());}
			
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Dis d:disList){
				supplyAcct.setPositn(d.getPositnCode());
				supplyAcct.setSp_code(d.getSp_code());
				d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
			modelMap.put("disList", disList);
		}
		modelMap.put("action",action);
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("fuJiaXiangList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_FUJIAXIANG_RESON)));
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKOUT_NEW, modelMap);
	}
	/**
	 * 生成出库单
	 */
	@RequestMapping(value = "/saveYsckChkout.do")
	public ModelAndView saveYsckChkout(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CREATE,
				"生成出库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		String str=null;//接收方法执行后的状态信息
			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
		//单线程执行
		synchronized(this){
			List<Dis> disList=disChuKuService.findAllDis(dis,ind1,null);//如果page为空，则不分页  查询所有的2014.10.24wjf
			String accountName=session.getAttribute("accountName").toString();
			if(disList.size()!=0){
				try{
					str = chkoutService.saveYsckChkout(disList,acct,accountName,dis.getInout(),maded1!=null?maded1:new Date());
				}catch(Exception e){
					str = e.getMessage();
				}
			}else{
				str="0";
			}
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKOUT_NEW, modelMap);
	}
	/**
	 * 出库单打印
	 */
	@RequestMapping(value = "/viewYsckChkstom")
	public ModelAndView viewYsckChkstom(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"出库单打印 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("ind", DateFormat.getStringByDate(dis.getInd(),"yyyy-MM-dd"));
		param.put("inout", dis.getInout());
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		if("inout".equals(dis.getInout())){param.put("chkin", "Y");}
		if("out".equals(dis.getInout())){param.put("chkin", "N");}
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disChuKu/viewYsckChkstom.do");//传入回调路径
		//根据关键字查询
			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
		List<Dis> disList=disChuKuService.findAllDisForReport(dis,ind1);
		
		for(Dis di:disList){
//			di.setPricein(di.getPricesale());
			//调整备注和附加项
			String memo0 = null == di.getMemo()?"":di.getMemo();
	    	String memo1 = "";
	    	if(memo0.length()>2 && memo0.contains("##")){
	    		String[] testMemo0 = memo0.split("##");
	    		memo0 = testMemo0.length>0?testMemo0[0]:"";
	    		memo1 = testMemo0.length>1?testMemo0[1]:"";
	    	}
	    	di.setMemo(memo0);
	    	di.setMemo1(memo1);
		}
		
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("物资出库明细表");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_YSCK_URL,DisConstants.REPORT_YSCK_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}		
	/*******************************************验收出库end***********************************************/
	
	/**
	 * ajax调用验收出库编辑发货数量
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param dis
	 * @param ind1
	 * @param maded1
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateChkoutNum")
	@ResponseBody
	public String updateChkoutNum(ModelMap modelMap,HttpSession session,String listStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//解析json字符串
		JSONArray json = JSONArray.fromObject(listStr);
		//转为对象
		List<Dis> list = (List<Dis>) JSONArray.toCollection(json, Dis.class);
		return disChuKuService.updateChkoutNum(list);
	}
	
}