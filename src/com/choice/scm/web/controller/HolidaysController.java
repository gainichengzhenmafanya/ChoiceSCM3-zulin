package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.HolidayConstant;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.domain.Holiday;
import com.choice.scm.service.HolidaysService;
import com.choice.scm.util.ReadReportConstants;
import com.choice.scm.util.ReadReportUrl;

@RequestMapping("holidays")
@Controller
public class HolidaysController {

	@Autowired
	private HolidaysService holidayService;
	
	/**
	 * 加载页面、查询节假日信息
	 * @param modelMap
	 * @param session
	 * @param tableName
	 * @param page
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	@RequestMapping("/list")
	public ModelAndView listFirm(ModelMap modelMap,HttpSession session,String tableName,Page page,Holiday holiday) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(tableName == null || "".equals(tableName)){
			tableName="holiday";
		}
		if(!"n".equals(holiday.getLoad())){
			holiday.setYear(String.valueOf(DateFormat.getYear(new Date())));
		}
		modelMap.put("holidayList",holidayService.listHoliday(holiday,page));
		modelMap.put("pageobj", page);
		modelMap.put("holiday", holiday);;
		return new ModelAndView(HolidayConstant.LIST_HOLIDAY,modelMap);
	}
	
	/**
	 * 跳转到节假日添加页面
	 * @param modelMap
	 * @param session
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	@RequestMapping("/addHoliday")
	public ModelAndView addHoliday(ModelMap modelMap,HttpSession session,Holiday holiday) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return new ModelAndView(HolidayConstant.ADD_HOLIDAY,modelMap);
	}
	
	/**
	 * 检测是否已存在所选年度的周六日信息
	 * @param modelMap
	 * @param session
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/countHoliday")
	@ResponseBody
	public int countHoliday(ModelMap modelMap,HttpSession session,Holiday holiday,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return holidayService.listHoliday(holiday,page).size();
	}
	
	/**
	 * 保存添加
	 * @param modelMap
	 * @param session
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/saveByAddHoliday")
	@ResponseBody
	public String saveByAddHoliday(ModelMap modelMap,HttpSession session,Holiday holiday) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return holidayService.saveByAddHoliday(holiday);
	}
	
	/**
	 * 自动生成周六日信息
	 * @param modelMap
	 * @param session
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/createHolidayAuto")
	@ResponseBody
	public String createHolidayAuto(ModelMap modelMap,HttpSession session,Holiday holiday) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return holidayService.saveBycreateHolidayAuto(holiday);
	}
	
	/**
	 * 删除节假日定义
	 * @param modelMap
	 * @param session
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/deleteHoliday")
	public ModelAndView deleteHoliday(ModelMap modelMap, HttpSession session, String dat, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		holidayService.deleteHoliday(dat);
		modelMap.put("pageobj", page);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 报表导出excel
	 * @param response
	 * @param request
	 * @param session
	 * @param condition
	 * @param reportName
	 * @throws Exception
	 */
	@RequestMapping("/exportToExcel")
	@ResponseBody
	public void exportToExcel(HttpServletResponse response,HttpServletRequest request,HttpSession session,Holiday holiday) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName="节假日定义";
		Page pager = new Page();
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		holidayService.exportHolidayService(response.getOutputStream(), holidayService.listHoliday(holiday));
		//exportExcel.creatWorkBook(response.getOutputStream(),holidayService.listHolidayMap(holiday, pager),"节假日定义",dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_HOLIDAY));
	}
	
	/**
	 * 报表打印
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param condition
	 * @param type
	 * @param reportName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/printReport")
	public ModelAndView printReport(ModelMap modelMap,HttpServletRequest request,HttpSession session,Holiday holiday,String type,String reportName,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//设置分页，查询所有数据
		page.setPageSize(Integer.MAX_VALUE);
		//获取并执行查询方法，获取查询结果
		modelMap.put("List",holidayService.listHolidayMap(holiday, page));
	 	HashMap<String,Object>  parameters = new HashMap<String,Object>();
	    parameters.put("report_name", ReadReportConstants.getReportNameCN(ScmStringConstant.class, reportName));
	    Map<String,String> map = new HashMap<String,String>();
	    map.put("year", holiday.getYear());
	    map.put("reportName", reportName);
	    modelMap.put("actionMap", map);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("year",holiday.getYear());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", request.getServletPath());//传入回调路径
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ReadReportConstants.getIReportPrint(HolidayConstant.class,reportName));//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
