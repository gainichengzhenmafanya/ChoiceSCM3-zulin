package com.choice.scm.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.ChkoutService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisService;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "disBak")
/**
 * 报货分拨、报货验收
 * @author csb
 *
 */
public class DisController {

	@Autowired
	private DisService disService;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private ChkoutService chkoutService;
	@Autowired
	private CodeDesService codeDesService;
	/********************************************报货分拨start******************************************/	
	/**
	 * 报货分拨
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap,Page page,HttpSession session,Dis dis,String action,String ind1,String chectim1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		if(null!=action && "init".equals(action)){//页面初次加载时
			dis.setMaded(new Date());
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setInd(dis.getMaded());
				dis.setMaded(null);
			}
			modelMap.put("disesList1", disService.findAllDis(dis,page));
			if(null!=ind1 && !"".equals(ind1)){
				dis.setMaded(dis.getInd());
			}
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("pageobj", page);
		modelMap.put("accountName", accountName);
		modelMap.put("ind1", ind1);
		modelMap.put("chectim1", chectim1);
		modelMap.put("dis", dis);
		return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}
	/**
	 * 批量修改
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		disService.updateByIds(dis,accountName);
		return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}
	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap,HttpSession session,DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String accountName=session.getAttribute("accountName").toString();
		return disService.updateDis(disList);
//		modelMap.put("accountName", accountName);
//		return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}	
	/**
	 * 报货分拨汇总
	 */
	@RequestMapping(value = "/findTotal")
	public ModelAndView findTotal(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("disList", disService.findTotal(dis,page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.CAIGOU_TOTAL_DISTRIBUTION, modelMap);
	}	
	/**
	 * 打印分拨单
	 */
	@RequestMapping(value = "/printIndent")
	public ModelAndView printIndent(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询条件
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("indent");
		param.put("danjuTyp", "indent");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/printIndent.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disService.findAllDisForReport(dis);	
		//记录合计数
		Map<String,Object> countMap=new HashMap<String,Object>();
		//所有的数据
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		String deliverDes2="";
		String sp_name2="";
		//计算合计
		double count=0;
		//------------------------------------------------ 
	    int tdnum1=1;//标记列1
	    int tdnum2=1;//标记列2
	    int tdnum3=1;//标记列3
	    //存放数据，往报表页面传
	    Map<String,Object> resultMap=new HashMap<String,Object>();
	    //报表文件同种物资类似行列转换用到的方法
	    for(int i=0;i<disList.size();i++){
	    	if(deliverDes2.equals(disList.get(i).getDeliverDes())){
			     if(sp_name2.equals(disList.get(i).getSp_name())){
					  if(tdnum1==4){
						   tdnum1=1;
						   tdnum2=1;
						   tdnum3=1;
						   list.add(resultMap);
						   resultMap=new HashMap<String,Object>();
						   resultMap.put("STR", "");
						   resultMap.put("SP_NAME",disList.get(i).getSp_name());
						   resultMap.put("DELIVER", disList.get(i).getDeliverDes());
						   resultMap.put("UNIT",disList.get(i).getUnit());
						   resultMap.put("CNT",disList.get(i).getCnt());
					  }
				      count+=disList.get(i).getAmount();
				      countMap.put(disList.get(i).getSp_name(),count);
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				      resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
			     }else{
				      if(resultMap.size()!=0){
				    	  list.add(resultMap);
				      }
				      tdnum1 = 1;
				      tdnum2 = 1;
				      tdnum3 = 1;
				      count = 0;
				      resultMap=new HashMap<String,Object>();
				      resultMap.put("STR", "STR");
				      resultMap.put("SP_NAME",disList.get(i).getSp_name());
				      resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				      resultMap.put("UNIT",disList.get(i).getUnit());
				      resultMap.put("CNT",disList.get(i).getCnt());
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				      resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
				      count=disList.get(i).getAmount();
				      countMap.put(disList.get(i).getSp_name(),count);
				      sp_name2=disList.get(i).getSp_name();
			     }
	    	}else{
	    		if(resultMap.size()!=0){
	    			list.add(resultMap);
			    }
				tdnum1 = 1;
				tdnum2 = 1;
				tdnum3 = 1;
				count = 0;
				resultMap=new HashMap<String,Object>();
				resultMap.put("STR", "STR");
				resultMap.put("SP_NAME",disList.get(i).getSp_name());
				resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				resultMap.put("UNIT",disList.get(i).getUnit());
				resultMap.put("CNT",disList.get(i).getCnt());
				resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
				count=disList.get(i).getAmount();
				countMap.put(disList.get(i).getSp_name(),count);
				sp_name2=disList.get(i).getSp_name();
				deliverDes2=disList.get(i).getDeliverDes();
		     }
	    }
	    list.add(resultMap);//将最后一行加上
		//计算完合计，存放进map
		for (int i = 0; i < list.size(); i++) {
			Map<String,Object> map=list.get(i);
			map.put("TOTALAMT", countMap.get(map.get("SP_NAME")));
		}
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("分拨单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        modelMap.put("List", list);
	    modelMap.put("parameters", parameters);
	    Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INDENT_URL,DisConstants.REPORT_INDENT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
        //return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印验货单
	 */
	@RequestMapping(value = "/printInspection")
	public ModelAndView printInspection(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户前台输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("inspection");
		param.put("danjuTyp", "inspection");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/printInspection.do");//传入回调路径
		//根据关键字进行查询
		List<Dis> disList=disService.findAllDisForReport(dis);	
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("验货单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表用输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INSPECTION_URL,DisConstants.REPORT_INSPECTION_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印配送单
	 */
	@RequestMapping(value = "/printDelivery")
	public ModelAndView printDelivery(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("delivery");
		param.put("danjuTyp", "delivery");
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/printDelivery.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disService.findAllDisForReport(dis);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("配送清单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_DELIVERY_URL,DisConstants.REPORT_DELIVERY_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 汇总打印
	 */
	@RequestMapping(value = "/printDisTotal")
	public ModelAndView printDisTotal(ModelMap modelMap,HttpSession session,Page page,Dis dis,Date maded,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/printDelivery.do");//传入回调路径		
		List<Dis> disList=disService.findTotal(dis,page);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("申购汇总");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(maded,"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
	    modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/dis/printDisTotal.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_TOTAL_URL,DisConstants.REPORT_TOTAL_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出
	 */
	@RequestMapping(value = "/exportExcel")
	public ModelAndView exportExcel(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/exportExcel.do");//传入回调路径		
		List<Dis> disList=disService.findAllDisForReport(dis);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("申购明细");
        parameters.put("report_name", report_name); 
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
	    modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/dis/printDisTotal.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl("excel",DisConstants.REPORT_EXCEL_URL,DisConstants.REPORT_EXCEL_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/********************************************报货分拨end******************************************/	
	/********************************************报货验收start******************************************/	
	@RequestMapping(value = "/listCheck")
	public ModelAndView listCheck(ModelMap modelMap,Page page,HttpSession session,Dis dis,String action,String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		if(null!=action && "init".equals(action)){//页面初次加载时
			dis.setBdat(new Date());
			dis.setEdat(new Date());
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			dis.setChkin("N");
			dis.setChkout("N");
			dis.setChk1("Y");
			dis.setSta("Y");
			modelMap.put("disesList1", disService.findAllDis(dis,page));
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("pageobj", page);
		modelMap.put("accountName", accountName);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		return new ModelAndView(DisConstants.LIST_DIS_CHECK, modelMap);
	}
	/**
	 * 打印验收单
	 */
	@RequestMapping(value = "/printReceipt")
	public ModelAndView printReceipt(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("receipt");
		param.put("danjuTyp", "receipt");
		dis.setInout("in,inout");;
		dis.setChkin("N");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		param.put("chkin", "N");
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
		param.put("inout", "in,inout");
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/printReceipt.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disService.findAllDisForReport(dis);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("验收单");
        parameters.put("report_name", report_name);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_RECEIPT_URL,DisConstants.REPORT_RECEIPT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}
	/********************************************报货验收end******************************************/
	/*******************************************验收入库start***********************************************/
	/**
	 * 验收入库主界面
	 */
	@RequestMapping(value = "/listCheckin.do")
	public ModelAndView findAllCheckin(ModelMap modelMap,HttpSession session,Page page,Dis dis,String action,String ind1,Date maded1,String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);    //得到前一天
		Date date = calendar.getTime();
		if(null!=action && !"".equals(action)){
			dis.setBdat(date);
			dis.setEdat(date);
			modelMap.put("maded1", new Date());
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			dis.setChkin("N");
			dis.setChkout("N");
			dis.setChk1("Y");
			dis.setSta("Y");
			dis.setInout("in,inout");
			dis.setCheckinSelect("checkinSelect");
			List<Dis> disList=disService.findAllDis(dis,page);
			if(null!=maded1){modelMap.put("maded1", maded1);}
			else{modelMap.put("maded1", new Date());}
			modelMap.put("disList", disList);
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKIN,modelMap);
	}
	/**
	 * 生成入库单
	 */
	@RequestMapping(value = "/saveYsrkChkinm.do")
	public ModelAndView saveYsrkChkinm(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,Date maded1,String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);    //得到前一天
		if(null!=ind1 && !"".equals(ind1)){
			dis.setChksend("Y");
		}else{
			dis.setChksend("N");
		}
		dis.setChkin("N");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		dis.setInout("in,inout");
		dis.setCheckinSelect("checkinSelect");
		List<Dis> disList=disService.findAllDis(dis,page);
		//报货单转入库单方法
		String  acct=session.getAttribute("ChoiceAcct").toString();//当前帐套
		String  accountName=session.getAttribute("accountName").toString();//当前用户
		if("ysrk".equals(ysrkStr)){//验收入库的方法
			if(disList.size()!=0){
				chkinmService.saveYsrkChkinm(disList,acct,dis.getPositnCode(),maded1!=null?maded1:new Date(),accountName);
				str=1;//不为空，成功
			}else{
				str=2;//为空，不能进行入库
			}
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKIN,modelMap);
	}
	
	/**
	 * 入库单打印
	 */
	@RequestMapping(value = "/viewYsrkChkstom")
	public ModelAndView viewYsrkChkstom(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("memo1", dis.getMemo1());
		param.put("chkin", "N");
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
		param.put("inout", "in,inout");
		param.put("psarea", dis.getPsarea());
		param.put("checkinSelect", dis.getCheckinSelect());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		dis.setChkin("N");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		dis.setInout("in,inout");
		dis.setCheckinSelect("checkinSelect");
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/viewYsrkChkstom.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disService.findAllDisForReport(dis);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("物资入库明细表");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_YSRK_URL,DisConstants.REPORT_YSRK_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}	
	/*******************************************验收入库end****************************************************/
	/*******************************************验收出库start***********************************************/
	/**
	 * 验收出库
	 */
	@RequestMapping(value = "/listCheckout.do")
	public ModelAndView findAllCheckout(ModelMap modelMap,HttpSession session,Page page,Dis dis,String action,String ind1,String ysckStr,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=action && !"".equals(action)){
			dis.setInd(new Date());
			modelMap.put("maded1", new Date());
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			dis.setChkin("Y");
			dis.setChkout("N");
			dis.setChk1("Y");
			dis.setSta("Y");
			List<Dis> disList=disService.findAllDis(dis,page);
			if(null!=maded1){modelMap.put("maded1", maded1);}
			else{modelMap.put("maded1", new Date());}
			if(null!=dis.getInd()){}
			else{dis.setInd(new Date());}
			modelMap.put("disList", disList);
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKOUT,modelMap);
	}
	/**
	 * 生成出库单
	 */
	@RequestMapping(value = "/saveYsckChkout.do")
	public ModelAndView saveYsckChkout(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,String ysckStr,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String str=null;//接收方法执行后的状态信息
		if(null!=ind1 && !"".equals(ind1)){
			dis.setChksend("Y");
		}else{
			dis.setChksend("N");
		}
		dis.setChkin("Y");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		List<Dis> disList=disService.findAllDis(dis,page);
		String accountName=session.getAttribute("accountName").toString();
		String  acct=session.getAttribute("ChoiceAcct").toString();
		if("zfck".equals(ysckStr)){//直发出库的方法
			if(disList.size()!=0){
					str=chkoutService.saveYsckChkout(disList,acct,accountName,dis.getInout(),maded1!=null?maded1:new Date());
			}else{
				str="数据为空！不能进行出库！";
			}
		}
		if("kcck".equals(ysckStr)){//库存出库的方法
			if(disList.size()!=0){
					str=chkoutService.saveYsckChkout(disList,acct,accountName,dis.getInout(),maded1!=null?maded1:new Date());
			}else{
				str="数据为空！不能进行出库！";
			}
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKOUT,modelMap);
	}
	/**
	 * 出库单打印
	 */
	@RequestMapping(value = "/viewYsckChkstom")
	public ModelAndView viewYsckChkstom(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("ind", DateFormat.getStringByDate(dis.getInd(),"yyyy-MM-dd"));
		param.put("inout", dis.getInout());
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("chkin", "Y");
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		dis.setChkin("Y");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/dis/viewYsckChkstom.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disService.findAllDisForReport(dis);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("物资出库明细表");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_YSCK_URL,DisConstants.REPORT_YSCK_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}		
	/*******************************************验收出库end***********************************************/
}