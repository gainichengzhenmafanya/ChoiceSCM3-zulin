package com.choice.scm.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SppriceDemoConstants;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.SDList;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.SppriceDemoService;
/**
 * 物资价格模板
 * @author yp
 *
 */
@Controller
@RequestMapping("sppriceDemo")
public class SppriceDemoController {

	@Autowired
	private SppriceDemoService sppriceDemoService;
	@Autowired
	private DeliverService deliverService;
	
	/**
	 * 查询价格模板信息
	 * @param modelMap
	 * @param demo
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView findSppriceDemo(ModelMap modelMap,SppriceDemo demo,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sppriceDemo",demo);
		modelMap.put("pageobj", page);
		modelMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		modelMap.put("demoList", sppriceDemoService.findSppriceDemo(demo,page));
		//modelMap.put("deliverList", deliverService.findAllDelivers(null));
		return new ModelAndView(SppriceDemoConstants.LIST_SPPRICEDEMO,modelMap);
	}
	@RequestMapping("/showList")
	public ModelAndView showSppriceDemo(ModelMap modelMap,SppriceDemo demo,HttpSession session,Page page){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceDemoConstants.MANAGE_SPPRICEDEMO,modelMap);
	}
	/**
	 * 转到添加页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public ModelAndView addSppriceDemo(ModelMap modelMap,SppriceDemo demo)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sppriceDemo", demo);
		Deliver deliver=new Deliver();
		deliver.setCode(demo.getDeliver());
		modelMap.put("deliver", deliver);
		modelMap.put("bdat",DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(),"day",1,1), "yyyy-MM-dd"));
		modelMap.put("edat",DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(),"day",1,8), "yyyy-MM-dd"));
		return new ModelAndView(SppriceDemoConstants.ADD_SPPRICEDEMO,modelMap);
	}
	
	/**
	 * 转到批量添加页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/addBatch")
	public ModelAndView addBatchDemo(ModelMap modelMap,Deliver deliver)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliver", deliverService.findAllDelivers(deliver).get(0));
		//modelMap.put("deliverList", deliverService.findAllDelivers(null));
		return new ModelAndView(SppriceDemoConstants.ADD_BATCHDEMO,modelMap);
	}

	/**
	 * 批量添加价格模板
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByBatch")
	public ModelAndView saveByBatch(ModelMap modelMap,SppriceDemo demo,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try{
			demo.setAcct(session.getAttribute("ChoiceAcct").toString());
			sppriceDemoService.saveBatchDemo(demo);
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.put("msg", "数据已存在，添加失败！");
			return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
		}
	}
	/**
	 * 添加价格模板信息
	 * @param modelMap
	 * @param demo
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,SppriceDemo demo,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try{
			demo.setAcct(session.getAttribute("ChoiceAcct").toString());
			demo.setCzdate((new SimpleDateFormat("yyyy-MM-dd  kk:mm:ss ")).format(new Date()));
			sppriceDemoService.saveSppriceDemo(demo);
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.put("msg", "数据已存在，添加失败！");
			return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
		}
	}
	/**
	 * 更新价格模板信息
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,SppriceDemo demo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceDemoService.updateSppriceDemo(demo);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 批量更新价格模板信息
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByUpdateBatch")
	public ModelAndView saveByUpdateBatch(ModelMap modelMap,SDList demos) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceDemoService.updateSppriceDemoBatch(demos);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	
	/**
	 * 删除物资编码信息
	 * @param modelMap
	 * @param spprice
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public ModelAndView deleteSppriceDemo(ModelMap modelMap,SDList demos) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			sppriceDemoService.deleteSppriceDemo(demos);
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		
		
	}
	
	/**
	 * 更新模板物资价格
	 * @param modelMap
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("updatePrice")
	public ModelAndView updatePrice(ModelMap modelMap,SppriceDemo demo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceDemoService.updatePrice(demo);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	
	@RequestMapping("/toUpdatePrice")
	public ModelAndView updatePrice(){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceDemoConstants.UPDATE_PRICE);
	}
	
	@RequestMapping("/toUpdateDate")
	public ModelAndView updateDate(){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceDemoConstants.UPDATE_DATE);
	}
	
}
