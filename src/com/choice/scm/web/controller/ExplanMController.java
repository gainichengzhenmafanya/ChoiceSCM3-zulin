package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.ExplanMConstants;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.domain.AcctLineFirm;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.ExplanD;
import com.choice.scm.domain.ExplanM;
import com.choice.scm.domain.ExplanSpcode;
import com.choice.scm.domain.LinesFirm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.LinesFirmMapper;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.ExplanMService;
import com.choice.scm.service.ExplanService;
import com.choice.scm.service.InventoryService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.PrdctbomService;
import com.choice.scm.service.SupplyDefaultDeliverService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 加工单
 * @author css
 *
 */
@Controller
@RequestMapping(value = "explanM")
public class ExplanMController {

	@Autowired
	private ExplanMService explanMService;
	@Autowired
	PositnService positnService;
	@Autowired
	ChkinmService chkinmService;
	@Autowired
	private PrdctbomService prdctbomService;
	@Autowired
	private ExplanService explanService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private LinesFirmMapper linesFirmMapper;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private SupplyDefaultDeliverService supplyDefaultDeliverService;
	
    /**
     * 查询加工单list
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllExplanM(ModelMap modelMap, ExplanM explanM, Date madedEnd, String startDate, Page page, String action)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=action && "init".equals(action)){
			if(null!=startDate && !"".equals(startDate)){
				explanM.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			}else{
				explanM.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			}
			modelMap.put("explanMList", explanMService.findAllExplan(explanM, page, DateFormat.formatDate(new Date(), "yyyy-MM-dd")));
			modelMap.put("madedEnd", new Date());
		}else{
			modelMap.put("explanMList", explanMService.findAllExplan(explanM, page, madedEnd));
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("explanM", explanM);
		modelMap.put("status", explanM.getStatus());
		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.LIST_EXPLANM, modelMap);
	}
	
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/add")
	public ModelAndView add(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			ExplanM explanM=new ExplanM();
			explanM.setMadeby(accountName);
			explanM.setMaded(new Date());
			explanM.setHoped(DateFormat.getDateBefore(new Date(), "day", 1, 1));
			explanM.setExplanno(explanMService.getMaxExplanno());
			modelMap.put("explanM",explanM);//日期等参数
			modelMap.put("curStatus", "add");//当前页面状态
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			modelMap.put("listPositn", positnService.findAllPositn(positn));
		}else{
			modelMap.put("tableFrom", tableFrom);//当前页面状态
		}
		return new ModelAndView(ExplanMConstants.UPDATE_EXPLAN,modelMap);	
	}
		
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(ModelMap modelMap, ExplanM explanM) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("explanM",explanMService.findExplanMByid(explanM));
		modelMap.put("curStatus", "show");//当前页面状态
		Positn positn=new Positn();
		positn.setTyp(PositnConstants.type_3);
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		return new ModelAndView(ExplanMConstants.UPDATE_EXPLAN,modelMap);
	}

	/**
	 * 修改加工单
	 * @param modelMap
	 * @param explanM
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateExplanM")
	@ResponseBody
	public int updateExplanM(ModelMap modelMap, HttpSession session, String explannoids, ExplanM explanM)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Integer  result=0;
		ExplanM explanMs = new ExplanM();
		if (explannoids!=null){
			List<String> ids = Arrays.asList(explannoids.split(","));
			if (ids.size()>0){//tableExplanM.jsp界面的加入加工按钮方法，只修改status字段为1
				for (int i=0;i<ids.size();i++) {
					explanMs.setExplanno(Integer.valueOf(ids.get(i)));
					explanMs.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
					explanMs.setStatus(explanM.getStatus());
					result=explanMService.updateExplanMM(explanMs, session.getAttribute("accountName").toString());
				}
			}
		}else{
			explanMs=explanMService.findExplanMByid(explanM);
			explanM.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
			if(null!=explanMs){//存在则修改
				if(explanM!=null && "0".equals(explanM.getStatus())){
					result=explanMService.updateExplanM(explanM);
				}else{
					result=explanMService.updateExplanMM(explanM, session.getAttribute("accountName").toString());
				}
			}else{//不存在则添加   
				result=explanMService.saveExplanM(explanM);
			}	
		}
		return result;	
	}
	
	/**
	 * 删除加工单
	 * @param modelMap
	 * @param explannoids
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String explannoids, Page page, ExplanM explanM,
			Date madedEnd, String action, String flag) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> ids = Arrays.asList(explannoids.split(","));
		explanMService.deleteExplanM(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else{
			modelMap.put("explanM", null);
			return new ModelAndView(ExplanMConstants.UPDATE_EXPLAN, modelMap);
		}
	}
		
	/****
	 * 安全库存 到加工间    
	 * @author wjf        
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param firmCode
	 * @param firmDes
	 * @param supplyAcct
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/safeStock")
	public ModelAndView safeCntToPlan(ModelMap modelMap, HttpSession session, Page page, String firmCode,
			String firmDes, SupplyAcct supplyAcct, String action) throws Exception{
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			modelMap.put("pageobj", page);
	//		modelMap.put("firmCode", firmCode);//貌似现在用不着了
	//		modelMap.put("firmDes", firmDes);
	//		modelMap.put("sp_code", supplyAcct.getSp_code());
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			List<Positn> listPositn = positnService.findAllPositn(positn);
			modelMap.put("listPositn", listPositn);
			if(null == supplyAcct.getPositn() || "".equals(supplyAcct.getPositn())){
				modelMap.put("ListBean", "123");//table体list
//				supplyAcct.setPositn(0 == listPositn.size()?null :listPositn.get(0).getCode());//默认查第一个加工间的
			}else{
				condition.put("supplyAcct", supplyAcct);
				condition.put("positnType", PositnConstants.type_5);
		//		condition.put("codes", firmCode);
				modelMap.put("ListBean", JSONArray.fromObject(explanMService.safeCntToPlan(condition, page)).toString());//table体list
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
 		return new ModelAndView(ExplanMConstants.SAFECNT_PLAN,modelMap);
 		
	}
	
	/**
	 * 新增调度  来自安全库存
	 */
	@RequestMapping(value = "saveExplanBySafeCnt")
	@ResponseBody
	public Object saveExplanBySafeCnt(ModelMap modelMap, String sp_code, ExplanM explan, Page page, Supply supply, HttpSession session)throws Exception {
		//帐套
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explan.setExplanno(explanMService.getMaxExplanno());
		explan.setMadeby(session.getAttribute("accountName").toString());
		explan.setTyp("2");
		return explanMService.saveExplanBySafeCnt(explan, acct);//来自计划  
	}
	
	/***
	 * 报货到加工单
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param firmCode
	 * @param firmDes
	 * @param supplyAcct
	 * @param action
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateExplanByPlan")
	public ModelAndView updateExplanByPlan(ModelMap modelMap, HttpSession session, Page page, String firmCode,
			String firmDes, SupplyAcct supplyAcct, String action) throws Exception{
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			//得到所有加工间
			Positn positn2=new Positn();
			positn2.setTyp(PositnConstants.type_3);
			List<Positn> listPositn = positnService.findAllPositn(positn2);
			modelMap.put("listPositn", listPositn);
			
			//得到所有分店
			condition.put("positnType", PositnConstants.type_5);
			condition.put("codes", CodeHelper.replaceCode(firmCode));
			modelMap.put("columns", explanService.findTableHead(condition));
	//		modelMap.put("sp_code", supplyAcct.getSp_code());
			if("init".equals(action)){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				String dates=sdf.format(new Date());
				supplyAcct.setBdat(sdf.parse(dates));
				supplyAcct.setEdat(sdf.parse(dates));	
				supplyAcct.setDeal("0");
				supplyAcct.setPositn(0 == listPositn.size()?null :listPositn.get(0).getCode());//默认查第一个加工间的
			}
			condition.put("positnex", supplyAcct.getPositn());//解决差不着加工间的问题，wjf
			condition.put("supplyAcct", supplyAcct);
			condition.put("codes", firmCode);
			modelMap.put("ListBean", JSONArray.fromObject(explanService.findPlanFirm_new(condition, page)).toString());//table体list
			modelMap.put("supplyAcct", supplyAcct);
			
			//根据第一个加工间获得所在配送片区
			String error = supplysNoSetExPositn(supplyAcct.getPositn(),session.getAttribute("ChoiceAcct").toString());
			modelMap.put("error", error);
			
			modelMap.put("pageobj", page);
			modelMap.put("firmCode", firmCode);
			modelMap.put("firmDes", firmDes);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return new ModelAndView(ExplanMConstants.PLAN_CHKSTOM,modelMap);
	}
	
	/**
	 * 得到配送片区未设置出品加工间的物资
	 * @param response
	 * @param psarea
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getSupplysNoSetExPositn")
	@ResponseBody
	public String getSupplysNoSetExPositn(HttpServletResponse response,String code, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return supplysNoSetExPositn(code,session.getAttribute("ChoiceAcct").toString());
	};
	
	/**
	 * 得到配送片区未设置配送片区默认
	 * @param psarea
	 * @return
	 * @throws Exception 
	 */
	public String supplysNoSetExPositn(String code,String acct) throws Exception{
		//根据仓位编码得到配送片区编码名称
		Positn positntj = new Positn();
		positntj.setCode(code);
		List<Positn> positns = positnService.findAllPositn(positntj);
		//返回信息
		String error = "";
		String psareacode = "";
		String psareades = "";
		if(positns!=null&&positns.size()>0&&positns.get(0)!=null){
			positntj = positns.get(0);
			psareacode = positntj.getPsareacode();
			psareades = positntj.getPsarea();
		}
		Supply supply = new Supply();
		supply.setAcct(acct);
		supply.setEx1("Y");
		Page page = new Page();
		List<Map<String,Object>> lists = supplyDefaultDeliverService.findSupplyDefaultPositn(supply, "", "", psareacode, "", "", page, "POSITNEX");
		int size = 0;
		for(int i=0,len=lists.size();i<len;i++){
			Map<String,Object> map = lists.get(i);
			if(map!=null){
				if(map.get("DES")==null){
					if(size>=5){
						break;
					}
					if(size==0){
						error = psareades+":";
					}
					error += (map.get("SP_CODE") + "," + map.get("SP_NAME"))+";";
					size++;
				}
			}
		}
		//去掉最后的分号
		if(error.endsWith(";")){
			error = error.substring(0,error.length()-1);
		}
		return error;
	}
	
	/***
	 * 报货到加工单2 竖直显示分店报货量
	 * @author jinshuai 20160420
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param firmCode
	 * @param firmDes
	 * @param supplyAcct
	 * @param action
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateExplanByPlan2")
	public ModelAndView updateExplanByPlan2(ModelMap modelMap, HttpSession session, Page page, String firmCode,
			String firmDes, SupplyAcct supplyAcct, String action, String line) throws Exception{
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			//得到所有加工间
			Positn positn2=new Positn();
			positn2.setTyp(PositnConstants.type_3);
			List<Positn> listPositn = positnService.findAllPositn(positn2);
			//前台展示
			modelMap.put("listPositn", listPositn);
			
			//得到所有分店信息
			condition.put("positnType", PositnConstants.type_5);
			List<Positn> positnList = explanService.findTableHead(condition);
			
			//配送线路
			LinesFirm lf = new LinesFirm();
			lf.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<LinesFirm> linesFirms = linesFirmMapper.findLinesFirm(lf);
			modelMap.put("linesFirms", linesFirms);
			//初始进入页面设置
			if("init".equals(action)){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				String dates=sdf.format(new Date());
				supplyAcct.setBdat(sdf.parse(dates));
				supplyAcct.setEdat(sdf.parse(dates));	
				supplyAcct.setDeal("0");
				supplyAcct.setPositn(0 == listPositn.size()?null :listPositn.get(0).getCode());//默认查第一个加工间的
				
				//默认查询第一条线路
				if(linesFirms==null||linesFirms.size()==0){
					line = null;
				}else{
					line = linesFirms.get(0).getCode();
				}
			}
			
			condition.put("positnex", supplyAcct.getPositn());//解决差不着加工间的问题，wjf
			condition.put("supplyAcct", supplyAcct);
			
			//保存选择的线路信息
			modelMap.put("line", line);
			
			String endFirm = "";
			//如果未设置配送线路
			if(line!=null){
				//根据选择的配送线路过滤分店
				AcctLineFirm alf = new AcctLineFirm();
				if(null==firmCode||"".equals(firmCode)){
					alf.setFirm(null);
				}else{
					Positn positn = new Positn();
					positn.setCode(firmCode);
					alf.setFirm(positn);
				}
				alf.setCode(line);
				alf.setAcct(session.getAttribute("ChoiceAcct").toString());
				//得到分店
				List<AcctLineFirm> alfs = linesFirmMapper.findFirmByLinesFirmId(alf);
				//拼接分店编码
				String firmCode2 = null;
				for(int i=0,len=alfs.size();i<len;i++){
					AcctLineFirm alf2 = alfs.get(i);
					firmCode2 += ","+alf2.getFirm().getCode();
				}
				
				endFirm = firmCode2;
			}
			else{
				endFirm = firmCode;
			}
			
			//放到查询条件中
			condition.put("codes", endFirm);
			List<Map<String,Object>> spmes = new ArrayList<Map<String,Object>>();
			//如果配送线路有对应的分店才查询数据
			if(endFirm!=null){
				//物资信息
				//spmes = explanService.findPlanFirm_new(condition, page);
				//不分页暂时
				spmes = explanService.findPlanFirm_new(condition, null);
			}
			modelMap.put("supplyAcct", supplyAcct);
			modelMap.put("pageobj", page);
			modelMap.put("firmCode", firmCode);
			modelMap.put("firmDes", firmDes);
			
			//把Positn集合转换为Map
			//用在下发通过key得到名称
			Map<String,String> positnMap = new HashMap<String,String>();
			for(int i=0,len=positnList.size();i<len;i++){
				Positn p = positnList.get(i);
				String code = p.getCode();
				String des = p.getDes();
				positnMap.put(code, des);
			}
			
			//以竖直方式显示分店申购信息
			//对数据进行转换
			for(int i=0,len=spmes.size();i<len;i++){
				Map<String,Object> map = spmes.get(i);
				Iterator<String> iter = map.keySet().iterator();
				//每条物资下面有分店集合
				List<Map<String,Object>> fdmaps = new ArrayList<Map<String,Object>>();
				int num = 1;
				while(iter.hasNext()){
					String key = iter.next();
					if(key.indexOf("F_")==0){
						Map<String,Object> fdmap = new HashMap<String,Object>();
						//分店编码
						String code = key.substring(2);
						//分店名称
						String des = positnMap.get(code);
						//订货量
						String dhl = String.valueOf(map.get(key));
						//序号
						fdmap.put("num", num++);
						fdmap.put("code", code);
						fdmap.put("des", des);
						fdmap.put("dhl", dhl);
						fdmap.put("unit", map.get("UNIT"));
						fdmaps.add(fdmap);
					}
				}
				//放到物资信息中
				map.put("fdmaps", fdmaps);
			}
			//绑定数据循环
			modelMap.put("ListBeanC", spmes);
		} catch (Exception e) {
		}
		return new ModelAndView(ExplanMConstants.PLAN_CHKSTOM2,modelMap);
	}
	
	/***
	 * 报货到加工单  导出  2014.10.21wjf
	 * @param response
	 * @param request
	 * @param session
	 * @param explanM
	 * @param madedEnd
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportExplanM")
	@ResponseBody
	public boolean exportExplanM(HttpServletResponse response,HttpServletRequest request,HttpSession session,String firmCode,
			String firmDes, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setAcct(acct);
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("positnType", PositnConstants.type_5);
		condition.put("codes", CodeHelper.replaceCode(firmCode));
		List<Positn> fendianList = explanService.findTableHead(condition);
		condition.put("codes", firmCode);
		condition.put("positnex", supplyAcct.getPositn());
		condition.put("supplyAcct", supplyAcct);
		List<Map<String,Object>> list = explanService.findPlanFirm_new(condition, null);//table体list
		String fileName = "报货到加工单导出表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return explanMService.exportExplanM(response.getOutputStream(),fendianList,list);
	}
	
	
	/***
	 * 报货到加工单  导出 20160415 jinshuai
	 * @param response
	 * @param request
	 * @param session
	 * @param explanM
	 * @param madedEnd
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportExplanM2")
	@ResponseBody
	public boolean exportExplanM2(HttpServletResponse response,HttpServletRequest request,HttpSession session,String firmCode,
			String firmDes, SupplyAcct supplyAcct, String line) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setAcct(acct);
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("positnType", PositnConstants.type_5);
		//condition.put("codes", CodeHelper.replaceCode(firmCode));
		//查询所有分店
		List<Positn> fendianList = explanService.findTableHead(condition);
		
		String endFirm = "";
		//如果未设置配送线路
		if(line!=null){
			//根据择的配送线路过滤分店
			AcctLineFirm alf = new AcctLineFirm();
			if(null==firmCode||"".equals(firmCode)){
				alf.setFirm(null);
			}else{
				Positn positn = new Positn();
				positn.setCode(firmCode);
				alf.setFirm(positn);
			}
			alf.setCode(line);
			alf.setAcct(session.getAttribute("ChoiceAcct").toString());
			//得到分店
			List<AcctLineFirm> alfs = linesFirmMapper.findFirmByLinesFirmId(alf);
			//拼接分店编码
			String firmCode2 = null;
			for(int i=0,len=alfs.size();i<len;i++){
				AcctLineFirm alf2 = alfs.get(i);
				firmCode2 += ","+alf2.getFirm().getCode();
			}
			endFirm = firmCode2;
		}else{
			endFirm = firmCode;
		}
		
		//放到查询条件中
		condition.put("codes", endFirm);
		condition.put("positnex", supplyAcct.getPositn());
		condition.put("supplyAcct", supplyAcct);
		
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		//如果配送线路有对应的分店才查询数据
		if(endFirm!=null){
			//物资信息
			list = explanService.findPlanFirm_new(condition, null);
		}
		
		String fileName = "报货到加工单";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		//没有数据则返回
		if(list==null||list.size()==0||fendianList==null||fendianList.size()==0){
			return false;
		}
		
		return explanMService.exportExplanM2(response.getOutputStream(),fendianList,list);
	}
	
	
	/**
	 * 打印报货到加工单
	 * jinshuai 20160426
	 */
	@RequestMapping(value = "/printPlan")
	public ModelAndView printPlan(ModelMap modelMap, HttpSession session, Page page, String firmCode,
			String firmDes, SupplyAcct supplyAcct, String action, String line)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印报货到加工单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收用户输入的查询条件
		HashMap<String, Object> param=new HashMap<String, Object>();
		
		//暂时未使用
		param.put("firmCode", firmCode);
		param.put("firmDes", firmDes);
		param.put("supplyAcct", supplyAcct);
		param.put("action", action);
		param.put("line", line);
		
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/explanM/printPlan.do");//传入回调路径
		
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("positnType", PositnConstants.type_5);
		//查询所有分店
		List<Positn> positnList = explanService.findTableHead(condition);
		
		//把Positn集合转换为Map
		//用在下发通过key得到名称
		Map<String,String> positnMap = new HashMap<String,String>();
		for(int i=0,len=positnList.size();i<len;i++){
			Positn p = positnList.get(i);
			String code = p.getCode();
			String des = p.getDes();
			positnMap.put(code, des);
		}
		
		String endFirm = "";
		//如果未设置配送线路
		if(line!=null){
			//根据择的配送线路过滤分店
			AcctLineFirm alf = new AcctLineFirm();
			if(null==firmCode||"".equals(firmCode)){
				alf.setFirm(null);
			}else{
				Positn positn = new Positn();
				positn.setCode(firmCode);
				alf.setFirm(positn);
			}
			alf.setCode(line);
			alf.setAcct(session.getAttribute("ChoiceAcct").toString());
			//得到分店
			List<AcctLineFirm> alfs = linesFirmMapper.findFirmByLinesFirmId(alf);
			//拼接分店编码
			String firmCode2 = null;
			for(int i=0,len=alfs.size();i<len;i++){
				AcctLineFirm alf2 = alfs.get(i);
				firmCode2 += ","+alf2.getFirm().getCode();
			}
			endFirm = firmCode2;
		}else{
			endFirm = firmCode;
		}
		
		//放到查询条件中
		condition.put("codes", endFirm);
		condition.put("positnex", supplyAcct.getPositn());
		condition.put("supplyAcct", supplyAcct);
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		//如果配送线路有对应的分店才查询数据
		if(endFirm!=null){
			//物资信息
			list = explanService.findPlanFirm_new(condition, null);
		}
		
		//所有的数据
		List<Map<String,Object>> listAlls=new ArrayList<Map<String,Object>>();
		
		//以竖直方式显示分店申购信息
		//对数据进行转换
		for(int i=0,len=list.size();i<len;i++){
			Map<String,Object> map = list.get(i);
			Iterator<String> iter = map.keySet().iterator();
			
			//分组字符串
			StringBuilder sb = new StringBuilder();
			sb.append("物资编码：");
			sb.append(map.get("SP_CODE"));
			sb.append("  ");
			sb.append("物资名称：");
			sb.append(map.get("SP_NAME"));
			sb.append("  ");
			sb.append("仓库库存量：");
			sb.append(map.get("AMOUNT_NOW"));
			sb.append("  ");
			sb.append("车间需生产量：");
			sb.append(map.get("AMOUNT_XU"));
			//序号
			int num = 1;
			while(iter.hasNext()){
				String key = iter.next();
				if(key.indexOf("F_")==0){
					Map<String,Object> fdmap = new HashMap<String,Object>();
					//分店编码
					String code = key.substring(2);
					//分店名称
					String des = positnMap.get(code);
					//订货量
					String dhl = String.valueOf(map.get(key));
					//序号
					fdmap.put("num", num++);
					fdmap.put("code", code);
					fdmap.put("des", des);
					fdmap.put("dhl", dhl);
					fdmap.put("unit", map.get("UNIT"));
					fdmap.put("groupStr", sb.toString());
					//增加合计
					fdmap.put("amount", map.get("AMOUNT"));
					//放到数据集合中
					listAlls.add(fdmap);
				}
			}
		}
		
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name= "报货到加工单";
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        modelMap.put("List", listAlls);
	    modelMap.put("parameters", parameters);
	    Map<String,String> rs=ReadReportUrl.redReportUrl(ExplanMConstants.ORDER_TO_WORK_PRINTPDF_STR,ExplanMConstants.ORDER_TO_WORK_PRINT_MODULE,ExplanMConstants.ORDER_TO_WORK_PRINT_MODULE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}
	
	
	
	/**
	 * 新增调度  来自报货计划
	 */
	@RequestMapping(value = "saveExplanByPlan")
	@ResponseBody
	public Object saveExplanByPlan(ModelMap modelMap, String sp_code, ExplanM explan, Page page,
			Supply supply, HttpSession session)throws Exception {
		//帐套
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explan.setExplanno(explanMService.getMaxExplanno());
		explan.setMadeby(session.getAttribute("accountName").toString());
		explan.setTyp("1");
		return explanMService.saveExplanByPlan(explan, acct);//来自计划  
	}
	
	/**
	 * 查询已审核加工单主表
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listExplanM")
	public ModelAndView listExplanM(HttpSession session,ModelMap modelMap, ExplanM explanM, Date madedEnd, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			explanM.setStatus("1");
			if(null==madedEnd){
				modelMap.put("madedEnd", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			}else{
				modelMap.put("madedEnd", madedEnd);
			}
			if(null==explanM.getMaded()){
				explanM.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			}
			List<ExplanM> listExplanM=explanMService.findAllExplanMByinput(explanM, page, madedEnd);
			modelMap.put("listExplanM", listExplanM);
			modelMap.put("explanM", explanM);
			modelMap.put("pageobj", page);
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			modelMap.put("listPositn", positnService.findAllPositn(positn));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView(ExplanMConstants.LIST_EXPLANM_JG,modelMap);
	}
	
	/**
	 * 查询已审核加工单子表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("tableExplanM")
	public ModelAndView listChkstoDemod(ModelMap modelMap, ExplanD explanD, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ExplanD> listExplanD = explanMService.findAllExplanD(explanD);
		modelMap.put("listExplanD", listExplanD);
		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.TABLE_EXPLANM_JG);
	}
	
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addLL")
	public ModelAndView addLL(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			ExplanSpcode explanSpcode=new ExplanSpcode();
			explanSpcode.setMadeby(accountName);
			explanSpcode.setDat(new Date());
			explanSpcode.setNo(explanMService.getMaxExplanLLNo());
			modelMap.put("explanSpcode",explanSpcode);//日期等参数
			modelMap.put("curStatus", "add");//当前页面状态
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			modelMap.put("listPositn", positnService.findAllPositn(positn));
		}else{
			modelMap.put("tableFrom", tableFrom);//当前页面状态
		}
		return new ModelAndView(ExplanMConstants.UPDATE_EXPLANLL,modelMap);	
	}
	
	/**
	 * 生成领料单 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkoutm")
	public ModelAndView toChkstom(ModelMap modelMap, ExplanM explanM, String idList, HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("productList",prdctbomService.findSumMaterialByExplanno(idList, acct, explanM.getPositn().getCode()));
		modelMap.put("idList", idList);
		modelMap.put("explanM", explanM);
		//加入判断昨天是否盘点过的标志0已盘点1未盘点
		String yearr = mainInfoService.findYearrList().get(0);//会计年
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("yearr",yearr);
		map.put("acct", acct);
		map.put("positn", explanM.getPositn().getCode());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.set(year, month, day, 0, 0, 0);
		//开始日期
		Date bdate = cal.getTime();
		cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		//结束日期
		Date edate = cal.getTime();
		map.put("bdate", bdate);
		map.put("edate", edate);
		//得到最后盘点日期
		int num = inventoryService.findExLastPdDate(map);
		if(num>0){
			modelMap.put("tag", 0);
		}else{
			modelMap.put("tag", 1);
		}
		return new ModelAndView(ExplanMConstants.SAVE_CHKOUTM, modelMap);
	}
	
	/**
	 * 生成出库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkoutm")
	@ResponseBody
	public String saveChkoutm(ModelMap modelMap, ExplanSpcode explanSpcode, HttpSession session, String idList) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanMService.saveChkoutm(explanSpcode, session.getAttribute("ChoiceAcct").toString(), session.getAttribute("accountName").toString(), idList);
	}
	
	/**
	 * 生成领料单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkout")
	@ResponseBody
	public String saveChkout(ModelMap modelMap, ExplanSpcode explanSpcode, HttpSession session, String idList) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanMService.saveChkout(explanSpcode, session.getAttribute("ChoiceAcct").toString(),session.getAttribute("accountName").toString(), idList);
	}
	
	/**
	 * 审核领料单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/auditChkout")
	@ResponseBody
	public String auditChkout(ModelMap modelMap, ExplanSpcode explanSpcode, HttpSession session, String idList) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanMService.saveChkoutm(explanSpcode, session.getAttribute("ChoiceAcct").toString(), session.getAttribute("accountName").toString(), idList);
	}
	
	/**
	 * 领料单填制页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkout")
	public ModelAndView toChkout(ModelMap modelMap, ExplanSpcode explanSpcode, String action, String idList, 
			HttpSession session, String tableFrom) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			explanSpcode=new ExplanSpcode();
			explanSpcode.setMadeby(accountName);
			explanSpcode.setDat(new Date());
			explanSpcode.setNo(explanMService.getMaxExplanLLNo());
			modelMap.put("explanSpcode", explanSpcode);//日期等参数
			modelMap.put("curStatus", "add");//当前页面状态
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			modelMap.put("listPositn", positnService.findAllPositn(positn));
			
		}else{
//			modelMap.put("explanLLList", explanMService.findAllExplanLL(explanSpcode);
			modelMap.put("tableFrom", tableFrom);//当前页面状态
		}
		return new ModelAndView(ExplanMConstants.TABLE_CHKOUT,modelMap);	
	}
	
//	/**
//     * 查询加工单list
//     * @param madedEnd  
//     * @param searchDate
//     * @param action  init表示初始页面
//     */
//	@RequestMapping(value = "/listExplanSpcode")
//	public ModelAndView listExplanSpcode(ModelMap modelMap, ExplanSpcode explanSpcode, Date madedEnd, Page page, String action)
//			throws Exception {
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		if(null!=explanSpcode.getDat() && !"".equals(explanSpcode.getDat())){
//			explanSpcode.setDat(DateFormat.formatDate(explanSpcode.getDat(), "yyyy-MM-dd"));
//		}else{
//			explanSpcode.setDat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
//		}
//		if (madedEnd==null) {
//			modelMap.put("madedEnd", new Date());
//		}else {
//			modelMap.put("madedEnd", madedEnd);
//		}
//		modelMap.put("explanSpcodeList", explanMService.findExplanSpcodeM(explanSpcode, page, DateFormat.formatDate(new Date(), "yyyy-MM-dd")));
//		modelMap.put("explanSpcode", explanSpcode);
//		modelMap.put("pageobj", page);
//		return new ModelAndView(ExplanMConstants.LIST_EXPLANSPCODE, modelMap);
//	}
		
	/**
     * 登记入库
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     */
	@RequestMapping(value = "/regist")
	public ModelAndView listRegist(ModelMap modelMap, ExplanD explanD, Page page, String action)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=explanD.getMaded() && !"".equals(explanD.getMaded())){
			explanD.setMaded(DateFormat.formatDate(explanD.getMaded(), "yyyy-MM-dd"));
		}else{
			explanD.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		if (null!=explanD.getDatEnd() && !"".equals(explanD.getDatEnd())) {
			explanD.setDatEnd(DateFormat.formatDate(explanD.getDatEnd(), "yyyy-MM-dd"));
		}else {
			explanD.setDatEnd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
//		explanD.setYnrk("N");
		modelMap.put("explanDList", explanMService.findExplanDForDj(explanD, page));
		modelMap.put("explanD", explanD);
//		modelMap.put("explanJson", JSONObject.fromObject(explanD));
		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.LIST_REGIST, modelMap);
	}
	
	/**
	 *  保存登记   修改数量 
	 * @param modelMap
	 * @param explan
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCntact")
	@ResponseBody
	public Object saveCntact(ModelMap modelMap, ExplanD expland, HttpSession session) throws Exception {
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanMService.updateCntact(expland, acct);
	}
	
	/**
	 * 生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinm")
	public ModelAndView saveChkinm(ModelMap modelMap,String ids,ExplanD explanD,Page page,HttpSession session,String positn) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountName=session.getAttribute("accountName").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explanD.setIds(ids);
		explanD.setPositn(positn);
		List<ExplanD> explanList = explanMService.findAllExplanD(explanD);
		chkinmService.saveChkinmM(explanList,ids,acct,new Date(),accountName);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	/**
	 * 添加入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveChkinmByCprk")
	public ModelAndView saveChkinmByCprk(ModelMap modelMap, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinmService.saveChkinm(chkinm,"in");
		return new ModelAndView(ChkinmConstants.SAVE_CHKINM,modelMap);	
	}
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addChkinm")
	public ModelAndView addChkinm(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		String inout=null;
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			Chkinm chkinm=new Chkinm();
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
			chkinm.setChkinno(chkinmService.getMaxChkinno());
			
			modelMap.put("chkinm",chkinm);//日期等参数
			String acct=session.getAttribute("ChoiceAcct").toString();
			String accountId=session.getAttribute("accountId").toString();
			modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
//			modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}else{
			modelMap.put("tableFrom", tableFrom);//当前页面状态
		}
		inout=ExplanMConstants.UPDATE_CHKINM;
		return new ModelAndView(inout,modelMap);	
	}
	
	/**
	 * 打印加工单  wangjie 2014年10月31日 10:02:45
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewExplanM")
	public ModelAndView viewExplanM(ModelMap modelMap,ExplanM explanm,String type,Page page,String checkno)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		explanm.setExplanno(Integer.parseInt(checkno));
		ExplanM explan=explanMService.findExplanMByid(explanm);
		List<ExplanD> explandList = explan.getExplanDList();
		
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
        parameters.put("report_name", "加工单"); 
        parameters.put("maded", DateFormat.getStringByDate(explan.getMaded(), "yyyy-MM-dd"));
        parameters.put("explanno", explan.getExplanno());
        parameters.put("positn.des", explan.getPositn().getDes());
        parameters.put("checby", explan.getChecby());
        parameters.put("madeby", explan.getMadeby());
        //sum_num sum_amount
        
        
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("checkno", checkno);
	    modelMap.put("List", explandList);
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/explanM/viewExplanM.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ExplanMConstants.REPORT_URL,ExplanMConstants.REPORT_URL);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}	
	
	/**
     * 查询领料单
     * @param madedEnd  
     * @param searchDate
     */
	@RequestMapping(value = "/listLL")
	public ModelAndView listLL(ModelMap modelMap, ExplanSpcode explanSpcode,Date madedStart, Date madedEnd, Page page)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positn = explanSpcode.getPositn();
		explanSpcode.setPositn(CodeHelper.replaceCode(positn));
		if (madedStart == null) {
			madedStart = new Date();
			modelMap.put("madedStart", new Date());
		}else {
			modelMap.put("madedStart", madedStart);
		}
		if (madedEnd == null) {
			madedEnd = new Date();
			modelMap.put("madedEnd", new Date());
		}else {
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("explanSpcodeList", explanMService.findExplanSpcodeM(explanSpcode, page, madedStart, madedEnd));
		explanSpcode.setPositn(positn);
		modelMap.put("explanSpcode", explanSpcode);
		//modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.LIST_EXPLANSPCODELL, modelMap);
	}
	
	/**
	 * 查询领料单详情
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/tableExplanSpcode")
	public ModelAndView tableExplanSpcode(ModelMap modelMap, ExplanSpcode explanSpcode, Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ExplanSpcode> listExplanSpcode = explanMService.findExplanSpcodeD(explanSpcode, page);
		modelMap.put("listExplanSpcode", listExplanSpcode);
		modelMap.put("explanSpcode", explanSpcode);
//		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.TABLE_EXPLANSPCODELL);
	}
	
	/**
	 * 查询加工单明细
	 * @param modelMap
	 * @param explanno
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/toSelectDetailed")
	public ModelAndView toSelectDetailed(ModelMap modelMap,int explanno) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("explandList", explanMService.toSelectDetailed(explanno));
		return new ModelAndView(ExplanMConstants.TABLE_LISTEXPLAND,modelMap);
	}
	
	/**
	 * 导出原材料需求单据  EXCEL  wangjie 2014年10月29日 16:20:18
	 * @param response
	 * @param session
	 * @param uncheckeditem 未选择的物资序号
	 * @param productcnt  物资调整量
	 * @param productmemo 物资备注
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyRequest")
	@ResponseBody
	public boolean exportChkinDetailS(HttpServletResponse response,HttpServletRequest request,
			ModelMap modelMap,String idList, HttpSession session,String code,String uncheckeditem,String productmemo,String productcnt) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		//所有原材料列表
		List<Product> productList = prdctbomService.findSumMaterialByExplanno(idList, acct, code);
		
		//未选择的原材料的序列号
		String[] index = uncheckeditem.split(",");
		if(index!=null && index.length!=0){
			for(int i=0; i<index.length; i++){
				if(!"".equals(index[i])){
					productList.remove(Integer.parseInt(index[i])-i);
				}
			}
		}
		
		String fileName = "原材料需求单据";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		return explanMService.exportSupplyRequst(response.getOutputStream(),productList,null,productmemo,productcnt);
	}
	
	/**
	 * 查找已审核报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedexplanM")
	public ModelAndView listCheckedexplanM(ModelMap modelMap, HttpSession session, Page page, String init, String sp_code, ExplanM explanM) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数，已审核报货单列表页面
		explanM.setAcct(session.getAttribute("ChoiceAcct").toString());
		explanM.setStatus("1");
		if(null!=init && ""!=init){
			explanM.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(new Date());  
	        calendar.add(Calendar.DATE, 1);  
			explanM.setHoped(DateFormat.formatDate(calendar.getTime(), "yyyy-MM-dd"));
		}
		//把参数放到MAP
		HashMap<String, Object> explanMMap=new HashMap<String, Object>();
		explanMMap.put("explanM", explanM);
		explanMMap.put("sp_code", sp_code);
		//把查询的结果集返回到页面
		modelMap.put("explanMList", explanMService.findByKey(explanMMap, page));
		modelMap.put("explanM", explanM);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanMConstants.TABLE_CHECKED_EXPLANM,modelMap);
	}
	
	/**
	 * 查看已审核加工单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedexplanM")
	public ModelAndView searchCheckedexplanM(ModelMap modelMap, HttpSession session, Page page, ExplanD expland, ExplanM explanM) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//关键字查询
		expland.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn positn=new Positn();
		positn.setTyp(PositnConstants.type_3);
		modelMap.put("positnList", positnService.findAllPositn(positn));
		modelMap.put("explanM", explanMService.findByExplanno(explanM));
		modelMap.put("explandList", explanMService.findAllExplanD(expland));
		return new ModelAndView(ExplanMConstants.SEARCH_CHECKED_EXPLANM,modelMap);
	}
	

	/**
	 * 反审核
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uncheckexplanM")
	@ResponseBody
	public int uncheckexplanM(ModelMap modelMap,Page page, HttpSession session, ExplanM explanM, String explannoIds) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		int result=0;
		if (explannoIds!=null){
			ExplanM explanMs= new ExplanM();
			List<String> ids = Arrays.asList(explannoIds.split(","));
			if (ids.size()>0){//tableExplanM.jsp界面的加入加工按钮方法，只修改status字段为1
				for (int i=0;i<ids.size();i++) {
					explanMs.setExplanno(Integer.valueOf(ids.get(i)));
					explanMs.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
					explanMs.setStatus("0");
					result=explanMService.updateExplanMM(explanMs, session.getAttribute("accountName").toString());
				}
			}
		}else{
			explanM.setStatus("0");
			result= explanMService.updateExplanMM(explanM, session.getAttribute("accountName").toString());
		}
		return  result;
	}
	
	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的   还要加上是否已经完成整个流程了2014.10.20wjf
	 */
	@RequestMapping(value = "/checkYnUnChk")
	@ResponseBody
	public Object checkYnUnChk(ModelMap modelMap, Page page, HttpSession session, ExplanM explanM) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		explanM.setAcct(session.getAttribute("ChoiceAcct").toString());
		return explanMService.checkYnUnChk(explanM);
	}
	
}
