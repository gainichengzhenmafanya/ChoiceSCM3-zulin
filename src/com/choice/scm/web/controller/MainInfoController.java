package com.choice.scm.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.choice.framework.domain.system.*;
import com.choice.framework.vo.AccountCache;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.service.system.AccountFirmService;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.RoleOperateService;
import com.choice.framework.service.system.RoleService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.constants.MainInfoConstants;
import com.choice.scm.domain.Announcement;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.ReportModule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.AnnouncementService;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.SppriceService;
import com.choice.scm.util.ArrayUtil;

@Controller
@RequestMapping("mainInfo")
public class MainInfoController {

	@Autowired
	private MainInfoService mainInfoService;
	@Autowired	
	private AcctService acctService;
	@Autowired	
	private AnnouncementService announcementService;
	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	private SppriceService sppriceService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private RoleOperateService roleOperateService;
	@Autowired
	private AccountFirmService accountFirmService;
	@Autowired
	private AccountPositnService accountPositnService;
	
	/**
	 * 查询帐套信息
	 * @param modelMap
	 * @param yearr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mainList")
	public ModelAndView findMainInfo(ModelMap modelMap,String yearr) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Calendar calendar = Calendar.getInstance();
		int date = calendar.get(Calendar.DATE);
		List<String> yearrList= mainInfoService.findYearrList();
		int year = Integer.valueOf(yearrList.get(0));
		MainInfo mainInfo= mainInfoService.findMainInfo(year);
		int month = Integer.parseInt(mainInfo.getMonthh());
		modelMap.put("yearrList",yearrList);
		modelMap.put("dyear",year);
		modelMap.put("dmonth",month);
		modelMap.put("dday",date);
		modelMap.put("mainInfo",mainInfo);
		return new ModelAndView(MainInfoConstants.LIST_MAININFO,modelMap);
	}
	
	/**
	 * 根据年份查询帐套信息
	 * @param modelMap
	 * @param yearr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getMainList")
	@ResponseBody
	public Object getMainList(ModelMap modelMap,String yearr) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		if(null!=yearr && !yearr.equals("")){
			year = Integer.valueOf(yearr);
		}
		return mainInfoService.findMainInfoByYear(year);
	}

	/***
	 * 修改帐套保存
	 * @param modelMap
	 * @param mainInfo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,MainInfo mainInfo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		mainInfoService.updateMainInfo(mainInfo);
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int date = calendar.get(Calendar.DATE);
		
		MainInfo resultInfo= mainInfoService.findMainInfo(year);
		List<String> yearrList= mainInfoService.findYearrList();
		
		int month = acctService.getAccountMonth(resultInfo, null, new Date());
		
		modelMap.put("yearrList",yearrList);
		modelMap.put("dyear",year);
		modelMap.put("dmonth",month);
		modelMap.put("dday",date);
		modelMap.put("mainInfo",resultInfo);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/***
	 * 查询用户定义的所需信息
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findTableMain(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountId = (String)(session.getAttribute("accountId"));
		int trLength = 10;
		Date startDate = DateFormat.getDateBefore(new Date(), "day", -1, 3);
		String startDateString = DateFormat.getStringByDate(startDate,"yyyy-MM-dd");
		Date yesterDay = DateFormat.getDateBefore(new Date(), "day", -1, 1);
		String yesterDayString = DateFormat.getStringByDate(yesterDay, "yyyy-MM-dd");
		//所有菜单的list信息
		Map<String,Object> conditionMap = new HashMap<String, Object>();
		conditionMap.put("acct", acct);
		conditionMap.put("topN", trLength);
		conditionMap.put("maded", startDate);
		conditionMap.put("today", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		conditionMap.put("yesterday", yesterDay);
		
		
		//查询未审核单据
		List<Map<String,Object>> unCheckedList = null;//未审核信息
		List<Announcement> announcementList = null;//公告信息
		List<Supply> supplyListOver= null;//库存超过上限
		List<Supply> supplyListLow= null;//库存不足下限
		List<Spprice> sppriceList = null;//将到期物资
		List<Spprice> sppriceLIST = null;
		List<Map<String,Object>> stackBillSumList = null;//进货单据汇总
		List<Map<String,Object>> outBillSumList = null;//出库单单据汇总
//		List<Map<String,Object>> firmAmtList = new ArrayList<Map<String,Object>>();//各门店昨日营业额
//		List<Map<String,Object>> uploadList = new ArrayList<Map<String, Object>>();//各门店上传情况
//		List<Map<String,Object>> storeYyeList = new ArrayList<Map<String, Object>>();//门店营业额
		List<Map<String,Object>> yiBiaoPan = new ArrayList<Map<String, Object>>();//营业额增长率、人均、单客增长率
		
		//查询用户自定义菜单信息
		WorkBench workBench = mainInfoService.findWorkBench(accountId);
		if(null!= workBench && null != workBench.getMenu() && !"".equals(workBench.getMenu())){
			String[] menuArray = workBench.getMenu().split(",");
			for(String menu:menuArray){
				if(menu.equals("m_1_1")){//未审核信息
					unCheckedList = mainInfoService.findUnChecked(conditionMap);
					modelMap.put("today", unCheckedList.get(3).get("count"));
					modelMap.put("yesterday", unCheckedList.get(4).get("count"));
					unCheckedList = unCheckedList.subList(0, 3);
					
				}else if(menu.equals("m_1_2")){//查询公告信息
					announcementList = announcementService.findAllAnnouncement();
				}else if(menu.equals("m_2_1")){//库存超过上限
					supplyListOver = mainInfoService.findSupplyOver(conditionMap);
				}else if(menu.equals("m_2_2")){//库存不足下限
					supplyListLow = mainInfoService.findSupplyLow(conditionMap);
				}else if(menu.equals("m_2_3")){//进货单据汇总
					/*Map<String,Object> condition = new HashMap<String, Object>();
					condition.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, 3), "yyyy-MM-dd"));
					condition.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					stackBillSumList = mainInfoService.findStackBillSum(condition);*/
				}else if(menu.equals("m_2_4")){//出库单据汇总
					/*Map<String,Object> condition = new HashMap<String, Object>();
					condition.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, 3), "yyyy-MM-dd"));
					condition.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					outBillSumList = mainInfoService.findOutBillSum(condition);*/
				}else if(menu.equals("m_2_5")){//将到期物资报价
					Spprice spprice = new Spprice();
					spprice.setAcct(acct);
					spprice.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
					Page page = new Page();
//					page.setPageSize(Integer.MAX_VALUE);
					page.setPageSize(10);
					sppriceList = sppriceService.findSppriceByEdatMain(spprice,page);
					if(sppriceList.size()>trLength){
						sppriceList.subList(0, trLength);
					}
					
					//计算物资报价剩余日期
					sppriceLIST = new ArrayList<Spprice>();
					for(Spprice sp:sppriceList){
						if(sp.getDeltaT()!=null && sp.getDeltaT()>=0){
							Date date = sp.getEdat();
							Date now = new Date();
							long deltaT = (date.getTime()-now.getTime())/86400000+1;
							sp.setDeltaT((int)deltaT);
							sppriceLIST.add(sp);
						}
					}
//				}else if(menu.equals("m_2_7")){//各门店昨日营业额
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//					String firmids = "";
//					AccountFirm accountFirm = new AccountFirm();
//					accountFirm.setAccountId(accountId);
//					List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
//					if(list.size()>0){
//						for(AccountFirm afirm : list)
//							firmids += afirm.getFirmId()+",";
//						firmids = firmids.substring(0,firmids.length()-1);
//					}
//					Condition condition = new Condition();
//					condition.setFirmid(firmids);
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
//					condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//					condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//					condition.setPage(10);
////					firmAmtList = mainInfoService.firmAmtList(condition);
//				}else if(menu.equals("m_2_11")){//各门店昨日未上传数据
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//					String firmids = "";
//					AccountFirm accountFirm = new AccountFirm();
//					accountFirm.setAccountId(accountId);
//					List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
//					if(list.size()>0){
//						for(AccountFirm afirm : list)
//							firmids += afirm.getFirmId()+",";
//						firmids = firmids.substring(0,firmids.length()-1);
//					}
//					Condition condition = new Condition();
//					condition.setFirmid(firmids);
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
//					condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//					condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
////					uploadList = mainInfoService.getUnUploadFirm(condition);
				}else if(menu.equals("m_2_20")){//常用报表快捷方式
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					String locale = session.getAttribute("locale").toString();
					ReportModule rm = new ReportModule();
					rm.setAccountId(accountId);
					rm.setLocale(locale);
					List<ReportModule> list = mainInfoService.findMyReportModule(rm);
					modelMap.put("reportModuleList",list);
//				}else if(menu.equals("m_2_12")){//门店营业额
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//					storeYyeList = mainInfoService.findStoreYye();
//				}else if(menu.equals("m_66_1")||menu.equals("m_66_2")){//营业额增长率、人均、单客增长率 总部boh  中餐
//					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//					yiBiaoPan = mainInfoService.getYiBiaoPan();
//					modelMap.put("y1",yiBiaoPan.get(0).get("ZQAMTPER"));
//					modelMap.put("y2",yiBiaoPan.get(0).get("ZHAMT"));
//					modelMap.put("y3",yiBiaoPan.get(0).get("PAXPER"));
//					modelMap.put("y4",yiBiaoPan.get(0).get("PAXRJ"));
				}else if(menu.equals("m_76_1")||menu.equals("m_76_2")){//营业额增长率、人均、单客增长率 总部boh 快餐
					DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
					yiBiaoPan = mainInfoService.getYiBiaoPanKcBoh();
					modelMap.put("ky1",yiBiaoPan.get(0).get("ZQAMTPER"));
					modelMap.put("ky2",yiBiaoPan.get(0).get("ZHAMT"));
					modelMap.put("ky3",yiBiaoPan.get(0).get("PAXPER"));
					modelMap.put("ky4",yiBiaoPan.get(0).get("PAXRJ"));
				}else if(menu.equals("m_33_1")){//misboh查询当天要报货的类别
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					//1.判断有没有分配所属分店，没有分配直接返回
					AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
					if (null != accountPositn && null != accountPositn.getPositn()) {
						String positnCode = accountPositn.getPositn().getCode();
						ScheduleD scheduleD = new ScheduleD();
						scheduleD.setOrderDate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
						scheduleD.setPositnCode(positnCode);
						List<ScheduleD> detailsList = mainInfoService.findSchedulesByPositn(scheduleD);
						modelMap.put("scheduleList", detailsList);
					}
				}else if(menu.equals("m_33_2")){//misboh查询当天 要验货的物资
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					//1.判断有没有分配所属分店，没有分配直接返回
					AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
					if (null != accountPositn && null != accountPositn.getPositn()) {
						String positnCode = accountPositn.getPositn().getCode();
						//1.查询今日直配到货数量
						Dis dis = new Dis();
						dis.setFirmCode(positnCode);
						dis.setInd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
						List<Dis> direList = mainInfoService.findDireCountGroupbyDeliver(dis);
						modelMap.put("direList", direList);
						//2.查询统配到货数量
						List<SupplyAcct> outList = mainInfoService.findOutCount(dis);
						modelMap.put("outList", outList);
						//3.查询调拨到货数量
						List<SupplyAcct> dbList = mainInfoService.findDbCountGroupByPositn(dis);
						modelMap.put("dbList", dbList);
					}
				}
			}
		}
		
		modelMap.put("currDate", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("startDate", startDateString);
		modelMap.put("yesterDay", yesterDayString);
		modelMap.put("menu",null==workBench?"":workBench.getMenu());
		modelMap.put("unCheckedList",unCheckedList);
		modelMap.put("announcementList",announcementList);
		modelMap.put("supplyListOver",supplyListOver);
		modelMap.put("supplyListLow",supplyListLow);
		modelMap.put("sppriceList",sppriceLIST);
		modelMap.put("stackBillSumList", stackBillSumList);
		modelMap.put("outBillSumList", outBillSumList);
		
		return new ModelAndView(MainInfoConstants.MAIN,modelMap);
	}
	
	//查询图标
	@RequestMapping("/getXml")
	public void getXmlForGroupSales(HttpServletResponse response,HttpSession session,String id,String days) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		String firmids = "";
		AccountFirm accountFirm = new AccountFirm();
		accountFirm.setAccountId(accountId);
		List<AccountFirm> list = accountFirmService.findAccountFirm(accountFirm);
		if(list.size()>0){
			for(AccountFirm afirm : list)
				firmids += afirm.getFirmId()+",";
			firmids = firmids.substring(0,firmids.length()-1);
		}
		String acct = session.getAttribute("ChoiceAcct").toString();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("acct", acct);
		map.put("topN", 10);
		
		if(id.equals("m_2_1")){
			mainInfoService.findXmlForSupplyOver(response, map).output();
		}else if(id.equals("m_2_2")){
			mainInfoService.findXmlForSupplyLow(response, map).output();
		}else if(id.equals("m_2_3")){
			if(null==days){
				days="30";
			}
			map.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			map.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, Integer.valueOf(days)), "yyyy-MM-dd"));
			mainInfoService.findXmlForPurchaseInSum(response,map).output();
		}else if(id.equals("m_2_4")){
			if(null==days){
				days="30";
			}
			map.put("endDate", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			map.put("startDate", DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", -1, Integer.valueOf(days)), "yyyy-MM-dd"));
			mainInfoService.findXmlForOutBillSum(response,map).output();
		}else if(id.equals("m_2_5")){
			
		}else if(id.equals("m_2_6")){
			mainInfoService.findXmlForOutTop10(response).output();
//		}else if(id.equals("m_2_8")){
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
//			Condition condition = new Condition();
//			condition.setFirmid(firmids);
//			condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//			condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//			mainInfoService.findModAmt(response, condition).output();
//		}else if(id.equals("m_2_9")){
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
//			Condition condition = new Condition();
//			condition.setFirmid(firmids);
//			condition.setBdat(DateFormat.getFirstDayOfCurrMonth());
//			condition.setEdat(DateFormat.getEndDayOfCurrMonth(new Date()));
//	//		Condition conBefore = new Condition();
//	//		conBefore.setBdat(DateFormat.getDateByString("2013-01-01", "yyyy-MM-dd"));
//	//		conBefore.setEdat(DateFormat.getDateByString("2013-01-31", "yyyy-MM-dd"));
//			mainInfoService.findModLineAmt(response,condition).output();
//		}else if(id.equals("m_2_10")){
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
//			Condition condition = new Condition();
//			condition.setFirmid(firmids);
//			condition.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//			condition.setEdat(DateFormat.getDateBefore(new Date(),"day",-1,1));
//			condition.setDetailMod(1);
//			mainInfoService.findCaiPinLeiBiePaiHang(response,condition).output();
//		}else if(id.equals("m_2_13")){
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//			mainInfoService.findLbtjList(response).output();
//		}else if(id.equals("m_2_14")){
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//			mainInfoService.findSczbList(response).output();
//		}else if(id.equals("m_66_3")){//昨日营业额
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//			mainInfoService.findModMultiAmtList(response).output();
//		}else if(id.equals("m_66_4")){//本月营业额走势图
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//	 		mainInfoService.jituanyingyeeyuefenxi(response).output();
//		}else if(id.equals("m_66_5")){//菜品类别排行
//			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//	 		mainInfoService.findCaiPinLeiBiePaiHang(response).output();
		}else if(id.equals("m_76_4")){//本月营业额走势图 快餐
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
	 		mainInfoService.findTotalMoneyQushiByDays(response).output();
		}else if(id.equals("m_76_5")){//菜品类别排行 快餐
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			mainInfoService.findCplbzbList(response).output();
		}
	}
	
	/**
	 * 查询全部的 超上限物资
	 */
	@RequestMapping(value = "/overLimit")
	public ModelAndView supplyOverLimit(ModelMap modelMap,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Supply supply = new Supply();
		supply.setAcct(acct);
		List<Supply> announcementList= mainInfoService.findAllSupplyOver(supply, page);
		modelMap.put("pageobj", page);
		modelMap.put("supplyList",announcementList);
		return new ModelAndView(MainInfoConstants.OVER_LIMIT,modelMap);
	}
	
	/**
	 * 查询全部的 不足上限物资
	 */
	@RequestMapping(value = "/lessLimit")
	public ModelAndView supplyLessLimit(ModelMap modelMap,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Supply supply = new Supply();
		supply.setAcct(acct);
		List<Supply> announcementList= mainInfoService.findAllSupplyLow(supply, page);
		modelMap.put("pageobj", page);
		modelMap.put("supplyList",announcementList);
		return new ModelAndView(MainInfoConstants.LESS_LIMIT,modelMap);
	}
	
	/**
	 * 用户工作台配置界面
	 */
	@RequestMapping(value = "/open")
	public ModelAndView openTableMain(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		//根据账号查询角色中的  menu字段，确定显示哪些模块
		List<Role> roleList=roleService.findRoleMainTableList(accountId);
		String allMenu = roleList.get(0).getMenu();
		for(Role role:roleList){
			allMenu = ArrayUtil.getSumString(allMenu, role.getMenu(), ",");
		}
		//查询用户自定义菜单信息
		WorkBench workBench = mainInfoService.findWorkBench(accountId);
		
		if(null!=workBench){
			String showMenu = ArrayUtil.getMixedString(allMenu, workBench.getMenu(), ",");
			modelMap.put("menu",showMenu);
		}
		modelMap.put("roleMenu",allMenu);
		return new ModelAndView(MainInfoConstants.CONFIG_MAIN,modelMap);
	}
	
	/**
	 * 保存用户配置的工作台
	 */
	@RequestMapping(value = "/saveWorkbench") 
	@ResponseBody
	public String saveWorkbench(ModelMap modelMap,String menu,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		WorkBench workbench = mainInfoService.findWorkBench(accountId);
		
		if(null != workbench){
			workbench.setAccountid(accountId);
			workbench.setMenu(menu);
			workbench.setId(workbench.getId());
			mainInfoService.updateWorkBench(workbench);
		}else{
			workbench = new WorkBench();
			workbench.setAccountid(accountId);
			workbench.setMenu(menu);
			workbench.setId(CodeHelper.createUUID());
			mainInfoService.saveWorkBench(workbench);
		}
		
		return "ok";
	}
	/**
	 * 跳转到常用操作选择页面
	 */
	@RequestMapping(value = "/addUsedTag") 
	public ModelAndView addUsedTag(ModelMap modelMap,String menu,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		Role role = new Role();
		role.setId(accountId);
		//查询角色现有的操作列表
		modelMap.put("roleOperateList",roleOperateService.findRoleOperateList(role));
		String tagIdList = "";
		if(mainInfoService.findWorkBench(accountId)!=null ){
			tagIdList = mainInfoService.findWorkBench(accountId).getTagpage();
		}
		modelMap.put("tagIdList", tagIdList);
		return new ModelAndView(MainInfoConstants.USED_TAG,modelMap);
	}
	/**
	 * 保存常用操作id
	 */
	@RequestMapping(value = "/saveUsedModel") 
	@ResponseBody
	public String saveUsedModel(ModelMap modelMap,HttpSession session,String roleOperateId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		WorkBench workBench = new WorkBench();
		workBench.setAccountid(accountId);
		workBench.setTagpage(roleOperateId);
		//查询角色现有的操作列表
		return mainInfoService.saveUsedModel(workBench);
	}
	
	/***
	 * 查询所有的年列表
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllYears") 
	@ResponseBody
	public Object findAllYears(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = (String)(session.getAttribute("ChoiceAcct"));
		return mainInfoService.findAllYears(acct);
	}
	
	/**
	 * 跳转到常用报表设置界面
	 */
	@RequestMapping(value = "/addReportModule") 
	public ModelAndView addReportModule(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		String locale = session.getAttribute("locale").toString();
		ReportModule rm = new ReportModule();
		rm.setLocale(locale);
		rm.setAccountId(accountId);
		List<ReportModule> list = mainInfoService.findMyReportModule(rm);
		//从权限中得到所有的module 20170815wjf
		Map<String, Object> reportModule = new HashMap<String, Object>();
		AccountCache accountCache = (AccountCache)session.getAttribute("accountCache");
		if(accountCache != null && accountCache.getModuleList() != null && accountCache.getModuleList().size() > 0){
			for(Module m : accountCache.getModuleList()){
				if("0021-0010".equals(m.getCode())){//报表系统的节点
					reportModule.put("id",m.getId());
					reportModule.put("text",m.getName().substring(0,m.getName().indexOf(",")));
					reportModule.put("state","open");
					getChildren(reportModule, accountCache.getModuleList(), list);
				}
			}
		}
		modelMap.put("data", JSONArray.fromObject(reportModule).toString());
		return new ModelAndView(MainInfoConstants.USED_REPORT,modelMap);
	}

	/***
	 * 递归获取子节点
	 * @param reportModule
	 * @param allList
	 * @param myList
	 */
	private void getChildren(Map<String , Object> reportModule, List<Module> allList, List<ReportModule> myList){
		List<Map<String , Object>> moduleList = new ArrayList<Map<String , Object>>();
		for(Module m : allList){
			if(m.getParentModule() != null && reportModule.get("id").equals(m.getParentModule().getId())){
				Map<String , Object> childRm = new HashMap<String , Object>();
				childRm.put("id", m.getId());
				childRm.put("text", m.getName().substring(0,m.getName().indexOf(",")));
				childRm.put("checked", false);
				for(ReportModule rm : myList){
					if(m.getId().equals(rm.getId())){
						childRm.put("checked", true);
						break;
					}
				}
				moduleList.add(childRm);
				getChildren(childRm,allList,myList);
			}
		}
		if(moduleList.size() > 0) {
			reportModule.put("children", moduleList);
		}
	}
	
	/**
	 * 保存常用报表
	 */
	@RequestMapping(value = "/saveUsedReport") 
	@ResponseBody
	public String saveUsedReport(ModelMap modelMap,HttpSession session,ReportModule rm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = (String)(session.getAttribute("accountId"));
		rm.setAccountId(accountId);
		try{
			mainInfoService.saveUsedReport(rm);
			return "success";
		}catch(Exception e){
			return "fail";
		}
	}
}
