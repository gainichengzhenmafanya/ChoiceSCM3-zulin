package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostbomConstants;
import com.choice.scm.constants.PrdctConstants;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.PrdctbomService;
import com.choice.scm.util.ReadReportUrl;

/**
* 产品bom
* @author -  css
*/

@Controller
@RequestMapping("prdctbom")
public class PrdctbomController {
	@Autowired
	PrdctbomService prdctbomService;
		
	/**
	 * 模糊查询半成品
	 * @param modelMap
	 * @param prdct
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findPrdct(ModelMap modelMap,Supply prdct, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		prdct.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("supplyList", prdctbomService.findPrdct(prdct));
		modelMap.put("queryPrdct", prdct);
		return new ModelAndView(PrdctConstants.LIST_PRDCTBOM,modelMap);
	}
	
	/**
	 * 门店模糊查询半成品
	 * @param modelMap
	 * @param prdct
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listFirm")
	public ModelAndView findPrdctFirm(ModelMap modelMap,Supply prdct, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		prdct.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("supplyList", prdctbomService.findPrdctFirm(prdct));
		modelMap.put("queryPrdct", prdct);
		return new ModelAndView(PrdctConstants.LIST_PRDCTBOM_FIRM,modelMap);
	}
	
	/**
	 * 根据key查询半成品
	 * @param modelMap
	 * @param key
	 * @param prdctcard
	 * @param code
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findByKeyAjax",method=RequestMethod.POST)
	@ResponseBody
	public List<Supply> findByKey(ModelMap modelMap, String key, String prdctcard, String code, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return prdctbomService.findByKey(key.toUpperCase(), prdctcard, code, session.getAttribute("ChoiceAcct").toString());
	}
	
	/**
	 * 根据key查询半成品
	 * @param modelMap
	 * @param key
	 * @param prdctcard
	 * @param code
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findByKeyAjaxFirm",method=RequestMethod.POST)
	@ResponseBody
	public List<Supply> findByKeyFirm(ModelMap modelMap, String key, String prdctcard, String code, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return prdctbomService.findByKeyFirm(key.toUpperCase(), prdctcard, code, session.getAttribute("ChoiceAcct").toString());
	}
	
	/**
	 * 根据code查询原材料
	 * @param modelMap
	 * @param item     左边的半成品编码  sp_code
	 * @param unit
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableBycode")
	public ModelAndView findmaterial(ModelMap modelMap,String item, String itemNm, String unit, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Spcodeexm spcodeexm = new Spcodeexm();
		spcodeexm.setItem(item);
		spcodeexm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("productList", prdctbomService.findmaterial(spcodeexm));
		modelMap.put("item", item);//左边的半成品编码  sp_code
		modelMap.put("itemNm", itemNm);
		modelMap.put("unit", unit);
		return new ModelAndView(PrdctConstants.TABLE_PRDCTBOM,modelMap);
	}

	/**
	 * 查询物资存在的产品以及物资用量
	 * @author css
	 */
	@RequestMapping(value = "/select")
	public ModelAndView select(ModelMap modelMap, Product product) throws Exception {
		modelMap.put("resultList", prdctbomService.listResult(product));   //项目进度list
		return new ModelAndView(PrdctConstants.SELECT, modelMap);
	}
	
	/**
	 * 复制成本卡------页面跳转
	 * @param modelMap
	 * @param costbom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toCopy")
	public ModelAndView toCopy(ModelMap modelMap,String item) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("item", item);
		Supply prdct = new Supply();
		prdct.setVcode(item);
		modelMap.put("supplyList", prdctbomService.findPrdct(prdct));
		return new ModelAndView(PrdctConstants.PRDCTTYP, modelMap);
	}
	
	/**
	 * 复制成本卡----实际物料
	 * @param modelMap
	 * @param item
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/copyPrdct")
	public ModelAndView copyPrdct(String item, String ids, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		prdctbomService.copyPrdct(item, ids, acct);
		return new ModelAndView(CostbomConstants.DONE);
	}
	/**
	 * 增删改原材料
	 * @param modelMap
	 * @param code
	 * @param pro
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdateOrDelPrdctbom")
	public ModelAndView saveOrUpdateOrDelCostbom(ModelMap modelMap,String code, Product pro, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pro.setItem(code);
		pro.setAcct(session.getAttribute("ChoiceAcct").toString());
		prdctbomService.saveOrUpdateOrDelPrdctbom(pro);
		modelMap.put("product", pro);
		modelMap.put("item", code);
		Spcodeexm spcodeexm = new Spcodeexm();
		spcodeexm.setItem(code);
		spcodeexm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("productList", prdctbomService.findmaterial(spcodeexm));
		return new ModelAndView(PrdctConstants.TABLE_PRDCTBOM,modelMap);
	}
	
	/**
	 * 产品bom打印
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/print")
	public ModelAndView printCast(ModelMap modelMap, HttpSession session, String type, String item)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		Spcodeexm spcodeexm = new Spcodeexm();
		spcodeexm.setItem(item);
		spcodeexm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		List<Product> disList = prdctbomService.findmaterial(spcodeexm);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
 		// 根据物资编码获取物资名称
 		Supply supply = new Supply();
 		supply.setSp_code(item);
 		String itemNm = prdctbomService.getItemNm(supply);
        parameters.put("report_name", "产品bom");  
        parameters.put("itemNm", itemNm);  
        modelMap.put("parameters", parameters);//map
	       
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("item", item);
	    map.put("acct", session.getAttribute("ChoiceAcct").toString());
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/prdctbom/print.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdctConstants.REPORT,PrdctConstants.REPORT);//判断跳转路径
        modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 *产品bom---Excel 
	 */
	@RequestMapping(value = "/export")
	@ResponseBody
	public boolean export(HttpServletResponse response, HttpServletRequest request, HttpSession session, Page page,Product product,String lefttj) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "半成品项目成本卡明细表";
//		Product product = new Product();
//		List<Product> listCostbom = prdctbomService.findAllSpcodeexmIdList();
//		String itemIds="";
//		for (int i = 0; i < listCostbom.size(); i++) {
//			if(i==0){
//				itemIds+=listCostbom.get(i).getItem();
//			}else{
//				itemIds+=","+listCostbom.get(i).getItem();
//			}
//		}
//		product.setItem(itemIds);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}
		
		//update by jinshuai at 20160407
		//根据左侧条件查询导出excel
		String code = "";
		String prdctcard = "";
		if(lefttj!=null){
			String[] lefttjs = lefttj.split(":");
			if(lefttjs!=null){
				try{
					code = lefttjs[0];
				}catch(Exception e){
				}
				try{
					prdctcard = lefttjs[1];
				}catch(Exception e){
				}
			}
		}
		List<Supply> list = prdctbomService.findByKey("", prdctcard, code, session.getAttribute("ChoiceAcct").toString());
		List<Product> listp = new ArrayList<Product>();
		for(int i=0,len=list.size();i<len;i++){
			Supply s = list.get(i);
			Product p = new Product();
			p.setItem(s.getSp_code());
			p.setItemDes(s.getSp_name());
			listp.add(p);
		}
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		
		//如果没数据直接返回jinshuai
		if(listp.size()==0){
			return false;
		}
		
		return prdctbomService.exportExcel(response.getOutputStream(), listp);
	}
}
