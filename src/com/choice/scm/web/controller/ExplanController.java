package com.choice.scm.web.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ExplanConstants;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Explan;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.ExplanService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.PrdctbomService;
import com.choice.scm.util.CalChkNum;

@Controller
@RequestMapping(value = "explan")
public class ExplanController {

	private final static int PAGE_SIZE = 25;
	@Autowired
	private ExplanService explanService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private PrdctbomService prdctbomService;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private CalChkNum calChkNum;
	
	/**
	 * 初始跳转
	 * @param modelMap
	 * @param explan
	 * @return
	 * @throws Exception
	 
	@RequestMapping(value = "/list")
	public ModelAndView findAllExplan(ModelMap modelMap,Explan explan,String curStatus,Page page,String action)throws Exception {
		if(null!=action && "init".equals(action)){
			Date date=new Date();
			explan.setDat(DateFormat.getDateByString(date.toLocaleString().substring(0, 10), "yyyy-MM-dd"));
			explan.setDatEnd(DateFormat.getDateByString(date.toLocaleString().substring(0, 10), "yyyy-MM-dd"));
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("explanList", explanService.findAllExplan(explan, curStatus, "", page));
		modelMap.put("explan", explan);
		modelMap.put("curStatus", curStatus);
		modelMap.put("pageobj", page);
		return new ModelAndView(ExplanConstants.LIST_EXPLAN, modelMap);
	}
	*/
	
	/**
	 * 初始跳转
	 * @param modelMap
	 * @param explan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllExplan(ModelMap modelMap,Explan explan,String curStatus,String action)throws Exception {
		if(null!=action && "init".equals(action)){
			explan.setDat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			explan.setDatEnd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			modelMap.put("action", "init");
		}else{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			int totalCount = explanService.findAllExplanCount(explan, curStatus);
			explan.setStartNum(0);
			explan.setEndNum(PAGE_SIZE);
			modelMap.put("explanList", explanService.findAllExplanPage(explan, curStatus));
			modelMap.put("totalCount", totalCount);
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);
			}
		}
		
		modelMap.put("explanJson", JSONObject.fromObject(explan));
		modelMap.put("explan", explan);
		modelMap.put("curStatus", curStatus);
		modelMap.put("pageSize", PAGE_SIZE);
		return new ModelAndView(ExplanConstants.LIST_EXPLAN, modelMap);
	}
	
	/**
	 * 调度中心Ajax加载
	 * @param modelMap
	 * @param explan
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(Explan explan,String curStatus,String action,String currPage,String totalCount)throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explan.setStartNum(intCurrPage*PAGE_SIZE);
		explan.setEndNum((intCurrPage+1)*PAGE_SIZE);

		Map<String,Object> modelMap = new HashMap<String, Object>();
		
		modelMap.put("explanList", explanService.findAllExplanPage(explan, curStatus));
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		modelMap.put("currPage", currPage);
		modelMap.put("explanJson", JSONObject.fromObject(explan));
		modelMap.put("explan", explan);
		modelMap.put("curStatus", curStatus);
		
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/**
	 * 新增调度
	 */
	@RequestMapping(value = "save")
	public ModelAndView saveExplan(ModelMap modelMap,Explan explan,HttpSession session)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		String acct=session.getAttribute("ChoiceAcct").toString();
		explan.setExno(calChkNum.getMaxsequences("EXNO"));//取得序列最大值的公共方法
		explan.setAcct(acct);
		explanService.saveExplan(explan);//手动添加产品
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 *  保存计划   修改需加工量 
	 * @param modelMap
	 * @param explan
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCnt")
	@ResponseBody
	public Object saveCnt(ModelMap modelMap,Explan explan,HttpSession session) throws Exception {
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanService.updateCnt(explan,acct);
	}
	
	/**
	 *  保存登记   修改数量   数量2
	 * @param modelMap
	 * @param explan
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCntact")
	@ResponseBody
	public Object saveCntact(ModelMap modelMap,Explan explan,HttpSession session) throws Exception {
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return explanService.updateCntact(explan,acct);
	}
	
	/**
	 * 中类   多选框
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectGrp")
	public ModelAndView selectGrp(ModelMap modelMap,String grp,HttpSession session) throws Exception { 
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("grpList", grpTypService.findAllGrp(null));//中类
		modelMap.put("grp", grp);//中类
		return new ModelAndView(ExplanConstants.SELECT_GRP, modelMap);
	}
	
	/**
	 * 加工间   多选框
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPositnex")
	public ModelAndView selectPositnex(ModelMap modelMap,String positnex,HttpSession session,String type) throws Exception { 
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn=new Positn();
		if("firm".equals(type)){
			//positn.setTyp(PositnConstants.type_5);
		}else{
			positn.setTyp(PositnConstants.type_3);
		}
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("positn", positnex);//中类
		return new ModelAndView(ExplanConstants.SELECT_POSITN, modelMap);
	}
	
	/**
	 * 生成报货单 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkstom")
	public ModelAndView toChkstom(ModelMap modelMap,String idList,String positnex,Positn positn,String itemIds,HttpSession session) throws Exception {  
		Product product=new Product();
		String acct=session.getAttribute("ChoiceAcct").toString();
		positn.setAcct(acct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positn",positnService.findPositnByCode(positn));
		product.setAcct(acct);
		product.setItem(itemIds);
		modelMap.put("productList",prdctbomService.findSumMaterialBylist(idList,acct));
		modelMap.put("idList",idList);
		return new ModelAndView(ExplanConstants.SAVE_CHKSTOM, modelMap);
	}
	
	/**
	 * 生成报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkstom")
	@ResponseBody
	public String saveChkstom(ModelMap modelMap,String idList,Product product,String firm,HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountName=session.getAttribute("accountName").toString();
		product.setAcct(acct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String aa= explanService.saveChkstom(product,firm,accountName,idList);
		return aa;
	}
	
	/**
	 * 生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinm")
	public ModelAndView saveChkinm(ModelMap modelMap,String ids,Explan explan,Page page,HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountName=session.getAttribute("accountName").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Explan> explanList=explanService.findAllExplan(explan, null, ids, page);
		chkinmService.saveChkinm(explanList,ids,acct,new Date(),accountName);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 分解产品
	 * @throws Exception
	 */
	@RequestMapping(value = "/resolveExplan")
	public ModelAndView resolveExplan(ModelMap modelMap,String sp_codeIds,String ids,Explan explan,Page page,HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explanService.updateResolveExplan(ids,sp_codeIds,acct);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap,String ids,Explan explan,Page page) throws Exception {
		List<String> listids = Arrays.asList(ids.split(","));
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explanService.deleteExplan(listids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 根据加工间编码和物资编码获取供应商信息并返回json对象
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(String positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Deliver deliver = new Deliver();
		deliver.setPositn(positn);
		return explanService.findDeliverByPositn(deliver).getCode();
	}
	
	/****        报货计划  --计划        **/
	@RequestMapping(value = "/updateExplanByPlan")
	public ModelAndView updateExplanByPlan(ModelMap modelMap, HttpSession session, Page page, String firmCode,
			String firmDes, SupplyAcct supplyAcct, String action) throws Exception{
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			//得到所有加工间
			Positn positn2=new Positn();
			positn2.setTyp(PositnConstants.type_3);
			List<Positn> listPositn = positnService.findAllPositn(positn2);
			modelMap.put("listPositn", listPositn);
			
			//得到所有分店
			condition.put("positnType", PositnConstants.type_5);
			condition.put("codes", CodeHelper.replaceCode(firmCode));
			modelMap.put("columns", explanService.findTableHead(condition));
	//		modelMap.put("sp_code", supplyAcct.getSp_code());
			if("init".equals(action)){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				String dates=sdf.format(new Date());
				supplyAcct.setBdat(sdf.parse(dates));
				supplyAcct.setEdat(sdf.parse(dates));	
				supplyAcct.setDeal("0");
				supplyAcct.setPositn(0 == listPositn.size()?null :listPositn.get(0).getCode());//默认查第一个加工间的
			}
			condition.put("positnex", supplyAcct.getPositn());//解决差不着加工间的问题，wjf
			condition.put("supplyAcct", supplyAcct);
			condition.put("codes", firmCode);
			modelMap.put("ListBean", JSONArray.fromObject(explanService.findPlanFirm(condition, page)).toString());//table体list
			modelMap.put("supplyAcct", supplyAcct);
			
			modelMap.put("pageobj", page);
			modelMap.put("firmCode", firmCode);
			modelMap.put("firmDes", firmDes);
		} catch (Exception e) {
			// TODO: handle exception
		}
 		return new ModelAndView(ExplanConstants.PLAN_CHKSTOM,modelMap);
	}
	
	/**
	 * 新增调度  来自计划
	 */
	@RequestMapping(value = "saveExplanByPlan")
	public ModelAndView saveExplanByPlan(ModelMap modelMap,String sp_code, Explan explan,Page page, Supply supply,HttpSession session)throws Exception {
		//帐套
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explan.setExno(calChkNum.getMaxsequences("EXNO"));//取得序列最大值的公共方法
		explanService.saveExplanByPlan(explan, acct);//来自计划  
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/****        安全库存  --计划        **/
	@RequestMapping(value = "/safeCntToPlan")
	public ModelAndView safeCntToPlan(ModelMap modelMap,HttpSession session,Page page,String firmCode,String firmDes,SupplyAcct supplyAcct,String action) throws Exception{
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			modelMap.put("pageobj", page);
	//		modelMap.put("firmCode", firmCode);//貌似现在用不着了
	//		modelMap.put("firmDes", firmDes);
	//		modelMap.put("sp_code", supplyAcct.getSp_code());
			Positn positn=new Positn();
			positn.setTyp(PositnConstants.type_3);
			List<Positn> listPositn = positnService.findAllPositn(positn);
			modelMap.put("listPositn", listPositn);
			if(null == supplyAcct.getPositn() || "".equals(supplyAcct.getPositn())){
				modelMap.put("ListBean", "123");//table体list
//				supplyAcct.setPositn(0 == listPositn.size()?null :listPositn.get(0).getCode());//默认查第一个加工间的
			}else{
				condition.put("supplyAcct", supplyAcct);
				condition.put("positnType", PositnConstants.type_5);
		//		condition.put("codes", firmCode);
				modelMap.put("ListBean", JSONArray.fromObject(explanService.safeCntToPlan(condition, page)).toString());//table体list
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
 		return new ModelAndView(ExplanConstants.SAFECNT_PLAN,modelMap);
	}
	
	/**
	 * 新增调度  来自安全库存
	 */
	@RequestMapping(value = "saveExplanBySafeCnt")
	public ModelAndView saveExplanBySafeCnt(ModelMap modelMap,String sp_code,Explan explan,Page page, Supply supply,HttpSession session)throws Exception {
		//帐套
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		explan.setExno(calChkNum.getMaxsequences("EXNO"));//取得序列最大值的公共方法
		
		explanService.saveExplanBySafeCnt(explan,acct);//来自计划  
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}
