package com.choice.scm.web.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkstodemoConstants;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.ChkstodemoService;
/**
 * 报货单模板新增、删除、查询
 * @author csb
 *
 */
@Controller
@RequestMapping(value = "chkstodemo")

public class ChkstodemoController {

	@Autowired
	private ChkstodemoService chkstodemoService;
	
	/**
	 * 查询所有的申购单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkstodemo(ModelMap modelMap,HttpSession session,Page page,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//申购模板页面初始化
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkstodemo", chkstodemo);
		modelMap.put("pageobj", page);
		modelMap.put("chkstodemoList", chkstodemoService.findAllTitle(chkstodemo));
		return new ModelAndView(ChkstodemoConstants.TABLE_CHKSTODEMO,modelMap);
	}
	/**
	 * 申购模板批量添加报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstodemo")
	public ModelAndView addChkstodemo(ModelMap modelMap,HttpSession session,Page page,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//申购单模板批量添加时候的初始化页面
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemo(chkstodemo,page));
		return new ModelAndView(ChkstodemoConstants.ADD_CHKSTODEMO,modelMap);
	}
	/**
	 * 保存新增的申购单信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addNewChkstodemo")
	public ModelAndView addNewChkstodemo(ModelMap modelMap,HttpSession session,String action,Supply supply,String title2,Chkstodemo chkstodemo) throws Exception
	{	
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=action && !"".equals(action) && "init".equals(action)){
			chkstodemo.setTitle(new String(chkstodemo.getTitle().getBytes("ISO-8859-1"),"utf-8"));
			modelMap.put("chkstodemo", chkstodemo);
			return new ModelAndView(ChkstodemoConstants.SAVE_CHKSTODEMO,modelMap);
		}else{
			//保存新添加的申购模板
			chkstodemo.setTitle(title2);
			chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
			chkstodemo.setSupply(supply);
			//获取申购模板最大的序号
			int rec = chkstodemoService.findMaxRec(chkstodemo);
			chkstodemo.setRec(rec);
			chkstodemoService.saveNewChkstodemo(chkstodemo);
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		}
	}
	
	/**
	 * 根据title进行查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkstodemo")
	public ModelAndView findChkstodemo(ModelMap modelMap,HttpSession session,Page page,String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存用户输入的参数
		Chkstodemo chkstodemo=new Chkstodemo();
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=title && !"".equals(title)){
			chkstodemo.setTitle(title);
		}
		modelMap.put("pageobj", page);
		modelMap.put("maxRec", chkstodemoService.findMaxRec(chkstodemo));
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("title", title);
		//根据输入的title参数查询的结果集返回到页面
		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemo(chkstodemo,page));
		return new ModelAndView(ChkstodemoConstants.LIST_CHKSTODEMO,modelMap);
	}
	/**
	 * 申购模板添加报货单查询
	 * @param modelMap
	 * @param page
	 * @param chkstodemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkstodemoByAddChkstodemo")
	public ModelAndView findChkstodemoByAddChkstodemo(ModelMap modelMap,HttpSession session,Page page,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//申购模板批量添加时候的页面
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(null));
		modelMap.put("title", chkstodemo.getTitle());
		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemo(chkstodemo,page));
		return new ModelAndView(ChkstodemoConstants.ADD_CHKSTODEMO,modelMap);
	}

	/**
	 * 分页查询
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByPage")
	public ModelAndView findChkByPage(ModelMap modelMap,Page page,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("chkstodemo", chkstodemo);
		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemo(chkstodemo,page));
		return new ModelAndView(ChkstodemoConstants.LIST_CHKSTODEMO,modelMap);
	}

	/**
	 * 申购模板添加报货单分页查询
	 * @param modelMap
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByAddChkstodemoPage")
	public ModelAndView findChkByAddChkstodemoPage(ModelMap modelMap,HttpSession session,Page page,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//批量添加页面上的分页
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemo(null,page));
		return new ModelAndView(ChkstodemoConstants.ADD_CHKSTODEMO,modelMap);
	}

	/**
	 * 删除申购信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView deleteChkstodemo(ModelMap modelMap,HttpSession session,String rec) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//删除申购单
		Map<String,Object> map=new HashMap<String, Object>();
		Chkstodemo chkstodemo=new Chkstodemo();
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<String> chkstodemoList=Arrays.asList(rec.split(","));
		map.put("chkstodemo", chkstodemo);
		map.put("chkstodemoList", chkstodemoList);
		chkstodemoService.deleteChkstodemo(map);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
