package com.choice.scm.web.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmSupplyConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.FirmSupplyService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;

/**
 * 分店物资属性
 * @author css
 *
 */
@Controller
@RequestMapping("firmSupply")
public class FirmSupplyController {
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private FirmSupplyService firmSupplyService;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 * 1203
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, Positn positnSpcode, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		modelMap.put("listPositn", positnService.findPositnBySupply1(positnSpcode));//查询所有分店
		Positn positn = new Positn();
		positn.setTypn("'1201','1202'");
		List<Positn> listPositn1 = positnService.findPositnSuperNOPage(positn);//查询所有主直拨库、1202
		modelMap.put("listPositn1", listPositn1);
		//获取所有区域 wangjie 2014年12月24日 12:55:29
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		List<CodeDes> areaList = codeDesService.findCodeDes(codeDes, page);
//		modelMap.put("pageobj", page);
		modelMap.put("positnSpcode", positnSpcode);
		modelMap.put("areaList", areaList);
		return new ModelAndView(FirmSupplyConstants.LIST_FIRM, modelMap);
	}
	
	/**
	 * 分店物资
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/table")
	public ModelAndView table(ModelMap modelMap, PositnSpcode positnSpcode, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listFirmSupply",firmSupplyService.findFirmSupply(positnSpcode, page));
		modelMap.put("pageobj", page);
		modelMap.put("positnCode", positnSpcode.getPositn());
		modelMap.put("positnSpcode", positnSpcode);
		return new ModelAndView(FirmSupplyConstants.TABLE_SUPPLY, modelMap);
	}
	
	/**
	 * 转到修改页面
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/updateSupply")
	public ModelAndView updateSupply(ModelMap modelMap, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取选择的所有分店
//		String positnParam = positnSpcode.getPositn();
//		PositnSpcode spcode = new PositnSpcode();
		//当多个分店批量修改时 跳转到修改页面 默认查询第一家分店的物资
//		if(positnSpcode.getPositn().split(",").length>1){
//			positnSpcode.setPositn(positnSpcode.getPositn().split(",")[0]);
//		}
		
//		if(firmSupplyService.findFirmSupply(positnSpcode)!=null && 
//				firmSupplyService.findFirmSupply(positnSpcode).size()!=0){//如果存在该物资
//			spcode = firmSupplyService.findFirmSupply(positnSpcode).get(0);
//		}
		
//		if(positnSpcode.getSp_code().split(",").length>0){//批量操作
//			spcode.setSp_code(positnSpcode.getSp_code());
//		}
//		if(positnSpcode.getPositn().split(",").length>0){//批量操作
//			spcode.setPositn(positnSpcode.getPositn());
//		}
//		spcode.setPositn(positnParam);
		if(positnSpcode.getSp_code().split(",").length == 1 && positnSpcode.getPositn().split(",").length == 1){
			positnSpcode = firmSupplyService.findFirmSupplyByFirmAndSpcode(positnSpcode);
		}
		modelMap.put("positnSpcode", positnSpcode);	
		return new ModelAndView(FirmSupplyConstants.UPDATE, modelMap);
	}
	
	/**
	 * 更新分店物资
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmSupplyService.updateFirmSupply(positnSpcode);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 新增物资        
	 */
	@RequestMapping(value = "/addSupplyBatch")
	public ModelAndView addPositnBatch(ModelMap modelMap, String defaultName, String defaultCode, String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("type", type);
		return new ModelAndView(FirmSupplyConstants.ADD_SUPPLY_BATCH_FIRM_ADD, modelMap);
	}
	/**
	 * 查询多条物资   左侧
	 */
	@RequestMapping(value = "/selectNSupply")
	public ModelAndView selectNSupply(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(FirmSupplyConstants.SELECT_NSUPPLY, modelMap);
	}
	/**
	 * 查询多条物资    弹出框 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNSupply")
	public ModelAndView selectTableNSupply(ModelMap modelMap, Supply supply, String level, String code, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(FirmSupplyConstants.SELECT_TABLENSUPPLY, modelMap);
	}
	
	/**
	*获取物资大类 
	*
	*/
	@RequestMapping(value = "/findDivisionSupply")
	public ModelAndView chooseSupply(ModelMap modelMap,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		return new ModelAndView(FirmSupplyConstants.DIVISIONSUPPLY, modelMap);
	}
	/**
	 * 获取物资中类
	 */
	@RequestMapping(value="/findGroupTyp")
	public ModelAndView findGroupTyp(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		return new ModelAndView(FirmSupplyConstants.GROUPSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资小
	 */
	@RequestMapping(value="/findSmallClass")
	public ModelAndView findSmallClass(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(FirmSupplyConstants.CLASSSUPPLY, modelMap);
	}
	
	/**
	 * 添加物资
	 */
	@RequestMapping("/saveByAddOnly")
	@ResponseBody
	public String saveByAddOnly(HttpSession session, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return firmSupplyService.saveByAddOnly(positnSpcode);
	}
	/**
	 * 添加物资
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String save(HttpSession session, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return firmSupplyService.saveSupply(positnSpcode);
	}
	
	/**
	 * 删除物资
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(HttpSession session, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmSupplyService.deleteFirmSupply(positnSpcode);
	}
	
	/**
	 * 选择分店
	 */
	@RequestMapping("/copy")
	public ModelAndView copy(ModelMap modelMap, PositnSpcode positnSpcode, Positn positn, String positnCode, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (positnSpcode.getPositn()==null||"".equals(positnSpcode.getPositn())) {
			modelMap.put("positnCode", positnCode);
		}else{
			modelMap.put("positnCode", positnSpcode.getPositn());
		}
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());//分店类型
//		positn.setTyp("1203");//只查询分店
		positn.setTypn("'1201','1202','1203'");//查询主直拨库、1202、1203
		modelMap.put("listPositn", positnService.findPositnSuper(positn, page));
		modelMap.put("pageobj", page);
		modelMap.put("queryPositn", positn);
		return new ModelAndView(FirmSupplyConstants.SELECT_POSITN_F, modelMap);
	}
	
	
	/**
	 * 添加物资--复制单条或者多条物资
	 * @param modelMap
	 * @param ids --物资编码集合
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/copySupply")
	public ModelAndView copySupply(ModelMap modelMap, PositnSpcode positnSpcode, Positn positn, String positnCode, String ids, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (positnSpcode.getPositn()==null||"".equals(positnSpcode.getPositn())) {
			modelMap.put("positnCode", positnCode);
		}else{
			modelMap.put("positnCode", positnSpcode.getPositn());
		}
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());//分店类型
		modelMap.put("listPositn", positnService.findPositn(positn, page));
		modelMap.put("positn", positn);
		modelMap.put("ids", ids);
		modelMap.put("pageobj", page);
		return new ModelAndView(FirmSupplyConstants.SELECT_POSITN_D, modelMap);
	}
	
//	/**
//	 * 添加物资
//	 */
//	@RequestMapping("/saveByCopy")
//	public ModelAndView saveByCopy(ModelMap modelMap, String positnCode, PositnSpcode positnSpcode) throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		firmSupplyService.saveByCopy(positnCode, positnSpcode);
//		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
//	}
	
	/**
	 * 添加物资
	 */
	@RequestMapping("/saveByCopySupply")
	@ResponseBody
	public String saveByCopySupply(ModelMap modelMap, String positnCode,String ids, PositnSpcode positnSpcode,String code, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return firmSupplyService.saveByCopySupply(positnCode, positnSpcode,code,ids,acct);
	}
	
	
	/**
	 * 添加物资
	 */
	@RequestMapping("/saveByCopy")
	public ModelAndView saveByCopy(ModelMap modelMap, HttpSession session,String positnCode, PositnSpcode positnSpcode,String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		code = code.substring(0, code.length()-1);

		if(positnCode.indexOf(",")>-1){
			positnCode = positnCode.split(",")[1];
		}
		firmSupplyService.saveByCopy(positnCode, positnSpcode,code,acct);
		
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 根据类别添加物资
	 * @param session
	 * @param accountId 
	 * @param classType 大0中1小2类
	 * @param classCode 类别编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/addSupplyBatchByByClass")
	@ResponseBody
	public Object addSupplyBatchByByClass( HttpSession session,String positnCode,String classType,String classCode) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源

		try {
			String acct = session.getAttribute("ChoiceAcct").toString();
			List<Supply> listSupply = new ArrayList<Supply>();
			if("0".equals(classType)){
				listSupply = grpTypService.findGrp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else if("1".equals(classType)){//中类
				listSupply = grpTypService.findTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else{//小类
				listSupply = grpTypService.findSupplyByTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}
			firmSupplyService.saveFirmSupply(positnCode, listSupply, acct);
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
	}

	/**
	 * 根据类别添加物资
	 * @param session
	 * @param classType 大0中1小2类
	 * @param classCode 类别编码
	 * @param positnCode 分店编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveSupplyByClass")
	@ResponseBody
	public Object saveSupplyByClass( HttpSession session,String classType,String classCode,String positnCode) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Date beg = new Date();
		try {
			List<Supply> listSupply = new ArrayList<Supply>();
			if("0".equals(classType)){
				listSupply = grpTypService.findGrp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else if("1".equals(classType)){//中类
				listSupply = grpTypService.findTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else{//小类
				listSupply = grpTypService.findSupplyByTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}
			firmSupplyService.saveSupplyByClass(listSupply,positnCode);
			Date end = new Date();
			System.out.println("*******************************"+(end.getTime() -beg.getTime())/1000);
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
	}
	
	
	/**
	 * 导出分店物资属性  EXCEL  wangjie 2014年11月21日 16:51:37
	 * @param response
	 * @param session
	 * @param uncheckeditem 未选择的物资序号
	 * @param productcnt  物资调整量
	 * @param productmemo 物资备注
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmSupply")
	@ResponseBody
	public boolean exportFirmSupply(HttpServletResponse response,HttpServletRequest request,
			ModelMap modelMap,PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		Map<String, List<PositnSpcode>> map = new HashMap<String, List<PositnSpcode>>();//存放所有分店的 所有物资
		//遍历选择的分店
		String[] positnAttr = positnSpcode.getPositn().split(",");
		for(String positn:positnAttr){
			//分店物资属性列表
			positnSpcode.setPositn(positn);
			List<PositnSpcode> positnList = firmSupplyService.findFirmSupply(positnSpcode);
			Positn posi = new Positn();
			posi.setCode(positn);
			posi.setTypn("'1201','1202','1203'");
			List<Positn> postn = positnService.findPositnSuperNOPage(posi);
			if(postn!=null && postn.size()!=0){
				map.put(postn.get(0).getDes(), positnList);
			}
		}
		
		String fileName = "分店物资属性";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		return firmSupplyService.exportFirmSupply(response.getOutputStream(),map,null);
//		return false;
	}
	
	/***
	 * 保存配送单位
	 * @param session
	 * @param positnSpcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/updateDisunit")
	@ResponseBody
	public int updateDisunit(HttpSession session, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return firmSupplyService.updateDisunit(positnSpcode);
	}
	
	/***
	 * 批量复制配送单位
	 * @param session
	 * @param positnSpcode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/updateDisunitBatch")
	@ResponseBody
	public int updateDisunitBatch(HttpSession session, PositnSpcode positnSpcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return firmSupplyService.updateDisunitBatch(positnSpcode);
	}
}
