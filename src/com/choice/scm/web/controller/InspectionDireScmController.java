package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.InspectionDireScmConstants;
import com.choice.scm.domain.Arrivald;
import com.choice.scm.domain.Arrivalm;
import com.choice.scm.service.DeliverPlatService;

/***
 * 九毛九直配验货Controller
 * @author wjf
 *
 */
@Controller
@RequestMapping(value = "inspectionDireScm")
public class InspectionDireScmController {

	@Autowired
	private DeliverPlatService deliverPlatService;
	
	@Autowired
	private LogsMapper logsMapper;
	
	/*********************************************九毛九按单据验货start***************************************************/
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param Arrivalm
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listArrivalm")
	public ModelAndView listArrivalm(ModelMap modelMap, HttpSession session, Arrivalm arrivalm,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == arrivalm.getBdate() || null == arrivalm.getEdate()){
			arrivalm.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			arrivalm.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		if(arrivalm.getSelectType() == 0){//按单据
			List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm,page);
			modelMap.put("arrivalmList", amList);
			modelMap.put("arrivalm", arrivalm);
			modelMap.put("pageobj", page);
			return new ModelAndView(InspectionDireScmConstants.LIST_ARRIVALM, modelMap);
		}else if(arrivalm.getSelectType() == 1){//按物资
			List<Arrivald> amList = deliverPlatService.findArrivalmListSupply(arrivalm,page);
			modelMap.put("arrivalmList", amList);
			modelMap.put("arrivalm", arrivalm);
			modelMap.put("pageobj", page);
			return new ModelAndView(InspectionDireScmConstants.LIST_ARRIVALM_SUPPLY, modelMap);
		}else{//按日期
			List<Arrivald> amList = deliverPlatService.findArrivalmListDate(arrivalm,page);
			modelMap.put("arrivalmList", amList);
			modelMap.put("arrivalm", arrivalm);
			modelMap.put("pageobj", page);
			return new ModelAndView(InspectionDireScmConstants.LIST_ARRIVALM_DATE, modelMap);
		}
	}
	
	/***
	 * 查询九毛九直配到货单
	 * @param modelMap
	 * @param session
	 * @param Arrivalm
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findArrivald")
	public ModelAndView findArrivald(ModelMap modelMap, HttpSession session, Arrivalm arrivalm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm);
		List<Arrivald> adList = new ArrayList<Arrivald>();
		if(amList.size() != 0){
			arrivalm = amList.get(0);
			adList = deliverPlatService.findArrivaldList(arrivalm);
			if(null == arrivalm.getMaded())//null说明没验货的,入库日期默认等于到货日期
				arrivalm.setMaded(DateFormat.getDateByString(arrivalm.getDarrbilldate(), "yyyy-MM-dd"));
		}
		modelMap.put("arrivalm", arrivalm);
		modelMap.put("arrivaldList", adList);
		return new ModelAndView(InspectionDireScmConstants.LIST_ARRIVALD,modelMap);
	}
	
	/***
	 * 九毛九直配验货生成入库单
	 * @param modelMap
	 * @param session
	 * @param arrivalm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkArrivalm.do")
	public ModelAndView checkArrivalm(ModelMap modelMap,HttpSession session,Arrivalm arrivalm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"九毛九总部直配验货,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		arrivalm.setIstate(2);//供应商已确认
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm);
		List<Arrivald> adList = new ArrayList<Arrivald>();
		try{
			if(amList.size() != 0){
				Date maded = arrivalm.getMaded() == null ? new Date() : arrivalm.getMaded();
				String acct = session.getAttribute("ChoiceAcct").toString();
				String accountName = session.getAttribute("accountName").toString();//当前用户
				arrivalm = amList.get(0);
				arrivalm.setMaded(maded);
				arrivalm.setAcct(acct);
				arrivalm.setMadeby(accountName);
				adList = deliverPlatService.findArrivaldList(arrivalm);
				//1.先更新到货单主表的状态 防止有人同时验收这张入库单，如果有异常 ，回滚的时候将状态更新回来
				arrivalm.setIstate(3);//已验货
				deliverPlatService.updateArrivalm2(arrivalm);
				String pr = deliverPlatService.checkArrivalm(arrivalm,adList);
				arrivalm.setPr(pr);
			}else{
				arrivalm.setPr("0");//数据为空 已经验货
			}
		}catch(Exception e){
			arrivalm.setPr("-1");//审核失败
			arrivalm.setIstate(2);//供应商已确认
			deliverPlatService.updateArrivalm2(arrivalm);
		}
		modelMap.put("arrivalm", arrivalm);
		modelMap.put("arrivaldList", adList);
		return new ModelAndView(InspectionDireScmConstants.LIST_ARRIVALD,modelMap);
	}
	
	/***
	 * 九毛九直配验货生成入库单 批量
	 * @param session
	 * @param arrivalm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkArrivalms")
	@ResponseBody
	public String checkArrivalms(HttpSession session,String pk_arrivals) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"九毛九总部直配批量验货,生成门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Map<String,String> result = new HashMap<String,String>();
		try{
			Arrivalm arrivalm = new Arrivalm();
			arrivalm.setPk_arrivals(pk_arrivals);//批量的主键
			arrivalm.setIstate(2);//供应商已确认
			List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm);
			if(amList.size() != 0){
				String acct = session.getAttribute("ChoiceAcct").toString();
				String accountName = session.getAttribute("accountName").toString();//当前用户
				for(Arrivalm am : amList){
					am.setAcct(acct);
					am.setMadeby(accountName);
					am.setMaded(DateFormat.getDateByString(am.getDarrbilldate(),"yyyy-MM-dd"));//入库日期默认等于到货日期
					List<Arrivald> adList = deliverPlatService.findArrivaldList(am);
					//1.先更新到货单主表的状态 防止有人同时验收这张入库单，如果有异常 ，回滚的时候将状态更新回来
					am.setIstate(3);//已验货
					deliverPlatService.updateArrivalm2(am);
					try{
						deliverPlatService.checkArrivalm(am,adList);
					}catch(Exception e){
						am.setIstate(2);//未验货
						deliverPlatService.updateArrivalm2(am);
						throw new Exception(e.getMessage());
					}
				}
				result.put("pr", "1");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}else{
				result.put("pr", "0");
				result.put("msg", "没有要验收的状态为“供应商已确认”的单据，请确认。");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}
		}catch(Exception e){
			result.put("pr", "-1");
			result.put("msg", e.getMessage());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}
	}
	
	/***
	 * 进入发送邮件界面
	 * @param modelMap
	 * @param session
	 * @param Arrivalm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toSendEmail")
	public ModelAndView toSendEmail(ModelMap modelMap, HttpSession session, Arrivalm arrivalm, int flag) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH九毛九直配进入发送邮件界面",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm);
		List<Arrivald> adList = new ArrayList<Arrivald>();
		if(amList.size() != 0){
			arrivalm = amList.get(0);
			adList = deliverPlatService.findArrivaldList(arrivalm);
		}
		modelMap.put("arrivalm", arrivalm);
		modelMap.put("arrivaldList", adList);
		modelMap.put("flag", flag);
		return new ModelAndView(InspectionDireScmConstants.SEND_EMAIL,modelMap);
	}
	
	
	/***
	 * 九毛九直配验货修改判断
	 * @param modelMap
	 * @param session
	 * @param disList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkState")
	@ResponseBody
	public String checkState(ModelMap modelMap, HttpSession session,Arrivalm arrivalm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH查询九毛九直配到货单状态",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		List<Arrivalm> amList = deliverPlatService.findArrivalmList(arrivalm);
		if(amList.size() != 0){
			arrivalm = amList.get(0);
		}
		return arrivalm.getIstate()+"";
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportArrivalm")
	@ResponseBody
	public void exportArrivalm(HttpServletResponse response,HttpServletRequest request,HttpSession session,Arrivalm arrivalm, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		page.setPageSize(Integer.MAX_VALUE);
		String fileName = "直配验收数据表";
		List<Arrivald> amList = null;
		if(arrivalm.getSelectType() == 1){//按物资
			fileName = "直配验收数据按物资汇总表";
			amList = deliverPlatService.findArrivalmListSupply(arrivalm,page);
		}else if(arrivalm.getSelectType() == 2){//按日期
			fileName = "直配验收数据按日期汇总表";
			amList = deliverPlatService.findArrivalmListDate(arrivalm,page);
		}
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		if(arrivalm.getSelectType() == 1){//按物资
			deliverPlatService.exportArrivalm("直配验收数据按物资汇总表", response.getOutputStream(), amList);
		}else if(arrivalm.getSelectType() == 2){//按日期
			deliverPlatService.exportArrivalm2("直配验收数据按日期汇总表", response.getOutputStream(), amList);
		}
	}
	
	
}
