package com.choice.scm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.MdYanhuoChaxunService;

/**
 * 门店验货查询
 * 
 * @author jinshuai
 * 20160316
 * 
 */
@Controller
@RequestMapping(value = "MdYanhuoChaxun")
public class MdYanhuoChaxunController {

	@Autowired
	private MdYanhuoChaxunService MdYanhuoChaxunService;

	/**
	 * 转到门店验货查询页面
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("toMdYanhuoChaxun")
	public ModelAndView toMdYanhuoChaxun(ModelMap modelMap) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(SupplyAcctConstants.MDYANHUO_REPORT, modelMap);
	}

	/**
	 * 根据时间拼装列
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findFreetimeHeader")
	@ResponseBody
	public Object findFreetimeHeader(ModelMap modelMap, SupplyAcct supplyAcct) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		//返回List集合	用于生成动态表头
		return MdYanhuoChaxunService.findFreetimeHeader(supplyAcct);
	}

	/**
	 * 查询数据用于前台显示
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findYanhuoChaxunDatas")
	@ResponseBody
	public Object findYanhuoChaxunDatas(ModelMap modelMap, Page pager, SupplyAcct supplyAcct) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		//返回数据	前台展示
		return MdYanhuoChaxunService.findYanhuoChaxunDatas(supplyAcct, pager);
	}

}
