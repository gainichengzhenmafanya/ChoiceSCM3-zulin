package com.choice.scm.web.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.scm.constants.BanChengPinPriceConstants;
import com.choice.scm.domain.FirmCostAvg;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.SpCodeExPrice;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.service.BanChengPinPriceService;
import com.choice.scm.service.PrdctbomService;

@Controller
@RequestMapping("BanChengPinPrice")
public class BanChengPinPriceController {
	@Autowired
	private BanChengPinPriceService banChengPinPriceService;
	@Autowired
	private PrdctbomService prdctbomService;
	
	/**
	 * 半成品价格 跳转
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list() throws Exception{
		return new ModelAndView(BanChengPinPriceConstants.LIST_BANCHENGPIN);
	}
	/**
	 * 半成品价格 显示
	 * @throws Exception
	 */
	@RequestMapping(value = "/findData")
	public ModelAndView findData(HttpSession session, ModelMap modelMap, int typ, String month) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String url=null;
		if(month==null || "".equals(month)){
			Calendar cal = Calendar.getInstance();
			month = cal.get(Calendar.MONTH )+1+"";
		}
		switch(typ){
			case 1://中心费用 查询
				FirmCostAvg firmCostAvg = new FirmCostAvg();
				firmCostAvg.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
				firmCostAvg.setMonthh(month);
				firmCostAvg.setAcct(session.getAttribute("ChoiceAcct").toString());
				List<FirmCostAvg> list=banChengPinPriceService.findFirmCostAvg(firmCostAvg);
				modelMap.put("firmCostAvgList",list);
				modelMap.put("allAmt",list.get(0).getAllAmt());
				modelMap.put("month",month);
				url=BanChengPinPriceConstants.LIST_FIRMCOSTAVG;
				break;
			case 2://加工工时 查询
				SpCodeExPrice spCodeExPrice = new SpCodeExPrice();
				spCodeExPrice.setAcct(session.getAttribute("ChoiceAcct").toString());
				spCodeExPrice.setMonthh(month);
				spCodeExPrice.setYearr(DateFormat.getStringByDate(new Date(), "yyyy"));
				modelMap.put("spCodeExPriceList", banChengPinPriceService.findSpCodeExPrice(spCodeExPrice));
				modelMap.put("month", month);
				url=BanChengPinPriceConstants.LIST_EXTIM;
				break;
			case 3://半成品费用 查询
				spCodeExPrice = new SpCodeExPrice();
				spCodeExPrice.setMonthh(month);
				spCodeExPrice.setYearr(DateFormat.getStringByDate(new Date(), "yyyy"));
				modelMap.put("spCodeExPriceList", banChengPinPriceService.findSpCodeExPrice(spCodeExPrice));
				//modelMap.put("firmSpCodeExPrice", banChengPinPriceService.findSpCodeExPrice(firmCostAvg));
				modelMap.put("month", month);
				url=BanChengPinPriceConstants.LIST_SPCODEEXPRICE;
				break;
		}
		return new ModelAndView(url,modelMap);
	}
	/**
	 * 半成品价格计算 显示NEW
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDataNew")
	public ModelAndView findDataNew(HttpSession session, ModelMap modelMap, int typ, String month) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String url=null;
		if(month==null || "".equals(month)){
			Calendar cal = Calendar.getInstance();
			month = cal.get(Calendar.MONTH )+1+"";
		}
		switch(typ){
			case 1://中心费用 查询
				FirmCostAvg firmCostAvg = new FirmCostAvg();
				firmCostAvg.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
				firmCostAvg.setMonthh(month);
				firmCostAvg.setAcct(session.getAttribute("ChoiceAcct").toString());
				List<FirmCostAvg> list=banChengPinPriceService.findFirmCostAvg(firmCostAvg);
				modelMap.put("firmCostAvgList",list);
				modelMap.put("allAmt",list.get(0).getAllAmt());
				modelMap.put("month",month);
				url=BanChengPinPriceConstants.LIST_FIRMCOSTAVGNEW;
				break;
			case 2://加工工时 查询
				SpCodeExPrice spCodeExPrice = new SpCodeExPrice();
				spCodeExPrice.setAcct(session.getAttribute("ChoiceAcct").toString());
				spCodeExPrice.setMonthh(month);
				spCodeExPrice.setYearr(DateFormat.getStringByDate(new Date(), "yyyy"));
				modelMap.put("spCodeExPriceList", banChengPinPriceService.findSpCodeExPrice(spCodeExPrice));
				modelMap.put("month", month);
				url=BanChengPinPriceConstants.LIST_EXTIMNEW;
				break;
			case 3://半成品费用 查询
				spCodeExPrice = new SpCodeExPrice();
				spCodeExPrice.setMonthh(month);
				spCodeExPrice.setYearr(DateFormat.getStringByDate(new Date(), "yyyy"));
				modelMap.put("spCodeExPriceList", banChengPinPriceService.findSpCodeExPrice(spCodeExPrice));
				//modelMap.put("firmSpCodeExPrice", banChengPinPriceService.findSpCodeExPrice(firmCostAvg));
				modelMap.put("month", month);
				url=BanChengPinPriceConstants.LIST_SPCODEEXPRICENEW;
				break;
		}
		return new ModelAndView(url,modelMap);
	}
	/**
	 *  保存  中心费用
	 * @throws Exception
	 */
	@RequestMapping(value = "/savefirmCostAvg")
	@ResponseBody
	public String savefirmCostAvg(HttpSession session,FirmCostAvg firmCostAvg) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return banChengPinPriceService.savefirmCpstAcg(session,firmCostAvg);
	}
	
	/**
	 * 保存  加工工时
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveExTim")
	@ResponseBody
	public String saveExTim(HttpSession session,SpCodeExPrice spCodeExPriceList) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return banChengPinPriceService.saveExTim(acct, spCodeExPriceList.getSpCodeExPriceList());
	}
	
	/**
	 * 根据item查询原材料
	 * @param modelMap
	 * @param item
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findProductList")
	@ResponseBody
	public List<Product> findProductList(ModelMap modelMap,String item,String monthh, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Spcodeexm spcodeexm = new Spcodeexm();
		spcodeexm.setItem(item);
		spcodeexm.setAcct(session.getAttribute("ChoiceAcct").toString());
		spcodeexm.setMonthh(monthh);
		spcodeexm.setYearr(DateFormat.getStringByDate(new Date(), "yyyy"));
		return prdctbomService.findmaterial(spcodeexm);
	}
	
	/**
	 * 根据item查询费用
	 * @param modelMap
	 * @param item
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSpexfeicostList")
	@ResponseBody
	public List<FirmCostAvg> findSpexfeicostList(ModelMap modelMap, FirmCostAvg firmCostAvg, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmCostAvg.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		firmCostAvg.setAcct(session.getAttribute("ChoiceAcct").toString());
		return banChengPinPriceService.findSpexfeicost(firmCostAvg);
	}
	
	/**
	 * 根据item查询费用
	 * @param modelMap
	 * @param item
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/calculate")
	@ResponseBody
	public String calculate(ModelMap modelMap, String month, String extimSum, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return banChengPinPriceService.updatecalculate(acct, month, extimSum);
	}
}
