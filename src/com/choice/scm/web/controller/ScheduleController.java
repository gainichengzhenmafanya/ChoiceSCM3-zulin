package com.choice.scm.web.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Account;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScheduleConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Schedule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.ScheduleStore;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.ScheduleService;

/**
 * 配送班表
 *
 */
@Controller
@RequestMapping("schedule")
public class ScheduleController {

	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private PositnService positnService;
	@Autowired
	CodeDesService codeDesService;
	
	/**
	 * 查询配送班表 list
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findSchedule(HttpSession session, ModelMap modelMap, Schedule schedule,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = new Positn();
		positn.setTyp("1203");//配送班表 只关联1203
		positn.setCwqx("1");
		positn.setAccountId(session.getAttribute("accountId").toString());
		String accountId = session.getAttribute("accountId").toString();
		Account account = new Account();
		account.setId(accountId);
		//查询当前账号是否是系统管理员角色
		if (scheduleService.findAccount(account).size()==0) {
			schedule.setAccountId(accountId);
		}
		modelMap.put("positnList", positnService.findAllPositn(positn));//仓位
		modelMap.put("scheduleList", scheduleService.findSchedule(schedule));
		modelMap.put("schedule", schedule);
		return new ModelAndView(ScheduleConstants.LIST_SCHEDULE,modelMap);
	}

	/**
	 *  添加 左边 配送班主表
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/saveSchedule")
	public ModelAndView saveSchedule(Schedule schedule) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(schedule.getScheduleName()!=null){
			scheduleService.saveSchedule(schedule);
			return new ModelAndView(StringConstant.ACTION_DONE);
		}
		return new ModelAndView(ScheduleConstants.ADD_SCHEDULE);
	}
	/**
	 *  修改配送路线 跳转
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, Schedule schedule) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("Schedule", scheduleService.findScheduleById(schedule));
		return new ModelAndView(ScheduleConstants.UPDATE_SCHEDULE,modelMap);
	}
	
	/**
	 *  修改配送路线
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/updateSchedule")
	public ModelAndView updateSchedule(Schedule schedule) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.updateSchedule(schedule);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 *  刪除 左边 配送路线
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/deleteSchedule")
	public ModelAndView deleteSchedule(Schedule schedule) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.deleteSchedule(schedule);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	//---------------------右边--------------------------------
	/**
	 * 配送班表-门店
	 * @throws Exception
	 */
	@RequestMapping("/findFirmByScheduleId")
	public ModelAndView findFirmByScheduleId(ModelMap modelMap, String scheduleId, Positn positn, Page page,String cwqx,HttpSession session,String single) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//查询区域
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		
		//查询已经关联的仓位
		List<ScheduleStore> list = scheduleService.findFirmByScheduleId(scheduleId);
		String code = "";
		for(ScheduleStore scheduleStore:list){
			code +=scheduleStore.getsCode()+",";
		}
		positn.setCode(code);
		modelMap.put("listPositn1", positnService.selectPositnNew(positn));
		
		//根据页面传的仓位类型 查询所有仓位
		positn.setAccountId(session.getAttribute("accountId").toString());
		String locale = session.getAttribute("locale").toString();//语言类型
		positn.setLocale(locale);
		//清空positn中的code值，查询所有仓位
		positn.setCode("");
		positn.setTypn(CodeHelper.replaceCode(positn.getTypn()));
		modelMap.put("listPositn", positnService.findPositnSuperNOPage(positn));
		
		//获取仓位类型
		CodeDes codedes = new CodeDes();
		codedes.setTyp("12");
		codedes.setLocale(locale);
		
		if(positn.getTypn() != null && !"".equals(positn.getTypn())){
			codedes.setCode(positn.getTypn());
		}
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(codedes));
		modelMap.put("scheduleId", scheduleId);
		modelMap.put("positn", positn);
		modelMap.put("single", single);
		return new ModelAndView(ScheduleConstants.LIST_POSITN_SCHEDULE,modelMap);
	}
	
	/**
	 * 查询门店包含那些配送班表(跳转)
	 * @param modelMap
	 * @param scheduleId
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findScheduleByPositn")
	public ModelAndView findScheduleByPositn(ModelMap modelMap,Positn positn, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(positn.getTypn()!=null && "4".equals(positn.getTypn())){
			positn.setTypn("'1203'");
		}
		modelMap.put("listPositn", positnService.findPositnSuper(positn,page));
		//经营区域
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("3");
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		modelMap.put("positn", positn);
		return new ModelAndView(ScheduleConstants.LIST_SCHEDULE_POSITN,modelMap);
	}
	
	/**
	 * 查询1203的班表
	 * @param modelMap
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toListFirmSchedule")
	public ModelAndView toListFirmSchedule(ModelMap modelMap,Page page,Schedule schedule) throws Exception{
		modelMap.put("scheduleList", scheduleService.findFirmScheduleList(schedule, page));
		modelMap.put("pageobj", page);
		modelMap.put("schedule", schedule);
		return new ModelAndView(ScheduleConstants.SHOW_FIRM_SCHEDULE,modelMap);
	}
	
	/**
	 * 删除1203班表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteFirmSchedule")
	public ModelAndView deleteFirmSchedule(ScheduleStore schedule) throws Exception{
		scheduleService.deleteFirmSchedule(schedule);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 * 保存账号 仓位范围
	 * @throws Exception
	 */
	@RequestMapping("/saveFirmByScheduleId")
	public ModelAndView savePositnAccount(ModelMap modelMap, String scheduleId, String positnIds) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listPositnId=Arrays.asList(positnIds.split(","));
		scheduleService.saveFirmByScheduleId(scheduleId, listPositnId);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
//	/**
//	 * 配送班明细表
//	 * @throws Exception
//	 */
//	@RequestMapping("/findDetailsByDay")
//	@ResponseBody
//	public Object findDetailsByDay(ModelMap modelMap, ScheduleD scheduleD) throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		return scheduleService.findDetailsByDay(scheduleD);
//	}
	
	/**
	 * 配送班明细表(按天)
	 * @throws Exception
	 */
	@RequestMapping("/findScheduleByMonth")
	@ResponseBody
	public Object findScheduleByMonth(ModelMap modelMap, ScheduleD scheduleD, String scheduleMonth) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return scheduleService.findScheduleByMonth(scheduleD, scheduleMonth);
	}
	
	/**
	 * 配送班明细表（按月）
	 * @throws Exception
	 */
	@RequestMapping("/findDetailsByScheduleId")
	public ModelAndView findDetailsByScheduleId(ModelMap modelMap, String scheduleId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("scheduleDetailsList", scheduleService.findDetailsByScheduleId(scheduleId));
		modelMap.put("scheduleId", scheduleId);
		return new ModelAndView(ScheduleConstants.LIST_ACCTSCHEDULE,modelMap);
	}
	
	/**
	 *  添加  配送班表明细
	 */
	@RequestMapping(value = "/addScheduleDetails")
	public ModelAndView addScheduleDetails(ModelMap modelMap, HttpSession session, ScheduleD scheduleD) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.saveScheduleD(scheduleD);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	/**
	 *  修改明细
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
//	@RequestMapping(value = "/updateScheduleDetails")
//	public ModelAndView updateScheduleDetails(ModelMap modelMap, HttpSession session, ScheduleD scheduleD) throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		scheduleD = scheduleService.findDetails(scheduleD).get(0);
//		modelMap.put("orderDate", DateFormat.getDateByString(scheduleD.getOrderDate(), "yyyy-MM-dd"));
//		modelMap.put("receiveDate", DateFormat.getDateByString(scheduleD.getReceiveDate(), "yyyy-MM-dd"));
//		modelMap.put("ScheduleD", scheduleD);
//		return new ModelAndView(ScheduleConstants.UPDATE_DETAIL, modelMap);
//	}
	
	/**
	 *  跳转到修改配送班表 明细界面
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/updateScheduleDetails_c")
	public Object updateScheduleDetails_C(ModelMap modelMap, HttpSession session, ScheduleD scheduleD,
			String date, String days, String scheduleMonth) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String month1=scheduleMonth;//当前月
		String month2;//下月
		String month3;//下下月
//		String year=scheduleMonth.substring(0, 3);
//		int days1=0;//当前月的天数
		int days2=0;//下月的天数
		int days3=0;//下下月的天数
		if (Integer.parseInt(scheduleMonth.substring(5, 7))+1<10) {
			month2=scheduleMonth.substring(0, 5)+"0"+(Integer.parseInt(scheduleMonth.substring(5, 7))+1);
			if (Integer.parseInt(scheduleMonth.substring(5, 7))+2<10) {
				month3=scheduleMonth.substring(0, 5)+"0"+(Integer.parseInt(scheduleMonth.substring(5, 7))+2);
			}else if(Integer.parseInt(scheduleMonth.substring(5, 7))+2>12){
				month3=scheduleMonth.substring(0, 5)+"01";
			}else {
				month3=scheduleMonth.substring(0, 5)+(Integer.parseInt(scheduleMonth.substring(5, 7))+2);
			}
		}else if(Integer.parseInt(scheduleMonth.substring(5, 7))+1>12){
			month2=(Integer.parseInt(scheduleMonth.substring(0, 4))+1)+"-01";
			month3=(Integer.parseInt(scheduleMonth.substring(0, 4))+1)+"-02";
		}else {
			month2=scheduleMonth.substring(0, 5)+(Integer.parseInt(scheduleMonth.substring(5, 7))+1);
			if(Integer.parseInt(scheduleMonth.substring(5, 7))+2>12){
				month3=(Integer.parseInt(scheduleMonth.substring(0, 4))+1)+"-01";
			}else {
				month3=scheduleMonth.substring(0, 5)+(Integer.parseInt(scheduleMonth.substring(5, 7))+2);
			}
		}
//		switch (Integer.parseInt(month1.substring(5, 6))) {
//			case   1:case   3:case   5:case   7:case   8:case   10:case   12:  
//			days1 = 31;  
//			case   4:case   6:case   9:case   11:  
//			days1 = 30; 
//		}
		switch (Integer.parseInt(month2.substring(5, 7))) {
		case   1:case   3:case   5:case   7:case   8:case   10:case   12:  
		days2 = 31; break;  
		case   4:case   6:case   9:case   11:  
		days2 = 30; break;
		case 2:
			int year = Integer.parseInt(month2.substring(0, 4));
			if((year%4==0&&year%1!=0)||(year%400==0)){
				days2 = 29;
			}else{
				days2 = 28;
			}
		}
		switch (Integer.parseInt(month3.substring(5, 7))) {
			case   1:case   3:case   5:case   7:case   8:case   10:case   12:  
			days3 = 31;  break;  
			case   4:case   6:case   9:case   11:  
			days3 = 30;  break;
			case 2:
				int year = Integer.parseInt(month3.substring(0, 4));
				if((year%4==0&&year%1!=0)||(year%400==0)){//闰年
					days3 = 29;
				}else{
					days3 = 28;
				}
		}
		scheduleD = scheduleService.findDetails(scheduleD).get(0);
		modelMap.put("orderDate", DateFormat.getDateByString(scheduleD.getOrderDate(), "yyyy-MM-dd"));
		modelMap.put("receiveDate", DateFormat.getDateByString(scheduleD.getReceiveDate(), "yyyy-MM-dd"));
		modelMap.put("ScheduleD", scheduleD);
		modelMap.put("month1", month1);
		modelMap.put("month2", month2);
		modelMap.put("month3", month3);
		modelMap.put("days1", days);
		modelMap.put("days2", days2+"");
		modelMap.put("days3", days3+"");
//		modelMap.put("days", days);
//		modelMap.put("listGrpTyp", grpTypService.findAllGrpTypA(session.getAttribute("ChoiceAcct").toString()));//大类
		return new ModelAndView(ScheduleConstants.UPDATE_DETAIL_C, modelMap);
	}
		
	/**
	 *  跳转到 复制报货类别界面
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/copyScheduleDetails_c")
	public Object copyScheduleDetails_C(ModelMap modelMap, HttpSession session, ScheduleD scheduleD) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("ScheduleD", scheduleD);
		CodeDes code = new CodeDes();
		code.setTyp("11");
		List<CodeDes> list = codeDesService.findCodeDes(code);//获取所有报货类别
		List<Schedule> scheList = scheduleService.findSchedule(null);//获取所有班表
		//要复制的班表类别list
//		List<ScheduleD> scheduleList = scheduleService.findDetails(scheduleD);
//		for(CodeDes codedes:list){//遍历code表 看当前班表下的 那些类别已经被复制过  已经复制过的 不能再复制了
//			ScheduleD schedule = new ScheduleD();
//			schedule.setCategory_Code(codedes.getCode());
//			schedule.setScheduleID(scheduleD.getScheduleID());
//			List<ScheduleD> scheduleDList = scheduleService.findDetails(schedule);
//			
//			if(scheduleDList!=null && scheduleDList.size()!=0 && scheduleService.compareSchedule(scheduleList, scheduleDList)){
//				codedes.setLocked("Y");//暂且用locked字段存放 该报货类别在是否已经被复制wangjie 2015.1.5
//			}else{
//				codedes.setLocked("N");
//			}
//		}
		modelMap.put("grptypList", list);
		modelMap.put("scheList", scheList);
		return new ModelAndView(ScheduleConstants.COPY_DETAIL_C, modelMap);
	}
	
	/**
	 *  修改配送班表明细，包括日期复制
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/saveDetailesByUpdate")
	public ModelAndView saveDetailesByUpdate(ScheduleD scheduleD, String ids) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.updateScheduleD(scheduleD, ids);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 *  复制报货类别下的配送班表
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/saveDetailesByCopy")
	public ModelAndView saveDetailesByCopy(ScheduleD scheduleD, String ids, String schedules) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.saveDetailesByCopy(scheduleD, ids,schedules);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 *  删除1203 右边
	 */
	@RequestMapping(value = "/deleteScheduleDetails")
	@ResponseBody
	public Object deleteScheduleDetails(ScheduleD scheduleD) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return scheduleService.deleteScheduleD(scheduleD);
	}
	
	/**
	 * 查询配送班表 list
	 * @param modelMap
	 * @param schedule
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping(value = "/listCalendar")
	public ModelAndView listCalendar(HttpSession session, ModelMap modelMap, Schedule schedule, String month, String category_Code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = new Positn();
		positn.setCwqx("1");
		positn.setAccountId(session.getAttribute("accountId").toString());
		String accountId = session.getAttribute("accountId").toString();
		if (!"93bce7e4a7884946bba212e9370e216f".equals(accountId)) {
			//除了admin账号，其余都是只能看到自己负责范围内的配送线路
			schedule.setAccountId(accountId);
		}
		modelMap.put("positnList", positnService.findAllPositn(positn));//仓位
		modelMap.put("scheduleList", scheduleService.findSchedule(schedule));
		schedule = scheduleService.findScheduleById(schedule);
		modelMap.put("schedule", schedule);
		modelMap.put("month", month);
		modelMap.put("category_Code", category_Code);
		CodeDes code = new CodeDes();
		code.setTyp("11");
		modelMap.put("grptypList", codeDesService.findCodeDes(code));
		return new ModelAndView(ScheduleConstants.LIST_CALENDAR,modelMap);
	}
	
	/**
	 *  添加 右边 配送班表明细
	 */
	@RequestMapping(value = "/addSchedule")
	public ModelAndView addSchedule(ModelMap modelMap, HttpSession session, ScheduleD scheduleD, String date) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		if(scheduleD.getCategory_Code()!=null){
//			scheduleService.saveScheduleD(scheduleD);
//			return new ModelAndView(StringConstant.ACTION_DONE);
//		}
//		scheduleD.
//		modelMap.put("listGrpTyp", grpTypService.findAllGrpTypA(session.getAttribute("ChoiceAcct").toString()));//大类
//		scheduleD.setOrderDate();
		modelMap.put("scheduleD", scheduleD);
		modelMap.put("orderDate", DateFormat.getDateByString(date,"yyyy-MM-dd"));
		CodeDes code = new CodeDes();
		code.setTyp("11");
		modelMap.put("grptypList", codeDesService.findCodeDes(code));
		return new ModelAndView(ScheduleConstants.ADD_DETAIL_C, modelMap);
	}
	
	/**
	 * 批量删除配送班表     wangjie 2014年10月24日 10:30:43
	 */
	@RequestMapping(value="/batchDeleteScheduleD")
	@ResponseBody
	public Object batchDeleteScheduleD(String scheduleMonth,ScheduleD scheduleD) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return scheduleService.batchDeleteScheduleD(scheduleD,scheduleMonth);
	}
	
	/**
	 * 跳转到复制配送班表页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/copy")
	public ModelAndView copy(ModelMap modelMap,Schedule schedule) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("Schedule", scheduleService.findScheduleById(schedule));
		return new ModelAndView(ScheduleConstants.COPY_DETAIL, modelMap);
	}
	
	/**
	 * 复制配送班表
	 * @param map
	 * @param schedule
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/copySchedule")
	public ModelAndView copySchedule(ModelMap map,Schedule schedule) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		scheduleService.copySchedule(schedule);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
//	public ModelAndView 
}
