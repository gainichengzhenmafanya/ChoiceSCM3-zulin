package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.AccountSupplyTypConstants;
import com.choice.scm.domain.AccountSupplyTyp;
import com.choice.scm.service.AccountSupplyTypService;
import com.choice.scm.service.GrpTypService;


@Controller
@RequestMapping(value = "accountSupplyTyp")
public class AccountSupplyTypController {
	
	@Autowired
	private AccountSupplyTypService accountSupplyTypService;
	@Autowired
	private GrpTypService grpTypService;

	
	/**
	 * 查询此账号关联的所有物资小类
	 * @author wjf
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addSupplyTypBatch")
	public ModelAndView addSupplyTypBatch(HttpSession session,ModelMap modelMap,String accountId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		AccountSupplyTyp ast = new AccountSupplyTyp();
		ast.setAccountId(accountId);
		List<AccountSupplyTyp> astList = accountSupplyTypService.findAccountSupplyTyp(ast);
		String typcodes = "";
		for (int i=0;i<astList.size();i++) {
			if (i!=0){
				typcodes += ",";
			}
			typcodes += astList.get(i).getTypcode();
		}
		modelMap.put("defaultCode", typcodes);
		modelMap.put("accountId", accountId);
		return new ModelAndView(AccountSupplyTypConstants.ADD_SUPPLYTYPBATCH, modelMap);
	}
	
	/**
	 * 修改账户具有的物资小类
	 * @param accountSupply
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAccountSupplyTyp")
	@ResponseBody
	public String updateAccountSupplyTyp(AccountSupplyTyp accountSupplyTyp){
		try{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源		
			accountSupplyTypService.saveAccountSupplyTyp(accountSupplyTyp);
			return "0";
		}catch(Exception e){
			return "1";
		}
	}
}

