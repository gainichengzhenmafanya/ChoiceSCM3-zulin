package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmMisConstants;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.domain.Main;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.FirmMisService;
import com.choice.scm.service.PrdPrcCostManageService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 成本管理模块：物资成本差异、毛利查询、应产率分析、核减明细、每日差异报告、每日差异对比；
 * 特殊操作模块：分店月结、年结转、月末结转；
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "firmMis")
public class FirmMisController2 {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private FirmMisService firmMisService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private PrdPrcCostManageService prdPrcCostManageService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctService acctService;
	
	/****************************************************物资成本差异 start***************************************************/
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findCostVarianceHeaders")
	@ResponseBody	
	public Object getCostVarianceHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_COST_VARIANCE_FROZEN);
		return columns;
	}
	/**
	 * 跳转到分店部门物资进出明细(成本差异)页面
	 */
	@RequestMapping("/toCostVariance")
	public ModelAndView toCostVariance(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		return new ModelAndView(FirmMisConstants.LIST_COST_VARIANCE,modelMap);
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseCostVariance")
	public ModelAndView toColChooseCostVariance(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(FirmMisConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询物资仓库进出表
	 */
	@RequestMapping("/findCostVariance")
	@ResponseBody
	public Object findCostVariance(ModelMap modelMap,HttpSession session,String page,String rows,String firmCode,String firmDept,String sp_code,Date bdat,Date edat,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null==accountPositn && null==accountPositn.getPositn()){
            return null;
		}
        String positnCode=accountPositn.getPositn().getCode();
        content.put("firm", positnCode);
        supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("month", DateFormat.getMonth(bdat));
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("sp_code", sp_code);
		content.put("supplyAcct",supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			content.put("SUPPLYACCT", "SUPPLYACCT");
			content.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			content.put("POSITNSUPPLY", "POSITNSUPPLY");
		}else{
			content.put("SUPPLYACCT", "firmSUPPLYACCT");
			content.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			content.put("POSITNSUPPLY", "FIRMPOSITNSUPPLY");
		}
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return prdPrcCostManageService.findCostVariance(content, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCostVariance")
	@ResponseBody
	public void exportCostVariance(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,String firmCode,String firmDept,String sp_code,Date bdat,Date edat,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资成本差异";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			condition.put("firm", positnCode);
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("month", DateFormat.getMonth(bdat));
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		condition.put("sp_code", sp_code);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findCostVariance(condition,pager).getRows(), "分店部门物资进出明细", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));	
	}	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printCostVariance")
	public ModelAndView printCostVariance(ModelMap modelMap,Page pager,HttpSession session,String type,String page,String rows,String firmCode,String firmDept,String sp_code,Date bdat,Date edat,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	 	String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			condition.put("firm", positnCode);
			params.put("firm", positnCode);
			parameters.put("firm", positnCode);
		}
		condition.put("month", DateFormat.getMonth(bdat));
		params.put("month", DateFormat.getMonth(bdat));
		parameters.put("month", DateFormat.getMonth(bdat));
		condition.put("bdat", bdat);
		params.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));//wjf
		params.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		condition.put("edat", edat);
		parameters.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));//wjf
		parameters.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		condition.put("sp_code", sp_code);
		params.put("sp_code", sp_code);
		parameters.put("sp_code", sp_code);
		condition.put("supplyAcct", supplyAcct);
		params.put("supplyAcct",supplyAcct);
		parameters.put("supplyAcct", supplyAcct);
		modelMap.put("List",prdPrcCostManageService.findCostVariance(condition, pager).getRows());
	    parameters.put("report_name", "分店部门物资进出明细");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printCostVariance.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_COST_VARIANCE,PrdPrcCMConstant.REPORT_EXP_URL_COST_VARIANCE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}			
	/****************************************************物资成本差异 end***************************************************/

	/****************************************************毛利查询 start***************************************************/
	/**
	 * 毛利查询
	 */
	@RequestMapping(value = "/toGrossProfitList")
	public ModelAndView toGrossProfitList(ModelMap modelMap,HttpSession session,Page page,Date bdat,Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(FirmMisConstants.LIST_GROSS_PROFIT,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findGrossProfitHeaders")
	@ResponseBody
	public Object findGrossProfitHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(FirmMisConstants.TABLE_NAME_MAOLI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_MAOLI));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_MAOLI_FROZEN);
		return columns;
	}
	/**
	 * 毛利查询
	 */
	@RequestMapping("/findGrossProfit")
	@ResponseBody
	public Object findGrossProfit(ModelMap modelMap,HttpSession session,String page,String rows,String bdat,String edat,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			content.put("positnCode", positnCode);
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("supplyAcct",supplyAcct);
		pager.setNowPage(page==""||page==null ? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return firmMisService.findGrossProfit(content, pager);
	}
	
	/**
	 * 导出
	 */
	@RequestMapping("/exportGrossProfit")
	@ResponseBody
	public void exportGrossProfit(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "毛利查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			condition.put("positnCode", positnCode);
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(FirmMisConstants.TABLE_NAME_MAOLI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisService.findGrossProfit(condition,pager).getRows(), "毛利查询", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_MAOLI));	
	}
	/**
	 * 打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printGrossProfit")
	public ModelAndView printGrossProfit(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			condition.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			condition.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			params.put("positnCode",positnCode);
			condition.put("positnCode",positnCode);
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisService.findGrossProfit(condition, pager).getRows());
	    parameters.put("report_name", "毛利查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printGrossProfit.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_GROSS_PROFIT,FirmMisConstants.REPORT_PRINT_URL_GROSS_PROFIT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/****************************************************毛利查询 end***************************************************/
	
	/****************************************************应产率分析 start***************************************************/
	/**
	 * 应产率分析
	 */
	@RequestMapping(value = "/toShouldYield")
	public ModelAndView findShouldYield(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			Positn positn=new Positn();
			String positnCode=accountPositn.getPositn().getCode();
			String positnDes=accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
		}
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(FirmMisConstants.LIST_SHOULD_YIELD,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findShouldYieldAnaHeaders")
	@ResponseBody
	public Object findShouldYieldAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_SHOULDYIEDLANA));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_SHOULDYIEDLANA_FROZEN);
		return columns;
	}	
	/**
	 * 查询应产率
	 */
	@RequestMapping("/findShouldYieldAna")
	@ResponseBody
	public Object findShouldYieldAna(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,String jingPinBox,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			Positn positn=new Positn();
			String positnCode=accountPositn.getPositn().getCode();
			String positnDes=accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
		}
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return prdPrcCostManageService.findShouldYieldAna(condition,pager);
	}	
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printShouldYieldAna")
	public ModelAndView printShouldYieldAna(ModelMap modelMap,Page pager,HttpSession session,String type,String sort,String order,Date inbdat,Date inedat,Date lybdat,Date lyedat,String byly,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getBdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("grpdes", supplyAcct.getGrpdes());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("inbdat", DateFormat.formatDate(inbdat, "yyyy-MM-dd"));
		condition.put("inedat", DateFormat.formatDate(inedat, "yyyy-MM-dd"));
		condition.put("lybdat", DateFormat.formatDate(lybdat, "yyyy-MM-dd"));
		condition.put("lyedat", DateFormat.formatDate(lyedat, "yyyy-MM-dd"));
		condition.put("byly", byly);
		modelMap.put("List",prdPrcCostManageService.findShouldYieldAna(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "应产率分析");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/FendianLeibieHuizong/printPositnCategorySum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_SHOULDYIELDANA,PrdPrcCMConstant.REPORT_PRINT_URL_SHOULDYIELDANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出应产率分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportShouldYieldAna")
	@ResponseBody
	public void exportShouldYieldAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,Date inbdat,Date inedat,Date lybdat,Date lyedat,String byly,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "应产率分析";
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("inbdat", DateFormat.formatDate(inbdat, "yyyy-MM-dd"));
		condition.put("inedat", DateFormat.formatDate(inedat, "yyyy-MM-dd"));
		condition.put("lybdat", DateFormat.formatDate(lybdat, "yyyy-MM-dd"));
		condition.put("lyedat", DateFormat.formatDate(lyedat, "yyyy-MM-dd"));
		condition.put("byly", byly);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findShouldYieldAna(condition,pager).getRows(), "应产率分析", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	/****************************************************应产率分析 start***************************************************/
	
	/**************************************************每日差异报告start*********************************************/
	/**
	 * 每日差异报告
	 */
	@RequestMapping(value = "/toDayDifReport")
	public ModelAndView dayDifReport(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", FirmMisConstants.TABLE_NAME_DAYDIFREPORT);
		return new ModelAndView(FirmMisConstants.LIST_DAYDIFREPORT,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findDayDifReportHeaders")
	@ResponseBody
	public Object getdayDifReportHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(FirmMisConstants.TABLE_NAME_DAYDIFREPORT);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_DAYDIFREPORT));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_DAYDIFREPORT_FROZEN);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 */
	@RequestMapping("/toColChooseDayDifReport")
	public ModelAndView toColChooseDayDifReport(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(FirmMisConstants.TABLE_NAME_DAYDIFREPORT);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", FirmMisConstants.TABLE_NAME_DAYDIFREPORT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,FirmMisConstants.BASICINFO_REPORT_DAYDIFREPORT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(FirmMisConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询每日差异报告
	 */
	@RequestMapping("/findDayDifReport")
	@ResponseBody
	public Object findDayDifReport(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,String riPanBox,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null==accountPositn && null==accountPositn.getPositn()){
            return null;
		}
        String positnCode=accountPositn.getPositn().getCode();
        condition.put("positnCode", positnCode);
        condition.put("firmCode", positnCode);
		if(null!=supplyAcct.getBdat()){
			condition.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		}
		if(null!=supplyAcct.getEdat()){
			condition.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("yngr", riPanBox);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return firmMisService.findDayDifReport(condition,pager);
	}
	/**
	 * 导出
	 */
	@RequestMapping("/exportDayDifReport")
	@ResponseBody
	public void exportDayDifReport(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "每日差异报告";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
        if(null==accountPositn && null==accountPositn.getPositn()){
            return;
        }
        String positnCode=accountPositn.getPositn().getCode();
        condition.put("positnCode", positnCode);
        condition.put("firmCode", positnCode);
		if(null!=supplyAcct.getBdat()){
			condition.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		}
		if(null!=supplyAcct.getEdat()){
			condition.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(FirmMisConstants.TABLE_NAME_DAYDIFREPORT);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisService.findDayDifReport(condition,pager).getRows(), "每日差异报告", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_DAYDIFREPORT));	
	}
	/**
	 * 打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printDayDifReport")
	public ModelAndView printDayDifReport(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			condition.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			condition.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			params.put("positnCode",positnCode);
			condition.put("positnCode",positnCode);
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisService.findDayDifReport(condition, pager).getRows());
	    parameters.put("report_name", "每日差异报告");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printDayDifReport.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_DAYDIFREPORT,FirmMisConstants.REPORT_PRINT_URL_DAYDIFREPORT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}			
	/**************************************************每日差异报告end*********************************************/	
	
	/**************************************************每日差异对比start*********************************************/	
	/**
	 * 每日差异对比
	 */
	@RequestMapping(value = "/toDayDifCompare")
	public ModelAndView toDayDifCompare(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("pageobj", page);
		modelMap.put("columns", null);
		modelMap.put("ListBean", "123");//table体list
		return new ModelAndView(FirmMisConstants.LIST_DAYDIFCOMPARE,modelMap);
	}
	/**
	 * 每日差异查询
	 */
	@RequestMapping(value = "/findDayDifCompare")
	public ModelAndView findDayDifCompare(ModelMap modelMap,HttpSession session,Page page,String dayCheckBox,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			condition.put("positnCode", positnCode);
			condition.put("firmCode", positnCode);
		}
		condition.put("yngr", dayCheckBox);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
//		List<String> list=new ArrayList<String>();
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//		String yearr=null;
//		Date bdate=null;
//		Date edate=null;
//		int bDay=1;
//		int eDay=1;
//		if(bdat!=null && bdat!=""){
//			yearr=bdat.substring(0,4);
//			bdate=sdf.parse(bdat);
//			edate=sdf.parse(edat);
//		}else{
//			Date sysDate=new Date();
//			yearr=sdf.format(sysDate).substring(0,4);
//			bdate=sysDate;
//			edate=sysDate;
//		}
//		Calendar cal1 = Calendar.getInstance();
//		Calendar cal2 = Calendar.getInstance();
//		Calendar cal3 = Calendar.getInstance();
//		cal1.setTime(bdate);
//		cal2.setTime(edate);
//		int bMonth=cal1.get(Calendar.MONTH)+1;
//		int eMonth=cal2.get(Calendar.MONTH)+1;
//		bDay=cal1.get(Calendar.DATE);
//		eDay=cal2.get(Calendar.DATE);
//		for (int i = bMonth; i <= eMonth; i++) {
//			String m=i+"";
//			SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM");
//			Date date=sd.parse(yearr+"-"+i);
//			cal3.setTime(date);
//			int lastDay=cal3.getActualMaximum(Calendar.DATE);
//			if(i==eMonth){
//				lastDay=eDay;
//			}
//			for (int j = bDay;j <= lastDay; j++) {
//				String d=j+"";
//				if(i<10){m="0"+i;}
//				if(j<10){d="0"+j;}
//				list.add(m+"-"+d);
//				if(j==lastDay){
//					bDay=1;
//					break;
//				}
//			}
//		}		
//		modelMap.put("dateList", list);
		modelMap.put("pageobj", page);
		modelMap.put("columns", firmMisService.findTableHead(condition));
		modelMap.put("ListBean", JSONArray.fromObject(firmMisService.findDayDifCompare(condition, page)).toString());//table体list
		modelMap.put("dayCheckBox", dayCheckBox);
		modelMap.put("bdat", supplyAcct.getBdat());
		modelMap.put("edat", supplyAcct.getEdat());
//		modelMap.put("datList", firmMisService.findDayDifCompare(condition,page));
		return new ModelAndView(FirmMisConstants.LIST_DAYDIFCOMPARE,modelMap);
	}
	/**
	 * 每日差异对比报表导出
	 */
	@RequestMapping("/exportDayDifCompare")
	@ResponseBody
	public boolean exportDayDifCompare(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "每日差异对比";
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			condition.put("positnCode", positnCode);
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		List<Map<String,Object>> exportList = firmMisService.findDayDifCompare(condition,page);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return firmMisService.exportExcel(response.getOutputStream(), exportList, condition);	
	}	
	
	
	
	
	
	
	
	
	/**************************************************每日差异对比end*********************************************/	
	/***********************************************************核减start************************************************/
	/**
	 * 核减明细
	 */
	@RequestMapping(value = "/toHeJian")
	public ModelAndView toHeJian(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(FirmMisConstants.LIST_HEJIAN,modelMap);
	}	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findHejianHeaders")
	@ResponseBody	
	public Object getHejianHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_HEJIAN_FROZEN);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseHejian")
	public ModelAndView toColChooseHejian(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(FirmMisConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}	
	/**
	 * 查询核减明细
	 */
	@RequestMapping("/findHejian")
	@ResponseBody
	public Object findHejian(ModelMap modelMap,HttpSession session,String page,String rows,String sp_code,String bdat,String edat,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String firmCode=accountPositn.getPositn().getCode();
			content.put("firmCode",firmCode);
		}
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("sp_code", sp_code);
		content.put("supplyAcct",supplyAcct);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return firmMisService.findHejian(content, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportHejian")
	@ResponseBody
	public void exportHejian(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "核减明细";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisService.findHejian(condition,pager).getRows(), "核减明细", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_HEJIAN));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printHejian")
	public ModelAndView printHejian(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			params.put("firmCode",positnCode);
			condition.put("firmCode",positnCode);
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisService.findHejian(condition, pager).getRows());
	    parameters.put("report_name", "核减明细");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printHejian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_HEJIAN,FirmMisConstants.REPORT_PRINT_URL_HEJIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/***********************************************************核减end**************************************************/	
	/**************************************************仓库盘点start*********************************************/
	@RequestMapping(value = "/toCangKuPanDian")
	public ModelAndView toCangKuPanDian(ModelMap modelMap,HttpSession session,Page page,Date bdat,Date edat,String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			Positn positn=new Positn();
			String positnCode=accountPositn.getPositn().getCode();
			String positnDes=accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
		}
		if(null!=action && ""!=action){
			modelMap.put("bdat", new Date());
			modelMap.put("edat", new Date());
		}else{
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
		}
		return new ModelAndView(FirmMisConstants.LIST_CANGKU_PANDIAN,modelMap);
	}
	/**************************************************仓库盘点end***********************************************/	
	/**************************************************分店月结start*********************************************/
	@RequestMapping(value = "/toFenDianYueJie")
	public ModelAndView toFenDianYueJie(ModelMap modelMap,HttpSession session,Page page,Date bdat,Date edat,String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			Positn positn=new Positn();
			String positnCode=accountPositn.getPositn().getCode();
			String positnDes=accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
		}
		if(null!=action && ""!=action){
			modelMap.put("bdat", new Date());
			modelMap.put("edat", new Date());
		}else{
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
		}
		return new ModelAndView(FirmMisConstants.LIST_FENDIAN_YUEJIE,modelMap);
	}
	/**************************************************分店月结end***********************************************/
	/**************************************************年终结转start*********************************************/
	@RequestMapping(value = "/toNianJieZhuan")
	public ModelAndView toNianJieZhuan(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("main", firmMisService.findMain(DateFormat.getStringByDate(new Date(),"yyyy")));
		return new ModelAndView(FirmMisConstants.LIST_NIAN_JIE_ZHUAN,modelMap);
	}
	/**************************************************年终结转end***********************************************/
	/**************************************************月末结账start*********************************************/
	@RequestMapping(value = "/toYueMoJieZhang")
	public ModelAndView toYueMoJieZhang(ModelMap modelMap,HttpSession session,Page page,String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		//查询当前结转月份
		Main main=firmMisService.findMain(DateFormat.getStringByDate(new Date(),"yyyy"));
		Map<String,Object> param=new HashMap<String, Object>();
		param.put("acct", acct);
		//检查是否系统盘点中
		Map<String,Object> result=firmMisService.findChktag(param);
		String num=result.get("num").toString();
		if(null!=action && !"".equals(action) && "init".equals(action)){
			modelMap.put("action", action);
			return new ModelAndView(FirmMisConstants.LIST_YUEMO_JIEZHANG,modelMap);
		}else{
			if(0==Integer.parseInt(num) && null!=main && Integer.parseInt(main.getMonthh())<13){
				param.put("monthh", Integer.parseInt(main.getMonthh())+1);
				param.put("yearr", mainInfoMapper.findYearrList().get(0));
				firmMisService.updateMonthh(param);
			}
			modelMap.put("monthh", main.getMonthh());
			modelMap.put("num", num);
			return new ModelAndView(FirmMisConstants.LIST_YUEMO_JIEZHANG,modelMap);
		}
	}
	/**************************************************月末结账end***********************************************/	
	
	
	
	/************************************手机报货 2015.2.28 wjf****************************************/
	/**
	 * 查询物资成本差异
	 *
	 */
	@RequestMapping("/findCostVarianceWAP")
	@ResponseBody
	public Object findCostVarianceWAP(SupplyAcct supplyAcct,String acct,String positnCode,Date bdat,Date edat,Page pager,String jsonpcallback) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		content.put("firm", positnCode);
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("month", DateFormat.getMonth(bdat));
		content.put("supplyAcct", supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(acct).getYnkc())) {
			content.put("SUPPLYACCT", "SUPPLYACCT");
			content.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			content.put("POSITNSUPPLY", "POSITNSUPPLY");
		}else{
			content.put("SUPPLYACCT", "firmSUPPLYACCT");
			content.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			content.put("POSITNSUPPLY", "FIRMPOSITNSUPPLY");
		}
		ReportObject<Map<String,Object>> lists = prdPrcCostManageService.findCostVariance(content, pager);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("list", lists);
		map.put("page", pager);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
