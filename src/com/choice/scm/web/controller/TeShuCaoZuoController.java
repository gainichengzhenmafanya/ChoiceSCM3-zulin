
package com.choice.scm.web.controller;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.TeShuCaoZuoConstants;
import com.choice.scm.domain.*;
import com.choice.scm.service.*;
import com.choice.scm.util.FileWorked;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 分店月结、年终结转、月末结转
 * @author csb
 *
 */
@Controller
@RequestMapping("teShuCaoZuo")
public class TeShuCaoZuoController {

	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private TeShuCaoZuoService teShuCaoZuoService;
	@Autowired
	private AcctService acctservice;
	@Autowired
	private PositnService positnService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private FirmSupplyService firmSupplyService;
	/**************************************************分店月结start*********************************************/
	@RequestMapping(value = "/toFenDianYueJie")
	public ModelAndView toFenDianYueJie(ModelMap modelMap, HttpSession session, Page page, 
			Date bdat, Date edat, String action, Positn positn, String init) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String accountId=session.getAttribute("accountId").toString();
//		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
//		if(null!=accountPositn && null!=accountPositn.getPositn()){
//			Positn positn=new Positn();
//			String positnCode=accountPositn.getPositn().getCode();
//			String positnDes=accountPositn.getPositn().getDes();
//			positn.setCode(positnCode);
//			positn.setDes(positnDes);
//		}
		modelMap.put("listPositn", positnService.findPositn(positn, page));
		if(null!=action && ""!=action){
			modelMap.put("bdat", new Date());
			modelMap.put("edat", new Date());
		}else{
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
		}
		modelMap.put("init", init);
		return new ModelAndView(TeShuCaoZuoConstants.LIST_FENDIAN_YUEJIE,modelMap);
	}
	/**************************************************分店月结end***********************************************/		
	
	/**************************************************月末结账start*********************************************/
	@RequestMapping(value = "/toYueMoJieZhang")
	public ModelAndView toYueMoJieZhang(ModelMap modelMap, HttpSession session, Page page,
			String action, Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		//查询当前结转月份
//		String yearr = teShuCaoZuoService.findMax().getYearr();
		Main main=teShuCaoZuoService.findMain();
		Map<String,Object> param=new HashMap<String, Object>();
		param.put("acct", acct);
		//检查是否系统盘点中
		Map<String,Object> result=teShuCaoZuoService.findChktag(param);
		String num=result.get("num").toString();
		//检查系统是否这个月有未审核的单子
			MainInfo mainInfo=acctservice.getmainInfo(new Date());
			param.put("bdat", mainInfo.getBdat1());
			param.put("edat", mainInfo.getEdat1());
			//查下是否有未审核的入库单或者直发单
			List<String> listChkinno=teShuCaoZuoService.findChkchkinm(param);
			//查询是否有未时候的出库单 
			List<String> listChkoutno=teShuCaoZuoService.findChkchkoutm(param);
			String isChk="";
			if(listChkinno.size()!=0){
				isChk="本月内以下入库或者直发单号未审核，不能月结:";
				for (int i = 0; i < listChkinno.size(); i++) {
					isChk+=listChkinno.get(i)+",";
				}
			}
			if(listChkoutno.size()!=0){
				isChk+="\n 本月内以下出库单号未审核，不能月结:";
				for (int i = 0; i < listChkoutno.size(); i++) {
					isChk+=listChkoutno.get(i)+",";
				}
			}
		
				
		if(null!=action && !"".equals(action) && "init".equals(action)){
			modelMap.put("action", action);
			modelMap.put("monthh", main.getMonthh());
			modelMap.put("num", num);
			modelMap.put("isChk", isChk);
			modelMap.put("listPositn", positnService.findPositn(positn, page));
			return new ModelAndView(TeShuCaoZuoConstants.LIST_YUEMO_JIEZHANG,modelMap);
		}else{
			if(0==Integer.parseInt(num) && null!=main && Integer.parseInt(main.getMonthh())<13){
				param.put("monthh", Integer.parseInt(main.getMonthh())+1);
				param.put("yearr", main.getYearr());
				teShuCaoZuoService.updateMonthh(param);
			}
			modelMap.put("monthh", main.getMonthh());
			modelMap.put("num", num);
			modelMap.put("listPositn", positnService.findPositn(positn, page));
			return new ModelAndView(TeShuCaoZuoConstants.LIST_YUEMO_JIEZHANG,modelMap);
		}
	}
	/**************************************************月末结账end***********************************************/
	
	/**************************************************年终结转start*********************************************/
	@RequestMapping(value = "/toNianJieZhuan")
	public ModelAndView toNianJieZhuan(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("main", teShuCaoZuoService.findMain());
		return new ModelAndView(TeShuCaoZuoConstants.LIST_NIAN_JIE_ZHUAN,modelMap);
	}
	
	/**
	 * 结转
	 * @param modelMap
	 * @param session
	 * @param firm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/jiezhuan")
	@ResponseBody
	public String jiezhuan(ModelMap modelMap, HttpSession session, Main main) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return teShuCaoZuoService.jiezhuan(session.getAttribute("ChoiceAcct").toString(), main);
	}
	/**************************************************年终结转end***********************************************/
	
	/**************************************************清空数据start***********************************************/
	@RequestMapping(value = "/toClearData")
	public ModelAndView clearData(ModelMap modelMap,HttpSession session,Page page,String action,String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		String str=null;
		if("Y".equals(type) || "N".equals(type)){
			str=teShuCaoZuoService.clearData(acct,type,DateFormat.getYear(new Date())+"");
		}
		
		modelMap.put("type", type);
		modelMap.put("str", str);
		return new ModelAndView(TeShuCaoZuoConstants.LIST_CLEAR_DATA,modelMap);
	}
	/**************************************************清空数据end***********************************************/
	
	/**************************************************仓库期初start*********************************************/
	@RequestMapping(value = "/ckInit")
	public ModelAndView ckInit(ModelMap modelMap, HttpSession session, Page page, String positnCd, String positnNm, String typCode, 
			String status, String typDes, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			Positn positn=new Positn();
			String positnCode=accountPositn.getPositn().getCode();
			String positnDes=accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
		}
		if(null!=action && ""!=action){
		}else{
			PositnSupply positnSupply =  new PositnSupply();
			positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
			positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			positnSupply.setPositn(positnCd);
			if (typCode!=null && typCode!="") {
				positnSupply.setSp_type(typCode);
			}
			modelMap.put("positnSupplyList", teShuCaoZuoService.getpositnSupplyList(positnSupply));
			modelMap.put("positnCd", positnCd);
			modelMap.put("positnNm", positnNm);
			modelMap.put("typCode", typCode);
			modelMap.put("typDes", typDes);
			Positn positn = new Positn();
			positn.setAcct(session.getAttribute("ChoiceAcct").toString());
			positn.setCode(positnCd);
			if ("Y".equals(teShuCaoZuoService.getQC(positn).getQc())) {
				modelMap.put("status", "init");
			}else {
				modelMap.put("status", status);
			}
		}
		return new ModelAndView(TeShuCaoZuoConstants.LIST_CANGKU_QICHU,modelMap);
	}
	
	/**
	 * 保存仓库期初
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCkInit")
	@ResponseBody
	public String updateCkInit(ModelMap modelMap, HttpSession session,  PositnSupply positnSupply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		String yearr = DateFormat.getStringByDate(new Date(),"yyyy");
		teShuCaoZuoService.updateCkInit(positnSupply, acct, yearr);
		return "success";
	}
	
	/**
	 * 仓库期初-确认初始
	 * @throws Exception
	 */
	@RequestMapping(value = "/initation")
	@ResponseBody
	public void initation(ModelMap modelMap, HttpSession session, String positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		PositnSupply positnSupply = new PositnSupply();
		positnSupply.setAcct(acct);
		positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		positnSupply.setPositn(positn);
		teShuCaoZuoService.initation(teShuCaoZuoService.getpositnSupplyList(positnSupply), positn, acct);
	}
	/**
	 * 导出
	 */
	@RequestMapping("/exportCangkuInit")
	@ResponseBody
	public boolean exportCangkuInit(HttpServletResponse response,HttpServletRequest request,HttpSession session, String positnCd, String positnNm, String typCode, 
			String status, String typDes, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String acct = session.getAttribute("ChoiceAcct").toString();
		PositnSupply positnSupply =  new PositnSupply();
		positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		positnSupply.setPositn(positnCd);
		if (typCode!=null && typCode!="") {
			positnSupply.setSp_type(typCode);
		}
		List<PositnSupply> positnSupplyList= teShuCaoZuoService.getpositnSupplyList(positnSupply);
		String fileName = "仓库期初表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return teShuCaoZuoService.exportCangkuInit(response.getOutputStream(), positnSupplyList,positnCd);
	}
	
	/**
	 * 安装仓位物资属性导出仓位的物资
	 * @param response
	 * @param request
	 * @param session
	 * @param positnCd
	 * @param positnNm
	 * @param typCode
	 * @param status
	 * @param typDes
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportCangkuInitStart")
	@ResponseBody
	public boolean exportCangkuInitStart(HttpServletResponse response,HttpServletRequest request,HttpSession session, String positnCd, String positnNm, String typCode, 
			String status, String typDes, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PositnSupply positnSupply =  new PositnSupply();
		positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		positnSupply.setPositn(positnCd);
		if (typCode!=null && typCode!="") {
			positnSupply.setSp_type(typCode);
		}
		//得到仓位物资属性中物资
		PositnSpcode psp = new PositnSpcode();
		psp.setPositn(positnCd);
		List<PositnSpcode> positnSpcodeList = firmSupplyService.findFirmSupply(psp);
		String fileName = "仓库期初表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		return teShuCaoZuoService.exportCangkuInitStart(response.getOutputStream(), positnSpcodeList,positnCd);
	}
	
	/**
	 * 跳转到导入页面   导入 CSS
	 */
	@RequestMapping("/importInit")
	public ModelAndView importChkstom(ModelMap modelMap,String positn) throws Exception{
		modelMap.put("positn", positn);
		return new ModelAndView(TeShuCaoZuoConstants.IMPORT_INIT,modelMap);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,String positn) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = teShuCaoZuoService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		modelMap.put("positn", positn);
		return new ModelAndView(TeShuCaoZuoConstants.IMPORT_RESULT, modelMap);
	}
	
		
    /**
	 * 导入期初  wjf
	 */
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpServletRequest request, HttpSession session, ModelMap modelMap, @RequestParam String realFilePath,PositnSupply positnSupply)
				throws Exception {
	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	    request.setCharacterEncoding("UTF-8");
		System.out.println("文件路径： "+ realFilePath);
		boolean checkResult = ReadInitExcel.check(realFilePath, supplyService, positnSupply.getPositn());
		if (checkResult) {//成功
			List<PositnSupply> psList = ReadInitExcel.psList;
			String acct = session.getAttribute("ChoiceAcct").toString();
			String yearr = mainInfoService.findYearrList().get(0);
			positnSupply.setAcct(acct);
			positnSupply.setYearr(yearr);
			modelMap.put("positnSupply", positnSupply);
			modelMap.put("status", "import");
			modelMap.put("positnSupplyList", psList);
		} else {
			List<String> errorList = ReadInitExcel.errorList;
			modelMap.put("status", "select");
			modelMap.put("importError", errorList);
			System.out.println("错误消息： "+ errorList);
		}
		modelMap.put("positnCd", positnSupply.getPositn());
		Positn p = new Positn();
		p.setCode(positnSupply.getPositn());
		modelMap.put("positnNm", positnService.findPositnByCode(p).getDes());
		FileWorked.deleteFile(realFilePath);//删除上传后的的文件
		return new ModelAndView(TeShuCaoZuoConstants.LIST_CANGKU_QICHU,modelMap);
	}
	
	
	/**************************************************仓库期初end***********************************************/	
	
	
	/***
	 * 执行sql文件方法
	 * @return
	 */
	@RequestMapping(value = "/doSql")
	@ResponseBody
	public String doSql(HttpServletRequest request) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return teShuCaoZuoService.doSql(request);
	}
	
	/**************************************************基础设置查错start***********************************************/	
	
	/**
	 * 基础设置查错
	 * 
	 * @param modelMap
	 * @param project
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/maincheck")
	public ModelAndView maincheck(ModelMap modelMap, String action) throws Exception {
		if(!"init".equals(action)) {
			modelMap.put("listSpPosition", teShuCaoZuoService.findSpPosition()); //1.检查物资的默认仓位
			modelMap.put("spPositionSize", teShuCaoZuoService.findSpPosition().size()); 
			modelMap.put("listDeliver", teShuCaoZuoService.findDeliver()); //2.检查物资的默认供应商
			modelMap.put("deliverSize", teShuCaoZuoService.findDeliver().size()); 
			modelMap.put("listPositnEx1", teShuCaoZuoService.findPositnEx1()); //3.检查设置了半成品但是没有设置加工仓位
			modelMap.put("positnEx1Size", teShuCaoZuoService.findPositnEx1().size()); 
			modelMap.put("listEx1Bom", teShuCaoZuoService.findEx1Bom()); //4.检查设置了半成品但是没有设置BOM
			modelMap.put("ex1BomSize", teShuCaoZuoService.findEx1Bom().size()); 
			modelMap.put("listEx1BomSpCode", teShuCaoZuoService.findEx1BomSpCode()); //5.检查半成品BOM用的原材料已删除
			modelMap.put("ex1BomSpCodeSize", teShuCaoZuoService.findEx1BomSpCode().size()); 
			modelMap.put("listItemBomSpCode", teShuCaoZuoService.findItemBomSpCode()); //6.检查菜品BOM用的原材料已删除
			modelMap.put("itemBomSpCodeSize", teShuCaoZuoService.findItemBomSpCode().size()); 
			modelMap.put("listMainPositn", teShuCaoZuoService.findMainPositn()); //7.检查主直拨库
			modelMap.put("mainPositnSize", teShuCaoZuoService.findMainPositn().size());
			modelMap.put("listPositnDes1", teShuCaoZuoService.findPositnDes1()); //8.检查有仓位没有设定简称，将影响分拨功能的使用
			modelMap.put("positnDes1Size", teShuCaoZuoService.findPositnDes1().size());
			modelMap.put("listDeliverPositn", teShuCaoZuoService.findDeliverPositn()); //9.检查有主仓库或加工间没有在供应商中定义
			modelMap.put("deliverPositnSize", teShuCaoZuoService.findDeliverPositn().size());
			modelMap.put("listHoliday", teShuCaoZuoService.findHoliday()); //10.检查没有定义节假日
			modelMap.put("holidaySize", teShuCaoZuoService.findHoliday().size());
			modelMap.put("listPositnSpcode", teShuCaoZuoService.findPositnSpcode()); //11.检查分店物资属性 同虚拟物料的不同物资
			modelMap.put("positnSpcodeSize", teShuCaoZuoService.findPositnSpcode().size()); 
			
			//add by js at 20160328 物资属性查错
			modelMap.put("listSupplyAttribute", teShuCaoZuoService.findSupplyAttributePm()); //12.检查物资属性设置有问题的物资
			modelMap.put("SupplyAttributeSize", teShuCaoZuoService.findSupplyAttributePm().size()); 
			
			action="";
		}
		modelMap.put("action", action);
		return new ModelAndView(TeShuCaoZuoConstants.MAIN_CHECK, modelMap);
	}
}
