package com.choice.scm.web.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.AcctConstants;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.PublicExportTemplate;
import com.choice.scm.util.ReadReportUrl;
/**
 * 入库单
 * @author lehui csb
 *
 */
@Controller
@RequestMapping(value = "chkinm")
public class ChkinmController {

	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	PositnService positnService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private CodeDesService codeDesService;
    /**
     * 查询入库单list
     * @param checkOrNot   是否审核
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     * @param inout   zf  表示查询直发    rk  表示rk
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllChkinm(ModelMap modelMap, String checkOrNot, Chkinm chkinm, Date madedEnd, String startDate, Page page, String action,HttpSession session)
			throws Exception {
				if("CPRK".equals(chkinm.getTyp())){
					chkinm.setTyp("9905");
				}
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
						"查询入库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
				logsMapper.addLogs(logd);
				if(null!=action && "init".equals(action)){
					Date date=new Date();
					if(null!=startDate && !"".equals(startDate)){
						chkinm.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
					}else{
						chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
					}
					modelMap.put("chkinmList", chkinmService.findAllChkinm(checkOrNot,chkinm,page,DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
					modelMap.put("madedEnd", date);
				}else{
					modelMap.put("chkinmList", chkinmService.findAllChkinm(checkOrNot,chkinm,page,madedEnd,session.getAttribute("locale").toString()));
					modelMap.put("madedEnd", madedEnd);
				}
				modelMap.put("chkinm", chkinm);
				modelMap.put("checkOrNot", checkOrNot);
				modelMap.put("pageobj", page);
				if("check".equals(checkOrNot)){
					return new ModelAndView(ChkinmConstants.LIST_CHECKED_CHKINM, modelMap);
				}else{
					return new ModelAndView(ChkinmConstants.LIST_CHKINM, modelMap);
		}
	}
	
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/add")
	public ModelAndView add(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		
		String inout=null;
		String locale = session.getAttribute("locale").toString();//语言
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			Chkinm chkinm=new Chkinm();
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
			chkinm.setChkinno(chkinmService.getMaxChkinno());
			inout=ChkinmConstants.UPDATE_CHKINM;
			modelMap.put("chkinm",chkinm);//日期等参数
			String accountId=session.getAttribute("accountId").toString();
			modelMap.put("positnList", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1202'"));//入库单填制只能看到基地仓库 ghc 2016年11月16日13:03:43
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp("RK");
			//单据类型
			modelMap.put("billType", codeDesService.findDocumentType(codeDes));
			deliver.setIsRealAndExAndPYPK("1");
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
//			modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}else{
			modelMap.put("tableFrom", tableFrom);//当前页面状态
			inout=ChkinmConstants.UPDATE_CHKINM;
		}
		return new ModelAndView(inout,modelMap);	
	}
	/**
	 * 获取入库单单号
	 * @param maded
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded,String zf) throws ParseException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(zf != null && "Y".equals(zf)){//如果是直发单则掉直发
			return calChkNum.getNext(CalChkNum.CHKZB, DateFormat.getDateByString(maded, "yyyy-MM-dd"));
		}
		return calChkNum.getNext(CalChkNum.CHKIN, DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}
	
	/**
	 * 添加入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveChkinm")
	public ModelAndView saveChkinm(ModelMap modelMap, Chkinm chkinm,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志事件
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"添加入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//保存方法类
		chkinmService.saveChkinm(chkinm,"in");
		//保存后方法跳转
		return new ModelAndView(ChkinmConstants.SAVE_CHKINM,modelMap);	
	}
	
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(ModelMap modelMap,HttpSession session, Chkinm chkinm, String isCheck, String inout) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		modelMap.put("positnList", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), session.getAttribute("accountId").toString(),"'1201','1202','1203'"));//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
		modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
		modelMap.put("chkinm",chkinmService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");//当前页面状态
		if(null!=isCheck && !"".equals(isCheck)){
			if(null!=inout && "zf".equals(inout)){
				String locale = session.getAttribute("locale").toString();
				CodeDes codeDes = new CodeDes();
				codeDes.setLocale(locale);
				codeDes.setCodetyp("ZF");
				modelMap.put("billType", codeDesService.findDocumentType(codeDes));
				return new ModelAndView(ChkinmConstants.SEARCH_CHECKED_CHKINMZB,modelMap);
			}else{
				String locale = session.getAttribute("locale").toString();
				CodeDes codeDes = new CodeDes();
				codeDes.setLocale(locale);
				codeDes.setCodetyp("RK");
				modelMap.put("billType", codeDesService.findDocumentType(codeDes));
				return new ModelAndView(ChkinmConstants.SEARCH_CHECKED_CHKINM,modelMap);
			}
		}else{
			if(null!=inout && "zf".equals(inout)){
				String locale = session.getAttribute("locale").toString();
				CodeDes codeDes = new CodeDes();
				codeDes.setLocale(locale);
				codeDes.setCodetyp("ZF");
				modelMap.put("billType", codeDesService.findDocumentType(codeDes));
				return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB,modelMap);
			}else{
				String locale = session.getAttribute("locale").toString();
				CodeDes codeDes = new CodeDes();
				codeDes.setLocale(locale);
				codeDes.setCodetyp("RK");
				modelMap.put("billType", codeDesService.findDocumentType(codeDes));
				return new ModelAndView(ChkinmConstants.UPDATE_CHKINM,modelMap);
			}
		}	
	}

	/**
	 * 修改入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinm")
	@ResponseBody
	public int updateChkinm(ModelMap modelMap, HttpSession session, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"修改入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Chkinm chkinms=chkinmService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		try{
			acctService.getOnlyAccountMonth2(chkinm.getMaded());//判断制单日期会计日
		}catch(Exception e1){
			return -9;
		}
		if(null!=chkinms){//存在则修改
			chkinm.setInout(ChkinmConstants.in);
			result=chkinmService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN, chkinm.getMaded()));//从新获得单号
			result=chkinmService.saveChkinm(chkinm,"in");
		}
		return result;	
	}
	/**
	 * 审核入库单  单条审核
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkChkinm")
	@ResponseBody
	public Object checkChkinm(ModelMap modelMap, Chkinm chkinm, HttpSession session, String zb)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		try {
			acctService.getOnlyAccountMonth(chkinm.getMaded());//  此字段当成月份使用    加入会计月 
		} catch (Exception e) {
			// TODO: handle exception
			 if(e.getMessage().equals(AcctConstants.NOT_NIAN_JIE_ZHUAN)){
				 System.out.println("没有年结转！！请先停止使用，联系辰森研发人员！");
				 return "-9";
			 }
			 //不能操作当前会计月之前的数据
			 if(e.getMessage().equals(AcctConstants.CAN_NOT_CHECK_BEFORE_DATA)){
				 System.out.println("不能操作当前会计期之前的数据！");
				 return "-4";
			 }
		}
		
		
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"审核入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountId);
		String pr=chkinmService.checkChkinm(chkinm,null);
		return pr;
//		if("zb".equals(zb))
//			return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB,modelMap);	
//		else
//			
//			return new ModelAndView(ChkinmConstants.UPDATE_CHKINM,modelMap);
	}
	/**
	 * 审核操作  批量审核
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids, String checkOrNot, Chkinm chkinm, Date madedEnd, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"批量审核入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountName);
		chkinmService.checkChkinm(chkinm,chkinnoids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除入库单
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String chkinnoids, Page page, String checkOrNot, Chkinm chkinm, Date madedEnd, String action, String flag,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"删除入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		chkinmService.deleteChkinm(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else if("zb".equals(flag)){
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB, modelMap);
		}else{
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmConstants.UPDATE_CHKINM, modelMap);
		}
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap,HttpServletRequest request,Chkinm chkinm,String type,Page page,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"打印入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkinm=chkinmService.findChkinmByid(chkinm);
		Object pk_group = UserSpace.getSession().getAttribute("pk_group");
		String oldName;
		String realName;
        String realUrl;
			List<Chkind> chkindList=chkinm.getChkindList();
//			int num=chkindList.size()%12;
//			for (int i = 0; i < 12-num; i++) {
//				chkindList.add(null);
//			}
			modelMap.put("List",chkindList);
	 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
	 		String report_name = "入库单";
	 		if("inout".equals(chkinm.getInout())){
	 			report_name = "直发单";
	 		}
	        parameters.put("report_name", report_name); 
	        if(null!=chkinm.getDeliver() && null!= chkinm.getDeliver().getDes()){
	        	parameters.put("deliver.des", chkinm.getDeliver().getDes()); 
	        }
	        parameters.put("positn.des", chkinm.getPositn().getDes()); 
	        parameters.put("vouno", chkinm.getVouno()); 
	        parameters.put("maded", DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")); 
	        parameters.put("madeby", chkinm.getMadebyName()); 
	        parameters.put("checby", chkinm.getChecbyName());
	        CodeDes codeDes = new CodeDes();
	        codeDes.setCode(chkinm.getTyp());
	        codeDes.setLocale(session.getAttribute("locale").toString());
	        parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("chkinno", chkinm.getChkinno()+"");
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/chkinm/viewChkinm.do");//传入回调路径
	 	if("inout".equals(chkinm.getInout())){			
			oldName = "scmChkinmzbReport.jasper";
			realName  = pk_group.toString()+oldName;
	        realUrl = PublicExportTemplate.filePathUse(request,realName,oldName);
	 		Map<String,String> rs=ReadReportUrl.redReportUrl(type,realUrl,realUrl);//判断跳转路径
	 		modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
			return new ModelAndView(rs.get("url"),modelMap);
 		}else{
			oldName = "scmChkinmReport.jasper";
			realName  = pk_group.toString()+oldName;
	        realUrl = PublicExportTemplate.filePathUse(request,realName,oldName);
 			Map<String,String> rs=ReadReportUrl.redReportUrl(type,realUrl,realUrl);//判断跳转路径
 			modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
 			return new ModelAndView(rs.get("url"),modelMap);
 		}
        
	}
	
	/**
	 * 转入直拨单添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addzb")
	public ModelAndView addzb(ModelMap modelMap, HttpSession session, String action)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp("ZF");
			Chkinm chkinm=new Chkinm();
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
			chkinm.setChkinno(chkinmService.getMaxChkinno());
			modelMap.put("chkinm",chkinm);//日期等参数
			String acct=session.getAttribute("ChoiceAcct").toString();
			String accountId=session.getAttribute("accountId").toString();
			List<Positn> positnList = positnRoleService.findAllPositn(acct, accountId);
			//直拨单填制审核入库仓位只能选择主直拨库和基地仓库  此处做限制 wjf
			List<Positn> positnList1 = new ArrayList<Positn>();
			for(int i = 0;i<positnList.size();i++){
				if("1201".equals(positnList.get(i).getTyp()) || "1202".equals(positnList.get(i).getTyp())){
					positnList1.add(positnList.get(i));
				}
			}
			modelMap.put("positnList", positnList1);//带有权限控制的仓位
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			deliver.setIsRealAndEx("1");
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
			modelMap.put("billType", codeDesService.findDocumentType(codeDes));
//			modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}
		return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB,modelMap);	
	}
	
	/**
	 * 修改直拨单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinmzb")
	@ResponseBody
	public int updateChkinmzb(ModelMap modelMap,HttpSession session,Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"修改直拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Chkinm chkinms=chkinmService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		try{
			acctService.getOnlyAccountMonth2(chkinm.getMaded());//判断制单日期会计日
		}catch(Exception e1){
			return -9;
		}
		if(null!=chkinms){//存在则修改
			chkinm.setInout("inout");
			result=chkinmService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout("inout");
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB, chkinm.getMaded()));//从新获得单号
			result=chkinmService.saveChkinm(chkinm,"zb");
		}
		return result;	
	}
	
	/**
	 * 查询是否必须入到默认仓位
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/chkPositn")
	@ResponseBody
	public String chkPositn(ModelMap modelMap, HttpSession session, SpCodeMod spCodeMod)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spCodeMod.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmService.chkPositn(spCodeMod);
	}
	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param modelMap
	 * @param session
	 * @param chkinm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value="/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmService.chkChect(active,chkinm);
	}
	
	/**
	 * 冲消查询界面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmByCx")
	public ModelAndView addChkinmByCx(ModelMap modelMap, Page page, HttpSession session, Spbatch spbatch, String bdat, String edat) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (bdat == null) {
			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
		}else {
			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
		}
		if (edat == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else {
			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		}
		
		modelMap.put("spbatchList", chkinmService.addChkinmByCx(spbatch));//获取入库数据
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmConstants.TABLE_CHKINM_CX,modelMap);
	}
	
	/**
	 * 直发物资冲消查询界面
	 * @author 2014.11.4wjf
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmzfByCx")
	public ModelAndView addChkinmzfByCx(ModelMap modelMap, Spbatch spbatch, String bdat, String edat,String action) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (bdat == null) {
			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
		}else {
			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
		}
		if (edat == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else {
			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		}
		if(!"init".equals(action)){
			modelMap.put("spbatchList", chkinmService.addChkinmzfByCx(spbatch));//获取直发数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmConstants.TABLE_CHKINMZF_CX,modelMap);
	}
	
	/*************************************************供应商结算界面*****************************************************/
	
	/**
	 * 供应商结算
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deliverSaList")
	public ModelAndView deliverSaList(ModelMap modelMap, Chkinm chkinm, Page page, Date madedEnd, String checkOrNot,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"供应商结算",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
        if (chkinm != null && chkinm.getMaded() == null) {
            chkinm.setMaded(new Date());
        }
        if (madedEnd == null) {
            madedEnd = new Date();
        }
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())) {

            List<Chkinm> listChkinm = chkinmService.findAllChkinmNew(checkOrNot, chkinm, page, madedEnd,session.getAttribute("locale").toString());
            Chkinm totalChkinm = chkinmService.findAllChkinmTotal(checkOrNot, chkinm, madedEnd);
            modelMap.put("listChkinm", listChkinm);
            modelMap.put("totalChkinm", totalChkinm);
        }
        modelMap.put("pageobj", page);
        modelMap.put("Chkinm", chkinm);
        modelMap.put("madedEnd", madedEnd);
        modelMap.put("checkOrNot", checkOrNot);
		return new ModelAndView(ChkinmConstants.DELIVERSA_LIST,modelMap);	
	}
	
	/**
	 * 查询付款记录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findPayData")
	public ModelAndView findPayData(ModelMap modelMap, Chkinm chkinm, String madedEnd,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())){
            List<Map<String,Object>> listFolio = chkinmService.findPayData(chkinm, madedEnd);
            if(listFolio != null && !listFolio.isEmpty()){
                Map<String,Object> map = listFolio.get(listFolio.size()-1);
                listFolio.remove(listFolio.size()-1);
                modelMap.put("totalPay", map);
            }else{
                Map<String,Object> map = new HashMap<String,Object>();
                modelMap.put("totalPay", map);
            }
            modelMap.put("listFolio", listFolio);
        }
		return new ModelAndView(ChkinmConstants.FOLIO_LIST,modelMap);	
	}
	
	/**
	 * 查询发票记录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findBillData")
	public ModelAndView findBillData(ModelMap modelMap, Chkinm chkinm, String madedEnd)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())) {
            List<Map<String, Object>> listBill = chkinmService.findBillData(chkinm, madedEnd);
            if (listBill != null && !listBill.isEmpty()) {
                Map<String, Object> map = listBill.get(listBill.size() - 1);
                listBill.remove(listBill.size() - 1);
                modelMap.put("totalBill", map);
            } else {
                Map<String, Object> map = new HashMap<String, Object>();
                modelMap.put("totalBill", map);
            }
            modelMap.put("listBill", listBill);
        }
		return new ModelAndView(ChkinmConstants.BILL_LIST,modelMap);	
	}
	
	/**
	 * 审核结账
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkedBill")
	public ModelAndView checkedBill(ModelMap modelMap, String chkinno)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinmService.checkedChkinmBill(chkinno);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**
	 * 跳转到付款界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toPayMoney")
	public ModelAndView toPayMoney(ModelMap modelMap, String chkinno,String money)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("chkinno", chkinno);
		modelMap.put("money", money);
		return new ModelAndView(ChkinmConstants.PAY_MONEY,modelMap);
	}
	
	/**
	 * 付款
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/payMoney")
	public ModelAndView payMoney(ModelMap modelMap,HttpSession session, String chkinno, Date dat, String memo, double pay)throws Exception{
		List<Chkinm> listChkinm = chkinmService.findChkinmByIds(chkinno);
		String madeby = session.getAttribute("accountName").toString();
		chkinmService.payMoney(listChkinm,dat,memo,pay,madeby);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**
	 * 跳转到发票界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddBill")
	public ModelAndView toAddBill(ModelMap modelMap, String deliverCode)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(ChkinmConstants.ADD_BILL,modelMap);	
	}
	
	/**
	 * 添加发票
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * String deliver.code, Date dat, String memo, double pay
	 */
	@RequestMapping(value="/addBill")
	public ModelAndView addBill(ModelMap modelMap, Chkinm chkinm, HttpSession session)throws Exception{
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		chkinmService.addBill(chkinm);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	
	/**
	 * 跳转到添加期初金额
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddDeliverMoney")
	public ModelAndView toAddDeliverMoney(ModelMap modelMap, String deliverCode)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(ChkinmConstants.ADD_DELIVER_MONEY,modelMap);	
	}
	
	/**
	 * 添加供应商未结金额
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * String deliver.code, Date dat, String memo, double pay
	 */
	@RequestMapping(value="/addDeliverMoney")
	public ModelAndView addDeliverMoney(ModelMap modelMap, Chkinm chkinm, HttpSession session)throws Exception{
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		chkinmService.addDeliverMoney(chkinm);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**************************************供应商结算 结束*************************************************/
	
	/**
	 * 跳转到导入页面   入库单导入 wjf
	 */
	@RequestMapping("/importChkinm")
	public ModelAndView importChkinm(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkinmConstants.IMPORT_CHKINM,modelMap);
	}
	
	/**
	 * 下载模板信息 入库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		chkinmService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkinmService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkinmConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入报货单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountId").toString();
		Object obj=chkinmService.check(realFilePath, accountId);
		if(obj instanceof Chkinm){//导入对了
			String accountName=session.getAttribute("accountName").toString();
			((Chkinm) obj).setMadeby(accountName);//得到当前操作人
			((Chkinm) obj).setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
			((Chkinm) obj).setChkinno(chkinmService.getMaxChkinno());
			modelMap.put("chkinm",(Chkinm) obj);
//			String acct=session.getAttribute("ChoiceAcct").toString();
			modelMap.put("positnList", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202','1203'"));//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
			modelMap.put("curStatus", "add");//当前页面状态
			modelMap.put("importFlag","OK");
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(session.getAttribute("locale").toString());
			codeDes.setCodetyp("RK");
			modelMap.put("billType", codeDesService.findDocumentType(codeDes));//单据类型
			return new ModelAndView(ChkinmConstants.UPDATE_CHKINM,modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkinmConstants.UPDATE_CHKINM,modelMap);
		}
	}
	
	/**
	 * 跳转到 同步NC数据页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="synchroNCData")
	public ModelAndView synchroNCData(ModelMap modelMap,String type) throws Exception{
		modelMap.put("type", type);
		return new ModelAndView(ChkinmConstants.SYNCHRONCDATA,modelMap);
	}
	 
	/********************************安卓盘点机部分方法开始************************************/
	/**
	 * 安卓手机返回所有入库 仓位
	 */
	@RequestMapping(value = "/findPositnListWAP")
	@ResponseBody
	public Object findPositnListWAP(Deliver deliver,String jsonpcallback,String accountId) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, String>> listString=new ArrayList<Map<String,String>>();
		//关键字查询
		List<Positn> listPositn= positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202'");//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
		for (int i = 0; i < listPositn.size(); i++) {
			Map<String, String> map1=new HashMap<String, String>();
			map1.put("code", listPositn.get(i).getCode());
			map1.put("des", listPositn.get(i).getDes());
			listString.add(map1);
		}
		map.put("listPositn",listString);
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 安卓  手机返回所有供应商
	 */
	@RequestMapping(value = "/findDeliverListWAP")
	@ResponseBody
	public Object findDeliverListWAP(Deliver deliver,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, String>> listString=new ArrayList<Map<String,String>>();
		//关键字查询
		List<Deliver> listDeliver= deliverService.findAllDelivers(deliver);
		for (int i = 0; i < listDeliver.size(); i++) {
			Map<String, String> map1=new HashMap<String, String>();
			map1.put("code", listDeliver.get(i).getCode());
			map1.put("des", listDeliver.get(i).getDes());
			listString.add(map1);
		}
		map.put("listDeliver",listString);
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 安卓  手机返回所有入库仓位 +   供应商
	 */
	@RequestMapping(value = "/findPositnAndDeliverListWAP")
	@ResponseBody
	public Object findPositnAndDeliverListWAP(Deliver deliver,String jsonpcallback,String accountId) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, String>> listString=new ArrayList<Map<String,String>>();
		List<Map<String, String>> listString1=new ArrayList<Map<String,String>>();
		//关键字查询 入库仓位
		List<Positn> listPositn= positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202'");//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
		for (int i = 0; i < listPositn.size(); i++) {
			Map<String, String> map1=new HashMap<String, String>();
			map1.put("code", listPositn.get(i).getCode());
			map1.put("des", listPositn.get(i).getDes());
			listString.add(map1);
		}
		//入库供应商
		List<Deliver> listDeliver= deliverService.findAllDelivers(deliver);
		for (int i = 0; i < listDeliver.size(); i++) {
			Map<String, String> map2=new HashMap<String, String>();
			map2.put("code", listDeliver.get(i).getCode());
			map2.put("des", listDeliver.get(i).getDes());
			listString1.add(map2);
		}
		map.put("listPositn",listString);
		map.put("listDeliver",listString1);
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 安卓手机   返回所有仓位 + 领用仓位
	 */
	@RequestMapping(value = "/findPositnAndFirmListWAP")
	@ResponseBody
	public Object findPositnAndFirmListWAP(String jsonpcallback,String accountId) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, String>> listString=new ArrayList<Map<String,String>>();
		List<Map<String, String>> listString1=new ArrayList<Map<String,String>>();
		//关键字查询  仓位
		List<Positn> listPositn= positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1202','1204'");//加工间也可以出库!勿动
		for (int i = 0; i < listPositn.size(); i++) {
			Map<String, String> map1=new HashMap<String, String>();
			map1.put("code", listPositn.get(i).getCode());
			map1.put("des", listPositn.get(i).getDes());
			listString.add(map1);
		}
		//领用仓位
		List<Positn> listFirm= positnService.findChkoutPositnIn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId);
		for (int i = 0; i < listFirm.size(); i++) {
			Map<String, String> map2=new HashMap<String, String>();
			map2.put("code", listFirm.get(i).getCode());
			map2.put("des", listFirm.get(i).getDes());
			listString1.add(map2);
		}
		
		map.put("listPositn",listString);
		map.put("listFirm",listString1);
//		map.put("page", page);
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 安卓   新增或修改保存   入库单
	 */
	@RequestMapping(value = "/saveByAddOrUpdateWAP")
	@ResponseBody
	public Object saveByAddOrUpdateWAP(String chkinmStrjson,String sta) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		//Map<String, Object> dataSupply = new HashMap<String, Object>();
		// 将json字符串转换成jsonObject
		JSONObject jsonObject = JSONObject.fromObject(chkinmStrjson);
		@SuppressWarnings("rawtypes")
		Iterator it = jsonObject.keys();
		// 遍历jsonObject数据，添加到Map对象
		while (it.hasNext()) {
			String key = String.valueOf(it.next());
			Object value = (Object) jsonObject.get(key);
			data.put(key, value);
		}
	  String  positnCode=  data.get("positn").toString();//仓位
	  String  deliverCode=  data.get("deliver").toString();	 // 供应商
	  String  supplyJson=  data.get("supplies").toString();	 // 子表物资等
	  
	  Positn positn=new Positn();
	  positn.setCode(positnCode);
	  Deliver deliver=new Deliver();
	  deliver.setCode(deliverCode);
	  List<Chkind> chkindList=new ArrayList<Chkind>();
	   
	  JSONArray jsonObjectSupply= JSONArray.fromObject(supplyJson) ;
	   for (int j = 0; j < jsonObjectSupply.size(); j++) {
		   JSONObject   jsonobjec=(JSONObject)jsonObjectSupply.get(j);
			  Chkind chkind=new Chkind();
			  Supply supply=new Supply();
			  supply.setSp_code(jsonobjec.get("sp_code").toString());
			  chkind.setSupply(supply);
			  chkind.setAmount(Double.parseDouble(jsonobjec.get("sp_count").toString()));
			  chkind.setPrice(Double.parseDouble(jsonobjec.get("price").toString()));
			  chkindList.add(chkind);
	}
		
	  Chkinm  chkinm=new Chkinm();
	  chkinm.setMaded(new Date());
	  chkinm.setPositn(positn);
	  chkinm.setDeliver(deliver);
	  chkinm.setTyp("9900");//正常入库
	  chkinm.setChkindList(chkindList);
	  chkinm.setMemo("盘点机");
	  chkinm.setAcct("1");
	  
	  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		Integer result=0;
		sta="add";
		//当前帐套
		if("add".equals(sta)){
			Integer chkinno = chkinmService.getMaxChkinno();
			chkinm.setChkinno(chkinno);
			chkinm.setInout("in");
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN, chkinm.getMaded()));//从新获得单号
			map.put("chkinno", chkinno+"");
			result=chkinmService.saveChkinm(chkinm,"in");
		}else{
			Chkinm chkinm1 = chkinmService.findChkinmByid(chkinm);
			chkinm.setVouno(chkinm1.getVouno());
			chkinm.setTyp(chkinm1.getTyp());
			chkinm.setDeliver(chkinm1.getDeliver());
			chkinm.setInout("in");
			chkinm.setMadeby(chkinm1.getMadeby());
			result=chkinmService.updateChkinm(chkinm);
			map.put("chkinno", chkinm.getChkinno()+"");
		}
		map.put("pr", result+"");
		return JSONObject.fromObject(map).toString();
	}
}
