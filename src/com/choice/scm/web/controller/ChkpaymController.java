package com.choice.scm.web.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkpaymConstants;
import com.choice.scm.domain.Chkpayd;
import com.choice.scm.domain.Chkpaym;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.service.ChkpaydService;
import com.choice.scm.service.ChkpaymService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;
/**
 * 报销单新增、删除、修改、查询、打印
 * @author csb
 *
 */
@Controller
@RequestMapping("/chkpaym")
public class ChkpaymController {
		@Autowired
		private ChkpaymService chkpaymService;
		@Autowired
		private ChkpaydService chkpaydService;
		@Autowired
		private PositnService positnService;
		@Autowired
		private CalChkNum calChkNum;
		@Autowired
		private PositnRoleService positnRoleService;
		
		Integer chkno=null;
		/**
		 * 报销单主页面
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("table")
		public ModelAndView table(ModelMap modelMap,Page page,HttpSession session) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			return new ModelAndView(ChkpaymConstants.TABLE_CHKPAYM,modelMap);
		}
		
		/**
		 * 添加操作时候，刷新页面
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping(value = "/addChkpaym")
		public ModelAndView addChkpaym(ModelMap modelMap,Page page,HttpSession session,Chkpaym chkpaym,Positn positn) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			//当前登录用户
			String accountName=session.getAttribute("accountName").toString();
			chkpaym.setMadeby(accountName);
			chkpaym.setBdat(new Date());
			chkpaym.setEdat(new Date());
			chkpaym.setChkno(chkpaymService.getMaxChkno());
			chkpaym.setVouno(calChkNum.getNext(CalChkNum.CHKPAY,new Date()));
			//positn.setCode("1000");
			chkpaym.setPositn(positn);
			modelMap.put("chkpaym", chkpaym);
			modelMap.put("sta", "add");
			//仓位列表
			String acct=session.getAttribute("ChoiceAcct").toString();
			String accountId=session.getAttribute("accountId").toString();
			modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));
			//modelMap.put("positnList",positnService.findPositnOut());
			return new ModelAndView(ChkpaymConstants.TABLE_CHKPAYM,modelMap);
		}	
		
		/**
		 * 新增单据
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("saveByAdd")
		@ResponseBody
		public Object saveByAdd(ModelMap modelMap,HttpSession session,String typeCode2,Typ typ,Chkpaym chkpaym) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> map=new HashMap<String,Object>();
			Date maded1=new Date();
			String maded2=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss");
			chkpaym.setMaded(maded1);
			chkpaym.setMadet(maded2);
			typ.setCode(typeCode2);
			chkpaym.setTyp(typ);
			chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
			Chkpayd chkpayd=new Chkpayd();
			chkpayd.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<String> chkpaymList=new ArrayList<String>();
			//当前单号
			chkpaymList.add(String.valueOf(chkpaym.getChkno()));
			map.put("chkpaym", chkpaym);
			map.put("chkpayd", chkpayd);
			map.put("chkpaymList", chkpaymList);
			map.put("chkpaydList", chkpaymList);
			//返回
			return chkpaymService.saveNewChkpay(chkpaym,map);
		}
		
		/**
		 * 关键字查询
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("searchByKey")
		public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String init,Chkpaym chkpaym) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
			HashMap<Object, Object> chkpaymMap=new HashMap<Object, Object>();
			if(null!=init && !"".equals(init)){
				chkpaym.setBdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
				chkpaym.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			}
			chkpaymMap.put("chkpaym", chkpaym);
			if(null!=chkpaym.getPositn() && !"".equals(chkpaym.getPositn())){
				chkpaymMap.put("positn", chkpaym.getPositn().getCode());
			}
			modelMap.put("chkpaymList", chkpaymService.findByKey(chkpaymMap,page));
			modelMap.put("pageobj", page);
			modelMap.put("chkpaym", chkpaym);
			modelMap.put("positnList", positnService.findPositnOut());
			return new ModelAndView(ChkpaymConstants.SEARCH_CHKPAYM,modelMap);
		}
		
		/**
		 * 双击查找
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("findChk")
		public ModelAndView findChk(ModelMap modelMap,Page page,HttpSession session,String chkno1,Chkpayd chkpayd,Chkpaym chkpaym) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			if(chkno1!=null && chkno1!=""){
				chkno=Integer.parseInt(chkno1);
				chkpayd.setChkno(Integer.parseInt(chkno1));
				chkpayd.setAcct(session.getAttribute("ChoiceAcct").toString());
				chkpaym.setChkno(Integer.parseInt(chkno1));
				chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
			}
			modelMap.put("sta", "show");
			modelMap.put("chkpaym", chkpaymService.findByChkno(chkpaym));
			modelMap.put("positnList", positnService.findPositnOut());
			modelMap.put("chkpaydList", chkpaydService.findByChkno(chkpayd));
			return new ModelAndView(ChkpaymConstants.TABLE_CHKPAYM,modelMap);
		}
		
		
		/**
		 * 删除报销单，查找页面上的批量删除
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("deleteMoreChkpaym")
		@ResponseBody
		public Object deleteMoreChkpaym(ModelMap modelMap,HttpSession session,Page page,String chkno2, Chkpaym chkpaym) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Map<String,Object> map=new HashMap<String,Object>();
			List<String> chkpaymList=Arrays.asList(chkno2.split(","));
			
			//wangjie 2014年11月20日 09:22:45 （查询报销单  支持批量删除）
//			chkpaym.setChkno(chkno2!=null && chkno2!=""?Integer.parseInt(chkno2):0);
			chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
			Chkpayd chkpayd=new Chkpayd();
			chkpayd.setAcct(session.getAttribute("ChoiceAcct").toString());
			map.put("chkpayd", chkpayd);
			map.put("chkpaym", chkpaym);
			map.put("chkpaydList", chkpaymList);
			map.put("chkpaymList", chkpaymList);
			modelMap.put("pageobj", page);
			modelMap.put("chkpaym", chkpaym);
			modelMap.put("positnList", positnService.findPositnOut());
			return chkpaymService.deleteMoreChkpay(map);
		}

		/**
		 * 删除报销单，编辑页面上的整条删除
		 * @param modelMap
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("deleteOneChkpaym")
		@ResponseBody
		public Object deleteOneChkpaym(ModelMap modelMap,HttpSession session,Page page,String chkno2,Chkpaym chkpaym) throws Exception
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
			if(chkno2!=null && chkno2!=""){
				chkpaym.setChkno(Integer.parseInt(chkno2));
			}
			String accountName=session.getAttribute("accountName").toString();
			modelMap.put("accountName", accountName);
			modelMap.put("positnList",positnService.findPositnOut());
			return chkpaymService.deleteOneChkpay(chkpaym);
		}		
		
		/**
		 * 打印
		 * @param modelMap
		 * @return
		 * @throws CRUDException
		 */
		@RequestMapping(value = "/printChkpaym")
		public ModelAndView printChkpaym(ModelMap modelMap,HttpSession session,Page page,Supply supply,String chkno,Date bdat,Date edat,String type)throws CRUDException
		{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
	        String report_name=new String("入库审核汇总报销单");
	        parameters.put("report_name", report_name); 
	        parameters.put("report_date", DateFormat.formatDate(new Date(), "yyyy-MM-dd")); 
	        Chkpaym chkpaym=new Chkpaym();
	        chkpaym.setChkno(Integer.parseInt(chkno));
	        chkpaym.setBdat(bdat);
			chkpaym.setEdat(edat);
			chkpaym.setAcct(session.getAttribute("ChoiceAcct").toString());
	        List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
	        Map<String,Object> map=new HashMap<String,Object>();
	        map=chkpaydService.printChkpay(chkpaym);
	        list.add(map);
		    modelMap.put("List", list);
		    modelMap.put("parameters", parameters);
		 	modelMap.put("action", "/chkpaym/printChkpaym.do");//传入回调路径
		 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkpaymConstants.REPORT_PRINT_URL,ChkpaymConstants.REPORT_PRINT_URL);//判断跳转路径
	        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
			return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
			//return new ModelAndView(rs.get("url"),modelMap);
		}
}
