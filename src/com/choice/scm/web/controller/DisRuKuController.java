package com.choice.scm.web.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisFenBoService;
import com.choice.scm.service.DisRuKuService;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "disRuKu")
/**
 * 验收入库
 * @author csb
 *
 */
public class DisRuKuController {

	@Autowired
	private DisRuKuService disRuKuService;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private DisFenBoService disFenBoService;
	@Autowired
	private LogsMapper logsMapper;
	/*******************************************验收入库start***********************************************/
	/**
	 * 验收入库主界面
	 */
	@RequestMapping(value = "/listCheckin.do")
	public ModelAndView findAllCheckin(ModelMap modelMap,HttpSession session,Page page,Dis dis,String action,String ind1,Date maded1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"验收入库查询 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, +1);    //得到前一天
		Date date = calendar.getTime();

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
//		}
		if(null!=action && !"".equals(action)){
			if(null==dis.getBdat()){dis.setBdat(new Date());};
			if(null==dis.getEdat()){dis.setEdat(date);};
			modelMap.put("maded1", new Date());
		}else{
			if("1".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"))){//报货分拨下查询是否需要按照账号物资权限筛选-验收入库
				dis.setAccountId(session.getAttribute("accountId").toString());
			}
			List<Dis> disList=disRuKuService.findAllDis(dis,ind1,page);
			if(null!=maded1){modelMap.put("maded1", maded1);}
			else{modelMap.put("maded1", new Date());}
			modelMap.put("disList", disList);
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		modelMap.put("action", action);//判断是否初始进入页面 init初始 为空则是查询等操作
		return new ModelAndView(DisConstants.TABLE_CHECKIN_NEW,modelMap);
	}
	/**
	 * 生成入库单
	 */
	@RequestMapping(value = "/saveYsrkChkinm.do")
	public ModelAndView saveYsrkChkinm(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,Date maded1,String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"生成入库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);    //得到前一天

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
//		}
		List<Dis> disList=disRuKuService.findAllDis(dis,ind1,null);//如果page为空，则不分页  查询所有的2014.10.24wjf
		//报货单转入库单方法
		String  accountName=session.getAttribute("accountName").toString();//当前用户
		if(disList.size()!=0){
			chkinmService.saveYsrkChkinm(disList,acct,dis.getPositnCode(),maded1!=null?maded1:new Date(),accountName);
			str=1;//不为空，成功
		}else{
			str=2;//为空，不能进行入库
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.TABLE_CHECKIN_NEW,modelMap);
	}
	
	/**
	 * 入库单打印
	 */
	@RequestMapping(value = "/viewYsrkChkstom")
	public ModelAndView viewYsrkChkstom(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"入库单打印 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("memo1", dis.getMemo1());
		param.put("chkin", "N");
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
//		param.put("inout", "入库,直发");
		param.put("inout", "in");//改为只查询入库方向，不查询直发  2014.11.20 wjf
		param.put("psarea", dis.getPsarea());
		param.put("checkinSelect", dis.getCheckinSelect());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disRuKu/viewYsrkChkstom.do");//传入回调路径
		//根据关键字查询

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
//		}
		List<Dis> disList=disRuKuService.findAllDisForReport(dis,ind1);
		for(Dis di:disList){
			//调整备注和附加项
			String memo0 = null == di.getMemo()?"":di.getMemo();
	    	String memo1 = "";
	    	if(memo0.contains("##")){
	    		memo1 = memo0.split("##")[1];
	    		memo0 = memo0.split("##")[0];
	    	}
	    	di.setMemo(memo0);
	    	di.setMemo1(memo1);
		}
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("物资入库明细表");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_YSRK_URL,DisConstants.REPORT_YSRK_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}	
	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap,HttpSession session,DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return disFenBoService.updateDis(disList);
	}	
	/*******************************************验收入库end****************************************************/
}