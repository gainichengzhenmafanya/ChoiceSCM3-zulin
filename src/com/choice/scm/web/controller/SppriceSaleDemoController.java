package com.choice.scm.web.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.SppriceSaleDemoConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.SppriceSaleDemo;
import com.choice.scm.domain.SppriceSaleDemoList;
import com.choice.scm.domain.SppriceSaleList;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceSaleDemoService;
/**
 * 售价单模板、售价单
 * @author Administrator
 *
 */
@Controller
@RequestMapping("sppriceSaleDemo")
public class SppriceSaleDemoController {

	@Autowired
	SppriceSaleDemoService sppriceSaleDemoService;
	@Autowired
	GrpTypService grpTypService;
	@Autowired
	PositnService positnService;
	@Autowired
	private LogsMapper logsMapper;
		
	/**
	 * 售价单模板左侧列表
	 * @param modelMap
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn=new Positn();
		positn.setTyp("1203");
		modelMap.put("firmList", positnService.findAllPositn(positn));
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleDemoConstants.TABLE_SPPRICE_SALE_DEMO,modelMap);
	}
	/**
	 * 查询售价单模板
	 * @param modelMap
	 * @param page
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSppriceSaleDemo")
	public ModelAndView findSppriceSaleDemo(ModelMap modelMap,Page page,SppriceSaleDemo sppriceSaleDemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("firm", sppriceSaleDemo.getFirm());
		modelMap.put("sppriceSaleDemoList", sppriceSaleDemoService.findSppriceSaleDemo(sppriceSaleDemo, page));
		modelMap.put("sppriceSaleDemo", sppriceSaleDemo);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleDemoConstants.LIST_SPPRICE_SALE_DEMO,modelMap);
	}
	/**
	 * 查询1203
	 * @param modelMap
	 * @param page
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findFirm")
	public ModelAndView findFirm(ModelMap modelMap,Page page,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setTyp("1203");
		modelMap.put("firmList", positnService.findAllPositn(positn));
		modelMap.put("key", null == positn.getCode() ? positn.getInit() : positn.getCode());
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleDemoConstants.TABLE_SPPRICE_SALE_DEMO,modelMap);
	}
	/**
	 * 打开新增售价单模板页面
	 */
	@RequestMapping(value = "/toAddSppriceSaleDemo")
	public Object toAddSppriceSale(ModelMap modelMap,Page page,String firm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("firm", firm);
		modelMap.put("bdat", DateFormat.getDateBefore(new Date(),"day",1,1));
		modelMap.put("edat", DateFormat.getDateBefore(new Date(),"day",1,8));
		return new ModelAndView(SppriceSaleDemoConstants.ADD_SPPRICESALEDEMO,modelMap);
	}
	/**
	 * 打开修改售价单模板页面
	 * @param modelMap
	 * @param page
	 * @param firm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toUpdateSppriceSaleDemo")
	public Object toUpdateSppriceSaleDemo(ModelMap modelMap,Page page,SppriceSaleDemo sppriceSaleDemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sppriceSaleDemo", sppriceSaleDemoService.findSppriceSaleDemoOne(sppriceSaleDemo));
		return new ModelAndView(SppriceSaleDemoConstants.UPDATE_SPPRICESALEDEMO,modelMap);
	}
	/**
	 * 添加新售价单模板
	 */
	@RequestMapping(value = "/addSppriceSaleDemo")
	public ModelAndView addSppriceSaleDemo(ModelMap modelMap,Page page,HttpSession session,SppriceSaleDemo sppriceSaleDemo,String h_sp_code,String h_sp_name) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceSaleDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		String pr=sppriceSaleDemoService.saveByAddSppriceSaleDemo(sppriceSaleDemo,h_sp_code,h_sp_name);
		if(pr.equals("2")){
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		}else{
			modelMap.put("msg", "数据已存在，添加失败！");
			return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
		}
	}
	/**
	 * 保存修改
	 */
	@RequestMapping(value = "/saveSppriceSaleDemo")
	public ModelAndView saveSppriceSaleDemo(ModelMap modelMap,HttpSession session,SppriceSaleDemo sppriceSaleDemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceSaleDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		sppriceSaleDemoService.saveByUpdateSppriceSaleDemo(sppriceSaleDemo);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 批量添加售价单模板
	 */
	@RequestMapping(value = "/addSppriceSaleDemoMore")
	@ResponseBody
	public Object addSppriceSaleDemoMore(ModelMap modelMap,Page page,HttpSession session,SppriceSaleDemo sppriceSaleDemo,String h_sp_code,String h_sp_name) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceSaleDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		return sppriceSaleDemoService.saveByAddSppriceSaleDemoMore(sppriceSaleDemo,h_sp_code,h_sp_name);
	}
	/**
	 * 添加新售价单
	 */
	@RequestMapping(value = "/addSppriceSale")
	@ResponseBody
	public Object addSppriceSale(ModelMap modelMap,Page page,HttpSession session,SppriceSaleDemo sppriceSaleDemo,SppriceSaleDemoList sppriceSaleDemoList,String type,String firm,String all) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		sppriceSaleDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		map.put("acct", session.getAttribute("ChoiceAcct").toString());
		if("Y".equals(all)){   //添加新售价单    全部
			sppriceSaleDemoList.setSppriceSaleDemoList(sppriceSaleDemoService.findSppriceSaleDemoNoPage(sppriceSaleDemo));
			map.put("sppriceSaleDemoList", sppriceSaleDemoList);
		}else{
			map.put("sppriceSaleDemoList", sppriceSaleDemoList);
		}
		map.put("emp", session.getAttribute("accountName").toString());
		map.put("firm", firm);
		map.put("sppriceSaleDemo", sppriceSaleDemo);
		return sppriceSaleDemoService.saveByAddSppriceSale(map);
	}

	/**
	 * 批量修改日期
	 * @param modelMap
	 * @param sppriceSaleDemoList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveByUpdateBatch")
	public ModelAndView saveByUpdateBatch(ModelMap modelMap,SppriceSaleDemoList sppriceSaleDemoList,SppriceSaleList sppriceSaleList,String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=type && !"".equals(type)){
			sppriceSaleDemoService.updateSppriceSaleBatch(sppriceSaleList);
		}else{
			sppriceSaleDemoService.updateSppriceSaleDemoBatch(sppriceSaleDemoList);
		}
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 删除售价单模板
	 * @param modelMap
	 * @param page
	 * @param sppriceSaleDemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(ModelMap modelMap,Page page,SppriceSaleDemo sppriceSaleDemo,HttpSession session) throws Exception
	{
		//根据单号和TYP进行删除操作
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> spCodeList=Arrays.asList(sppriceSaleDemo.getSupply().getSp_code().split(","));
		List<String> firmList=Arrays.asList(sppriceSaleDemo.getFirm().split(","));
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"售价单模版删除：物资编码("+sppriceSaleDemo.getSupply().getSp_code()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		return sppriceSaleDemoService.delete(spCodeList,firmList);
	}
	/**
	 * 售价审核页面
	 * @param modelMap
	 * @param page
	 * @param sppricesale
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/check")
	public ModelAndView check(ModelMap modelMap,Page page,SppriceSale sppriceSale,String action) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("firmList", positnService.findAllPositn(null));
		if("init".equals(action)){
			sppriceSale = new SppriceSale();
		}
		modelMap.put("sppriceSale", sppriceSale);
		modelMap.put("sppriceSaleList", sppriceSaleDemoService.findSppriceSale(sppriceSale,page));
		return new ModelAndView(SppriceSaleDemoConstants.SPPRICE_SALE_CHECK,modelMap);
	}
	/**
	 * 审核售价单
	 * @param modelMap
	 * @param sppriceSale
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkSppriceSale")
	@ResponseBody
	public Object checkSppriceSale(ModelMap modelMap,HttpSession session,SppriceSale sppriceSale,String type,String checkAll) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"售价审核",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		if(null!=checkAll && !"".equals(checkAll) && "checkAll".equals(checkAll)){
			sppriceSale.setChecby(session.getAttribute("accountName").toString());
			sppriceSale.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			return sppriceSaleDemoService.checkAll(sppriceSale);
		}else{
			sppriceSale.setChecby(session.getAttribute("accountName").toString());
			sppriceSale.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			return sppriceSaleDemoService.checkSppriceSale(sppriceSale,type);
		}
	}
	
	/**
	 * 全部反审核售价单
	 * @param modelMap
	 * @param sppriceSale
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/reverseCheckSppriceSale")
	@ResponseBody
	public Object reverseCheckSppriceSale(ModelMap modelMap,HttpSession session,SppriceSale sppriceSale) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceSale.setChecby(session.getAttribute("accountName").toString());
		sppriceSale.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		return sppriceSaleDemoService.reverseCheckSppriceSale(sppriceSale);
	}
	
	/**
	 * 查询物资售价
	 */
	@RequestMapping(value = "/findSupplySppriceSale")
	public ModelAndView findSupplySppriceSale(ModelMap modelMap,Page page,HttpSession session,SppriceSale sppriceSale,String action,String nodate,String firmDes,String typDes)throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=action && !"".equals(action) && "init".equals(action)){
			sppriceSale.setBdat(new Date());
			sppriceSale.setEdat(new Date());
			sppriceSale.setSta("Y");
			modelMap.put("sppriceSale", sppriceSale);
			modelMap.put("grptypList", grpTypService.findAllTypA(session.getAttribute("ChoiceAcct").toString(),null));
		}else{
			List<SppriceSale> sppriceSaleList = sppriceSaleDemoService.findSppriceSaleByReport(sppriceSale,nodate,page);
			modelMap.put("sppriceSaleList", sppriceSaleList);
			modelMap.put("grptypList", grpTypService.findAllTypA(session.getAttribute("ChoiceAcct").toString(),null));
			modelMap.put("sppriceSale", sppriceSale);
			modelMap.put("firmDes", firmDes);
			modelMap.put("typDes", typDes);
			modelMap.put("nodate", nodate);
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleDemoConstants.LIST_SPPRICESALE,modelMap);
	}
	/**
	 * 删除未审核的报价
	 */
	@RequestMapping(value = "/deleteUncheckSpprice")
	@ResponseBody
	public Object deleteUncheckSpprice(ModelMap modelMap,Page page,SppriceSale sppriceSale) throws Exception
	{
		//根据单号和TYP进行删除操作
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return sppriceSaleDemoService.deleteUncheckSpprice(sppriceSale);
	}
	
	/**
	 * 跳转到导入页面   售价单导入 wjf
	 */
	@RequestMapping("/importSppriceSale")
	public ModelAndView importSppriceSale(ModelMap modelMap) throws Exception{
		return new ModelAndView(SppriceSaleDemoConstants.IMPORT_SPPRICESALE,modelMap);
	}
	
	/**
	 * 下载模板信息 入库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		sppriceSaleDemoService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = sppriceSaleDemoService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(SppriceSaleDemoConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入报货单  wjf
	 */
	@RequestMapping(value = "/importExcel")
	@ResponseBody
	public List<String> importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountId").toString();
		String acct = session.getAttribute("ChoiceAcct").toString();
		String madeby = session.getAttribute("accountName").toString();
		return sppriceSaleDemoService.check(realFilePath, accountId,acct,madeby);
	}
}
