package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.domain.FirmDept;
import com.choice.scm.service.FirmDeptService;
/**
 * 分店部门的查询
 * @author csb
 *
 */
@Controller
@RequestMapping("firmDept")
public class FirmDeptController {

	@Autowired
	private FirmDeptService firmDeptService;
	/**
	 * 查找分店部门
	 * @param modelMap
	 * @param session
	 * @param firmDept
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findFirmDept")
	@ResponseBody
	public Object findFirmDept(ModelMap modelMap,HttpSession session,FirmDept firmDept) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=firmDept.getFirm() && !"".equals(firmDept.getFirm())){
			return firmDeptService.findFirmDept(firmDept);
		}else{
			return null;
		}
	}
}
