package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SppriceSaleQuickConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceSaleDemoService;
import com.choice.scm.service.SppriceSaleQuickService;

@Controller
@RequestMapping("sppriceSaleQuick")
public class SppriceSaleQuickController {
	@Autowired
	private SppriceSaleQuickService sppriceSaleQuickService;
	@Autowired
	PositnService positnService;
	@Autowired
	SppriceSaleDemoService sppriceSaleDemoService;
	
		
	/**
	 * 售价单模板左侧列表
	 * @param modelMap
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn=new Positn();
		positn.setTyp("1203");
		modelMap.put("firmList", positnService.findAllPositn(positn));
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleQuickConstants.SPPRICESALEQUICK_LIST,modelMap);
	}

	/**
	 * 售价快速加入 跳转
	 * @throws Exception 
	 */
	@RequestMapping("findFirm")
	public ModelAndView list(ModelMap modelMap,Page page,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setTyp("1203");
		modelMap.put("firmList", positnService.findAllPositn(positn));
		modelMap.put("key", null == positn.getCode() ? positn.getInit() : positn.getCode());
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleQuickConstants.SPPRICESALEQUICK_LIST,modelMap);
	}
	/**
	 * 查询售价单模板
	 * @param modelMap
	 * @param page
	 * @param action
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSppriceSaleQuick")
	public ModelAndView findSppriceSaleQuick(ModelMap modelMap,Page page,SppriceSale sppriceSale) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("firm", sppriceSale.getDeliver().getCode());
		modelMap.put("sppriceSaleList", sppriceSaleQuickService.findSppriceSale(sppriceSale, page));//修改为取sppricesail 表
		modelMap.put("sppriceSale", sppriceSale);
//		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceSaleQuickConstants.SPPRICESALEDEMO_LIST_FIRMID,modelMap);
	}
	
	/**
	 * 根据id获取物资信息并返回json对象
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Supply supply,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		return sppriceSaleQuickService.findSupplyById(supply);
	}
	
	/**
	 * 快速 批量插入售价单 并审核
	 */
	@RequestMapping("saveSppriceSaleQuick")
	@ResponseBody
	public String saveSppriceSaleQuick(HttpSession session,SppriceSale sppriceSale) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
 		return sppriceSaleQuickService.saveSppriceSaleQuick(session,sppriceSale);
	}
}
