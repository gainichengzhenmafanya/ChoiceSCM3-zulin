package com.choice.scm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.MonitConstants;
import com.choice.scm.service.MonitService;

/**
 * 帐套管理相关
 * @author lq 
 *
 */
@Controller
@RequestMapping("monit")
public class MonitController {

	@Autowired
	MonitService monitService;
		
	/**
	 * 转到测试网络状况页面
	 * @param modelMap
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView listMonit(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(MonitConstants.LIST_MONIT,modelMap);
	}
	
	/**
	 * 查询数据(站点网络监控)
	 * @param modelMap
	 * @param firm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getMonit")
	@ResponseBody
	public Object getMonit(ModelMap modelMap)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		return monitService.findMonit();
	}
}
