package com.choice.scm.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyConstants;
import com.choice.scm.service.CommonService;

@Controller
@RequestMapping(value = "common")
public class CommonController {

	@Autowired
	private CommonService commonService;

	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = commonService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(SupplyConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	@ResponseBody
	public List<String> importExcel(ModelMap modelMap,@RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listError=commonService.check(realFilePath);
		return listError;
	}
	/**
	 * 下载模板信息
	 */
	@RequestMapping(value = "/download")
	public void downloadTemplate(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		commonService.downloadTemplate(response, request,ScmStringConstant.REPORT_FILE_NAME,ScmStringConstant.REPORT_FILE_ADDRESS);
	}
	
}
