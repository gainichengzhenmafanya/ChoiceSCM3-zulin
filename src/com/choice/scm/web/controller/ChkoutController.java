package com.choice.scm.web.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.AcctConstants;
import com.choice.scm.constants.ChkoutConstants;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkoutService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.ShipAndReceivedService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 出库单
 * 
 * @author yp
 */

@Controller
@RequestMapping("/chkout")
public class ChkoutController {

	@Autowired
	private ChkoutService chkoutService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private Chkoutm chkoutm;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private ShipAndReceivedService shipAndReceivedService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private AcctService acctService;
	/**
	 * 查找已审核出库单详细信息
	 * 
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("searchCheckedChkout")
	public ModelAndView searchCheckedChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查找已审核出库单详细信息",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				supplyAcct.setPositn(chkoutm.getPositn().getCode());
				supplyAcct.setSp_code(ckd.getSp_code().getSp_code());
				ckd.getSp_code().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}
		String locale = session.getAttribute("locale").toString();//语言类型
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("CK");
		modelMap.put("chkoutm", chkoutm);
		
		//单据类型
		modelMap.put("listBill", codeDesService.findDocumentType(codeDes));
		// modelMap.put("positnIn", positnService.findPositnIn());
		// modelMap.put("positnOut", positnService.findPositnOut());
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutConstants.SEARCH_CHECKED_CHKOUT, modelMap);
	}

	/**
	 * 通过id获取出库单详细信息
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @author yp
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getChkoutById")
	public ModelAndView findChkoutById(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", chkoutService.findChkoutmById(chkoutm));
		// modelMap.put("positnIn", positnService.findPositnIn());
		// modelMap.put("positnOut", positnService.findPositnOut());
		return new ModelAndView(ChkoutConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 模糊查询出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,
			String action, String startDate, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"模糊查询出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
		} else {
			if (null != startDate && !"".equals(startDate)) {
				modelMap.put("bdat",
						DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
				modelMap.put("edat", new Date());
				bdat = DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			} else {
				modelMap.put("bdat", bdat);
				modelMap.put("edat", edat);
			}
		}
		modelMap.put("chkoutList",
				chkoutService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString()));
		if (null != chkoutm.getChecby() && chkoutm.getChecby().equals("c")) {
			return new ModelAndView(ChkoutConstants.CHECKED_CHKOUT, modelMap);
		} else {
			modelMap.put("chkoutm", chkoutm);
			return new ModelAndView(ChkoutConstants.MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String typ = "13";
		CodeDes  codes = new CodeDes();		
		codes.setTyp(typ);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		//查询配置文件，查看是否配置了出库单可以修改售价
		//update by jinshuai at 20160411
		String chkoutCanUpdatesppricesale = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "chkout_can_updatesppricesale");
		boolean isTrue = (chkoutCanUpdatesppricesale!=null&&chkoutCanUpdatesppricesale.trim().equalsIgnoreCase("Y"))?true:false;
		if (action == null || !action.equals("init")) {
			chkoutm.setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setChkoutno(chkoutService.findNextNo().intValue());
//			String acct = session.getAttribute("ChoiceAcct").toString();
			String accountId = session.getAttribute("accountId").toString();
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp("CK");
			// modelMap.put("positnIn", positnService.findPositnIn());
//			modelMap.put("positnIn", positnService.findPositnUse());// 使用仓位下拉条
			//单据类型隐藏-部门调拨-类型 code为9950
			List<CodeDes> billTyps = codeDesService.findDocumentType(codeDes);
			if(billTyps!=null){
				Iterator<CodeDes> iter = billTyps.iterator();
				while(iter.hasNext()){
					CodeDes cd = iter.next();
					if(cd!=null&&"9950".equals(cd.getCode())){
						iter.remove();
						break;
					}
				}
			}
			modelMap.put("billType", billTyps);
			modelMap.put("positnIn", positnService.findChkoutPositnIn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			// modelMap.put("positnOut", positnService.findPositnOut());
			// modelMap.put("positnOut", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
//			modelMap.put("positnOut", positnService.findPositnCangwei());// 仓位下拉条
			modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1202','1204'"));//加工间也可以出库!勿动！ 2014.12.15wjf
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
			//修改售价的配置
			modelMap.put(chkoutCanUpdatesppricesale,isTrue);
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(ChkoutConstants.ADD_CHKOUT, modelMap);
	}

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus, String scrapped,String scrappedname) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String,String> resul = new HashMap<String,String>();
		String result = null;
		//加判断：如果是报废，则memo取scrapped 和scrappedname ；如果是其他则不取   wjf
		try{
			acctService.getOnlyAccountMonth2(chkoutm.getMaded());//判断制单日期会计日
		}catch(Exception e1){
			resul.put("pr", "-3");
			JSONObject rs = JSONObject.fromObject(resul);
			result = rs.toString();
			return result;
		}
		if("9915".equals(chkoutm.getTyp())){
			String memo = scrapped+'-'+scrappedname;		
			chkoutm.setMemo(memo);
		}
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (curStatus.equals("add")) {
			result = chkoutService.saveChkoutm(chkoutm);
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"添加出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"更新出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			result = chkoutService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"删除出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkoutService.deleteChkoutm(chkoutm);
		// return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				supplyAcct.setPositn(chkoutm.getPositn().getCode());
				supplyAcct.setSp_code(ckd.getSp_code().getSp_code());
				ckd.getSp_code().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}		
		//查询配置文件，查看是否配置了出库单可以修改售价
		//update by jinshuai at 20160411
		String chkoutCanUpdatesppricesale = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "chkout_can_updatesppricesale");
		boolean isTrue = (chkoutCanUpdatesppricesale!=null&&chkoutCanUpdatesppricesale.trim().equalsIgnoreCase("Y"))?true:false;	
		modelMap.put("chkoutCanUpdatesppricesale",isTrue );
		modelMap.put("chkoutm",chkoutm );
		// modelMap.put("positnOut", positnService.findPositnOut());
//		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountId = session.getAttribute("accountId").toString();
		modelMap.put("positnIn", positnService.findChkoutPositnIn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
		modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1202','1204'"));//加工间也可以出库!勿动！ 2014.12.15wjf
		modelMap.put("curStatus", "show");
		//查询的时候也要查询报废原因  wjf
		String typ = "13";
		CodeDes  codes = new CodeDes();		
		codes.setTyp(typ);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setTyp("18");
		codes.setCodetyp("CK");
		modelMap.put("billType", codeDesService.findDocumentType(codes));
		return new ModelAndView(ChkoutConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 审核出库单
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("auditChkout")
	@ResponseBody
	public Object auditChkout(ModelMap modelMap, Chkoutm chkoutm,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		try {
			acctService.getOnlyAccountMonth(chkoutm.getMaded());//  此字段当成月份使用    加入会计月 
		} catch (Exception e) {
			// TODO: handle exception
			 if(e.getMessage().equals(AcctConstants.NOT_NIAN_JIE_ZHUAN)){
				System.out.println("没有年结转！！请先停止使用，联系辰森研发人员！");
				Map<String,String> result = new HashMap<String,String>();
				result.put("pr", "-9");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			 }
			 if(e.getMessage().equals(AcctConstants.CAN_NOT_CHECK_BEFORE_DATA)){
				System.out.println("不能操作当前会计期之前的数据！");
				Map<String,String> result = new HashMap<String,String>();
				result.put("pr", "-44");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			 }
		}
		
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"审核出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		try {
//			return chkoutService.updateByAudit(chkoutm, session.getAttribute("accountName").toString());
			String chk1memo = chkoutm.getChk1memo();//审核备注  wjf
//			chkoutm = chkoutService.findChkoutmById(chkoutm);
			chkoutm.setChk1memo(chk1memo);
		    String chkectby=session.getAttribute("accountName").toString();
			chkoutm.setChecby(chkectby);
			synchronized(this){
				return chkoutService.updateByAudit(chkoutm,chkectby);  //修改洞庭反应的问题2014.11.21
			}
		} catch (Exception e) {
			return e.getLocalizedMessage();
		}
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"出库单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutService.findChkoutmById(chkoutm);
		
		//wangjie 2014年11月5日 10:12:51 （伊天园，验收出库，显示的是售价，但是打印显示的是报价。不一致。）
//		for(Chkoutd chkd: chkoutm.getChkoutd()){
//			chkd.setTotalamt(chkd.getPriceamt());
			//chkd.setPrice(chkd.getPricesale());
//		}
		
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//		parameters.put("typ", chkoutm.getTyp());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action",
				"/chkout/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutConstants.REPORT_PRINT_URL,
				ChkoutConstants.REPORT_PRINT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 获取出库单单号
	 * 
	 * @param maded
	 * @return
	 * @throws ParseException
	 * @author yp
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded) throws ParseException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return calChkNum.getNext(CalChkNum.CHKOUT,
				DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}

	/**
	 * 操作成功页面跳转
	 * 
	 * @return
	 * @author yp
	 */
	@RequestMapping("success")
	public ModelAndView success() {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param modelMap
	 * @param session
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value = "/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session,
			String active, Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		return chkoutService.chkChect(active, chkoutm);
	}

	/**
	 * 冲消查询界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkoutByCx")
	public ModelAndView addChkoutByCx(ModelMap modelMap, Page page,
			HttpSession session, Spbatch spbatch) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if("show".equals(spbatch.getSp_desc())){//使用规格字段判断是否需要查询数据
			modelMap.put("spbatchList", chkoutService.findChkoutByCx(spbatch));// 获取出库冲销数据
		}else{
			spbatch.setBdat(DateFormat.getDateBefore(new Date(), "day", -1, 30));// 一个月以内的数据
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkoutConstants.TABLE_CHKOUT_CX, modelMap);
	}
	
	/**
	 * 冲消批次界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkoutByPc")
	public ModelAndView findChkoutByPc(ModelMap modelMap, HttpSession session, Spbatch spbatch) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if("show".equals(spbatch.getSp_desc())){//使用规格字段判断是否需要查询数据
			modelMap.put("spbatchList", chkoutService.findChkoutByPc(spbatch));// 获取出库冲销数据
		}else{
			spbatch.setBdat(DateFormat.getDateBefore(new Date(), "day", -1, 30));// 一个月以内的数据
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkoutConstants.TABLE_CHKOUT_PC, modelMap);
	}
	
	/**
	 * 跳转到导入页面   出库单导入 wjf
	 */
	@RequestMapping("/importChkout")
	public ModelAndView importChkout(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkoutConstants.IMPORT_CHKOUT,modelMap);
	}
	
	/**
	 * 下载模板信息 出库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		chkoutService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkoutService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkoutConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入出库单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"导入出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId = session.getAttribute("accountId").toString();
		Object obj=chkoutService.check(realFilePath, accountId);
		if(obj instanceof Chkoutm){//导入对了
			String typ = "13";
			CodeDes  codes = new CodeDes();		
			codes.setTyp(typ);
			modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
			((Chkoutm) obj).setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			((Chkoutm) obj).setMadeby(session.getAttribute("accountName").toString());
			((Chkoutm) obj).setChkoutno(chkoutService.findNextNo().intValue());
			modelMap.put("positnIn", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("chkoutm", ((Chkoutm) obj));
			modelMap.put("curStatus", "add");
			modelMap.put("importFlag","OK");
			String locale = session.getAttribute("locale").toString();//语言类型
			codes.setLocale(locale);
			codes.setTyp("18");
			codes.setCodetyp("CK");
			modelMap.put("billType", codeDesService.findDocumentType(codes));
			return new ModelAndView(ChkoutConstants.UPDATE_CHKOUT, modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkoutConstants.UPDATE_CHKOUT, modelMap);
		}
	}
	
	
	/*******************************************验货差异处理start 2014.12.19wjf***********************************************/
	
	/***
	 * 查询总部配送和门店验收差异
	 * @author wjf
	 * @param modelMap
	 * @param action
	 * @param supplyAcct
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("listDiff")
	public ModelAndView listDiff(ModelMap modelMap,String action, SupplyAcct supplyAcct,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if (null != action && !"".equals(action)) {
			supplyAcct.setIscy("2");//默认显示分店亏
			supplyAcct.setDat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else{
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			disMap.put("dat", supplyAcct.getDat());
			disMap.put("issk", supplyAcct.getIscy());//分店盈还是分店亏
			disMap.put("positn",CodeHelper.replaceCode(supplyAcct.getPositn()));
			disMap.put("firm",CodeHelper.replaceCode(supplyAcct.getFirm()));
			disMap.put("check", "1");//在这里查已经验货的
			List<SupplyAcct> disList = shipAndReceivedService.findShipAndReceiveList(disMap,page);//查询差异
			modelMap.put("disList",disList);
		}
		modelMap.put("dis",supplyAcct);
		modelMap.put("pageobj", page);
		String accountId = session.getAttribute("accountId").toString();
		modelMap.put("positnIn", positnService.findChkoutPositnIn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
		modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202','1204'"));//加工间也可以出库!勿动！ 2014.12.15wjf
		return new ModelAndView(ChkoutConstants.LIST_DIFF, modelMap);
	}
	
	/***
	 * 查询总部配送和门店验收差异
	 * @author wjf
	 * @param modelMap
	 * @param action
	 * @param supplyAcct
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("dealDiff")
	@ResponseBody
	public String dealDiff(ModelMap modelMap,SupplyAcct supplyAcct,String ids,String typ,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		try{
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			String acct = session.getAttribute("ChoiceAcct").toString();
			String acctName = session.getAttribute("accountName").toString();
			disMap.put("acct", acct);
			disMap.put("dat", supplyAcct.getDat());
			disMap.put("issk", supplyAcct.getIscy());//分店亏
			disMap.put("positn",CodeHelper.replaceCode(supplyAcct.getPositn()));
			disMap.put("firm",CodeHelper.replaceCode(supplyAcct.getFirm()));
			disMap.put("check", "1");//在这里查已经验货的
			disMap.put("ids", ids);//查询当前选择的数据
			List<SupplyAcct> disList = shipAndReceivedService.findShipAndReceiveList(disMap);//查询当前日期下所有差异
			if(disList.size() == 0){//没有要处理的数据，则直接返回提示
				Map<String,Object> result = new HashMap<String,Object>();
				result.put("pr", "-1");
				result.put("msg", "当前条件下没有要处理的数据！");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}else{
				return chkoutService.dealDiff(typ,disList,acct,acctName, supplyAcct,ids);//冲消报损处理
			}
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	/**
	 * 安卓手机    新增或修改保存  出库单
	 */
	@RequestMapping(value = "/saveByAddOrUpdateWAP")
	@ResponseBody
	public Object saveByAddOrUpdateWAP(String chkoutmStrjson,String sta,Chkoutm chkoutm) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		//Map<String, Object> dataSupply = new HashMap<String, Object>();
		// 将json字符串转换成jsonObject
		JSONObject jsonObject = JSONObject.fromObject(chkoutmStrjson);
		@SuppressWarnings("rawtypes")
		Iterator it = jsonObject.keys();
		// 遍历jsonObject数据，添加到Map对象
		while (it.hasNext()) {
			String key = String.valueOf(it.next());
			Object value = (Object) jsonObject.get(key);
			data.put(key, value);
		}
	  String  positnCode=  data.get("positn").toString();
	  String  firmCode=  data.get("firm").toString();	 
	  String  supplyJson=  data.get("supplies").toString();	 //子表拼装
	  Positn positn=new Positn();
	  positn.setCode(positnCode);
	  Positn firm=new Positn();
	  firm.setCode(firmCode);
	  List<Chkoutd> chkoutdList=new ArrayList<Chkoutd>();
	  JSONArray jsonObjectSupply= JSONArray.fromObject(supplyJson) ;
	   for (int j = 0; j < jsonObjectSupply.size(); j++) {//子表得到物资数量及物资编码 
		   JSONObject   jsonobjec=(JSONObject)jsonObjectSupply.get(j);
			  Chkoutd chkoutd=new Chkoutd();
			  Supply supply=new Supply();
			  supply.setSp_code(jsonobjec.get("sp_code").toString());
			  chkoutd.setSp_code(supply);
			  chkoutd.setAmount(Double.parseDouble(jsonobjec.get("sp_count").toString()));
			  chkoutdList.add(chkoutd);
	   }
	  chkoutm.setMaded(new Date());
	  chkoutm.setPositn(positn);
	  chkoutm.setFirm(firm);
	  chkoutm.setChkoutd(chkoutdList);
	  chkoutm.setMemo("盘点机");
	  chkoutm.setAcct("1");
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		String result="0";
		sta="add";
		//当前帐套
		if("add".equals(sta)){
			Integer chkoutno = chkoutService.findNextNo().intValue();
			chkoutm.setChkoutno(chkoutno); 
			chkoutm.setVouno(calChkNum.getNext(CalChkNum.CHKOUT, chkoutm.getMaded()));//从新获得单号
			map.put("chkoutno", chkoutno+"");
			result=chkoutService.saveChkoutm(chkoutm);
		}else{
			Chkoutm chkoutm1 = chkoutService.findChkoutmById(chkoutm);
			chkoutm.setVouno(chkoutm1.getVouno());
			chkoutm.setTyp(chkoutm1.getTyp());  
			chkoutm.setMadeby(chkoutm1.getMadeby());
			result=chkoutService.updateChkoutm(chkoutm);
			map.put("chkoutno", chkoutm.getChkoutno()+"");
		}
		return JSONObject.fromObject(result).toString();
	}
}
