package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmDeliverConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.FirmDeliverService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ArrayUtil;
/**
 * 分店供应商
 * @author dell
 *
 */
@Controller
@RequestMapping("firmDeliver")
public class FirmDeliverController {

	@Autowired
	PositnService positnService;
	@Autowired
	FirmDeliverService firmDeliverService;
	@Autowired
	CodeDesService codeDesService;
	/**
	 * 报货分拨修改供应商时候，查询方向
	 */
	@RequestMapping(value = "/findInout")
	@ResponseBody
	public Object findInout(ModelMap modelMap,HttpSession session,Deliver deliver,String oldCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return firmDeliverService.findInout(deliver,oldCode);
	}
	
	/**
	 * 查询仓位信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findPositn(ModelMap modelMap,HttpSession session,String searchInfo,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null !=searchInfo && !"".equals(searchInfo)){
			searchInfo = searchInfo.toUpperCase();
		}
		
		modelMap.put("pageobj", page);
		modelMap.put("searchInfo", searchInfo);
		modelMap.put("listPositn", positnService.findPositnDelNoPage(searchInfo));
		//获取所有区域 wangjie 2014年12月24日 12:55:29
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		List<CodeDes> areaList = codeDesService.findCodeDes(codeDes, page);
		modelMap.put("areaList", areaList);
		return new ModelAndView(FirmDeliverConstants.LIST_POSITN,modelMap);
	}
	
	/**
	 * 根据仓位查询供应商
	 * @throws Exception
	 */
	@RequestMapping(value = "/listFirmDeliver")
	public ModelAndView findFirmDeliver(ModelMap modelMap,String positnCode,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnCode = ("".equals(positnCode)||null==positnCode)?"---":positnCode;//如果为空的话就赋值---为了使页面查不到值
		modelMap.put("pageobj", page);
		modelMap.put("listFirmDeliver", firmDeliverService.findDeliverByPositn(positnCode, page));
		modelMap.put("deliverCodes", firmDeliverService.findAllDeliverByPositn(positnCode));
		modelMap.put("positnCode", positnCode);
		return new ModelAndView(FirmDeliverConstants.LIST_FIRMDELIVER,modelMap);
	}
	
	/**
	 * 新增供应商
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveFirmDeliver(ModelMap modelMap,HttpSession session,String positnCode,String deliver,String deliverCodes,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		if(!"".equals(deliverCodes)){
			deliver = ArrayUtil.getMinString(deliver, deliverCodes, ",");
		}
		if(!"".equals(positnCode) && !"".equals(deliver)){
			firmDeliverService.saveDeliverByPositn(acct, positnCode, deliver);
		}	
		
		modelMap.put("listFirmDeliver", firmDeliverService.findDeliverByPositn(positnCode, page));
		modelMap.put("positnCode", positnCode);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 检查是否重复
	 */
	@RequestMapping(value = "/checkOne")
	@ResponseBody
	public String checkOne(ModelMap modelMap,HttpSession session,String firm,String deliver) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		return firmDeliverService.checkOne(acct, firm, deliver);
	}
	/**
	 * 删除供应商
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView deleteFirmDeliver(ModelMap modelMap,HttpSession session,@RequestParam("positnCode")String positnCode,@RequestParam("deliver")String deliver) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		firmDeliverService.deleteDeliverByPositn(acct,positnCode, deliver);
		modelMap.put("positnCode", positnCode);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
