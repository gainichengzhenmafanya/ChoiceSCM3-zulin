package com.choice.scm.web.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.LinesFirmConstants;
import com.choice.scm.domain.AcctLineFirm;
import com.choice.scm.domain.LinesFirm;
import com.choice.scm.service.LinesFirmService;
/**
 * 配送线路
 *
 */
@Controller
@RequestMapping("linesFirm")
public class LinesFirmController {

	@Autowired
	private LinesFirmService linesFirmService;
		
	/**
	 *  查询配送线路 list
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findLinesFirm(ModelMap modelMap,LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("linesFirmList", linesFirmService.findLinesFirm(linesFirm));
		return new ModelAndView(LinesFirmConstants.LIST_LINEFIRM,modelMap);
	}

	/**
	 *  添加 左边 配送路线
	 */
	@RequestMapping(value = "/saveLiensFirm")
	public ModelAndView saveLiensFirm(HttpSession session,LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(linesFirm.getDes()!=null){
			linesFirmService.saveLiensFirm(session,linesFirm);
			return new ModelAndView(StringConstant.ACTION_DONE);
		}
		return new ModelAndView(LinesFirmConstants.ADD_LINEFIRM);
	}
	/**
	 *  修改配送路线 跳转
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap,LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("LineFirm", linesFirmService.findLineFirmById(linesFirm).get(0));
		return new ModelAndView(LinesFirmConstants.UPDATE_LINEFIRM,modelMap);
	}
	/**
	 *  修改配送路线
	 */
	@RequestMapping(value = "/updateLineFirm")
	public ModelAndView updateLineFirm(HttpSession session,LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		linesFirmService.updateLineFirm(session,linesFirm);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	/**
	 *  刪除 左边 配送路线
	 */
	@RequestMapping(value = "/deleteLiensFirm")
	public ModelAndView deleteLiensFirm(LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		linesFirmService.deleteLiensFirm(linesFirm);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	//---------------------右边--------------------------------
	/**
	 *  根据配送ID查询该配送线下的所有分店
	 */
	@RequestMapping(value = "/findFirmByLinesFirmId")
	public ModelAndView findFirmByLinesFirmId(ModelMap modelMap,AcctLineFirm acctlinefirm,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("AcctLineFirmList", linesFirmService.findFirmByLinesFirmId(acctlinefirm));
		modelMap.put("linesFirm", acctlinefirm);
		modelMap.put("pageobj", page);
		return new ModelAndView(LinesFirmConstants.LIST_ACCTLINEFIRM,modelMap);
	}
	/**
	 *  修改到店时间 跳转
	 */
	@RequestMapping(value = "/updateTim")
	public ModelAndView updateTim(ModelMap modelMap,AcctLineFirm acctlinefirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> code=Arrays.asList(acctlinefirm.getFirm().getCode().split(","));
		modelMap.put("frimCode", code);
		modelMap.put("Code", acctlinefirm.getCode());
		modelMap.put("Acct", acctlinefirm.getAcct());
		return new ModelAndView(LinesFirmConstants.LIST_UDATETIM,modelMap);
	}
	/**
	 *  修改到店时间
	 */
	@RequestMapping(value = "/updateTimto")
	@ResponseBody
	public String updateTimto(LinesFirm linesFirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return linesFirmService.updateTimto(linesFirm);
	}
	
	/**
	 *  删除分店 右边
	 */
	@RequestMapping(value = "/deleteAcctLinesFirm")
	public ModelAndView deleteAcctLinesFirm(AcctLineFirm acctlinefirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		linesFirmService.deleteAcctLinesFirm(acctlinefirm);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	/**
	 *  添加 左边 配送路线
	 */
	@RequestMapping(value = "/saveAcctLineFirm")
	@ResponseBody
	public String saveAcctLineFirm(AcctLineFirm acctlinefirm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		linesFirmService.deleteAcctLinesFirm(acctlinefirm);
		return linesFirmService.saveAcctLineFirm(acctlinefirm);
	}

}
