package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.constants.GrpTypConstants;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Typoth;
import com.choice.scm.service.GrpTypService;

/**
 * 物资类别管理
 * 
 * @author yp
 * 
 */
@Controller
@RequestMapping(value = "grpTyp")
public class GrpTypController {

	@Autowired
	private GrpTypService grpTypService;

	/**
	 * 物资类别列表
	 * 
	 * @param modelMap
	 * @param grpTyp
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllGrpTyp(ModelMap modelMap, HttpSession session,
			String level, String code, String des) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		if (null == level || "".equals(level)) {
			modelMap.put("level", 1);
			modelMap.put("code", 0);
			modelMap.put("des", 0);
		} else {
			modelMap.put("level", level);
			modelMap.put("code", code);
			modelMap.put("des", des);
		}
		// modelMap.put("queryGrpTyp",grpTyp);
		return new ModelAndView(GrpTypConstants.LIST_GRPTYP, modelMap);
	}

	/**
	 * 根据等级获取物资类别列表
	 * 
	 * @param modelMap
	 * @param level
	 * @param code
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findByLevel(ModelMap modelMap, int level, String code,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("listResult", grpTypService.findByLevel(level - 1, code,
				session.getAttribute("ChoiceAcct").toString()));
		modelMap.put("level", level);
		modelMap.put("code", code);
		return new ModelAndView(GrpTypConstants.TABLE_GRPTYP, modelMap);
	}

	/**
	 * 打开新增页面
	 * 
	 * @param modelMap
	 * @param grpTyp
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap, int level, String code,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("level", level);
		modelMap.put("code", code);
		if (level == 1) {
			modelMap.put("des", ((GrpTyp) (grpTypService.findByCode(level,
					code, session.getAttribute("ChoiceAcct").toString())))
					.getDes());
		} else if (level == 2) {
			modelMap.put("des", ((Grp) (grpTypService.findByCode(level, code,
					session.getAttribute("ChoiceAcct").toString()))).getDes());
		} else if (level == -1) {
			modelMap.put("msg", "请选择左侧物资类别！");
			return new ModelAndView(GrpTypConstants.ERROR_DONE, modelMap);
		}
		return new ModelAndView(GrpTypConstants.SAVE_GRPTYP, modelMap);
	}

	/**
	 * 保存物资类别
	 * 
	 * @param modelMap
	 * @param grpTyp
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView save(ModelMap modelMap, String des, String grp,
			String grptyp, int level, String code, HttpSession session,String typ,String cost,String amt) {
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
			grpTypService.saveGrpTyp(des, grp, grptyp, level, code, session
					.getAttribute("ChoiceAcct").toString(),typ,cost,amt);
		} catch (CRUDException e) {
			e.printStackTrace();
			modelMap.put("msg", "编码已存在，请不要重复添加！");
			return new ModelAndView(GrpTypConstants.ERROR_DONE, modelMap);
		}
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/**
	 * 打开修改页面
	 * 
	 * @param modelMap
	 * @param grpTyp
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, int level, String code,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put(
				"grpTyp",
				grpTypService.findByCode(level - 1, code,
						session.getAttribute("ChoiceAcct").toString()));
		modelMap.put("level", level);
		return new ModelAndView(GrpTypConstants.UPDATE_GRPTYP, modelMap);
	}

	/**
	 * 判断选中的类别是否已经被引用
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/checkGrpTyp")
	public String checkGrpTyp(String code,int level)throws Exception {
		return grpTypService.checkGrpTyp(code,level);
	}
	
	/**
	 * 修改物资类别信息
	 * 
	 * @param modelMap
	 * @param grpTyp
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, String des,
			String code, int level, String oldCode,String typ,String cost,String amt,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		//判断是否有相同名称的类型
		GrpTyp gt = new GrpTyp();
		gt.setDes(des);
		List<GrpTyp> list = grpTypService.findTypeByDes(gt);
		String view = StringConstant.ACTION_DONE;
		//判断
		if(list!=null&&list.size()>0){
			modelMap.put("grpTyp",grpTypService.findByCode(level - 1, code,session.getAttribute("ChoiceAcct").toString()));
			modelMap.put("level", level);
			modelMap.put("str", "desError");
			view = GrpTypConstants.UPDATE_GRPTYP;
		}else{
			grpTypService.updateGrpTyp(des, code, level - 1, oldCode,typ,cost,amt);
		}
		return new ModelAndView(view, modelMap);
	}

	/**
	 * 删除物资类别信息
	 * 
	 * @param modelMap
	 * @param grpTypId
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String code,
			HttpSession session, int level) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		grpTypService.deleteGrpTyp(code, session.getAttribute("ChoiceAcct")
				.toString(), level - 1);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/**
	 * 打开选择物资类别
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/selectGrptyp")
	public ModelAndView selectGrptyp(ModelMap modelMap, HttpSession session,
			int level) throws CRUDException {

		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		modelMap.put("level", level - 1);
		return new ModelAndView(GrpTypConstants.SELECT_SUPPLY, modelMap);
	}

	/**
	 * 获取所有物资中类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllGrp")
	@ResponseBody
	public Object findAllGrp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllGrpA(session.getAttribute("ChoiceAcct")
				.toString(), null);
	}

	/**
	 * 获取所有物资小类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllTyp")
	@ResponseBody
	public Object findAllTyp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllTypA(session.getAttribute("ChoiceAcct")
				.toString(), null);
	}

	/**
	 * 获取所有物资大类信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("/findAllGrpTyp")
	@ResponseBody
	public Object findAllGrpTyp(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return grpTypService.findAllGrpTypA(session.getAttribute("ChoiceAcct")
				.toString());
	}

	/**
	 * 获取所有物资类别信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/selectOneGrpTyp")
	public Object selectOneGrpTyp(ModelMap modelMap, String defaultCode,
			String defaultName) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("listTyp", grpTypService.findAllTyp(null));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("defaultName", defaultName);
		return new ModelAndView(GrpTypConstants.SELECT_TYP, modelMap);
	}
	
	/**
	 * 获取所有物资类别信息,多选
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author wjf
	 */
	@RequestMapping(value = "/selectMoreGrpTyp")
	public Object selectMoreGrpTyp(ModelMap modelMap, HttpSession session, String defaultCode, String resulttyp) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));// 大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct, null));// 中类
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));// 小类
		modelMap.put("defaultCode", defaultCode);
		//返回值类型为空返回编码含括号例如:('1','2','3')
		//为1返回值编码不包含括号例如:'1','2','3'
		modelMap.put("resulttyp", (resulttyp==null||"".equals(resulttyp.trim()))?"":resulttyp);
		return new ModelAndView(GrpTypConstants.SELECT_TYP_MORE, modelMap);
	}

	/**
	 * 查询编码是否重复
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findGrpTypByCode")
	@ResponseBody
	public Object findGrpTypByCode(HttpSession session, int level, String code)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return grpTypService.findByCode(level + 1, code, acct);
	}

	/**
	 * 
	 * @Title: findGrpTypByDes
	 * @Description: TODO(查询名称是否重复)
	 * @Author：LI Shuai
	 * @date：2014-2-12下午2:03:40
	 * @param level
	 * @param code
	 * @param des
	 * @return String
	 * @throws
	 */
	@RequestMapping("/findGrpTypByDes")
	@ResponseBody
	public Object findGrpTypByDes(HttpSession session, int level, String code,
			String des) {

		String acct = session.getAttribute("ChoiceAcct").toString();

		return grpTypService.findbydes(level + 1, code, des, acct);

	}
	
	/****************************************参考类别设置2014.11.19 wjf*************************************************/
	
	/***
	 * 进入参考类别设置
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableTypoth")
	public ModelAndView tableTypoth(ModelMap modelMap,HttpSession session,Typoth typoth) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Typoth> typothList = grpTypService.findTypoth(typoth);
		modelMap.put("typothList", typothList);
		return new ModelAndView(GrpTypConstants.TABLE_TYPOTH, modelMap);
	}
	
	/***
	 * 进入新增参考类别界面
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addTypoth")
	public ModelAndView addTypoth(ModelMap modelMap,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(GrpTypConstants.SAVE_TYPOTH, modelMap);
	}
	
	/***
	 * 查询编码或者名称是否重复
	 * @author wjf
	 * @param session
	 * @param typoth
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findTypoth")
	@ResponseBody
	public String findTypothByCode(HttpSession session, Typoth typoth)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Typoth> typothList = grpTypService.findTypoth(typoth);
		if(typothList.size() == 0){
			return "true";
		}
		return "fail";
	}

	/***
	 * 新增参考类别
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @param typoth
	 * @return
	 */
	@RequestMapping(value = "/saveTypothByAdd")
	public ModelAndView saveTypothByAdd(ModelMap modelMap, HttpSession session,Typoth typoth) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		grpTypService.saveTypoth(typoth);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/***
	 * 进入参考类别修改界面 
	 * @author wjf
	 * @param modelMap
	 * @param typoth
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateTypoth")
	public ModelAndView updateTypoth(ModelMap modelMap,HttpSession session,Typoth typoth) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Typoth> typothList = grpTypService.findTypoth(typoth);
		modelMap.put("typoth",typothList.get(0));//查出来 显示
		return new ModelAndView(GrpTypConstants.UPDATE_TYPOTH, modelMap);
	}

	/***
	 * 修改参考类别信息 
	 * @author wjf
	 * @param modelMap
	 * @param typoth
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveTypothByUpdate")
	public ModelAndView saveTypothByUpdate(ModelMap modelMap, HttpSession session, Typoth typoth) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		grpTypService.updateTypoth(typoth);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/***
	 * 删除参考类别
	 * @author wjf
	 * @param modelMap
	 * @param typoth
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteTypoth")
	public ModelAndView deleteTypoth(ModelMap modelMap,HttpSession session,Typoth typoth) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
		typoth.setCode(CodeHelper.replaceCode(typoth.getCode()));
		grpTypService.deleteTypoth(typoth);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}
