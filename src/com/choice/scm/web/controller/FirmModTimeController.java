package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmModTimeConstants;
import com.choice.scm.domain.FirmModTime;
import com.choice.scm.service.FirmModTimeService;
import com.choice.scm.service.PositnService;

/**
 * 成本卡方案的查询
 * @author css
 *
 */
@Controller
@RequestMapping("firmModTime")
public class FirmModTimeController {

	@Autowired
	private FirmModTimeService firmModTimeService;
	@Autowired
	PositnService positnService;
	
	/**
	 * 左侧分店
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findPositn(ModelMap modelMap,HttpSession session,String searchInfo,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null !=searchInfo && !"".equals(searchInfo)){
			searchInfo = searchInfo.toUpperCase();
		}
		modelMap.put("pageobj", page);
		modelMap.put("searchInfo", searchInfo);
		modelMap.put("listPositn", positnService.findPositnDel(searchInfo, page));
		return new ModelAndView(FirmModTimeConstants.LIST_FIRMMODTIME,modelMap);
	}
	
	/**
	 * 查找成本卡方案
	 * @param modelMap
	 * @param session
	 * @param firmDept
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public Object findFirmModTime(ModelMap modelMap, HttpSession session, FirmModTime firmModTime, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("listFirmModTime", firmModTimeService.findFirmModTime(firmModTime, page));
		modelMap.put("firmModTime", firmModTime);
		return new ModelAndView(FirmModTimeConstants.TABLE_FIRMMODTIME,modelMap);
	}
	
	/**
	 * 新增成本卡方案
	 */
	@RequestMapping(value = "/add")
	public ModelAndView addFirmModTime(ModelMap modelMap,HttpSession session, FirmModTime firmModTime) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源		
		modelMap.put("listCodeDes", firmModTimeService.findMod(firmModTime));
		modelMap.put("firmModTime", firmModTime);
		return new ModelAndView(FirmModTimeConstants.ADD_FIRMMODTIME,modelMap);
	}
	
	/**
	 * 新增保存成本卡方案
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveFirmModTime(ModelMap modelMap, HttpSession session, FirmModTime firmModTime, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmModTimeService.saveFirmModTime(firmModTime);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 删除成本卡方案
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView deleteFirmModTime(ModelMap modelMap, String ids, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmModTimeService.deleteFirmModTime(ids);
//		modelMap.put("positnCode", positnCode);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 转到修改页面
	 * @param modelMap
	 * @param firmModTime
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/update")
	public ModelAndView updatePositn(ModelMap modelMap, FirmModTime firmModTime) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("firmModTime",firmModTimeService.findFirmModTime(firmModTime).get(0));
		return new ModelAndView(FirmModTimeConstants.UPDATE_FIRMMODTIME,modelMap);
	}
	
	/**
	 * 更新分店和仓位信息
	 * @param modelMap
	 * @param firmModTime
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, FirmModTime firmModTime) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		firmModTimeService.updateFirmModTime(firmModTime);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
