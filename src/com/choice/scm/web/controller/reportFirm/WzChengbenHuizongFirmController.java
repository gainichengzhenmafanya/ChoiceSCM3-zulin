package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportFirm.WzChengbenHuizongFirmService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("wzChengbenHuizong")
public class WzChengbenHuizongFirmController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzChengbenHuizongFirmService wzChengbenHuizongFirmService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	
	/********************************************物资成本汇总表报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseWzchengbenHuizong")
	public ModelAndView toColChooseWzchengbenHuizong(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENHUIZONG);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_WZCHENGBENHUIZONG);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_WZCHENGBENHUIZONG));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findWzchengbenHuizongHeaders")
	@ResponseBody
	public Object getWzchengbenHuizongHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENHUIZONG);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_WZCHENGBENHUIZONG));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_WZCHENGBENHUIZONG_FROZEN);
		return columns;
	}
	/**
	 * 跳转到物资成本汇总表报表页面
	 * @return
	 */
	@RequestMapping("/toWzChengbenHuizong")
	public ModelAndView toWzChengbenHuizong(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_WZCHENGBENHUIZONG);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_WZCHENGBENHUIZONG_FIRM,modelMap);
	}
	/**
	 * 查询物资成本汇总表报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findWzchengbenHuizong")
	@ResponseBody
	public Object findWzchengbenHuizong(ModelMap modelMap, HttpSession session, String without0, String sort, String order,
			SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		List<Map<String,Object>> listNMoney = wzChengbenHuizongFirmService.getNMoneyList(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		condition.put("listNMoney", listNMoney);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		return wzChengbenHuizongFirmService.findWzChengbenHuizong(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printWzchengbenHuizong")
	public ModelAndView printWzchengbenHuizong(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		List<Map<String,Object>> listNMoney = wzChengbenHuizongFirmService.getNMoneyList(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		//params.put("delivercode",supplyAcct.getDelivercode());
		//params.put("accby", supplyAcct.getAccby());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		condition.put("listNMoney", listNMoney);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List", wzChengbenHuizongFirmService.findWzChengbenHuizong(condition).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资成本汇总表");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/wzChengbenHuizong/printWzchengbenHuizong.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_WZCHENGBENHUIZONG,SupplyAcctConstants.REPORT_EXP_URL_WZCHENGBENHUIZONG);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出物资成本汇总表报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportWzchengbenHuizong")
	@ResponseBody
	public void exportWzchengbenHuizong(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		List<Map<String,Object>> listNMoney = wzChengbenHuizongFirmService.getNMoneyList(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资成本汇总表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("listNMoney", listNMoney);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENHUIZONG);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzChengbenHuizongFirmService.findWzChengbenHuizong(condition).getRows(), "物资成本汇总表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
