package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportFirm.WzChengbenMingxiFirmService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("wzChengbenMingxi")
public class WzChengbenMingxiFirmController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzChengbenMingxiFirmService wzChengbenMingxiFirmService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	
	/********************************************物资成本明细表报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseWzChengbenMingxi")
	public ModelAndView toColChooseWzChengbenMingxi(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENMINGXI);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_WZCHENGBENMINGXI);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_SUPPLYSUMINOUT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findWzChengbenMingxiHeaders")
	@ResponseBody
	public Object getWzChengbenMingxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENMINGXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_WZCHENGBENMINGXI));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_WZCHENGBENMINGXI_FROZEN);
		return columns;
	}
	/**
	 * 跳转到物资综合进出表报表页面
	 * @return
	 */
	@RequestMapping("/toWzChengbenMingxi")
	public ModelAndView toWzChengbenMingxi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_WZCHENGBENMINGXI);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_WZCHENGBENMINGXI_FIRM,modelMap);
	}
	/**
	 * 查询物资综合进出表报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findWzChengbenMingxi")
	@ResponseBody
	public Object findWzChengbenMingxi(ModelMap modelMap,HttpSession session,String without0,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Map<String,Object> listNMoney = wzChengbenMingxiFirmService.getNMoney(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		condition.put("listNMoney", listNMoney);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		return wzChengbenMingxiFirmService.findWzChengbenMingxi(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printWzChengbenMingxi")
	public ModelAndView printWzChengbenMingxi(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Map<String,Object> listNMoney = wzChengbenMingxiFirmService.getNMoney(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		condition.put("supplyAcct", supplyAcct);
		condition.put("listNMoney", listNMoney);
		modelMap.put("List", wzChengbenMingxiFirmService.findWzChengbenMingxi(condition).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资综合进出表");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/wzChengbenMingxi/printWzChengbenMingxi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_WZCHENGBENMINGXI,SupplyAcctConstants.REPORT_EXP_URL_WZCHENGBENMINGXI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出物资综合进出表报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportWzChengbenMingxi")
	@ResponseBody
	public void exportWzChengbenMingxi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		Map<String,Object> listNMoney = wzChengbenMingxiFirmService.getNMoney(supplyAcct);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资成本明细表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("listNMoney", listNMoney);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZCHENGBENMINGXI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzChengbenMingxiFirmService.findWzChengbenMingxi(condition).getRows(), "物资成本明细表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
