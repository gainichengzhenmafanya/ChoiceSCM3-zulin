package com.choice.scm.web.controller.reportFirm;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportFirm.MeiriChayiDuizhaoService;

/**
 * 门店日盘点状态表
 * @author css
 */
@Controller
@RequestMapping(value="MeiriChayiDuizhao")
public class MeiriChayiDuizhaoController {
	
	@Autowired
	private MeiriChayiDuizhaoService meiriChayiDuizhaoService;
//	@Autowired
//	private Page pager;
	
	/**
	 * 门店日盘点状态表页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("toMeiriChayiDuizhao")
	public ModelAndView findMdRiPanDianZhuangTaiBiao(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MEIRICHAYIDUIZHAO_FIRM,modelMap);
	}
	/**
	 * 门店日盘点状态表查询数据
	 * @param modelMap
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findMeiriChayiDuizhao")
	@ResponseBody
	public Object findMeiriChayiDuizhao(ModelMap modelMap, SupplyAcct supplyAcct, Page pager, String page, String rows) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		if (Integer.parseInt(supplyAcct.getMonthh())<10){
			supplyAcct.setMonthh("0"+supplyAcct.getMonthh());
		}
		supplyAcct.setBdat(DateFormat.getDateByString(supplyAcct.getYearr()+"-"+supplyAcct.getMonthh()+"-01","yyyy-MM-dd"));
		supplyAcct.setEdat(DateFormat.getEndDayOfCurrMonth(supplyAcct.getBdat()));
		content.put("supplyAcct",supplyAcct);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return meiriChayiDuizhaoService.findMeiriChayiDuizhao_bzb(content, pager);
	}
		 
	/**
	 * 获取动态日期
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findHeader")
	@ResponseBody
	public Object findHeader() throws CRUDException{
		return meiriChayiDuizhaoService.findHeader();
	}
}
