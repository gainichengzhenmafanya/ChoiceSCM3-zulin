package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportFirm.ChayiGuanliService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("ChayiGuanli")
public class ChayiGuanliController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private ChayiGuanliService chayiGuanliService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/*******************************************差异管理报表*****************************************************/
	/**
	 * 跳转到差异管理页面
	 * @param modelMap
	 * @return
	 * @author css
	 */
	@RequestMapping("/toChayiGuanli")
	public ModelAndView toChayiGuanli(ModelMap modelMap, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_CHAYIGUANLI);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHAYIGUANLI_FIRM,modelMap);
	}
	
	/**
	 * 查询差异管理表头
	 * @param session
	 * @return
	 * @author css
	 */
	@RequestMapping("/findChayiGuanliHeaders")
	@ResponseBody
	public Object findChayiGuanliHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHAYIGUANLI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHAYIGUANLI_FIRM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_CHAYIGUANLI_FROZEN);
		return columns;
	}
	
	/**
	 * 查询差异管理
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author css
	 */
	@RequestMapping("/findChayiGuanli")
	@ResponseBody
	public Object findChayiGuanli(ModelMap modelMap, HttpSession session, String page, String typ,
			String rows, String sort, String order, SupplyAcct supplyAcct, String zero, String qimo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		content.put("zero", zero);
		content.put("qimo", qimo);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		
		return chayiGuanliService.findChayiGuanli_bzb(content, pager);
		
	}
	
	/**
	 * 导出差异管理表excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChayiGuanli")
	@ResponseBody
	public void exportChayiGuanli(HttpServletResponse response, String sort, String order, 
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct, String zero, String qimo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "差异管理表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("zero", zero);
		condition.put("qimo", qimo);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHAYIGUANLI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
	
		exportExcelMap.creatWorkBook(response.getOutputStream(), chayiGuanliService.findChayiGuanli_bzb(condition,pager).getRows(), "差异管理表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHAYIGUANLI_FIRM));
	}
	/**
	 * 打印差异管理表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/printChayiGuanli")
	public ModelAndView printChayiGuanli(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct, String delivertyp, String folio, String zero, String qimo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
//		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		//params.put("chktyp",supplyAcct.getChktyp());
		//params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("firm",supplyAcct.getFirm());
//		params.put("grptyp",supplyAcct.getaGrptyp());
//		params.put("grp",supplyAcct.getGrp());
//		params.put("typ",supplyAcct.getTyp());
		//params.put("folio", folio);
		//params.put("delivertyp", delivertyp);
//		condition.put("zero", zero);
//		condition.put("qimo", qimo);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",chayiGuanliService.findChayiGuanli_bzb(condition,pager).getRows());
		
	    parameters.put("report_name", "差异管理表");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/ChayiGuanli/printChayiGuanli.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHAYIGUANLI,SupplyAcctConstants.REPORT_EXP_URL_CHAYIGUANLI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/toColChooseChayiGuanli")
	public ModelAndView toColChooseChayiGuanli(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHAYIGUANLI);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_CHAYIGUANLI);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHAYIGUANLI_FIRM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}
