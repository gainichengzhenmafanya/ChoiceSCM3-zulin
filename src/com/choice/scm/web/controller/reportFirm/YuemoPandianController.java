package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportFirm.YuemoPandianService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("YuemoPandian")
public class YuemoPandianController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private YuemoPandianService yuemoPandianService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/*******************************************月末盘点报表*****************************************************/
	/**
	 * 跳转到月末盘点页面
	 * @param modelMap
	 * @return
	 * @author css
	 */
	@RequestMapping("/toYuemoPandian")
	public ModelAndView toYuemoPandian(ModelMap modelMap, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_YUEMOPANDIAN_FIRM,modelMap);
	}
	
	/**
	 * 查询月末盘点表头
	 * @param session
	 * @return
	 * @author css
	 */
	@RequestMapping("/findYuemoPandianHeaders")
	@ResponseBody
	public Object findYuemoPandianHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
//		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FROZEN);
		return columns;
	}
	
	/**
	 * 查询月末盘点
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author css
	 */
	@RequestMapping("/findYuemoPandian")
	@ResponseBody
	public Object findYuemoPandian(ModelMap modelMap, HttpSession session, String page, String typ,
			String rows, String sort, String order, SupplyAcct supplyAcct, String zero, String qimo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("typ", typ);
		content.put("supplyAcct",supplyAcct);
//		String type = supplyAcct.getTyp();
//		if(type!=null && !"".equals(type)){
//			type = "'"+type+"'";
//			type = type.replace(",","','");
//			supplyAcct.setTyp(type);
//		}
		content.put("sort",sort);
		content.put("order", order);
		content.put("zero", zero);
		content.put("qimo", qimo);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return yuemoPandianService.findYuemoPandian(content, pager);
	}
	
	/**
	 * 导出月末盘点表excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYuemoPandian")
	@ResponseBody
	public void exportYuemoPandian(HttpServletResponse response, String sort, String order, 
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct, String zero, String qimo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月末盘点明细表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("zero", zero);
		condition.put("qimo", qimo);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), yuemoPandianService.findYuemoPandian(condition,pager).getRows(), "月末盘点明细表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM));
		
	}
	/**
	 * 打印月末盘点明细表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/printYuemoPandian")
	public ModelAndView printYuemoPandian(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct, String delivertyp, String folio, String zero, String qimo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
//		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		//params.put("chktyp",supplyAcct.getChktyp());
		//params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("firm",supplyAcct.getFirm());
//		params.put("grptyp",supplyAcct.getaGrptyp());
//		params.put("grp",supplyAcct.getGrp());
//		params.put("typ",supplyAcct.getTyp());
		//params.put("folio", folio);
		//params.put("delivertyp", delivertyp);
//		condition.put("zero", zero);
//		condition.put("qimo", qimo);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",yuemoPandianService.findYuemoPandian(condition,pager).getRows());
	    parameters.put("report_name", "月末盘点明细表");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/YuemoPandian/printYuemoPandian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_YUEMOPANDIAN,SupplyAcctConstants.REPORT_EXP_URL_YUEMOPANDIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/toColChooseYuemoPandian")
	public ModelAndView toColChooseYuemoPandian(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 跳转到月末盘点页面
	 * @param modelMap
	 * @return
	 * @author css
	 */
	@RequestMapping("/toYuemoPandian2")
	public ModelAndView toYuemoPandian2(ModelMap modelMap, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_YUEMOPANDIAN_FIRM2,modelMap);
	}
	
	/**
	 * 查询月末盘点表头
	 * @param session
	 * @return
	 * @author css
	 */
	@RequestMapping("/findYuemoPandianHeaders2")
	@ResponseBody
	public Object findYuemoPandianHeaders2(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN2);
//		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM2));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FROZEN2);
		return columns;
	}
	
	/**
	 * 查询月末盘点
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author css
	 */
	@RequestMapping("/findYuemoPandian2")
	@ResponseBody
	public Object findYuemoPandian2(ModelMap modelMap, HttpSession session, String page, String typ,
			String rows, String sort, String order, SupplyAcct supplyAcct, String zero, String qimo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("typ", typ);
		content.put("supplyAcct",supplyAcct);
//		String type = supplyAcct.getTyp();
//		if(type!=null && !"".equals(type)){
//			type = "'"+type+"'";
//			type = type.replace(",","','");
//			supplyAcct.setTyp(type);
//		}
		content.put("sort",sort);
		content.put("order", order);
		content.put("zero", zero);
		content.put("qimo", qimo);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return yuemoPandianService.findYuemoPandian2(content, pager);
	}
	
	/**
	 * 导出月末盘点表excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYuemoPandian2")
	@ResponseBody
	public void exportYuemoPandian2(HttpServletResponse response, String sort, String order, 
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct, String zero, String qimo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月末盘点汇总表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("zero", zero);
		condition.put("qimo", qimo);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), yuemoPandianService.findYuemoPandian2(condition,pager).getRows(), "月末盘点汇总表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM));
		
	}
	/**
	 * 打印月末盘点表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/printYuemoPandian2")
	public ModelAndView printYuemoPandian2(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct, String delivertyp, String folio, String zero, String qimo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
//		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		//params.put("chktyp",supplyAcct.getChktyp());
		//params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("firm",supplyAcct.getFirm());
//		params.put("grptyp",supplyAcct.getaGrptyp());
//		params.put("grp",supplyAcct.getGrp());
//		params.put("typ",supplyAcct.getTyp());
		//params.put("folio", folio);
		//params.put("delivertyp", delivertyp);
//		condition.put("zero", zero);
//		condition.put("qimo", qimo);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",yuemoPandianService.findYuemoPandian(condition,pager).getRows());
	    parameters.put("report_name", "月末盘点汇总表");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/YuemoPandian/printYuemoPandian2.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_YUEMOPANDIAN,SupplyAcctConstants.REPORT_EXP_URL_YUEMOPANDIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	@RequestMapping("/toColChooseYuemoPandian2")
	public ModelAndView toColChooseYuemoPandian2(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_YUEMOPANDIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_YUEMOPANDIAN_FIRM2));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}

