package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.reportFirm.YueChengbenZongheFenxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("YueChengbenZongheFenxi")
public class YueChengbenZongheFenxiController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private YueChengbenZongheFenxiService yueChengbenZongheFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
    @Autowired
    private AcctService acctService;
	
	
	/********************************************月成本综合分析报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseYueChengbenZongheFenxi")
	public ModelAndView toColChooseYueChengbenZongheFenxi(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = ScmStringConstant.REPORT_NAME_YUECHENGBENZONGHEFENXI;
		String defaultColumns = ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI;
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(table_name);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", table_name);
        if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
            modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns, defaultColumns));
        }else{
            modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_Y));
        }
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findYueChengbenZongheFenxiHeaders")
	@ResponseBody
	public Object getYueChengbenZongheFenxiHeaders(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String table_name = ScmStringConstant.REPORT_NAME_YUECHENGBENZONGHEFENXI;
		String defaultColumns = ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI;
		String frozen = ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_FROZEN;
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(table_name);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
        if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
            columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, defaultColumns));
            columns.put("frozenColumns", frozen);
        }else {
            columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_Y));
            columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_FROZEN_Y);
        }
		return columns;
	}
	/**
	 * 跳转到月成本综合分析报表页面
	 * @return
	 */
	@RequestMapping("/toYueChengbenZongheFenxi")
	public ModelAndView toWzChengbenHuizong(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_YUECHENGBENZONGHEFENXI);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_YUECHENGBENZONGHEFENXI_FIRM,modelMap);
	}
	/**
	 * 查询月成本综合分析报表内容
	 * @param modelMap
	 * @param session
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findYueChengbenZongheFenxi")
	@ResponseBody
	public Object findYueChengbenZongheFenxi(ModelMap modelMap, HttpSession session, String sort, String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		condition.put("acctDes", acct.getDes());
        if ("Y".equals(acct.getYnkc())) {
            return yueChengbenZongheFenxiService.findYueChengbenZongheFenxi_Y(supplyAcct);
        }else{
    		List<Map<String,Object>> listNMoney = yueChengbenZongheFenxiService.getNMoneyList(supplyAcct,"N");
            condition.put("listNMoney", listNMoney);
            return yueChengbenZongheFenxiService.findYueChengbenZongheFenxi(condition);
        }
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printYueChengbenZongheFenxi")
	public ModelAndView printYueChengbenZongheFenxi(ModelMap modelMap,Page pager,HttpSession session,String type,String without0,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		//params.put("delivercode",supplyAcct.getDelivercode());
		//params.put("accby", supplyAcct.getAccby());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		condition.put("acctDes", acct.getDes());
        if ("N".equals(acct.getYnkc())) {
//        	List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    			List<Map<String,Object>> listNMoney = yueChengbenZongheFenxiService.getNMoneyList(supplyAcct,"N");
                condition.put("listNMoney", listNMoney);
                modelMap.put("List", yueChengbenZongheFenxiService.findYueChengbenZongheFenxi(condition).getRows());
    		
        }else{
            modelMap.put("List", yueChengbenZongheFenxiService.findYueChengbenZongheFenxi_Y(supplyAcct).getRows());
        }
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "月成本综合分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/yueChengbenZongheFenxi/printYueChengbenZongheFenxi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_YUECHENGBENZONGHEFENXI,SupplyAcctConstants.REPORT_EXP_URL_YUECHENGBENZONGHEFENXI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出月成本综合分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYueChengbenZongheFenxi")
	@ResponseBody
	public void exportYueChengbenZongheFenxi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		supplyAcct.setBdate(DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdate(DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月成本综合分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("sort", sort);
		condition.put("order", order);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YUECHENGBENZONGHEFENXI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		condition.put("acctDes", acct.getDes());
        if ("N".equals(acct.getYnkc())) {
        	List<Map<String, Object>> listNMoney = yueChengbenZongheFenxiService.getNMoneyList(supplyAcct,"N");
            condition.put("listNMoney", listNMoney);
            exportExcelMap.creatWorkBook(response.getOutputStream(), yueChengbenZongheFenxiService.findYueChengbenZongheFenxi(condition).getRows(), "月成本综合分析", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI));
        }else {
            exportExcelMap.creatWorkBook(response.getOutputStream(), yueChengbenZongheFenxiService.findYueChengbenZongheFenxi_Y(supplyAcct).getRows(), "月成本综合分析", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_Y));
        }
	}
}
