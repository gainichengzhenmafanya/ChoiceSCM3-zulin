package com.choice.scm.web.controller.reportFirm;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportFirm.DbMingxiChaxunFirmService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("DbMingxiChaxunFirm")
public class DbMingxiChaxunFirmController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DbMingxiChaxunFirmService dbMingxiChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	PositnService positnService;
	
	/********************************************调拨明细查询报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseChkAllotDetailQuery")
	public ModelAndView toColChooseChkAllotDetailQuery(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKALLOTDETAILQUERY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_CHKALLOTDETAILQUERY);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHKALLOTDETAILQUERY));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkAllotDetailQueryHeaders")
	@ResponseBody
	public Object getChkChkAllotDetailQuery(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKALLOTDETAILQUERY);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHKALLOTDETAILQUERY ));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_CHKALLOTDETAILQUERY_FROZEN );
		return columns;
	}
	/**
	 * 跳转报表html页面
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/toChkAllotDetailQuery")
	public ModelAndView toChkAllotDetailQuery(ModelMap modelMap,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != supplyAcct.getFirmdes() && !"".equals(supplyAcct.getFirmdes())) && ("".equals(supplyAcct.getFirmcode()) || null == supplyAcct.getFirmcode())){
			Positn firm = new Positn();
			firm.setDes(supplyAcct.getFirmdes());
			List<Positn> listp = positnService.findAllPositn(firm);
			supplyAcct.setFirmcode(listp.size()>0?listp.get(0).getCode():"");
		}
		if((null != supplyAcct.getPositndes() && !"".equals(supplyAcct.getPositndes())) && ("".equals(supplyAcct.getPositn()) || null == supplyAcct.getPositn())){
			Positn positn = new Positn();
			positn.setDes(supplyAcct.getPositndes());
			List<Positn> listp = positnService.findAllPositn(positn);
			supplyAcct.setPositn(listp.size()>0?listp.get(0).getCode():"");
		}
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_CHKALLOTDETAILQUERY );
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKALLOTDETAILQUERY_FIRM,modelMap);
	}
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findChkAllotDetailQuery")
	@ResponseBody
	public Object findChkAllotDetailQuery(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return dbMingxiChaxunService.findChkAllotDetailQuery(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkAllotDetailQuery")
	public ModelAndView printChkAllotDetailQuery(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktag", String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",dbMingxiChaxunService.findChkAllotDetailQuery(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "调拨明细查询");
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/DbMingxiChaxunFirm/printChkAllotDetailQuery.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKALLOTDETAILQUERY,SupplyAcctConstants.REPORT_EXP_URL_CHKALLOTDETAILQUERY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkAllotDetailQuery")
	@ResponseBody
	public void exportChkAllotDetailQuery(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "调拨明细查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKALLOTDETAILQUERY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), dbMingxiChaxunService.findChkAllotDetailQuery(condition,pager).getRows(), "调拨明细查询", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
}
