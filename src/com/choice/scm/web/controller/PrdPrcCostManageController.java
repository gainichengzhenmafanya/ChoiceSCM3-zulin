package com.choice.scm.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.ProCostAna;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.PrdPrcCostManageService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * 毛利率周分析、分店部门物资进出明细、分店物资成本差异、核减、菜品利润、加工间综合利润（加工产品理论成本）、分店耗用对比、应产率分析、生产耗用
 * @author lq
 *
 */
@Controller
@RequestMapping("prdPrcCostManage")
public class PrdPrcCostManageController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private PrdPrcCostManageService prdPrcCostManageService;
	@Autowired
	private PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	AcctService acctService;
	
	/**
	 * 成本报表列表
	 * @return
	 */
	@RequestMapping("/reportList")
	public ModelAndView toReportList(){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(PrdPrcCMConstant.REPORT_LIST);
	}
	
	/****************************************************物资成本差异 start***************************************************/
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findCostVarianceHeaders")
	@ResponseBody	
	public Object getCostVarianceHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE_FROZEN);
		return columns;
	}
	/**
	 * 跳转到物资成本差异页面
	 */
	@RequestMapping("/toCostVariance")
	public ModelAndView toCostVariance(ModelMap modelMap,HttpSession session,String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		modelMap.put("type", checkMis);
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				modelMap.put("firm",accountPositn.getPositn().getCode());
			}
		}
		return new ModelAndView(PrdPrcCMConstant.LIST_COST_VARIANCE,modelMap);
	}
	
	/**
	 * 物资成本差异
	 * @param modelMap
	 * @param page
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/listCostVariance")
//	public ModelAndView findCostVariance(ModelMap modelMap,Page page) throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		return new ModelAndView(PrdPrcCMConstant.LIST_COST_VARIANCE,modelMap);
//	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseCostVariance")
	public ModelAndView toColChooseCostVariance(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询物资成本差异
	 */
	@RequestMapping("/findCostVariance")
	@ResponseBody
	public Object findCostVariance(ModelMap modelMap,HttpSession session,String page,String rows,String firmCode,String firmDept,String sp_code,Date bdat,Date edat,
			SupplyAcct supplyAcct,String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=firmDept && !"".equals(firmDept)){
			firmDept = "'"+firmDept+"'";
			firmDept = firmDept.replace(",","','");
			content.put("dept", firmDept);
		}
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				content.put("firmCode",accountPositn.getPositn().getCode());
			}
		}else{
			content.put("firmCode", firmCode);
		}
		content.put("month", DateFormat.getMonth(bdat));
		content.put("yearr", DateFormat.getYear(bdat)+"");
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("sp_code", sp_code);
		content.put("supplyAcct",supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			content.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			content.put("POSITNSUPPLY", "POSITNSUPPLY");
			content.put("SUPPLYACCT", "SUPPLYACCT");
		}else{
			content.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			content.put("POSITNSUPPLY", "FIRMPOSITNSUPPLY");
			content.put("SUPPLYACCT", "firmSUPPLYACCT");
		}
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return prdPrcCostManageService.findCostVariance(content, pager);
	}

	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCostVariance")
	@ResponseBody
	public void exportCostVariance(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,String firmCode,String firmDept,
				String sp_code,Date bdat,Date edat,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资成本差异";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=firmDept && !"".equals(firmDept)){
			firmDept = "'"+firmDept+"'";
			firmDept = firmDept.replace(",","','");
			condition.put("dept", firmDept);
		}
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				condition.put("firmCode",accountPositn.getPositn().getCode());
			}
		}else{
			condition.put("firmCode", firmCode);
		}
		condition.put("month", DateFormat.getMonth(bdat));
		condition.put("yearr", DateFormat.getYear(bdat));
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		condition.put("sp_code", sp_code);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			condition.put("SUPPLYACCT", "SUPPLYACCT");
            condition.put("POSITNSUPPLY", "POSITNSUPPLY");
		}else{
            condition.put("POSITNSUPPLY", "FIRMPOSITNSUPPLY");
			condition.put("COSTITEMSPCODE", "FIRMCOSTITEMSPCODE");
			condition.put("SUPPLYACCT", "FIRMSUPPLYACCT");
		}
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_COST_VARIANCE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findCostVariance(condition,pager).getRows(), "物资成本差异", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COST_VARIANCE));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printCostVariance")
	public ModelAndView printCostVariance(ModelMap modelMap,Page pager,HttpSession session,String type,String page,String rows,String firmCode,String firmDept,
			String sp_code,Date bdat,Date edat,SupplyAcct supplyAcct,String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(null!=firmDept && !"".equals(firmDept)){
			firmDept = "'"+firmDept+"'";
			firmDept = firmDept.replace(",","','");
			condition.put("dept", firmDept);
			params.put("dept", firmDept);
			
		}
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				condition.put("firmCode",accountPositn.getPositn().getCode());
				params.put("firmCode", accountPositn.getPositn().getCode());
			}
		}else{
			condition.put("firmCode", firmCode);
			params.put("firmCode", firmCode);
		}
		condition.put("month", DateFormat.getMonth(bdat));
		condition.put("yearr", DateFormat.getYear(bdat));
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		condition.put("sp_code", sp_code);
		condition.put("supplyAcct", supplyAcct);
        if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
            condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
            condition.put("SUPPLYACCT", "SUPPLYACCT");
            condition.put("POSITNSUPPLY", "POSITNSUPPLY");
        }else{
            condition.put("POSITNSUPPLY", "FIRMPOSITNSUPPLY");
            condition.put("COSTITEMSPCODE", "FIRMCOSTITEMSPCODE");
            condition.put("SUPPLYACCT", "FIRMSUPPLYACCT");
        }
		// 
		params.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		params.put("sp_code", sp_code);
		params.put("grptyp", supplyAcct.getGrptyp());
		params.put("grp", supplyAcct.getGrp());
		params.put("typ", supplyAcct.getTyp());
		
		parameters.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		parameters.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		
		modelMap.put("List",prdPrcCostManageService.findCostVariance(condition, pager).getRows());
	    parameters.put("report_name", "物资成本差异");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printCostVariance.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_COST_VARIANCE,PrdPrcCMConstant.REPORT_EXP_URL_COST_VARIANCE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/****************************************************物资成本差异 end***************************************************/
	
	/***********************************************************菜品利润start************************************************/	
	/**
	 * 查询表头信息     菜品利润
	 */
	@RequestMapping("/findCprlHeaders")
	@ResponseBody
	public Object findCprlHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_CPLR);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_CPLR));
		return columns;
	}
	/**
	 * 查询表信息     菜品利润
	 */
	@RequestMapping("/findCprlDetailsInfo")
	@ResponseBody
	public Object findCprlDetailsInfo(ModelMap modelMap,HttpSession session,String page,String withdat,String rows,String sort,String order,SupplyAcct supplyAcct,String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("withdat", withdat);
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return prdPrcCostManageService.findCprlDetailsInfo(content, pager);
	}
	/**
	 * 跳转到菜品利润                   主页面
	 * @throws Exception 
	 */
	@RequestMapping("/toCplrList")
	public ModelAndView toCplrList(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(PrdPrcCMConstant.LIST_CPLR,modelMap);
	}
	/**
	 * 跳转到菜品利润页面           左侧
	 * @throws Exception 
	 */
	@RequestMapping("/listPositnCplr")
	public ModelAndView listPositnCplr(ModelMap modelMap,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPosion", positnService.findAllPositn(positn));
		return new ModelAndView(PrdPrcCMConstant.LIST_POSITN_CPLR,modelMap);
	}
	/**
	 * 跳转到菜品利润页面           右侧
	 * @throws Exception 
	 */
	@RequestMapping("/tableFromCplrPositn")
	public ModelAndView tableFromCplrPositn(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(PrdPrcCMConstant.TABLE_POSITN_CPLR,modelMap);
	}
	/***********************************************************菜品利润end************************************************/	

	/***********************************************************核减start************************************************/
	/**
	 * 打开核减详细
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/openHeJian")
	public ModelAndView openHeJian(ModelMap modelMap,HttpSession session,Date bdat,Date edat, SupplyAcct supplyacct,String checkMis)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyacct", supplyacct);
		modelMap.put("type", checkMis);
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				modelMap.put("firm",accountPositn.getPositn().getCode());
			}
		}
		return new ModelAndView(PrdPrcCMConstant.LIST_HEJIAN,modelMap);
	}
	/**
	 * 核减明细
	 */
	@RequestMapping(value = "/toHeJian")
	public ModelAndView toHeJian(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(PrdPrcCMConstant.LIST_HEJIAN,modelMap);
	}	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findHejianHeaders")
	@ResponseBody	
	public Object getHejianHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN_FROZEN);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseHejian")
	public ModelAndView toColChooseHejian(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}	
	/**
	 * 查询核减明细
	 */
	@RequestMapping("/findHejian")
	@ResponseBody
	public Object findHejian(ModelMap modelMap,HttpSession session,String page,String rows,	SupplyAcct supplyAcct,String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				content.put("firmCode",accountPositn.getPositn().getCode());
			}
		}else{
			content.put("firmCode",supplyAcct.getFirmcode());
		}
		content.put("supplyAcct",supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			content.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			content.put("COSTITEM", "COSTITEM");
		}else{
			content.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			content.put("COSTITEM", "firmCOSTITEM");
		}
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return prdPrcCostManageService.findHejian(content, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportHejian")
	@ResponseBody
	public void exportHejian(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "核减明细";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(accountPositn.getPositn().getCode());
			}
		}
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct",supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_HEJIAN);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findHejian(condition,pager).getRows(), "核减明细", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_HEJIAN));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printHejian")
	public ModelAndView printHejian(ModelMap modelMap, Page pager, HttpSession session, String type, String sort, String order,
			SupplyAcct supplyAcct, String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
		params.put("firmCode",supplyAcct.getFirmcode());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("sp_code", supplyAcct.getSp_code());
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(accountPositn.getPositn().getCode());
			}
		}
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("List",prdPrcCostManageService.findHejian(condition, pager).getRows());
	    parameters.put("report_name", "核减明细");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printHejian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_HEJIAN,PrdPrcCMConstant.REPORT_EXP_URL_HEJIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/***********************************************************核减end**************************************************/
	/**************************************************分店耗用对比start*********************************************/	
	/**
	 * 分店耗用
	 */
	@RequestMapping(value = "/toFirmUseCompare")
	public ModelAndView toFirmUseCompare(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		return new ModelAndView(PrdPrcCMConstant.LIST_FIRMUSECOMPARE,modelMap);
	}
	/**
	 * 分店耗用查询
	 */
	@RequestMapping(value = "/findFirmUseCompare")
	public ModelAndView findFirmUseCompare(ModelMap modelMap,HttpSession session,Page page,String dayCheckBox,String firmCode,String firmDes,Date bdat,Date edat,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		condition.put("yngr", dayCheckBox);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);		
		condition.put("firmCode", firmCode);
		condition.put("firmDes", firmDes);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("SUPPLYACCT", "SUPPLYACCT");
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("SUPPLYACCT", "firmSUPPLYACCT");
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("positnHeadList", prdPrcCostManageService.findPositnHead(condition));
		modelMap.put("positnList", JSONArray.fromObject(prdPrcCostManageService.findPositnHead(condition)));
		modelMap.put("dataList", JSONArray.fromObject(prdPrcCostManageService.findFirmUseCompare(condition, page)));
		modelMap.put("dayCheckBox", dayCheckBox);
		modelMap.put("firmCode", firmCode);
		modelMap.put("firmDes", firmDes);
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		//将类别中的代码单引号去掉
//		supplyAcct.setGrptyp(supplyAcct.getGrptyp().replace("'", ""));
//		supplyAcct.setGrp(supplyAcct.getGrp().replace("'", ""));
//		supplyAcct.setTyp(supplyAcct.getTyp().replace("'", ""));
		modelMap.put("supplyAcct",supplyAcct);
		return new ModelAndView(PrdPrcCMConstant.LIST_FIRMUSECOMPARE,modelMap);
	}
	/**************************************************分店耗用对比end*********************************************/	

	/********************************************毛利率周分析****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_MAOWEEKANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_MAOWEEKANA);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.REPORT_MAOWEEKANA));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMaoWeekAnaHeaders")
	@ResponseBody
	public Object findMaoWeekAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_MAOWEEKANA);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_MAOWEEKANA));
		
		return columns;
	}
	
	/**
	 * 跳转到毛利率周分析页面
	 * @return
	 */
	@RequestMapping("/toMaoWeekAna")
	public ModelAndView toMaoWeekAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_MAOWEEKANA);
		return new ModelAndView(PrdPrcCMConstant.SHOW_MAOWEEKANA,modelMap);
	}
	/**
	 * 查询毛利率周分析内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findMaoWeekAna")
	@ResponseBody
	public Object MaoWeekAna(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,String dept) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("dept", dept);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		
		ReportObject<Map<String,Object>> result = prdPrcCostManageService.findMaoWeekAna(condition,pager);
		List<Map<String,Object>> rowsList = result.getRows();//结果集(不包含期初结存)
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//最终的结果集
		List<Map<String,Object>> list = prdPrcCostManageService.findMaoWeekAnaQiChu1(condition);//计算年初到指定日期入减出
		Map<String,Object> bperiodMap = prdPrcCostManageService.findMaoWeekAnaQiChu2(condition);//计算年初结存
		for(Map<String,Object> maoWeekAna : rowsList){
			double bperiod = 0;
			if(null != bperiodMap.get(maoWeekAna.get("FIRMCODE").toString())){
				bperiod = Double.valueOf(""+bperiodMap.get(maoWeekAna.get("FIRMCODE").toString()));
			}
			for(Map<String,Object> mao : list){
				if(maoWeekAna.get("FIRMCODE").toString().equals(mao.get("FIRMCODE").toString()) &&
						maoWeekAna.get("WEEKNO").toString().equals(mao.get("WEEKNO").toString())){
					
					maoWeekAna.put("BPERIOD", Double.valueOf(""+mao.get("BPERIOD")));//年初到指定日期入减出
					
					maoWeekAna.put("BALANCES",Double.valueOf(""+mao.get("BPERIOD"))+Double.valueOf(""+maoWeekAna.get("INAMT"))-Double.valueOf(""+maoWeekAna.get("COST")));//年初到指定日期入减出+日期段的入-出
				}
			}
			maoWeekAna.put("BPERIOD", bperiod+Double.valueOf(""+maoWeekAna.get("BPERIOD")));
			maoWeekAna.put("BALANCES",bperiod+Double.valueOf(""+maoWeekAna.get("BALANCES")));
			resultRowsList.add(maoWeekAna);
		}
		result.setRows(resultRowsList);
		return result;
	}
	/**
	 * 打印毛利率周分析报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printMaoWeekAna")
	public ModelAndView printMaoWeekAna(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct,String dept)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("dept", dept);
		condition.put("supplyAcct", supplyAcct);
		
		Map<String,Object> params = new HashMap<String,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		}
		if(supplyAcct.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		}
		params.put("positn", supplyAcct.getPositn());
		params.put("dept",dept);
		//----------------处理数据start---------------------------------------------
		ReportObject<Map<String,Object>> result = prdPrcCostManageService.findMaoWeekAna(condition,pager);
		List<Map<String,Object>> rowsList = result.getRows();
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//最终的结果集
		List<Map<String,Object>> list = prdPrcCostManageService.findMaoWeekAnaQiChu1(condition);//计算年初到指定日期入减出
		Map<String,Object> bperiodMap = prdPrcCostManageService.findMaoWeekAnaQiChu2(condition);//计算年初结存
		for(Map<String,Object> maoWeekAna : rowsList){
			double bperiod = 0;
			if(null != bperiodMap.get(maoWeekAna.get("FIRMCODE").toString())){
				bperiod = Double.valueOf(""+bperiodMap.get(maoWeekAna.get("FIRMCODE").toString()));
			}
			for(Map<String,Object> mao : list){
				if(maoWeekAna.get("FIRMCODE").toString().equals(mao.get("FIRMCODE").toString()) &&
						maoWeekAna.get("WEEKNO").toString().equals(mao.get("WEEKNO").toString())){
					
					maoWeekAna.put("BPERIOD", Double.valueOf(""+mao.get("BPERIOD")));//年初到指定日期入减出
					
					maoWeekAna.put("BALANCES",Double.valueOf(""+mao.get("BPERIOD"))+Double.valueOf(""+maoWeekAna.get("INAMT"))-Double.valueOf(""+maoWeekAna.get("COST")));//年初到指定日期入减出+日期段的入-出
				}
			}
			maoWeekAna.put("BPERIOD", bperiod+Double.valueOf(""+maoWeekAna.get("BPERIOD")));
			maoWeekAna.put("BALANCES",bperiod+Double.valueOf(""+maoWeekAna.get("BALANCES")));
			resultRowsList.add(maoWeekAna);
		}
		//----------------处理数据end---------------------------------------------
		modelMap.put("List",resultRowsList);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "毛利率周分析");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));   
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printMaoWeekAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_MAOWEEKANA,PrdPrcCMConstant.REPORT_EXP_URL_MAOWEEKANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出毛利率周分析
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportMaoWeekAna")
	@ResponseBody
	public void exportMaoWeekAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String dept) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "毛利率周分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("dept", dept);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_MAOWEEKANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		ReportObject<Map<String,Object>> result = prdPrcCostManageService.findMaoWeekAna(condition,pager);
		List<Map<String,Object>> rowsList = result.getRows();//结果集(不包含期初结存)
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//最终的结果集
		List<Map<String,Object>> list = prdPrcCostManageService.findMaoWeekAnaQiChu1(condition);//计算年初到指定日期入减出
		Map<String,Object> bperiodMap = prdPrcCostManageService.findMaoWeekAnaQiChu2(condition);//计算年初结存
		for(Map<String,Object> maoWeekAna : rowsList){
			double bperiod = 0;
			if(null != bperiodMap.get(maoWeekAna.get("FIRMCODE").toString())){
				bperiod = Double.valueOf(""+bperiodMap.get(maoWeekAna.get("FIRMCODE").toString()));
			}
			for(Map<String,Object> mao : list){
				if(maoWeekAna.get("FIRMCODE").toString().equals(mao.get("FIRMCODE").toString()) &&
						maoWeekAna.get("WEEKNO").toString().equals(mao.get("WEEKNO").toString())){
					
					maoWeekAna.put("BPERIOD", Double.valueOf(""+mao.get("BPERIOD")));//年初到指定日期入减出
					
					maoWeekAna.put("BALANCES",Double.valueOf(""+mao.get("BPERIOD"))+Double.valueOf(""+maoWeekAna.get("INAMT"))-Double.valueOf(""+maoWeekAna.get("COST")));//年初到指定日期入减出+日期段的入-出
				}
			}
			maoWeekAna.put("BPERIOD", bperiod+Double.valueOf(""+maoWeekAna.get("BPERIOD")));
			maoWeekAna.put("BALANCES",bperiod+Double.valueOf(""+maoWeekAna.get("BALANCES")));
			resultRowsList.add(maoWeekAna);
		}
		
		exportExcelMap.creatWorkBook(response.getOutputStream(), resultRowsList, "毛利率周分析", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	/********************************************毛利率周分析****************************************************/

	/********************************************分类别成本周趋势报表****************************************************/
	/**
	 * 打开分类别成本周趋势
	 */
	@RequestMapping(value = "/openType")
	public ModelAndView openType(ModelMap modelMap,String firm,String bdat,String edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "分类别成本周趋势");
		modelMap.put("firm", URLDecoder.decode(firm,"UTF-8"));
		modelMap.put("bdat", DateFormat.getDateByString(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		return new ModelAndView(PrdPrcCMConstant.SHOW_GRPWEEK,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findGrpWeekHeaders")
	@ResponseBody
	public Object findGrpWeekHeaders(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Grp grp = new Grp();
		grp.setAcct(session.getAttribute("ChoiceAcct").toString());
		Map<String,Object> columns = new HashMap<String,Object>();
		columns.put("columns", prdPrcCostManageService.findGrp());
		return columns;
	}
	/**
	 * 分类别成本周趋势
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findGrpWeek")
	@ResponseBody
	public Object findGrpWeek(ModelMap modelMap,HttpSession session,String firm,Date bdat,Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("firm", URLDecoder.decode(firm,"UTF-8"));
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		return prdPrcCostManageService.findGrpFirm(condition);
				
	}
	/**
	 * 导出分类别成本周趋势
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportGrpWeek")
	@ResponseBody
	public void exportGrpWeek(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String firm,String bdat,String edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "分类别成本周趋势";
		Map<String,Object> condition = new HashMap<String,Object>();
		dictColumns.setTableName("分类别成本周趋势");
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		condition.put("firm", URLDecoder.decode(firm,"UTF-8"));
		condition.put("bdat", DateFormat.getDateByString(bdat, "yyyy-MM-dd"));
		condition.put("edat", DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		prdPrcCostManageService.exportGrpWeek(response.getOutputStream(), condition);
		
	}
	/********************************************分类别成本周趋势报表****************************************************/

	/********************************************应产率分析****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseShouldYieldAna")
	public ModelAndView toColChooseShouldYieldAna(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.REPORT_SHOULDYIEDLANA));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findShouldYieldAnaHeaders")
	@ResponseBody
	public Object findShouldYieldAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_SHOULDYIEDLANA));
		columns.put("frozenColumns", PrdPrcCMConstant.REPORT_SHOULDYIEDLANA_FROZEN);
		return columns;
	}
	/**
	 * 跳转到应产率分析报表页面
	 * @return
	 */
	@RequestMapping("/toShouldYieldAna")
	public ModelAndView toShouldYieldAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		return new ModelAndView(PrdPrcCMConstant.SHOULDYIEDLANA,modelMap);
	}
	/**
	 * 查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findShouldYieldAna")
	@ResponseBody
	public Object findShouldYieldAna(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,Date inbdat,Date inedat,Date lybdat,Date lyedat,String byly,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("inbdat", DateFormat.formatDate(inbdat, "yyyy-MM-dd"));
		condition.put("inedat", DateFormat.formatDate(inedat, "yyyy-MM-dd"));
		condition.put("lybdat", DateFormat.formatDate(lybdat, "yyyy-MM-dd"));
		condition.put("lyedat", DateFormat.formatDate(lyedat, "yyyy-MM-dd"));
		condition.put("byly", byly);
		
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return prdPrcCostManageService.findShouldYieldAna(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printShouldYieldAna")
	public ModelAndView printShouldYieldAna(ModelMap modelMap,Page pager,HttpSession session,String type,String sort,String order,Date inbdat,
			Date inedat,Date lybdat,Date lyedat,String byly,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();

		params.put("sort", sort);
		params.put("order", order);
		params.put("inbdat", DateFormat.getStringByDate(inbdat, "yyyy-MM-dd"));
		params.put("inedat", DateFormat.getStringByDate(inedat, "yyyy-MM-dd"));
		params.put("lybdat", DateFormat.getStringByDate(lybdat, "yyyy-MM-dd"));
		params.put("lyedat", DateFormat.getStringByDate(lyedat, "yyyy-MM-dd"));
		params.put("byly", byly);
		params.put("supplyAcct.positn",supplyAcct.getPositn());
		params.put("supplyAcct.grptyp",supplyAcct.getGrptyp());
		params.put("supplyAcct.grp",supplyAcct.getGrp());
		params.put("supplyAcct.chktyp",supplyAcct.getChktyp());
		params.put("supplyAcct.grpdes", supplyAcct.getGrpdes());
		params.put("supplyAcct.sp_code", supplyAcct.getSp_code());
		
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("inbdat", DateFormat.formatDate(inbdat, "yyyy-MM-dd"));
		condition.put("inedat", DateFormat.formatDate(inedat, "yyyy-MM-dd"));
		condition.put("lybdat", DateFormat.formatDate(lybdat, "yyyy-MM-dd"));
		condition.put("lyedat", DateFormat.formatDate(lyedat, "yyyy-MM-dd"));
		condition.put("byly", byly);
		modelMap.put("List",prdPrcCostManageService.findShouldYieldAna(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "应产率分析");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat",DateFormat.getStringByDate(inbdat, "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(inedat, "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printShouldYieldAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_SHOULDYIELDANA,PrdPrcCMConstant.REPORT_PRINT_URL_SHOULDYIELDANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出应产率分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportShouldYieldAna")
	@ResponseBody
	public void exportShouldYieldAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,Date inbdat,Date inedat,Date lybdat,Date lyedat,String byly,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "应产率分析";
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("inbdat", DateFormat.formatDate(inbdat, "yyyy-MM-dd"));
		condition.put("inedat", DateFormat.formatDate(inedat, "yyyy-MM-dd"));
		condition.put("lybdat", DateFormat.formatDate(lybdat, "yyyy-MM-dd"));
		condition.put("lyedat", DateFormat.formatDate(lyedat, "yyyy-MM-dd"));
		condition.put("byly", byly);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_SHOULDYIEDLANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findShouldYieldAna(condition,pager).getRows(), "应产率分析", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	
	/********************************************加工间综合利润分析****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseJGJLiRunAna")
	public ModelAndView toColChooseJGJLiRunAna(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJLIRUNANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_JGJLIRUNANA);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.REPORT_JGJLIRUNANA));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findJGJLiRunAnaHeaders")
	@ResponseBody
	public Object findJGJLiRunAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJLIRUNANA);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_JGJLIRUNANA));
		columns.put("frozenColumns", PrdPrcCMConstant.REPORT_JGJLIRUNANA_FROZEN);
		return columns;
	}
	/**
	 * 跳转到加工间综合利润分析报表页面
	 * @return
	 */
	@RequestMapping("/toJGJLiRunAna")
	public ModelAndView toJGJLiRunAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_JGJLIRUNANA);
		return new ModelAndView(PrdPrcCMConstant.JGJLIRUNANA,modelMap);
	}
	/**
	 * 查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findJGJLiRunAna")
	@ResponseBody
	public Object findJGJLiRunAna(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return prdPrcCostManageService.findJGJLiRunAna(condition,pager);
	}
	/**
	 * 打印加工间综合利润分析报表页面
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printJGJLiRunAna")
	public ModelAndView printJGJLiRunAna(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getBdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("grpdes", supplyAcct.getGrpdes());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",prdPrcCostManageService.findJGJLiRunAna(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "加工间综合利润分析");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printJGJLiRunAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_JGJLIRUN,PrdPrcCMConstant.REPORT_PRINT_URL_JGJLIRUN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出加工间综合利润分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportJGJLiRunAna")
	@ResponseBody
	public void exportJGJLiRunAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "加工间综合利润分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJLIRUNANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findJGJLiRunAna(condition,pager).getRows(), "加工间综合利润分析", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	
	/**
	 * 跳转到加工产品理论成本列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseJGJProductTheoryCost")
	public ModelAndView toColChooseJGJProductTheoryCost(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJPRODUCTCOST);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_JGJPRODUCTCOST);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.REPORT_JGJPRODUCTCOST));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询加工产品理论成本表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findJGJProductTheoryCostHeaders")
	@ResponseBody
	public Object findJGJProductTheoryCostHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJPRODUCTCOST);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.REPORT_JGJPRODUCTCOST));
		columns.put("frozenColumns", PrdPrcCMConstant.REPORT_JGJLIRUNANA_FROZEN);
		return columns;
	}
	/**
	 * 跳转到加工间加工产品理论成本页面
	 * @return
	 */
	@RequestMapping("/toJGJProductTheoryCost")
	public ModelAndView toJGJProductTheoryCost(ModelMap modelMap,Date bdat,Date edat,String code,String positn){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_JGJPRODUCTCOST);
		try {
			positn = URLDecoder.decode(positn, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		modelMap.put("code", code);
		modelMap.put("positn", positn);
		return new ModelAndView(PrdPrcCMConstant.JGJPRODUCTCOST,modelMap);
	}
	/**
	 * 查询加工产品理论成本报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findJGJProductTheoryCost")
	@ResponseBody
	public Object findJGJProductTheoryCost(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return prdPrcCostManageService.findProductTheoryCost(condition,pager);
	}
	/**
	 * 打印加工产品理论成本
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printJGJProductTheoryCost")
	public ModelAndView printJGJProductTheoryCost(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getBdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("grpdes", supplyAcct.getGrpdes());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",prdPrcCostManageService.findProductTheoryCost(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "加工产品理论成本");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/FendianLeibieHuizong/printPositnCategorySum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_EXTHEORYCOST,PrdPrcCMConstant.REPORT_PRINT_URL_EXTHEORYCOST);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出加工产品理论成本报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportJGJProductTheoryCost")
	@ResponseBody
	public void exportJGJProductTheoryCost(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "加工产品理论成本";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_JGJPRODUCTCOST);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findProductTheoryCost(condition,pager).getRows(), "加工产品理论成本", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	/***********************************************************生产耗用start**************************************************/
	/**
	 * 跳转加工耗用
	 */
	@RequestMapping("/toProCostAna")
	public ModelAndView toProCostAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", PrdPrcCMConstant.TABLE_NAME_PROCOSTANA);
		return new ModelAndView(PrdPrcCMConstant.LIST_PROCOSTANA,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findProCostAnaHeaders")
	@ResponseBody	
	public Object getProCostAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_PROCOSTANA);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_PROCOSTANA));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_PROCOSTANAE_FROZEN);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseProCostAna")
	public ModelAndView toColChooseProCostAna(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_PROCOSTANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "supply");
		modelMap.put("tableName", PrdPrcCMConstant.TABLE_NAME_PROCOSTANA);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_PROCOSTANA));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询生产耗用分析
	 */
	@RequestMapping("/findProCostAna")
	@ResponseBody
	public Object findProCostAna(ModelMap modelMap,HttpSession session,String page,String rows,ProCostAna proCostAna){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		proCostAna.setFirm(CodeHelper.replaceCode(proCostAna.getFirm()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return prdPrcCostManageService.findProCostAna(proCostAna, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportProCostAna")
	@ResponseBody
	public void exportProCostAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,ProCostAna proCostAna) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "生产耗用分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_PROCOSTANA);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), prdPrcCostManageService.findProCostAna(proCostAna,pager).getRows(), "生产耗用分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_PROCOSTANA));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printProCostAna")
	public ModelAndView printProCostAna(ModelMap modelMap,Page pager,HttpSession session,String type,String page,String rows,ProCostAna proCostAna)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		params.put("bdat",proCostAna);
		params.put("grptyp",proCostAna.getGrptyp());
		params.put("grp",proCostAna.getGrp());
		params.put("typ",proCostAna.getTyp());
		params.put("sp_code",proCostAna.getSp_code());
		if(proCostAna.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(proCostAna.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",proCostAna.getBdat());
		}
		params.put("firm",proCostAna.getFirm());
		if(proCostAna.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(proCostAna.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",proCostAna.getEdat());
		}	
		modelMap.put("List",prdPrcCostManageService.findProCostAna(proCostAna, pager).getRows());
	    parameters.put("report_name", "生产耗用分析");
	    modelMap.put("actionMap", params);
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/prdPrcCostManage/printProCostAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_PROCOSTANA,PrdPrcCMConstant.REPORT_EXP_URL_PROCOSTANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		

	/***********************************************************生产耗用start**************************************************/		
}
