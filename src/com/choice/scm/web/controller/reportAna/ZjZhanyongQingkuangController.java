package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.ZjZhanyongQingkuangService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("ZjZhanyongQingkuang")
public class ZjZhanyongQingkuangController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private ZjZhanyongQingkuangService zjZhanyongQingkuangService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************资金占用情况****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseZiJinZhanYongQingKuang")
	public ModelAndView toColChooseChkinDS(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZIJINZHANYONGQINGKUANG);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_ZIJINZHANYONGQINGKUANG);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 跳转到资金占用情况页面
	 * @return
	 */
	@RequestMapping("/toZiJinZhanYongQingKuang")
	public ModelAndView toZiJinZhanYongQingKuang(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_ZIJINZHANYONGQINGKUANG);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_ZIJINZHANYONGQINGKUANG,modelMap);
	}
	
	/**
	 * 查询资金占用情况     表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findZiJinZhanYongQingKuangHeaders")
	@ResponseBody
	public Object getZiJinZhanYongQingKuangHeaders(HttpSession session, String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZIJINZHANYONGQINGKUANG);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG_FROZEN);
		return columns;
	}
	
	/**
	 * 资金占用情况  table  
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findZiJinZhanYongQingKuang")
	@ResponseBody
	public Object findZiJinZhanYongQingKuang(ModelMap modelMap, HttpSession session, String page, String rows, String sort,
			String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return zjZhanyongQingkuangService.findZiJinZhanYongQingKuang(condition, pager);
	}
	
	/**
	 * 导出资金占用情况
	 * @throws Exception
	 */
	@RequestMapping("/exportZiJinZhanYongQingKuang")
	@ResponseBody
	public void exportZiJinZhanYongQingKuang(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "资金占用情况";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZIJINZHANYONGQINGKUANG);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), zjZhanyongQingkuangService.findZiJinZhanYongQingKuang(condition,pager).getRows(), "资金占用情况", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG));
	}
	
	/**
	 * 资金占用情况     打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printZiJinZhanYongQingKuang")
	public ModelAndView printZiJinZhanYongQingKuang(ModelMap modelMap, Page pager, HttpSession session, String type,
			String sort, String order, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		
		Map<String,Object> params = new HashMap<String,Object>();//回调参数
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("year", DateFormat.getYear(supplyAcct.getBdat()));
		params.put("positn", CodeHelper.replaceCode(supplyAcct.getPositn()));
		params.put("grptyp", CodeHelper.replaceCode(supplyAcct.getGrptyp()));
		params.put("grp", CodeHelper.replaceCode(supplyAcct.getGrp()));
		params.put("sort",sort);//回调参数
		params.put("order",order);//回调参数
		modelMap.put("actionMap", params);//回调参数
		
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
	    modelMap.put("List",zjZhanyongQingkuangService.findZiJinZhanYongQingKuang(condition, pager).getRows());//报表list
	    HashMap<Object,Object>  parameters = new HashMap<Object,Object>();//报表map    
	    parameters.put("report_name", "资金占用情况");//报表map   
	    parameters.put("bdat", supplyAcct.getBdat());
	    parameters.put("edat", supplyAcct.getEdat());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);//报表map   
	 	modelMap.put("action", "/ZjZhanyongQingkuang/printZiJinZhanYongQingKuang.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_ZIJINZHANYONGQINGKUANG,SupplyAnaConstants.REPORT_PRINT_URL_ZIJINZHANYONGQINGKUANG);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
}
