package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.CgCesuanService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("CgCesuan")
public class CgCesuanController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CgCesuanService cgCesuanService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	
	/***********************************************************采购测算start**************************************************/
	/**
	 * 跳转采购测算分析
	 */
	@RequestMapping("/toCaiGouCeSuan")
	public ModelAndView toCaiGouCeSuan(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(ScmStringConstant.LIST_CAIGOUCESUAN,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findCaiGouCeSuanHeaders")
	@ResponseBody	
	public Object getCaiGouCeSuanHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.CAIGOUCESUAN_TABLE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.CAIGOUCESUAN_COLUMN));
		columns.put("frozenColumns", ScmStringConstant.FROZEN_CAIGOUCESUAN);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseCaiGouCeSuan")
	public ModelAndView toColChooseCaiGouCeSuan(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.CAIGOUCESUAN_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.CAIGOUCESUAN_TABLE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.CAIGOUCESUAN_COLUMN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询采购测算分析分析
	 */
	@RequestMapping("/findCaiGouCeSuan")
	@ResponseBody
	public Object findCaiGouCeSuan(ModelMap modelMap,HttpSession session,String page,String rows,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		
		return cgCesuanService.findCaiGouCeSuan(supplyAcct, pager);
	}
	/**
	 * 计算采购测算分析
	 */
	@RequestMapping("/changeCaiGouCeSuan")
	@ResponseBody
	public Object changeCaiGouCeSuan(ModelMap modelMap,HttpSession session,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setBdat(DateFormat.formatDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		supplyAcct.setEdat(DateFormat.formatDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		return cgCesuanService.changeCaiGouCeSuan(supplyAcct);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCaiGouCeSuan")
	@ResponseBody
	public void exportCaiGouCeSuan(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "采购测算分析分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.CAIGOUCESUAN_TABLE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), cgCesuanService.findCaiGouCeSuan(supplyAcct,pager).getRows(), "采购测算分析分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.CAIGOUCESUAN_COLUMN));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printCaiGouCeSuan")
	public ModelAndView printCaiGouCeSuan(ModelMap modelMap, Page pager, HttpSession session, String type,
			String page, String rows, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	 	params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	 	params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
	 	params.put("sp_code", supplyAcct.getSp_code());
		modelMap.put("List",cgCesuanService.findCaiGouCeSuan(supplyAcct, pager).getRows());
	    parameters.put("report_name", "采购测算分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
	    parameters.put("sp_code", supplyAcct.getSp_code());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CgCesuan/printCaiGouCeSuan.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_CAIGOUCESUAN, SupplyAnaConstants.REPORT_PRINT_URL_CAIGOUCESUAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		

	/***********************************************************采购测算分析end**************************************************/	
}
