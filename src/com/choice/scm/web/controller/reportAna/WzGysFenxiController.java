package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.reportAna.WzGysFenxiService;

@Controller
@RequestMapping("WzGysFenxi")
public class WzGysFenxiController {

	@Autowired
	private WzGysFenxiService wzGysFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	AcctService acctService;
	
	/******************************************************************
	 * 分店分析报表列表
	 * @return
	 */
	@RequestMapping("/toSupplyAnaReport")
	public ModelAndView toSupplyAnaReport(ModelMap modelMap,HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		try {
			if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			    modelMap.put("YNKC", "N");
			}
		} catch (CRUDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView(SupplyAnaConstants.REPORT_LIST);
	}

	/**
	 * 分店分析报表列表
	 * @return
	 */
	@RequestMapping("/toSupplyAnaReportMis")
	public ModelAndView toSupplyAnaReportMis(){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAnaConstants.MIS_REPORT_LIST);
	}
	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/********************************************物资供应商分析****************************************************/
	/**
	 * 跳转到物资供应商分析页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/toSupplyDeliver")
	public ModelAndView toSupplyDeliver(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_SUPPLYDELIVER);
	}
	/**
	 * 查询表头
	 * @param session
	 * @param spcode
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findSupplyDeliverHeader")
	@ResponseBody
	public Object findSupplyDeliverHeader(HttpSession session, String spcode) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return wzGysFenxiService.findSupplyDeliverHeaders(spcode);
	}
	/**
	 * 查询物资供应商分析内容
	 * @param modelMap
	 * @param session
	 * @param spcode
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findSupplyDeliver")
	@ResponseBody
	public Object findSupplyDeliver(ModelMap modelMap, HttpSession session, String spcode) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return wzGysFenxiService.findSupplyDeliver(spcode);
	}
	
	/**
	 * 导出物资供应商分析excel文件
	 * @param response
	 * @param request
	 * @param session
	 * @param spcode
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyDeliver")
	@ResponseBody
	public void exportSupplyDeliver(HttpServletResponse response, HttpServletRequest request, HttpSession session, String spcode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资供应商分析";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		wzGysFenxiService.exportSupplyDeliver(response.getOutputStream(), spcode);
		
	}
	
}
