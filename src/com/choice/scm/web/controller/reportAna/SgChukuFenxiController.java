package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.SgChukuFenxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("SgChukuFenxi")
public class SgChukuFenxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private SgChukuFenxiService sgChukuFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************供应商申购出库分析****************************************************/
	/**
	 * 跳转到申购出库分析页面
	 * @param modelMap
	 * @return
	 * @author ZGL
	 */
	@RequestMapping("/toApplyOutStatistics")
	public ModelAndView toApplyOutStatistics(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.BASICINFO_REPORT_APPLYOUTSTATISTICS);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_APPLYOUTSTATISTICS);
	}
	/**
	 * 得到申购出库分析表头
	 * @param session
	 * @return
	 * @author ZGL
	 */
	@RequestMapping("/findApplyOutStatisticsHeader")
	@ResponseBody
	public Object findApplyOutStatisticsHeader(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_APPLYOUTSTATISTICS);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_APPLYOUTSTATISTICS));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_APPLYOUTSTATISTICS_FROZEN);
		return columns;		
	}
	/**
	 * 查询申购出库分析内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CURDException
	 */
	@RequestMapping("/findApplyOutStatistics")
	@ResponseBody
	public Object findApplyOutStatistics(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setFirmcode(CodeHelper.replaceCode(supplyAcct.getFirmcode()));
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return sgChukuFenxiService.findApplyOutAmount(condition,pager);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseApplyOutStatistics")
	public ModelAndView ApplyOutStatistics(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_APPLYOUTSTATISTICS);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_APPLYOUTSTATISTICS);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_APPLYOUTSTATISTICS));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 导出申购出库分析excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportApplyOutStatistics")
	@ResponseBody
	public void exportApplyOutStatistics(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商申购出库分析汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_APPLYOUTSTATISTICS);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), sgChukuFenxiService.findApplyOutAmount(condition,pager).getRows(), "供应商申购出库分析汇总", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_APPLYOUTSTATISTICS));
	}
	
	/**
	 * 打印申购出库分析
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printApplyOutStatistics")
	public ModelAndView printApplyOutStatistics(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List", sgChukuFenxiService.findApplyOutAmount(condition,pager).getRows());
		params.put("positn",supplyAcct.getPositn());
		params.put("firmcode",supplyAcct.getFirmcode());
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
	    params.put("sp_code", supplyAcct.getSp_code());
	    params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp", supplyAcct.getGrp());
	    params.put("typ", supplyAcct.getTyp());
	    params.put("madeby", session.getAttribute("accountName").toString());
		modelMap.put("actionMap", params);
		
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "供应商申购出库分析汇总");
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy/MM/dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy/MM/dd"));
		parameters.put("madeby", session.getAttribute("accountName").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/SgChukuFenxi/printApplyOutStatistics.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_APPLYOUTSTATISTICS,SupplyAnaConstants.REPORT_EXP_URL_APPLYOUTSTATISTICS);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
}
