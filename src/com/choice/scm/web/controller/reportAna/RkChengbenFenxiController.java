package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.RkChengbenFenxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("RkChengbenFenxi")
public class RkChengbenFenxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private RkChengbenFenxiService rkChengbenFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
		
	/******************************************************************
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/********************************************入库成本分析****************************************************/
	/**
	 * 入库成本分析--列选择
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseReceiptCostAnalysisSum")
	public ModelAndView toColChooseReceiptCostAnalysisSum(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 跳转到入库成本分析页面
	 * @return
	 */
	@RequestMapping("/toReceiptCostAnalysis")
	public ModelAndView toReceiptCostAnalysis(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_SUM);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_RUKUCHENGBENFENXI,modelMap);
	}
	
	/**
	 * 查询入库成本分析     表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findReceiptCostAnalysisHeaders")
	@ResponseBody
	public Object getReceiptCostAnalysisHeaders(HttpSession session, String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		if(typ.equals("sum")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_SUM);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_SUM));
		}
		if(typ.equals("detail")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_DETAIL));
		}
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_FROZEN);
		return columns;
	}
	
	/**
	 * 查询入库明细查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findReceiptCostAnalysisSum")
	@ResponseBody
	public Object findReceiptCostAnalysisSum(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct, String month) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		Calendar cal = Calendar.getInstance();
		supplyAcct.setYearr(""+cal.get(Calendar.YEAR));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("month", month);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkChengbenFenxiService.findReceiptCostAnalysisSum(condition, pager);
	}
	/**
	 * 导出入库成本分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportReceiptCostAnalysisSum")
	@ResponseBody
	public void exportReceiptCostAnalysisSum(HttpServletResponse response, HttpServletRequest request,
			HttpSession session, String sort, String order, SupplyAcct supplyAcct, String month) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库成本分析报表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		conditions.put("supplyAcct", supplyAcct);
		conditions.put("sort", sort);
		conditions.put("order", order);
		conditions.put("month", month);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), rkChengbenFenxiService.findReceiptCostAnalysisSum(conditions, pager).getRows(), "入库成本分析", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
	/**
	 * 入库成本分析     打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printReceiptCostAnalysisSum")
	public ModelAndView printReceiptCostAnalysisSum(ModelMap modelMap, Page pager, HttpSession session, String type,
			String sort, String order, SupplyAcct supplyAcct, String month)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		
		Map<String,String> params = new HashMap<String,String>();//回调参数
		params.put("positn",supplyAcct.getPositn());//回调参数
		params.put("grptyp",supplyAcct.getGrptyp());//回调参数
		params.put("sp_code",supplyAcct.getSp_code());//回调参数
		params.put("grp",supplyAcct.getGrp());//回调参数
		params.put("typ",supplyAcct.getTyp());//回调参数
		params.put("typdes",supplyAcct.getTypdes());//回调参数
		params.put("sort",sort);//回调参数
		params.put("order",order);//回调参数
		params.put("month",month);//回调参数
		modelMap.put("actionMap", params);//回调参数
		
		Map<String,Object> condition = new HashMap<String,Object>();//报表list
		condition.put("supplyAcct", supplyAcct);//报表list
		condition.put("sort", sort);//报表list
		condition.put("order", order);//报表list
		condition.put("month", month);//报表list
	    modelMap.put("List",rkChengbenFenxiService.findReceiptCostAnalysisSum(condition, pager).getRows());//报表list
	    HashMap<Object,Object>  parameters = new HashMap<Object,Object>();//报表map    
	    parameters.put("report_name", "入库成本分析");//报表map   
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);//报表map   
	 	modelMap.put("action", "/RkChengbenFenxi/printReceiptCostAnalysisSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_RECEIPTCOSTANALYSISSUM,SupplyAnaConstants.REPORT_PRINT_URL_RECEIPTCOSTANALYSISSUM);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/********************************************周入库成本分析****************************************************/
	/**
	 * 跳转到周入库成本分析页面
	 * @return
	 */
	@RequestMapping("/toZhouRuKuChengBenFenXi")
	public ModelAndView toZhouRuKuChengBenFenXi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	//	modelMap.put("reportName", ScmStringConstant.REPORT_NAME_RECEIPT_COST_ANALYSIS_SUM);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_ZHOURUKUCHENGBENFENXI,modelMap);
	}
	/**
	 * 周入库成本分析--列选择
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseZhouRuKuChengBenFenXi")
	public ModelAndView toColChooseZhouRuKuChengBenFenXi(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询周入库成本分析     表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findZhouRuKuChengBenFenXiHeaders")
	@ResponseBody
	public Object getZhouRuKuChengBenFenXiHeaders(HttpSession session, String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI);
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_ZHOU_RU_KU_CHENG_BEN_FEN_XI));
	
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_ZHOU_RU_KU_CHENG_BEN_FEN_XI_FROZEN);
		return columns;
	}
	/**
	 * 周入库成本分析  table  
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findZhouRuKuChengBenFenXi")
	@ResponseBody
	public Object findZhouRuKuChengBenFenXi(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkChengbenFenxiService.findZhouRuKuChengBenFenXi(condition, pager);
	}
	
	/**
	 * 导出周入库成本分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportZhouRuKuChengBenFenXi")
	@ResponseBody
	public void exportZhouRuKuChengBenFenXi(HttpServletResponse response, HttpServletRequest request,
			HttpSession session, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "周入库成本分析报表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		conditions.put("supplyAcct", supplyAcct);
		conditions.put("sort", sort);
		conditions.put("order", order);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), rkChengbenFenxiService.findZhouRuKuChengBenFenXi(conditions, pager).getRows(), "周入库成本分析", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
	
	/**
	 * 周入库成本分析     打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printZhouRuKuChengBenFenXi")
	public ModelAndView printZhouRuKuChengBenFenXi(ModelMap modelMap, Page pager, HttpSession session, String type,
			String sort, String order, SupplyAcct supplyAcct, String month)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		
		Map<String,String> params = new HashMap<String,String>();//回调参数
		params.put("positn", supplyAcct.getPositn());//回调参数
		params.put("grptyp", supplyAcct.getGrptyp());//回调参数
		params.put("grp", supplyAcct.getGrp());//回调参数
		params.put("dat", DateFormat.getStringByDate(supplyAcct.getDat(),"yyyy-MM-dd"));//回调参数
		params.put("sort", sort);//回调参数
		params.put("order", order);//回调参数
		modelMap.put("actionMap", params);//回调参数
		
		Map<String,Object> condition = new HashMap<String,Object>();//报表list
		condition.put("supplyAcct", supplyAcct);//报表list
		condition.put("sort", sort);//报表list
		condition.put("order", order);//报表list
		condition.put("month", month);//报表list
	    modelMap.put("List",rkChengbenFenxiService.findZhouRuKuChengBenFenXi(condition, pager).getRows());//报表list
	    HashMap<Object,Object>  parameters = new HashMap<Object,Object>();//报表map    
	    parameters.put("report_name", "周入库成本分析");//报表map   
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);//报表map   
	 	modelMap.put("action", "/RkChengbenFenxi/printZhouRuKuChengBenFenXi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type, SupplyAnaConstants.REPORT_PRINT_URL_ZHOURUKUCHENGBENFENXI, SupplyAnaConstants.REPORT_PRINT_URL_ZHOURUKUCHENGBENFENXI);//判断跳转路径
	    modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"), modelMap);
	}
	
	
}
