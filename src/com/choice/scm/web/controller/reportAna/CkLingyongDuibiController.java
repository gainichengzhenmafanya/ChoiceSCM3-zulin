package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.CkLingyongDuibiService;

@Controller
@RequestMapping("CkLingyongDuibi")
public class CkLingyongDuibiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private CkLingyongDuibiService ckLingyongDuibiService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/*************************************************仓库领用对比********************************************************/
	/**WarehouseRequisitionedContrast
	 * 跳转到仓库领用对比页面
	 * @param modelMap
	 * @return
	 * @author ZGL
	 */
	@RequestMapping("/toWarehouseRequisitionedContrast")
	public ModelAndView toWarehouseRequisitionedContrast(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "仓库领用对比");
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_WAREHOUSEREQUISITIONEDCONTRAST,modelMap);
	}
	/**
	 * 得到仓库领用对比表头
	 * @param session
	 * @return
	 * @author ZGL
	 * @throws CRUDException 
	 */
	@RequestMapping("/findWarehouseRequisitionedContrastHeader")
	@ResponseBody
	public Object findWarehouseRequisitionedContrastHeader(HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		Map<String,Object> columns = new HashMap<String,Object>();
		columns.put("columns", ckLingyongDuibiService.findWarehouseRequisitionedContrastHeader(condition));
		return columns;		
	}
	/**
	 * 查询仓库领用对比内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CURDException
	 * @author ZGL
	 */
	@RequestMapping("/findWarehouseRequisitionedContrast")
	@ResponseBody
	public Object findWarehouseRequisitionedContrast(ModelMap modelMap, HttpSession session, String page,
			String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckLingyongDuibiService.findCangKuLingYongDuiBi(condition, pager);
	}
	/**
	 * 导出仓库领用对比Excel
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping("/exportCangKuLingYongDuiBi")
	@ResponseBody
	public void exportCangKuLingYongDuiBi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName="仓库领用对比";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ckLingyongDuibiService.exportCangKuLingYongDuiBi(response.getOutputStream(),condition);
	}
}
