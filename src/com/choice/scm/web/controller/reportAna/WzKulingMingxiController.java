package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.WzKulingMingxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("WzKulingMingxi")
public class WzKulingMingxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzKulingMingxiService wzKulingMingxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	

	/********************************************物资库龄明细****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsSupplyKuLingDetail")
	public ModelAndView toColumnsSupplyKuLingDetail(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYKULINGDETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_SUPPLYKULINGDETAIL);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_SUPPLYKULINGDETAIL));
		
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findSupplyKuLingDetailHeaders")
	@ResponseBody
	public Object findSupplyKuLingDetailHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYKULINGDETAIL);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYKULINGDETAIL));
		
		return columns;
	}
	
	/**
	 * 跳转到物资库龄明细页面
	 * @return
	 */
	@RequestMapping("/toSupplyKuLingDetail")
	public ModelAndView toSupplyKuLingDetail(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_SUPPLYKULINGDETAIL);
		return new ModelAndView(SupplyAnaConstants.LIST_SUPPLYKULINGDETAIL,modelMap);
	}
	/**
	 * 查询物资库龄明细内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findSupplyKuLingDetail")
	@ResponseBody
	public Object findSupplyKuLingDetail(ModelMap modelMap,HttpSession session,
			String page, String rows, String sort, String order, SupplyAcct supplyAcct, String age) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("age", age);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return wzKulingMingxiService.findSupplyKuLingDetail(condition,pager);
	}
	
	/**
	 * 打印物资库龄明细报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printSupplyKuLingDetail")
	public ModelAndView printSupplyKuLingDetail(ModelMap modelMap, Page pager, HttpSession session, SupplyAcct supplyAcct,
			String type, String age)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("age",age);
		condition.put("age", age);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",wzKulingMingxiService.findSupplyKuLingDetail(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资库龄明细");
	    parameters.put("age", age);
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzKulingMingxi/printSupplyKuLingDetail.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_SUPPLYKULINGDETAIL,SupplyAnaConstants.REPORT_PRINT_URL_SUPPLYKULINGDETAIL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出物资库龄明细
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyKuLingDetail")
	@ResponseBody
	public void exportSupplyKuLingDetail(HttpServletResponse response, String sort, String order, HttpServletRequest request,
			HttpSession session, String age, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资库龄明细";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("age", age);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYKULINGDETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzKulingMingxiService.findSupplyKuLingDetail(condition,pager).getRows(), "物资库龄明细", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
	
}
