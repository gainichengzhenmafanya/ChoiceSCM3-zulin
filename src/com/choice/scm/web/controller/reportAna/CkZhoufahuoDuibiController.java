package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAna.CkZhoufahuoDuibiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("CkZhoufahuoDuibi")
public class CkZhoufahuoDuibiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CkZhoufahuoDuibiService ckZhoufahuoDuibiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	
	/*************************************************仓库周发货对比********************************************************/
	/**WarehouseRequisitionedContrast
	 * 跳转到仓库周发货对比页面
	 * @param modelMap
	 * @return
	 * @author ZGL
	 */
	@RequestMapping("/toCangKuZhouFaHuoDuiBi")
	public ModelAndView toCangKuZhouFaHuoDuiBi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_CANGKUZHOUFAHUODUIBI);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_CANGKUZHOUFAHUODUIBI,modelMap);
	}
	/**
	 * 得到仓库周发货对比表头
	 * @param session
	 * @return
	 * @author ZGL
	 * @throws CRUDException 
	 */
	@RequestMapping("/findCangKuZhouFaHuoDuiBiHeader")
	@ResponseBody
	public Object findCangKuZhouFaHuoDuiBiHeader(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CANGKUZHOUFAHUODUIBI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CANGKUZHOUFAHUODUIBI));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_CANGKUZHOUFAHUODUIBI_FROZEN);
		return columns;
	}
	/**
	 * 查询仓库周发货对比内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CURDException
	 */
	@RequestMapping("/findCangKuZhouFaHuoDuiBi")
	@ResponseBody
	public Object findCangKuZhouFaHuoDuiBi(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckZhoufahuoDuibiService.findCangKuZhouFaHuoDuiBi(condition, pager);
	}
	/**
	 * 导出仓库周发货对比excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCangKuZhouFaHuoDuiBi")
	@ResponseBody
	public void exportCangKuZhouFaHuoDuiBi(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "仓库周发货对比";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CANGKUZHOUFAHUODUIBI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ReportObject<Map<String,Object>> list = ckZhoufahuoDuibiService.findCangKuZhouFaHuoDuiBi(condition,pager);
		List<Map<String,Object>> lists = list.getRows();
		lists.addAll(list.getFooter());
		exportExcelMap.creatWorkBook(response.getOutputStream(), lists, "仓库周发货对比", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CANGKUZHOUFAHUODUIBI));
	}
	/**
	 * 打印仓库周发货对比
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printCangKuZhouFaHuoDuiBi")
	public ModelAndView printCangKuZhouFaHuoDuiBi(ModelMap modelMap, Page pager, HttpSession session,
			String type, String sort, String order, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("acct",supplyAcct.getAcct());
		params.put("positn",supplyAcct.getPositn());
		params.put("firm",supplyAcct.getFirm());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("sort", sort);
		params.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		modelMap.put("List",ckZhoufahuoDuibiService.findCangKuZhouFaHuoDuiBi(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "仓库周发货对比");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy/MM/dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy/MM/dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkZhoufahuoDuibi/printCangKuZhouFaHuoDuiBi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_CANGKUZHOUFAHUODUIBI,SupplyAnaConstants.REPORT_EXP_URL_CANGKUZHOUFAHUODUIBI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
}
