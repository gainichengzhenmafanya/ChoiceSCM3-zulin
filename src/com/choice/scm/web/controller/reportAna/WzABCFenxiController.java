package com.choice.scm.web.controller.reportAna;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.service.reportAna.WzABCFenxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("WzABCFenxi")
public class WzABCFenxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzABCFenxiService WzABCFenxiAnaService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************物资ABC分析****************************************************/
	/**
	 * 跳转到物资ABC分析列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseSupplyABCStatisticsSum")
	public ModelAndView toColChooseSupplyABCStatisticsSum(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_SUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_SUM);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_SUPPLYABCSTATISTICS_SUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findSupplyABCStatisticsSumHeaders")
	@ResponseBody
	public Object getSupplyABCStatisticsSumHeaders(HttpSession session, String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		if(typ.equals("sum")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_SUM);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYABCSTATISTICS_SUM));
		}
		if(typ.equals("detail")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_DETAIL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYABCSTATISTICS_DETAIL));
		}
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_SUPPLYABCSTATISTICSSUM_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到物资ABC汇总报表页面
	 * @return
	 */
	@RequestMapping("/toSupplyABCStatisticsSum")
	public ModelAndView toSupplyABCStatisticsSumDetailS(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_SUM);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_SUPPLYABCSTATISTICS,modelMap);
	}
	/**
	 * 查询物资ABC报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findSupplyABCStatisticsSum")
	@ResponseBody
	public Object findSupplyABCStatisticsSum(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, String typ, String abc) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("abc", abc);
		condition.put("typ", typ);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return WzABCFenxiAnaService.findSupplyABCStatistics(condition, pager);
	}
	
	/**
	 * 打印物资ABC分析报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printSupplyABCStatisticsSum")
	public ModelAndView printSupplyABCStatisticsSum(ModelMap modelMap, Page pager, HttpSession session, String typ,
			String type, String sort, String order, String abc)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		condition.put("abc", abc);
		condition.put("typ", typ);
		condition.put("sort", sort);
		condition.put("order", order);
		params.put("abc", abc);
		params.put("typ", typ);
		params.put("sort", sort);
		params.put("order", order);
		modelMap.put("List",WzABCFenxiAnaService.findSupplyABCStatistics(condition, pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资ABC查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzABCFenxi/printSupplyABCStatisticsSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_SUPPLYABCSTATISTICS,SupplyAnaConstants.REPORT_EXP_URL_SUPPLYABCSTATISTICS);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出物资ABC查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyABCStatisticsSum")
	@ResponseBody
	public void exportSupplyABCStatisticsSum(HttpServletResponse response, HttpServletRequest request, HttpSession session,
			String typ, String abc) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资ABC查询报表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		conditions.put("typ", typ);
		conditions.put("abc", abc);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYABCSTATISTICS_DETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), WzABCFenxiAnaService.findSupplyABCStatistics(conditions, pager).getRows(), "物资ABC分析", dictColumnsService.listDictColumnsByTable(dictColumns));
	}	
}
