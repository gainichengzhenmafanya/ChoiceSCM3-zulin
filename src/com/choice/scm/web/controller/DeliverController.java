package com.choice.scm.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.Condition;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.util.AssitResultForSpInfo;
import com.choice.assistant.domain.util.PageCondition;
import com.choice.assistant.domain.util.asstGoods.AssitResultGoods;
import com.choice.assistant.domain.util.asstGoods.AsstGoods;
import com.choice.assistant.domain.util.asstSupplier.AssitSupplierResult;
import com.choice.assistant.service.system.SearchingService;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Account;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.DeliverConstants;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Tax;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.DisFenBoService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.util.MaterialScopeList;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "deliver")
public class DeliverController {

	@Autowired
	private Page pager;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private CodeDesService codeDesService;	
	@Autowired
	private PositnService positnService;
	@Autowired
	private DisFenBoService disFenBoService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private SearchingService searchingService;
	
	/**
	 * 供货商单位设置
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/table")
//	public ModelAndView findAllDelivers(ModelMap modelMap,Page page, Deliver deliver) throws Exception
//	{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		//供应商定义主界面
//		modelMap.put("pageobj", page);
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		CodeDes codeDes=new CodeDes();
//		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
//		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
//		modelMap.put("deliverList", deliverService.findDeliver(deliver,page));
//		modelMap.put("pageobj", page);
//		return new ModelAndView(DeliverConstants.TABLE_DELIVERS,modelMap);
//	}
	/**
	 * 供应商类型列表（左侧）
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllDelivers1(ModelMap modelMap,Page page, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		modelMap.put("delivierListType", deliverService.findDeliverType());
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverConstants.TABLE_DELIVERS1,modelMap);
	}
	/**
	 * 供货商列表（右侧）
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deliverList")
	public ModelAndView findAllDelivers(ModelMap modelMap, Page page, Deliver deliver, String typCode, HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("typCode", typCode);
		return new ModelAndView(DeliverConstants.TABLE_DELIVERS,modelMap);
	}
	
	/**
	 * 查询所有编码----用于删除供应商时候的验证
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteyh")
	@ResponseBody
	public String deleteyh(String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.deleteyh(code);
	}
	
	/**
	 * 打开选择仓位
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectDeliverPositn")
	public ModelAndView selectDeliverPositn(ModelMap modelMap, String deliver,Positn positn,String positncode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("deliver", deliver);
		modelMap.put("positncode", positncode);
		return new ModelAndView(DeliverConstants.SELECT_DELIVERPOSITN, modelMap);
	}
	
	/**
	 * 打开选择仓位==用于供应商对应仓位编码，不可重复
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectDeliverPositns")
	public ModelAndView selectDeliverPositns(ModelMap modelMap, String deliver,Positn positn,String positncode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositns(positn));
		modelMap.put("deliver", deliver);
		modelMap.put("positncode", positncode);
		return new ModelAndView(DeliverConstants.SELECT_DELIVERPOSITNS, modelMap);
	}
	
	/**
	 * 保存默认仓位
	 */
	@RequestMapping(value = "/saveDeliverPositn") 
	public ModelAndView saveDeliverPositn(ModelMap modelMap, String positn, String code1) throws Exception{
		//帐套
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存修改
		Deliver deliver = new Deliver();
		deliver.setCode(code1);
		deliver.setPositn(positn);
		deliverService.saveDeliverPositn(deliver);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 打开新增供货商页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap, String typCode) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商新增页面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		modelMap.put("typCode", typCode);//供货商类型编码
		//modelMap.put("code", deliverService.getMaxCode());
		return new ModelAndView(DeliverConstants.SAVE_DELIVERS,modelMap);
	}
	
	/**
	 * 保存新增供货商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,Page page, Deliver deliver,HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//方向标识符,保存新增后的页面跳转
		String inout = "";
		String str = deliverService.saveDeliver(deliver,session.getAttribute("accountId").toString());
		modelMap.put("pageobj", page);		
		modelMap.put("str", str);
		if("error".equals(str)){//添加保存页面
			inout=DeliverConstants.SAVE_DELIVERS;
		}else{
			inout=StringConstant.ACTION_DONE;
		}
		return new ModelAndView(inout,modelMap);
	}
	
	/**
	 * 供应商物资到期提醒设置 页面 wj
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addWarnDays")
	public ModelAndView addWarnDays(ModelMap modelMap,String typCode) throws Exception{
		modelMap.put("typCode", typCode);
		return new ModelAndView(DeliverConstants.SAVE_WARMDAYS,modelMap);
	}
	
	/**
	 * 保存供应商物资到期提醒设置 wj
	 * @param modelMap
	 * @param typCode
	 * @param warmDays
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveByAddWarmDays")
	public ModelAndView saveByAddWarmDays(ModelMap modelMap,String typCode,String warmDays) throws Exception{
		deliverService.saveWarmDays(typCode, warmDays);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 根据供应商编码查找供应商名称
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findByDeliver")
	@ResponseBody
	public Object findByDeliver(ModelMap modelMap, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//根据供应商编码查找
		return deliverService.findDeliverToJS(deliver);
	}
	
	/**
	 * 查找供货商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchDeliverByKey(ModelMap modelMap, Page page, Deliver deliver, String use, HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//价格模板用到的供应查找
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		deliver.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("deliver", deliver);
		modelMap.put("key", null == deliver.getCode() ? (null == deliver.getInit() ? deliver.getDes() : deliver.getInit()) : deliver.getCode());
		if("spprice".equals(use))
			return new ModelAndView(DeliverConstants.PRICE_DELIVERS,modelMap);
		else
			return new ModelAndView(DeliverConstants.TABLE_DELIVERS,modelMap);
	}

	/**
	 * 报货分拨模块上的小查询页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKeyOnDis")
	public ModelAndView searchByKeyOnDis(ModelMap modelMap,Page page,String sech,String key,Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//报货分拨用到的查找
		if(sech.equals("init")){
			deliver.setInit(key);
		}
		if(sech.equals("code")){
			deliver.setCode(key);
		}
		if(sech.equals("des")){
			deliver.setDes(key);
		}
		if(sech.equals("person1")){
			deliver.setPerson1(key);
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliverList", deliverService.findDeliver(deliver,page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverConstants.SEARCH_DELIVERS,modelMap);
	}
	
	/**
	 * 匹配供货商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchDeliver")
	public ModelAndView searchDeliver(ModelMap modelMap,String sech,String key,String firm,String inout,String action) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Deliver deliver=new Deliver();
		if(null!=action && !"".equals(action) && "init".equals(action)){
			sech="init";//默认让缩写单选按钮选中
		}else{
			if(sech.equals("init")){
				deliver.setInit(key);
			}
		 	if(sech.equals("code")){
				deliver.setCode(key);
			}
			if(sech.equals("des")){
				deliver.setDes(key);
			}
			if(sech.equals("person1")){
				deliver.setPerson1(key);
			}
		}
		modelMap.put("deliverList", deliverService.findDeliverByInout(firm,inout,deliver));
		modelMap.put("sech", sech);
		modelMap.put("key", key);
		modelMap.put("firm", firm);
		modelMap.put("inout", inout);
		return new ModelAndView(DeliverConstants.SEARCH_DELIVERS,modelMap);
	}

	/**
	 * 匹配供货商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchAllDeliver")
	public ModelAndView searchAllDeliver(ModelMap modelMap, Deliver deliver,String defaultCode,String defaultName,Page page) throws Exception
	{
		//报货分拨上的供应商查找主页面
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliverList", deliverService.findAllPageDelivers(deliver,page));
		modelMap.put("queryDeliver", deliver);
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverConstants.SEARCH_ALLDELIVERS,modelMap);
	}
	
	/**
	 * 打开复制页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/copy")
	public ModelAndView copy(ModelMap modelMap, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商复制
		deliver = deliverService.findDeliverByCode(deliver);
		//deliver.setCode(deliverService.getMaxCode());
		modelMap.put("deliver",deliver);
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		return new ModelAndView(DeliverConstants.COPY_DELIVERS,modelMap);
	}
	
	/**
	 * 打开修改页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商修改
		modelMap.put("code", deliver.getCode());
		modelMap.put("deliver",deliverService.findDeliverByCode(deliver));
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		return new ModelAndView(DeliverConstants.UPDATE_DELIVERS,modelMap);
	}

	/**
	 * 保存修改信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author wangchao
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,String oldCode, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存修改
		HashMap<String, Object> deliverMap=new HashMap<String, Object>();
		deliver.setInit(deliver.getInit().toUpperCase());//缩写转为大写
		deliverMap.put("deliver", deliver);
		deliverMap.put("oldCode", oldCode);
		deliverService.updateDeliver(deliverMap);
		deliverService.updateSupply(deliverMap);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 删除供应商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap,Page page, String code, Deliver deliver) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收供应商编码，进行删除
		List<String> deliverList=Arrays.asList(code.split(","));
		deliverService.deleteDeliver(deliverList);
		modelMap.put("pageobj", page);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}

	/**
	 * 查找所有供应商，并返回json数组
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "findAllDeliver")
	@ResponseBody
	public Object findAllDeliver(Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		page.setPageSize(Integer.MAX_VALUE);
		return deliverService.findDeliver(null,page);
	}
	
	/**
	 * 获取供应商类别（已使用）
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "findDeliverCategoryInUse")
	@ResponseBody
	public List<Deliver> findDeliverCategoryInUse() throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.findDeliverCategoryInUse();
	}
	/**
	 * 供应商信息打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeliver")
	public ModelAndView printDeliver(ModelMap modelMap, HttpSession session, String type)throws CRUDException
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商信息打印，接收前台参数
		String accountName=session.getAttribute("accountName").toString();
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("供应商信息表");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");         
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date); 
        parameters.put("madeby", accountName);
        List<Deliver> deliverList = deliverService.findDeliverType();
        List<Deliver> list = new ArrayList<Deliver>();
        for(Deliver code:deliverList){
        	Deliver deliver = new Deliver();
        	deliver.setTyp(code.getTyp());
        	list.addAll(deliverService.printDeliver(deliver));
        }
	    modelMap.put("List",list);
        modelMap.put("parameters", parameters);     		 	
	 	modelMap.put("action", "/deliver/printDeliver.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DeliverConstants.REPORT_PRINT_URL,DeliverConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 显示选择的供应商
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addDeliverBatch")
	public ModelAndView addDeliverBatch(ModelMap modelMap, String defaultName, String defaultCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		return new ModelAndView(DeliverConstants.ADD_DELIVERSBATCH, modelMap);
	}
	
	/**
	 * 选择供应商
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectDeliver")
	public ModelAndView selectDeliver(ModelMap modelMap, Deliver deliver, String defaultName, 
			String defaultCode, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商权限筛选
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("queryDeliver", deliver);
		modelMap.put("pageobj", page);
		modelMap.put("defaultName", defaultName);
		modelMap.put("defaultCode", defaultCode);
		return new ModelAndView(DeliverConstants.SELECT_DELIVERS, modelMap);
	}
	
	/**
	 * 选择供应商
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectOneDeliver")
	public ModelAndView selectOneDeliver(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliver", deliver);
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商定义主界面
		modelMap.put("delivierListType", deliverService.findDeliverType());
		return new ModelAndView(DeliverConstants.SELECT_ONEDELIVERS2, modelMap);
	}
	
	/**
	 * 
	 * @Title: findAllDelivers2 
	 * @Description: (供货商列表右侧（选择单一供货商使用）) 
	 * @Author：LI Shuai
	 * @date：2014-1-23上午9:50:10
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @return
	 * @throws Exception  ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "/deliverList2")
	public ModelAndView findAllDelivers2(ModelMap modelMap, Page page, Deliver deliver, HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		deliver.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("typCode", deliver.getTypCode());
		modelMap.put("deliver", deliver);
		return new ModelAndView(DeliverConstants.TABLE_DELIVERS2,modelMap);
	}
	
	/**
	 * 查询编码是否重复
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findDeliverByCode")
	@ResponseBody
	public Object findDeliverByCode(Deliver deliver)throws Exception{
//		Deliver deliver = new Deliver();
//		deliver.setCode(code);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.findDeliverByCode(deliver);
	}
	
	/********************************************供应商配送分拨start******************************************/	
	/**
	 * 供应商配送分拨主界面
	 * @throws Exception
	 */
	@RequestMapping(value = "/deliverdis")
	public ModelAndView list(ModelMap modelMap,Page page,HttpSession session,Dis dis,String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		Account account = new Account();
		account.setName(accountName);
		String roleName = deliverService.getRoleNm(account).getName();
		if ("供应商角色".equals(roleName)) {
			Deliver deliver = new Deliver();
			deliver.setCode(accountName);
			dis.setDeliverDes(deliverService.findDeliverByCode(deliver).getDes());
			dis.setDeliverCode(accountName);
		}
		if(null!=action && "init".equals(action)){//页面初次加载时
//			dis.setBdat(new Date());
//			dis.setEdat(new Date());
			dis.setMaded(new Date());
		}else {
			String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
			dis.setAcct(acct);
//			modelMap.put("disesList1", disFenBoService.findAllDis(dis,page));
			modelMap.put("deliverMessage1", deliverService.findDeliverMessage1(dis));//几家单位未送货
			modelMap.put("deliverMessage2", deliverService.findDeliverMessage2(dis));//共有多少物资未送货
			modelMap.put("disesList1", deliverService.findAllDis(dis,page));
		}
//		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("pageobj", page);
		modelMap.put("accountName", accountName);
		modelMap.put("dis", dis);
		return new ModelAndView(DeliverConstants.LIST_DELIVERDIS, modelMap);
	}
	
	/**
	 * 打印分拨单
	 */
	@RequestMapping(value = "/printIndent")
	public ModelAndView printIndent(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询条件
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("indent");
		param.put("danjuTyp", "indent");
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliver/printIndent.do");//传入回调路径
		//根据关键字查询
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
//		dis.setStartNum(0);
//		dis.setEndNum(Integer.MAX_VALUE);
//		List<Dis> disList=deliverService.findAllDisPage(dis);	
//		List<Dis> disList=deliverService.findAllDisPage(dis);	
		//记录合计数
		Map<String,Object> countMap=new HashMap<String,Object>();
		//所有的数据
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		String deliverDes2="";
		String sp_name2="";
		//计算合计
		double count=0;
		//------------------------------------------------ 
	    int tdnum1=1;//标记列1
	    int tdnum2=1;//标记列2
	    int tdnum3=1;//标记列3
	    //存放数据，往报表页面传
	    Map<String,Object> resultMap=new HashMap<String,Object>();
	    //报表文件同种物资类似行列转换用到的方法
	    for(int i=0;i<disList.size();i++){
	    	if(deliverDes2.equals(disList.get(i).getDeliverDes())){
			     if(sp_name2.equals(disList.get(i).getSp_name())){
					  if(tdnum1==4){
						   tdnum1=1;
						   tdnum2=1;
						   tdnum3=1;
						   list.add(resultMap);
						   resultMap=new HashMap<String,Object>();
						   resultMap.put("STR", "");
						   resultMap.put("SP_NAME",disList.get(i).getSp_name());
						   resultMap.put("DELIVER", disList.get(i).getDeliverDes());
						   resultMap.put("UNIT",disList.get(i).getUnit());
						   resultMap.put("CNT",disList.get(i).getCnt());
					  }
				      count+=disList.get(i).getAmountin();
//				      count+=disList.get(i).getAmount();
				      countMap.put(disList.get(i).getSp_name(),count);
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmountin());
//				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				      resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
			     }else{
				      if(resultMap.size()!=0){
				    	  list.add(resultMap);
				      }
				      tdnum1 = 1;
				      tdnum2 = 1;
				      tdnum3 = 1;
				      count = 0;
				      resultMap=new HashMap<String,Object>();
				      resultMap.put("STR", "STR");
				      resultMap.put("SP_NAME",disList.get(i).getSp_name());
				      resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				      resultMap.put("UNIT",disList.get(i).getUnit());
				      resultMap.put("CNT",disList.get(i).getCnt());
				      resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
//				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				      resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmountin());
				      resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
				      count=disList.get(i).getAmountin();
//				      count=disList.get(i).getAmount();
				      countMap.put(disList.get(i).getSp_name(),count);
				      sp_name2=disList.get(i).getSp_name();
			     }
	    	}else{
	    		if(resultMap.size()!=0){
	    			list.add(resultMap);
			    }
				tdnum1 = 1;
				tdnum2 = 1;
				tdnum3 = 1;
				count = 0;
				resultMap=new HashMap<String,Object>();
				resultMap.put("STR", "STR");
				resultMap.put("SP_NAME",disList.get(i).getSp_name());
				resultMap.put("DELIVER", disList.get(i).getDeliverDes());
				resultMap.put("UNIT",disList.get(i).getUnit());
				resultMap.put("CNT",disList.get(i).getCnt());
				resultMap.put("POSITN"+tdnum1++, disList.get(i).getFirmDes());
				resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmountin());
//				resultMap.put("AMOUNT"+tdnum2++, disList.get(i).getAmount());
				resultMap.put("MEMO"+tdnum3++, disList.get(i).getMemo()); 
				count=disList.get(i).getAmountin();
//				count=disList.get(i).getAmount();
				countMap.put(disList.get(i).getSp_name(),count);
				sp_name2=disList.get(i).getSp_name();
				deliverDes2=disList.get(i).getDeliverDes();
		     }
	    }
	    list.add(resultMap);//将最后一行加上
		//计算完合计，存放进map
		for (int i = 0; i < list.size(); i++) {
			Map<String,Object> map=list.get(i);
			map.put("TOTALAMT", countMap.get(map.get("SP_NAME")));
		}
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("分拨单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd")); 
        parameters.put("print_time", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss")); 
        modelMap.put("List", list);
	    modelMap.put("parameters", parameters);
	    Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INDENT_URL,DisConstants.REPORT_INDENT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
        //return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印验货单
	 */
	@RequestMapping(value = "/printInspection")
	public ModelAndView printInspection(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户前台输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("inspection");
		param.put("danjuTyp", "inspection");
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliver/printInspection.do");//传入回调路径
		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		//根据关键字进行查询
//		dis.setStartNum(0);
//		dis.setEndNum(Integer.MAX_VALUE);
//		List<Dis> disList=deliverService.findAllDisPage(dis);	
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("验货单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表用输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_INSPECTION_URL,DisConstants.REPORT_INSPECTION_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印配送单
	 */
	@RequestMapping(value = "/printDelivery")
	public ModelAndView printDelivery(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("delivery");
		param.put("danjuTyp", "delivery");
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("maded", DateFormat.getStringByDate(dis.getMaded(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliver/printDelivery.do");//传入回调路径
//		List<Dis> disList=disFenBoService.findAllDisForReport(dis);
		//根据关键字查询
//		dis.setStartNum(0);
//		dis.setEndNum(Integer.MAX_VALUE);
		List<Dis> disList=deliverService.findAllDisPage(dis);	
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("配送清单");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用到的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_DELIVERY_URL,DisConstants.REPORT_DELIVERY_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 报货分拨，采购汇总
	 */
	@RequestMapping(value = "/findCaiGouTotal")
	public ModelAndView findCaiGouTotal(ModelMap modelMap,Page page,HttpSession session,Dis dis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("disList", deliverService.findCaiGouTotal(dis,page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.CAIGOU_TOTAL_DELIVERDISTRIBUTION, modelMap);
	}
	/**
	 * 汇总打印
	 */
	@RequestMapping(value = "/printDisTotal")
	public ModelAndView printDisTotal(ModelMap modelMap,HttpSession session,Page page,Dis dis,Date maded,String type,String ind1)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("chectim", dis.getChectim());
		param.put("ynArrival", dis.getYnArrival());
		param.put("ex1", dis.getEx1());
		param.put("yndo", dis.getYndo());
		param.put("inout", dis.getInout());
		param.put("psarea", dis.getPsarea());
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/deliver/printDisTotal.do");//传入回调路径		
		List<Dis> disList=deliverService.findCaiGouTotal(dis,null);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
        String report_name=new String("申购汇总");
        parameters.put("report_name", report_name); 
        parameters.put("report_date", DateFormat.getStringByDate(maded,"yyyy-MM-dd")); 
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
	    modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	modelMap.put("action", "/deliver/printDisTotal.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_TOTAL_DELIVER_URL,DisConstants.REPORT_TOTAL_DELIVER_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 新增物资        
	 */
	@RequestMapping(value = "/addSupplyBatch")
	public ModelAndView addPositnBatch(ModelMap modelMap, String defaultName, String defaultCode, String type,String chkValue) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("type", type);
		
		modelMap.put("chkValue", chkValue);
		return new ModelAndView(DeliverConstants.ADD_SUPPLY_BATCH_FIRM_ADD, modelMap);
	}
	/**
	 * 查询多条物资   左侧
	 */
	@RequestMapping(value = "/selectNSupply")
	public ModelAndView selectNSupply(ModelMap modelMap, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(DeliverConstants.SELECT_NSUPPLY, modelMap);
	}
	/**
	 * 查询多条物资    弹出框 下右
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectTableNSupply")
	public ModelAndView selectTableNSupply(ModelMap modelMap, Supply supply, String level, String code, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("supply", supply);//查询记忆
		return new ModelAndView(DeliverConstants.SELECT_TABLENSUPPLY, modelMap);
	}
	
	/**
	*获取物资大类 
	*
	*/
	@RequestMapping(value = "/findDivisionSupply")
	public ModelAndView chooseSupply(ModelMap modelMap,HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		return new ModelAndView(DeliverConstants.DIVISIONSUPPLY, modelMap);
	}
	/**
	 * 获取物资中类
	 */
	@RequestMapping(value="/findGroupTyp")
	public ModelAndView findGroupTyp(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		return new ModelAndView(DeliverConstants.GROUPSUPPLY, modelMap);
	}
	
	/**
	 * 获取物资小
	 */
	@RequestMapping(value="/findSmallClass")
	public ModelAndView findSmallClass(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//左侧导航树
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(DeliverConstants.CLASSSUPPLY, modelMap);
	}
	
	//跳转到添加供应物资范围的页面
	@RequestMapping(value="/tosaveDeliverSupply")
	public ModelAndView tosaveDeliverSupply(ModelMap modelMap,HttpSession session,String delivercode)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		MaterialScope materialScope = new MaterialScope();
		materialScope.setAcct(session.getAttribute("ChoiceAcct").toString());
		materialScope.setDelivercode(delivercode);
		List<MaterialScope> materialScopeList = deliverService.tosaveDeliverSupply(materialScope);
		modelMap.put("materialScopeList", materialScopeList);
		modelMap.put("delivercode", delivercode);
		
		//税率列表 jinshuai 20160422
		List<Tax> listTax = supplyService.findTax(null);
		modelMap.put("listTax", listTax);
		return new ModelAndView(DeliverConstants.SAVEDELIVERSUPPLY, modelMap);
	}
	
	/**
	 * 根据类别添加物资
	 * @param session
	 * @param classType 大0中1小2类
	 * @param classCode 类别编码
	 * @param positnCode 分店编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveSupplyByClass")
	@ResponseBody
	public Object saveSupplyByClass( HttpSession session,String classType,String classCode,String delivercode) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Date beg = new Date();
		try {
			List<Supply> listSupply = new ArrayList<Supply>();
			if("0".equals(classType)){
				listSupply = grpTypService.findGrp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else if("1".equals(classType)){//中类
				listSupply = grpTypService.findTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}else{//小类
				listSupply = grpTypService.findSupplyByTyp(classCode,session.getAttribute("ChoiceAcct").toString());
			}
			deliverService.saveSupplyByClass(listSupply,delivercode);
			Date end = new Date();
			System.out.println("*******************************"+(end.getTime() -beg.getTime())/1000);
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
	}
	
	/**
	 * 添加供应物资范围
	 * @param delivercode
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/insertDeliverSupply")
	@ResponseBody
	public String insertDeliverSupply(String delivercode,String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> spcodeList=Arrays.asList(code.split(","));
		return deliverService.insertDeliverSupply(delivercode,spcodeList);
	}
	
	/**
	 * 删除物资
	 * @param modelMap
	 * @param delivercode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteDeliverSupply")
	public ModelAndView deleteDeliverSupply(ModelMap modelMap,String code,MaterialScope materialScope)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listStr = Arrays.asList(code.split(","));
		materialScope.setListpk_materialscope(listStr);
		deliverService.deleteDeliverSupply(materialScope);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 保存的时候更新价格
	 * @param pk_materialscope
	 * @param sp_price
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateDeliverSupply")
	@ResponseBody
	public boolean updateDeliverSupply(MaterialScopeList materialScopeList)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.updateDeliverSupply(materialScopeList);
	}
	
	/**
	 * 保存的时候更新税率 jinshuai 20160425
	 * @param pk_materialscope
	 * @param sp_price
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateTaxRate")
	@ResponseBody
	public boolean updateTaxRate(MaterialScopeList materialScopeList)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.updateTaxRate(materialScopeList);
	}
	
	/********************************************供应商配送分拨end******************************************/
	
	/***
	 * 确认送货
	 * @param ids
	 * @return
	 */
	@RequestMapping("/confirmSh")
	@ResponseBody
	public String confirmSh(String ids){
		deliverService.confirmSh(ids);
		return "OK";
	}
	/***
	 * 确认所有送货
	 * @param ids
	 * @return
	 */
	@RequestMapping("/confirmShAll")
	@ResponseBody
	public String confirmShAll(Dis dis){
		deliverService.confirmShAll(dis);
		return "OK";
	}
	
	/****************************************************关联商城供应商*******************************************/
	/**
	 * 共用查询供应商列表====来自商城的供应商
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/supplierJmuList")
	public ModelAndView supplierJmuList(ModelMap modelMap,Deliver deliver,Page page,HttpSession session,String code,String des) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Condition condition = new Condition();
		if(ValueCheck.IsNotEmpty(deliver.getDes())){
			des = URLDecoder.decode(des,"UTF-8");
		}
		condition.setVname(des);
		condition.setVinit(code);
		condition.setPager(page);
		deliver.setCode(code);
		deliver.setDes(des);
		//名称不为空才能查询
		if(ValueCheck.IsNotEmpty(condition.getVname())){
			AssitSupplierResult assitSupplierResult = searchingService.findAsstSupplierList(condition, session);
			if (assitSupplierResult == null || "".equals(assitSupplierResult) || assitSupplierResult.getData() == null || "".equals(assitSupplierResult.getData())) {
				modelMap.put("sellerList", null);
			}else {
				modelMap.put("sellerList", assitSupplierResult.getData().getResult());
				PageCondition pagerc = assitSupplierResult.getData().getPageCondition();
				if (pagerc != null) {
					page.setCount(Integer.parseInt(pagerc.getAllCount()));
					page.setNowPage(Integer.parseInt(pagerc.getPageNo()));
					page.setPageSize(Integer.parseInt(pagerc.getPageSize()));
				}
			}
		}
		modelMap.put("supplier", deliver);
		modelMap.put("code", code);
		modelMap.put("des", des);
		modelMap.put("single", true);
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverConstants.SELECTJMUDELIVER,modelMap);
	}
	
	/**
	 * 添加关联商城供应商
	 * @param deliver
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value="/updateJmuDeliver")
	public int updateJmuDeliver(Deliver deliver,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.updateJmuDeliver(deliver,session);
	}
	
	/**
	 * 删除供应商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteJumDeliver")
	public int deleteJumDeliver(String code, Deliver deliver) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收供应商编码，进行删除
		List<String> deliverList=Arrays.asList(code.split(","));
		return deliverService.deleteJumDeliver(deliverList);
	}
	
	/****************************************************关联商城供应商*******************************************/
	/****************************************************关联商城物资********************************************/
	
	/**
	 * 查询物资列表====来自商城的物资
	 * @param modelMap
	 * @param Materialtype
	 * @param Material
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/supplyJmuList")
	public ModelAndView supplyJmuList(ModelMap modelMap,Supply supply,Page page,HttpSession session,String sp_code,String sp_name,String delivercode,String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Deliver del = new Deliver();
		del.setCode(delivercode);
		Deliver deliver = deliverService.findDeliverByCode(del);
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(ValueCheck.IsNotEmpty(supply.getSp_name())){
			sp_name = URLDecoder.decode(sp_name,"UTF-8");
		}
		Condition condition = new Condition();
		condition.setVname(sp_name);
		condition.setVcode(sp_code);
		condition.setVdelivername(deliver.getJmudelivername());
		supply.setSp_code(sp_code);
		supply.setSp_name(sp_name);
        if(ValueCheck.IsNotEmpty(condition.getVname()) && "1".equals(type)){
        	condition.setPager(pager);
			AssitResultGoods assitResult = searchingService.findAsstMaterialList(condition, session);
			if(assitResult != null && !"".equals(assitResult)){
				if(assitResult.getData()!=null && ValueCheck.IsNotEmpty(assitResult.getData().getResult())){
					for(AsstGoods asstGoods : assitResult.getData().getResult()){
						if(ValueCheck.IsEmpty(asstGoods.getPrice())){
							asstGoods.setPrice("");
						}
					}
					modelMap.put("commodityList", assitResult.getData().getResult());
				}
				PageCondition pagerc = assitResult.getData().getPageCondition();
				if(ValueCheck.IsNotEmpty(pagerc)){
					pager.setCount(Integer.parseInt(pagerc.getAllCount()));
					pager.setNowPage(Integer.parseInt(pagerc.getPageNo()));
					pager.setPageSize(Integer.parseInt(pagerc.getPageSize()));
				}
			}
        }
		modelMap.put("condition", condition);
		modelMap.put("pageobj", pager);
		modelMap.put("single", true);
		modelMap.put("supply", supply);
		modelMap.put("type", type);
		modelMap.put("delivercode", delivercode);
		modelMap.put("condition", condition);
		return new ModelAndView(DeliverConstants.SELECTJMUSUPPLY,modelMap);
	}
    /**
     * 跳转到商城物资选择页面
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     * @throws UnsupportedEncodingException 
     */
    @RequestMapping(value = "/toMallMaterialDetial")
    @ResponseBody
    public Object toMallMaterialDetial(ModelMap modelMap,Condition condition,Page page,HttpSession session) throws CRUDException, UnsupportedEncodingException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	if(ValueCheck.IsNotEmpty(condition.getVcode())){//如果商品主键不为空调用查询商城商品接口
	    	AssitResultForSpInfo assitResult = searchingService.findAsstMaterialByPk(condition, session);
	    	if(ValueCheck.IsNotEmpty(assitResult.getData())){
	    		return assitResult.getData().getGoods();
	    	}
    	}
  		return null;
    }
    
    /**
     * 添加关联商城物资
     * @param materialScope
     * @param session
     * @return
     * @throws Exception
     */
    @ResponseBody
	@RequestMapping(value = "/updateJmuSupply")
    public int updateJmuSupply(MaterialScope materialScope,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.updateJmuSupply(materialScope,session);
	}
	
	/**
	 * 删除供应商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteJumSupply")
	public int deleteJumSupply(String code, MaterialScope materialScope) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收供应商编码，进行删除
		List<String> deliverList=Arrays.asList(code.split(","));
		materialScope.setListpk_materialscope(deliverList);
		return deliverService.deleteJumSupply(materialScope);
	}
	
	/**
	 * 查询供应物资范围的前十条供应商，包括税率，报价
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findGySpGysTop10")
	@ResponseBody
	public Object findGySpGysTop10(MaterialScope materialScope) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverService.findGySpGysTop10(materialScope);
	}
	
	/****************************************************关联商城物资********************************************/
	
}
