package com.choice.scm.web.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.Chkstom_xConstants;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.service.Chkstod_xService;
import com.choice.scm.service.ChkstodemoService;
import com.choice.scm.service.Chkstom_xService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ReadReportUrl;
/**
 * 报货单新增、修改、删除、查询、打印、审核
 * @author csb
 *
 */
@Controller
@RequestMapping(value = "chkstom_x")

public class Chkstom_xController {

	@Autowired
	private Chkstom_xService chkstomService;
	@Autowired
	private Chkstod_xService chkstodService;
	@Autowired
	private ChkstodemoService chkstodemoService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private PositnRoleService positnRoleService;
	/**
	 * 查询所有未审核报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkstom(ModelMap modelMap,Page page,HttpSession session,String tableFrom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("tableFrom", tableFrom);
		//报货单主页面
		return new ModelAndView(Chkstom_xConstants.TABLE_CHKSTOM);
	}
	/**
	 * 添加操作时候，刷新页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountId=session.getAttribute("accountId").toString();
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		return new ModelAndView(Chkstom_xConstants.TABLE_CHKSTOM,modelMap);
	}	
	
	/**
	 * 检查是否设置供应商
	 */
	@RequestMapping(value = "/checkSaveNewChk")
	@ResponseBody
	public Object checkSaveNewChk(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.checkSaveNewChk(chkstom);
	}	
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap,Page page,HttpSession session,String sta,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.saveOrUpdateChk(chkstom, sta);
	}
	
	/**
	 * 查找已审核报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap,HttpSession session,Page page,String init,String sp_code,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数，已审核报货单列表页面
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=init && ""!=init){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		//把参数放到MAP
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		chkstomMap.put("checked", "checked");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		//把查询的结果集返回到页面
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(Chkstom_xConstants.TABLE_CHECKED_CHKSTOM,modelMap);
	}
	/**
	 * 查找报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String startDate,String sp_code,String init,Chkstom chkstom, String firmDes) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		Date bdate=null;
		Date edate=null;
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null!=init && !"".equals(init)){
			if(null!=startDate && !"".equals(startDate)){//不为空的话，赋值用户输入的日期
				bdate=DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			}else{//为空的话，赋值系统当前日期
				bdate=new Date();
			}
			edate=new Date();
			chkstom.setbMaded(bdate);
			chkstom.seteMaded(edate);
		}
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		//关键字查询
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("firmDes", firmDes);
		modelMap.put("pageobj", page);
		return new ModelAndView(Chkstom_xConstants.SEARCH_CHKSTOM,modelMap);
	}
	
	/**
	 * 双击查找
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		return new ModelAndView(Chkstom_xConstants.TABLE_CHKSTOM,modelMap);
	}

	/**
	 * 查看已审核报货单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedChkstom")
	public ModelAndView searchCheckedChkstom(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//关键字查询
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		return new ModelAndView(Chkstom_xConstants.SEARCH_CHECKED_CHKSTOM,modelMap);
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(ModelMap modelMap,HttpSession session,Page page,String chkstoNoIds,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		return chkstomService.deleteChkstom(chkstom,chkstoNoIds);
	}
	/**
	 * 虚转实
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkstom_x")
	@ResponseBody
	public Object checkOneChkstom(ModelMap modelMap, Page page, HttpSession session, Chkstom chkstom, String chkstoNoIds) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//报货单填制上的整条删除
		return chkstomService.chkstoxs(chkstom, chkstoNoIds);
	}
	
	/**
	 * 确认修改，添加模板数据到报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enterUpdate")
	@ResponseBody
	public Object enterUpdate(ModelMap modelMap, HttpSession session, String rec2, String cnt, String cnt1, String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//申购物资批量添加时候的数量假修改
		String acct=session.getAttribute("ChoiceAcct").toString();
		List<String> recList=Arrays.asList(rec2.split(","));
		List<String> cntList=Arrays.asList(cnt.split(","));
		List<String> cnt1List=Arrays.asList(cnt1.split(","));
		return chkstodemoService.findChkstodemoByRec(recList, cntList, cnt1List, acct, title);
	}
	
	/**
	 * 打开查看分店上传数据页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUpload")
	public ModelAndView searchUpload(ModelMap modelMap) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//初始化查看上传页面
		modelMap.put("curDate", new Date());
		return new ModelAndView(Chkstom_xConstants.SEARCH_UPLOAD,modelMap);
	}

	/**
	 * 查看分店上传数据
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUploadByDate")
	public ModelAndView searchUploadByDate(ModelMap modelMap,HttpSession session,Chkstom chkstom,String startDate) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收日期，然后查询有哪些店上传与未上传
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=startDate && !"".equals(startDate)){//日期不为空
			chkstom.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			//查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		}else{
			//查看上传数据
			modelMap.put("chkstomList",chkstomService.findUpload(chkstom));
			//查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		}
		
		//系统日期
		modelMap.put("curDate", chkstom.getMaded());
		//仓位列表
		modelMap.put("positnList",positnService.findAllPositn(null));
		return new ModelAndView(Chkstom_xConstants.SEARCH_UPLOAD,modelMap);
	}
	
	/**
	 * 报货单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(ModelMap modelMap,HttpSession session,Page page,String type,Chkstod chkstod)throws CRUDException
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		List<Chkstod> list=chkstodService.findAllChkstod(chkstod);
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("申购数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date); 
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/chkstom/printChkstom.do?chkstoNo="+chkstod.getChkstoNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,Chkstom_xConstants.REPORT_PRINT_URL,Chkstom_xConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        //return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
