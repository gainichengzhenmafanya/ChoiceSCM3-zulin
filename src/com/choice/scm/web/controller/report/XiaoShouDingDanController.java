package com.choice.scm.web.controller.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.XiaoShouDingDanService;
import com.choice.scm.util.PublicExportExcel;
/**
 * 销售订单报表
 * @author Administrator
 *
 */
@Controller
@RequestMapping("XiaoShouDingDan")
public class XiaoShouDingDanController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private XiaoShouDingDanService xiaoShouDingDanService;
	@Autowired
	private DictColumnsService dictColumnsService;
	
	/********************************************销售订单报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChoose")
	public ModelAndView toColChoose(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName("XiaoShouDingDan");
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName","XiaoShouDingDan");
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findHeaders")
	@ResponseBody
	public Object findHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_XIAOSHOUDINGDAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_XIAOSHOUDINGDAN));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_XIAOSHOUDINGDAN_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到报表页面
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/toXiaoShouDingDanQuery")
	public ModelAndView toXiaoShouDingDanQuery(ModelMap modelMap, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName","XiaoShouDingDan");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_XIAOSHOUDINGDAN,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/queryXiaoShouDingDan")
	@ResponseBody
	public Object queryXiaoShouDingDan(ModelMap modelMap,String chktyp, String page, String rows,HttpSession session,Chkoutm chkoutm,Date bdat, Date edat) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//条件集合
		Map<String,Object> condition = new HashMap<String,Object>();
		//设置条件
		condition.put("checby", "c");
		condition.put("firm", chkoutm.getFirm().getCode());
		condition.put("positn", chkoutm.getPositn().getCode());
		condition.put("ACCT", session.getAttribute("ChoiceAcct").toString());
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		if(chktyp!=null&&!chktyp.startsWith("'")){
			condition.put("chktyp", CodeHelper.replaceCode(chktyp));
		}else{
			condition.put("chktyp", chktyp);
		}
		//分页
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return xiaoShouDingDanService.findDatas(condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportQuery")
	@ResponseBody
	public void exportQuery(HttpServletRequest request,HttpServletResponse response,String chktyp, ModelMap modelMap,HttpSession session,Chkoutm chkoutm,Date bdat, Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "销售订单报表";
		//条件集合
		Map<String,Object> condition = new HashMap<String,Object>();
		//设置条件
		condition.put("checby", "c");
		condition.put("firm", chkoutm.getFirm().getCode());
		condition.put("positn", chkoutm.getPositn().getCode());
		condition.put("ACCT", session.getAttribute("ChoiceAcct").toString());
		condition.put("bdat", bdat);
		condition.put("edat", edat);
		if(chktyp!=null&&!chktyp.startsWith("'")){
			condition.put("chktyp", CodeHelper.replaceCode(chktyp));
		}else{
			condition.put("chktyp", chktyp);
		}
		//导出全部
		pager.setNowPage(1);
		pager.setPageSize(Integer.MAX_VALUE);
		//表头
		List<Map<String,String>> headers = new ArrayList<Map<String,String>>();
		
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("日期" + PublicExportExcel.SEPARATOR_COLONS + "DAT", "");
		headers.add(map1);
		
		Map<String, String> map111 = new HashMap<String, String>();
		map111.put("销售方式" + PublicExportExcel.SEPARATOR_COLONS + "XSFS", "");
		headers.add(map111);
		
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put("编号" + PublicExportExcel.SEPARATOR_COLONS + "VNO", "");
		headers.add(map2);
		
		Map<String, String> map3 = new HashMap<String, String>();
		map3.put("币别" + PublicExportExcel.SEPARATOR_COLONS + "BIBIE", "");
		headers.add(map3);
		
		Map<String, String> map4 = new HashMap<String, String>();
		map4.put("购货单位_代码" + PublicExportExcel.SEPARATOR_COLONS + "GHDWDM", "");
		headers.add(map4);
		
		Map<String, String> map5 = new HashMap<String, String>();
		map5.put("购货单位" + PublicExportExcel.SEPARATOR_COLONS + "GHDW", "");
		headers.add(map5);
		
		Map<String, String> map6 = new HashMap<String, String>();
		map6.put("部门_代码" + PublicExportExcel.SEPARATOR_COLONS + "BMDM", "");
		headers.add(map6);
		
		Map<String, String> map7 = new HashMap<String, String>();
		map7.put("部门" + PublicExportExcel.SEPARATOR_COLONS + "BM", "");
		headers.add(map7);
		
		Map<String, String> map8 = new HashMap<String, String>();
		map8.put("业务员_代码" + PublicExportExcel.SEPARATOR_COLONS + "YWYDM", "");
		headers.add(map8);
		
		Map<String, String> map9 = new HashMap<String, String>();
		map9.put("业务员" + PublicExportExcel.SEPARATOR_COLONS + "YWY", "");
		headers.add(map9);
		
		Map<String, String> map99 = new HashMap<String, String>();
		map99.put("制单" + PublicExportExcel.SEPARATOR_COLONS + "ZD", "");
		headers.add(map99);
		
		Map<String, String> map10 = new HashMap<String, String>();
		map10.put("汇率类型" + PublicExportExcel.SEPARATOR_COLONS + "HLLX", "");
		headers.add(map10);
		
		Map<String, String> map11 = new HashMap<String, String>();
		map11.put("汇率" + PublicExportExcel.SEPARATOR_COLONS + "HL", "");
		headers.add(map11);
		
		Map<String, String> map1111 = new HashMap<String, String>();
		map1111.put("摘要" + PublicExportExcel.SEPARATOR_COLONS + "ZY", "");
		headers.add(map1111);
		
		Map<String, String> map12 = new HashMap<String, String>();
		map12.put("销售范围" + PublicExportExcel.SEPARATOR_COLONS + "XSFW", "");
		headers.add(map12);
		
		Map<String, String> map13 = new HashMap<String, String>();
		map13.put("计划类别" + PublicExportExcel.SEPARATOR_COLONS + "JHLB", "");
		headers.add(map13);
		
		Map<String, String> map14 = new HashMap<String, String>();
		map14.put("产品代码" + PublicExportExcel.SEPARATOR_COLONS + "CPDM", "");
		headers.add(map14);
		
		Map<String, String> map15 = new HashMap<String, String>();
		map15.put("单位" + PublicExportExcel.SEPARATOR_COLONS + "DW", "");
		headers.add(map15);
		
		Map<String, String> map16 = new HashMap<String, String>();
		map16.put("数量" + PublicExportExcel.SEPARATOR_COLONS + "SL", "");
		headers.add(map16);
		
		Map<String, String> map1616 = new HashMap<String, String>();
		map1616.put("单价" + PublicExportExcel.SEPARATOR_COLONS + "DJ", "");
		headers.add(map1616);
		
		Map<String, String> map17 = new HashMap<String, String>();
		map17.put("建议交货日期" + PublicExportExcel.SEPARATOR_COLONS + "JYJHRQ", "");
		headers.add(map17);
		
		Map<String, String> map18 = new HashMap<String, String>();
		map18.put("计划模式" + PublicExportExcel.SEPARATOR_COLONS + "JHMS", "");
		headers.add(map18);
		
		Map<String, String> map19 = new HashMap<String, String>();
		map19.put("计划跟踪号" + PublicExportExcel.SEPARATOR_COLONS + "JHGZH", "");
		headers.add(map19);
		
		Map<String, String> map1919 = new HashMap<String, String>();
		map1919.put("BOM编号" + PublicExportExcel.SEPARATOR_COLONS + "BOMBH", "");
		headers.add(map1919);
		
		Map<String, String> map20 = new HashMap<String, String>();
		map20.put("交货日期" + PublicExportExcel.SEPARATOR_COLONS + "JHRQ", "");
		headers.add(map20);
		
		//数据
		List<Map<String,Object>> datas = xiaoShouDingDanService.findDatas(condition,pager).getRows();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Map<String,Object> map = null;
		String d1 = null;
		String d2 = null;
		for(int i=0,len=datas.size();i<len;i++){
			map = datas.get(i);
			//把日期，建议交货日期，交货日期	精确到天
			d1 = format.format(map.get("DAT"));
			d2 = format.format(map.get("JYJHRQ"));
			d1 = (d1==null||d1.trim().equals(""))?"":d1.substring(0,10);
			d2 = (d2==null||d2.trim().equals(""))?"":d2.substring(0,10);
			map.put("DAT", d1);
			map.put("JYJHRQ", d2);
			map.put("JHRQ", d2);
		}
		
		// 导出
		PublicExportExcel pee = new PublicExportExcel();
		// 导出无表头，无空列，无空行
		pee.creatWorkBookNoTitle(request, response, fileName, fileName, headers, datas, null);
	}
}