package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.RkLeibieHuizongService;
@Controller
@RequestMapping("RkLeibieHuizong")
public class RkLeibieHuizongController {

	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private RkLeibieHuizongService rkLeibieHuizongService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************入库类别汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinCategorySumHeaders")
	@ResponseBody
	public Object getChkinCategorySum(HttpSession session,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Grp grp = new Grp();
		grp.setGrptyp(supplyAcct.getGrptyp());
		grp.setCode(supplyAcct.getGrp());
		grp.setAcct(session.getAttribute("ChoiceAcct").toString());
		Map<String,Object> columns = new HashMap<String,Object>();
		columns.put("columns", grpTypService.selectAllGrptypByCode(grp));
		return columns;
	}
	/**
	 * 跳转到入库类别汇总报表页面
	 * @return
	 */
	@RequestMapping("/toChkinCategorySum")
	public ModelAndView toChkinCategorySum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "入库类别汇总");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKINMCATEGORYSUMMARY,modelMap);
	}
	/**
	 * 查询入库类别汇总内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinCategorySum")
	@ResponseBody
	public Object findChkinCategorySum(ModelMap modelMap, HttpSession session, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		return rkLeibieHuizongService.findAllChkinmGrp(supplyAcct);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
//	@RequestMapping(value = "/printChkinCategorySum")
//	public ModelAndView printChkinCategorySum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
//		pager.setPageSize(Integer.MAX_VALUE);
//		Map<String,String> params = new HashMap<String,String>();
//		if(supplyAcct.getBdat() != null)
//			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
//		params.put("positn",supplyAcct.getPositn());
//		params.put("grptyp",supplyAcct.getGrptyp());
//		if(supplyAcct.getBdat() != null)
//			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
//		params.put("grp",supplyAcct.getGrp());
//		params.put("chktyp",supplyAcct.getChktyp());
//		params.put("delivercode",supplyAcct.getDelivercode());
//		
//		modelMap.put("List",supplyAcctService.findAllChkinmGrp(supplyAcct).getRows());
//	 	HashMap  parameters = new HashMap();
//	    parameters.put("report_name", "入库类别汇总");
//	    modelMap.put("actionMap", params);
//	    parameters.put("maded",new Date());
//	    parameters.put("madeby", session.getAttribute("accountName").toString());
//	        
//        modelMap.put("parameters", parameters);
//	 	modelMap.put("action", "/RkLeibieHuizong/printChkinCategorySum.do");//传入回调路径
//	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINMCATEGORYSUMMARY,SupplyAcctConstants.REPORT_EXP_URL_CHKINMCATEGORYSUMMARY);//判断跳转路径
//        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
//		return new ModelAndView(rs.get("url"),modelMap);
//	}
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinCategorySum")
	@ResponseBody
	public void exportChkinCategorySum(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库类别汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		dictColumns.setTableName("入库类别汇总");
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		rkLeibieHuizongService.exportChkinCategorySum(response.getOutputStream(), supplyAcct);
		
	}
	
}
