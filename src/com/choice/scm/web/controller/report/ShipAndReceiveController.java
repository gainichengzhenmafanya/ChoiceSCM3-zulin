package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.report.ReportConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.ShipAndReceivedService;

/**
 * 收发货报表
 * 
 * @author ygb
 */

@Controller
@RequestMapping("/shipAndReceive")
public class ShipAndReceiveController {

	@Autowired
	private ShipAndReceivedService shipAndReceivedService;

	/**
	 * 收发货报表
	 * @return
	 * @throws Exception
	 * @author ygb
	 */
	@RequestMapping("list")
	public ModelAndView searchFirmIntransitd(ModelMap modelMap, HttpSession session, String mold, SupplyAcct sa, Page page, String check, Date bdate, Date edate) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		// 点击查询的时候符合条件的配送中心到货信息。
		if ("select".equals(mold)) {
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("bdate", bdate);
			disMap.put("edate", edate);
			disMap.put("check", check);
			disMap.put("iszs", sa.getIszs());
			disMap.put("iscy", sa.getIscy());
			disMap.put("firm",CodeHelper.replaceCode(sa.getPositn()));
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			//wangjie 2014年11月11日 14:01:37 增加出库仓位
			disMap.put("positn", CodeHelper.replaceCode(sa.getFirm()));
			
			List<SupplyAcct> disList=shipAndReceivedService.findShipAndReceiveList(disMap, page);
			
			modelMap.put("dis", sa);
			modelMap.put("disList", disList);
		}
		
		//初次加载页面传值
		if (bdate==null) {
			bdate=new Date();
		}
		if (edate==null) {
			edate=new Date();
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("check", check);
		modelMap.put("pageobj", page);
		return new ModelAndView(ReportConstants.SCM_SHIPANDRECEIVE_LIST, modelMap);
	}

	/**
	 * 导出收发货excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportDispatch")
	@ResponseBody
	public void exportDispatch(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session,
			String mold, SupplyAcct sa, Page page, String check, Date bdate, Date edate) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("iszs", sa.getIszs());
		disMap.put("iscy", sa.getIscy());
		disMap.put("firm",CodeHelper.replaceCode(sa.getPositn()));
		disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		
		List<SupplyAcct> disList = shipAndReceivedService.findShipAndReceiveList(disMap);
		String fileName = "收发货报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		sa.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", sa);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		shipAndReceivedService.exportExcel(response.getOutputStream(), disList);
		
	}
}
