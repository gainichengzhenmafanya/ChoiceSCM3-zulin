package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.GysFendianHuizongService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("GysFendianHuizong")
public class GysFendianHuizongController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private GysFendianHuizongService gysFendianHuizongService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************供应商分店汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeliverPositnSumHeaders")
	@ResponseBody
	public Object getDeliverPositnSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERPOSITNSUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_DELIVERPOSITNSUM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_DELIVERPOSITNSUM_FROZEN);
		return columns;
	}
	/**
	 * 跳转到供应商分店汇总页面
	 * @return
	 */
	@RequestMapping("/toDeliverPositnSum")
	public ModelAndView toDeliverPositnSum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_DELIVERPOSITNSUM);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DELIVERPOSITNSUM,modelMap);
	}
	/**
	 * 查询供应商进货汇总报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverPositnSum")
	@ResponseBody
	public Object ChkinDeliverPositnSum(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gysFendianHuizongService.findDeliverPositnSum(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeliverPositnSum")
	public ModelAndView printDeliverPositnSum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("delivercode",supplyAcct.getDelivercode());
		condition.put("supplyAcct", supplyAcct);
		//改一致的合计
		ReportObject<Map<String,Object>> allData = gysFendianHuizongService.findDeliverPositnSum(condition,pager);
		modelMap.put("List",allData.getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "供应商分店汇总");
	    
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    //合计值
	    parameters.put("ptotalamt", (allData.getFooter()==null||allData.getFooter().size()==0)?0:(allData.getFooter().get(0).get("AMT")));
	    
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/GysFendianHuizong/printDeliverPositnSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_DELIVERPOSITNSUM,SupplyAcctConstants.REPORT_EXP_URL_DELIVERPOSITNSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出供应商分店汇总报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverPositnSum")
	@ResponseBody
	public void exportDeliverPositnSum(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商分店汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERPOSITNSUM);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), gysFendianHuizongService.findDeliverPositnSum(condition,pager).getRows(), "供应商分店汇总", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
}
