package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.RkZongheChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("RkZongheChaxun")
public class RkZongheChaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private RkZongheChaxunService rkZongheChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************入库综合查询报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkinmSynQueryHeaders")
	@ResponseBody
	public Object getChkinmSynQuery(HttpSession session,String querytype){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		if(querytype.equals("1"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_DETAIL);
		if(querytype.equals("3"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_CATL);
		if(querytype.equals("2"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_SUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByTable(dictColumns));
		return columns;
	}
	
	/**
	 * 跳转到报表页面
	 * @return
	 */
	@RequestMapping("/toChkinmSynQuery")
	public ModelAndView toChkinmSynQuery(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "入库综合查询");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKINMSYNQUERY,modelMap);
	}
	
	/**
	 * 查询 入库综合查询 内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinmSynQuery")
	@ResponseBody
	public Object findChkinmSynQuery(ModelMap modelMap, HttpSession session, String checby, String order,
			String querytype, String page, String rows, String sort, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("querytype", querytype);
		condition.put("checby", checby);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkZongheChaxunService.findAllChkinmZh(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkinmSynQuery")
	public ModelAndView printChkinmSynQuery(ModelMap modelMap, Page pager, HttpSession session,
			String querytype, String type, SupplyAcct supplyAcct, String checby)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("supplyAcct", supplyAcct);
		condition.put("querytype", querytype);
		condition.put("checby", checby);
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getChktyp() != null && !supplyAcct.getChktyp().equals(""))
			params.put("chktyp",supplyAcct.getChktyp());
		if(supplyAcct.getPositn() != null && !supplyAcct.getPositn().equals(""))
			params.put("positn", supplyAcct.getPositn());
		if(supplyAcct.getDelivercode() != null && !supplyAcct.getDelivercode().equals(""))
			params.put("delivercode",supplyAcct.getDelivercode());
		if(supplyAcct.getGrp() != null && !supplyAcct.getGrp().equals(""))
			params.put("grp",supplyAcct.getGrp());
		if(supplyAcct.getGrptyp() != null && !supplyAcct.getGrptyp().equals(""))
			params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getTyp() != null && !supplyAcct.getTyp().equals(""))
			params.put("typ", supplyAcct.getTyp());
		if(supplyAcct.getBdat() != null)
			params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		if(supplyAcct.getEdat() != null)
			params.put("edat",  DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		if(null != checby)
			params.put("checby", checby);
		params.put("querytype", querytype);
		List<Map<String,Object>> rowList = rkZongheChaxunService.findAllChkinmZh(condition,pager).getRows();
		for(Map<String,Object> map:rowList){
			Date sdate = (Date)map.get("DAT");
			map.put("DAT", DateFormat.getStringByDate(sdate, "yyyy-MM-dd"));/*格式化日期*/
		}
		modelMap.put("List",rowList);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    modelMap.put("actionMap", params);
	    parameters.put("report_name", "入库综合查询");
	    parameters.put("maded",DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/RkZongheChaxun/printChkinmSynQuery.do");//传入回调路径
	 	Map<String,String> rs=null;
	 	if(querytype.equals("1"))
	 		rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINMSYNQUERY_DETAIL,SupplyAcctConstants.REPORT_EXP_URL_CHKINMSYNQUERY_DETAIL);//判断跳转路径
		if(querytype.equals("3"))
			rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINMSYNQUERY_CATL,SupplyAcctConstants.REPORT_EXP_URL_CHKINMSYNQUERY_CATL);//判断跳转路径
		if(querytype.equals("2"))
			rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINMSYNQUERY_SUM,SupplyAcctConstants.REPORT_EXP_URL_CHKINMSYNQUERY_SUM);//判断跳转路径
	 	
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinSynQuery")
	@ResponseBody
	public void exportChkinmSynQuery(HttpServletResponse response, String sort, String order,
			String querytype, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库综合查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("querytype", querytype);
		pager.setPageSize(Integer.MAX_VALUE);
		if(querytype.equals("1"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_DETAIL);
		if(querytype.equals("3"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_CATL);
		if(querytype.equals("2"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_SUM);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), rkZongheChaxunService.findAllChkinmZh(condition,pager).getRows(), "入库综合查询", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
}
