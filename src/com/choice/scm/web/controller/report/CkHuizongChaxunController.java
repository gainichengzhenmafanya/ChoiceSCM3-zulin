package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.CkHuizongChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("CkHuizongChaxun")
public class CkHuizongChaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CkHuizongChaxunService ckHuizongChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	

	/********************************************出库汇总查询报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseChkoutSumQuery")
	public ModelAndView toColChooseChkoutSumQuery(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTSUMQUERY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_CHKOUTSUMQUERY );
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHKOUTSUMQUERY));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkoutSumQueryHeaders")
	@ResponseBody
	public Object getChkoutSumQuery(HttpSession session, String monthMax, String bycomp){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String defaultCol = ScmStringConstant.BASICINFO_REPORT_CHKOUTSUMQUERY ;
		String comp = "136,137,138,139,";
		String month = "140,141";
		if(!"true".equals(bycomp))defaultCol = defaultCol.replace(comp, "");
		if(!"true".equals(monthMax))defaultCol = defaultCol.replace(month,"");
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTSUMQUERY);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns,defaultCol));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_CHKOUTSUMQUERY_FROZEN );
		return columns;
	}
	
	/**
	 * 跳转到报表html页面
	 * @return
	 */
	@RequestMapping("/toChkoutSumQuery")
	public ModelAndView toChkoutSumQuery(ModelMap modelMap, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_CHKOUTSUMQUERY );
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKOUTSUMQUERY,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkoutSumQuery")
	@ResponseBody
	public Object findChkoutSumQuery(ModelMap modelMap, HttpSession session, Date bdatprev,
			Date edatprev, String bycomp, String page, String rows, String sort, String order,
			SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(supplyAcct.getPositn());
		supplyAcct.setFirm(supplyAcct.getFirm());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bycomp", bycomp);
		condition.put("bdatprev", bdatprev);
		condition.put("edatprev", edatprev);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckHuizongChaxunService.findChkoutSumQuery(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkoutSumQuery")
	public ModelAndView printChkoutSumQuery(ModelMap modelMap, Page pager, HttpSession session,
			String type, SupplyAcct supplyAcct, String bycomp, Date bdatprev, Date edatprev)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(supplyAcct.getPositn());
		supplyAcct.setFirm(supplyAcct.getFirm());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		}
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null) {
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktag", String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("bycomp", bycomp);
		if(bdatprev!=null){
			params.put("bdatprev", DateFormat.getStringByDate(bdatprev,"yyyy-MM-dd"));
		}
		if(edatprev!=null){
			params.put("edatprev", DateFormat.getStringByDate(edatprev,"yyyy-MM-dd"));
		}
		
		condition.put("supplyAcct", supplyAcct);
		condition.put("bycomp", bycomp);
		condition.put("bdatprev", bdatprev);
		condition.put("edatprev", edatprev);
		modelMap.put("List",ckHuizongChaxunService.findChkoutSumQuery(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "出库汇总查询");
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bycomp", bycomp);
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkHuizongChaxun/printChkoutSumQuery.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKOUTSUMQUERY,SupplyAcctConstants.REPORT_EXP_URL_CHKOUTSUMQUERY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkoutSumQuery")
	@ResponseBody
	public void exportChkoutSumQuery(HttpServletResponse response, String sort, String order,
			String bycomp, Date bdatprev, Date edatprev, HttpServletRequest request, 
			HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "出库汇总查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bycomp", bycomp);
		condition.put("bdatprev", bdatprev);
		condition.put("edatprev", edatprev);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTSUMQUERY);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), ckHuizongChaxunService.findChkoutSumQuery(condition,pager).getRows(), "出库汇总查询", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}

}
