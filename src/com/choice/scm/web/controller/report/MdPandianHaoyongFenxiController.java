package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.MdPandianHaoyongFenxiService;
@Controller
@RequestMapping("MdPandianHaoyongFenxi")
public class MdPandianHaoyongFenxiController {

	@Autowired
	private Page pager;
	@Autowired
	private MdPandianHaoyongFenxiService mdPandianHaoyongFenxiService;
	@Autowired
	private PositnService positnService;
	
	private static List<Positn> positnList = new ArrayList<Positn>();
	
	/**
	 * 查询表头信息(根据选择的领用仓位 动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findMdPandianHaoyongFenxiHeaders")
	@ResponseBody
	public List<Positn> findMdPandianHaoyongFenxiHeaders(HttpSession session,String firm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(firm == null || "".equals(firm)){
			return positnList;
		}else{
			String[] stringList = firm.split(",");
			List<Positn> lists = new ArrayList<Positn>();
			for(String code:stringList){
				for(Positn p : positnList){
					if(code.equals(p.getCode())){
						lists.add(p);
					}
				}
			}
			return lists;
		}
	}
	
	/********************************************门店盘点分析表****************************************************/
	
	/**
	 * 跳转到报表html页面
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianFenxi")
	public ModelAndView toMdPandianFenxi(ModelMap modelMap) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//查询所有的分店和档口
		positnList = positnService.findPositn(null,null,"'1203','1207'");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDPANDIANFENXI,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findMdPandianFenxi")
	@ResponseBody
	public Object findMdPandianFenxi(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return mdPandianHaoyongFenxiService.findMdPandianFenxi(findMdPandianHaoyongFenxiHeaders(session,supplyAcct.getFirm()),condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandianFenxi")
	@ResponseBody
	public void exportMdPandianFenxi(HttpServletResponse response,HttpServletRequest request,HttpSession session,
			String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点分析表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		//exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		mdPandianHaoyongFenxiService.exportMdPandianFenxi(response.getOutputStream(), condition, findMdPandianHaoyongFenxiHeaders(session,supplyAcct.getFirm()));

	}
	
	/********************************************门店耗用分析表****************************************************/
	
	/**
	 * 跳转到报表html页面
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdHaoyongFenxi")
	public ModelAndView toMdHaoyongFenxi(ModelMap modelMap) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//查询所有的分店和档口
		positnList = positnService.findPositn(null,null,"'1203','1207'");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDHAOYONGFENXI,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findMdHaoyongFenxi")
	@ResponseBody
	public Object findMdHaoyongFenxi(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return mdPandianHaoyongFenxiService.findMdHaoyongFenxi(findMdPandianHaoyongFenxiHeaders(session,supplyAcct.getFirm()),condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportMdHaoyongFenxi")
	@ResponseBody
	public void exportMdHaoyongFenxi(HttpServletResponse response,HttpServletRequest request,HttpSession session,
			String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店耗用分析表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		//exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		mdPandianHaoyongFenxiService.exportMdHaoyongFenxi(response.getOutputStream(), condition, findMdPandianHaoyongFenxiHeaders(session,supplyAcct.getFirm()));

	}
	
}
