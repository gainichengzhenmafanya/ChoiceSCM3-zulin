package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.PositnPand;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.LsPandianchaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/***
 * 历史盘点查询报表
 * @author 2014.12.9wjf
 *
 */
@Controller
@RequestMapping("lsPandianchaxun")
public class LsPandianchaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private LsPandianchaxunService lsPandianchaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;


	/********************************************盘点历史报表****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/toLsPandianchaxun")
	public ModelAndView toLsPandianchaxun(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_LISHIPANDIAN);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_LISHIPANDIAN,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findLsPandianHeaders")
	@ResponseBody
	public Object getSupplyBalanceHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_LISHIPANDIAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_LISHIPANDIAN));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_LISHIPANDIAN_FROZEN );
		return columns;
	}
	
	/***
	 * 查询历史盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findLishiPandian")
	@ResponseBody
	public Object findLishiPandian(ModelMap modelMap, HttpSession session, String page, String rows, 
			String sort, String order,PositnPand positnPand) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		positnPand.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		positnPand.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		positnPand.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnPand.setPositn(CodeHelper.replaceCode(positnPand.getPositn()));
		content.put("positnPand",positnPand);
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return lsPandianchaxunService.findLsPandian(content, pager);
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportLsPandian")
	@ResponseBody
	public void exportLsPandian(HttpServletResponse response, String sort, String order, HttpServletRequest request, HttpSession session,
			PositnPand positnPand) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "历史盘点查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		positnPand.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		positnPand.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		positnPand.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("positnPand",positnPand);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_LISHIPANDIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), lsPandianchaxunService.findLsPandian(condition,pager).getRows(), "历史盘点查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_LISHIPANDIAN));
		
	}
	
	/***
	 * 打印盘点查询
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param without0
	 * @param type
	 * @param supplyAcct
	 * @param withamountin
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printLsPandian")
	public ModelAndView printLsPandian(ModelMap modelMap, Page pager, HttpSession session,String type, PositnPand positnPand)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnPand.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		positnPand.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		positnPand.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(positnPand.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(positnPand.getBdat(),"yyyy-MM-dd"));
		if(positnPand.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(positnPand.getEdat(),"yyyy-MM-dd"));
		params.put("positn",positnPand.getPositn());
		params.put("sp_code",positnPand.getSp_code());
		params.put("grptyp",positnPand.getGrptyp());
		params.put("grp",positnPand.getGrp());
		params.put("typ",positnPand.getTyp());
		params.put("accby",positnPand.getAccby());
		condition.put("positnPand", positnPand);
		modelMap.put("List",lsPandianchaxunService.findLsPandian(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "盘点历史查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountNames").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/lsPandianchaxun/printLsPandian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_LISHIPANDIAN,SupplyAcctConstants.REPORT_EXP_URL_LISHIPANDIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseLsPandian")
	public ModelAndView toColChooseLsPandian(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_LISHIPANDIAN);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_LISHIPANDIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_LISHIPANDIAN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}
