package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.CkMonthHuizongService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("CkMonthHuizong")
public class CkMonthHuizongController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CkMonthHuizongService ckMonthHuizongService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	private ExportExcel<Map<String,Object>> exportmapExcel;
	@Autowired
	PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	AccountPositnService accountPositnService;
	String monthofyear="";
	
	
		
	/********************************************出库按月汇总报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @param ModelMap
	 * @param HttpSession
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseChkoutMonthSum")
	public ModelAndView toColChooseChkoutMonthSum(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTMonthSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_CHKOUTMonthSUM );
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHKOUTMonthSUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @param year  会计年
	 * @param month 会计月
	 * @return
	 */
	@RequestMapping("/findChkoutMonthSumHeaders")
	@ResponseBody
	public Object getChkoutMonthSumHeader(HttpSession session,@RequestParam(value="yearr")String year,String month){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTMonthSUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());					
//		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_CHKOUTMonthSUM ));
		columns.put("columns", ckMonthHuizongService.getCkMonthHuizongHead(year,month));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_CHKOUTMonthSUM_FROZEN );
		return columns;
	}
	
		
	/**
	 * @param ModelMap
	 * 跳转到出库按月汇总页面
	 * @return ModelAndView
	 */
	@RequestMapping("/toChkoutMonthSum")
	public ModelAndView toChkoutMonthSum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_CHKOUTMonthSUM );		
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKOUTMONTHSUM,modelMap);
	}
	
	
	
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param yearr
	 * @param month
	 * @param sort
	 * @param order
	 * @param bysale
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkoutMonthSum")
	@ResponseBody
	public Object findChkoutMonthSum(ModelMap modelMap, HttpSession session, String page, String rows,
			@RequestParam(value="yearr")String year,String month,String sort, String order,String bysale, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		int jmj=0;		
		condition.put("jmj", jmj);				
		String[] strarray=ckMonthHuizongService.getSQLByDateOfMonth(year,month,jmj);
		String datesql=strarray[2];
		condition.put("monthofyear",monthofyear);
		condition.put("datesql", datesql);		
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckMonthHuizongService.findChkoutMonthSum(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkoutMonthSum")
	public ModelAndView printChkoutMonthSum(ModelMap modelMap, Page pager, HttpSession session,
			String type,String sort, String order,String bysale, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("sort", sort);
		params.put("order", order);
		params.put("bysale", bysale);
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		condition.put("supplyAcct", supplyAcct);
	    modelMap.put("actionMap", params);
		modelMap.put("List",ckMonthHuizongService.findChkoutMonthSum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "出库按月汇总");
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
//	    parameters.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkMonthHuizong/printChkoutMonthSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKOUTDATESUM2,SupplyAcctConstants.REPORT_EXP_URL_CHKOUTDATESUM2);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 获取会计年和会计月,前台查看明细时使用
	 * @param year
	 * @param month
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryDate")
	@ResponseBody
	public Object queryDate(@RequestParam(value="yearr")String year,String month,ModelMap modelMap)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		int jmj=0;
		String[] strarray=ckMonthHuizongService.getSQLByDateOfMonth(year,month,jmj);
		modelMap.addAttribute("begindate", strarray[0]);
		modelMap.addAttribute("enddate", strarray[1]);
		return modelMap;
	}
	
	/**
	 * 导出excel
	 * @param response
	 * @param sort
	 * @param order
	 * @param bysale
	 * @param year
	 * @param month
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkoutMonthSum")
	@ResponseBody
	public void exportChkoutMonthSum(HttpServletResponse response, String sort, String order,String bysale,@RequestParam(value="yearr")String year,String month,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "出库按月汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		int jmj=0;		
		condition.put("jmj", jmj);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTMonthSUM);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   		
				
		String datesql=ckMonthHuizongService.getSQLByDateOfMonth(year,month,jmj)[2];				
		condition.put("datesql", datesql);
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportmapExcel.creatWorkBook(response.getOutputStream(), ckMonthHuizongService.findChkoutMonthSum(condition,pager).getRows(), "出库按月汇总", ckMonthHuizongService.getCkMonthHuizongHead(year,month));
	}

}
