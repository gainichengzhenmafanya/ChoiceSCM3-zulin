package com.choice.scm.web.controller.report;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.RkMingxiChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("RkMingxiChaxun")
public class RkMingxiChaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private RkMingxiChaxunService rkMingxiChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private AcctService acctService;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
    ChkinmService chkinmService;
	

	/********************************************入库明细查询报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkinDQHeaders")
	@ResponseBody
	public Object getChkinDQHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKIN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKIN));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_CHKIN_FROZEN);
		
		return columns;
	}
	
	/**
	 * 跳转到入库明细查询报表页面
	 * @return
	 * @throws CRUDException 
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/toChkinDetailS")
	public ModelAndView toChkinDetailS(ModelMap modelMap,SupplyAcct supplyAcct) throws CRUDException, UnsupportedEncodingException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != supplyAcct.getDeliverdes() && !"".equals(supplyAcct.getDeliverdes()))||(null == supplyAcct.getDelivercode() && "".equals(supplyAcct.getDelivercode()))){
			Deliver deliver = new Deliver();
			deliver.setDes(supplyAcct.getDeliverdes());
			List<Deliver> list = deliverService.findAllDelivers(deliver);
			supplyAcct.setDelivercode(list.size()>0?list.get(0).getCode():"");
		}
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_CHKIN);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKINDETAILS,modelMap);
	}
	
	/**
	 * 查询入库明细查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkinDetailS")
	@ResponseBody
	public Object ChkinDetailS(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return rkMingxiChaxunService.findChkinDetailS(condition,pager);
	}
	
	/**
	 * 打印入库库明细查询报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkinDetailS")
	public ModelAndView printChkinDetailS(ModelMap modelMap, Page pager, HttpSession session,
			SupplyAcct supplyAcct, String type)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("des",supplyAcct.getDes());
		condition.put("supplyAcct", supplyAcct);
		
		//查询数据
		ReportObject<SupplyAcct> ro = rkMingxiChaxunService.findChkinDetailS(condition,pager);
		//详细数据
		List<SupplyAcct> supplyacctList = ro.getRows();
		for(SupplyAcct supplyacct:supplyacctList){
			supplyacct.setChkinid(supplyacct.getChkno());//打印时单号查的chkinid 这里把单号chkno赋给 chkinid 暂不改报表了
		}
		modelMap.put("List",supplyacctList);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "入库单明细查询");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    
		//合计值
		List<Map<String,Object>> foot = ro.getFooter();
	    //把合计数据传入到模板直接显示
		if(foot!=null&&foot.size()>0){
			Map<String,Object> footMap = foot.get(0);
			parameters.put("sum_cntin", String.valueOf(footMap.get("cntin")));
			parameters.put("sum_amtin", String.valueOf(footMap.get("AMTIN")));
		}
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/RkMingxiChaxun/printChkinDetailS.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINDETAILS,SupplyAcctConstants.REPORT_EXP_URL_CHKINDETAILS);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出入库明细查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkinDetailS")
	@ResponseBody
	public void exportChkinDetailS(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "入库明细查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKIN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), rkMingxiChaxunService.findChkinDetailS(condition,pager).getRows(), "入库明细查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKIN));		
	}

    //-----------------------------------------------------------

    /**
     * 入库明细穿透至入库单据
     * @param modelMap
     * @param chkinm
     * @param dat
     * @param chktag
     * @param page
     * @return
     * @throws Exception
     */
    @RequestMapping("/toRkBill")
    public ModelAndView toRkBillByNo(ModelMap modelMap, Chkinm chkinm,Date dat,String chktag, Page page,HttpSession session)throws Exception {
        if ("1".equals(chktag)) {
//            chkinm.setTyp("产品入库");
            chkinm.setInout("rk");
        }else{
            chkinm.setInout("zf");
        }
        if(chkinm.getMaded()==null) {
            chkinm.setMaded(dat);
        }
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        modelMap.put("chkinmList", chkinmService.findAllChkinm("check", chkinm, page, dat,session.getAttribute("locale").toString()));
        modelMap.put("madedEnd", dat);
        modelMap.put("chkinm", chkinm);
        modelMap.put("checkOrNot", "check");
        modelMap.put("pageobj", page);
        return new ModelAndView(ChkinmConstants.LIST_CHECKED_CHKINM, modelMap);
    }
	
}
