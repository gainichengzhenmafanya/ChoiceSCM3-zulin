package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkoutConstants;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkoutService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.service.report.CkMingxiChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("CkMingxiChaxun")
public class CkMingxiChaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CkMingxiChaxunService ckMingxiChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
    private ChkoutService chkoutService;
	@Autowired
	private SupplyService supplyService;
	
	/********************************************出库明细查询报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseChkoutDetailQuery")
	public ModelAndView toColChooseChkoutDetailQuery(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTDETAILQUERY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_CHKOUTDETAILQUERY );
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkoutDetailQueryHeaders")
	@ResponseBody
	public Object getChkoutDetailQuery(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTDETAILQUERY);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKOUTDETAILQUERY));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_CHKOUTDETAILQUERY_FROZEN );
		return columns;
	}
	
	/**
	 * 跳转到报表页面
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/toChkoutDetailQuery")
	public ModelAndView toChkoutDetailQuery(ModelMap modelMap, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != supplyAcct.getGrptypdes() && !"".equals(supplyAcct.getGrptypdes())) && ("".equals(supplyAcct.getGrptyp()) || null == supplyAcct.getGrptyp())){
			List<GrpTyp> list = grpTypService.findGrpTypByDes(supplyAcct.getGrptypdes());
			supplyAcct.setGrptyp(list.size()>0?list.get(0).getCode():"");
		}
		if((null != supplyAcct.getFirmdes() && !"".equals(supplyAcct.getFirmdes())) && ("".equals(supplyAcct.getFirmcode()) || null == supplyAcct.getFirmcode())){
			Positn firm = new Positn();
			firm.setDes(supplyAcct.getFirmdes());
			List<Positn> listf = positnService.findAllPositn(firm);
			supplyAcct.setFirmcode(listf.size()>0?listf.get(0).getCode():"");
		}
		if((null != supplyAcct.getPositndes() && !"".equals(supplyAcct.getPositndes())) && ("".equals(supplyAcct.getPositn()) || null == supplyAcct.getPositn())){
			Positn positn = new Positn();
			positn.setDes(supplyAcct.getPositndes());
			List<Positn> listp = positnService.findAllPositn(positn);
			supplyAcct.setPositn(listp.size()>0?listp.get(0).getCode():"");
		}
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_CHKOUTDETAILQUERY );
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKOUTDETAILQUERY,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkoutDetailQuery")
	@ResponseBody
	public Object findChkoutDetailQuery(ModelMap modelMap, HttpSession session, String bysale,
			String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckMingxiChaxunService.findChkoutDetailQuery(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkoutDetailQuery")
	public ModelAndView printChkoutDetailQuery(ModelMap modelMap, Page pager, String bysale,
			HttpSession session, String type, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		
		String positn = supplyAcct.getPositn();
		String firm = supplyAcct.getFirm();
		String delivercode = supplyAcct.getDelivercode();
		
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",positn);
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("firm",firm);
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktag", String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",delivercode);
		params.put("bysale", bysale);
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		
		//按售价
		condition.put("bysale", bysale);
		
		modelMap.put("List",ckMingxiChaxunService.findChkoutDetailQuery(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    modelMap.put("actionMap", params);
	    parameters.put("report_name", "出库明细查询");
	    parameters.put("maded",DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkMingxiChaxun/printChkoutDetailQuery.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKOUTDETAILQUERY,SupplyAcctConstants.REPORT_EXP_URL_CHKOUTDETAILQUERY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportChkoutDetailQuery")
	@ResponseBody
	public void exportChkoutDetailQuery(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, String bysale, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "出库明细查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKOUTDETAILQUERY);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), ckMingxiChaxunService.findChkoutDetailQuery(condition,pager).getRows(), "出库明细查询报表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_CHKOUTDETAILQUERY));
	}
    //===========================================================
    @RequestMapping("/toCkBill")
    public ModelAndView toCkBillByNo(ModelMap modelMap, Chkoutm chkoutm, Date dat,Date edat,HttpSession session, Page page) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
        chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
        chkoutm.setChecby("c");
        modelMap.put("pageobj", page);
        modelMap.put("bdat", dat);
        if(edat==null) {
            modelMap.put("edat", dat);
        }else{
            modelMap.put("edat",edat);
        }
        modelMap.put("chkoutList",chkoutService.findChkoutm(chkoutm, dat, edat==null?dat:edat, page,session.getAttribute("locale").toString()));
        modelMap.put("chkoutm", chkoutm);
        return new ModelAndView(ChkoutConstants.CHECKED_CHKOUT, modelMap);
//        return new ModelAndView(ChkoutConstants.MANAGE_CHKOUT, modelMap);
    }
    
    /***
	 * 查询所有的税率列表
	 * @author wjf
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllTax") 
	@ResponseBody
	public Object findAllTax(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return supplyService.findTax(null);
	}
}
