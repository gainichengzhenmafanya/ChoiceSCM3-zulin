package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.GysLeibieHuizongService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("GysLeibieHuizong")
public class GysLeibieHuizongController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private GysLeibieHuizongService gysLeibieHuizongService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	@Autowired
	PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	private GrpTypService grpTypService;
	
	/********************************************供应商类别汇总报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseDeliverCategorySum")
	public ModelAndView toColChooseDeliverCategorySum(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERCATEGORYSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_DELIVERCATEGORYSUM);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_DELIVERCATEGORYSUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeliverCategorySumHeaders")
	@ResponseBody
	public Object getDeliverCategorySumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERCATEGORYSUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_DELIVERCATEGORYSUM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_DELIVERCATEGORYSUM_FROZEN);
		return columns;
	}
	/**
	 * 跳转到供应商类别汇总报表页面
	 * @return
	 */
	@RequestMapping("/toDeliverCategorySum")
	public ModelAndView toDeliverCategorySum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_DELIVERCATEGORYSUM);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DELIVERCATEGORYSUM,modelMap);
	}
	/**
	 * 查询供应商类别汇总报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverCategorySum")
	@ResponseBody
	public Object ChkinDeliverCategorySum(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gysLeibieHuizongService.findDeliverCategorySum(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeliverCategorySum")
	public ModelAndView printDeliverCategorySum(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grpdes",supplyAcct.getGrpdes());
		params.put("grptyp",supplyAcct.getGrptyp());
		//params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("chktag",String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",gysLeibieHuizongService.findDeliverCategorySum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "供应商类别汇总");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/GysLeibieHuizong/printDeliverCategorySum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_DELIVERCATEGORYSUM,SupplyAcctConstants.REPORT_EXP_URL_DELIVERCATEGORYSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出供应商类别汇总报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverCategorySum")
	@ResponseBody
	public void exportDeliverCategorySum(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商类别汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERCATEGORYSUM);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), gysLeibieHuizongService.findDeliverCategorySum(condition,pager).getRows(), "供应商类别汇总", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
	
	/********************************************供应商类别汇总报表2  热辣定制  2014.11.13wjf***************************************************/
	
	/**
	 * 跳转到供应商类别汇总报表页面2  热辣定制 
	 * @author wjf
	 * @return
	 */
	@RequestMapping("/toDeliverCategorySum2")
	public ModelAndView toDeliverCategorySum2(HttpSession session,ModelMap modelMap,Page page,String action,SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positn = supplyAcct.getPositn();
		String delivercode = supplyAcct.getDelivercode();
		String sgrptyp = supplyAcct.getGrptyp();
		String grp = supplyAcct.getGrp();
		String chktyp = supplyAcct.getChktyp();
		try{
			String acct = session.getAttribute("ChoiceAcct").toString();
			if("init".equals(action)){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				String dates=sdf.format(new Date());
				supplyAcct.setBdat(sdf.parse(dates));
				supplyAcct.setEdat(sdf.parse(dates));
			}
			//根据条件得到所有的大类,包含中类
			GrpTyp grptyp = new GrpTyp();
			grptyp.setAcct(acct);
			grptyp.setCode(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			grptyp.setGrpcode(CodeHelper.replaceCode(supplyAcct.getGrp()));
			List<GrpTyp> grptyps = grpTypService.findAllGrpTypByCondition(grptyp);
			modelMap.put("columns",grptyps);//得到类别
			Map<String,Object> condition = new HashMap<String,Object>();
			supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
			supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
			supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
			// 分店是否使用库存系统
			supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
			condition.put("supplyAcct", supplyAcct);
			condition.put("grptyps", grptyps);
			modelMap.put("supplyAcct", supplyAcct);
			modelMap.put("ListBean", JSONArray.fromObject(gysLeibieHuizongService.findDeliverCategorySum2(condition,page)).toString());//table体list
			modelMap.put("pageobj", page);
			supplyAcct.setPositn(positn);
			supplyAcct.setDelivercode(delivercode);
			supplyAcct.setGrptyp(sgrptyp);
			supplyAcct.setGrp(grp);
			supplyAcct.setChktyp(chktyp);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DELIVERCATEGORYSUM2,modelMap);
	}
	
	/**
	 * 导出供应商类别汇总报表2
	 * @author wjf
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverCategorySum2")
	@ResponseBody
	public void exportDeliverCategorySum2(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商类别汇总2";
		Map<String,Object> condition = new HashMap<String,Object>();
		String acct = session.getAttribute("ChoiceAcct").toString();
		//根据条件得到所有的大类,包含中类
		GrpTyp grptyp = new GrpTyp();
		grptyp.setAcct(acct);
		grptyp.setCode(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
		grptyp.setGrpcode(CodeHelper.replaceCode(supplyAcct.getGrp()));
		List<GrpTyp> grptyps = grpTypService.findAllGrpTypByCondition(grptyp);
		supplyAcct.setAcct(acct);
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("grptyps", grptyps);
		List<Map<String,Object>> lists = gysLeibieHuizongService.findDeliverCategorySum2(condition,null);//table体list
		
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		gysLeibieHuizongService.exportGysLeibieHuizong2(response.getOutputStream(), grptyps,lists);
	}
	
	
	
	/********************************************供应商类别汇总3  快客利定制 20151207wjf****************************************************/
	
	/**
	 * 查询表头信息(根据选择的领用仓位 动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findGysLeibieHuizongHeaders3")
	@ResponseBody
	public List<GrpTyp> findGysLeibieHuizongHeaders3(String typ) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<GrpTyp> grpTypList = gysLeibieHuizongService.findAllType(typ);
		return grpTypList;
	}
	
	/**
	 * 跳转到报表html页面
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toDeliverCategorySum3")
	public ModelAndView toDeliverCategorySum3(ModelMap modelMap, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DELIVERCATEGORYSUM3,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverCategorySum3")
	@ResponseBody
	public ReportObject<Map<String,Object>> findDeliverCategorySum3(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gysLeibieHuizongService.findDeliverCategorySum3(findGysLeibieHuizongHeaders3(supplyAcct.getTyp()),condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverCategorySum3")
	@ResponseBody
	public void exportDeliverCategorySum3(HttpServletResponse response,HttpServletRequest request,HttpSession session,
			String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商类别汇总3";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		List<GrpTyp> grpTypList = findGysLeibieHuizongHeaders3(supplyAcct.getTyp());
		List<Map<String,Object>> list = gysLeibieHuizongService.findDeliverCategorySum3(findGysLeibieHuizongHeaders3(supplyAcct.getTyp()),condition,null).getRows();
//		exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		gysLeibieHuizongService.exportDeliverCategorySum3(response.getOutputStream(), grpTypList, list);

	}
}
