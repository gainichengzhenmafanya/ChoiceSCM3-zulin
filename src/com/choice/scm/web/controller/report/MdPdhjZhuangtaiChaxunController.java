package com.choice.scm.web.controller.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.MdPdhjZhuangtaiChaxunService;

/**
 * 门店BOH 盘点核减状态查询
 * @author wjf
 *
 */
@Controller
@RequestMapping(value="MdPdhjZhuangtaiChaxun")
public class MdPdhjZhuangtaiChaxunController {
	@Autowired
	private MdPdhjZhuangtaiChaxunService mdPdhjZhuangtaiChaxunService;
	/**
	 * 盘点核减状态查询页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("toMdPdhjState")
	public ModelAndView toMdPdhjState(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDPDHJZHUANGTAICHAXUN,modelMap);
	}
	/**
	 * 门店盘点状态表查询数据
	 * @param modelMap
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findMdPdhjState")
	@ResponseBody
	public Object findMdPdhjState(ModelMap modelMap,Page pager,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return mdPdhjZhuangtaiChaxunService.findMdPdhjState(supplyAcct, pager);
	}
	/**
	 * 获取动态日期
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findFreetimeHeader")
	@ResponseBody
	public Object findFreetimeHeader(SupplyAcct condition) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return mdPdhjZhuangtaiChaxunService.findFreetimeHeader(condition);
	}
}
