package com.choice.scm.web.controller.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.MdRiPanDianZhuangTaiBiaoService;

/**
 * 门店日盘点状态表
 * @author 孙胜彬
 */
@Controller
@RequestMapping(value="mdRiPanDianZhuangTaiBiao")
public class MdRiPanDianZhuangTaiBiaoController {
	@Autowired
	private MdRiPanDianZhuangTaiBiaoService mdRiPanDianZhuangTaiBiaoService;
	/**
	 * 门店日盘点状态表页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("findMdRiPanDianZhuangTaiBiao")
	public ModelAndView findMdRiPanDianZhuangTaiBiao(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.LIST_MDRIPANDIANZHUANGTAIBIAO_PROFIT,modelMap);
	}
	/**
	 * 门店日盘点状态表查询数据
	 * @param modelMap
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listMdRiPanDianZhuangTaiBiao")
	@ResponseBody
	public Object listMdRiPanDianZhuangTaiBiao(ModelMap modelMap,Page pager,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return mdRiPanDianZhuangTaiBiaoService.findMdRiPanDianZhuangTaiBiao(supplyAcct, pager);
	}
	/**
	 * 获取动态日期
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("findFreetimeHeader")
	@ResponseBody
	public Object findFreetimeHeader(SupplyAcct condition) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return mdRiPanDianZhuangTaiBiaoService.findFreetimeHeader(condition);
	}
}
