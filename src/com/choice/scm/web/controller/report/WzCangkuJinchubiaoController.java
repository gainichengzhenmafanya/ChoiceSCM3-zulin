package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.WzCangkuJinchubiaoService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzCangkuJinchubiao")
public class WzCangkuJinchubiaoController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzCangkuJinchubiaoService wzCangkuJinchubiaoService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************物资仓库进出表报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findGoodsStoreInoutHeaders")
	@ResponseBody	
	public Object getGoodsStoreInoutHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_GOODSSTOREINOUT);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_GOODSSTOREINOUT));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_GOODSSTOREINOUT_FROZEN);
		return columns;
	}
	/**
	 * 跳转到物资仓库进出表页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/toGoodsStoreInout")
	public ModelAndView toGoodsStoreInout(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_GOODSSTOREINOUT);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_GOODSSTOREINOUT,modelMap);
	}
	/**
	 * 查询物资仓库进出表
	 */
	@RequestMapping("/findGoodsStoreInout")
	@ResponseBody
	public Object findGoodsStoreInout(ModelMap modelMap, HttpSession session, String page, String rows,String sort,String order, String bdat, String edat,
			String filtZero, SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		content.put("noZero",filtZero);
		content.put("supplyAcct",supplyAcct);
		content.put("sort", sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return wzCangkuJinchubiaoService.findGoodsStoreInout(content, pager);
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseGoodsStoreInout")
	public ModelAndView toColChooseGoodsStoreInout(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_GOODSSTOREINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_GOODSSTOREINOUT);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_GOODSSTOREINOUT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printGoodsStoreInout")
	public ModelAndView printGoodsStoreInout(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct,String filtZero)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		params.put("noZero",filtZero);
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		}
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		}	
		condition.put("supplyAcct", supplyAcct);
		condition.put("noZero",filtZero);
		modelMap.put("List",wzCangkuJinchubiaoService.findGoodsStoreInout(condition,pager).getRows());
	 	
	    parameters.put("report_name", "库存物资本期仓位进出表");
	    modelMap.put("actionMap", params);
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzCangkuJinchubiao/printGoodsStoreInout.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_EXP_URL_GOODSSTOREINOUT,SupplyAcctConstants.REPORT_EXP_URL_GOODSSTOREINOUT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出excel
	 */
	@RequestMapping("/exportGoodsStoreInout")
	@ResponseBody
	public void exportGoodsStoreInout(HttpServletResponse response,HttpServletRequest request,HttpSession session,String grp,String grptyp,String typ,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "库存物资类别进出表";
		List<String> grpList=null;
		List<String> grptypList=null;
		List<String> typList=null;
	
		if(grp!=null && grp!=""){
			grpList=Arrays.asList(grp.split(","));
		}
		if(grptyp!=null&&grptyp!=""){
			grptypList=Arrays.asList(grptyp.split(","));
		}
		if(typ!=null&&typ!=""){
			typList=Arrays.asList(typ.split(","));
		}
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("grpList",grpList);
		condition.put("grptypList", grptypList);
		condition.put("typList", typList);
		condition.put("supplyAcct", supplyAcct);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_GOODSSTOREINOUT);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzCangkuJinchubiaoService.findGoodsStoreInout(condition,pager).getRows(), "库存物资类别进出表", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_GOODSSTOREINOUT));	
	}
}
