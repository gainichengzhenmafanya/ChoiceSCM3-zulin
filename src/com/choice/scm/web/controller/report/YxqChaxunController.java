package com.choice.scm.web.controller.report;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.YxqChaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/***
 * 有效期查询报表
 * @author wjf
 *
 */
@Controller
@RequestMapping("YxqChaxun")
public class YxqChaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private YxqChaxunService yxqChaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<SupplyAcct> exportExcel;
	

	/********************************************有效期查询报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseYxqChaxun")
	public ModelAndView toColChooseChkoutDetailQuery(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YXQ);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_YXQ );
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findYxqHeaders")
	@ResponseBody
	public Object getChkinDQHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YXQ);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_YXQ));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_YXQ_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到有效期查询报表页面
	 * @return
	 * @throws CRUDException 
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/toYxqDetailS")
	public ModelAndView toChkinDetailS(ModelMap modelMap) throws CRUDException, UnsupportedEncodingException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_YXQ);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_YXQ,modelMap);
	}
	
	/**
	 * 查询有效期查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findYxqDetailS")
	@ResponseBody
	public Object findYxqDetailS(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, Spbatch spbatch) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		spbatch.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		spbatch.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		spbatch.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		spbatch.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("spbatch", spbatch);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return yxqChaxunService.findYxqDetailS(condition,pager);
	}
	
	/**
	 * 打印入库库明细查询报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printYxqDetailS")
	public ModelAndView printYxqDetailS(ModelMap modelMap, Page pager, HttpSession session,
			Spbatch spbatch, String type)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spbatch.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		spbatch.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		spbatch.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		spbatch.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("ind",DateFormat.getStringByDate(spbatch.getInd(), "yyyy-MM-dd"));
		params.put("positn",spbatch.getPositn());
		params.put("grptyp",spbatch.getGrptyp());
		params.put("grp",spbatch.getGrp());
		params.put("typ",spbatch.getTyp());
		params.put("deliver",spbatch.getDeliver());
		condition.put("spbatch", spbatch);
		modelMap.put("List",yxqChaxunService.findYxqDetailS(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "有效期查询");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/YxqChaxun/printYxqDetailS.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_YXQ,SupplyAcctConstants.REPORT_EXP_URL_YXQ);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出入库明细查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYxqDetailS")
	@ResponseBody
	public void exportYxqDetailS(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, HttpSession session, Spbatch spbatch) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "有效期查询报表";
		Map<String,Object> condition = new HashMap<String,Object>();
		spbatch.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		spbatch.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		spbatch.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		spbatch.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		condition.put("spbatch", spbatch);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_YXQ);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(), yxqChaxunService.findYxqDetailS(condition,pager).getRows(), "有效期报表查询", dictColumnsService.listDictColumnsByTable(dictColumns));		
	}
	
}
