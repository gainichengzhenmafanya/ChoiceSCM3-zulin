package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.report.WzMingxiZhangService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzMingxiZhang")
public class WzMingxiZhangController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzMingxiZhangService wzMingxiZhangService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	
	/********************************************物资明细帐报表****************************************************/
	/**
	 * 跳转到物资明细帐页面
	 * @param modelMap
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toSupplyDetailsInfo")
	public ModelAndView toSupplyDetailsInfo(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_SUPPLYDETAILSINFO,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyDetailsInfoHeaders")
	@ResponseBody
	public Object getSupplyDetailsInfoHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO_FROZEN);
		return columns;
	}
	
	/**
	 * 查询物资明细账列表
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyDetailsInfo")
	@ResponseBody
	public Object findSupplyDetailsInfo(ModelMap modelMap, HttpSession session, String page, String withdat,
			String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());

		content.put("withdat", withdat);
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return wzMingxiZhangService.findSupplyDetailsInfo(content, pager, String.valueOf(session.getAttribute("local")));
	}
	/**
	 * 导出物资明细账excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyDetailsInfo")
	@ResponseBody
	public void exportSupplyDetailsInfo(HttpServletResponse response, String sort, String order,String withdat,
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "库存物资明细分类账";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("withdat", withdat);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYDETAILSINFO);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzMingxiZhangService.findSupplyDetailsInfo(condition,pager,String.valueOf(session.getAttribute("local"))).getRows(), "库存物资明细分类账", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYDETAILSINFO));
		
	}
	
	/**
	 * 打印物资明细账
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printSupplyDetailsInfo")
	public ModelAndView printSupplyDetailsInfo(ModelMap modelMap, Page pager, HttpSession session,
			SupplyAcct supplyAcct, String type, String withdat)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("positndes",supplyAcct.getPositndes());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("sp_name",supplyAcct.getSp_name());
		params.put("sp_desc",supplyAcct.getSp_desc());
		params.put("unit",supplyAcct.getUnit());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("withdat",withdat);
		condition.put("supplyAcct", supplyAcct);
		condition.put("withdat", withdat);
		modelMap.put("List",wzMingxiZhangService.findSupplyDetailsInfo(condition,pager,String.valueOf(session.getAttribute("local"))).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "库存物资明细分类账");
	    modelMap.put("actionMap", params);
	    parameters.put("positn",supplyAcct.getPositndes());
		parameters.put("sp_code",supplyAcct.getSp_code());
		parameters.put("sp_name",supplyAcct.getSp_name());
		parameters.put("sp_desc",supplyAcct.getSp_desc());
		parameters.put("unit",supplyAcct.getUnit());
		parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzMingxiZhang/printSupplyDetailsInfo.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_SUPPLYDETAILSINFO,SupplyAcctConstants.REPORT_EXP_URL_SUPPLYDETAILSINFO);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
}
