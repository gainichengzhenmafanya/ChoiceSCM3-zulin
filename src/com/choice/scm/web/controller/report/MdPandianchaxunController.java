package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Chkstore;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.service.SupplyService;
import com.choice.scm.service.report.MdPandianchaxunService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/***
 * 门店boh盘点查询报表
 * @author 2015.9.1wjf
 *
 */
@Controller
@RequestMapping("MdPandianchaxun")
public class MdPandianchaxunController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private MdPandianchaxunService mdPandianchaxunService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private SupplyService supplyService;
	private static List<SupplyUnit> suList = new ArrayList<SupplyUnit>();
	
	
	/********************************************门店盘点报表单据****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianchaxun")
	public ModelAndView toMdPandianchaxun(ModelMap modelMap) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_MDPANDIAN);
//		suList = supplyService.findAllSupplyUnit(new SupplyUnit());
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDPANDIAN,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianHeaders")
	@ResponseBody
	public Object findMdPandianHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandian")
	@ResponseBody
	public Object findMdPandian(ModelMap modelMap, HttpSession session, String page, String rows, Chkstore chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstore.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		chkstore.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandian(chkstore, pager);
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandian")
	@ResponseBody
	public void exportMdPandian(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstore chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点查询";
		chkstore.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		chkstore.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), mdPandianchaxunService.findMdPandian(chkstore,pager).getRows(), "门店盘点查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN));
		
	}
	
	/***
	 * 打印盘点查询
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param without0
	 * @param type
	 * @param supplyAcct
	 * @param withamountin
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printMdPandian")
	public ModelAndView printMdPandian(ModelMap modelMap, Page pager, HttpSession session,String type, Chkstore chkstore)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstore.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		chkstore.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		if(chkstore.getBdate() != null)
			params.put("bdat",chkstore.getBdate());
		if(chkstore.getEdate() != null)
			params.put("edat",chkstore.getEdate());
		params.put("dept", CodeHelper.replaceCode(chkstore.getDept()));
		params.put("pantyp", chkstore.getPantyp());
		params.put("state", chkstore.getState());
		params.put("ecode", chkstore.getEcode());
		params.put("checkCode", chkstore.getCheckCode());
		modelMap.put("List",mdPandianchaxunService.findMdPandian(chkstore,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "门店盘点查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountNames").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/MdPandianchaxun/printMdPandian.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_MDPANDIAN,SupplyAcctConstants.REPORT_EXP_URL_MDPANDIAN);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseMdPandian")
	public ModelAndView toColChooseMdPandian(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_MDPANDIAN);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_MDPANDIAN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}


	/********************************************门店盘点报表明细****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianchaxunDetail")
	public ModelAndView toMdPandianchaxunDetail(ModelMap modelMap,Chkstore chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_MDPANDIAN_DETAIL);
		modelMap.put("chkstore", chkstore);
		suList = supplyService.findAllSupplyUnit(new SupplyUnit());
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDPANDIAN_DETAIL,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianDetailHeaders")
	@ResponseBody
	public Object findMdPandianDetailHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_DETAIL);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_DETAIL));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_DETAIL_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandianDetail")
	@ResponseBody
	public Object findMdPandianDetail(ModelMap modelMap, HttpSession session, String page, String rows, Chkstore chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandian(chkstore, pager, suList);
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandianDetail")
	@ResponseBody
	public void exportMdPandianDetail(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstore chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点明细查询";
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_DETAIL);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook_j1(response.getOutputStream(), mdPandianchaxunService.findMdPandian(chkstore,pager, suList).getRows(), "门店盘点明细查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_DETAIL));
		
	}
	
	/***
	 * 打印盘点查询
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param without0
	 * @param type
	 * @param supplyAcct
	 * @param withamountin
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printMdPandianDetail")
	public ModelAndView printMdPandianDetail(ModelMap modelMap, Page pager, HttpSession session,String type, Chkstore chkstore)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("chkstorefid",chkstore.getChkstorefid());
		params.put("sp_code",chkstore.getSp_code());
		params.put("grptyp",chkstore.getGrptyp());
		params.put("grp",chkstore.getGrp());
		params.put("typ",chkstore.getTyp());
		params.put("ynpd",chkstore.getYnpd());
		modelMap.put("List",mdPandianchaxunService.findMdPandian(chkstore,pager, suList).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "门店盘点查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountNames").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/MdPandianchaxun/printMdPandianDetail.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_MDPANDIAN_DETAIL,SupplyAcctConstants.REPORT_EXP_URL_MDPANDIAN_DETAIL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/***
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseMdPandianDetail")
	public ModelAndView toColChooseMdPandianDetail(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_DETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_MDPANDIAN_DETAIL);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_DETAIL));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/********************************************门店盘点明細****************************************************/
	/**
	 * 跳转到盘点历史查询页面
	 * @param modelMap
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/toMdPandianMingxi")
	public ModelAndView toMdPandianMingxi(ModelMap modelMap) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_MDPANDIAN_MINGXI);
		suList = supplyService.findAllSupplyUnit(new SupplyUnit());
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_MDPANDIAN_MINGXI,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMdPandianMingxiHeaders")
	@ResponseBody
	public Object findMdPandianMingxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_MINGXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_MINGXI));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_MINGXI_FROZEN );
		return columns;
	}
	
	/***
	 * 查询门店盘点数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param positnPand
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMdPandianMingxi")
	@ResponseBody
	public Object findMdPandianMingxi(ModelMap modelMap, HttpSession session, String page, String rows, Chkstore chkstore) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstore.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		chkstore.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return mdPandianchaxunService.findMdPandianMingxi(chkstore, pager, suList);
	}
	/**
	 * 导出历史盘点查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param positnPand
	 * @throws Exception
	 */
	@RequestMapping("/exportMdPandianMingxi")
	@ResponseBody
	public void exportMdPandianMingxi(HttpServletResponse response, HttpServletRequest request, HttpSession session, Chkstore chkstore) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "门店盘点明细";
		chkstore.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		chkstore.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		chkstore.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstore.setDept(CodeHelper.replaceCode(chkstore.getDept()));
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_MINGXI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), mdPandianchaxunService.findMdPandianMingxi(chkstore,pager, suList).getRows(), "门店盘点明细", 
				dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_MINGXI));
		
	}
	
	/***
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseMdPandianMingxi")
	public ModelAndView toColChooseMdPandianMingxi(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_MDPANDIAN_MINGXI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_MDPANDIAN_MINGXI);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_MDPANDIAN_MINGXI));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}
