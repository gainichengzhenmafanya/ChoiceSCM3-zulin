package com.choice.scm.web.controller.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.LyGysLeibieHuizongService;
import com.choice.scm.util.PublicExportExcel;

/**
 * 领用供应商类别汇总
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("LyGysLeibieHuizong")
public class LyGysLeibieHuizongController {
	@Autowired
	private Page pager;
	@Autowired
	private LyGysLeibieHuizongService lyGysLeibieHuizongService;
	@Autowired
	PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	AccountPositnService accountPositnService;

	/**************************************** 领用供应商类别汇总报表 *******************************************/
	/**
	 * 跳转到领用供应商类别汇总报表
	 * 
	 * @return
	 */
	@RequestMapping("/toLyGysLeibieHuizong")
	public ModelAndView toLyGysLeibieHuizong(ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 初始设置时间
		Date date = new Date();
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setBdat(date);
		supplyAcct.setEdat(date);
		modelMap.put("supplyAcct", supplyAcct);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_LYDELIVERCATEGORYSUM, modelMap);
	}

	/**
	 * 查询领用供应商类别汇总报表
	 * 
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findLyGysLeibieHuizong")
	public ModelAndView findLyGysLeibieHuizong(ModelMap modelMap, HttpSession session, String nowPage, String pageSize, String sort, String order, SupplyAcct supplyAcct) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> condition = new HashMap<String, Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());// 账号
		// 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));// 是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));// 是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);

		// 查询数据返回
		List<?> lists = lyGysLeibieHuizongService.lyGysLeibieHuizongDetail(condition, pager);
		if (lists != null) {
			modelMap.put("list", lists.get(0));
			modelMap.put("foot", lists.get(1));
			modelMap.put("delivers", lists.get(2));
		}
		modelMap.put("supplyAcct", supplyAcct);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_LYDELIVERCATEGORYSUM, modelMap);
	}

	/**
	 * 导出供应商类别汇总报表
	 * 
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/exportLyGysLeibieHuizong")
	@ResponseBody
	public void exportLyGysLeibieHuizong(HttpServletResponse response, String sort, String order, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> condition = new HashMap<String, Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());// 账号
		// 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));// 是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));// 是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);

		// 查询数据返回
		List<?> lists = lyGysLeibieHuizongService.lyGysLeibieHuizongDetail(condition, pager);
		if (lists != null) {
			// 要导出的数据集合
			List<Map<String, Object>> list = (List<Map<String, Object>>) lists.get(0);
			List<Map<String, Object>> footerList = (List<Map<String, Object>>) lists.get(1);
			List<Deliver> theDelivers = (List<Deliver>) lists.get(2);
			if (footerList != null && footerList.size() > 0) {
				footerList.get(0).put("TYP", "合计");
				list.addAll(footerList);
			}
			// 表头
			List<Map<String, String>> headers = new ArrayList<Map<String, String>>();
			Map<String, String> map1 = new HashMap<String, String>();
			map1.put("编码" + PublicExportExcel.SEPARATOR_COLONS + "TYP", "");
			headers.add(map1);

			Map<String, String> map2 = new HashMap<String, String>();
			map2.put("名称" + PublicExportExcel.SEPARATOR_COLONS + "TYPDES", "");
			headers.add(map2);

			Map<String, String> map3 = new HashMap<String, String>();
			map3.put("合计" + PublicExportExcel.SEPARATOR_COLONS + "AMTIN", "");
			headers.add(map3);

			// 拼装表头
			for (int i = 0, len = theDelivers.size(); i < len; i++) {
				Deliver d = theDelivers.get(i);
				String des = d.getDes();
				String code = d.getCode();
				Map<String, String> map4 = new HashMap<String, String>();
				map4.put(des + PublicExportExcel.SEPARATOR_COLONS + "D_" + code, "");
				headers.add(map4);
			}

			// 导出
			PublicExportExcel pee = new PublicExportExcel();
			pee.creatWorkBook(request, response, "领用供应商类别汇总报表", "领用供应商类别汇总报表", headers, list);
		}
	}
}