package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.WzFendianHuizongService;
@Controller
@RequestMapping("WzFendianHuizong")
public class WzFendianHuizongController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzFendianHuizongService wzFendianHuizongService;
	@Autowired
	private PositnService positnService;
	
	/********************************************物资分店汇总报表****************************************************/
	
	/**
	 * 跳转到报表html页面
	 * @return
	 */
	@RequestMapping("/toWzFendianHuizong")
	public ModelAndView toWzFendianHuizong(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_WZFENDIANHUIZONG );
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_WZFENDIANHUIZONG,modelMap);
	}
	
	/**
	 * 查询表头信息(根据选择的领用仓位 动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findWzFendianHuizongHeaders")
	@ResponseBody
	public List<Positn> findWzFendianHuizongHeaders(HttpSession session,String firm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(firm==null || "".equals(firm)){
			return positnService.findAllPositn(null);
		}else{
			String[] stringList = firm.split(",");
			List<Positn> positnList = new ArrayList<Positn>();
			for(String code:stringList){
				Positn posi = new Positn();
				posi.setCode(code);
				
				List<Positn> list = positnService.findAllPositn(posi);
				if(list!=null && list.size()>0){
					positnList.add(list.get(0));
				}
			}
			return positnList;
		}
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findWzFendianHuizong")
	@ResponseBody
	public Object findWzFendianHuizong(ModelMap modelMap, HttpSession session, String bysale,
			String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return wzFendianHuizongService.findWzFendianHuizong(findWzFendianHuizongHeaders(session,supplyAcct.getFirm()),condition,pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportWzFendianHuizong")
	@ResponseBody
	public void exportWzFendianHuizong(HttpServletResponse response,HttpServletRequest request,HttpSession session,
			String sort, String order, String bysale, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资分店汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_WZFENDIANHUIZONG);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		//exportExcel.creatWorkBook(response.getOutputStream(), ckRiqiHuizongService.findChkoutDateSum(condition,pager).getRows(), "", dictColumnsService.listDictColumnsByTable(dictColumns));
		wzFendianHuizongService.exportWzFendianHuizong(response.getOutputStream(), condition, findWzFendianHuizongHeaders(session,supplyAcct.getFirm()));

	}
	
}
