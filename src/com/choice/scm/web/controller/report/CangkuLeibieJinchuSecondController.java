package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.CangkuLeibieJinchuSecondService;

@Controller
@RequestMapping("CangkuLeibieJinchuSecond")
public class CangkuLeibieJinchuSecondController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CangkuLeibieJinchuSecondService CangkuLeibieJinchuSecondService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;

	/******************************************** 仓库类别进出表报表 ****************************************************/

	/**
	 * 查询表头信息
	 * 
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCangkuLeibieJinchuSecondHeaders")
	@ResponseBody
	public Object findCangkuLeibieJinchuSecondHeaders(HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> columns = new HashMap<String, Object>();
		columns.put("columns", grpTypService.findAllGrpTypA(session.getAttribute("ChoiceAcct").toString()));
		return columns;
	}

	/**
	 * 跳转到仓库类别进出表报表页面
	 * 
	 * @return
	 */
	@RequestMapping("/toCangkuLeibieJinchuSecond")
	public ModelAndView toCangkuLeibieJinchuSecond(ModelMap modelMap) {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_STOCKCATEGORYINOUT);

		// 初始化时间
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());

		return new ModelAndView(SupplyAcctConstants.CANGKU_LEIBIE_INOUTB, modelMap);
	}

	/**
	 * 查询仓库类别进出表报表内容
	 * 
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCangkuLeibieJinchuSecond")
	@ResponseBody
	public Object findCangkuLeibieJinchuSecond(ModelMap modelMap, HttpSession session, String page, String rows, SupplyAcct supplyAcct) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Map<String, Object> condition = new HashMap<String, Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());// 账号
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));// 是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));// 是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		condition.put("accountId", session.getAttribute("accountId").toString());
		return CangkuLeibieJinchuSecondService.findStockCategoryInOut(condition, pager);
	}

	/**
	 * 导出仓库类别进出表报表
	 * 
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCangkuLeibieJinchuSecond")
	@ResponseBody
	public void exportCangkuLeibieJinchuSecond(HttpServletResponse response, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String fileName = "仓库类别进出表";
		Map<String, Object> condition = new HashMap<String, Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());// 账号
																				// 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));// 是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));// 是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("accountId", session.getAttribute("accountId").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_STOCKCATEGORYINOUT);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
			// IE
			fileName = URLEncoder.encode(fileName, "UTF-8");
		} else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
			// firefox
			fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		} else {
			// other
			fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		}
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		CangkuLeibieJinchuSecondService.exportStockCategoryInOut(response.getOutputStream(), condition);
	}
}
