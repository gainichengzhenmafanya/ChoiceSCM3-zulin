package com.choice.scm.web.controller.report;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.report.FdYingliQingkuangService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("FdYingliQingkuang")
public class FdYingliQingkuangController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private FdYingliQingkuangService fdYingliQingkuangService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************分店盈利情况表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseFdYingliQingkuang")
	public ModelAndView toColChooseFdYingliQingkuang(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FDYINGLIQINGKUANG);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",ScmStringConstant.REPORT_NAME_FDYINGLIQINGKUANG );
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFdYingliQingkuangHeaders")
	@ResponseBody
	public Object getFdYingliQingkuang(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FDYINGLIQINGKUANG);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_FDYINGLIQINGKUANG));
		columns.put("frozenColumns",ScmStringConstant.BASICINFO_REPORT_FDYINGLIQINGKUANG_FROZEN );
		return columns;
	}
	
	/**
	 * 跳转到报表页面
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/toFdYingliQingkuang")
	public ModelAndView toFdYingliQingkuang(ModelMap modelMap, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != supplyAcct.getGrptypdes() && !"".equals(supplyAcct.getGrptypdes())) && ("".equals(supplyAcct.getGrptyp()) || null == supplyAcct.getGrptyp())){
			List<GrpTyp> list = grpTypService.findGrpTypByDes(supplyAcct.getGrptypdes());
			supplyAcct.setGrptyp(list.size()>0?list.get(0).getCode():"");
		}
		if((null != supplyAcct.getFirmdes() && !"".equals(supplyAcct.getFirmdes())) && ("".equals(supplyAcct.getFirmcode()) || null == supplyAcct.getFirmcode())){
			Positn firm = new Positn();
			firm.setDes(supplyAcct.getFirmdes());
			List<Positn> listf = positnService.findAllPositn(firm);
			supplyAcct.setFirmcode(listf.size()>0?listf.get(0).getCode():"");
		}
		if((null != supplyAcct.getPositndes() && !"".equals(supplyAcct.getPositndes())) && ("".equals(supplyAcct.getPositn()) || null == supplyAcct.getPositn())){
			Positn positn = new Positn();
			positn.setDes(supplyAcct.getPositndes());
			List<Positn> listp = positnService.findAllPositn(positn);
			supplyAcct.setPositn(listp.size()>0?listp.get(0).getCode():"");
		}
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_FDYINGLIQINGKUANG );
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_FDYINGLIQINGKUANG,modelMap);
	}
	
	/**
	 * 查询报表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findFdYingliQingkuang")
	@ResponseBody
	public Object findFdYingliQingkuang(ModelMap modelMap, HttpSession session, String bysale,
			String page, String rows, String sort, String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return fdYingliQingkuangService.findFdYingliQingkuang(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printFdYingliQingkuang")
	public ModelAndView printFdYingliQingkuang(ModelMap modelMap, Page pager, String bysale,
			HttpSession session, String type, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getEdat() != null)
			params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktag", String.valueOf(supplyAcct.getChktag()));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("bysale", bysale);
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",fdYingliQingkuangService.findFdYingliQingkuang(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    modelMap.put("actionMap", params);
	    parameters.put("report_name", "分店盈利情况表");
	    parameters.put("maded",DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkMingxiChaxun/printFdYingliQingkuang.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_FDYINGLIQINGKUANG,SupplyAcctConstants.REPORT_EXP_URL_FDYINGLIQINGKUANG);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportFdYingliQingkuang")
	@ResponseBody
	public void exportFdYingliQingkuang(HttpServletResponse response, String sort, String order,
			HttpServletRequest request, String bysale, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "分店盈利情况表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		// 分店是否使用库存系统
		supplyAcct.setYnkc(acctService.findAcctById(session.getAttribute("ChoiceAcct").toString()).getYnkc());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FDYINGLIQINGKUANG);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), fdYingliQingkuangService.findFdYingliQingkuang(condition,pager).getRows(),
				"分店盈利情况表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
