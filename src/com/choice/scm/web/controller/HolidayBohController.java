package com.choice.scm.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.HolidayBohConstant;
import com.choice.scm.domain.HolidayBoh;
import com.choice.scm.service.HolidayBohService;


@Controller
@RequestMapping(value = "holidayBoh")
public class HolidayBohController {
	
	@Autowired
	private HolidayBohService holidayBohService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 *查询所有的节假日
	 * @param modelMap
	 * @param holiday
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelMap modelMap, HolidayBoh holiday, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		List<HolidayBoh> holidays = holidayBohService.list(holiday,page);
		modelMap.put("holidayList", holidays);
		modelMap.put("pageobj", page);
		modelMap.put("holiday", holiday);
		return new ModelAndView(HolidayBohConstant.LIST_HOLIDAY, modelMap);
	}
	
	/**
	 *打开新增页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return new ModelAndView(HolidayBohConstant.SAVE_HOLIDAY, modelMap);
	}
	
	/**
	 * 保存节假日
	 * @param modelMap
	 * @param holiday
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView save(ModelMap modelMap, HolidayBoh holiday, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		holidayBohService.saveHoliday(holiday);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		Logs logd=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增节假日:"+holiday.getVholidayname(),session.getAttribute("ip").toString(),ProgramConstants.BOH);
		logsMapper.addLogs(logd);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 *打开批量新增节假日页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/generate")
	public ModelAndView generate(ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return new ModelAndView(HolidayBohConstant.GENERATE_HOLIDAY, modelMap);
	}
	
	/**
	 * 保存批量生成节假日
	 * @param modelMap
	 * @param holiday
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByGenerate")
	public ModelAndView saveByGenerate(ModelMap modelMap, HolidayBoh holiday, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		holidayBohService.saveGenerateHoliday(holiday);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		Logs logd=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"批量新增节假日:"+holiday.getVyear(),session.getAttribute("ip").toString(),ProgramConstants.BOH);
		logsMapper.addLogs(logd);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 打开修改页面
	 * @param modelMap
	 * @param holiday
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update (ModelMap modelMap, HolidayBoh holiday) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		modelMap.put("holiday", holidayBohService.findHolidayById((holiday.getPk_holidaydefine())));
		return new ModelAndView(HolidayBohConstant.UPDATE_HOLIDAY, modelMap);
	}
	
	/**
	 * 保存修改
	 * @param modelMap
	 * @param holiday
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap, HolidayBoh holiday, HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		holidayBohService.updateHoliday(holiday);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		Logs logd=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改节假日:"+holiday.getVholidayname(),session.getAttribute("ip").toString(),ProgramConstants.BOH);
		logsMapper.addLogs(logd);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 启用停用节假日
	 * @param modelMap
	 * @param pk_joiningruler
	 * @param holiday
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enable")
	public ModelAndView delete(ModelMap modelMap, HolidayBoh holiday, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		holidayBohService.deleteHoliday(holiday);
		String type = "启用";
		if(3==holiday.getEnablestate()){
			type="停用";
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		Logs logd=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,type+"节假日【"+holiday.getDholidaydate()+"】",session.getAttribute("ip").toString(),ProgramConstants.BOH);
		logsMapper.addLogs(logd);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	
}

