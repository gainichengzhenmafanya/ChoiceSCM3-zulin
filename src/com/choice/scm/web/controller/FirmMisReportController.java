package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmMisConstants;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.FirmMisReportService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportMis.CkLeibieHuizongMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 分店报表 
 * @author dell
 * 部门物资进出表、部门成本差异报表、分店物资进货价格比较、出库综合查询报表、分店菜品利润表、
 */
@Controller
@RequestMapping("firmMis")
public class FirmMisReportController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private FirmMisReportService firmMisReportService;
	@Autowired
	private CkLeibieHuizongMisService ckLeibieHuizongMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AcctService acctService;
	@Autowired
	AccountPositnService accountPositnService;
	
	
	/********************************************部门物资进出表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsDeptSupplyInOut")
	public ModelAndView toColumnsDeptSupplyInOut(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTSUPPLYINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", FirmMisConstants.REPORT_NAME_DEPTSUPPLYINOUT);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,FirmMisConstants.BASICINFO_REPORT_DEPTSUPPLYINOUT));
		
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeptSupplyInOutHeaders")
	@ResponseBody
	public Object findDeptSupplyInOutHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTSUPPLYINOUT);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_DEPTSUPPLYINOUT));
		
		return columns;
	}
	
	/**
	 * 跳转到部门物资进出表
	 * @return
	 */
	@RequestMapping("/toDeptSupplyInOut")
	public ModelAndView toDeptSupplyInOut(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			modelMap.put("firm",accountPositn.getPositn().getCode());
		}
		modelMap.put("reportName", FirmMisConstants.REPORT_NAME_DEPTSUPPLYINOUT);
		return new ModelAndView(FirmMisConstants.LIST_DEPTSUPPLYINOUT,modelMap);
	}
	/**
	 * 查询部门物资进出表
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeptSupplyInOut")
	@ResponseBody
	public Object findDeptSupplyInOut(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,String month,SupplyAcct supplyAcct,String firmDept,String lilun,String only2supply) throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		condition.put("firmDept", firmDept);
		condition.put("lilun", lilun);
		condition.put("only2supply", only2supply);
		condition.put("month", month);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return firmMisReportService.findDeptSupplyInOut(condition,pager);
	}
	/**
	 * 打印部门物资进出表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeptSupplyInOut")
	public ModelAndView printDeptSupplyInOut(ModelMap modelMap,Page pager,HttpSession session,SupplyAcct supplyAcct,String type)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getBdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisReportService.findDeptSupplyInOut(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "部门物资进出表");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printDeptSupplyInOut.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_DEPTSUPPLYINOUT,FirmMisConstants.REPORT_PRINT_URL_DEPTSUPPLYINOUT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出部门物资进出表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeptSupplyInOut")
	@ResponseBody
	public void exportDeptSupplyInOut(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "部门物资进出表";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTSUPPLYINOUT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisReportService.findDeptSupplyInOut(condition,pager).getRows(), "部门物资进出表", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	
	/********************************************部门成本差异报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsDeptCostDiff")
	public ModelAndView toColumnsDeptCostDiff(ModelMap modelMap,HttpSession session)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTCOSTDIFF);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", FirmMisConstants.REPORT_NAME_DEPTCOSTDIFF);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,FirmMisConstants.BASICINFO_REPORT_DEPTCOSTDIFF));
		
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeptCostDiffHeaders")
	@ResponseBody
	public Object findDeptCostDiffHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTCOSTDIFF);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_DEPTCOSTDIFF));
		columns.put("frozenColumns", FirmMisConstants.BASICINFO_REPORT_DEPTCOSTDIFF_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到部门成本差异
	 * @return
	 */
	@RequestMapping("/toDeptCostDiff")
	public ModelAndView toDeptCostDiff(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			modelMap.put("firm", accountPositn.getPositn().getCode());
		}
		modelMap.put("reportName", FirmMisConstants.REPORT_NAME_DEPTCOSTDIFF);
		return new ModelAndView(FirmMisConstants.LIST_DEPTCOSTDIFF,modelMap);
	}
	/**
	 * 查询部门成本差异
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeptCostDiff")
	@ResponseBody
	public Object findDeptCostDiff(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return firmMisReportService.findDeptCostDiff(condition,pager);
	}
	/**
	 * 打印部门成本差异
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeptCostDiff")
	public ModelAndView printDeptCostDiff(ModelMap modelMap,Page pager,HttpSession session,SupplyAcct supplyAcct,String type)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		if(supplyAcct.getBdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("firm",supplyAcct.getFirm());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("des", supplyAcct.getDes());
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisReportService.findDeptCostDiff(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "部门成本差异");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printDeptCostDiff.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_DEPTCOSTDIFF,FirmMisConstants.REPORT_PRINT_URL_DEPTCOSTDIFF);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 导出部门成本差异
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeptCostDiff")
	@ResponseBody
	public void exportDeptCostDiff(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "部门成本差异";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_DEPTCOSTDIFF);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisReportService.findDeptCostDiff(condition,pager).getRows(), "部门成本差异", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
	
	/********************************************出库类别汇总报表****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findChkoutmSynQueryHeaders")
	@ResponseBody
	public Object getChkoutmSynQuery(HttpSession session,String querytype){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		if(querytype.equals("1"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_DETAIL);
		if(querytype.equals("3"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_CATL);
		if(querytype.equals("2"))
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_SUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByTable(dictColumns));
		return columns;
	}
	/**
	 * 跳转到报表页面
	 * @return
	 */
	@RequestMapping("/toChkoutmSynQuery")
	public ModelAndView toChkoutmSynQuery(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "出库类别汇总");
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_CHKOUTMSYNQUERY_MIS,modelMap);
	}
	/**
	 * 查询 出库类别汇总 内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findChkoutmSynQuery")
	@ResponseBody
	public Object findChkoutmSynQuery(ModelMap modelMap,HttpSession session,String checby,String querytype,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		Map<String,Object> condition = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("querytype", querytype);
		condition.put("checby", checby);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ckLeibieHuizongMisService.findChkoutCategorySum(condition);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkoutmSynQuery")
	public ModelAndView printChkoutmSynQuery(ModelMap modelMap,Page pager,HttpSession session,String querytype,String type,SupplyAcct supplyAcct)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("supplyAcct", supplyAcct);
		condition.put("querytype", querytype);
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getChktyp() != null && !supplyAcct.getChktyp().equals(""))
			params.put("typ",supplyAcct.getChktyp());
		if(supplyAcct.getPositn() != null && !supplyAcct.getPositn().equals(""))
			params.put("positn", supplyAcct.getPositn());
		if(supplyAcct.getDelivercode() != null && !supplyAcct.getDelivercode().equals(""))
			params.put("deliverdes",supplyAcct.getDelivercode());
		if(supplyAcct.getGrp() != null && !supplyAcct.getGrp().equals(""))
			params.put("grp",supplyAcct.getGrp());
		if(supplyAcct.getGrptyp() != null && !supplyAcct.getGrptyp().equals(""))
			params.put("grptyp",supplyAcct.getGrptyp());
		if(supplyAcct.getTyp() != null && !supplyAcct.getTyp().equals(""))
			params.put("typ", supplyAcct.getTyp());
		if(supplyAcct.getBdat() != null)
			params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		if(supplyAcct.getEdat() != null)
			params.put("edat",  DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("querytype", querytype);
//		modelMap.put("List",ckLeibieHuizongMisService.findChkoutCategorySum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    modelMap.put("actionMap", params);
	    parameters.put("report_name", "出库类别汇总");
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CkZongheChaxun/printChkoutmSynQuery.do");//传入回调路径
	 	Map<String,String> rs=null;
	 	if(querytype.equals("1"))
	 		rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_DETAIL,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_DETAIL);//判断跳转路径
		if(querytype.equals("3"))
			rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_CATL,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_CATL);//判断跳转路径
		if(querytype.equals("2"))
			rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_SUM,FirmMisConstants.REPORT_PRINT_URL_CHKOUTMSYNQUERY_SUM);//判断跳转路径
	 	
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
//	@RequestMapping("/exportChkoutmSynQuery")
//	@ResponseBody
//	public void exportChkoutmSynQuery(HttpServletResponse response,String sort,String order,String querytype,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String fileName = "出库类别汇总";
//		Map<String,Object> condition = new HashMap<String,Object>();
//		String accountId=session.getAttribute("accountId").toString();
//		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
//		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
//		if(null!=accountPositn && null!=accountPositn.getPositn()){
//			supplyAcct.setPositn(accountPositn.getPositn().getCode());
//		}
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
//		condition.put("supplyAcct", supplyAcct);
//		condition.put("sort", sort);
//		condition.put("order", order);
//		condition.put("querytype", querytype);
//		pager.setPageSize(Integer.MAX_VALUE);
//		if(querytype.equals("1"))
//			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_DETAIL);
//		if(querytype.equals("3"))
//			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_CATL);
//		if(querytype.equals("2"))
//			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_CHKINMSYNQUERY_SUM);
//		response.setContentType("application/msexcel; charset=UTF-8");
//		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
//		    //IE  
//		    fileName = URLEncoder.encode(fileName, "UTF-8");              
//		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
//		    //firefox  
//		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
//		}else{                
//		    // other          
//		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
//		}   
//		response.setHeader("Content-disposition", "attachment; filename="  
//                + fileName + ".xls");
//		exportExcelMap.creatWorkBook(response.getOutputStream(), ckLeibieHuizongMisService.findChkoutCategorySum(condition,pager).getRows(), "出库汇总查询", dictColumnsService.listDictColumnsByTable(dictColumns));
//		
//	}
	@RequestMapping("/exportChkoutCategorySum")
	@ResponseBody
	public void exportChkoutCategorySum(HttpServletResponse response, String sort, String order, 
			HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "出库类别汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		supplyAcct.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		supplyAcct.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//是否启用供应商权限wjf
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//wjf 解决查询门店时报错
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		condition.put("supplyAcct", supplyAcct);
		dictColumns.setTableName("出库类别汇总");
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ckLeibieHuizongMisService.exportChkoutCategorySum(response.getOutputStream(), condition);
	}
	/********************************************分店菜品利润表****************************************************/

	/**
	 * 跳转到列选择
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseFirmFoodProfit")
	public ModelAndView toColChooseFirmFoodProfit(ModelMap modelMap,HttpSession session)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_FIRMFOODPROFIT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", FirmMisConstants.REPORT_NAME_FIRMFOODPROFIT);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,FirmMisConstants.BASICINFO_REPORT_FIRMFOODPROFIT));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(FirmMisConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFirmFoodProfitHeaders")
	@ResponseBody
	public Object getFirmFoodProfitHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_FIRMFOODPROFIT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, FirmMisConstants.BASICINFO_REPORT_FIRMFOODPROFIT));
		return columns;
	}
	
	/**
	 * 跳转到分店菜品利润表页面
	 * @return
	 */
	@RequestMapping("/toFirmFoodProfit")
	public ModelAndView toFirmFoodProfit(ModelMap modelMap,String checkMis){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", FirmMisConstants.REPORT_NAME_FIRMFOODPROFIT);
		if(null!=checkMis && !"".equals(checkMis)){
			modelMap.put("checkMis", checkMis);
			return new ModelAndView(FirmMisConstants.LIST_FIRMFOODPROFIT,modelMap);
		}else{
			return new ModelAndView(FirmMisConstants.LIST_FOODPROFIT,modelMap);
		}
	}
	/**
	 * 查询分店菜品利润表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findFirmFoodProfit")
	@ResponseBody
	public Object findFirmFoodProfit(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,
								String exp0,String onlycbk,String doublefood,String foodType,String food,String payment) throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		condition.put("supplyAcct", supplyAcct);
		condition.put("food", food);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("exp0", exp0);
		condition.put("onlycbk", onlycbk);
		condition.put("doublefood", doublefood);
		condition.put("foodType", foodType);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEM", "costitem");
		}else{
			condition.put("COSTITEM", "firmcostitem");
		}
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		//firmMisReportService.findFirmFoodProfit(condition, pager).getFooter().get(0).get("INFASHENGE");
		return firmMisReportService.findFirmFoodProfit(condition, pager);
	}
	
	/**
	 * 查看成本卡
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/findCostCard")
	public ModelAndView findCostCard(ModelMap modelMap, String itcode, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("costdtlmList", firmMisReportService.findCostdtlm(map));
		modelMap.put("costdtlList", firmMisReportService.findCostdtl(map));
		return new ModelAndView(FirmMisConstants.LIST_COSTCARD,modelMap);
	}	

	/**
	 * 查看菜品成本组成物资明细
	 * @param modelMap
	 * @param itcode
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCostDetail")
	public ModelAndView findCostDetail(ModelMap modelMap, String itcode, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("bdat", bdat);
		map.put("edat", edat);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEMSPCODE", "COSTITEMSPCODE");
		}else{
			map.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
		}
		modelMap.put("costDetailList", firmMisReportService.findCostDetail(map));
		return new ModelAndView(FirmMisConstants.LIST_COSTDETAIL,modelMap);
	}	
	/**
	 * 菜品销售成本利润走势分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findSaleCostProfitTrends")
	public ModelAndView findSaleCostProfitTrends(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("saleCostList", firmMisReportService.findSaleCostProfitTrends(map));
		return new ModelAndView(FirmMisConstants.LIST_SALECOSTPROFITTRENDS,modelMap);
	}
	
	/**
	 * 菜品销售成本利润走势分析--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlForSaleCostProfitTrends")
	public void findXmlForSaleCostProfitTrends(HttpServletResponse response, HttpSession session, String itcode, String onlycbk, Date bdat, Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		firmMisReportService.findXmlForSaleCostProfitTrends(response, map).output();
	}
	
	/**
	 * 菜品成本区间分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findCostInterval")
	public ModelAndView findCostInterval(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("costIntervalList", firmMisReportService.findCostIntervalList(map));
		return new ModelAndView(FirmMisConstants.LIST_COSTINTERVAL,modelMap);
	}
	
	/**
	 * 菜品成本区间分析--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlCostInterval")
	public void findXmlCostInterval(HttpServletResponse response, HttpSession session, String itcode, String onlycbk, Date bdat, Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		firmMisReportService.findXmlCostInterval(response, map).output();
	}
	
	/**
	 * 菜品毛利率区间分析
	 * @param modelMap
	 * @param itcode
	 * @param onlycbk
	 * @param bdat
	 * @param edat
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findMaolilvInterval")
	public ModelAndView findMaolilvInterval(ModelMap modelMap, String itcode, String onlycbk, Date bdat, Date edat, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", bdat);
		map.put("edat", edat);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		modelMap.put("itcode", itcode);
		modelMap.put("onlycbk", onlycbk);
		modelMap.put("bdat", DateFormat.getStringByDate(bdat, "yyyy-MM-dd"));
		modelMap.put("edat", DateFormat.getStringByDate(edat, "yyyy-MM-dd"));
		modelMap.put("maolilvIntervalList", firmMisReportService.findMaolilvIntervalList(map));
		return new ModelAndView(FirmMisConstants.LIST_MAOLILVINTERVAL,modelMap);
	}	
	
	/**
	 * 菜品毛利率区间--图表
	 * @param response
	 * @param session
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/findXmlMaolilvInterval")
	public void findXmlMaolilvInterval(HttpServletResponse response,HttpSession session,String itcode,String onlycbk,Date bdat,Date edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("itcode", itcode);
		map.put("onlycbk", onlycbk);
		map.put("bdat", DateFormat.formatDate(bdat, "yyyy-MM-dd"));
		map.put("edat", DateFormat.formatDate(edat, "yyyy-MM-dd"));
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			map.put("COSTITEM", "COSTITEM");
		}else{
			map.put("COSTITEM", "firmCOSTITEM");
		}
		firmMisReportService.findXmlMaolilvInterval(response, map).output();
	}
	
	/**
	 * 打印分店菜品利润表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printFirmFoodProfit")
	public ModelAndView printFirmFoodProfit(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grp",supplyAcct.getGrp());
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
			params.put("positn",accountPositn.getPositn().getCode());
		}
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEM", "costitem");
		}else{
			condition.put("COSTITEM", "firmcostitem");
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",firmMisReportService.findFirmFoodProfit(condition, pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "分店项目销售利润分析");
	    parameters.put("bdat", supplyAcct.getBdat());
	    parameters.put("edat", supplyAcct.getEdat());
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printFirmFoodProfit.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,FirmMisConstants.REPORT_PRINT_URL_FIRMFOODPROFIT,FirmMisConstants.REPORT_PRINT_URL_FIRMFOODPROFIT);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出分店菜品利润表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmFoodProfit")
	@ResponseBody
	public void exportFirmFoodProfit(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "分店菜品利润表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		conditions.put("supplyAcct", supplyAcct);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			conditions.put("COSTITEM", "costitem");
		}else{
			conditions.put("COSTITEM", "firmcostitem");
		}
		dictColumns.setTableName(FirmMisConstants.REPORT_NAME_FIRMFOODPROFIT);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), firmMisReportService.findFirmFoodProfit(conditions, pager).getRows(), "分店菜品利润表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
