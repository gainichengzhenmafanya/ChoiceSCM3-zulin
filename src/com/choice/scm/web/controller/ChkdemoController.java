package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkdemoConstants;
import com.choice.scm.domain.Chkdemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.ChkdemoService;

@Controller
@RequestMapping(value = "chkdemo")
/**
 * 盘点模板
 * @author lehui
 *
 */
public class ChkdemoController {

	@Autowired
	private ChkdemoService chkdemoService;
	
	/**
	 * 查询所有的盘点
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkdemo(ModelMap modelMap,HttpSession session,Page page,Chkdemo chkdemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//盘点模板页面初始化
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkdemo", chkdemo);
		modelMap.put("pageobj", page);
		modelMap.put("chkdemoList", chkdemoService.findAllTitle(chkdemo));
		return new ModelAndView(ChkdemoConstants.TABLE_CHKDEMO,modelMap);
	}

	/**
	 * 盘点模板批量添加盘点
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkdemo")
	public ModelAndView addChkdemo(ModelMap modelMap,HttpSession session,Page page,Chkdemo chkdemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//盘点模板批量添加时候的初始化页面
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkdemoService.findAllTitle(chkdemo));
		modelMap.put("chkdemoList", chkdemoService.findChkdemo(chkdemo,page));
		return new ModelAndView(ChkdemoConstants.ADD_CHKDEMO,modelMap);
	}

	/**
	 * 保存新增的盘点信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addNewChkdemo")
	public ModelAndView addNewChkdemo(ModelMap modelMap,HttpSession session,Supply supply,String title2,Chkdemo chkdemo) throws Exception
	{	
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存新添加的盘点模板
		chkdemo.setTitle(title2);
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkdemo.setSupply(supply);
		//获取盘点模板最大的序号
		String id = chkdemoService.findMaxId(chkdemo);
		if(null!=id && !"".equals(id)){
			chkdemo.setId(Integer.parseInt(id)+1);
		}else{
			chkdemo.setId(1);
		}
		chkdemoService.saveNewChkdemo(chkdemo);
		if(null==id){
			return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
		}else{
			modelMap.put("chkdemoList", chkdemoService.findChkdemo(chkdemo,new Page()));
			modelMap.put("pageobj", new Page());
			return new ModelAndView(ChkdemoConstants.LIST_CHKDEMO,modelMap);
		}
	}
	
	/**
	 * 根据title进行查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkdemo")
	public ModelAndView findChkdemo(ModelMap modelMap,HttpSession session,Page page,String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存用户输入的参数
		Chkdemo chkdemo=new Chkdemo();
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=title && !"".equals(title)){
			chkdemo.setTitle(title);
		}
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkdemoService.findAllTitle(chkdemo));
		modelMap.put("title", title);
		//根据输入的title参数查询的结果集返回到页面
		modelMap.put("chkdemoList", chkdemoService.findChkdemo(chkdemo,page));
		return new ModelAndView(ChkdemoConstants.LIST_CHKDEMO,modelMap);
	}
	/**
	 * 盘点模板添加盘点查询
	 * @param modelMap
	 * @param page
	 * @param chkdemo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkdemoByAddChkdemo")
	public ModelAndView findChkdemoByAddChkdemo(ModelMap modelMap,HttpSession session,Page page,Chkdemo chkdemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//盘点模板批量添加时候的页面
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkdemoService.findAllTitle(chkdemo));
		modelMap.put("title", chkdemo.getTitle());
		modelMap.put("chkdemoList", chkdemoService.findChkdemo(chkdemo,page));
		return new ModelAndView(ChkdemoConstants.ADD_CHKDEMO,modelMap);
	}

	/**
	 * 分页查询
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByPage")
	public ModelAndView findChkByPage(ModelMap modelMap,Page page,String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		Chkdemo chkdemo = new Chkdemo();
		chkdemo.setTitle(title);
		modelMap.put("title", title);
		modelMap.put("chkdemoList", chkdemoService.findChkdemo(chkdemo,page));
		return new ModelAndView(ChkdemoConstants.LIST_CHKDEMO,modelMap);
	}

	/**
	 * 盘点模板添加盘点分页查询
	 * @param modelMap
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByAddChkdemoPage")
	public ModelAndView findChkByAddChkdemoPage(ModelMap modelMap,HttpSession session,Page page,Chkdemo chkdemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//批量添加页面上的分页
		chkdemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkdemoService.findAllTitle(chkdemo));
		modelMap.put("chkdemoList", chkdemoService.findChkdemo(null,page));
		return new ModelAndView(ChkdemoConstants.ADD_CHKDEMO,modelMap);
	}

	/**
	 * 删除盘点信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public ModelAndView deleteChkdemo(ModelMap modelMap,HttpSession session,Chkdemo chkdemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkdemoService.deleteChkdemo(chkdemo,session.getAttribute("ChoiceAcct").toString());
		return null;
	}
}
