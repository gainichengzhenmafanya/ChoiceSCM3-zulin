package com.choice.scm.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkstoDemomConstants;
import com.choice.scm.domain.ChkstoDemoFirm;
import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemod_x;
import com.choice.scm.domain.ChkstoDemom;
import com.choice.scm.domain.ChkstoDemom_x;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.ExecSql;
import com.choice.scm.service.ChkstoDemodService;
import com.choice.scm.service.ChkstoDemomService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;

/**
 * 报货单单据模板
 * @author 孙胜彬
 */
@Controller
@RequestMapping(value="chkstodemom")
public class ChkstoDemomController {
	@Autowired
	private ChkstoDemomService chkstoDemomService;
	@Autowired
	private ChkstoDemodService chkstoDemodService;
	@Autowired
	private PositnService positnService;
	@Autowired
	ExecSql execSql;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 * 查询报货单单据模板
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemom")
	public ModelAndView listChkstoDemom(ModelMap modelMap,ChkstoDemom chkstoDemom,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemom> listChkstoDemom=chkstoDemomService.listChkstoDemom(chkstoDemom, page);
		modelMap.put("listChkstoDemom", listChkstoDemom);
		modelMap.put("chkstoDemom", chkstoDemom);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomConstants.LIST_CHKSTODEMOM);
	}
	
	/**
	 * 查询报货单单据模板子表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemod")
	public ModelAndView listChkstoDemod(ModelMap modelMap,ChkstoDemod chkstoDemod,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemod> listChkstoDemod=chkstoDemodService.listChkstoDemodd(chkstoDemod);
		modelMap.put("listChkstoDemod", listChkstoDemod);
		modelMap.put("chkstodemono", chkstoDemod.getChkstodemono());
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomConstants.LIST_CHKSTODEMOD);
	}
	
	/**
	 * 新增报货单单据模板页面
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("addChkstoDemod")
	public ModelAndView addChkstoDemod(ModelMap modelMap,String chkstodemono,HttpSession session,String tempTyp) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		modelMap.put("chkstodemono", execSql.getMaxsequences("CHKSTODEMOM"));
		modelMap.put("accountName", accountName);
		modelMap.put("sta", "add");
		modelMap.put("tempTyp", tempTyp);
		return new ModelAndView(ChkstoDemomConstants.ADD_ChkstoDemod,modelMap);
	}
	
	/**
	 * 新增报货单单据模板
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("saveChkstoDemom")
	@ResponseBody
	public String saveChkstoDemom(ChkstoDemom chkstoDemom,ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sta", "add");
		return chkstoDemomService.saveChkstoDemoms(chkstoDemom,session);
	}
	
	/**
	 * 删除报货单单据模板
	 * @param ids
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("deleteChkstoDemom")
	public ModelAndView deleteChkstoDemom(String ids) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemomService.deleteChkstoDemom(ids);
		chkstoDemodService.deleteChkstoDemod(ids);
		chkstoDemomService.deleteChkstoDemoFirm(ids);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 * 修改报货单单据页面
	 * @param modelMap
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updChkstoDemom")
	public ModelAndView updChkstoDemom(ModelMap modelMap,ChkstoDemom chkstoDemom,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemom> listChkstoDemom=chkstoDemomService.listChkstoDemom(chkstoDemom, page);
		ChkstoDemod chkstoDemod=new ChkstoDemod();
		chkstoDemod.setChkstodemono(chkstoDemom.getChkstodemono());
		List<ChkstoDemod> listChkstoDemod=chkstoDemodService.listChkstoDemod(chkstoDemod, page);
		modelMap.put("chkstoDemom", listChkstoDemom.get(0));
		modelMap.put("listChkstoDemod", listChkstoDemod);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstoDemomConstants.UPD_CHKSTODEMOD,modelMap);
	}
	
	/**
	 * 修改报货单单据模
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updateChkstoDemom")
	@ResponseBody
	public int updateChkstoDemom(ModelMap modelMap,ChkstoDemom chkstoDemom,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstoDemomService.updChkstoDemom(chkstoDemom);
	}
	
	/**
	 * 查询适用分店
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("listChkstoDemoFirm")
	public ModelAndView listChkstoDemoFirm(ModelMap modelMap,ChkstoDemoFirm chkstoDemoFirm,String chkstodemo,String accountId,Positn positn,
			Page page,String firmCode,String single,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//查询区域
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		
		//查询已经关联的仓位
		chkstoDemoFirm.setChksto(chkstodemo);
		List<ChkstoDemoFirm> list = chkstoDemomService.listChkstoDemoFirm(chkstoDemoFirm);
		String code = "";
		for(ChkstoDemoFirm scheduleStore:list){
			code +=scheduleStore.getFirm()+",";
		}
		positn.setCode(code);
		modelMap.put("listPositn1", positnService.selectPositnNew(positn));
		
		//根据页面传的仓位类型 查询所有仓位
		positn.setAccountId(session.getAttribute("accountId").toString());
		String locale = session.getAttribute("locale").toString();//语言类型
		positn.setLocale(locale);
		//清空positn中的code值，查询所有仓位
		positn.setCode("");
		modelMap.put("listPositn", positnService.findPositnSuperNOPage(positn));
		
		//获取仓位类型
		CodeDes codedes = new CodeDes();
		codedes.setTyp("12");
		codedes.setLocale(locale);
		
		if(positn.getTypn() != null && !"".equals(positn.getTypn())){
			codedes.setCode(positn.getTypn());
		}
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(codedes));
		
		modelMap.put("chkstodemo", chkstodemo);
		return new ModelAndView(ChkstoDemomConstants.LIST_listChkstoDemoFirm,modelMap);
	}
	
	/**
	 * 新增适用分店
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("saveChkstoDemoFirm")
	public ModelAndView saveChkstoDemoFirm(ModelMap modelMap,ChkstoDemoFirm chkstoDemoFirm,String chkstodemonoa,String firm) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemomService.saveChkstoDemoFirm(chkstoDemoFirm,chkstodemonoa);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 报货模板调用
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstoDemo")
	public ModelAndView addChkstoDemo(ModelMap modelMap, HttpSession session, Page page, ChkstoDemom chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<ChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		ChkstoDemod chkstoDemod = new ChkstoDemod();
		if(listChkstoDemo.size() > 0) {//报货单填制选模板时有点问题 2014.11.10 wjf
			chkstoDemod.setChkstodemono(listChkstoDemo.get(0).getChkstodemono());
		}else{
			chkstoDemod.setChkstodemono(0);
//			StringBuffer buffer = new StringBuffer();
//			buffer.append("(");
//			for(ChkstoDemom chkstoDemom:listChkstoDemo){
//				buffer.append(chkstoDemom.getChkstodemono()+",");
//			}
//			buffer.append("0)");
//			chkstoDemod.setChkstodemonos(buffer.toString());
		}
		modelMap.put("firm", chkstodemo.getFirm());//wjf
		modelMap.put("title", chkstodemo.getTitle());
		modelMap.put("chkstodemoList", chkstoDemodService.listChkstoDemod(chkstoDemod, page));
		modelMap.put("pageobj", page);
		//最后查标题栏
		chkstodemo.setTitle(null);//不要查特定的，查所有此门店适用的
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo));//改为查带适用分店权限的2014.11.18wjf
		return new ModelAndView(ChkstoDemomConstants.ADD_CHKSTO_DEMO_NEW, modelMap);
	}
	
	/**
	 * 新增调度  来自计划
	 */
	@RequestMapping(value = "/enterUpdate")
	@ResponseBody
	public Object enterUpdate(ModelMap modelMap, Chkstodemo chkstodemo)throws Exception {
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("chkstodemoList", chkstodemo.getChkstodemoList());
		JSONObject rs = JSONObject.fromObject(result);
		return rs.toString();
	}
	
	//*********************虚拟物料模板begin*******************************************
	/**
	 * 查询报货单单据模板--虚拟物料
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemom_x")
	public ModelAndView listChkstoDemom_x(ModelMap modelMap,ChkstoDemom_x chkstoDemom_x,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemom_x> listChkstoDemom_x=chkstoDemomService.listChkstoDemom_x(chkstoDemom_x, page);
		modelMap.put("listChkstoDemom_x", listChkstoDemom_x);
		modelMap.put("title", chkstoDemom_x.getTitle());
		modelMap.put("chkstoDemom_x", chkstoDemom_x);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomConstants.LIST_CHKSTODEMOM_X);
	}
	
	/**
	 * 查询报货单单据模板子表--虚拟物料
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("listChkstoDemod_x")
	public ModelAndView listChkstoDemod_x(ModelMap modelMap,String chkstodemono,ChkstoDemod_x chkstoDemod_x,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemod_x> listChkstoDemod_x=chkstoDemodService.listChkstoDemodd_x(chkstoDemod_x);
		System.out.println(chkstoDemod_x.getChkstodemono());
		modelMap.put("listChkstoDemod_x", listChkstoDemod_x);
		modelMap.put("chkstodemono", chkstoDemod_x.getChkstodemono());
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomConstants.LIST_CHKSTODEMOD_X);
	}
	
	/**
	 * 新增报货单单据模板页面--虚拟物料(跳转到添加的页面)
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("addChkstoDemod_x")
	public ModelAndView addChkstoDemod_x(ModelMap modelMap,String chkstodemono,HttpSession session,ChkstoDemod_x chkstoDemod_x) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		modelMap.put("chkstodemono", execSql.getMaxsequences("CHKSTODEMOM"));
		modelMap.put("accountName", accountName);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstoDemomConstants.ADD_CHKSTODEMOD_X,modelMap);
	}
	
	/**
	 * 新增报货单单据模板--虚拟物料保存
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("saveChkstoDemom_x")
	@ResponseBody
	public String saveChkstoDemom_x(ChkstoDemom_x chkstoDemom_x,ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("sta", "add");
		return chkstoDemomService.saveChkstoDemoms_x(chkstoDemom_x,session);
	}
	
	/**
	 * 修改报货单单据页面--虚拟物料
	 * @param modelMap
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updChkstoDemom_x")
	public ModelAndView updChkstoDemom_x(ModelMap modelMap,ChkstoDemom_x chkstoDemom_x,Page page) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<ChkstoDemom_x> listChkstoDemom_x=chkstoDemomService.listChkstoDemom_x(chkstoDemom_x, page);
		ChkstoDemod_x chkstoDemod_x=new ChkstoDemod_x();
		chkstoDemod_x.setChkstodemono(chkstoDemom_x.getChkstodemono());
		List<ChkstoDemod_x> listChkstoDemod_x=chkstoDemodService.listChkstoDemod_x(chkstoDemod_x, page);
		modelMap.put("chkstoDemom_x", listChkstoDemom_x.get(0));
		modelMap.put("listChkstoDemod_x", listChkstoDemod_x);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstoDemomConstants.UPD_CHKSTODEMOD_X,modelMap);
	}
	
	/**
	 * 修改报货单单据模--虚拟物料
	 * @param chkstoDemom
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updateChkstoDemom_x")
	@ResponseBody
	public int updateChkstoDemom_x(ModelMap modelMap,ChkstoDemom_x chkstoDemom_x,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemom_x.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstoDemomService.updChkstoDemom_x(chkstoDemom_x);
	}
	
	/**
	 * 删除报货单单据模板--虚拟物料
	 * @param ids
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("deleteChkstoDemom_x")
	public ModelAndView deleteChkstoDemom_x(String ids) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemomService.deleteChkstoDemom_x(ids);
		chkstoDemodService.deleteChkstoDemod_x(ids);
		chkstoDemomService.deleteChkstoDemoFirm_x(ids);
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	/**
	 * 查询适用分店--虚拟物料
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("listChkstoDemoFirm_x")
	public ModelAndView listChkstoDemoFirm_x(ModelMap modelMap,ChkstoDemoFirm chkstoDemoFirm,String chkstodemo,String accountId,Positn positn,Page page,String firmCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstoDemoFirm.setChksto(chkstodemo);
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("chkstoDemoFirm", chkstoDemomService.listChkstoDemoFirm(chkstoDemoFirm, page));
		modelMap.put("accountId", accountId);
		modelMap.put("positn", positn);
		modelMap.put("firmCode", firmCode);
		modelMap.put("chkstodemo", chkstodemo);
		return new ModelAndView(ChkstoDemomConstants.LIST_listChkstoDemoFirm,modelMap);
	}

	/**
	 * 报货模板调用
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstoDemo_x")
	public ModelAndView addChkstoDemo_x(ModelMap modelMap, HttpSession session, Page page, ChkstoDemom chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listTitle", chkstoDemomService.listChkstoDemom(null));
		List<ChkstoDemom> listChkstoDemo = chkstoDemomService.listChkstoDemom(chkstodemo);
		modelMap.put("listChkstodemo", listChkstoDemo);
		ChkstoDemod chkstoDemod = new ChkstoDemod();
		if(listChkstoDemo.size() == 1) {
			chkstoDemod.setChkstodemonos(listChkstoDemo.get(0).getChkstodemono()+"");
		}else{
			StringBuffer buffer = new StringBuffer();
			buffer.append("(");
			for(ChkstoDemom chkstoDemom:listChkstoDemo){
				buffer.append("'"+chkstoDemom.getChkstodemono()+"',");
			}
			buffer.append("'')");
			chkstoDemod.setChkstodemonos(buffer.toString());
		}
		modelMap.put("title", chkstodemo.getTitle());
		modelMap.put("chkstodemoList", chkstoDemodService.listChkstoDemod(chkstoDemod, page));
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstoDemomConstants.ADD_CHKSTO_DEMO_X, modelMap);
	}
	//*********************虚拟物料模板begin*******************************************
	
	
	/*************************WAP 调用后台******************************/
	/**
	 * 报货模板查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstoDemoWAP")
	@ResponseBody
	public Object addChkstoDemoWAP(ChkstoDemom chkstodemo,Page page,String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("listTitle", chkstoDemomService.listChkstoDemom(chkstodemo, page));//改为查带适用分店权限的2014.11.18wjf
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 报货模板查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getChkstoDemoWAP")
	@ResponseBody
	public Object getChkstoDemoWAP(ChkstoDemom chkstodemo,String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		ChkstoDemod chkstoDemod = new ChkstoDemod();
		chkstoDemod.setChkstodemono(chkstodemo.getChkstodemono());
		map.put("chkstodemoList", chkstoDemodService.listChkstoDemod(chkstoDemod, null));
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
