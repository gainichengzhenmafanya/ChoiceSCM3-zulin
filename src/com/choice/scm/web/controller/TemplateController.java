package com.choice.scm.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;


import com.choice.scm.constants.TemplateConstants;
import com.choice.scm.domain.Template;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.TemplateService;
/**
 * 全局模版设置
 * @author ghc
 *
 */
@Controller
@RequestMapping(value = "template")
public class TemplateController {

	@Autowired
	private TemplateService templateService;
	@Autowired
	PositnService positnService;
	@Autowired
	SupplyMapper supplyMapper;
	
	/**
	 * @throws CRUDException 
	 * 导入模版
	 * @param modelMap
	 * @return modelMap
	 * @throws 
	 */
	@RequestMapping("/importTemplate")
	public ModelAndView importSupply(ModelMap modelMap,Template template) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Template> list  = templateService.getTemplate(template);
		modelMap.put("listTemplate", list);
		return new ModelAndView(TemplateConstants.IMPORT_TEMPLATE,modelMap);
	}
	
	/**
	 * 
	 * @param template
	 * @return List<Template> 
	 * @throws CRUDException
	 * 查询所有模版
	 */
	@RequestMapping("/list")
	@ResponseBody
	public List<Template> getTemplate(Template template) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return templateService.getTemplate(template);
	}
	
	
	
	/**
	 * 上传jrxml
	 * @param request  response  modelMap
	 * @return ModelAndView
	 * @throws 
	 */
	@RequestMapping(value = "/loadJrxml", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,Template template) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String codeIn = request.getParameter("codeIn");
		template.setCode(codeIn);
		
		String realName = templateService.getTemplate(template).get(0).getRealName();
		String realFilePath = templateService.uploadTemplate(request, response,realName);  //将文件上传到指定文件夹
		
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(TemplateConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	@ResponseBody
	public List<String> importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		System.out.println("上传文件路径： "+ realFilePath);
		List<String> listError=templateService.check(realFilePath, session.getAttribute("accountId").toString());
		return listError;
	}
	
	
	/**
	 * 下载标准模板信息
	 * @throws CRUDException 
	 */
	@RequestMapping(value = "/filePathUseStandard")
	public void filePathUseStandard(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,Template template) throws  Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String codeIn = request.getParameter("codeOutStandard");
		template.setCode(codeIn);
		String realName = templateService.getTemplate(template).get(0).getRealName();
		templateService.downloadTemplateStandard(response, request,realName);
	}
	
	
	/**
	 * 下载个性化模板信息
	 */
	@RequestMapping(value = "/filePathUseIndividualization")
	@ResponseBody
	public void filePathUseIndividualization(HttpServletResponse response,HttpServletRequest request,Template template) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Object pk_group = UserSpace.getSession().getAttribute("pk_group");
		String codeIn = request.getParameter("codeOutIndividualization");
		template.setCode(codeIn);
		String realName = pk_group+templateService.getTemplate(template).get(0).getRealName();
		templateService.downloadTemplateIndividualization(response, request,realName);
	}
	
	/**
	 * 下载个性化模板信息
	 */
	@RequestMapping(value = "/fileExitsOrNot")
	@ResponseBody
	public String fileExitsOrNot(HttpServletResponse response,HttpServletRequest request,Template template,String codeOutIndividualization) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Object pk_group = UserSpace.getSession().getAttribute("pk_group");
		template.setCode(codeOutIndividualization);
		String realName = pk_group+templateService.getTemplate(template).get(0).getRealName();
		return templateService.fileExitsOrNot(response, request, realName);
	}
	
	
	/**
	 * 读取操作文档详细列表
	 * @param response
	 * @param request
	 * @param modelMap
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/showHelpFiles")
	public ModelAndView showHelpFiles(HttpServletResponse response,HttpServletRequest request,ModelMap modelMap) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("files", templateService.showHelpFiles(response, request));
		return new ModelAndView(LoginConstants.HELP_FILES,modelMap);
	}
	
}
