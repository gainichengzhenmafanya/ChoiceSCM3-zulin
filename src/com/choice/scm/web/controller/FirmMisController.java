package com.choice.scm.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.ChkoutConstants;
import com.choice.scm.constants.ChkstodemoConstants;
import com.choice.scm.constants.ChkstomConstants;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Holiday;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.service.ChkoutService;
import com.choice.scm.service.ChkstodService;
import com.choice.scm.service.ChkstodemoService;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.DisService;
import com.choice.scm.service.HolidaysService;
import com.choice.scm.service.LinesFirmService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 采购验货模块：报货单、到货验收等； 库存管理模块：入库单、出库单等；配送中心到货验收;
 * 
 * @author csb,css
 * 
 */
@Controller
@RequestMapping(value = "firmMis")
public class FirmMisController {

	@Autowired
	private ChkstodemoService chkstodemoService;
	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	private ChkstodService chkstodService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private DisService disService;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private ChkoutService chkoutService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private Chkoutm chkoutm;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private LinesFirmService linesFirmService;
	@Autowired
	private HolidaysService holidayService;
	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private LogsMapper logsMapper;

	private final static int PAGE_SIZE = 25;// ajax每次加载的数据条数

	/******************************************************** 申购单模板start  已经没用了  *************************************************/
	/**
	 * 分店MIS申购模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chkstodemoTable")
	public ModelAndView findAllChkstodemo(ModelMap modelMap,
			HttpSession session, Page page, String type, Chkstodemo chkstodemo)
			throws Exception {
		// 页面初始化
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkstodemo", chkstodemo);
		modelMap.put("pageobj", page);
		modelMap.put("chkstodemoList",
				chkstodemoService.findAllTitle(chkstodemo));
		return new ModelAndView(ChkstodemoConstants.MIS_TABLE_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 根据title进行查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkstodemo")
	public ModelAndView findChkstodemo(ModelMap modelMap, HttpSession session,
			Page page, String title) throws Exception {
		// 接收title进行查询
		Chkstodemo chkstodemo = new Chkstodemo();
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (null != title && !"".equals(title)) {
			chkstodemo.setTitle(title);
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("maxRec", chkstodemoService.findMaxRec(chkstodemo));
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("title", title);
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemo(chkstodemo, page));
		return new ModelAndView(ChkstodemoConstants.MIS_LIST_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 保存新增的申购单信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addNewChkstodemo")
	public ModelAndView addNewChkstodemo(ModelMap modelMap,
			HttpSession session, String action, Supply supply, String title2,
			Chkstodemo chkstodemo) throws Exception {
		if (null != action && !"".equals(action) && "init".equals(action)) {
			chkstodemo.setTitle(new String(chkstodemo.getTitle().getBytes(
					"ISO-8859-1"), "utf-8"));
			modelMap.put("chkstodemo", chkstodemo);
			return new ModelAndView(ChkstodemoConstants.MIS_SAVE_CHKSTODEMO,
					modelMap);
		} else {
			// 保存新添加的申购模板
			chkstodemo.setTitle(title2);
			chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
			chkstodemo.setSupply(supply);
			// 获取申购模板最大的序号
			int rec = chkstodemoService.findMaxRec(chkstodemo);
			chkstodemo.setRec(rec);
			chkstodemoService.saveNewChkstodemo(chkstodemo);
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}
	}

	/**
	 * 分页查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByPage")
	public ModelAndView findChkByPage(ModelMap modelMap, Page page,
			Chkstodemo chkstodemo) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("chkstodemo", chkstodemo);
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemo(chkstodemo, page));
		return new ModelAndView(ChkstodemoConstants.MIS_LIST_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 删除申购信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteChkstodemo")
	public ModelAndView deleteChkstodemo(ModelMap modelMap,
			HttpSession session, String rec) throws Exception {
		// 接收参数，进行删除
		Map<String, Object> map = new HashMap<String, Object>();
		Chkstodemo chkstodemo = new Chkstodemo();
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<String> chkstodemoList = Arrays.asList(rec.split(","));
		map.put("chkstodemo", chkstodemo);
		map.put("chkstodemoList", chkstodemoList);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstodemoService.deleteChkstodemo(map);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/**
	 * 申购模板添加报货单查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkstodemoByAddChkstodemo")
	public ModelAndView findChkstodemoByAddChkstodemo(ModelMap modelMap,
			HttpSession session, Page page, Chkstodemo chkstodemo)
			throws Exception {
		// 报货单填制时候的模板批量添加
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		modelMap.put("titleList", chkstodemoService.findAllTitle(null));
		modelMap.put("title", chkstodemo.getTitle());
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemo(chkstodemo, page));
		return new ModelAndView(ChkstodemoConstants.MIS_ADD_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 申购模板添加报货单分页查询
	 * 
	 * @param modelMap
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkByAddChkstodemoPage")
	public ModelAndView findChkByAddChkstodemoPage(ModelMap modelMap,
			HttpSession session, Page page, Chkstodemo chkstodemo)
			throws Exception {
		// 模板弹窗的分页
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemo(null, page));
		return new ModelAndView(ChkstodemoConstants.MIS_ADD_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 申购模板批量添加报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstodemo")
	public ModelAndView addChkstodemo(ModelMap modelMap, HttpSession session,
			Page page, Chkstodemo chkstodemo) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemo(chkstodemo, page));
		return new ModelAndView(ChkstodemoConstants.MIS_ADD_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 申购模板批量添加报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstodemoForFirm")
	public ModelAndView addChkstodemoFirm(ModelMap modelMap,
			HttpSession session, Page page, Chkstodemo chkstodemo)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		Account account = accountMapper.findAccountById(session.getAttribute(
				"accountId").toString());
		if (account.getPositn() != null) {
			chkstodemo.setFirm(account.getPositn().getCode());
		}
		modelMap.put("pageobj", page);
		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
		modelMap.put("chkstodemoList",
				chkstodemoService.findChkstodemoForFirm(chkstodemo, page));
		return new ModelAndView(ChkstodemoConstants.MIS_ADD_CHKSTODEMO,
				modelMap);
	}

	/**
	 * 确认修改，添加模板数据到报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enterUpdate")
	@ResponseBody
	public Object enterUpdate(ModelMap modelMap, HttpSession session,
			String rec2, String cnt, String cnt1, String title)
			throws Exception {
		// 批量添加上的数量假修改
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		List<String> recList = Arrays.asList(rec2.split(","));
		List<String> cntList = Arrays.asList(cnt.split(","));
		List<String> cnt1List = Arrays.asList(cnt1.split(","));
		return chkstodemoService.findChkstodemoByRec(recList, cntList,
				cnt1List, acct, title);
	}

	/**
	 * 确认修改，添加模板数据到报货单 分店
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enterUpdateForFirm")
	@ResponseBody
	public Object enterUpdateForFirm(ModelMap modelMap, HttpSession session,
			String rec2, String cnt, String cnt1, String title)
			throws Exception {
		// 批量添加上的数量假修改
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		Account account = accountMapper.findAccountById(session.getAttribute(
				"accountId").toString());
		String firm = null;
		if (account.getPositn() != null) {
			firm = account.getPositn().getCode();
		}
		List<String> recList = Arrays.asList(rec2.split(","));
		List<String> cntList = Arrays.asList(cnt.split(","));
		List<String> cnt1List = Arrays.asList(cnt1.split(","));
		return chkstodemoService.findChkstodemoForFirmByRec(recList, cntList,
				cnt1List, acct, firm, title);
	}

	/******************************************************** 申购单模板end *************************************************/

	/******************************************************** 报货单start *************************************************/
	/**
	 * 查找已审核报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap,
			HttpSession session, Page page, String init, String sp_code,
			Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if (null != accountPositn && null != accountPositn.getPositn()) {
			Positn positn = new Positn();
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
			chkstom.setFirm(positnCode);
			chkstom.setPositn(positn);
		}
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (null != init && "" != init) {
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		HashMap<String, Object> chkstomMap = new HashMap<String, Object>();
		chkstomMap.put("checked", "checked");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		// 关键字查询
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap, page));
//		modelMap.put("positnList", positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomConstants.MIS_TABLE_CHECKED_CHKSTOM,
				modelMap);
	}

	/**
	 * 查看已审核报货单的信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedChkstom")
	public ModelAndView searchCheckedChkstom(ModelMap modelMap,
			HttpSession session, Page page, Chkstod chkstod, Chkstom chkstom)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 查询已审核报货单主界面
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if (null != accountPositn && null != accountPositn.getPositn()) {
			Positn positn = new Positn();
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
			chkstom.setPositn(positn);
		}
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList", positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomConstants.MIS_SEARCH_CHECKED_CHKSTOM,
				modelMap);
	}

	/**
	 * 查询所有未审核报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chkstomTable")
	public ModelAndView findAllChkstom(ModelMap modelMap, Page page,
			HttpSession session, Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 获取当前分店今天的 单据情况 已审核多少条， 未审核多少条
		chkstom.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			chkstom.setFirm(positnCode);
			modelMap.put("chkstom", chkstomService.findCountByFirmId(chkstom));
		}
		modelMap.put("acctLineFirm", linesFirmService.findFirmByLinesFirmIdOne(
				accountId, "accountId"));// 活的配送时间
	
		return new ModelAndView(ChkstomConstants.MIS_TABLE_CHKSTOM_NEW,modelMap);
	}

	/**
	 * 添加操作时候，刷新页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap, Page page,
			HttpSession session, Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 当前登录用户
		String accountName = session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());

		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			positnList.add(accountPositn.getPositn());
			chkstom.setFirm(positnCode);
		}
		// 获取下一个单号
		chkstom.setVouno(calChkNum.getNext(CalChkNum.CHKSTO, new Date()));
	
		modelMap.put("positnList", positnList);
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		modelMap.put("acctLineFirm", linesFirmService.findFirmByLinesFirmIdOne(accountId, "accountId"));// 活的配送时间
		// 是否节假日
		return new ModelAndView(ChkstomConstants.MIS_TABLE_CHKSTOM_NEW,modelMap);
	}

	/**
	 * 查找报货单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap, HttpSession session,
			Page page, String startDate, String sp_code, String init,
			Chkstom chkstom, String firmDes) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		chkstom.setAcct(acct);
		Date bdate = null;
		Date edate = null;
		HashMap<String, Object> chkstomMap = new HashMap<String, Object>();
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		// AccountPositn
		// accountPositn=accountPositnService.findAccountById(accountId);
		// 是否是#mis程序控制 申购 入库 门店范围 Y 表示 根据权限取的门店
		List<String> listPositn = new ArrayList<String>();
		listPositn.add(accountPositnService.findAccountById(accountId).getPositn().getCode());
		chkstom.setListPositn(listPositn);
		// 不是初始化的时候，取用户输入的查询日期
		if (null != init && !"".equals(init)) {
			if (null != startDate && !"".equals(startDate)) {
				bdate = DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			} else {
				bdate = new Date();
			}
			edate = new Date();
			chkstom.setbMaded(bdate);
			chkstom.seteMaded(edate);
		}
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		// 关键字查询
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap, page));
		// modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("firmDes", firmDes);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomConstants.MIS_SEARCH_CHKSTOM, modelMap);
	}

	/**
	 * 打开查看分店上传数据页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUpload")
	public ModelAndView searchUpload(ModelMap modelMap) throws Exception {
		// 查看上传数据
		modelMap.put("curDate", new Date());
		return new ModelAndView(ChkstomConstants.MIS_SEARCH_UPLOAD, modelMap);
	}

	/**
	 * 查看分店上传数据
	 * 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUploadByDate")
	public ModelAndView searchUploadByDate(ModelMap modelMap,
			HttpSession session, Chkstom chkstom, String startDate)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 接收日期，然后查询有哪些店上传与未上传
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (null != startDate && !"".equals(startDate)) {// 日期不为空
			chkstom.setMaded(DateFormat
					.getDateByString(startDate, "yyyy-MM-dd"));
			// 查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		} else {
			// 查看上传数据
			modelMap.put("chkstomList", chkstomService.findUpload(chkstom));
			// 查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		}

		// 系统日期
		modelMap.put("curDate",
				DateFormat.formatDate(chkstom.getMaded(), "yyyy-MM-dd"));
		// 仓位列表
		modelMap.put("positnList", positnService.findAllPositn(null));
		return new ModelAndView(ChkstomConstants.MIS_SEARCH_UPLOAD, modelMap);
	}

	/**
	 * 删除
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(ModelMap modelMap, HttpSession session,
			Page page, String chkstoNoIds, Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList", positnService.findAllPositn(null));
		return chkstomService.deleteChkstom(chkstom, chkstoNoIds);
	}

	/**
	 * 双击查找
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap, HttpSession session,
			Page page, Chkstod chkstod, Chkstom chkstom) throws Exception {
		// 报货单弹窗查询结果集，双击后，跳转到详细信息页
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		modelMap.put("positnList", positnService.findAllPositn(null));
		Chkstom chkstom1 = chkstomService.findByChkstoNo(chkstom);
		chkstom1.setCountChecked(chkstom.getCountChecked());
		chkstom1.setCountNoChecked(chkstom.getCountNoChecked());
		modelMap.put("chkstom", chkstom1);
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		// 是否节假日
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Holiday holiday = holidayService.findHolidayByDate(new Holiday(sdf
				.format(new Date())));
		if (null != holiday && null != holiday.getDat()) {
			modelMap.put("holiday", true);
		}
		return new ModelAndView(ChkstomConstants.MIS_TABLE_CHKSTOM_NEW,modelMap);
	}

	/**
	 * 检查是否设置供应商
	 */
	@RequestMapping(value = "/checkSaveNewChk")
	@ResponseBody
	public Object checkSaveNewChk(ModelMap modelMap, Page page,
			HttpSession session, Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.checkSaveNewChk(chkstom);
	}

	/**
	 * 保存新增
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap, Page page,
			HttpSession session, String sta, Chkstom chkstom, Positn positn)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.saveOrUpdateChkMis(chkstom, sta);
	}

	/**
	 * 报货单打印
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(ModelMap modelMap, HttpSession session,
			Page page, String type, Chkstod chkstod) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据用户输入的参数进行报货单查询结果集的打印
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		List<Chkstod> list = chkstodService.findAllChkstod(chkstod);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		String report_name = new String("申购数据打印");
		String report_date = DateFormat.getStringByDate(new Date(),
				"yyyy-MM-dd");
		parameters.put("report_name", report_name);
		parameters.put("report_date", report_date);
		modelMap.put("List", list);
		modelMap.put("parameters", parameters);
		modelMap.put("actionMap", disMap);// 回调参数
		modelMap.put("action",
				"/firmMis/printChkstom.do?chkstoNo=" + chkstod.getChkstoNo());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkstomConstants.REPORT_PRINT_URL,
				ChkstomConstants.REPORT_PRINT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		// return new ModelAndView(rs.get("url").replace("ireport",
		// "ireport/mapSource"),modelMap);
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/******************************************************** 报货单end *************************************************/
	
	/***************************************门店安全库存报货 开始2015.5.12wjf ****************************************/
	/**
	 * 进入安全库存报货
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableSafeChkstom")
	public ModelAndView tableSafeChkstom(ModelMap modelMap, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"mis进入安全库存报货:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId=session.getAttribute("accountId").toString();
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			modelMap.put("positn", accountPositn.getPositn());
		}
		return new ModelAndView(ChkstomConstants.TABLE_SAFE_CHKSTOM_MIS,modelMap);
	}
	
	/**
	 * 查询安全库存报货
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSafeChkstom")
	public ModelAndView findSafeChkstom(ModelMap modelMap, HttpSession session,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询安全库存报货:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId=session.getAttribute("accountId").toString();
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			modelMap.put("positn", accountPositn.getPositn());
		}
		modelMap.put("chkstom", chkstom);
		//查询库存
		List<Chkstod> list = chkstomService.findSafeChkstodList(chkstom);
		modelMap.put("chkstodList", list);
		return new ModelAndView(ChkstomConstants.TABLE_SAFE_CHKSTOM_MIS,modelMap);
	}
	
	/**
	 * 保存安全库存报货
	 */
	@RequestMapping(value = "/saveBySafeChkstom")
	@ResponseBody
	public Object saveBySafeChkstom(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"新增安全库存报货:单号("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		chkstom.setMadeby(session.getAttribute("accountName").toString());
		return chkstomService.saveOrUpdateChk(chkstom, "add");
	}
	
	/***************************************门店安全库存报货 结束2015.5.12wjf ****************************************/

	/******************************************************** 直配验收   开始 *************************************************/
	/**
	 * 直配验货主界面
	 * @author wjf
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheck.do")
	public ModelAndView findAllCheck(ModelMap modelMap, HttpSession session,
			Page page, Dis dis, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			dis.setFirmCode(accountPositn.getPositn().getCode());
		}
		dis.setInout("in,dire");
//		dis.setChkin("N");
		if (null != action && !"".equals(action)) {
			Calendar calendar = Calendar.getInstance();
			dis.setBdat(calendar.getTime());
			calendar.add(Calendar.DATE, 1); // 得到后一天
			Date date = calendar.getTime();
			dis.setEdat(date);
			modelMap.put("action", "init");
			dis.setYndo("NO");
		} else {
			String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
			dis.setAcct(acct);
			int totalCount = disService.findAllDisCount(dis);
			dis.setStartNum(0);
			dis.setEndNum(PAGE_SIZE);
			modelMap.put("totalCount", totalCount);

			if (totalCount <= PAGE_SIZE) {
				modelMap.put("currState", 1);
			} else {
				modelMap.put("currState", PAGE_SIZE * 1.0 / totalCount);
			}
			List<Dis> disList = disService.findAllDisPage(dis);
			modelMap.put("disList", disList);
			//判断当前条件下是否有档口报货来的验货单
			StringBuffer ids = new StringBuffer();
			for(Dis d : disList){
				ids.append(d.getChkstoNo()+",");
			}
			ids.append("0");
			int count = disService.findDeptChkstom(ids.toString());
			modelMap.put("is_dept", count > 0?"Y":"N");
		}
		modelMap.put("dis", dis);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		return new ModelAndView(DisConstants.MIS_TABLE_CHECK, modelMap);
	}

	/**
	 * 到货验收Ajax加载
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session, Dis dis, String currPage,
			String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		dis.setStartNum(intCurrPage * PAGE_SIZE);
		dis.setEndNum((intCurrPage + 1) * PAGE_SIZE);
		List<Dis> disList = disService.findAllDisPage(dis);
		modelMap.put("disesList1", disList);
		//判断当前条件下是否有档口报货来的验货单
		StringBuffer ids = new StringBuffer();
		for(Dis d : disList){
			ids.append(d.getChkstoNo()+",");
		}
		ids.append("0");
		int count = disService.findDeptChkstom(ids.toString());
		modelMap.put("is_dept", count > 0?"Y":"N");
		modelMap.put("currState", (intCurrPage + 1) * PAGE_SIZE * 1.0
				/ intTotalCount);
		modelMap.put("currPage", currPage);
		if ((intCurrPage + 1) * PAGE_SIZE >= intTotalCount) {
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap, HttpSession session,
			DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// String accountName=session.getAttribute("accountName").toString();
		return disService.updateDis(disList);
		// modelMap.put("accountName", accountName);
		// return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}
	
	/**
	 * 打印验收单
	 */
	@RequestMapping(value = "/printReceipt")
	public ModelAndView printReceipt(ModelMap modelMap, HttpSession session,
			Dis dis, String type, String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			dis.setFirmCode(accountPositn.getPositn().getCode());
		}
		// 接收用户输入的查询参数
		HashMap<String, Object> param = new HashMap<String, Object>();
		dis.setDanjuTyp("receipt");
		param.put("danjuTyp", "receipt");
		dis.setInout("in,dire");
		dis.setChkin("N");
		dis.setChkout("N");
		dis.setSta("Y");//已审核的
		param.put("chkin", "N");
		param.put("chkout", "N");
		param.put("inout", "in,dire");
		param.put("bdat",
				DateFormat.getStringByDate(dis.getBdat(), "yyyy-MM-dd"));
		param.put("edat",
				DateFormat.getStringByDate(dis.getEdat(), "yyyy-MM-dd"));
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		if (null != ind1 && !"".equals(ind1)) {
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);// 回调参数
		modelMap.put("action", "/firmMis/printReceipt.do");// 传入回调路径
		// 根据关键字查询
		List<Dis> disList = disService.findAllDisForReport(dis);
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		String report_name = new String("验收单");
		parameters.put("report_name", report_name);
		modelMap.put("List", disList);
		modelMap.put("parameters", parameters);// 报表文件用的输入参数
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				DisConstants.REPORT_RECEIPT_URL,
				DisConstants.REPORT_RECEIPT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 验收入库主界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheckin.do")
	public ModelAndView tableCheckin(ModelMap modelMap, HttpSession session,
			Page page, Dis dis, String action, String ind1, Date maded1,
			String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			dis.setFirmCode(positnCode);
			dis.setFirmDes(positnDes);
			Positn positn = positnService.findPositnByCode(accountPositn.getPositn());
			modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
		}
		// 接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); // 得到前一天
		Date date = calendar.getTime();
		if (null != action && !"".equals(action)) {
			dis.setBdat(date);
			dis.setEdat(date);
			modelMap.put("maded1", new Date());
		} else {
			dis.setInout("in,dire");
			dis.setChkin("N");
//			dis.setCheckinSelect("checkinSelect");//可以查验货数量为0 的 只是不生成入库单wjf
			String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
			dis.setAcct(acct);
			List<Dis> disList = disService.findAllDis(dis, page);
			if (null != maded1) {
				modelMap.put("maded1", maded1);
			} else {
				modelMap.put("maded1", new Date());
			}
			modelMap.put("disList", disList);
		}
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.MIS_TABLE_CHECKIN_NEW,modelMap);
	}
	
	/**
	 * 生成入库单
	 */
	@RequestMapping(value = "/saveYsrkChkinm.do")
	public ModelAndView saveYsrkChkinm(ModelMap modelMap,HttpSession session,Page page,Dis dis,String ind1,Date maded1,String ysrkStr) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
//				"生成入库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收方法执行后的结果状态，1：执行成功；2：页面查询结果集为空，不能进行入库；
		int str=0;
		List<Dis> disList=disService.findAllDis(dis,page);//如果page为空，则不分页  查询所有的2014.10.24wjf
		//报货单转入库单方法
		String  accountName=session.getAttribute("accountName").toString();//当前用户
		if(disList.size()!=0){
			disService.saveYsrkChkinm(disList,acct,dis,dis.getFirmCode(),maded1!=null?maded1:new Date(),accountName);
			str=1;//不为空，成功
		}else{
			str=2;//为空，不能进行入库
		}
		modelMap.put("maded1", maded1);
		modelMap.put("chkMsg", str);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.MIS_TABLE_CHECKIN_NEW,modelMap);
	}

	/**
	 * 入库单打印
	 */
	@RequestMapping(value = "/viewYsrkChkstom")
	public ModelAndView viewYsrkChkstom(ModelMap modelMap, HttpSession session,
			Page page, Dis dis, String type, String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 接收用户输入的查询参数
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("bdat",
				DateFormat.getStringByDate(dis.getBdat(), "yyyy-MM-dd"));
		param.put("edat",
				DateFormat.getStringByDate(dis.getEdat(), "yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("memo1", dis.getMemo1());
		param.put("chkin", "N");
		param.put("inout", "in,dire");
		if (null != ind1 && !"".equals(ind1)) {
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		dis.setInout("in,dire");
		dis.setChkin("N");
		dis.setCheckinSelect("checkinSelect");
		modelMap.put("actionMap", param);// 回调参数
		modelMap.put("action", "/firmMis/viewYsrkChkstom.do");// 传入回调路径
		// 根据关键字查询
		List<Dis> disList = disService.findAllDisForReport(dis);
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		String report_name = new String("物资入库明细表");
		parameters.put("report_name", report_name);
		parameters.put("report_date",
				DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		String accountName = session.getAttribute("accountName").toString();
		parameters.put("madeby", accountName);
		modelMap.put("List", disList);
		modelMap.put("parameters", parameters);// 报表文件用到的输入参数
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				DisConstants.REPORT_YSRK_URL, DisConstants.REPORT_YSRK_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
	/**
	 * 生成档口入库单 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmDept")
	public ModelAndView toChkinmDept(ModelMap modelMap, Dis dis, HttpSession session,String iszp) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyacctList", disService.findSupplyacctList(dis,iszp));
		if("Y".equals(iszp)){//直配验货查询
			return new ModelAndView(DisConstants.SAVE_CHKINM_DEPT_ZP, modelMap);
		}
		modelMap.put("dis", dis);
		return new ModelAndView(DisConstants.SAVE_CHKINM_DEPT, modelMap);
	}
	
	/**
	 * 生成档口直发单(直配验货)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmDept_zp")
	@ResponseBody
	public int saveChkinmDept_zp(ModelMap modelMap,Chkinm chkinm, String ids, String sp_ids,HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			chkinm.setPositn(accountPositn.getPositn());
		}
		chkinm.setAcct(acct);
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		return disService.saveChkinmDept_zp(chkinm,ids,sp_ids);
	}
	
	/**
	 * 直配验货分档口 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroupZP")
	public ModelAndView toChkinmGroupZP(ModelMap modelMap, Dis dis, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		List<Dis> disList=disService.findAllDisForReport(dis);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<Dis> list = new ArrayList<Dis>();
			for(Dis d : disList){
				list.add(d);
				for(Positn p : deptList){
					Dis s = new Dis();
					s.setFirmCode(p.getCode());
					s.setFirmDes(p.getDes());
					s.setChkstoNo(d.getChkstoNo());
					s.setUnitper(d.getUnitper());
					s.setDued(d.getDued());
					s.setPcno(d.getPcno());
					s.setDeliverCode(d.getDeliverCode());
					s.setAmountin(0.0);
					s.setAmount1in(0.0);
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", dis.getInCondition());
		return new ModelAndView(DisConstants.SAVE_CHKINM_GROUP_ZP, modelMap);
	}
	
	/**
	 * 门店的就生成门店入库单，档口的生成档口直发单(统配验货)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroupZP")
	@ResponseBody
	public int saveChkinmGroupZP(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			chkinm.setPositn(accountPositn.getPositn());
		}
		chkinm.setAcct(acct);
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		return disService.saveChkinmGroupZP(chkinm,ids);
	}

	/******************************************************** 直配验收end *************************************************/
	
	/******************************************************** 配送中心到货验收start *************************************************/
	/**
	 * 配送中心到货验收
	 * @author wjf
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dispatch.do")
	public ModelAndView diapatchList(ModelMap modelMap, HttpSession session,
			String mold, SupplyAcct sa, Page page, String check, Date bdate,
			Date edate) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 点击查询的时候符合条件的配送中心到货信息。
		if ("select".equals(mold)) {
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("bdate", bdate);
			disMap.put("edate", edate);
			disMap.put("check", check);
			// disMap.put("firm", "1101");
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				disMap.put("firm", positnCode);
				modelMap.put("firmCode", positnCode);
				Positn positn = positnService.findPositnByCode(accountPositn.getPositn());
				modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
			}

			List<SupplyAcct> disList = disService.selectByKey(disMap, page);

			modelMap.put("dis", sa);
			modelMap.put("disList", disList);
		}

		// 初次加载页面传值
		if (bdate == null || edate == null) {
			bdate = new Date();
			edate = new Date();
			check = "2";//默认未审核
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("check", check);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.LIST_DISPATCH, modelMap);
	}

	/**
	 * 配送中心到货验收
	 * 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcct")
	@ResponseBody
	public ModelAndView updateAcct(ModelMap modelMap, SupplyAcct supplyAcct,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		disService.updateAcct(supplyAcct, acct);
		return null;
	}

	/**
	 * 配送中心到货审核(没有启用档口的)
	 * 
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/check")
	public ModelAndView check(ModelMap modelMap, String ids, HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		disService.check(ids, session.getAttribute("accountName").toString(),
				session.getAttribute("ChoiceAcct").toString());
		modelMap.put("check", "2");
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 统配验货分档口 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroup")
	public ModelAndView toChkinmGroup(ModelMap modelMap, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", ids);
		List<SupplyAcct> disList = disService.selectByKey(disMap);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<SupplyAcct> list = new ArrayList<SupplyAcct>();
			for(SupplyAcct sa : disList){
				sa.setPositn(positnCode);
				sa.setPositndes(positnDes);
				list.add(sa);
				for(Positn p : deptList){
					SupplyAcct s = new SupplyAcct();
					Supply supply = new Supply();
					supply.setUnitper(sa.getSupply().getUnitper());
					s.setSupply(supply);
					s.setDued(sa.getDued());
					s.setPcno(sa.getPcno());
					s.setChkstoNo(sa.getChkstoNo());
					s.setPositn(p.getCode());
					s.setPositndes(p.getDes());
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", ids);
		return new ModelAndView(DisConstants.SAVE_CHKINM_GROUP, modelMap);
	}
	
	/**
	 * 门店的就生成门店入库单，档口的生成档口直发单(统配验货)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroup")
	@ResponseBody
	public int saveChkinmGroup(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountPositn accountPositn = accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			chkinm.setPositn(accountPositn.getPositn());
		}
		chkinm.setAcct(acct);
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		return disService.saveChkinmGroup(chkinm,ids);
	}
	
	/**
	 * 生成档口直发单(统配验货)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmDept")
	@ResponseBody
	public int saveChkinmDept(ModelMap modelMap,Chkinm chkinm, String ids, String sp_ids,HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountPositn accountPositn = accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			chkinm.setPositn(accountPositn.getPositn());
		}
		chkinm.setAcct(acct);
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		return disService.saveChkinmDept(chkinm,ids,sp_ids);
	}

	/**
	 * 配送中心到货验收表打印
	 * 
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDispatch")
	public ModelAndView printDispatch(ModelMap modelMap, HttpSession session,
			String type, String check, Date bdate, Date edate)
			throws CRUDException {

		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			disMap.put("firm", positnCode);
		}
		List<SupplyAcct> disList = disService.selectByKey(disMap);
		int num = disList.size() % 19;
		for (int i = 0; i < 19 - num; i++) {
			disList.add(null);
		}
		modelMap.put("List", disList);// list
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("report_name", "配送中心到货验收表");
		parameters.put("report_time",
				DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("parameters", parameters);// map

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("check", check);
		map.put("bdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
		map.put("edate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
		modelMap.put("actionMap", map);
		modelMap.put("action", "/firmMis/printDispatch.do");// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				DisConstants.REPORT_DISPATCH_URL,
				DisConstants.REPORT_DISPATCH_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
	/******************************************************** 配送中心到货验收end *************************************************/
	
	/******************************************************** 门店调拨验收start *************************************************/
	/**
	 * 门店调拨验收
	 * @author wjf
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dispatchDb.do")
	public ModelAndView diapatchDbList(ModelMap modelMap, HttpSession session,
			String mold, SupplyAcct sa, Page page, String check, Date bdate,
			Date edate) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 点击查询的时候符合条件的配送中心到货信息。
		if ("select".equals(mold)) {
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("bdate", bdate);
			disMap.put("edate", edate);
			disMap.put("check", check);
			disMap.put("db", "db");//查调拨类型
			disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				disMap.put("firm", positnCode);
				modelMap.put("firmCode", positnCode);
				Positn positn = positnService.findPositnByCode(accountPositn.getPositn());
				modelMap.put("ynUseDept", positn.getYnUseDept());//页面判断用
			}
			List<SupplyAcct> disList = disService.selectByKey(disMap, page);
			modelMap.put("dis", sa);
			modelMap.put("disList", disList);
		}
		// 初次加载页面传值
		if (bdate == null || edate == null) {
			bdate = new Date();
			edate = new Date();
			check = "2";//默认未审核
		}
		modelMap.put("bdate", bdate);
		modelMap.put("edate", edate);
		modelMap.put("check", check);
		modelMap.put("pageobj", page);
		return new ModelAndView(DisConstants.LIST_DISPATCH_DB, modelMap);
	}

	/**
	 * 配送中心到货验收
	 * 
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcctDb")
	@ResponseBody
	public ModelAndView updateAcctDb(ModelMap modelMap, SupplyAcct supplyAcct,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		disService.updateAcct(supplyAcct, acct);
		return null;
	}

	/**
	 * 调拨到货审核  不分档口的
	 * 
	 * @param modelMap
	 * @param ids
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkDb")
	public ModelAndView checkDb(ModelMap modelMap, String ids, HttpSession session)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		disService.checkDb(ids, session.getAttribute("accountName").toString(),
				session.getAttribute("ChoiceAcct").toString());
		modelMap.put("check", "2");
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 调拨验货分档口 页面跳转   
	 * idList  主键    itemIds：sp_code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toChkinmGroupDb")
	public ModelAndView toChkinmGroupDb(ModelMap modelMap, String ids, HttpSession session) throws Exception {  
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", ids);
		disMap.put("db", "db");
		List<SupplyAcct> disList = disService.selectByKey(disMap);
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			List<Positn> deptList = positnService.findPositnSuperNOPage(positn);
			List<SupplyAcct> list = new ArrayList<SupplyAcct>();
			for(SupplyAcct sa : disList){
				sa.setDelivercode(sa.getPositn());//供应商
				sa.setPositn(positnCode);
				sa.setPositndes(positnDes);
				list.add(sa);
				for(Positn p : deptList){
					SupplyAcct s = new SupplyAcct();
					Supply supply = new Supply();
					supply.setUnitper(sa.getSupply().getUnitper());
					s.setSupply(supply);
					s.setDued(sa.getDued());
					s.setPcno(sa.getPcno());
					s.setChkstoNo(sa.getChkstoNo());
					s.setDelivercode(sa.getDelivercode());
					s.setPositn(p.getCode());
					s.setPositndes(p.getDes());
					list.add(s);
				}
			}
			modelMap.put("disList", list);
		}
		modelMap.put("ids", ids);
		return new ModelAndView(DisConstants.SAVE_CHKINM_GROUP_DB, modelMap);
	}
	
	/**
	 * 门店的就生成门店入库单，档口的生成档口直发单(调拨验货)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveChkinmGroupDb")
	@ResponseBody
	public int saveChkinmGroupDb(ModelMap modelMap,Chkinm chkinm, String ids, HttpSession session) throws Exception {  
		String acct=session.getAttribute("ChoiceAcct").toString();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountPositn accountPositn = accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			chkinm.setPositn(accountPositn.getPositn());
		}
		chkinm.setAcct(acct);
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		return disService.saveChkinmGroupDb(chkinm,ids);
	}

	/**
	 * 调拨到货验收表打印
	 * 
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDispatchDb")
	public ModelAndView printDispatchDb(ModelMap modelMap, HttpSession session,
			String type, String check, Date bdate, Date edate)
			throws CRUDException {

		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("db", "db");
		disMap.put("acct", session.getAttribute("ChoiceAcct").toString());
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			disMap.put("firm", positnCode);
		}
		List<SupplyAcct> disList = disService.selectByKey(disMap);
		int num = disList.size() % 19;
		for (int i = 0; i < 19 - num; i++) {
			disList.add(null);
		}
		modelMap.put("List", disList);// list
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("report_name", "调拨到货验收表");
		parameters.put("report_time",
				DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("parameters", parameters);// map

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("check", check);
		map.put("bdate", DateFormat.getStringByDate(bdate, "yyyy-MM-dd"));
		map.put("edate", DateFormat.getStringByDate(edate, "yyyy-MM-dd"));
		modelMap.put("actionMap", map);
		modelMap.put("action", "/inspection/printDispatch.do");// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				DisConstants.REPORT_DISPATCH_URL,
				DisConstants.REPORT_DISPATCH_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}
	
	/********************************************************门店调拨验货 end****************************************************************************/
	
	/******************************************************** 入库单start 这个不用了wjf2015.5.21 *************************************************/

	/**
	 * 转入添加页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/chkinmAdd")
	public ModelAndView add(ModelMap modelMap, HttpSession session,
			String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if (!"init".equals(action)) {// 不是页面初始化的时候
			String accountName = session.getAttribute("accountName").toString();
			Chkinm chkinm = new Chkinm();
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN, new Date()));
			chkinm.setChkinno(chkinmService.getMaxChkinno());
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService
					.findAccountById(accountId);
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				positnList.add(accountPositn.getPositn());
				Positn positn = new Positn();
				positn.setCode(positnCode);
				chkinm.setPositn(positn);
			}
			modelMap.put("chkinm", chkinm);// 日期等参数
			String acct = session.getAttribute("ChoiceAcct").toString();
			Positn positn = new Positn();
			positn.setAcct(acct);
			modelMap.put("positnList", positnList);
			modelMap.put("deliverList",
					deliverService.findAllDelivers(new Deliver()));// 供应商
			modelMap.put("curStatus", "add");// 当前页面状态
		}
		return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM, modelMap);
	}

	/**
	 * 修改跳转
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap, Chkinm chkinm,
			String isCheck, String type) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String inout = null;
		modelMap.put("positnList", positnService.findAllPositn(new Positn()));// 仓位
		modelMap.put("deliverList",
				deliverService.findAllDelivers(new Deliver()));// 供应商
		modelMap.put("chkinm", chkinmService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");// 当前页面状态
		if (null != isCheck && !"".equals(isCheck)) {
			inout = ChkinmConstants.MIS_SEARCH_CHECKED_CHKINM;
		} else {
			inout = ChkinmConstants.MIS_UPDATE_CHKINM;
		}
		return new ModelAndView(inout, modelMap);
	}

	/**
	 * 查询入库单list
	 * 
	 * @param checkOrNot
	 *            是否审核
	 * @param madedEnd
	 * @param searchDate
	 * @param action
	 *            init表示初始页面
	 */
	@RequestMapping(value = "/chkinmList")
	public ModelAndView findAllChkinm(ModelMap modelMap, HttpSession session,
			String checkOrNot, Chkinm chkinm, Date madedEnd, String startDate,
			Page page, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		// AccountPositn
		// accountPositn=accountPositnService.findAccountById(accountId);
		List<Positn> positnList = positnRoleService.findAllPositn(session
				.getAttribute("ChoiceAcct").toString(), accountId);// 带有权限控制的仓位
		// 获取带有权限控制的仓位
		List<String> listPositn = new ArrayList<String>();
		if (positnList.size() != 0) {
			chkinm.setPositn(positnList.get(0));//取第一条为账号默认仓位
			for (int i = 0; i < positnList.size(); i++) {
				if(null != positnList.get(i)){
					listPositn.add(positnList.get(i).getCode());
				}
			}
		}
		if(listPositn.size() != 0){
			chkinm.setListPositn(listPositn);
		}
		/*
		 * if(null!=accountPositn && null!=accountPositn.getPositn()){ // String
		 * positnCode=accountPositn.getPositn().getCode(); // String positnName
		 * = accountPositn.getPositn().getDes(); // Positn positn=new Positn();
		 * // positn.setCode(positnCode); // positn.setDes(positnName);
		 * chkinm.setPositn(accountPositn.getPositn()); }
		 */
		if (null != action && "init".equals(action)) {// 页面初始化的日期设置
			Date date = new Date();
			if (null != startDate && !"".equals(startDate)) {
				chkinm.setMaded(DateFormat.getDateByString(startDate,
						"yyyy-MM-dd"));
			} else {
				chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
			}
			modelMap.put("chkinmList", chkinmService.findAllChkinm(checkOrNot,
					chkinm, page, DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", date);
		} else {
			modelMap.put("chkinmList", chkinmService.findAllChkinm(checkOrNot,
					chkinm, page, madedEnd,session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("chkinm", chkinm);
		modelMap.put("checkOrNot", checkOrNot);
		modelMap.put("pageobj", page);
		if ("check".equals(checkOrNot)) {
			return new ModelAndView(ChkinmConstants.MIS_LIST_CHECKED_CHKINM,
					modelMap);
		} else {
			return new ModelAndView(ChkinmConstants.MIS_LIST_CHKINM, modelMap);
		}
	}

	/**
	 * 修改入库单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateChkinm")
	@ResponseBody
	public int updateChkinm(ModelMap modelMap, HttpSession session,
			Chkinm chkinm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 入库单数据修改
		Chkinm chkinms = chkinmService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		Integer result = 0;
		if (null != chkinms) {// 存在则修改
			chkinm.setInout(ChkinmConstants.in);
			result = chkinmService.updateChkinm(chkinm);
		} else {// 不存在则添加
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,
					chkinm.getMaded()));// 从新获得单号
			result = chkinmService.saveChkinm(chkinm, "in");
		}
		return result;
	}

	/**
	 * 删除入库单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteChkinm")
	public ModelAndView delete(ModelMap modelMap, String chkinnoids, Page page,
			String checkOrNot, Chkinm chkinm, Date madedEnd, String action)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据单号进行删除
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		chkinmService.deleteChkinm(ids);
		if (!"init".equals(action)) {
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		} else {
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM, modelMap);
		}
	}

	/**
	 * 审核入库单 单条审核
	 * 
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkinm")
	public ModelAndView checkChkinm(ModelMap modelMap, Chkinm chkinm,
			HttpSession session, String zb) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String accountId = session.getAttribute("accountName").toString();
		chkinm.setChecby(accountId);
		chkinmService.checkChkinm(chkinm, null);
		if ("zb".equals(zb))
			return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB, modelMap);
		else
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM, modelMap);
	}

	/**
	 * 审核操作 批量审核
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids,
			String checkOrNot, Chkinm chkinm, Date madedEnd, Page page,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 批量审核
		String accountName = session.getAttribute("accountName").toString();
		chkinm.setChecby(accountName);
		chkinmService.checkChkinm(chkinm, chkinnoids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}

	/**
	 * 入库单打印
	 * 
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap, Chkinm chkinm,
			String type, Page page,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 入库单打印
		chkinm = chkinmService.findChkinmByid(chkinm);
		List<Chkind> chkindList = chkinm.getChkindList();
		int num = chkindList.size() % 12;
		for (int i = 0; i < 12 - num; i++) {
			chkindList.add(null);
		}
		modelMap.put("List", chkindList);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		// 报表文件用到的输入参数
		parameters.put("report_name", "入库单");
		if (null != chkinm.getDeliver() && null != chkinm.getDeliver().getDes()) {
			parameters.put("deliver.des", chkinm.getDeliver().getDes());
		}
		parameters.put("positn.des", chkinm.getPositn().getDes());
		parameters.put("vouno", chkinm.getVouno());
		parameters.put("maded",
				DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd"));
		parameters.put("madeby", chkinm.getMadeby());
		parameters.put("checby", chkinm.getChecby());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkinm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("chkinno", chkinm.getChkinno() + "");
		modelMap.put("parameters", parameters);
		modelMap.put("actionMap", map);
		modelMap.put("action", "/chkinm/viewChkinm.do");// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkinmConstants.REPORT_URL, ChkinmConstants.REPORT_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/******************************************************** 入库单end *************************************************/
	/******************************************************** 出库单start  这个也没用了*************************************************/
	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String type, String action) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			Positn positn = new Positn();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
			chkoutm.setPositn(positn);
		}
		if (action == null || !action.equals("init")) {// 不是页面初始化的时候
			chkoutm.setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setChkoutno(chkoutService.findNextNo().intValue());
			modelMap.put("positnIn", positnService.findPositnIn());
			String acct = session.getAttribute("ChoiceAcct").toString();
			modelMap.put("positnOut",
					positnRoleService.findAllPositn(acct, accountId));// 带有权限控制的仓位
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		}
		return new ModelAndView(ChkoutConstants.MIS_ADD_CHKOUT, modelMap);
	}

	/**
	 * 模糊查询出库单
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("chkoutList")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,
			Date bdat, Date edat, HttpSession session, Page page)
			throws Exception {
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			Positn positn = new Positn();
			String positnCode = accountPositn.getPositn().getCode();
			String positnDes = accountPositn.getPositn().getDes();
			positn.setCode(positnCode);
			positn.setDes(positnDes);
			chkoutm.setPositn(positn);
//			chkoutm.setFirm(positn);
		}
		modelMap.put("pageobj", page);
		if (null != bdat && !"".equals(bdat)) {// 日期不为空，设置默认日期
			modelMap.put("bdat", bdat);
		} else {
			modelMap.put("bdat", new Date());
		}
		if (null != edat && !"".equals(edat)) {// 日期不为空，设置默认日期
			modelMap.put("edat", edat);
		} else {
			modelMap.put("edat", new Date());
		}
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("chkoutList",
				chkoutService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString()));
		if (null != chkoutm.getChecby() && chkoutm.getChecby().equals("c")) {
			return new ModelAndView(ChkoutConstants.MIS_CHECKED_CHKOUT,
					modelMap);
		} else {
			return new ModelAndView(ChkoutConstants.MIS_MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 更新出库单
		String result = null;
		if (curStatus.equals("add")) {
			chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
			result = chkoutService.saveChkoutm(chkoutm);
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(accountService.findAccountById(
					session.getAttribute("accountId").toString()).getName());
			result = chkoutService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, String type,
			Chkoutm chkoutm, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 修改出库单
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", chkoutService.findChkoutmById(chkoutm));
		modelMap.put("positnIn", positnService.findPositnIn());
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountId = session.getAttribute("accountId").toString();
		modelMap.put("positnOut",
				positnRoleService.findAllPositn(acct, accountId));// 带有权限控制的仓位
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutConstants.MIS_UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 根据单号进行删除
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkoutService.deleteChkoutm(chkoutm);
	}

	/**
	 * 查找已审核出库单详细信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("searchCheckedChkout")
	public ModelAndView searchCheckedChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 查询已审核出库单
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", chkoutService.findChkoutmById(chkoutm));
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutConstants.SEARCH_CHECKED_CHKOUT, modelMap);
	}

	/**
	 * 出库单打印
	 * 
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		// 出库单打印
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = chkoutService.findChkoutmById(chkoutm);
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		Positn positn = new Positn();
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		positn.setCode(chkoutm.getFirm().getCode());
		parameters.put("firm", positnMapper.findPositnByCode(positn).getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//		parameters.put("typ", chkoutm.getTyp());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadeby());
		parameters.put("checby", chkoutm.getChecby());

		modelMap.put("parameters", parameters);
		modelMap.put("action",
				"/chkout/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutConstants.REPORT_PRINT_URL,
				ChkoutConstants.REPORT_EXP_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	@RequestMapping("success")
	public ModelAndView success() {
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/******************************************************** 出库单end *************************************************/
	
	/******************************************************** 手机报货 到货验收  2015.2.25wjf *************************************************/
	
	/**
	 * 得到所有供应商列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getDeliverListWAP")
	@ResponseBody
	public Object getDeliverListWAP(Deliver deliver,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));//供应商权限
		//关键字查询
		map.put("deliverList", deliverService.findDeliver(deliver, page));
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 得到供应商验货列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableCheckinWAP")
	@ResponseBody
	public Object tableCheckinWAP(Dis dis,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//关键字查询
		dis.setInout("in,dire");
		dis.setChkin("N");
		List<Dis> disList = disService.findAllDis(dis, page);
		map.put("disList", disList);
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 修改验货数量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateDisWAP")
	@ResponseBody
	public Object updateDisWAP(DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result= disService.updateDis(disList);
		return  result;
	}
	
	/**
	 * 验收生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveYsrkChkinmWAP")
	@ResponseBody
	public Object saveYsrkChkinmWAP(Dis dis,String accountName,Date maded1,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		List<Dis> disList = disService.findAllDisForReport(dis);
		if(disList.size()!=0){
			disService.saveYsrkChkinm(disList,dis.getAcct(),dis,dis.getFirmCode(),maded1!=null?maded1:new Date(),accountName);
			map.put("pr", 1);
		}else{
			map.put("pr", 2);//为空，不能进行入库
		}
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 得到配送中心验货列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/diapatchListWAP")
	@ResponseBody
	public Object diapatchListWAP(SupplyAcct sa,String acct,String firmCode,String check, Date bdate, Date edate,Page page,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		HashMap<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("bdate", bdate);
		disMap.put("edate", edate);
		disMap.put("check", check);
		disMap.put("firm", firmCode);
		disMap.put("acct", acct);
		List<SupplyAcct> disList = disService.selectByKey(disMap,page);
		map.put("disList", disList);
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 修改验货数量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAcctWAP")
	@ResponseBody
	public Object updateAcctWAP(SupplyAcct supplyAcct) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		disService.updateAcct(supplyAcct, supplyAcct.getAcct());
		map.put("pr", "succ");
		return  JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 配送中心验收生成入库单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkWAP")
	@ResponseBody
	public Object checkWAP(String ids, String acct ,String accountName,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		disService.check(ids, accountName,acct);
		map.put("pr", 1);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
}
