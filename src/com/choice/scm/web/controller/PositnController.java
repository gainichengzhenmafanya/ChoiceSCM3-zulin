package com.choice.scm.web.controller;

import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.service.system.AssitWebserviceService;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ExplanConstants;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnFirmScm;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;

/**
 * 分店管理
 * @author yp
 *
 */
@Controller
@RequestMapping("positn")
public class PositnController {

	@Autowired
	private PositnService positnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	private AssitWebserviceService assitWebserviceService;
	@Autowired
	private DeliverService deliverService;
	
	/**
	 * 模糊查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return modelMap
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/listDetail")
	public ModelAndView findPositn(ModelMap modelMap, Positn positn, Page page,String descode,String descodeName, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes code = new CodeDes();
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code));
		
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code));
		//供应商权限筛选
		positn.setSta("all");//只有这里查所有分店，其他地方都查sta=Y的  wjf
		//判断是否查询的是分店
		if(positn.getTyp()!=null && "firm".equals(positn.getTyp())){
			positn.setTyp("1203");
		}
		Positn positn2 = new Positn();
		positn2.setCode(positn.getUcode());
		String locale = session.getAttribute("locale").toString();//语言类型
		positn.setLocale(locale);
		modelMap.put("listPositn", positnService.findPositn(positn, page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("descode",descode);
		if(positn.getUcode()!=null && !"".equals(positn.getUcode())){
			modelMap.put("ynUseDept",  positnService.findPositnByCode(positn2).getYnUseDept());//判断是否启用档口
		}
		if(descodeName!=null && !"".equals(descodeName)){
			System.out.println( new String(descodeName.getBytes("iso-8859-1"),"UTF-8"));
//			System.out.println(java.net.URLDecoder.decode(descodeName));
			modelMap.put("descodeName", new String(descodeName.getBytes("iso-8859-1"),"UTF-8") );
		}
		return new ModelAndView(PositnConstants.LIST_POSITN,modelMap);
	}
	
	/**
	 * 
	* @Title: findPositnType 
	* @Description: (分店类型，页面左侧） 
	* @Author：LI Shuai
	* @date：2014-1-22下午2:01:57
	* @param modelMap
	* @param positn
	* @param page
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	@RequestMapping("/list")
	public ModelAndView findPositnType(ModelMap modelMap ,Positn positn,Page page,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String locale = session.getAttribute("locale").toString();
		positn.setLocale(locale);
		List<CodeDes> listPositnType = positnService.findCodedesType(positn);
		
		List<CodeDes> listDeptType = new ArrayList<CodeDes>();//存放部门节点的集合
		//移除部门节点
		for(CodeDes code:listPositnType){
			if(code.getDes().indexOf("1207")>-1 || code.getDes().indexOf("部門") > -1
					|| code.getDes().indexOf("dept") >-1){
				listDeptType.add(code);
			}
		}
		if(listDeptType!=null && listDeptType.size()==1 ){
			listPositnType.removeAll(listDeptType);
		}
		
		//wangjie 2014年11月24日 10:19:14 获取所有分店
		if(listPositnType!=null && listPositnType.size()!=0){//当查询的是分店时 查询条件清空
			positn.setDes("");
		}
		List<Positn> firmList = positnService.selectFirmByCodeOrDes(positn);
		
		//获取所有区域 wangjie 2014年12月24日 12:55:29
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		List<CodeDes> areaList = codeDesService.findCodeDes(codeDes, page);
		
		if(firmList!=null && firmList.size()!=0){
			Positn p = new Positn();
			p.setDes("1203");//添加分店节点
			p.setLocale(locale);
			List<CodeDes> list = positnService.findCodedesType(p);
			if(list!=null && list.size()!=0){
				listPositnType.add(list.get(0));
			}
		}
		
		modelMap.put("areaList", areaList);
		modelMap.put("listPositnType",listPositnType);
		modelMap.put("queryPositn", positn);
		modelMap.put("firmList", firmList);
		return new ModelAndView(PositnConstants.LIST_POSITN_TYPE,modelMap);
		
	}
	/**
	 * 分店仓位选择查询
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/choosePositn")
	public ModelAndView choosePositn(ModelMap modelMap, Positn positn, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findPositn(positn, page));
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		return new ModelAndView(PositnConstants.CHOOSE_POSITN,modelMap);
	}
	
	/**
	 * 查询所有的分店和仓位
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/findAllPositn")
	public ModelAndView findAllPositn(ModelMap modelMap,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		return new ModelAndView(PositnConstants.LIST_POSITN,modelMap);
	}
	
	/**
	 * 查询所有的分店和仓位
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 * @param single 判断是单选  还是多选  1单选 0多选
	 * @param typN 仓位
	 */
	@RequestMapping("/searchAllPositn")
	public ModelAndView searchAllPositn(HttpSession session, ModelMap modelMap, Positn positn, String mis, String single,String tagName,String tagId,String callBack) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
		}
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));//配送片区
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));//经营区域
		codeDes.setTyp(String.valueOf(CodeDes.S_FRIMTYP));//查询门店类型wjf
		modelMap.put("firmtyp", codeDesService.findCodeDes(codeDes, page));
		String locale = session.getAttribute("locale").toString();//语言类型
		codeDes.setLocale(locale);
		codeDes.setTyp("12");
		if(positn.getTypn() != null && !"".equals(positn.getTypn())){
			codeDes.setCode(CodeHelper.replaceCode(positn.getTypn()));
		}
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(codeDes,page));//仓位类型
		//已经选中的仓位
		if(positn.getCode() != null && !"".equals(positn.getCode())){
			modelMap.put("listPositn1", positnService.selectPositnNew(positn));
		}
		//清空positn中的code值，查询所有仓位
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setLocale(locale);
		positn.setCode("");
		modelMap.put("listPositn", positnService.findPositnSuperNOPage(positn));
		
		//查询所有的分店
		String typ = positn.getTyp();
		positn.setTyp("1203");
		modelMap.put("firmList", positnService.findPositnSuperNOPage(positn));
		positn.setTyp(typ);
		
		modelMap.put("positn", positn);
		//add wangjie 2014年10月30日 14:31:23
		modelMap.put("single", single);
		modelMap.put("tagName", tagName);
		modelMap.put("tagId", tagId);
		modelMap.put("callBack", callBack);//回调函数名称
		modelMap.put("mis", mis);
		return new ModelAndView(PositnConstants.SEARCH_ALLPOSITN,modelMap);
	}
	
	/***
	 * ajax查询所有仓位（新公用方法）
	 * @param session
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/searchAllPositnNew")
	@ResponseBody
	public List<Positn> searchAllPositnNew(HttpSession session, Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		positn.setAccountId(session.getAttribute("accountId").toString());
		return positnService.findAllPositn(positn);
	}
	
	/**
	 * 查询所有的分店和仓位----用于配送线路门店选择
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/searchAllPositnPs")
	public ModelAndView searchAllPositnPs(HttpSession session, ModelMap modelMap, Positn positn, String mis,String defaultCode,String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
		}
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_POSITN_TYP));
		modelMap.put("typ", codeDesService.findCodeDes(codeDes, page));
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		positn.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("listPositn", positnService.searchAllPositnPs(positn));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		CodeDes codedes = new CodeDes();
		codedes.setTyp("10");
		modelMap.put("listFirmTyp", codeDesService.findCodeDes(codedes));
		modelMap.put("positn", positn);
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName,"UTF-8"));
		}
		modelMap.put("mis", mis);
		return new ModelAndView(PositnConstants.SEARCH_ALLPOSITN,modelMap);
	}
	
	/**
	 * 根据acct和code查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/table")
	public ModelAndView findPositnByCode(ModelMap modelMap,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positn",positnService.findPositnByCode(positn));
		return new ModelAndView(PositnConstants.TABLE_ACCOUNT,modelMap);
	}
	
	/**
	 * 转到修改页面
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/update")
	public ModelAndView updatePositn(ModelMap modelMap,Positn positn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		CodeDes typCode = new CodeDes();
		String locale = session.getAttribute("locale").toString();
		typCode.setLocale(locale);
		codeDes.setLocale("zh_CN");
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		typCode.setTyp(String.valueOf(CodeDes.S_POSITN_TYP));
		modelMap.put("typ", codeDesService.findCodeDes(typCode, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("syq", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_FRIMTYP));//查询门店类型wjf
		modelMap.put("firmtyps", codeDesService.findCodeDes(codeDes));
		codeDes.setTyp(String.valueOf(CodeDes.S_COST_TYP));//查询成本卡类型
		modelMap.put("costtyps", codeDesService.findCodeDes(codeDes));
		positn.setSta("all");//这里查所有分店，其他地方都查sta=Y的  wjf
		positn.setLocale(locale);
		modelMap.put("positn",positnService.findPositnByCode(positn));
		modelMap.put("firmList",positnService.findPositnOut2());
		//是否是否连接决策库
		return new ModelAndView(PositnConstants.UPDATE_POSITN,modelMap);
	}
	
	/**
	 * 转到复制页面
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/copy")
	public ModelAndView copyPositn(ModelMap modelMap,Positn positn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		CodeDes typCode = new CodeDes();
		String locale = session.getAttribute("locale").toString();
		typCode.setLocale(locale);
		codeDes.setLocale("zh_CN");
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		typCode.setTyp(String.valueOf(CodeDes.S_POSITN_TYP));
		modelMap.put("typ", codeDesService.findCodeDes(typCode, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("syq", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_FRIMTYP));//查询门店类型wjf
		modelMap.put("firmtyps", codeDesService.findCodeDes(codeDes));
		codeDes.setTyp(String.valueOf(CodeDes.S_COST_TYP));//查询成本卡类型
		modelMap.put("costtyps", codeDesService.findCodeDes(codeDes));
		positn.setSta("all");//这里查所有分店，其他地方都查sta=Y的  wjf
		positn.setLocale(locale);
		modelMap.put("positn",positnService.findPositnByCode(positn));
		modelMap.put("firmList",positnService.findPositnOut2());
		return new ModelAndView(PositnConstants.COPY_POSITN,modelMap);
	}
	
	/**
	 * 更新分店和仓位信息
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,Positn positn) throws Exception{
		//租赁版不对接BOH,此代码不再用
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//wangjie 2014年10月28日 17:05:16   增加部门属性
		if(!positn.getTyp().equals("1207")){
			positn.setDeptattr("");
		}else{
			positn.setDeptattr(positn.getDeptattr().split(",")[1]);
		}
		positnService.updatePositn(positn);
		positnService.updateSupply(positn);
		assitWebserviceService.uploadAddress(positn);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 添加分店和仓位信息
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,Positn positn,HttpSession session) throws Exception{
		//租赁版不对接BOH,此代码不再用
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		positn.setAccountId(session.getAttribute("accountId").toString());//操作人账号
		positnService.savePositn(positn);
		assitWebserviceService.uploadAddress(positn);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 转到添加页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/add")
	public ModelAndView add(ModelMap modelMap,String descode,String descodeName,String areacode,String ucode,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		CodeDes typCode = new CodeDes();
		String locale = session.getAttribute("locale").toString();
		typCode.setLocale(locale);
		codeDes.setLocale("zh_CN");
//		codeDes.setLocale(locale);
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		typCode.setTyp(String.valueOf(CodeDes.S_POSITN_TYP));
		modelMap.put("typ", codeDesService.findCodeDes(typCode, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("syq", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_FRIMTYP));//查询门店类型wjf
		modelMap.put("firmtyps", codeDesService.findCodeDes(codeDes));
		codeDes.setTyp(String.valueOf(CodeDes.S_COST_TYP));//查询成本卡类型
		modelMap.put("costtyps", codeDesService.findCodeDes(codeDes));
		modelMap.put("firmList",positnService.findPositnOut2());
		modelMap.put("areacode", areacode);
		modelMap.put("ucode", ucode);
		modelMap.put("descode", descode);
		modelMap.put("descodeName", descodeName==null || "".equals(descodeName)?"":new String(descodeName.getBytes("iso-8859-1"),"UTF-8") );
		return new ModelAndView(PositnConstants.ADD_POSITN,modelMap);
	}
	
	/**
	 * 删除分店和仓位信息
	 * @param ids
	 * @return
	 * @throws Exception 
	 * @author yp
	 */
	@RequestMapping("/deleteByIds")
	public ModelAndView deleteByIds(ModelMap modelMap,String code) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnService.deleteByIds(code);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除分店和仓位信息
	 * @param ids
	 * @return
	 * @throws Exception   
	 * @author yp
	 */
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap modelMap,Positn positn) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positnService.deletePositn(positn);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/selectList")
	public ModelAndView findPositn(ModelMap modelMap,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("queryPositn", positn);
		return new ModelAndView(PositnConstants.SELECT_POSITN,modelMap);
	}
	
	/**
	 * 查询所有出库仓位(主直拨库和基地仓库)
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/findAllPositnOut")
	@ResponseBody
	public Object findPositnOut()throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositnOut();
	}
	
	/**
	 * 查询所有出库仓位(主直拨库和基地仓库，1204)
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/findAllPositnOut1")
	@ResponseBody
	public Object findPositnOut1()throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositnOut1();
	}
	
	/**
	 * 获取所有分店
	 * @return
	 * @throws Exception
	 * @author css
	 */
	@RequestMapping("/findAllPositnOut2")
	@ResponseBody
	public Object findPositnOut2()throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositnOut2();
	}
	
	/**
	 * 1203  多选框
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPositnex")
	public ModelAndView selectPositnex(ModelMap modelMap,String positnex,HttpSession session) throws Exception { 
		Positn positn=new Positn();
		positn.setTyp(PositnConstants.type_5);
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("positn", positnex);//中类
		return new ModelAndView(ExplanConstants.SELECT_POSITN, modelMap);
	}
	
	/**
	 * 查询所有入库仓位
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/findAllPositnIn")
	@ResponseBody
	public Object findPositnIn()throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositnIn();
	}
	
	/**
	 * 获取主直拨库
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/findMainPositn")
	@ResponseBody
	public Object findMainPositn(String psarea)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = new Positn();
		positn.setTyp("1201");
		positn.setPsarea(psarea);
		return positnService.findAllPositn(positn);
	}
	
	/**
	 * 显示选择的仓位
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	/*@RequestMapping(value = "/addPositnBatch")
	public ModelAndView addPositnBatch(ModelMap modelMap, String mis,String defaultName,String defaultCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("mis", mis);
		
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		return new ModelAndView(PositnConstants.ADD_POSITNBATCH, modelMap);
	}*/
	
	/**
	 * 显示选择的仓位（不含基地仓库和主直拨库）
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addPositnBatch1")
	public ModelAndView addPositnBatch1(ModelMap modelMap, String defaultName, String defaultCode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("deliverDirect", "y");
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		return new ModelAndView(PositnConstants.ADD_POSITNBATCH, modelMap);
	}
	
	/**
	 * 选择仓位
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	/*@RequestMapping(value = "/selectPositn")
	public ModelAndView selectPositn(ModelMap modelMap,Positn positn,String mold, String mis, String deliverDirect, String defaultName,String defaultCode, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
			modelMap.put("mis", "y");
		}
		if ("y".equals(deliverDirect)) {
			positn.setDeliverDirect("y");
			modelMap.put("deliverDirect", "y");
		}
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("pageobj", page);
		if("one".equals(mold)){
			return new ModelAndView(PositnConstants.SELECT_ONEPOSITN, modelMap);
		}else {
			return new ModelAndView(PositnConstants.SELECT_POSITN, modelMap);
		}
	}*/
	/**
	 * 
	* @Title: selectPositn 
	* @Description: (选择仓位左侧（选择单一仓位使用）) 
	* @Author：LI Shuai
	* @date：2014-1-23下午4:44:41
	* @param modelMap
	* @param positn
	* @param deliverDirect
	* @param defaultName
	* @param defaultCode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	@RequestMapping(value = "/selectPositn")
	public ModelAndView selectPositn(ModelMap modelMap,Positn positn, String cwqx, String deliverDirect, String defaultName, String defaultCode, String typ,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("firm".equals(typ)) {
			positn.setDes("1203");
		}
		if ("cangku".equals(typ)) {//只查仓库和住直播库 用在直发单填制审核 入库仓位查询 wjf
			positn.setDes("库");
		}
		//基地仓库，主直拨库，分店
		if("123".equals(typ)){
			positn.setDes("123");
		}
		String locale = session.getAttribute("locale").toString();
		positn.setLocale(locale);
		modelMap.put("listPositnType",positnService.findCodedesType(positn));
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("queryPositn", positn);
		modelMap.put("cwqx", cwqx);
		return new ModelAndView(PositnConstants.LIST_POSITN_TYPE_FORSE,modelMap);
		
	}
	
	/**
	 * 
	* @Title: selectPositn 
	* @Description: (选择仓位左侧（选择单一仓位使用）) ====用于预估系统模块
	* @Author：LI Shuai
	* @date：2014-1-23下午4:44:41
	* @param modelMap
	* @param positn
	* @param deliverDirect
	* @param defaultName
	* @param defaultCode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	@RequestMapping(value = "/selectPositnFirm")
	public ModelAndView selectPositnFirm(ModelMap modelMap,Positn positn, String deliverDirect, String defaultName, String defaultCode, String typ)throws Exception{
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("firm".equals(typ)) {
			positn.setDes("1203");
		}
		modelMap.put("listPositnType",positnService.findCodedesTypeFirm(positn));
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("queryPositn", positn);
		return new ModelAndView(PositnConstants.LIST_POSITN_TYPE_FORSE_FIRM,modelMap);
		
	}
	
	/**
	 * 
	* @Title: findPositnForse 
	* @Description: (选择仓位右侧（选择单一仓位使用）)  ====用于预估系统模块
	* @Author：LI Shuai
	* @date：2014-1-23下午5:12:59
	* @param modelMap
	* @param positn
	* @param page
	* @param descode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	
	@RequestMapping("/listDetailForseFirm")
	public ModelAndView listDetailForseFirm(ModelMap modelMap, HttpSession session, Positn positn, Page page, String descode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("descode",descode);
		return new ModelAndView(PositnConstants.LIST_POSITN_FORSE_FIRM,modelMap);
	}
	
	/**
	 * 
	* @Title: findPositnForse 
	* @Description: (选择仓位右侧（选择单一仓位使用）) 
	* @Author：LI Shuai
	* @date：2014-1-23下午5:12:59
	* @param modelMap
	* @param positn
	* @param page
	* @param descode
	* @return
	* @throws Exception  ModelAndView
	* @throws
	 */
	
	@RequestMapping("/listDetailForse")
	public ModelAndView findPositnForse(ModelMap modelMap, HttpSession session, Positn positn, Page page, String descode, String cwqx) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		if (cwqx==null && "".equals(cwqx)){
			positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		}
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		modelMap.put("pageobj", page);
		modelMap.put("descode",descode);
		return new ModelAndView(PositnConstants.LIST_POSITN_FORSE,modelMap);
	}
	
	
	
	
	/**
	 * ajax查询分店信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findAllPositnN", method=RequestMethod.POST)
	@ResponseBody
	public List<Positn> findAllPositnN(Positn positn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		 return positnService.findPositnN(positn);
	}
	
	/**
	 * 选择仓位
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectFirm")
	public ModelAndView selectFirm(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findPositnOut());
		return new ModelAndView(PositnConstants.SELECT_FIRM, modelMap);

	}
	
	/**
	 * 查询货架
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping("findPositn1")
	@ResponseBody
	public Object findPositn1() throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return positnService.findPositn1();
	}
	
	/**
	 * 查询编码是否重复
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findPositnByCode")
	@ResponseBody
	public Object findPositnByCode(String code,String des)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn = new Positn();
		positn.setSta("all");//wjf
		positn.setCode(code);
		positn.setDes(des);
		return positnService.findPositnByCode(positn);
	}
	
	
	//----------------------------------------------------最新公用
	//-------------------------不推荐使用------------使用方法		findPositnSuper		代替
	/**
	 * 查询分店 增加类别限制
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPositnByTyp")
	public ModelAndView selectPositnByTyp(ModelMap modelMap,Positn positn,String mold, String mis, String deliverDirect, String defaultName,String defaultCode, Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("y".equals(mis)) {
			positn.setTyp("1203");
			modelMap.put("mis", "y");
		}
		if ("y".equals(deliverDirect)) {
			positn.setDeliverDirect("y");
			modelMap.put("deliverDirect", "y");
		}
		if(positn.getTyp()!=null&&positn.getTyp()!=""){
			try {
				Field f=PositnConstants.class.getField(positn.getTyp());//通过映射获取类别typ可以传type_1、type_2...
				positn.setTyp(f.get(null).toString());//具体单考PositnConstants类。。
			} catch (Exception e) {
				//如果报错，则默认不修改
			}
		}
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		modelMap.put("listPositn", positnService.findPositn(positn,page));
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("pageobj", page);
		if("one".equals(mold)){
			return new ModelAndView(PositnConstants.SELECT_ONEPOSITN, modelMap);
		}else if("oneTone".equals(mold)){
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_ONEPOSITN, modelMap);
		}else if("oneTmany".equals(mold)){
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_MANYPOSITN, modelMap);
		}else{
			return new ModelAndView(PositnConstants.SELECT_POSITN, modelMap);
		}
	}
	/**
	 * 查询仓位属性
	 * @param modelMap
	 * @param page
	 * @param session
	 * @param sta
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findTypByPositn")
	@ResponseBody
	public String findTypByPositn(ModelMap modelMap,HttpSession session,Positn positn,Supply supply) throws Exception{
		String result="RK";
		positn = positnService.findPositnByCode(positn);
		String typ = positn.getPcode();//typ放到pcode里了。。。
		if("1201".equals(typ)||"1202".equals(typ)){
			return "RK";
		}else{//仓位是分店，判断是不是出库方向，只有出库方向才查售价
			//1.查物资是否设置了直发
			supply = supplyService.findSupplyById(supply);
			if("Y".equals(supply.getInout())){
				return "RK";
			}else{
				//2.查配送片区默认仓位 如果是基地仓库，则是出库 ，如果为空，则查supply的sp_position
				Positn spcodemod = supplyService.findSpcodeMod(supply.getSp_code(),positn.getCode());
				if(null == spcodemod){
					Positn positn1 = new Positn();
					positn1.setCode(supply.getSp_position());
					spcodemod = positnService.findPositnByCode(positn1);
				}else{
					spcodemod.setPcode(spcodemod.getTyp());
				}
				if(null != spcodemod && "1202".equals(spcodemod.getPcode())){
					return "CK";
				}
			}
		}
		return result;
	}
	
	/**
	 * 根据分店编码code 获取分店信息并返回json对象   wangjie 2014年10月30日 13:25:27
	 * @param Positn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findById")
	@ResponseBody
	public Object findById(Positn positn,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return  positnService.findPositnByCode(positn);
	}
	
	/** 
	 * 严禁修改     严禁修改     严禁修改     严禁修改      严禁修改   严禁修改   严禁修改
	 * 查询仓位信息公共 。。。。。。。。。。。。。。。。。。。。。。。。。2015.1.8后 所有方法都可以走 2015.1.8  xlh    严禁修改
	 *  控制typ值就行     比如   查询 基地  仓位和加工间     typ= “typ in （基地仓位，1204）”
	 *  mold  是否多选    one  是多选， 其它为
	 *  iffirm 是否为分店  （mis报表中 查询分店的档口专用 wangjie）
	 */
	@RequestMapping(value = "/findPositnSuper")
	public ModelAndView findPositnSuper(ModelMap modelMap,Positn positn,String mold, String defaultName,String defaultCode, Page page,String iffirm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAccountId(session.getAttribute("accountId").toString());
		positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
//		String positninit=positn.getTypn();
		
		if(positn.getTypn().equals("1")){
			positn.setTypn("'1201'");
		}else  if(positn.getTypn().equals("2")){
			positn.setTypn("'1202'");
		}else  if(positn.getTypn().equals("3")){
			positn.setTypn("'1203'");
		}else  if(positn.getTypn().equals("4")){
			positn.setTypn("'1204'");
		}else  if(positn.getTypn().equals("5")){
			positn.setTypn("'1205'");
		}else  if(positn.getTypn().equals("6")){
			positn.setTypn("'1206'");
		}else  if(positn.getTypn().equals("7")){
			positn.setTypn("'1207'");
		}else  if(positn.getTypn().equals("1-2-4")){// 以下照着这个去写
			positn.setTypn("'1201','1202','1204'");
		}else  if(positn.getTypn().equals("1-2-3")){
			positn.setTypn("'1201','1202','1203'");
		}else  if(positn.getTypn().equals("2-4")){
			positn.setTypn("'1202','1204'");
		}else  if(positn.getTypn().equals("3-7")){
			positn.setTypn("'1203','1207'");
		}else  if(positn.getTypn().equals("1-5")){
			positn.setTypn("'1201','1202','1203','1204','1205'");
		}else  if(positn.getTypn().equals("1-2-3-4-5-6-7")){
			positn.setTypn("'1201','1202','1203','1204','1205','1206','1207'");
		}
		
		List<Positn> positns = new ArrayList<Positn>();
		if(!"".equals(iffirm) && iffirm != null){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				positn.setUcode(accountPositn.getPositn().getCode());
			}
			Positn positn2 = new Positn();
			positn2.setCode(accountPositn.getPositn().getCode());
			positn2.setTypn("'1203'");
			positn2.setDes(positn.getDes());
			positn2.setInit(positn.getInit());
			List<Positn> list = positnService.findPositnSuper(positn2,page);
			if(list !=null && list.size() == 1){
				positns.add(list.get(0));
			}
		}
		
		CodeDes code = new CodeDes();
		Page p = new Page();
		p.setPageSize(Integer.MAX_VALUE);
		code.setTyp(String.valueOf(CodeDes.S_SYQ));
		modelMap.put("listMod", codeDesService.findCodeDes(code, p));
		code.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("listArea", codeDesService.findCodeDes(code, p));
		List<Positn> listPositn = positnService.findPositnSuper(positn,page);
		positns.addAll(listPositn);
		modelMap.put("listPositn", positns);
		modelMap.put("listPositnTyp", positnService.findAllPositnTyp());
		modelMap.put("queryPositn", positn);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("pageobj", page);
		modelMap.put("iffirm", iffirm);
		if("one".equals(mold)){//选择单条仓位
			return new ModelAndView(PositnConstants.SELECT_ONEPOSITN_SUPER, modelMap);
		}else if("oneTone".equals(mold)){//同一类别仓位单选
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_ONEPOSITN_SUPER, modelMap);
		}else if("oneTmany".equals(mold)){//同一类别仓位多选
			return new ModelAndView(PositnConstants.SELECT_ONE_TO_MANYPOSITN_SUPER, modelMap);
		}else{
			return new ModelAndView(PositnConstants.SELECT_POSITN_SUPER, modelMap);
		}
	}
	
	/****
	 * 判断指定仓位有没有做期初
	 * @author wjf
	 * @param positn
	 * @return
	 */
	@RequestMapping("/checkQC")
	@ResponseBody
	public boolean checkQC(HttpSession session,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null == positn.getCode() || "".equals(positn.getCode())){
			return false;
		}
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		Positn positn1 = positnService.findPositnByCode(positn);
		if(null != positn1){
			if ("Y".equals(positn1.getQc())) {
				return true;
			}else {
				return false;
			}
		}else{
			return false;
		}
	}
	
	
	
	/**
	 * 跳转到组织结构选择界面list
	 * @param modelMap
	 * @param department
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseDepartmentList")
	public ModelAndView queryFirm(ModelMap modelMap, Positn positn,HttpSession session,String single,String callBack,String domId,String type,Page page,String pk_orgs,String pk_material) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
//		positn.setPk_material(pk_material);
//		if(!"admin".equals(session.getAttribute("loginName"))){//如果不是admin 就走权限控制
//			positn.setPk_accountid(session.getAttribute("accountId").toString());
//		}
//		if(null!=pk_orgs && !"".equals(pk_orgs)){
//			positn.setPk_orgs("'"+pk_orgs.replace(",", "','")+"'");
//		}
		
		modelMap.put("positnList", positnService.queryFirm(positn,page));
		
		Page pager = new Page();
		pager.setMaxSize(Integer.MAX_VALUE);
//		Department qryd = new Department();
//		qryd.setAcct(session.getAttribute("ChoiceAcct").toString());
//		modelMap.put("orgStructureList", positnService.queryFirm(qryd,pager));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		modelMap.put("department", positn);
		modelMap.put("pk_orgs", pk_orgs);
		modelMap.put("pk_material", pk_material);
		return new ModelAndView(PositnConstants.CHOOSE_DEPARTMENTLIST, modelMap);
	}
	
	/**
	 * 跳转到BOH2SCM绑定页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toConnectDept")
	public ModelAndView toConnectDept(Page page,Positn positn,ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		//查询所有BOH档口
		page.setPageSize(Integer.MAX_VALUE);//改成不分页的
		List<Positn> bohDeptList = positnService.getBOHDeptList(page, positn);
		modelMap.put("positn", positn);
		PositnFirmScm positnFirmScm=new PositnFirmScm();
		positnFirmScm.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnFirmScm.setPositn(positn.getCode());
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("PositnFirm", positnService.FindPositnFirmScmBy(positnFirmScm));//查询对应关系对象返回对象
		modelMap.put("bohDeptList", bohDeptList);
		modelMap.put("pageobj", page);
		return new ModelAndView(PositnConstants.RELATION_DEPT,modelMap);
	}
	
	/**
	 * 处理绑定
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/doAddConnectDept")
	public ModelAndView doAddConnectDept(ModelMap modelMap,Positn positn,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		positnService。doAddConnectDept(positn);
		String acct = session.getAttribute("ChoiceAcct").toString();
		positn.setAcct(acct);
		positnService.doAddConnectDept(positn,page);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 跳转到同步虚拟供应商页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/setDeliver")
	public ModelAndView setDeliver(ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//查询所有未设置虚拟供应商的朱直拨库，基地仓库，加工间，分店，其他
		List<Positn> positnList = positnService.findPositnNoSetDeliver();
		if(positnList.size() > 0){
			Positn first = positnList.get(0);
			for(int i = 1;i < positnList.size(); i++){
				if(first.getTyp().equals(positnList.get(i).getTyp())){
					positnList.get(i).setTyp("");
				}else{
					first = positnList.get(i);
				}
			}
		}
		modelMap.put("positnList", positnList);
		//查找供应商类型
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		return new ModelAndView(PositnConstants.SETDELIVER,modelMap);
	}
	
	/****
	 * 保存虚拟供应商
	 * @author wjf
	 * @param positn
	 * @return
	 */
	@RequestMapping("/saveDeliver")
	@ResponseBody
	public String saveDeliver(HttpSession session,Positn positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> map = new HashMap<String,String>();
		List<String> codes = new ArrayList<String>();
		for(Deliver d : positn.getDeliverList()){
			Deliver newd = new Deliver();
			newd.setCode(d.getCode());
			newd = deliverService.findDeliverByCode(newd);
			if(newd != null){
				codes.add(d.getCode());
			}else{
				deliverService.saveDeliver(d);
			}
		}
		if(codes.size() != 0){
			map.put("pr", "-1");
			map.put("error", codes.toString());
		}else{
			map.put("pr", "1");
		}
		JSONObject rs = JSONObject.fromObject(map);
		return rs.toString();
	}
}
