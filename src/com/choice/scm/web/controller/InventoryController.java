package com.choice.scm.web.controller;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.InventoryConstants;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.InventoryService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * 盘点   日盘   周盘   月盘    存货盘点表    月盘点差异报告
 */
@Controller
@RequestMapping(value = "inventory")
public class InventoryController {
	
	@Autowired
	private InventoryService inventoryService;
//	@Autowired
//	private InventoryDemomService inventoryDemomService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private Page pager;
	@Autowired
	private LogsMapper logsMapper;
	private final static int PAGE_SIZE = 40;
	
	/**
	 * 仓库盘点主跳转
	 * @return pd=start  开始盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllInventory(ModelMap modelMap, HttpSession session, String action, Inventory inventory,
			String status, String type, String chkstodemono) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"总部查询盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Positn positn = inventory.getPositn();
		Positn p = new Positn();//仓位属性
		if(null != action && "init".endsWith(action)){//初始
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			String month = mainInfoService.findMainInfo(Integer.parseInt(yearr)).getMonthh();
			inventory.setYearr(yearr);
			inventory.setMonth(month);
			modelMap.put("action", "init");
		}else{//查询
			String acct = session.getAttribute("ChoiceAcct").toString();
			if(null != status && "start".endsWith(status)){//开始盘点
				p.setAcct(acct);
				p.setOldcode(inventory.getPositn().getCode());
				p.setPd("N");
				positnService.updatePositn(p);
				positn.setPd("N");
			}else{
				positn.setAcct(acct);
				positn.setCode(inventory.getPositn().getCode());
				p = positnService.findPositnByCode(positn);
				positn.setPd(p.getPd());//  查询是否盘点				
			}
			inventory.setAcct(acct);
			inventory.setStartNum(0);
			inventory.setEndNum(PAGE_SIZE);
			modelMap.put("inventoryList", inventoryService.findAllInventoryPage(inventory));		//查询盘点列表
			int totalCount = inventoryService.findAllInventoryCount(inventory);
			modelMap.put("totalCount", totalCount);
			
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
			}
			modelMap.put("currPage", 1);
			modelMap.put("pageSize", PAGE_SIZE);
			modelMap.put("disJson", JSONObject.fromObject(inventory));
		}
		if(null!=type && ("type".endsWith(type)||"dept".endsWith(type))){//分店标志
			modelMap.put("type",type);//分店标志 
			//档口盘点特有
			Positn deptpositn= new Positn();
			if("dept".endsWith(type) && null!=inventory.getPositn()){
				deptpositn=inventory.getPositn();
			}
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null != accountPositn.getPositn()){
				positn.setCode(accountPositn.getPositn().getCode());
				positn.setDes(accountPositn.getPositn().getDes());
				inventory.setPositn(positn);
			}
			//档口盘点特有
			if("dept".endsWith(type) && null!=deptpositn){
				modelMap.put("firmcode", inventory.getPositn().getCode());
				modelMap.put("firmname", inventory.getPositn().getDes());
				inventory.setPositn(deptpositn);
			}
			
		}
		
		modelMap.put("inventory", inventory);
		
		if (type != null && "type".equals(type)) {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY_MIS, modelMap);
		} else if (type != null && "dept".equals(type)) {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY_DEPT, modelMap);
		} else {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY, modelMap);
		}
	}
	
	/**
	 * 仓库盘点Ajax加载
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session,Inventory inventory,String currPage,String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventory.setStartNum(intCurrPage*PAGE_SIZE);
		inventory.setEndNum((intCurrPage+1)*PAGE_SIZE);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		modelMap.put("inventoryList", inventoryService.findAllInventoryPage(inventory));
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/**
	 * 加工间盘点Ajax加载
	 * @throws Exception
	 */
	@RequestMapping(value = "/listExAjax")
	@ResponseBody
	public Object listExAjax(HttpSession session,Inventory inventory,String currPage,String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventory.setStartNum(intCurrPage*PAGE_SIZE);
		inventory.setEndNum((intCurrPage+1)*PAGE_SIZE);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		modelMap.put("inventoryList", inventoryService.findExInventoryPage(inventory));
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
//	/**
//	 * 盘点模板调用
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/addInvendemo")
//	public ModelAndView addInvendemo(ModelMap modelMap, HttpSession session, Page page, InventoryDemom inventorydemom) throws Exception
//	{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		inventorydemom.setAcct(session.getAttribute("ChoiceAcct").toString());
//		modelMap.put("pageobj", page);
//		List<InventoryDemom> listInventoryDemom = inventoryDemomService.listInvenDemomByFirm(inventorydemom, page);
//		modelMap.put("listInventoryDemom", listInventoryDemom);
//		modelMap.put("inventory.positn.code", inventorydemom.getFirm());
//		return new ModelAndView(InventoryConstants.ADD_INVENTORY_DEMO, modelMap);
//	}
	
//	/**
//	 * 申购模板批量添加报货单
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping(value = "/addChkstodemoForFirm")
//	public ModelAndView addChkstodemoFirm(ModelMap modelMap,HttpSession session,Page page,Chkstodemo chkstodemo) throws Exception
//	{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());	
//		Account account=accountMapper.findAccountById(session.getAttribute("accountId").toString());
//		if(account.getPositn()!=null){
//			chkstodemo.setFirm(account.getPositn().getCode());
//		}
//		modelMap.put("pageobj", page);
//		modelMap.put("titleList", chkstodemoService.findAllTitle(chkstodemo));
//		modelMap.put("chkstodemoList", chkstodemoService.findChkstodemoForFirm(chkstodemo,page));
//		return new ModelAndView(ChkstodemoConstants.MIS_ADD_CHKSTODEMO,modelMap);
//	}
	
	/**
	 * 保存盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateInventory")
	@ResponseBody
	public String updateInventory(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"总部保存盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		inventory.setYearr(yearr);
		inventoryService.updateInventory(inventory,acct);
		return "success";
	}
	
	/**
	 * 结束盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/endInventory")
	@ResponseBody
	public String endInventory(HttpSession session, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"总部结束盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		//1.先判断有没有结束盘点
		Positn p = new Positn();
		p.setAcct(acct);
		p.setCode(inventory.getPositn().getCode());
		Positn positn = positnService.findPositnByCode(p);
		if("Y".equals(positn.getPd())){//已经结束
			return null;
		}
		//2.先更新仓位盘点状态
		p.setPd("Y");
		p.setCode(null);
		p.setOldcode(inventory.getPositn().getCode());
		positnService.updatePositn(p);//更新pd状态
		
		String accountName = session.getAttribute("accountName").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		inventory.setAcct(acct);
		inventory.setYearr(yearr);
		if ("Y".equals(inventory.getYndaypan())) {
			inventoryService.saveInventory(inventory);
		}
		List<Inventory> listInventory=inventoryService.findAllInventory(inventory);	
		//保存盘点表 2014.12.9wjf
		inventoryService.savePositnPan(accountName,inventory,listInventory);
		return inventoryService.endInventory(accountName,inventory,listInventory);//生成盘亏出库，盘盈入库
	}
	
	/***
	 * 查询未结束盘点仓位
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/noInventoryPostin")
	public ModelAndView noInventoryPostin(ModelMap modelMap,String typn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn=new Positn();
		positn.setPd("N");
		if(null != typn && !"".equals(typn)){
			positn.setTypn(typn);
		}else{
			positn.setTypn("'1202','1204'");
		}
		List<Positn> listPositn = positnService.findAllPositn(positn);
		modelMap.put("ListPositn", listPositn);
		return new ModelAndView(InventoryConstants.LIST_NO_INVENTORY_POSITN,modelMap);
	}

	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportInventory")
	@ResponseBody
	public boolean exportInventory(HttpServletResponse response,HttpServletRequest request,HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		inventory.setYearr(yearr);
		List<Inventory> inventorys = inventoryService.findAllInventory(inventory);
		String fileName = "仓库盘点表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return inventoryService.exportInventory(response.getOutputStream(), inventorys,inventory.getPositn().getDes());
	}

	/***
	 * 导出
	 * @param response
	 * @param request
	 * @param session
	 * @param inventory
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportExInventory")
	@ResponseBody
	public boolean exportExInventory(HttpServletResponse response,HttpServletRequest request,HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		inventory.setYearr(yearr);
		inventory.setEndNum(Integer.MAX_VALUE);
		List<Inventory> inventorys = inventoryService.findExInventoryPage(inventory);
		String fileName = "仓库盘点表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return inventoryService.exportInventory(response.getOutputStream(), inventorys,inventory.getPositn().getDes());
	}
	
	/***
	 * 打印盘点表
	 * @author wjf
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printInventory")
	public ModelAndView printInventory(ModelMap modelMap, Page pager, HttpSession session,String type, Inventory inventory)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		List<Inventory> inventorys = inventoryService.findAllInventory(inventory);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn.code",inventory.getPositn().getCode());
		params.put("positn.des",inventory.getPositn().getDes());
		params.put("month",inventory.getMonth());
		params.put("supply.sp_code",inventory.getSupply().getSp_code());
		params.put("supply.sp_type",inventory.getSupply().getSp_type());
		modelMap.put("actionMap", params);
		
		modelMap.put("List",inventorys);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "仓库盘点表");
	    parameters.put("positn",inventory.getPositn().getDes());
	    parameters.put("month", inventory.getMonth());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/inventory/printInventory.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_INVENTORY,InventoryConstants.REPORT_PRINT_URL_INVENTORY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/***
	 * 打印盘点表
	 * @author wjf
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printExInventory")
	public ModelAndView printExInventory(ModelMap modelMap, Page pager, HttpSession session,String type, Inventory inventory)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		inventory.setEndNum(Integer.MAX_VALUE);
		List<Inventory> inventorys = inventoryService.findExInventoryPage(inventory);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn.code",inventory.getPositn().getCode());
		params.put("positn.des",inventory.getPositn().getDes());
		params.put("month",inventory.getMonth());
		params.put("supply.sp_code",inventory.getSupply().getSp_code());
		params.put("supply.sp_type",inventory.getSupply().getSp_type());
		params.put("date", new SimpleDateFormat("yyyy-MM-dd").format(inventory.getDate()));
		modelMap.put("actionMap", params);
		
		modelMap.put("List",inventorys);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "仓库盘点表");
	    parameters.put("positn",inventory.getPositn().getDes());
	    parameters.put("month", inventory.getMonth());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/inventory/printExInventory.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_INVENTORY,InventoryConstants.REPORT_PRINT_URL_INVENTORY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/*****************存货盘点表 （日周月）start********************/
	/**
	 * 跳转存货盘点表分析
	 */
	@RequestMapping("/toStockList")
	public ModelAndView toStock(ModelMap modelMap,String checkMis){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("type", checkMis);
		modelMap.put("reportName", InventoryConstants.STOCK_TABLE);
		return new ModelAndView(InventoryConstants.LIST_STOCK,modelMap);
	}
	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findStockHeaders")
	@ResponseBody	
	public Object getStockHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(InventoryConstants.STOCK_TABLE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, InventoryConstants.STOCK_COLUMN));
		columns.put("frozenColumns", InventoryConstants.FROZEN_STOCK);
		return columns;
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseStock")
	public ModelAndView toColChooseStock(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(InventoryConstants.STOCK_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", InventoryConstants.STOCK_TABLE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,InventoryConstants.STOCK_COLUMN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 判断该分店是否有该物资
	 */
	@RequestMapping("/checkSupply")
	@ResponseBody
	public Object checkSupply(HttpSession session,String sp_code) throws CRUDException{
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setPositn(accountPositn.getPositn().getCode());
		supplyAcct.setSp_code(sp_code);
		boolean flag = inventoryService.checkSupply(supplyAcct);
		return flag;
	}
	
	/**
	 * 查询存货盘点表分析分析
	 */
	@RequestMapping("/findStock")
	@ResponseBody
	public Object findStock(ModelMap modelMap,HttpSession session,String page,String rows,SupplyAcct supplyAcct,String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		return inventoryService.findStock(supplyAcct, pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportStock")
	@ResponseBody
	public void exportStock(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "存货盘点表分析分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(InventoryConstants.STOCK_TABLE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		exportExcelMap.creatWorkBook(response.getOutputStream(), inventoryService.findStock(supplyAcct,pager).getRows(), "存货盘点表分析分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, InventoryConstants.STOCK_COLUMN));	
	}

	/***
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param page
	 * @param rows
	 * @param supplyAcct
	 * @param checkMis
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printStock")
	public ModelAndView printStock(ModelMap modelMap, Page pager, HttpSession session, String type,
			String page, String rows, SupplyAcct supplyAcct,String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("sp_code", supplyAcct.getSp_code());
		parameters.put("madeby", session.getAttribute("accountName").toString());
		modelMap.put("List", inventoryService.findStock(supplyAcct, pager).getRows());
	    parameters.put("report_name", "存货盘点表分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/inventory/printStock.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_STOCK, InventoryConstants.REPORT_PRINT_URL_STOCK);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}	
	
	
	/*****************存货盘点表 （日周月）end********************/
	/*****************月盘点差异报告start********************/
	/**
	 * 跳转到月盘点差异报告页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/listMonthPd")
	public ModelAndView listMonthPd(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", InventoryConstants.BASICINFO_REPORT_MONTHPd);
		return new ModelAndView(InventoryConstants.REPORT_SHOW_MONTHPd);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMonthPdHeaders")
	@ResponseBody
	public Object getInventoryAgingSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		dictColumns.setTableName(InventoryConstants.TABLE_NAME_MONTHPd);
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, InventoryConstants.BASICINFO_REPORT_MONTHPd));

		columns.put("frozenColumns", InventoryConstants.BASICINFO_REPORT_MONTHPd_FROZEN);
		return columns;
	}
	
	/**
	 * 查询表格信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMonthPdSum")
	@ResponseBody
	public Object findInventoryAgingSum(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct, String monthFrom, String monthTo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//仓位
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return inventoryService.findMonthPdSum(condition, pager);
	}
	
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseMonthPd")
	public ModelAndView toColChooseMonthPd(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(InventoryConstants.TABLE_NAME_MONTHPd);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName",InventoryConstants.TABLE_NAME_MONTHPd);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,InventoryConstants.BASICINFO_REPORT_MONTHPd));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportMonthPd")
	@ResponseBody
	public void exportMonthPd(HttpServletResponse response,String sort,String order,HttpServletRequest request,String bysale,HttpSession session,SupplyAcct supplyAcct,String monthFrom,String monthTo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月盘点差异报告";
		Map<String,Object> condition = new HashMap<String,Object>();
		//仓位
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(accountPositn.getPositn().getCode());
			}
		supplyAcct.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(InventoryConstants.TABLE_NAME_MONTHPd);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), inventoryService.findMonthPdSum(condition, pager).getRows(), "月盘点差异报告", dictColumnsService.listDictColumnsByTable(dictColumns));		
	}
	
	/**
	 * 打印报表
	 */
	@RequestMapping(value = "/printMonthPd")
	public ModelAndView printMonthPd(ModelMap modelMap,Page pager,HttpSession session,String type,String sort,String order,SupplyAcct supplyAcct,String monthFrom,String monthTo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();//回调参数
		params.put("positn",supplyAcct.getPositn());//回调参数
		params.put("sp_code",supplyAcct.getSp_code());//回调参数
		params.put("typ",supplyAcct.getTyp());//回调参数
		params.put("sort",sort);//回调参数
		params.put("order",order);//回调参数
		params.put("monthFrom", monthFrom);
		params.put("monthTo", monthTo);
		modelMap.put("actionMap", params);//回调参数
		
		Map<String,Object> condition = new HashMap<String,Object>();//报表list
		condition.put("supplyAcct", supplyAcct);//报表list
		condition.put("sort", sort);//报表list
		condition.put("order", order);//报表list
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
	    modelMap.put("List",inventoryService.findMonthPdSum(condition, pager).getRows());//报表list
	    HashMap<Object,Object>  parameters = new HashMap<Object,Object>();//报表map    
	    parameters.put("report_name", "月盘点差异报告");//报表map 
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("monthFrom", monthFrom);
	    parameters.put("monthTo", monthTo);  
	    modelMap.put("parameters", parameters);//报表map   
	 	modelMap.put("action", "/inventory/printMonthPd.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_MONTHPD,InventoryConstants.REPORT_PRINT_URL_MONTHPD);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/*****************月盘点差异报告  end********************/
	
	/**
	 * 安卓盘点机  盘点保存   
	 */
	@RequestMapping(value = "/saveByAddOrUpdateWAP")
	@ResponseBody
	public Object saveByAddOrUpdateWAP(HttpSession session,String inventoryStrjson,String sta) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
//		Map<String, Object> dataSupply = new HashMap<String, Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		// 将json字符串转换成jsonObject
		JSONObject jsonObject = JSONObject.fromObject(inventoryStrjson);
		@SuppressWarnings("rawtypes")
		Iterator it = jsonObject.keys();
		// 遍历jsonObject数据，添加到Map对象
		while (it.hasNext()) {
			String key = String.valueOf(it.next());
			Object value = (Object) jsonObject.get(key);
			data.put(key, value);
		}
	  String  positnCode=  data.get("positn").toString();
	  String  supplyJson=  data.get("supplies").toString();	 
	  
	  Positn positn=new Positn();
	  positn.setCode(positnCode);
	  List<Inventory> inventoryList=new ArrayList<Inventory>();
	   
	  JSONArray jsonObjectSupply= JSONArray.fromObject(supplyJson) ;
	   for (int j = 0; j < jsonObjectSupply.size(); j++) {
		   JSONObject   jsonobjec=(JSONObject)jsonObjectSupply.get(j);
		     Inventory inventory1=new Inventory();
			  Supply supply=new Supply();
			  supply.setSp_code(jsonobjec.get("sp_code").toString());
			  inventory1.setSupply(supply);
			  inventory1.setCnttrival(Double.parseDouble(jsonobjec.get("sp_count").toString()));
			  inventory1.setCntutrival(Double.parseDouble(jsonobjec.get("sp_count").toString()));
			  inventoryList.add(inventory1);
	}
//		String acct=session.getAttribute("ChoiceAcct").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		Inventory inventory=new Inventory();
		inventory.setPositn(positn);
		inventory.setAcct("1");
		inventory.setYearr(yearr);
		inventory.setInventoryList(inventoryList);
		try {
			inventoryService.updateInventory(inventory,"1");	
			map.put("pr", "1");
		} catch (Exception e) {
			// TODO: handle exception
			map.put("pr", "-1");
		}
		return JSONObject.fromObject(map).toString();
	}
	
	//=======================================加工间盘点=============================================
	/**
	 * 仓库盘点主跳转
	 * @return pd=start  开始盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/listExInventory")
	public ModelAndView listExInventory(ModelMap modelMap, HttpSession session, String action, Inventory inventory,
			String status, String type, String chkstodemono) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"总部查询盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Positn positn = inventory.getPositn();
		Positn p = new Positn();//仓位属性
		if(null != action && "init".endsWith(action)){//初始
			String yearr = mainInfoService.findYearrList().get(0);//会计年
			String month = mainInfoService.findMainInfo(Integer.parseInt(yearr)).getMonthh();
			inventory.setDate(new Date());
			inventory.setYearr(yearr);
			inventory.setMonth(month);
			modelMap.put("action", "init");
		}else{//查询
			String acct = session.getAttribute("ChoiceAcct").toString();
			if(null != status && "start".endsWith(status)){//开始盘点
				p.setAcct(acct);
				p.setOldcode(inventory.getPositn().getCode());
				p.setPd("N");
				positnService.updatePositn(p);
				positn.setPd("N");
			}else{
				positn.setAcct(acct);
				positn.setCode(inventory.getPositn().getCode());
				p = positnService.findPositnByCode(positn);
				positn.setPd(p.getPd());//  查询是否盘点				
			}
			inventory.setAcct(acct);
			inventory.setStartNum(0);
			inventory.setEndNum(PAGE_SIZE);
			modelMap.put("inventoryList", inventoryService.findExInventoryPage(inventory));		//查询盘点列表
			int totalCount = inventoryService.findAllInventoryCount(inventory);
			modelMap.put("totalCount", totalCount);
			//分页
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
			}
			modelMap.put("currPage", 1);
			modelMap.put("pageSize", PAGE_SIZE);
			modelMap.put("disJson", JSONObject.fromObject(inventory));
		}
		if(null!=type && ("type".endsWith(type)||"dept".endsWith(type))){//分店标志
			modelMap.put("type",type);//分店标志 
			//档口盘点特
			Positn deptpositn= new Positn();
			if("dept".endsWith(type) && null!=inventory.getPositn()){
				deptpositn=inventory.getPositn();
			}
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null != accountPositn.getPositn()){
				positn.setCode(accountPositn.getPositn().getCode());
				positn.setDes(accountPositn.getPositn().getDes());
				inventory.setPositn(positn);
			}
			//档口盘点特
			if("dept".endsWith(type) && null!=deptpositn){
				modelMap.put("firmcode", inventory.getPositn().getCode());
				modelMap.put("firmname", inventory.getPositn().getDes());
				inventory.setPositn(deptpositn);
			}
		}
		//传值到前台
		modelMap.put("inventory", inventory);
		//判断跳转页面
		if (type != null && "type".equals(type)) {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY_MIS, modelMap);
		} else if (type != null && "dept".equals(type)) {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY_DEPT, modelMap);
		} else {
			return new ModelAndView(InventoryConstants.LIST_EXINVENTORY, modelMap);
		}
	}
	
	/**
	 * 结束盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/endExInventory")
	@ResponseBody
	public String endExInventory(HttpSession session, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,"总部结束盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		//审核人
		String accountName = session.getAttribute("accountName").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		//判断加工间是否已经日盘过
		//日盘日期
		Date rpdate = inventory.getDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(rpdate);
		//cal.add(Calendar.DAY_OF_MONTH, -1);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.set(year, month, day, 0, 0, 0);
		//开始日期
		Date bdate = cal.getTime();
		//cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		//结束日期
		Date edate = cal.getTime();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("yearr",yearr);
		map.put("acct", acct);
		map.put("positn", inventory.getPositn().getCode());
		map.put("bdate", bdate);
		map.put("edate", edate);
		//得到最后盘点日期
//		int num = inventoryService.findExLastPdDate(map);
		//已盘点过
//		if(num>0){
//			Map<String,String> result = new HashMap<String,String>();
//			result.put("pr", "-1");
//			result.put("msg", "已盘点！");
//			//返回信息
//			JSONObject rs = JSONObject.fromObject(result);
//			return rs.toString();
//		}
		inventory.setAcct(acct);
		inventory.setYearr(yearr);
		inventoryService.saveInventory(inventory);
		List<Inventory> listInventory=inventoryService.findExInventory(inventory);	
		//保存盘点表 2014.12.9wjf
		inventoryService.savePositnPan(accountName,inventory,listInventory);
		return inventoryService.endExInventory(accountName,inventory,listInventory);//生成盘亏出库，盘盈入库
	}
}
