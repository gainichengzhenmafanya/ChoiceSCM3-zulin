package com.choice.scm.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.DeliverDirectConstants;
import com.choice.scm.service.DeliverDirectService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.util.ArrayUtil;
/**
 * 供应商直配
 * @author dell
 *
 */
@Controller
@RequestMapping("deliverDirect")
public class DeliverDirectController {

	@Autowired
	DeliverService deliverService;
	@Autowired
	DeliverDirectService deliverDirectService;
	
	/**
	 * 查询供应商信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findDeliver(ModelMap modelMap,HttpSession session,String searchInfo,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null !=searchInfo && !"".equals(searchInfo)){
			searchInfo = searchInfo.toUpperCase();
		}
		modelMap.put("searchInfo", searchInfo);
		modelMap.put("listDeliver", deliverService.findDeliverBy(null, searchInfo, page));
		modelMap.put("pageobj", page);
		return new ModelAndView(DeliverDirectConstants.LIST_DELIVER,modelMap);
	}
	
	/**
	 * 根据供应商查询仓位
	 * @throws Exception
	 */
	@RequestMapping(value = "/listDeliverDirect")
	public ModelAndView findDeliverDirect(ModelMap modelMap,String deliverCode,Page page,String single,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		deliverCode = ("".equals(deliverCode)||null==deliverCode)?"---":deliverCode;//如果为空的话就赋值---为了使页面查不到值
		modelMap.put("pageobj", page);
		modelMap.put("listDeliverDirect", deliverDirectService.findPositnByDeliver(deliverCode, page));
		modelMap.put("positnCodes", deliverDirectService.findAllPositnByDeliver(deliverCode));
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(DeliverDirectConstants.LIST_DELIVERDIRECT,modelMap);
	}
	
	/**
	 * 新增仓位
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveFirmDeliver(ModelMap modelMap,HttpSession session,String deliverCode,String positn,String positnCodes,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		if(!"".equals(positnCodes)){
			positn = ArrayUtil.getMinString(positn, positnCodes, ",");
		}
		if(!"".equals(deliverCode) && !"".equals(positn)){
			deliverDirectService.savePositnByDeliver(acct, deliverCode, positn);
		}
		
		modelMap.put("listFirmDeliver", deliverDirectService.findPositnByDeliver(deliverCode, page));
		modelMap.put("positnCode", positn);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 检查是否重复
	 */
	@RequestMapping(value = "/checkOne")
	@ResponseBody
	public Object checkOne(ModelMap modelMap,HttpSession session,String firm,String deliver) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return deliverDirectService.checkOne(firm, deliver);
	}
	/**
	 * 删除仓位
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView deleteDeliverDirect(ModelMap modelMap,HttpSession session,@RequestParam("deliverCode")String deliverCode,@RequestParam("positn")String positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		deliverDirectService.deletePositnByDeliver(acct,deliverCode, positn);
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
