package com.choice.scm.web.controller.reportAnaMis;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportAnaMis.JhJiageBijiaoMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("JhJiageBijiaoMis")
public class JhJiageBijiaoMisController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private JhJiageBijiaoMisService jhJiageBijiaoMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	PositnService positnService;
	
	/******************************************************************
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/********************************************分店进货价格分析****************************************************/
	
	/**
	 * 跳转到分店进货价格分析页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/tofindFirmStockPrice")
	public ModelAndView tofindFirmStockPrice(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.BASICINFO_REPORT_FIRMSTOCKPRICE);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_FIRMSTOCKPRICE_MIS);
	}
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFirmStockPriceHeader")
	@ResponseBody
	public Object findfindFirmStockPriceHeader(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FIRMSTOCKPRICE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_FIRMSTOCKPRICE));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_FIRMSTOCKPRICE_FROZEN);
		return columns;		
	}
	/**
	 * 查询分店进货价格分析内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param month
	 * @param max
	 * @param min
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findFirmStockPrice")
	@ResponseBody
	public Object findfindFirmStockPrice(ModelMap modelMap, HttpSession session, String page, String rows, String sort,
			String order, SupplyAcct supplyAcct, String searchType) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("searchType", searchType);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return jhJiageBijiaoMisService.findFirmStockPrice(condition,pager);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseFirmStockPrice")
	public ModelAndView toColChoosefindFirmStockPrice(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FIRMSTOCKPRICE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_FIRMSTOCKPRICE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_FIRMSTOCKPRICE));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 导出分店进货价格分析excel文件
	 * @param response
	 * @param month
	 * @param sort
	 * @param order
	 * @param max
	 * @param min
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportfindFirmStockPrice")
	@ResponseBody
	public void exportfindFirmStockPrice(HttpServletResponse response, String sort, String order, HttpServletRequest request,
			HttpSession session, SupplyAcct supplyAcct, String searchType) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		String fileName = "分店进货价格分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("searchType", searchType);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_FIRMSTOCKPRICE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), jhJiageBijiaoMisService.findFirmStockPrice(condition,pager).getRows(), "分店进货价格分析", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_FIRMSTOCKPRICE));
	}
	
	/**
	 * 打印分店进货价格分析
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param max
	 * @param min
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printFirmStockPrice")
	public ModelAndView printfindFirmStockPrice(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct, String searchType)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		supplyAcct.setAccountId(accountId);
		params.put("accountId", accountId);
		params.put("searchType", searchType);
		params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("positn",supplyAcct.getPositn());
		params.put("typ",supplyAcct.getTyp());
		condition.put("supplyAcct", supplyAcct);
		condition.put("searchType", searchType);
		modelMap.put("List",jhJiageBijiaoMisService.findFirmStockPrice(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "分店进货价格分析");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/JhJiageBijiaoMis/printFirmStockPrice.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_FIRMSTOCKPRICE,SupplyAnaConstants.REPORT_EXP_URL_FIRMSTOCKPRICE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/********************************************分店物资进货价格比较****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findSupplyInPriceCompareHeaders")
	@ResponseBody
	public Object getSupplyInPriceCompare(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		List<Positn> columnList = new ArrayList<Positn>();
		Positn positn = new Positn();
		positn.setCode("0001");
		positn.setDes("编码");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0002");
		positn.setDes("名称");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0003");
		positn.setDes("规格");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0004");
		positn.setDes("单位");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0005");
		positn.setDes("平均价");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0006");
		positn.setDes("最高价");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0007");
		positn.setDes("最低价");
		columnList.add(positn);
		positn = new Positn();
		positn.setCode("0008");
		positn.setDes("波动率");
		columnList.add(positn);
		List<Positn> positnList = positnService.findAllPositn();
		columnList.addAll(positnList);
		columns.put("columns", columnList);
		return columns;
	}
	/**
	 * 跳转到分店物资进货价格比较页面
	 * @return
	 */
	@RequestMapping("/toSupplyInPriceCompare")
	public ModelAndView toSupplyInPriceCompare(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", "分店物资进货价格比较");
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null != accountPositn.getPositn()){
			modelMap.put("positnCode",accountPositn.getPositn().getCode());
			modelMap.put("positnDes",accountPositn.getPositn().getDes());
		}
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_FIRMSTOCKPRICE_MIS,modelMap);
	}
	
	/**
	 * 查询分店物资进货价格比较
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findSupplyInPriceCompare")
	@ResponseBody
	public Object findSupplyInPriceCompare(ModelMap modelMap, HttpSession session, String page, String rows, SupplyAcct supplyAcct) throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setAccountId(accountId);
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("supplyAcct", supplyAcct);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return jhJiageBijiaoMisService.findSupplyInPriceCompare(condition,pager);
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
//	@RequestMapping(value = "/printSupplyInPriceCompare")
//	public ModelAndView printSupplyInPriceCompare(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
//		pager.setPageSize(Integer.MAX_VALUE);
//		Map<String,String> params = new HashMap<String,String>();
//		if(supplyAcct.getBdat() != null)
//			params.put("bdat",new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getBdat()));
//		params.put("positn",supplyAcct.getPositn());
//		params.put("grptyp",supplyAcct.getGrptyp());
//		if(supplyAcct.getBdat() != null)
//			params.put("edat",new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getEdat()));
//		params.put("grp",supplyAcct.getGrp());
//		params.put("chktyp",supplyAcct.getChktyp());
//		params.put("delivercode",supplyAcct.getDelivercode());
//		
//		modelMap.put("List",firmMisReportService.findSupplyInPriceCompare(supplyAcct).getRows());
//	 	HashMap  parameters = new HashMap();
//	    parameters.put("report_name", "分店物资进货价格比较");
//	    modelMap.put("actionMap", params);
//	    parameters.put("maded",new Date());
//	    parameters.put("madeby", session.getAttribute("accountName").toString());
//	        
//        modelMap.put("parameters", parameters);
//	 	modelMap.put("action", "/SupplyAcct/printSupplyInPriceCompare.do");//传入回调路径
//	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_CHKINMCATEGORYSUMMARY,SupplyAcctConstants.REPORT_EXP_URL_CHKINMCATEGORYSUMMARY);//判断跳转路径
//        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
//		return new ModelAndView(rs.get("url"),modelMap);
//	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyInPriceCompare")
	@ResponseBody
	public void exportSupplyInPriceCompare(HttpServletResponse response, String sort, String order, HttpServletRequest request,
			HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "分店物资进货价格比较";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		dictColumns.setTableName("分店物资进货价格比较");
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		jhJiageBijiaoMisService.exportSupplyInPriceCompare(response.getOutputStream(), supplyAcct);
		
	}
}
