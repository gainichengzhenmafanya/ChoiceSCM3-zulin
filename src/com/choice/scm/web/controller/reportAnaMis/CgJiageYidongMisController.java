package com.choice.scm.web.controller.reportAnaMis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportAnaMis.CgJiageYidongMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("CgJiageYidongMis")
public class CgJiageYidongMisController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CgJiageYidongMisService cgJiageYidongService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	private PositnService positnService;

	/**
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap, DictColumns dictColumns, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/********************************************采购价格异动分析****************************************************/
	
	/**
	 * 跳转到采购价格异动分析页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/toPurchasePriceChange")
	public ModelAndView toPurchasePriceChange(HttpSession session, ModelMap modelMap)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.BASICINFO_REPORT_PURCHASEPRICECHANGE);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null != accountPositn.getPositn()){
			modelMap.put("positnCode",accountPositn.getPositn().getCode());
			modelMap.put("positnDes",accountPositn.getPositn().getDes());
		}
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_PURCHASEPRICECHANGE_MIS);
	}
	/**
	 * 查询表头
	 * @param session
	 * @return
	 */
	@RequestMapping("/findPurchasePriceChangeHeader")
	@ResponseBody
	public Object findPurchasePriceChangeHeader(HttpSession session, String monthnew){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_PURCHASEPRICECHANGE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_PURCHASEPRICECHANGE));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_PURCHASEPRICECHANGE_FROZEN);
		return columns;		
	}
	/**
	 * 查询采购价格异动分析内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param month
	 * @param max
	 * @param min
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findPurchasePriceChange")
	@ResponseBody
	public Object findPurchasePriceChange(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, String month, String max, String min,SupplyAcct supplyAcct,
			String monthnew) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=posi.getCode()+",";
				}
				positncode+=accountPositn.getPositn().getCode();
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		supplyAcct.setAccountId(accountId);
		condition.put("max",max);
		condition.put("month",month);
		condition.put("monthnew", monthnew);
		condition.put("min",min);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return cgJiageYidongService.findPurchasePriceChange(condition,pager);
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChoosePurchasePriceChange")
	public ModelAndView toColChoosePurchasePriceChange(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_PURCHASEPRICECHANGE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_PURCHASEPRICECHANGE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_PURCHASEPRICECHANGE));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 导出采购价格异动分析excel文件
	 * @param response
	 * @param month
	 * @param sort
	 * @param order
	 * @param max
	 * @param min
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportPurchasePriceChange")
	@ResponseBody
	public void exportPurchasePriceChange(HttpServletResponse response, String month, String sort, String order, String max,
			String min, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct, String monthnew) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "采购价格异动分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=posi.getCode()+",";
				}
				positncode+=accountPositn.getPositn().getCode();
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		supplyAcct.setAccountId(accountId);
		condition.put("max",max);
		condition.put("month",month);
		condition.put("monthnew", monthnew);
		condition.put("min",min);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_PURCHASEPRICECHANGE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), cgJiageYidongService.findPurchasePriceChange(condition,pager).getRows(), "采购价格异动分析", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_PURCHASEPRICECHANGE));
	}
	
	/**
	 * 打印采购价格异动分析
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param max
	 * @param min
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printPurchasePriceChange")
	public ModelAndView printPurchasePriceChange(ModelMap modelMap, Page pager, HttpSession session, String month, 
			String type, SupplyAcct supplyAcct, String max, String min, String monthnew)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=posi.getCode()+",";
				}
				positncode+=accountPositn.getPositn().getCode();
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		supplyAcct.setAccountId(accountId);
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("month",month);
		params.put("max", max);
		params.put("monthnew", monthnew);
		params.put("min", min);
		params.put("accountId", accountId);
		params.put("positn", supplyAcct.getPositn());
		params.put("sp_code", supplyAcct.getSp_code());
		params.put("grptyp", supplyAcct.getGrptyp());
		params.put("grp", supplyAcct.getGrp());
		params.put("typ", supplyAcct.getTyp());
		condition.put("month",month);
		condition.put("supplyAcct", supplyAcct);
		condition.put("monthnew", monthnew);
		condition.put("max",max);
		condition.put("min",min);
		modelMap.put("List",cgJiageYidongService.findPurchasePriceChange(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "采购价格异动分析");
	    parameters.put("month",month);
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/CgJiageYidongMis/printPurchasePriceChange.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_PURCHASEPRICECHANGE,SupplyAnaConstants.REPORT_EXP_URL_PURCHASEPRICECHANGE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
}
