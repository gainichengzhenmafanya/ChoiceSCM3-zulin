package com.choice.scm.web.controller.reportAnaMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAnaMis.KlZongheFenxiMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("KlZongheFenxiMis")
public class KlZongheFenxiMisController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private KlZongheFenxiMisService KlZongheFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
	


/********************************************库龄综合分析****************************************************/

	/**
	 * 跳转到库龄综合分析页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseInventoryAgingSum")
	public ModelAndView toColChooseInventoryAgingSum(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_INVENTORYAGING_SUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_INVENTORYAGING_SUM);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_INVENTORYAGING_SUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findInventoryAgingSumHeaders")
	@ResponseBody
	public Object getInventoryAgingSumHeaders(HttpSession session, String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		if(typ.equals("sum")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_INVENTORYAGING_SUM);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_INVENTORYAGING_SUM));
		}
		if(typ.equals("detail")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_INVENTORYAGING_DETAIL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_INVENTORYAGING_DETAIL));
		}
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_INVENTORYAGINGSUM_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到库龄汇总分析报表页面
	 * @return
	 */
	@RequestMapping("/toInventoryAgingSum")
	public ModelAndView toINVENTORYAGINGSUMDetailS(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_INVENTORYAGING_SUM);
		return new ModelAndView(SupplyAnaConstants.REPORT_SHOW_INVENTORYAGINGSUM_MIS,modelMap);
	}
	
	/**
	 * 查询库龄汇总分析报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findInventoryAgingSum")
	@ResponseBody
	public Object findInventoryAgingSum(ModelMap modelMap, HttpSession session, String page, String rows, String sort,
			String order, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return KlZongheFenxiService.findInventoryAgingSum(condition, pager);
	}
	
	/**
	 * 打印库龄综合分析报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printInventoryAgingSum")
	public ModelAndView printInventoryAgingSum(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn",supplyAcct.getPositn());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
			params.put("positn",accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		params.put("accountId",accountId);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",KlZongheFenxiService.findInventoryAgingSum(condition, pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "库龄综合分析");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/KlZongheFenxiMis/printInventoryAgingSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_INVENTORYAGINGSUM,SupplyAnaConstants.REPORT_EXP_URL_INVENTORYAGINGSUM);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出库龄综合查询报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportInventoryAgingSum")
	@ResponseBody
	public void exportInventoryAgingSum(HttpServletResponse response, HttpServletRequest request, HttpSession session,
			SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "库龄综合查询报表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		conditions.put("supplyAcct", supplyAcct);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_INVENTORYAGING_SUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), KlZongheFenxiService.findInventoryAgingSum(conditions, pager).getRows(), "库龄综合分析", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
