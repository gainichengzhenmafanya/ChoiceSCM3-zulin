package com.choice.scm.web.controller.reportAnaMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.reportAnaMis.KcZhouzhuanLvMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("KcZhouzhuanLvMis")
public class KcZhouzhuanLvMisController {
	
	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private KcZhouzhuanLvMisService kcZhouzhuanLvService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	AccountPositnService accountPositnService;
		
	/******************************************************************
	 * 保存现有显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/saveColumnsChoose")
	public ModelAndView saveColumnsChoose(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());	
		dictColumnsService.saveColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 恢复默认显示字段
	 * @param modelMap
	 * @param dictColumns
	 * @param session
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/recoverDefaultColumns")
	public ModelAndView recoverDefaultColumns(ModelMap modelMap,DictColumns dictColumns,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumnsService.deleteColumnsChoose(dictColumns);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/********************************************库存周转率****************************************************/

	/**
	 * 跳转到库存周转率页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseKuCunZhouZhuanLv")
	public ModelAndView toColChooseKuCunZhouZhuanLv(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_KCZZL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_KCZZL);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_KCZZL));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findKuCunZhouZhuanLvHeaders")
	@ResponseBody
	public Object getKuCunZhouZhuanLvHeaders(HttpSession session,String typ){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		if(typ.equals("sum")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_KCZZL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_KCZZL));
		}
		if(typ.equals("detail")){
			dictColumns.setTableName(ScmStringConstant.REPORT_NAME_KCZZL_DETAIL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_KCZZL_DETAIL));
		}
		return columns;
	}
	
	/**
	 * 跳转到库存周转率报表页面
	 * @return
	 */
	@RequestMapping("/toKuCunZhouZhuanLv")
	public ModelAndView toKuCunZhouZhuanLv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_KCZZL);
		return new ModelAndView(SupplyAnaConstants.LIST_KCZZL_MIS,modelMap);
	}
	/**
	 * 查询库存周转率报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findKuCunZhouZhuanLv")
	@ResponseBody
	public Object findKuCunZhouZhuanLv(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,String month,String firmCode,String type,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		if(null!=firmCode && !"".equals(firmCode)){
			supplyAcct.setPositn(firmCode);
		}
		supplyAcct.setAccountId(accountId);
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("month", month);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		boolean flag = false;
		if(type.equals("detail")){
			flag = true;
		}
		return kcZhouzhuanLvService.findKuCunZhouZhuanLv(condition, pager,flag);
	}
	
	/**
	 * 打印库存周转率报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printKuCunZhouZhuanLv")
	public ModelAndView printKuCunZhouZhuanLv(ModelMap modelMap, Page pager, HttpSession session, String type, 
			String month, SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn",supplyAcct.getPositn());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
			params.put("positn",accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		params.put("accountId", accountId);
		params.put("month", month);
		condition.put("month", month);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",kcZhouzhuanLvService.findKuCunZhouZhuanLv(condition, pager,false).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "库存周转率");
	    modelMap.put("actionMap", params);
	    parameters.put("month",month);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/KcZhouzhuanLvMis/printKuCunZhouZhuanLv.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAnaConstants.REPORT_PRINT_URL_KCZZL,SupplyAnaConstants.REPORT_PRINT_URL_KCZZL);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出库存周转率报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportKuCunZhouZhuanLv")
	@ResponseBody
	public void exportKuCunZhouZhuanLv(HttpServletResponse response, HttpServletRequest request, HttpSession session,
			String month, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "库存周转率报表";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAccountId(accountId);
		conditions.put("month", month);
		conditions.put("supplyAcct", supplyAcct);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_KCZZL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), kcZhouzhuanLvService.findKuCunZhouZhuanLv(conditions, pager,false).getRows(), "库存周转率", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
}
