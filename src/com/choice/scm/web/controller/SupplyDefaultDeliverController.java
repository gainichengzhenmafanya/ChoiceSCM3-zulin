package com.choice.scm.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyDefaultDeliverConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.PrdctbomService;
import com.choice.scm.service.SupplyDefaultDeliverService;
import com.choice.scm.service.SupplyService;

@Controller
@RequestMapping("supplyDefaultDeliver")
public class SupplyDefaultDeliverController {

	@Autowired
	SupplyDefaultDeliverService supplyDefaultDeliverService;
	@Autowired
	private GrpTypService grpTypService;	
	@Autowired
	private CodeDesService codeDesService;	
	@Autowired
	private PositnService positnService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	PrdctbomService prdctbomService;
	
	/**
	 * 物资编码列表
	 * @param modelMap
	 * @param supply
	 * @param grpTyp
	 * @param grp
	 * @param typ
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllSupply(ModelMap modelMap, String init, String type, HttpSession session)
			throws Exception {
		//左侧导航树
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		if ("half".equals(init)) {
			Supply prdct = new Supply();
			prdct.setAcct(acct);
			prdct.setEx1("Y");
			//添加物资权限
			prdct.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			prdct.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
			prdct.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("supplyList", prdctbomService.findPrdct(prdct));
		}
		modelMap.put("init", init);
		modelMap.put("modList", codeDesService.findCodeDes("5"));//配送片区
		modelMap.put("type", type);
		return new ModelAndView(SupplyDefaultDeliverConstants.LIST_SUPPLYDEFAULTDELIVER, modelMap);
	}
	
	/**
	 * 根据左侧的树   显示 右侧的物资编码
	 * @param modelMap
	 * @param supply
	 * @param level
	 * @param spcodeList
	 * @param code
	 * @param page
	 * @param session
	 * @param type 判断是供应商默认仓位 还是 出品加工间
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllSupplyByLeftGrpTyp(ModelMap modelMap, Supply supply, String init, String level, String mod, String code, String orderBy,String orderDes,String type,Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null==orderBy){
			orderBy="sp_code";
		}
		modelMap.put("mode", mod);
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("orderBy", orderBy);
		modelMap.put("orderDes", orderDes);
		if ("half".equals(init)) {
			supply.setEx1("Y");
		}
		//添加物资权限
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限 0是对物资 1是对小类
		supply.setAccountId(session.getAttribute("accountId").toString());
		modelMap.put("supplyList", supplyDefaultDeliverService.findSupplyDefaultPositn(supply, level, code, mod, orderBy, orderDes, page, type));
		modelMap.put("pageobj", page);
		modelMap.put("type", type);
		modelMap.put("init", init);
		if(type.equals("POSITN")){
			return new ModelAndView(SupplyDefaultDeliverConstants.TABLE_SUPPLYDEFAULTDELIVER, modelMap);
		}else{
			return new ModelAndView(SupplyDefaultDeliverConstants.TABLE_SUPPLYWORKSHOP, modelMap);
		}
		
	}
	
	
	/**
	 * 保存默认仓位
	 * type 判断是物资默认供应商 还是 出品加工间
	 */
	@RequestMapping(value = "/save") 
	public ModelAndView savePositn(ModelMap modelMap, String sp_code, String positn, String mod, String type, 
			String code1, String level, HttpSession session) throws Exception{
		//帐套
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("acct", session.getAttribute("ChoiceAcct").toString());
		if ("".equals(sp_code)||",".equals(sp_code)) {
			sp_code="";
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			//添加物资权限
//			supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
//			supply.setAccountId(session.getAttribute("accountId").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply, level, code1);
			if(supplyList.size()==0){
				return new ModelAndView(StringConstant.ACTION_DONE, modelMap);				
			}
			for (int i=0;i<supplyList.size();i++) {
				sp_code += supplyList.get(i).getSp_code();
				sp_code += ",";
			}
			sp_code=sp_code.substring(0, sp_code.length()-1);
		}
		condition.put("sp_code", sp_code);
		if(type.equals("POSITN")){
			condition.put("positn", positn);
		}else{
			condition.put("positnex", positn);
		}
		condition.put("type", type);
		condition.put("mod", mod);
		supplyDefaultDeliverService.saveMethod(condition,type);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	
	/**
	 * 打开选择仓位
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPositnByPage")
	public ModelAndView selectPositnByPage(ModelMap modelMap,Page page, Positn positn, String sp_code,
			String mod,String type, String spcodeList, String code1, String level,String jg,String cwqx,HttpSession session,String single) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"N".equals(jg)){//加工间过来   勿动 wjf
			positn.setTyp("1204");//查询加工间类型
		}
		modelMap.put("jg", jg);
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		if (cwqx==null && "".equals(cwqx)){
			positn.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		}
		
		if(positn.getCode() != null && !"".equals(positn.getCode())){
			modelMap.put("listPositn1", positnService.selectPositnNew(positn));
		}
		
		positn.setAccountId(session.getAttribute("accountId").toString());
		String locale = session.getAttribute("locale").toString();//语言类型
		positn.setLocale(locale);
		//清空positn中的code值，查询所有仓位
		positn.setCode("");
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		CodeDes codedes = new CodeDes();
		codedes.setTyp("12");
		codedes.setLocale(locale);
		modelMap.put("listPositnTyp", codeDesService.findCodeDes(codedes));
		modelMap.put("pageobj", page);
		modelMap.put("mode", mod);
		modelMap.put("sp_code", sp_code);
		modelMap.put("type", type);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		modelMap.put("single", single);
		return new ModelAndView(SupplyDefaultDeliverConstants.SELECT_SUPPLYPOSITN, modelMap);
	}
	
	/**
	 * 清除默认仓位
	 * @param modelMap
	 * @param page
	 * @param positn
	 * @param sp_code
	 * @param mod
	 * @param type
	 * @param spcodeList
	 * @param code1
	 * @param level
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletePositn")
	public ModelAndView deletePositn(ModelMap modelMap, String sp_code,String mod,HttpSession session, String code, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==sp_code || ",".equals(sp_code)) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code);
			if(supplyList.size()==0){
				return new ModelAndView(StringConstant.ACTION_DONE, modelMap);				
			}
			sp_code="";
			for (int i=0;i<supplyList.size();i++) {
				sp_code += supplyList.get(i).getSp_code();
				sp_code += ",";
			}
			sp_code=sp_code.substring(0, sp_code.length()-1);
		}
		
		String[] str = sp_code.split(",");
		String sp_codes = "";
		for (int i = 0; i < str.length; i++) {
			if (i == str.length -1) {
				sp_codes += str[i];
			} else {
				sp_codes += str[i]+",";
			}
		}
		supplyDefaultDeliverService.deletePositn(sp_codes, mod);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
}
