package com.choice.scm.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.DisConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisYanShouService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping(value = "disYanShou")
/**
 * 报货验收
 * @author csb
 *
 */
public class DisYanShouController {

	private final static int PAGE_SIZE = 20;
	@Autowired
	private DisYanShouService disYanShouService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private LogsMapper logsMapper;
	/**
	 * 修改
	 */
	@RequestMapping(value = "/updateDis")
	@ResponseBody
	public Object updateDis(ModelMap modelMap,HttpSession session,DisList disList) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String accountName=session.getAttribute("accountName").toString();
		return disYanShouService.updateDis(disList);
//		modelMap.put("accountName", accountName);
//		return new ModelAndView(DisConstants.LIST_DIS, modelMap);
	}	
	
	/**
	 * 全部操作
	 */
	@RequestMapping(value = "/updateAll")
	public ModelAndView updateDisAll(ModelMap modelMap,HttpSession session,Dis dis,String ind1,String param) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dis.setStartNum(0);
		dis.setEndNum(Integer.MAX_VALUE);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		List<Dis> disList=disYanShouService.findAllDisPage(dis,ind1);
		StringBuffer idsBuffer = new StringBuffer();
		for(Dis d:disList){
			idsBuffer.append(d.getId()+",");
		}
		dis.setId(idsBuffer.substring(0, idsBuffer.lastIndexOf(",")));
		if(param.equals("Y")){
			dis.setBak2("1");
		}else{
			dis.setBak2("0");
		}
		disYanShouService.updateAll(dis);
		
		int totalCount = disList.size();
		dis.setEndNum(PAGE_SIZE);

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());

		modelMap.put("disesList1", disYanShouService.findAllDisPage(dis,ind1));
		modelMap.put("totalCount", totalCount);
		
		if(totalCount<=PAGE_SIZE){
			modelMap.put("currState", 1);
		}else{
			modelMap.put("currState", PAGE_SIZE*1.0/totalCount);
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("currPage", 1);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		return new ModelAndView(DisConstants.LIST_DIS_CHECK, modelMap);
	}
	
	/********************************************报货验收start******************************************/	
	@RequestMapping(value = "/listCheck")
	public ModelAndView listCheck(ModelMap modelMap,HttpSession session,Dis dis,String action,String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"报货验收查询 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
//		String buttonQiet = "0";
		if(null!=action && "init".equals(action)){//页面初次加载时
			dis.setBdat(new Date());
			dis.setEdat(new Date());
			modelMap.put("action", "init");
		}else{

				dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
				dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
				dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
				dis.setAccountId(session.getAttribute("accountId").toString());
//			}
			int totalCount = disYanShouService.findAllDisCount(dis,ind1);
			dis.setStartNum(0);
			dis.setEndNum(PAGE_SIZE);
			List<Dis> disesList1 = disYanShouService.findAllDisPage(dis,ind1);
			if(disesList1!=null && disesList1.size()>0){
				//查询库存量--物资余额表数据
				SupplyAcct supplyAcct = new SupplyAcct();
				supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
				for(Dis d:disesList1){
					supplyAcct.setPositn(d.getPositnCode());
					supplyAcct.setSp_code(d.getSp_code());
					d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
				}
			}
			modelMap.put("disesList1", disesList1);
//			for (int i = 0; i < disesList1.size(); i++) {
//				Dis d = new Dis();
//				if (d.getBak2() == "1") {
//					buttonQiet = "1";
//				} else {
//					buttonQiet = "0";
//				}
//			}
//			modelMap.put("buttonQiet", buttonQiet);
			modelMap.put("totalCount", totalCount);
			
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);
			}
			
//			modelMap.put("disesList1", disYanShouService.findAllDisPage(dis,ind1));
		}
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));
		modelMap.put("accountName", accountName);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("ind1", ind1);
		modelMap.put("dis", dis);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		return new ModelAndView(DisConstants.LIST_DIS_CHECK, modelMap);
	}
	
	/**
	 * 报货分拨Ajax加载
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session,Dis dis,String ind1,String currPage,String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		dis.setStartNum(intCurrPage*PAGE_SIZE);
		dis.setEndNum((intCurrPage+1)*PAGE_SIZE);

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());

		modelMap.put("disesList1", disYanShouService.findAllDisPage(dis,ind1));
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		if(null!=ind1 && !"".equals(ind1)){
			dis.setMaded(dis.getInd());
		}
		modelMap.put("ind1", ind1);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
		//return modelMap;
	}
	
	
	/**
	 * 打印验收单
	 */
	@RequestMapping(value = "/printReceipt")
	public ModelAndView printReceipt(ModelMap modelMap,HttpSession session,Page page,Dis dis,String type,String ind1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"打印验收单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();//得到帐套信息 wjf
		dis.setAcct(acct);
		//接收用户输入的查询参数
		HashMap<String, Object> param=new HashMap<String, Object>();
		dis.setDanjuTyp("receipt");
		param.put("danjuTyp", "receipt");
		dis.setInout("in,inout");;
		dis.setChkin("N");
		dis.setChkout("N");
		dis.setChk1("Y");
		dis.setSta("Y");
		param.put("chkin", "N");
		param.put("chkout", "N");
		param.put("chk1", "Y");
		param.put("sta", "Y");
		param.put("inout", "in,inout");
		param.put("bdat", DateFormat.getStringByDate(dis.getBdat(),"yyyy-MM-dd"));
		param.put("edat", DateFormat.getStringByDate(dis.getEdat(),"yyyy-MM-dd"));
		param.put("positnCode", dis.getPositnCode());
		param.put("deliverCode", dis.getDeliverCode());
		param.put("typCode", dis.getTypCode());
		param.put("firmCode", dis.getFirmCode());
		param.put("sp_code", dis.getSp_code());
		param.put("sp_init", dis.getSp_init());
		param.put("psarea", dis.getPsarea());
		if(null!=ind1 && !"".equals(ind1)){
			dis.setInd(dis.getMaded());
			param.put("ind", dis.getMaded());
			dis.setMaded(null);
		}
		modelMap.put("actionMap", param);//回调参数
		modelMap.put("action", "/disYanShou/printReceipt.do");//传入回调路径
		//根据关键字查询

			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());

		List<Dis> disList=disYanShouService.findAllDisForReport(dis,ind1);
 		HashMap<Object,Object>  parameters = new HashMap<Object,Object>(); 
        String report_name=new String("验收单");
        String accountName=session.getAttribute("accountName").toString();
        parameters.put("madeby", accountName);
        parameters.put("report_name", report_name);
        modelMap.put("List", disList);
	    modelMap.put("parameters", parameters);//报表文件用的输入参数
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,DisConstants.REPORT_RECEIPT_URL,DisConstants.REPORT_RECEIPT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}
	/********************************************报货验收end******************************************/
}