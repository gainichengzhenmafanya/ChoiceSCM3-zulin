package com.choice.scm.web.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.AnnouncementConstants;
import com.choice.scm.domain.Announcement;
import com.choice.scm.service.AnnouncementService;
/**
 * 公告管理
 * @author lq
 *
 */
@Controller
@RequestMapping("announcement")
public class AnnouncementController {

	@Autowired
	AnnouncementService announcementService;
	
	/**
	 * 查询公告信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAnnouncement(ModelMap modelMap,Announcement announcement,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Announcement> announcementList= announcementService.findAnnouncementWithNoTeg(announcement,page);
		modelMap.put("pageobj", page);
		modelMap.put("announcementList",announcementList);
		modelMap.put("announcement", announcement);
		return new ModelAndView(AnnouncementConstants.LIST_ANNOUNCE,modelMap);
	}
	
	/**
	 * 打开新增公告页面
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap,Announcement announcement) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(AnnouncementConstants.ADD_ANNOUNCE,modelMap);
	}
	
	/**
	 * 新增公告
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,Announcement announcement) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		announcement.setCreate_time(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		announcement.setCreate_date(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		announcement.setId(announcementService.getId());
		announcementService.addAnnouncement(announcement);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 修改公告
	 */
	@RequestMapping(value = "/update")
	public ModelAndView UpdateAnnouncement(ModelMap modelMap,String id) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("announcement", announcementService.findAnnouncementById(id));
		return new ModelAndView(AnnouncementConstants.UPDATE_ANNOUNCE,modelMap);
	}
	
	/**
	 * 修改公告保存
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,Announcement announcement) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		announcementService.updateAnnouncement(announcement);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 删除公告
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView DeleteAnnouncement(ModelMap modelMap,Announcement announcement,String ids,String acct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		announcementService.deleteAnnouncement(ids);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 查看公告
	 */
	@RequestMapping(value = "/show")
	public ModelAndView seeAnnouncement(ModelMap modelMap,String id) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("announcement", announcementService.findAnnouncementById(id));
		return new ModelAndView(AnnouncementConstants.SEE_ANNOUNCE,modelMap);
	}
}
