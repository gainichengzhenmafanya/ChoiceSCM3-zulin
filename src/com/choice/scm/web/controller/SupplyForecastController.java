package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyForecastConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.FirmSupplyService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.SupplyForecastService;
import com.choice.scm.service.SupplyService;

/**
* 物资管理
* @author -  css
*/

@Controller
@RequestMapping(value = "supplyForecast")
public class SupplyForecastController {

	@Autowired
	private SupplyForecastService supplyForecastService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	FirmSupplyService firmSupplyService;
	
	/**
	 * 物资编码列表
	 * @param modelMap
	 * @param supply
	 * @param grpTyp
	 * @param grp
	 * @param typ
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllSupply(ModelMap modelMap, Supply supply, GrpTyp grpTyp, Grp grp, Typ typ, HttpSession session)
			throws Exception {
		//左侧导航树
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		return new ModelAndView(SupplyForecastConstants.LIST_SUPPLYMNG, modelMap);
	}
	
	/**
	 * 根据左侧的树   显示 右侧的物资编码
	 * @param modelMap
	 * @param supply
	 * @param level
	 * @param spcodeList
	 * @param code
	 * @param page
	 * @param session
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllSupplyByLeftGrpTyp(ModelMap modelMap, Supply supply, String level, String spcodeList,
			String code, Page page, HttpSession session, String type, String update, String bdate, String edate ) throws Exception {
		//帐套
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("update", update);
		//查询记忆
		modelMap.put("supply", supply);	
		modelMap.put("type", type);//安全库存设置
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page));
		modelMap.put("pageobj", page);
		return new ModelAndView(SupplyForecastConstants.TABLE_SUPPLYMNG, modelMap);
	}	
		
	/**
	 * 打开修改信息界面
	 * @param modelMap
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/selectABC")
	public ModelAndView selectPositn1(ModelMap modelMap, String spcodeList, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("11");
		modelMap.put("typEasList", codeDesService.findCodeDes(codeDes));//报货类别
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyForecastConstants.SET_ABC, modelMap);
	}
	
	/**
	 * 保存修改的信息
	 * @param modelMap
	 * @param positn1
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveABCByUpd")
	public ModelAndView saveABCByUpd(ModelMap modelMap, Supply supply1, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyForecastService.saveABCByUpd(spcodeList, supply1);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}
