package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SppriceFirmConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceFirmService;

/**
 * 分店报价模板复制
 * @author css
 *
 */
@Controller
@RequestMapping("sppriceFirm")
public class SppriceFirmController {
	@Autowired
	PositnService positnService;
	@Autowired
	SppriceFirmService sppriceFirmService;
	
	/**
	 * 分店
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView list(ModelMap modelMap, String searchInfo, Page page, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		modelMap.put("listPositn", positnService.findPositn(searchInfo, page));
		modelMap.put("pageobj", page);
		modelMap.put("searchInfo", searchInfo);
		return new ModelAndView(SppriceFirmConstants.LIST_FIRM, modelMap);
	}
	
	/**
	 * 分店报价、售价
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/table")
	public ModelAndView table(ModelMap modelMap, String positnCode) throws Exception{
		modelMap.put("positnCode", positnCode);
		return new ModelAndView(SppriceFirmConstants.TABLE_SUPPLY, modelMap);
	}
	
	/**
	 * 复制
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/copy")
	public ModelAndView copy(ModelMap modelMap, SppriceDemo sppriceDemo, Page page, Positn positn, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("positnCode", sppriceDemo.getPositnCode());
		modelMap.put("type", sppriceDemo.getType());
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("queryPositn", positn);
//		modelMap.put("listPositn", positnService.findPositn("", page, 
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceFirmConstants.SELECT_POSITN, modelMap);
	}
	
	/**
	 * 添加报价、售价
	 */
	@RequestMapping("/saveByCopy")
	public ModelAndView saveByCopy(ModelMap modelMap, SppriceDemo sppriceDemo, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
		sppriceDemo.setMadeby(session.getAttribute("accountName").toString());
		sppriceFirmService.saveByCopy(sppriceDemo);
		modelMap.put("positnCode", sppriceDemo.getPositnCode());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 门店报价列表
	 * @param modelMap
	 * @param positnCode
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSpprice")
	public ModelAndView toSpprice(ModelMap modelMap, String positnCode,Page page) throws Exception{
		Spprice spprice = new Spprice();
		spprice.setArea(positnCode);//设置门店
		List<Spprice> spprices = sppriceFirmService.findFirmPrice(spprice,page);
		modelMap.put("positnCode", positnCode);
		modelMap.put("spprices", spprices);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceFirmConstants.TO_SPPRICE, modelMap);
	}
	
	/**
	 * 门店售价列表
	 * @param modelMap
	 * @param positnCode
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSppriceSale")
	public ModelAndView toSppriceSale(ModelMap modelMap, String positnCode,Page page) throws Exception{
		SppriceSale sale = new SppriceSale();
		sale.setMemo(positnCode);
		List<SppriceSale> sppriceSales = sppriceFirmService.findFirmSalePrice(sale,page);//门店售价
		modelMap.put("positnCode", positnCode);
		modelMap.put("sppriceSales", sppriceSales);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceFirmConstants.TO_SPPRICESALE, modelMap);
	}
}
