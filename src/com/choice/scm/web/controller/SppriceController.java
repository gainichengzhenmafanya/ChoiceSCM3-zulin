package com.choice.scm.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SppriceConstants;
import com.choice.scm.domain.SDList;
import com.choice.scm.domain.Spprice;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SppriceService;
import com.choice.scm.service.SupplyService;
/**
 * 物资价格管理
 * @author yp
 *
 */
@Controller
@RequestMapping("spprice")
public class SppriceController {

	@Autowired
	private SppriceService sppriceService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private PositnService positnService;
	
	/**
	 * 列出物资树
	 * @param modelMap
	 * @param spprice
	 * @param session
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/list")
	public ModelAndView listTree(ModelMap modelMap,Spprice spprice,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("supplyList", supplyService.findAllSupply(null));
		return new ModelAndView(SppriceConstants.LIST_SPPRICE,modelMap);
	}
	
	/**
	 * 根据物资编码查询价格列表
	 * @param modelMap
	 * @param spprice
	 * @param session
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/table")
	public ModelAndView findSppriceBySupply(ModelMap modelMap,Spprice spprice,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sppriceList", sppriceService.findSpprice(spprice,page));
		modelMap.put("spprice",spprice);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceConstants.TABLE_SPPRICE,modelMap);
	}
	
	/**
	 * 全部反审核or全部审核
	 * @param modelMap
	 * @param spprice 
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/antiAllAudit")
	public ModelAndView antiAllAudit(ModelMap modelMap,Spprice spprice,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceService.updateAllSpprice(session.getAttribute("accountName").toString(), session.getAttribute("ChoiceAcct").toString(),spprice);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 更新报价状态
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/updateBySave")
	public ModelAndView updateSpprice(ModelMap modelMap,String rec,HttpSession session,String sta) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceService.updateSpprice(rec, session.getAttribute("accountName").toString(), session.getAttribute("ChoiceAcct").toString(),sta);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	/**
	 * 删除未审核报价
	 * @param rec
	 * @return
	 * @throws CRUDException 
	 * @author yp
	 */
	@RequestMapping("/deleteSpprice")
	public ModelAndView deleteSpprice(String rec,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceService.deleteSpprice(rec, session.getAttribute("ChoiceAcct").toString());
		return new ModelAndView(StringConstant.ACTION_DONE);
	}
	
	/**
	 * 批量插入价格数据
	 * @param modelMap
	 * @param spprice
	 * @param session
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/insertBatch")
	public ModelAndView insertBatch(ModelMap modelMap,SDList demos,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceService.saveBatch(demos,session);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 模糊查询价格
	 * @param modelMap
	 * @param spprice
	 * @param session
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/listByCon")
	public ModelAndView findSppriceByDate(ModelMap modelMap,Spprice spprice,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		spprice.setAcct(acct);
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		//if("true".equals(first))spprice.setEdat(fmt.parse(fmt.format(new Date())));
		if(null == spprice.getChecby())spprice.setChecby("uncheck");
		modelMap.put("spprice", spprice);
		modelMap.put("deliverList",deliverService.findAllDelivers(null) );
		modelMap.put("typList", grpTypService.findAllTypA(acct, null));
		modelMap.put("positnList", positnService.findAllPositn(null));
//		modelMap.put("sppriceList", sppriceService.findSpprice(spprice,page));
		int audit=("admin".equals(accountName)?1:0);
		modelMap.put("audit", audit);
		List<Map<String, Object>> mapList = sppriceService.findSppriceT(spprice,page);
		modelMap.put("mapList", mapList);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceConstants.SEARCH_SPPRICE,modelMap);
	}
	
	/**
	 * 跳转到导入页面
	 * jinshuai 20160504
	 */
	@RequestMapping("/importSpprice")
	public ModelAndView importSpprice(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceConstants.SPPRICE_IMPORT_VIEW,modelMap);
	}
	
	/**
	 * 下载模板信息
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		sppriceService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = sppriceService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(SppriceConstants.SPPRICE_IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	@ResponseBody
	public List<String> importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> errorList = sppriceService.check(realFilePath, session.getAttribute("accountName").toString());
		return errorList;
	}
	
	
	/**
	 * 按spcode查询价格信息
	 * @param modelMap
	 * @param spprice
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/viewSpprice")
	public ModelAndView findSppriceBySpcode(ModelMap modelMap,Spprice spprice,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sppriceList",sppriceService.findSpprice(spprice, page));
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceConstants.SPPRICE_VIEW,modelMap);
		
	}
	/**
	 * 查询将到期价格
	 * @param modelMap
	 * @param spprice
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("/findSppriceByEdat")
	public ModelAndView findSppriceByEdat(ModelMap modelMap,Spprice spprice,HttpSession session,Page page,String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		spprice.setAcct(acct);
		
		List<Spprice> sppriceList = sppriceService.findSppriceByEdat(spprice,page);
		//计算物资报价剩余日期
		List<Spprice> sppriceLIST = new ArrayList<Spprice>();
		for(Spprice sp:sppriceList){
			if(sp.getDeltaT()!=null && sp.getDeltaT()>=0){//在提示日期范围内
				Date date = sp.getEdat();
				Date now = new Date();
				long deltaT = (date.getTime()-now.getTime())/86400000+1;
				sp.setDeltaT((int)deltaT);
				sppriceLIST.add(sp);
			}else{//不在提示天数范围内
				if(action!=null && !"".equals(action)){
					if(sp.getDeltaT()==null){
						sp.setDeltaT(10);
					}else{
						sp.setDeltaT(sp.getDeltaT());
					}
					sppriceLIST.add(sp);
				}
			}
		}
		
		if(null == spprice.getEdat()){
			spprice.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		modelMap.put("deliverList",deliverService.findAllDelivers(null) );
		modelMap.put("sppriceList", sppriceLIST);
		
		modelMap.put("spprice", spprice);
		modelMap.put("pageobj", page);
		return new ModelAndView(SppriceConstants.EDAT_SPPRICE,modelMap);
	}
	/**
	 * 跳转到物资价格管理页面
	 * @return
	 */
	@RequestMapping("/manageSpprice")
	public ModelAndView manageSpprice(){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SppriceConstants.MANAGE_SPPRICE);
	}
	
}
