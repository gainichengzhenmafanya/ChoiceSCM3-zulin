package com.choice.scm.web.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.service.DictService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkstomConstants;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.OfferCargo;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkstodService;
import com.choice.scm.service.ChkstodemoService;
import com.choice.scm.service.ChkstomCService;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.DeliverDirectService;
import com.choice.scm.service.EasService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.PublicExportTemplate;
import com.choice.scm.util.ReadReportUrl;
/**
 * 报货单新增、修改、删除、查询、打印、审核
 * @author csb
 *
 */
@Controller
@RequestMapping(value = "chkstom")

public class ChkstomController {

	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	private ChkstomCService chkstomCService;
	@Autowired
	private ChkstodService chkstodService;
	@Autowired
	private ChkstodemoService chkstodemoService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private EasService easService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private DictService dictService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private DeliverDirectService deliverDirectService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private LogsMapper logsMapper;
	/**
	 * 查询所有未审核报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkstom(ModelMap modelMap,Page page,HttpSession session,String tableFrom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询未审核报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//报货单主页面
		return new ModelAndView(ChkstomConstants.TABLE_CHKSTOM_NEW,modelMap);
	}
	/**
	 * 添加操作时候，刷新页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountId=session.getAttribute("accountId").toString();
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		modelMap.put("bhDate", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "BH_DATE")); // 报货日期是否可选
		modelMap.put("bhfl", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "BH_FJX_LEN")); // 报货附加项长度
		return new ModelAndView(ChkstomConstants.TABLE_CHKSTOM_NEW,modelMap);
	}	
	
	/**
	 * 检查是否设置供应商
	 */
	@RequestMapping(value = "/checkSaveNewChk")
	@ResponseBody
	public Object checkSaveNewChk(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.checkSaveNewChk(chkstom);
	}
	
	/**
	 * 检查是加盟商金额是否充足
	 */
	@RequestMapping(value = "/checkSppriceSale")
	@ResponseBody
	public Object checkSppriceSale(ModelMap modelMap,Page page,HttpSession session,Positn positn,int chkstoNo) throws Exception
	{
			return 0;
	}
	
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap,Page page,HttpSession session,String sta,Chkstom chkstom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if("add".equals(sta)){
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"新增报货单:单号("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		}else{
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"修改报货单:单号("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			Chkstom chkstom1 = chkstomService.findByChkstoNo(chkstom);
			chkstom.setVouno(chkstom1.getVouno());
		}
		//捕获异常
		try{
			return chkstomService.saveOrUpdateChk(chkstom, sta);
		}catch(Exception e){
			return e.getMessage();
		}
	}
	
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdateDept")
	@ResponseBody
	public Object saveByAddOrUpdateDept(ModelMap modelMap, String idList, HttpSession session, Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"新增档口报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		return chkstomService.saveOrUpdateChkDept(chkstom, idList);
	}
	
	/**
	 * 查找已审核报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap,HttpSession session,Page page,String init,String sp_code,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查找已审核报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收前台参数，已审核报货单列表页面
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=init && ""!=init){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		//把参数放到MAP
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		chkstomMap.put("checked", "checked");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		//把查询的结果集返回到页面
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap,page));
//		modelMap.put("positnList",positnService.findAllPositn(null));//页面根本没有，浪费效率 wjf
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomConstants.TABLE_CHECKED_CHKSTOM,modelMap);
	}
	
	/**
	 * 查找报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String startDate,String sp_code,String init,Chkstom chkstom, String firmDes) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查找报货单:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		Date bdate=null;
		Date edate=null;
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null!=init && !"".equals(init)){
			if(null!=startDate && !"".equals(startDate)){//不为空的话，赋值用户输入的日期
				bdate=DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			}else{//为空的话，赋值系统当前日期
				bdate=new Date();
			}
			edate=new Date();
			chkstom.setbMaded(bdate);
			chkstom.seteMaded(edate);
		}
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		//关键字查询
		modelMap.put("chkstomList", chkstomService.findByKey(chkstomMap,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("firmDes", firmDes);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomConstants.SEARCH_CHKSTOM,modelMap);
	}
	
	/**
	 * 双击查找
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountId = session.getAttribute("accountId").toString();
		chkstod.setAcct(acct);
		chkstom.setAcct(acct);
		modelMap.put("sta", "show");
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		modelMap.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		modelMap.put("bhfl", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "BH_FJX_LEN"));//判断附加项长度
		return new ModelAndView(ChkstomConstants.TABLE_CHKSTOM_NEW,modelMap);
	}

	/**
	 * 查看已审核报货单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedChkstom")
	public ModelAndView searchCheckedChkstom(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查看已审核报货单的信息:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//关键字查询
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomConstants.SEARCH_CHECKED_CHKSTOM_NEW,modelMap);
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(ModelMap modelMap,HttpSession session,Page page,String chkstoNoIds,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"删除报货单:单号为("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		return chkstomService.deleteChkstom(chkstom,chkstoNoIds);
	}
	
	/**
	 * 审核
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkstom")
	@ResponseBody
	public Object checkOneChkstom(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom,String chkstoNoIds) throws Exception
	{
		try {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"审核报货单:单号为("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//报货单填制上的整条删除
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setChecby(accountName);
	 
		String result= chkstomService.checkChk(chkstom,chkstoNoIds);

		return  result;
		
		} catch (Exception e) {
			 
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的   还要加上是否已经完成整个流程了2014.10.20wjf
	 */
	@RequestMapping(value = "/checkYnUnChk")
	@ResponseBody
	public Object checkYnUnChk(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		return chkstomService.checkYnUnChk(chkstom);
	}
	
	/**
	 * 反审核
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uncheckChkstom")
	@ResponseBody
	public Object uncheckOneChkstom(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom, String chkstoNoIds) throws Exception
	{
		try {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),"反"+ProgramConstants.CHECK,
				"反审核报货单:单号为("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String result= chkstomService.uncheckChk(chkstom,chkstoNoIds);
		return  result;
		
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	/**
	 * 上传
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateChkstomC")
	@ResponseBody
	public String updateChkstomC(ModelMap modelMap, String chkstoNoIds) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result = "0";
		List<String> ids = Arrays.asList(chkstoNoIds.split(","));
		for (int i = 0; i < ids.size(); i++) {
			Chkstom chkstom = new Chkstom();
			chkstom.setChkstoNo_c(ids.get(i));
			chkstom = chkstomCService.findByChkstoNo(chkstom);
			if (!"Y".equals(chkstom.getYnjd())) {
				OfferCargo offerCargo = easService.findOfferCargo(chkstom);
				if (offerCargo.getDeliveryUnitID()==null){
					result = "2";
					return result;
				}else if(offerCargo.getDeliveryUnitID()==null) {
					result = "3";
					return result;
				}else if(offerCargo.getCustomerID()==null) {
					result = "4";
					return result;
				}
//				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				offerCargo.setNumber(chkstom.getChkstoNo_c());
				offerCargo.setMaterialGroupID(easService.getMaterialGroupID(chkstom).getId());
				easService.saveOfferCargo(offerCargo);
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				chkstom.setYnjd("Y");
				chkstomCService.updateChkstomC(chkstom);
			}
			result = "1";
		}
		return result;
	}
	
	/**
	 * 确认修改，添加模板数据到报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enterUpdate")
	@ResponseBody
	public Object enterUpdate(ModelMap modelMap, HttpSession session, String rec2, String cnt, String cnt1, String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"添加模板数据到报货单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//申购物资批量添加时候的数量假修改
		String acct=session.getAttribute("ChoiceAcct").toString();
		List<String> recList=Arrays.asList(rec2.split(","));
		List<String> cntList=Arrays.asList(cnt.split(","));
		List<String> cnt1List=Arrays.asList(cnt1.split(","));
		return chkstodemoService.findChkstodemoByRec(recList, cntList, cnt1List, acct, title);
	}
	
	/**
	 * 打开查看分店上传数据页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUpload")
	public ModelAndView searchUpload(ModelMap modelMap,String tableFrom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//初始化查看上传页面
		modelMap.put("curDate", new Date());
		modelMap.put("str", tableFrom);
		return new ModelAndView(ChkstomConstants.SEARCH_UPLOAD,modelMap);
	}

	/**
	 * 查看分店上传数据
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchUploadByDate")
	public ModelAndView searchUploadByDate(ModelMap modelMap,HttpSession session,Chkstom chkstom,String startDate) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查看分店上传数据",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收日期，然后查询有哪些店上传与未上传
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=startDate && !"".equals(startDate)){//日期不为空
			chkstom.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			//查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		}else{
			//查看上传数据
			modelMap.put("chkstomList",chkstomService.findUpload(chkstom));
			//查看未上传数据
			modelMap.put("noUpList", chkstomService.findNoUpload(chkstom));
		}
		
		//系统日期
		modelMap.put("curDate", chkstom.getMaded());
		//仓位列表
		modelMap.put("positnList",positnService.findAllPositn(null));
		return new ModelAndView(ChkstomConstants.SEARCH_UPLOAD,modelMap);
	}
	
	/**
	 * 报货单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(HttpServletRequest request,ModelMap modelMap,HttpSession session,Page page,String type,Chkstod chkstod)throws CRUDException
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"报货单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Object pk_group = UserSpace.getSession().getAttribute("pk_group");
		String oldName = "scmChkstomReport.jasper";
		String realName  = pk_group.toString()+oldName;
        String realUrl = PublicExportTemplate.filePathUse(request,realName,oldName);
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		List<Chkstod> list=chkstodService.findAllChkstod(chkstod);
		
		if(list!=null && list.size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkstod ckd:list){
				supplyAcct.setPositn(ckd.getPositn());
				supplyAcct.setSp_code(ckd.getSupply().getSp_code());
				ckd.getSupply().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));//库存量取物资余额表数据
				//附加项打印
				String memo0 = null == ckd.getMemo() ? "" : ckd.getMemo();
				String memo1 = "";
				if(memo0.contains("##")){
					memo1 = memo0.split("##")[1];
					memo0 = memo0.split("##")[0];
				}
				ckd.setMemo(memo0);
				ckd.setMemo1(memo1);
			}
		}	
		
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("申购数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date); 
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/chkstom/printChkstom.do?chkstoNo="+chkstod.getChkstoNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,realUrl,realUrl);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        //return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 跳转到导入页面   报货单导入 wjf
	 */
	@RequestMapping("/importChkstom")
	public ModelAndView importChkstom(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkstomConstants.IMPORT_CHKSTOM,modelMap);
	}
	
	/**
	 * 下载模板信息 报货单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		chkstomService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = chkstomService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkstomConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入报货单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"导入报货单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId = session.getAttribute("accountId").toString();
		Object obj=chkstomService.check(realFilePath, accountId);
		if(obj instanceof Chkstom){//导入对了
			String accountName=session.getAttribute("accountName").toString();
			((Chkstom) obj).setMadeby(accountName);//得到当前操作人
			((Chkstom) obj).setChkstoNo(chkstomService.getMaxChkstono());//获取最大单号
			modelMap.put("sta", "add");
			modelMap.put("positnList",positnService.findAllPositn(null));
			modelMap.put("chkstom", (Chkstom)obj);
			modelMap.put("importFlag","OK");
			modelMap.put("bhfl", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "BH_FJX_LEN"));//判断附加项长度
			List<Chkstod> chkstods = ((Chkstom) obj).getChkstodList();
			//在这里为物资查找单价，在读报表的时候查太麻烦了。。。
			//1.//判断是取报价还是售价 出库售价 入库报价
			Positn positn = new Positn();
			positn.setCode(((Chkstom) obj).getFirm());
			for(Chkstod chkstod:chkstods){
				String rs = findTypByPositn(positn,chkstod.getSupply());
				if("RK".equals(rs)){//入库取报价
					Spprice spprice = new Spprice();
					spprice.setSp_code(chkstod.getSupply().getSp_code());
					spprice.setArea(((Chkstom) obj).getFirm());
					spprice.setMadet(((Chkstom) obj).getMadet());
					spprice.setAcct(accountId);
					spprice = supplyService.findBprice(spprice);
					if(spprice != null){
						chkstod.getSupply().setSp_price(Double.parseDouble(spprice.getPrice()+""));
					}
				}else{//出库取售价
					SppriceSale sppriceSale = new SppriceSale();
				 	Supply supply = new Supply();
				 	supply.setSp_code(chkstod.getSupply().getSp_code());
				 	Positn positn1 = new Positn();
				 	positn1.setCode(((Chkstom) obj).getFirm());
				 	sppriceSale.setSupply(supply);
				 	sppriceSale.setArea(positn1);
				 	sppriceSale.setMadet(((Chkstom) obj).getMadet());
				 	sppriceSale.setAcct(accountId);
				 	sppriceSale = supplyService.findSprice(sppriceSale);
				 	if(sppriceSale != null){
				 		chkstod.getSupply().setSp_price(sppriceSale.getPrice());
				 	}
				}
			}
			modelMap.put("chkstodList", chkstods);
			return new ModelAndView(ChkstomConstants.TABLE_CHKSTOM_NEW,modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkstomConstants.TABLE_CHKSTOM_NEW,modelMap);
		}
	}
	
	/***
	 * 判断物资取什么价格wjf
	 * @param positn
	 * @param supply
	 * @return
	 * @throws Exception
	 */
	private String findTypByPositn(Positn positn,Supply supply) throws Exception{
		String result="CK";
		positn = positnService.findPositnByCode(positn);
		String typ=positn.getTyp();
		if("1201".equals(typ)||"1202".equals(typ)){
			result="RK";
		}else{//仓位是分店，判断是不是直配方向 直配也和入库一样 也取报价   wjf 
			supply = supplyService.findSupplyById(supply);
			Positn positn1 = new Positn();
			positn1.setCode(supply.getSp_position());
			positn1 = positnService.findPositnByCode(positn1);
			FirmDeliver firmDeliver = new FirmDeliver();
			firmDeliver.setFirm(positn.getCode());
			firmDeliver.setInout(1);//1是直配?   0是直发?
			List<FirmDeliver> firmDelivers = deliverDirectService.findFrimDeliverByFirm(firmDeliver);
			if("1201".equals(positn1.getTyp()) && firmDelivers != null){//如果是默认仓位是物流中心且设置了供应商直配
				result  = "RK";//直配和入库一样也取报价
			}
		}
		return result;
	}
	
	/***************************************安全库存报货2015.5.12wjf (如需加方法  加到此分割线之上)****************************************/
	/**
	 * 进入安全库存报货
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tableSafeChkstom")
	public ModelAndView tableSafeChkstom(ModelMap modelMap, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"进入安全库存报货:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountId=session.getAttribute("accountId").toString();
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		return new ModelAndView(ChkstomConstants.TABLE_SAFE_CHKSTOM,modelMap);
	}
	
	/**
	 * 查询安全库存报货
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSafeChkstom")
	public ModelAndView findSafeChkstom(ModelMap modelMap, HttpSession session,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询安全库存报货:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountId=session.getAttribute("accountId").toString();
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		modelMap.put("chkstom", chkstom);
		//查询库存
		List<Chkstod> list = chkstomService.findSafeChkstodList(chkstom);
		modelMap.put("chkstodList", list);
		return new ModelAndView(ChkstomConstants.TABLE_SAFE_CHKSTOM,modelMap);
	}
	
	/**
	 * 保存安全库存报货
	 */
	@RequestMapping(value = "/saveBySafeChkstom")
	@ResponseBody
	public Object saveBySafeChkstom(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"新增安全库存报货:单号("+chkstom.getChkstoNo()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		chkstom.setMadeby(session.getAttribute("accountName").toString());
		return chkstomService.saveOrUpdateChk(chkstom, "add");
	}
	
	/*************************************************九毛九报货单状态查询功能**********************************************************/
	
	/**
	 * 分店报货单状态查询
	 * @param modelMap
	 * @param session
	 * @param chkstom
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listChkstomState")
	public ModelAndView listChkstomState(ModelMap modelMap,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"分店报货单状态查询",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//接收日期，然后查询有哪些店上传与未上传
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null == chkstom.getMaded()){
			chkstom.setMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		modelMap.put("chkstomList", chkstomService.listChkstomState(chkstom));
		Integer isReportJmj = 0;
		chkstom.setBak2(isReportJmj);
		modelMap.put("chkstom", chkstom);
		
		return new ModelAndView(ChkstomConstants.LIST_CHKSTOM_STATE,modelMap);
	}
	
	
	/***************************************手机报货相关2015.1.10wjf (如需加方法  加到此分割线之上)****************************************/
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdateWAP")
	@ResponseBody
	public Object saveByAddOrUpdateWAP(String sta,Chkstom chkstom) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		for(Chkstod chkstod:chkstom.getChkstodList()){
			chkstod.setMemo(URLDecoder.decode(chkstod.getMemo(), "UTF-8"));
		}
		//当前帐套
		if("add".equals(sta)){
			Integer chkstono = chkstomService.getMaxChkstono();
			chkstom.setChkstoNo(chkstono);
			chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",chkstom.getMaded()));
			map.put("chkstoNo", chkstono);
		}else{
			Chkstom chkstom1 = chkstomService.findByChkstoNo(chkstom);
			chkstom.setVouno(chkstom1.getVouno());
		}
		map.put("pr", chkstomService.saveOrUpdateChk(chkstom, sta));
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 审核
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkChkstomWAP")
	@ResponseBody
	public Object checkChkstomWAP(Chkstom chkstom,String chkstoNoIds,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result= chkstomService.checkChk(chkstom,chkstoNoIds);
		return  jsonpcallback + "(" + result + ");";
	}
	
	/**
	 * 查找报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChkstomListWAP")
	@ResponseBody
	public Object findChkstomListWAP(Page page,Chkstom chkstom,String checked,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstom.setbMaded(DateFormat.formatDate(chkstom.getMaded(), "yyyy-MM-dd"));
		chkstom.seteMaded(DateFormat.formatDate(chkstom.getMaded(), "yyyy-MM-dd"));
		//把参数放到MAP
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		chkstomMap.put("checked", checked);
		chkstomMap.put("chkstom", chkstom);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("chkstomList", chkstomService.findByKey(chkstomMap,page));//改为查带适用分店权限的2014.11.18wjf
		map.put("page", page);
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 查看已审核报货单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findCheckedChkstomWAP")
	@ResponseBody
	public Object findCheckedChkstomWAP(Chkstod chkstod,Chkstom chkstom,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String,Object>();
		//关键字查询
		map.put("chkstom", chkstomService.findByChkstoNo(chkstom));
		map.put("chkstodList", chkstodService.findByChkstoNo(chkstod));
		return jsonpcallback + "(" + JSONObject.fromObject(map).toString() + ");";
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deleteChkstomWAP")
	@ResponseBody
	public Object deleteChkstomWAP(Chkstom chkstom,String jsonpcallback) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result = chkstomService.deleteChkstom(chkstom,null);
		return  jsonpcallback + "(" + result + ");";
	}
}
