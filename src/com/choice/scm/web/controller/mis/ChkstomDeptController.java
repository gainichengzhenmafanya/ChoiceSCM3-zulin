package com.choice.scm.web.controller.mis;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkstomDeptConstants;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkstodemoService;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.ChkstodDeptService;
import com.choice.scm.service.mis.ChkstomDeptService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 报货单新增、修改、删除、查询、打印、审核
 * @author css
 *
 */
@Controller
@RequestMapping(value = "chkstomDept")

public class ChkstomDeptController {

	@Autowired
	private ChkstomDeptService chkstomDeptService;
	@Autowired
	private ChkstodDeptService chkstodDeptService;
	@Autowired
	private ChkstodemoService chkstodemoService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	private AccountPositnService accountPositnService;
	
	/**
	 * 查询所有未审核报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllChkstom(ModelMap modelMap,Page page,HttpSession session,String tableForm) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("tableForm", tableForm);
		//报货单主页面
		return new ModelAndView(ChkstomDeptConstants.TABLE_CHKSTOM_NEW,modelMap);
	}
	
	/**
	 * 添加操作时候，刷新页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkstom")
	public ModelAndView addChkstom(ModelMap modelMap,Page page,HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomDeptService.getMaxChkstono());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			chkstom.setFirm(positnCode);
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("sta", "add");
		return new ModelAndView(ChkstomDeptConstants.TABLE_CHKSTOM_NEW,modelMap);
	}	
	
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap,Page page,HttpSession session,String sta,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if("add".equals(sta)){
			chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getDept()+"-",new Date()));
		}else{
			Chkstom chkstom1 = chkstomDeptService.findByChkstoNo(chkstom);
			chkstom.setVouno(chkstom1.getVouno());
		}
		
		return chkstomDeptService.saveOrUpdateChk(chkstom, sta);
	}
	
	/**
	 * 档口报货上传
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap,HttpSession session, Chkstom chkstom,Page page,String sp_code) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null == chkstom.getbMaded()){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				chkstom.setFirm(positnCode);
			}
		}
		chkstom.setBak1("N");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		modelMap.put("chkstomList", chkstomDeptService.findByKey(chkstomMap,page));
		if (null != chkstom.getFirm() && !"".equals(chkstom.getFirm())) {
			Positn positn = new Positn();
			positn.setUcode(chkstom.getFirm());
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomDeptConstants.LIST_CHKSTOM,modelMap);
	}
	
	/**
	 * 双击查找详情
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findDetail")
	public ModelAndView findDetail(ModelMap modelMap,HttpSession session,Chkstom chkstom, Chkstod chkstod) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		chkstom = chkstomDeptService.findByChkstoNo(chkstom);
		Positn p = new Positn();
		p.setCode(chkstom.getDept());
		chkstom.setDept(positnService.findPositnByCode(p).getDes());
		modelMap.put("chkstom", chkstom);
		modelMap.put("chkstodList", chkstodDeptService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomDeptConstants.TABLE_DETAIL_NEW,modelMap);
	}
	
	/**
	 * 生成门店报货单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toChkstom")
	public ModelAndView update(ModelMap modelMap, Chkstom chkstom, String idList, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		//获取最大单号
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			chkstom.setFirm(positnCode);
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("idList", idList);
		modelMap.put("positnList",positnService.findAllPositn(null));
		Chkstod chkstod = new Chkstod();
		chkstod.setChkstoNos(idList);
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkstodList", chkstodDeptService.findByChkstoNos(chkstod));
		return new ModelAndView(ChkstomDeptConstants.TABLE_DETAIL_2_NEW,modelMap);
	}
	
	/**
	 * 查找报货单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String sp_code,String init,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		if(null!=init && !"".equals(init)){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				chkstom.setFirm(positnCode);
			}
		}
		chkstom.setBak1("N");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		modelMap.put("chkstomList", chkstomDeptService.findByKey(chkstomMap,page));
		if (null != chkstom.getFirm() && !"".equals(chkstom.getFirm())) {
			Positn positn = new Positn();
			positn.setUcode(chkstom.getFirm());
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomDeptConstants.SEARCH_CHKSTOM,modelMap);
	}
	
	/**
	 * 双击查找
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findChk")
	public ModelAndView findChk(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//从报货单弹窗查询页面上的双击单条数据，进行查看申购的物资详细
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			Positn positn = new Positn();
			positn.setUcode(positnCode);
			modelMap.put("positnList", positnService.findDeptByPositn(positn));//根据门店编码获取所有部门
		}
		modelMap.put("chkstom", chkstomDeptService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodDeptService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomDeptConstants.TABLE_CHKSTOM_NEW,modelMap);
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deleteChkstom")
	@ResponseBody
	public Object deleteChkstom(ModelMap modelMap,HttpSession session,Page page,String chkstoNoIds,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//报货单填制页面上的整条删除
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		return chkstomDeptService.deleteChkstom(chkstom,chkstoNoIds);
	}
	
	/**
	 * 确认修改，添加模板数据到报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/enterUpdate")
	@ResponseBody
	public Object enterUpdate(ModelMap modelMap, HttpSession session, String rec2, String cnt, String cnt1, String title) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//申购物资批量添加时候的数量假修改
		String acct=session.getAttribute("ChoiceAcct").toString();
		List<String> recList=Arrays.asList(rec2.split(","));
		List<String> cntList=Arrays.asList(cnt.split(","));
		List<String> cnt1List=Arrays.asList(cnt1.split(","));
		return chkstodemoService.findChkstodemoByRec(recList, cntList, cnt1List, acct, title);
	}
	
	/**
	 * 报货单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printChkstom")
	public ModelAndView printChkstomm(ModelMap modelMap,HttpSession session,Page page,String type,Chkstod chkstod)throws CRUDException
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("chkstod", chkstod);
		List<Chkstod> list=chkstodDeptService.findAllChkstod(chkstod);
		
		if(list!=null && list.size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkstod ckd:list){
				supplyAcct.setPositn(ckd.getPositn());
				supplyAcct.setSp_code(ckd.getSupply().getSp_code());
				ckd.getSupply().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}	
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("申购数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date); 
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/chkstom/printChkstom.do?chkstoNo="+chkstod.getChkstoNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkstomDeptConstants.REPORT_PRINT_URL,ChkstomDeptConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        //return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
		return new ModelAndView(rs.get("url"),modelMap);
	}	
}
