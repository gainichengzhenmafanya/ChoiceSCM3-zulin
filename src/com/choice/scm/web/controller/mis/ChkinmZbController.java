package com.choice.scm.web.controller.mis;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmZbConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.ChkinmZbService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 门店直拨单
 * @author css
 *
 */
@Controller
@RequestMapping(value = "chkinmZb")
public class ChkinmZbController {

	@Autowired
	private ChkinmZbService chkinmZbService;
	@Autowired
	PositnService positnService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private CalChkNum calChkNum;
//	@Autowired
//	private PositnRoleService positnRoleService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private CodeDesService codeDesService;
    /**
     * 查询入库单list
     * @param checkOrNot   是否审核
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     * @param inout   zf  表示查询直发    rk  表示rk
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllChkinm(ModelMap modelMap, String checkOrNot, Chkinm chkinm, Date madedEnd, String startDate, Page page, String action,HttpSession session)
		throws Exception {
//			if("CPRK".equals(chkinm.getTyp())){
//				chkinm.setTyp("产品入库");
//			}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positnCode="";
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			positnCode = accountPositn.getPositn().getCode();
			positnList.add(accountPositn.getPositn());
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkinm.setPositn(positn);
		}
	
		if(null!=action && "init".equals(action)){
			Date date=new Date();
			if(null!=startDate && !"".equals(startDate)){
				chkinm.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
			}else{
				chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
			}
			modelMap.put("chkinmList", chkinmZbService.findAllChkinm(checkOrNot,chkinm,page,DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", date);
		}else{
			modelMap.put("chkinmList", chkinmZbService.findAllChkinm(checkOrNot,chkinm,page,madedEnd,session.getAttribute("locale").toString()));
			modelMap.put("madedEnd", madedEnd);
		}
		modelMap.put("chkinm", chkinm);
		modelMap.put("checkOrNot", checkOrNot);
		modelMap.put("pageobj", page);
		if("check".equals(checkOrNot)){
			return new ModelAndView(ChkinmZbConstants.LIST_CHECKED_CHKINM, modelMap);
		}else{
			return new ModelAndView(ChkinmZbConstants.LIST_CHKINM, modelMap);
		}
	}
	

	/**
	 * 获取入库单单号
	 * @param maded
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded) throws ParseException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return calChkNum.getNext(CalChkNum.CHKZB, DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}
	
	/**
	 * 添加入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value="/saveChkinm")
//	public ModelAndView saveChkinm(ModelMap modelMap, Chkinm chkinm)throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		chkinmZbService.saveChkinm(chkinm,"in");
//		return new ModelAndView(ChkinmZbConstants.SAVE_CHKINM,modelMap);	
//	}
	
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(HttpSession session, ModelMap modelMap, Chkinm chkinm, String isCheck, String inout) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("ZF");
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			String positnCode = accountPositn.getPositn().getCode();
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkinm.setPositn(positn);
		}
		//单据类型
		modelMap.put("billType", codeDesService.findDocumentType(codeDes));
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
//		modelMap.put("positnList", positnService.findAllPositn(new Positn()));//仓位
		modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
		modelMap.put("chkinm",chkinmZbService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");//当前页面状态
		if(null!=isCheck && !"".equals(isCheck)){
			return new ModelAndView(ChkinmZbConstants.SEARCH_CHECKED_CHKINMZB,modelMap);
		}else{
			return new ModelAndView(ChkinmZbConstants.UPDATE_CHKINMZB,modelMap);
		}
	}

	/**
	 * 审核入库单  单条审核
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkChkinm")
	@ResponseBody
	public Object checkChkinm(ModelMap modelMap, Chkinm chkinm, HttpSession session, String zb)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountId);
		String pr=chkinmZbService.checkChkinm(chkinm,null);
		return pr;
//		if("zb".equals(zb))
//			return new ModelAndView(ChkinmZbConstants.UPDATE_CHKINMZB,modelMap);	
//		else
//			
//			return new ModelAndView(ChkinmZbConstants.UPDATE_CHKINM,modelMap);
	}
	/**
	 * 审核操作  批量审核
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids, String checkOrNot, Chkinm chkinm, Date madedEnd, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountName=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountName);
		chkinmZbService.checkChkinm(chkinm,chkinnoids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除入库单
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String chkinnoids, Page page, String checkOrNot, Chkinm chkinm, Date madedEnd, String action, String flag) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		chkinmZbService.deleteChkinm(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else{
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmZbConstants.UPDATE_CHKINMZB, modelMap);
		}
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap,Chkinm chkinm,String type,Page page,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		chkinm=chkinmZbService.findChkinmByid(chkinm);
			List<Chkind> chkindList=chkinm.getChkindList();
			modelMap.put("List",chkindList);
	 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
	 		String report_name = "直拨单";
	        parameters.put("report_name", report_name); 
	        if(null!=chkinm.getDeliver() && null!= chkinm.getDeliver().getDes()){
	        	parameters.put("deliver.des", chkinm.getDeliver().getDes()); 
	        }
	        parameters.put("positn.des", chkinm.getPositn().getDes()); 
	        parameters.put("vouno", chkinm.getVouno()); 
	        parameters.put("maded", DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")); 
	        parameters.put("madeby", chkinm.getMadeby()); 
	        parameters.put("checby", chkinm.getChecby()); 
	        CodeDes codeDes = new CodeDes();
			codeDes.setCode(chkinm.getTyp());
			codeDes.setLocale(session.getAttribute("locale").toString());
			parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//	        parameters.put("typ", chkinm.getTyp());
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("chkinno", chkinm.getChkinno()+"");
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/chkinm/viewChkinm.do");//传入回调路径
 		Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkinmZbConstants.REPORT_URL_ZB,ChkinmZbConstants.REPORT_URL_ZB);//判断跳转路径
 		modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
        
	}
	
	/**
	 * 转入直拨单添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addzb")
	public ModelAndView addzb(ModelMap modelMap, HttpSession session, String action)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String positnCode="";
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp("ZF");
			Chkinm chkinm=new Chkinm();
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService
					.findAccountById(accountId);
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != accountPositn && null != accountPositn.getPositn()) {
				positnCode = accountPositn.getPositn().getCode();
				positnList.add(accountPositn.getPositn());
				Positn positn = new Positn();
				positn.setCode(positnCode);
				chkinm.setPositn(positn);
			}
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
			chkinm.setChkinno(chkinmZbService.getMaxChkinno());
			modelMap.put("chkinm",chkinm);//日期等参数
			modelMap.put("billType", codeDesService.findDocumentType(codeDes));
			modelMap.put("positnList", positnList);

			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
//			modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}
		return new ModelAndView(ChkinmZbConstants.UPDATE_CHKINMZB,modelMap);	
	}
	
	/**
	 * 修改直拨单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinmzb")
	@ResponseBody
	public int updateChkinmzb(ModelMap modelMap,HttpSession session,Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Chkinm chkinms=chkinmZbService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		if(null!=chkinms){//存在则修改
			chkinm.setInout(ChkinmZbConstants.conk);
			result=chkinmZbService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(ChkinmZbConstants.conk);
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB, chkinm.getMaded()));//从新获得单号
			result=chkinmZbService.saveChkinm(chkinm,"zb");
		}
		return result;	
	}
	
	/**
	 * 查询是否必须入到默认仓位
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/chkPositn")
	@ResponseBody
	public String chkPositn(ModelMap modelMap, HttpSession session, SpCodeMod spCodeMod)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spCodeMod.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmZbService.chkPositn(spCodeMod);
	}
	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param modelMap
	 * @param session
	 * @param chkinm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value="/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return chkinmZbService.chkChect(active,chkinm);
	}
	
	/**
	 * ajax查询分店信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findAllPositnDeptN", method=RequestMethod.POST)
	@ResponseBody
	public List<Positn> findAllPositnN(Positn positn, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService
					.findAccountById(accountId);
			if (null != accountPositn && null != accountPositn.getPositn()) {
				String positnCode = accountPositn.getPositn().getCode();
				positn.setUcode(positnCode);
				return positnService.findDeptByPositn(positn);//根据门店编码获取所有部门
			}else{
				return null;
			}
	}
	
	/**
	 * 直发物资冲消查询界面
	 * @author 2014.11.4wjf
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmzfByCx")
	public ModelAndView addChkinmzfByCx(ModelMap modelMap, Spbatch spbatch, String bdat, String edat,String action,HttpSession session) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (bdat == null) {
			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
		}else {
			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
		}
		if (edat == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else {
			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		}
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			spbatch.setPositn(accountPositn.getPositn().getCode());
		}
		if(!"init".equals(action)){
			modelMap.put("spbatchList", chkinmZbService.addChkinmzfByCx(spbatch));//获取直发数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmZbConstants.TABLE_CHKINMZF_CX,modelMap);
	}
	
	/**
	 * 冲消查询界面
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value = "/addChkinmByCx")
//	public ModelAndView addChkinmByCx(ModelMap modelMap, Page page, HttpSession session, Spbatch spbatch, String bdat, String edat) throws Exception
//	{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		
//		if (bdat == null) {
//			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
//		}else {
//			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
//		}
//		if (edat == null) {
//			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
//		}else {
//			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
//		}
//		
//		modelMap.put("spbatchList", chkinmService.addChkinmByCx(spbatch));//获取入库数据
//		modelMap.put("spbatch", spbatch);
//		return new ModelAndView(ChkinmZbConstants.TABLE_CHKINM_CX,modelMap);
//	}	
}
