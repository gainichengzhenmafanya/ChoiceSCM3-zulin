package com.choice.scm.web.controller.mis;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkoutDbConstants;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.MisChkoutService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/***
 * 门店调拨单
 * @author wjf
 *
 */
@Controller
@RequestMapping("/chkoutDb")
public class ChkoutDbController {

	@Autowired
	private MisChkoutService MischkoutService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private LogsMapper LogsMapper;
	
	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom,Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		CodeDes codes = new CodeDes();		
		codes.setCode("9909");//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		if (action == null || !action.equals("init")) {
			chkoutm.setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setChkoutno(MischkoutService.findNextNo().intValue());
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != accountPositn && null != accountPositn.getPositn()) {
				positnList.add(accountPositn.getPositn());
				Positn p = new Positn();
				p.setUcode(accountPositn.getPositn().getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			//出库仓位
			modelMap.put("positnOut", positnList);
			//领用仓位   查询所有门店，及盘亏1999，前台js 过滤掉自己
			Positn positn1=new Positn();
			positn1.setTypn("'1203'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			modelMap.put("positnIn", lyList);
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(ChkoutDbConstants.UPDATE_CHKOUT, modelMap);
	}
	
	/**
	 * 通过id获取出库单详细信息
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @author yp
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getChkoutById")
	public ModelAndView findChkoutById(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", MischkoutService.findChkoutmById(chkoutm));
		return new ModelAndView(ChkoutDbConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 模糊查询调拨单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,String action, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"MISBOH模糊查询调拨出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		LogsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			//查询该分店的档口
			Positn positn = new Positn();
			positn.setUcode(accountPositn.getPositn().getCode());
			List<Positn> list = positnService.findPositnSuperNOPage(positn);
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(accountPositn.getPositn().getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
		}
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		}
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		//只查调拨 
		chkoutm.setTyp("9909");//调拨出库
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("chkoutList", MischkoutService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),"db"));
		return new ModelAndView(ChkoutDbConstants.MANAGE_CHKOUT, modelMap);
	}

	

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus, String scrapped,String scrappedname) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String result = null;
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (curStatus.equals("add")) {
			result = MischkoutService.saveChkoutm(chkoutm);
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"MISBOH添加调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			LogsMapper.addLogs(logd);
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(accountService.findAccountById(
					session.getAttribute("accountId").toString()).getName());
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"MISBOH更新调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			LogsMapper.addLogs(logd);
			result = MischkoutService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"Misboh删除调拨单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		LogsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return MischkoutService.deleteChkoutm(chkoutm);
	}

	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = MischkoutService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				supplyAcct.setPositn(chkoutm.getPositn().getCode());
				supplyAcct.setSp_code(ckd.getSp_code().getSp_code());
				ckd.getSp_code().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}		
		modelMap.put("chkoutm",chkoutm );
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			positnList.add(accountPositn.getPositn());
			Positn p = new Positn();
			p.setUcode(accountPositn.getPositn().getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		//出库仓位
		modelMap.put("positnOut", positnList);
		CodeDes codes = new CodeDes();		
		codes.setCode("9909");//调拨出库
		List<CodeDes> billTypes = new ArrayList<CodeDes>();
		billTypes.add(codeDesService.findDocumentByCode(codes));
		modelMap.put("billType", billTypes);
		
		//领用仓位   查询所有门店，前台js 过滤掉自己
		Positn positn1=new Positn();
		positn1.setTypn("'1203'");
		List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
		modelMap.put("positnIn", lyList);
		modelMap.put("curStatus", "show");
		return new ModelAndView(ChkoutDbConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 审核出库单
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("auditChkout")
	@ResponseBody
	public Object auditChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"MISBOH审核调拨单:单号("+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		LogsMapper.addLogs(logd);
		try {
			String chk1memo = chkoutm.getChk1memo();//审核备注  wjf
			chkoutm.setChk1memo(chk1memo);
		    String chkectby=session.getAttribute("accountName").toString();
			chkoutm.setChecby(chkectby);
			return MischkoutService.updateByAudit(chkoutm,chkectby);  //修改洞庭反应的问题2014.11.21
		} catch (Exception e) {
			return e.getLocalizedMessage();
		}
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"MISBOH调拨单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		LogsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = MischkoutService.findChkoutmById(chkoutm);
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action",
				"/chkoutDbMis/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				ChkoutDbConstants.REPORT_PRINT_URL,
				ChkoutDbConstants.REPORT_EXP_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 获取出库单单号
	 * 
	 * @param maded
	 * @return
	 * @throws ParseException
	 * @author yp
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded) throws ParseException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return calChkNum.getNext(CalChkNum.CHKOUT,
				DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}

	/**
	 * 操作成功页面跳转
	 * 
	 * @return
	 * @author yp
	 */
	@RequestMapping("success")
	public ModelAndView success() {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param modelMap
	 * @param session
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value = "/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session,
			String active, Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		return MischkoutService.chkChect(active, chkoutm);
	}

}
