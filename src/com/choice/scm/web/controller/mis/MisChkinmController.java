package com.choice.scm.web.controller.mis;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.FirmDeliverService;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.MisChkinmService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;
/**
 * 门店入库单
 * @author lehui 
 *
 */
@Controller
@RequestMapping(value = "misChkinm")
public class MisChkinmController {

	@Autowired
	private MisChkinmService misChkinmService;
	@Autowired
	PositnService positnService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private FirmDeliverService firmDeliverService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private CodeDesService codeDesService;
    /**
     * 查询门店入库单list
     * @param checkOrNot   是否审核
     * @param madedEnd  
     * @param searchDate
     * @param action  init表示初始页面
     * @param inout   zf  表示查询直发    rk  表示rk
     */
	@RequestMapping(value = "/list")
	public ModelAndView findAllChkinm(ModelMap modelMap, String checkOrNot, Chkinm chkinm, Date madedEnd, String startDate, Page page, String action,HttpSession session)
			throws Exception {
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
					String accountId = session.getAttribute("accountId").toString();
					AccountPositn accountPositn = accountPositnService
							.findAccountById(accountId);
					if (null != accountPositn && null != accountPositn.getPositn()) {
						//查询该分店的档口
						Positn positn = new Positn();
						positn.setUcode(accountPositn.getPositn().getCode());
						List<Positn> list = positnService.findPositnSuperNOPage(positn);
						List<String> listPositn = new ArrayList<String>();
						for(Positn posi:list){
							listPositn.add(posi.getCode());
						}
						listPositn.add(accountPositn.getPositn().getCode());//加上分店本身
						chkinm.setListPositn(listPositn);
					}
		
				if("CPRK".equals(chkinm.getTyp())){
					chkinm.setTyp("9905");
				}
				
				Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
						"查询门店入库单 ",session.getAttribute("ip").toString(),ProgramConstants.SCM);
				logsMapper.addLogs(logd);
				if(null!=action && "init".equals(action)){
					Date date=new Date();
					if(null!=startDate && !"".equals(startDate)){
						chkinm.setMaded(DateFormat.getDateByString(startDate, "yyyy-MM-dd"));
					}else{
						chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
					}
					modelMap.put("chkinmList", misChkinmService.findAllChkinm(checkOrNot,chkinm,page,DateFormat.formatDate(date, "yyyy-MM-dd"),session.getAttribute("locale").toString()));
					modelMap.put("madedEnd", date);
				}else{
					modelMap.put("chkinmList", misChkinmService.findAllChkinm(checkOrNot,chkinm,page,madedEnd,session.getAttribute("locale").toString()));
					modelMap.put("madedEnd", madedEnd);
				}
				modelMap.put("chkinm", chkinm);
				modelMap.put("checkOrNot", checkOrNot);
				modelMap.put("pageobj", page);
				if("check".equals(checkOrNot)){
					return new ModelAndView(ChkinmConstants.MIS_LIST_CHECKED_CHKINM, modelMap);
				}else{
					return new ModelAndView(ChkinmConstants.MIS_LIST_CHKINM, modelMap);
		}
	}
	
	/**
	 * 转入添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/add")
	public ModelAndView add(ModelMap modelMap, HttpSession session, String action, String tableFrom)throws Exception{
		String inout=null;
		String positnCode="";
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			//判断用户选择的语言系统
			String locale = session.getAttribute("locale").toString();
			CodeDes codeDes = new CodeDes();
			codeDes.setLocale(locale);
			codeDes.setCodetyp("RK");
			modelMap.put("billType", codeDesService.findDocumentType(codeDes));//单据类型
			//判断  当报价为0时候   是否 可以修改入库价格  
			modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
			if(!"init".equals(action)){
				String accountName=session.getAttribute("accountName").toString();
				Chkinm chkinm=new Chkinm();
				chkinm.setMadeby(accountName);
				chkinm.setMaded(new Date());
				chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
				chkinm.setChkinno(misChkinmService.getMaxChkinno());
				inout=ChkinmConstants.MIS_UPDATE_CHKINM;
				
				// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
				String accountId = session.getAttribute("accountId").toString();
				AccountPositn accountPositn = accountPositnService
						.findAccountById(accountId);
				List<Positn> positnList = new ArrayList<Positn>();
				if (null != accountPositn && null != accountPositn.getPositn()) {
					positnCode = accountPositn.getPositn().getCode();
					positnList.add(accountPositn.getPositn());
					Positn positn = new Positn();
					positn.setCode(positnCode);
					chkinm.setPositn(positn);
					//查询该分店的档口
					Positn p = new Positn();
					p.setUcode(accountPositn.getPositn().getCode());
					positnList.addAll(positnService.findPositnSuperNOPage(p));
				}
				modelMap.put("chkinm",chkinm);//日期等参数
				modelMap.put("positnList", positnList);
				modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));
				modelMap.put("curStatus", "add");//当前页面状态
			}else{
				modelMap.put("tableFrom", tableFrom);//当前页面状态
				inout=ChkinmConstants.MIS_UPDATE_CHKINM;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return new ModelAndView(inout,modelMap);	
	}
	/**
	 * 获取门店入库单单号
	 * @param maded
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded) throws ParseException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return calChkNum.getNext(CalChkNum.CHKIN, DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}
	
	/**
	 * 添加门店入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveChkinm")
	public ModelAndView saveChkinm(ModelMap modelMap, Chkinm chkinm,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"添加门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		misChkinmService.saveChkinm(chkinm,"in");
		return new ModelAndView(ChkinmConstants.MIS_SAVE_CHKINM,modelMap);	
	}
	
	/**
	 * 修改跳转
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/update")
	public ModelAndView update(ModelMap modelMap,HttpSession session, Chkinm chkinm, String isCheck, String inout) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		//用户选择语言
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("RK");
		modelMap.put("billType", codeDesService.findDocumentType(codeDes));
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		String positnCode = "";
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			positnCode = accountPositn.getPositn().getCode();
			positnList.add(accountPositn.getPositn());
			//查询该分店的档口
			Positn p = new Positn();
			p.setUcode(accountPositn.getPositn().getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		modelMap.put("positnList", positnList);
		modelMap.put("deliverList",firmDeliverService.findDeliverByPositn(positnCode));
		modelMap.put("chkinm",misChkinmService.findChkinmByid(chkinm));
		modelMap.put("curStatus", "show");//当前页面状态
		if(null!=isCheck && !"".equals(isCheck)){
			if(null!=inout && "zf".equals(inout))
				return new ModelAndView(ChkinmConstants.MIS_SEARCH_CHECKED_CHKINMZB,modelMap);
			else
				return new ModelAndView(ChkinmConstants.MIS_SEARCH_CHECKED_CHKINM,modelMap);
				
		}else{
			if(null!=inout && "zf".equals(inout)){
				return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINMZB,modelMap);
			}else{
				return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM,modelMap);
			}
		}	
	}

	/**
	 * 修改门店入库单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinm")
	@ResponseBody
	public int updateChkinm(ModelMap modelMap, HttpSession session, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"修改门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Chkinm chkinms=misChkinmService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		if(null!=chkinms){//存在则修改
			chkinm.setInout(ChkinmConstants.in);
			result=misChkinmService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN, chkinm.getMaded()));//从新获得单号
			result=misChkinmService.saveChkinm(chkinm,"in");
		}
		return result;	
	}
	/**
	 * 审核门店入库单  单条审核
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkChkinm")
	@ResponseBody
	public Object checkChkinm(ModelMap modelMap, Chkinm chkinm, HttpSession session, String zb)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"审核门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountId);
		String pr=misChkinmService.checkChkinm(chkinm,null);
		return pr;
//		if("zb".equals(zb))
//			return new ModelAndView(ChkinmConstants.UPDATE_CHKINMZB,modelMap);	
//		else
//			
//			return new ModelAndView(ChkinmConstants.UPDATE_CHKINM,modelMap);
	}
	/**
	 * 审核操作  批量审核
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAll")
	public ModelAndView checkAll(ModelMap modelMap, String chkinnoids, String checkOrNot, Chkinm chkinm, Date madedEnd, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"批量审核门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		chkinm.setChecby(accountName);
		misChkinmService.checkChkinm(chkinm,chkinnoids);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除门店入库单
	 * @param modelMap
	 * @param chkinno
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap, String chkinnoids, Page page, String checkOrNot, Chkinm chkinm, Date madedEnd, String action, String flag,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"删除门店入库单:单号("+chkinm.getChkinno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		//判断  当报价为0时候   是否 可以修改入库价格  
		modelMap.put("YNChinmSpprice",ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CHKINM_SPPRICE_IS_YN"));
		List<String> ids = Arrays.asList(chkinnoids.split(","));
		misChkinmService.deleteChkinm(ids);
		if(!"init".equals(action)){
			return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		}else if("zb".equals(flag)){
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINMZB, modelMap);
		}else{
			modelMap.put("chkinm", null);
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM, modelMap);
		}
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param chkinm
	 * @param type
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/viewChkinm")
	public ModelAndView viewChkinm(ModelMap modelMap,Chkinm chkinm,String type,Page page,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"打印门店入库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkinm=misChkinmService.findChkinmByid(chkinm);
			List<Chkind> chkindList=chkinm.getChkindList();
//			int num=chkindList.size()%12;
//			for (int i = 0; i < 12-num; i++) {
//				chkindList.add(null);
//			}
			modelMap.put("List",chkindList);
	 		HashMap<String, Object>  parameters = new HashMap<String, Object>(); 
	 		String report_name = "门店入库单";
	 		if(chkinm.getVouno().contains("ZB")){
	 			report_name = "直拨单";
	 		}
	        parameters.put("report_name", report_name); 
	        if(null!=chkinm.getDeliver() && null!= chkinm.getDeliver().getDes()){
	        	parameters.put("deliver.des", chkinm.getDeliver().getDes()); 
	        }
	        parameters.put("positn.des", chkinm.getPositn().getDes()); 
	        parameters.put("vouno", chkinm.getVouno()); 
	        parameters.put("maded", DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")); 
	        parameters.put("madeby", chkinm.getMadebyName()); 
	        parameters.put("checby", chkinm.getChecbyName());
	        CodeDes codeDes = new CodeDes();
			codeDes.setCode(chkinm.getTyp());
			codeDes.setLocale(session.getAttribute("locale").toString());
			parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//	        parameters.put("typ", chkinm.getTyp());
	    HashMap<String, String> map=new HashMap<String, String>();
	    map.put("chkinno", chkinm.getChkinno()+"");
        modelMap.put("parameters", parameters);
        modelMap.put("actionMap", map);
	 	modelMap.put("action", "/chkinm/viewChkinm.do");//传入回调路径
	 	if(chkinm.getVouno().contains("ZB")){
	 		Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkinmConstants.REPORT_URL_ZB,ChkinmConstants.REPORT_URL_ZB);//判断跳转路径
	 		modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
			return new ModelAndView(rs.get("url"),modelMap);
 		}else{
 			Map<String,String> rs=ReadReportUrl.redReportUrl(type,ChkinmConstants.REPORT_URL,ChkinmConstants.REPORT_URL);//判断跳转路径
 			modelMap.put("reportUrl", rs.get("reportUrl"));//ireport文件地址
 			return new ModelAndView(rs.get("url"),modelMap);
 		}
        
	}
	
	/**
	 * 转入直拨单添加页面    
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addzb")
	public ModelAndView addzb(ModelMap modelMap, HttpSession session, String action)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(!"init".equals(action)){
			String accountName=session.getAttribute("accountName").toString();
			Chkinm chkinm=new Chkinm();
			chkinm.setMadeby(accountName);
			chkinm.setMaded(new Date());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
			chkinm.setChkinno(misChkinmService.getMaxChkinno());
			modelMap.put("chkinm",chkinm);//日期等参数
			String acct=session.getAttribute("ChoiceAcct").toString();
			String accountId=session.getAttribute("accountId").toString();
			List<Positn> positnList = positnRoleService.findAllPositn(acct, accountId);
			//直拨单填制审核入库仓位只能选择主直拨库和基地仓库  此处做限制 wjf
			List<Positn> positnList1 = new ArrayList<Positn>();
			for(int i = 0;i<positnList.size();i++){
				if("1201".equals(positnList.get(i).getTyp()) || "1202".equals(positnList.get(i).getTyp())){
					positnList1.add(positnList.get(i));
				}
			}
			modelMap.put("positnList", positnList1);//带有权限控制的仓位
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
//			modelMap.put("deliverList", deliverService.findAllDelivers(new Deliver()));//供应商
			modelMap.put("curStatus", "add");//当前页面状态
		}
		return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINMZB,modelMap);	
	}
	
	/**
	 * 修改直拨单
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateChkinmzb")
	@ResponseBody
	public int updateChkinmzb(ModelMap modelMap,HttpSession session,Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
				"修改直拨单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Chkinm chkinms=misChkinmService.findChkinmByid(chkinm);
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		Integer  result=0;
		if(null!=chkinms){//存在则修改
			chkinm.setInout(ChkinmConstants.conk);
			result=misChkinmService.updateChkinm(chkinm);
		}else{//不存在则添加   
			chkinm.setInout(ChkinmConstants.conk);
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB, chkinm.getMaded()));//从新获得单号
			result=misChkinmService.saveChkinm(chkinm,"zb");
		}
		return result;	
	}
	
	/**
	 * 查询是否必须入到默认仓位
	 * @param modelMap
	 * @param chkinm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/chkPositn")
	@ResponseBody
	public String chkPositn(ModelMap modelMap, HttpSession session, SpCodeMod spCodeMod)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spCodeMod.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return misChkinmService.chkPositn(spCodeMod);
	}
	/**
	 * 检测要保存、删除的门店入库单据是否被审核
	 * @param modelMap
	 * @param session
	 * @param chkinm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value="/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session, String active, Chkinm chkinm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());//帐套
		return misChkinmService.chkChect(active,chkinm);
	}
	
	/**
	 * 冲消查询界面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmByCx")
	public ModelAndView addChkinmByCx(ModelMap modelMap, Page page, HttpSession session, Spbatch spbatch, String bdat, String edat) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (bdat == null) {
			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
		}else {
			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
		}
		if (edat == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else {
			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		}
		
		modelMap.put("spbatchList", misChkinmService.addChkinmByCx(spbatch));//获取入库数据
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmConstants.MIS_TABLE_CHKINM_CX,modelMap);
	}
	
	/**
	 * 直发物资冲消查询界面
	 * @author 2014.11.4wjf
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkinmzfByCx")
	public ModelAndView addChkinmzfByCx(ModelMap modelMap, Spbatch spbatch, String bdat, String edat,String action) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (bdat == null) {
			spbatch.setBdat(DateFormat.getDateBefore(new Date(),"day",-1,30));//一个月以内的数据
		}else {
			spbatch.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//一个月以内的数据
		}
		if (edat == null) {
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}else {
			spbatch.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		}
		if(!"init".equals(action)){
			modelMap.put("spbatchList", misChkinmService.addChkinmzfByCx(spbatch));//获取直发数据
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(ChkinmConstants.TABLE_CHKINMZF_CX,modelMap);
	}
	
	/**
	 * 供应商结算
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deliverSaList")
	public ModelAndView deliverSaList(ModelMap modelMap, Chkinm chkinm, Page page, Date madedEnd, String checkOrNot,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"供应商结算",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
        if (chkinm != null && chkinm.getMaded() == null) {
            chkinm.setMaded(new Date());
        }
        if (madedEnd == null) {
            madedEnd = new Date();
        }
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())) {

            List<Chkinm> listChkinm = misChkinmService.findAllChkinmNew(checkOrNot, chkinm, page, madedEnd);
            Chkinm totalChkinm = misChkinmService.findAllChkinmTotal(checkOrNot, chkinm, madedEnd);
            modelMap.put("listChkinm", listChkinm);
            modelMap.put("totalChkinm", totalChkinm);
        }
        modelMap.put("pageobj", page);
        modelMap.put("Chkinm", chkinm);
        modelMap.put("madedEnd", madedEnd);
        modelMap.put("checkOrNot", checkOrNot);
		return new ModelAndView(ChkinmConstants.DELIVERSA_LIST,modelMap);	
	}
	
	/**
	 * 查询付款记录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findPayData")
	public ModelAndView findPayData(ModelMap modelMap, Chkinm chkinm, String madedEnd,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())){
            List<Map<String,Object>> listFolio = misChkinmService.findPayData(chkinm, madedEnd);
            if(listFolio != null && !listFolio.isEmpty()){
                Map<String,Object> map = listFolio.get(listFolio.size()-1);
                listFolio.remove(listFolio.size()-1);
                modelMap.put("totalPay", map);
            }else{
                Map<String,Object> map = new HashMap<String,Object>();
                modelMap.put("totalPay", map);
            }
            modelMap.put("listFolio", listFolio);
        }
		return new ModelAndView(ChkinmConstants.FOLIO_LIST,modelMap);	
	}
	
	/**
	 * 查询发票记录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findBillData")
	public ModelAndView findBillData(ModelMap modelMap, Chkinm chkinm, String madedEnd)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(chkinm.getDeliver()!=null&&chkinm.getDeliver().getCode()!=null&&!"".equals(chkinm.getDeliver().getCode())) {
            List<Map<String, Object>> listBill = misChkinmService.findBillData(chkinm, madedEnd);
            if (listBill != null && !listBill.isEmpty()) {
                Map<String, Object> map = listBill.get(listBill.size() - 1);
                listBill.remove(listBill.size() - 1);
                modelMap.put("totalBill", map);
            } else {
                Map<String, Object> map = new HashMap<String, Object>();
                modelMap.put("totalBill", map);
            }
            modelMap.put("listBill", listBill);
        }
		return new ModelAndView(ChkinmConstants.BILL_LIST,modelMap);	
	}
	
	/**
	 * 审核结账
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkedBill")
	public ModelAndView checkedBill(ModelMap modelMap, String chkinno)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		misChkinmService.checkedChkinmBill(chkinno);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**
	 * 跳转到付款界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toPayMoney")
	public ModelAndView toPayMoney(ModelMap modelMap, String chkinno,String money)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("chkinno", chkinno);
		modelMap.put("money", money);
		return new ModelAndView(ChkinmConstants.PAY_MONEY,modelMap);
	}
	
	/**
	 * 付款
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/payMoney")
	public ModelAndView payMoney(ModelMap modelMap,HttpSession session, String chkinno, Date dat, String memo, double pay)throws Exception{
		List<Chkinm> listChkinm = misChkinmService.findChkinmByIds(chkinno);
		String madeby = session.getAttribute("accountName").toString();
		misChkinmService.payMoney(listChkinm,dat,memo,pay,madeby);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**
	 * 跳转到发票界面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddBill")
	public ModelAndView toAddBill(ModelMap modelMap, String deliverCode)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(ChkinmConstants.ADD_BILL,modelMap);	
	}
	
	/**
	 * 添加发票
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * String deliver.code, Date dat, String memo, double pay
	 */
	@RequestMapping(value="/addBill")
	public ModelAndView addBill(ModelMap modelMap, Chkinm chkinm, HttpSession session)throws Exception{
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		misChkinmService.addBill(chkinm);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	
	/**
	 * 跳转到添加期初金额
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddDeliverMoney")
	public ModelAndView toAddDeliverMoney(ModelMap modelMap, String deliverCode)throws Exception{
		modelMap.put("dat", new Date());
		modelMap.put("deliverCode", deliverCode);
		return new ModelAndView(ChkinmConstants.ADD_DELIVER_MONEY,modelMap);	
	}
	
	/**
	 * 添加供应商未结金额
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * String deliver.code, Date dat, String memo, double pay
	 */
	@RequestMapping(value="/addDeliverMoney")
	public ModelAndView addDeliverMoney(ModelMap modelMap, Chkinm chkinm, HttpSession session)throws Exception{
		chkinm.setMadeby(session.getAttribute("accountName").toString());
		misChkinmService.addDeliverMoney(chkinm);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);	
	}
	
	/**
	 * 跳转到导入页面   门店入库单导入 wjf
	 */
	@RequestMapping("/importChkinm")
	public ModelAndView importChkinm(ModelMap modelMap) throws Exception{
		return new ModelAndView(ChkinmConstants.IMPORT_CHKINM,modelMap);
	}
	
	/**
	 * 下载模板信息 门店入库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		misChkinmService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = misChkinmService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(ChkinmConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入门店入库单  wjf
	 */
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountId").toString();
		Object obj=misChkinmService.check(realFilePath, accountId);
		if(obj instanceof Chkinm){//导入对了
			String accountName=session.getAttribute("accountName").toString();
			((Chkinm) obj).setMadeby(accountName);//得到当前操作人
			((Chkinm) obj).setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
			((Chkinm) obj).setChkinno(misChkinmService.getMaxChkinno());
			modelMap.put("chkinm",(Chkinm) obj);
			String acct=session.getAttribute("ChoiceAcct").toString();
			modelMap.put("positnList", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId,"'1201','1202','1203'"));//只能入到这些类型  加工间，办公室都不能选 2015.1.3wjf
			Deliver deliver = new Deliver();
			deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			deliver.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("deliverList", deliverService.findAllDelivers(deliver));
			modelMap.put("curStatus", "add");//当前页面状态
			modelMap.put("importFlag","OK");
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM,modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(ChkinmConstants.MIS_UPDATE_CHKINM,modelMap);
		}
	}
}
