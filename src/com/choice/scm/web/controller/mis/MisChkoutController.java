package com.choice.scm.web.controller.mis;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.mis.MisChkoutConstants;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.MisChkoutService;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;

/**
 * 出库单
 * 
 * @author yp
 */

@Controller
@RequestMapping("/misChkout")
public class MisChkoutController {

	@Autowired
	private MisChkoutService misChkoutService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查找已审核出库单详细信息
	 * 
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("searchCheckedChkout")
	public ModelAndView searchCheckedChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查找已审核出库单详细信息",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String positnCode="";
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			positnCode = accountPositn.getPositn().getCode();
			positnList.add(accountPositn.getPositn());
			Positn positn = new Positn();
			positn.setCode(positnCode);
			chkoutm.setPositn(positn);
		}
		
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("CK");
		
		chkoutm = misChkoutService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				supplyAcct.setPositn(chkoutm.getPositn().getCode());
				supplyAcct.setSp_code(ckd.getSp_code().getSp_code());
				ckd.getSp_code().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}	
		modelMap.put("chkoutm", chkoutm);
		modelMap.put("listBill", codeDesService.findDocumentType(codeDes));
		// modelMap.put("positnIn", positnService.findPositnIn());
		// modelMap.put("positnOut", positnService.findPositnOut());
		modelMap.put("curStatus", "show");
		return new ModelAndView(MisChkoutConstants.SEARCH_CHECKED_CHKOUT, modelMap);
	}

	/**
	 * 通过id获取出库单详细信息
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @author yp
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("getChkoutById")
	public ModelAndView findChkoutById(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("chkoutm", misChkoutService.findChkoutmById(chkoutm));
		return new ModelAndView(MisChkoutConstants.UPDATE_CHKOUT, modelMap);
	}
	
	/**
	 * 模糊查询出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param bdat
	 * @param edat
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 * @author yp
	 */
	@RequestMapping("list")
	public ModelAndView findChkout(ModelMap modelMap, Chkoutm chkoutm,
			String action, String startDate, Date bdat, Date edat,
			HttpSession session, Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"模糊查询出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
		if (null != accountPositn && null != accountPositn.getPositn()) {
			//查询该分店的档口
			Positn positn = new Positn();
			positn.setUcode(accountPositn.getPositn().getCode());
			List<Positn> list = positnService.findPositnSuperNOPage(positn);
			List<String> listPositn = new ArrayList<String>();
			for(Positn posi:list){
				listPositn.add(posi.getCode());
			}
			listPositn.add(accountPositn.getPositn().getCode());//加上分店本身
			chkoutm.setListPositn(listPositn);
		}
		
		modelMap.put("pageobj", page);
		if (null != action && !"".equals(action)) {
			bdat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
			edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		}
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		modelMap.put("chkoutm", chkoutm);
		if (null != chkoutm.getChecby() && chkoutm.getChecby().equals("c")) {//审核的 查所有的
			modelMap.put("chkoutList", misChkoutService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),null));
			return new ModelAndView(MisChkoutConstants.CHECKED_CHKOUT, modelMap);
		} else {//未审核的 不查调拨到其他门店的
			modelMap.put("chkoutList", misChkoutService.findChkoutm(chkoutm, bdat, edat, page,session.getAttribute("locale").toString(),"ck"));
			return new ModelAndView(MisChkoutConstants.MANAGE_CHKOUT, modelMap);
		}
	}

	/**
	 * 获取单号，跳转添加页面
	 * 
	 * @param modelMap
	 * @return
	 * @author yp
	 * @throws CRUDException
	 */
	@RequestMapping("addChkout")
	public ModelAndView addChkout(ModelMap modelMap, HttpSession session,
			String action, String tableFrom,Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String typ = "13";
		CodeDes  codes = new CodeDes();		
		codes.setTyp(typ);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		String locale = session.getAttribute("locale").toString();
		codes.setLocale(locale);
		codes.setCodetyp("CK");
		codes.setTyp("18");
		modelMap.put("billType", codeDesService.findDocumentType(codes));
		if (action == null || !action.equals("init")) {
			chkoutm.setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			chkoutm.setMaded(new Date());
			chkoutm.setMadeby(session.getAttribute("accountName").toString());
			chkoutm.setChkoutno(misChkoutService.findNextNo().intValue());
			String positnCode="";
			// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			String accountId = session.getAttribute("accountId").toString();
			AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
			List<Positn> positnList = new ArrayList<Positn>();
			if (null != accountPositn && null != accountPositn.getPositn()) {
				positnCode = accountPositn.getPositn().getCode();
				positnList.add(accountPositn.getPositn());
				Positn positn = new Positn();
				positn.setCode(positnCode);
				chkoutm.setPositn(positn);
				//查询该分店的档口
				Positn p = new Positn();
				p.setUcode(accountPositn.getPositn().getCode());
				positnList.addAll(positnService.findPositnSuperNOPage(p));
			}
			//出库仓位
			modelMap.put("positnOut", positnList);
			//领用仓位   查询盘亏1999，前台js 过滤掉自己
			Positn positn1=new Positn();
			positn1.setTypn("'1206'");
			List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
			lyList.addAll(positnList);
			modelMap.put("positnIn", lyList);
			modelMap.put("chkoutm", chkoutm);
			modelMap.put("curStatus", "add");
		} else {
			modelMap.put("tableFrom", tableFrom);
		}
		return new ModelAndView(MisChkoutConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 更新,添加出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("saveOrUpdate")
	@ResponseBody
	public Object saveByAdd(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session, String curStatus, String scrapped,String scrappedname) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		String result = null;
		//加判断：如果是报废，则memo取scrapped 和scrappedname ；如果是其他则不取   wjf
		if("9915".equals(chkoutm.getTyp())){//出库报废
			String memo = scrapped+'-'+scrappedname;		
			chkoutm.setMemo(memo);
		}
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if (curStatus.equals("add")) {
			result = misChkoutService.saveChkoutm(chkoutm);
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
					"添加出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
		} else if (curStatus.equals("edit")) {
			chkoutm.setMadeby(accountService.findAccountById(
					session.getAttribute("accountId").toString()).getName());
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,
					"更新出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			result = misChkoutService.updateChkoutm(chkoutm);
		}
		return result;
	}

	/**
	 * 删除出库单
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("deleteChkout")
	@ResponseBody
	public Object deleteChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,
				"删除出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return misChkoutService.deleteChkoutm(chkoutm);
		// return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到更新页面
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("updateChkout")
	public ModelAndView updateChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = misChkoutService.findChkoutmById(chkoutm);
		if(chkoutm!=null &&chkoutm.getChkoutd()!=null && chkoutm.getChkoutd().size()>0){
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Chkoutd ckd:chkoutm.getChkoutd()){
				supplyAcct.setPositn(chkoutm.getPositn().getCode());
				supplyAcct.setSp_code(ckd.getSp_code().getSp_code());
				ckd.getSp_code().setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
			}
		}		
		modelMap.put("chkoutm",chkoutm );
		// 根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId = session.getAttribute("accountId").toString();
		AccountPositn accountPositn = accountPositnService
				.findAccountById(accountId);
		List<Positn> positnList = new ArrayList<Positn>();
		if (null != accountPositn && null != accountPositn.getPositn()) {
			positnList.add(accountPositn.getPositn());
			//查询该分店的档口
			Positn p = new Positn();
			p.setUcode(accountPositn.getPositn().getCode());
			positnList.addAll(positnService.findPositnSuperNOPage(p));
		}
		//出库仓位
		modelMap.put("positnOut", positnList);
		CodeDes codeDes = new CodeDes();
		String locale = session.getAttribute("locale").toString();
		codeDes.setLocale(locale);
		codeDes.setCodetyp("CK");
		codeDes.setTyp("18");
		modelMap.put("billType", codeDesService.findDocumentType(codeDes));
		
		//领用仓位   查询所有门店，及盘亏1999，前台js 过滤掉自己
		Positn positn1=new Positn();
		positn1.setTypn("'1206'");
		List<Positn> lyList = positnService.findPositnSuperNOPage(positn1);
		lyList.addAll(positnList);
		modelMap.put("positnIn", lyList);
		modelMap.put("curStatus", "show");
		//查询的时候也要查询报废原因  wjf
		String typ = "13";
		CodeDes  codes = new CodeDes();		
		codes.setTyp(typ);
		modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
		return new ModelAndView(MisChkoutConstants.UPDATE_CHKOUT, modelMap);
	}

	/**
	 * 审核出库单
	 * 
	 * @param chkoutm
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping("auditChkout")
	@ResponseBody
	public Object auditChkout(ModelMap modelMap, Chkoutm chkoutm,
			HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
				"审核出库单:单号（"+chkoutm.getChkoutno()+")",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		try {
//			return misChkoutService.updateByAudit(chkoutm, session.getAttribute("accountName").toString());
			String chk1memo = chkoutm.getChk1memo();//审核备注  wjf
//			chkoutm = misChkoutService.findChkoutmById(chkoutm);
			chkoutm.setChk1memo(chk1memo);
		    String chkectby=session.getAttribute("accountName").toString();
			chkoutm.setChecby(chkectby);
			return misChkoutService.updateByAudit(chkoutm,chkectby);  //修改洞庭反应的问题2014.11.21
		} catch (Exception e) {
			return e.getLocalizedMessage();
		}
	}

	/**
	 * 出库单打印
	 * 
	 * @param modelMap
	 * @param chkoutm
	 * @param type
	 * @param page
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author yp
	 */
	@RequestMapping(value = "/printChkout")
	public ModelAndView viewChkout(ModelMap modelMap, Chkoutm chkoutm,
			String type, Page page, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.PRINT,
				"出库单打印",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkoutm = misChkoutService.findChkoutmById(chkoutm);
		
		//wangjie 2014年11月5日 10:12:51 （伊天园，验收出库，显示的是售价，但是打印显示的是报价。不一致。）
//		for(Chkoutd chkd: chkoutm.getChkoutd()){
//			chkd.setTotalamt(chkd.getPriceamt());
			//chkd.setPrice(chkd.getPricesale());
//		}
		
		modelMap.put("List", chkoutm.getChkoutd());
		HashMap<Object, Object> parameters = new HashMap<Object, Object>();
		parameters.put("comName", "");
		parameters.put("firm", chkoutm.getFirm().getDes());
		parameters.put("positin", chkoutm.getPositn().getDes());
		CodeDes codeDes = new CodeDes();
		codeDes.setCode(chkoutm.getTyp());
		codeDes.setLocale(session.getAttribute("locale").toString());
		parameters.put("typ", codeDesService.findDocumentByCode(codeDes).getDes());
//		parameters.put("typ", chkoutm.getTyp());
		parameters.put("maded", chkoutm.getMaded());
		parameters.put("vouno", chkoutm.getVouno());
		parameters.put("madeby", chkoutm.getMadebyName());//打印审核人姓名wjf
		parameters.put("checby", chkoutm.getChecbyName());

		modelMap.put("parameters", parameters);
		modelMap.put("action",
				"/misChkout/printChkout.do?chkoutno=" + chkoutm.getChkoutno());// 传入回调路径
		Map<String, String> rs = ReadReportUrl.redReportUrl(type,
				MisChkoutConstants.REPORT_PRINT_URL,
				MisChkoutConstants.REPORT_EXP_URL);// 判断跳转路径
		modelMap.put("reportUrl", rs.get("reportUrl"));// ireport文件地址
		return new ModelAndView(rs.get("url"), modelMap);
	}

	/**
	 * 获取出库单单号
	 * 
	 * @param maded
	 * @return
	 * @throws ParseException
	 * @author yp
	 */
	@RequestMapping("getVouno")
	@ResponseBody
	public Object getVouno(String maded) throws ParseException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return calChkNum.getNext(CalChkNum.CHKOUT,
				DateFormat.getDateByString(maded, "yyyy-MM-dd"));
	}

	/**
	 * 操作成功页面跳转
	 * 
	 * @return
	 * @author yp
	 */
	@RequestMapping("success")
	public ModelAndView success() {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		return new ModelAndView(StringConstant.ACTION_DONE);
	}

	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * 
	 * @param modelMap
	 * @param session
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws Exception
	 * @author ZGL
	 */
	@RequestMapping(value = "/chkChect")
	@ResponseBody
	public String chkChect(ModelMap modelMap, HttpSession session,
			String active, Chkoutm chkoutm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		chkoutm.setAcct(session.getAttribute("ChoiceAcct").toString());// 帐套
		return misChkoutService.chkChect(active, chkoutm);
	}

	/**
	 * 冲消查询界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addChkoutByCx")
	public ModelAndView addChkoutByCx(ModelMap modelMap, Page page,
			HttpSession session, Spbatch spbatch) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		if("show".equals(spbatch.getSp_desc())){//使用规格字段判断是否需要查询数据
			modelMap.put("spbatchList", misChkoutService.findChkoutByCx(spbatch));// 获取出库冲销数据
		}else{
			spbatch.setBdat(DateFormat.getDateBefore(new Date(), "day", -1, 30));// 一个月以内的数据
			spbatch.setEdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		modelMap.put("spbatch", spbatch);
		return new ModelAndView(MisChkoutConstants.TABLE_CHKOUT_CX, modelMap);
	}
	
	/**
	 * 跳转到导入页面   出库单导入 wjf
	 */
	@RequestMapping("/importChkout")
	public ModelAndView importChkout(ModelMap modelMap) throws Exception{
		return new ModelAndView(MisChkoutConstants.IMPORT_CHKOUT,modelMap);
	}
	
	/**
	 * 下载模板信息 出库单模板下载 wjf
	 */
	@RequestMapping(value = "/downloadTemplate")
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		misChkoutService.downloadTemplate(response, request);
	}
	
	/**
	 * 先上传excel
	 */
	@RequestMapping(value = "/loadExcel", method = RequestMethod.POST)
	public ModelAndView loadExcel(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String realFilePath = misChkoutService.upload(request, response);
		modelMap.put("realFilePath", realFilePath);
		return new ModelAndView(MisChkoutConstants.IMPORT_RESULT, modelMap);
	}
	
	/**
	 * 导入出库单  wjf
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/importExcel")
	public ModelAndView importExcel(HttpSession session, ModelMap modelMap, @RequestParam String realFilePath)
			throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.IMPORT,
				"导入出库单",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountId = session.getAttribute("accountId").toString();
		Object obj=misChkoutService.check(realFilePath, accountId);
		if(obj instanceof Chkoutm){//导入对了
			String typ = "13";
			CodeDes  codes = new CodeDes();		
			codes.setTyp(typ);
			modelMap.put("codeDesList", codeDesService.findCodeDes(codes));//查询所有的报废原因(不分页)
			((Chkoutm) obj).setVouno(calChkNum.getNext(CalChkNum.CHKOUT, new Date()));
			((Chkoutm) obj).setMadeby(session.getAttribute("accountName").toString());
			((Chkoutm) obj).setChkoutno(misChkoutService.findNextNo().intValue());
			modelMap.put("positnIn", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("positnOut", positnService.findPositn(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"), accountId));
			modelMap.put("chkoutm", ((Chkoutm) obj));
			modelMap.put("curStatus", "add");
			modelMap.put("importFlag","OK");
			return new ModelAndView(MisChkoutConstants.UPDATE_CHKOUT, modelMap);
		}else{
			modelMap.put("importError", (List<String>)obj);
			return new ModelAndView(MisChkoutConstants.UPDATE_CHKOUT, modelMap);
		}
	}
	
}
