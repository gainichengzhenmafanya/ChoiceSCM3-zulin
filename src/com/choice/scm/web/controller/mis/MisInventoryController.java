package com.choice.scm.web.controller.mis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.InventoryConstants;
import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.InventoryDemomService;
import com.choice.scm.service.InventoryService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.MisInventoryService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * MIS盘点
 */
@Controller
@RequestMapping(value = "misInventory")
public class MisInventoryController {
	
	@Autowired
	private MisInventoryService misInventoryService;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private InventoryDemomService inventoryDemomService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private Page pager;
	@Autowired
	private LogsMapper logsMapper;
	private final static int PAGE_SIZE = 40;
	
	/**
	 * 仓库盘点主跳转
	 * @return pd=start  开始盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllInventory(ModelMap modelMap, HttpSession session, String action, Inventory inventory,
			String status,String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"查询盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		Positn positn = inventory.getPositn();
		Positn p = new Positn();//仓位属性
		if(null != action && "init".endsWith(action)){//初始
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			inventory.setDate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			inventory.setYearr(yearr);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if("type".equals(type)){//分店盘点才走这个
				String accountId = session.getAttribute("accountId").toString();
				AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
				if(null!=accountPositn && null != accountPositn.getPositn()){
					positn = new Positn();
					positn.setCode(accountPositn.getPositn().getCode());
					positn.setDes(accountPositn.getPositn().getDes());
					inventory.setPositn(positn);
				}
			}
			modelMap.put("action", "init");
		}else{//查询
			String acct = session.getAttribute("ChoiceAcct").toString();
			if(null != status && "start".endsWith(status)){//开始盘点
				p.setAcct(acct);
				p.setOldcode(inventory.getPositn().getCode());
				p.setPd("N");
				positnService.updatePositn(p);
				positn.setPd("N");
			}
			positn.setAcct(acct);
			positn.setCode(inventory.getPositn().getCode());
			p = positnService.findPositnByCode(positn);
			positn.setYnUseDept(p.getYnUseDept());//是否使用多档口
			positn.setPd(p.getPd());//  查询是否盘点	
			
			inventory.setAcct(acct);
			inventory.setStartNum(0);
			inventory.setEndNum(PAGE_SIZE);
			modelMap.put("inventoryList", misInventoryService.findAllInventoryPage(inventory));		//查询盘点列表
			int totalCount = misInventoryService.findAllInventoryCount(inventory);
			modelMap.put("totalCount", totalCount);
			
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
			}
			modelMap.put("currPage", 1);
			modelMap.put("pageSize", PAGE_SIZE);
			modelMap.put("disJson", JSONObject.fromObject(inventory));
		}
//		if(null!=type && ("type".endsWith(type)||"dept".endsWith(type))){//分店标志
//			modelMap.put("type",type);//分店标志 
//			//档口盘点特有
//			Positn deptpositn= new Positn();
//			if("dept".endsWith(type) && null!=inventory.getPositn()){
//				deptpositn=inventory.getPositn();
//			}
			//档口盘点特有
//			if("dept".endsWith(type) && null!=deptpositn){
//				modelMap.put("firmcode", inventory.getPositn().getCode());
//				modelMap.put("firmname", inventory.getPositn().getDes());
//				inventory.setPositn(deptpositn);
//			}
			
//		}
		
		modelMap.put("inventory", inventory);
		modelMap.put("type", type);
		if (type != null && "dept".equals(type)) {
			return new ModelAndView(InventoryConstants.LIST_INVENTORY_DEPT, modelMap);
		} 
		return new ModelAndView(InventoryConstants.LIST_INVENTORY_MIS, modelMap);
	}
	
	/**
	 * 仓库盘点Ajax加载
	 * @throws Exception
	 */
	@RequestMapping(value = "/listAjax")
	@ResponseBody
	public Object listAjax(HttpSession session,Inventory inventory,String currPage,String totalCount) throws Exception {
		int intCurrPage = Integer.valueOf(currPage);
		int intTotalCount = Integer.valueOf(totalCount);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventory.setStartNum(intCurrPage*PAGE_SIZE);
		inventory.setEndNum((intCurrPage+1)*PAGE_SIZE);
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		modelMap.put("inventoryList", misInventoryService.findAllInventoryPage(inventory));
		modelMap.put("currState", (intCurrPage+1)*PAGE_SIZE*1.0/intTotalCount);
		modelMap.put("currPage", currPage);
		if((intCurrPage+1)*PAGE_SIZE>=intTotalCount){
			modelMap.put("currState", 1);
			modelMap.put("over", "over");
		}
		return JSONObject.fromObject(modelMap).toString();
	}
	
	/**
	 * 盘点模板调用
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addInvendemo")
	public ModelAndView addInvendemo(ModelMap modelMap, HttpSession session, Page page, InventoryDemom inventorydemom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inventorydemom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("pageobj", page);
		List<InventoryDemom> listInventoryDemom = inventoryDemomService.listInvenDemomByFirm(inventorydemom, page);
		modelMap.put("listInventoryDemom", listInventoryDemom);
		modelMap.put("inventory.positn.code", inventorydemom.getFirm());
		return new ModelAndView(InventoryConstants.ADD_INVENTORY_DEMO, modelMap);
	}
	
	/**
	 * 保存盘点
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateInventory")
	@ResponseBody
	public String updateInventory(HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,
				"保存盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String acct=session.getAttribute("ChoiceAcct").toString();
		String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
		inventory.setYearr(yearr);
		misInventoryService.updateInventory(inventory,acct);
		return "success";
	}
	
	/**
	 * 结束盘点(分店仓位)
	 * @throws Exception
	 */
	@RequestMapping(value = "/endInventory")
	@ResponseBody
	public String endInventory(HttpSession session, Inventory inventory) throws Exception{
		try{
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.CHECK,
					"结束盘点:",session.getAttribute("ip").toString(),ProgramConstants.SCM);
			logsMapper.addLogs(logd);
			String acct=session.getAttribute("ChoiceAcct").toString();
			
			//1.先判断有没有结束盘点
			Positn p = new Positn();
			p.setAcct(acct);
			p.setCode(inventory.getPositn().getCode());
			Positn positn = positnService.findPositnByCode(p);
			if("Y".equals(positn.getPd())){//已经结束
				return null;
			}
			//2.先更新仓位盘点状态
			p.setPd("Y");
			p.setCode(null);
			p.setOldcode(inventory.getPositn().getCode());
			positnService.updatePositn(p);//更新pd状态
			
			String accountName = session.getAttribute("accountName").toString();
			String yearr = mainInfoService.findYearrList().get(0);//会计年wjf
			inventory.setAcct(acct);
			inventory.setYearr(yearr);
	//		if ("Y".equals(inventory.getYndaypan())) {
	//			inventoryService.saveInventory(inventory);
	//		}
			List<Inventory> listInventory=misInventoryService.findAllInventory(inventory);
			//保存positndaypan
			misInventoryService.savePositndaypan(inventory,listInventory);
			//保存盘点表 2014.12.9wjf
			inventoryService.savePositnPan(accountName,inventory,listInventory);
			//判断门店是否启用档口，如果启用的话 采用总部的盘点方式 否则采用倒挤盘点 
			String ynUseDept = inventory.getPositn().getYnUseDept();
			String result="";
			if(ynUseDept!=null && "Y".equals(ynUseDept)){//启用档口
				result= misInventoryService.endDeptInventory(accountName,inventory,listInventory);//生成盘亏出库，盘盈入库
			}else{//禁用档口
				result= misInventoryService.endInventory(accountName,inventory,listInventory);//生成盘亏出库
			}
			return result;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询未盘点仓位
	 * @throws Exception
	 */
	@RequestMapping(value = "/noInventoryPostin")
	public ModelAndView noInventoryPostin(ModelMap modelMap, HttpSession session, Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Positn positn=new Positn();
		positn.setPd("N");
		modelMap.put("ListPositn", positnService.findAllPositn(positn));
		return new ModelAndView(InventoryConstants.LIST_NO_INVENTORY_POSITN,modelMap);
	}
	
	/*****************存货盘点表 （日周月）start********************/
	/**
	 * 跳转存货盘点表分析
	 */
	@RequestMapping("/toStockList")
	public ModelAndView toStock(ModelMap modelMap,String checkMis){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("type", checkMis);
		modelMap.put("reportName", InventoryConstants.STOCK_TABLE);
		return new ModelAndView(InventoryConstants.LIST_STOCK,modelMap);
	}
	
	/**
	 * 判断该分店是否有该物资
	 */
	@RequestMapping("/checkSupply")
	@ResponseBody
	public Object checkSupply(HttpSession session,String sp_code) throws CRUDException{
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		SupplyAcct supplyAcct = new SupplyAcct();
		supplyAcct.setPositn(accountPositn.getPositn().getCode());
		supplyAcct.setSp_code(sp_code);
		boolean flag = inventoryService.checkSupply(supplyAcct);
		return flag;
	}
	
	/**
	 * 查询存货盘点表分析分析
	 */
	@RequestMapping("/findStock")
	@ResponseBody
	public Object findStock(ModelMap modelMap,HttpSession session,String page,String rows,SupplyAcct supplyAcct,String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		return inventoryService.findStock(supplyAcct, pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportStock")
	@ResponseBody
	public void exportStock(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "存货盘点表分析分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(InventoryConstants.STOCK_TABLE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		exportExcelMap.creatWorkBook(response.getOutputStream(), inventoryService.findStock(supplyAcct,pager).getRows(), "存货盘点表分析分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, InventoryConstants.STOCK_COLUMN));	
	}
	
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printStock")
	public ModelAndView printStock(ModelMap modelMap, Page pager, HttpSession session, String type,
			String page, String rows, SupplyAcct supplyAcct,String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("sp_code", supplyAcct.getSp_code());
		parameters.put("madeby", session.getAttribute("accountName").toString());
		modelMap.put("List", inventoryService.findStock(supplyAcct, pager).getRows());
	    parameters.put("report_name", "存货盘点表分析");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/inventory/printStock.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_STOCK, InventoryConstants.REPORT_PRINT_URL_STOCK);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}	
	
	
	/*****************存货盘点表 （日周月）end********************/
	/*****************月盘点差异报告start********************/
	/**
	 * 跳转到月盘点差异报告页面
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/listMonthPd")
	public ModelAndView listMonthPd(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", InventoryConstants.BASICINFO_REPORT_MONTHPd);
		return new ModelAndView(InventoryConstants.REPORT_SHOW_MONTHPd);
	}
	
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMonthPdHeaders")
	@ResponseBody
	public Object getInventoryAgingSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		
		dictColumns.setTableName(InventoryConstants.TABLE_NAME_MONTHPd);
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, InventoryConstants.BASICINFO_REPORT_MONTHPd));

		columns.put("frozenColumns", InventoryConstants.BASICINFO_REPORT_MONTHPd_FROZEN);
		return columns;
	}
	
	/**
	 * 查询表格信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findMonthPdSum")
	@ResponseBody
	public Object findInventoryAgingSum(ModelMap modelMap, HttpSession session, String page, String rows,
			String sort, String order, SupplyAcct supplyAcct, String monthFrom, String monthTo) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		//仓位
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return inventoryService.findMonthPdSum(condition, pager);
	}
	
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportMonthPd")
	@ResponseBody
	public void exportMonthPd(HttpServletResponse response,String sort,String order,HttpServletRequest request,String bysale,HttpSession session,SupplyAcct supplyAcct,String monthFrom,String monthTo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月盘点差异报告";
		Map<String,Object> condition = new HashMap<String,Object>();
		//仓位
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(accountPositn.getPositn().getCode());
			}
		supplyAcct.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("bysale", bysale);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(InventoryConstants.TABLE_NAME_MONTHPd);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), inventoryService.findMonthPdSum(condition, pager).getRows(), "月盘点差异报告", dictColumnsService.listDictColumnsByTable(dictColumns));		
	}
	
	/**
	 * 打印报表
	 */
	@RequestMapping(value = "/printMonthPd")
	public ModelAndView printMonthPd(ModelMap modelMap,Page pager,HttpSession session,String type,String sort,String order,SupplyAcct supplyAcct,String monthFrom,String monthTo)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			supplyAcct.setPositn(accountPositn.getPositn().getCode());
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();//回调参数
		params.put("positn",supplyAcct.getPositn());//回调参数
		params.put("sp_code",supplyAcct.getSp_code());//回调参数
		params.put("typ",supplyAcct.getTyp());//回调参数
		params.put("sort",sort);//回调参数
		params.put("order",order);//回调参数
		params.put("monthFrom", monthFrom);
		params.put("monthTo", monthTo);
		modelMap.put("actionMap", params);//回调参数
		
		Map<String,Object> condition = new HashMap<String,Object>();//报表list
		condition.put("supplyAcct", supplyAcct);//报表list
		condition.put("sort", sort);//报表list
		condition.put("order", order);//报表list
		condition.put("monthFrom", monthFrom);
		condition.put("monthTo", monthTo);
	    modelMap.put("List",inventoryService.findMonthPdSum(condition, pager).getRows());//报表list
	    HashMap<Object,Object>  parameters = new HashMap<Object,Object>();//报表map    
	    parameters.put("report_name", "月盘点差异报告");//报表map 
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("monthFrom", monthFrom);
	    parameters.put("monthTo", monthTo);  
	    modelMap.put("parameters", parameters);//报表map   
	 	modelMap.put("action", "/inventory/printMonthPd.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_MONTHPD,InventoryConstants.REPORT_PRINT_URL_MONTHPD);//判断跳转路径
	    modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/*****************月盘点差异报告  end********************/
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportInventory")
	@ResponseBody
	public boolean exportInventory(HttpServletResponse response,HttpServletRequest request,HttpSession session,Inventory inventory) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		List<Inventory> inventorys = misInventoryService.findAllInventory(inventory);
		String fileName = "仓库盘点表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return inventoryService.exportInventory(response.getOutputStream(), inventorys,inventory.getPositn().getDes());
	}
	
	/***
	 * 打印盘点表
	 * @author wjf
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printInventory")
	public ModelAndView printInventory(ModelMap modelMap, Page pager, HttpSession session,String type, Inventory inventory)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inventory.setAcct(acct);
		inventory.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		List<Inventory> inventorys = misInventoryService.findAllInventory(inventory);
		for(Inventory inventory2:inventorys){
			Supply supply = inventory2.getSupply();
			//物资规格sp_desc为null时 赋值为"" 打印时用 wangjie 2015.01.23
			if(supply.getSp_desc()==null)
				supply.setSp_desc("");
			inventory2.setSupply(supply);
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,String> params = new HashMap<String,String>();
		params.put("positn.code",inventory.getPositn().getCode());
		params.put("positn.des",inventory.getPositn().getDes());
		params.put("date",DateFormat.getStringByDate(inventory.getDate(), "yyyy-MM-dd"));
		params.put("supply.sp_code",inventory.getSupply().getSp_code());
		params.put("supply.sp_type",inventory.getSupply().getSp_type());
		modelMap.put("actionMap", params);
		
		modelMap.put("List",inventorys);
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "仓库盘点表");
	    parameters.put("positn.des",inventory.getPositn().getDes());
	    parameters.put("date", DateFormat.formatDate(inventory.getDate(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/misInventory/printInventory.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,InventoryConstants.REPORT_PRINT_URL_MIS_INVENTORY,InventoryConstants.REPORT_PRINT_URL_MIS_INVENTORY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url"),modelMap);
	}	
}
