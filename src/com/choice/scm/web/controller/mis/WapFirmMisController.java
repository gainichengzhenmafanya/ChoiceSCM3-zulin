package com.choice.scm.web.controller.mis;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.Account;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.scm.constants.WapFirmMisConstants;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.mis.WapFirmMisService;
import com.choice.scm.util.CalChkNum;

@Controller
@RequestMapping("wapFirmMis")
public class WapFirmMisController {
	@Autowired
	private WapFirmMisService wapFirmMisService;
	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	private CalChkNum calChkNum;
	
	/**
	 * 账号登录
	 */
	@RequestMapping(value = "/waploginIn")
	public ModelAndView loginIn(HttpSession session,ModelMap modelMap,Account account,Chkstodemo chkstodemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String info=wapFirmMisService.validateLogin(session,account);//登陆 并保持session
		String firmName=(String)session.getAttribute("firmName");
		if((null != firmName) && (!"".equals(firmName))){			//判断是否登陆成功
			chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
			modelMap.put("firmName", firmName);
			modelMap.put("listChkstodemo", wapFirmMisService.findChkstodemo(chkstodemo));
			return new ModelAndView(WapFirmMisConstants.LIST_CHKSTODEMO,modelMap);
		}
		modelMap.put("result", info.equals("T")?"管理员不能使用此功能！":info);
		return new ModelAndView(WapFirmMisConstants.WAPFIRMMIS_LOGIN,modelMap);
	}
	/**
	 * 登录跳转
	 */
	@RequestMapping(value = "/waplogin")
	public ModelAndView login() throws Exception{
		return new ModelAndView(WapFirmMisConstants.WAPFIRMMIS_LOGIN);
	}
	
	/**
	 * 查询申购模板 手机网页
	 */
	@RequestMapping(value = "/findChkstodemo")
	public ModelAndView findChkstodemo(HttpSession session,ModelMap modelMap,Chkstom chkstom,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String firmName=(String)session.getAttribute("firmName").toString();
		if((null != firmName) && (!"".equals(firmName))){			//判断是否登陆
			chkstodemo.setAcct(session.getAttribute("ChoiceAcct").toString());
			chkstodemo.setTitle("期初");
			modelMap.put("listChkstodemo", wapFirmMisService.findChkstodemo(chkstodemo));//获取模板
			modelMap.put("firmName", firmName);
			return new ModelAndView(WapFirmMisConstants.LIST_CHKSTODEMO,modelMap);
		}
		modelMap.put("result", "请重新登陆！");
		return new ModelAndView(WapFirmMisConstants.WAPFIRMMIS_LOGIN,modelMap);
	}

	/**
	 * 查询所有 分店
	 */
	@RequestMapping(value = "/findPositn")
	public ModelAndView findPositn(HttpSession session,ModelMap modelMap) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String firmName=(String)session.getAttribute("UsersCode");
		if((null != firmName) && (!"".equals(firmName))&&(firmName.equals("002"))){	//判断是不是管理员
			modelMap.put("firmList", wapFirmMisService.AppfindPositn());
			return new ModelAndView(WapFirmMisConstants.SET_FIRM,modelMap);
		}
		modelMap.put("result", "非管理员不具备分店选择功能！");
		return new ModelAndView(WapFirmMisConstants.WAPFIRMMIS_LOGIN,modelMap);
	}
	
	/**
	 * 查询当天 提交的申购单
	 */
	@RequestMapping(value = "/findTodayChkstodemo")
	public ModelAndView findTodayChkstodemo(HttpSession session,ModelMap modelMap,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkstom.setbMaded(DateFormat.getDateBefore(new Date(), "day", -1, 1));
		String firmName=(String)session.getAttribute("firmName");
		chkstom.setFirm((firmName != null||firmName!="")?firmName:null);
		modelMap.put("firmName", firmName);
		modelMap.put("listChkstom", wapFirmMisService.findChkstomToday(chkstom));
		return new ModelAndView(WapFirmMisConstants.LIST_CHKSTODEMO_TODAY,modelMap);
	}
	
	/**
	 * 申购模板批量添加报货单
	 */
	@RequestMapping(value = "/saveChkstodemo")
	@ResponseBody
	public String saveChkstodemo(HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=(String)session.getAttribute("accountId");
		if((null != accountId) && (!"".equals(accountId))){			//判断是否登陆
			try{
				chkstom.setMadeby(session.getAttribute("accountName").toString());
				chkstom.setFirm(session.getAttribute("firmName").toString());
				chkstom.setMaded(new Date());
				chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
				chkstom.setChkstoNo(chkstomService.getMaxChkstono());
				chkstom.setVouno(calChkNum.getNext(CalChkNum.CHKSTO,new Date()));
				}catch (Exception e) {
					e.printStackTrace();
				}
			if(chkstom.getChkstodList()!=null){
				return wapFirmMisService.saveChkstodemo(chkstom);
			}else{
				return "数量不能为空 !";
			}
		}else{
			return "请重新登陆 !";
		}
	}
	
	//========================================================================
	/**
	 * 手机应用 登录
	 */
	@RequestMapping(value = "/ApploginIn")
	@ResponseBody
	public HashMap<String,String> ApploginIn(HttpSession session,ModelMap modelMap,Account account,Chkstodemo chkstodemo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return wapFirmMisService.AppvalidateLogin(account);
	}
	/**
	 * 手机应用  查询申购单模板
	 */
	@RequestMapping(value = "/AppfindChkstodemo")
	@ResponseBody
	public List<Chkstodemo> AppfindChkstodemo(HttpSession session,Chkstodemo chkstodemo) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != chkstodemo.getAcct()) && (chkstodemo.getAcct()!="")){	
			chkstodemo.setTitle("期初");
			return wapFirmMisService.findChkstodemo(chkstodemo);
		}
		return null;
	}
	/**
	 * 查询所有 分店
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/AppfindPositn")
	@ResponseBody
	public List<Positn> AppfindPositn(Positn positn) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != positn.getAcct()) && (positn.getAcct()!="")){	
			return wapFirmMisService.AppfindPositn();
		}
		return null;
	}
	
	/**
	 * 手机应用 申购模板批量添加报货单
	 */
	@RequestMapping(value = "/AppsaveChkstodemo")
	@ResponseBody
	public String AppsaveChkstodemo(HttpSession session,Chkstom chkstom,Account account) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != account.getId()) && (!"".equals(account.getId()))){			//判断是否登陆
			try{
				String accountName=account.getName();
				chkstom.setMadeby(accountName);
				chkstom.setFirm(account.getPositn().getCode());
				chkstom.setMaded(new Date());
				chkstom.setChkstoNo(chkstomService.getMaxChkstono());
				chkstom.setVouno(calChkNum.getNext(CalChkNum.CHKSTO,new Date()));
			}catch (Exception e) {
				e.printStackTrace();
			}
			return wapFirmMisService.saveChkstodemo(chkstom);
		}else{
			return "请重新登陆！";
		}
	}
	/**
	 * 手机应用       查询当天提交的申购单
	 */
	@RequestMapping(value = "/AppFindTodayChkstodemo")
	@ResponseBody
	public List<Chkstom> AppFindTodayChkstodemo(HttpSession session,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if((null != chkstom.getAcct()) && (chkstom.getAcct()!="")){	
			chkstom.setbMaded(DateFormat.getDateBefore(new Date(), "day", -1, 1));
			return wapFirmMisService.findChkstomToday(chkstom);
		}
		return null;
	}

}
