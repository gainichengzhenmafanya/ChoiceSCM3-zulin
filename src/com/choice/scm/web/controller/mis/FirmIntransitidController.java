package com.choice.scm.web.controller.mis;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmIntransitdConstants;
import com.choice.scm.domain.FirmIntransitd;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.mis.FirmIntransitidService;

/**
 * 在途清单
 * 
 * @author ygb
 */

@Controller
@RequestMapping("/intransit")
public class FirmIntransitidController {

	@Autowired
	private FirmIntransitidService firmIntransitidService;

	/**
	 * 查询在途清单
	 * 
	 * @return
	 * @throws Exception
	 * @author ygb
	 */
	@RequestMapping("list")
	public ModelAndView searchFirmIntransitd(ModelMap modelMap, FirmIntransitd firmIntransitd,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
		if(null==firmIntransitd.getBdate()){
			firmIntransitd.setBdate(sft.format(new Date()));
		}
		if(null==firmIntransitd.getEdate()){
			firmIntransitd.setEdate(sft.format(new Date()));
		}
		modelMap.put("firmIntransitdList", firmIntransitidService.findFirmIntransitd(firmIntransitd,page));
		modelMap.put("firmIntransitd", firmIntransitd);
		modelMap.put("pageobj", page);
		return new ModelAndView(FirmIntransitdConstants.LIST_FIRMINTRANSITD, modelMap);
	}

	/**
	 * 导出配送中心到货审核excel表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/exportIntransit")
	@ResponseBody
	public void exportDispatch(HttpServletResponse response, FirmIntransitd firmIntransitd, String sort,
			String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct, String check) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源		
		List<FirmIntransitd> disList=firmIntransitidService.findFirmIntransitd(firmIntransitd);
		String fileName = "在途清单";
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("firmIntransitd", firmIntransitd);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		firmIntransitidService.exportExcel(response.getOutputStream(), disList);
		
	}
}
