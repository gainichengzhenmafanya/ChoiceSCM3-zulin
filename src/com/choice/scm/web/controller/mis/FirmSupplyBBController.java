package com.choice.scm.web.controller.mis;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FirmSupplyConstants;
import com.choice.scm.domain.FirmSupply;
import com.choice.scm.service.mis.FirmSupplyBBService;

/**
 * 物料盘存表
 * 
 * @author ygb
 */

@Controller
@RequestMapping("/bohfirmsupply")
public class FirmSupplyBBController {

	@Autowired
	private FirmSupplyBBService firmSupplyService;

	/**
	 * 查询物料盘存表
	 * 
	 * @return
	 * @throws Exception
	 * @author ygb
	 */
	@RequestMapping("list")
	public ModelAndView searchFirmIntransitd(ModelMap modelMap, FirmSupply firmSupply,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		FirmSupply firmSupply1=firmSupply;
		
		modelMap.put("firmSupplyList", firmSupplyService.findFirmSupply(firmSupply, page));
//		System.out.println("========================"+firmSupply.getFirm()+":"+firmSupply.getFirmDes());
		modelMap.put("firmSupply1", firmSupply1);
//		System.out.println("========================"+firmSupply.getFirm()+":"+firmSupply.getFirmDes());
		modelMap.put("pageobj", page);
		return new ModelAndView(FirmSupplyConstants.LIST_FIRMSUPPLY, modelMap);
	}
	
	/**
	 * 物料盘存表Excel
	 * @throws Exception
	 * @author  hhq
	 * 
	 */
	@RequestMapping("exportFirmSupply")
	@ResponseBody
	public boolean  exportFirmSupply(HttpServletResponse response,HttpServletRequest request,FirmSupply firmSupply,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);// 选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "物料盘存表";
		List<FirmSupply> exportList = firmSupplyService.findFirmSupply(firmSupply,page);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		return firmSupplyService.exportExcel(response.getOutputStream(), exportList);
	}
}
