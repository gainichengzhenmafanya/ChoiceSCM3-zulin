package com.choice.scm.web.controller.mis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.TeShuCaoZuoConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.mis.TeShuCaoZuoMisService;

@Controller
@RequestMapping("teShuCaoZuoMis")
public class TeShuCaoZuoMisController {

	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private TeShuCaoZuoMisService teShuCaoZuoMisService;
	@Autowired
	PositnService positnService;
	
	/**************************************************仓库期初start*********************************************/
	@RequestMapping(value = "/ckInit")
	public ModelAndView ckInit(ModelMap modelMap, HttpSession session, Page page, String positnCd, String positnNm, String typCode, 
			String status, String typDes, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId=session.getAttribute("accountId").toString();
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		AccountPositn accountPositn = accountPositnService.findAccountById(accountId);
//		String positnCode = accountPositn.getPositn().getCode();
//		String positnDes = accountPositn.getPositn().getDes();
		if(null!=action && ""!=action){
		}else{
			PositnSupply positnSupply =  new PositnSupply();
			positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
			positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			positnSupply.setPositn(positnCd);
			if (typCode!=null && typCode!="") {
				positnSupply.setSp_type(typCode);
			}
			modelMap.put("positnSupplyList", teShuCaoZuoMisService.getpositnSupplyList(positnSupply));
//			modelMap.put("positnCd", positnCd);
//			modelMap.put("positnNm", positnNm);
			modelMap.put("typCode", typCode);
			modelMap.put("typDes", typDes);
			Positn positn = new Positn();
			positn.setAcct(session.getAttribute("ChoiceAcct").toString());
			positn.setCode(positnCd);
			if ("Y".equals(teShuCaoZuoMisService.getQC(positn).getQc())) {
				modelMap.put("status", "init");
			}else {
				modelMap.put("status", status);
			}
		}
		modelMap.put("positn", positnCd);
		modelMap.put("positn_name", positnNm);
		return new ModelAndView(TeShuCaoZuoConstants.LIST_CANGKU_QICHU_MIS,modelMap);
	}
	
	/**
	 * 保存仓库期初
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCkInit")
	@ResponseBody
	public String updateCkInit(ModelMap modelMap, HttpSession session,  PositnSupply positnSupply) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		String yearr = DateFormat.getStringByDate(new Date(),"yyyy");
		teShuCaoZuoMisService.updateCkInit(positnSupply, acct, yearr);
		return "success";
	}
	
	/**
	 * 仓库期初-确认初始
	 * @throws Exception
	 */
	@RequestMapping(value = "/initation")
	@ResponseBody
	public void initation(ModelMap modelMap, HttpSession session, String positn) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		PositnSupply positnSupply = new PositnSupply();
		positnSupply.setAcct(acct);
		positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		positnSupply.setPositn(positn);
		teShuCaoZuoMisService.initation(teShuCaoZuoMisService.getpositnSupplyList(positnSupply), positn, acct);
	}
	/**
	 * 导出
	 */
	@RequestMapping("/exportCangkuInit")
	@ResponseBody
	public boolean exportCangkuInit(HttpServletResponse response,HttpServletRequest request,HttpSession session, String positnCd, String positnNm, String typCode, 
			String status, String typDes, String action) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PositnSupply positnSupply =  new PositnSupply();
		positnSupply.setAcct(session.getAttribute("ChoiceAcct").toString());
		positnSupply.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		positnSupply.setPositn(positnCd);
		if (typCode!=null && typCode!="") {
			positnSupply.setSp_type(typCode);
		}
		List<PositnSupply> positnSupplyList= teShuCaoZuoMisService.getpositnSupplyList(positnSupply);
		String fileName = "仓库期初表";
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //fire_fox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		return teShuCaoZuoMisService.exportCangkuInit(response.getOutputStream(), positnSupplyList,positnCd);
	}
	/**************************************************仓库期初end***********************************************/	
	
}
