package com.choice.scm.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.MessageConstants;
import com.choice.scm.domain.Message;
import com.choice.scm.service.MessageService;

@Controller
@RequestMapping("message")
public class MessageController {

	@Autowired
	MessageService messageService;
	
	/**
	 * 查询邮件信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findMessage(ModelMap modelMap,HttpSession session,String title,String content,String senderName,Date startTime,Date endTime,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountNames").toString();
		List<Message> messageList= messageService.findAllMessage(accountId, title, content, senderName, startTime, endTime, page);
		modelMap.put("pageobj", page);
		modelMap.put("messageList",messageList);
		
		modelMap.put("title", title);
		modelMap.put("content", content);
		modelMap.put("senderName", senderName);
		modelMap.put("startTime", DateFormat.formatDate(startTime, "yyyy-MM-dd"));
		modelMap.put("endTime", DateFormat.formatDate(endTime, "yyyy-MM-dd"));
		return new ModelAndView(MessageConstants.LIST_EMAIL,modelMap);
	}
	
	/**
	 * 打开新增邮件页面
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap,String id) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=id && !"".equals(id)){
			Message message =  messageService.findMessageById(id);
			modelMap.put("receiver",message.getSender());
			modelMap.put("receiverName", message.getSenderName());
			modelMap.put("title", "回复："+message.getTitle());
		}
		return new ModelAndView(MessageConstants.ADD_EMAIL,modelMap);
	}
	
	/**
	 * 新增邮件
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,HttpSession session,Message message) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountNames").toString();
		message.setSender(accountId);
		message.setSendTime(DateFormat.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		String receiverString = message.getReceiverName();
		if(receiverString.split(",").length>1){
			for(String receiver:receiverString.split(",")){
				message.setId(CodeHelper.createUUID());
				message.setReceiverName(receiver);
				messageService.sendMessage(message);
			}
		}else{
			message.setId(CodeHelper.createUUID());
			messageService.sendMessage(message);
		}
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 查看邮件
	 */
	@RequestMapping(value = "/show")
	public ModelAndView showMessage(ModelMap modelMap,String id,String state) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("message", messageService.findMessageById(id));
		if(state.equals("0")){
			messageService.updateMessage(id);
		}
		return new ModelAndView(MessageConstants.SEE_EMAIL,modelMap);
	}
	
	/**
	 * 未读邮件数
	 */
	@RequestMapping(value = "/unRead")
	@ResponseBody
	public Object getUnReadMessageCounts(ModelMap modelMap,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountName").toString();
		return messageService.findUnReadMessage(accountId);
	}
	
	/**
	 * 删除公告
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView DeleteMessage(ModelMap modelMap,String ids) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		messageService.deleteMessage(ids);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 查询账户
	 */
	@RequestMapping(value = "/findAccount")
	public ModelAndView findAccount(ModelMap modelMap,String searchInfo) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Message> messageList = messageService.findAccount(searchInfo);
		modelMap.put("messageList", messageList);
		modelMap.put("searchInfo",searchInfo);
		return new ModelAndView(MessageConstants.ACCOUNT,modelMap);
	}
}
