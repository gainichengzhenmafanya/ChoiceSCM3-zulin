package com.choice.scm.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyMngConstants;
import com.choice.scm.domain.CntUse;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
import com.choice.scm.service.FirmSupplyService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SupplyMngService;
import com.choice.scm.service.SupplyService;

/**
* 物资管理
* @author -  css
*/

@Controller
@RequestMapping(value = "supplyMng")
public class SupplyMngController {

	@Autowired
	private SupplyMngService supplyMngService;
	@Autowired
	private GrpTypService grpTypService;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private PositnService positnService;
	@Autowired
	FirmSupplyService firmSupplyService;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 * 物资编码列表
	 * @param modelMap
	 * @param supply
	 * @param grpTyp
	 * @param grp
	 * @param typ
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllSupply(ModelMap modelMap, Supply supply, GrpTyp grpTyp, Grp grp, Typ typ, String type, HttpSession session)
			throws Exception {
		//左侧导航树
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("type", type);//安全库存设置
		return new ModelAndView(SupplyMngConstants.LIST_SUPPLYMNG, modelMap);
	}
	
	/**
	 * 根据左侧的树   显示 右侧的物资编码
	 * @param modelMap
	 * @param supply
	 * @param level
	 * @param spcodeList
	 * @param code
	 * @param page
	 * @param session
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllSupplyByLeftGrpTyp(ModelMap modelMap, Supply supply, String level, String spcodeList,
			String code, Page page, HttpSession session, String type, String update, String bdate, String edate ) throws Exception {
		//帐套
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("update", update);
		//查询记忆
		modelMap.put("supply", supply);	
		modelMap.put("type", type);//安全库存设置
		String table = "";
		if ("1".equals(type)) {	// 1代表跳转到安全库存设置
			if ("0".equals(update)) {// 0代表更新日均耗用量
				DateFormat.getDateByString(bdate,"yyyy-MM-dd");
				Date startdate = DateFormat.getDateByString(bdate,"yyyy-MM-dd");
				Date enddate = DateFormat.getDateByString(edate,"yyyy-MM-dd");
				CntUse cntUse=new CntUse();
				cntUse.setAcct(session.getAttribute("ChoiceAcct").toString());
				cntUse.setBdat(startdate);
				cntUse.setEdat(enddate);
				supplyMngService.updateCntUse(cntUse);
				modelMap.put("bdate", startdate);	
				modelMap.put("edate", enddate);
				modelMap.put("update", "ok");
			}else { // 1代表打开安全库存设置页面的初始状态
				modelMap.put("bdate", DateFormat.getDateBefore(new Date(), "day", -1, 30));	
				modelMap.put("edate", new Date());
			}
			table = SupplyMngConstants.TABLE_SECUSTOCK;
		}else if ("0".equals(type)) { // 0代表跳转到物资管理
			table = SupplyMngConstants.TABLE_SUPPLYMNG;
		}
		//添加物资权限
//		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
//		supply.setAccountId(session.getAttribute("accountId").toString());
		supply.setSta("all");
		List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply, level, code, page);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("15");
		List<CodeDes> codeDesList = codeDesService.findCodeDes(codeDes);//货架
		for(CodeDes cd : codeDesList){
			for(Supply s : supplyList){
				if(cd.getCode().equals(s.getAttribute())){
					s.setAttribute(cd.getDes());
				}
			}
		}
		modelMap.put("supplyList", supplyList);
		modelMap.put("pageobj", page);
		return new ModelAndView(table, modelMap);
	}
	
	/**
	 * 打开选择供应商类别
	 * @param modelMap
	 * @param spcodeList
	 * @param inputVal
	 * @param deliver
	 * @param page
	 * @param prdctcard
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/selectDeliver")
	public ModelAndView selectDeliver(ModelMap modelMap, String bytyp, String spcodeList, String code1, String level, String inputVal, Deliver deliver, Page page, String prdctcard) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("0".equals(prdctcard)) {//编码查询
			deliver.setCode(inputVal);
		}else if ("1".equals(prdctcard)) {//名称	
			deliver.setDes(inputVal);
		}else if ("2".equals(prdctcard)) {//缩写
			deliver.setInit(inputVal);
		}else if ("3".equals(prdctcard)) {//联系人
			deliver.setPerson(inputVal);
		}
		List<Deliver> deliverList = deliverService.findDeliver(deliver, page);
		modelMap.put("deliverList", deliverList);
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		modelMap.put("pageobj", page);
		modelMap.put("inputVal", inputVal);
		modelMap.put("prdctcard", prdctcard);
		return new ModelAndView(SupplyMngConstants.SELECT_SUPPLYDELIVER, modelMap);
	}
	
	/**
	 * 打开选择仓位
	 * @param modelMap
	 * @param positn
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/selectPositn")
	public ModelAndView selectPositn(ModelMap modelMap, Positn positn, String spcodeList,String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("listPositn", positnService.findAllPositn(positn));
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.SELECT_SUPPLYPOSITN, modelMap);
	}

	/**
	 * 打开选择仓位
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectPositnByPage")
	public ModelAndView selectPositnByPage(ModelMap modelMap,Page page, Positn positn, String spcodeList, String code1, String level, 
			String mod, String sp_code, String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		positn.setCode(positn.getPcode());
		positn.setDes(positn.getPname());
		modelMap.put("listPositn", positnService.findAllPositnByPage(positn,page));
		modelMap.put("pageobj", page);
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		modelMap.put("mod", mod);
		modelMap.put("sp_code", sp_code);
		modelMap.put("type", type);
		return new ModelAndView(SupplyMngConstants.SELECT_SUPPLYPOSITN, modelMap);
	}
	
	/**
	 * 打开选择虚拟物料
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateSupply_x")
	public ModelAndView selectSupply_x(ModelMap modelMap,Page page, Positn positn, String spcodeList, String code1, String level, 
		String mod, String sp_code, String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyList", supplyService.selectAllSupply_x(null));
		modelMap.put("pageobj", page);
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		modelMap.put("mod", mod);
		modelMap.put("sp_code", sp_code);
		modelMap.put("type", type);
		return new ModelAndView(SupplyMngConstants.UPDATE_SUPPLY_X, modelMap);
	}
	
	
	/**
	 * 打开选择实际物料
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateSupply")
	public ModelAndView selectSupply(ModelMap modelMap,Page page, Positn positn, String spcodeList, String code1, String level, 
			String mod, String sp_code, String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("pageobj", page);
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		modelMap.put("mod", mod);
		modelMap.put("sp_code", sp_code);
		modelMap.put("type", type);
		return new ModelAndView(SupplyMngConstants.UPDATE_SUPPLY, modelMap);
	}
	
	/**
	 * 打开选择货架
	 * @param modelMap
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/selectPositn1")
	public ModelAndView selectPositn1(ModelMap modelMap, String spcodeList, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("17");
		modelMap.put("positn1List", codeDesService.findCodeDes(codeDes));//货架
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.SELECT_SUPPLYPOSITN1, modelMap);
	}
	
	/**
	 * 打开选择货架
	 * @param modelMap
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/setABC")
	public ModelAndView setABC(ModelMap modelMap, String spcodeList, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.SET_ABC, modelMap);
	}
	
	/**
	 * 保存修改的供应商
	 * @param modelMap
	 * @param deliver
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveDelByUpd")
	public ModelAndView saveByDelUpd(ModelMap modelMap, Deliver deliver, String spcodeList, String code1, String level, String bytyp, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyMngService.saveDelByUpd(spcodeList, deliver.getCode(), deliver.getDes());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	
	/**
	 *保存修改的虚拟物料 
	 * @param modelMap
	 * @param positn
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveSupply_x")
	public ModelAndView saveSupply_x(ModelMap modelMap, Supply supply , String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply1 = new Supply();
			supply1.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply1,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyMngService.saveSupply_xByUpd(spcodeList, supply.getSp_code_x(),supply.getSp_name_x(),supply.getSp_init_x(),supply.getUnit_x(),supply.getUnitRate_x());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
		
		
	}
	
	
	/**
	 *保存修改的实际物料 
	 * @param modelMap
	 * @param positn
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveSupply")
	public ModelAndView saveSupply(ModelMap modelMap, Supply supply , String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyMngService.saveSupplyByUpd(spcodeList, supply.getSp_code_x(),supply.getSp_name_x(),supply.getSp_init_x(),supply.getUnit_x(),supply.getUnitRate_x());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
			
	}
	
	/**
	 * 保存修改的仓位
	 * @param modelMap
	 * @param positn
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/savePstByUpd")
	public ModelAndView savePstByUpd(ModelMap modelMap, Positn positn, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyMngService.savePstByUpd(spcodeList, positn.getCode(), positn.getDes());
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 保存修改的货架
	 * @param modelMap
	 * @param positn1
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/savePst1ByUpd")
	public ModelAndView savePst1ByUpd(ModelMap modelMap, String positn1, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyMngService.savePst1ByUpd(spcodeList, positn1);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 保存修改的货架
	 * @param modelMap
	 * @param abc
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveABCByUpd")
	public ModelAndView saveABCByUpd(ModelMap modelMap, String abc, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcodeList) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		supplyMngService.saveABCByUpd(spcodeList, abc);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 *  保存修改最大最小库存
	 * @param modelMap
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateStock")
	@ResponseBody
	public ModelAndView updateStock(ModelMap modelMap, Supply supply, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		supplyMngService.updateStock(supply,acct);
		return null;
	}
	
	/**
	 * 更新日均耗用
	 * @param modelMap
	 * @param bdat
	 * @param edat
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCntUse")
	public ModelAndView updateCntUse(ModelMap modelMap, String bdat, String edat, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Date startdate = DateFormat.getDateByString(bdat,"yyyy-MM-dd");
		Date enddate = DateFormat.getDateByString(edat,"yyyy-MM-dd");
		CntUse cntUse=new CntUse();
		cntUse.setAcct(session.getAttribute("ChoiceAcct").toString());
		cntUse.setBdat(startdate);
		cntUse.setEdat(enddate);
		supplyMngService.updateCntUse(cntUse);
//		return null;
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 更新库存量
	 * @param modelMap
	 * @param bdat
	 * @param edat
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toUpdateCnt")
	public ModelAndView toUpdateCnt(ModelMap modelMap, String level, String code){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("code", code);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.UPDATE_CNT, modelMap);
	}
	
	/**
	 * 批量保存修改最大最小库存
	 * @param modelMap
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateCnt")
	public ModelAndView updateCnt(ModelMap modelMap, HttpSession session, String level, String code, String ratemin, String ratemax) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String spcodeList = "";
		Supply supply = new Supply();
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code);
		for (int i=0;i<supplyList.size();i++) {
			spcodeList += supplyList.get(i).getSp_code();
			spcodeList += ",";
		}
		spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		supplyMngService.updateCnt(spcodeList, ratemin, ratemax);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 修改报货分类
	 * @param modelMap
	 * @param code
	 * @param spcode
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/updateTypEas")
	public ModelAndView updateTypEas(ModelMap modelMap,String code,String spcode,HttpSession session, String code1, String level) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (""==spcode) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcode += supplyList.get(i).getSp_code();
				spcode += ",";
			}
			spcode=spcode.substring(0, spcode.length()-1);
		}
		supplyMngService.updateTypEas(spcode, code);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
//		String acct=session.getAttribute("ChoiceAcct").toString();
//		List<String> listCode=Arrays.asList(spcode.split(","));
//		for(int i=0;i<listCode.size();i++){
//			Supply supply=new Supply();
//			supply.setSp_code(listCode.get(i));
//			supply.setTyp_eas(code);
//			supply.setAcct(acct);
//			supplyMngService.updateTypEas(supply);
//		}
	}
	
	/**
	 * 分店安全库存设置
	 * @param modelMap
	 * @param supply
	 * @param level
	 * @param spcodeList
	 * @param code
	 * @param page
	 * @param session
	 * @param type
	 * @returnupdateStockFirmupdateStockFirm
	 * @throws Exception
	 */
	@RequestMapping(value = "/positnSpcode")
	public ModelAndView findPositnSpcode(ModelMap modelMap, Supply supply, String level, String spcodeList, String action,
			String code, Page page, HttpSession session, String firmDes, String firmCode, String update, String bdate, String edate) throws Exception {
		
		//帐套
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("update", update);
		//查询记忆
		modelMap.put("supply", supply);	
		modelMap.put("firmCode", firmCode);//门店
		modelMap.put("firmDes", firmDes);//门店
		if ("0".equals(update)) {// 0代表更新日均耗用量
			DateFormat.getDateByString(bdate,"yyyy-MM-dd");
			Date startdate = DateFormat.getDateByString(bdate,"yyyy-MM-dd");
			Date enddate = DateFormat.getDateByString(edate,"yyyy-MM-dd");
			CntUse cntUse=new CntUse();
			cntUse.setAcct(session.getAttribute("ChoiceAcct").toString());
			cntUse.setBdat(startdate);
			cntUse.setEdat(enddate);
			cntUse.setPositn(firmCode);
			//firmCode 判断为分店  还是仓库
			Positn positn = new Positn();
			positn.setCode(firmCode);
			Positn firm = positnService.findPositnByCode(positn);
			
			if(firm.getPcode()!=null && "1203".equals(firm.getPcode())){
				//设置分店的安全库存
				supplyMngService.updateCntUseFirm(cntUse);
			}else{
				//设置仓库的安全库存
				supplyMngService.updateCntUsePositn(cntUse);
			}
			
			modelMap.put("bdate", startdate);	
			modelMap.put("edate", enddate);
			modelMap.put("update", "ok");
		}else { // 1代表打开安全库存设置页面的初始状态
			modelMap.put("bdate", DateFormat.getDateBefore(new Date(), "day", -1, 30));	
			modelMap.put("edate", new Date());
		}
		if (!"init".equals(action)) {
			supply.setSp_position(firmCode);
			//firmCode 判断为分店  还是仓库
			Positn positn = new Positn();
			positn.setCode(firmCode);
			Positn firm = positnService.findPositnByCode(positn);
//			if(firm.getTyp()!=null && "分店".equals(firm.getTyp())){
			modelMap.put("supplyList", firmSupplyService.findPositnSpCode(supply, level, code, page));
//			}else{
//				modelMap.put("supplyList", firmSupplyService.findCangKuSpCode(supply, page));
//			}
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(SupplyMngConstants.TABLE_SECUSTOCK_FIRM, modelMap);
	}
	
	/**
	 *  保存修改最大最小库存
	 * @param modelMap
	 * @param supply
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateStockFirm")
	@ResponseBody
	public ModelAndView updateStockFirm(ModelMap modelMap, PositnSpcode positnSpcode, HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyMngService.updateStockFirm(positnSpcode);
		return null;
	}
	
	/**
	 * 打开显示物资属性的页面 add by jinshuai at 20160229
	 * @param modelMap
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/modifyAttribute")
	public ModelAndView modifyAttribute(ModelMap modelMap, String spcodeList, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp("15");
		modelMap.put("attributeList", codeDesService.findCodeDes(codeDes));//货架
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.MODIFY_ATTRIBUTE, modelMap);
	}
	
	/**
	 * 保存修改的属性 add by jinshuai at 20160301
	 * @param modelMap
	 * @param positn1
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveModifyAttribute")
	public ModelAndView saveModifyAttribute(ModelMap modelMap, String attribute, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (null==spcodeList||"".equals(spcodeList)) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		
		supplyMngService.saveModifyAttribute(spcodeList, attribute);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 打开显示物资加工间是否领料的属性
	 * @param modelMap
	 * @param spcodeList
	 * @return
	 * @throws CRUDException
	 * @throws com.choice.framework.exception.CRUDException 
	 */
	@RequestMapping(value = "/updateExkc")
	public ModelAndView updateExkc(ModelMap modelMap, String spcodeList, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("spcodeList", spcodeList);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(SupplyMngConstants.SHOW_UPDATE_EXKC, modelMap);
	}
	
	/**
	 * 保存修改物资加工间是否领料的属性
	 * @param modelMap
	 * @param positn1
	 * @param spcodeList
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveUpdateExkc")
	public ModelAndView saveUpdateExkc(ModelMap modelMap, String exkc, String spcodeList, String code1, String level, HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//按照类别来修改的
		if (null==spcodeList||"".equals(spcodeList)) {
			Supply supply = new Supply();
			supply.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Supply> supplyList = supplyService.findAllSupplyByLeftGrpTyp(supply,level,code1);
			for (int i=0;i<supplyList.size();i++) {
				spcodeList += supplyList.get(i).getSp_code();
				spcodeList += ",";
			}
			spcodeList=spcodeList.substring(0, spcodeList.length()-1);
		}
		//修改
		supplyMngService.saveUpdateExkc(spcodeList, exkc);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
}
