package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ana.XsLirunFenxiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("XsLirunFenxi")
public class XsLirunFenxiController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private XsLirunFenxiService xsLirunFenxiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private AccountPositnService accountPositnService;
	
	/****************************************************毛利查询 start***************************************************/
	/**
	 * 毛利查询
	 */
	@RequestMapping(value = "/toGrossProfitList")
	public ModelAndView toGrossProfitList(ModelMap modelMap,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		if(null != checkMis && !"".equals(checkMis)){
			modelMap.put("type", checkMis);
		}
		return new ModelAndView(SupplyAcctConstants.LIST_GROSS_PROFIT,modelMap);
	}
	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findGrossProfitHeaders")
	@ResponseBody
	public Object findGrossProfitHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_MAOLI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_MAOLI));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_MAOLI_FROZEN);
		return columns;
	}
	
	/**
	 * 毛利查询
	 */
	@RequestMapping("/findGrossProfit")
	@ResponseBody
	public Object findGrossProfit(ModelMap modelMap,HttpSession session,String page,String rows,String bdat,String edat,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(CodeHelper.replaceCode(accountPositn.getPositn().getCode()));
			}
		}
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("bdat", bdat);
		content.put("edat", edat);
		content.put("supplyAcct",supplyAcct);
		pager.setNowPage(page==""||page==null ? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return xsLirunFenxiService.findGrossProfit(content, pager);
	}
	
	/**
	 * 导出
	 */
	@RequestMapping("/exportGrossProfit")
	@ResponseBody
	public void exportGrossProfit(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "毛利查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(CodeHelper.replaceCode(accountPositn.getPositn().getCode()));
			}
		}
		
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.TABLE_NAME_MAOLI);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), xsLirunFenxiService.findGrossProfit(condition,pager).getRows(), "毛利查询", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_MAOLI));	
	}
	
	/**
	 * 打印
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printGrossProfit")
	public ModelAndView printGrossProfit(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct,String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			condition.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			condition.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				supplyAcct.setPositn(CodeHelper.replaceCode(accountPositn.getPositn().getCode()));
			}
		}
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",xsLirunFenxiService.findGrossProfit(condition, pager).getRows());
	    parameters.put("report_name", "毛利查询");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/firmMis/printGrossProfit.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_GROSS_PROFIT,SupplyAcctConstants.REPORT_PRINT_URL_GROSS_PROFIT);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/****************************************************毛利查询 end***************************************************/
}
