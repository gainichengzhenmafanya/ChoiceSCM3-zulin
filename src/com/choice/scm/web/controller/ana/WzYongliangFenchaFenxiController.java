package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.service.ana.WzYongliangfenchaFenxiService;
import com.choice.scm.util.ExportExcel;

/**
 * 物资 用量分差分析
 * @author wjf
 *
 */
@Controller
@RequestMapping("WzYongliangFenchaFenxi")
public class WzYongliangFenchaFenxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private WzYongliangfenchaFenxiService wzYongliangfenchaFenxiService;
	
	/***********************************************************物资用量分差分析start**************************************************/
	/**
	 * 跳转物资用量分差分析
	 */
	@RequestMapping("/toWzYongliangFenchaFenxi")
	public ModelAndView toWzYongliangFenchaFenxi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", AnaConstants.REPORT_NAME_WZYONGLIANGFENCHAFENXI);
		return new ModelAndView(AnaConstants.REPORT_SHOW_WZYONGLIANGFENCHAFENXI,modelMap);
	}
	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findWzYongliangFenchaFenxiHeaders")
	@ResponseBody	
	public Object findWzYongliangFenchaFenxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(AnaConstants.REPORT_NAME_WZYONGLIANGFENCHAFENXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_WZYONGLIANGFENCHAFENXI));
		columns.put("frozenColumns", AnaConstants.BASICINFO_REPORT_WZYONGLIANGFENCHAFENXI_FROZEN);
		return columns;
	}
	
	/**
	 * 统计物资用量分差分析
	 */
	@RequestMapping("/calWzYongliangFenchaFenxi")
	@ResponseBody
	public String calWzYongliangFenchaFenxi(PtivityAna ptivity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return wzYongliangfenchaFenxiService.callScmWzYongliangFenchaFenxi(ptivity);
	}
	
	/**
	 * 查询物资用量分差分析
	 */
	@RequestMapping("/findWzYongliangFenchaFenxi")
	@ResponseBody
	public Object findWzYongliangFenchaFenxi(ModelMap modelMap, HttpSession session, String page, String rows, PtivityAna ptivity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		ptivity.setAcct(session.getAttribute("ChoiceAcct").toString());
		return wzYongliangfenchaFenxiService.findWzYongliangFenchaFenxi(ptivity, pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportWzYongliangFenchaFenxi")
	@ResponseBody
	public void exportWzYongliangFenchaFenxi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,PtivityAna ptivity) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资用量分差分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(AnaConstants.REPORT_NAME_WZYONGLIANGFENCHAFENXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ptivity.setAcct(session.getAttribute("ChoiceAcct").toString());
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzYongliangfenchaFenxiService.findWzYongliangFenchaFenxi(ptivity,pager).getRows(), 
				"物资用量分差分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_WZYONGLIANGFENCHAFENXI));	
	}

}
