package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.CaiPinShiJiMaoLiConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.CaiPinShiJiMaoLiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 菜品实际毛利 报表
 * @author csb
 *
 */
@Controller
@RequestMapping("caiPinShiJiMaoLi")
public class CaiPinShiJiMaoLiController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private CaiPinShiJiMaoLiService caiPinMaoLiService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private AcctService acctService;
	/**
	 * 跳转到列选择
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseCaiPinShiJiMaoLi")
	public ModelAndView toColChooseCaiPinShiJiMaoLi(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(CaiPinShiJiMaoLiConstants.REPORT_NAME_CAIPIN_SHIJI_MAOLI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", CaiPinShiJiMaoLiConstants.REPORT_NAME_CAIPIN_SHIJI_MAOLI);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,CaiPinShiJiMaoLiConstants.BASICINFO_REPORT_CAIPIN_SHIJI_MAOLI));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(CaiPinShiJiMaoLiConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findCaiPinShiJiMaoLiHeaders")
	@ResponseBody
	public Object getCaiPinShiJiMaoLiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(CaiPinShiJiMaoLiConstants.REPORT_NAME_CAIPIN_SHIJI_MAOLI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, CaiPinShiJiMaoLiConstants.BASICINFO_REPORT_CAIPIN_SHIJI_MAOLI));
		return columns;
	}
	
	/**
	 * 跳转到分店菜品利润表页面
	 * @return
	 */
	@RequestMapping("/toCaiPinShiJiMaoLi")
	public ModelAndView toCaiPinShiJiMaoLi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", CaiPinShiJiMaoLiConstants.REPORT_NAME_CAIPIN_SHIJI_MAOLI);
		return new ModelAndView(CaiPinShiJiMaoLiConstants.LIST_CAIPIN_SHIJI_MAOLI,modelMap);
	}
	/**
	 * 查询分店菜品利润表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findCaiPinShiJiMaoLi")
	@ResponseBody
	public Object findCaiPinShiJiMaoLi(ModelMap modelMap, HttpSession session, String page, String rows, 
			String sort, String order, String sp_cost, SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("sp_cost", sp_cost);
		supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
			condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
			condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return caiPinMaoLiService.findCaiPinShiJiMaoLi(condition, pager);
	}
	/**
	 * 导出
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param sp_cost
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCaiPinShiJiMaoLi")
	@ResponseBody
	public void exportCostVariance(HttpServletRequest request, HttpServletResponse response, 
			ModelMap modelMap, HttpSession session,String page, String rows, String sort, String order,
			String sp_cost, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "菜品实际毛利";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sp_cost", sp_cost);
		condition.put("sort", sort);
		condition.put("order", order);
        if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
            condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
            condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
            condition.put("COSTITEM", "COSTITEM");
        }else{
            condition.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
            condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
            condition.put("COSTITEM", "firmCOSTITEM");
        }
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(CaiPinShiJiMaoLiConstants.REPORT_NAME_CAIPIN_SHIJI_MAOLI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), 
				caiPinMaoLiService.findCaiPinShiJiMaoLi(condition,pager).getRows(), "菜品实际毛利", 
				dictColumnsService.listDictColumnsByAccount(dictColumns, CaiPinShiJiMaoLiConstants.BASICINFO_REPORT_CAIPIN_SHIJI_MAOLI));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printCaiPinShiJiMaoLi")
	public ModelAndView printCaiPinShiJiMaoLi(ModelMap modelMap,Page pager,HttpSession session, String type,
			String sp_cost,SupplyAcct supplyAcct, String sort, String order)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		if(supplyAcct.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy/MM/dd"));
		}
		if(supplyAcct.getEdat() != null){
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy/MM/dd"));
		}
        if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
            condition.put("COSTITEMSPCODE", "COSTITEMSPCODE");
            condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
            condition.put("COSTITEM", "COSTITEM");
        }else{
            condition.put("COSTITEMSPCODE", "firmCOSTITEMSPCODE");
            condition.put("DEPTSUPPLYACCT", "firmDEPTSUPPLYACCT");
            condition.put("COSTITEM", "firmCOSTITEM");
        }
		condition.put("supplyAcct", supplyAcct);
		condition.put("sp_cost", sp_cost);
		condition.put("sort", sort);
		condition.put("order", order);
		params.put("firm", supplyAcct.getFirm());
		params.put("sp_code", supplyAcct.getSp_code());
		params.put("sp_cost", sp_cost);
		params.put("sort", sort);
		params.put("order", order);
		modelMap.put("List",caiPinMaoLiService.findCaiPinShiJiMaoLi(condition,pager).getRows());
	    parameters.put("reportName", "菜品实际毛利");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/caiPinShiJiMaoLi/printCaiPinShiJiMaoLi.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,CaiPinShiJiMaoLiConstants.REPORT_PRINT_URL_CAIPIN_SHIJI_MAOLI,CaiPinShiJiMaoLiConstants.REPORT_PRINT_URL_CAIPIN_SHIJI_MAOLI);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}			
}
