package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ana.BcpChengbenFenxiService;
import com.choice.scm.util.ExportExcel;
/***
 * 半成品成本分析
 * @author wjf
 *
 */
@Controller
@RequestMapping("bcpChengbenFenxi")
public class BcpChengbenFenxiController {

    @Autowired
	private Page pager;
	@Autowired
	private BcpChengbenFenxiService bcpChengbenFenxiService;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	
	/**
	 * 跳转到半成品成本分析
	 * @return
	 */
	@RequestMapping("/toBcpChengbenFenxi")
	public ModelAndView toBcpChengbenFenxi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", AnaConstants.REPORT_NAME_BCPCHENGBENFENXI);
		return new ModelAndView(AnaConstants.LIST_BCPCHENGBENFENXI,modelMap);
	}

	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findBcpChengbenFenxiHeaders")
	@ResponseBody
	public Object findBcpChengbenFenxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.REPORT_NAME_BCPCHENGBENFENXI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_BCPCHENGBENFENXI));
		return columns;
	}
	
	/**
	 * 查询半成品成本分析
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findBcpChengbenFenxi")
	@ResponseBody
	public Object findBcpChengbenFenxi(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,String itcode,String itdes) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setFirm(supplyAcct.getPositn());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("itcode", itcode);
		condition.put("itdes", itdes);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return bcpChengbenFenxiService.findBcpChengbenFenxi(condition, pager);
	}
	
	/**
	 * 导出半成品成本分析
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportBcpChengbenFenxi")
	@ResponseBody
	public void exportBcpChengbenFenxi(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String itcode,String itdes) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "半成品成本分析";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		supplyAcct.setFirm(supplyAcct.getPositn());
		conditions.put("supplyAcct", supplyAcct);
		conditions.put("itcode", itcode);
		conditions.put("itdes", itdes);
		dictColumns.setTableName(AnaConstants.REPORT_NAME_BCPCHENGBENFENXI);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), bcpChengbenFenxiService.findBcpChengbenFenxi(conditions, pager).getRows(), "单菜毛利率", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.BASICINFO_REPORT_BCPCHENGBENFENXI));
	}
}
