package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.DcMaolilvService;
import com.choice.scm.service.ana.GdMaolilvService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.PublicExportExcel;
/***
 * 单菜毛利率
 * @author wjf
 *
 */
@Controller
@RequestMapping("dcMaolilv")
public class DcMaolilvController {

    @Autowired
	private Page pager;
    @Autowired
	private GdMaolilvService gdMaolilvService;
	@Autowired
	private DcMaolilvService dcMaolilvService;
	@Autowired
    private AcctService acctService;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	
	/********************************************单菜毛利横向对比报表****************************************************/
    
    /**
	 * 跳转到单菜毛利横向对比
	 * @return
	 */
	@RequestMapping("/toDcMaolilv")
	public ModelAndView toDcMaolilv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DCMLHENGXIANGDUIBI,modelMap);
	}
	
	/**
	 * 查询表头信息(动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findDcMaolilvHeaders")
	@ResponseBody
	public List<WeekSet> findDcMaolilvHeaders(SupplyAcct sa) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(sa.getBdat() == null || sa.getEdat() == null){
			Date nowDate = new Date();
			nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
			sa.setBdat(nowDate);
			sa.setEdat(nowDate);
			sa.setShowtyp(2);
		}
		return gdMaolilvService.findWeekSet(sa);
	}
	
	/***
	 * 查询单菜毛利率
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findDcMaolilv")
	@ResponseBody
	public Object findDcMaolilv(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(choiceAcct);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return dcMaolilvService.findDcMaolilv(findDcMaolilvHeaders(supplyAcct),condition,pager);
	}
	
	/**
	 * 导出单菜毛利率
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDcMaolilv")
	@ResponseBody
	public void exportDcMaolilv(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setAcct(choiceAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		List<WeekSet> wsList = findDcMaolilvHeaders(supplyAcct);
		List<Map<String,String>> headerList = new ArrayList<Map<String,String>>();
		Map<String,String> positn = new HashMap<String,String>();
		positn.put("编码:::POSITN", "null");
		headerList.add(positn);
		Map<String,String> positndes = new HashMap<String,String>();
		positndes.put("名称:::POSITNDES", "null");
		headerList.add(positndes);
		for(WeekSet ws : wsList){
			Map<String,String> map = new HashMap<String,String>();
			if(null == ws.getDes() || "".equals(ws.getDes())){
				map.put(ws.getWeekk()+":::W_"+ws.getWeekk(), "null");
			}else{
				map.put(ws.getWeekk()+"("+ws.getDes()+"):::W_"+ws.getWeekk(), "null");
			}
			headerList.add(map);
		}
		ReportObject<Map<String,Object>> result = dcMaolilvService.findDcMaolilv(wsList,condition,null);
		if(null != result.getFooter())
			result.getRows().addAll(result.getFooter());
		new PublicExportExcel().creatWorkBook(request, response, "单菜毛利率", "单菜毛利率", headerList, result.getRows());
	}
	
/********************************************单菜毛利率****************************************************/
	
	/**
	 * 跳转到单菜毛利率
	 * @return
	 */
	@RequestMapping("/toDancaiMaolilv")
	public ModelAndView toDancaiMaolilv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", AnaConstants.REPORT_NAME_DANCAIMAOLILV);
		return new ModelAndView(AnaConstants.LIST_DANCAIMAOLILV,modelMap);
	}

	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDancaiMaolilvHeaders")
	@ResponseBody
	public Object findDancaiMaolilvHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.REPORT_NAME_DANCAIMAOLILV);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_DANCAIMAOLILV));
		return columns;
	}
	
	/**
	 * 查询分店菜品利润表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDancaiMaolilv")
	@ResponseBody
	public Object findDancaiMaolilv(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct,String itcode,String itdes) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplyAcct.setFirm(supplyAcct.getPositn());
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("itcode", itcode);
		condition.put("itdes", itdes);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return dcMaolilvService.findDancaiMaolilv(condition, pager);
	}
	
	/**
	 * 导出单菜毛利率
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDancaiMaolilv")
	@ResponseBody
	public void exportDancaiMaolilv(HttpServletResponse response,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct,String itcode,String itdes) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "单菜毛利率";
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> conditions = new HashMap<String,Object>();
		supplyAcct.setFirm(supplyAcct.getPositn());
		supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		conditions.put("supplyAcct", supplyAcct);
		conditions.put("itcode", itcode);
		conditions.put("itdes", itdes);
		dictColumns.setTableName(AnaConstants.REPORT_NAME_DANCAIMAOLILV);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
	            + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), dcMaolilvService.findDancaiMaolilv(conditions, pager).getRows(), "单菜毛利率", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.BASICINFO_REPORT_DANCAIMAOLILV));
	}
}
