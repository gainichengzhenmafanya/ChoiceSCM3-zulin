package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.UseCostAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.ana.UseCostAnaService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 耗用成本 报表
 * @author csb
 *
 */
@Controller
@RequestMapping("useCostAna")
public class UseCostAnaController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private UseCostAnaService useCostAnaService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;
	/**
	 * 跳转到列选择
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseUseCostAna")
	public ModelAndView toColChooseUseCostAna(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源	
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(UseCostAnaConstants.REPORT_NAME_USECOSTANA_DETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "supply");
		modelMap.put("tableName", UseCostAnaConstants.REPORT_NAME_USECOSTANA_DETAIL);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,UseCostAnaConstants.BASICINFO_REPORT_USECOSTANA_DETAIL));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(UseCostAnaConstants.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findUseCostAnaHeaders")
	@ResponseBody
	public Object getUseCostAnaHeaders(HttpSession session,String type){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		if(type.equals("sum")){
			dictColumns.setTableName(UseCostAnaConstants.REPORT_NAME_USECOSTANA);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, UseCostAnaConstants.BASICINFO_REPORT_USECOSTANA));
		}
		if(type.equals("detail")){
			dictColumns.setTableName(UseCostAnaConstants.REPORT_NAME_USECOSTANA_DETAIL);
			columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, UseCostAnaConstants.BASICINFO_REPORT_USECOSTANA_DETAIL));
		}
		return columns;
	}
	/**
	 * 查看图表
	 */
	@RequestMapping("/findUseCostAnaTuBiao")
	public ModelAndView findUseCostAnaTuBiao(ModelMap modelMap,SupplyAcct supplyAcct){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		return new ModelAndView(UseCostAnaConstants.LIST_USECOSTANA_TUBIAO,modelMap);
	}
	/**
	 * 图表
	 */
	@RequestMapping("/findXmlForUseCostAnaTuBiao")
	public void findXmlForUseCostAnaTuBiao(HttpServletResponse response,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("supplyAcct", supplyAcct);
		useCostAnaService.findXmlForUseCostAna(response, map).output();
	}

	/**
	 * 跳转到耗用成本分析表页面
	 * @return
	 */
	@RequestMapping("/toUseCostAna")
	public ModelAndView toUseCostAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(UseCostAnaConstants.LIST_USECOSTANA,modelMap);
	}
	/**
	 * 查询耗用成本分析表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findUseCostAna")
	@ResponseBody
	public Object findUseCostAna(ModelMap modelMap,HttpSession session,String page,String type,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("type", type);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return useCostAnaService.findUseCostAna(condition, pager);
	}
	/**
	 * 导出
	 * @param request
	 * @param response
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param sp_cost
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportUseCostAna")
	@ResponseBody
	public void exportUseCostAna(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,String sp_cost,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "耗用成本分析";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("sp_cost", sp_cost);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(UseCostAnaConstants.REPORT_NAME_USECOSTANA_DETAIL);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), useCostAnaService.findUseCostAna(condition,pager).getRows(), "耗用成本分析", dictColumnsService.listDictColumnsByAccount(dictColumns, UseCostAnaConstants.BASICINFO_REPORT_USECOSTANA_DETAIL));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printUseCostAna")
	public ModelAndView printUseCostAna(ModelMap modelMap,Page pager,HttpSession session,String type,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",useCostAnaService.findUseCostAna(condition,pager).getRows());
	    parameters.put("report_name", "耗用成本分析");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/useCostAna/printUseCostAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,UseCostAnaConstants.REPORT_PRINT_URL_USECOSTANA,UseCostAnaConstants.REPORT_PRINT_URL_USECOSTANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}				
}
