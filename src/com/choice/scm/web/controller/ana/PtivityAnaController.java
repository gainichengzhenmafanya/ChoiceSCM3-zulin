package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.PtivityAnaService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * 分店应产率分析
 * @author lq
 *
 */
@Controller
@RequestMapping("ptivityAna")
public class PtivityAnaController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private PtivityAnaService ptivityAnaService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	AcctService acctService;
	
	/***********************************************************应产率分析start**************************************************/
	/**
	 * 跳转应产率分析
	 */
	@RequestMapping("/toPtivityAna")
	public ModelAndView toPtivityAna(ModelMap modelMap,String checkMis){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("type", checkMis);
		modelMap.put("reportName", AnaConstants.PTIVITYANA_TABLEJ);
		return new ModelAndView(AnaConstants.LIST_PTIVITYANA,modelMap);
	}
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findPtivityAnaHeaders")
	@ResponseBody	
	public Object getPtivityAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(AnaConstants.PTIVITYANA_TABLEJ);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.PTIVITYANA_COLUMN));
		columns.put("frozenColumns", AnaConstants.FROZEN_PTIVITYANA);
		return columns;
	}
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChoosePtivityAna")
	public ModelAndView toColChoosePtivityAna(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.PTIVITYANA_TABLEJ);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", AnaConstants.PTIVITYANA_TABLEJ);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.PTIVITYANA_COLUMN));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询应产率分析分析
	 */
	@RequestMapping("/findPtivityAna")
	@ResponseBody
	public Object findPtivityAna(ModelMap modelMap, HttpSession session, String page, String rows,
			PtivityAna ptivity, String sp_cost, String checkMis) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			ptivity.setSupplyacct("SUPPLYACCT");
			ptivity.setCostitem("COSTITEM");
		}else{
			ptivity.setSupplyacct("firmSUPPLYACCT");
			ptivity.setCostitem("firmCOSTITEM");
		}
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null==accountPositn || null==accountPositn.getPositn()){
                return null;//防止查不出门店导致获取了所有数据
			}
            ptivity.setPositn(CodeHelper.replaceCode(accountPositn.getPositn().getCode()));
        }
		return ptivityAnaService.findPtivityAna(ptivity, pager);
	}
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportPtivityAna")
	@ResponseBody
	public void exportPtivityAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,PtivityAna ptivity,String checkMis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			ptivity.setSupplyacct("SUPPLYACCT");
			ptivity.setCostitem("COSTITEM");
		}else{
			ptivity.setSupplyacct("firmSUPPLYACCT");
			ptivity.setCostitem("firmCOSTITEM");
		}
		String fileName = "应产率分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(AnaConstants.PTIVITYANA_TABLEJ);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				ptivity.setPositn(accountPositn.getPositn().getCode());
			}
		}
		exportExcelMap.creatWorkBook(response.getOutputStream(), ptivityAnaService.findPtivityAna(ptivity,pager).getRows(), "应产率分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.PTIVITYANA_COLUMN));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printPtivityAna")
	public ModelAndView printPtivityAna(ModelMap modelMap, Page pager, HttpSession session, String type,
			String page, String rows, PtivityAna ptivity, String checkMis)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			ptivity.setSupplyacct("SUPPLYACCT");
			ptivity.setCostitem("COSTITEM");
		}else{
			ptivity.setSupplyacct("firmSUPPLYACCT");
			ptivity.setCostitem("firmCOSTITEM");
		}
		if(null != checkMis && !"".equals(checkMis)){
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				ptivity.setPositn(accountPositn.getPositn().getCode());
			}
		}
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("positn",ptivity.getPositn());
		params.put("sp_code",ptivity.getSp_code());
		params.put("grptyp",ptivity.getGrptyp());
		params.put("grp",ptivity.getGrp());
		params.put("typ",ptivity.getTyp());
		params.put("onlyjp",ptivity.getOnlyjp());
		params.put("bdat",DateFormat.getStringByDate(ptivity.getBdat(), "yyyy-MM-dd"));
		params.put("edat",DateFormat.getStringByDate(ptivity.getEdat(), "yyyy-MM-dd"));
		modelMap.put("actionMap", params);
		modelMap.put("List",ptivityAnaService.findPtivityAna(ptivity, pager).getRows());
		
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "应产率分析分析");
	    parameters.put("bdat",DateFormat.getStringByDate(ptivity.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(ptivity.getEdat(), "yyyy-MM-dd"));
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/ptivityAna/printPtivityAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,AnaConstants.REPORT_PRINT_URL_PTIVITYANA, AnaConstants.REPORT_PRINT_URL_PTIVITYANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		

	/***********************************************************应产率分析start**************************************************/		
}
