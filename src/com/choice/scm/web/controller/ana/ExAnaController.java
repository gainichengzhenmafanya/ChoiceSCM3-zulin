package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ana.ExThCostAna;
import com.choice.scm.service.ExAnaService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
/**
 * 产品理论成本分析   报表                 
 * @author lehui
 *
 */
@Controller
@RequestMapping("exAna")
public class ExAnaController {

	@Autowired
	private ExAnaService exAnaService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<ExThCostAna> exportExcel;
	/**
	 * 产品理论成本分析
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exTheoryCostAna")
	public ModelAndView findAllExplan(ModelMap modelMap,Page page,ExThCostAna exThCostAna,HttpSession session,String action)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.EX_THORY_COST_ANA_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.EX_THORY_COST_ANA_COLUMN));
 		if(null!=action&&"init".equals(action)){
 			exThCostAna.setDat(new Date());
 			exThCostAna.setDatEnd(new Date());
 			modelMap.put("ListBean", "123");//table体list
 			modelMap.put("ListBean_total","123");//table总计体list
 		}else{
 			String positnex = exThCostAna.getPositnex();//先记录值2014.12.11wjf
 			modelMap.put("ListBean", JSONArray.fromObject(exAnaService.exTheoryCostAna(exThCostAna,page)).toString());//table体list			
 			modelMap.put("ListBean_total", JSONArray.fromObject(exAnaService.exTheoryCostAna_total(exThCostAna)).toString());//合计
 			exThCostAna.setPositnex(positnex);//再更回来 2014.12.11wjf
 		}
		modelMap.put("pageobj", page);
		modelMap.put("exThCostAna", exThCostAna);
		return new ModelAndView(AnaConstants.EX_THEORY_COST_ANA, modelMap);
	}
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColumnsChoose")
	public ModelAndView toColumnsChoose(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		DictColumns dictColumns = new DictColumns();
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.EX_THORY_COST_ANA_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "supply");
		modelMap.put("tableName", AnaConstants.EX_THORY_COST_ANA_TABLE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.EX_THORY_COST_ANA_COLUMN));
		dictColumns.setAccountId("");
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByAccount(dictColumns,null));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 导出产品理论成本分析
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportExTheoryCostAna")
	@ResponseBody
	public void exportExTheoryCostAna(HttpServletResponse response,HttpServletRequest request,HttpSession session,ExThCostAna exThCostAna) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName="产品理论成本分析";
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(AnaConstants.EX_THORY_COST_ANA_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(),exAnaService.exTheoryCostAna(exThCostAna), "产品理论成本分析", dictColumnsService.listDictColumnsByAccount(dictColumns,AnaConstants.EX_THORY_COST_ANA_COLUMN));
	}
	/**
	 * 打印产品理论成本分析
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printExTheoryCostAna")
	public ModelAndView printExTheoryCostAna(ModelMap modelMap, HttpSession session,String type,ExThCostAna exThCostAna)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> params = new HashMap<String,String>();
		
		List<ExThCostAna> disList = exAnaService.exTheoryCostAna(exThCostAna);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		if(exThCostAna.getDat() != null){
			params.put("dat",DateFormat.getStringByDate(exThCostAna.getDat(), "yyyy-MM-dd"));
		}
		if(exThCostAna.getDatEnd() != null){
			params.put("datEnd",DateFormat.getStringByDate(exThCostAna.getDatEnd(), "yyyy-MM-dd"));
		}
		params.put("grp",exThCostAna.getGrp());
		params.put("positnex",exThCostAna.getPositnex());
		params.put("sp_code",exThCostAna.getSp_code());
		params.put("sortName",exThCostAna.getSortName());
		params.put("sortOrder",exThCostAna.getSortOrder());
		modelMap.put("List",disList);//list
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "产品理论成本分析");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("dat",DateFormat.getStringByDate(exThCostAna.getDat(), "yyyy-MM-dd"));
	    parameters.put("datEnd",DateFormat.getStringByDate(exThCostAna.getDatEnd(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/exAna/printExTheoryCostAna.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,AnaConstants.REPORT_PRINT_URL_EXTHEORYCOST,AnaConstants.REPORT_PRINT_URL_EXTHEORYCOST);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
