package com.choice.scm.web.controller.ana;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.service.ana.YlYingchanlvService;
import com.choice.scm.util.PublicExportExcel;
/***
 * 原料应产率横向对比
 * @author wjf
 *
 */
@Controller
@RequestMapping("ylYingchanlv")
public class YlYingchanlvController {

	@Autowired
	private Page pager;
	@Autowired
	private YlYingchanlvService ylYingchanlvService;
    
	
	/********************************************原料应产率报表****************************************************/
    
    /**
	 * 跳转到原料应产率报表页面
	 * @return
	 */
	@RequestMapping("/toYlYingchanlv")
	public ModelAndView toYlYingchanlv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_YLYINGCHANLV,modelMap);
	}
	
	/**
	 * 查询表头信息(动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findYlYingchanlvHeaders")
	@ResponseBody
	public List<WeekSet> findYlYingchanlvHeaders(SupplyAcct sa) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(sa.getBdat() == null || sa.getEdat() == null){
			Date nowDate = new Date();
			nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
			sa.setBdat(nowDate);
			sa.setEdat(nowDate);
			sa.setShowtyp(2);
		}
		return ylYingchanlvService.findWeekSet(sa);
	}
	
	/***
	 * 查询原料应产率
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findYlYingchanlv")
	@ResponseBody
	public Object findYlYingchanlv(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return ylYingchanlvService.findYlYingchanlv(findYlYingchanlvHeaders(supplyAcct),condition,pager);
	}
	
	/**
	 * 导出原料应产率
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportYlYingchanlv")
	@ResponseBody
	public void exportYlYingchanlv(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		List<WeekSet> wsList = findYlYingchanlvHeaders(supplyAcct);
		List<Map<String,String>> headerList = new ArrayList<Map<String,String>>();
		Map<String,String> positn = new HashMap<String,String>();
		positn.put("编码:::POSITN", "null");
		headerList.add(positn);
		Map<String,String> positndes = new HashMap<String,String>();
		positndes.put("名称:::POSITNDES", "null");
		headerList.add(positndes);
		for(WeekSet ws : wsList){
			Map<String,String> map = new HashMap<String,String>();
			if(null == ws.getDes() || "".equals(ws.getDes())){
				map.put(ws.getWeekk()+":::W_"+ws.getWeekk(), "null");
			}else{
				map.put(ws.getWeekk()+"("+ws.getDes()+"):::W_"+ws.getWeekk(), "null");
			}
			headerList.add(map);
		}
		List<Map<String,Object>> rows = ylYingchanlvService.findYlYingchanlv(wsList,condition,null).getRows();
		new PublicExportExcel().creatWorkBook(request, response, "原料应产率", "原料应产率", headerList, rows);
	}
}
