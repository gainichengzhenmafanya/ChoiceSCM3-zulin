package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.ProfitAnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.Profit;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.ProfitAnaService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

@Controller
@RequestMapping("profitAna")
public class ProfitAnaController {
	
	@Autowired
	private ProfitAnaService profitAnaService;
	@Autowired
	AccountPositnService accountPositnService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Profit> exportExcel;
	@Autowired
	PositnService positnService;
	@Autowired
	AcctService acctService;
	
/**************************************************毛利日趋势start*********************************************/	
	/**
	 * 毛利日趋势
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/profitDay")
	public ModelAndView profitDay(ModelMap modelMap,Page page,Profit profit, HttpSession session,String action)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		if(null==action && !"init".equals(action)){
			String oFirmcode = profit.getFirmcode();
			profit.setFirmcode(CodeHelper.replaceCode(oFirmcode));
			modelMap.put("ListBean", JSONArray.fromObject(profitAnaService.findProfitDay(profit, page)).toString());//table体list
			modelMap.put("ListBean_total", JSONArray.fromObject(profitAnaService.findProfitDay_total(profit)).toString());//合计
			profit.setFirmcode(oFirmcode);
			
		}else{
			profit.setBdat(new Date());
			profit.setEdat(new Date());
			modelMap.put("ListBean", "123");//table体list
			modelMap.put("ListBean_total","123");//table总计体list
		}
		//加入列选择
		DictColumns dictColumns = new DictColumns();					
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ProfitAnaConstants.PROFIT_DAY_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ProfitAnaConstants.PROFIT_DAY_COLUMN));
		modelMap.put("pageobj", page);
		modelMap.put("profit", profit);
		return new ModelAndView(ProfitAnaConstants.LIST_PROFITDAY, modelMap);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportProfitDay")
	@ResponseBody
	public void exportProfitDay(HttpServletResponse response,HttpServletRequest request,HttpSession session,Profit profit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName="毛利日趋势";
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ProfitAnaConstants.PROFIT_DAY_TABLE);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		profit.setFirmcode(CodeHelper.replaceCode(profit.getFirmcode()));
		exportExcel.creatWorkBook(response.getOutputStream(),profitAnaService.findProfitDay(profit), "毛利日趋势", 
				dictColumnsService.listDictColumnsByAccount(dictColumns,ProfitAnaConstants.PROFIT_DAY_COLUMN));
	}
	
	/**
	 * 打印毛利日趋势报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printProfitDay")
	public ModelAndView printProfitDay(ModelMap modelMap, HttpSession session, String type, Profit profit)throws CRUDException{
		//选择数据源
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		String oFirmcode = profit.getFirmcode();
		profit.setFirmcode(CodeHelper.replaceCode(oFirmcode));
		List<Profit> disList = profitAnaService.findProfitDay(profit);
		profit.setFirmcode(oFirmcode);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
		//回调参数
		Map<String,String> params = new HashMap<String,String>();
		if(profit.getBdat() != null){
			params.put("bdat", DateFormat.getStringByDate(profit.getBdat(), "yyyy-MM-dd"));
		}
		if(profit.getBdat() != null){
			params.put("edat", DateFormat.getStringByDate(profit.getEdat(), "yyyy-MM-dd"));
		}
		params.put("firmcode", profit.getFirmcode());
		params.put("costby", profit.getCostby());
		params.put("sortName", profit.getSortName());
		params.put("sortOrder", profit.getSortOrder());
		modelMap.put("actionMap", params);
		//报表参数
		HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "毛利日趋势");
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", DateFormat.getStringByDate(profit.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(profit.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/profitAna/printProfitDay.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ProfitAnaConstants.REPORT_PRINT_URL_PROFITDAY,ProfitAnaConstants.REPORT_PRINT_URL_PROFITDAY);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
/**************************************************毛利日趋势end*********************************************/	
	
/**************************************************毛利月趋势start*********************************************/	
	/**
	 * 毛利月趋势
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/profitMonth")
	public ModelAndView profitMonth(ModelMap modelMap,Page page,Profit profit, HttpSession session,String action)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		if(null==action && !"init".equals(action)){
			String oFirmcode = profit.getFirmcode();
			profit.setFirmcode(CodeHelper.replaceCode(oFirmcode));
			modelMap.put("ListBean", JSONArray.fromObject(profitAnaService.findProfitMonth(profit, page)).toString());//table体list
			modelMap.put("ListBean_total", JSONArray.fromObject(profitAnaService.findProfitMonth_total(profit)).toString());//合计
			profit.setFirmcode(oFirmcode);
		}else{
			profit.setBdat(new Date());
			profit.setEdat(new Date());
			profit.setDept("QB");
			modelMap.put("ListBean", "123");//table体list
			modelMap.put("ListBean_total","123");//table总计体list
		}
		//加入列选择
		DictColumns dictColumns = new DictColumns();					
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ProfitAnaConstants.PROFIT_MONTH_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ProfitAnaConstants.PROFIT_MONTH_COLUMN));
		modelMap.put("pageobj", page);
		modelMap.put("profit", profit);
		return new ModelAndView(ProfitAnaConstants.LIST_PROFITMONTH, modelMap);
	}
	
	/**
	 * 导出毛利月趋势
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportProfitMonth")
	@ResponseBody
	public void exportProfitMonth(HttpServletResponse response,HttpServletRequest request,HttpSession session,Profit profit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		String fileName="毛利月趋势";
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ProfitAnaConstants.PROFIT_MONTH_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		profit.setFirmcode(CodeHelper.replaceCode(profit.getFirmcode()));
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcel.creatWorkBook(response.getOutputStream(),profitAnaService.findProfitMonth(profit), "毛利月趋势", 
				dictColumnsService.listDictColumnsByAccount(dictColumns,ProfitAnaConstants.PROFIT_MONTH_COLUMN));
	}
	
	/**
	 * 打印毛利月趋势报表
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printProfitMonth")
	public ModelAndView printProfitMonth(ModelMap modelMap, HttpSession session,String type,Profit profit)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		Map<String,String> params = new HashMap<String,String>();
		profit.setFirmcode(CodeHelper.replaceCode(profit.getFirmcode()));
		List<Profit> disList = profitAnaService.findProfitMonth(profit);
		int num=disList.size()%19;
		for (int i = 0; i < 19-num; i++) {
			disList.add(null);
		}
		modelMap.put("List",disList);//list
		if(profit.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(profit.getBdat(), "yyyy-MM-dd"));
		}
		if(profit.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(profit.getEdat(), "yyyy-MM-dd"));
		}
		params.put("firmcode", profit.getFirmcode());
		params.put("dept", profit.getDept());
		params.put("sortName", profit.getSortName());
		params.put("sortOrder", profit.getSortOrder());
		
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "毛利月趋势");
	    modelMap.put("actionMap", params);
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    parameters.put("bdat", DateFormat.getStringByDate(profit.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(profit.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/profitAna/printProfitMonth.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,ProfitAnaConstants.REPORT_PRINT_URL_PROFITMONTH,ProfitAnaConstants.REPORT_PRINT_URL_PROFITMONTH);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
/**************************************************毛利月趋势end*********************************************/	

/**************************************************分店日毛利分析start*********************************************/	
	/**
	 * 分店日毛利分析
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/firmDayProfit")
	public ModelAndView firmDayProfit(ModelMap modelMap,HttpSession session,Page page,SupplyAcct supplyAcct,String firmDes,String firmDeptCode,String deptcheck,String action,String firmDeptDes)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("acct", session.getAttribute("ChoiceAcct").toString());
		condition.put("codes", CodeHelper.replaceCode(firmDeptCode));
		condition.put("deptcheck", deptcheck);
		condition.put("positnType", "1203");
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("SUPPLYACCT", "SUPPLYACCT");
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("SUPPLYACCT", "firmSUPPLYACCT");
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		if(null!=action && !"".equals(action) && "init".equals(action)){
			modelMap.put("bdat", new Date());
			modelMap.put("edat", new Date());
			modelMap.put("dept", "QB");
		}else{
			condition.put("supplyAcct", supplyAcct);
			modelMap.put("firmCode", supplyAcct.getFirm());
			modelMap.put("firmDes", firmDes);
			modelMap.put("dept", deptcheck);
			modelMap.put("bdat", supplyAcct.getBdat());
			modelMap.put("edat", supplyAcct.getEdat());
			modelMap.put("firmDeptCode", firmDeptCode);
			modelMap.put("firmDeptDes", firmDeptDes);
//			modelMap.put("firmDeptList", profitAnaService.findFirmDept(condition));
			modelMap.put("firmDeptList", positnService.findPositnByTypCode(condition));
			modelMap.put("deptList", JSONArray.fromObject(positnService.findPositnByTypCode(condition)));
			modelMap.put("dataList", JSONArray.fromObject(profitAnaService.findFirmProfitCmp(condition, page)));
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(ProfitAnaConstants.LIST_FIRM_DAYPROFIT, modelMap);
	}
	
	/**
	 * 分店日毛利分析导出Excel
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmDayProfit")
	@ResponseBody
	public void exportFirmDayProfit(HttpServletResponse response,HttpServletRequest request,HttpSession session,Profit profit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			profit.setSupplyacct("SUPPLYACCT");
			profit.setCostitem("COSTITEM");
		}else{
			profit.setSupplyacct("firmSUPPLYACCT");
			profit.setCostitem("firmCOSTITEM");
		}
		String fileName="分店日毛利分析";
		DictColumns dictColumns = new DictColumns();					//加入列选择
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ProfitAnaConstants.PROFIT_DAY_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		profit.setFirmcode(CodeHelper.replaceCode(profit.getFirmcode()));
		exportExcel.creatWorkBook(response.getOutputStream(),profitAnaService.findProfitDay(profit), "分店日毛利分析", 
				dictColumnsService.listDictColumnsByAccount(dictColumns,ProfitAnaConstants.PROFIT_DAY_COLUMN));
	}
/**************************************************分店日毛利分析end*********************************************/	

}
