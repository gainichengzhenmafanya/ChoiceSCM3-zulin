package com.choice.scm.web.controller.ana;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.common.servlet.Jdbconfig;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.SupplyAcct;
@Controller
@RequestMapping("MdChengbenDuibi")
public class MdChengbenDuibiController {

	@Autowired
	private Jdbconfig config;
	/**
	 * 跳转到门店成本对比报表页面
	 * @return
	 * @throws CRUDException 
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/toMdChengbenDuibi")
	public ModelAndView toChkinDetailS(ModelMap modelMap,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("supplyAcct", supplyAcct);
		return new ModelAndView(AnaConstants.REPORT_SHOW_MDCHENGBENDUIBI,modelMap);
	}
	
	/**
	 * 跳转到报表页面
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws JRException 
	 * @throws IOException 
	 */
	@RequestMapping("/toReport")
	@ResponseBody
	public Object toReport(ModelMap modelMap,SupplyAcct supplyAcct,HttpServletResponse response,HttpServletRequest request) throws CRUDException, JRException, ClassNotFoundException, SQLException, IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		Map<String, Object> params = new HashMap<String,Object>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",supplyAcct.getBdat());
		if(supplyAcct.getEdat() != null)
			params.put("edat",supplyAcct.getEdat());
		StringBuffer sb = new StringBuffer();
		if(null != supplyAcct.getPositn() && !"".equals(supplyAcct.getPositn()))
			sb.append(" and sa.positn in ("+CodeHelper.replaceCode(supplyAcct.getPositn())+") ");
		if(null != supplyAcct.getSp_code() && !"".equals(supplyAcct.getSp_code()))
			sb.append(" and sa.sp_code in ("+CodeHelper.replaceCode(supplyAcct.getSp_code())+") ");
		params.put("sql",sb.toString());
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "report/ana/MdChengbenDuibi";//jasper文件路径;
		InputStream inputStream = new FileInputStream(filePath+".jasper");  
		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, params, getConnection());  
	    JasperExportManager.exportReportToHtmlFile(jasperPrint, filePath+".html");  
	    return filePath+".html";
	}
	
	@RequestMapping("/toHtmlReport")
	public ModelAndView toHtmlReport(ModelMap modelMap,HttpServletResponse response,String reprotPath,HttpServletRequest request) throws CRUDException, JRException, ClassNotFoundException, SQLException, IOException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
		return new ModelAndView("view/report/" + reprotPath + ".html",modelMap);
	}
	
	/**
	 * Ireport数据源获取共用方法
	 * 描述：
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * author:spt
	 * 日期：2014-5-21
	 */
	public Connection getConnection() throws ClassNotFoundException, SQLException {  
        return config.getCurrentConnetion();
    }
	
}
