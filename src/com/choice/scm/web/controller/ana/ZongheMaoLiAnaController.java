package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.CostProfit;
import com.choice.scm.service.ana.ZongheMaoLiAnaService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * 月综合毛利分析、周综合毛利分析
 * @author lq
 *
 */
@Controller
@RequestMapping("zongheMaoLiAna")
public class ZongheMaoLiAnaController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ZongheMaoLiAnaService zongheMaoLiAnaService;
	@Autowired
	AccountPositnService accountPositnService;
	
	/********************************************月综合毛利分析 start****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFirmMonthMaoLiAnaHeaders")
	@ResponseBody
	public Object findfirmMonthMaoLiAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(AnaConstants.MONTHZHMAOLIANA_TABLE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.MONTHZHMAOLIANA_COLUMN));
		columns.put("frozenColumns", AnaConstants.FROZEN_MONTHZHMAOLIANA);
		return columns;
	}
	/**
	 * 跳转到月综合毛利分析报表页面
	 * @return
	 */
	@RequestMapping("/toFirmMonthMaoLiAna")
	public ModelAndView tofirmMonthMaoLiAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", AnaConstants.MONTHZHMAOLIANA_TABLE);
		return new ModelAndView(AnaConstants.LIST_MONTHZHMAOLIANA,modelMap);
	}
	/**
	 * 查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param costProfit
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findFirmMonthMaoLiAna")
	@ResponseBody
	public Object findfirmMonthMaoLiAna(ModelMap modelMap,HttpSession session,String page,String rows,CostProfit costProfit) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		ReportObject<Map<String,Object>> result = zongheMaoLiAnaService.findFirmMonthMaoLiAna(costProfit,pager);
		
		List<Map<String,Object>> rowsList = result.getRows();
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//结果集
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
		List<Map<String,Object>> monthList = zongheMaoLiAnaService.findMonthKeLiuEmpNo(costProfit);
		for(Map<String,Object> rowsMap : rowsList){
			for(Map<String,Object> monthMap : monthList){
				if(rowsMap.get("FIRMCODE").toString().equals(monthMap.get("FIRM").toString()) &&
						rowsMap.get("MONTHH").toString().equals(monthMap.get("MONTHH").toString().replace("0", ""))){
					rowsMap.put("KELIU", monthMap.get("PAX"));
					rowsMap.put("YUANGONGNUM", monthMap.get("EMP"));
					rowsMap.put("INAMT", monthMap.get("AMT"));
					rowsMap.put("COSTZGCRJ", (int)(Double.parseDouble(""+rowsMap.get("COSTZGC"))/Double.parseDouble(""+monthMap.get("EMP"))*100)/100);
					rowsMap.put("COSTRJSF", (int)(Double.parseDouble(""+rowsMap.get("COSTSF"))/Double.parseDouble(""+monthMap.get("EMP"))*100)/100);
				}
			}
			resultRowsList.add(rowsMap);
		}
		result.setRows(resultRowsList);
		return result;
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printFirmMonthMaoLiAna")
	public ModelAndView printfirmMonthMaoLiAna(ModelMap modelMap,Page pager,HttpSession session,String type,CostProfit costProfit)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setPageSize(Integer.MAX_VALUE);
		
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "月综合毛利分析");
	    
	    parameters.put("bdat",DateFormat.getStringByDate(costProfit.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(costProfit.getEdat(), "yyyy-MM-dd"));
	        
        modelMap.put("parameters", parameters);
        Map<String,Object> params = new HashMap<String,Object>();
		if(costProfit.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(costProfit.getBdat(), "yyyy-MM-dd"));
		}
		if(costProfit.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(costProfit.getEdat(), "yyyy-MM-dd"));
		}
		params.put("positn", costProfit.getPositn());
		modelMap.put("actionMap", params);
	 	modelMap.put("action", "/zongheMaoLiAna/printFirmMonthMaoLiAna.do");//传入回调路径
	 	modelMap.put("List",zongheMaoLiAnaService.findFirmMonthMaoLiAna(costProfit,pager).getRows());
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,AnaConstants.REPORT_PRINT_URL_MONTHZHMAOLIANA,AnaConstants.REPORT_PRINT_URL_MONTHZHMAOLIANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出月综合毛利分析报表
	 * @param response
	 * @param session
	 * @param costProfit
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmMonthMaoLiAna")
	@ResponseBody
	public void exportfirmMonthMaoLiAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,CostProfit costProfit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "月综合毛利分析";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(AnaConstants.MONTHZHMAOLIANA_TABLE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		List<DictColumns> list = dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.MONTHZHMAOLIANA_COLUMN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		
		ReportObject<Map<String,Object>> result = zongheMaoLiAnaService.findFirmMonthMaoLiAna(costProfit,pager);
		
		List<Map<String,Object>> rowsList = result.getRows();
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//结果集
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
		List<Map<String,Object>> monthList = zongheMaoLiAnaService.findMonthKeLiuEmpNo(costProfit);
		for(Map<String,Object> rowsMap : rowsList){
			for(Map<String,Object> monthMap : monthList){
				if(rowsMap.get("FIRMCODE").toString().equals(monthMap.get("FIRM").toString()) &&
						rowsMap.get("MONTHH").toString().equals(monthMap.get("MONTHH").toString().replace("0", ""))){
					rowsMap.put("KELIU", monthMap.get("PAX"));
					rowsMap.put("YUANGONGNUM", monthMap.get("EMP"));
					rowsMap.put("INAMT", monthMap.get("AMT"));
					rowsMap.put("COSTZGCRJ", (int)(Double.parseDouble(""+rowsMap.get("COSTZGC"))/Double.parseDouble(""+monthMap.get("EMP"))*100)/100);
					rowsMap.put("COSTRJSF", (int)(Double.parseDouble(""+rowsMap.get("COSTSF"))/Double.parseDouble(""+monthMap.get("EMP"))*100)/100);
				}
			}
			resultRowsList.add(rowsMap);
		}
		
		exportExcelMap.creatWorkBook(response.getOutputStream(), resultRowsList, "月综合毛利分析",list);
	}
	/********************************************月综合毛利分析 end****************************************************/
	
	/********************************************周综合毛利分析 start****************************************************/
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findFirmWeekMaoLiAnaHeaders")
	@ResponseBody
	public Object findfirmWeekMaoLiAnaHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(AnaConstants.WEEKZHMAOLIANA_TABLE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.WEEKZHMAOLIANA_COLUMN));
		columns.put("frozenColumns", AnaConstants.FROZEN_WEEKZHMAOLIANA);
		return columns;
	}
	
	/**
	 * 跳转到周综合毛利分析报表页面
	 * @return
	 */
	@RequestMapping("/toFirmWeekMaoLiAna")
	public ModelAndView tofirmWeekMaoLiAna(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", AnaConstants.WEEKZHMAOLIANA_TABLE);
		return new ModelAndView(AnaConstants.LIST_WEEKZHMAOLIANA,modelMap);
	}
	
	/**
	 * 查询报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param costProfit
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findFirmWeekMaoLiAna")
	@ResponseBody
	public Object findfirmWeekMaoLiAna(ModelMap modelMap,HttpSession session,String page,String rows,CostProfit costProfit) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		ReportObject<Map<String,Object>> result = zongheMaoLiAnaService.findFirmWeekMaoLiAna(costProfit,pager);
		
		List<Map<String,Object>> rowsList = result.getRows();
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//结果集
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
		List<Map<String,Object>> weekList = zongheMaoLiAnaService.findWeekKeLiuEmpNo(costProfit);
		for(Map<String,Object> rowsMap : rowsList){
			for(Map<String,Object> weekMap : weekList){
				if(rowsMap.get("FIRMCODE").toString().equals(weekMap.get("FIRM").toString()) &&
						rowsMap.get("WEEKNO").toString().equals(""+DateFormat.getYear(costProfit.getBdat())+weekMap.get("WEEKNO").toString())){
					rowsMap.put("KELIU", weekMap.get("PAX"));
					rowsMap.put("YUANGONGNUM", weekMap.get("EMP"));
					rowsMap.put("INAMT", weekMap.get("AMT"));
					rowsMap.put("COSTZGCRJ", (int)(Double.parseDouble(""+rowsMap.get("COSTZGC"))/Double.parseDouble(""+weekMap.get("EMP"))*100)/100);
					rowsMap.put("COSTRJSF", (int)(Double.parseDouble(""+rowsMap.get("COSTSF"))/Double.parseDouble(""+weekMap.get("EMP"))*100)/100);
				}
			}
			resultRowsList.add(rowsMap);
		}
		result.setRows(resultRowsList);
		return result;
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printFirmWeekMaoLiAna")
	public ModelAndView printfirmWeekMaoLiAna(ModelMap modelMap,Page pager,HttpSession session,String type,CostProfit costProfit)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setPageSize(Integer.MAX_VALUE);
		
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "周综合毛利分析");
	    parameters.put("bdat",DateFormat.getStringByDate(costProfit.getBdat(), "yyyy-MM-dd"));
	    parameters.put("edat", DateFormat.getStringByDate(costProfit.getEdat(), "yyyy-MM-dd"));
        modelMap.put("parameters", parameters);
        Map<String,Object> params = new HashMap<String,Object>();
		if(costProfit.getBdat() != null){
			params.put("bdat",DateFormat.getStringByDate(costProfit.getBdat(), "yyyy-MM-dd"));
		}
		if(costProfit.getBdat() != null){
			params.put("edat",DateFormat.getStringByDate(costProfit.getEdat(), "yyyy-MM-dd"));
		}
		params.put("positn", costProfit.getPositn());
		modelMap.put("actionMap", params);
	 	modelMap.put("action", "/zongheMaoLiAna/printFirmWeekMaoLiAna.do");//传入回调路径
	 	modelMap.put("List",zongheMaoLiAnaService.findFirmWeekMaoLiAna(costProfit,pager).getRows());
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,AnaConstants.REPORT_PRINT_URL_WEEKZHMAOLIANA,AnaConstants.REPORT_PRINT_URL_WEEKZHMAOLIANA);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 导出周综合毛利分析报表
	 * @param response
	 * @param session
	 * @param costProfit
	 * @throws Exception
	 */
	@RequestMapping("/exportFirmWeekMaoLiAna")
	@ResponseBody
	public void exportfirmWeekMaoLiAna(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,CostProfit costProfit) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "周综合毛利分析";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(AnaConstants.WEEKZHMAOLIANA_TABLE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		List<DictColumns> list = dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.WEEKZHMAOLIANA_COLUMN);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		
		ReportObject<Map<String,Object>> result = zongheMaoLiAnaService.findFirmWeekMaoLiAna(costProfit,pager);
		
		List<Map<String,Object>> rowsList = result.getRows();
		
		List<Map<String,Object>> resultRowsList = new ArrayList<Map<String,Object>>();//结果集
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
		List<Map<String,Object>> weekList = zongheMaoLiAnaService.findWeekKeLiuEmpNo(costProfit);
		for(Map<String,Object> rowsMap : rowsList){
			for(Map<String,Object> weekMap : weekList){
				if(rowsMap.get("FIRMCODE").toString().equals(weekMap.get("FIRM").toString()) &&
						rowsMap.get("WEEKNO").toString().equals(""+DateFormat.getYear(costProfit.getBdat())+weekMap.get("WEEKNO").toString())){
					rowsMap.put("KELIU", weekMap.get("PAX"));
					rowsMap.put("YUANGONGNUM", weekMap.get("EMP"));
					rowsMap.put("INAMT", weekMap.get("AMT"));
					rowsMap.put("COSTZGCRJ", (int)(Double.parseDouble(""+rowsMap.get("COSTZGC"))/Double.parseDouble(""+weekMap.get("EMP"))*100)/100);
					rowsMap.put("COSTRJSF", (int)(Double.parseDouble(""+rowsMap.get("COSTSF"))/Double.parseDouble(""+weekMap.get("EMP"))*100)/100);
				}
			}
			resultRowsList.add(rowsMap);
		}
		
		exportExcelMap.creatWorkBook(response.getOutputStream(), resultRowsList, "周综合毛利分析", list);
		
	}
	/********************************************周综合毛利分析 end****************************************************/
}
