package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.service.ana.WzWanyuanyongliangFenxiService;
import com.choice.scm.util.ExportExcel;

/**
 * 物资 万元用量分析
 * @author wjf
 *
 */
@Controller
@RequestMapping("WzWanyuanyongliangFenxi")
public class WzWanyuanyongliangFenxiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private WzWanyuanyongliangFenxiService wzWanyuanyongliangFenxiService;
	
	/***********************************************************物资万元用量分析start**************************************************/
	/**
	 * 跳转物资万元用量分析
	 */
	@RequestMapping("/toWzWanyuanyongliangFenxi")
	public ModelAndView toWzWanyuanyongliangFenxi(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", AnaConstants.REPORT_NAME_WZWANYUANYONGLIANGFENXI);
		return new ModelAndView(AnaConstants.REPORT_SHOW_WZWANYUANYONGLIANGFENXI,modelMap);
	}
	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findWzWanyuanyongliangFenxiHeaders")
	@ResponseBody	
	public Object findWzWanyuanyongliangFenxiHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(AnaConstants.REPORT_NAME_WZWANYUANYONGLIANGFENXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_WZWANYUANYONGLIANGFENXI));
		columns.put("frozenColumns", AnaConstants.BASICINFO_REPORT_WZWANYUANYONGLIANGFENXI_FROZEN);
		return columns;
	}
	
	/**
	 * 统计物资万元用量分析
	 */
	@RequestMapping("/calWzWanyuanyongliangFenxi")
	@ResponseBody
	public String calWzWanyuanyongliangFenxi(PtivityAna ptivity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return wzWanyuanyongliangFenxiService.calWzWanyuanyongliangFenxi(ptivity);
	}
	
	/**
	 * 查询物资万元用量分析
	 */
	@RequestMapping("/findWzWanyuanyongliangFenxi")
	@ResponseBody
	public Object findWzWanyuanyongliangFenxi(ModelMap modelMap, HttpSession session, String page, String rows, PtivityAna ptivity) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		ptivity.setAcct(session.getAttribute("ChoiceAcct").toString());
		return wzWanyuanyongliangFenxiService.findWzWanyuanyongliangFenxi(ptivity, pager);
	}
	
	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportWzWanyuanyongliangFenxi")
	@ResponseBody
	public void exportWzWanyuanyongliangFenxi(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,String rows,PtivityAna ptivity) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资万元用量分析表";
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(AnaConstants.REPORT_NAME_WZWANYUANYONGLIANGFENXI);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		ptivity.setAcct(session.getAttribute("ChoiceAcct").toString());
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzWanyuanyongliangFenxiService.findWzWanyuanyongliangFenxi(ptivity,pager).getRows(), 
				"物资万元用量分析表", dictColumnsService.listDictColumnsByAccount(dictColumns, AnaConstants.BASICINFO_REPORT_WZWANYUANYONGLIANGFENXI));	
	}

}
