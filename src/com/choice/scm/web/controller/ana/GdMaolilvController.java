package com.choice.scm.web.controller.ana;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.GdMaolilvService;
import com.choice.scm.util.PublicExportExcel;
/***
 * 各店毛利率
 * @author wjf
 *
 */
@Controller
@RequestMapping("gdMaolilv")
public class GdMaolilvController {

	@Autowired
	private Page pager;
	@Autowired
	private GdMaolilvService gdMaolilvService;
	@Autowired
    private AcctService acctService;
	
	/********************************************各店毛利率报表****************************************************/
    
    /**
	 * 跳转到各店毛利率报表页面
	 * @return
	 */
	@RequestMapping("/toGdMaolilv")
	public ModelAndView toGdMaolilv(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_GDMAOLILV,modelMap);
	}
	
	/**
	 * 查询表头信息(动态显示表头)
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/findGdMaolilvHeaders")
	@ResponseBody
	public List<WeekSet> findGdMaolilvHeaders(SupplyAcct sa) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(sa.getBdat() == null || sa.getEdat() == null){
			Date nowDate = new Date();
			nowDate = DateFormat.formatDate(nowDate, "yyyy-MM-dd");
			sa.setBdat(nowDate);
			sa.setEdat(nowDate);
			sa.setShowtyp(2);
		}
		return gdMaolilvService.findWeekSet(sa);
	}
	
	/***
	 * 查询各店毛利率
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/findGdMaolilv")
	@ResponseBody
	public Object findGdMaolilv(ModelMap modelMap, HttpSession session, String page, String rows, String sort, String order,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		condition.put("sort",sort);
		condition.put("order", order);
		supplyAcct.setAcct(choiceAcct);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return gdMaolilvService.findGdMaolilv(findGdMaolilvHeaders(supplyAcct),condition,pager);
	}
	
	/**
	 * 导出月成本综合分析报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportGdMaolilv")
	@ResponseBody
	public void exportGdMaolilv(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		String choiceAcct = session.getAttribute("ChoiceAcct").toString();
		supplyAcct.setAcct(choiceAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		condition.put("supplyAcct", supplyAcct);
		Acct acct = acctService.findAcctById(choiceAcct);
		condition.put("acctDes", acct.getDes());
		List<WeekSet> wsList = findGdMaolilvHeaders(supplyAcct);
		List<Map<String,String>> headerList = new ArrayList<Map<String,String>>();
		Map<String,String> positn = new HashMap<String,String>();
		positn.put("编码:::POSITN", "null");
		headerList.add(positn);
		Map<String,String> positndes = new HashMap<String,String>();
		positndes.put("名称:::POSITNDES", "null");
		headerList.add(positndes);
		for(WeekSet ws : wsList){
			Map<String,String> map = new HashMap<String,String>();
			if(null == ws.getDes() || "".equals(ws.getDes())){
				map.put(ws.getWeekk()+":::W_"+ws.getWeekk(), "null");
			}else{
				map.put(ws.getWeekk()+"("+ws.getDes()+"):::W_"+ws.getWeekk(), "null");
			}
			headerList.add(map);
		}
		List<Map<String,Object>> rows = gdMaolilvService.findGdMaolilv(wsList,condition,null).getRows();
		new PublicExportExcel().creatWorkBook(request, response, "各店毛利率", "各店毛利率", headerList, rows);
	}
}
