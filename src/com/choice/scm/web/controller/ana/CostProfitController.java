package com.choice.scm.web.controller.ana;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.CostProfitService;
import com.choice.scm.util.ExportExcel2;
/**
 * 分店毛利对比
 * @author lq
 *
 */
@Controller
@RequestMapping("costProfitAna")
public class CostProfitController {
	
	@Autowired
	private CostProfitService costProfitService;
	@Autowired
	AccountPositnService accountPositnService;
//	@Qualifier("scmExportExcel")
	@Autowired
	ExportExcel2 exportExcel;
	@Autowired
	AcctService acctService;
	
/**************************************************分店毛利对比start*********************************************/	
	
	@RequestMapping(value = "/firmProfitCmp")
	public ModelAndView firmProfitCmp(ModelMap modelMap,Page page,SupplyAcct supply,String dept,String firmDes,HttpSession session,String action)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("acct", session.getAttribute("ChoiceAcct").toString());
		condition.put("positnType", PositnConstants.type_5);
		condition.put("dept", dept);
		condition.put("codes", CodeHelper.replaceCode(supply.getPositn()));
		condition.put("bdat", supply.getBdat());
		condition.put("edat", supply.getEdat());
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		if(null==action && !"init".equals(action)){
			modelMap.put("bdat", supply.getBdat());
			modelMap.put("edat", supply.getEdat());
			modelMap.put("dept", dept);
			modelMap.put("firmDes", firmDes);
			modelMap.put("columns", costProfitService.findTableHead(condition));
			modelMap.put("ListBean", JSONArray.fromObject(costProfitService.findFirmProfitCmp(condition, page)).toString());//table体list
		}else{
			modelMap.put("bdat", new Date());
			modelMap.put("edat", new Date());
			modelMap.put("dept", "QB");
			modelMap.put("columns", null);
			modelMap.put("ListBean", "123");//table体list
		}
		modelMap.put("pageobj", page);
		modelMap.put("supply", supply);
		return new ModelAndView(AnaConstants.LIST_FIRMPROFITCMP, modelMap);
	}
	
	
	/**
	 * 导出
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/exportGrossProfit")
	public void exportGrossProfit(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap,Page page,SupplyAcct supply,String dept,HttpSession session,String type,String headers)throws Exception{ 
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("acct", session.getAttribute("ChoiceAcct").toString());
		condition.put("positnType", PositnConstants.type_5);
		condition.put("dept", dept);
		condition.put("codes", supply.getFirmcode());
		condition.put("bdat", supply.getBdat());
		condition.put("edat", supply.getEdat());
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("COSTITEM", "COSTITEM");
		}else{
			condition.put("COSTITEM", "firmCOSTITEM");
		}
		page.setPageSize(Integer.MAX_VALUE);
		List<Map<String,Object>> a=costProfitService.findFirmProfitCmp(condition, page);
		if (a.size()==0){
			exportExcel.creatWorkBook(request, response, null,"分店成本毛利对比", null, headers);
		}else{
			exportExcel.creatWorkBook(request, response, a,"分店成本毛利对比", null, headers);
		}
		
//		exportExcelMap.creatWorkBook(response.getOutputStream(), costProfitService.findFirmProfitCmp(condition, page).getRows(), "物资成本汇总表", dictColumnsService.listDictColumnsByTable(dictColumns));
	}
//	@RequestMapping(value = "/exportGrossProfit")
//	public boolean exportGrossProfit(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap,Page page,SupplyAcct supply,String dept,HttpSession session,String type,String headers)throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		response.setContentType("application/msexcel; charset=UTF-8");
//		String fileName = "分店毛利对比";
//		Map<String,Object> condition = new HashMap<String, Object>();
//		condition.put("acct", session.getAttribute("ChoiceAcct").toString());
//		condition.put("positnType", PositnConstants.type_5);
//		condition.put("dept", dept);
//		condition.put("codes", supply.getFirmcode());
//		condition.put("bdat", supply.getBdat());
//		condition.put("edat", supply.getEdat());
//		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
//			condition.put("COSTITEM", "COSTITEM");
//		}else{
//			condition.put("COSTITEM", "firmCOSTITEM");
//		}
//		List<Map<String,Object>> exportList = costProfitService.findFirmProfitCmp(condition,page);
//		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
//		    //IE  
//		    fileName = URLEncoder.encode(fileName, "UTF-8");              
//		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
//		    //firefox  
//		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
//		}else{                
//		    // other          
//		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
//		}   
//		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
//		return true;
//		return costProfitService.exportExcel(response.getOutputStream(), exportList, costProfit);
		//page.setPageSize(Integer.MAX_VALUE);		
		//exportExcel.creatWorkBook(request, response, costProfit.setPositn(CodeHelper.replaceCode(costProfit.getPositn()));.findFirmDangKouProfit(costProfit, page),"分店档口毛利", null, headers);
//	}
	
	/**************************************************分店毛利对比end*********************************************/	

}
