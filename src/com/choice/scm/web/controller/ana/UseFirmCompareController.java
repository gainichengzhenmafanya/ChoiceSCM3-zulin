package com.choice.scm.web.controller.ana;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PositnConstants;
import com.choice.scm.constants.ana.UseFirmCompareConstants;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.UseFirmService;

@Controller
@RequestMapping("useFirmCompare")
public class UseFirmCompareController {

	@Autowired
	private UseFirmService useFirmService;
	@Autowired
	AcctService acctService;
	
	/**
	 * 耗用分店对比页面
	 * @param modelMap
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toUseFirmCompare")
	public ModelAndView toFirmUseCompare(ModelMap modelMap,HttpSession session,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("pageobj", page);
		modelMap.put("columns", null);
		modelMap.put("ListBean", "123");//table体list
		return new ModelAndView(UseFirmCompareConstants.LIST_USEFIRMCOMPARE,modelMap);
	}
	/**
	 * 耗用分店数据查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findUseFirmCompare")
	public ModelAndView findUseFirmCompare(ModelMap modelMap,HttpSession session,Page page,String firmCode,String firmDes,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		condition.put("codes", CodeHelper.replaceCode(firmCode));
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("positnType", PositnConstants.type_5);
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			condition.put("SUPPLYACCT", "SUPPLYACCT");
		}else{
			condition.put("SUPPLYACCT", "firmSUPPLYACCT");
		}
		modelMap.put("pageobj", page);
		modelMap.put("firmCode", firmCode);
		modelMap.put("firmDes", firmDes);
		modelMap.put("bdat", supplyAcct.getBdat());
		modelMap.put("edat", supplyAcct.getEdat());
		modelMap.put("supplyAcct", supplyAcct);
		modelMap.put("columns", useFirmService.findTableHead(condition));
//		condition.put("codes", firmCode);
		modelMap.put("ListBean", JSONArray.fromObject(useFirmService.findUseFirm(condition, page)).toString());//table体list
		//将类别中的代码单引号去掉
//		supplyAcct.setGrptyp(supplyAcct.getGrptyp().replace("'", ""));
//		supplyAcct.setGrp(supplyAcct.getGrp().replace("'", ""));
//		supplyAcct.setTyp(supplyAcct.getTyp().replace("'", ""));
		return new ModelAndView(UseFirmCompareConstants.LIST_USEFIRMCOMPARE,modelMap);
	}
}
