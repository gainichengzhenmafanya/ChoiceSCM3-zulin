package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ana.AnaConstants;
import com.choice.scm.domain.ana.CostProfit;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.FirmDangKouProfitService;
import com.choice.scm.util.ExportExcel2;
/**
 * 分店档口毛利
 * @author lq
 *
 */
@Controller
@RequestMapping("firmDangKouProfit")
public class FirmDangKouProfitController {
	
	@Autowired
	private FirmDangKouProfitService firmDangKouProfitService;
	@Qualifier("scmExportExcel")
	ExportExcel2 exportExcel;
	@Autowired
	AcctService acctService;
	
/**************************************************分店档口毛利*********************************************/	
	
	@RequestMapping(value = "/findDangKouProfit")
	public ModelAndView findDangKouProfit(ModelMap modelMap,Page page,CostProfit costProfit,HttpSession session,String action)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null==action && !"init".equals(action)){
            modelMap.put("costProfit", costProfit);
			modelMap.put("columns", firmDangKouProfitService.findTableHead_storedept(costProfit));
			if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
				costProfit.setCostitem("COSTITEM");
			}else{
				costProfit.setCostitem("firmCOSTITEM");
			}
			modelMap.put("ListBean", JSONArray.fromObject(firmDangKouProfitService.findFirmDangKouProfit(costProfit, page)).toString());//table体list
		}else{
			costProfit.setBdat(new Date());
			costProfit.setEdat(new Date());
			costProfit.setDept("QB");
			modelMap.put("costProfit", costProfit);
			modelMap.put("columns", null);
			modelMap.put("ListBean", "123");//table体list
		}
		modelMap.put("pageobj", page);
		return new ModelAndView(AnaConstants.LIST_FIRMDANGKOUPROFIT, modelMap);
	}
	
	/**
	 * 导出
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/exportFirmDangKouProfit")
	public void exportFirmDangKouProfit(HttpServletRequest request,HttpServletResponse response,ModelMap modelMap,Page page,CostProfit costProfit,String firmDes,HttpSession session,String headers)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		response.setContentType("application/msexcel; charset=UTF-8");
		String fileName = "分店档口毛利";
		if ("N".equals(acctService.findYnkcFromAcct(session.getAttribute("ChoiceAcct").toString()).getYnkc())) {
			costProfit.setCostitem("COSTITEM");
		}else{
			costProfit.setCostitem("firmCOSTITEM");
		}
		List<Map<String,Object>> exportList = firmDangKouProfitService.findFirmDangKouProfit(costProfit,page);
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){              
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");		
		firmDangKouProfitService.exportExcel(response.getOutputStream(), exportList, costProfit);
	}
	
	
	/**************************************************分店档口毛利*********************************************/	

}
