package com.choice.scm.web.controller.ana;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PrdPrcCMConstant;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ana.WzChengbenChayiService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;

/**
 * 物资成本差异
 * @author wjf
 *
 */
@Controller
@RequestMapping("wzChengbenChayi")
public class WzChengbenChayiController {
	
	@Autowired
	private Page pager;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private WzChengbenChayiService wzChengbenChayiService;
	
	
	/****************************************************物资成本差异 start***************************************************/
	/**
	 * 跳转到物资成本差异页面
	 */
	@RequestMapping("/toCostVariance")
	public ModelAndView toCostVariance(ModelMap modelMap,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("bdat", new Date());
		modelMap.put("edat", new Date());
		modelMap.put("reportName", PrdPrcCMConstant.BASICINFO_REPORT_COSTVARIANCECHOICE3);
		return new ModelAndView(PrdPrcCMConstant.LIST_COST_VARIANCECHOICE3,modelMap);
	}
	
	/**
	 * 查询表头信息
	 */
	@RequestMapping("/findCostVarianceHeaders")
	@ResponseBody	
	public Object getCostVarianceHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(PrdPrcCMConstant.REPORT_NAME_COSTVARIANCECHOICE3);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COSTVARIANCECHOICE3));
		columns.put("frozenColumns", PrdPrcCMConstant.BASICINFO_REPORT_COSTVARIANCECHOICE3_FROZEN);
		return columns;
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseCostVariance")
	public ModelAndView toColChooseCostVariance(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(PrdPrcCMConstant.REPORT_NAME_COSTVARIANCECHOICE3);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", PrdPrcCMConstant.REPORT_NAME_COSTVARIANCECHOICE3);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,PrdPrcCMConstant.BASICINFO_REPORT_COSTVARIANCECHOICE3));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(PrdPrcCMConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	
	/**
	 * 查询物资成本差异
	 */
	@RequestMapping("/findCostVariance")
	@ResponseBody
	public Object findCostVariance(ModelMap modelMap,HttpSession session,String page,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order",order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		content.put("accountId", session.getAttribute("accountId").toString());
		return wzChengbenChayiService.findWzChengbenChayiPrc(content, pager);
	}

	/**
	 * 导出
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportCostVariance")
	@ResponseBody
	public void exportCostVariance(HttpServletResponse response,String sort,String order,HttpServletRequest request,HttpSession session,String page,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资成本差异";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(PrdPrcCMConstant.REPORT_NAME_COSTVARIANCECHOICE3);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
		//导出标志
		condition.put("excel", true);
		condition.put("accountId", session.getAttribute("accountId").toString());
		exportExcelMap.creatWorkBook(response.getOutputStream(), wzChengbenChayiService.findCostVarianceChoice3(condition,pager).getRows(), "物资成本差异", dictColumnsService.listDictColumnsByAccount(dictColumns, PrdPrcCMConstant.BASICINFO_REPORT_COSTVARIANCECHOICE3));	
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param month
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/printCostVariance")
	public ModelAndView printCostVariance(ModelMap modelMap,Page pager,HttpSession session,String type,String page,String rows,String sort,String order,SupplyAcct supplyAcct)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,Object> params = new HashMap<String,Object>();
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
		condition.put("supplyAcct", supplyAcct);
		params.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		params.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		params.put("sp_code", supplyAcct.getSp_code());
		params.put("grptyp", supplyAcct.getGrptyp());
		params.put("grp", supplyAcct.getGrp());
		params.put("typ", supplyAcct.getTyp());
		//时间
		parameters.put("bdat", DateFormat.getStringByDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		parameters.put("edat", DateFormat.getStringByDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		//用户
		condition.put("accountId", session.getAttribute("accountId").toString());
		//导出标志
		condition.put("excel", true);
		modelMap.put("List",wzChengbenChayiService.findCostVarianceChoice3(condition, pager).getRows());
	    parameters.put("report_name", "物资成本差异");
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	    modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/wzChengbenChayi/printCostVariance.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PrdPrcCMConstant.REPORT_PRINT_URL_COST_VARIANCECHOICE3,PrdPrcCMConstant.REPORT_EXP_URL_COST_VARIANCECHOICE3);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}		
	/****************************************************物资成本差异 end***************************************************/
	
}
