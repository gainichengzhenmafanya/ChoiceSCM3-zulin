/*package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.EasConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.EasService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.SupplyService;

*//**
 * 分店管理
 * @author css
 *
 *//*
@Controller
@RequestMapping("eas")
public class EasController {

	@Autowired
	EasService easService;
	@Autowired
	CodeDesService codeDesService;
	@Autowired
	SupplyService supplyService;
	@Autowired
	GrpTypService grpTypService;
	
	*//**
	 * 模糊查询分店和仓位
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return modelMap
	 * @throws Exception
	 * @author yp
	 *//*
	@RequestMapping("/list")
	public ModelAndView listEas(ModelMap modelMap, Positn positn, Page page) throws Exception{
		return new ModelAndView(EasConstants.LIST_EAS,modelMap);
	}
	
	*//**
	 * 分店仓位选择查询
	 * @param modelMap
	 * @param positn
	 * @return
	 * @throws Exception
	 * @author css
	 *//*
	@RequestMapping("/updateToEas")
	public ModelAndView choosePositn(ModelMap modelMap, String code, HttpSession session) throws Exception{
		String[] cds = code.split(",");
		for(int i=0;i<cds.length;i++){
			//物资同步
			if ("supply".equals(cds[i])) {
				String acct=session.getAttribute("ChoiceAcct").toString();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				List<Supply> listSupply = easService.findAllSupply();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//				supplyService.deleteAllSupply();
				for (int j=0;j<listSupply.size();j++) {
					Supply supply = new Supply();
					supply.setSp_code(listSupply.get(j).getSp_code());
					if (null==supplyService.findSupplyById(supply)) {
						listSupply.get(j).setAcct(acct);
						supplyService.saveSupply(listSupply.get(j));
					}else{//存在则修改
						listSupply.get(j).setAcct(acct);
						easService.updateSupply(listSupply.get(j));
					}
				}
			}
			
			//物资大类同步
			if ("grptyp".equals(cds[i])) {
				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				List<GrpTyp> listGrpTyp = easService.findAllGrpTyp();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				easService.deleteAllGrpTyp();
				for (int j=0;j<listGrpTyp.size();j++) {
					grpTypService.saveGrpTyp(listGrpTyp.get(j).getDes(), null, null, 0,
							listGrpTyp.get(j).getCode(), 
							session.getAttribute("ChoiceAcct").toString(),null,"N",null);//wangjie
				}
			}
			
			//物资中类同步
			if ("grp".equals(cds[i])) {
				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				List<Grp> listGrp = easService.findAllGrp();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				easService.deleteAllGrp();
				for (int j=0;j<listGrp.size();j++) {
					grpTypService.saveGrpTyp(listGrp.get(j).getDes(), null, 
							listGrp.get(j).getGrptyp(), 1, listGrp.get(j).getCode(), 
							session.getAttribute("ChoiceAcct").toString(),null,"N",null);//wangjie
				}
			}
			//物资小类同步
			if ("typ".equals(cds[i])) {
				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				List<Typ> listTyp = easService.findAllTyp();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				easService.deleteAllTyp();
				for (int j=0;j<listTyp.size();j++) {
					grpTypService.saveGrpTyp(listTyp.get(j).getDes(), listTyp.get(j).getGrp(), 
							listTyp.get(j).getGrptyp(), 2, listTyp.get(j).getCode(), 
							session.getAttribute("ChoiceAcct").toString(),null,"N",null);//wangjie
				}
			}
			
			//报货分类同步
			if ("bhtyp".equals(cds[i])) {
				DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
				List<CodeDes> listCodeDes = easService.findAllBhTyp();
				DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
				CodeDes codeDes = new CodeDes();
				codeDes.setTyp("11");
				easService.deleteAllBhTyp(codeDes);
				for (int j=0;j<listCodeDes.size();j++) {
					listCodeDes.get(j).setTyp("11");
					easService.saveCodeDes(listCodeDes.get(j));
				}
			}
		}
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
}
*/