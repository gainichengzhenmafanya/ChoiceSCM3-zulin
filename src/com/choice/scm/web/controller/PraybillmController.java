package com.choice.scm.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.PraybillmConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Praybilld;
import com.choice.scm.domain.Praybillm;
import com.choice.scm.service.PositnRoleService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.PraybilldService;
import com.choice.scm.service.PraybillmService;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.ReadReportUrl;
/**
 * 请购单新增、修改、删除、查询、打印、审核
 * @author 王吉峰
 *
 */
@Controller
@RequestMapping(value = "praybillm")

public class PraybillmController {

	@Autowired
	private PositnService positnService;
	@Autowired
	private PositnRoleService positnRoleService;
	@Autowired
	private CalChkNum calChkNum;
	
	@Autowired
	private PraybillmService praybillmService;
	@Autowired
	private PraybilldService praybilldService;
	
	/***
	 * 查询所有未审核请购单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView table(ModelMap modelMap,Page page,HttpSession session,String tableFrom) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("tableFrom", tableFrom);
		//请购单主页面
		return new ModelAndView(PraybillmConstants.TABLE_PRAYBILLM);
	}
	
	/***
	 * 添加操作时候，刷新页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addPraybillm")
	public ModelAndView addPraybillm(ModelMap modelMap,Page page,HttpSession session,Praybillm praybillm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前登录用户
		String accountName=session.getAttribute("accountName").toString();
		praybillm.setMadeby(accountName);
		praybillm.setMaded(new Date());
		//获取最大单号
		praybillm.setPraybillmNo(praybillmService.getMaxPraybillmno());
		String acct=session.getAttribute("ChoiceAcct").toString();
		String accountId=session.getAttribute("accountId").toString();
		modelMap.put("positnList", positnRoleService.findAllPositn(acct, accountId));//带有权限控制的仓位
		modelMap.put("praybillm", praybillm);
		modelMap.put("sta", "add");
		return new ModelAndView(PraybillmConstants.TABLE_PRAYBILLM,modelMap);
	}	
	
	/**
	 * 新增或修改保存
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap,Page page,HttpSession session,String sta,Praybillm praybillm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//当前帐套
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if("add".equals(sta)){
			praybillm.setVouno(calChkNum.getNextBytable("PRAYBILLM","QG"+praybillm.getFirm()+"-",new Date()));
		}else{
			Praybillm praybillm1 = praybillmService.findByPraybillmNo(praybillm);
			praybillm.setVouno(praybillm1.getVouno());
		}
		
		return praybillmService.saveOrUpdateChk(praybillm, sta);
	}
	
	/**
	 * 查找请购单
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchByKey")
	public ModelAndView searchByKey(ModelMap modelMap,HttpSession session,Page page,String sp_code,Praybillm praybillm, String firmDes) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		HashMap<String, Object> map=new HashMap<String, Object>();
		if(praybillm.getBdate() == null){
			praybillm.setBdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		if(praybillm.getEdate() == null){
			praybillm.setEdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		map.put("praybillm", praybillm);
		map.put("sp_code", sp_code);
		//关键字查询
		modelMap.put("praybillmList", praybillmService.findByKey(map,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("praybillm", praybillm);
		modelMap.put("sp_code", sp_code);
		modelMap.put("firmDes", firmDes);
		modelMap.put("pageobj", page);
		return new ModelAndView(PraybillmConstants.SEARCH_PRAYBILLM,modelMap);
	}
	
	/**
	 * 双击查找
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findPraybill")
	public ModelAndView findPraybill(ModelMap modelMap,HttpSession session,Page page,Praybilld praybilld,Praybillm praybillm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		praybilld.setAcct(session.getAttribute("ChoiceAcct").toString());
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("sta", "show");
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("praybillm", praybillmService.findByPraybillmNo(praybillm));
		modelMap.put("praybilldList", praybilldService.findByPraybillmNo(praybilld));
		return new ModelAndView(PraybillmConstants.TABLE_PRAYBILLM,modelMap);
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "/deletePraybillm")
	@ResponseBody
	public Object deletePraybillm(ModelMap modelMap,HttpSession session,Page page,String praybillmNos,Praybillm praybillm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//报货单填制页面上的整条删除
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		return praybillmService.deletePraybillm(praybillm,praybillmNos);
	}
	
	/**
	 * 检查是加盟商金额是否充足
	 */
	@RequestMapping(value = "/checkSppriceSale")
	@ResponseBody
	public Object checkSppriceSale(ModelMap modelMap,Page page,HttpSession session,Positn positn,int praybillmNo) throws Exception {
		return 0;
	}
	
	/**
	 * 审核
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/checkPraybillm")
	@ResponseBody
	public Object checkPraybillm(ModelMap modelMap,Page page,HttpSession session,String praybillmNos,Praybillm praybillm) throws Exception {
		try {
			DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
			String accountName=session.getAttribute("accountName").toString();
			praybillm.setChecby(accountName);
			String result= praybillmService.checkChk(praybillm,praybillmNos);
			return  result;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找已审核请购单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedPraybillm")
	public ModelAndView listCheckedPraybillm(ModelMap modelMap,HttpSession session,Page page,String init,String sp_code,Praybillm praybillm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数，已审核报货单列表页面
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=init && ""!=init){
			praybillm.setBdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		//把参数放到MAP
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("checked", "checked");
		map.put("praybillm", praybillm);
		map.put("sp_code", sp_code);
		//把查询的结果集返回到页面
		modelMap.put("praybillmList", praybillmService.findByKey(map,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("praybillm", praybillm);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(PraybillmConstants.TABLE_CHECKED_PRAYBILLM,modelMap);
	}
	
	/**
	 * 查看已审核报货单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedPraybillm")
	public ModelAndView searchCheckedPraybillm(ModelMap modelMap,HttpSession session,Page page,Praybilld praybilld,Praybillm praybillm) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//关键字查询
		praybilld.setAcct(session.getAttribute("ChoiceAcct").toString());
		praybillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("praybillm", praybillmService.findByPraybillmNo(praybillm));
		modelMap.put("praybilldList", praybilldService.findByPraybillmNo(praybilld));
		return new ModelAndView(PraybillmConstants.SEARCH_CHECKED_PRAYBILLM,modelMap);
	}
	
	/**
	 * 请购单打印
	 * @param modelMap
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printPraybillm")
	public ModelAndView printPraybillm(ModelMap modelMap,HttpSession session,Page page,String type,Praybilld praybilld)throws CRUDException {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收参数，根据关键字查询收，进行结果集的打印
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		praybilld.setAcct(session.getAttribute("ChoiceAcct").toString());
		disMap.put("praybilld", praybilld);
		List<Praybilld> list=praybilldService.findAllPraybilld(praybilld);
 		HashMap<String,Object>  parameters = new HashMap<String,Object>(); 
        String report_name=new String("请购数据打印");
        String report_date=DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");      
        parameters.put("report_name", report_name); 
        parameters.put("report_date", report_date); 
 	    modelMap.put("List",list);  
        modelMap.put("parameters", parameters);     
        modelMap.put("actionMap", disMap);//回调参数
	 	modelMap.put("action", "/praybillm/printPraybillm.do?chkstoNo="+praybilld.getPraybillmNo());//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,PraybillmConstants.REPORT_PRINT_URL,PraybillmConstants.REPORT_PRINT_URL);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        //return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
}
