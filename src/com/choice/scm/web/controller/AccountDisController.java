package com.choice.scm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.constants.AccountDisConstants;
import com.choice.scm.domain.AccountDis;
import com.choice.scm.service.AccountDisService;

/***
 * 报货分拨用户权限
 * @author 王吉峰
 *
 */
@Controller
@RequestMapping("accountDis")
public class AccountDisController {
	
	@Autowired
	private AccountDisService accountDisService;

	/***
	 * 账号管理其他权限 wjf
	 * 
	 */
	@RequestMapping(value = "/otherPermission")
	public ModelAndView otherPermission(ModelMap modelMap,AccountDis accountDis) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountDis ad = accountDisService.getAccountDisByAccountId(accountDis.getAccountId());
		if(ad != null)
			accountDis = ad;
		modelMap.put("accountDis", accountDis);
		return new ModelAndView(AccountDisConstants.OTHER_PERMISSION,modelMap);
	}
	
	/***
	 * 保存其他权限
	 * @return
	 */
	@RequestMapping(value = "/ajaxSaveOtherPermission")
	@ResponseBody
	public String ajaxSaveOtherPermission(AccountDis accountDis) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return accountDisService.saveOtherPermission(accountDis);
	}
}
