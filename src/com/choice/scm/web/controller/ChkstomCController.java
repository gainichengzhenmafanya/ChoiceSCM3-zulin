package com.choice.scm.web.controller;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkstomCConstants;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.service.ChkstodCService;
import com.choice.scm.service.ChkstomCService;
import com.choice.scm.service.PositnService;

/**
 * 拆分报货单查询
 * @author css
 *
 */
@Controller
@RequestMapping(value = "chkstom_c")

public class ChkstomCController {

	@Autowired
	private ChkstomCService chkstomCService;
	@Autowired
	private ChkstodCService chkstodCService;
	@Autowired
	private PositnService positnService;
	
	/**
	 * 查找已审核报货单
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listCheckedChkstom")
	public ModelAndView listCheckedChkstom(ModelMap modelMap, HttpSession session, Page page, String init, String sp_code, Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//接收前台参数，已审核报货单列表页面
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=init && ""!=init){
			chkstom.setbMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			chkstom.seteMaded(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		//把参数放到MAP
		HashMap<String, Object> chkstomMap=new HashMap<String, Object>();
		chkstomMap.put("checked", "checked");
		chkstomMap.put("chkstom", chkstom);
		chkstomMap.put("sp_code", sp_code);
		//把查询的结果集返回到页面
		modelMap.put("chkstomList", chkstomCService.findByKey(chkstomMap,page));
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstom);
		modelMap.put("sp_code", sp_code);
		modelMap.put("pageobj", page);
		return new ModelAndView(ChkstomCConstants.TABLE_CHECKED_CHKSTOM,modelMap);
	}

	/**
	 * 查看已审核报货单的信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchCheckedChkstom")
	public ModelAndView searchCheckedChkstom(ModelMap modelMap,HttpSession session,Page page,Chkstod chkstod,Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//关键字查询
		chkstod.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("positnList",positnService.findAllPositn(null));
		modelMap.put("chkstom", chkstomCService.findByChkstoNo(chkstom));
		modelMap.put("chkstodList", chkstodCService.findByChkstoNo(chkstod));
		return new ModelAndView(ChkstomCConstants.SEARCH_CHECKED_CHKSTOM,modelMap);
	}
}
