package com.choice.scm.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.service.DictService;
import com.choice.scm.constants.SpCodeUseConstants;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.PosItemPlan;
import com.choice.scm.domain.PosSalePlan;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ScheduleStore;
import com.choice.scm.domain.SpCodeUse;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.ForecastService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.SpCodeUseService;
import com.choice.scm.util.CalChkNum;

/**
* 物资千人、万元用量
* @author -  css
*/

@Controller
@RequestMapping("spCodeUse")
public class SpCodeUseController {

	@Autowired
	private SpCodeUseService spCodeUseService;
	@Autowired
	private AccountPositnService accountPositnService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private DictService dictService;
	@Autowired
	private ForecastService forecastService;
	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private PositnService positnService;
	
	/**
	 * 物资千人、万元用量查询
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list")
	public ModelAndView spCodeUseList(ModelMap modelMap, HttpSession session, SpCodeUse spCodeUse, String type) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		if (null==type) {
			// 获取分店code
			String accountId=session.getAttribute("accountId").toString();
			AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				spCodeUse.setFirm(accountPositn.getPositn().getCode());
			}
			modelMap.put("spCodeUseList", spCodeUseService.listSpCodeUse(spCodeUse));
		}
		//初次加载页面传值
		if (spCodeUse.getBdat() == null) {
			modelMap.put("bdat", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
		}else {
			modelMap.put("bdat", DateFormat.getStringByDate(spCodeUse.getBdat(),"yyyy-MM-dd"));
		}
		if (spCodeUse.getEdat() == null) {
			modelMap.put("edat", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
		}else {
			modelMap.put("edat", DateFormat.getStringByDate(spCodeUse.getEdat(),"yyyy-MM-dd"));
		}
		return new ModelAndView(SpCodeUseConstants.LIST_SPCODEUSE,modelMap);
	}
	
	/**
	 * 删除预估用量
	 * @param modelMap
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping("/delete")
	public ModelAndView delete(ModelMap modelMap, HttpSession session, String msg, SpCodeUse spCodeUse) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			spCodeUse.setFirm(accountPositn.getPositn().getCode());
		}
		// 删除预估用量
		spCodeUseService.deleteAll(spCodeUse);
		modelMap.put("spCodeUseList", spCodeUseService.listSpCodeUse(spCodeUse));
		
		if (spCodeUse.getBdat() == null) {
			modelMap.put("bdate", new Date());
		}
		if (spCodeUse.getEdat() == null) {
			modelMap.put("edate", new Date());
		}
		return new ModelAndView(SpCodeUseConstants.LIST_SPCODEUSE,modelMap);
	}
	
	/**
	 * 计算预估用量
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculate")
	public ModelAndView calculate(ModelMap modelMap, HttpSession session, SpCodeUse spCodeUse, String bdat, String edat) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		// 获取分店code
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			spCodeUse.setFirm(accountPositn.getPositn().getCode());
		}
		String tele_boh ="choice7";
		SpCodeUse spCode = new SpCodeUse();//获取营业额和人数总和
		if("tele".equals(tele_boh)){
			Positn positn = new Positn();
			positn.setCode(spCodeUse.getFirm());
			positn=positnService.findPositnByCode(positn);
			spCodeUse.setFirm(positn.getFirm());
			DataSourceSwitch.setDataSourceType(DataSourceInstances.TELE);//选择数据源
			spCode = spCodeUseService.getAmt(spCodeUse);
		}else if("choice3".equals(tele_boh) || "choice7".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			spCode = spCodeUseService.getAmt_boh(spCodeUse);
		}else if("pos".equals(tele_boh)){
			DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
			spCode = spCodeUseService.getAmt_pos(spCodeUse);
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//获取计算方式（千人或者万元）
		String typ = spCodeUseService.getPlanTyp(acct);
		if (spCode!=null) {
			if ("人数".equals(typ)) {
				spCodeUse.setLv(spCode.getPax()/1000);
			} else {
				spCodeUse.setLv(spCode.getAmt()/1000);
			}
		}else {
			spCodeUse.setLv(0);
			modelMap.put("yysj", "0");
			modelMap.put("bdat", bdat);
			modelMap.put("edat", edat);
			return new ModelAndView(SpCodeUseConstants.LIST_SPCODEUSE, modelMap);
		}
		spCodeUse.setBdat(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));
		spCodeUse.setEdat(DateFormat.getDateByString(edat, "yyyy-MM-dd"));
		modelMap.put("bdat", bdat);
		modelMap.put("edat", edat);
		spCodeUse.setFirm(accountPositn.getPositn().getCode());
		modelMap.put("spCodeUseList", spCodeUseService.calculate(spCodeUse));
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseConstants.LIST_SPCODEUSE, modelMap);
	}
	
	/**
	 *  保存修改调整量
	 * @param modelMap
	 * @param itemUse
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public ModelAndView update(ModelMap modelMap, SpCodeUse spCodeUse) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spCodeUseService.deleteAll(spCodeUse);
		spCodeUseService.update(spCodeUse);
		return null;
	}	
	
	/**
	 * 报货分类主页面
	 * @param modelMap
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargo")
	public ModelAndView findstorecargo(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, String next, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		// 获取分店code
		String accountId=session.getAttribute("accountId").toString();
		String firm = null;
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			firm = accountPositn.getPositn().getCode();
			spCodeUse.setFirm(firm);
		}
		if (next==null) {
			next="1";
		}
		//判断是否做过盘点
		if ("Y".equals("")) {
			modelMap.put("status_pd", spCodeUseService.getStatus_pd(spCodeUse));
			modelMap.put("next", next);
			modelMap.put("spCodeUse", spCodeUse);
			return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO,modelMap);
		}else {
			modelMap.put("status_pd", "Y");
		}
		//预估报货是否使用配送班表
		String psbb = "";
		modelMap.put("psbb", psbb);
		CodeDes code = new CodeDes();
		code.setTyp("11");
		if ("Y".equals(psbb)&&(spCodeUse.getBdat()==null||spCodeUse.getEdat()==null||spCodeUse.getDdat()==null)) {
			ScheduleStore scheduleStore = new ScheduleStore();
			scheduleStore.setsCode(firm);
			scheduleStore.setGrptyp(spCodeUse.getGrptyp());
			modelMap.put("grptypList", codeDesService.findCodeDes(code));
//			modelMap.put("grptypList", spCodeUseService.findGrpTypByPsbb(scheduleStore));
			if (Integer.parseInt(next)>1){//选择完报货类别以后的界面
				SpCodeUse spCodeUseDate = spCodeUseService.findDateByPsbb(scheduleStore);
				if (spCodeUseDate==null){
					modelMap.put("status_psbb", "N");
					modelMap.put("next", next);
					modelMap.put("spCodeUse", spCodeUse);
					return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO,modelMap);
				}else {
					spCodeUse.setEdat(spCodeUseDate.getEdat());
					spCodeUse.setBdat(spCodeUseDate.getBdat());
					spCodeUse.setDdat(spCodeUseDate.getDdat());
				}	
			}
		}else {
			modelMap.put("grptypList", codeDesService.findCodeDes(code));
//			modelMap.put("grptypList", grpTypService.findAllGrpTyp(null));
			spCodeUse.setEdat(DateFormat.formatDate(DateFormat.getDateBefore(new Date(),"day",1,1), "yyyy-MM-dd"));
			spCodeUse.setBdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		}
		if (Integer.parseInt(next)>2){//选择完报货日期以后的界面
			//判断是否做过营业预估
			PosSalePlan posSalePlan = new PosSalePlan();
			posSalePlan.setFirm(spCodeUse.getFirm());
			posSalePlan.setStartdate(spCodeUse.getBdat());
			posSalePlan.setEnddate(spCodeUse.getEdat());
			List<PosSalePlan> PosSalePlanList = forecastService.findPosSalePlan(posSalePlan);
			if (PosSalePlanList.size()!=1+(spCodeUse.getEdat().getTime()-spCodeUse.getBdat().getTime()+1000000)/(3600*24*1000)) {
				modelMap.put("status_yyyg", "N");
				modelMap.put("next", next);
				modelMap.put("spCodeUse", spCodeUse);
				return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO,modelMap);
			}else {
				modelMap.put("posSalePlanList", PosSalePlanList);
			}
			//判断是否做过千人万元用量预估
			List<SpCodeUse> spCodeUseList = spCodeUseService.listSpCodeUse(spCodeUse);
			if (spCodeUseList.size()==0) {
				modelMap.put("status_qwyg", "N");
			}
		}
		modelMap.put("next", next);
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO,modelMap);
	}
	
	/**
	 * 报货分类主页面
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/table")
	public ModelAndView tableStorecargo(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());;
		spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
		List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSpSupplyCalculate(spCodeUse);
		modelMap.put("listSpSupplyCal", listSpSupplyCal);
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseConstants.TABLE_STORECARGO, modelMap);
	}
	
	/**
	 * 生成报货单
	 * @param modelMap
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveByAddOrUpdate")
	@ResponseBody
	public Object saveByAddOrUpdate(ModelMap modelMap, HttpSession session, Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			String positnCode=accountPositn.getPositn().getCode();
			chkstom.setFirm(positnCode);
		}
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		return spCodeUseService.saveOrUpdateChk(chkstom);
	}
	
	/**
	 * 生成报货单总部预估
	 * @param modelMap
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveByAddOrUpdateZB")
	@ResponseBody
	public Object saveByAddOrUpdateZB(ModelMap modelMap, HttpSession session, Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//总部仓位
		chkstom.setFirm("1000");//总部
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		return spCodeUseService.saveOrUpdateChk(chkstom);
	}
	/**
	 * 生成报货单加工间预估
	 * @param modelMap
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveByAddOrUpdateJGJ")
	@ResponseBody
	public Object saveByAddOrUpdateJGJ(ModelMap modelMap, HttpSession session, Chkstom chkstom) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//总部仓位
		chkstom.setFirm("1011");//加工间
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",new Date()));
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		return spCodeUseService.saveOrUpdateChk(chkstom);
	}
	/**
	 * 报货分类主页面
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/tableCk")
	public ModelAndView tableStorecargoCk(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session, String action) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if (!"init".equals(action)) {
			spCodeUse.setLv((spCodeUse.getEdat().getTime()-spCodeUse.getBdat().getTime())/1000/60/60/24);
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyAcctCk(spCodeUse);
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		// 获取分店code
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			spCodeUse.setFirm(accountPositn.getPositn().getCode());
		}
		if (spCodeUse.getBdat() == null) {
			spCodeUse.setBdat(DateFormat.formatDate(DateFormat.getDateBefore(new Date(),"day",-1,90), "yyyy-MM-dd"));
		}
		if (spCodeUse.getEdat() == null) {
			spCodeUse.setEdat(DateFormat.formatDate(DateFormat.getDateBefore(new Date(),"day",1,1), "yyyy-MM-dd"));
		}
		modelMap.put("spCodeUse", spCodeUse);
		return new ModelAndView(SpCodeUseConstants.TABLE_STORECARGO_CK, modelMap);
	}
	
	/**
	 * 门店报货分类主页面（按照菜品销售数量和菜品bom）
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargoBMis")
	public ModelAndView findstorecargoBMis(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String firm = null;
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			firm = accountPositn.getPositn().getCode();
			spCodeUse.setFirm(firm);
		}
		PosItemPlan posItemPlan = new PosItemPlan();
		posItemPlan.setFirm(firm);
		posItemPlan.setStartdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		posItemPlan.setEnddate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 2), "yyyy-MM-dd"));
		int days=forecastService.findPosItemPlanForDays(posItemPlan).size();
		if (days==3) {
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());;
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyCalculateByBom(spCodeUse);
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("spCodeUse", spCodeUse);
		modelMap.put("days", days);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO_B, modelMap);
	}
	
	/**
	 * 门店报货分类主页面（按照门店营业额预估和千元用量）（木屋）
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargoMis")
	public ModelAndView findstorecargoMis(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String firm = null;
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		if(null!=accountPositn && null!=accountPositn.getPositn()){
			firm = accountPositn.getPositn().getCode();
			spCodeUse.setFirm(firm);
		}
		//查找千元用量
		int qyyl = spCodeUseService.listSpCodeUse(spCodeUse).size();
		//查找门店营业额预估数据
		PosSalePlan posSalePlan = new PosSalePlan();
		posSalePlan.setFirm(firm);
		posSalePlan.setStartdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		posSalePlan.setEnddate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 2), "yyyy-MM-dd"));
		int days = forecastService.findPosSalePlan(posSalePlan).size();
		
		if (days == 3 && qyyl > 0) {
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyCalculate(spCodeUse);
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("spCodeUse", spCodeUse);
		modelMap.put("days", days);
		modelMap.put("qyyl", qyyl);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO_MIS, modelMap);
	}
	
	/**
	 * 总部报货分类主页面（按照菜品销售数量和菜品bom）
	 * @param modelMap
	 * @param spCodeUse
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargoB")
	public ModelAndView tableStorecargoB(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PosItemPlan posItemPlan = new PosItemPlan();
		posItemPlan.setStartdate(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		posItemPlan.setEnddate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 2), "yyyy-MM-dd"));
		int days=forecastService.findPosItemPlanForDays(posItemPlan).size();
		if (days==3) {
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());;
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyCalculateByBomZb(spCodeUse);//总部预估数据获取方法
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("spCodeUse", spCodeUse);
		modelMap.put("days", days);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGOZB_B, modelMap);
	}
	/**
	 * 总部预估
	 * @param modelMap
	 * @param safetystock
	 * @param spCodeUse
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargoZB")
	public ModelAndView findstorecargoZB(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Positn> positnList = positnService.findPositnMonit();
		int posCount = positnList.size();//区域下分店数
		
		//查找千元用量
		int qyyl = spCodeUseService.listSpCodeUse(spCodeUse).size();
		//查找门店营业额预估数据
		PosSalePlan posSalePlan = new PosSalePlan();
		posSalePlan.setStartdate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 3), "yyyy-MM-dd"));//总部查询第4 第5天预估营业额
		posSalePlan.setEnddate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 4), "yyyy-MM-dd"));
		int days = forecastService.findPosSalePlan(posSalePlan).size();//查询总部预付营业额数据条数
		if (days>0 && days==posCount*2 && qyyl > 0) {
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());;
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyCalculateZb(spCodeUse);
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		sf.format(posSalePlan.getStartdate());
		
		String dateStr =sf.format(posSalePlan.getStartdate())+","+sf.format(posSalePlan.getEnddate());
		spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("spCodeUse", spCodeUse);
		modelMap.put("days",posCount*2-days==0?0:dateStr);
		modelMap.put("qyyl", qyyl);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO_ZB, modelMap);
	}
	/**
	 * 加工间预估
	 * @param modelMap
	 * @param safetystock
	 * @param spCodeUse
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findstorecargoJGJ")
	public ModelAndView findstorecargoJGJ(ModelMap modelMap, Double safetystock, SpCodeUse spCodeUse, HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Positn> positnList = positnService.findPositnMonit();
		int posCount = positnList.size();//区域下分店数
		
		//查找千元用量
		int qyyl = spCodeUseService.listSpCodeUse(spCodeUse).size();
		//查找门店营业额预估数据
		PosSalePlan posSalePlan = new PosSalePlan();
		posSalePlan.setStartdate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 3), "yyyy-MM-dd"));//总部查询第3 第4天加工间的预估营业额
		posSalePlan.setEnddate(DateFormat.formatDate(DateFormat.getDateBefore(new Date(), "day", 1, 4), "yyyy-MM-dd"));
		int days = forecastService.findPosSalePlan(posSalePlan).size();//查询总部预付营业额数据条数
		if (days>0 && days==posCount*2 && qyyl > 0) {
			spCodeUse.setAcct(session.getAttribute("ChoiceAcct").toString());;
			spCodeUse.setYearr(DateFormat.getStringByDate(new Date(),"yyyy"));
			List<SpCodeUse> listSpSupplyCal = spCodeUseService.listSupplyCalculateJGJ(spCodeUse);
			modelMap.put("listSpSupplyCal", listSpSupplyCal);
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		sf.format(posSalePlan.getStartdate());
		
		String dateStr =sf.format(posSalePlan.getStartdate())+","+sf.format(posSalePlan.getEnddate());
		spCodeUse.setDdat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("spCodeUse", spCodeUse);
		modelMap.put("days",posCount*2-days==0?0:dateStr);
		modelMap.put("qyyl", qyyl);
		return new ModelAndView(SpCodeUseConstants.LIST_STORECARGO_JGJ, modelMap);
	}
	public static void main(String[] args) {
		
		Date d1= DateFormat.getDateBefore(new Date(), "day", 1, 3);
		Date d2= DateFormat.getDateBefore(new Date(), "day", 1, 4);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr =sf.format(d1)+","+sf.format(d2);
		System.out.println(dateStr);
	}
}
