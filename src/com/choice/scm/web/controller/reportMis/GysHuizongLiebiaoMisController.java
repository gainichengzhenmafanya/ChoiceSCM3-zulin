package com.choice.scm.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportMis.GysHuizongLiebiaoMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("GysHuizongLiebiaoMis")
public class GysHuizongLiebiaoMisController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private GysHuizongLiebiaoMisService GysHuizongLiebiaoMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	AccountPositnService accountPositnService;

	/********************************************供应商汇总列表报表****************************************************/
	/**
	 * 跳转到列选择页面
	 * @throws CRUDException
	 */
	@RequestMapping("/toColChooseDeliverSum")
	public ModelAndView toColChooseDeliverSum(ModelMap modelMap,HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_DELIVERSUM);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_DELIVERSUM));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
	/**
	 * 查询表头信息
	 * @param session
	 * @return
	 */
	@RequestMapping("/findDeliverSumHeaders")
	@ResponseBody
	public Object getDeliverSumHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERSUM);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_DELIVERSUM));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_DELIVERSUM_FROZEN);
		return columns;
	}
	/**
	 * 跳转到供应商汇总报表页面
	 * @return
	 */
	@RequestMapping("/toDeliverSum")
	public ModelAndView toDeliverSum(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName", ScmStringConstant.REPORT_NAME_DELIVERSUM);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_DELIVERSUM_MIS,modelMap);
	}
	/**
	 * 查询供应商汇总报表内容
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException 
	 */
	@RequestMapping("/findDeliverSum")
	@ResponseBody
	public Object ChkinDeliverSum(ModelMap modelMap,HttpSession session,String page,String delivertyp,String rows,String sort,String order,SupplyAcct supplyAcct) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		condition.put("delivertyp", delivertyp);
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setNowPage(page == "" || page == null ? 0 : Integer.parseInt(page));
		pager.setPageSize(rows == "" || rows == null ? 10 : Integer.parseInt(rows));
		return GysHuizongLiebiaoMisService.findDeliverSum(condition,pager);
	}
	/**
	 * 打印
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/printDeliverSum")
	public ModelAndView printDeliverSum(ModelMap modelMap, Page pager, HttpSession session, String type,
			SupplyAcct supplyAcct,String sort, String order, String delivertyp)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}

		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("delivertyp", delivertyp);
		params.put("sort", sort);
		params.put("order", order);
		params.put("accountId", accountId);
		supplyAcct.setAccountId(accountId);
		condition.put("supplyAcct", supplyAcct);
		condition.put("delivertyp", delivertyp);
		condition.put("sort", sort);
		condition.put("order", order);
		modelMap.put("List",GysHuizongLiebiaoMisService.findDeliverSum(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "供应商汇总");
	    modelMap.put("actionMap", params);
	    parameters.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
	    parameters.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/GysHuizongLiebiaoMis/printDeliverSum.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_DELIVERSUM,SupplyAcctConstants.REPORT_EXP_URL_DELIVERSUM);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	/**
	 * 导出供应商汇总报表
	 * @param response
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportDeliverSum")
	@ResponseBody
	public void exportDeliverSum(HttpServletResponse response,String sort,String order,String delivertyp,HttpServletRequest request,HttpSession session,SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "供应商汇总";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}

		condition.put("delivertyp", delivertyp);
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_DELIVERSUM);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), GysHuizongLiebiaoMisService.findDeliverSum(condition,pager).getRows(), "供应商汇总", dictColumnsService.listDictColumnsByTable(dictColumns));
		
	}
}
