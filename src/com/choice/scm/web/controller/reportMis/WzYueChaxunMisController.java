package com.choice.scm.web.controller.reportMis;

import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.service.DictColumnsService;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.constants.SupplyAcctConstants;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.service.ChkstomService;
import com.choice.scm.service.PositnService;
import com.choice.scm.service.reportMis.WzYueChaxunMisService;
import com.choice.scm.util.ExportExcel;
import com.choice.scm.util.ReadReportUrl;
@Controller
@RequestMapping("WzYueChaxunMis")
public class WzYueChaxunMisController {

	@Autowired
	private Page pager;
	@Autowired
	private DictColumns dictColumns;
	@Autowired
	private WzYueChaxunMisService WzYueChaxunMisService;
	@Autowired
	private DictColumnsService dictColumnsService;
	@Autowired
	private ExportExcel<Map<String,Object>> exportExcelMap;
	@Autowired
	PositnService positnService;
	@Autowired
	private ChkstomService chkstomService;
	@Autowired
	AccountPositnService accountPositnService;


	/********************************************物资余额查询报表****************************************************/
	/**
	 * 跳转到物资余额查询页面
	 * @param modelMap
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toSupplyBalance")
	public ModelAndView toSupplyBalance(ModelMap modelMap){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("reportName",ScmStringConstant.REPORT_NAME_SUPPLYBALANCE);
		return new ModelAndView(SupplyAcctConstants.REPORT_SHOW_SUPPLYBALANCE_MIS,modelMap);
	}
	
	/**
	 * 查询表头
	 * @param session
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyBalanceHeaders")
	@ResponseBody
	public Object getSupplyBalanceHeaders(HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> columns = new HashMap<String,Object>();
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYBALANCE);
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setLocale(session.getAttribute("locale").toString());
		columns.put("columns", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYBALANCE));
		columns.put("frozenColumns", ScmStringConstant.BASICINFO_REPORT_SUPPLYBALANCE_FROZEN);
		return columns;
	}
	
	/**
	 * 查询物资余额列表
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param bz
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @return
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/findSupplyBalance")
	@ResponseBody
	public Object findSupplyBalance(ModelMap modelMap, HttpSession session, String page, String typ, SupplyAcct supplyAcct,
			String month, String without0, String withamountin, String rows, String sort, String order) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());

		content.put("yearr", supplyAcct.getYearr());
		content.put("month", month);
		content.put("without0", without0);
		content.put("typ", typ);
		content.put("withamountin", withamountin);
		
//		String type = supplyAcct.getTyp();
//		String positn1 = supplyAcct.getPositn1();
//		if(type!=null && type!=""){
//		type = "'"+type+"'";
//		type = type.replace(",","','");
//		supplyAcct.setTyp(type);
//		}
//		if(positn1!=null && positn1!=""){
//			positn1 = "'" +positn1 +"'";
//			positn1 = positn1.replace(",","','");
//			supplyAcct.setPositn1(positn1);
//		}
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		supplyAcct.setPositn1(CodeHelper.replaceCode(supplyAcct.getPositn1()));
		content.put("bill",supplyAcct.getBill());
		content.put("supplyAcct",supplyAcct);
		content.put("sort",sort);
		content.put("order", order);
		pager.setNowPage(page==""||page==null? 0:Integer.parseInt(page));
		pager.setPageSize(rows==""||rows==null ? 20:Integer.parseInt(rows));
		return WzYueChaxunMisService.findSupplyBalance(content, pager);
	}
	
	/**
	 * 导出物资余额查询excel文件
	 * @param response
	 * @param sort
	 * @param order
	 * @param request
	 * @param session
	 * @param supplyAcct
	 * @throws Exception
	 */
	@RequestMapping("/exportSupplyBalance")
	@ResponseBody
	public void exportSupplyBalance(HttpServletResponse response, String month, String without0, String order, String typ,
			String withamountin, String sort, HttpServletRequest request, HttpSession session, SupplyAcct supplyAcct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String fileName = "物资余额查询";
		Map<String,Object> condition = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}

		condition.put("yearr", supplyAcct.getYearr());
		condition.put("typ", typ);
		condition.put("month",month);
		condition.put("without0", without0);
		condition.put("withamountin", withamountin);
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		supplyAcct.setPositn1(CodeHelper.replaceCode(supplyAcct.getPositn1()));
		condition.put("bill",supplyAcct.getBill());
		condition.put("supplyAcct", supplyAcct);
		condition.put("sort", sort);
		condition.put("order", order);
		pager.setPageSize(Integer.MAX_VALUE);
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYBALANCE);
		response.setContentType("application/msexcel; charset=UTF-8");
		if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
		    //IE  
		    fileName = URLEncoder.encode(fileName, "UTF-8");              
		}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
		    //firefox  
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}else{                
		    // other          
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
		}   
		response.setHeader("Content-disposition", "attachment; filename="  
                + fileName + ".xls");
		exportExcelMap.creatWorkBook(response.getOutputStream(), WzYueChaxunMisService.findSupplyBalance(condition,pager).getRows(), "物资余额查询", dictColumnsService.listDictColumnsByAccount(dictColumns, ScmStringConstant.BASICINFO_REPORT_SUPPLYBALANCE));
		
	}
	
	/**
	 * 打印物资余额
	 * @param modelMap
	 * @param pager
	 * @param session
	 * @param type
	 * @param supplyAcct
	 * @param delivertyp
	 * @param folio
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/printSupplyBalance")
	public ModelAndView printSupplyBalance(ModelMap modelMap, Page pager, HttpSession session, String month,String typ,
			String without0, String type, SupplyAcct supplyAcct, String withamountin, String folio)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		pager.setPageSize(Integer.MAX_VALUE);
		Map<String,Object> condition = new HashMap<String,Object>();
		Map<String,String> params = new HashMap<String,String>();
		if(supplyAcct.getBdat() != null)
			params.put("bdat",DateFormat.getStringByDate(supplyAcct.getBdat(),"yyyy-MM-dd"));
		params.put("positn",supplyAcct.getPositn());
		if(supplyAcct.getEdat() != null)
			params.put("edat",DateFormat.getStringByDate(supplyAcct.getEdat(),"yyyy-MM-dd"));
		params.put("chktyp",supplyAcct.getChktyp());
		params.put("sp_code",supplyAcct.getSp_code());
		params.put("grptyp",supplyAcct.getGrptyp());
		params.put("grp",supplyAcct.getGrp());
		params.put("typ",supplyAcct.getTyp());
		params.put("folio",String.valueOf(supplyAcct.getFolio()));
		params.put("bill",String.valueOf(supplyAcct.getBill()));
		params.put("without0",without0);
		params.put("positn1",supplyAcct.getPositn1());
		params.put("month",month);
		params.put("delivercode",supplyAcct.getDelivercode());
		params.put("folio", folio);
		params.put("withamountin", withamountin);
		String accountId=session.getAttribute("accountId").toString();
		AccountPositn accountPositn=accountPositnService.findAccountById(accountId);
		//根据当前登录的用户名进行分店仓位匹配，看用户属于哪个分店仓位
		if(supplyAcct.getPositn()==null || "".equals(supplyAcct.getPositn())){//选择档口为空时，默认查询该分店的入库明细
			if(null!=accountPositn && null!=accountPositn.getPositn()){
				//查询该分店的档口
				Positn positn = new Positn();
				positn.setUcode(accountPositn.getPositn().getCode());
				List<Positn> list = positnService.findPositnSuperNOPage(positn);
				String positncode = "";
				for(Positn posi:list){
					positncode +=CodeHelper.replaceCode(posi.getCode())+",";
				}
				positncode+=CodeHelper.replaceCode(accountPositn.getPositn().getCode());
				supplyAcct.setPositn(positncode);
			}
		}else{
			supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
		}

		condition.put("yearr", supplyAcct.getYearr());
		condition.put("typ", typ);
		supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
		supplyAcct.setPositn1(CodeHelper.replaceCode(supplyAcct.getPositn1()));
		condition.put("bill",supplyAcct.getBill());
		condition.put("month",month);
		condition.put("without0",without0);
		condition.put("withamountin", withamountin);
		condition.put("supplyAcct", supplyAcct);
		modelMap.put("List",WzYueChaxunMisService.findSupplyBalance(condition,pager).getRows());
	 	HashMap<Object,Object>  parameters = new HashMap<Object,Object>();
	    parameters.put("report_name", "物资余额查询");
	    parameters.put("month",month);
	    modelMap.put("actionMap", params);
	    parameters.put("maded",new Date());
	    parameters.put("madeby", session.getAttribute("accountName").toString());
	        
        modelMap.put("parameters", parameters);
	 	modelMap.put("action", "/WzYueChaxunMis/printSupplyBalance.do");//传入回调路径
	 	Map<String,String> rs=ReadReportUrl.redReportUrl(type,SupplyAcctConstants.REPORT_PRINT_URL_SUPPLYBALANCE,SupplyAcctConstants.REPORT_EXP_URL_SUPPLYBALANCE);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
        return new ModelAndView(rs.get("url").replace("ireport", "ireport/mapSource"),modelMap);
	}
	
	/**
	 * 生成报货单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param typ
	 * @param month
	 * @param without0
	 * @param rows
	 * @param sort
	 * @param order
	 * @param supplyAcct
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	@RequestMapping("/saveBill")
	@ResponseBody
	public String saveBill(ModelMap modelMap, HttpSession session, String page, String typ, String month,
			String without0, String rows, String sort, String order, SupplyAcct supplyAcct, Chkstom chkstom) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,Object> content = new HashMap<String,Object>();
		supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
		content.put("month", month);
		content.put("without0", without0);
		content.put("typ", typ);
		content.put("supplyAcct",supplyAcct);
		String type = supplyAcct.getTyp();
		if(type!=null && !type.equals("")){	
			type = "'"+type+"'";
			type = type.replace(",","','");
			supplyAcct.setTyp(type);
		}
		content.put("sort",sort);
		content.put("order", order);
		//当前帐套
		chkstom.setAcct(session.getAttribute("ChoiceAcct").toString());
		chkstom.setChkstoNo(chkstomService.getMaxChkstono());
		String accountName=session.getAttribute("accountName").toString();
		chkstom.setMadeby(accountName);
		chkstom.setMaded(new Date());
		chkstom.setFirm(chkstom.getPositn().getCode());
		return chkstomService.saveOrUpdateChk(chkstom,"add");
	}
	
	/**
	 * 跳转到列选择页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	@RequestMapping("/toColChooseSupplyBalance")
	public ModelAndView toColChooseSupplyBalance(ModelMap modelMap, HttpSession session)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		dictColumns.setAccountId(session.getAttribute("accountId").toString());
		dictColumns.setTableName(ScmStringConstant.REPORT_NAME_SUPPLYBALANCE);
		dictColumns.setLocale(session.getAttribute("locale").toString());
		modelMap.put("objBean", "SupplyAcct");
		modelMap.put("tableName", ScmStringConstant.REPORT_NAME_SUPPLYBALANCE);
		modelMap.put("dictColumnsListByAccount", dictColumnsService.listDictColumnsByAccount(dictColumns,ScmStringConstant.BASICINFO_REPORT_SUPPLYBALANCE));
		modelMap.put("dictColumnsListByName", dictColumnsService.listDictColumnsByTable(dictColumns));
		return new ModelAndView(ScmStringConstant.TO_COLUMNS_CHOOSE_VIEW,modelMap);
	}
}
