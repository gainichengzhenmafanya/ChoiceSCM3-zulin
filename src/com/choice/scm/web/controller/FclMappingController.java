/*package com.choice.scm.web.controller;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.FclMappingConstants;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FclMapping;
import com.choice.scm.service.FclMappingService;

@Controller
@RequestMapping(value = "FclMapping")
*//**
 * 描述：财务科目映射设置
 * 日期：2014年1月12日 上午11:30:57
 * 作者：毛明武mmw
 * 文件：FclMappingController.java
 *//*
public class FclMappingController {

	@Autowired
	private FclMappingService fclMappingService;

	*//**
	 * 描述：打开列表界面
	 * 日期：2014年1月12日 上午11:33:08
	 * 作者：毛明武mmw
	 * 文件：FclMappingController.java
	 * @param modelMap
	 * @param FclMapping
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/list")
	public ModelAndView findAllFclMapping(ModelMap modelMap,FclMapping fclMapping,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<FclMapping> FclMappingList = fclMappingService.findAllFclMapping(fclMapping,page);
		String pk_idList="setPkid";
		for (int i = 0; i < FclMappingList.size(); i++) {
			FclMapping fcls = FclMappingList.get(i);
			pk_idList+=","+fcls.getPk_id();
		}
		modelMap.put("pk_idList", pk_idList);
		modelMap.put("FclMappingList", FclMappingList);
		modelMap.put("FclMapping", fclMapping);
		modelMap.put("pageobj", page);
		return new ModelAndView(FclMappingConstants.LIST_FCLMAPPING, modelMap);
	}
	
	*//**
	 * 描述:映射界面选择界面，暂时不用
	 * 作者:mmw
	 * 日期:2013-11-14 上午10:20:16
	 * @param modelMap
	 * @param single
	 * @param callBack
	 * @param pk_id
	 * @return
	 * @throws Exception
	 *//*
//	@RequestMapping(value = "/toChooseFclMapping")
//	public ModelAndView toChooseFclMapping(ModelMap modelMap,String single,String callBack,String pk_id)throws Exception{
//		DataSourceSwitch.setDataSourceType(DataSourceInstances.BOH);//选择数据源
//		if(null == pk_id || pk_id.equals("")){
//			pk_id="selectType";
//		}
//		List<FclMapping> FclMappingList = fclMappingService.findAllFclMappingbyId(pk_id);
//		
//		modelMap.put("FclMappingList", FclMappingList);
//		modelMap.put("callBack", callBack);
//		modelMap.put("single", single);
//		modelMap.put("pk_id", pk_id);
//		return new ModelAndView(FclMappingConstants.TOCHOOSELIST_FCLMAPPING, modelMap);
//	}
	
	*//**
	 * 打开新增页面
	 * @param modelMap
	 * @param role
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/editopen")
	public ModelAndView editopen(ModelMap modelMap,FclMapping fclMapping,String edittype) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null != edittype &&  !"".equals(edittype)){
			if(edittype.equals("update")){
				fclMapping=fclMappingService.findFclMappingById(fclMapping.getPk_fclmapping());
			}
		}
		if(null == fclMapping){
			fclMapping=new FclMapping();
		}
		modelMap.put("FclMapping",fclMapping);
		modelMap.put("edittype", edittype);
		return new ModelAndView(FclMappingConstants.EDIT_FCLMAPPING, modelMap);
	}
	
	*//**
	 * 描述：修改或新增
	 * 日期：2014年1月12日 上午11:38:00
	 * 作者：毛明武mmw
	 * 文件：FclMappingController.java
	 * @param modelMap
	 * @param FclMapping
	 * @param type，修改：update，新增：save
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/edit",method={RequestMethod.POST})
	@ResponseBody
	public Object save(ModelMap modelMap, FclMapping fclMapping,String edittype) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return fclMappingService.editFclMapping(fclMapping,edittype);
	}
	
	*//**
	 * 描述：删除
	 * 日期：2014年1月12日 上午11:38:37
	 * 作者：毛明武mmw
	 * 文件：FclMappingController.java
	 * @param modelMap
	 * @param FclMapping
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/deletes")
	public ModelAndView delete(ModelMap modelMap,FclMapping fclMapping,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<String> listId = Arrays.asList(fclMapping.getPk_fclmapping().split(","));
		fclMappingService.deleteFclMappingById(listId);
		List<FclMapping> FclMappingList = fclMappingService.findAllFclMapping(fclMapping,page);
		String pk_idList="setPkid";
		for (int i = 0; i < FclMappingList.size(); i++) {
			FclMapping fcls = FclMappingList.get(i);
			pk_idList+=","+fcls.getPk_id();
		}
		modelMap.put("pk_idList", pk_idList);
		modelMap.put("FclMappingList", FclMappingList);
		modelMap.put("FclMapping", fclMapping);
		modelMap.put("pageobj", page);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	*//**
	 * 选择供应商
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/selectOneDeliver")
	public ModelAndView selectOneDeliver(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商选择界面
		modelMap.put("deliverList", fclMappingService.findDelivers(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("queryDeliver", deliver);
		return new ModelAndView(FclMappingConstants.TABLE_DELIVERS, modelMap);
	}
	
	*//**
	 * 选择仓位
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/selectOnePositn")
	public ModelAndView selectOnePositn(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//仓位选择界面
		modelMap.put("positnList", fclMappingService.findPositns(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("queryPositn", deliver);
		return new ModelAndView(FclMappingConstants.TABLE_POSITNS, modelMap);
	}
	
	*//**
	 * 选择客户
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/selectCustomer")
	public ModelAndView selectCustomer(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商选择界面
		modelMap.put("deliverList", fclMappingService.findCustomer(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("queryDeliver", deliver);
		return new ModelAndView(FclMappingConstants.TABLE_CUSTOMER, modelMap);
	}
	
	*//**
	 * 选择仓库
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping(value = "/selectWareHouse")
	public ModelAndView selectWareHouse(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.EAS);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商选择界面
		modelMap.put("deliverList", fclMappingService.findCustomer(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("queryDeliver", deliver);
		return new ModelAndView(FclMappingConstants.TABLE_WAREHOUSE, modelMap);
	}
}
*/