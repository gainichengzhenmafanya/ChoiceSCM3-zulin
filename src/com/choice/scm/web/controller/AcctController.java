package com.choice.scm.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.AcctConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ana.CostProfitService;

/**
 * 帐套管理相关
 * @author lq 
 *
 */
@Controller
@RequestMapping("acct")
public class AcctController {

	@Autowired
	private AcctService acctService;
	@Autowired
	private CostProfitService costProfitService;
		
	/**
	 * 模糊查询帐套信息
	 * @param modelMap
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAcct(ModelMap modelMap,Acct acct,String des) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(des !=null && !des.equals("")){
			acct.setDes(des);
		}
		List<Acct> acctList= acctService.findAcct(acct);
//		modelMap.put("pageobj", page);
		modelMap.put("acctList",acctList);
		return new ModelAndView(AcctConstants.LIST_ACCT,modelMap);
	}
	
	/**
	 * 新增帐套
	 */
	@RequestMapping(value = "/add")
	public ModelAndView saveAcct(ModelMap modelMap,Acct acct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return new ModelAndView(AcctConstants.ADD_ACCT,modelMap);
	}
	
	/**
	 * 查询ID是否已经存在
	 */
	@RequestMapping(value = "/checkId") 
	@ResponseBody
	public Object checkId(ModelMap modelMap,String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return acctService.checkAcctById(code);
	}
	
	/**
	 * 新增帐套
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView saveByAdd(ModelMap modelMap,Acct acct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(acct.getWarn().equals("Y")){
			acctService.updateAcctWarn();
		}
		acct.setUsernam("");
		acct.setPass("");
		acctService.saveAcct(acct);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 修改帐套
	 */
	@RequestMapping(value = "/update")
	public ModelAndView UpdateAcct(ModelMap modelMap,String code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("acct", acctService.findAcctById(code));
		return new ModelAndView(AcctConstants.UPDATE_ACCT,modelMap);
	}
	
	/**
	 * 修改帐套保存
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,Acct acct) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if("Y".equals(acct.getWarn())){
			acctService.updateAcctWarn();
		}
		acct.setUsernam("");
		acct.setPass("");
		acctService.updateAcct(acct);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 生成月次
	 * @throws Exception
	 */
	@RequestMapping(value="/checkAndUpdateMonth")
	@ResponseBody
	public int checkAndUpdateMonth(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct=session.getAttribute("ChoiceAcct").toString();
		return acctService.checkAndUpdateMonth(acct);
	}
	
	@RequestMapping(value="/checkAndUpdateWeek")
	@ResponseBody
	public int checkAndUpdateWeek(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//		String acct=session.getAttribute("ChoiceAcct").toString();
		return costProfitService.checkAndUpdateWeek();
	}
}
