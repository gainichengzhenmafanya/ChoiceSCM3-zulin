package com.choice.scm.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CodeDesConstants;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Tax;
import com.choice.scm.domain.Typoth;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.SupplyService;
/**
 * 系统编码的新增、修改、删除
 * @author csb
 *
 */
@Controller
@RequestMapping(value = "codeDes")
public class CodeDesController {

	@Autowired
	private CodeDesService codeDesService;
	@Autowired
	private SupplyService supplyService;
	private static final Logger LOG = LoggerFactory.getLogger(CodeDesController.class);

	/**
	 * 系统编码列表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public ModelAndView findAllDes(ModelMap modelMap) throws Exception
	{
		//系统编码主页面
		return new ModelAndView(CodeDesConstants.LIST_CODEDES,modelMap);
	}
	
	/**
	 * 查询所有编码
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/table")
	public ModelAndView findAllCodeDes(ModelMap modelMap,Page page,String pageTyp,HttpSession session) throws Exception
	{
		//如果分页标识不为空，根据页标识进行查询
		CodeDes codeDes = new CodeDes();
		
		String locale = session.getAttribute("locale").toString();//获取语言类型
		if("12".equals(pageTyp) || "14".equals(pageTyp)|| "15".equals(pageTyp)|| "18".equals(pageTyp)){
			codeDes.setLocale(locale);
		}
		
		if(null!=pageTyp && !"".equals(pageTyp)){
			codeDes.setTyp(pageTyp);
		}else{//初始化页面的时候不查询记录
			codeDes.setTyp("root");
			pageTyp="root";
		}
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if ("11".equals(codeDes.getTyp())) {
			modelMap.put("codeDesList", codeDesService.findCodeDes_typ(codeDes,page));
		} else if("16".equals(codeDes.getTyp())){//物资编码-辅助信息-参考类别
			List<Typoth> typoths = supplyService.findTypoth(null,null);
			modelMap.put("codeDesList", typoths);
			modelMap.put("fz", "1");
		} else if("17".equals(codeDes.getTyp())){//货架
			List<CodeDes> list = codeDesService.findCodeDes(codeDes,page);
			for(CodeDes codedes:list){
				Supply supply = new Supply();
				supply.setPositn1(codedes.getDes());
				String acct = session.getAttribute("ChoiceAcct").toString();
				supply.setAcct(acct);
				List<Supply> list2 = supplyService.findAllSupply(supply);
				if(list2 != null && list2.size() >0){
					codedes.setLocked("已引用");
				}else{
					codedes.setLocked("未引用");
				}
			}
			modelMap.put("codeDesList", list);
		}else if("19".equals(codeDes.getTyp())){//税率
			Tax tax = new Tax();
			modelMap.put("taxList", codeDesService.findTaxRateList(tax,page));
		}else {
			modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes,page));
		}
		modelMap.put("pageobj", page);
		modelMap.put("pageTyp", pageTyp);
		if ("11".equals(codeDes.getTyp())) {
			return new ModelAndView(CodeDesConstants.TABLE_CODEDES_T,modelMap);
		} else if ("12".equals(codeDes.getTyp()) || "14".equals(codeDes.getTyp()) || "15".equals(codeDes.getTyp()) || "18".equals(codeDes.getTyp())) {
			return new ModelAndView(CodeDesConstants.TABLE_CODEDES_S,modelMap);
		} else {
			//修改税率
			if("19".equals(codeDes.getTyp())){
				return new ModelAndView(CodeDesConstants.SHOW_TAX_LIST,modelMap);
			}
			return new ModelAndView(CodeDesConstants.TABLE_CODEDES,modelMap);
		}
	}

	/**
	 * 报货分类查询跳转页面
	 */
	@RequestMapping(value = "/classification")
	public ModelAndView findclassification(ModelMap modelMap,Page page,Supply supply,CodeDes codeDes,String spcode, String code1, String level) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		codeDes.setTyp("11");
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes,page));
//		modelMap.put("supplyList", supplyService.findAllSupply(supply));
		modelMap.put("spcode", spcode);
		modelMap.put("pageobj", page);
		modelMap.put("code1", code1);
		modelMap.put("level", level);
		return new ModelAndView(CodeDesConstants.SELECTCLASSIFICATION,modelMap);
	}
	
	/**
	 * 打开新增页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelMap modelMap,CodeDes codeDes) throws Exception
	{
		//系统编码新增页面
		int t = Integer.parseInt(codeDesService.getMaxBeatsequence(codeDes))+1;
		codeDes.setBeatsequence(t);
		modelMap.put("codeDes", codeDes);
		return new ModelAndView(CodeDesConstants.SAVE_CODEDES,modelMap);
	}
	
	/**
	 * 打开新增页面--报货类别
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add_typ")
	public ModelAndView add_typ(ModelMap modelMap,CodeDes codeDes) throws Exception
	{
		//系统编码新增页面
		int t = Integer.parseInt(codeDesService.getMaxBeatsequence(codeDes))+1;
		codeDes.setBeatsequence(t);
		modelMap.put("codeDes", codeDes);
		return new ModelAndView(CodeDesConstants.SAVE_CODEDES_T,modelMap);
	}
	
	/**
	 * 保存系统编码信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd")
	public ModelAndView save(ModelMap modelMap,Page page,CodeDes codeDes,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//标识方向的变量
		String inout="";
		//数据库执行返回标识
		String str = "";
		//保存的时候，去掉一下空格
		String bfcode = codeDes.getCode();
		String bfdes = codeDes.getDes();
		if(bfcode!=null){
			codeDes.setCode(bfcode.trim());
		}
		if(bfdes!=null){
			codeDes.setDes(bfdes.trim());
		}
		if(codeDes.getTyp()!=null && "16".equals(codeDes.getTyp())){//新增物资参考类别
			Typoth typoth = new Typoth();
			typoth.setCode(codeDes.getCode());
			typoth.setDes(codeDes.getDes());
			typoth.setLocked("N");
			typoth.setAcct(session.getAttribute("ChoiceAcct").toString());
			str = supplyService.insertTypoth(typoth);
		}else{//所有通用
			codeDesService.updateCodedesPX(codeDes);//修改排序列
			str=codeDesService.saveCodeDes(codeDes,page);
		}
		modelMap.put("pageobj", page);		
		modelMap.put("str", str);
		//如果有已存在，返回添加页面
		if("codeError".equals(str) || "desError".equals(str) || "allError".equals(str)){
			//modelMap.put("parent_id", codeDes.getParent_id());
			modelMap.put("typ", codeDes.getTyp());
			modelMap.put("codeDes", codeDes);
			inout=CodeDesConstants.SAVE_CODEDES;
		}else{//否则执行添加
			inout=StringConstant.ACTION_DONE;
		}
		return new ModelAndView(inout,modelMap);
	}
	
	
	/**
	 * 打开新增税率页面 jinshuai 20160429
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addTax")
	public ModelAndView addTax(ModelMap modelMap) throws Exception{
		return new ModelAndView(CodeDesConstants.SHOW_TAX_ADD,modelMap);
	}
	
	/**
	 * 保存税率信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveTaxAdd")
	public ModelAndView saveTaxAdd(ModelMap modelMap,Page page,Tax tax,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存的时候，去掉一下空格
		String bfcode = tax.getCode();
		String bfdes = tax.getTaxdes();
		if(bfcode!=null){
			tax.setCode(bfcode.trim());
		}
		if(bfdes!=null){
			tax.setTaxdes(bfdes.trim());
		}
		//保存税率
		String str = codeDesService.saveTaxMes(tax);
		String view = StringConstant.ACTION_DONE;
		if("taxError".equals(str)||"desError".equals(str)){
			view = CodeDesConstants.SHOW_TAX_ADD;
		}
		modelMap.put("str", str);
		modelMap.put("tax", tax);
		modelMap.put("pageobj", page);
		return new ModelAndView(view,modelMap);
	}
	
	/**
	 * 打开修改税率页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateTax")
	public ModelAndView updateTax(ModelMap modelMap,Page page,Tax tax) throws Exception{
		//修改主页面
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//参数
		Tax paramTax = new Tax();
		paramTax.setId(tax.getId());
		//根据ID得到税率信息传到前台
		modelMap.put("tax",codeDesService.findTaxRateList(paramTax, page).get(0));
		return new ModelAndView(CodeDesConstants.SHOW_TAX_UPDATE,modelMap);
	}
	
	/**
	 * 修改税率信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveTaxByUpdate")
	public ModelAndView saveTaxByUpdate(ModelMap modelMap,Page page,Tax tax) throws Exception{
		//接收参数修改系统编码
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存的时候，去掉一下空格
		String bfcode = tax.getCode();
		String bfdes = tax.getTaxdes();
		if(bfcode!=null){
			tax.setCode(bfcode.trim());
		}
		if(bfdes!=null){
			tax.setTaxdes(bfdes.trim());
		}
		//更新税率信息
		String str = codeDesService.updateTaxMes(tax);
		String view = StringConstant.ACTION_DONE;
		if("taxError".equals(str)||"desError".equals(str)){
			view = CodeDesConstants.SHOW_TAX_UPDATE;
		}
		modelMap.put("str", str);
		modelMap.put("tax", tax);
		modelMap.put("pageobj", page);
		return new ModelAndView(view,modelMap);
	}
	
	
	/**
	 * 删除税率
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteTax")
	public ModelAndView deleteTax(ModelMap modelMap,Page page, String ids) throws Exception{
		//根据单号和TYP进行删除操作
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//可以多项删除
		ids = CodeHelper.replaceCode(ids);
		//删除税率信息
		codeDesService.deleteTaxMes(ids);
		modelMap.put("pageobj", page);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 保存系统编码信息--报货类别
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByAdd_typ")
	public ModelAndView save_typ(ModelMap modelMap,Page page,CodeDes codeDes) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//标识方向的变量
		String inout="";
		codeDesService.updateCodedesPX(codeDes);//修改排序列
		String str=codeDesService.saveCodeDes(codeDes,page);
		modelMap.put("pageobj", page);		
		modelMap.put("str", str);
		//如果有已存在，返回添加页面
		if("codeError".equals(str) || "desError".equals(str) || "allError".equals(str)){
			//modelMap.put("parent_id", codeDes.getParent_id());
			modelMap.put("typ", codeDes.getTyp());
			modelMap.put("codeDes", codeDes);
			inout=CodeDesConstants.SAVE_CODEDES;
		}else{//否则执行添加
			inout=StringConstant.ACTION_DONE;
		}
		return new ModelAndView(inout,modelMap);
	}
	
	/**
	 * 打开修改页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update")
	public ModelAndView update(ModelMap modelMap,Page page,CodeDes codeDes) throws Exception
	{
		//修改主页面
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(codeDes.getTyp()!=null && "16".equals(codeDes.getTyp())){
			List<Typoth> typoths = supplyService.findTypoth(codeDes.getCode(),null);
			if(typoths!=null && typoths.size()!=0){
				modelMap.put("codeDes",typoths.get(0));
				modelMap.put("fz", "1");
			}
		}else{
			modelMap.put("codeDes",codeDesService.findCodeDesByCode(codeDes));
		}
		return new ModelAndView(CodeDesConstants.UPDATE_CODEDES,modelMap);
	}
	
	/**
	 * 打开修改页面--报货类别
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/update_typ")
	public ModelAndView update_typ(ModelMap modelMap,Page page,CodeDes codeDes) throws Exception
	{
		//修改主页面
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("codeDes",codeDesService.findCodeDesByCode_typ(codeDes));
		return new ModelAndView(CodeDesConstants.UPDATE_CODEDES_T,modelMap);
	}
	
	/**
	 * 修改系统编码信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate")
	public ModelAndView saveByUpdate(ModelMap modelMap,Page page,String oldCode, CodeDes codeDes) throws Exception
	{
		//接收参数修改系统编码
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//保存的时候，去掉一下空格
		String bfcode = codeDes.getCode();
		String bfdes = codeDes.getDes();
		if(bfcode!=null){
			codeDes.setCode(bfcode.trim());
		}
		if(bfdes!=null){
			codeDes.setDes(bfdes.trim());
		}
		HashMap<String, Object> codeDesMap=new HashMap<String, Object>();
		codeDesMap.put("codeDes", codeDes);
		codeDesMap.put("oldCode", oldCode);
		String view = StringConstant.ACTION_DONE;
		String returnStr = "";
		if(codeDes.getTyp()==null){//修改物资参考类别
			Typoth typoth = new Typoth();
			typoth.setCode(oldCode);
			typoth.setDes(codeDes.getDes());
			modelMap.put("codeDes", typoth);
			modelMap.put("fz", "1");
			returnStr = supplyService.updateTypoth(typoth);
		}else{
			modelMap.put("codeDes", codeDes);
			codeDes.setCode(oldCode);
			codeDesService.updateCodedesPx(codeDes);//修改排序列
			returnStr = codeDesService.updateCodeDes(codeDesMap);
		}
		modelMap.put("str", returnStr);
		modelMap.put("typ", codeDes.getTyp());
		//如果有已存在，返回添加页面
		if("desError".equals(returnStr)){
			view=CodeDesConstants.UPDATE_CODEDES;
		}
		modelMap.put("pageobj", page);		
		return new ModelAndView(view,modelMap);
	}
	
	/**
	 * 修改系统编码信息====用于报货类别页面
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveByUpdate_typ")
	public ModelAndView saveByUpdate_typ(ModelMap modelMap,Page page,String oldCode, CodeDes codeDes) throws Exception
	{
		//接收参数修改系统编码
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		HashMap<String, Object> codeDesMap=new HashMap<String, Object>();
		codeDesMap.put("codeDes", codeDes);
		codeDesMap.put("oldCode", oldCode);
		codeDes.setCode(oldCode);
		codeDesService.updateCodedesPx(codeDes);//修改排序列
		codeDesService.updateCodeDes(codeDesMap);
		modelMap.put("pageobj", page);		
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 删除记录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public ModelAndView delete(ModelMap modelMap,Page page,CodeDes codeDes) throws Exception
	{
		//根据单号和TYP进行删除操作
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(codeDes.getTyp()!=null && "16".equals(codeDes.getTyp())){//删除物资参考类型
			String[] code = codeDes.getCode().split(",");
			for(String cod:code){
				Typoth typoth = new Typoth();
				typoth.setCode(cod);
				supplyService.deleteTypoth(typoth);
			}
		}else{
			List<CodeDes> list = codeDesService.getAllCodeDes(codeDes);//获取所有要删除的编码
			codeDesService.deleteCodeDes(codeDes.getCode(),codeDes.getTyp());
			for (int i = 0; i < list.size(); i++) {
				CodeDes cd = list.get(i);
				codeDesService.updateCodedespx(cd);//删除之后重新排序
			}
		}
//		Map<String,Integer> map = new HashMap<String,Integer >();
//		map.put("min", list.get(0).getBeatsequence());
//		map.put("max", list.get(list.size()-1).getBeatsequence());
//		map.put("dif", list.size());
		modelMap.put("pageobj", page);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 编码名称唯一验证
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "getDes")
	@ResponseBody
	public int getDes(CodeDes codeDes) throws CRUDException{
		return codeDesService.getDes(codeDes);
	}
	
	
	/**
	 * 修改时编码名称唯一验证
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "getDesu")
	@ResponseBody
	public int getDesu(CodeDes codeDes) throws CRUDException{
		return codeDesService.getDesu(codeDes);
	}
	
	/**
	 * 查询所有编码
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteyh")
	@ResponseBody
	public int deleteyh(String code,String typ) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return codeDesService.deleteyh(code,typ);
	}
	
	/**
	 * 获取所单据类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findAllBillType")
	@ResponseBody
	public Object findAllBillType(String codetyp,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String locale = session.getAttribute("locale").toString();
		CodeDes codeDes = new CodeDes();
		codeDes.setLocale(locale);
		codeDes.setCodetyp(codetyp);
		return codeDesService.findDocumentType(codeDes);
	}
	/**
	 * 供应商类型
	 */
	@RequestMapping(value="/findAllDeliverType")
	@ResponseBody
	public Object findAllDeliverType(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		codeDes.setLocale("zh_CN");
		Map<String, List<CodeDes>> map = new HashMap<String, List<CodeDes>>();
		map.put("DeliverTypeList", codeDesService.findCodeDes(codeDes));
//		JSONObject  jsonObject = JSONObject.fromObject(codeDesService.findCodeDes(codeDes));  
		return JSONObject.fromObject(map).toString();
	}
	/**
	 * 物资属性
	 */
	@RequestMapping(value="/findAllAttribute")
	@ResponseBody
	public Object findAllAttribute(HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_ATTRIBUTE));// 
		codeDes.setLocale("zh_CN");
		return codeDesService.findCodeDes(codeDes);
//		map.put("AttributeList", codeDesService.findCodeDes(codeDes));
//		JSONObject  jsonObject = JSONObject.fromObject(codeDesService.findCodeDes(codeDes));  
//		return JSONObject.fromObject(map).toString();
	}
}
