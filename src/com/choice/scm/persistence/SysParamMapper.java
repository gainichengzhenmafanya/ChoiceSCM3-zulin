package com.choice.scm.persistence;

import java.util.List;

import com.choice.framework.redis.cache.SysParam;

public interface SysParamMapper {
	
	public List<SysParam> getAllParams(SysParam param);

	public List<SysParam> getAll();
	
	public SysParam getParamByCode(SysParam param);
	
	public int update(SysParam param);
	
}
