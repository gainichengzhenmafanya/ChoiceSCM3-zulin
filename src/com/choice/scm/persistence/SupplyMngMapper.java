package com.choice.scm.persistence;

import java.util.HashMap;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.CntUse;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;

public interface SupplyMngMapper {
	
	/**
	 * 更新供应商信息
	 * @param map
	 */
	public void saveDelByUpd(HashMap<String, Object> map);
	
	/**
	 *更新虚拟物料信息 
	 * @map
	 */
	public void saveSupply_xByUpd(HashMap<String, Object> map);
	
	/**
	 *更新实际物料信息 
	 * @map
	 */
	public void saveSupplyByUpd(HashMap<String, Object> map);
	
	/**
	 * 更新仓位信息
	 * @param map
	 */
	public void savePstByUpd(HashMap<String, Object> map);
	
	/**
	 * 更新货架信息
	 * @param map
	 */
	public void savePst1ByUpd(HashMap<String, Object> map);
	
	/**
	 * 更新ABC信息
	 * @param map
	 */
	public void saveABCByUpd(HashMap<String, Object> map);
	
	/**
	 * 更新最大库存
	 * @param supply
	 */
	public void updateStock(Supply supply);
	
	/**
	 * 更新库存量
	 * @param map
	 */
	public void updateCnt(HashMap<String, Object> map);
	
	/**
	 * 更新日均耗用
	 * @param cntUse
	 * @return
	 */
	public void updateCntUse(CntUse cntUse);
	
	/**
	 * 门店更新日均耗用
	 * @param cntUse
	 * @return
	 */
	public void updateCntUseFirm(CntUse cntUse);
	
	/**
	 * 仓库更新日均耗用
	 * @param cntUse
	 */
	public void updateCntUsePositn(CntUse cntUse);
	
	/**
	 * 更新报货分类
	 * @param spcode 物资编码用逗号隔开
	 * @param code 报货分类编码
	 * @throws CRUDException 
	 */
	public void updateTypEas(HashMap<String, Object> map);
	
	/**
	 * 更新最大库存
	 * @param supply
	 */
	public void updateStockFirm(PositnSpcode positnSpcode);
	
	/**
	 * 更新物资属性信息
	 * @param map
	 */
	public void saveModifyAttribute(HashMap<String, Object> map);
	
	/**
	 * 更新物资加工间是否领料的属性
	 * @param map
	 */
	public void saveUpdateExkc(HashMap<String, Object> map);
}
