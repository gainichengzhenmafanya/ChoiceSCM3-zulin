package com.choice.scm.persistence.mis;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;

public interface ChkinmZbMapper {
	
	/**
	 * 查询入库单号序列最大值
	 */
	public Integer getMaxChkinno();

	/**
	 * 查询所有入库单
	 * @param chkinm
	 * @return
	 */
	public List<Chkinm> findAllChkinm(Chkinm chkinm);
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinput(Map<String,Object> map);
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinputNew(Map<String,Object> map);
	/**
	 * 添加入库单
	 * @param chkinm
	 */
	public void saveChkinm(Chkinm chkinm);
	/**
	 * 修改入库单
	 * @param chkinm
	 */
	public void updateChkinm(Chkinm chkinm);
	/**
	 * 审核入库单
	 * @param chkinm
	 */
	public void checkChkinm(Chkinm chkinm);
	/**
	 * 查询单条入库单
	 * @param chkind
	 */
	public  Chkinm  findChkinmByid(Chkinm chkinm);
	/**
	 * 删除入库单
	 * @param listId
	 */
	public void deleteChkinm(List<String> listChkinno);
	
	/**
	 * 查询默认仓位
	 * @param chkinm
	 */
	public List<SpCodeMod> getPositn(SpCodeMod spCodeMod);
	public List<Supply> getPositn1(Supply supply);
	
	/**
	 * 查询默认仓位是否正确
	 * @param chkinm
	 */
	public List<SpCodeMod> chkPositn(SpCodeMod spCodeMod);
	
	/**
	 * 获取入库数据
	 * @param chkinm
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch);

	/**
	 *  查询入库金额汇总
	* @param map
	* @return
	 */
	public Chkinm findAllChkinmTotal(Map<String, Object> map);

	/**
	 * 根据主键字符串查找数据
	* @param replaceCode
	* @return
	 */
	public List<Chkinm> findChkinmByIds(@Param("chkinno")String replaceCode);
	
	/**
	 * 获取直发冲消数据
	 * @param spbatch
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch);
	/**
	 * 获取直发冲消数据--分店
	 * @param spbatch
	 */
	public List<Spbatch> addChkinmzfByCx_x(Spbatch spbatch);

}
