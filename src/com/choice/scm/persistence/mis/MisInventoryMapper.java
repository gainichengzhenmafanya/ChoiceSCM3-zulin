package com.choice.scm.persistence.mis;


import java.util.List;

import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.Positndaypan;
import com.choice.scm.domain.SupplyAcct;

public interface MisInventoryMapper {

	/**盘点  查询list
	 * @return
	 */
	public List<Inventory> findAllInventory(Inventory inventory);
	
	/**
	 * 保存盘点
	 * @param inventory
	 * @return
	 */
	public void updateInventory(Inventory inventory);

	/***
	 * mis 盘点
	 * @author wjf
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory);

	/***
	 * mis 盘点 总数
	 * @param inventory
	 * @return
	 */
	public int findAllInventoryCount(Inventory inventory);

	/***
	 * 删除positndaypan 表
	 * @param pd
	 */
	public void deletePositndaypan(Positndaypan pd);

	/***
	 * 往positndaypan 表里插入数据
	 * @param pd
	 */
	public void insertPositndaypan(Positndaypan pd);
	
	/***
	 * 修改positndaypan
	 * @param pd
	 */
	public void updatePositndaypan(Positndaypan pd);
	
	/***
	 * 查物资进出明细 最后一次进价
	 * @param sa
	 * @return
	 */
	public Double findLastPriceBySupplyAcct(SupplyAcct sa);
}
