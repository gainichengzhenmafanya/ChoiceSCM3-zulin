package com.choice.scm.persistence.mis;

import java.util.List;

import com.choice.scm.domain.Chkind;

public interface MisChkindMapper {

	/**
	 * 查询所有入库单从表
	 * @param chkind
	 * @return
	 */
	public List<Chkind> findAllChkind(Chkind chkind);
	/**
	 * 添加入库单从表
	 * @param chkind
	 */
	public void saveChkind(Chkind chkind);
	/**
	 * 删除入库单从表
	 * @param chkind
	 */
	public void deleteChkindByChkinno(List<String> listChkinno);
	
}
