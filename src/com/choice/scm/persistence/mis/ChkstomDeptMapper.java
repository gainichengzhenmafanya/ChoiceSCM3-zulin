package com.choice.scm.persistence.mis;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.Chkstom;

public interface ChkstomDeptMapper {
	
	/**
	 * 查询所有的报货单
	 * @param chkstom
	 * @return
	 */
	public List<Chkstom> findChkstom(Chkstom chkstom);
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom);
	
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxChkstono();
	
	/**
	 * 添加报货单主表
	 * @param chkstom
	 */
	public void saveNewChkstom(Chkstom chkstom);

	/**
	 * 更新出库单
	 * @param chkstomMap
	 */
	public void updateChkstom(Chkstom chkstom);
	
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<Object, Object> chkstomMap);
	
	/**
	 * 删除
	 */
	public void deleteChk(Chkstom chkstom);
}
