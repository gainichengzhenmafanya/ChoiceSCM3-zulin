package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkpaym;

public interface ChkpaymMapper {
	
	/**
	 * 查询所有的单
	 * @param chkpaym
	 * @return
	 */
	public List<Chkpaym> findChkpaym(Chkpaym chkpaym);
	
	/**
	 * 根据单号查询
	 * @param chkpaym
	 * @return
	 */
	public Chkpaym findByChkno(Chkpaym chkpaym);
	
	/**
	 * 查询最大单号
	 * @return
	 */
	public int getMaxChkno();
	
	/**
	 * 查询当前凭证号
	 * @return
	 */
	public String findCurVouno();
	/**
	 * 新增报销单主表
	 * @param chkpaym
	 */
	public void saveNewChkpaym(Map<String,Object> chkpaymMap);
	
	/**
	 * 根据关键字查询报销单
	 * @param chkpaym
	 * @return
	 */
	public List<Chkpaym> findByKey(HashMap<Object, Object> chkpaymMap);
	/**
	 * 删除报销单主表
	 * @param chkpaym
	 */
	public void deleteChkpaym(Map<String,Object> map);
}
