package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.domain.Typoth;

public interface GrpTypMapper {

	/**
	 * 通过编号查询物资大类
	 * @param grpTyp
	 * @return
	 */
	public GrpTyp findGrpTypByCode(@Param(value="code")String code,@Param(value="acct")String acct);
	
	/**
	 * 通过名称查询物资大类
	 * @param grpTyp
	 * @return
	 */
	public List<GrpTyp> findGrpTypByDes(@Param(value="des")String code);
	
	/**
	 * 通过编号查询物资中类
	 * @param typ
	 * @return
	 */
	public List<Grp> findGrpByCode(@Param(value="code")String code,@Param(value="acct")String acct);
	
	/**
	 * 通过编号查询物资小类
	 * @param grp
	 * @return
	 */
	public List<Typ> findTypByCode(@Param(value="code")String code,@Param(value="acct")String acct);
	/**
	 * 通过条件查询物资大类     中类      小类列表
	 * @param grpTyp  Grp  Typ
	 * @return
	 */
	public List<GrpTyp> findAllGrpTyp(GrpTyp grpTyp);
	public List<Grp> findAllGrp(@Param(value="parent")String parent);
	public List<Typ> findAllTyp(@Param(value="parent")String parent);
	
	/**
	 * 通过条件(acct)查询物资大类     中类      小类列表
	 * @param acct
	 * @return
	 */
	public List<GrpTyp> findAllGrpTypA(@Param(value="acct")String acct);
	public List<Grp> findAllGrpA(@Param(value="parent")String parent,@Param(value="acct")String acct);
	public List<Typ> findAllTypA(@Param(value="parent")String parent,@Param(value="acct")String acct);
	
	/**
	 * 查询所有的物资大类信息，并标记当前账户已关联的物资大类，用于界面显示
	 * @param accountId
	 * @return
	 */
	public List<GrpTyp> findGrpTypList(String accountId);
	
	/**
	 * 保存物资大类信息
	 * @param grpTyp
	 */
	public void saveGrpTyp(GrpTyp grpTyp);
	/**
	 * 修改物资大类信息
	 * @param grpTyp
	 */
	public void updateGrpTyp(GrpTyp grpTyp);
	/**
	 * 删除物资大类信息
	 * @param listId
	 */
	public void deleteGrpTyp(List<String> listCode);

	/**
	 * 保存物资中类信息
	 * @param grp
	 */
	public void saveGrp(Grp grp);
	/**
	 * 修改物资中类信息
	 * @param grp
	 */
	public void updateGrp(Grp grp);
	/**
	 * 删除物资中类信息
	 * @param listCode
	 */
	public void deleteGrp(List<String> listCode);

	/**
	 * 保存物资小类信息
	 * @param typ
	 */
	public void saveTyp(Typ typ);
	/**
	 * 修改物资小类信息
	 * @param typ
	 */
	public void updateTyp(Typ typ);
	/**
	 * 删除物资小类信息
	 * @param listCode
	 */
	public void deleteTyp(List<String> listCode);
	/**
	 * 更新物资中类中物资大类信息
	 * @param grpTyp
	 */
	public void updateGrpTypOfGrp(GrpTyp grpTyp);
	/**
	 * 更新物资小类中物资大类信息
	 * @param grpTyp
	 */
	public void updateGrpTypOfTyp(GrpTyp grpTyp);
	/**
	 * 更新物资小类中物资中类信息
	 * @param grp
	 */
	public void updateGrpOfTyp(Grp grp);

	/**
	 * 判断选中的类别是否已经被引用===大类
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int checkGrpTyp(@Param(value="code")String code);

	/**
	 * 判断选中的类别是否已经被引用===中类
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int checkGrp(@Param(value="code")String code);

	/**
	 * 判断选中的类别是否已经被引用===小类
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public int checkTyp(@Param(value="code")String code);
	
	/**
	 * 根据物资大类删除物资小类信息
	 * @param grpTyp
	 */
	public void deleteTypByGrpTyp(List<String> listCode);
	/**
	 * 根据物资中类删除物资小类信息
	 * @param grp
	 */
	public void deleteTypByGrp(List<String> listCode);
	/**
	 * 根据物资大类删除物资中类信息
	 * @param grpTyp
	 */
	public void deleteGrpByGrpTyp(List<String> listCode);
	
	public List<Typ> findByDes(@Param(value="code")String code,@Param(value="acct")String acct,@Param(value="des")String des);
	
	/***
	 *根据多个选中的大类查询被选中大类下的所有物资 
	 */
	public List<Supply> findGrp(Map<String, Object> map);
	
	/***
	 *根据多个选中的中类查询被选中中类下的所有物资
	 */
	public List<Supply> findTyp(Map<String, Object> map);
	
	/**
	 * 根据多个选中的小类查询被选中小类下的所有物资
	 */
	public List<Supply> findSupplyByTyp(Map<String, Object> map);

	/***
	 * 根据条件查询所有大中类  供应商类别报表用 wjf
	 * @param grpTyp
	 */
	public List<GrpTyp> findAllGrpTypByCondition(GrpTyp grpTyp);

	/***
	 * 查询参考类别 wjf
	 * @param typoth
	 * @return
	 */
	public List<Typoth> findTypoth(Typoth typoth);

	/***
	 * 新增参考类别 wjf
	 * @param typoth
	 */
	public void saveTypoth(Typoth typoth);

	/***
	 * 修改参考类别 wjf
	 * @param typoth
	 */
	public void updateTypoth(Typoth typoth);

	/***
	 * 删除参考类别 wjf
	 * @param typoth
	 */
	public void deleteTypoth(Typoth typoth);
	
	/**
	 * 入库类别汇总专用 (查询所有中类)
	 * @param grp
	 * @return
	 */
	public List<Grp> selectAllGrptypByCode(Map<String,Object> map);
	
	/**通过des查询全部大类中类小类
	 * @param grp
	 * @return
	 */
	public List<GrpTyp> findTypeByDes(GrpTyp grpTyp);
}
