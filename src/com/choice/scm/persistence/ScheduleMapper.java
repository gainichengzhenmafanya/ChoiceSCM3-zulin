package com.choice.scm.persistence;

import java.util.List;

import com.choice.framework.domain.system.Account;
import com.choice.scm.domain.Schedule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.ScheduleStore;

public interface ScheduleMapper {

	 
	/**
	 * 查询配送班表 list
	 */
	public List<Schedule> findSchedule(Schedule schedule);
	public Schedule findScheduleById(Schedule schedule);
	/**
	 * 查询当前账号是否是系统管理员角色
	 */
	public List<Account> findAccount(Account account);
	/**
	 * 添加 左边 配送路线
	 */
	public void saveSchedule(Schedule schedule);
	/**
	 * 修改配送路线 左边
	 */
	public void updateSchedule(Schedule schedule);
	/**
	 * 刪除 左边 配送路线
	 */
	public void deleteSchedule(Schedule schedule);
	
	/**
	 * 根据配送ID查询该配送线下的所有分店
	 */
	public List<ScheduleStore> findFirmByScheduleId(Schedule schedule);
	/**
	 * 刪除 左边 配送路线下所有分店
	 */
	public void deleteFirmByScheduleId(Schedule schedule);
	/**
	 * 根据分店ID查询对应路线
	 */
	public void saveFirmByScheduleId(ScheduleStore scheduleStore);
	
	/**
	 * 根据配送ID查询从表
	 */
	public List<ScheduleD> findDetailsByScheduleId(ScheduleD scheduled);
	
	/**
	 * 根据配送ID查询从表
	 */
	public List<ScheduleD> findDetails(ScheduleD scheduleD);
	
	/**
	 * 刪除 右边  配送班表明细
	 */
	public void deleteScheduleD(ScheduleD scheduleD);
	
	/**
	 * 添加 右边  配送班表明细
	 */
	public void saveScheduleD(ScheduleD scheduleD);
	
	/**
	 * 修改 右边  配送班表明细
	 */
	public void updateScheduleD(ScheduleD scheduleD);
	
	/**
	 * 批量删除配送班表
	 */
	public void batchDeleteScheduleD(ScheduleD scheduleD); //wangjie
	
	/**
	 * 复制班表
	 * @param schedule
	 */
	public void copySchedule(Schedule schedule);
	
	/**
	 * 查询分店下的所有班表
	 * @param schedule
	 * @return
	 */
	public List<Schedule> findFirmScheduleList(Schedule schedule);
	
	/**
	 * 删除分店班表
	 * @param schedule
	 */
	public void deleteFirmSchedule(ScheduleStore schedule);
	
 
}
