package com.choice.scm.persistence;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.AccountDis;

/***
 * 报货分拨对用户特殊权限mapper
 * @author 王吉峰
 *
 */
public interface AccountDisMapper {


	/***
	 * 根据用户ID查询
	 * @param accountId
	 * @return
	 */
	public AccountDis getAccountDisByAccountId(@Param(value="accountId")String accountId);

	/***
	 * 新增
	 * @param accountDis
	 */
	public void addAccountDis(AccountDis accountDis);

	/**
	 * 修改
	 * @param accountDis
	 */
	public void updAccountDis(AccountDis accountDis);
}
