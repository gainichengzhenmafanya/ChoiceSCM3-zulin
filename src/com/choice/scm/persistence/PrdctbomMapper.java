package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Product;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;

public interface PrdctbomMapper {


	/**
	 * 模糊查询半成品
	 * @param prdct
	 * @return
	 */
	public List<Supply> findPrdct(Supply prdct);
	
	/**
	 * 模糊查询半成品
	 * @param prdct
	 * @return
	 */
	public List<Supply> findPrdctFirm(Supply prdct);
	
	/**
	 * 根据缩写码查询半成品
	 * @param key
	 * @return
	 */
	public List<Supply> findByKey(HashMap<String, Object> map);
	public List<Supply> findByKeyFirm(HashMap<String, Object> map);
	/**
	 * 根据code查询原材料     _x 是虚拟物料方法
	 * @param prdct
	 * @return
	 */
	public List<Product> findmaterial(Spcodeexm spcodeexm);
	public List<Product> findmaterial_x(Spcodeexm spcodeexm);
	
	/**
	 * 查询物资存在的产品以及物资用量
	 * @param prdct
	 * @return
	 */
	public List<Product> listResult(Product pro);
	
	/**
	 * 多条code   根据code查询原材料
	 * @param map
	 * @return
	 */
	public List<Product> findmaterialBylist(HashMap<String, Object> map);
	
	/**
	 * 生成加工  申购单跳转 分组求和
	 * @param map
	 * @return
	 */
	public List<Product> findSumMaterialBylist(HashMap<String, Object> map);
	
	/**
	 * 生成加工  申购单跳转 分组求和（生成领料单）
	 * @param map
	 * @return
	 */
	public List<Product> findSumMaterialByExplanno(HashMap<String, Object> map);
	
	/**
	 * 原材料操作
	 * @param pro
	 * @return
	 */
	public void saveOrUpdateOrDelPrdctbom(Product pro);
	
	/**
	 * 根据物资编码获取物资名称
	 * @param supply
	 * @return
	 */
	public String getItemNm(Supply supply);
	
	/**
	 * 查询已做成本卡Id   list
	 * @param product
	 * @return
	 */
	public List<Product> findAllSpcodeexmIdList(Product product);
	/**
	 * 删除半成品原材料实际用量明细
	 * @param map
	 */
	public void deleteSpCodeExPriceCnt(Map<String, Object> map);
	/**
	 * 新增半成品原材料的实际用量信息
	 * @param map
	 */
	public void saveSpCodeExPriceCnt(Map<String, Object> map);
	
	/**
	 * 删除主表数据----实际物料
	 * @param costbom
	 */
	public void deleteProductByItemId(Product product);
	
	
	/**
	 * 保存bom ----实际物料
	 * @param costbom
	 */
	public void saveProductByItem(Product product);
}
