package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;

public interface ChkstomMapper {

	/**
	 * 查看上传
	 * @param chkstom
	 * @return
	 */
	public List<Chkstom> findUpload(Chkstom chkstom);
	
	/**
	 * 查看未上传
	 * @return
	 */
	public List<Positn> findNoUpload(Chkstom chkstom);
	
	/**
	 * 查看未上传数目
	 * @return
	 */
	public int findNoUploadCount(Chkstom chkstom);
	
	/**
	 * 查询所有的报货单
	 * @param chkstom
	 * @return
	 */
	public List<Chkstom> findChkstom(Chkstom chkstom);
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom);
	/**
	 * 该分店今天     已审核多少条， 未审核多少条
	 */
	public Chkstom findCountByFirmId(Chkstom chkstom);
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxChkstono();
	
	/**
	 * 添加报货单主表
	 * @param chkstom
	 */
	public void saveNewChkstom(Chkstom chkstom);
	public void saveNewChkstomMis(Chkstom chkstom);
	
	/**
	 * 调用报货单拆单存储过程
	 * @param chkstom
	 */
	public void saveNewChkstom_c(Chkstom chkstom);
	/**
	 * 五芳斋金蝶扣款
	 * @param chkstom
	 */
	public void JOININGCHKSTOM(Chkstom chkstom);
	/**
	 * 更新报货单
	 * @param chkstomMap
	 */
	public void updateChkstom(Chkstom chkstom);
	public void updateChkstomMis(Chkstom chkstom);
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<Object,Object> chkstomMap);
	
	/**
	 * 删除
	 */
	public void deleteChk(Chkstom chkstom);
	
	/**
	 * 删除拆单
	 */
	public void deleteChkM_c(Chkstom chkstom);
	public void deleteChkD_c(Chkstom chkstom);
	/**
	 * 审核
	 */
	public void checkChk(Chkstom chkstom);
	/**
	 * 检查是金额是否充足
	 */
	public double checkSppriceSale(String frim);
	/**
	 * 传入门店编码返回   该门店所在主直拨库编码
	 */
	public String findMainPositnByFirm(String firm);
	
	/**
	 * 更新仓位引用状态
	 * @param chkstom
	 */
	public void updatePositn(Chkstom chkstom);
	
	/**
	 * 更新档口报货单上传状态
	 * @param chkstomMap
	 */
	public void updateChkstomDept(Chkstod chkstod);
	
	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的
	 * @return
	 */
	public int checkYnUnChk(Chkstom chkstom);
	
	/**
	 * 反审核报货单主表 
	 * @return
	 */
	public void updateChkstomByUnchk(Chkstom chkstom);
	
	/**
	 * 反审核报货单从表
	 * @return
	 */
	public void updateChkstodByUnchk(Chkstom chkstom);

	/***
	 * 新的审核存储过程，需要判断申购单从表里的采购数量是不是0  加工间报货会有这种情况 wjf
	 * @param c
	 */
	public void checkChk_new(Chkstom c);

	/***
	 * 更新报货单主表的totalamt
	 * @param chkstom
	 */
	public void updateChkstomTotalamt(Chkstom chkstom);

	/***
	 * sqlserver 专用 wjf2015.3.21
	 * @param chkstod
	 */
	public void updateChkstomDept0(Chkstod chkstod);

	/***
	 * 根据单号查档口报货单号
	 * @param id
	 * @return
	 */
	public List<Chkstom> findChkstonoDept(@Param(value="id")Integer id);

	/***
	 * 更新一级审核人
	 * @param c
	 */
	public void checkChkstom(Chkstom c);
	
	/***
	 * 安全库存报货  
	 * @param chkstom
	 * @return
	 */
	public List<Chkstod> findSafeChkstodList(Chkstom chkstom);

	/***
	 * 分店报货单状态查询
	 * @param chkstom
	 * @return
	 */
	public List<Map<String, Object>> listChkstomState(Chkstom chkstom);

}
