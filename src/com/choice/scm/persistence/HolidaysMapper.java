package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Holiday;

public interface HolidaysMapper {

	/**
	 * 查找全节假日信息
	 * @param holiday
	 * @return
	 */
	public List<Holiday> listHoliday(Holiday holiday);
	public List<Holiday> listHolidayMap(Holiday holiday);
	/**
	 * 保存添加
	 * @param holiday
	 */
	public void saveByAdd(Holiday holiday);
	/**
	 * 	 *查询某个时间是否节假日
	 */
	public Holiday findHolidayByDate(Holiday holiday);
	/**
	 * 删除节假日定义
	 * @param holiday
	 */
	public void deleteHoliday(Holiday holiday);
	/**
	 * 自动生成周六日信息
	 * @param holiday
	 */
	public void saveBycreateHolidayAuto(Holiday holiday);
}
