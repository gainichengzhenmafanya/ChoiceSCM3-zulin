package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnRole;

public interface PositnRoleMapper {
	
	/**
	 * 根据角色 查询  仓位
	 */
	public List<PositnRole> findRolePositnList(String roleId);
	/**
	 * 根据角色id 删除  所有该角色下的仓位权限
	 * @param roleId
	 */
	public void deleteRolePositnByRoleId(String roleId);
	/**
	 * 添加某个角色的仓位权限
	 * @param positnRole
	 */
	public void saveRolePositn(PositnRole positnRole);
	
	/**
	 * 查询所有仓位信息
	 * @param Positn
	 */
	public List<Positn> findPositn(HashMap<String, Object> map);
	/**
	 * 查询所有仓位信息    根据账号
	 * @param Positn
	 */
	public List<Positn> findPositnByAccount(HashMap<String, Object> map);
	/**
	 * 保存  改账号  的所属分店    可能多个账号
	 * @param Positn
	 */
	public void saveFrim(HashMap<String, Object> map);
}
