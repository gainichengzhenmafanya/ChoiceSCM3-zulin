package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.FirmModTime;

public interface FirmModTimeMapper {

	/**
	 * 查询成本卡方案
	 * @param firmModTime
	 */
	public List<FirmModTime> findFirmModTime(FirmModTime firmModTime);
	
	/**
	 * 查询成本卡类型
	 * @param firmModTime
	 */
	public List<CodeDes> findMod(FirmModTime firmModTime);
	
	/**
	 * 新增保存成本卡方案
	 * @param firmModTime
	 */
	public void saveFirmModTime(FirmModTime firmModTime);
	
	/**
	 * 删除成本卡方案
	 * @param firmModTime
	 */
	public void deleteFirmModTime(FirmModTime firmModTime);
	
	/**
	 * 更新成本卡方案
	 * @param firmModTime
	 */
	public void updateFirmModTime(FirmModTime firmModTime);
}
