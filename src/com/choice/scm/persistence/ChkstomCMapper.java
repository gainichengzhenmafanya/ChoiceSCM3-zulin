package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.Chkstom;

public interface ChkstomCMapper {
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom);
	public List<Chkstom> findListByChkstoNo(Chkstom chkstom);
	
	/**
	 * 更改传库状态
	 * @param chkstom
	 */
	public void updateChkstomC(Chkstom chkstom);
	
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<Object,Object> chkstomMap);
}
