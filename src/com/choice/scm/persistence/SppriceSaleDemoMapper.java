package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.SppriceSaleDemo;

public interface SppriceSaleDemoMapper {

	/**
	 * 查询售价单模板
	 * @param sppriceSaleDemo
	 */
	public List<SppriceSaleDemo> findSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo);
	
	/**
	 * 查询单条记录
	 * @param sppriceSaleDemo
	 * @return
	 */
	public SppriceSaleDemo findSppriceSaleDemoOne(SppriceSaleDemo sppriceSaleDemo);
	/**
	 * 查询售价单
	 * @param sppriceSale
	 * @return
	 */
	public List<SppriceSale> findSppriceSale(SppriceSale sppriceSale);

	/**
	 * 查询最大序号
	 * @param ss 
	 * @return
	 */
	public Integer getMaxRec(SppriceSale ss);
		
	/**
	 * 保存新增售价单模板
	 * @param sppriceSaleDemo
	 */
	public void saveByAddSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo);
	
	
	/**
	 * 保存新增售价单
	 * @param sppriceSale
	 */
	public void saveByAddSppriceSale(SppriceSale sppriceSale);
	
	/**
	 * 批量更新--售价单模板
	 * @param sppriceSaleDemo
	 */
	public void updateSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo);
	/**
	 * 批量更新--售价单
	 * @param sppriceSaleDemo
	 */
	public void updateSppriceSale(SppriceSale sppriceSale);
	/**
	 * 删除售价单模板
	 * @param sppriceSaleDemo
	 */
	public void delete(SppriceSaleDemo sppriceSaleDemo);
	
	/**
	 * 删除未审核的报价
	 * @param sppriceSale
	 */
	public void deleteUncheckSpprice(SppriceSale sppriceSale);
	/**
	 * 审核售价单
	 * @param sppriceSale
	 */
	public void checkSppriceSale(SppriceSale sppriceSale);
	
	/**
	 * 全部反审核所有的售价
	 * @param sppriceSale
	 * @return
	 * @throws CRUDException
	 */
	public void reverseCheckSppriceSale(SppriceSale sppriceSale);
	
	/**
	 * 物资售价综合查询
	 * @param conditions
	 * @return
	 */
	public List<SppriceSale> findSppriceSaleByReport(Map<String,Object> map);

	/***
	 * 审核所有售价 wjf2014.11.25
	 * @param sppriceSale
	 */
	public void checkAllSppriceSale(SppriceSale sppriceSale);
}
