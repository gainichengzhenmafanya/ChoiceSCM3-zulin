package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemod_x;

/**
 * 报货单单据模板子表
 * @author 孙胜彬
 */
public interface ChkstoDemodMapper {
	
	/**
	 * 报货单从表查询
	 * @param chkstoDemod
	 * @return
	 */
	public List<ChkstoDemod> listChkstoDemod(ChkstoDemod chkstoDemod);
	/**
	 * 新增报货单从表
	 * @param chkstoDemod
	 */
	public void saveChkstoDemod(ChkstoDemod chkstoDemod);
	/**
	 * 修改报货单从表
	 * @param chkstoDemod
	 */
	public void updateChkstoDemod(ChkstoDemod chkstoDemod);
	/**
	 * 删除报货单从表
	 * @param chkstoDemod
	 */
	public void deleteChkstoDemod(List<String> idList);
	
	/**
	 * 报货单从表查询--虚拟物料
	 * @param chkstoDemod_x
	 * @return
	 */
	public List<ChkstoDemod_x> listChkstoDemod_x(ChkstoDemod_x chkstoDemod_x);
	/**
	 * 新增报货单从表--虚拟物料
	 * @param chkstoDemod_x
	 */
	public void saveChkstoDemod_x(ChkstoDemod_x chkstoDemod_x);
	/**
	 * 修改报货单从表
	 * @param chkstoDemod
	 */
	public void updateChkstoDemod_x(ChkstoDemod_x chkstoDemod_x);
	/**
	 * 删除报货单从表
	 * @param chkstoDemod
	 */
	public void deleteChkstoDemod_x(List<String> idList);
	
}
