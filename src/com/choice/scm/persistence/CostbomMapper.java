package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.ItemPrgm;
import com.choice.scm.domain.Pubitem;

public interface CostbomMapper {
	
	/**
	 * 查询传入某道菜的     成本
	 */
	public Pubitem findPubitemCostByItem(Pubitem pubitem);
	
	/**
	 * 查询所有菜品
	 * @param map
	 * @return
	 */
	public List<Pubitem> findAllPubitem(Pubitem pubitem);
	
	/**
	 * 所有类别
	 * @param Pubgrp
	 * @return
	 */
	public List<Costbom> findAllPubgrp();
	
	/**
	 * 查询菜谱方案
	 * @return
	 */
	public List<ItemPrgm> findItemPrgm();
	
	/**
	 * 所有类别
	 * @param Pubgrp
	 * @return
	 */
	public List<Costbom> findAllPubgrp_boh();
	public List<Costbom> findAllPubgrp_pos();
	/**
	 * 查询菜谱方案
	 * @return
	 */
	public List<ItemPrgm> findItemPrgm_boh();
	public List<ItemPrgm> findItemPrgm_pos();
	
	/**
	 * 查询所有菜品
	 * @param map
	 * @return
	 */
	public List<Pubitem> findAllPubitem_boh(Pubitem pubitem);
	
	/**
	 * 查询所有菜品
	 * @param map
	 * @return
	 */
	public List<Pubitem> findAllPubitem_pos(Pubitem pubitem);
	
	/**
	 * 查询成本卡类型
	 * @return
	 */
	public List<CodeDes> findMod();
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Costbom> findAllCostbombomByLeftId(Costbom costbom);
	
	/**
	 * 模糊查询(虚拟物料)
	 * @param map
	 * @return
	 */
	public List<Costbom> findAllCostbombomByLeftId_x(Costbom costbom);
	
	/**
	 * 查询右侧下方(虚拟物料)
	 * @param map
	 * @return
	 */
	public List<Costbom> getDownCostbom(Costbom costbom);
	/**
	 * 查询右侧下方(替代物料)
	 * @param map
	 * @return
	 */
	public List<Costbom> getDownCostbom_n(Costbom costbom);
	
	/**
	 * 查询已做成本卡Id   list
	 * @param costbom
	 * @return
	 */
	public List<Costbom> findAllCostdtlmIdList();
	
	/**
	 * 查询已做成本卡Id   list 虚拟
	 * @param costbom
	 * @return
	 */
	public List<Costbom> findAllCostdtlmIdList_x();
	
	/**
	 * 增 删 改 方法
	 * @param costbom
	 */
	public  void saveOrUpdateOrDelCostbom(Costbom costbom);
	/**
	 * 增 删 改 方法  替换物料   
	 * @param costbom
	 */
	public  void saveOrUpdateOrDelCostbom_n(Costbom costbom);
	
	/**
	 * 增 删 改 方法(虚拟物料)
	 * @param costbom
	 */
	public  void saveOrUpdateOrDelCostbom_x(Costbom costbom);
	
	/**
	 * 保存菜品BOM主表信息
	 */
	public void saveCostdtlm(Costbom costbom);
	
	/**
	 *保存 
	 * 
	 */
	public void saveCostbom(Costbom costbom);
	
	/**
	 *修改(菜品BOM虚拟物料修改) 
	 */
	public void updateCostbom(Costbom costbom);
	
	/**
	 *数据保存到虚拟物料成本卡明细表
	 */
	public void saveToCostdtl_x_d(Costbom costbom);
	
	/**
	 * 修改成本卡
	 * @param costbom
	 */
	public void checkCostbom(Costbom costbom);
	/**
	 * 查询单条成本卡
	 * @param chkind
	 */
	public  Costbom  findCostbomByid(Costbom costbom);
	/**
	 * 查询已做成本卡菜品数据
	 * @param chkind
	 */
	public List<Costbom> findCostBomList();
	
	/**
	 * 批量删除菜品------主表实际物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbom(Map<String,Object> map);
	
	/**
	 * 批量删除菜品------主表实际物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbomd(Map<String,Object> map);
	
	/**
	 * 批量删除菜品------主表虚拟物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbom_x(Map<String,Object> map);
	
	/**
	 * 批量删除菜品------子表虚拟物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbom_x_d(Map<String,Object> map);
	
	
	/*
	 * 获取所有的成本卡类型
	 */
	public List<CodeDes> selectCodedes();
	
	/**
	 * 查询要复制的成本卡--主表----虚拟物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostbomByItem(Costbom costbom);
	
	/**
	 * 查询要复制的成本卡--子表----虚拟物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostbomByItem_d(Costbom costbom);
	
	/**
	 * 查询要复制的成本卡--主表----实际物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostbomByItemId(Costbom costbom);
	
	/**
	 * 查询要复制的成本卡--子表----实际物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostbomByItemId_d(Costbom costbom);
	
	/**
	 * 按照类别查询bom----虚拟物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostbomByMods(Costbom costbom);
	
	/**
	 * 按照类别查询bom----实际物料
	 * @param costbom
	 * @return
	 */
	public List<Costbom> selectCostBomByMods(Costbom costbom);
	
	/**
	 * 删除主表数据----虚拟物料
	 * @param costbom
	 */
	public void deleteCostbomByItem(Costbom costbom);
	
	/**
	 * 删除子表数据----虚拟物料
	 * @param costbom
	 */
	public void deleteCostbomByItem_d(Costbom costbom);
	
	/**
	 * 删除主表数据----实际物料
	 * @param costbom
	 */
	public void deleteCostbomByItemId(Costbom costbom);
	
	/**
	 * 删除子表数据----实际物料
	 * @param costbom
	 */
	public void deleteCostbomByItemId_d(Costbom costbom);
	
	/**
	 * 保存bom ----实际物料
	 * @param costbom
	 */
	public void saveCostbombomByItem(Costbom costbom);
	
	/**
	 * 更新物资编码的单位
	 * @return
	 */
	public void updateUnit(Costbom costbom);

	/***
	 * 批量复制costdtlm
	 * @param dc
	 */
	public void copyCostdtlm(Costbom dc);
	
	/***
	 * 批量复制costdtl
	 * @param dc
	 */
	public void copyCostdtl(Costbom dc);

	/***
	 * 
	 * @param vcode
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> getCostDtlForChkPos(Map<String,Object> cost);
}
