package com.choice.scm.persistence;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.ExplanD;
import com.choice.scm.domain.ExplanM;
import com.choice.scm.domain.ExplanSpcode;

public interface ExplanMMapper {
	
	/**
	 * 查询加工单号序列最大值
	 */
	public Integer getMaxExplanNo();

	/**
	 * 查询加工单号序列最大值
	 */
	public Integer getMaxExplanDNo();
	
	/**
	 * 查询加工单号序列最大值
	 */
	public Integer getMaxExplanLLNo();
	
	/**
	 * 查询所有加工单
	 * @param explanM
	 * @return
	 */
//	public List<ExplanM> findAllExplan(ExplanM explanM);
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<ExplanM> findAllExplanMByinput(Map<String,Object> map);
	
	/**
	 * 查询报货单单据模板子表
	 * @param map
	 * @return
	 */
	public List<ExplanD> findAllExplanD(ExplanD explanD);
	
	/**
	 * 查询单条加工单
	 * @param explanD
	 */
	public  ExplanM  findExplanMByid(ExplanM explanM);
	
	/**
	 * 添加加工单
	 * @param explanM
	 */
	public void saveExplanM(ExplanM explanM);
	
	/**
	 * 添加加工单从表
	 * @param explanD
	 */
	public void saveExplanD(ExplanD explanD);
	
	/**
	 * 添加领料单
	 * @param explanM
	 */
	public void saveExplanMLL(ExplanSpcode explanSpcode);
	
	/**
	 * 领料单拆分
	 * @param explanM
	 */
	public void explanSpcode(Map<String,Object> map);
	
	/**
	 * 修改加工单主表
	 * @param explanM
	 */
	public void updateExplanM(ExplanM explanM);
	
	/**
	 * 反审核
	 * @param explanM
	 */
	public void updateExplanMForUncheck(ExplanM explanM);
	/**
	 * 修改加工单主表为已领料
	 * @param explanM
	 */
	public void updateExplanMForYnll(Map<String,String> mapResult);
	
	/**
	 * 修改加工单从表
	 * @param explanM
	 */
	public void updateExplanD(ExplanM explanM);
	
	/**
	 * 删除加工单主表
	 * @param listId
	 */
	public void deleteExplanM(List<String> listExplanno);
		
	/**
	 * 删除加工单从表
	 * @param listId
	 */
	public void deleteExplanD(List<String> listExplanno);
	
	/**
	 * 查询安全库存报货数据
	 * @param explan
	 */
	public List<Map<String,Object>> safeCntToPlan(HashMap<String, Object> map);
	
	/**
	 * 查询领料单主表
	 * @param map
	 * @return
	 */
	public List<ExplanSpcode> findExplanSpcodeM(HashMap<String, Object> map);
	
	/**
	 * 查询领料单子表
	 * @param map
	 * @return
	 */
	public List<ExplanSpcode> findExplanSpcodeD(ExplanSpcode explanSpcode);
	
	/**
	 * 登记入库界面
	 * @param map
	 * @return
	 */
	public List<ExplanD> findExplanDForDj(ExplanD explanD);
	
	/**
	 * 保存登记   修改数量 
	 * @param explan
	 */
	public void updateCntact(ExplanD explanD);
	
	/**
	 * 查询加工单明细
	 * @param explanno
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanD> toSelectDetailed(ExplanD explanD);

	/***
	 * 判断是否走新的报货流程 
	 * @author 2015.1.6wjf
	 * @param map
	 */
	public void explanSpcode_new(Map<String, Object> map);
	
	/**
	 * 关键字查询
	 * @param explanMMap
	 * @return
	 */
	public List<ExplanM> findByKey(HashMap<Object,Object> explanMMap);
	
	/**
	 * 按单号查询
	 * @param explanM
	 * @return
	 */
	public ExplanM findByExplanno(ExplanM explanM);
	
	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的
	 * @return
	 */
	public int checkYnUnChk(ExplanM explanM);
	
	/**
	 * 查询是否已领料
	 * @return
	 */
	public String checkYnLL(ExplanM explanM);
}
