package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkdemo;

public interface ChkdemoMapper {

	/**
	 * 查找当前最大顺序号
	 * @return
	 */
	public String findMaxId(Chkdemo chkstodmeo);
	/**
	 * 查询所有的盘点标题
	 * @return
	 */
	public List<Chkdemo> findAllTitle(Chkdemo chkstodemo);
	
	/**
	 * 查找所有盘点
	 * @return
	 */
	public List<Chkdemo> findChkdemo(Chkdemo chkstodemo);
	
	/**
	 * 模板数据查询
	 * @param recList
	 * @return
	 */
	public List<Chkdemo> findChkdemoByRec(Map<String,Object> map);
	/**
	 * 添加盘点
	 * @param sp_codes
	 */
	public void saveNewChkdemo(Chkdemo chkstodemo);
	/**
	 * 删除申购信息
	 * @param sp_codes
	 */
	public void deleteChkdemoByTitleAndId(Chkdemo chkdemo);
	public void deleteChkdemo(Map<String,Object> map);

}
