package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FirmDeliver;

public interface FirmDeliverMapper {

	/**
	 * 报货分拨修改供应商，查找方向
	 * @param deliver
	 */
	public String findInout(Deliver deliver);
	
	/**
	 * 根据仓位查询供应商
	 * @param map
	 * @return
	 */
	public List<Deliver> findDeliverByPositn(Map<String,Object> map);
	
	/**
	 * 根据仓位增加供应商
	 * @param firmdeliver
	 * @return
	 */
	public void saveFirmDeliver(FirmDeliver firmdeliver);
	
	/**
	 * 根据仓位删除供应商
	 * @param map
	 * @return
	 */
	public void deleteFirmDeliver(Map<String,Object> map);
	
	/**
	 * 根据 供应商查询分店仓位信息
	 * @param Map<String,Object> map
	 * @return deliverList
	 * @throws CRUDException
	 */
	public List<Deliver> findPositnByDeliver(Map<String,Object> map);
}

