package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.SppriceSale;
public interface SppriceSaleQuickMapper {
	/**
	 * 快速 批量插入售价单 并审核
	 * @param sppriceSale
	 */
	public void saveSppriceSale(SppriceSale sppriceSale);

	/***
	 * 得到所有的报货单
	 * @param sppriceSale
	 * @return
	 */
	public List<SppriceSale> findSppriceSale(SppriceSale sppriceSale);

	/***
	 * 根据各种条件查询售价，防止重复添加
	 * @param map
	 * @return
	 */
	public Integer findSppriceSaleByDeliverPositnSupplyPriceDate(Map map);
	
	/**
	 * 更新物资对分店的最新审核通过的售价
	 * @param map
	 */
	public void updateNewCheckedSppriceSale(Map<String,Object> map);
}
