package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Tax;

public interface CodeDesMapper {

	/**
	 * 查询所有的系统编码列表
	 */
	public List<CodeDes> findCodeDes(CodeDes codeDes);

	/**
	 * 查询报货类别
	 */
	public List<CodeDes> findCodeDes_typ(CodeDes codeDes);
	/**
	 * 根据code查询信息
	 */
	public CodeDes findCodeDesByCode(CodeDes codeDes);
	/**
	 * 根据code查询信息--报货类别
	 */
	public CodeDes findCodeDesByCode_typ(CodeDes codeDes);
	/**
	 * 保存系统编码信息
	 */
	public void saveCodeDes(CodeDes codeDes);
	
	/**
	 * 更新系统编码信息
	 */
	public void updateCodeDes(HashMap<String, Object> codeDesMap);
	
	/**
	 * 删除系统编码信息
	 */
	public void deleteCodeDes(CodeDes codeDes);
	
	/**
	 * 获取所有要删除的编码
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> getAllCodeDes(CodeDes codeDes);
	
	/**
	 * 删除之后重新排序
	 * @param codeDes
	 */
	public void updateCodedespx(CodeDes codeDes);
	
	/**
	 * 验证输入的名称在所选的类别下是否有已存在的记录
	 * @param codeDes
	 * @return
	 */
	public int findSameDesByTyp(CodeDes codeDes);
	
	/**
	 * 验证输入的名称在所选的类别下是否有已存在的记录--修改
	 * @param codeDes
	 * @return
	 */
	public int findSameDesByTypu(CodeDes codeDes);
	
	/**
	 * 保存系统编码信息--报货类别需要添加报货方式
	 */
	public void saveCodeDes_typ(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--供应商校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHDeliver(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--区域校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHArea(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--区域校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHMod2(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--配送片区校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHDistributionArea(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--成本卡类型校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHCardType(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--分店类型校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHFirmTyp(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--报货分类校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHDailyGoodsTyp(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--物资单位校验
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHDailyNuit(CodeDes codeDes);
	
	/**
	 * 查询是否被引用--报废原因
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteYHDailyScrap(CodeDes codeDes);
	
	
	/**
	 * 获取最大的排序列的值
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	public String getMaxBeatsequence(CodeDes codeDes);
	
	/**
	 * 判断排序列是否存在
	 * @param codeDes
	 * @return
	 */
	public int getBeatsequence(CodeDes codeDes);
	
	/**
	 * 修改排序列====用于新增的时候
	 * @param codeDes
	 */
	public void updateCodedesPX(CodeDes codeDes);
	
	/**
	 * 根据code查找codedes
	 * @param codeDes
	 * @return
	 */
	public CodeDes selectCodedesByCode(CodeDes codeDes);
	
	/**
	 * 修改排序列====用于修改的时候(从大的改成小的)
	 * @param codeDes
	 */
	public void updateCodedesPx1(Map<String,Object> map);
	
	/**
	 * 修改排序列====用于修改的时候(从小的改成大的)
	 * @param codeDes
	 */
	public void updateCodedesPx2(Map<String,Object> map);
	
	/**
	 * 获取单据类型
	 * @param codeDes
	 * @return
	 */
	public List<CodeDes> findDocumentType(Map<String, Object> map);
	
	/**
	 * 根据单据类型编码 查询单据类型名称
	 * @param codeDes
	 * @return
	 */
	public CodeDes findDocumentByCode(CodeDes codeDes);
	
	/**
	 * 查询税率 jinshuai 20160428
	 * @param codeDes
	 * @return
	 */
	public List<Tax> findTaxRateList(Tax tax);
	
	/** 
	 * 保存税率 jinshuai 20160428
	 */
	public void saveTaxMes(Tax tax);
	
	/**
	 * 修改税率 jinshuai 20160428
	 */
	public void updateTaxMes(Tax tax);
	
	/**
	 * 删除税率 jinshuai 20160428
	 */
	public void deleteTaxMes(@Param(value="ids")String ids);
	
}
