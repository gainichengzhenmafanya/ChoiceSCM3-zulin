package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;


public interface DisFenBoMapper {

	/**
	 * 修改到货日期之后执行的操作
	 */
	public void deleteChkstodInd(Dis dis);
	public void saveChkstodInd(Dis dis);
	/**
	 * 修改供应商之后执行的操作
	 */
	public void deleteChkstodDeliver(Dis dis);
	public void saveChkstodDeliver(Dis dis);
	/**
	 * 报货分拨
	 */
	public List<Dis> findAllDis(Dis dis);
	
	/**
	 * 报货分拨查询总数
	 */
	public int findAllDisCount(Dis dis);
	
	/**
	 * 报货分拨
	 */
	public List<Dis> findAllDisPage(Dis dis);
	
	/**
	 * 采购订单
	 */
	public List<Dis> findAllDisPage_cg(Dis dis);
	
	//导出分拨单用
	public List<Dis> findAllDisPageFenbo(Dis dis);
	/**
	 * 批量修改      验收入库      验收出库    采购确认   采购审核  的标志字段      的统一 方法
	 */
	public void updateByIds(Dis dis);
	
	/**
	 * 修改
	 */
	public void updateDis(Dis dis);
	
	/**
	 * 查询出入库状态
	 */
	public Chkstod findChkByKey(String id);
	
	/**
	 * 报货分拨，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis);
	
	/**
	 * 报货分拨，配送汇总
	 */
	public List<Dis> findPeiSongTotal(Dis dis);
	
	/**
	 * 报货分拨，部门明细
	 */
//	public List<Dis> findDeptDetail(Map<String,Object> map);
	public List<Map<String,Object>> findDeptDetail(Map<String,Object> map);
	/**
	 * 查询分店中申请订单的部门
	 * */
	public List<String> findAllDept(String firmCode);
	/**
	 * 
	 *判断分店和供应商在视图中是否存在 
	 */
	public int findDateisNull(Dis dis);
	
	/**
	 * 报货分拨，验货直发
	 */
	public List<Dis> findYanHuoZhiFa(Dis dis);
	
	/**
	 *查询供货商邮箱 
	 */
	public  Deliver findDeliverOne(String deliverCode);
	
	/**
	 *报货分拨数据拆分
	 */
	public  void updateDispatch(Chkstod chkstod);
	public  void updateDispatchNew(Chkstod chkstod);
	public  void updateDispatchNew_x(Chkstod chkstod);
	
	/***
	 * 新报货方式的拆分报货单wjf
	 * @param chkstod
	 */
	public void updateDispatch_new(Chkstod chkstod);
	
	/***
	 * 部门采购明细 wjf
	 * @param dis
	 * @return
	 */
	public List<Dis> findDeptDetail2(Dis dis);
	
	/***
	 * 更新最新报价
	 * @param dis
	 * @return
	 */
	public int updateLastPrice(Dis dis);
}
