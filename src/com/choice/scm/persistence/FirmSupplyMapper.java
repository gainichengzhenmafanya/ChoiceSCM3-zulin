package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;

public interface FirmSupplyMapper {
	/**
	 * 根据分店编码查询分店物资
	 * @param  
	 * @return
	 */
	public List<PositnSpcode> findFirmSupply(PositnSpcode positnSpcode);
	/**
	 * 根据分店编码 和 物资 编码 查询是否存在 
	 */
	public PositnSpcode findFirmSupplyByFirmAndSpcode(PositnSpcode positnSpcode);

	/**
	 * 添加分店物资
	 * @param  
	 * @return
	 */
	public void saveSupply(PositnSpcode positnSpcode);
	
	/**
	 * 更新分店物资
	 * @param positn
	 * @throws CRUDException
	 */
	public void updateFirmSupply(PositnSpcode positnSpcode);
	
	/**
	 * 删除分店物资
	 * @param deliver
	 * @return
	 */
	public void deleteFirmSupply(PositnSpcode positnSpcode);
	
	/**
	 * 分店安全库存设置
	 * @param deliver
	 * @return
	 */
	public List<PositnSpcode> findPositnSpCode(PositnSpcode positnSpcod);
	
	/**
	 * 仓库安全库存设置
	 * @param positnSpcod
	 * @return
	 */
	public List<PositnSpcode> findCangKuSpCode(Supply supply);
	
	/**
	 * 根据分店编码查询分店物资
	 * @param deliver
	 * @return
	 */
	public List<Supply> findAllFirmSupply(@Param("ids")String ids);
	
	/**
	 * 删除当前门店下已经存在的物资信息
	 * @param ids
	 */
	public void deleteFirmSupplyByIds(PositnSpcode positnSpcode);

	/***
	 * 分店复制功能 调用存储过程 wjf
	 * @param map
	 */
	public void saveSupplyByCopy(Map map);

	/***
	 * 为了校验选择的物资是不是实际物料对应虚拟物料 1对1关系  wjf
	 * @param positnSpcode2
	 * @return
	 */
	public List<PositnSpcode> checkSupply121(PositnSpcode positnSpcode2);

	/***
	 * 物资复制新增分店物资属性
	 * @param positnSpcode
	 */
	public void savePositnSpcodeByCopy(PositnSpcode positnSpcode);
	
	/**
	 * 分店复制 （洞庭专用）wangjie 2014年12月9日 14:34:00
	 */
	public void deleteFirmSupplyForDT(String vfirm);
	
	public void insertFirmSupplyForDT(PositnSpcode positnSpcode);
	
	/**
	 * 物资编码-加入分店 （洞庭专用）wangjie 2014年12月9日 14:34:05
	 */
	public List<PositnSpcode> selectFirmSupplyForDT(PositnSpcode positnSpcode);
	
	public void insertSupplyForDT(PositnSpcode positnSpcode);
	
	/***
	 * 保存配送单位
	 * @param positnSpcodeList
	 * @return
	 */
	public int updateDisunit(PositnSpcode positnSpcode);
	
	/***
	 * 批量复制配送单位
	 * @param ps
	 */
	public void updateDisunitBatch(PositnSpcode ps);
}
