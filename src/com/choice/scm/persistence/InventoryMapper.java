package com.choice.scm.persistence;


import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.PositnPand;
import com.choice.scm.domain.PositnPanm;

public interface InventoryMapper {

	/**盘点  查询list
	 * @return
	 */
	public List<Inventory> findAllInventory(Inventory inventory);
	
	/**盘点加工间  查询list
	 * @return
	 */
	public List<Inventory> findAllInventoryByJGJ(Inventory inventory);
	
	/**
	 * 保存盘点
	 * @param inventory
	 * @return
	 */
	public void updateInventory(Inventory inventory);
	
	/**
	 * 记录日盘点
	 * @param inventory
	 * @return
	 */
	public void saveInventory(Map<String,Object> condition);
	
	/**
	 * 判断是否做过盘点
	 * @param Map
	 * @return
	 */
	public List<Map<String, Object>> getStatus_pd(Map<String, Object> condition);
	
	/**
	 * 月盘点差异报告
	 */
	public List<Map<String, Object>> findMonthPdSum(Map<String, Object> condition);
	public List<Map<String, Object>> findMonthPdSum_total(Map<String, Object> condition);
	
	/**
	 * 存货盘点表（日周月）
	 */
	public Object checkSupply(Map<String, Object> condition);
	public List<Map<String,Object>> findStockSum(Map<String, Object> condition);
	public List<Map<String,Object>> findStock(Map<String, Object> condition);
	//删除加工间物料盘点数据
	public void deleteInventoryByJGJ(Inventory inventory);
	//新增加工间物料库存数据
	public void saveInventoryByJGJ(Map<String,Object> map);
	//结束盘点
	public void upadteInventoryByJGJ(Inventory inventory);
	
	public List<Inventory> selectInventoryByJGJ(Inventory inventory);
	//获取加工间原材料耗用
	public List<Inventory> findInventoryByJGJ(Inventory inventory);

	/***
	 * 插入盘点主表 wjf
	 * @param positnPanm
	 */
	public void savePositnPanm(PositnPanm positnPanm);

	/***
	 * 插入盘点子表 wjf
	 * @param pand
	 */
	public void savePositnPand(PositnPand pand);

	/***
	 * 查询盘点物资总数量
	 * @param inventory
	 * @return
	 */
	public int findAllInventoryCount(Inventory inventory);

	/***
	 * 专门分页用的wjf
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory);
	
	//=========================================加工间盘点==================================================
	/**
	 * 加工间盘点分页
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findExInventoryPage(Inventory inventory);
	/**
	 * 不分页
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findExInventory(Inventory inventory);
	
	/**
	 * 得到加工间最近盘点日期
	 * @param map
	 * @return
	 */
	public Map<String,Object> findExLastPdDate(Map<String,Object> map);
	
}
