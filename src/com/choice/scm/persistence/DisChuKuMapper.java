package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Dis;


public interface DisChuKuMapper {

	/**
	 * 验收出库
	 */
	public List<Dis> findAllDis(Dis dis);

	/**
	 * 批量修改      验收入库      验收出库    采购确认   采购审核  的标志字段      的统一 方法
	 */
	public void updateByIds(Dis dis);
	
	/**
	 * 验收出库	编辑发货数量
	 * @param dis
	 */
	public void updateChkoutNum(Dis dis);
}
