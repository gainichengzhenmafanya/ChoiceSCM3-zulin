package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.FirmDept;
import com.choice.scm.domain.ana.Profit;

public interface ProfitAnaMapper {

	/**
	 * 毛利日趋势
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitDay(Profit profit);
	
	/**
	 * 毛利日趋势合计
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitDay_total(Profit profit);
	/**
	 * 毛利月趋势
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitMonth(Profit profit);
	
	/**
	 * 毛利月趋势合计
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitMonth_total(Profit profit);
	
	/**
	 * 根据code 查询仓位信息
	 * @param firmDept
	 * @param codes
	 */
	public List<FirmDept> findFirmDeptByCode(Profit profit);
	/**
	 * 查询分店毛利对比信息
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findFirmProfitCmp(Map<String,Object> condition);
}
