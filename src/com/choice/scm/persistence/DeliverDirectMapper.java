package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.Positn;

public interface DeliverDirectMapper {

	/**
	 * 根据仓位查询供应商直配
	 * @param map
	 * @return
	 */
	public List<Positn> findPositnByDeliver(Map<String,Object> map);
	
	/**
	 * 根据供应商增加仓位
	 * @param firmdeliver
	 * @return
	 */
	public void saveDeliverDirect(FirmDeliver firmdeliver);
	
	/**
	 * 根据供应商删除仓位
	 * @param map
	 * @return
	 */
	public void deleteDeliverDirect(Map<String,Object> map);

	/***
	 * 根据门店查询他配置的供应商直配信息（报货单填制提交查询价格前 查询方向用）   wjf
	 * @param firmDeliver
	 * @return
	 */
	public List<FirmDeliver> findFrimDeliverByFirm(FirmDeliver firmDeliver);

	/***
	 * 更新inout 直配添加 用  直配删除也用
	 * @author wjf
	 * @param firmDeliver
	 */
	public void updateDeliverDirect(FirmDeliver firmDeliver);
}
