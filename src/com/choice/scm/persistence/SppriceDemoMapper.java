package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.SppriceDemo;

public interface SppriceDemoMapper {

	/**
	 * 模糊查询所有的价格模板
	 * @param demo
	 * @return
	 */
	public List<SppriceDemo> findSppriceDemo(SppriceDemo demo);
	
	/**
	 * 更新价格模板信息
	 * @param demo
	 */
	public void updateSppriceDemo(SppriceDemo demo);
	
	/**
	 * 删除价格模板信息
	 * @param demo
	 */
	public void deleteSppriceDemo(SppriceDemo demo);
	
	/**
	 * 添加价格模板信息
	 * @param demo
	 */
	public void saveSppriceDemo(SppriceDemo demo);
	
	/**
	 * 通过id获取模板信息
	 * @param demo
	 * @return
	 */
	public SppriceDemo findSppriceDemoById(SppriceDemo demo);
	
	/**
	 * 根据供应商和物资编码更新报价信息
	 * @param spprice
	 */
	public void updatePrice(SppriceDemo demo);
}
