package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnFirmScm;

public interface PositnMapper {
	
	/**
	 * 模糊查询分店和仓位
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositn(Positn positn);
	
	/**
	 * 模糊查询加工间
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnex(Positn positn);
	
	/**
	 * 模糊查询分店和仓位----用于配送线路门店选择
	 * @param positn
	 * @return
	 */
	public List<Positn> searchAllPositnPs(Positn positn);
	
	/**
	 * 模糊查询分店和仓位---- 用于供应商对应仓位编码，不可重复
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositns(Positn positn);
	
	/**
	 * 查询所有分店和仓位类别
	 * @param positn
	 * @return
	 */
	public List<Positn> findAllPositnTyp();
	
	/**
	 * 查询分店
	 */
	public List<Positn> findPositnMonit();
	
	/**
	 * 模糊查询仓位
	 * @param searchInfo
	 * @return
	 */
	public List<Positn> findPositnBy(Map<String,Object> map);
	
	/**
	 * 模糊查询仓位----用于分店物资属性，过滤掉主直拨库和基地仓库
	 * @param searchInfo
	 * @return
	 */
	public List<Positn> findPositnBySupply(Map<String,Object> map);
	
	/**
	 * 模糊查询仓位--用于分店供应商
	 * @param searchInfo
	 * @return
	 */
	public List<Positn> findPositnByDel(Map<String,Object> map);
	
	/**
	 * 根据acct和code查询分店和仓位信息
	 * @param positn
	 * @return
	 */
	public Positn findPositnByCode(Positn positn);
	
	/**
	 * 根据acct、code（多个）、类型 查询仓位信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<Positn> findPositnByTypCode(Map<String,Object> map);
	
	/**
	 * 保存分店和仓位
	 * @param positn
	 */
	public void savePositn(Positn positn);
	
	/**
	 * 更新分店和仓位====用于更新到物资表中的分店仓位
	 * @param positn
	 */
	public void updateSupply(Positn positn);
	
	/**
	 * 更新分店和仓位
	 * @param positn
	 */
	public void updatePositn(Positn positn);

	
	/**
	 * 删除分店和仓位
	 * @param listPositn
	 */
	public void deletePositn(Positn positn);

	
	/**
	 * 删除分店和仓位
	 * @param listPositn
	 */
	public void deleteByIds(@Param("code")String code);
	/**
	 * 获取所有仓位
	 * @return
	 */
	public List<Positn> findAllPositn();
	/**
	 * ajax查询前N条记录
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnN(Positn positn);
	/**
	 * 查找货架
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,String>> findPositn1();
	
	/**
	 * 查询仓位（出库单/入库单填制）
	 * @return
	 */
	public List<Positn>findPositnCangwei();
	/**
	 * 查询使用仓位（出库单/入库单填制）
	 * @return
	 */
	public List<Positn>findPositnUse();
	/**
	 * 
	* @Title: findCodedesType 
	* @Description: TODO(查询仓库分类)
	* @Author：LI Shuai
	* @date：2014-1-22下午3:34:37
	* @return  List<CodeDes>
	* @throws
	 */
	public List<CodeDes>findCodedesType(Positn positn);
	/**
	 * 
	* @Title: findCodedesType 
	* @Description: TODO(查询仓库分类) ====用于预估系统模块
	* @Author：LI Shuai
	* @date：2014-1-22下午3:34:37
	* @return  List<CodeDes>
	* @throws
	 */
	public List<CodeDes>findCodedesTypeFirm(Positn positn);

	/***
	 * 查询分店类型的分店，物资编码中 加入分店物资属性用 wjf
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnBySupply1(Positn positn);
	
		/**
	 * 根据区域查询仓位
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnByArea(Positn positn);
	
	/***
	 * 根据门店编码获取所有部门
	 * @param positn
	 * @return
	 */
	public List<Positn> findDeptByPositn(Positn positn);

	/***
	 * 添加仓位类型查询条件模糊查询仓位 只为出库单使用仓位用  wjf
	 * @param map
	 * @return
	 */
	public List<Positn> findChkoutPositnIn(Map<String, Object> map);
	
	/**
	 * 快速报价模块 获取所有仓位（不包含部门）
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnForQuickSpprice(Positn positn);
	
	/**
	 * 分店仓位  根据分店编码或者分店名称 查询所有分店 wangjie 2014年12月24日 14:46:44
	 * @param positn
	 * @return
	 */
	public List<Positn> selectFirmByCodeOrDes(Positn positn);
	
	/**
	 * 查询所有基地仓库和主直拨库   新增物资编码时 
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnForAddSupply(Positn positn);
	
	/** 
	 * 严禁修改     严禁修改     严禁修改     严禁修改      严禁修改   严禁修改   严禁修改
	 * 查询仓位信息公共 。。。。。。。。。。。。。。。。。。。。。。。。。2015.1.8后 所有方法都可以走 2015.1.8  xlh    严禁修改
	 *  控制typn值就行     比如   查询 基地  仓位和加工间     typn= “typ in （基地仓位，加工间）”
	 */
	public List<Positn> findPositnSuper(Positn positn);
	
	/**
	 * 获取门店BOH 部门列表
	 * @return
	 */
	public List<Positn> getBOHDeptList(Positn positn);
	/**
	 * 获取scm档口和boh部门关系，返回单条
	 * @return
	 */
	public PositnFirmScm FindPositnFirmScmBy(PositnFirmScm positnFirmScm);
	/**
	 * 添加绑定
	 * @param positn
	 */
	public void insertPositnFirm(Positn positn);
	
	/**
	 * 删除对应关系
	 * @param positn
	 */
	public void deletePositnFirm(Positn positn);
	/**
	 * 删除对应关系s
	 * @param positn
	 */
	public void deletePositnFirms(Positn positn);
	
	/**
	 * 新的 查询仓位的方法
	 * @param positn
	 * @return
	 */
	public List<Positn> selectPositnNew(Positn positn);

	/***
	 * 查询所有未设置虚拟供应商的朱直拨库，基地仓库，加工间，分店，其他
	 * @return
	 */
	public List<Positn> findPositnNoSetDeliver();

	/***
	 * 得到boh的成本卡类型
	 * @param code
	 * @return
	 */
	public String getAirdittypeFromBoh(@Param("code")String code);

	/***
	 * 直营店还是加盟店
	 * @param firm
	 * @return
	 */
	public String findFirmtypByScode(@Param(value="scode")String scode);

	/***
	 * 加盟商金额查询
	 * @param firm
	 * @return
	 */
	public Map<String,Object> checkSppriceSale(@Param(value="firm")String firm);

	/***
	 * 加盟商扣款
	 * @param c
	 */
	public void joiningdeduction(Chkoutm c);
}
