package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkstodemo;

public interface ChkstodemoMapper {

	/**
	 * 查找当前最大顺序号
	 * @return
	 */
	public int findMaxRec(Chkstodemo chkstodmeo);
	
	/**
	 * 查询所有的申购单标题
	 * @return
	 */
	public List<Chkstodemo> findAllTitle(Chkstodemo chkstodemo);
	
	/**
	 * 查找所有申购单
	 * @return
	 */
	public List<Chkstodemo> findChkstodemo(Chkstodemo chkstodemo);
	
	/**
	 * 模板数据查询
	 * @param recList
	 * @return
	 */
	public List<Chkstodemo> findChkstodemoByRec(Map<String,Object> map);
	
	/**
	 * 根据分店 查找所有申购单模板
	 * @return
	 */
	public List<Chkstodemo> findChkstodemoForFirm(Chkstodemo chkstodemo);
	
	/**
	 * 模板数据查询根据分店
	 * @param recList
	 * @return
	 */
	public List<Chkstodemo> findChkstodemoForFirmByRec(Map<String,Object> map);
	/**
	 * 添加申购单
	 * @param sp_codes
	 */
	public void saveNewChkstodemo(Chkstodemo chkstodemo);
	
	/**
	 * 删除申购信息
	 * @param sp_codes
	 */
	public void deleteChkstodemo(Map<String,Object> map);
}
