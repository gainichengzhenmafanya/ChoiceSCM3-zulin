package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.ana.ExThCostAna;

public interface ExAnaMapper {

	/**
	 * 产品理论成本分析
	 */
	public List<ExThCostAna> exTheoryCostAna(ExThCostAna exThCostAna);
	/**
	 * 产品理论成本分析   合计
	 */
	public ExThCostAna exTheoryCostAna_total(ExThCostAna exThCostAna);
	
}
