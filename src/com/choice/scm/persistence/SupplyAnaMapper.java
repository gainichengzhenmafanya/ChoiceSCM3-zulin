package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SupplyAnaMapper {

	/**
	 * 查询库龄分组信息
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findInventoryAgingSum(Map<String,Object> conditions);
	/**
	 * 查询库龄分组物资信息
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findInventoryAgingDetail(Map<String,Object> conditions);
	/**
	 * 查询库龄合计
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findCalForInventoryAging(Map<String,Object> conditions);
	/**
	 * 查询价格预警与比重报表内容
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findPriceWarning(Map<String, Object> condition);
	/**
	 * 查询价格预警与比重报表内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForPriceWarning(Map<String, Object> condition);
	
	/**
	 * 查询不合格进货统计报表内容
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findUnqualifiedStatistics(Map<String, Object> condition);
	/**
	 * 查询不合格进货统计表内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForUnqualifiedStatistics(Map<String, Object> condition);

	/**
	 * 采购价格异动分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findPurchasePriceChange(Map<String,Object> condition);
	/**
	 * 物资供应商分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findSupplyDeliver(@Param("spcode")String spcode);
	/**
	 * 物资供应商表头查询
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findSupplyDeliverHeaders(@Param("spcode")String spcode);



	/**
	 * 入库成本分析  的           列表  列表总计    明细   明细总计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findReceiptCostAnalysisSum(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisSum_total(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisDetail(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisDetail_total(Map<String, Object> condition);
	
	
	/**
	 * 资金占用情况   table  和总计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findZiJinZhanYongQingKuang(Map<String, Object> condition);
	public List<Map<String, Object>> findZiJinZhanYongQingKuangSum(Map<String, Object> condition);
	
	/**
	 * 周入库成本分析   table  和   总计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findZhouRuKuChengBenFenXi(Map<String, Object> condition);
	public List<Map<String, Object>> findZhouRuKuChengBenFenXi_total(Map<String, Object> condition);
	
	/**
	 * 查询物流配送分析内容
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findLogisticsAnalysis(Map<String, Object> condition);
	/**
	 * 查询物流配送分析内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForLogisticsAnalysis(Map<String, Object> condition);
	/**
	 * 查询库存量预估使用内容
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findInventoryEstimates(Map<String, Object> condition);
	/**
	 * 申购到货分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findApplyArriveAmount(Map<String,Object> condition);
	/**
	 * 申购到货分析汇总
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findApplyArriveAmountCount(Map<String,Object> condition);
	/**
	 * 申购出库分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findApplyOutAmount(Map<String,Object> condition);
	/**
	 * 申购出库分析汇总
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findApplyOutAmountCount(Map<String,Object> condition);
	/**
	 * 物资ABC分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findSupplyABCStatistics(Map<String,Object> condition);
	/**
	 * 物资ABC分析汇总分类
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findSupplyABCStatisticsSum(Map<String,Object> condition);
	/**
	 * 物资ABC分析汇总全部
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findSupplyABCStatisticsSumAll(Map<String,Object> condition);
	/**
	 * 查询库存量预估使用内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForInventoryEstimates(Map<String, Object> condition);
	/**
	 * 分店进货价格分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findFirmStockPrice(Map<String,Object> condition);
	/**
	 * 采购价格异动分析1
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findPurchasePriceChange1(Map<String,Object> condition);

	/**
	 * 得到仓库领用对比表头
	 * @param object
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findWarehouseRequisitionedContrastHeader(Map<String, Object> condition);
	/**
	 * 查询仓库领用对比
	 * @param object
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCangKuLingYongDuiBi(Map<String,Object> condition);
	/**
	 * 查询仓库领用对比总计
	 * @param object
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,Object>> findCalForCangKuLingYongDuiBi(Map<String,Object> condition);
	/**
	 * 查询仓库周发货对比
	 * @param object
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCangKuZhouFaHuoDuiBi(Map<String,Object> condition);
	/**
	 * 查询仓库周发货对比总计
	 * @param object
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,Object>> findCalForCangKuZhouFaHuoDuiBi(Map<String,Object> condition);

	/**
	 * 物资库龄明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findSupplyKuLingDetail(Map<String,Object> conditions);
	
	/**
	 * 物资库龄明细合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplyKuLingDetailSum(Map<String,Object> content);
	
	/**
	 * 库存周转率汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLvSum(Map<String,Object> conditions);
	
	/**
	 * 库存周转率
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLv(Map<String,Object> conditions);
	
	/**
	 * 库存周转率详情
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLvDetail(Map<String,Object> condition);
	
	/**
	 * 采购测算
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCaiGouCeSuan(Map<String,Object> condition);
	
	/**
	 * 计算采购测算
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> changeCaiGouCeSuan(Map<String,Object> condition);
}
