package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Message;

public interface MessageMapper {

	/**
	 * 写邮件
	 * @param message
	 */
	public void sendMessage(Message message);
	
	/**
	 * 修改邮件状态（修改state由0改为1）
	 * String id
	 */
	public void updateMessage(Map<String,Object> map);
	
	/**
	 * 查询邮件内容
	 * String id
	 */
	public Message findMessageById(Map<String,Object> map);
	
	/**
	 * 查找所有邮件
	 * String receiver
	 * String searchInfo
	 * String orderby:sender,sendtime desc,state
	 */
	public List<Message> findAllMessage(Map<String,Object> map);
	
	/**
	 * 查询未读邮件数
	 * String receiver
	 */
	public int findUnReadMessage(Map<String,Object> map);
	
	/**
	 * 删除邮件
	 */
	public void deleteMessage(List<String> list);
	
	/**
	 * 查询账号信息
	 */
	public List<Message> findAccount(Map<String,Object> map);
}
