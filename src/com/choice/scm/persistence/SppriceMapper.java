package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Spprice;

public interface SppriceMapper {

	/**
	 * 模糊查询价格列表
	 * @param spprice
	 * @return
	 */
	public List<Spprice> findSpprice(Spprice spprice);
	
	/**
	 * 模糊查询价格(返回map集合包含物资报价上下限)
	 * @param spprice
	 * @return
	 */
	public List<Map<String, Object>> findSppriceT(Spprice spprice); 
	
	/**
	 * 全部反审核or全部审核
	 * @param spprice
	 */
	public void updateAllSppriceStatus(Spprice spprice);
	
	/**
	 * 更新价格记录状态（进行价格审核）
	 * @param spprice
	 */
	public void updateSppriceStatus(@Param("list") List<String> listRec,@Param("spprice") Spprice spprice);
	
	/**
	 * 根据传入的条件查询有没有设置供应商
	 * @param spprice
	 * @return
	 */
	public List<Spprice> findDeliverBySpprice(Spprice spprice);

	/**
	 * 批量更新新插入数据
	 * @param spprice
	 */
	public void updateBatch(Spprice spprice);
	
	
	/**
	 * 添加新报价
	 * @param spprice
	 */
	public void addSpprice(Spprice spprice);
	/**
	 * 获取最大流水号
	 * @return
	 */
	public Spprice getLastRec();
	
	/**
	 * 删除未审核报价
	 * @param asList
	 * @param spprice
	 */
	public void deleteSpprice(@Param("list") List<String> listRec,@Param("spprice") Spprice spprice);
	/**
	 * 查询将到期价格
	 * @param spprice
	 * @return
	 */
	public List<Spprice> findSppriceByEdat(Spprice spprice);
	
}
