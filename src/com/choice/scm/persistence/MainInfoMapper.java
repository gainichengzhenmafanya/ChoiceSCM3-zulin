package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Condition;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.ReportModule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.domain.WorkBench;

public interface MainInfoMapper {

	/**
	 * 查询帐套信息
	 * @param mainInfo
	 */
	public MainInfo findMainInfo(int yearr);
	
	/**
	 * 查询所有帐套的年份
	 */
	public List<String> findYearrList();
	/**
	 * 更新帐套信息
	 * @param mainInfo
	 */
	public void updateMainInfo(MainInfo	mainInfo);
	/**
	 * 查询帐套信息
	 * @param mainInfo
	 */
	public List<WeekSet> findWeekSet(int yearr);
	
	/**
	 * 查询月份信息
	 * @param yearr
	 * @return
	 */
	public int findMonthSet(int yearr);
	
	/**
	 * 查询当前流水号
	 * @return
	 */
	public int getMaxWeekk();
	/**
	 * 添加周次信息
	 * @param mainInfo
	 */
	public void insertWeekSet(WeekSet weekSet);
	
	/**
	 * 添加月份信息
	 * @param map
	 */
	public void insertMonthSet(Map<String,Object> map);
	//--------------------------------------------------------
	public List<Supply> findSupplyOver(Map<String,Object> map);
	public List<Supply> findSupplyLow(Map<String,Object> map);
	public List<Supply> findAllSupplyOver(Supply supply);
	public List<Supply> findAllSupplyLow(Supply supply);
	
	public List<Map<String,Object>> findUnChecked(Map<String,Object> map);
	public List<Map<String,Object>> findStackBillSum(Map<String,Object> map);
	public List<Map<String,Object>> findOutBillSum(Map<String,Object> map);
	public List<Map<String,Object>> findOutTop10(Map<String,Object> map);
	public Map<String,Object> findOutSum(Map<String,Object> map);
	
	//根据角色ID查询菜单信息
	public String findRoleWorkBench(String id);
	//保存用户配置菜单信息
	public void saveWorkBench(WorkBench workbench);
	public void updateWorkBench(WorkBench workbench);
	public WorkBench findWorkBench(String accountId);
	//保存常用操作模块id
	public void saveUsedModel(WorkBench workBench);

	//-------------------------------------------------------临时功能，待定--------------------------------------------------------------------------------
	//各门店昨日营业额
	public List<Map<String, Object>> firmAmtList(Condition condition);
	//事业群营业额
	public List<Map<String, Object>> modAmtList(Condition condition);
	//本月营业额走势
	public List<Map<String, Object>> modAmtLineList(Condition condition);
	//上月营业额走势
	public List<Map<String, Object>> modAmtLineListBefore(Condition condition);
	//菜品类别排行（柱状）
	public List<Map<String, Object>> queryDdxstjbycltj(Condition condition);
	//昨日未上传分店
	public List<Map<String, Object>> getUnUploadFirm(Condition condition);

	/***
	 * 查询所有的会计年
	 * @param acct
	 * @return
	 */
	public List<MainInfo> findAllYears(String acct);
	
	//-------------------------------总部BOH快餐---------------------------------------------
		/**
		 * 描述：门店营业额
		 * @param map
		 * @return
		 * author:spt
		 * 日期：2014-5-7
		 */
		public List<Map<String,Object>> findStoreYye(@Param("dateString")String dateString);
		/**
		 * 描述：获取类别统计营业额
		 * @return
		 * author:spt
		 * 日期：2014-5-7
		 */
		public List<Map<String,Object>> findLbtjList(@Param("dateString")String dateString);
		/**
		 * 描述：市场占比
		 * @return
		 * author:spt
		 * 日期：2014-5-7
		 */
		public List<Map<String,Object>> findSczbList(@Param("dateString")String dateString);
		/**
		 *TODO:菜品类别占比
		 *@return
		 *@author:spt
		 *2015-1-22 下午1:40:22
		 */
		public List<Map<String,Object>> findCplbzbList(@Param("dateString")String dateString);
		
		/**
		 *TODO:月营业额月分析  折现图
		 *@return
		 *@author:spt
		 *2015-1-22 下午1:40:22
		 */
		public List<Map<String, Object>> findTotalMoneyQushiByDays(@Param("con")Condition condition,@Param("dateNum")Integer dateNum);
		/**
		 *TODO:快餐四个仪表盘
		 *@param pk_store
		 *@param bdat
		 *@param edat
		 *@param sbdat
		 *@param sedat
		 *@return
		 *@author:spt
		 *2015-1-22 下午1:40:33
		 */
		public List<Map<String, Object>> getYiBiaoPanKcBoh(@Param("pk_store")String pk_store,@Param("bdat")String bdat,@Param("edat")String edat,@Param("sbdat")String sbdat,@Param("sedat")String sedat);
		//-------------------------------总部BOH中餐---------------------------------------------
		//各门店昨日营业额
		public List<Map<String, Object>> firmAmtList(@Param("pk_store")String pk_store,@Param("bdat")String bdat,@Param("edat")String edat);
		//菜品类别排行（柱状）
		public List<Map<String, Object>> queryDdxstjbycltj(@Param("pk_store")String pk_store,@Param("bdat")String bdat,@Param("edat")String edat);
		//四个仪表盘
		public List<Map<String, Object>> getYiBiaoPan(@Param("pk_store")String pk_store,@Param("bdat")String bdat,@Param("edat")String edat,@Param("sbdat")String sbdat,@Param("sedat")String sedat);
		//集团月营业额月分析柱状图表
		public List<Map<String, Object>> jituanyingyeeyuefenxi();

		/***
		 * 根据门店和日期查询配送班表
		 * @param scheduleD
		 * @return
		 */
		public List<ScheduleD> findSchedulesByPositn(ScheduleD scheduleD);

		/***
		 * 查询直配需要验货的数量 根据供应商分组显示
		 * @param dis
		 * @return
		 */
		public List<Dis> findDireCountGroupbyDeliver(Dis dis);

		/***
		 * 查询统配需要验货的总数量
		 * @param dis
		 * @return
		 */
		public List<SupplyAcct> findOutCount(Dis dis);

		/***
		 * 查询调拨需要验货的总数 根据门店来查
		 * @param dis
		 * @return
		 */
		public List<SupplyAcct> findDbCountGroupByPositn(Dis dis);

		/***
		 * 查询配置的报表
		 * @param rm
		 * @return
		 */
		public List<ReportModule> findMyReportModule(ReportModule rm);

		/***
		 * 查询我的报表
		 * @param rm
		 * @return
		 */
		public List<ReportModule> findAllReportModule(ReportModule rm);

		/***
		 * 删除我的常用报表
		 * @param rm
		 */
		public void deleteUsedReport(ReportModule rm);
		
		/***
		 * 保存我的常用报表
		 * @param rm
		 */
		public void saveUsedReport(ReportModule rm);

}
