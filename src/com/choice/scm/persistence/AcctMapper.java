package com.choice.scm.persistence;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Acct;

public interface AcctMapper {

	/**
	 * 增加帐套信息
	 * @param acct
	 */
	public void saveAcct(Acct acct);
	
	/**
	 * 查询帐套
	 * @param acct
	 */
	public List<Acct> findAcct(Acct acct);
	
	/**
	 * 通过id获取帐套信息
	 * @param acct
	 * @return
	 */
	public Acct findAcctById(String idss);
	
	/**
	 * 查询门店是否使用库存程序
	 * @param acct
	 * @return
	 */
	public Acct findYnkcFromAcct(String idss);
	
	/**
	 * 将当前默认改为非默认
	 */
	public void updateAcctWarn();
	
	/**
	 * 更新帐套信息
	 * @param acct
	 */
	public void updateAcct(Acct acct);
	
	/**
	 * 查询帐套会计月信息
	 */
	public Map<String,Object> findAcctMainInfo(int yearr);

	/***
	 * 根据日期查询所在会计年
	 * @param date
	 * @return
	 */
	public String findYearrByDate(@Param(value="date")Date date);
}
