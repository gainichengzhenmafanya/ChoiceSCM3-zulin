package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.Supply;

public interface SppriceQuickMapper {
	/**
	 * 根据供货商ID 查询报价
	 * @param deliver
	 * @return
	 */
	public List<Spprice> findSppriceByDeliverCode(Deliver deliver);
	/**
	 * 根据id查询 物资
	 * @param deliver
	 * @return
	 */
	public List<Supply> findSupplyById(Map<String, Object> params);
	/**
	 * 添加报价
	 * @param deliver
	 * @return
	 */
	public void saveSpprice(Spprice spprice);
	
	/*********************************************************************
	 * 添加价格
	 * @param deliver
	 * @return
	 */
	public void saveSppriceDemo(SppriceDemo sppriceDemo);
	
	/***
	 * 根据条件：供应商，仓位，时间，物资，价格条件查询报价 ，校验  wjf
	 * @param map
	 * @return
	 */
	public Integer findSppriceByDeliverPositnSupplyPriceDate(Map map);
}
