package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CostCut;
import com.choice.scm.domain.CostItem;
import com.choice.scm.domain.DateList;
import com.choice.scm.domain.Positn;

public interface CostCutMapper {
	
/********************************************************菜品理论成本核减start*************************************************/

	/**
	 * 模糊查询半成品
	 * @param prdct
	 * @return
	 */
	public List<Positn> findPositn();
	
	/***
	 * 查询已核减数据分页
	 * @param costCut
	 * @param page
	 * @return
	 */
	public List<CostCut> findCostItemByCostCut(CostCut costCut);
	
	/**
	 * 根据门店编码查询门店类型
	 * add by wangkai 2016-1-27 
	 * @return
	 */
	public String findVfoodSignByVcode(Positn positn);
	
	/**
	 * 获取菜品销售情况统计
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:19:23
	 * @param vcode
	 * @param bdat
	 * @param edat
	 * @return
	 */
	public List<java.util.Map<String, Object>> findDatSubtract(@Param(value="vcode")String vcode,
			@Param(value="bdat")String bdat, @Param(value="edat")String edat, @Param(value="ynUseDept")String ynUseDept);
	
	/**
	 * 获取菜品销售情况统计 中餐
	 * @author wangkai
	 * @param 2016-1-27
	 * @return
	 */
	public List<java.util.Map<String, Object>> findDatSubtract_cn(@Param(value="vcode")String vcode,
			@Param(value="bdat")String bdat, @Param(value="edat")String edat, @Param(value="ynUseDept")String ynUseDept);
	
	/**
	 * 保存菜品销售数据
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:19:47
	 * @param list
	 */
	public void saveCostItem(List<CostItem> list);

	/**
	 * 核减物资使用率
	 * @author 文清泉
	 * @param 2015年4月28日 下午3:37:46
	 * @param list
	 */
	public void excutecostitemspcode(CostItem costItem);
	
	/***
	 * 反核减
	 * @param cost
	 */
	public void excutecostcut_re(CostItem cost);

	/***
	 * sqlserver保存核减主表
	 * @param cost
	 */
	public void saveCostItem(CostItem cost);

	/**
	 * 根据日期查询
	 * @param prdct
	 * @return
	 */
	public List<CostItem> queryByDate(CostItem costItem);
	
	/**
	 * 根据缩写码查询分店
	 * @param key
	 * @return
	 */
	public List<Positn> findByKey(HashMap<String, Object> map);
	
	/**
	 * 核减理论成本数据
	 * @param pro
	 * @return
	 */
	public int excutecostcut(CostItem costItem);
	public int excutecostcut_x(CostItem costItem);
	
	/**
	 * 获取门店成本卡类型
	 * @param pro
	 * @return
	 */
	public String getAirdittypeFromBoh(CostItem costItem);
	
	/**
	 * 获取会计期月
	 * @param pro
	 * @return
	 */
	public  DateList selectmonth(HashMap<String, Object> map);
	
	/**
	 * 查询某分店是否已经核减过
	 * @param pro
	 * @return
	 */
	public  List<CostItem> chkOrNot(CostCut costCut);
	/**
	 * 反核减
	 */
	public  void excutecostcutAnti(CostCut costCut);
	
	/***
	 * 删除costitemspcode
	 * @param costCut
	 */
	public void deleteCostitemSpcode(CostCut costCut);
	
	/**
	 * 根据分店号查询分店名称
	 * @param pro
	 * @return
	 */
	public String getFirmdes(Positn positn);
	
	/**
	 *  获取核减数据
	 * @param pro
	 * @return
	 */
	public List<CostCut> getCostcut(HashMap<String, Object> map);
	
	/***
	 * 得到boh中的核减数据
	 */
	public List<CostCut> getCostcutBoh(CostCut costCut);
	
	/***
	 * 得到boh中的核减数据
	 */
	public List<CostCut> getCostcutBoh_tele(CostCut costCut);
	
	/**
	 * 插入CostItem表
	 * @param pro
	 * @return
	 */
	public void insertCostItem(CostCut costCut);
	
	/**
	 * 获取最大流水号
	 * @return
	 */
	public Integer getLastRec();
	
	/***
	 * 根据门店得到门店下的档口关系
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnfirmByPositn(Positn positn);
	
/********************************************************菜品理论成本核减end*************************************************/
/********************************************************加工间理论成本核减start*************************************************/
	
	/**
	 * 查询入库单
	 * @param prdct
	 * @return
	 */
	public List<Chkinm> findChkinm(Chkinm chkinm);
	
	/**
	 * 删除Excostspcode表
	 * @param pro
	 * @return
	 */
	public void deleteExcostspcode(Chkinm chkinm);
	
	/**
	 * 更新PositnSupply表
	 * @param pro
	 * @return
	 */
	public void updatePositnSupply(Chkinm chkinm);
	
	/**
	 * 加工间理论成本数据核减
	 * @param pro
	 * @return
	 */
	public int excuteExcostcut(Chkinm chkinm);
/********************************************************加工间理论成本核减end*************************************************/
}
