package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Main;

public interface FirmMisMapper {

	/**
	 * 毛利查询
	 * @param conditions
	 * @return
	 */
	public List<String> findGrossProfit(Map<String,Object> conditions);
	/**
	 * 查询每日差异报告
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDayDifReport(Map<String,Object> conditions);
	/**
	 * 每日差异报告合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForDayDifReport(Map<String,Object> conditions);
	/**
	 * 查询每日差异对比
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDayDifCompare(Map<String,Object> conditions);
	/**
	 * 核减明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findHejian(Map<String,Object> conditions);	
	/**
	 * 检查系统是否正在盘点
	 * @param accountId
	 * @return
	 */
	public Map<String,Object> findChktag(Map<String,Object> map);
	/**
	 * 查找月末结转的月份
	 * @return
	 */
	public Main findMain(@Param("yearr")String yearr);
	/**
	 * 月末结转
	 */
	public void updateMonthh(Map<String,Object> map);
}
