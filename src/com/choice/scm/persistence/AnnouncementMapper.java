package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Announcement;

public interface AnnouncementMapper {

	public List<Announcement> findAllAnnouncement(Announcement announcement);
	
	public Announcement findAnnouncementById(String id);
	public int getId();
	public void addAnnouncement(Announcement announce);
	public void deleteAnnouncement(List<String> list);
	public void updateAnnouncement(Announcement announce);
}
