package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Explan;
import com.choice.scm.domain.Supply;

public interface ExplanMapper {
	
	
	/**
	 * 查询所有调度
	 * @param map
	 * @return
	 */
	public List<Explan> findAllExplan(Explan explan);
	
	/**
	 * 查询调度总数
	 */
	public int findAllExplanCount(Explan explan);
	/**
	 * 查询所有调度分页
	 * @param map
	 * @return
	 */
	public List<Explan> findAllExplanPage(Explan explan);
	
	/**
	 * 增加调度
	 * @param explan
	 */
	public void saveExplan(Explan explan);
	
	/**
	 * 设置库存量
	 * @param supply
	 */
	public double getCntkc(Supply supply);
	
	/**
	 * 设置已加工量
	 * @param supply
	 */
	public double getCntplan1(Supply supply);
	
	/**
	 *  保存计划   修改需加工量  
	 * @param explan
	 */
	/**
	 * 批量  修改四个状态
	 * @param explan
	 */
	public void updateExplan(HashMap<String, Object> map);
	public void updateCnt(Explan explan);
	
	/**
	 * 保存登记   修改数量   数量2	
	 * @param explan
	 */
	public void updateCntact(Explan explan);
	
	/**
	 * 删除调度
	 * @param ids
	 */
	public void deleteExplan(List<String> ids);
	
	/**
	 * 增 删 改 方法
	 * @param explan
	 */
	public  void saveOrUpdateOrDelExplan(Explan explan);
	
	/**
	 * 修改调度
	 * @param explan
	 */
	public void checkExplan(Explan explan);
	
	/**
	 * 查询单条调度
	 * @param chkind
	 */
	public  Explan  findExplanByid(Explan explan);
	
	/**
	 * 删除调度
	 * @param listId
	 */
	public List<Map<String,Object>> findPlanFirm(Map<String,Object> condition);
	
	/**
	 * 根据仓位编码获取供应商编码
	 * @param supply
	 * @return
	 */
	public Deliver findDeliverByPositn(Deliver deliver);
	
	/**
	 * 修改报货单从表的标记状态bak1，代表已加入计划
	 * @param explan
	 */
	public void saveExplanByPlan(Map<String,Object> chkstodMap);
	
	/**
	 * 查询安全库存报货数据
	 * @param explan
	 */
	public List<Map<String,Object>> safeCntToPlan(HashMap<String, Object> map);

	/***
	 * 报货到加工间
	 * @author wjf
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findPlanFirm_new(Map<String, Object> map);

	/*****************************报货到加工间 新修改 2015.1.15wjf**********************************************/
	/***
	 * 查询是否走配送片区默认仓位 或者 出品加工间
	 * @author wjf
	 * @return
	 */
	public int findSpcodemodCount();

	/***
	 * 不走配送片区默认仓位和出品加工间的
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findPlanFirm1(Map<String, Object> map);

	/***
	 * 走配送片区默认仓位和出品加工间的
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findPlanFirm2(Map<String, Object> map);
}
