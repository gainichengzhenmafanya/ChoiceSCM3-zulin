package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.FirmDept;

public interface FirmDeptMapper {

	/**
	 * 查询分店部门
	 * @param acct
	 */
	public List<FirmDept> findFirmDept(FirmDept firmDept);
	
	/**
	 * 根据 分店 部门查询（为成本报表 分店档口毛利 组装column）
	 * @param firmDept
	 * @return
	 */
	public List<FirmDept> findFirmDeptByFirmType(FirmDept firmDept);
	/**
	 * 根据 分店 部门查询（为成本报表 分店档口毛利 组装column）
	 * @param firmDept
	 * @return
	 */
	public List<FirmDept> findFirmDeptByFirmType_storedept(FirmDept firmDept);
}
