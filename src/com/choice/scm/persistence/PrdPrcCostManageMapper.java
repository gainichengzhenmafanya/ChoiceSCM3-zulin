package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Grp;
import com.choice.scm.domain.ana.ProCostAna;


public interface PrdPrcCostManageMapper {

	/**
	 * 生产耗用合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForProCostAna(ProCostAna proCostAna);
	/**
	 * 生产耗用明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findProCostAna(ProCostAna proCostAna);
	/**
	 * 分店毛利查询合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForGrossProfit(Map<String,Object> conditions);
	/**
	 * 分店毛利查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findGrossProfit(Map<String,Object> conditions);
	/**
	 * 查询分店耗用对比
	 * @param conditions
	 * @return
	 */
	public List<String> findFirmUseCompare(Map<String,Object> conditions);
	/**
	 * 分店部门物资进出明细合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForCostVariance(Map<String,Object> content);
	/**
	 * 分店部门物资进出明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCostVariance(Map<String,Object> conditions);
	
	/**
	 * 分店部门物资进出明细合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForHejian(Map<String,Object> content);
	
	/**
	 * 核减明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findHejian(Map<String,Object> conditions);	
	
	/**
	 * 分店部门物资进出明细合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findHejianFoot(Map<String,Object> content);
	
	/**
	 * 查询毛利率周分析汇总
	 * @param prdct
	 * @return
	 */
	public List<Map<String,Object>> findMaoWeekAnaSum(Map<String,Object> conditions);
	
	/**
	 * 查询毛利率周分析
	 * @param prdct
	 * @return
	 */
	public List<Map<String,Object>> findMaoWeekAna(Map<String,Object> conditions);
	
	/**
	 * 查询毛利率周分析期初(年初到指定日期部分的 入减出)
	 * @param prdct
	 * @return
	 */
	public List<Map<String,Object>> findMaoWeekAnaQiChu1(Map<String,Object> conditions);
	
	/**
	 * 查询毛利率周分析期初(年初结存)
	 * @param prdct
	 * @return
	 */
	public List<Map<String,Object>> findMaoWeekAnaQiChu2(Map<String,Object> conditions);
	
	/**
	 * 查询费用类别
	 */
	public List<Grp> findGrp();
	
	/**
	 * 根据分店查询类别成本周趋势
	 */
	public List<Map<String,Object>> findGrpFirm(Map<String,Object> conditions);
	
	/**
	 * 根据分店查询类别成本周趋势汇总
	 */
	public List<Map<String,Object>> findGrpFirmSum(Map<String,Object> conditions);
	
	/**
	 * 应产率分析
	 */
	public List<Map<String,Object>> findShouldYieldAnaByHY(Map<String,Object> conditions);
	public List<Map<String,Object>> findShouldYieldAna(Map<String,Object> conditions);
	
	/**
	 * 菜品利润
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCprlDetailsInfo(Map<String,Object> conditions);

	/**
	 * 加工间综合利润分析
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findJGJLiRunAna(Map<String,Object> conditions);
	
	/**
	 * 加工间综合利润分析合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findJGJLiRunAnaSum(Map<String,Object> conditions);
	
	/**
	 * 加工产品成本理论成本
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findProductTheoryCost(Map<String,Object> conditions);
	
	/**
	 * 加工产品成本理论成本合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findProductTheoryCostSum(Map<String,Object> conditions);
}
