package com.choice.scm.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.ScheduleStore;
import com.choice.scm.domain.SpCodeUse;

public interface SpCodeUseMapper {

	/**
	 *  查找千人万元用量预估
	 * @return
	 */
	public List<SpCodeUse> listSpCodeUse(SpCodeUse spCodeUse);
	
	/**
	 * 获取点击率表达方式（单数，营业额，人数）
	 * @param acct
	 * @return
	 */
	public String getPlanTyp(@Param("acct")String acct);
	
	/**
	 *  查找千人万元用量预估
	 * @return
	 */
	public SpCodeUse getAmt(SpCodeUse spCodeUse);
	public SpCodeUse getAmt_pos(SpCodeUse spCodeUse);
	public SpCodeUse getAmt_boh(SpCodeUse spCodeUse);
	
	/**
	 *  查找预估物资的使用量信息
	 * @return
	 */
	public List<SpCodeUse> listSupplyAcct(SpCodeUse spCodeUse);
	
	/**
	 *  查找预估物资的使用量信息
	 * @return
	 */
	public List<SpCodeUse> listSupplyAcctCk(SpCodeUse spCodeUse);
	
	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param itemUse
	 * @return
	 */
	public void insertSpCodeUse(SpCodeUse spCodeUse);
	
	/**
	 * 删除物资用量预估
	 * @param 
	 * @return
	 */
	public void deleteAll(SpCodeUse spCodeUse);	
	
	/**
	 * 查询门店预估营业额
	 * @param spCodeUse
	 */
	public List<SpCodeUse> listPossalePlan(SpCodeUse spCodeUse);
	
	/**
	 * 原始的预估报货物资耗用计算
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSpSupply(SpCodeUse spCodeUse);
	
	/**
	 * 按照bom的预估报货物资耗用计算（门店）
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSupplyByBom(SpCodeUse spCodeUse);
	
	/**
	 * 按照bom的预估报货物资耗用计算（门店木屋）
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSupplyByMW(SpCodeUse spCodeUse);
	
	/**
	 * 按照bom的预估报货物资耗用计算(总部)
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSupplyByBomZb(SpCodeUse spCodeUse);
	
	/**
	 * 获取配送班表对应的该分店的可报货的报货分类
	 * @param spCodeUse
	 * @return
	 */
//	public List<GrpTyp> findGrpTypByPsbb(ScheduleStore scheduleStore);
	
	/**
	 * 获取配送班表对应的该分店的报货日期和到货日期
	 * @param spCodeUse
	 * @return
	 */
	public SpCodeUse findDateByPsbb(ScheduleStore scheduleStore);
	/**
	 * 预估报货物资耗用计算（总部木屋）
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSupplyByMWZB(SpCodeUse spCodeUse);
	/**
	 * 预估报货物资耗用计算（加工间木屋）
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSupplyByMWJGJ(SpCodeUse spCodeUse);
	
}
