package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Accpaysum;
import com.choice.scm.domain.Braset;
import com.choice.scm.domain.BrasetItem;
import com.choice.scm.domain.BrasetItemd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Recsum;

/**
 * 应收应付
 * Created by mc on 14-12-27.
 */
public interface AccruedMapper {
    /**
     * 获取 应收款汇总
     * @param recsum
     * @return
     */
    public List<Recsum> findRecsum(Recsum recsum);

    /**
     * 获取 应付款汇总
     * @param accpaysum
     * @return
     */
    public List<Accpaysum> findAccpaysum(Accpaysum accpaysum);

    /**
     * 获取 分店结算数据
     * @param braset
     * @return
     */
    public List<Braset> findBraset(Braset braset);
    /**
     * 获取 分店结算数据 根据单据编号
     * @param brasetItem
     * @return
     */
    public List<Braset> findBrasetByNo(BrasetItem brasetItem);

    /**
     * 获取 分店结算-支付明细
     * @param braset
     * @return
     */
    public List<BrasetItem> findBrasetItem(Braset braset);

    /**
     * 获取Folio表最大值并加一
     * @return
     */
    public int findFolioMaxId();

    /**
     * 保存支付明细
     * @param brasetItem
     */
    public void addBrasetItem(BrasetItem brasetItem);

    /**
     * 保存分摊明细
     * @param brasetItemd
     */
    public void addBrasetItemd(BrasetItemd brasetItemd);

    /**
     * 修改出库单金额
     * @param brasetItemd
     */
    public void updateChkoutmPay(BrasetItemd brasetItemd);

    /**
     * 修改入库单金额
     * @param brasetItemd
     */
    public void updateChkinmPay(BrasetItemd brasetItemd);
    /**
     * 修改出库单金额
     * @param brasetItem
     */
    public void updateChkoutmChk(BrasetItem brasetItem);

    /**
     * 修改入库单金额
     * @param brasetItem
     */
    public void updateChkinmChk(BrasetItem brasetItem);

    /**
     * 期初未接金额
     * @param chkoutm
     */
    public void addChkoutm(Chkoutm chkoutm);
}
