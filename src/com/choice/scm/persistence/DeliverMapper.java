package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.Role;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;

public interface DeliverMapper {

	/**
	 * 根据方向和分店查询供应商
	 * @param map
	 * @return
	 */
	public List<Deliver> findDeliverByInout(Map<String,Object> map);
	
	/**
	 * 根据方向和分店查询供应商
	 * @param map
	 * @return
	 */
	public List<Deliver> findDeliverByOut(Map<String,Object> map);
	
	/**
	 * 查询单条      供应商信息
	 * @return
	 */
	public Deliver findDeliverByCode(Deliver deliver);
	
	/**
	 * 查询所有的供应商信息
	 * @return
	 */
	public List<Deliver> findDeliver(Deliver deliver);
	
	/**
	 * 获取供应商中的最大编码+1返回
	 * @return
	 */
	public String getMaxCode();
	
	/**
	 * 查询所有的供应商信息
	 * @return
	 */
	public List<Deliver> findDeliverBy(Map<String,Object> map);
	
	
	/**
	 * 添加供应商信息
	 * @param deliver
	 */
	public void saveDeliver(Deliver deliver);
	
	/**
	 * 保存修改信息
	 * @param deliver
	 */
	public void updateDeliver(HashMap<String, Object> deliverMap);
	
	/**
	 * 保存修改信息====将修改的供应商名称同步到物资表中
	 * @param deliver
	 * @author wangchao
	 */
	public void updateSupply(HashMap<String, Object> deliverMap);
	
	/**
	 * 修改供应商对应仓位编码
	 * @param deliver
	 */
	public void saveDeliverPositn(Deliver deliver);
	
	/**
	 * 查询是否被引用----用于删除供应商时候的验证
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteyh(Deliver deliver);
	
	/**
	 * 根据编码获取供应商的名称
	 * @param deliver
	 * @return
	 */
	public Deliver selectDeliverByCode(Deliver deliver);
	
	/**
	 * 删除供应商信息
	 * @param codes
	 */
	public void deleteDeliver(List<String> codes);
	/**
	 * 获取供应商类别（已使用）
	 * @return
	 */
	public List<Deliver> findDeliverCategoryInUse();
	
	/**
	 * 查询账号所属角色
	 * @return
	 */
	public Role getRoleNm(Account account);
	/**
	 * 供应商查询送哪些物资
	 */
	public List<Dis> findAllDis(Dis dis);
	/**
	 * 供应商          采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis);
	/**
	 * 
	 * 查询供应商类型
	 */
	public List<Deliver> findDeliverType();

	/***
	 * 供应商     确认送货
	 * @param ids
	 */
	public void confirmSh(@Param(value="ids")String ids);

	/***
	 * 查询某个供应商    有几家 门店没有确认送货     
	 * @param dis
	 * @return
	 */
	public Integer findDeliverMessage1(Dis dis);

	/***
	 * 查询某个供应商 共有多少物资未送货
	 * @param dis
	 * @return
	 */
	public Integer findDeliverMessage2(Dis dis);
	
	/**
	 * @param dis *   供应商确认送所有的货
	 * 
	 */
	public void confirmShAll(Dis dis);
	
	/**
	 * 保存供应商物资到期提醒设置  wj
	 * @param map
	 */
	public void saveWarmDays(Map<String, Object> map);
	
	/**
	 * 跳转到添加物资界面
	 * @param materialScope
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialScope> tosaveDeliverSupply(MaterialScope materialScope) ;
	/**
	 * 添加供应物资范围
	 * @param delivercode
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public void insertDeliverSupply(MaterialScope materialScope);
	
	/**
	 * 删除物资
	 * @param materialScope
	 * @throws CRUDException
	 */
	public void deleteDeliverSupply(MaterialScope materialScope);
	
	/**
	 * 更新单价
	 * @param materialScopeList
	 * @return
	 * @throws CRUDException
	 */
	public boolean updateDeliverSupply(MaterialScope materialScope);
	
	/**
	 * 更新税率 jinshuai 20160425
	 * @param materialScopeList
	 * @return
	 * @throws CRUDException
	 */
	public boolean updateTaxRate(MaterialScope materialScope);
    
	/**
	 * 添加关联商城供应商
	 * @param deliver
	 * @param session
	 * @return
	 * @throws Exception
	 */
    public void updateJmuDeliver(Deliver deliver);
	
	/**
	 * 删除供应商信息
	 * @param codes
	 */
	public void deleteJumDeliver(List<String> codes);
    
    /**
     * 添加关联商城物资
     * @param materialScope
     * @param session
     * @return
     * @throws Exception
     */
    public int updateJmuSupply(MaterialScope materialScope);
	
	/**
	 * 删除供应商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	public void deleteJumSupply(MaterialScope materialScope);
	
	/**
	 * 查询供应物资范围的前十条供应商，包括税率，报价
	 * @return
	 */
	public List<MaterialScope> findGySpGysTop10(MaterialScope materialScope);
}
