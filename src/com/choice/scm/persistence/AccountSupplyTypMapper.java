package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.AccountSupplyTyp;

/***
 * 账号物资小类关联mapper
 * @author 王吉峰
 *
 */
public interface AccountSupplyTypMapper {

	/***
	 * 得到指定账号下的物资小类
	 * @param ast
	 * @return
	 */
	public List<AccountSupplyTyp> findAccountSupplyTyp(AccountSupplyTyp ast);

	/***
	 * 新增
	 * @param newAccountSupplyTyp
	 */
	public void saveAccountSupplyTyp(AccountSupplyTyp newAccountSupplyTyp);

	/***
	 * 删除
	 * @param accountSupplyTyp
	 */
	public void deleteAccountSupplyTyp(AccountSupplyTyp accountSupplyTyp);
}
