package com.choice.scm.persistence.reportFirm;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;

public interface SupplyAcctFirmMapper {

	/**
	 * 对比上期
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQComp(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSumQuery(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比月最大耗用）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQByMonthMax(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（当前日期）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQCur(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比日期）
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQPrev(Map<String,Object> conditions);
	
	/**
	 * 出库单据查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkout(Map<String,Object> conditions);
	
	/**
	 * 出库类别汇总
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmSum(Map<String,Object> condition);
	
	/**
	 *  出库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmTypSum(Map<String, Object> map);
	/**
	 *  出库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmSumQuery(Map<String, Object> map);
	/**
	 *  出库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmDetailQuery(Map<String, Object> map);
	
	/**
	 * 出库明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDetailQuery(Map<String,Object> conditions);
	
	/**
	 * 出库明细汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDetailSum(Map<String,Object> conditions);
	
	/**
	 * 出库明细查询（按售价计算）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDQBySale(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商进货汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverStockSum(Map<String,Object> conditions);
	/**
	 * 供应商进货汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverStockSum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverCategorySum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverCateSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 入库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 * 入库汇总合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 *  入库类别汇总
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmGrp(Map<String, Object> map);
	
	/**
	 * 入库类别汇总求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findChkinmCalSum(Map<String, Object> map);
	
	/**
	 * 入库明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailS(Map<String,Object> conditions);
	
	/**
	 * 查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCal(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总 合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 *  入库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmTypSum(Map<String, Object> map);
	
	/**
	 *  入库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSumQuery(Map<String, Object> map);
	
	/**
	 *  入库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmDetailQuery(Map<String, Object> map);
	
	/**
	 * 入库类别综合查询求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSum(Map<String, Object> map);
	
	/**
	 * 物资类别进出表
	 * @param content
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,Object>> findSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资类别进出表求和
	 * @author ZGL
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资明细进出表查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细进出表总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细账
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资明细账合计
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资余额列表查询
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findSupplyBalance(Map<String,Object> content);
	/**
	 * 物资余额列总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyBalance(Map<String,Object> content);
	
	/**
	 * 供应商汇总列表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 供应商汇总列表求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 进货单据汇总详细信息列表
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findStackBillSum(Map<String,Object> content);
	/**
	 * 进货单据汇总列表求和
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findCalForStockBillSum(Map<String,Object> content);
	
	/**
	 * 物资综合进出表
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplySumInOut(Map<String,Object> content);
	/**
	 * 物资综合进出表求和
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplySumInOut(Map<String,Object> content);
	
	/**
	 * 物资仓库进出表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findGoodsStoreInout(Map<String,Object> conditions);
	
	/**
	 * 物资仓库进出表合计查询 +
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForGoodsStoreInout(Map<String,Object> content);
	
	/**
	 * 调拨明细查询求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkAllotDetail(Map<String,Object> conditions);
	
	/**
	 * 调拨汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkAllotSumQuery(Map<String,Object> conditions);
	
	/**
	 * 调拨汇总查询求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkAllotSum(Map<String,Object> conditions);
	
	/**
	 * 存货盘点报表查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCunhuoPandian(Map<String,Object> conditions);
	
	/**
	 * 存货盘点报表查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCunhuoPandianHeji(Map<String,Object> conditions);
	
	/**
	 * 月末盘点报表查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findYuemoPandian(Map<String,Object> conditions);
	
	/**
	 * 月末盘点报表总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findYuemoPandianFoot(Map<String,Object> content);
	
	/**
	 * 月末盘点明细表查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findYuemoPandian2(Map<String,Object> conditions);
	
	/**
	 * 月末盘点汇总表总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findYuemoPandianFoot2(Map<String,Object> content);
	
	/**
	 * 差异管理查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChayiGuanli(Map<String,Object> conditions);
	public List<Map<String,Object>> findChayiGuanli_bzb(Map<String,Object> conditions);
	
	/**
	 * 每日差异报告查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findMeiriChayiDuizhao(Map<String,Object> conditions);
	public List<Map<String,Object>> findMeiriChayiDuizhao_bzb(Map<String,Object> conditions);
	
	/**
	 * 历史盘点查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findLishiPandian_bzb(Map<String,Object> conditions);
	
	/**
	 * 调拨汇总查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDbHuizong_bzb(Map<String,Object> conditions);
	
	/**
	 * 物资成本汇总表
	 * @param content
	 * @return
	 */
//	public List<Map<String,Object>> findWzChengbenHuizong(Map<String,Object> content);
	
	/**
	 * 物资成本成本表营业额获取
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> getNMoneyList(SupplyAcct supplyacct);
	
	/**
	 * 物资成本汇总表
	 * @param content
	 * @return
	 */
	public Map<String,Object> findWzChengbenHuizong(Map<String,Object> content);
	
	/**
	 * 物资成本明细表
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findWzChengbenMingxi(Map<String,Object> content);
	
	/**
	 * 物资成本明细表foot
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findWzChengbenMingxiHeji(Map<String,Object> content);
	
	/**
	 * 物资成本明细表营业额获取
	 * @param content
	 * @return
	 */
	public Map<String,Object> getNMoney(SupplyAcct supplyacct);
	
	/**
	 * 月成本综合分析营业额获取
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> getNMoneysList(SupplyAcct supplyacct);
	
	/**
	 * 月成本综合分析
	 * @param content
	 * @return
	 */
	public Map<String,Object> findYueChengbenZongheFenxi(Map<String,Object> content);
	/**
	 * 月成本综合分析 成本数据 YNKC
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findYueChengbenZongheFenxi_chengb(SupplyAcct supplyAcct);
	/**
	 * 月成本综合分析 水电气 费用 YNKC
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findYueChengbenZongheFenxi_sdq(SupplyAcct supplyAcct);
	/**
	 * 物资成本成本表营业额获取 YNKC=Y
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>>  findNMoneyList(SupplyAcct supplyAcct);

    /**
     * 月成本综合分析 合计 成本数据 YNKC
     * @param supplyAcct
     * @return
     */
    public List<Map<String,Object>> findYueChengbenZongheFenxi_chengb_hj(SupplyAcct supplyAcct);
    /**
     * 月成本综合分析 合计 水电气 费用 YNKC
     * @param supplyAcct
     * @return
     */
    public List<Map<String,Object>> findYueChengbenZongheFenxi_sdq_hj(SupplyAcct supplyAcct);
    /**
     * 物资成本成本表营业额 合计 YNKC=Y
     * @param supplyAcct
     * @return
     */
    public List<Map<String,Object>>  findNMoneyList_hj(SupplyAcct supplyAcct);
    
    /***
	 * 查询营业额choice3
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String, Object>> findNMoneyListChoice3(SupplyAcct supplyAcct);
	
	/***
	 * choice3查营业数据合计
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String, Object>> findNMoneyList_hjChoice3(SupplyAcct supplyAcct);
	
	/***
	 * choice3 查成本合计
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String, Object>> findYueChengbenZongheFenxi_chengb_hjChoice3(SupplyAcct supplyAcct);
	
	/***
	 * choice3 查成本
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String, Object>> findYueChengbenZongheFenxi_chengbChoice3(SupplyAcct supplyAcct);
	
	/**
	 * 存货汇总报表查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCunhuoHuizong(Map<String,Object> conditions);
	
	/**
	 * 第二单位查询-物资
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findSecondUnit_supply(Map<String, Object> map);
	
	/**
	 * 第二单位查询-日期
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findSecondUnit_date(Map<String, Object> map);
	
	/**
	 * 第二单位查询-差异
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findSecondUnitCy(Map<String, Object> map);
	
	/**
	 * 第二单位查询-合计
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findSecondUnitHeji(Map<String, Object> map);

	/***
	 * 查询周次，月次表头
	 * @param ws
	 * @return
	 */
	public List<WeekSet> findWeekSet(SupplyAcct sa);

	/***
	 * 各店毛利率查询
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findGdMaolilv(Map<String, Object> condition);

	/***
	 * 各店毛利率合计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findGdMaolilvSum(Map<String, Object> condition);

	/***
	 * 单菜毛利率
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findDcMaolilv(Map<String, Object> condition);

	/***
	 * 单菜毛利率合计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findDcMaolilvSum(Map<String, Object> condition);
	/***
	 * 单菜毛利率
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findDcMaolilv2(Map<String, Object> condition);
	
	/***
	 * 单菜毛利率合计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findDcMaolilvSum2(Map<String, Object> condition);

	/***
	 * 原料应产率
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findYlYingchanlv(Map<String, Object> condition);
	
	/***
	 * 原料应产率合计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findYlYingchanlvSum(Map<String, Object> condition);
	
	/***
	 * 西贝物资成本差异choice3
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findCostVarianceChoice3(Map<String, Object> conditions);

	/***
	 * 西贝物资成本差异choice3合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findCalForCostVarianceChoice3(Map<String, Object> conditions);
	
	/***
	 * 西贝物资成本差异choice3存储过程
	 * 调用存储过程的
	 * @param conditions
	 * @return
	 */
	public Map<String, Object> callScmPrcWZChenbenChayi(Map<String, Object> conditions);
	
	/***
	 * 西贝物资成本差异choic3查询临时表
	 * 调用存储过程的
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findTempWZChenbenChayi(Map<String, Object> conditions);
	
	/***
	 * 西贝物资成本差异choice3合计查询临时表
	 * 调用存储过程的
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findTempWZChenbenChayiHj(Map<String, Object> conditions);

	/***
	 * 
	 * @param condition
	 */
	public Map<String, Object> callScmPrcDcMaolilvTemp(Map<String, Object> condition);
	
	/***
	 * 单菜毛利率
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findDancaiMaolilv(Map<String, Object> conditions);
	
	/***
	 * 单菜毛利率
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findCalForDancaiMaolilv(Map<String, Object> conditions);
	
	/***
	 * 单菜毛利率实际耗用
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findDancaiMaolilvOut(Map<String, Object> conditions);
	
	/***
	 * 单菜毛利率理论耗用
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findDancaiMaolilvCost(Map<String, Object> conditions);
	
	/***
	 * 半成品成本分析
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findBcpChengbenFenxi(Map<String, Object> conditions);
	
	/***
	 * 半成品成本分析实际耗用
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findBcpChengbenFenxiOut(Map<String, Object> conditions);
	
	/***
	 * 半成品成本分析理论耗用
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findBcpChengbenFenxiCost(Map<String, Object> conditions);
}
