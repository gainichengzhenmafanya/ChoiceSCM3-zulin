package com.choice.scm.persistence;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;

public interface ChkinmMapper {
	
	/**
	 * 查询入库单号序列最大值
	 */
	public Integer getMaxChkinno();

	/**
	 * 查询所有入库单
	 * @param chkinm
	 * @return
	 */
	public List<Chkinm> findAllChkinm(Chkinm chkinm);
	
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinput(Map<String,Object> map);
	
	/**
	 * 分店mis
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmMisByinput(Map<String,Object> map);
	
	/**
	 * 模糊查询
	 * @param map
	 * @return
	 */
	public List<Chkinm> findAllChkinmByinputNew(Map<String,Object> map);
	/**
	 * 添加入库单
	 * @param chkinm
	 */
	public void saveChkinm(Chkinm chkinm);
	/**
	 * 修改入库单
	 * @param chkinm
	 */
	public void updateChkinm(Chkinm chkinm);
	/**
	 * 审核入库单
	 * @param chkinm
	 */
	public void checkChkinm(Chkinm chkinm);
	/**
	 * 查询单条入库单
	 * @param chkind
	 */
	public  Chkinm  findChkinmByid(Chkinm chkinm);
	/**
	 * 删除入库单
	 * @param listId
	 */
	public void deleteChkinm(List<String> listChkinno);
	/**
	 *  入库类别汇总
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmGrp(Map<String, Object> map);
	/**
	 * 入库类别汇总求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findChkinmCalSum(Map<String, Object> map);
	/**
	 *  入库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmTypSum(Map<String, Object> map);
	/**
	 *  入库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSumQuery(Map<String, Object> map);
	/**
	 *  入库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmDetailQuery(Map<String, Object> map);
	/**
	 * 入库类别综合查询求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSum(Map<String, Object> map);
	
	/**
	 * 查询默认仓位
	 * @param chkinm
	 */
	public List<SpCodeMod> getPositn(SpCodeMod spCodeMod);
	public List<Supply> getPositn1(Supply supply);
	
	/**
	 * 查询默认仓位是否正确
	 * @param chkinm
	 */
	public List<SpCodeMod> chkPositn(SpCodeMod spCodeMod);
	
	/**
	 * 获取入库数据
	 * @param chkinm
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch);
	
	/**
	 * 获取直发冲消数据
	 * @param spbatch
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch);
	/**
	 * 获取直发冲消数据--分店
	 * @param spbatch
	 */
	public List<Spbatch> addChkinmzfByCx_x(Spbatch spbatch);

	/**
	 *  查询入库金额汇总
	* @param map
	* @return
	 */
	public Chkinm findAllChkinmTotal(Map<String, Object> map);

	/**
	 * 查询付款记录
	* @param map
	* @return
	 */
	public List<Map<String, Object>> findPayData(Map<String, Object> map);
	
	/**
	 * 查询发票记录
	* @param map
	* @return
	 */
	public List<Map<String, Object>> findBillData(Map<String, Object> map);
	
	/**
	 * 审核结账
	* @param chkinm
	* @return
	 */
	public void checkedChkinmBill(@Param("chkinno")String chkinno);
	
	/**
	 * 根据主键字符串查找数据
	* @param replaceCode
	* @return
	 */
	public List<Chkinm> findChkinmByIds(@Param("chkinno")String replaceCode);

	/**
	 * 更新付款记录
	* @param chk
	 */
	public void updateChkinmPay(Chkinm chk);

	public void addFolio(Chkinm chk);

	public void addFolioChkm(Chkinm chk);

	public void addBill(Chkinm chkinm);

	public void addDeliverMoney(Chkinm chkinm);

	/***
	 * folio表主键
	 * @return
	 */
	public Integer getGen_foliNextVal();

	/***
	 * 根据入库单号查询供应商是否加工间 如果是加工间则此入库单需要进行核减
	 * @param chkinm
	 * @return
	 */
	public int findDeliverTypByChkinno(Chkinm chkinm);
}
