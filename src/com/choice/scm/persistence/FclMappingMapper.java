package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FclMapping;
import com.choice.scm.domain.Positn;

/**
 * 描述：财务科目映射设置
 * 日期：2014年1月12日 上午11:18:43
 * 作者：毛明武mmw
 * 文件：FclMappingMapper.java
 */
public interface FclMappingMapper {
	/***查出所有数据***/
	public List<FclMapping> findAllFclMapping(FclMapping fclMapping);
	/***根据id查出信息***/
	public FclMapping findFclMappingById(String pk_id);
	/***修改***/
	public void updateFclMapping(FclMapping fclMapping);
	/***新增***/
	public void saveFclMapping(FclMapping fclMapping);
	/***根据id删除***/
	public void deleteFclMappingById(List<String> listId);
	/**
	 * 描述:参照界面
	 * 作者:mmw
	 * 日期:2014-1-12上午10:12:32
	 * @param listId
	 * @return
	 */
	public List<FclMapping> findAllFclMappingbyId(List<String> listId);
	/**
	 * 查询所有的供应商信息
	 * @return
	 */
	public List<Deliver> findDeliver(Deliver deliver);
	/**
	 * 查询所有的仓位信息
	 * @return
	 */
	public List<Positn> findPositn(Deliver deliver);
	/**
	 * 查询所有的客户信息
	 * @return
	 */
	public List<Deliver> findCustomer(Deliver deliver);

}
