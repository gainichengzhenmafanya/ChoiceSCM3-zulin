package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;


public interface DisMapper {

	/**
	 * 报货分拨、报货验收
	 */
	public List<Dis> findAllDis(Dis dis);
	
	/**
	 * 报货分拨、报货验收 分页查询
	 */
	public List<Dis> findAllDisPage(Dis dis);

	/**
	 * 报货分拨 报货验收 总数
	 */
	public int findAllDisCount(Dis dis);
	
	/**
	 * 批量修改      验收入库      验收出库    采购确认   采购审核  的标志字段      的统一 方法
	 */
	public void updateByIds(Dis dis);
	
	/**
	 * 修改
	 */
	public void updateDis(Dis dis);
	
	/**
	 * 查询出入库状态
	 */
	public List<Chkstod> findChkByKey(String id);
	
	/**
	 * 报货分拨，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis);
	
	/**
	 * 报货分拨，配送汇总
	 */
	public List<Dis> findPeiSongTotal(Dis dis);
	
	/**
	 * 获得收货单数据（分店接受配送中心送货用）
	 */
	public List<SupplyAcct> selectByKey(HashMap<String,Object> disMap);
	
	/**
	 * 更新验货信息
	 * @param supplyAcct
	 */
	public void updateAcct(SupplyAcct supplyAcct);
	
	/**
	 * 更新验货审核信息
	 * @param supplyAcct
	 */
	public void check(HashMap<String, Object> disMap);
	
	/**
	 * 获取入库单数据
	 */
	public List<Dis> findSupplyacctList(Dis dis);
	
	/**
	 * 更新验货信息
	 * @param supplyAcct
	 */
	public void updateYnDept(HashMap<String, Object> disMap);
	
	public List<Dis> findAllMisDisPage(Dis dis);
	
	public List<Dis> findSupplyacctDeptList(Dis dis);

	/***
	 * 当前分店所属片区的主直拨库对应的供应商
	 * @param positn
	 * @return
	 */
	public Deliver findTpDeliver(Positn positn);

	/***
	 * 直配验货查询多档口
	 * @param dis
	 * @return
	 */
	public List<Dis> findChkstodList(Dis dis);

	/***
	 * 是否有多档口报货来的数据
	 * @param ids
	 * @return
	 */
	public int findDeptChkstom(@Param(value="ids")String ids);

	/***
	 * 更新chkstod_dept chkin 字段为已验货
	 * @param sp_ids
	 */
	public void updateChkstodDeptChkin(@Param(value="sp_ids")String sp_ids);

	/***
	 * 查询调拨出库的门店对应的供应商
	 * @param positn
	 * @return
	 */
	public Deliver findDeliverByPositn(Positn positn);

	/***
	 * 统配验货修改报货单为已验货，安全库存报货用
	 * @param chkind
	 */
	public void updateChkstodByChkstonoSpcode(Chkind chkind);
	
	/***
	 * 根据档口报货单从表更新报货单状态为已验货
	 * @param sp_ids
	 */
	public void updateChkstodByChkstodDeptId(@Param(value="sp_ids")String sp_ids);

}
