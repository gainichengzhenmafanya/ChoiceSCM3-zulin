package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkpayd;
import com.choice.scm.domain.Chkpaym;

public interface ChkpaydMapper {

	
	/**
	 * 获取当前序号
	 * @return
	 */
	public int getCurId();

	/**
	 * 添加报销单副表
	 * @param chkpayd
	 */
	public void saveNewChkpayd(Map<String,Object> chkpaydMap);
	
	/**
	 * 根据单号查询
	 * @param chkpayd
	 * @return
	 */
	public List<Chkpayd> findByChkno(Chkpayd chkpayd);
	
	/**
	 * 根据id查询
	 * @param chkpayd
	 * @return
	 */
	public Chkpayd findById(Chkpayd chkpayd);
	/**
	 * 查询所有的报销单
	 * @return
	 */
	public List<Chkpayd> findAllChkpayd(Chkpaym chkpaym);
	
	/**
	 * 打印报销单
	 * @param chkpaym
	 * @return
	 */
	public Map<String,Object> printChkpay(Chkpaym chkpaym);
	/**
	 * 保存修改
	 * @param chkpayd
	 */
	public void saveByUpdate(Chkpayd chkpayd);
	/**
	 * 删除报销单副表
	 * @param chkpayd
	 */
	public void deleteChkpayd(Map<String,Object> map);
}
