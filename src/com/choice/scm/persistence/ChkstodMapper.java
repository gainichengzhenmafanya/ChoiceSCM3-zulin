package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;


public interface ChkstodMapper {

	/**
	 * 查询报货单副表
	 * @return
	 */
	public List<Chkstod> findChkstod(Chkstod chkstod);
	/**
	 * 查找所有的报货单
	 * @return
	 */
	public List<Chkstod> findAllChkstod(Chkstod chkstod);
	/**
	 * 根据仓位查找报货单
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByPositn(Chkstod chkstod);
	
	/**
	 * 添加报货单副表
	 * @param chkstodemo
	 */
	public void saveNewChkstod(Chkstod chkstod);
	public void saveNewChkstodMis(Chkstod chkstod);
	
	/**
	 * 根据id查询
	 * @param chkstod
	 * @return
	 */
	public Chkstod findById(Chkstod chkstod);
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod);
	/**
	 * 保存修改
	 * @param chkstod
	 */
	public void saveByUpdate(Map<String,Object> chkstod);
	
	/**
	 * 删除报货单
	 * @param sp_codes
	 */
	public void delete(Map<String,Object> map);
	
	/**
	 * 审核单条
	 * @param sp_codes
	 */
	public void checkOneChkstom(Chkstom chkstom);
	
	/**
	 * 审核多条
	 * @param param
	 * @return
	 */
	public String checkMoreChkstom(Map<Object, Object> param);
	
	/***
	 * 新的报货单从表保存页面  2014.12.27wjf
	 * @param chkstod
	 */
	public void saveNewChkstod_new(Chkstod chkstod);
}
