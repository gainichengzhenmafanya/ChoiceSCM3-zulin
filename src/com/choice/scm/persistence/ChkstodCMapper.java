package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Chkstod;


public interface ChkstodCMapper {

	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod);
}
