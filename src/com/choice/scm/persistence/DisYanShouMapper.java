package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Dis;


public interface DisYanShouMapper {

	/**
	 * 报货验收
	 */
	public List<Dis> findAllDis(Dis dis);

	/**
	 * 报货验收查询总数
	 */
	public int findAllDisCount(Dis dis);
	
	/**
	 * 报货验收ajax分页
	 */
	public List<Dis> findAllDisPage(Dis dis);
	
	/**
	 * 批量修改      验收入库      验收出库    采购确认   采购审核  的标志字段      的统一 方法
	 */
	public void updateByIds(Dis dis);
	
	/**
	 * 修改
	 */
	public void updateDis(Dis dis);
	
	/**
	 * 批量修改 已对
	 */
	public void updateAll(Dis dis);
	
	/**
	 * 查询出入库状态
	 */
	public Chkstod findChkByKey(String id);
}
