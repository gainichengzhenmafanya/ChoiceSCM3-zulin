package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Praybilld;


public interface PraybilldMapper {
	
	/**
	 * 添加请购单副表
	 * @param praybilld
	 */
	public void saveNewPraybilld(Praybilld praybilld);
	
	/***
	 * 根据主表单号查询
	 * @param praybilld
	 * @return
	 */
	public List<Praybilld> findByPraybillmNo(Praybilld praybilld);
	
	/**
	 * 查找所有的请购单
	 * @return
	 */
	public List<Praybilld> findAllPraybilld(Praybilld praybilld);
	
}
