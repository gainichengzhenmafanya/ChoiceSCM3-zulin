package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.FirmCostAvg;
import com.choice.scm.domain.SpCodeExPrice;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.SupplyAcct;

public interface BanChengPinPriceMapper {
	/**
	 * 查询 中心费用
	 * @param firmCostAvg
	 * @return
	 */
	public List<FirmCostAvg> findFirmCostAvg(FirmCostAvg firmCostAvg);
	/**
	 * 查询 中心费用 项
	 * @return
	 */
	public List<FirmCostAvg> findCodeDes();
	/**
	 * 保存 中心费用
	 * @return
	 */
	public void saveFirmCostAvg(FirmCostAvg firmCostAvg);
	/**
	 * 查询该物资实际耗用  根据物资，查询supplyacct出库数据
	 */
	public double findsupplyCnt(Map<String, Object> map);
	
	/**
	 * 删除  中心费用
	 * @param firmCostAvg
	 */
	public void delectFirmCostAvg(@Param("yearr")String yearr,@Param("monthh")String monthh);
	
	/**
	 * 查询  加工工时  ----主记录
	 */
	public List<SpCodeExPrice> findSpCodeExPrice(SpCodeExPrice spCodeExPrice);
	
	/**
	 * 查询  加工工时  ---  主记录   不重复的加工间
	 */
	public List<SpCodeExPrice> findSpCodeExPricePositn(SpCodeExPrice spCodeExPrice);
	
	/**
	 * 查找  加工工时
	 * @param spCodeExPrice
	 */
	public SpCodeExPrice selectSpCodeExPrice(SpCodeExPrice spCodeExPrice);

	/**
	 * 更新  加工工时
	 * @param spCodeExPrice
	 */
	public void updateSpCodeExPrice(SpCodeExPrice spCodeExPrice);
	
	/**
	 * 添加  加工工时
	 * @param spCodeExPrice
	 */
	public void saveSpCodeExPrice(SpCodeExPrice spCodeExPrice);	
	
	/**
	 * 查询  费用
	 * @param firmCostAvg
	 * @return
	 */
	public List<FirmCostAvg> findSpexfeicost(FirmCostAvg firmCostAvg);
	
	/**
	 * 删除 费用
	 * @param firmCostAvg
	 */
	public void deleteSpexFeiCost(FirmCostAvg firmCostAvg);
	
	/**
	 * 添加 费用
	 * @param firmCostAvg
	 */
	public void insertSpexFeiCost(FirmCostAvg firmCostAvg);
	/**
	 * 查询当月半成品理论原材料耗用
	 * @param map
	 * @return
	 */
	public List<Spcodeexm> findSpcodeexmByMonth(Map<String, Object> map);
	/**
	 * 计算原材料价格
	 * @param map
	 * @return
	 */
	public List<SupplyAcct> findPriceBySupplyAcct(Map<String, Object> map);
	
	/**
	 * 调用存储过程更新半成品入库单价格
	 */
	public void updateEXPriceByPosint(SpCodeExPrice spCodeExPrice);
}
