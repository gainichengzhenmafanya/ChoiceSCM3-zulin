package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;

public interface Chkstom_xMapper {

	/**
	 * 查看上传
	 * @param chkstom
	 * @return
	 */
	public List<Chkstom> findUpload(Chkstom chkstom);
	
	/**
	 * 查看未上传
	 * @return
	 */
	public List<Positn> findNoUpload(Chkstom chkstom);
	
	/**
	 * 查看未上传数目
	 * @return
	 */
	public int findNoUploadCount(Chkstom chkstom);
	
	/**
	 * 查询所有的报货单
	 * @param chkstom
	 * @return
	 */
	public List<Chkstom> findChkstom(Chkstom chkstom);
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom);
	/**
	 * 该分店今天     已审核多少条， 未审核多少条
	 */
	public Chkstom findCountByFirmId(Chkstom chkstom);
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxChkstono();
	
	/**
	 * 添加报货单主表
	 * @param chkstom
	 */
	public void saveNewChkstom(Chkstom chkstom);
	
	/**
	 * 更新出库单
	 * @param chkstomMap
	 */
	public void updateChkstom(Chkstom chkstom);
	/**
	 * 关键字查询
	 * @param chkstomMap
	 * @return
	 */
	public List<Chkstom> findByKey(HashMap<Object,Object> chkstomMap);
	/**
	 * 删除
	 */
	public void deleteChk(Chkstom chkstom);
	/**
	 * 审核
	 */
	public void checkChk(Chkstom chkstom);
	
	/**
	 * 根据主表单据号查询字表物资数量
	 * @param chkstono
	 * @return
	 */
	public int findSupplyCountByChkstoNo(Chkstom chkstom);
}
