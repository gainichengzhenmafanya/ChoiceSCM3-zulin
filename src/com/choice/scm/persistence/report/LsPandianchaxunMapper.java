package com.choice.scm.persistence.report;

import java.util.List;
import java.util.Map;

public interface LsPandianchaxunMapper {

	/***
	 * 查询历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findLsPandianDetailQuery(Map<String, Object> content);

	/***
	 * 查询历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findLsPandianCount(
			Map<String, Object> content);
}
