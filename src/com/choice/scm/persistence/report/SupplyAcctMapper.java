package com.choice.scm.persistence.report;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.domain.SupplyAcct;

public interface SupplyAcctMapper {

	/**
	 * 毛利查询合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findGrossProfitSum(Map<String,Object> conditions);
	
	/**
	 * 毛利查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findGrossProfit(Map<String,Object> conditions);
	
	/**
	 * 仓库类别进出表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findStockCategoryInOut(Map<String,Object> conditions);
	
	/**
	 * 仓库类别进出表求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForStockCategoryInOut(Map<String,Object> conditions);
	
	/**
	 * 出库单据汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutOrderSum(Map<String,Object> conditions);
	
	/**
	 * 出库单据汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutOrderSum(Map<String,Object> conditions);

	/**
	 * 对比上期
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQComp(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSumQuery(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比月最大耗用）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQByMonthMax(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（当前日期）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQCur(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比日期）
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQPrev(Map<String,Object> conditions);
	
	/**
	 * 出库类别汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutCategorySum(Map<String,Object> conditions);
	/**
	 * 出库类别汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutCategorySum(Map<String,Object> conditions);
	
	/**
	 * 出库明细查询          和求和
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDetailQuery(Map<String,Object> conditions);
	public List<Map<String,Object>> findChkoutDetailQuerySUM(Map<String,Object> conditions);
	
	/**
	 * 出库明细查询（按售价计算）         和求和
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDQBySale(Map<String,Object> conditions);
	public List<Map<String,Object>> findChkoutDQBySaleSUM(Map<String,Object> conditions);
	/**
	 * 出库明细汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDetailSum(Map<String,Object> conditions);
	
	/**
	 * 出库单据查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkout(Map<String,Object> conditions);
	
	/**
	 * 出库日期汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDateSum(Map<String,Object> conditions);
	
	/**
	 * 出库日期汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutDateSum(Map<String,Object> conditions);
	
	/**
	 * 出库日期汇总（按售价计算）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDSBySale(Map<String,Object> conditions);
	
	/**
	 * 按售价计算 合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutDSBySale(Map<String,Object> conditions);
	
	/**
	 * 出库日期汇总1
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDateSum1(Map<String,Object> conditions);
	
	/**
	 * 出库日期汇总2
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDateSum2(Map<String,Object> conditions);
	
	/**
	 * 出库按月汇总合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutMonthSumHJ(Map<String,Object> conditions);
	
	/**
	 * 出库按月汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutMonthSum(Map<String,Object> conditions);
	
	/**
	 * 统计会计月起止日期
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findMainMonthDate(Map<String,Object> conditions);
	
	/**
	 * 出库单，日期报表求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutDate(Map<String,Object> conditions);
	
	/**
	 * 出库盈利查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutProfitQuery(Map<String,Object> conditions);
	
	/**
	 * 出库盈利查询求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutProfitQuery(Map<String,Object> conditions);
	
	/**
	 * 调拨汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkAllotSumQuery(Map<String,Object> conditions);
	
	/**
	 * 调拨汇总查询求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkAllotSum(Map<String,Object> conditions);
	
	/**
	 * 调拨明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkAllotDetailQuery(Map<String,Object> conditions);
	
	/**
	 * 调拨明细查询求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkAllotDetail(Map<String,Object> conditions);
	
	/**
	 * 分店类别汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findPositnCategorySum(Map<String,Object> conditions);
	
	/**
	 * 类别分店汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryPositnSum(Map<String,Object> conditions);
	
	/**
	 * 供应商分店汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverPositnSum(Map<String,Object> conditions);
	
	/**
	 * 供应商分店汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverPositnSum(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商汇总列表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 供应商汇总列表求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 供应商进货汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverStockSum(Map<String,Object> conditions);
	/**
	 * 供应商进货汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverStockSum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverCategorySum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverCateSum(Map<String,Object> conditions);
	
	/**
	 * 进货单据汇总详细信息列表
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findStackBillSum(Map<String,Object> content);
	/**
	 * 进货单据汇总列表求和
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findCalForStockBillSum(Map<String,Object> content);
	
	/**
	 * 类别分店汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryPositnSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总1
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum(Map<String,Object> conditions);
	/**
	 * 类别供应商汇总1求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 配送验货查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliveryExamineQuery(Map<String,Object> conditions);
	/**
	 * 配送验货求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliveryExamineQuery(Map<String,Object> conditions);
	
	/**
	 * 入库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 * 入库汇总合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 *  入库类别汇总
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmGrp(Map<String, Object> map);
	
	/**
	 * 入库类别汇总求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findChkinmCalSum(Map<String, Object> map);
	
	/**
	 * 入库明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailS(Map<String,Object> conditions);
	
	/**
	 * 查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCal(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总 合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 *  入库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmTypSum(Map<String, Object> map);
	
	/**
	 *  入库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSumQuery(Map<String, Object> map);
	
	/**
	 *  入库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmDetailQuery(Map<String, Object> map);
	
	/**
	 * 入库类别综合查询求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSum(Map<String, Object> map);
	
	/**
	 * 物资仓库进出表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findGoodsStoreInout(Map<String,Object> conditions);
	
	/**
	 * 物资仓库进出表合计查询 +
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForGoodsStoreInout(Map<String,Object> content);
	
	/**
	 * 物资类别进出表
	 * @param content
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,Object>> findSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资类别进出表求和
	 * @author ZGL
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资明细进出表查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细进出表总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细账
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资明细账合计
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资余额列表查询
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findSupplyBalance(Map<String,Object> content);
	/**
	 * 物资余额列表查询
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public Double findSupplyBalanceOnlyEndNum(SupplyAcct supplyAcct);
	/**
	 * 物资余额列总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyBalance(Map<String,Object> content);
	
	/**
	 * 物资综合进出表
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplySumInOut(Map<String,Object> content);
	/**
	 * 物资综合进出表求和
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplySumInOut(Map<String,Object> content);
	/**
	 * 门店日盘点状态表
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findMdRiPanDianZhuangTaiBiao(SupplyAcct content);
	
	/**
	 * 分店盈利情况表
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findFdYingliQingkuang(Map<String,Object> conditions);
	public List<Map<String,Object>> findFdYingliQingkuangSUM(Map<String,Object> conditions);

	/***
	 * 供应商类别报表  热辣定制
	 * @author wjf
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findDeliverCategorySum2(Map<String, Object> map);

	/***
	 * 有效期查询
	 * @author wjf
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findYxqCal(Map<String, Object> conditions);
	
	/***
	 * 有效期查询
	 * @author wjf
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findYxqDetailS(Map<String, Object> map);

	/***
	 * 出库日期汇总1 合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findChkoutDateSumHJ1(
			Map<String, Object> conditions);

	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @param supplyAcct
	 * @return
	 */
	public PositnSupply findViewPositnSupply(SupplyAcct supplyAcct);
	
	/**
	 * 直配验货查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDireInspectionQuery(Map<String,Object> conditions);
	/**
	 * 直配验货求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDireInspectionQuery(Map<String,Object> conditions);

	/***
	 * 查询分店物资汇总 
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findWzFendianHuizong(Map<String, Object> conditions);

	/***
	 * 查询分店物资汇总合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findWzFendianHuizongSum(Map<String, Object> conditions);

	/***
	 * 门店盘点分析表
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianFenxi(Map<String, Object> conditions);
	
	/***
	 * 门店盘点分析表
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianFenxiSum(Map<String, Object> conditions);
	
	/***
	 * 门店盘点分析表
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdHaoyongFenxi(Map<String, Object> conditions);
	
	/***
	 * 门店盘点分析表
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdHaoyongFenxiSum(Map<String, Object> conditions);

	/***
	 * 查询所有物资小类
	 * @param acct
	 * @return
	 */
	public List<GrpTyp> findAllType(@Param(value="typ")String typ);

	/***
	 * 查询供应商类别汇总3
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findDeliverCategorySum3(Map<String, Object> map);

	/***
	 * 供应商类别汇总3合计
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findDeliverCategorySum3hj(Map<String, Object> map);
	
	
	/**
	 * 根据时间段查询仓库类别进程明细
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> getCangkuLeibieJincheSd(Map<String, Object> content);

	/**
	 * 根据时间段查询仓库类别进程明细合计
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> getCangkuLeibieJincheHj(Map<String, Object> content);
	
	/**
	 * 领用供应商类别汇总
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> lyGysLeibieHuizongDetail(Map<String, Object> content);
	
	/**
	 * 领用供应商类别汇总合计
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> lyGysLeibieHuizongSum(Map<String, Object> content);
	/**
	 * 销售订单报表详情
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> xiaoShouDingDanDetail(Map<String, Object> content);
	
	/**
	 * 销售订单报表详情合计
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> xiaoShouDingDanSum(Map<String, Object> content);
		
	
	/***
	 * 西贝仓库类别进出2 choice3存储过程
	 * 调用存储过程的
	 * @param conditions
	 * @return
	 */
	public Map<String, Object> callScmPrcCkLeibieJinChuSecond(Map<String, Object> conditions);
	
	/***
	 * 西贝仓库类别进出2 
	 * 查询临时表
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findTempCkLeiBieJinChuSecond(Map<String, Object> conditions);
	
	/***
	 * 西贝仓库类别进出2 
	 * 查询临时表合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findTempCkLeiBieJinChuSecondHj(Map<String, Object> conditions);
	

}
