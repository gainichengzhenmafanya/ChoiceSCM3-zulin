package com.choice.scm.persistence.report;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.report.CustomReport;

public interface CustomReportScmMapper {
	/**
	 * 描述：查询树
	 * @return
	 * author:spt
	 * 日期：2014-6-6
	 */
	public List<Map<String, Object>> listCustomReportTree();
	/**
	 * 描述：查询树
	 * @return
	 * author:spt
	 * 日期：2014-6-6
	 */
	public CustomReport findCustomReport(String pk_customReport);
	/**
	 * 描述：根据id  查询自定义报表对象
	 * @param pk_customReport
	 * @return
	 * author:spt
	 * 日期：2014-6-6
	 */
	public CustomReport searchReport(String pk_customReport);
	/**
	 * 描述：添加树节点
	 * @param customReport
	 * author:spt
	 * 日期：2014-6-6
	 */
	public void saveCustomReport(CustomReport customReport);
	/**
	 * 描述：删除自定义节点
	 * @param pk_customReport
	 * author:spt
	 * 日期：2014-6-6
	 */
	public void deleteCustomReport(String pk_customReport);
	
	/**
	 * 描述：修改自定义节点
	 * @param customReport
	 * author:spt
	 * 日期：2014-6-6
	 */
	public void updateCustomReport(CustomReport customReport);
}
