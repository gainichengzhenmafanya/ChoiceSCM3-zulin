package com.choice.scm.persistence.report;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkstore;
import com.choice.scm.domain.SupplyAcct;

public interface MdPandianchaxunMapper {
	
	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandian(Chkstore chkstore);

	/***
	 * 查询门店历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianCount(Chkstore chkstore);

	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianDetailQuery(Chkstore chkstore);

	/***
	 * 查询门店历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianMingxiCount(Chkstore chkstore);
	
	/***
	 * 查询门店历史盘点数据
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianMingxiDetailQuery(Chkstore chkstore);

	/***
	 * 查询门店历史盘点汇总
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findMdPandianDetailCount(Chkstore chkstore);

	/**
	 * 门店盘点核减状态查询
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findMdPdhjState(SupplyAcct conditions);
}
