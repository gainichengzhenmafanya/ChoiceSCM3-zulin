package com.choice.scm.persistence.report;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.SupplyAcct;

public interface MdYanhuoChaxunMapper {

	/**
	 * 门店盘点核减状态查询
	 * 
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findYanhuoChaxunDatas(SupplyAcct conditions);
}
