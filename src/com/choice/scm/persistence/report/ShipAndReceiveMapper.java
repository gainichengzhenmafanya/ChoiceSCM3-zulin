package com.choice.scm.persistence.report;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.SupplyAcct;
public interface ShipAndReceiveMapper {

	/**
	 * 查询收发货
	 * @param shipAndReceive
	 */
	public List<SupplyAcct> findShipAndReceiveList(HashMap<String, Object> disMap);
}
