package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.HolidayBoh;

public interface HolidayBohMapper {
	
	/**
	 * 显示所有的节假日信息
	 * @param holiday
	 * @return
	 */
	public List<HolidayBoh> list(HolidayBoh holiday);
	
	/**
	 * 通过节假日主键查询节假日信息
	 * @param pk_holidaydefine
	 * @return
	 */
	public HolidayBoh fingHolidayById(String pk_holidaydefine);
	
	/**
	 * 新增节假日信息
	 * @param holiday
	 */
	public void saveHoliday(HolidayBoh holiday);
	
	/**
	 * 修改节假日信息
	 * @param holiday
	 */
	public void updateHoliday(HolidayBoh holiday);
	
	/**
	 * 删除节假日信息
	 * @param listId
	 */
	public void deleteHoliday(HolidayBoh holiday);
	
	/**
	 * 通过年份删除节假日信息
	 * @param holiday
	 */
	public void deleteByVyear(HolidayBoh holiday);
	
	/**
	 * 通过日期查询节假日信息  add ygb 20160120
	 * @param dholidaydate
	 * @return
	 */
	public List<HolidayBoh> fingHolidayByDholidaydate(String dholidaydate);
}
