package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

public interface LeiBieChengBenMapper {
	/**
	 * 类别成本详细
	 * 
	 * @param content
	 * @return
	 */
	public List<Map<String, Object>> findLeiBieChengBen(Map<String, Object> content);

	/**
	 * 类别成本合计
	 * 
	 * @param conditions
	 * @return
	 */
	public List<Map<String, Object>> findLeiBieChengBenSum(Map<String, Object> conditions);
}
