package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.Arrivald;
import com.choice.scm.domain.Arrivalm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;


/***
 * 供应商平台mapper
 * @author wjf
 *
 */
public interface DeliverPlatMapper {
	
	/***
	 * 账号关联供应商
	 * @param accountId
	 * @param deliverCode
	 */
	public void saveAccountSupplier(@Param(value="accountId")String accountId, @Param(value="deliverCode")String deliverCode);

	/***
	 * 根据账号查询所属供应商
	 * @param account
	 * @return
	 */
	public Deliver findDeliverByAccount(@Param(value="account")String account);
	
	/***
	 * 报货查询
	 * @param dis
	 * @return
	 */
	public List<Dis> findChkstom(Dis dis);
	
	/***
	 * 门店报货汇总
	 * @param dis
	 * @return
	 */
	public List<Dis> findChkstomSum(Dis dis);
	
	/***
	 * 确认门店报货单
	 * @param ids
	 */
	public void confirmChkstom(@Param(value="ids")String ids);

	/***
	 * 查询门店到货单表
	 * @param dis
	 * @return
	 */
	public List<Dis> findInspection(Dis dis);
	
	/***
	 * 供应商用查询门店到货单表
	 * @param dis
	 * @return
	 */
	public List<Dis> findInspectionBZB(Dis dis);

	/***
	 * 查询门店到货汇总
	 * @param dis
	 * @return
	 */
	public List<Dis> findInspectionSum(Dis dis);
	
	/***
	 * 验货确认
	 * @param dis
	 */
	public void confirmIns(Dis dis);
	
	/***
	 * 查询九毛九直配到货单
	 * @param arrivalmMis
	 * @return
	 */
	public List<Arrivalm> findArrivalmList(Arrivalm arrivalm);

	/***
	 * 根据到货单主表 查询到货单从表
	 * @param arrivalm
	 * @return
	 */
	public List<Arrivald> findArrivaldList(Arrivalm arrivalm);
	
	/***
	 * 修改九毛九直配验货数量
	 * @param ad
	 */
	public void updateArrivald(Arrivald ad);

	/***
	 * 修改到货单状态为已验货
	 * @param arrivalmMis
	 */
	public void updateArrivalm(Arrivalm arrivalm);

	/***
	 * 更新到货单状态2
	 * @param arrivalmMis
	 */
	public void updateArrivalm2(Arrivalm arrivalmMis);

	/***
	 * 更新报货单表的状态
	 * @param arrivalmMis
	 */
	public void updateChkstodByArrivalm(Arrivalm arrivalmMis);

	/***
	 * 根据到货单查询报货门店
	 * @param arrivalmMis
	 * @return
	 */
	public List<Positn> findPositnByArrivalm(Arrivalm arrivalmMis);

	/***
	 * 报货汇总查询
	 * @param arrivalmMis
	 * @return
	 */
	public List<Map<String, Object>> findArrivaldListSum(Arrivalm arrivalmMis);


}
