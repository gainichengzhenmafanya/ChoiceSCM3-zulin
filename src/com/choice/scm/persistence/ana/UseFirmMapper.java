package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

public interface UseFirmMapper {

	/**
	 * 耗用分店查询
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findUseFirm(Map<String,Object> condition);
	
}
