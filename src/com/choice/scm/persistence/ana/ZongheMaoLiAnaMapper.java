package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.ana.CostProfit;


public interface ZongheMaoLiAnaMapper {


	/**
	 * 月综合毛利分析
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findFirmMonthMaoLiAna(CostProfit costProfit);
	
	/**
	 * 查询月综合毛利分析客流员工数
	 * @param costProfit
	 * @return
	 */
	public List<Map<String,Object>> findMonthKeLiuEmpNo(CostProfit costProfit);
	
	/**
	 * 月综合毛利分析合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findFirmMonthMaoLiAnaSum(CostProfit costProfit);
	
	/**
	 * 周综合毛利分析
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findFirmWeekMaoLiAna(CostProfit costProfit);
	
	/**
	 * 查询周综合毛利分析客流员工数
	 * @param costProfit
	 * @return
	 */
	public List<Map<String,Object>> findWeekKeLiuEmpNo(CostProfit costProfit);
	
	/**
	 * 周综合毛利分析合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findFirmWeekMaoLiAnaSum(CostProfit costProfit);
}
