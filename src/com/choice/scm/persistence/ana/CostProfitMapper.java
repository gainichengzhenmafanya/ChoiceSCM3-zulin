package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

public interface CostProfitMapper {

	/**
	 * 查询分店毛利对比信息
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findFirmProfitCmp(Map<String,Object> condition);
	
}
