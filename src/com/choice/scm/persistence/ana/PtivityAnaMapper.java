package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.ana.PtivityAna;


public interface PtivityAnaMapper {

	/**
	 * 应产率分析
	 * @param costProfit
	 * @return
	 */
	public List<Map<String,Object>> findPtivityAna(PtivityAna ptivityAna);

	/***
	 * 计算万元用量
	 * @param ptivity
	 */
	public void callScmWzWanyuanyongliangTemp(PtivityAna ptivity);
	
	/***
	 * 物资万元用量分析合计
	 * @param ptivity
	 * @return
	 */
	public List<Map<String, Object>> findWzWanyuanyongliangFenxiSum(PtivityAna ptivity);
	
	/***
	 * 物资万元用量分析
	 * @param ptivity
	 * @return
	 */
	public List<Map<String, Object>> findWzWanyuanyongliangFenxi(PtivityAna ptivity);
	
	/***
	 * 计算万元用量
	 * @param ptivity
	 */
	public void callScmWzYongliangFenchaFenxiTemp(PtivityAna ptivity);
	
	/***
	 * 物资万元用量分析合计
	 * @param ptivity
	 * @return
	 */
	public List<Map<String, Object>> findWzYongliangFenchaFenxiSum(PtivityAna ptivity);
	
	/***
	 * 物资万元用量分析
	 * @param ptivity
	 * @return
	 */
	public List<Map<String, Object>> findWzYongliangFenchaFenxi(PtivityAna ptivity);

}
