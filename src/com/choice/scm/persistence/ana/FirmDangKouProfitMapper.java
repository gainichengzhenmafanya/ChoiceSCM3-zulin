package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.ana.CostProfit;

public interface FirmDangKouProfitMapper {

	/**
	 * 查询分店档口毛利信息
	 */
	public List<Map<String,Object>> findFirmDangKouProfit(CostProfit costProfit);
	/**
	 * 查询分店档口毛利合计
	 */
	public List<Map<String,Object>> findFirmDangKouProfitSum(CostProfit costProfit);
	
}
