package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

public interface UseCostAnaMapper {

	/**
	 * 耗用成本分析合计
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findUseCostAnaSum(Map<String,Object> condition);
	
	/**
	 * 耗用分析明细
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findUseCostAnaDetail(Map<String,Object> condition);
	
}
