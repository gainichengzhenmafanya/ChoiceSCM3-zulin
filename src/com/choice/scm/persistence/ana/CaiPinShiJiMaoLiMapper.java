package com.choice.scm.persistence.ana;

import java.util.List;
import java.util.Map;

public interface CaiPinShiJiMaoLiMapper {

	/**
	 * 菜品实际毛利查询
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findCaiPinShiJiMaoLi(Map<String,Object> condition);
	
}
