package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.ChkstoDemoFirm;
import com.choice.scm.domain.ChkstoDemom;
import com.choice.scm.domain.ChkstoDemom_x;

/**
 * 报货单单据模板主表
 * @author 孙胜彬
 */
public interface ChkstoDemomMapper {
	
	/**
	 * 查询报货单主表信息
	 * @param chkstoDemom
	 * @return
	 */
	public List<ChkstoDemom> listChkstoDemom(ChkstoDemom chkstoDemom);
	/**
	 * 新增报货单主表
	 * @param chkstoDemom
	 */
	public void saveChkstoDemom(ChkstoDemom chkstoDemom);
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 */
	public void updateChkstoDemom(ChkstoDemom chkstoDemom);
	/**
	 * 删除报货单主表
	 * @param chkstoDemom
	 */
	public void deleteChkstoDemom(List<String> idList);
	/**
	 * 查询适用分店
	 * @param chkstoDemoFirm
	 * @return
	 */
	public List<ChkstoDemoFirm> listChkstoDemoFirm(ChkstoDemoFirm chkstoDemoFirm);
	/**
	 * 添加适用分店
	 * @param chkstoDemoFirm
	 */
	public void saveChkstoDemoFirm(ChkstoDemoFirm chkstoDemoFirm);
	/**
	 * 删除适用分店
	 * @param idList
	 */
	public void deleteChkstoDemoFirm(List<String> idList);
	
	/**
	 * 查询报货单主表信息--虚拟物料
	 * @param chkstoDemom_x
	 * @return
	 */
	public List<ChkstoDemom_x> listChkstoDemom_x(ChkstoDemom_x chkstoDemom_x);
	/**
	 * 新增报货单主表--虚拟物料
	 * @param chkstoDemom_x
	 */
	public void saveChkstoDemom_x(ChkstoDemom_x chkstoDemom_x);
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 */
	public void updateChkstoDemom_x(ChkstoDemom_x chkstoDemom_x);
	/**
	 * 删除报货单主表
	 * @param chkstoDemom
	 */
	public void deleteChkstoDemom_x(List<String> idList);
	/**
	 * 删除适用分店
	 * @param idList
	 */
	public void deleteChkstoDemoFirm_x(List<String> idList);
	
	
}
