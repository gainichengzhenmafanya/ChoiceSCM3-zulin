package com.choice.scm.persistence;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.scm.domain.ItemUse;
import com.choice.scm.domain.PosItemPlan;
import com.choice.scm.domain.PosSalePlan;

public interface ForecastMapper {

	/**
	 *  查找所有的菜品点击率
	 * @return
	 */
	public List<ItemUse> findAllItemUse(ItemUse itemUse);
	
	/**
	 * 获取点击率表达方式（单数，营业额，人数）
	 * @param acct
	 * @return
	 */
	public String getPlanTyp(@Param("acct")String acct);
	
	/**
	 * 把这道菜的点击率统计信息添加到表
	 * @param itemUse
	 * @return
	 */
	public void insertItemUse(ItemUse itemUse);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAll(ItemUse itemUse);
	
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @return
	 */
	public void update(ItemUse itemUse);
	
	/**
	 *  查找营业预估
	 * @return
	 */
	public List<PosSalePlan> findPosSalePlan(PosSalePlan posSalePlan);
	
	/**
	 * 删除某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void deletePosSalePlan(PosSalePlan posSalePlan);
	
	/**
	 * 添加某日期营业预估
	 * @param posSalePlan
	 * @return
	 */
	public void insertPosSalePlan(PosSalePlan posSalePlan);
	
	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @return
	 */
	public void updateSalePlan(PosSalePlan posSalePlan);
	
	/**
	 *  查找预估菜品销售计划
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlan(PosItemPlan posItemPlan);
	
	/**
	 *  查找菜品销售计划天数
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlanForDays(PosItemPlan posItemPlan);
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posItemPlan
	 * @return
	 */
	public void updateItemPlan(PosItemPlan posItemPlan);
	
	/**
	 * 删除所有菜品点击率
	 * @param 
	 * @return
	 */
	public void deleteAllPlan(PosItemPlan posItemPlan);
	
	/**
	 * 添加菜品营业销售计划
	 * @param map
	 * @return
	 */
	public void insertPosItemPlan(PosItemPlan posItemPlan);
	
	/**
	 *  查找菜品营业销售计划
	 * @return
	 */
	public List<PosSalePlan> findPosSalePlan1(PosSalePlan posSalePlan);
	
	/**
	 *  查找一段时间内的预估菜品销售计划
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlan1(PosItemPlan posItemPlan);
	
	/**
	 *  查找某日某班次的菜品预估信息
	 * @return
	 */
	public List<PosItemPlan> findPosItemPlan2(PosItemPlan posItemPlan);
}
