package com.choice.scm.persistence;

import org.apache.ibatis.annotations.Param;

public interface ExecSql {

	public int execSql(@Param(value="sql") String sql);
	
	/**
	 * 取得序列最大值
	 * @param seqName
	 * @return
	 */
	public int getMaxsequences(@Param(value="seqName") String seqName);
}
