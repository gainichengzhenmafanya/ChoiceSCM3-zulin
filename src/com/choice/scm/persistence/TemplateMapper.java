package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Template;

/***
 * 模版使用mapper
 * @author ghc
 *
 */
public interface TemplateMapper {


	/***
	 * 根据用户code查询模版
	 * @param template
	 * @return
	 */
	public List<Template> getTemplate(Template template);

	/***
	 * 新增
	 * @param template
	 */
	public void insertTemplate(Template template);

	/**
	 * 修改
	 * @param template
	 */
	public void updateTemplate(Template template);
}
