package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.OfferCargo;
import com.choice.scm.domain.OfferCargoEntry;
import com.choice.scm.domain.OfferCargoWhEntry;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;

public interface EasMapper {
	
	/**
	 * 查询所有物资
	 * @param positn
	 * @return
	 */
	public List<Supply> findAllSupply();
	/**
	 * 修改成  同步的 金蝶物资编码信息
	 * @param supply
	 */
	public void updateSupply(Supply supply);
	/**
	 * 查询所有物资大类
	 * @param positn
	 * @return
	 */
	public List<GrpTyp> findAllGrpTyp();
	
	/**
	 * 查询所有物资中类
	 * @param positn
	 * @return
	 */
	public List<Grp> findAllGrp();
	
	/**
	 * 查询所有物资小类
	 * @param positn
	 * @return
	 */
	public List<Typ> findAllTyp();
	
	/**
	 * 查询所有物资报货类别
	 * @param positn
	 * @return
	 */
	public List<CodeDes> findAllBhTyp();
	public void saveCodeDes(CodeDes codeDes);
	public void deleteAllGrpTyp();
	public void deleteAllGrp();
	public void deleteAllTyp();
	public void deleteAllBhTyp(CodeDes codeDes);
	public void saveOfferCargo(OfferCargo offerCargo);
	public void saveOfferCargoWhEntry(OfferCargoWhEntry offerCargoWhEntry);
	public void saveOfferCargoEntry(OfferCargoEntry offerCargoEntry);
	public OfferCargo findOfferCargo(Chkstom chkstom);
	public OfferCargoWhEntry findOfferCargoWhEntry(Chkstom chkstom);
	public Chkstom getMaterialGroupID(Chkstom chkstom);
	public OfferCargoEntry getMaterial(Supply supply);
}
