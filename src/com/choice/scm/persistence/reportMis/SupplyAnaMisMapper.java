package com.choice.scm.persistence.reportMis;

import java.util.List;
import java.util.Map;

public interface SupplyAnaMisMapper {

	/**
	 * 查询库龄分组信息
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findInventoryAgingSum(Map<String,Object> conditions);
		
	/**
	 * 查询库龄分组物资信息
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findInventoryAgingDetail(Map<String,Object> conditions);
	
	/**
	 * 查询库龄合计
	 * @param supplyAcct
	 * @return
	 */
	public List<Map<String,Object>> findCalForInventoryAging(Map<String,Object> conditions);
	
	/**
	 * 查询价格预警与比重报表内容
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findPriceWarning(Map<String, Object> condition);
	
	/**
	 * 查询价格预警与比重报表内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForPriceWarning(Map<String, Object> condition);

	/**
	 * 采购价格异动分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findPurchasePriceChange(Map<String,Object> condition);

	/**
	 * 入库成本分析  的           列表  列表总计    明细   明细总计
	 * @param condition
	 * @return
	 */
	public List<Map<String, Object>> findReceiptCostAnalysisSum(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisSum_total(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisDetail(Map<String, Object> condition);
	public List<Map<String, Object>> findReceiptCostAnalysisDetail_total(Map<String, Object> condition);
	
	/**
	 * 申购到货分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findApplyArriveAmount(Map<String,Object> condition);
	
	/**
	 * 查询库存量预估使用内容总计
	 * @param condition
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForInventoryEstimates(Map<String, Object> condition);
	
	/**
	 * 分店进货价格分析
	 * @param condition
	 * @return
	 */
	public List<Map<String,Object>> findFirmStockPrice(Map<String,Object> condition);

	/**
	 * 物资库龄明细
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findSupplyKuLingDetail(Map<String,Object> conditions);
	
	/**
	 * 物资库龄明细合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplyKuLingDetailSum(Map<String,Object> content);
	
	/**
	 * 库存周转率汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLvSum(Map<String,Object> conditions);
	
	/**
	 * 库存周转率
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLv(Map<String,Object> conditions);
	
	/**
	 * 库存周转率详情
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findKuCunZhouZhuanLvDetail(Map<String,Object> condition);
	
	/**
	 * 分店菜品利润合计
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findCalForFirmFoodProfit(Map<String, Object> map);
}
