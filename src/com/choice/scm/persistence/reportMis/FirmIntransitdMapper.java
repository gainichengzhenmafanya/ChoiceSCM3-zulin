package com.choice.scm.persistence.reportMis;

import java.util.List;

import com.choice.scm.domain.FirmIntransitd;

public interface FirmIntransitdMapper {

	/**
	 * 查询在途清单
	 * @param firmIntransitd
	 */
	public List<FirmIntransitd> queryFirmIntransitd(FirmIntransitd firmIntransitd);
}
