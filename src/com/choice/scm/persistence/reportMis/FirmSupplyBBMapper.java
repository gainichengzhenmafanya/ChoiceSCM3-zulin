package com.choice.scm.persistence.reportMis;

import java.util.List;

import com.choice.scm.domain.FirmSupply;

public interface FirmSupplyBBMapper {

	/**
	 * 查询物料盘存表
	 * @param firmSupply
	 */
	public List<FirmSupply> queryFirmSupply(FirmSupply firmSupply);
}
