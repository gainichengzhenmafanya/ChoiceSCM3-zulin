package com.choice.scm.persistence.reportMis;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.SupplyAcct;

public interface SupplyAcctMisMapper {

	/**
	 * 对比上期
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQComp(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSumQuery(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比月最大耗用）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQByMonthMax(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（当前日期）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutSQCur(Map<String,Object> conditions);
	
	/**
	 * 出库汇总查询（对比日期）
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findChkoutSQPrev(Map<String,Object> conditions);
	
	/**
	 * 出库单据查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkout(Map<String,Object> conditions);
	
	/**
	 * 出库类别汇总
	 * @param condition
	 * @return
	 */
//	public List<Map<String,Object>> findAllChkoutmSum(Map<String,Object> condition);
	
	/**
	 * 出库类别汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutCategorySum(Map<String,Object> conditions);
	
	/**
	 * 出库类别汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkoutCategorySum(Map<String,Object> conditions);
	
	/**
	 *  出库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmTypSum(Map<String, Object> map);
	/**
	 *  出库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmSumQuery(Map<String, Object> map);
	/**
	 *  出库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkoutmDetailQuery(Map<String, Object> map);
	
	/**
	 * 出库明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDetailQuery(Map<String,Object> conditions);
	
	/**
	 * 出库明细查询（按售价计算）
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkoutDQBySale(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商付款情况求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverPayment(Map<String,Object> conditions);
	
	/**
	 * 供应商进货汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverStockSum(Map<String,Object> conditions);
	/**
	 * 供应商进货汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverStockSum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findDeliverCategorySum(Map<String,Object> conditions);
	
	/**
	 * 供应商类别汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverCateSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 类别供应商汇总求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForCategoryDeliverSum1(Map<String,Object> conditions);
	
	/**
	 * 入库汇总查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 * 入库汇总合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinSumQuery(Map<String,Object> conditions);
	
	/**
	 *  入库类别汇总
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmGrp(Map<String, Object> map);
	
	/**
	 * 入库类别汇总求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findChkinmCalSum(Map<String, Object> map);
	
	/**
	 * 入库明细查询
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailS(Map<String,Object> conditions);
	
	/**
	 * 查询统计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCal(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总
	 * @param conditions
	 * @return
	 */
	public List<SupplyAcct> findChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 * 入库明细汇总 合计
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForChkinDetailSum(Map<String,Object> conditions);
	
	/**
	 *  入库综合查询（类别汇总）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmTypSum(Map<String, Object> map);
	
	/**
	 *  入库综合查询（汇总查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSumQuery(Map<String, Object> map);
	
	/**
	 *  入库综合查询（明细查询）
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmDetailQuery(Map<String, Object> map);
	
	/**
	 * 入库类别综合查询求和 
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findAllChkinmSum(Map<String, Object> map);
	
	/**
	 * 物资类别进出表
	 * @param content
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,Object>> findSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资类别进出表求和
	 * @author ZGL
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyTypInOut(Map<String,Object> content);
	
	/**
	 * 物资明细进出表查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细进出表总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyInOutInfo(Map<String,Object> content);
	
	/**
	 * 物资明细账
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资明细账合计
	 * @param conditions
	 * @return
	 * @author ZGL
	 */
	public List<Map<String, Object>> findCalForSupplyDetailsInfo(Map<String, Object> conditions);
	
	/**
	 * 物资余额列表查询
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findSupplyBalance(Map<String,Object> content);
	/**
	 * 物资余额列总计查询
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplyBalance(Map<String,Object> content);
	
	/**
	 * 供应商汇总列表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 供应商汇总列表求和
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findCalForDeliverSum(Map<String,Object> conditions);
	
	/**
	 * 进货单据汇总详细信息列表
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findStackBillSum(Map<String,Object> content);
	/**
	 * 进货单据汇总列表求和
	 * @param content
	 * @return
	 * @author ZGL_ZANG
	 */
	public List<Map<String,Object>> findCalForStockBillSum(Map<String,Object> content);
	
	/**
	 * 物资综合进出表
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findSupplySumInOut(Map<String,Object> content);
	/**
	 * 物资综合进出表求和
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForSupplySumInOut(Map<String,Object> content);
	
	/**
	 * 物资仓库进出表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findGoodsStoreInout(Map<String,Object> conditions);
	
	/**
	 * 物资仓库进出表合计查询 +
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findCalForGoodsStoreInout(Map<String,Object> content);
}
