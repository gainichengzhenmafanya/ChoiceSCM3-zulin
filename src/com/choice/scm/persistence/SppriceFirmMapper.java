package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.SppriceSale;

public interface SppriceFirmMapper {

	/**
	 * 门店报价模板，报价复制
	 * @param deliver
	 * @return
	 */
	public void saveByCopySpprice(SppriceDemo sppriceDemo);	
	
	/**
	 * 门店售价模板，售价复制
	 * @param deliver
	 * @return
	 */
	public void saveByCopySppriceSale(SppriceDemo sppriceDemo);
	
	/**
	 * 查询门店售价
	 * @param sale
	 * @return
	 */
	public List<SppriceSale> findFirmSalePrice(SppriceSale sale);
	
	/**
	 * 查询门店报价
	 * @param spprice
	 * @return
	 */
	public List<Spprice> findFirmPrice(Spprice spprice);
}
