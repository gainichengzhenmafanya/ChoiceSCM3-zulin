package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

public interface FirmMisReportMapper {

	/**
	 * 部门物资进出表
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeptSupplyInOut(Map<String,Object> conditions);
	
	/**
	 * 部门物资进出表合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findDeptSupplyInOutSum(Map<String,Object> content);
	
	/**
	 * 部门成本差异
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDeptCostDiff(Map<String,Object> conditions);
	
	/**
	 * 部门成本差异合计
	 * @param content
	 * @return
	 */
	public List<Map<String,Object>> findDeptCostDiffSum(Map<String,Object> content);
	
	/**
	 * 分店菜品利润
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findFirmFoodProfit(Map<String, Object> map);
	
	/**
	 * 分店菜品利润合计
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findCalForFirmFoodProfit(Map<String, Object> map);
	
	/**
	 * 根据物资编码查询菜品成本明细--主表
	 * @param sp_code
	 * @return
	 */
	public List<Map<String,Object>> findCostdtlm(Map<String,Object> map);
	
	/**
	 * 根据物资编码查询菜品成本明细--子表
	 * @param sp_code
	 * @return
	 */
	public List<Map<String,Object>> findCostdtl(Map<String,Object> map);
	
	/**
	 * 菜品成本组成物资明细
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findCostDetail(Map<String,Object> map);
	
	/**
	 * 菜品销售成本利润走势分析
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findSaleCostProfitTrends(Map<String,Object> map);
	
	/**
	 * 菜品成本区间分析
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findCostIntervalList(Map<String,Object> map);
	
	/**
	 * 菜品毛利率区间分析
	 * @param map
	 * @return
	 */
	public List<Map<String,Object>> findMaolilvIntervalList(Map<String,Object> map);
}
