package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.AcctLineFirm;
import com.choice.scm.domain.LinesFirm;

public interface LinesFirmMapper {

	 
	/**
	 * 查询配送线路 list
	 */
	public List<LinesFirm> findLinesFirm(LinesFirm linesFirm);
	/**
	 * 根据配送ID查询该配送线下的所有分店
	 */
	public List<AcctLineFirm> findFirmByLinesFirmId(AcctLineFirm acctlinefirm);
	/**
	 * 根据分店ID查询对应路线
	 */
	public List<AcctLineFirm> findFirmByLinesFirmIdOne(String firmCode);
	/**
	 * 添加 左边 配送路线
	 */
	public void saveLiensFirm(LinesFirm linesFirm);
	/**
	 * 刪除 左边 配送路线
	 */
	public void deleteLiensFirm(LinesFirm linesFirm);
	/**
	 * 刪除 左边 配送路线下所有分店
	 */
	public void deleteAllAcctLiensFirm(LinesFirm linesFirm);
	/**
	 * 刪除 右边  配送路线下指定分店
	 */
	public void deleteAcctLiensFirm(Map<String,Object> params);
	/**
	 * 添加 右边  配送路线下分店
	 */
	public void saveAcctLineFirm(AcctLineFirm acctLineFirm);
	/**
	 * 修改配送路线 左边
	 */
	public void updateLineFirm(LinesFirm linesFirm);
	/**
	 * 修改到店时间
	 */
	public void updateTimto(AcctLineFirm acctLineFirm);
 
}
