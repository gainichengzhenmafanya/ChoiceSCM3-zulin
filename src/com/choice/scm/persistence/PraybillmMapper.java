package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;

import com.choice.scm.domain.Praybillm;

public interface PraybillmMapper {
	
	/**
	 * 查询当前最大单号
	 * @return
	 */
	public int getMaxPraybillmno();
	
	/**
	 * 按单号查询
	 * @param praybillm
	 * @return
	 */
	public Praybillm findByPraybillmNo(Praybillm praybillm);
	
	/**
	 * 添加请购单主表
	 * @param praybillm
	 */
	public void saveNewPraybillm(Praybillm praybillm);
	
	/**
	 * 关键字查询
	 * @param map
	 * @return
	 */
	public List<Praybillm> findByKey(HashMap<Object,Object> map);
	
	/**
	 * 更新请购单
	 * @param chkstomMap
	 */
	public void updatePraybillm(Praybillm praybillm);
	
	/***
	 * 删除请购单
	 * @param p
	 */
	public void deletePraybill(Praybillm p);
	
	/**
	 * 审核
	 */
	public void checkPraybill(Praybillm p);
	
	/**
	 * 检查是金额是否充足
	 */
	public double checkSppriceSale(String frim);

	
}
