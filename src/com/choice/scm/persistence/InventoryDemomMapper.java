package com.choice.scm.persistence;

import java.util.List;

import com.choice.scm.domain.InventoryDemoFirm;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;

/**
 * 盘点模板
 * @author 孙胜彬
 */
public interface InventoryDemomMapper {
	
	/**
	 * 盘点模板子表查询
	 * @param inventoryDemod
	 * @return
	 */
	public List<InventoryDemod> listInventoryDemod(InventoryDemod inventoryDemod);
	
	/**
	 * 盘点模板主表查询
	 * @param inventoryDemom
	 * @return
	 */
	public List<InventoryDemom> listInventoryDemom(InventoryDemom inventoryDemom);
	
	/**
	 * 根据分店查询使用的所有模板
	 * @param inventoryDemom
	 * @return
	 */
	public List<InventoryDemom> listInvenDemomByFirm(InventoryDemom inventoryDemom);
	
	/**
	 * 盘点模板适用分店查询
	 * @param inventoryDemoFirm
	 * @return
	 */
	public List<InventoryDemoFirm> listInventoryDemoFirm(InventoryDemoFirm inventoryDemoFirm);
	
	/**
	 * 新增盘点模板子表
	 * @param inventoryDemod
	 */
	public void saveInventoryDemod(InventoryDemod inventoryDemod);
	
	/**
	 * 新增盘点模板主表
	 * @param inventoryDemom
	 */
	public void saveInventoryDemom(InventoryDemom inventoryDemom);
	
	/**
	 * 新增盘点模板适用分店
	 * @param inventoryDemoFirm
	 */
	public void saveInventoryDemoFirm(InventoryDemoFirm inventoryDemoFirm);
	
	/**
	 * 修改盘点模板子表
	 * @param inventoryDemod
	 */
	public void updateInventoryDemod(InventoryDemod inventoryDemod);
	
	/**
	 * 修改盘点模板主表
	 * @param inventoryDemom
	 */
	public void updateInventoryDemom(InventoryDemom inventoryDemom);
	
	/**
	 * 删除盘点模板子表
	 * @param inventoryDemod
	 */
	public void deleteInventoryDemod(List<String> idList);
	
	/**
	 * 删除盘点模板主表
	 * @param inventoryDemom
	 */
	public void deleteInventoryDemom(List<String> idList);
	
	/**
	 * 查找所有的盘点模板标题
	 * @return
	 */
	public List<InventoryDemom> findAllTitle(InventoryDemom inventoryDemom);

	/**
	 * 删除盘点模板适用分店
	 * @param inventoryDemom
	 */
	public void deleteInventoryDemoFirm(List<String> idList);

	/***
	 * 得到主键
	 * @return
	 */
	public Integer findMaxNo();
}
