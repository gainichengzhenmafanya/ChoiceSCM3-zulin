package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkoutd;

public interface ChkoutdMapper {

	/**
	 * 分店MIS验收查询
	 * @param map
	 * @return
	 */
	public List<Chkoutd> findChkoutFromMis(Map<String,Object> map);
	/**
	 * 添加申购单详单
	 * @param chkoutd
	 */
	public void saveChkoutd(Map<String,Object> chkoutd);
	
	/**
	 * 删除申购单详单信息
	 * @param chkoutd
	 */
	public void deleteChkoutd(Map<String,String> chkout);
	
	/**
	 * 更新申购单详单信息
	 * @param chkoutd
	 */
	public void updateChkoutd(Chkoutd chkoutd);
	
	/**
	 * 查询申购单详单
	 * @return
	 */
	public List<Chkoutd> findChkoutd(Chkoutd chkoutd);

	/**
	 * 添加申购单详单
	 * @param chkoutd
	 */
	public Double selectSupplyPrice(Map<String,Object> chkoutd);
}
