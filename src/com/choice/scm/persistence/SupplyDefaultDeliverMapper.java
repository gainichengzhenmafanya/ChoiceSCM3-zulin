package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.SpCodeMod;

public interface SupplyDefaultDeliverMapper {

	/**
	 * 查询物资信息和默认仓位信息 
	 */
	public List<Map<String,Object>> findSupplyDefaultPositn(Map<String,Object> condition);
	
	/**
	 * 新增物资默认供应商
	 */
	public void saveSupplyDefaultPositn(Map<String,Object> condition);
	
	/**
	 * 根据配送片区物资编码查询物资默认供应商信息
	 * @return
	 */
	public List<SpCodeMod> findDefaultPositn(Map<String,Object> condition);
	
	/**
	 * 修改默认供应商
	 */
	public void updateSupplyDefaultPositn(Map<String,Object> condition);
	
	/**
	 * 修改出品加工间
	 */
	public void updateSupplyWorkShop(Map<String,Object> condition);
	/**
	 * 清楚默认仓位
	 * @param condition
	 */
	public void deletePositn(Map<String,Object> condition);
	
}
