package com.choice.scm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.domain.Tax;
import com.choice.scm.domain.Typ;
import com.choice.scm.domain.Typoth;

public interface SupplyMapper {

	/**
	 * 通过编号查询物资编码
	 * @param supply
	 * @return
	 */
	public Supply findSupplyById(Supply supply);
	public Supply findSupplyById_x(Supply supply);
	public Supply findSupplyById_x1(Supply supply);
	/**
	 * 多条添加虚拟物资到模板中
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public Supply findSupplyById_x2(Supply supply);
	/**
	 * 通过编码查询物资默认供应商
	 */
	public Supply findDeliverByCode(Supply supply);
	/**
	 * 通过条件查询物资编码列表
	 * @param supply
	 * @return
	 */
	public List<Supply> findAllSupply(Supply supply);
	public List<Supply> findAllSupplySXS(Supply supply);
	public List<Supply> findAllSupplySXS_edit(Costbom costbom);
	public List<Supply> findAllSupply_x(Supply supply);
	public List<Supply> findSupplyBySpCodeX(Supply supply);
	
	/**
	 * 查询所有的物资编码信息，并标记当前账户已关联的物资编码，用于界面显示
	 * @param accountId
	 * @return
	 */
	public List<Supply> findSupplyList(String accountId);
	
	/**
	 * Ajax  根据物资缩写，或者 物资编码 ，或者 物资名称       查询前n条  物资记录
	 * @param supply
	 * @return
	 */
	public List<Supply> findSupplyListTopN(Supply supply);
	public List<Supply> findSupplyListTopN_x(Supply supply);
	
	/**
	 * 根据输入的条件
	 * 查询供应商供应物资前十条记录
	 * @param supply
	 * @return
	 */
	public List<Supply> findGysSpListTopN(Supply supply);
	
	/**
	 * Ajax  根据条件查询售价
	 * @param supply
	 * @return
	 */
	public SppriceSale findSprice(SppriceSale spprice);
	
	/**
	 * Ajax  根据条件查询报价
	 * @param supply
	 * @return
	 */
	public Spprice findBprice(Spprice spprice);
	
	/**
	 * 保存物资编码信息
	 * @param supply
	 */
	public void saveSupply(Supply supply);
	/**
	 * 保存物资编码信息
	 * @param supply
	 */
	public void saveSupply_new(Supply supply);
	/**
	 * 修改物资编码信息
	 * @param supply
	 */
	public void updateSupply(Supply supply);
	/**
	 * 修改物资编码信息
	 * @param supply
	 */
	public void updateSupply_new(Supply supply);
	/**
	 * 修改物资编码信息
	 * @param supply
	 */
	public void updatePositnSpcode(Supply supply);
	/**
	 * 修改物资编码大类信息
	 * @param grpTyp
	 */
	public void updateGrpTyp(GrpTyp grpTyp);
	/**
	 * 修改物资编码中类信息
	 * @param grp
	 */
	public void updateGrp(Grp grp);
	/**
	 * 修改物资编码小类信息
	 * @param typ
	 */
	public void updateTyp(Typ typ);
	/**
	 * 删除物资编码信息  先查询是否有Y，有的不让删
	 * @param listId
	 */
	public List<Supply> findAllSupplySfyyByIds(List<String> listId);
	public void deleteSupply(List<String> listId);
	public void deleteAllSupply();
	/**
	 * 根据所属小类删除物资编码信息
	 * @param listCode
	 */
	public void deleteByTyp(List<String> listCode);
	/**
	 * 根据所属中类删除物资编码信息
	 * @param listCode
	 */
	public void deleteByGrp(List<String> listCode);
	/**
	 * 根据所属大类删除物资编码信息
	 * @param listCode
	 */
	public void deleteByGrpTyp(List<String> listCode);
	/**
	 * 根据传进的小类代码查询该类下最大的编码并+1
	 * @param code
	 * @return
	 */
	public String getMaxSpcode(@Param("CODE")String code);
	/**
	 * 无妨斋
	 */
	public String getMaxSpcodeWFZ(@Param("CODE")String code);

	/**
	 * 查询所有参考类别
	 * @return
	 */
	public List<Typoth> findTypoth(@Param("code")String code,@Param("des")String des);

	public List<Tax> findTax(@Param("id")Integer id);
	
	public Integer SupplyCodeCount(Supply supply);
	
	/**
	 *获取所有的虚拟物料 信息  ====用于物资编码，虚拟物料查询
	 *@author wangchao
	 */
	public List<Supply> selectSupply_x(Supply supply);
	
	/**
	 * 跳转到虚拟物料修改界面
	 * @param supply
	 * @return
	 */
	public Supply toUpdateSupply_x(Supply supply);
	
	/**
	 * 虚拟物料修改
	 * @param supply
	 * @throws CRUDException
	 */
	public void saveByUpdateSupply_x(Supply supply);
	
	/**
	 *获取所有的虚拟物料 信息 
	 */
	public List<Supply> selectAllSupply_x(Supply supply);
	/**
	 *获取所有的实际物料 信息 
	 */
	public List<Supply> selectAllSupply(Supply supply);
	/**
	 * 获取所有虚拟物料信息（导出）
	 * @param contions
	 * @return
	 */
	public List<Supply> selectAllSupply_x_export(HashMap<String, Object> contions);
	/***
	 * 获取所有物资（导出）
	 * @param contions
	 * @return
	 */
	public List<Supply> findAllSupply_export(HashMap<String, Object> contions);
	
	/**
	 * 根据
	 * @param sp_code_x
	 * @return
	 */
	public List<Supply> checkUnit(Supply supply);
	
	/**
	 * 检查虚拟物资编码和名称是否重复
	 * @param sp_code_x
	 * @return
	 */
	public int checkSupply_x(Supply supply);
	
	/**
	 * 根据选中的虚拟物资  查询实际的物资
	 * @param supply
	 * @return
	 */
	public List<Supply> downSupply(Supply supply);
	
	/***
	 * 物资直接加入分店 wjf
	 * @param map
	 */
	public void joinPositn(Map<String,String> map);
	
	/***
	 * 查询物资多单位
	 * @param map
	 */
	public List<SupplyUnit> findAllSupplyUnit(SupplyUnit supplyUnit);
	
	/**
	 * 保存物资多单位信息
	 * @param supply
	 */
	public void saveSupplyUnit(SupplyUnit supplyUnit);
	/**
	 * 修改物资多单位信息
	 * @param supply
	 */
	public void updateSupplyUnit(SupplyUnit supplyUnit);
	
	/**
	 * 通过id查询物资多单位
	 * @param supplyUnit
	 * @return
	 */
	public SupplyUnit findSupplyUnitById(SupplyUnit supplyUnit);
	
	/**
	 * 删除物资多单位
	 * @param listPositn
	 */
	public void deleteByIds(@Param("sp_code")String sp_code);
	
	/**
	 * 新增物资参考类别wangjie 2014年12月22日 15:05:24
	 * @param typoth
	 */
	public void insertTypoth(Map<String, Typoth> typoth);
	
	/**
	 * 删除物资参考类别
	 * @param typoth
	 */
	public void deleteTypoth(Map<String, Typoth> typoth);
	
	/**
	 * 修改物资参考类别
	 */
	public void updateTypoth(Map<String, Typoth> typoth);
	
	/**
	 * 批量修改物资参考类别
	 * @param modelMap
	 */
	public void batchTypoth(Map<String,Object> modelMap);
	
	/***
	 * 查询出品加工间物资前10条
	 * @param supply
	 * @return
	 */
	public List<Supply> findSupplyExTop10(Supply supply);
	/**
	 * 删除物资编码信息  先查询是否有Y，有的不让删
	 * @param listId
	 */
	public List<Supply> findSupplyBySpcode(List<String> listId);
	
	/***
	 * 查询指定物资的配送片区默认仓位
	 * @param sp_code
	 * @param mod
	 * @return
	 */
	public Positn findSpcodeMod(@Param("sp_code")String sp_code, @Param("firm")String firm);
	
	/***
	 * 查询此物资配送单位是不是已经引用
	 * @param su
	 * @return
	 */
	public int findSupplyUnitByPositnspcode(SupplyUnit su);
	
	/***
	 * 更新仓位物资属性转换率和最小申购量
	 * @param supplyUnit
	 */
	public void updatePsDisunit(SupplyUnit supplyUnit);
	
	/***
	 * 根据供应商可供物资查询税率
	 * @param s
	 * @return
	 */
	public Supply findTaxByDeliverSpcode(Supply s);
}
