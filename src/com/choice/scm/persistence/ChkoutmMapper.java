package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;

public interface ChkoutmMapper {

	/**
	 * 添加申购单
	 * @param chkoutm
	 */
	public void saveChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 查询申购单信息（时间段、出库仓位、领用仓位、单号、凭证号）
	 * @param chkoutm
	 */
	public List<Chkoutm> findChkoutm(Map<String,Object> map);
	
	/**
	 * 删除申购单信息
	 * @param chkoutm
	 */
	public void deleteChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 通过id获取申购单信息
	 * @param chkoutm
	 * @return
	 */
	public Chkoutm findChkoutmById(Chkoutm chkoutm);
	
	/**
	 * 更新申购单信息
	 * @param chkoutm
	 */
	public void updateChkoutm(Map<String,Object> chkoutm);
	
	/**
	 * 获取下一个单号
	 * @return
	 */
	public int findNextNo();
	
	/**
	 * 审核出库单
	 * @param chkoutm
	 */
	public void AuditChkout(Map<String,Object> chkoutm);
	
	/**
	 * 获取冲销数据  非门店
	 * @param chkout
	 */
	public List<Spbatch> findChkoutByCx(Spbatch spbatch);
	/**
	 * 获取冲销数据  门店
	 * @param chkout
	 */
	public List<Spbatch> findChkoutByCx_X(Spbatch spbatch);
	
	/**
	 * 审核出库单后更新总金额（冲销时总金额不正确）
	 * @param chkoutm
	 */
	public void updateChkoutAmount(Chkoutm chkoutm);

	/***
	 * 更新chkks状态  配送差异处理用 2014.12.20wjf
	 * @param supplyAcct
	 */
	public void updateChkks(SupplyAcct supplyAcct);

	/***
	 * 查询批次出库
	 * @param spbatch
	 * @return
	 */
	public List<Spbatch> findChkoutByPc(Spbatch spbatch);
	
	
	/*
	 * 获取未核减账单
	 */
	public List<Map<String, Object>> findFolioByNoChkoutm(Map<String, Object> folio);
	
	/*
	 * 获取未核减账单明细
	 */
	public List<Map<String, Object>> findOrdrsByNoChkoutm(Map<String, Object> folio);
	
	/**
	 * 更新账单表是否已核减状态
	 * @param chkoutm
	 */
	public void updateFolioIsChked(String vbcode);
	
	/*
	 * 根据账单号从出库表中查询是否已存在此账单
	 */
	public List<Map<String, Object>> findChkoutmByMemo(String vbcode);
}
