package com.choice.scm.persistence;

import java.util.List;
import java.util.Map;

import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Holiday;
import com.choice.scm.domain.Main;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;

public interface TeShuCaoZuoMapper {

	/**
	 * 检查系统是否正在盘点
	 * @param accountId
	 * @return
	 */
	public Map<String,Object> findChktag(Map<String,Object> map);
	
	/**
	 * 检查系统是否有未审核入库单或者直发单
	 */
	public List<String> findChkchkinm(Map<String,Object> map);
	/**
	 * 检查系统是否有未审核出库单
	 */
	public List<String> findChkchkoutm(Map<String,Object> map);
	
	/**
	 * 查找当前会计年
	 * @return
	 */
	public Main findMax();
	
	/**
	 * 查找月末结转的月份
	 * @return
	 */
	public Main findMain();
	
	/**
	 * 年结转
	 * @return
	 */
	public void insertMain(Main main);
	public String closeYear(Map<String,Object> map);
	
	/**
	 * 月末结转
	 */
	public void updateMonthh(Map<String,Object> map);
	
	/**
	 * 清空数据
	 * @param map
	 * @return
	 */
	public void clearData(Map<String,Object> map);
	
	/**
	 * 获取仓位是否已经初始
	 * @param monthh
	 */
	public Positn getQC(Positn positn);
	
	/**
	 * 仓库期初
	 * @param positnSupply
	 */
	public List<PositnSupply> getpositnSupplyList(PositnSupply positnSupply);
	
	/**
	 * 修改期初
	 * @param positnSupply
	 * @return
	 */
	public void updateCkInit(PositnSupply positnSupply);
	/**
	 * 删除期初
	 * @param positnSupply
	 * @return
	 */
	public void deleteCkInit(PositnSupply positnSupply);
	/**
	 * 保存期初
	 * @param positnSupply
	 * @return
	 */
	public void saveCkInit(PositnSupply positnSupply);
	
	/**
	 * 获取最大id
	 * @return
	 */
	public Spbatch getMaxId();
	
	/**
	 * 确认初始
	 * @param spbatch
	 * @return
	 */
	public void insertSpbatch(Spbatch spbatch);
	/**
	 * 更新物资表中的物资余额
	 */
	public void updateSupplyCnt(Supply supply);
	
	/**
	 * 更新仓位状态为已初始
	 * @param positnSupply
	 * @return
	 */
	public void updatePositn(PositnSupply PositnSupply);
	
	/**
	 * 1.检查物资的默认仓位
	 */
	public List<Supply> findSpPosition();
	
	/**
	 * 2.检查物资的默认供应商
	 */
	public List<Supply> findDeliver();
	
	/**
	 * 3.检查设置了半成品但是没有设置加工仓位
	 */
	public List<Supply> findPositnEx1();
	
	/**
	 * 4.检查设置了半成品但是没有设置BOM
	 */
	public List<Supply> findEx1Bom();
	
	/**
	 * 5.检查半成品BOM用的原材料已删除
	 */
	public List<Spcodeexm> findEx1BomSpCode();
	
	/**
	 * 6.检查菜品BOM用的原材料已删除
	 */
	public List<Costbom> findItemBomSpCode();
	
	/**
	 * 7.检查主直拨库
	 */
	public List<Positn> findMainPositn();
	
	/**
	 * 8.检查有仓位没有设定简称，将影响分拨功能的使用
	 */
	public List<Positn> findPositnDes1();
	
	/**
	 * 9.检查有主仓库或加工间没有在供应商中定义
	 */
	public List<Positn> findDeliverPositn();
	
	/**
	 * 10.检查没有定义节假日
	 */
	public List<Holiday> findHoliday();
	
	/**
	 * 11.检查分店物资属性 同虚拟物料的不同物资
	 */
	public List<Supply> findPositnSpcode();
	
	/**
	 * 12.检查物资属性设置有问题的物资
	 */
	public List<Supply> findSupplyAttributePm();

	/**
	 * 更新positn表月结月份为1
	 * @param positn 
	 */
	public void updatePositnMonthh(Positn positn);
}
