package com.choice.scm.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 第三方接口相关数据也验证类
 *
 */
public class ThrChkUtil {
	   private static final String CHECKTYPE_PK = "1";		//主键型		数字字母组合(可包含英文状态'-','_';不包含非英文状态字符及空格.)
	   private static final String CHECKTYPE_INT = "2";		//正整数型1	整数的字符串值(不包含任何除数字以外的字符)
	   private static final String CHECKTYPE_NAME = "3";	//名称型		中英文状态字符(每个中文状态字符为2位,英文状态字符为1位).
	   private static final String CHECKTYPE_DATE = "4";	//日期型		英文状态下的格式为:"yyyy-mm-dd"
	   private static final String CHECKTYPE_NUMBER = "5";	//数值型1	整数最大20位,小数最大8位的数值字符串(包含小数点)

    /**
     * 字符串默认值为空
     *
     * @author wangchao
     */
    private static final String empty = "";

    /**
     * 根据给定字符串及检测类型返回检测结果
     *
     * @param String chkStr	待检测字符串
     * @param String chkType	检测类型
     * @param String successMsg	检测成功提示内容
     * @param String errMsg	检测失败提示内容
     * @param HashMap paramMap 其他检测参数集合
     * @return 通过检测返回true否则返回false
     */
    public static boolean thrCheck(String chkStr, String chkType, String successMsg, String errMsg, HashMap paramMap) {
        if ((CHECKTYPE_PK).equals(chkType)) {
            return chkPk20(chkStr);
        } else if ((CHECKTYPE_INT).equals(chkType)) {
            return chkNumStr(chkStr);
        } else if ((CHECKTYPE_NAME).equals(chkType)) {
        	int minVal = Integer.parseInt(paramMap.get("min").toString());
        	int maxVal = Integer.parseInt(paramMap.get("max").toString());
        	
            return chkStrLen(chkStr, minVal, maxVal);
        } else if ((CHECKTYPE_DATE).equals(chkType)) {
            return chkDate(chkStr);
        } else if ((CHECKTYPE_NUMBER).equals(chkType)) {
            return chkFloatVal(chkStr);
        } else {
            return false;
        }
    }
    
    

    /**
     * 判断字符串是否为空
     *
     * @param str the string to validation
     * @return true if the string is empty
     * @author wangchao
     */
    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断字符串是否为空
     *
     * @param str the string to validation
     * @return true if the string is not empty
     * @author wangchao
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
    
    /**
     * 判断字符串是否为20位数字字母及-或_的组合字符串
     *
     * @param String str	验证目标字符串
     * @return boolean		验证匹配返回true 不匹配返回false
     */
    public static boolean chkPk20(String str) {
    	boolean isMatch = false;
        Pattern pat = Pattern.compile("[0-9a-zA-Z_-]{5,20}");

        Matcher mat = pat.matcher(str);
        if(mat.matches()){
        	isMatch = true;
        }
        
        return isMatch;
    }
    
    /**
     * 判断字符串是否为指定长度范围内的数字字母及-或_的组合字符串
     *
     * @param String str	验证目标字符串
     * @param String minLen	验证目标字符串最小长度
     * @param String maxLen	验证目标字符串最大长度
     * @return boolean		验证匹配返回true 不匹配返回false
     */
    public static boolean chkPkEx(String str, String minLen, String maxLen) {
    	boolean isMatch = false;
        Pattern pat = Pattern.compile("[0-9a-zA-Z_-]{" + minLen + "," + maxLen + "}");
        Matcher mat = pat.matcher(str);
        if(mat.matches()){
        	isMatch = true;
        }
        
        return isMatch;
    }
    
    /**
     * 判断字符串是否纯数字字符串
     *
     * @param String str	验证目标字符串
     * @return boolean		验证匹配返回true 不匹配返回false
     */
    public static boolean chkNumStr(String str) {
    	boolean isMatch = false;
        Pattern pat = Pattern.compile("^\\d+$");

        Matcher mat = pat.matcher(str);
        if(mat.matches()){
        	isMatch = true;
        }
        
        return isMatch;
    }
    
    /**
     * 判断字符串在英文状态下是否在指定长度范围内
     *
     * @param String str	验证目标字符串
     * @param String str	验证目标字符串最小长度(包含)
     * @param String str	验证目标字符串最大长度(包含)
     * @return boolean		验证匹配返回true 不匹配返回false
     */
    public static boolean chkStrLen(String str, int min, int max) {
    	boolean rv = false;
        String tmpStr = getChineseStr(str);
        int enLen = str.length() - tmpStr.length();
        int fullLen = enLen + tmpStr.length()*2;
        
        if(fullLen >= min && fullLen <= max){
        	rv = true;
        }
        
        return rv;
    }
    
    /**
     * 从给定字符串中获取中文字符
     *
     * @param String str	源字符串
     * @return String		匹配的中文字符
     */
    public static String getChineseStr(String str) {
        String rv = "";
        
        Pattern p=Pattern.compile("[\u4e00-\u9fa5]"); 
        Matcher m=p.matcher(str);
        while(m.find()){
        	rv = rv + m.group();
        }
        
        return rv;
    }
    
    /** 
     * 判断日期格式 格式必须为"yyyy-MM-dd"
     * @param str	待检测字符串
     * @return boolean	验证匹配返回true 不匹配返回false
     */  
    public static boolean chkDate(String str) {  
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
        try{  
            Date date = (Date)formatter.parse(str);  
            return str.equals(formatter.format(date));  
        }catch(Exception e){  
            return false;  
        }  
    }  
    
    /** 
     * 判断日期时间格式 格式必须为"yyyy-MM-dd HH:mm:ss"
     * @param str	待检测字符串
     * @return boolean	验证匹配返回true 不匹配返回false
     */  
    public static boolean chkDateTime(String str) {  
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        try{  
            Date date = (Date)formatter.parse(str);  
            return str.equals(formatter.format(date));  
        }catch(Exception e){  
            return false;  
        }  
    }  
    
    /** 
     * 判断浮点型数据格式(8位小数)
     * @param str	待检测字符串
     * @return boolean	验证匹配返回true 不匹配返回false
     */  
    public static boolean chkFloatVal(String str) {  
        boolean rv = false; 
        try{  
        	String reg = "^[0-9]+(.[0-9]+)?$";  
        	rv = str.matches(reg);  
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        return rv;
    }  

}