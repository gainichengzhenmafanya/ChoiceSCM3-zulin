package com.choice.scm.util;

import java.util.List;

import com.choice.assistant.domain.supplier.MaterialScope;



public class MaterialScopeList {
	
	private List<MaterialScope> listMaterialScope;

	public List<MaterialScope> getListMaterialScope() {
		return listMaterialScope;
	}

	public void setListMaterialScope(List<MaterialScope> listMaterialScope) {
		this.listMaterialScope = listMaterialScope;
	}
	
}
