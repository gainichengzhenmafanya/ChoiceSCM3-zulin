package com.choice.scm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.ServiceException;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.aliyun.oss.model.ObjectMetadata;
import com.choice.framework.util.ForResourceFiles;

/**
 * 阿里云OSS客户端工具
 * 
 * @author 
 *
 */
public class AliyunOssClient
{
	/**
	 * 阿里云OSS客户端
	 */
	private static OSSClient	ossClient;
	/**
	 * 阿里云accessKeyId
	 */
	private static String		accessKeyId			= ForResourceFiles.getValByKey("config-oss.properties","oss.accessKeyId");
	/**
	 * 阿里云accessKeySecret
	 */
	private static String		accessKeySecret		= ForResourceFiles.getValByKey("config-oss.properties","oss.accessKeySecret");

	/**
	 * bucket
	 */
	private static String		bucketName			= ForResourceFiles.getValByKey("config-oss.properties","oss.bucketName");
	/**
	 * 外网域名: scm-oss.oss-cn-shanghai.aliyuncs.com
	 * 
	 * 内网域名:scm-oss.oss-cn-shanghai-internal.aliyuncs.com
	 */
	private static String		endPoint			= ForResourceFiles.getValByKey("config-oss.properties","oss.endPoint");

	/**
	 * 租户模版文件夹标准版
	 */
	public static final String	templateStandard	= "templateStandard/";
	
	/**
	 * 租户模版文件夹个性化版
	 */
	public static final String	templateIndividualization	= "templateIndividualization/";
	
	public static final String	pathPrefix	= ForResourceFiles.getValByKey("config-oss.properties","oss.pathPrefix");


	public static void main(String[] args)
	{
		try
		{
			ossClient = new OSSClient("oss-cn-shanghai.aliyuncs.com", "g3XZGe2bDzaSiytF", "mzD24g25lkC5DFyTf3B7iX5b2SXfzL");

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 构建OSS客户端
	 */
	private static void generateOSSClient()
	{
		if (!AirUtils.hv(ossClient))
		{
			ossClient = new OSSClient(endPoint, accessKeyId, accessKeySecret);
		}
	}

	/**
	 * 创建Bucket
	 * 
	 * @param client
	 *            OSSClient对象
	 * @param bucketName
	 *            BUCKET名
	 * @throws OSSException
	 * @throws ClientException
	 */
	public static void ensureBucket(OSSClient client, String bucketName) throws OSSException, ClientException
	{
		try
		{
			client.createBucket(bucketName);
		}
		catch (ServiceException e)
		{
			throw e;
		}
	}

	/**
	 * 删除一个Bucket和其中的Objects
	 * 
	 * @param client
	 *            OSSClient对象
	 * @param bucketName
	 *            Bucket名
	 * @throws OSSException
	 * @throws ClientException
	 */
	public static void deleteBucket(OSSClient client, String bucketName) throws OSSException, ClientException
	{
		ObjectListing ObjectListing = client.listObjects(bucketName);
		List<OSSObjectSummary> listDeletes = ObjectListing.getObjectSummaries();
		for (int i = 0; i < listDeletes.size(); i++)
		{
			String objectName = listDeletes.get(i).getKey();
			System.out.println("objectName = " + objectName);
			// 如果不为空，先删除bucket下的文件
			client.deleteObject(bucketName, objectName);
		}
		client.deleteBucket(bucketName);
	}

	/**
	 * 把Bucket设置成所有人可读
	 * 
	 * @param client
	 *            OSSClient对象
	 * @param bucketName
	 *            Bucket名
	 * @throws OSSException
	 * @throws ClientException
	 */
	public static void setBucketPublicReadable(String bucketName) throws OSSException, ClientException
	{
		generateOSSClient();
		// 创建bucket
		ossClient.createBucket(bucketName);

		// 设置bucket的访问权限， public-read-write权限
		ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
	}

	/**
	 * 上传文件
	 * 
	 * @param client
	 *            OSSClient对象
	 * @param bucketName
	 *            Bucket名
	 * @param Objectkey
	 *            上传到OSS起的名（bucket下级 路径+名）
	 * @param filename
	 *            本地文件名（MultipartFile类型的文件）
	 * @throws OSSException
	 * @throws ClientException
	 * @throws IOException
	 * 
	 * @return String 返回图片路径
	 */
	public static String uploadFile(String Objectkey, MultipartFile file) throws OSSException, ClientException, IOException
	{
		generateOSSClient();
		ObjectMetadata objectMeta = new ObjectMetadata();
		objectMeta.setContentLength(file.getSize());
		String filename = file.getOriginalFilename();
		// 判断上传类型，多的可根据自己需求来判定
		if (filename.endsWith("xml"))
		{
			objectMeta.setContentType("text/xml");
		}
		else if (filename.endsWith("jpg"))
		{
			objectMeta.setContentType("image/jpeg");
		}
		else if (filename.endsWith("png"))
		{
			objectMeta.setContentType("image/png");
		}
		else if (filename.endsWith("zip"))
		{
			objectMeta.setContentType("zip");
		}
		else if (filename.endsWith("p12"))
		{
			objectMeta.setContentType("p12");
		}
		else if (filename.endsWith("apk"))
		{
			objectMeta.setContentType("application/vnd.android.package-archive");
		}else if(filename.endsWith("jrxml")){
			
			objectMeta.setContentType("jrxml");
		}else if(filename.endsWith("jasper")){
			
			objectMeta.setContentType("jasper");
		}
		
		InputStream input = file.getInputStream();
		ossClient.putObject(bucketName, Objectkey, input, objectMeta);

		return pathPrefix + Objectkey;
	}

	public static String uploadFile(String Objectkey, File file) throws OSSException, ClientException, IOException
	{
		generateOSSClient();
		ObjectMetadata objectMeta = new ObjectMetadata();
		String filename = file.getName();
		// 判断上传类型，多的可根据自己需求来判定
		if (filename.endsWith("xml"))
		{
			objectMeta.setContentType("text/xml");
		}
		else if (filename.endsWith("jpg"))
		{
			objectMeta.setContentType("image/jpeg");
		}
		else if (filename.endsWith("png"))
		{
			objectMeta.setContentType("image/png");
		}
		else if (filename.endsWith("zip"))
		{
			objectMeta.setContentType("zip");
		}
		else if (filename.endsWith("p12"))
		{
			objectMeta.setContentType("p12");
		}
		else if (filename.endsWith("apk"))
		{
			objectMeta.setContentType("application/vnd.android.package-archive");
		}
		else if (filename.endsWith("pdf"))
		{
			objectMeta.setContentType("pdf");
		}
		InputStream fileContent = new FileInputStream(file);
		ossClient.putObject(bucketName, Objectkey, fileContent, objectMeta);

		/*return pathPrefix + Objectkey;*/
		return  Objectkey;
	}

	/**
	 * 下载文件
	 * 
	 * @param client
	 *            OSSClient对象
	 * @param bucketName
	 *            Bucket名
	 * @param Objectkey
	 *            上传到OSS起的名(包括bucket以下的路径和文件名)
	 * @param filepath
	 *            文件下载到本地保存的路径（包括路径加上文件名）
	 * @throws OSSException
	 * @throws ClientException
	 */
	public static void downloadFile(String bucketName, String Objectkey, String filepath)
	{
		generateOSSClient();
		long startTime = System.currentTimeMillis();  
		ossClient.getObject(new GetObjectRequest(bucketName, Objectkey), new File(filepath));
		long endTime = System.currentTimeMillis();  
		System.out.println("下载花费时间约：" + (endTime - startTime) + " ms"); 
	}
	
	public static void downloadFile(String bucketName, String Objectkey, File filepath)
	{   
		long startTime = System.currentTimeMillis();  
		generateOSSClient();
		ossClient.getObject(new GetObjectRequest(bucketName, Objectkey), filepath);
		long endTime = System.currentTimeMillis();  
		System.out.println("下载花费时间约：" + (endTime - startTime) + " ms"); 
	}
	

	/**
	 * 从OOS上读取文件流
	 * 
	 * @param bucketName
	 * @param Objectkey
	 * @return
	 */
	public static InputStream loadOssFile(String Objectkey)
	{
		generateOSSClient();
		OSSObject a = ossClient.getObject(bucketName, Objectkey);
		return a.getObjectContent();
	}
	
	/**
	 * 从OOS判断文件存不存在 
	 * true 存在   false 不存在
	 * @param bucketName
	 *        bucket名称
	 * @param Objectkey
	 *        文件路径加名称
	 * @return
	 */
	public  static boolean headObject(String Objectkey)
	{
		generateOSSClient();
	    boolean flag= ossClient.doesObjectExist(bucketName, Objectkey);
		return flag;
	}

	/**
	 * 根据key删除OSS服务器上的文件
	 * 
	 * @Title: deleteFile
	 * @Description:
	 * @param @param ossConfigure
	 * @param @param filePath 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public static void deleteFile(String filePath)
	{
		generateOSSClient();
		ossClient.deleteObject(bucketName, filePath);
	}
	
	

}
