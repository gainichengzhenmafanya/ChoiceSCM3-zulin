package com.choice.scm.util;

import java.util.ArrayList;
import java.util.List;


public class ArrayUtil {

	/**
	 * 求两个String数组的交集
	 * @param array1 数组1
	 * @param array2 数组2
	 * @return
	 */
	public static String[] getMixedArray(String[] array1,String[] array2){
		List<String> list = new ArrayList<String>();
		String[] resultArray;
		if(null!=array1 && null!=array2 && array1.length!=0 && array2.length!=0){
			for(String s1:array1){
				for(String s2:array2){
					if(s1.equals(s2)){
						list.add(s1);
					}
				}
			}
			resultArray = new String[list.size()];
			int index = 0;
			for(String s : list){
				resultArray[index++] = s;
			}
			
			return resultArray;
		}else{
			return new String[0];
		}
	}
	
	/**
	 * 得到第一个数组中不包含第二个数组中元素的数组。例如: 数组1为["aa","bb","cc","dd"]，数组2为["bb","dd","ee"]，结果为["aa","cc"] 
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static String[] getMinArray(String[] array1,String[] array2){
		String[] resultArray;
		List<String> list = new ArrayList<String>();
		for(String s:array1){
			list.add(s);
		}
		
		if(null!=array1 && null!=array2 && array1.length!=0 && array2.length!=0){
			for(String s1:array1){
				for(String s2:array2){
					if(s1.equals(s2)){
						list.remove(s1);
					}
				}
			}
			
			resultArray = new String[list.size()];
			int i=0;
			for(String s:list){
				resultArray[i++]=s;
			}
			return resultArray;
		}else{
			return new String[0];
		}
		
		
	}
	
	/**
	 * 求两个String数组的合集
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static String[] getSumArray(String[] array1,String[] array2){
		List<String> list = new ArrayList<String>();
		String[] resultArray;
		if(null!=array1 && null!=array2 && array1.length!=0 && array2.length!=0){
			for(String s1:array1){
				list.add(s1);
			}
			for(String s2:array2){
				if(!list.contains(s2)){
					list.add(s2);
				}
			}
			
			resultArray = new String[list.size()];
			int index = 0;
			for(String s : list){
				resultArray[index++] = s;
			}
			return resultArray;
		}else{
			return (array1==null||array1.length==0)?array2:array1;
		}
	}
	
	/**
	 * 求两个String类型的元素交集
	 * @param ss1
	 * @param ss2
	 * @param split 分隔符
	 * @return
	 */
	public static String getMixedString(String ss1,String ss2,String split){
		if(null!=ss1 && null!=ss2){
			StringBuffer result = new StringBuffer();
			String[] ssArray1 = ss1.split(split);
			String[] ssArray2 = ss2.split(split);
			String[] resultArray = getMixedArray(ssArray1,ssArray2);
			for(String str : resultArray){
				result.append(str).append(split);
			}
			return result.length()!=0?result.substring(0, result.lastIndexOf(split)):"";
		}else{
			return "";
		}
		
	}
	
	/**
	 * 求第一个String 元素中去除第二个String 元素的String
	 * 例如：ss1 = "11,12,13,14,15" ss2="11,13,15,17,19" 结果为"12,14"
	 * @param ss1
	 * @param ss2
	 * @param split 分隔符
	 * @return
	 */
	public static String getMinString(String ss1,String ss2,String split){
		if(null!=ss1 && null!=ss2){
			StringBuffer result = new StringBuffer();
			String[] ssArray1 = ss1.split(split);
			String[] ssArray2 = ss2.split(split);
			String[] resultArray = getMinArray(ssArray1,ssArray2);
			for(String str : resultArray){
				result.append(str).append(split);
			}
			return result.length()!=0?result.substring(0, result.lastIndexOf(split)):"";
		}else{
			return "";
		}
		
	}
	
	/**
	 * 求两个String类型的元素合集
	 * @param ss1
	 * @param ss2
	 * @param split 分隔符
	 * @return
	 */
	public static String getSumString(String ss1,String ss2,String split){
		StringBuffer result = new StringBuffer();
		if(null != ss1 && null != ss2){
			String[] ssArray1 = ss1.split(split);
			String[] ssArray2 = ss2.split(split);
			String[] resultArray = getSumArray(ssArray1,ssArray2);
			for(String str : resultArray){
				result.append(str).append(split);
			}
			return result.substring(0, result.lastIndexOf(split));
		}else{
			return null==ss1?ss2:ss1;
		}
		
	}
	
	//测试
	public static void main(String[] args) {
		System.out.println(getMinString("11,12,13,14,15", "13,14,15,16,17", ","));
		
	}
}
