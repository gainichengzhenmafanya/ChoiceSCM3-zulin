package com.choice.scm.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import net.sf.json.JSONObject;

public class caidan {
	

	public static void main(String[] args) {
		String accessToken = getAccessToken();
//		accessToken=getAccessToken();
//		accessToken="jhJKDrjCatgh-Jt0WA40kbybTGHbIBLCdgmbjt11aqji8Le024bYfOFickCiWqFn5tsNayzDd5tPCwiJVqE5qMa5cvAL-5F_cEgnI3sYoBY";
		// createMenu(getGson(),accessToken);

		// System.out.println(sendMessgage());

//		System.out.println(accessToken);
		
//		String template = getTemplate(accessToken);
		
//		sendMes(sendMessgage(), accessToken);
		sendMes(send("",""), accessToken);
	}
	
	
	

	
	/**
	 * 发送服务信息
	 * @Description:
	 * @Title:send
	 * @Author:dwh
	 * @Date:2015-1-28 下午6:36:06 	
	 * @return
	 */
	public static String send(String openid,String content) {
		JSONObject jobj = new JSONObject();
		JSONObject msginfo = new JSONObject();
		
//		jobj.put("touser", "oYZE_uIV44zwV5FfEH4M85uolvqg");
		jobj.put("touser", "opemod");
		jobj.put("msgtype", "text");
		
		msginfo.put("content", "茶叶5斤\n小米5斤\n大米5斤\n");
//		msginfo.put("content", content);
		jobj.put("text", msginfo);
		System.out.println(jobj.toString());
		return jobj.toString();
	}
	
	
	
	/**
	 * 獲取accesstoken
	 * 
	 * @Description:
	 * @Title:getAccessToken
	 * @Author:dwh
	 * @Date:2015-1-19 下午3:50:59
	 */
	public static String getAccessToken() {
		StringBuffer bufferRes = new StringBuffer();
		String   appid="wx48c06e3b32f3c027";
		String   secret="57dbed1d1ec5de6c65e941c2bc7f6e43";
		try {
			// URL("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxc1437f5d60f3755e&secret=4d879044188e5c2e262e4ebcbbd2c12e");
			URL realUrl = new URL(
					"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret);
			HttpURLConnection conn = (HttpURLConnection) realUrl
					.openConnection();
			// 连接超时 ​​
			conn.setConnectTimeout(25000);
			// 读取超时 --服务器响应比较慢，增大时间
			conn.setReadTimeout(25000);
			HttpURLConnection.setFollowRedirects(true);
			// 请求方式
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0");
			conn.setRequestProperty("Referer", "https://api.weixin.qq.com/");
			conn.connect();
			// 获取URLConnection对象对应的输出流
			OutputStreamWriter out = new OutputStreamWriter(
					conn.getOutputStream());
			// 发送请求参数
			// out.write(URLEncoder.encode(params,"UTF-8"));
			// out.write(params);
			out.flush();
			out.close();
			InputStream in = conn.getInputStream();
			BufferedReader read = new BufferedReader(new InputStreamReader(in,
					"UTF-8"));
			String valueString = null;
			while ((valueString = read.readLine()) != null) {
				bufferRes.append(valueString);
			}
			System.out.println(bufferRes.toString());
			in.close();
			if (conn != null) {
				// 关闭连接
				conn.disconnect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JSONObject json = new JSONObject();
		//将返回的json串转会为json对象
		json = JSONObject.fromObject(bufferRes.toString());
		//通过json对象取到json值
		String accesstoken = json.getString("access_token");
		return accesstoken;

//		return bufferRes.toString();

	}

	
	
	
	
	
	/**
	 * 发送消息模板信息
	 * @Description:
	 * @Title:sendMes
	 * @Author:dwh
	 * @Date:2015-1-28 下午6:35:39 	
	 * @param params
	 * @param accessToken
	 */
	public static void sendMes(String params, String accessToken) {
		StringBuffer bufferRes = new StringBuffer();
		try {
			// 测试测试环境url
			// URL realUrl = new
			// URL("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxd6c7035d76ad8ab0&secret=aab813fdf22f20707e3f4d5cf5be1dbf");
//			URL realUrl = new URL("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+ accessToken);
			URL realUrl = new URL("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="+ accessToken);
			HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
			// 连接超时 ​​
			conn.setConnectTimeout(25000);
			// 读取超时 --服务器响应比较慢，增大时间
			conn.setReadTimeout(25000);
			HttpURLConnection.setFollowRedirects(true);
			// 请求方式
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0");
			conn.setRequestProperty("Referer", "https://api.weixin.qq.com/");
			conn.connect();
			// 获取URLConnection对象对应的输出流
			OutputStreamWriter out = new OutputStreamWriter(
					conn.getOutputStream());
			// 发送请求参数
			// out.write(URLEncoder.encode(params,"UTF-8"));
			out.write(params);
			out.flush();
			out.close();
			InputStream in = conn.getInputStream();
			BufferedReader read = new BufferedReader(new InputStreamReader(in,
					"UTF-8"));
			String valueString = null;
			while ((valueString = read.readLine()) != null) {
				bufferRes.append(valueString);
			}
			System.out.println(bufferRes.toString());
			in.close();
			if (conn != null) {
				// 关闭连接
				conn.disconnect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
}