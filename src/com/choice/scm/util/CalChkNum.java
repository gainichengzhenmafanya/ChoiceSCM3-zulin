package com.choice.scm.util;

import groovy.lang.Singleton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.persistence.ExecSql;

@Component
@Singleton
public class CalChkNum {

	// 盘点仓位
	public static final String profitDeliver = "1999";
	
	public static final String CHKIN = "RK";
	public static final String CHKZB = "ZF";
	public static final String CHKOUT = "CK";
	public static final String CHKOTH = "";
	public static final String CHKPAY = "";
	public static final String CHKSTO = "SG";
	public static final String INVENTPD = "PD";
	public static final String CHKSTOBH = "BH";
	private static HashMap<String,String> map = new HashMap<String,String>();
	@Autowired
	ExecSql execSql;
	
	public CalChkNum(){
		map.put(CalChkNum.CHKIN, "chkinm");
		map.put(CalChkNum.CHKZB, "chkinm");
		map.put(CalChkNum.CHKOUT, "chkoutm");
		map.put(CalChkNum.CHKOTH, "chkothm");
		map.put(CalChkNum.CHKPAY, "chkpaym");
		map.put(CalChkNum.CHKSTO, "chkstom");
		map.put(CalChkNum.INVENTPD, "inventorydemom");
		map.put(CalChkNum.CHKSTOBH, "chkstodemom");
	}
	
	public String getNext(String type,Date date){
		Integer num;
		DateFormat fmt = new SimpleDateFormat("yyMMdd");
		StringBuffer vouno = new StringBuffer(type).append(fmt.format(date)).append("-");
		StringBuffer sql = new StringBuffer();
		sql.append("select max(subString(vouno,10,3)) from ").append(map.get(type))
		.append(" where vouno like '").append(vouno).append("___'");
		try{
			num = execSql.execSql(sql.toString());
		}catch(Exception e){
			num = 0;
		}
		
		vouno.append(String.format("%1$03d", ++num));
		return vouno.toString();
	}
	/**
	 * 申购单  需要拼接  如    SG7000001-140227-001  一样的    凭证号 
	 * @return
	 */
	public String getNextBytable(String table,String type,Date date){
		Integer num;
		DateFormat fmt = new SimpleDateFormat("yyMMdd");
		StringBuffer vouno = new StringBuffer(type).append(fmt.format(date)).append("-");
		StringBuffer sql = new StringBuffer();
		
		sql.append("select max(substr(vouno,-3,3)) from ").append(table)
		.append(" where vouno like '").append(vouno).append("___'");
		try{
			num = execSql.execSql(sql.toString());
		}catch(Exception e){
			num = 0;
		}
		
		vouno.append(String.format("%1$03d", ++num));
		return vouno.toString();
	}
	
	/**
	 * 取得序列最大值
	 * @param seqName
	 * @return
	 * @throws CRUDException
	 */
	public Integer getMaxsequences(String seqName) throws CRUDException {
		return execSql.getMaxsequences(seqName);
	}
}
