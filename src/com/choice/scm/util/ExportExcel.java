package com.choice.scm.util;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.stereotype.Component;

import com.choice.orientationSys.domain.DictColumns;
@Component
public class ExportExcel<T> {
	
	//导出EXCEL每个SHEET最大的行数
	private static final int ONESHEET_MAX_ROWS = 65530;

	private DecimalFormat decimalFormat = new DecimalFormat("0.00");
	WritableWorkbook workBook;
	
	/**
	 * js 20160831 修改支持多sheet，超过设定记录数，则新加sheet
	 * @param os
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @return
	 */
	public boolean creatWorkBook(OutputStream os,List<T> data,String title,List<DictColumns> dictColumns){
		//标题内容样式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);//标题格式
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);//文本内容格式
		WritableCellFormat intStyle = new WritableCellFormat(contentFont,new NumberFormat("0"));//设定带小数数字单元格格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont,new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			workBook = Workbook.createWorkbook(os);
			titleStyle.setAlignment(Alignment.CENTRE);
			//create Sheet named "Sheet1". 0 means this is 1st page.
			//数据量
			int dataSize = data.size();
			//有多少SHEET
			int sheetNum = dataSize%ONESHEET_MAX_ROWS==0?dataSize/ONESHEET_MAX_ROWS:(dataSize/ONESHEET_MAX_ROWS)+1;
			//遍历生成SHEET
			for(int sn=0;sn<sheetNum;sn++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(sn+1), sn);
        		//define cell column and row in Label Constructor, and cell content write title.   
        		//cell is 1st-Column,1st-Row. value is title.
        		Label label = new Label(0,0,title,titleStyle);
        		//add defined cell above to sheet instance.   
        		sheet.addCell(label);
        		//设置表格表头
        		for(int bi = 0 ; bi < dictColumns.size();bi++){
        			Label head = new Label(bi,1,dictColumns.get(bi).getZhColumnName());
        			sheet.addCell(head);
        		}
        		//合并标题列
        		sheet.mergeCells(0, 0, dictColumns.size()-1, 0);
				
        		//结束索引
        		int iEnd = (sn==sheetNum-1)?dataSize:(sn+1)*ONESHEET_MAX_ROWS;
        		int line = 0;
                //遍历list填充表格内容
                for(int i = sn*ONESHEET_MAX_ROWS ; i < iEnd; i++){
                	//查过每个SHEET的最大行数，则新建一个SHEET
                    //循环数据输出
                	for(int j = 0 ; j < dictColumns.size() ; j ++){
                		DictColumns curCol = dictColumns.get(j);
                		String result = "";
                		String typeName = "";
                		Stack<String> propStack = new Stack<String>();
                		propStack.addAll(Arrays.asList(curCol.getProperties().split("\\.")));
                		T cur = data.get(i);
                		Object curValue = getValue(propStack,cur);
                		curValue = curValue == null ? "":curValue;
                		typeName = curValue.getClass().getName();
                		typeName = typeName.substring(typeName.lastIndexOf('.')+1);
                		if("int".equals(typeName) || "Integer".equals(typeName)){
        					result = curValue.toString();
        				}else if("Date".equals(typeName)){
        					DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        					result = dft.format(curValue);
        				}else{
        					result = curValue.toString();
        				}
                		if("int".equals(curCol.getColumnType())){
                			try{
                				jxl.write.Number number = new jxl.write.Number(j,2+line,Double.parseDouble(result),intStyle);
                				sheet.addCell(number);
                			}catch(Exception e){
                				Label lab = new Label(j,2+line,result,contentStyle);
                        		sheet.addCell(lab);
                			}
                		}else if("double".equals(curCol.getColumnType())){
                			try{
                				jxl.write.Number number = new jxl.write.Number(j,2+line,Double.parseDouble(result),doubleStyle);
                				sheet.addCell(number);
                			}catch(Exception e){
                				Label lab = new Label(j,2+line,result,contentStyle);
                        		sheet.addCell(lab);
                			}
                		}else if("digital".equals(curCol.getColumnType())){
                			try{
                				jxl.write.Number number = new jxl.write.Number(j,2+line,Double.parseDouble(result),doubleStyle);
                				sheet.addCell(number);
                			}catch(Exception e){
                				Label lab = new Label(j,2+line,result,contentStyle);
                        		sheet.addCell(lab);
                			}
                		}else{
                			Label lab = new Label(j,2+line,result,contentStyle);
                    		sheet.addCell(lab);
                		}
                	}
                	//行增长
                	line++;
                }
			}
			//写入
			workBook.write();
			//写入
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	//解析对象数据
	@SuppressWarnings("unchecked")
	private Object getValue(Stack<String> stack,Object obj) throws Exception{
		String className = obj.getClass().getName();
		Object result = null;
		if(stack.empty())return null;
		String cur = stack.pop();
		if(className.indexOf("java.util.HashMap") >= 0){
			result = ((Map<Object,Object>)obj).get(cur.toUpperCase());
		}else{
			Field curFeild = Class.forName(className).getDeclaredField(cur);
			Boolean accessible = curFeild.isAccessible();
    		if(!accessible)curFeild.setAccessible(true);
    		result = curFeild.get(obj);
    		curFeild.setAccessible(accessible);
		}
		if(stack.empty())return result;
		return getValue(stack,result);
	}
	
	/**
	 * 判断是否是合计列
	 * @return
	 */
	public boolean isSumColumn(String columnName){
		if(columnName!=null){
			return (columnName.equalsIgnoreCase("stockcnt")||columnName.equalsIgnoreCase("totalamt")||columnName.equalsIgnoreCase("cnt1")||columnName.equalsIgnoreCase("cnt2")||columnName.equalsIgnoreCase("cnt3")||columnName.equalsIgnoreCase("cnt4"));
		}else{
			return false;
		}
	}
	
	

	/**
	 * 导出门店盘点明细
	 * 
	 * @param os
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @author jin shuai at 20160317 增加合计列
	 * @return
	 */
	public boolean creatWorkBook_j1(OutputStream os, List<T> data, String title, List<DictColumns> dictColumns) {
		
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat intStyle = new WritableCellFormat(contentFont, new NumberFormat("0"));// 设定带小数数字单元格格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		try {
			workBook = Workbook.createWorkbook(os);
			titleStyle.setAlignment(Alignment.CENTRE);
			// create Sheet named "Sheet1". 0 means this is 1st page.
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
			// define cell column and row in Label Constructor, and cell content
			// write title.
			// cell is 1st-Column,1st-Row. value is title.
			Label label = new Label(0, 0, title, titleStyle);
			// add defined cell above to sheet instance.
			sheet.addCell(label);
			
			// 增加记录合计列的map
			Map<Integer, Double> sumMap = new HashMap<Integer, Double>();

			// 设置表格表头
			for (int i = 0; i < dictColumns.size(); i++) {
				Label head = new Label(i, 1, dictColumns.get(i).getZhColumnName());
				sheet.addCell(head);
				
				//判断是否是合计列
				String columnName = dictColumns.get(i).getColumnName();
				
				if(isSumColumn(columnName)){
					sumMap.put(i, 0d);
				}
			}

			sheet.mergeCells(0, 0, dictColumns.size() - 1, 0);
			// 遍历list填充表格内容
			int dataSize = data.size();
			for (int i = 0; i < dataSize; i++) {
				for (int j = 0; j < dictColumns.size(); j++) {
					DictColumns curCol = dictColumns.get(j);
					String result = "";
					String typeName = "";
					Stack<String> propStack = new Stack<String>();
					propStack.addAll(Arrays.asList(curCol.getProperties().split("\\.")));
					T cur = data.get(i);
					Object curValue = getValue(propStack, cur);
					curValue = curValue == null ? "" : curValue;
					typeName = curValue.getClass().getName();
					typeName = typeName.substring(typeName.lastIndexOf('.') + 1);
					if ("int".equals(typeName) || "Integer".equals(typeName)) {
						result = curValue.toString();
					} else if ("Date".equals(typeName)) {
						DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
						result = dft.format(curValue);
					} else {
						result = curValue.toString();
					}
					if ("int".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), intStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else if ("double".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), doubleStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else if ("digital".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), doubleStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else {
						Label lab = new Label(j, 2 + i, result, contentStyle);
						sheet.addCell(lab);
					}

					// 合计列
					if (sumMap.containsKey(j)) {
						Double num = sumMap.get(j);
						Double addnum = 0d;
						try {
							addnum = Double.parseDouble(result);
						} catch (Exception e) {
							System.out.println(result + ":转换为double出错");
							addnum = 0d;
						}
						sumMap.put(j, num + addnum);
					}

				}

			}
			
			//有合计列
			if(sumMap!=null&&sumMap.size()>0){
				// 增加合计列
				Iterator<Integer> iter = sumMap.keySet().iterator();
				int numLine = dataSize + 5;
				// 合计字符串
				Label hjLabel = new Label(0, numLine, "合计:", contentStyle);
				sheet.addCell(hjLabel);
				while (iter.hasNext()) {
					int key = iter.next();
					// 得到值
					Double value = sumMap.get(key);
					// 添加合计
					jxl.write.Number number = new jxl.write.Number(key, numLine, Double.parseDouble(decimalFormat.format(value)), doubleStyle);
					sheet.addCell(number);
				}
			}
			

			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/**
	 * 导出带合计
	 * 
	 * @param os
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @param outColumns 需要排除合计的列-必须是列名-大写小写都可以
	 * @author jin shuai at 20160323 导出带合计
	 * @return
	 */
	public boolean creatWorkBookAddHj(OutputStream os, List<T> data, String title, List<DictColumns> dictColumns,String[] outColumns) {
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat intStyle = new WritableCellFormat(contentFont, new NumberFormat("0"));// 设定带小数数字单元格格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		try {
			workBook = Workbook.createWorkbook(os);
			titleStyle.setAlignment(Alignment.CENTRE);
			// create Sheet named "Sheet1". 0 means this is 1st page.
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
			// define cell column and row in Label Constructor, and cell content
			// write title.
			// cell is 1st-Column,1st-Row. value is title.
			Label label = new Label(0, 0, title, titleStyle);
			// add defined cell above to sheet instance.
			sheet.addCell(label);
			// 增加记录合计列的map
			Map<Integer, Double> sumMap1 = new HashMap<Integer, Double>();
			// 设置表格表头
			for (int i = 0; i < dictColumns.size(); i++) {
				Label head = new Label(i, 1, dictColumns.get(i).getZhColumnName());
				sheet.addCell(head);
				String columnName = dictColumns.get(i).getColumnName();
				//如果不排除就放到集合中
				if(!isOutColumn(columnName,outColumns)){
					sumMap1.put(i, 0d);
				}
			}
			sheet.mergeCells(0, 0, dictColumns.size() - 1, 0);
			// 遍历list填充表格内容
			int dataSize = data.size();
			for (int i = 0; i < dataSize; i++) {
				for (int j = 0; j < dictColumns.size(); j++) {
					DictColumns curCol = dictColumns.get(j);
					String result = "";
					String typeName = "";
					Stack<String> propStack = new Stack<String>();
					propStack.addAll(Arrays.asList(curCol.getProperties().split("\\.")));
					T cur = data.get(i);
					Object curValue = getValue(propStack, cur);
					curValue = curValue == null ? "" : curValue;
					typeName = curValue.getClass().getName();
					typeName = typeName.substring(typeName.lastIndexOf('.') + 1);
					if ("int".equals(typeName) || "Integer".equals(typeName)) {
						result = curValue.toString();
					} else if ("Date".equals(typeName)) {
						DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
						result = dft.format(curValue);
					} else {
						result = curValue.toString();
					}
					if ("int".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), intStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else if ("double".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), doubleStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else if ("digital".equals(curCol.getColumnType())) {
						try {
							jxl.write.Number number = new jxl.write.Number(j, 2 + i, Double.parseDouble(result), doubleStyle);
							sheet.addCell(number);
						} catch (Exception e) {
							Label lab = new Label(j, 2 + i, result, contentStyle);
							sheet.addCell(lab);
						}
					} else {
						Label lab = new Label(j, 2 + i, result, contentStyle);
						sheet.addCell(lab);
					}
					//是合计列
					if(isNumberClass(typeName)&&sumMap1.containsKey(j)){
							Double num = sumMap1.get(j);
							Double addnum = 0d;
							try {
								addnum = Double.parseDouble(curValue.toString());
							} catch (Exception e) {
								System.out.println(curValue + ":转换为double出错");
								addnum = 0d;
							}
							sumMap1.put(j, num + addnum);
					}
				}
			}
			//最后加合计
			if(sumMap1!=null&&sumMap1.size()>0){
				// 增加合计列
				Iterator<Integer> iter = sumMap1.keySet().iterator();
				int numLine = dataSize + 5;
				// 合计字符串
				Label hjLabel = new Label(0, numLine, "合计:", contentStyle);
				sheet.addCell(hjLabel);
				while (iter.hasNext()) {
					int key = iter.next();
					// 得到值
					Double value = sumMap1.get(key);
					// 添加合计
					jxl.write.Number number = new jxl.write.Number(key, numLine, Double.parseDouble(decimalFormat.format(value)), doubleStyle);
					sheet.addCell(number);
				}
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 判断是否是排除合计的列
	 * @param columName 需要判断的列名
	 * @param outColumns 所有排除的列名数组
	 * @return
	 */
	public boolean isOutColumn(String columName,String[] outColumns){
		//如果需要判断的列为空，则不排除
		if(columName==null||"".equals(columName)){
			return false;
		}
		
		//如果需要排除的列数组为空，则不排除
		if(outColumns==null||outColumns.length==0){
			return false;
		}
		else{
			for(String column : outColumns){
				if(column!=null){
					if(column.trim().equalsIgnoreCase(columName.trim())){
						return true;
					}
				}
			}
			return false;
		}
	}
	
	/**
	 * 判断类名是不是数字类
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isNumberClass(String className) {
		return className.equals("Short") || className.equals("Integer") || className.equals("Long") || className.equals("Float") || className.equals("Double") || className.equals("BigInteger") || className.equals("BigDecimal") || className.equals("short") || className.equals("int") || className.equals("long") || className.equals("float") || className.equals("double");
	}

}
