package com.choice.scm.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author wangchao
 * @version 2014.04.08 创建
 */
public class StringUtil {

    /**
     * 字符串默认值为空
     *
     * @author wangchao
     */
    private static final String empty = "";

    /**
     * 判断字符串是否为空
     *
     * @param str the string to validation
     * @return true if the string is empty
     * @author wangchao
     */
    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断字符串是否为空
     *
     * @param str the string to validation
     * @return true if the string is not empty
     * @author wangchao
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 判断字符串是否为空
     *
     * @param str    the string to validation
     * @param strdef the default string to return
     * @return str if str is not empty else return strdef
     * @author wangchao
     */
    public static String checkDbStr(Object str, String strdef) {
        boolean rv = false;
        if (str == null) {
            rv = true;
        } else if (String.valueOf(str).length() == 0) {
            rv = true;
        } else {
            rv = false;
        }

        return rv ? isEmpty(strdef) ? "" : strdef : String.valueOf(str);
    }

    /**
     * 指定字符串替换
     *
     * @param strSc  原始字符串
     * @param oldStr 将要被替换的字符串
     * @param newStr 新的字符串
     * @return the string replaced
     * @author wangchao
     */
    public static String str_replace(String oldStr, String newStr, String strSc) {
        StringBuffer bf = new StringBuffer("");
        StringTokenizer st = new StringTokenizer(strSc, oldStr, true);
        while (st.hasMoreTokens()) {
            String tmp = st.nextToken();
            if (tmp.equals(oldStr)) {
                bf.append(newStr);
            } else {
                bf.append(tmp);
            }
        }
        return bf.toString();
    }

    /**
     * 将字符串数组以指定的字符进行连接
     *
     * @param str   原始数组
     * @param token 链接标识
     * @return the string connected
     * @author wangchao
     */
    public static String join(String[] str, String token) {
        return join(Arrays.asList(str), token);
    }

    public static String join(Collection<String> str, String token) {
        StringBuffer sbf = new StringBuffer("");
        Iterator<String> iterator = str.iterator();
        while (iterator.hasNext()) {
            sbf.append(iterator.next());
            sbf.append(token);
        }

        return sbf.length() > 0 ? sbf.subSequence(0, sbf.length() - token.length()).toString() : sbf.toString();
    }

    /**
     * 检查字符串中是否包含某字符
     *
     * @param str  字符串
     * @param test 是否存在的字符或者字符串
     * @return 包含返回true
     * @author wangchao
     */
    public static boolean checkStr(String str, String test) {
        boolean flag = false;
        if (str.contains(test)) {
            flag = true;
        }
        return flag;
    }

    /**
     * 比较两个字符串的大小，返回较大的
     * index == 1 --&gt;str1大于str2 ， index == -1 --&gt; str1小于str2
     *
     * @param str1 the first string
     * @param str2 the second string
     * @return the larger string
     * @author wangchao
     */
    public static String compareBigStr(String str1, String str2) {
        String string = null;
        int index = str1.compareTo(str2);
        if (index == 1) {
            string = str1;
        } else {
            string = str2;
        }
        return string;
    }

    /**
     * 比较两个字符串的大小，返回较小的
     *
     * @param str1 the first string
     * @param str2 the second string
     * @return the littler string
     * @author wangchao
     */
    public static String compareSamilStr(String str1, String str2) {
        String string = null;
        int index = str1.compareTo(str2);
        if (index == 1) {
            string = str2;
        } else {
            string = str1;
        }
        return string;
    }

    /**
     * 截取并保留标志位之前的字符串
     *
     * @param str  传人字符串
     * @param expr 传人标识
     * @return 截取之后的字符串
     * @author wangchao
     */
    public static String substringBefore(String str, String expr) {
        if (isEmpty(str) || expr == null) {
            return str;
        }
        if (expr.length() == 0) {
            return empty;
        }
        int pos = str.indexOf(expr);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * 截取并保留标志位之后的字符串
     *
     * @param str  传人字符串
     * @param expr 传人标识
     * @return 截取之后的字符串
     * @author wangchao
     */
    public static String substringAfert(String str, String expr) {
        if (isEmpty(str)) {
            return str;
        }
        if (expr == null) {
            return empty;
        }
        int pos = str.indexOf(expr);
        if (pos == -1) {
            return empty;
        }
        return str.substring(pos + expr.length());
    }

    /**
     * 截取并保留最后一个标志位之前的字符串
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return the sub string
     * @author wangchao
     */
    public static String substringBeforeLast(String str, String expr) {
        if (isEmpty(str) || isEmpty(expr)) {
            return str;
        }
        int pos = str.lastIndexOf(expr);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * 截取并保留最后一个标志位之后的字符串
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return the substring
     * @author wangchao
     */
    public static String substringAfterLast(String str, String expr) {
        if (isEmpty(str)) {
            return str;
        }
        if (isEmpty(expr)) {
            return empty;
        }
        int pos = str.lastIndexOf(expr);
        if (pos == -1 || pos == (str.length() - expr.length())) {
            return empty;
        }
        return str.substring(pos + expr.length());
    }

    /**
     * 把字符串按分隔符转换为数组
     *
     * @param str  字符串
     * @param expr 标识符
     * @return the substring
     * @author wangchao
     */
    public static String[] stringToArray(String str, String expr) {
        return str.split(expr);
    }

    /**
     * 分割字符串
     *
     * @param str       字符串
     * @param splitsign 标识符
     * @return String 类型的数组
     * @author wangchao
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static String[] split(String str, String splitsign) {
        int index;
        if (isEmpty(str) || isEmpty(splitsign)) {
            return null;
        }
        List al = new ArrayList();
        while ((index = str.indexOf(splitsign)) != -1) {
            al.add(str.substring(0, index));
            str = str.substring(index + splitsign.length());
        }
        al.add(str);
        return (String[]) al.toArray(new String[0]);
    }

    /**
     * 根据给定float数值保留小数位数和是否四舍五入返回结果字符串
     *
     * @param val  要转换的数值
     * @param n    保留小数位数
     * @param isup 是否四舍五入
     * @return the result
     * @author wangchao
     */
    public static String floatToString(float val, int n, boolean isup) {
        String formatStr = "0.";
        String rv = "";
        if (isup) {
            if (n > 0) {
                for (int i = 0; i < n; i++) {
                    formatStr = formatStr + "0";
                }
                DecimalFormat decimalFormat = new DecimalFormat(formatStr);
                rv = decimalFormat.format(val);
            } else {
                rv = String.valueOf(val);
            }
        } else {
            rv = String.valueOf(val);
            rv = rv.substring(0, "111.123456".indexOf(".") + n);
        }

        return rv;
    }

    /**
     * 判断是否数字
     *
     * @param obj 待校验字符串
     * @return boolean 是否为数字验结果
     */
    public static boolean isNumeric(Object obj) {
        if (obj == null) return false;

        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(obj.toString());
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * 根据指定补白方式组合字符串
     *
     * @param oStr   待处理字符串
     * @param maxLen 处理结果总长度(半角状态英文字母长度为1)
     * @return spacePosition 结果字符串排列位置 0:居左 1:居右 2:居中
     */
    public static String fixVal(String oStr, int maxLen, int spacePosition) {
        String rv = "";
        if (StringUtil.isEmpty(oStr)) {
            return "";
        }
        int strLen = getStrLen(oStr);
        if (strLen > maxLen) {
            rv = getSubStr(oStr, maxLen);
        } else {
            String spaceChars = "";
            int iSpaceNum = maxLen - strLen;
            for (int c = 0; c < iSpaceNum; c++) {
                spaceChars = spaceChars + " ";
            }
            if (spacePosition == 0) {
                rv = oStr + spaceChars;
            } else if (spacePosition == 1) {
                rv = spaceChars + oStr;
            } else if (spacePosition == 2) {
                if (strLen >= 0) {
                    rv = spaceChars.substring(iSpaceNum / 2) + oStr;
                } else {
                    rv = oStr;
                }
            } else {
                rv = oStr;
            }
        }

        return rv;
    }

    public static int getStrLen(String str) {
        int rv = 0;
        if (StringUtil.isEmpty(str)) {
            return 0;
        }
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == ' ' || c == '.' || c == '+' || c == '-' || c == ':' || c == '(' || c == ')') {
                rv++;
            } else {
                rv++;
                rv++;
            }
        }

        return rv;
    }

    public static String getSubStr(String str, int len) {
        String rv = "";
        if (StringUtil.isEmpty(str)) {
            return "";
        }
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == ' ' || c == '.' || c == '+' || c == '-' || c == ':' || c == '(' || c == ')') {
                count++;
            } else {
                count++;
                count++;
            }
            if (count <= len) {
                rv = rv + c;
            }
        }

        return rv;
    }

    /**
     * 根据给定字符串返回Url编码(utf-8)字符串
     *
     * @param oStr 待转换字符串
     * @return 转换结果字符串
     */
    public static String getUrlEndoderStr(String oStr) {
        String rv = "";
        try {
            rv = java.net.URLEncoder.encode(oStr, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return rv;
    }

    /**
     * 根据给定字符串返回Url编码(utf-8)字符串
     *
     * @param oStr 待转换字符串
     * @return 转换结果字符串
     */
    public static String getUrlDecoderStr(String oStr) {
        String rv = "";
        try {
            rv = java.net.URLDecoder.decode(oStr, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return rv;
    }
    
    /**
    * 返回首字母
    * @param strChinese
    * @param bUpCase
    * @return
    */
   public static String getPYIndexStr(String strChinese, boolean bUpCase){
       try{
           StringBuffer buffer = new StringBuffer();
           byte b[] = strChinese.getBytes("GBK");//把中文转化成byte数组
           for(int i = 0; i < b.length; i++){
               if((b[i] & 255) > 128){
                   int char1 = b[i++] & 255;
                   char1 <<= 8;//左移运算符用“<<”表示，是将运算符左边的对象，向左移动运算符右边指定的位数，并且在低位补零。其实，向左移n位，就相当于乘上2的n次方
                   int chart = char1 + (b[i] & 255);
                   buffer.append(getPYIndexChar((char)chart, bUpCase));
                   continue;
               }
               char c = (char)b[i];
               if(!Character.isJavaIdentifierPart(c))//确定指定字符是否可以是 Java 标识符中首字符以外的部分。
                   c = 'A';
               buffer.append(c);
           }
           return buffer.toString();
       }catch(Exception e){
           System.out.println((new StringBuilder()).append("\u53D6\u4E2D\u6587\u62FC\u97F3\u6709\u9519").append(e.getMessage()).toString());
       }
       return null;
   }
   /**
    * 得到首字母
    * @param strChinese
    * @param bUpCase
    * @return
    */
   private static char getPYIndexChar(char strChinese, boolean bUpCase){
       int charGBK = strChinese;
       char result;
       if(charGBK >= 45217 && charGBK <= 45252)
           result = 'A';
       else
       if(charGBK >= 45253 && charGBK <= 45760)
           result = 'B';
       else
       if(charGBK >= 45761 && charGBK <= 46317)
           result = 'C';
       else
       if(charGBK >= 46318 && charGBK <= 46825)
           result = 'D';
       else
       if(charGBK >= 46826 && charGBK <= 47009)
           result = 'E';
       else
       if(charGBK >= 47010 && charGBK <= 47296)
           result = 'F';
       else
       if(charGBK >= 47297 && charGBK <= 47613)
           result = 'G';
       else
       if(charGBK >= 47614 && charGBK <= 48118)
           result = 'H';
       else
       if(charGBK >= 48119 && charGBK <= 49061)
           result = 'J';
       else
       if(charGBK >= 49062 && charGBK <= 49323)
           result = 'K';
       else
       if(charGBK >= 49324 && charGBK <= 49895)
           result = 'L';
       else
       if(charGBK >= 49896 && charGBK <= 50370)
           result = 'M';
       else
       if(charGBK >= 50371 && charGBK <= 50613)
           result = 'N';
       else
       if(charGBK >= 50614 && charGBK <= 50621)
           result = 'O';
       else
       if(charGBK >= 50622 && charGBK <= 50905)
           result = 'P';
       else
       if(charGBK >= 50906 && charGBK <= 51386)
           result = 'Q';
       else
       if(charGBK >= 51387 && charGBK <= 51445)
           result = 'R';
       else
       if(charGBK >= 51446 && charGBK <= 52217)
           result = 'S';
       else
       if(charGBK >= 52218 && charGBK <= 52697)
           result = 'T';
       else
       if(charGBK >= 52698 && charGBK <= 52979)
           result = 'W';
       else
       if(charGBK >= 52980 && charGBK <= 53688)
           result = 'X';
       else
       if(charGBK >= 53689 && charGBK <= 54480)
           result = 'Y';
       else
       if(charGBK >= 54481 && charGBK <= 55289)
           result = 'Z';
       else
           result = (char)(65 + (new Random()).nextInt(25));
       if(!bUpCase)
           result = Character.toLowerCase(result);
       return result;
   }

}
