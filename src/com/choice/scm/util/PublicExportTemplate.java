package com.choice.scm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.ForResourceFiles;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

/**
 * 导出模版公共的方法
 * 
 * @author Administrator
 * 
 */
@Component
public class PublicExportTemplate {
	/**
	 * 租户模版个性化文件夹
	 */
	public static final String	templateStandard	= "templateStandard/";
	/**
	 * 租户模版标准化文件夹
	 */
	public static final String	templateIndividualization	= "templateIndividualization/";
	public static final String	pathPrefix	= ForResourceFiles.getValByKey("config-oss.properties","oss.pathPrefix");
	/**
	 * bucket
	 */
	private static String		bucketName			= ForResourceFiles.getValByKey("config-oss.properties","oss.bucketName");

	



	/**
	 * 下载模板信息
	 * 适用于windows和linux
	 * @param response
	 * @param request
	 * @param templeteName
	 * @throws IOException
	 */
	public static void downloadTemplate(HttpServletResponse response,HttpServletRequest request,InputStream in,String realName) throws IOException {
		OutputStream outp = null;
		try {
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="+realName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	
	/**
	 * 判断自己上传的模版是否存在 返回下载下来的路径(后台下载使用)
	 * @param request
	 * @param Objectname
	 *        加上企业号的名字（不包括路径）
	 * @param ObjectnameOld
	 *        不加企业号的名字（不包括路径）
	 * @return
	 */
	public static String filePathUse(HttpServletRequest request,String Objectname,String ObjectnameOld){
		boolean flag = AliyunOssClient.headObject(templateIndividualization+Objectname);
		boolean state;
		if(flag == true){
			String ctxPath = request.getSession().getServletContext().getRealPath(File.separator)+ templateIndividualization + File.separator;
			File fil = new File(ctxPath);
		    state = fil.delete();
			if(state==true){
				System.out.println("删除文件夹成功");
			}else{
				System.out.println("删除文件夹失败");
			}
			if(!fil.exists()){
				fil.mkdirs();
			}
		    AliyunOssClient.downloadFile(bucketName,templateIndividualization+Objectname,ctxPath+Objectname);
		    return templateIndividualization+Objectname;//返回下载下来的文件的路径
		}else{
			String ctxPath = request.getSession().getServletContext().getRealPath(File.separator)+ templateStandard + File.separator;
			File fil = new File(ctxPath);
		    state = fil.delete();
			if(state==true){
				System.out.println("删除文件夹成功");
			}else{
				System.out.println("删除文件夹失败");
			}
			if(!fil.exists()){
				fil.mkdirs();
			}
			AliyunOssClient.downloadFile(bucketName,templateStandard+ObjectnameOld,ctxPath+ObjectnameOld);
			return templateStandard+ObjectnameOld;//返回下载下来的文件的路径
		}
	}
	
	
	/**
	 * 页面下载个性化版本判断存不存在
	 * @param request
	 * @param Objectname
	 *        加上企业号的名字（不包括路径）
	 * @param ObjectnameOld
	 *        不加企业号的名字（不包括路径）
	 * @return 
	 *        返回存在结果
	 * @throws IOException 
	 */
	public static String fileExitsOrNot(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException{
		    boolean flag = AliyunOssClient.headObject(templateIndividualization+realName);
		    String result="0";
		    if(flag == true){
		    	return result;	
		    }else{
		    	result = "1";
		    	return result;
		    }
	}
	
	/**
	 * 页面下载个性化版本使用
	 * @param request
	 * @param Objectname
	 *        加上企业号的名字（不包括路径）
	 * @param ObjectnameOld
	 *        不加企业号的名字（不包括路径）
	 * @return 
	 *        返回存在结果
	 * @throws IOException 
	 */
	public static void filePathUseIndividualization(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException{
	   		InputStream in  = AliyunOssClient.loadOssFile(templateIndividualization+realName);
	    	PublicExportTemplate.downloadTemplate(response, request,in,realName);
	}
	
	/**
	 * 页面下载标准版使用
	 * @param request
	 * @param Objectname
	 *        加上企业号的名字（不包括路径）
	 * @param ObjectnameOld
	 *        不加企业号的名字（不包括路径）
	 * @return 
	 *        返回下载需要的路径
	 * @throws IOException 
	 */
	public static void filePathUseStandard(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException{
			InputStream in  = AliyunOssClient.loadOssFile(templateStandard+realName);
	    	PublicExportTemplate.downloadTemplate(response, request,in,realName);	
	}
	
	
	/**
	 * 上传源文件 jrxml 适用于linux windows
	 * @param request
	 * @param response
	 * @param name
	 *        数据库中取的名字
	 * @param path
	 *        数据库中存的临时路径（考虑到系统不同路径不同， 改路径暂时放弃）
	 * @return
	 * @throws IOException
	 */
	public static String uploadTemplate(HttpServletRequest request,HttpServletResponse response,String name) throws IOException {
		Object pk_group = UserSpace.getSession().getAttribute("pk_group");
		String realFilePath = "";
		try {
			String ctxPath = request.getSession().getServletContext().getRealPath(File.separator)+ templateStandard + File.separator;
			/*String fileuploadPath = "D:"+path;*/
			File fil = new File(ctxPath);
	        boolean f = fil.delete();
	        if(f==true){
	        	System.out.println("上传使用的临时文件夹删除完毕");
	        }else{
	        	System.out.println("上传使用的临时文件夹删除失败");
	        }
			if(!fil.exists()){
				fil.mkdirs();
			}
			String realFileName = pk_group.toString()+name; // 新文件名字从库里取
			System.out.println(pk_group.toString()+"目录中的文件名为： "+ realFileName);
			realFilePath = ctxPath + realFileName;
			//上传
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file"); //在内存中提取文件
			
			CommonsMultipartFile cf = (CommonsMultipartFile)file;  
			DiskFileItem fi = (DiskFileItem) cf.getFileItem(); 
			File fileNew = fi.getStoreLocation();
						
			copy(fileNew,realFilePath);//复制jrxml
	        boolean fl = AliyunOssClient.headObject(templateIndividualization+realFileName);//判断上传之前存不存在
	        if(fl==true){
	        	AliyunOssClient.deleteFile(templateIndividualization+realFileName);
	        }
	        AliyunOssClient.uploadFile(templateIndividualization+realFileName, file); //oss上传jrxml
			doGet(request,response,realFilePath, ctxPath,realFileName);//编译 上传jasper
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	
	
	/**
	  * 编译jrxml并上传
	  * @param request
	  * @param response
	  * @param realFilePath
	  *          需要编译的文件所在路径（加上文件名）
	  * @param fileuploadPath
	  *          需要编译文件所在路径  （不加文件名）
	  * @param name
	  *          数据库中文件名
	  * @throws ServletException
	  * @throws IOException
	 * @throws InterruptedException 
	  */
	  public static void doGet(HttpServletRequest request, HttpServletResponse response,String realFilePath,String fileuploadPath,String name)  
	    throws ServletException, IOException, InterruptedException   
	    {   
		    try{
	    	    String nameNew = name.substring(0, name.indexOf("."));
	            String jrxmlDestSourcePath = fileuploadPath+nameNew+".jasper";  
	            JasperCompileManager.compileReportToFile(realFilePath,  
	            jrxmlDestSourcePath);   
		        boolean fl = AliyunOssClient.headObject(templateIndividualization+templateIndividualization+nameNew+".jasper");
		        if(fl==true){
		        	AliyunOssClient.deleteFile(templateIndividualization+templateIndividualization+nameNew+".jasper");
		        }
		        AliyunOssClient.uploadFile(templateIndividualization+nameNew+".jasper", new File(jrxmlDestSourcePath));//上传jasper

	        }   
	        catch (JRException e)   
	        {   
	            e.printStackTrace();  
	        }  
	     
	} 
	  
	  
	  
	  /**
	   * 复制jrxml
	   * @param file
	   *        需要复制的文件（内存中提取文件转file类型）
	   * @param toFileNew
	   *        复制后的文件名字以及路径
	   * @throws Exception
	   */
	  public static void copy(File file,String toFileNew) throws Exception {
			byte[] b = new byte[1024];
			int a;
			FileInputStream fis;
			FileOutputStream fos;
			File newFile = new File(toFileNew);
			fis = new FileInputStream(file);
			fos = new FileOutputStream(newFile);
			while ((a = fis.read(b)) != -1) {
				fos.write(b, 0, a);
			}
			fis.close();
			fos.close();
	  }	
	
}