package com.choice.scm.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import net.sf.json.JSONObject;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.springframework.stereotype.Component;

import com.choice.orientationSys.domain.DictColumns;
import com.choice.scm.domain.ReportObject;
@Component("scmExportExcel")
public class ExportExcel2 {

	WritableWorkbook workBook;
	/**
	 * Excel导出，固定列
	 * @param fileName
	 * @param request
	 * @param response
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @return
	 */
	public boolean creatWorkBook(String fileName,HttpServletRequest request,HttpServletResponse response,List<Map<String,Object>> data,String title,List<DictColumns> dictColumns){
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
			    fileName = URLEncoder.encode(fileName, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-disposition", "attachment; filename="  
	                + fileName + ".xls");
			os = response.getOutputStream();
			workBook = Workbook.createWorkbook(os);
			//create Sheet named "Sheet1". 0 means this is 1st page.
			WritableSheet sheet = workBook.createSheet(title, 0);
			//define cell column and row in Label Constructor, and cell content write title.   
            //cell is 1st-Column,1st-Row. value is title.
			Label label = new Label(0,0,title,titleStyle);
			//add defined cell above to sheet instance.   
            sheet.addCell(label);
            //设置表格表头
            for(int i = 0 ; i < dictColumns.size();i ++){
            	Label head = new Label(i,1,dictColumns.get(i).getZhColumnName(),contentStyle);
            	sheet.addCell(head);
            }
            sheet.mergeCells(0, 0, dictColumns.size()-1, 0);
            //遍历list填充表格内容
            for(int i = 0 ; i < data.size() ; i ++ ){
            	for(int j = 0 ; j < dictColumns.size() ; j ++){
            		DictColumns curCol = dictColumns.get(j);
            		String result = "";
            		String typeName = "";
            		Object curValue = data.get(i).get(curCol.getProperties().toUpperCase());
            		curValue = curValue == null ? "":curValue;
            		typeName = curValue.getClass().getName();
            		typeName = typeName.substring(typeName.lastIndexOf('.')+1);
            		if("int".equals(typeName) || "Integer".equals(typeName)){
    					result = curValue.toString();
    				}else if("Date".equals(typeName)){
    					DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
    					result = dft.format(curValue);
    				}else{
    					result = curValue.toString();
    				}
            		if("digital".equals(curCol.getColumnType())){
            			try{
            				jxl.write.Number number = new jxl.write.Number(j,2+i,Double.parseDouble(result),contentStyle);
            				sheet.addCell(number);
            			}catch(Exception e){
            				Label lab = new Label(j,2+i,result,contentStyle);
                    		sheet.addCell(lab);
            			}
            		}else{
            			Label lab = new Label(j,2+i,result,contentStyle);
                		sheet.addCell(lab);
            		}
            	}
            	
            }
            workBook.write();
            os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	/**
	 * Excel导出，不固定列（需要自定义filterData方法对查询结果进行格式化）
	 * @param request
	 * @param response
	 * @param data
	 * @param title
	 * @param Headers
	 */
	public void creatWorkBook(HttpServletRequest request,HttpServletResponse response,ReportObject<Map<String,Object>> reportObject,String title,String Headers){
		ByteArrayInputStream xmlStream = null;
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			response.setContentType("application/msexcel; charset=UTF-8");
			os = response.getOutputStream();
			workBook = Workbook.createWorkbook(os);
			//create Sheet named "Sheet1". 0 means this is 1st page.
			WritableSheet sheet = workBook.createSheet(title, 0);
			//define cell column and row in Label Constructor, and cell content write title.   
            //cell is 1st-Column,1st-Row. value is title.
			Label label = new Label(0,0,title,titleStyle);
			//add defined cell above to sheet instance.   
            sheet.addCell(label);
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
				title = URLEncoder.encode(title, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
				title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
				title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-disposition", "attachment; filename="  
	                + title + ".xls");
			//解析并生成表头
            Headers = "<div>" + Headers + "</div>";
			int x = 0;//记录单元格x坐标
			int y = 0;//记录单元格y坐标
			int tempX = 0;//记录单元格(有固定列时，记录非固定列(固定列多行循环时)x坐标起始位置)
			int maxspanY = 1;//记录表头所占行数
			boolean flag = true;
			Map<String,Integer> position = new HashMap<String,Integer>();//记录数据列位置坐标x
			xmlStream = new ByteArrayInputStream(Headers.getBytes());
			SAXBuilder reader = new SAXBuilder();
			Document doc = reader.build(xmlStream);
			Element root = doc.getRootElement();
			List<Element> tables = root.getChildren("table");
			for(Element table : tables){
				y = 0;
				tempX = x;
				maxspanY ++;
				List<Element> rows = table.getChildren().get(0).getChildren();
				for(Element row : rows){
					y ++;
					x = tempX;//记录冻结列位置
					List<Element> cols = row.getChildren();
					for(Element col : cols){
						int spanX = 0;
						int spanY = 0;
						Label head = new Label(x,y,col.getValue(),contentStyle);
						sheet.addCell(head);
						spanX = null == col.getAttributeValue("colspan") ? 1 : Integer.parseInt(col.getAttributeValue("colspan"));
						spanY = null == col.getAttributeValue("rowspan") ? 1 : Integer.parseInt(col.getAttributeValue("rowspan"));
						if(spanX == 1){
							position.put(col.getAttributeValue("field"), x);
						}
						sheet.mergeCells(x, y, x + spanX - 1, y + spanY - 1);
						x += spanX;
						if(flag && spanY > 1){
							tempX = x;
							flag = false;
						}
					}
				}
			}
			//合并单元格
			sheet.mergeCells(0, 0, x, 0);
			//遍历list生成excel数据
			List<Map<String,Object>> data = filterData(reportObject);
			for(int i = 0 ; i < data.size() ; i ++){
				int indexY = i + maxspanY;
				Map<String,Object> curRow = data.get(i);
				Set<String> cols =  position.keySet();
				for(String key : cols){
					Label content = new Label(position.get(key),indexY,null == curRow.get(key) ? "" : curRow.get(key).toString(),contentStyle);
					sheet.addCell(content);
				}
			}
			workBook.write();
            os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(null != xmlStream)
					workBook.close();
				if(null != xmlStream)
					xmlStream.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 导出excel表格（返回类型任意）
	 * @param request
	 * @param response
	 * @param data
	 * @param title
	 * @param dictColumns
	 * @return
	 */
	public<T> boolean creatWorkBook(HttpServletRequest request,HttpServletResponse response,List<T> data,String title,List<DictColumns> dictColumns,String headers){
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		Stack<Map<String,String>> fields = new Stack<Map<String,String>>();//存储field属性，按表格顺序
			try {
				titleStyle.setAlignment(Alignment.CENTRE);
				os = response.getOutputStream();
				workBook = Workbook.createWorkbook(os);
				//create Sheet named "Sheet1". 0 means this is 1st page.
				WritableSheet sheet = workBook.createSheet("Sheet1", 0);
				//define cell column and row in Label Constructor, and cell content write title.   
	            //cell is 1st-Column,1st-Row. value is title.
				Label label = new Label(0,0,title,titleStyle);
				//add defined cell above to sheet instance.   
	            sheet.addCell(label);
				response.setContentType("application/msexcel; charset=UTF-8");
				if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
				    //IE  
				    title = URLEncoder.encode(title, "UTF-8");              
				}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
				    //firefox  
				    title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
				}else{                
				    // other          
				    title = new String(title.getBytes("UTF-8"), "ISO8859-1");              
				}   
				response.setHeader("Content-disposition", "attachment; filename="  
		                + title + ".xls");
            //设置表格表头
			int headHeight = 0;
            if(null != headers && !"".equals(headers)){//如果headers不为空解析headers生成表头
            	headers = headers.toUpperCase();
        		System.out.println(headers);
            	String Headers = "<div>" + headers + "</div>";
            	Map<String,String> fieldMap = null;
        		ByteArrayInputStream xmlStream = null;
        		xmlStream = new ByteArrayInputStream(Headers.getBytes());
        		SAXBuilder reader = new SAXBuilder();
        		Document doc = reader.build(new InputStreamReader(xmlStream));//转换输入数据为Document对象
        		Element root = doc.getRootElement();//获取xml根标签
        		ArrayList<Stack<Element>> eleList = new ArrayList<Stack<Element>>();//存储所有td
        		List<Element> tables = root.getChildren("TABLE");//获取table标签
        		Element f = root.getChildren("FIELDMAP").size() > 0 ? root.getChildren("FIELDMAP").get(0) : null;
        		if(null != f && !"".equals(f.getValue()))
        			fieldMap = JSONObject.fromObject(f.getValue());
        		for(Element e : tables){//循环读取table，将td放入eleList
        			List<Element> rows = e.getChildren().get(0).getChildren();
        			for(int i = 0 ; i < rows.size() ; i ++){
        				if(eleList.size() < (i + 1)){
        					eleList.add(new Stack<Element>());
        				}
    					eleList.get(i).addAll(rows.get(i).getChildren());
        			}
        		}
        		//改
        		headHeight = eleList.size();
        		ArrayList<ArrayList<Integer>> cellSta = new ArrayList<ArrayList<Integer>>();//存储单元格index
        		for(int i = 0 ; i < headHeight ; i ++){
        			Collections.reverse(eleList.get(i));
        			cellSta.add(new ArrayList<Integer>());
        		}
        		Pattern p = Pattern.compile(".*DISPLAY\\s*:\\s*NONE\\s*;.*");//正则表达式匹配cell是否为隐藏
        		Pattern rightAlign = Pattern.compile(".*TEXT-ALIGN\\s*:\\s*RIGHT\\s*;.*");//正则表达式匹配是否为右对齐
        		Map<Integer,String> titles = new HashMap<Integer,String>();
        		int cell = 0;
        		int width = eleList.get(0).size();
        		for(int i = 0 ; i < width ; i ++){
        			Element top = eleList.get(0).pop();
        			String style = top.getAttributeValue("STYLE");
        			String divStyle = null == top.getChild("DIV") ? "": top.getChild("DIV").getAttributeValue("STYLE");
        			String value = null == top.getValue() ? "" : top.getValue();
        			String field = top.getAttributeValue("FIELD");
        			if(p.matcher(null == style ? "" : style).matches() || value.equals(""))continue;
        			int spanY = null == top.getAttributeValue("ROWSPAN") ? 1 : Integer.parseInt(top.getAttributeValue("ROWSPAN"));
        			int spanX = null == top.getAttributeValue("COLSPAN") ? 1 : Integer.parseInt(top.getAttributeValue("COLSPAN"));
        			for(int x = 0 ; x < spanX ; x ++){
        				for(int y = 0 ; y < spanY ; y ++)
        					cellSta.get(y).add(cell);
        			}
        			if(null != field && !"".equals(field)){
        				Map<String,String> local = new HashMap<String,String>();
        				if(rightAlign.matcher(divStyle).matches()){
        					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(field) : field);
        					local.put("type","digital");
        				}else{
        					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(field) : field);
        					local.put("type","text");
        				}
        				fields.push(local);
        			}
        			titles.put(cell, value);
        			cell ++;
        			for(int h = spanY ; h < headHeight;){
        				int innerDeep = 1;
        				for(int j = 0 ; j < spanX;){
        					Element e = eleList.get(h).pop();
                			String localdivStyle = null == e.getChild("DIV") ? "" : e.getChild("DIV").getAttributeValue("STYLE");
        					String fieldinner = e.getAttributeValue("FIELD");
        					int innerY = null == e.getAttributeValue("ROWSPAN") ? 1 : Integer.parseInt(e.getAttributeValue("ROWSPAN"));
        					int innerX = null == e.getAttributeValue("COLSPAN") ? 1 : Integer.parseInt(e.getAttributeValue("COLSPAN"));
        					for(int x = 0 ; x < innerX ; x ++){
                				for(int y = 0 ; y < innerY ; y ++)
                					cellSta.get(h+y).add(cell);
                			}
        					if(null != fieldinner && !"".equals(fieldinner)){
        						Map<String,String> local = new HashMap<String,String>();
        						if(rightAlign.matcher(localdivStyle).matches()){
                					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(fieldinner) : fieldinner);
                					local.put("type","digital");
                				}else{
                					local.put("key",null != fieldMap && fieldMap.size() > 0 ?  fieldMap.get(fieldinner) : fieldinner);
                					local.put("type","text");
                				}
        						fields.push(local);
        					}
        					titles.put(cell, e.getValue());
        					j += innerX;
        					innerDeep = innerY;
        					cell ++;
        				}
    					h += innerDeep;
        			}
        		}
        		for(int i = --cell ; i >= 0 ; i --){//循环数组，合并单元格
        			int Xstart = -1;
        			int Ystart = -1;
        			int Xend = -1;
        			int Yend = -1;
        			for(ArrayList<Integer> arr : cellSta){
        				int indexS = arr.indexOf(i);
        				int indexE = arr.lastIndexOf(i);
        				Xstart = indexS >= 0 ? indexS : Xstart;
        				Ystart = indexS >= 0 && Ystart == -1 ? cellSta.indexOf(arr) : Ystart;
        				Xend = indexE >= 0 ? indexE : Xend;
        				Yend = indexS >= 0 ? cellSta.indexOf(arr) : Yend;
        			}
        			Label head = new Label(Xstart,Ystart+1,titles.get(i),contentStyle);
        			sheet.addCell(head);
        			sheet.mergeCells(Xstart, Ystart+1, Xend, Yend+1);
        		}
        		sheet.mergeCells(0, 0, cellSta.get(0).size()-1, 0);
            }else if(null != dictColumns){//遍历dictColumns生成表头
            	headHeight = 1;
            	for(int i = 0 ; i < dictColumns.size();i ++){
                	Label head = new Label(i,1,dictColumns.get(i).getZhColumnName(),contentStyle);
                	sheet.addCell(head);
                	Map<String,String> local = new HashMap<String,String>();
                	local.put("key", dictColumns.get(i).getProperties());
                	local.put("type",dictColumns.get(i).getColumnType());
                	fields.push(local);
                }
            	sheet.mergeCells(0, 0, dictColumns.size()-1, 0);
            }
            //遍历list填充表格内容
            for(int i = 0 ; i < data.size() ; i ++ ){
            	for(int j = 0 ; j < fields.size() ; j ++){
            		String result = "";
            		String typeName = "";
            		Stack<String> propStack = new Stack<String>();
            		Object curValue = null;
            		if(null != headers){
            			propStack.push(fields.get(j).get("key"));
            		}else{
            			propStack.addAll(Arrays.asList(fields.get(j).get("key").split("\\.")));
            		}
            		T cur = data.get(i);
            		curValue = getValue(propStack,cur);
            		curValue = curValue == null ? "":curValue;
            		typeName = curValue.getClass().getName();
            		typeName = typeName.substring(typeName.lastIndexOf('.')+1);
            		if("int".equals(typeName) || "Integer".equals(typeName)){
    					result = curValue.toString();
    				}else if("Date".equals(typeName)){
    					DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
    					result = dft.format(curValue);
    				}else{
    					result = curValue.toString();
    				}
            		if("digital".equals(fields.get(j).get("type"))){
            			try{
            				jxl.write.Number number = new jxl.write.Number(j,i + headHeight + 1,Double.parseDouble(result),contentStyle);
            				sheet.addCell(number);
            			}catch(Exception e){
            				Label lab = new Label(j,i + headHeight + 1,result,contentStyle);
                    		sheet.addCell(lab);
            			}
            		}else{
            			Label lab = new Label(j,i + headHeight + 1,result,contentStyle);
                		sheet.addCell(lab);
            		}
            	}
            	
            }
            workBook.write();
            os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	//解析对象数据
	@SuppressWarnings("unchecked")
	private Object getValue(Stack<String> stack,Object obj) throws Exception{
		if(null == obj || stack.empty()) return null;
		String className = obj.getClass().getName();
		Object result = null;
		String cur = stack.pop();
		if(null == cur)return null;
		if(className.indexOf("java.util.HashMap") >= 0){
			result = ((Map<Object,Object>)obj).get(cur.toUpperCase());
		}else{
			Field curFeild = Class.forName(className.substring(0,className.indexOf('$'))).getDeclaredField(cur);
			Boolean accessible = curFeild.isAccessible();
    		if(!accessible)curFeild.setAccessible(true);
    		result = curFeild.get(obj);
    		curFeild.setAccessible(accessible);
		}
		if(stack.empty())return result;
		return getValue(stack,result);
	}
	/**
	 * 对传入data进行过滤整理
	 * @param data
	 * @return
	 */
	public List<Map<String,Object>> filterData(ReportObject<Map<String,Object>> reportObjcet){
		List<Map<String,Object>> list = reportObjcet.getRows();
		if(null != reportObjcet.getFooter())
			list.addAll(reportObjcet.getFooter());
		return list;
	}
}
