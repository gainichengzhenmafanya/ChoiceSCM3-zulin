package com.choice.scm.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 获取配置文件中的相关信息
 * @author qinyong
 *
 */
public class WebConfig {
	
	private static WebConfig webConfig;
	
	private WebConfig(){
	}
	
	public static WebConfig getInstance(){
		if(null==webConfig){
			webConfig = new WebConfig();
		}
		return webConfig;
	}
	
	public Properties getProperties(){
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("mail.properties");   //获取属性文件中设置的ip和端口 
		  Properties p = new Properties();    
		  try {    
		   p.load(inputStream);    
		  } catch (IOException e1) {    
		   e1.printStackTrace();    
		  }
		return p;
	}
	
	
	/** 
	 * @Method: getValue 
	 * @Description: 获取对应的值
	 * @param 
	 * @return 
	 * @throws
	 * @author qinyong
	 */
	public String getValue(String key){
		return getProperties().getProperty(key).trim();
	}
}
