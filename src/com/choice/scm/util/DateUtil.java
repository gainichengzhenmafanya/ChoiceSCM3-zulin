package com.choice.scm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.choice.framework.util.DateFormat;

/**
 * 日期处理工具类
 * @author 王超
 *
 */
public class DateUtil {
	
	public static SimpleDateFormat YYYY = new SimpleDateFormat("yyyy");
	public static SimpleDateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat YYYY_MM_DD_HH_mm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static SimpleDateFormat YYYY_MM_DD_HH_mm_ss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat YYYTMMDD = new SimpleDateFormat("yyyyMMdd");
	public static SimpleDateFormat YYYTMM = new SimpleDateFormat("yyyy-MM");
	public static SimpleDateFormat YYYYMM = new SimpleDateFormat("yyyyMM");
	public static SimpleDateFormat HH_mm_ss = new SimpleDateFormat("HH:mm:ss");
	public static SimpleDateFormat YYYY_MM = new SimpleDateFormat("yyyy-MM");
	public static SimpleDateFormat HH_mm = new SimpleDateFormat("HH:mm");
	public static SimpleDateFormat HH = new SimpleDateFormat("HH");
	public static SimpleDateFormat mm = new SimpleDateFormat("mm");
	public static SimpleDateFormat MM = new SimpleDateFormat("MM");
	public static SimpleDateFormat MMXDD = new SimpleDateFormat("MM/DD");
	public static SimpleDateFormat YYMMDD = new SimpleDateFormat("yyMMdd");

    /**
     * 获取两个日期
     * @author 王超
     * @param 2014年12月23日 下午3:48:12
     * @param start
     * @param end
     * @return
     * @throws ParseException
     */
    public static List<String> getDateList(String start,String end){
        List<String> ret = new ArrayList<String>();   
        SimpleDateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();   
        try {
			calendar.setTime(YYYY_MM_DD.parse(start));
			Date tmpDate = calendar.getTime();   
			long endTime = YYYY_MM_DD.parse(end).getTime();   
			while(tmpDate.before(YYYY_MM_DD.parse(end))||tmpDate.getTime() <= endTime){
				ret.add(YYYY_MM_DD.format(calendar.getTime()));   
				calendar.add(Calendar.DATE, 1);   
				tmpDate = calendar.getTime();   
			}          
		} catch (ParseException e) {
			e.printStackTrace();
		}   
        return ret;         
    } 
    
    /**
     * 计算time1的len天后的日期(len为负时，几天前)
     * @param len
     * @param time1
     * @return
     */
    public static String getLenTime(long len, String time1) {
		String returnT = "";
		long time2;
		try {
			Date date1 = YYYY_MM_DD.parse(time1);
			len = len * 1000 * 60 * 60 * 24;
			time2 = date1.getTime() + len;
			returnT = YYYY_MM_DD.format(time2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnT;
	}
    
    /**
     * 计算time1的len天后的日期(len为负时，几天前)
     * @param len
     * @param time1
     * @return
     */
    public static String getLenTimeEx(long len, String datetime1) {
		String returnT = "";
		long time2;
		try {
			Date date1 = YYYY_MM_DD_HH_mm_ss.parse(datetime1);
			len = len * 1000 * 60 * 60 * 24;
			time2 = date1.getTime() + len;
			returnT = YYYY_MM_DD_HH_mm_ss.format(time2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnT;
	}
    
    /**
     * 计算time1的len毫秒的时间
     * @param len
     * @param time1
     * @return YYYY_MM_DD_HH_mm_ss
     */
    public static Date getLenTime(long len, long time) {
		Date returnT = null;
		len += time;
		try {
			returnT = YYYY_MM_DD_HH_mm_ss.parse(YYYY_MM_DD_HH_mm_ss.format(len));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnT;
	}
    
    /**
     * 计算time1的len天之前的日期
     * @param len
     * @param time1
     * @return
     */
    public static String getLastTime(long len, String time1) {
		String returnT = "";
		long time2;
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = ft.parse(time1);
			len = len * 1000 * 60 * 60 * 24;
			time2 = date1.getTime() - len;
			returnT = ft.format(time2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnT;
	}
    
    /**
	 * 根据日期和转换格式得到字符串
	 * @param date 日期
	 * @param formatType 转换格式 例如  yyyy-MM-dd
	 * @return
	 */
	public static String getStringByDate(Date date,String formatType){
		return new SimpleDateFormat(formatType).format(null==date?new Date():date);
	}
	
	/**
	 * 获取当前日期是星期几
	 * 
	 * @param date
	 * @return 当前日期是星期几 星期日 0 星期一 1 星期二 2 星期三 3 星期四 4 星期五 5 星期六 6
	 */

	public static String getWeekOfDateToString(String date) {
		Calendar localTime = Calendar.getInstance();

		try {
			localTime.setTime(YYYY_MM_DD.parse(date));
		} catch (ParseException e) {
			return "----";
		}
		int[] weekDays = { 0, 1, 2, 3, 4, 5, 6 };
		int w = localTime.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		switch (weekDays[w]) {
		case 0:
			return "日";
		case 1:
			return "一";
		case 2:
			return "二";
		case 3:
			return "三";
		case 4:
			return "四";
		case 5:
			return "五";
		case 6:
			return "六";
		default:
			return "----";
		}
	}
	
	/**
	 * 描述：获取当前月份开始日期
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:01:42
	 * @param today
	 * @return
	 */
	public static String getMonthStart(String today) {
		Calendar localTime = Calendar.getInstance();
		try {
			localTime.setTime(YYYTMM.parse(today));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String strY = null;
		int x = localTime.get(Calendar.YEAR);
		int y = localTime.get(Calendar.MONTH) + 1;
		strY = y >= 10 ? String.valueOf(y) : ("0" + y);

		return x + "-" + strY + "-01";
	}
	
	/**
	 * 描述：获取当前月份结束时间
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:04:26
	 * @param today
	 * @return
	 */
	public static String getMonthEnd(String today) {
		Calendar localTime = Calendar.getInstance();
		
		try {
			localTime.setTime(YYYTMM.parse(today));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String strY = null;
		String strZ = null;
		boolean leap = false;
		int year = localTime.get(Calendar.YEAR);
		int month = localTime.get(Calendar.MONTH) + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8
				|| month == 10 || month == 12) {
			strZ = "31";
		}
		if (month == 4 || month == 6 || month == 9 || month == 11) {
			strZ = "30";
		}
		if (month == 2) {
			leap = leapYear(year);
			if (leap) {
				strZ = "29";
			} else {
				strZ = "28";
			}
		}
		strY = month >= 10 ? String.valueOf(month) : ("0" + month);
		return year + "-" + strY + "-" + strZ;
	}

    /**
     * 两个日期大小比较
     * @param time1 较早的日期
     * @param time2	较晚的日期
     * @return
     */
    public static boolean timeCompare(String time1, String time2) {
    	boolean returnSub = false;
		try {
			Date date1 = YYYY_MM_DD.parse(time1);
			Date date2 = YYYY_MM_DD.parse(time2);
			Calendar myCal1 = Calendar.getInstance();
			Calendar myCal2 = Calendar.getInstance();
			myCal1.setTime(date1);
			myCal2.setTime(date2);

			if (myCal1.after(myCal2)) {
				returnSub = false;
			} else {
				returnSub = true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnSub;
	}
    
    /**
     * 时间比较  HH_mm
     * @param time1 较早的日期
     * @param time2	较晚的日期
     * @return
     */
    public static boolean timeCompareDate(String time1, String time2) {
    	boolean returnSub = false;
    	try {
    		Date date1 = HH_mm.parse(time1);
    		Date date2 = HH_mm.parse(time2);
    		Calendar myCal1 = Calendar.getInstance();
    		Calendar myCal2 = Calendar.getInstance();
    		myCal1.setTime(date1);
    		myCal2.setTime(date2);
    		
    		if (myCal1.after(myCal2)) {
    			returnSub = false;
    		} else {
    			returnSub = true;
    		}
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
    	return returnSub;
    }
    
    /**
     * 时间比较  HH_mm
     * @param time1 较早的日期
     * @param time2	较晚的日期
     * @return
     */
    public static boolean timeCompareDateEx(String time1, String time2) {
    	boolean returnSub = false;
    	try {
    		Date date1 = YYYY_MM_DD_HH_mm_ss.parse(time1);
    		Date date2 = YYYY_MM_DD_HH_mm_ss.parse(time2);
    		Calendar myCal1 = Calendar.getInstance();
    		Calendar myCal2 = Calendar.getInstance();
    		myCal1.setTime(date1);
    		myCal2.setTime(date2);
    		
    		if (myCal1.after(myCal2)) {
    			returnSub = false;
    		} else {
    			returnSub = true;
    		}
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
    	return returnSub;
    }
	
	/**
	 * @param today yyyy-mm-dd 
	 * @return 月份最大天数
	 */
	public static String getMonthEndDay(String today) {
		
		Calendar localTime = Calendar.getInstance();
		
		try {
			localTime.setTime(YYYTMM.parse(today));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		
		String strZ = null;
		boolean leap = false;
		int year = localTime.get(Calendar.YEAR);
		int month = localTime.get(Calendar.MONTH) + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8
				|| month == 10 || month == 12) {
			strZ = "31";
		}
		if (month == 4 || month == 6 || month == 9 || month == 11) {
			strZ = "30";
		}
		if (month == 2) {
			leap = leapYear(year);
			if (leap) {
				strZ = "29";
			} else {
				strZ = "28";
			}
		}
		return  strZ;
	}
	
	/**
	 * 描述：获取当前系统日期
	 * @author 马振
	 * 创建时间：2015-4-10 下午3:39:01
	 * @return
	 */
	public static String getNowDate(){
		return YYYY_MM_DD.format(new Date());
	}
	
	/**
	 * 描述：获取当前系统时间
	 * @author 马振
	 * 创建时间：2015-4-11 下午10:39:01
	 * @return
	 */
	public static String getNowTime(){
		return HH_mm_ss.format(new Date());
	}
	
	/**
	 * 描述：获取当前系统年月日时分秒
	 * @author 马振
	 * 创建时间：2015-8-04 下午3:39:01
	 * @return
	 */
	public static String getNowDateTime(){
		return YYYY_MM_DD_HH_mm_ss.format(new Date());
	}
	
	/**
	 * 是否是闰年
	 * 
	 * @param year
	 * @return ???true ??false
	 * @author pure
	 */
	public static boolean leapYear(int year) {
		boolean leap;
		if (year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					leap = true;
				else
					leap = false;
			} else
				leap = true;
		} else
			leap = false;
		return leap;
	}
	
	/**
	 * 字符串类型的日期转为日期类型的
	 * @param date
	 * @return
	 */
	public static Date getDateByString(String date){
		Date dat = new Date();
		try {
			dat = YYYY_MM_DD.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dat;
	}
	
	/**
	 * 描述：获得去年同月
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:03:49
	 * @param date
	 * @return
	 */
	public static String getLastYearMonth(String date){
		Date dat = DateFormat.getDateByString(date, "yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.setTime(dat);
		int year = cal.get(Calendar.YEAR)-1;
		int month = cal.get(Calendar.MONTH)+1;
		return year + "-" + (month < 10 ? "0" + month : month);
	}
	
	/**
	 * 描述：获取去年同期
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:31:02
	 * @param date
	 * @return
	 */
	public static String getLastYearDate(String date){
		Date dat = DateFormat.getDateByString(date, "yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.setTime(dat);
		int year = cal.get(Calendar.YEAR) - 1;
		int month = cal.get(Calendar.MONTH) + 1;
		int days = cal.get(Calendar.DATE);
		return year + "-" + (month < 10 ? "0" + month : month) + "-" + (days < 10 ? "0" + days : days);
	}
	
	/**
	 * 描述：根据月份得出该月的天数
	 * @author 马振
	 * 创建时间：2015-4-23 上午10:15:53
	 * @param date
	 * @return
	 */
	public static int getLastDayOfMonth(String date){
		Date dat = DateFormat.getDateByString(date, "yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.setTime(dat);
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return maxDay;		
	}
	
	/**
	 * 描述：获取一个日期几个月后的日期
	 * @author 马振
	 * 创建时间：2015-5-19 下午4:42:01
	 * @param date
	 * @param len
	 * @return
	 */
	public static String getDateAfterMonth(String date, int len){
		Date dt = DateFormat.getDateByString(date, "yyyy-MM-dd");
		Calendar cd = Calendar.getInstance();
		cd.setTime(dt);
		cd.add(Calendar.MONTH, len);
		Date d = cd.getTime();
		
		return getStringByDate(d, "yyyy-MM-dd");
	}

	/**
	 * 描述：获取一个日期几年后的日期(几年前用负数)
	 * @author 马振
	 * 创建时间：2015-5-19 下午5:24:53
	 * @param date
	 * @param len
	 * @return
	 */
	public static String getDateAfterYear(String date, int len){
		Date dat = DateFormat.getDateByString(date, "yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.setTime(dat);
		cal.add(Calendar.YEAR, len);
		Date d = cal.getTime();
		
		return getStringByDate(d, "yyyy-MM-dd");
	}
	
	/**
	 * 获取2个日期段差异天数
	 * @author 文清泉
	 * @param 2015年6月5日 上午11:48:52
	 * @param bdate 开始日期
	 * @param edate 结束日期
	 * @return
	 * @throws ParseException
	 */
    public static Integer daysBetween(String bdate,String edate) throws ParseException{  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(YYYY_MM_DD.parse(bdate));    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(YYYY_MM_DD.parse(edate));    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
       return Integer.parseInt(String.valueOf(between_days));     
    }  
    
    /**
     * 描述：两个时间差
     * @author 马振
     * 创建时间：2015-6-17 下午5:16:47
     * @param bdate 开始时间
     * @param edate 结束时间
     * @return
     * @throws ParseException
     */
    public static double hourBetween(String bdate,String edate) throws ParseException{  
    	Date date1 = HH_mm.parse(bdate);
		Date date2 = HH_mm.parse(edate);
		long time1 = date1.getTime();
        long time2 = date2.getTime();
        double diff = Double.valueOf(String.valueOf(time2 - time1)) / (60 * 60 * 1000);
        return diff;   
    }
    
	/**
	 * 描述：获取一天内每隔interval分钟的时间段
	 * @author 马振
	 * 创建时间：2015-6-27 下午3:02:47
	 * @param interval
	 * @return
	 */
	public static List<String> getTimeList(String interval){
		List<String> rs = new ArrayList<String>();
		int len = (int)Math.ceil(24 * 60 / Integer.parseInt(interval));
		
		for(int i = 0; i < len; i++){
			String date = "";
			long longtime1 = 0;
			long longtime2 = 0;
			try {
				longtime1 = YYYY_MM_DD.parse(getNowDate()).getTime() + i * Integer.parseInt(interval) * 60 * 1000;
				longtime2 = YYYY_MM_DD.parse(getNowDate()).getTime() + (i + 1) * Integer.parseInt(interval) * 60 * 1000;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			if(i + 1 < len){
				date = HH_mm.format(new Date(longtime1)) + "-" + HH_mm.format(new Date(longtime2));
			}else{
				date = HH_mm.format(new Date(longtime1)) + "-24:00";
			}
			rs.add(date);
		}
		return rs;
	}
	
	/**
	 * 描述：获取两个时间段内，每个interval时间的时间串
	 * @author 马振
	 * 创建时间：2016-3-2 下午4:43:40
	 * @param time1
	 * @param time2
	 * @param interval
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getListBetweenInterval(String time1, String time2, String interval) throws ParseException{
		List<String> rs = new ArrayList<String>();
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = HH_mm.parse(time1);
			date2 = HH_mm.parse(time2);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
        int diff = Integer.valueOf(String.valueOf(date2.getTime() - date1.getTime())) / (60 * 60 * 1000);	//获取间隔的小时
		int len = (int)Math.ceil(diff * 60 / Integer.parseInt(interval));	//将间隔的小时按输入的分钟分割
		
		for(int i = 0; i < len; i++){
			String date = "";
			long longtime1 = 0;
			long longtime2 = 0;
			longtime1 = date1.getTime() + i * Integer.parseInt(interval) * 60 * 1000;
			longtime2 = date1.getTime() + (i + 1) * Integer.parseInt(interval) * 60 * 1000;
			
			if(i + 1 < len){
				date = HH_mm.format(new Date(longtime1)) + "-" + HH_mm.format(new Date(longtime2));
			}else{
				date = HH_mm.format(new Date(longtime1)) + "-" + time2;
			}
			rs.add(date);
		}
		return rs;
	}
	
	/**
	 * 描述：得到两个日期间隔的天数
	 * @author 马振
	 * 创建时间：2015-7-13 下午3:09:55
	 * @param beginDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static int getDaysBetween (String beginDate, String endDate) throws ParseException {
		Date bDate = YYYY_MM_DD.parse(beginDate);
		Date eDate = YYYY_MM_DD.parse(endDate);
		Calendar d1 = new GregorianCalendar();
		d1.setTime(bDate);
		Calendar d2 = new GregorianCalendar();
		d2.setTime(eDate);
        int days = d2.get(Calendar.DAY_OF_YEAR) - d1.get(Calendar.DAY_OF_YEAR);
        int y2 = d2.get(Calendar.YEAR);
        if (d1.get(Calendar.YEAR) != y2) {
            d1 = (Calendar) d1.clone();
            do {
                days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);//得到当年的实际天数
                d1.add(Calendar.YEAR, 1);
            } while (d1.get(Calendar.YEAR) != y2);
           
        }
        return days;
    }
	
    /**
     * 描述：获取当前日期上个月第一天和最后一天
     * @author 马振
     * 创建时间：2015-7-14 下午2:24:36
     * @param date
     * @return
     */
    public static Map<String, String> getFirstday_Lastday_Month(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        Date theDate = calendar.getTime();
        
        //上个月第一天
        GregorianCalendar gcLast = (GregorianCalendar) Calendar.getInstance();
        gcLast.setTime(theDate);
        gcLast.set(Calendar.DAY_OF_MONTH, 1);
        String day_first = YYYY_MM_DD.format(gcLast.getTime());
        StringBuffer str = new StringBuffer().append(day_first);
        day_first = str.toString();

        //上个月最后一天
        calendar.add(Calendar.MONTH, 1);    //加一个月
        calendar.set(Calendar.DATE, 1);        //设置为该月第一天
        calendar.add(Calendar.DATE, -1);    //再减一天即为上个月最后一天
        String day_last = YYYY_MM_DD.format(calendar.getTime());
        StringBuffer endStr = new StringBuffer().append(day_last);
        day_last = endStr.toString();

        Map<String, String> map = new HashMap<String, String>();
        map.put("first", day_first);
        map.put("last", day_last);
        return map;
    }
    
    /**
     * 描述：根据日期计算属于第几周
     * @author 马振
     * 创建时间：2016-2-22 上午11:25:21
     * @param date
     * @return
     */
    public static int getWeekOfYear(String date){
        try {
            Calendar cal = Calendar.getInstance();
            cal.setFirstDayOfWeek(Calendar.MONDAY); // 设置每周的第一天为星期一
            cal.setMinimalDaysInFirstWeek(1); 		// 设置每周最少为1天
            cal.setTime(YYYY_MM_DD.parse(date));
            return cal.get(Calendar.WEEK_OF_YEAR);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    /**
     * 描述：获取指定月份的所有日期
     * @author 马振
     * 创建时间：2016-3-3 下午4:30:22
     * @param month
     * @return
     */
    public static List<String> getAllDay(String month) throws ParseException {
    	List<String> dateList = new ArrayList<String>();
    	String endDay = getMonthEndDay(month);	//指定月份最大天数
    	
		for (int i = 1; i <= Integer.valueOf(endDay); i++) {
			if (i < 10) {
				dateList.add(month + "-0" + i); 
			} else {
				dateList.add(month + "-" + i); 
			}
		}
		
		return dateList;
	}
    
    public static void main(String[] args) {
    	try {
			System.out.println(getListBetweenInterval("19:01","19:34","1"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}