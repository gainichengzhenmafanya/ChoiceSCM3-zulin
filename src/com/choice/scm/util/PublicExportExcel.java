package com.choice.scm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 导出公共的方法
 * 
 * @author Administrator
 * 
 */
@Component
public class PublicExportExcel {
	//表格对象
	WritableWorkbook workBook;

	// 分隔符
	// 3个逗号
	public static final String SEPARATOR_DOTS = ",,,";
	// 3个冒号
	public static final String SEPARATOR_COLONS = ":::";
	// 空字符串
	public static final String EMPTY_STRING = "";
	// NULL字符串
	public static final String NULL_STRING = "null";
	// 保留两位小数
	public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

	/**
	 * 【【【导出方法1，不需要合计，左侧一列空列，上方一行空行】】】
	 * @param request
	 * @param response
	 * @param fileName
	 *            文件名
	 * @param title
	 *            表格标题名
	 * @param headers
	 * 			     表头对象集合，可以为HashMap对象的集合，每个HashMap对象存放一个key和value
	 *            可以导出最大支持两层表头
	 *            1.一层表头的情况，只设置key，value设置空字符串集合，key的格式为name:::properties
	 *            name为表头名，properties为要取数据集合元素中的属性
	 *            2.两层表头，key设置如1条
	 *            value设置为name1:::properties1,,,name2:::properties2以此类推
	 * @param datas
	 *            数据对象集合，自定义的对象，数据根据headers对应的propertis取值
	 * @version
	 * @Time 20160316
	 * @return
	 */
	public boolean creatWorkBook(HttpServletRequest request, HttpServletResponse response, String fileName, String title, List<?> headers, List<?> datas) {
		return creatWorkBook(request, response, fileName, title, headers, datas, null);
	}

	/**
	 * 【【【导出方法2，需要合计，左侧一列空列，上方一行空行】】】
	 * @param request
	 * @param response
	 * @param fileName
	 *            文件名
	 * @param title
	 *            表格标题名
	 * @param headers
	 * 			     表头对象集合，可以为HashMap对象的集合，每个HashMap对象存放一个key和value
	 *            可以导出最大支持两层表头
	 *            1.一层表头的情况，只设置key，value设置空字符串集合，key的格式为name:::properties
	 *            name为表头名，properties为要取数据集合元素中的属性
	 *            2.两层表头，key设置如1条
	 *            value设置为name1:::properties1,,,name2:::properties2以此类推
	 * @param datas
	 *            数据对象集合，自定义的对象，数据根据headers对应的propertis取值
	 * @param sumColumns
	 *            需要合计的列数组
	 * @version
	 * @Time 20160316
	 * @return
	 */
	public boolean creatWorkBook(HttpServletRequest request, HttpServletResponse response, String fileName, String title, List<?> headers, List<?> datas, int[] sumColumns) {

		/**
		 * 需要判断数据 标题是否为空
		 */
		if (headers == null || headers.size() == 0) {
			return false;
		}
		if (datas == null || datas.size() == 0) {
			return false;
		}

		// 设置编码
		OutputStream os = null;
		try {
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
				// IE
				fileName = URLEncoder.encode(fileName, "UTF-8");
			} else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
				// FIREFOX
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			} else {
				// OTHER
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
			os = response.getOutputStream();
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			return false;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return false;
		}

		// 如果传的表头对象为HashMap,则到Map方法去执行
		String className = headers.get(0).getClass().getName();
		if (className.indexOf("java.util.HashMap") >= 0) {
			return creatWorkBookMaps(os, title, headers, datas, sumColumns);
		}

		// 屏蔽掉headers集合为自定义对象的情况
		if (className.indexOf("java.util.HashMap") < 0) {
			return false;
		}

		// 屏蔽掉headers不是HashMap集合的情况
		return false;
	}
	
	
	/**
	 * 【【【导出方法3，无表头，无左侧空列，无上方空行】】】
	 * @param request
	 * @param response
	 * @param fileName
	 *            文件名
	 * @param title
	 *            表格标题名
	 * @param headers
	 * 			     表头对象集合，可以为HashMap对象的集合，每个HashMap对象存放一个key和value
	 *            可以导出最大支持两层表头
	 *            1.一层表头的情况，只设置key，value设置空字符串集合，key的格式为name:::properties
	 *            name为表头名，properties为要取数据集合元素中的属性
	 *            2.两层表头，key设置如1条
	 *            value设置为name1:::properties1,,,name2:::properties2以此类推
	 * @param datas
	 *            数据对象集合，自定义的对象，数据根据headers对应的propertis取值
	 * @param sumColumns
	 *            需要合计的列数组
	 * @version
	 * @Time 20160316
	 * @return
	 */
	public boolean creatWorkBookNoTitle(HttpServletRequest request, HttpServletResponse response, String fileName, String title, List<?> headers, List<?> datas, int[] sumColumns) {

		// 设置编码
		OutputStream os = null;
		try {
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
				// IE
				fileName = URLEncoder.encode(fileName, "UTF-8");
			} else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
				// FIREFOX
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			} else {
				// OTHER
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
			os = response.getOutputStream();
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			return false;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return false;
		}

		// 如果传的表头对象为HashMap,则到Map方法去执行
		String className = headers.get(0).getClass().getName();
		if (className.indexOf("java.util.HashMap") >= 0) {
			return creatWorkBookMaps2(os, title, headers, datas, sumColumns);
		}

		// 屏蔽掉headers集合为自定义对象的情况
		if (className.indexOf("java.util.HashMap") < 0) {
			return false;
		}

		// 屏蔽掉headers不是HashMap集合的情况
		return false;
	}
	

	/**
	 * 
	 * 【【【实际的导出方法】】】
	 * 有表头，及左侧一列空列，上方一行空行
	 * @param os
	 * @param title
	 * @param headers
	 * @param datas
	 * @param sumColumns
	 * @return
	 */
	private boolean creatWorkBookMaps(OutputStream os, String title, List<?> headers, List<?> datas, int[] sumColumns) {

		/**
		 * 需要判断数据 标题是否为空
		 */
		if (headers == null || headers.size() == 0) {
			return false;
		}
		if (datas == null || datas.size() == 0) {
			return false;
		}

		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont tableHeadFont = new WritableFont(WritableFont.TIMES, 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableCellFormat tableHeadStyle = new WritableCellFormat(tableHeadFont);// 表头格式
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式

		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		WritableCellFormat intStyle = new WritableCellFormat(contentFont, new NumberFormat("0"));// 设定整形数字单元格格式

		try {
			workBook = Workbook.createWorkbook(os);
			titleStyle.setAlignment(Alignment.CENTRE);

			// 标题背景
			titleStyle.setBackground(Colour.GREY_25_PERCENT);

			// 设置边框
			titleStyle.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);

			// 表头文字居中
			tableHeadStyle.setAlignment(Alignment.CENTRE);
			tableHeadStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			tableHeadStyle.setBackground(Colour.GREY_25_PERCENT);

			// 设置边框
			tableHeadStyle.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);

			WritableSheet sheet = workBook.createSheet("Sheet1", 0);

			// 标题集合长度
			int headersLen = headers.size();

			// 存放属性列表
			List<String> propList = new ArrayList<String>();

			// 判断有几行
			// 现在支持2行
			int numLine = 1;
			for (int i = 0; i < headersLen; i++) {
				Map<String, String> map = (Map<String, String>) headers.get(i);
				Iterator<String> ite = map.keySet().iterator();
				String keyStr = EMPTY_STRING;
				String valueStr = EMPTY_STRING;
				// 只得到第一个key和value
				if (ite.hasNext()) {
					keyStr = ite.next();
					valueStr = map.get(keyStr);
				}

				// key存放只有一行的表头信息
				// value如果有则是存放第二层节点的信息
				if (null != valueStr && !EMPTY_STRING.equals(valueStr) && !NULL_STRING.equals(valueStr) && valueStr.indexOf(SEPARATOR_DOTS) >= 0) {
					numLine = 2;
					break;
				}
			}

			// 记录到第几列了，因为有合并列的
			int endcolumn = 0;

			// 只有一层表头
			if (numLine == 1) {
				// 设置表格表头
				for (int i = 0; i < headersLen; i++) {
					Map<String, String> map = (Map<String, String>) headers.get(i);
					Iterator<String> ite = map.keySet().iterator();
					String keyStr = EMPTY_STRING;
					// 只得到第一个key
					if (ite.hasNext()) {
						keyStr = ite.next();
					}

					// 拆分得到表头名 和 属性
					if (null != keyStr && !EMPTY_STRING.equals(keyStr) && !NULL_STRING.equals(keyStr)) {
						String[] keyStrs = keyStr.split(SEPARATOR_COLONS);
						String theName = EMPTY_STRING;
						String theProp = EMPTY_STRING;
						if (keyStrs != null) {
							if (keyStrs.length == 2 && !"".equals(keyStrs[0]) && !"".equals(keyStrs[1]) && null != keyStrs[0] && null != keyStrs[1]) {
								theName = keyStrs[0];
								theProp = keyStrs[1];
							} else {
								theProp = NULL_STRING;
							}
						} else {
							theProp = NULL_STRING;
						}

						Label head = new Label(i + 1, 1, theName, tableHeadStyle);
						sheet.addCell(head);
						// 把属性放到集合中去
						propList.add(theProp);
					}
					// update by jin shuai at 20160317
					else {
						Label head = new Label(i + 1, 1, EMPTY_STRING, tableHeadStyle);
						sheet.addCell(head);
						// 把属性放到集合中去
						propList.add(NULL_STRING);
					}

					// 记录列
					endcolumn = i + 1;
				}
			}
			// 两层表头
			else if (numLine == 2) {
				// 设置表格表头
				for (int i = 0; i < headersLen; i++) {

					Map<String, String> map = (Map<String, String>) headers.get(i);
					Iterator<String> ite = map.keySet().iterator();
					String keyStr = EMPTY_STRING;
					String valueStr = EMPTY_STRING;
					// 只得到第一个key和value
					if (ite.hasNext()) {
						keyStr = ite.next();
						valueStr = map.get(keyStr);
					}

					String[] keyStrs = keyStr.split(SEPARATOR_COLONS);
					String theName = EMPTY_STRING;
					String theProp = EMPTY_STRING;
					if (keyStrs != null) {
						if (keyStrs.length == 2 && !"".equals(keyStrs[0]) && !"".equals(keyStrs[1]) && null != keyStrs[0] && null != keyStrs[1]) {
							theName = keyStrs[0];
							theProp = keyStrs[1];
						} else {
							theProp = NULL_STRING;
						}
					} else {
						theProp = NULL_STRING;
					}

					// 判断是否有第二层(有第二层)
					if (null != valueStr && !EMPTY_STRING.equals(valueStr) && !NULL_STRING.equals(valueStr) && valueStr.indexOf(SEPARATOR_DOTS) >= 0) {
						// 拆分
						String[] valueStrs = valueStr.split(SEPARATOR_DOTS);

						// 得到有几个子单元格
						int subheaderslen = valueStrs.length;

						// 父单元格合并并填充内容
						sheet.mergeCells(endcolumn + 1, 1, endcolumn + subheaderslen, 1);
						Label head = new Label(endcolumn + 1, 1, theName, tableHeadStyle);
						sheet.addCell(head);

						// 子单元格
						for (int sub = 0; sub < subheaderslen; sub++) {
							// 拆分子单元格名称和属性
							String[] subStrs = valueStrs[sub].split(SEPARATOR_COLONS);
							String subName = EMPTY_STRING;
							String subProp = EMPTY_STRING;
							if (subStrs != null) {
								if (subStrs.length == 2) {
									subName = subStrs[0];
									subProp = subStrs[1];
								} else {
									subProp = NULL_STRING;
								}
							} else {
								subProp = NULL_STRING;
							}

							Label sublab = new Label(endcolumn + 1, 2, subName, tableHeadStyle);
							sheet.addCell(sublab);
							// 记载列
							endcolumn++;

							// 属性放到集合中
							propList.add(subProp);
						}
					} else {
						// 合并
						sheet.mergeCells(endcolumn + 1, 1, endcolumn + 1, 2);
						Label head = new Label(endcolumn + 1, 1, theName, tableHeadStyle);
						sheet.addCell(head);

						// 把属性放到集合中去
						propList.add(theProp);

						// 记录列
						endcolumn++;
					}
				}
			}

			// 知道有多少列之后才添加标题
			sheet.mergeCells(1, 0, endcolumn, 0);
			Label label = new Label(1, 0, title, titleStyle);
			sheet.addCell(label);

			Map<Integer, Double> sumMap = new HashMap<Integer, Double>();
			// 是否有合计
			boolean hasSum = false;
			// 判断是否有合计
			if (sumColumns != null && sumColumns.length > 0) {
				// 无合计
				hasSum = true;
				int len = sumColumns.length;
				int propListLen = propList.size();
				// 把合计值放到map中
				for (int i = 0; i < len; i++) {
					int column = sumColumns[i];
					if (column > 0 && column <= propListLen) {
						sumMap.put(column, 0d);
					}
				}
			}

			// 遍历内容集合 填充表格
			if (datas != null && propList != null && propList.size() > 0 && datas.size() > 0) {
				// 数据集合长度
				int datasLen = datas.size();

				// 遍历集合
				// 分两种情况 一。数据对象为用户自定义对象
				// 二。数据对象为HashMap对象
				// 自定义对象
				String className = datas.get(0).getClass().getName();
				if (className.indexOf("java.util.HashMap") < 0) {
					for (int i = 0; i < datasLen; i++) {
						// 得到数据对象
						Object obj = datas.get(i);
						Class<?> clas = obj.getClass();
						// 属性的个数
						int numProp = propList.size();
						// 遍历属性 根据属性取得值
						for (int propi = 0; propi < numProp; propi++) {
							String propstr = propList.get(propi);
							// 根据异常判断属性是否存在
							Field field = null;
							try {
								field = clas.getDeclaredField(propstr);
								field.setAccessible(true);

								// 根据类名判断类型
								String fieldClassName = field.getClass().getName();
								String leiMing = fieldClassName.substring(fieldClassName.lastIndexOf(".") + 1);

								// 得到属性值
								Object fieldobj = field.get(obj);
								String fieldstr = fieldobj == null ? EMPTY_STRING : fieldobj.toString().trim();

								// 有合计
								if (hasSum) {
									if (sumMap.containsKey(propi + 1)) {
										Double oldvalue = sumMap.get(propi + 1);
										Double addvalue = 0d;
										try {
											addvalue = Double.parseDouble(fieldstr);
										} catch (Exception e) {
											System.out.println("转换数字出错");
											addvalue = 0d;
										}
										sumMap.put(propi + 1, oldvalue + addvalue);
									}
								}

								// 如果为数字列
								if (isNumberClass(leiMing)) {
									if (isIntNumber(leiMing)) {
										int result = 0;
										try {
											result = Integer.parseInt(fieldstr);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi + 1, numLine + 1 + i, result, intStyle);
										sheet.addCell(number);
									} else {
										double result = 0;
										try {
											result = Double.parseDouble(fieldstr);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi + 1, numLine + 1 + i, result, doubleStyle);
										sheet.addCell(number);
									}
								} else {
									Label textLabel = new Label(propi + 1, numLine + 1 + i, fieldstr, contentStyle);
									sheet.addCell(textLabel);
								}
							} catch (Exception e) {
								System.out.println("属性不存在");
								// 异常不影响导出
								Label textLabel = new Label(propi + 1, numLine + 1 + i, EMPTY_STRING, contentStyle);
								sheet.addCell(textLabel);
							}
						}
					}
				}
				// HashMap对象
				else {
					// 属性的个数
					int numProp = propList.size();
					for (int i = 0; i < datasLen; i++) {
						// 得到数据对象
						Map<String, Object> dataMap = (Map<String, Object>) datas.get(i);
						// 遍历属性 根据属性取得值
						for (int propi = 0; propi < numProp; propi++) {
							String propstr = propList.get(propi);
							try {
								// 得到属性值
								Object valueObj = dataMap.get(propstr);
								// 根据类名判断类型
								String fieldClassName = valueObj.getClass().getName();
								String leiMing = fieldClassName.substring(fieldClassName.lastIndexOf(".") + 1);

								String datMapValue = valueObj == null ? "" : valueObj.toString();
								// 有合计
								if (hasSum) {
									if (sumMap.containsKey(propi + 1)) {
										Double oldvalue = sumMap.get(propi + 1);
										Double addvalue = 0d;
										try {
											addvalue = Double.parseDouble(datMapValue);
										} catch (Exception e) {
											System.out.println("转换数字出错");
											addvalue = 0d;
										}
										sumMap.put(propi + 1, oldvalue + addvalue);
									}
								}

								// 如果为数字列
								if (isNumberClass(leiMing)) {
									if (isIntNumber(leiMing)) {
										int result = 0;
										try {
											result = Integer.parseInt(datMapValue);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi + 1, numLine + 1 + i, result, intStyle);
										sheet.addCell(number);
									} else {
										double result = 0;
										try {
											result = Double.parseDouble(datMapValue);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi + 1, numLine + 1 + i, result, doubleStyle);
										sheet.addCell(number);
									}
								} else {
									Label textLabel = new Label(propi + 1, numLine + 1 + i, datMapValue, contentStyle);
									sheet.addCell(textLabel);
								}
							} catch (Exception e) {
								System.out.println(propstr + ":属性不存在");
								// 异常不影响导出
								Label textLabel = new Label(propi + 1, numLine + 1 + i, EMPTY_STRING, contentStyle);
								sheet.addCell(textLabel);
							}
						}
					}
				}

				// 行数
				numLine = numLine + datasLen;
				numLine += 2;

				// 有合计
				if (hasSum) {
					Iterator<Integer> iter = sumMap.keySet().iterator();
					// 合计字符串
					Label hjLabel = new Label(0, numLine, "合计:", contentStyle);
					sheet.addCell(hjLabel);
					while (iter.hasNext()) {
						int key = iter.next();
						// 得到值
						Double value = sumMap.get(key);
						// 添加合计
						jxl.write.Number number = new jxl.write.Number(key, numLine, Double.parseDouble(DECIMAL_FORMAT.format(value)), doubleStyle);
						sheet.addCell(number);
					}
				}
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	
	/**
	 * 
	 * 【【【实际的导出方法2】】】
	 * 无表头，无左侧一列空列，无上方一行空行
	 * @param os
	 * @param title
	 * @param headers
	 * @param datas
	 * @param sumColumns
	 * @return
	 */
	private boolean creatWorkBookMaps2(OutputStream os, String title, List<?> headers, List<?> datas, int[] sumColumns) {

		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont tableHeadFont = new WritableFont(WritableFont.TIMES, 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableCellFormat tableHeadStyle = new WritableCellFormat(tableHeadFont);// 表头格式
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式

		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		WritableCellFormat intStyle = new WritableCellFormat(contentFont, new NumberFormat("0"));// 设定整形数字单元格格式

		try {
			workBook = Workbook.createWorkbook(os);
			titleStyle.setAlignment(Alignment.CENTRE);

			// 标题背景
			titleStyle.setBackground(Colour.GREY_25_PERCENT);

			// 设置边框
			titleStyle.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);

			// 表头文字居中
			tableHeadStyle.setAlignment(Alignment.CENTRE);
			tableHeadStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			tableHeadStyle.setBackground(Colour.GREY_25_PERCENT);

			// 设置边框
			tableHeadStyle.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			tableHeadStyle.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);

			WritableSheet sheet = workBook.createSheet("Sheet1", 0);

			// 标题集合长度
			int headersLen = headers.size();

			// 存放属性列表
			List<String> propList = new ArrayList<String>();

			// 判断有几行
			// 现在支持2行
			int numLine = 1;
			for (int i = 0; i < headersLen; i++) {
				Map<String, String> map = (Map<String, String>) headers.get(i);
				Iterator<String> ite = map.keySet().iterator();
				String keyStr = EMPTY_STRING;
				String valueStr = EMPTY_STRING;
				// 只得到第一个key和value
				if (ite.hasNext()) {
					keyStr = ite.next();
					valueStr = map.get(keyStr);
				}

				// key存放只有一行的表头信息
				// value如果有则是存放第二层节点的信息
				if (null != valueStr && !EMPTY_STRING.equals(valueStr) && !NULL_STRING.equals(valueStr) && valueStr.indexOf(SEPARATOR_DOTS) >= 0) {
					numLine = 2;
					break;
				}
			}

			// 记录到第几列了，因为有合并列的
			int endcolumn = 0;

			// 只有一层表头
			if (numLine == 1) {
				// 设置表格表头
				for (int i = 0; i < headersLen; i++) {
					Map<String, String> map = (Map<String, String>) headers.get(i);
					Iterator<String> ite = map.keySet().iterator();
					String keyStr = EMPTY_STRING;
					// 只得到第一个key
					if (ite.hasNext()) {
						keyStr = ite.next();
					}

					// 拆分得到表头名 和 属性
					if (null != keyStr && !EMPTY_STRING.equals(keyStr) && !NULL_STRING.equals(keyStr)) {
						String[] keyStrs = keyStr.split(SEPARATOR_COLONS);
						String theName = EMPTY_STRING;
						String theProp = EMPTY_STRING;
						if (keyStrs != null) {
							if (keyStrs.length == 2 && !"".equals(keyStrs[0]) && !"".equals(keyStrs[1]) && null != keyStrs[0] && null != keyStrs[1]) {
								theName = keyStrs[0];
								theProp = keyStrs[1];
							} else {
								theProp = NULL_STRING;
							}
						} else {
							theProp = NULL_STRING;
						}

						Label head = new Label(i, 0, theName, tableHeadStyle);
						sheet.addCell(head);
						// 把属性放到集合中去
						propList.add(theProp);
					}
					// update by jin shuai at 20160317
					else {
						Label head = new Label(i, 0, EMPTY_STRING, tableHeadStyle);
						sheet.addCell(head);
						// 把属性放到集合中去
						propList.add(NULL_STRING);
					}

					// 记录列
					endcolumn = i;
				}
			}
			// 两层表头
			else if (numLine == 2) {
				// 设置表格表头
				for (int i = 0; i < headersLen; i++) {

					Map<String, String> map = (Map<String, String>) headers.get(i);
					Iterator<String> ite = map.keySet().iterator();
					String keyStr = EMPTY_STRING;
					String valueStr = EMPTY_STRING;
					// 只得到第一个key和value
					if (ite.hasNext()) {
						keyStr = ite.next();
						valueStr = map.get(keyStr);
					}

					String[] keyStrs = keyStr.split(SEPARATOR_COLONS);
					String theName = EMPTY_STRING;
					String theProp = EMPTY_STRING;
					if (keyStrs != null) {
						if (keyStrs.length == 2 && !"".equals(keyStrs[0]) && !"".equals(keyStrs[1]) && null != keyStrs[0] && null != keyStrs[1]) {
							theName = keyStrs[0];
							theProp = keyStrs[1];
						} else {
							theProp = NULL_STRING;
						}
					} else {
						theProp = NULL_STRING;
					}

					// 判断是否有第二层(有第二层)
					if (null != valueStr && !EMPTY_STRING.equals(valueStr) && !NULL_STRING.equals(valueStr) && valueStr.indexOf(SEPARATOR_DOTS) >= 0) {
						// 拆分
						String[] valueStrs = valueStr.split(SEPARATOR_DOTS);

						// 得到有几个子单元格
						int subheaderslen = valueStrs.length;

						// 父单元格合并并填充内容
						sheet.mergeCells(endcolumn, 0, endcolumn + subheaderslen, 1);
						Label head = new Label(endcolumn, 0, theName, tableHeadStyle);
						sheet.addCell(head);

						// 子单元格
						for (int sub = 0; sub < subheaderslen; sub++) {
							// 拆分子单元格名称和属性
							String[] subStrs = valueStrs[sub].split(SEPARATOR_COLONS);
							String subName = EMPTY_STRING;
							String subProp = EMPTY_STRING;
							if (subStrs != null) {
								if (subStrs.length == 2) {
									subName = subStrs[0];
									subProp = subStrs[1];
								} else {
									subProp = NULL_STRING;
								}
							} else {
								subProp = NULL_STRING;
							}

							Label sublab = new Label(endcolumn, 1, subName, tableHeadStyle);
							sheet.addCell(sublab);
							// 记载列
							endcolumn++;

							// 属性放到集合中
							propList.add(subProp);
						}
					} else {
						// 合并
						sheet.mergeCells(endcolumn, 0, endcolumn + 1, 2);
						Label head = new Label(endcolumn, 0, theName, tableHeadStyle);
						sheet.addCell(head);

						// 把属性放到集合中去
						propList.add(theProp);

						// 记录列
						endcolumn++;
					}
				}
			}

			// 知道有多少列之后才添加标题
			//sheet.mergeCells(1, 0, endcolumn, 0);
			//Label label = new Label(1, 0, title, titleStyle);
			//sheet.addCell(label);

			Map<Integer, Double> sumMap = new HashMap<Integer, Double>();
			// 是否有合计
			boolean hasSum = false;
			// 判断是否有合计
			if (sumColumns != null && sumColumns.length > 0) {
				// 无合计
				hasSum = true;
				int len = sumColumns.length;
				int propListLen = propList.size();
				// 把合计值放到map中
				for (int i = 0; i < len; i++) {
					int column = sumColumns[i];
					if (column > 0 && column <= propListLen) {
						sumMap.put(column, 0d);
					}
				}
			}

			// 遍历内容集合 填充表格
			if (datas != null && propList != null && propList.size() > 0 && datas.size() > 0) {
				// 数据集合长度
				int datasLen = datas.size();

				// 遍历集合
				// 分两种情况 一。数据对象为用户自定义对象
				// 二。数据对象为HashMap对象
				// 自定义对象
				String className = datas.get(0).getClass().getName();
				if (className.indexOf("java.util.HashMap") < 0) {
					for (int i = 0; i < datasLen; i++) {
						// 得到数据对象
						Object obj = datas.get(i);
						Class<?> clas = obj.getClass();
						// 属性的个数
						int numProp = propList.size();
						// 遍历属性 根据属性取得值
						for (int propi = 0; propi < numProp; propi++) {
							String propstr = propList.get(propi);
							// 根据异常判断属性是否存在
							Field field = null;
							try {
								field = clas.getDeclaredField(propstr);
								field.setAccessible(true);

								// 根据类名判断类型
								String fieldClassName = field.getClass().getName();
								String leiMing = fieldClassName.substring(fieldClassName.lastIndexOf(".") + 1);

								// 得到属性值
								Object fieldobj = field.get(obj);
								String fieldstr = fieldobj == null ? EMPTY_STRING : fieldobj.toString().trim();

								// 有合计
								if (hasSum) {
									if (sumMap.containsKey(propi + 1)) {
										Double oldvalue = sumMap.get(propi + 1);
										Double addvalue = 0d;
										try {
											addvalue = Double.parseDouble(fieldstr);
										} catch (Exception e) {
											System.out.println("转换数字出错");
											addvalue = 0d;
										}
										sumMap.put(propi + 1, oldvalue + addvalue);
									}
								}

								// 如果为数字列
								if (isNumberClass(leiMing)) {
									if (isIntNumber(leiMing)) {
										int result = 0;
										try {
											result = Integer.parseInt(fieldstr);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi, numLine + i, result, intStyle);
										sheet.addCell(number);
									} else {
										double result = 0;
										try {
											result = Double.parseDouble(fieldstr);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi, numLine + i, result, doubleStyle);
										sheet.addCell(number);
									}
								} else {
									Label textLabel = new Label(propi, numLine + i, fieldstr, contentStyle);
									sheet.addCell(textLabel);
								}
							} catch (Exception e) {
								System.out.println("属性不存在");
								// 异常不影响导出
								Label textLabel = new Label(propi, numLine + i, EMPTY_STRING, contentStyle);
								sheet.addCell(textLabel);
							}
						}
					}
				}
				// HashMap对象
				else {
					// 属性的个数
					int numProp = propList.size();
					for (int i = 0; i < datasLen; i++) {
						// 得到数据对象
						Map<String, Object> dataMap = (Map<String, Object>) datas.get(i);
						// 遍历属性 根据属性取得值
						for (int propi = 0; propi < numProp; propi++) {
							String propstr = propList.get(propi);
							try {
								// 得到属性值
								Object valueObj = dataMap.get(propstr);
								// 根据类名判断类型
								String fieldClassName = valueObj.getClass().getName();
								String leiMing = fieldClassName.substring(fieldClassName.lastIndexOf(".") + 1);

								String datMapValue = valueObj == null ? "" : valueObj.toString();
								// 有合计
								if (hasSum) {
									if (sumMap.containsKey(propi + 1)) {
										Double oldvalue = sumMap.get(propi + 1);
										Double addvalue = 0d;
										try {
											addvalue = Double.parseDouble(datMapValue);
										} catch (Exception e) {
											System.out.println("转换数字出错");
											addvalue = 0d;
										}
										sumMap.put(propi + 1, oldvalue + addvalue);
									}
								}

								// 如果为数字列
								if (isNumberClass(leiMing)) {
									if (isIntNumber(leiMing)) {
										int result = 0;
										try {
											result = Integer.parseInt(datMapValue);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi, numLine + i, result, intStyle);
										sheet.addCell(number);
									} else {
										double result = 0;
										try {
											result = Double.parseDouble(datMapValue);
										} catch (Exception e) {
										}
										jxl.write.Number number = new jxl.write.Number(propi, numLine + i, result, doubleStyle);
										sheet.addCell(number);
									}
								} else {
									Label textLabel = new Label(propi, numLine + i, datMapValue, contentStyle);
									sheet.addCell(textLabel);
								}
							} catch (Exception e) {
								System.out.println(propstr + ":属性不存在");
								// 异常不影响导出
								Label textLabel = new Label(propi, numLine + i, EMPTY_STRING, contentStyle);
								sheet.addCell(textLabel);
							}
						}
					}
				}

				// 行数
				numLine = numLine + datasLen;
				numLine += 2;

				// 有合计
				if (hasSum) {
					Iterator<Integer> iter = sumMap.keySet().iterator();
					// 合计字符串
					Label hjLabel = new Label(0, numLine, "合计:", contentStyle);
					sheet.addCell(hjLabel);
					while (iter.hasNext()) {
						int key = iter.next();
						// 得到值
						Double value = sumMap.get(key);
						// 添加合计
						jxl.write.Number number = new jxl.write.Number(key, numLine, Double.parseDouble(DECIMAL_FORMAT.format(value)), doubleStyle);
						sheet.addCell(number);
					}
				}
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/**
	 * 判断类名是不是数字类
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isNumberClass(String className) {
		return className.equals("Short") || className.equals("Integer") || className.equals("Long") || className.equals("Float") || className.equals("Double") || className.equals("BigInteger") || className.equals("BigDecimal") || className.equals("short") || className.equals("int") || className.equals("long") || className.equals("float") || className.equals("double");
	}

	/**
	 * 判断是不是整形
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isIntNumber(String className) {
		return className.equals("Short") || className.equals("Integer") || className.equals("Long") || className.equals("BigInteger") || className.equals("short") || className.equals("int") || className.equals("long");
	}

	/**
	 * 判断是不是浮点数
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isDoubleNumber(String className) {
		return className.equals("Float") || className.equals("Double") || className.equals("BigDecimal") || className.equals("float") || className.equals("double");
	}
	
	/**
	 * 下载模板信息
	 * 适用于windows和linux
	 * @param response
	 * @param request
	 * @param templeteName
	 * @throws IOException
	 */
	public static void downloadTemplate(HttpServletResponse response,HttpServletRequest request,String templeteName) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String fileName = templeteName;
			if(templeteName!=null){
				if(!templeteName.endsWith(".xls")){
					fileName = templeteName + ".xls";
				}
			}
			String ctxPath = request.getSession().getServletContext().getRealPath(File.separator) + File.separator + "template" + File.separator;
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="+fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	/**
	 * 将文件上传到temp文件夹下，并返回文件路径
	 * 适用于windows和linux
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public static String uploadToTemp(HttpServletRequest request,HttpServletResponse response) throws IOException {
		Object pk_group = request.getSession().getAttribute("pk_group");
//		System.out.println("当前企业号为： " + pk_group);
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file"); //在内存中提取文件
			String realFileName = pk_group + "_" + file.getOriginalFilename(); // 为上传的文件名增加企业号 以区别不同企业的文件 by lbh 2017-07-07
			System.out.println("temp目录中的文件名为： "+ realFileName);
			//temp文件夹实际路径
			String ctxPath = request.getSession().getServletContext().getRealPath(File.separator)+ "temp" + File.separator;
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			//上传
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	
	
}