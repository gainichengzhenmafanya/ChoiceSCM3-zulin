package com.choice.scm.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.choice.assistant.domain.supplier.Suitstore;
/**
 * 物资价格
 * @author yp
 */

public class Spprice {

	private String acct;		//帐套号
	private String sp_code;		//物资编码
	private Date bdat;			//开始日期
	private Date edat;			//结束日期
	private String madet;		//填制时间
	private BigDecimal price;		//单价
	private BigDecimal priceold;		//上期价格
	private String deliver;		//供应商
	private String memo;		//备注
	private String sta;			//是否有效。有效直接写有效
	private BigDecimal rec;		//流水号
	private String emp;			//添加的员工
	private String area;		//对应的分店
	private String checby;		//审核人
	private String chect;		//审核时间
	private String downsta;		//是否下载
	private String downsta1;	//是否再次下载，暂不用
	
	private String sp_name;	//物资名称
	private String sp_desc;	//规格
	private String sp_mark;	//品牌
	private String sp_addr;	//产地
	private String unit;	//单位
	private String deliverDes;		//供应商名称
	private String typ;		//查询时接收参数
	private String areaDes;
	private Integer deltaT;//剩余到期天数
	private Integer warnDays;//供应商设置到期提醒天数 wangjie 2014年11月25日 15:18:26
	
	private List<Spprice> sppriceList;
	private List<Suitstore> suitstoreList;
	private List<Positn> positnList;

	//采购合约主键
	private String pk_purcontract;
	//报价批次
	private Integer iquotebatch;
	//报价主键
	private String pk_spprice;
	
	private Double tax = 0.0;   			//税率
	private String taxdes;  			//税率名称
	
	public List<Positn> getPositnList() {
		return positnList;
	}
	public void setPositnList(List<Positn> positnList) {
		this.positnList = positnList;
	}
	public String getPk_spprice() {
		return pk_spprice;
	}
	public void setPk_spprice(String pk_spprice) {
		this.pk_spprice = pk_spprice;
	}
	public Integer getIquotebatch() {
		return iquotebatch;
	}
	public void setIquotebatch(Integer iquotebatch) {
		this.iquotebatch = iquotebatch;
	}
	public String getPk_purcontract() {
		return pk_purcontract;
	}
	public void setPk_purcontract(String pk_purcontract) {
		this.pk_purcontract = pk_purcontract;
	}
	public Spprice(){
		this.downsta = "Y";
		this.downsta1 = "Y";
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPriceold() {
		return priceold;
	}
	public void setPriceold(BigDecimal priceold) {
		this.priceold = priceold;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public BigDecimal getRec() {
		return rec;
	}
	public void setRec(BigDecimal rec) {
		this.rec = rec;
	}
	public String getEmp() {
		return emp;
	}
	public void setEmp(String emp) {
		this.emp = emp;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getChect() {
		return chect;
	}
	public void setChect(String chect) {
		this.chect = chect;
	}
	public String getDownsta() {
		return downsta;
	}
	public void setDownsta(String downsta) {
		this.downsta = downsta;
	}
	public String getDownsta1() {
		return downsta1;
	}
	public void setDownsta1(String downsta1) {
		this.downsta1 = downsta1;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getSp_mark() {
		return sp_mark;
	}
	public void setSp_mark(String sp_mark) {
		this.sp_mark = sp_mark;
	}
	public String getSp_addr() {
		return sp_addr;
	}
	public void setSp_addr(String sp_addr) {
		this.sp_addr = sp_addr;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getAreaDes() {
		return areaDes;
	}
	public void setAreaDes(String areaDes) {
		this.areaDes = areaDes;
	}
	public List<Spprice> getSppriceList() {
		return sppriceList;
	}
	public void setSppriceList(List<Spprice> sppriceList) {
		this.sppriceList = sppriceList;
	}
	public Integer getWarnDays() {
		return warnDays;
	}
	public void setWarnDays(Integer warnDays) {
		this.warnDays = warnDays;
	}
	public Integer getDeltaT() {
		return deltaT;
	}
	public void setDeltaT(Integer deltaT) {
		this.deltaT = deltaT;
	}
	public List<Suitstore> getSuitstoreList() {
		return suitstoreList;
	}
	public void setSuitstoreList(List<Suitstore> suitstoreList) {
		this.suitstoreList = suitstoreList;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	
	
}
