package com.choice.scm.domain;
/**
 * 工作台管理
 * @author lq
 *
 */
public class WorkBench {

	private String id;//编号
	private String accountid;//账号编码
	private String menu;//菜单名称
	private String tagpage;	//常用操作模块id


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public String getTagpage() {
		return tagpage;
	}
	public void setTagpage(String tagpage) {
		this.tagpage = tagpage;
	}
	
}
