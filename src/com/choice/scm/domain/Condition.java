package com.choice.scm.domain;

import java.util.Date;

import com.choice.orientationSys.util.Page;
import com.choice.scm.util.NoUse;
/**
 * 查询条件
 * @author yp
 *
 */
public class Condition {
	
	private Date bdat;						//开始日期
	private Date edat;						//结束日期
	private String area;					//地区
	private String mode;					//经营模式
	private int sumMod;						//汇总方式
	private int detailMod;					//明细方式
	private String areaSum;					//地区汇总(y/n)
	private String modeSum;					//模式汇总(y/n)
	@NoUse
	private int page;						//请求页
	@NoUse
	private int rows;						//每页行数
	private String sort;					//排序字段
	private String order;					//排序方式
	@NoUse
	private Page pager;						//分页对象
	private String firmid;					//分店id
	private String firmdes;					//分店名称
	private String firmidck;				//参考分店id
	private String pubGrpid;				//类别id
	private String pubGrpdes;				//类别名称
	private String customCol;				//动态列名（决策分析）
	private String serial;					//流水号
	private String status;					//状态
	private String comp;					//仅招待单
	private String tmptsfr;					//仅挂账单
	private String roomtyp;					//台别
	private String tbldes;					//台号
	private String lastemp;					//结账员
	private String foliono;					//账单号
	private String codeid;					//单店账单分析账单明细id
	private String reportName;				//报表名
	private String sft;						//班次
	private String HolidayType;				//假日类型
	private String payment;					//结算方式
	private String startTime;				//开始时间
	private String endTime;					//结束时间
	private String init;					//缩写码
	private int queryMod;					//查询方式
	private String itcode;					//
	private String item;					//菜品code
	private String packageId;				//套餐ID
	private String packageTyp;				//套餐类别
	private String sptyp;					//二级类别
	private String subarea;					//门店类别
	private int costCategory;				//菜品种类（全部菜品，核心菜品，明星菜品，自选菜品）
	private int years;						//年数
	private String typoth;					//辅助类别
	private int refundTyp;	
	private String dept;					//部门
	private String sqlData;					//查询语句
	private String sqlWhen;					//查询条件
	private String sqlStr1;					//查询语句
	private String sqlStr2;					//查询语句
	private String sqlGroup;				//查询语句
	private String sqlDataGroup;			//查询语句
	private String sqlAll;					//查询语句
	private String inputby;					//点菜员
	private String pgrptyp;					//菜品大类
	private int days;						//天数
	
	public static final int BYFIRM = 0;			//按分店
	public static final int BYFOOD = 1;			//按菜品
	public static final int BYCATEGORY = 2;		//按类别
	public static final int BYDAY = 3;			//按天
	public static final int BYMONTH = 4;		//按月
	public static final int BYSEASON = 5;		//按季度
	public static final int BYYEAR = 6;			//按年
	public static final int BYWEEK = 7;			//按周
	public static final int BYWEEKDAY = 8;		//按周几
	public static final int BYHALFYEAR = 9;		//按半年
	public static final int BYAREA = 10;		//按地区
	public static final int BYMOD = 11;			//按经营模式
	public static final int DETAILBYDAY = 12;	//按天明细
	public static final int BYDIFF = 13;		//同期对比
	
	public static final int ALL = 0;			//全部
	public static final int PACKAGE = 1;		//套餐菜
	public static final int NORMAL = 2;			//零点菜
	public static final int PAXAVG = 3;			//人均消费
	public static final int INCOMESUM = 4;		//营收统计
	//消退菜类型
	public static final int RETURN = 0;			//退菜
	public static final int CANCEL = 1;			//取消
	public static final int PRESENT = 2;		//送菜
	public static final int REFUND = 3;			//免项
	
	public String getHolidayType() {
		return HolidayType;
	}

	public void setHolidayType(String holidayType) {
		HolidayType = holidayType;
	}

	public int getCostCategory() {
		return costCategory;
	}

	public void setCostCategory(int costCategory) {
		this.costCategory = costCategory;
	}

	public Condition(){
		pager = new Page();
	}
	
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public int getSumMod() {
		return sumMod;
	}
	public void setSumMod(int sumMod) {
		this.sumMod = sumMod;
	}
	public int getDetailMod() {
		return detailMod;
	}
	public void setDetailMod(int detailMod) {
		this.detailMod = detailMod;
	}
	public String getAreaSum() {
		return areaSum;
	}
	public void setAreaSum(String areaSum) {
		this.areaSum = areaSum;
	}
	public String getModeSum() {
		return modeSum;
	}
	public void setModeSum(String modeSum) {
		this.modeSum = modeSum;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
		pager.setNowPage(page);
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
		pager.setPageSize(rows);
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Page getPager() {
		return pager;
	}
	public void setPager(Page pager) {
		this.pager = pager;
	}

	public static int getByfirm() {
		return BYFIRM;
	}

	public static int getByfood() {
		return BYFOOD;
	}

	public static int getBycategory() {
		return BYCATEGORY;
	}

	public static int getByday() {
		return BYDAY;
	}

	public static int getBymonth() {
		return BYMONTH;
	}

	public static int getByseason() {
		return BYSEASON;
	}

	public static int getByyear() {
		return BYYEAR;
	}

	public static int getByweek() {
		return BYWEEK;
	}

	public static int getByweekday() {
		return BYWEEKDAY;
	}

	public static int getByhalfyear() {
		return BYHALFYEAR;
	}

	public static int getByarea() {
		return BYAREA;
	}

	public static int getBymod() {
		return BYMOD;
	}

	public String getFirmid() {
		return firmid;
	}

	public void setFirmid(String firmid) {
		this.firmid = firmid;
	}

	public String getFirmdes() {
		return firmdes;
	}

	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}

	public String getCustomCol() {
		return customCol;
	}

	public void setCustomCol(String customCol) {
		this.customCol = customCol;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getPubGrpid() {
		return pubGrpid;
	}

	public void setPubGrpid(String pubGrpid) {
		this.pubGrpid = pubGrpid;
	}

	public String getPubGrpdes() {
		return pubGrpdes;
	}

	public void setPubGrpdes(String pubGrpdes) {
		this.pubGrpdes = pubGrpdes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComp() {
		return comp;
	}

	public void setComp(String comp) {
		this.comp = comp;
	}

	public String getTmptsfr() {
		return tmptsfr;
	}

	public void setTmptsfr(String tmptsfr) {
		this.tmptsfr = tmptsfr;
	}

	public String getRoomtyp() {
		return roomtyp;
	}

	public void setRoomtyp(String roomtyp) {
		this.roomtyp = roomtyp;
	}

	public String getTbldes() {
		return tbldes;
	}

	public void setTbldes(String tbldes) {
		this.tbldes = tbldes;
	}

	public String getLastemp() {
		return lastemp;
	}

	public void setLastemp(String lastemp) {
		this.lastemp = lastemp;
	}

	public String getFoliono() {
		return foliono;
	}

	public void setFoliono(String foliono) {
		this.foliono = foliono;
	}

	public String getCodeid() {
		return codeid;
	}

	public void setCodeid(String codeid) {
		this.codeid = codeid;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getSft() {
		return sft;
	}

	public void setSft(String sft) {
		this.sft = sft;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public int getQueryMod() {
		return queryMod;
	}

	public void setQueryMod(int queryMod) {
		this.queryMod = queryMod;
	}

	public static int getAll() {
		return ALL;
	}

	public static int getPackage() {
		return PACKAGE;
	}

	public static int getNormal() {
		return NORMAL;
	}

	public String getItcode() {
		return itcode;
	}

	public void setItcode(String itcode) {
		this.itcode = itcode;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public static int getDetailbyday() {
		return DETAILBYDAY;
	}

	public static int getBydiff() {
		return BYDIFF;
	}

	public static int getPaxavg() {
		return PAXAVG;
	}

	public static int getIncomesum() {
		return INCOMESUM;
	}

	public int getRefundTyp() {
		return refundTyp;
	}

	public void setRefundTyp(int refundTyp) {
		this.refundTyp = refundTyp;
	}

	public static int getReturn() {
		return RETURN;
	}

	public static int getCancel() {
		return CANCEL;
	}

	public static int getPresent() {
		return PRESENT;
	}

	public static int getRefund() {
		return REFUND;
	}

	public String getFirmidck() {
		return firmidck;
	}

	public void setFirmidck(String firmidck) {
		this.firmidck = firmidck;
	}
	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getSqlData() {
		return sqlData;
	}

	public void setSqlData(String sqlData) {
		this.sqlData = sqlData;
	}

	public String getSqlWhen() {
		return sqlWhen;
	}

	public void setSqlWhen(String sqlWhen) {
		this.sqlWhen = sqlWhen;
	}

	public String getSqlStr1() {
		return sqlStr1;
	}

	public void setSqlStr1(String sqlStr1) {
		this.sqlStr1 = sqlStr1;
	}

	public String getSqlStr2() {
		return sqlStr2;
	}

	public void setSqlStr2(String sqlStr2) {
		this.sqlStr2 = sqlStr2;
	}

	public String getSqlGroup() {
		return sqlGroup;
	}

	public void setSqlGroup(String sqlGroup) {
		this.sqlGroup = sqlGroup;
	}

	public String getSqlDataGroup() {
		return sqlDataGroup;
	}

	public void setSqlDataGroup(String sqlDataGroup) {
		this.sqlDataGroup = sqlDataGroup;
	}

	public String getSptyp() {
		return sptyp;
	}

	public void setSptyp(String sptyp) {
		this.sptyp = sptyp;
	}

	public String getSqlAll() {
		return sqlAll;
	}

	public void setSqlAll(String sqlAll) {
		this.sqlAll = sqlAll;
	}

	public String getTypoth() {
		return typoth;
	}

	public void setTypoth(String typoth) {
		this.typoth = typoth;
	}

	public String getPackageTyp() {
		return packageTyp;
	}

	public void setPackageTyp(String packageTyp) {
		this.packageTyp = packageTyp;
	}
	
	public String getSubarea() {
		return subarea;
	}

	public void setSubarea(String subarea) {
		this.subarea = subarea;
	}

	public String getInputby() {
		return inputby;
	}

	public void setInputby(String inputby) {
		this.inputby = inputby;
	}

	public String getPgrptyp() {
		return pgrptyp;
	}

	public void setPgrptyp(String pgrptyp) {
		this.pgrptyp = pgrptyp;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}
}
