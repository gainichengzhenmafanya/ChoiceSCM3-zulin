package com.choice.scm.domain;

/**
 * 半成品成本卡
 * @author 
 *
 */
public class Spcodeexm {

	private String acct;   //帐套编码
	private String item;   //半成品编号
	private String sp_code;//物资编码
	private String sp_name;//物资编码
	private double cnt;	   //数量1
	private double excnt;   //标准数量
	private String status; //暂不用
	private double exrate;  //转化率
	private double cnt1;	   //数量1
	private double cnt2;	   //数量2
	private String unit; //暂不用
	private String is_supply_x;  // 是否存在虚拟物料
	private String monthh;
	private String yearr;
	
	public String getIs_supply_x() {
		return is_supply_x;
	}
	public void setIs_supply_x(String is_supply_x) {
		this.is_supply_x = is_supply_x;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getAcct() {
		return acct;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getExcnt() {
		return excnt;
	}
	public void setExcnt(double excnt) {
		this.excnt = excnt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getExrate() {
		return exrate;
	}
	public void setExrate(double exrate) {
		this.exrate = exrate;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public String getMonthh() {
		return monthh;
	}
	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	
	/**
	 * 构造方法
	 */
	
}
