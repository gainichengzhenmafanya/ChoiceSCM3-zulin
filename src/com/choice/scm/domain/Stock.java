package com.choice.scm.domain;

import java.util.Date;
 /**
  * 存货盘点表 
  * @author lehui
  *
  */
public class Stock {
	
	 private Date   dat;//日期
	 private String week;// 星期
	 private String cnttri;//期初
	 private String cntIn;//入库
	 private String cntOut;//结存
	 private String cntbla;//实际用量
	 private String cntcost;//理论用量
	 
	 private Date startDat;//时间
	 private Date endDat;//结束时间
	 
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getCnttri() {
		return cnttri;
	}
	public void setCnttri(String cnttri) {
		this.cnttri = cnttri;
	}
	public String getCntIn() {
		return cntIn;
	}
	public void setCntIn(String cntIn) {
		this.cntIn = cntIn;
	}
	public String getCntOut() {
		return cntOut;
	}
	public void setCntOut(String cntOut) {
		this.cntOut = cntOut;
	}
	public String getCntbla() {
		return cntbla;
	}
	public void setCntbla(String cntbla) {
		this.cntbla = cntbla;
	}
	public String getCntcost() {
		return cntcost;
	}
	public void setCntcost(String cntcost) {
		this.cntcost = cntcost;
	}
	public Date getStartDat() {
		return startDat;
	}
	public void setStartDat(Date startDat) {
		this.startDat = startDat;
	}
	public Date getEndDat() {
		return endDat;
	}
	public void setEndDat(Date endDat) {
		this.endDat = endDat;
	}
}
