package com.choice.scm.domain;

import java.util.Date;
import java.util.List;
/**
 * 报货分拨和验收实体类
 * @author csb
 *
 */
public class Dis {
	@Override
	public String toString() {
		return "Dis [acct=" + acct + ", bdat=" + bdat + ", edat=" + edat + ", maded=" + maded + ", id=" + id + ", chkstoNo=" + chkstoNo + ", chkstoNos=" + chkstoNos + ", amount=" + amount + ", amount1=" + amount1 + ", inout=" + inout + ", ind=" + ind + ", ind1=" + ind1 + ", hoped=" + hoped + ", memo=" + memo + ", memo1=" + memo1 + ", amountsto=" + amountsto + ", amountin=" + amountin + ", amount1in=" + amount1in + ", prncnt=" + prncnt + ", price=" + price + ", chkin=" + chkin + ", chkinout=" + chkinout + ", chkout=" + chkout + ", bak2=" + bak2 + ", pricein=" + pricein + ", pricesale=" + pricesale + ", totalAmt=" + totalAmt + ", yhrate=" + yhrate + ", chrate=" + chrate + ", sp_id=" + sp_id + ", pound=" + pound + ", dued=" + dued + ", sp_init=" + sp_init + ", unit=" + unit + ", unit1=" + unit1 + ", sp_code=" + sp_code + ", sp_name=" + sp_name + ", sp_desc=" + sp_desc + ", cnt=" + cnt + ", unitper=" + unitper + ", chk1tim=" + chk1tim + ", chk1emp=" + chk1emp + ", chk2tim=" + chk2tim
				+ ", chk2emp=" + chk2emp + ", puprordertim=" + puprordertim + ", puprorderemp=" + puprorderemp + ", ispuprorder=" + ispuprorder + ", ynArrival=" + ynArrival + ", yndo=" + yndo + ", chk1=" + chk1 + ", chksend=" + chksend + ", sta=" + sta + ", ex1=" + ex1 + ", chectim=" + chectim + ", bchectim=" + bchectim + ", echectim=" + echectim + ", lineCode=" + lineCode + ", lineDes=" + lineDes + ", psarea=" + psarea + ", psorder=" + psorder + ", typCode=" + typCode + ", typDes=" + typDes + ", firmCode=" + firmCode + ", firmDes=" + firmDes + ", firmDes1=" + firmDes1 + ", deliverCode=" + deliverCode + ", deliverDes=" + deliverDes + ", positnCode=" + positnCode + ", positnDes=" + positnDes + ", inCondition=" + inCondition + ", scm=" + scm + ", wzqx=" + wzqx + ", wzzhqx=" + wzzhqx + ", sp_type=" + sp_type + ", fxqx=" + fxqx + ", gysqx=" + gysqx + ", cwqx=" + cwqx + ", accountId=" + accountId + ", deliveryn=" + deliveryn + ", fbcxqx=" + fbcxqx + ", amount1sto=" + amount1sto
				+ ", unit3=" + unit3 + ", unitper3=" + unitper3 + ", sp_per1=" + sp_per1 + ", pcno=" + pcno + ", positn1=" + positn1 + ", ynbatch=" + ynbatch + ", ynprice=" + ynprice + ", chkyh=" + chkyh + ", accprate=" + accprate + ", accpratemin=" + accpratemin + ", empfirm=" + empfirm + ", isDept=" + isDept + ", deptList=" + deptList + ", ynkc=" + ynkc + ", amountyh=" + amountyh + ", vouno=" + vouno + ", chkinno=" + chkinno + ", invouno=" + invouno + ", typdes=" + typdes + ", grpdes=" + grpdes + ", grptypdes=" + grptypdes + ", chk1condition=" + chk1condition + ", pageSize=" + pageSize + ", orderBy=" + orderBy + ", danjuTyp=" + danjuTyp + ", checkinSelect=" + checkinSelect + ", startNum=" + startNum + ", endNum=" + endNum + "]";
	}
	private String acct;//帐套信息 wjf
	private Date bdat;//开始日期
	private Date edat;//结束日期
    private Date maded;//制单日期
    private String id;//序号
    private Integer chkstoNo;//单号
    private String chkstoNos;//单号
    private Double amount;//申购数量
    private Double amount1;//参考申购数量
    private String inout;//单据方向
    private Date ind;//到货日期
    private String ind1;//未知到货
    private String hoped;//期待到货日期
    private String memo;//子表备注
    private String memo1;//是否不合格
    private Double amountsto;//amountsto到货数量 报货分拨中修改的  amountin是验货数量 门店验货修改的 20151214wjf
    private Double amountin;//到货数量
    private Double amount1in;//参考到货数量
	private Integer prncnt;//打印数量
	private Double price;//申购单表价格
	private String chkin;//是否入库
	private String chkinout;//是否直发
	private String chkout;//是否出库
	private String bak2;//报货验收物资已至（1--代表已至，空代表未至）
	private Double pricein;//申购单入库价格
	private Double pricesale;//申购单售价
	private Double totalAmt;//申购单表金额
	private String yhrate;//入库验货比
	private String chrate;//出库验货比

	private int sp_id;//暂无用
	private Double pound;//暂无用
	private Date dued;//暂无用
	
	private String sp_codes;
	public String getSp_codes() {
		return sp_codes;
	}
	public void setSp_codes(String sp_codes) {
		this.sp_codes = sp_codes;
	}
	private String sp_init;//缩写码
	private String unit;//标准单位
	private String unit1;//参考单位
	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private String sp_desc;//规格
	private Double cnt;//物资表库存
	private Double unitper;  //第二单位比第一单位的转换率
	private String chk1tim;//采购确认时间
	private String chk1emp;//采购确认人
	private String chk2tim;//采购审核时间
	private String chk2emp;//采购审核人
	//采购系统用
	private String puprordertim;//生成采购订单 时间
	private String puprorderemp;//生成采购订单人员
	private String ispuprorder;//状态，是否生成采购订单
	//查询条件
	private String ynArrival;//是否确认到货
	private String yndo;//是否已处理
	private String chk1;//采购确认
	private String chksend;//到货未知
	private String sta;//采购审核
	private String ex1;//是否半成品
	private String chectim;//提交时间
	private String bchectim;//提交时间区间
	private String echectim;
	private String lineCode;//配送线路编码
	private String lineDes;//配送线路名称
	private String psarea;//配送片区
	private Integer psorder;//配送批次
	private String typCode;//类别编码
	private String typDes;//类别名称
	private String firmCode;//分店编码
	private String firmDes;//分店名称
	private String firmDes1;//分店名称简称
	private String deliverCode;//供应商编码
	private String deliverDes;//供应商名称
    private String positnCode;//仓位编码
    private String positnDes;//仓位名称
    private String inCondition;//id in 的查询条件

    private String scm;//项目名
    private String wzqx;//物资权限，无筛选0，按照分店物资属性1，按照账号物资权限2
    private String wzzhqx;//物资账号权限再细分：0具体到物资  1具体到小类  wjf
    private String sp_type;//物资小类 wjf 新增字段
    private String fxqx;//申购方向权限，无筛选0，按照账号方向权限1
    private String gysqx;//供应商权限，无筛选0，按照供应商权限1
    private String cwqx;//仓位权限，无筛选0，按照仓位权限1
	private String accountId;//物资权限为2时候的筛选用
	private String deliveryn;//供应商是否确认送货
	private String fbcxqx;//报货分拨模块下查询是否按照账号物资权限筛选
	
	private Double amount1sto;//新报货流程 中的采购修改数量 wjf
	private String unit3;//采购单位
	private String unitper3;//采购单位和标准单位的转换率
	private Integer sp_per1;//有效期
	private String pcno;//批次号
	private String positn1;//货架字段    鱼酷加的需求 ，导出分拨单要！ 2015.1.4wjf
	private String ynbatch;//是否批次管理
	private String ynprice;//是否有报价
	private String chkyh;//是否验货（直配用）
	private Double accprate=0.0;  //验货比率上限
	private Double accpratemin = 0.0;//门店验货比率下限
	private String empfirm;//直配验货人 mis用
	private String isDept;//是否有档口报货单 档口验货用 2015.3.6wjf
	private List<Chkstod> deptList;//报货分拨显示档口报货用 wjf
	private String ynkc;//门店是否有库存 报货分拨显示档口明细用 wjf
	
	private Double amountyh;//验货数量
	private String vouno;//凭证号
	
	private int chkinno;//验货时保存入库单单号
	private String invouno;//入库凭证号
	private String typdes;
	private String grpdes;
	private String grptypdes;
	private String scmProject;//项目名
	
	public String getScmProject() {
		return scmProject;
	}
	public void setScmProject(String scmProject) {
		this.scmProject = scmProject;
	}
	private String tax;
	private String taxdes;
	
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	private String chk1condition;//报货分拨页面采购新加采购条件
	
	private int pageSize;
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getChk1condition() {
		return chk1condition;
	}
	public void setChk1condition(String chk1condition) {
		this.chk1condition = chk1condition;
	}
	public String getTypdes() {
		return typdes;
	}
	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}
	public String getGrpdes() {
		return grpdes;
	}
	public void setGrpdes(String grpdes) {
		this.grpdes = grpdes;
	}
	public String getGrptypdes() {
		return grptypdes;
	}
	public void setGrptypdes(String grptypdes) {
		this.grptypdes = grptypdes;
	}
	public String getFirmDes1() {
		return firmDes1;
	}
	public void setFirmDes1(String firmDes1) {
		this.firmDes1 = firmDes1;
	}
	public String getPuprordertim() {
		return puprordertim;
	}
	public void setPuprordertim(String puprordertim) {
		this.puprordertim = puprordertim;
	}
	public String getPuprorderemp() {
		return puprorderemp;
	}
	public void setPuprorderemp(String puprorderemp) {
		this.puprorderemp = puprorderemp;
	}
	public String getIspuprorder() {
		return ispuprorder;
	}
	public void setIspuprorder(String ispuprorder) {
		this.ispuprorder = ispuprorder;
	}
	public String getYnkc() {
		return ynkc;
	}
	public void setYnkc(String ynkc) {
		this.ynkc = ynkc;
	}
	public List<Chkstod> getDeptList() {
		return deptList;
	}
	public void setDeptList(List<Chkstod> deptList) {
		this.deptList = deptList;
	}
	public String getIsDept() {
		return isDept;
	}
	public void setIsDept(String isDept) {
		this.isDept = isDept;
	}
	public String getEmpfirm() {
		return empfirm;
	}
	public void setEmpfirm(String empfirm) {
		this.empfirm = empfirm;
	}
	public Double getAccprate() {
		return accprate;
	}
	public void setAccprate(Double accprate) {
		this.accprate = accprate;
	}
	public Double getAccpratemin() {
		return accpratemin;
	}
	public void setAccpratemin(Double accpratemin) {
		this.accpratemin = accpratemin;
	}
	public String getChkyh() {
		return chkyh;
	}
	public void setChkyh(String chkyh) {
		this.chkyh = chkyh;
	}
	public String getYnprice() {
		return ynprice;
	}
	public void setYnprice(String ynprice) {
		this.ynprice = ynprice;
	}
	public String getYnbatch() {
		return ynbatch;
	}
	public void setYnbatch(String ynbatch) {
		this.ynbatch = ynbatch;
	}
	public String getPositn1() {
		return positn1;
	}
	public void setPositn1(String positn1) {
		this.positn1 = positn1;
	}
	public String getPcno() {
		return pcno;
	}
	public void setPcno(String pcno) {
		this.pcno = pcno;
	}
	public Integer getSp_per1() {
		return sp_per1;
	}
	public void setSp_per1(Integer sp_per1) {
		this.sp_per1 = sp_per1;
	}
	public Double getAmount1sto() {
		return amount1sto;
	}
	public void setAmount1sto(Double amount1sto) {
		this.amount1sto = amount1sto;
	}
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public String getUnitper3() {
		return unitper3;
	}
	public void setUnitper3(String unitper3) {
		this.unitper3 = unitper3;
	}
	public String getChkstoNos() {
		return chkstoNos;
	}
	public void setChkstoNos(String chkstoNos) {
		this.chkstoNos = chkstoNos;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYhrate() {
		return yhrate;
	}
	public void setYhrate(String yhrate) {
		this.yhrate = yhrate;
	}
	public String getChrate() {
		return chrate;
	}
	public void setChrate(String chrate) {
		this.chrate = chrate;
	}	
	public String getScm() {
		return scm;
	}
	public void setScm(String scm) {
		this.scm = scm;
	}
	private String orderBy;//排序
	
    public String getFbcxqx() {
		return fbcxqx;
	}
	public void setFbcxqx(String fbcxqx) {
		this.fbcxqx = fbcxqx;
	}
	public String getGysqx() {
		return gysqx;
	}
	public void setGysqx(String gysqx) {
		this.gysqx = gysqx;
	}
	public String getCwqx() {
		return cwqx;
	}
	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}
	public String getWzqx() {
		return wzqx;
	}
	public void setWzqx(String wzqx) {
		this.wzqx = wzqx;
	}
	public String getFxqx() {
		return fxqx;
	}
	public void setFxqx(String fxqx) {
		this.fxqx = fxqx;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getInd1() {
		return ind1;
	}
	public void setInd1(String ind1) {
		this.ind1 = ind1;
	}
	public String getChkinout() {
		return chkinout;
	}
	public void setChkinout(String chkinout) {
		this.chkinout = chkinout;
	}
	public String getHoped() {
		return hoped;
	}
	public void setHoped(String hoped) {
		this.hoped = hoped;
	}
	public String getInCondition() {
		return inCondition;
	}
	public void setInCondition(String inCondition) {
		this.inCondition = inCondition;
	}
	//标识字段
    private String danjuTyp;//单据类型，打印排序用
    private String checkinSelect;//入库查询标识
    
    private int startNum;
    private int endNum;
    
    
	public int getStartNum() {
		return startNum;
	}
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getChkstoNo() {
		return chkstoNo;
	}
	public void setChkstoNo(Integer chkstoNo) {
		this.chkstoNo = chkstoNo;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmount1() {
		return amount1;
	}
	public void setAmount1(Double amount1) {
		this.amount1 = amount1;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public Double getAmountsto() {
		return amountsto;
	}
	public void setAmountsto(Double amountsto) {
		this.amountsto = amountsto;
	}
	public Double getAmountin() {
		return amountin;
	}
	public void setAmountin(Double amountin) {
		this.amountin = amountin;
	}
	public Double getAmount1in() {
		return amount1in;
	}
	public void setAmount1in(Double amount1in) {
		this.amount1in = amount1in;
	}
	public Integer getPrncnt() {
		return prncnt;
	}
	public void setPrncnt(Integer prncnt) {
		this.prncnt = prncnt;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getChkin() {
		return chkin;
	}
	public void setChkin(String chkin) {
		this.chkin = chkin;
	}
	public String getChkout() {
		return chkout;
	}
	public void setChkout(String chkout) {
		this.chkout = chkout;
	}
	public String getBak2() {
		return bak2;
	}
	public void setBak2(String bak2) {
		this.bak2 = bak2;
	}
	public Double getPricein() {
		return pricein;
	}
	public void setPricein(Double pricein) {
		this.pricein = pricein;
	}
	public Double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(Double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public int getSp_id() {
		return sp_id;
	}
	public void setSp_id(int sp_id) {
		this.sp_id = sp_id;
	}
	public Double getPound() {
		return pound;
	}
	public void setPound(Double pound) {
		this.pound = pound;
	}
	public Date getDued() {
		return dued;
	}
	public void setDued(Date dued) {
		this.dued = dued;
	}
	public String getSp_init() {
		return sp_init;
	}
	public void setSp_init(String sp_init) {
		this.sp_init = sp_init;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public String getChk1tim() {
		return chk1tim;
	}
	public void setChk1tim(String chk1tim) {
		this.chk1tim = chk1tim;
	}
	public String getChk1emp() {
		return chk1emp;
	}
	public void setChk1emp(String chk1emp) {
		this.chk1emp = chk1emp;
	}
	public String getChk2tim() {
		return chk2tim;
	}
	public void setChk2tim(String chk2tim) {
		this.chk2tim = chk2tim;
	}
	public String getChk2emp() {
		return chk2emp;
	}
	public void setChk2emp(String chk2emp) {
		this.chk2emp = chk2emp;
	}
	public String getYnArrival() {
		return ynArrival;
	}
	public void setYnArrival(String ynArrival) {
		this.ynArrival = ynArrival;
	}
	public String getYndo() {
		return yndo;
	}
	public void setYndo(String yndo) {
		this.yndo = yndo;
	}
	public String getChk1() {
		return chk1;
	}
	public void setChk1(String chk1) {
		this.chk1 = chk1;
	}
	public String getChksend() {
		return chksend;
	}
	public void setChksend(String chksend) {
		this.chksend = chksend;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getEx1() {
		return ex1;
	}
	public void setEx1(String ex1) {
		this.ex1 = ex1;
	}
	public String getChectim() {
		return chectim;
	}
	public void setChectim(String chectim) {
		this.chectim = chectim;
	}
	public String getPsarea() {
		return psarea;
	}
	public void setPsarea(String psarea) {
		this.psarea = psarea;
	}
	public Integer getPsorder() {
		return psorder;
	}
	public void setPsorder(Integer psorder) {
		this.psorder = psorder;
	}
	public String getTypCode() {
		return typCode;
	}
	public void setTypCode(String typCode) {
		this.typCode = typCode;
	}
	public String getTypDes() {
		return typDes;
	}
	public void setTypDes(String typDes) {
		this.typDes = typDes;
	}
	public String getFirmCode() {
		return firmCode;
	}
	public void setFirmCode(String firmCode) {
		this.firmCode = firmCode;
	}
	public String getFirmDes() {
		return firmDes;
	}
	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}
	public String getDeliverCode() {
		return deliverCode;
	}
	public void setDeliverCode(String deliverCode) {
		this.deliverCode = deliverCode;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	public String getPositnDes() {
		return positnDes;
	}
	public void setPositnDes(String positnDes) {
		this.positnDes = positnDes;
	}
	public String getDanjuTyp() {
		return danjuTyp;
	}
	public void setDanjuTyp(String danjuTyp) {
		this.danjuTyp = danjuTyp;
	}
	public String getCheckinSelect() {
		return checkinSelect;
	}
	public void setCheckinSelect(String checkinSelect) {
		this.checkinSelect = checkinSelect;
	}
	public String getLineCode() {
		return lineCode;
	}
	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}
	public String getLineDes() {
		return lineDes;
	}
	public void setLineDes(String lineDes) {
		this.lineDes = lineDes;
	}
	public Double getPricesale() {
		return pricesale;
	}
	public void setPricesale(Double pricesale) {
		this.pricesale = pricesale;
	}
	public String getDeliveryn() {
		return deliveryn;
	}
	public void setDeliveryn(String deliveryn) {
		this.deliveryn = deliveryn;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public Double getUnitper() {
		return unitper;
	}
	public void setUnitper(Double unitper) {
		this.unitper = unitper;
	}
	public String getWzzhqx() {
		return wzzhqx;
	}
	public void setWzzhqx(String wzzhqx) {
		this.wzzhqx = wzzhqx;
	}
	public String getSp_type() {
		return sp_type;
	}
	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}
	public String getBchectim() {
		return bchectim;
	}
	public void setBchectim(String bchectim) {
		this.bchectim = bchectim;
	}
	public String getEchectim() {
		return echectim;
	}
	public void setEchectim(String echectim) {
		this.echectim = echectim;
	}
	public int getChkinno() {
		return chkinno;
	}
	public void setChkinno(int chkinno) {
		this.chkinno = chkinno;
	}
	public Double getAmountyh() {
		return amountyh;
	}
	public void setAmountyh(Double amountyh) {
		this.amountyh = amountyh;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getInvouno() {
		return invouno;
	}
	public void setInvouno(String invouno) {
		this.invouno = invouno;
	}
	
}
