package com.choice.scm.domain;

/**
 * 销售订单从表
 * @author bella
 *
 */
public class FiveNcSalePreSub {

	private String cinventoryid;//物资编码
	private String cunitid;//主计量编码
	private String dreceivedate;//要求到货日期
	private String dsenddate;//计划发货日期
	private String nnumber;//数量
	private String norgnlcursummny;//原币价税合计
	private String norgnlcurtaxprice;//原币含税单价
	private String npacknumber;  //参考数量 
	private String cpackunitid;  //参考计量编码
	
	 
	public String getNpacknumber() {
		return npacknumber;
	}
	public void setNpacknumber(String npacknumber) {
		this.npacknumber = npacknumber;
	}
	public String getCpackunitid() {
		return cpackunitid;
	}
	public void setCpackunitid(String cpackunitid) {
		this.cpackunitid = cpackunitid;
	}
	public String getCinventoryid() {
		return cinventoryid;
	}
	public void setCinventoryid(String cinventoryid) {
		this.cinventoryid = cinventoryid;
	}
	public String getCunitid() {
		return cunitid;
	}
	public void setCunitid(String cunitid) {
		this.cunitid = cunitid;
	}
	public String getDreceivedate() {
		return dreceivedate;
	}
	public void setDreceivedate(String dreceivedate) {
		this.dreceivedate = dreceivedate;
	}
	public String getDsenddate() {
		return dsenddate;
	}
	public void setDsenddate(String dsenddate) {
		this.dsenddate = dsenddate;
	}
	public String getNnumber() {
		return nnumber;
	}
	public void setNnumber(String nnumber) {
		this.nnumber = nnumber;
	}
	public String getNorgnlcursummny() {
		return norgnlcursummny;
	}
	public void setNorgnlcursummny(String norgnlcursummny) {
		this.norgnlcursummny = norgnlcursummny;
	}
	public String getNorgnlcurtaxprice() {
		return norgnlcurtaxprice;
	}
	public void setNorgnlcurtaxprice(String norgnlcurtaxprice) {
		this.norgnlcurtaxprice = norgnlcurtaxprice;
	}
	
	
}
