package com.choice.scm.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;

import com.choice.framework.domain.system.User;

public abstract class BaseDomain implements Serializable {

	private static final long serialVersionUID = -7628596783298541241L;

    protected User creator;
    protected User modifier;
    protected Date creationtime;
    protected Date modifiedtime;
    protected Date ts;
    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

	@SuppressWarnings("rawtypes")
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		Class clazz = this.getClass();
		Field[] fields = clazz.getDeclaredFields();
		try {
			for (int i = 0; i < fields.length; i++) {
				Field f = fields[i];
				if (Modifier.isStatic(f.getModifiers()))
					continue;
				f.setAccessible(true);
				Object value = f.get(this);
				if (buffer.length() > 0) {
					buffer.append(',');
				}
				if (value == null) {
					buffer.append(f.getName() + "=null");
				} else {
					buffer.append(f.getName() + "=" + value);
				}
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		return buffer.toString();
	}

	public Date getModifiedtime() {
		return modifiedtime;
	}

	public void setModifiedtime(Date modifiedtime) {
			this.modifiedtime = modifiedtime;
	}

	public User getModifier() {
		return modifier;
	}

	public void setModifier(User modifier) {
		this.modifier = modifier;
	}

	public Date getCreationtime() {
		return creationtime;
	}

	public void setCreationtime(Date creationtime) {
			this.creationtime = creationtime;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}
}