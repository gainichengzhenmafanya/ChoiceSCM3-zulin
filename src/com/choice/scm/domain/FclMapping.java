package com.choice.scm.domain;


/**
 * 描述：财务科目映射设置
 * 日期：2014年1月12日 上午11:11:21
 * 作者：毛明武mmw
 * 文件：FclMapping.java
 */
public class FclMapping extends BaseDomain {

	private static final long serialVersionUID = -1832920116722425171L;
	private String pk_fclmapping;
	private String pk_id;//系统项目名称
	private String pk_balatype;//科目项目名称
	private String svcode ;//系统编码
	private String svname ;//系统名称
	private String vcode ;//财务科目
	private String vname ;//科目名称
	private String type="6" ;//科目名称
	/**
	 * 自定义项1，以下类推
	 */
	private String vdef1;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private String vdef6;
	private String vdef7;
	private String vdef8;
	private String vdef9;
	private String vdef10;
	private String vdef11;
	private String vdef12;
	private String vdef13;
	private String vdef14;
	private String vdef15;
	private String vdef16;
	private String vdef17;
	private String vdef18;
	private String vdef19;
	private String vdef20;
	/**
	 * @return pk_fclmapping
	 */
	public String getPk_fclmapping() {
		return pk_fclmapping;
	}
	/**
	 * @param pk_fclmapping the pk_fclmapping to set
	 */
	public void setPk_fclmapping(String pk_fclmapping) {
		this.pk_fclmapping = pk_fclmapping;
	}
	/**
	 * @return pk_id
	 */
	public String getPk_id() {
		return pk_id;
	}
	/**
	 * @param pk_id the pk_id to set
	 */
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	/**
	 * @return svcode
	 */
	public String getSvcode() {
		return svcode;
	}
	/**
	 * @param svcode the svcode to set
	 */
	public void setSvcode(String svcode) {
		this.svcode = svcode;
	}
	/**
	 * @return svname
	 */
	public String getSvname() {
		return svname;
	}
	/**
	 * @param svname the svname to set
	 */
	public void setSvname(String svname) {
		this.svname = svname;
	}
	/**
	 * @return vcode
	 */
	public String getVcode() {
		return vcode;
	}
	/**
	 * @param vcode the vcode to set
	 */
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	/**
	 * @return vname
	 */
	public String getVname() {
		return vname;
	}
	/**
	 * @param vname the vname to set
	 */
	public void setVname(String vname) {
		this.vname = vname;
	}
	/**
	 * @return vdef1
	 */
	public String getVdef1() {
		return vdef1;
	}
	/**
	 * @param vdef1 the vdef1 to set
	 */
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	/**
	 * @return vdef2
	 */
	public String getVdef2() {
		return vdef2;
	}
	/**
	 * @param vdef2 the vdef2 to set
	 */
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	/**
	 * @return vdef3
	 */
	public String getVdef3() {
		return vdef3;
	}
	/**
	 * @param vdef3 the vdef3 to set
	 */
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	/**
	 * @return vdef4
	 */
	public String getVdef4() {
		return vdef4;
	}
	/**
	 * @param vdef4 the vdef4 to set
	 */
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	/**
	 * @return vdef5
	 */
	public String getVdef5() {
		return vdef5;
	}
	/**
	 * @param vdef5 the vdef5 to set
	 */
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	/**
	 * @return vdef6
	 */
	public String getVdef6() {
		return vdef6;
	}
	/**
	 * @param vdef6 the vdef6 to set
	 */
	public void setVdef6(String vdef6) {
		this.vdef6 = vdef6;
	}
	/**
	 * @return vdef7
	 */
	public String getVdef7() {
		return vdef7;
	}
	/**
	 * @param vdef7 the vdef7 to set
	 */
	public void setVdef7(String vdef7) {
		this.vdef7 = vdef7;
	}
	/**
	 * @return vdef8
	 */
	public String getVdef8() {
		return vdef8;
	}
	/**
	 * @param vdef8 the vdef8 to set
	 */
	public void setVdef8(String vdef8) {
		this.vdef8 = vdef8;
	}
	/**
	 * @return vdef9
	 */
	public String getVdef9() {
		return vdef9;
	}
	/**
	 * @param vdef9 the vdef9 to set
	 */
	public void setVdef9(String vdef9) {
		this.vdef9 = vdef9;
	}
	/**
	 * @return vdef10
	 */
	public String getVdef10() {
		return vdef10;
	}
	/**
	 * @param vdef10 the vdef10 to set
	 */
	public void setVdef10(String vdef10) {
		this.vdef10 = vdef10;
	}
	/**
	 * @return vdef11
	 */
	public String getVdef11() {
		return vdef11;
	}
	/**
	 * @param vdef11 the vdef11 to set
	 */
	public void setVdef11(String vdef11) {
		this.vdef11 = vdef11;
	}
	/**
	 * @return vdef12
	 */
	public String getVdef12() {
		return vdef12;
	}
	/**
	 * @param vdef12 the vdef12 to set
	 */
	public void setVdef12(String vdef12) {
		this.vdef12 = vdef12;
	}
	/**
	 * @return vdef13
	 */
	public String getVdef13() {
		return vdef13;
	}
	/**
	 * @param vdef13 the vdef13 to set
	 */
	public void setVdef13(String vdef13) {
		this.vdef13 = vdef13;
	}
	/**
	 * @return vdef14
	 */
	public String getVdef14() {
		return vdef14;
	}
	/**
	 * @param vdef14 the vdef14 to set
	 */
	public void setVdef14(String vdef14) {
		this.vdef14 = vdef14;
	}
	/**
	 * @return vdef15
	 */
	public String getVdef15() {
		return vdef15;
	}
	/**
	 * @param vdef15 the vdef15 to set
	 */
	public void setVdef15(String vdef15) {
		this.vdef15 = vdef15;
	}
	/**
	 * @return vdef16
	 */
	public String getVdef16() {
		return vdef16;
	}
	/**
	 * @param vdef16 the vdef16 to set
	 */
	public void setVdef16(String vdef16) {
		this.vdef16 = vdef16;
	}
	/**
	 * @return vdef17
	 */
	public String getVdef17() {
		return vdef17;
	}
	/**
	 * @param vdef17 the vdef17 to set
	 */
	public void setVdef17(String vdef17) {
		this.vdef17 = vdef17;
	}
	/**
	 * @return vdef18
	 */
	public String getVdef18() {
		return vdef18;
	}
	/**
	 * @param vdef18 the vdef18 to set
	 */
	public void setVdef18(String vdef18) {
		this.vdef18 = vdef18;
	}
	/**
	 * @return vdef19
	 */
	public String getVdef19() {
		return vdef19;
	}
	/**
	 * @param vdef19 the vdef19 to set
	 */
	public void setVdef19(String vdef19) {
		this.vdef19 = vdef19;
	}
	/**
	 * @return vdef20
	 */
	public String getVdef20() {
		return vdef20;
	}
	/**
	 * @param vdef20 the vdef20 to set
	 */
	public void setVdef20(String vdef20) {
		this.vdef20 = vdef20;
	}
	/**
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return pk_balatype
	 */
	public String getPk_balatype() {
		return pk_balatype;
	}
	/**
	 * @param pk_balatype the pk_balatype to set
	 */
	public void setPk_balatype(String pk_balatype) {
		this.pk_balatype = pk_balatype;
	}
	

}
