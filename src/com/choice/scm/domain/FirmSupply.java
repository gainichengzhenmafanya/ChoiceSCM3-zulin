package com.choice.scm.domain;

public class FirmSupply {
	
	//总部门店库存表
	private String firm;
	private String sp_code;
	private Double cnt;
	private Double cnt1;
	private Double amt;
	
	//物资
	private String spname;
	private String unit;
	private String grptypdes;
	private String grptyp;
	private String grpdes;
	private String grp;
	private String typdes;
	private String sp_type;
	private String sp_desc;
	
	private String firmDes;
	
	public FirmSupply(){
		super();
	}

	public String getFirm() {
		return firm;
	}

	public void setFirm(String firm) {
		this.firm = firm;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public Double getCnt() {
		return cnt;
	}

	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}

	public Double getCnt1() {
		return cnt1;
	}

	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}

	public Double getAmt() {
		return amt;
	}

	public void setAmt(Double amt) {
		this.amt = amt;
	}

	public String getSpname() {
		return spname;
	}

	public void setSpname(String spname) {
		this.spname = spname;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getGrptypdes() {
		return grptypdes;
	}

	public void setGrptypdes(String grptypdes) {
		this.grptypdes = grptypdes;
	}

	public String getGrptyp() {
		return grptyp;
	}

	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}

	public String getGrpdes() {
		return grpdes;
	}

	public void setGrpdes(String grpdes) {
		this.grpdes = grpdes;
	}

	public String getGrp() {
		return grp;
	}

	public void setGrp(String grp) {
		this.grp = grp;
	}

	public String getTypdes() {
		return typdes;
	}

	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}

	public String getSp_type() {
		return sp_type;
	}

	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}

	public String getSp_desc() {
		return sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public String getFirmDes() {
		return firmDes;
	}

	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}
	
}
