package com.choice.scm.domain;
  
public class Pubitem {
	private String pubitem;//内码
	private String itcode;//编码
	private String itcodes;//菜品理论利润专用编码
	private String itdes;  //菜名
	private String unit;//单位
	private double price;//价格
	private String unit2;//单位
	private double price2;//价格
	private String unit3;//单位
	private double price3;//价格
	private String pgrp;//类别
	private String init;//缩写
	private String prdctcard;//是否做成成本卡
	private String prgid;//菜谱方案
	private String mods;//成本卡类型
	
	//菜品理论利用率
	private String acct;//帐套
	private double cost;//成本
	private double liRun;//利润
	private double baiFenBi;//百分比
	
	public String getItcodes() {
		return itcodes;
	}
	public void setItcodes(String itcodes) {
		this.itcodes = itcodes;
	}
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	public double getPrice2() {
		return price2;
	}
	public void setPrice2(double price2) {
		this.price2 = price2;
	}
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public double getPrice3() {
		return price3;
	}
	public void setPrice3(double price3) {
		this.price3 = price3;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public double getLiRun() {
		return liRun;
	}
	public void setLiRun(double liRun) {
		this.liRun = liRun;
	}
	public double getBaiFenBi() {
		return baiFenBi;
	}
	public void setBaiFenBi(double baiFenBi) {
		this.baiFenBi = baiFenBi;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getMods() {
		return mods;
	}
	public void setMods(String mods) {
		this.mods = mods;
	}
	public String getPrgid() {
		return prgid;
	}
	public void setPrgid(String prgid) {
		this.prgid = prgid;
	}
	public String getPubitem() {
		return pubitem;
	}
	public void setPubitem(String pubitem) {
		this.pubitem = pubitem;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getItdes() {
		return itdes;
	}
	public void setItdes(String itdes) {
		this.itdes = itdes;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getPgrp() {
		return pgrp;
	}
	public void setPgrp(String pgrp) {
		this.pgrp = pgrp;
	}
	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	public String getPrdctcard() {
		return prdctcard;
	}
	public void setPrdctcard(String prdctcard) {
		this.prdctcard = prdctcard;
	}
	
}
