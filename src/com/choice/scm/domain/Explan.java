package com.choice.scm.domain;

import java.util.Date;
import java.util.List;
 /**
  * 调度中心
  * @author lehui
  *
  */
public class Explan {
	
	 private String acct;//帐套
	 private Integer id;  //流水号
	 private Integer exno;//加工号
	 private Date	dat;//日期
	 private Date	datEnd;//结束日期
	 private Supply supply;// 产品名称
	 private double cntplan;//计划加工
	 private double cnt;//需要加工
	 private double cntact;//产后登记数量
	 private String ynex;//是否分解
	 private String ynsto;//是否报货
	 private String ynin;//是否入库
	 private String ynout;//实际已经领料
	 private String btim;//开始时间
	 private String etim;//结束时间
	 private String empsto;//申购人
	 private String empin;//入库人
	 private String empout;//领料人
	 private String sta;//状态
	 private double cntplan1;//计划加工量2
	 private double cntact1;//实际加工量2
	 private double cntkc;//当前库存量
	 private List<Explan> explanList;
	 private double unitper;//物资第二单位比第一单位的比率
	 
	private String ids;//  id的  ，号分隔的形式
	
	private int pageSize;
	 
	 public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	private int startNum;//分页开始
	 private int endNum;//分页结束
	 
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getExno() {
		return exno;
	}
	public void setExno(Integer exno) {
		this.exno = exno;
	}
	public Date getDat() {
		return dat;
	}
	public Date getDatEnd() {
		return datEnd;
	}
	public void setDatEnd(Date datEnd) {
		this.datEnd = datEnd;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public double getCntplan() {
		return cntplan;
	}
	public void setCntplan(double cntplan) {
		this.cntplan = cntplan;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getCntact() {
		return cntact;
	}
	public void setCntact(double cntact) {
		this.cntact = cntact;
	}
	public String getYnex() {
		return ynex;
	}
	public void setYnex(String ynex) {
		this.ynex = ynex;
	}
	public String getYnsto() {
		return ynsto;
	}
	public void setYnsto(String ynsto) {
		this.ynsto = ynsto;
	}
	public String getYnin() {
		return ynin;
	}
	public void setYnin(String ynin) {
		this.ynin = ynin;
	}
	public String getYnout() {
		return ynout;
	}
	public void setYnout(String ynout) {
		this.ynout = ynout;
	}
	public String getBtim() {
		return btim;
	}
	public void setBtim(String btim) {
		this.btim = btim;
	}
	public String getEtim() {
		return etim;
	}
	public void setEtim(String etim) {
		this.etim = etim;
	}
	public String getEmpsto() {
		return empsto;
	}
	public void setEmpsto(String empsto) {
		this.empsto = empsto;
	}
	public String getEmpin() {
		return empin;
	}
	public void setEmpin(String empin) {
		this.empin = empin;
	}
	public String getEmpout() {
		return empout;
	}
	public void setEmpout(String empout) {
		this.empout = empout;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public double getCntplan1() {
		return cntplan1;
	}
	public void setCntplan1(double cntplan1) {
		this.cntplan1 = cntplan1;
	}
	public double getCntact1() {
		return cntact1;
	}
	public void setCntact1(double cntact1) {
		this.cntact1 = cntact1;
	}
	public double getCntkc() {
		return cntkc;
	}
	public void setCntkc(double cntkc) {
		this.cntkc = cntkc;
	}
	public List<Explan> getExplanList() {
		return explanList;
	}
	public void setExplanList(List<Explan> explanList) {
		this.explanList = explanList;
	}
	
	public double getUnitper() {
		return unitper;
	}
	public void setUnitper(double unitper) {
		this.unitper = unitper;
	}
	
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public int getStartNum() {
		return startNum;
	}
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}
}
