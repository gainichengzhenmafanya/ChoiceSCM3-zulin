package com.choice.scm.domain;

import java.util.Date;

/**
 * 核减数据表
 * @author css
 *
 */
public class CostItem {
	private String year;
	private int month;
	private int    rec;
	private String acct;
	private String firm;
	private String firmdes;
	private Date   dat;
	private String item;
	private Supply itcode;
	private String des;
	private String unit;
	private String typ;
	private String dept;
	private double  itprice;
	private double  costmax;
	private double  cnt;
	private double  num;
	private double  amt;
	private double  disc;
	private double  svc;
	private double  tax;
	private double  refund;
	private double  pickdisc;
	private double  total;
	private double  costtl;
	private double  costmx;
	private double  costre;
	private double  cost;
	private String tx;
	private String dept1;
	private String deptdes;
	private String chkcost;
	private String ynunit;
	private String positn;
	private int pr;
	private String airdittype;//成本卡类型
	
	public String getAirdittype() {
		return airdittype;
	}
	public void setAirdittype(String airdittype) {
		this.airdittype = airdittype;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getPr() {
		return pr;
	}
	public void setPr(int pr) {
		this.pr = pr;
	}
	public int getRec() {
		return rec;
	}
	public void setRec(int rec) {
		this.rec = rec;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Supply getItcode() {
		return itcode;
	}
	public void setItcode(Supply itcode) {
		this.itcode = itcode;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public double getItprice() {
		return itprice;
	}
	public void setItprice(double itprice) {
		this.itprice = itprice;
	}
	public double getCostmax() {
		return costmax;
	}
	public void setCostmax(double costmax) {
		this.costmax = costmax;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getNum() {
		return num;
	}
	public void setNum(double num) {
		this.num = num;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getDisc() {
		return disc;
	}
	public void setDisc(double disc) {
		this.disc = disc;
	}
	public double getSvc() {
		return svc;
	}
	public void setSvc(double svc) {
		this.svc = svc;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getRefund() {
		return refund;
	}
	public void setRefund(double refund) {
		this.refund = refund;
	}
	public double getPickdisc() {
		return pickdisc;
	}
	public void setPickdisc(double pickdisc) {
		this.pickdisc = pickdisc;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getCosttl() {
		return costtl;
	}
	public void setCosttl(double costtl) {
		this.costtl = costtl;
	}
	public double getCostmx() {
		return costmx;
	}
	public void setCostmx(double costmx) {
		this.costmx = costmx;
	}
	public double getCostre() {
		return costre;
	}
	public void setCostre(double costre) {
		this.costre = costre;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getTx() {
		return tx;
	}
	public void setTx(String tx) {
		this.tx = tx;
	}
	public String getDept1() {
		return dept1;
	}
	public void setDept1(String dept1) {
		this.dept1 = dept1;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getChkcost() {
		return chkcost;
	}
	public void setChkcost(String chkcost) {
		this.chkcost = chkcost;
	}
	public String getYnunit() {
		return ynunit;
	}
	public void setYnunit(String ynunit) {
		this.ynunit = ynunit;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	

}
