package com.choice.scm.domain;

/**
 * 分店部门物资
 * @author csb
 *
 */
public class FirmDeptSupply {
	private String 	 	 acct			;
	private String	 	 yearr			;
	private String   	 firm			;
	private String 	 	 sp_code		;
	private Double		 inc0			;
	private Double		 ina0			;
	private Double		 outc0			;
	private Double		 outa0			;
	private Double		 inc1			;
	private Double		 ina1			;
	private Double		 outc1			;
	private Double		 outa1			;
	private Double		 inc2			;
	private Double		 ina2			;
	private Double		 outc2			;
	private Double		 outa2			;
	private Double		 inc3			;
	private Double		 ina3			;
	private Double		 outc3			;
	private Double		 outa3			;
	private Double		 inc4			;
	private Double		 ina4			;
	private Double		 outc4			;
	private Double		 outa4			;
	private Double		 inc5			;
	private Double		 ina5			;
	private Double		 outc5			;
	private Double		 outa5			;
	private Double		 inc6			;
	private Double		 ina6			;
	private Double		 outc6			;
	private Double		 outa6			;
	private Double		 inc7			;
	private Double		 ina7			;
	private Double		 outc7			;
	private Double		 outa7			;
	private Double		 inc8			;
	private Double		 ina8			;
	private Double		 outc8			;
	private Double		 outa8			;
	private Double		 inc9			;
	private Double		 ina9			;
	private Double		 outc9			;
	private Double		 outa9			;
	private Double		 inc10			;
	private Double		 ina10			;
	private Double		 outc10			;
	private Double		 outa10			;
	private Double		 inc11			;
	private Double		 ina11			;
	private Double		 outc11			;
	private Double		 outa11			;
	private Double		 inc12			;
	private Double		 ina12			;
	private Double		 outc12			;
	private Double		 outa12			;
	private Double		 inc13			;
	private Double		 ina13			;
	private Double		 outc13			;
	private Double		 outa13			;
	private String 		 sp_cost		;
	private Double		 ninc0			;
	private Double		 nina0			;
	private Double		 noutc0			;
	private Double		 nouta0			;
	private Double		 cost1			;
	private Double		 cost2			;
	private Double		 cost3			;
	private Double		 cost4			;
	private Double		 cost5			;
	private Double		 cost6			;
	private Double		 cost7			;
	private Double		 cost8			;
	private Double		 cost9			;
	private Double		 cost10			;
	private Double		 cost11			;
	private Double		 cost12			;
	private Double		 costa1			;
	private Double		 costa2			;
	private Double		 costa3			;
	private Double		 costa4			;
	private Double		 costa5			;
	private Double		 costa6			;
	private Double		 costa7			;
	private Double		 costa8			;
	private Double		 costa9			;
	private Double		 costa10		;
	private Double		 costa11		;
	private Double		 costa12		;
	private Double		 incu0			;
	private Double		 incu1			;
	private Double		 incu2			;
	private Double		 incu3			;
	private Double		 incu4			;
	private Double		 incu5			;
	private Double		 incu6			;
	private Double		 incu7			;
	private Double		 incu8			;
	private Double		 incu9			;
	private Double		 incu10			;
	private Double		 incu11			;
	private Double		 incu12			;
	private Double		 incu13			;
	private Double		 outcu0			;
	private Double		 outcu1			;
	private Double		 outcu2			;
	private Double		 outcu3			;
	private Double		 outcu4			;
	private Double		 outcu5			;
	private Double		 outcu6			;
	private Double		 outcu7			;
	private Double		 outcu8			;
	private Double		 outcu9			;
	private Double		 outcu10		;
	private Double		 outcu11		;
	private Double		 outcu12		;
	private Double		 outcu13		;
	private Double		 costu1			;
	private Double		 costu2			;
	private Double		 costu3			;
	private Double		 costu4			;
	private Double		 costu5			;
	private Double		 costu6			;
	private Double		 costu7			;
	private Double		 costu8			;
	private Double		 costu9			;
	private Double		 costu10		;
	private Double		 costu11		;
	private Double		 costu12		;
	private Double		 cnttrival		;
	private Double		 cntutrivaL		;
	private String	 	 dept			;
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getInc0() {
		return inc0;
	}
	public void setInc0(Double inc0) {
		this.inc0 = inc0;
	}
	public Double getIna0() {
		return ina0;
	}
	public void setIna0(Double ina0) {
		this.ina0 = ina0;
	}
	public Double getOutc0() {
		return outc0;
	}
	public void setOutc0(Double outc0) {
		this.outc0 = outc0;
	}
	public Double getOuta0() {
		return outa0;
	}
	public void setOuta0(Double outa0) {
		this.outa0 = outa0;
	}
	public Double getInc1() {
		return inc1;
	}
	public void setInc1(Double inc1) {
		this.inc1 = inc1;
	}
	public Double getIna1() {
		return ina1;
	}
	public void setIna1(Double ina1) {
		this.ina1 = ina1;
	}
	public Double getOutc1() {
		return outc1;
	}
	public void setOutc1(Double outc1) {
		this.outc1 = outc1;
	}
	public Double getOuta1() {
		return outa1;
	}
	public void setOuta1(Double outa1) {
		this.outa1 = outa1;
	}
	public Double getInc2() {
		return inc2;
	}
	public void setInc2(Double inc2) {
		this.inc2 = inc2;
	}
	public Double getIna2() {
		return ina2;
	}
	public void setIna2(Double ina2) {
		this.ina2 = ina2;
	}
	public Double getOutc2() {
		return outc2;
	}
	public void setOutc2(Double outc2) {
		this.outc2 = outc2;
	}
	public Double getOuta2() {
		return outa2;
	}
	public void setOuta2(Double outa2) {
		this.outa2 = outa2;
	}
	public Double getInc3() {
		return inc3;
	}
	public void setInc3(Double inc3) {
		this.inc3 = inc3;
	}
	public Double getIna3() {
		return ina3;
	}
	public void setIna3(Double ina3) {
		this.ina3 = ina3;
	}
	public Double getOutc3() {
		return outc3;
	}
	public void setOutc3(Double outc3) {
		this.outc3 = outc3;
	}
	public Double getOuta3() {
		return outa3;
	}
	public void setOuta3(Double outa3) {
		this.outa3 = outa3;
	}
	public Double getInc4() {
		return inc4;
	}
	public void setInc4(Double inc4) {
		this.inc4 = inc4;
	}
	public Double getIna4() {
		return ina4;
	}
	public void setIna4(Double ina4) {
		this.ina4 = ina4;
	}
	public Double getOutc4() {
		return outc4;
	}
	public void setOutc4(Double outc4) {
		this.outc4 = outc4;
	}
	public Double getOuta4() {
		return outa4;
	}
	public void setOuta4(Double outa4) {
		this.outa4 = outa4;
	}
	public Double getInc5() {
		return inc5;
	}
	public void setInc5(Double inc5) {
		this.inc5 = inc5;
	}
	public Double getIna5() {
		return ina5;
	}
	public void setIna5(Double ina5) {
		this.ina5 = ina5;
	}
	public Double getOutc5() {
		return outc5;
	}
	public void setOutc5(Double outc5) {
		this.outc5 = outc5;
	}
	public Double getOuta5() {
		return outa5;
	}
	public void setOuta5(Double outa5) {
		this.outa5 = outa5;
	}
	public Double getInc6() {
		return inc6;
	}
	public void setInc6(Double inc6) {
		this.inc6 = inc6;
	}
	public Double getIna6() {
		return ina6;
	}
	public void setIna6(Double ina6) {
		this.ina6 = ina6;
	}
	public Double getOutc6() {
		return outc6;
	}
	public void setOutc6(Double outc6) {
		this.outc6 = outc6;
	}
	public Double getOuta6() {
		return outa6;
	}
	public void setOuta6(Double outa6) {
		this.outa6 = outa6;
	}
	public Double getInc7() {
		return inc7;
	}
	public void setInc7(Double inc7) {
		this.inc7 = inc7;
	}
	public Double getIna7() {
		return ina7;
	}
	public void setIna7(Double ina7) {
		this.ina7 = ina7;
	}
	public Double getOutc7() {
		return outc7;
	}
	public void setOutc7(Double outc7) {
		this.outc7 = outc7;
	}
	public Double getOuta7() {
		return outa7;
	}
	public void setOuta7(Double outa7) {
		this.outa7 = outa7;
	}
	public Double getInc8() {
		return inc8;
	}
	public void setInc8(Double inc8) {
		this.inc8 = inc8;
	}
	public Double getIna8() {
		return ina8;
	}
	public void setIna8(Double ina8) {
		this.ina8 = ina8;
	}
	public Double getOutc8() {
		return outc8;
	}
	public void setOutc8(Double outc8) {
		this.outc8 = outc8;
	}
	public Double getOuta8() {
		return outa8;
	}
	public void setOuta8(Double outa8) {
		this.outa8 = outa8;
	}
	public Double getInc9() {
		return inc9;
	}
	public void setInc9(Double inc9) {
		this.inc9 = inc9;
	}
	public Double getIna9() {
		return ina9;
	}
	public void setIna9(Double ina9) {
		this.ina9 = ina9;
	}
	public Double getOutc9() {
		return outc9;
	}
	public void setOutc9(Double outc9) {
		this.outc9 = outc9;
	}
	public Double getOuta9() {
		return outa9;
	}
	public void setOuta9(Double outa9) {
		this.outa9 = outa9;
	}
	public Double getInc10() {
		return inc10;
	}
	public void setInc10(Double inc10) {
		this.inc10 = inc10;
	}
	public Double getIna10() {
		return ina10;
	}
	public void setIna10(Double ina10) {
		this.ina10 = ina10;
	}
	public Double getOutc10() {
		return outc10;
	}
	public void setOutc10(Double outc10) {
		this.outc10 = outc10;
	}
	public Double getOuta10() {
		return outa10;
	}
	public void setOuta10(Double outa10) {
		this.outa10 = outa10;
	}
	public Double getInc11() {
		return inc11;
	}
	public void setInc11(Double inc11) {
		this.inc11 = inc11;
	}
	public Double getIna11() {
		return ina11;
	}
	public void setIna11(Double ina11) {
		this.ina11 = ina11;
	}
	public Double getOutc11() {
		return outc11;
	}
	public void setOutc11(Double outc11) {
		this.outc11 = outc11;
	}
	public Double getOuta11() {
		return outa11;
	}
	public void setOuta11(Double outa11) {
		this.outa11 = outa11;
	}
	public Double getInc12() {
		return inc12;
	}
	public void setInc12(Double inc12) {
		this.inc12 = inc12;
	}
	public Double getIna12() {
		return ina12;
	}
	public void setIna12(Double ina12) {
		this.ina12 = ina12;
	}
	public Double getOutc12() {
		return outc12;
	}
	public void setOutc12(Double outc12) {
		this.outc12 = outc12;
	}
	public Double getOuta12() {
		return outa12;
	}
	public void setOuta12(Double outa12) {
		this.outa12 = outa12;
	}
	public Double getInc13() {
		return inc13;
	}
	public void setInc13(Double inc13) {
		this.inc13 = inc13;
	}
	public Double getIna13() {
		return ina13;
	}
	public void setIna13(Double ina13) {
		this.ina13 = ina13;
	}
	public Double getOutc13() {
		return outc13;
	}
	public void setOutc13(Double outc13) {
		this.outc13 = outc13;
	}
	public Double getOuta13() {
		return outa13;
	}
	public void setOuta13(Double outa13) {
		this.outa13 = outa13;
	}
	public String getSp_cost() {
		return sp_cost;
	}
	public void setSp_cost(String sp_cost) {
		this.sp_cost = sp_cost;
	}
	public Double getNinc0() {
		return ninc0;
	}
	public void setNinc0(Double ninc0) {
		this.ninc0 = ninc0;
	}
	public Double getNina0() {
		return nina0;
	}
	public void setNina0(Double nina0) {
		this.nina0 = nina0;
	}
	public Double getNoutc0() {
		return noutc0;
	}
	public void setNoutc0(Double noutc0) {
		this.noutc0 = noutc0;
	}
	public Double getNouta0() {
		return nouta0;
	}
	public void setNouta0(Double nouta0) {
		this.nouta0 = nouta0;
	}
	public Double getCost1() {
		return cost1;
	}
	public void setCost1(Double cost1) {
		this.cost1 = cost1;
	}
	public Double getCost2() {
		return cost2;
	}
	public void setCost2(Double cost2) {
		this.cost2 = cost2;
	}
	public Double getCost3() {
		return cost3;
	}
	public void setCost3(Double cost3) {
		this.cost3 = cost3;
	}
	public Double getCost4() {
		return cost4;
	}
	public void setCost4(Double cost4) {
		this.cost4 = cost4;
	}
	public Double getCost5() {
		return cost5;
	}
	public void setCost5(Double cost5) {
		this.cost5 = cost5;
	}
	public Double getCost6() {
		return cost6;
	}
	public void setCost6(Double cost6) {
		this.cost6 = cost6;
	}
	public Double getCost7() {
		return cost7;
	}
	public void setCost7(Double cost7) {
		this.cost7 = cost7;
	}
	public Double getCost8() {
		return cost8;
	}
	public void setCost8(Double cost8) {
		this.cost8 = cost8;
	}
	public Double getCost9() {
		return cost9;
	}
	public void setCost9(Double cost9) {
		this.cost9 = cost9;
	}
	public Double getCost10() {
		return cost10;
	}
	public void setCost10(Double cost10) {
		this.cost10 = cost10;
	}
	public Double getCost11() {
		return cost11;
	}
	public void setCost11(Double cost11) {
		this.cost11 = cost11;
	}
	public Double getCost12() {
		return cost12;
	}
	public void setCost12(Double cost12) {
		this.cost12 = cost12;
	}
	public Double getCosta1() {
		return costa1;
	}
	public void setCosta1(Double costa1) {
		this.costa1 = costa1;
	}
	public Double getCosta2() {
		return costa2;
	}
	public void setCosta2(Double costa2) {
		this.costa2 = costa2;
	}
	public Double getCosta3() {
		return costa3;
	}
	public void setCosta3(Double costa3) {
		this.costa3 = costa3;
	}
	public Double getCosta4() {
		return costa4;
	}
	public void setCosta4(Double costa4) {
		this.costa4 = costa4;
	}
	public Double getCosta5() {
		return costa5;
	}
	public void setCosta5(Double costa5) {
		this.costa5 = costa5;
	}
	public Double getCosta6() {
		return costa6;
	}
	public void setCosta6(Double costa6) {
		this.costa6 = costa6;
	}
	public Double getCosta7() {
		return costa7;
	}
	public void setCosta7(Double costa7) {
		this.costa7 = costa7;
	}
	public Double getCosta8() {
		return costa8;
	}
	public void setCosta8(Double costa8) {
		this.costa8 = costa8;
	}
	public Double getCosta9() {
		return costa9;
	}
	public void setCosta9(Double costa9) {
		this.costa9 = costa9;
	}
	public Double getCosta10() {
		return costa10;
	}
	public void setCosta10(Double costa10) {
		this.costa10 = costa10;
	}
	public Double getCosta11() {
		return costa11;
	}
	public void setCosta11(Double costa11) {
		this.costa11 = costa11;
	}
	public Double getCosta12() {
		return costa12;
	}
	public void setCosta12(Double costa12) {
		this.costa12 = costa12;
	}
	public Double getIncu0() {
		return incu0;
	}
	public void setIncu0(Double incu0) {
		this.incu0 = incu0;
	}
	public Double getIncu1() {
		return incu1;
	}
	public void setIncu1(Double incu1) {
		this.incu1 = incu1;
	}
	public Double getIncu2() {
		return incu2;
	}
	public void setIncu2(Double incu2) {
		this.incu2 = incu2;
	}
	public Double getIncu3() {
		return incu3;
	}
	public void setIncu3(Double incu3) {
		this.incu3 = incu3;
	}
	public Double getIncu4() {
		return incu4;
	}
	public void setIncu4(Double incu4) {
		this.incu4 = incu4;
	}
	public Double getIncu5() {
		return incu5;
	}
	public void setIncu5(Double incu5) {
		this.incu5 = incu5;
	}
	public Double getIncu6() {
		return incu6;
	}
	public void setIncu6(Double incu6) {
		this.incu6 = incu6;
	}
	public Double getIncu7() {
		return incu7;
	}
	public void setIncu7(Double incu7) {
		this.incu7 = incu7;
	}
	public Double getIncu8() {
		return incu8;
	}
	public void setIncu8(Double incu8) {
		this.incu8 = incu8;
	}
	public Double getIncu9() {
		return incu9;
	}
	public void setIncu9(Double incu9) {
		this.incu9 = incu9;
	}
	public Double getIncu10() {
		return incu10;
	}
	public void setIncu10(Double incu10) {
		this.incu10 = incu10;
	}
	public Double getIncu11() {
		return incu11;
	}
	public void setIncu11(Double incu11) {
		this.incu11 = incu11;
	}
	public Double getIncu12() {
		return incu12;
	}
	public void setIncu12(Double incu12) {
		this.incu12 = incu12;
	}
	public Double getIncu13() {
		return incu13;
	}
	public void setIncu13(Double incu13) {
		this.incu13 = incu13;
	}
	public Double getOutcu0() {
		return outcu0;
	}
	public void setOutcu0(Double outcu0) {
		this.outcu0 = outcu0;
	}
	public Double getOutcu1() {
		return outcu1;
	}
	public void setOutcu1(Double outcu1) {
		this.outcu1 = outcu1;
	}
	public Double getOutcu2() {
		return outcu2;
	}
	public void setOutcu2(Double outcu2) {
		this.outcu2 = outcu2;
	}
	public Double getOutcu3() {
		return outcu3;
	}
	public void setOutcu3(Double outcu3) {
		this.outcu3 = outcu3;
	}
	public Double getOutcu4() {
		return outcu4;
	}
	public void setOutcu4(Double outcu4) {
		this.outcu4 = outcu4;
	}
	public Double getOutcu5() {
		return outcu5;
	}
	public void setOutcu5(Double outcu5) {
		this.outcu5 = outcu5;
	}
	public Double getOutcu6() {
		return outcu6;
	}
	public void setOutcu6(Double outcu6) {
		this.outcu6 = outcu6;
	}
	public Double getOutcu7() {
		return outcu7;
	}
	public void setOutcu7(Double outcu7) {
		this.outcu7 = outcu7;
	}
	public Double getOutcu8() {
		return outcu8;
	}
	public void setOutcu8(Double outcu8) {
		this.outcu8 = outcu8;
	}
	public Double getOutcu9() {
		return outcu9;
	}
	public void setOutcu9(Double outcu9) {
		this.outcu9 = outcu9;
	}
	public Double getOutcu10() {
		return outcu10;
	}
	public void setOutcu10(Double outcu10) {
		this.outcu10 = outcu10;
	}
	public Double getOutcu11() {
		return outcu11;
	}
	public void setOutcu11(Double outcu11) {
		this.outcu11 = outcu11;
	}
	public Double getOutcu12() {
		return outcu12;
	}
	public void setOutcu12(Double outcu12) {
		this.outcu12 = outcu12;
	}
	public Double getOutcu13() {
		return outcu13;
	}
	public void setOutcu13(Double outcu13) {
		this.outcu13 = outcu13;
	}
	public Double getCostu1() {
		return costu1;
	}
	public void setCostu1(Double costu1) {
		this.costu1 = costu1;
	}
	public Double getCostu2() {
		return costu2;
	}
	public void setCostu2(Double costu2) {
		this.costu2 = costu2;
	}
	public Double getCostu3() {
		return costu3;
	}
	public void setCostu3(Double costu3) {
		this.costu3 = costu3;
	}
	public Double getCostu4() {
		return costu4;
	}
	public void setCostu4(Double costu4) {
		this.costu4 = costu4;
	}
	public Double getCostu5() {
		return costu5;
	}
	public void setCostu5(Double costu5) {
		this.costu5 = costu5;
	}
	public Double getCostu6() {
		return costu6;
	}
	public void setCostu6(Double costu6) {
		this.costu6 = costu6;
	}
	public Double getCostu7() {
		return costu7;
	}
	public void setCostu7(Double costu7) {
		this.costu7 = costu7;
	}
	public Double getCostu8() {
		return costu8;
	}
	public void setCostu8(Double costu8) {
		this.costu8 = costu8;
	}
	public Double getCostu9() {
		return costu9;
	}
	public void setCostu9(Double costu9) {
		this.costu9 = costu9;
	}
	public Double getCostu10() {
		return costu10;
	}
	public void setCostu10(Double costu10) {
		this.costu10 = costu10;
	}
	public Double getCostu11() {
		return costu11;
	}
	public void setCostu11(Double costu11) {
		this.costu11 = costu11;
	}
	public Double getCostu12() {
		return costu12;
	}
	public void setCostu12(Double costu12) {
		this.costu12 = costu12;
	}
	public Double getCnttrival() {
		return cnttrival;
	}
	public void setCnttrival(Double cnttrival) {
		this.cnttrival = cnttrival;
	}
	public Double getCntutrivaL() {
		return cntutrivaL;
	}
	public void setCntutrivaL(Double cntutrivaL) {
		this.cntutrivaL = cntutrivaL;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
}
