package com.choice.scm.domain;


public class AcctLineFirm {
	private String acct;
	private String code;//id
	private Positn firm;//分店
	private int rec;//排序
	private String tim;//时间
	private double timcost;//花费时间
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public Positn getFirm() {
		return firm;
	}
	public void setFirm(Positn firm) {
		this.firm = firm;
	}
	public int getRec() {
		return rec;
	}
	public void setRec(int rec) {
		this.rec = rec;
	}
	public String getTim() {
		return tim;
	}
	public void setTim(String tim) {
		this.tim = tim;
	}
	public double getTimcost() {
		return timcost;
	}
	public void setTimcost(double timcost) {
		this.timcost = timcost;
	}
	
	
}
