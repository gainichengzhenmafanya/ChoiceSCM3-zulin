package com.choice.scm.domain;

/**
 * 物资默认供应商
 */
public class SpCodeMod {

	private String acct;//帐套
	private String sp_code;//物资编码
	private String mod;//片区
	private String positn;//默认仓位
	private String positnex;//默认加工间
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getPositnex() {
		return positnex;
	}
	public void setPositnex(String positnex) {
		this.positnex = positnex;
	}
		
}
