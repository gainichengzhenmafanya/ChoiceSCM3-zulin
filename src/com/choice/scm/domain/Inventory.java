package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

//盘点
public class Inventory {

	private String acct;//帐套
	private Supply supply;//物资
	private List<String> sp_codes;
	private Positn positn;//仓位
	private String yearr;//年
	private String month;//月
	private Date date;//日期
	private Double cntbla;//数量
	private Double cntubla;//数量2
	private Double amtbla;// 金额
	private String unitper;//
	private Double cnttrival;//盘点数量
	private Double cntutrival;//盘点数量2
	private String pec;//     
	
	private String inabal;//   查询金额条件
	private String incbal;//   查询数量条件
	private String incubal;//   查询金额条件
	private String yndaypan;    //是否日盘点
	private String ynweekpan;   //是否月盘点      
	private String bgDay;		//开始日期
	private String endDay;		//结束日期
	private String ynjs;		//加工间是否结束盘点
	private List<Inventory> inventoryList;//list 盘点

	private int startNum;//延迟加载用 wjf
    private int endNum;
    
    private int pageSize;
    
    public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	private String orderBy;//排序字符串
    private String orderZd;//排序字段
    private String orderOd;//升序降序列
    
    private Date bdate;
    private Date edate;
    
	public Date getBdate() {
		return bdate;
	}
	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	public String getOrderZd() {
		return orderZd;
	}
	public void setOrderZd(String orderZd) {
		this.orderZd = orderZd;
	}
	public String getOrderOd() {
		return orderOd;
	}
	public void setOrderOd(String orderOd) {
		this.orderOd = orderOd;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public int getStartNum() {
		return startNum;
	}
	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}
	public List<String> getSp_codes() {
		return sp_codes;
	}

	public void setSp_codes(List<String> sp_codes) {
		this.sp_codes = sp_codes;
	}

	public String getAcct() {
		return acct;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public Supply getSupply() {
		return supply;
	}

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public Positn getPositn() {
		return positn;
	}

	public void setPositn(Positn positn) {
		this.positn = positn;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getCntbla() {
		return cntbla;
	}

	public void setCntbla(Double cntbla) {
		this.cntbla = cntbla;
	}

	public Double getCntubla() {
		return cntubla;
	}

	public void setCntubla(Double cntubla) {
		this.cntubla = cntubla;
	}

	public Double getAmtbla() {
		return amtbla;
	}

	public void setAmtbla(Double amtbla) {
		this.amtbla = amtbla;
	}

	public String getUnitper() {
		return unitper;
	}

	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}

	public Double getCnttrival() {
		return cnttrival;
	}

	public void setCnttrival(Double cnttrival) {
		this.cnttrival = cnttrival;
	}

	public Double getCntutrival() {
		return cntutrival;
	}

	public void setCntutrival(Double cntutrival) {
		this.cntutrival = cntutrival;
	}

	public String getPec() {
		return pec;
	}

	public void setPec(String pec) {
		this.pec = pec;
	}

	public String getInabal() {
		return inabal;
	}

	public void setInabal(String inabal) {
		this.inabal = inabal;
	}

	public String getIncbal() {
		return incbal;
	}

	public void setIncbal(String incbal) {
		this.incbal = incbal;
	}

	public String getIncubal() {
		return incubal;
	}

	public void setIncubal(String incubal) {
		this.incubal = incubal;
	}

	public List<Inventory> getInventoryList() {
		return inventoryList;
	}

	public void setInventoryList(List<Inventory> inventoryList) {
		this.inventoryList = inventoryList;
	}

	public String getYndaypan() {
		return yndaypan;
	}

	public void setYndaypan(String yndaypan) {
		this.yndaypan = yndaypan;
	}

	public String getYnweekpan() {
		return ynweekpan;
	}

	public void setYnweekpan(String ynweekpan) {
		this.ynweekpan = ynweekpan;
	}

	public String getBgDay() {
		return bgDay;
	}

	public void setBgDay(String bgDay) {
		this.bgDay = bgDay;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public String getYnjs() {
		return ynjs;
	}

	public void setYnjs(String ynjs) {
		this.ynjs = ynjs;
	}
	
}
