package com.choice.scm.domain;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 大类
 * @author yp
 */

@Component("grpTyp")
@Scope("prototype")
public class GrpTyp {
	
	private String acct;  //帐套
	private String code;  //编码
	private String des;	  //名称
	private String locked;//是否已经使用
	private String oldCode;//更新记录时暂存更新前的code值
	private List<Grp> grpList;//物资中类  wjf
	private String grpcode;
	
	public GrpTyp(){
		this.locked = "N";
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getLocked() {
		return locked;
	}
	public void setLocked(String locked) {
		this.locked = locked;
	}
	public String getOldCode() {
		return oldCode;
	}
	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	public List<Grp> getGrpList() {
		return grpList;
	}
	public void setGrpList(List<Grp> grpList) {
		this.grpList = grpList;
	}
	public String getGrpcode() {
		return grpcode;
	}
	public void setGrpcode(String grpcode) {
		this.grpcode = grpcode;
	}
	
	
}
