package com.choice.scm.domain;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 中类
 * @author yp
 */

@Component("grp")
@Scope("prototype")
public class Grp {
	
	private String acct;  //帐套
	private String grptyp;//大类编码
	private String code;  //编码
	private String des;	  //名称
	private String locked;//是否已用
	private Double amt;	  //金额
	private String cost;  //是否计算成本
	private String oldCode;//更新记录时暂存更新前的code值
	private String typ; //物料属性   add wangjie 2014年10月28日 11:14:43
	private List<Typ> typList;//小类
	
	public Grp(){
		this.locked = "N";
		this.cost = "N";
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getLocked() {
		return locked;
	}
	public void setLocked(String locked) {
		this.locked = locked;
	}
	public Double getAmt() {
		return amt;
	}
	public void setAmt(Double amt) {
		this.amt = amt;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getOldCode() {
		return oldCode;
	}
	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public List<Typ> getTypList() {
		return typList;
	}
	public void setTypList(List<Typ> typList) {
		this.typList = typList;
	}
	
}
