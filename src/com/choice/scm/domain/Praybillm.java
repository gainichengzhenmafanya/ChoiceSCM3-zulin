package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.choice.framework.util.DateFormat;

/**
 * 请购单--主表
 * @author 王吉峰
 *
 */
public class Praybillm {

	private List<Praybilld> praybilldList;
	private String acct; //帐套号
	private String yearr; //年度
	private Integer praybillmNo; //申购单主键，流水号	
	private Date maded; //填制日期
	private String madet; //时间
	private Date checd; //审核日期
	private String chect; //审核时间
	private String madeby; //填制人
	private String checby; //审核人
	private String vouno; //凭证号 ，规则算出的 一般为年度日期-001
	private String dept; //申购的部门编号
	private String firm; //申购分店
	private Positn positn; //仓位，向哪个仓位申购。用处不大，目前默认都是主仓位
	private List<String> listPositn;
	private double totalAmt; //合计金额 明细数据的合计金额
	private String status; //状态
	private String prclass;//请购类型(普通请购)
	private String prfrom;//请购来源(自制，报货分拨)
	private Integer pr;         //用来接收存储过程的输出标识
	private Integer chkCount;   //接收查询出来的数量
	
	private Date bdate;//查询用
	private Date edate;
	
	public Praybillm() {
		super();
	}
	public Praybillm(Integer praybillmNo) {
		super();
		this.praybillmNo = praybillmNo;
	}

	public List<String> getListPositn() {
		return listPositn;
	}
	public void setListPositn(List<String> listPositn) {
		this.listPositn = listPositn;
	}
	public List<Praybilld> getPraybilldList() {
		return praybilldList;
	}
	public void setPraybilldList(List<Praybilld> praybilldList) {
		this.praybilldList = praybilldList;
	}
	public Integer getPraybillmNo() {
		return praybillmNo;
	}
	public void setPraybillmNo(Integer praybillmNo) {
		this.praybillmNo = praybillmNo;
	}
	public Integer getChkCount() {
		return chkCount;
	}
	public void setChkCount(Integer chkCount) {
		this.chkCount = chkCount;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public String getChect() {
		return chect;
	}
	public void setChect(String chect) {
		this.chect = chect;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getBdate() {
		return bdate;
	}
	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public String getPrclass() {
		return prclass;
	}
	public void setPrclass(String prclass) {
		this.prclass = prclass;
	}
	public String getPrfrom() {
		return prfrom;
	}
	public void setPrfrom(String prfrom) {
		this.prfrom = prfrom;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Praybillm.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Positn){
						result.append(((Positn)cur).getCode());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
}
