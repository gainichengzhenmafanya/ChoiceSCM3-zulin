package com.choice.scm.domain;

import java.math.BigInteger;
import java.util.Date;

/***
 * 模版类型
 * @author ghc
 *
 */
public class Template {
	/*
	 * 主键
	 */
	private Integer id;			
	/*
	 * 模版编码
	 */
	private String code;
	/*
	 * 模版名称
	 */
	private String name;
	/*
	 * 模版路径
	 */
	private String path;
	/*
	 * 模版主题（保留字段）
	 */
	private String title;
	/*
	 *引用者ID
	 */
	private String superId;
	/*
	 * 集团ID
	 */
	private String tenantId;
	/*
	 * 创建人
	 */
	private String create_user;
	/*
	 * 修改人
	 */
	private String update_user;
	/*
	 * 创建时间
	 */
	private Date create_date;
	/*
	 * 修改时间
	 */
	private Date update_date;
	/*
	 * 保留字段
	 */
	private String sha1;
	/*
	 * md5加密  保留
	 */
	private String md5;
	/*
	 * 文件名称
	 */
	private String realName;
	/*
	 * 文件大小
	 */
	private BigInteger size;
	/*
	 * 类型  1 为总部   2 为门店
	 */
	private String type;
	/*
	 * 文件类型  保留
	 */
	private String lastModifiedDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSuperId() {
		return superId;
	}
	public void setSuperId(String superId) {
		this.superId = superId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public String getSha1() {
		return sha1;
	}
	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public BigInteger getSize() {
		return size;
	}
	public void setSize(BigInteger size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	@Override
	public String toString() {
		return "Template [id=" + id + ", code=" + code + ", name=" + name + ", path=" + path + ", title=" + title
				+ ", superId=" + superId + ", tenantId=" + tenantId + ", create_user=" + create_user + ", update_user="
				+ update_user + ", create_date=" + create_date + ", update_date=" + update_date + ", sha1=" + sha1
				+ ", md5=" + md5 + ", realName=" + realName + ", size=" + size + ", type=" + type
				+ ", lastModifiedDate=" + lastModifiedDate + "]";
	}
	
	
	
}
