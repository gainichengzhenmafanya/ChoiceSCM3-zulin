package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 售价单
 * @author csb
 *
 */
public class SppriceSale {

	private String 	acct		;//帐套
	private Supply 	supply		;//物资
	private Date 	bdat		;//开始日期
	private Date 	edat		;//结束日期
	private String 	madet		;//制单时间
	private Double 	price		;//价格
	private Double 	priceold	;//上次价格
	private Deliver deliver		;//供应商
	private String 	memo		;//备注
	private String 	sta			;//状态
	private String rec			;//序号
	private String 	emp			;//
	private Positn 	area		;//区域
	private String 	checby		;//审核人
	private String 	chect		;//审核时间
	private String 	downsta		;//
	private String 	downsta1	;//
	
	/*查询时用  wangjie 01.14*/
	private String sp_name;	//物资名称
	private String sp_desc;	//规格
	private String sp_code;//物资编码
	//全部反审核的查询过滤条件
	private String nodate;
	
	private Integer id = 0;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNodate() {
		return nodate;
	}
	public void setNodate(String nodate) {
		this.nodate = nodate;
	}
	private List<SppriceSale> sppriceSaleList;
	private List<Positn> positnList;
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getPriceold() {
		return priceold;
	}
	public void setPriceold(Double priceold) {
		this.priceold = priceold;
	}
	public Deliver getDeliver() {
		return deliver;
	}
	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getRec() {
		return rec;
	}
	public void setRec(String rec) {
		this.rec = rec;
	}
	public String getEmp() {
		return emp;
	}
	public void setEmp(String emp) {
		this.emp = emp;
	}
	public Positn getArea() {
		return area;
	}
	public void setArea(Positn area) {
		this.area = area;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getChect() {
		return chect;
	}
	public void setChect(String chect) {
		this.chect = chect;
	}
	public String getDownsta() {
		return downsta;
	}
	public void setDownsta(String downsta) {
		this.downsta = downsta;
	}
	public String getDownsta1() {
		return downsta1;
	}
	public void setDownsta1(String downsta1) {
		this.downsta1 = downsta1;
	}
		public List<SppriceSale> getSppriceSaleList() {
		return sppriceSaleList;
	}
	public void setSppriceSaleList(List<SppriceSale> sppriceSaleList) {
		this.sppriceSaleList = sppriceSaleList;
	}
	public List<Positn> getPositnList() {
		return positnList;
	}
	public void setPositnList(List<Positn> positnList) {
		this.positnList = positnList;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	
}
