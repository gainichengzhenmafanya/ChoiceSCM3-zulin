package com.choice.scm.domain;



/**
 * 配送班表 从
 *
 */
public class ScheduleD {
	private Integer scheduleDetailsID;		//id	
	private String category_Code;			//物资一级类别
	private String category;				//类别名称
	private Integer scheduleID;				//配送班id
	private String orderDate;					//订货日期
	private String orderDate_NY;					//订货日期
	private String orderTime;					//订货时间
	private String orderWeekday;			//周次
	private String orderColor;				//显示颜色
	private String receiveDate;				//到货日期
	private String receiveDate_NY;				//到货日期
	private String receiveTime;				//到货时间
	private String receiveWeekday;			//周次
	private String receiveColor;			//到货颜色
	private String positnCode;//门店编码
	
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	public String getOrderDate_NY() {
		return orderDate_NY;
	}
	public void setOrderDate_NY(String orderDate_NY) {
		this.orderDate_NY = orderDate_NY;
	}
	public String getReceiveDate_NY() {
		return receiveDate_NY;
	}
	public void setReceiveDate_NY(String receiveDate_NY) {
		this.receiveDate_NY = receiveDate_NY;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getScheduleDetailsID() {
		return scheduleDetailsID;
	}
	public void setScheduleDetailsID(Integer scheduleDetailsID) {
		this.scheduleDetailsID = scheduleDetailsID;
	}
	public String getCategory_Code() {
		return category_Code;
	}
	public void setCategory_Code(String category_Code) {
		this.category_Code = category_Code;
	}
	public Integer getScheduleID() {
		return scheduleID;
	}
	public void setScheduleID(Integer scheduleID) {
		this.scheduleID = scheduleID;
	}
	public String getOrderWeekday() {
		return orderWeekday;
	}
	public void setOrderWeekday(String orderWeekday) {
		this.orderWeekday = orderWeekday;
	}
	public String getOrderColor() {
		return orderColor;
	}
	public void setOrderColor(String orderColor) {
		this.orderColor = orderColor;
	}
	public String getReceiveWeekday() {
		return receiveWeekday;
	}
	public void setReceiveWeekday(String receiveWeekday) {
		this.receiveWeekday = receiveWeekday;
	}
	public String getReceiveColor() {
		return receiveColor;
	}
	public void setReceiveColor(String receiveColor) {
		this.receiveColor = receiveColor;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	
	 
}
