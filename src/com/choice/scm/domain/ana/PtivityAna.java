package com.choice.scm.domain.ana;

import java.util.Date;
import java.util.Map;

/**
 * 成本报表应产率分析查询条件
 * @author lq
 *
 */
public class PtivityAna {

	private String acct;
	private String yearr;
	private Date bdat;//开始日期
	private Date edat;//结束日期
	private String positn;//分店
	private String sp_code;//物资编码
	private String grptyp;//大类
	private String grp;//中类
	private String typ;//小类
	private String onlyjp;//仅精品
	private String supplyacct;			//表名选择
	private String costitem;		//表名选择
	private String firm;//门店
	private Map<String,String> params;
	private String pr;
	private String ccl;//查什么应产率
	private String sort;
	private String order;

	public Map<String, String> getParams() {
		return params;
	}
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	public String getSupplyacct() {
		return supplyacct;
	}
	public void setSupplyacct(String supplyacct) {
		this.supplyacct = supplyacct;
	}
	public String getCostitem() {
		return costitem;
	}
	public void setCostitem(String costitem) {
		this.costitem = costitem;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getOnlyjp() {
		return onlyjp;
	}
	public void setOnlyjp(String onlyjp) {
		this.onlyjp = onlyjp;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	public String getCcl() {
		return ccl;
	}
	public void setCcl(String ccl) {
		this.ccl = ccl;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
}