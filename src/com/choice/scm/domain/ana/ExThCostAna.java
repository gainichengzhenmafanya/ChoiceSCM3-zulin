package com.choice.scm.domain.ana;

import java.util.Date;

/**
 * 产品理论成本分析   报表
 * @author lehui
 *
 */
public class ExThCostAna {

	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private String sp_desc;//规格
	private String unit;//单位
	private Double amount;//产量
	private Double price;//单价
	private Double amt;//金额
	private Double costamt;//理论成本
	private double mao;//毛利
	private String maol;//毛利率
	
	//查询条件
	private Date  dat;//开始时间
	private Date  datEnd;//结束时间
	private String positnex;//加工间编码
	private String positnexdes;//加工间名称
	private String grp;//中类编码
	private String grpdes;//中类名称
	
	//排序
	private String sortName;
	private String sortOrder;
	
	public double getMao() {
		return mao;
	}
	public void setMao(double mao) {
		this.mao = mao;
	}
	public String getMaol() {
		return maol;
	}
	public void setMaol(String maol) {
		this.maol = maol;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getPositnex() {
		return positnex;
	}
	public void setPositnex(String positnex) {
		this.positnex = positnex;
	}
	public String getPositnexdes() {
		return positnexdes;
	}
	public void setPositnexdes(String positnexdes) {
		this.positnexdes = positnexdes;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getGrpdes() {
		return grpdes;
	}
	public void setGrpdes(String grpdes) {
		this.grpdes = grpdes;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public Date getDatEnd() {
		return datEnd;
	}
	public void setDatEnd(Date datEnd) {
		this.datEnd = datEnd;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmt() {
		return amt;
	}
	public void setAmt(Double amt) {
		this.amt = amt;
	}
	public Double getCostamt() {
		return costamt;
	}
	public void setCostamt(Double costamt) {
		this.costamt = costamt;
	}
	
	
	
	
}
