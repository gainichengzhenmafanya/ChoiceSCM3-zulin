package com.choice.scm.domain.ana;

import java.util.List;

import com.choice.scm.domain.Dis;
/**
 * 部分字段批量修改用
 * @author csb
 *
 */
public class DisList {

	private List<Dis> listDis;

	public List<Dis> getListDis() {
		return listDis;
	}

	public void setListDis(List<Dis> listDis) {
		this.listDis = listDis;
	}
	
}
