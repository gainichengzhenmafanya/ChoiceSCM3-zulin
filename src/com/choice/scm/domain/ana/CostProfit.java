package com.choice.scm.domain.ana;

import java.util.Date;

/**
 * 产品理论成本分析   报表
 * @author css
 *
 */
public class CostProfit {
	
	//查询条件
	private Date  bdat;//开始时间
	private Date  edat;//结束时间
	private String positn;//加工间编码
	private String dept;//部门
	private String costitem;		//表名选择
	
	//拼的sql
	private String caseF;//
	private String sumF;//
	
	private String chengben;
	private String showtyp;
	
	private String firmDes;
	
	public String getFirmDes() {
		return firmDes;
	}
	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}
	public String getChengben() {
		return chengben;
	}
	public void setChengben(String chengben) {
		this.chengben = chengben;
	}
	public String getShowtyp() {
		return showtyp;
	}
	public void setShowtyp(String showtyp) {
		this.showtyp = showtyp;
	}
	public String getCostitem() {
		return costitem;
	}
	public void setCostitem(String costitem) {
		this.costitem = costitem;
	}
	public String getCaseF() {
		return caseF;
	}
	public void setCaseF(String caseF) {
		this.caseF = caseF;
	}
	public String getSumF() {
		return sumF;
	}
	public void setSumF(String sumF) {
		this.sumF = sumF;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
}
