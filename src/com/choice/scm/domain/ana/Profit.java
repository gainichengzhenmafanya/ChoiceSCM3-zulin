package com.choice.scm.domain.ana;

import java.io.Serializable;
import java.util.Date;

/**
 * 毛利率日分析   报表
 * @author css
 *
 */
public class Profit implements Serializable{
	
	private static final long serialVersionUID = -8015992538220004764L;
	private String firmcode;    	//分店编号
	private String firmdes; 		//分店名
	private String dat;				//日期
	private String week;			//星期几
	private String amt;				//销售金额
	private String cost;			//实际成本
	private String maorate;			//毛利率
	private String theorycost;		//理论成本
	private String theorymaorate;	//理论毛利率
	private String costdiff;		//分成本差异
	private String costdiffrate;	//分差异率
	private String inamt;			//期初
	private String bperiod;			//本期入
	private String balances;		//本期结存
	
	private String supplyacct;			//表名选择
	private String costitem;		//表名选择
	
	//查询条件
	private Date  bdat;//开始时间
	private Date  edat;//结束时间
	private String positnex;//分店编码
	private String positnexdes;//分店名称
	private String dept;//部门
	private String costby;//实际成本计算方法
	private String firmDept;//根据分店选择部门
	
	//排序
	private String sortName;
	private String sortOrder;
		
	public String getSupplyacct() {
		return supplyacct;
	}
	public void setSupplyacct(String supplyacct) {
		this.supplyacct = supplyacct;
	}
	public String getCostitem() {
		return costitem;
	}
	public void setCostitem(String costitem) {
		this.costitem = costitem;
	}
	public String getFirmDept() {
		return firmDept;
	}
	public void setFirmDept(String firmDept) {
		this.firmDept = firmDept;
	}
	public String getSortName() {
		return sortName;
	}
	public String getInamt() {
		return inamt;
	}
	public void setInamt(String inamt) {
		this.inamt = inamt;
	}
	public String getBperiod() {
		return bperiod;
	}
	public void setBperiod(String bperiod) {
		this.bperiod = bperiod;
	}
	public String getBalances() {
		return balances;
	}
	public void setBalances(String balances) {
		this.balances = balances;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getCostby() {
		return costby;
	}
	public void setCostby(String costby) {
		this.costby = costby;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getFirmcode() {
		return firmcode;
	}
	public void setFirmcode(String firmcode) {
		this.firmcode = firmcode;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public String getDat() {
		return dat;
	}
	public void setDat(String dat) {
		this.dat = dat;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getMaorate() {
		return maorate;
	}
	public void setMaorate(String maorate) {
		this.maorate = maorate;
	}
	public String getTheorycost() {
		return theorycost;
	}
	public void setTheorycost(String theorycost) {
		this.theorycost = theorycost;
	}
	public String getTheorymaorate() {
		return theorymaorate;
	}
	public void setTheorymaorate(String theorymaorate) {
		this.theorymaorate = theorymaorate;
	}
	public String getCostdiff() {
		return costdiff;
	}
	public void setCostdiff(String costdiff) {
		this.costdiff = costdiff;
	}
	public String getCostdiffrate() {
		return costdiffrate;
	}
	public void setCostdiffrate(String costdiffrate) {
		this.costdiffrate = costdiffrate;
	}
	public String getPositnexdes() {
		return positnexdes;
	}
	public void setPositnexdes(String positnexdes) {
		this.positnexdes = positnexdes;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getPositnex() {
		return positnex;
	}
	public void setPositnex(String positnex) {
		this.positnex = positnex;
	}
}
