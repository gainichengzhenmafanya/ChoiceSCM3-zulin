package com.choice.scm.domain.ana;

import java.util.Date;

/**
 * 生产耗用
 * @author csb
 *
 */
public class ProCostAna {

	
	private String sp_name  ; //物资名称				
	private String sp_desc  ; //规格						
	private String unit	    ; //单位					
	private double qcprice   ; //期初|单价
	private double qccnt		; //期初|数量
	private double qcamt		; //期初|金额
	private double inprice   ; //入库|单价
	private double incnt		; //入库|数量
	private double inamt		; //入库|金额
	private double shprice   ; //实际耗用|单价
	private double shcnt		; //实际耗用|数量
	private double shamt		; //实际耗用|金额
	private double sjprice   ; //实际结存|单价
	private double sjcnt		; //实际结存|数量
	private double sjamt		; //实际结存|金额
	private double lhcnt		; //理论耗用|数量
	private double lhamt		; //理论耗用|金额
	private double difcnt	; //差异|数量
	private double difamt	; //差异|金额
	private String difrate  ; //差异|差异率			
 
	/**
	 * 查询条件
	 */
	private Date bdat;//开始日期
	private Date edat;//结束日期
	private String firm;//部门
	private String sp_code  ; //物资编码				
	private String grptyp;//大类
	private String grp;//中类
	private String typ;//小类
	private String ifZero;// 是否过滤掉 数据为0的列  1表示是  0表示 wnagjie 2014年12月10日 16:19:00
	
	private String sort;
	private String order;
	
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getQcprice() {
		return qcprice;
	}
	public void setQcprice(double qcprice) {
		this.qcprice = qcprice;
	}
	public double getQccnt() {
		return qccnt;
	}
	public void setQccnt(double qccnt) {
		this.qccnt = qccnt;
	}
	public double getQcamt() {
		return qcamt;
	}
	public void setQcamt(double qcamt) {
		this.qcamt = qcamt;
	}
	public double getInprice() {
		return inprice;
	}
	public void setInprice(double inprice) {
		this.inprice = inprice;
	}
	public double getIncnt() {
		return incnt;
	}
	public void setIncnt(double incnt) {
		this.incnt = incnt;
	}
	public double getInamt() {
		return inamt;
	}
	public void setInamt(double inamt) {
		this.inamt = inamt;
	}
	public double getShprice() {
		return shprice;
	}
	public void setShprice(double shprice) {
		this.shprice = shprice;
	}
	public double getShcnt() {
		return shcnt;
	}
	public void setShcnt(double shcnt) {
		this.shcnt = shcnt;
	}
	public double getShamt() {
		return shamt;
	}
	public void setShamt(double shamt) {
		this.shamt = shamt;
	}
	public double getSjprice() {
		return sjprice;
	}
	public void setSjprice(double sjprice) {
		this.sjprice = sjprice;
	}
	public double getSjcnt() {
		return sjcnt;
	}
	public void setSjcnt(double sjcnt) {
		this.sjcnt = sjcnt;
	}
	public double getSjamt() {
		return sjamt;
	}
	public void setSjamt(double sjamt) {
		this.sjamt = sjamt;
	}
	public double getLhcnt() {
		return lhcnt;
	}
	public void setLhcnt(double lhcnt) {
		this.lhcnt = lhcnt;
	}
	public double getLhamt() {
		return lhamt;
	}
	public void setLhamt(double lhamt) {
		this.lhamt = lhamt;
	}
	public double getDifcnt() {
		return difcnt;
	}
	public void setDifcnt(double difcnt) {
		this.difcnt = difcnt;
	}
	public double getDifamt() {
		return difamt;
	}
	public void setDifamt(double difamt) {
		this.difamt = difamt;
	}
	public String getDifrate() {
		return difrate;
	}
	public void setDifrate(String difrate) {
		this.difrate = difrate;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getIfZero() {
		return ifZero;
	}
	public void setIfZero(String ifZero) {
		this.ifZero = ifZero;
	}
	
}
