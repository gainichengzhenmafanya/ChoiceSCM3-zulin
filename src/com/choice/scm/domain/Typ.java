package com.choice.scm.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 物资小类
 * @author yp
 */

@Component("typ")
@Scope("prototype")
public class Typ {
	
	private String acct;  // 帐套
	private String grptyp;//大类
	private String grp;   //二级类
	private String code;  //编码
	private String des;   //名称
	private String locked;//是否已用
	private String chktag;//暂未用
	private Integer levl;  //暂未用
	private String oldCode;//更新记录时暂存更新前的code值
	
	public Typ(){
		this.locked = "N";
		this.chktag = "N";
		this.levl = Integer.valueOf(0);
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getLocked() {
		return locked;
	}
	public void setLocked(String locked) {
		this.locked = locked;
	}
	public String getChktag() {
		return chktag;
	}
	public void setChktag(String chktag) {
		this.chktag = chktag;
	}
	public Integer getLevl() {
		return levl;
	}
	public void setLevl(Integer levl) {
		this.levl = levl;
	}
	public String getOldCode() {
		return oldCode;
	}
	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	
}
