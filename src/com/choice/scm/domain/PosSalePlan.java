package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 营业预估
 * @author css
 *
 */
public class PosSalePlan {   

	private List<PosSalePlan> PosSalePlanList;   //修改用
	private List<String> firmList;   //分店号list
	private String firm;//分店号
	private String firmNm;//分店名
	private Date dat;//日期
	private Date startdate;//起始日期
	private Date enddate;//结束日期
	private String week;//周
	private String memo;//备注
	private double pax;// 实际
	
	private double pax1;//一班调整
	private double pax2;//二班调整
	private double pax3;//三班调整
	private double pax4;//四班调整
	
	private double pax1old;//一班预估
	private double pax2old;//二班预估
	private double pax3old;//三班预估
	private double pax4old;//四班预估
	
	private double pax1act;// 一班实际
	private double pax2act;// 二班实际
	private double pax3act;// 三班实际
	private double pax4act;// 四班实际
	
	private double lv1;// 一班预估差异率
	private double lv2;// 二班预估差异率
	private double lv3;// 三班预估差异率
	private double lv4;// 四班预估差异率
	
	private double lv1old;// 一班调整差异率
	private double lv2old;// 二班调整差异率
	private double lv3old;// 三班调整差异率
	private double lv4old;// 四班调整差异率
	
	private double total;// 合计调整
	private double totalold;// 合计预估
	private double totalact;// 合计实际
	private double totallv;// 合计预估差异率
	private double totallvold;// 合计预估差异率
	private String ynZb;//是否总部
	private String area;//区域
	
	private String dept;//档口编码
	private String deptdes;//档口名称
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getYnZb() {
		return ynZb;
	}
	public void setYnZb(String ynZb) {
		this.ynZb = ynZb;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFirmNm() {
		return firmNm;
	}
	public void setFirmNm(String firmNm) {
		this.firmNm = firmNm;
	}
	public List<String> getFirmList() {
		return firmList;
	}
	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}
	public double getTotalact() {
		return totalact;
	}
	public void setTotalact(double totalact) {
		this.totalact = totalact;
	}
	public double getTotallv() {
		return totallv;
	}
	public void setTotallv(double totallv) {
		this.totallv = totallv;
	}
	public double getTotallvold() {
		return totallvold;
	}
	public void setTotallvold(double totallvold) {
		this.totallvold = totallvold;
	}
	public double getPax() {
		return pax;
	}
	public void setPax(double pax) {
		this.pax = pax;
	}
	public double getPax1act() {
		return pax1act;
	}
	public void setPax1act(double pax1act) {
		this.pax1act = pax1act;
	}
	public double getPax2act() {
		return pax2act;
	}
	public void setPax2act(double pax2act) {
		this.pax2act = pax2act;
	}
	public double getPax3act() {
		return pax3act;
	}
	public void setPax3act(double pax3act) {
		this.pax3act = pax3act;
	}
	public double getPax4act() {
		return pax4act;
	}
	public void setPax4act(double pax4act) {
		this.pax4act = pax4act;
	}
	public double getLv1() {
		return lv1;
	}
	public void setLv1(double lv1) {
		this.lv1 = lv1;
	}
	public double getLv2() {
		return lv2;
	}
	public void setLv2(double lv2) {
		this.lv2 = lv2;
	}
	public double getLv3() {
		return lv3;
	}
	public void setLv3(double lv3) {
		this.lv3 = lv3;
	}
	public double getLv4() {
		return lv4;
	}
	public void setLv4(double lv4) {
		this.lv4 = lv4;
	}
	public double getLv1old() {
		return lv1old;
	}
	public void setLv1old(double lv1old) {
		this.lv1old = lv1old;
	}
	public double getLv2old() {
		return lv2old;
	}
	public void setLv2old(double lv2old) {
		this.lv2old = lv2old;
	}
	public double getLv3old() {
		return lv3old;
	}
	public void setLv3old(double lv3old) {
		this.lv3old = lv3old;
	}
	public double getLv4old() {
		return lv4old;
	}
	public void setLv4old(double lv4old) {
		this.lv4old = lv4old;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getTotalold() {
		return totalold;
	}
	public void setTotalold(double totalold) {
		this.totalold = totalold;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public List<PosSalePlan> getPosSalePlanList() {
		return PosSalePlanList;
	}
	public void setPosSalePlanList(List<PosSalePlan> posSalePlanList) {
		PosSalePlanList = posSalePlanList;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}

	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getPax1() {
		return pax1;
	}
	public void setPax1(double pax1) {
		this.pax1 = pax1;
	}
	public double getPax2() {
		return pax2;
	}
	public void setPax2(double pax2) {
		this.pax2 = pax2;
	}
	public double getPax3() {
		return pax3;
	}
	public void setPax3(double pax3) {
		this.pax3 = pax3;
	}
	public double getPax4() {
		return pax4;
	}
	public void setPax4(double pax4) {
		this.pax4 = pax4;
	}
	public double getPax1old() {
		return pax1old;
	}
	public void setPax1old(double pax1old) {
		this.pax1old = pax1old;
	}
	public double getPax2old() {
		return pax2old;
	}
	public void setPax2old(double pax2old) {
		this.pax2old = pax2old;
	}
	public double getPax3old() {
		return pax3old;
	}
	public void setPax3old(double pax3old) {
		this.pax3old = pax3old;
	}
	public double getPax4old() {
		return pax4old;
	}
	public void setPax4old(double pax4old) {
		this.pax4old = pax4old;
	}
	
	
}
