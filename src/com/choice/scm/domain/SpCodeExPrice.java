package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

public class SpCodeExPrice {
	private String acct; //表示
	private String yearr; //年
	private String monthh;//月
	private Supply supply;//
	private Double price;//
	private Double priceold;//
	private Double amount;//
	private Double amt;//
	private Double extim;//
	private Double spcost;//
	private Double feicost;//
	private Date bdat;//开始日期
	private Date edat;//结束日期
	private String positnCode;//加工间编码
	private List<SpCodeExPrice> spCodeExPriceList;
	
	private Double extimSum;//累计加工工时用 2015.1.20wjf
	
	private  int  pr;

	public int getPr() {
		return pr;
	}
	public void setPr(int pr) {
		this.pr = pr;
	}
	public List<SpCodeExPrice> getSpCodeExPriceList() {
		return spCodeExPriceList;
	}	
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmt() {
		return amt;
	}
	public void setAmt(Double amt) {
		this.amt = amt;
	}
	public Double getSpcost() {
		return spcost;
	}
	public void setSpcost(Double spcost) {
		this.spcost = spcost;
	}
	public Double getFeicost() {
		return feicost;
	}
	public void setFeicost(Double feicost) {
		this.feicost = feicost;
	}
	public void setPriceold(Double priceold) {
		this.priceold = priceold;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public void setSpCodeExPriceList(List<SpCodeExPrice> spCodeExPriceList) {
		this.spCodeExPriceList = spCodeExPriceList;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getMonthh() {
		return monthh;
	}
	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public Double getPriceold() {
		return priceold;
	}
	public Double getAmount() {
		return amount;
	}
	public Double getExtim() {
		return extim;
	}
	public void setExtim(Double extim) {
		this.extim = extim;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public Double getExtimSum() {
		return extimSum;
	}
	public void setExtimSum(Double extimSum) {
		this.extimSum = extimSum;
	}
}
