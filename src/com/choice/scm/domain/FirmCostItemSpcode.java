package com.choice.scm.domain;

import java.util.Date;
/**
 * 分店成本
 * @author csb
 *
 */
public class FirmCostItemSpcode {
	private Integer	rec					;//序号			
	private Integer	costitemrec			;			
	private String	acct				;//帐套		
	private String	firm				;//分店		
	private Integer	item				;			
	private String	unit				;//单位		
	private String	dept1				;//部门		
	private String	sp_code				;//物资编码		
	private Double	cnt					;//数量
	private Double	cnt1				;//数量2
	private Double	price				;//价格
	private String	chkcost				;//是否做成本卡	
	private String	tx					;//菜品类别		
	private Date	dat					;//日期
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public Integer getCostitemrec() {
		return costitemrec;
	}
	public void setCostitemrec(Integer costitemrec) {
		this.costitemrec = costitemrec;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDept1() {
		return dept1;
	}
	public void setDept1(String dept1) {
		this.dept1 = dept1;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getChkcost() {
		return chkcost;
	}
	public void setChkcost(String chkcost) {
		this.chkcost = chkcost;
	}
	public String getTx() {
		return tx;
	}
	public void setTx(String tx) {
		this.tx = tx;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}	
	
}
