package com.choice.scm.domain;

/***
 * 用户报货分拨权限
 * @author 王吉峰
 *
 */
public class AccountDis {
	
	private Integer id;			//主键
	private String accountId;	//用户id
	private String ynzp;		//是否可修改直配方向的价格
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getYnzp() {
		return ynzp;
	}
	public void setYnzp(String ynzp) {
		this.ynzp = ynzp;
	}
	
}
