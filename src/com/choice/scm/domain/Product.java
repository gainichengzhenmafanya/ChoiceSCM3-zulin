package com.choice.scm.domain;

import java.util.List;

public class Product {
	private String  acct;       // 帐套
	private String item;	    // 半成品编码
	private String itemDes;	    // 半成品名称
	private Supply supply;		// 物资
//	private String sp_code;	    // 对应的物资编码
//	private String sp_name;	    // 对应的物资名称
//	private String sp_desc;	    // 对应的物资规格
	private Double exrate;		// 取料率
//	private String unit2;	    // 成本单位
	private Double excnt;	    // 成本净用量
	private Double cnt2;	        // 成本毛用量
//	private String unit;	    // 标准单位
	private String unit;        // 餐饮半成品单位
	private Double cnt;		    // 标准毛用量
//	private double pricesale;	// 单价
	private Double sale;	        // 金额
//	private String unit1;	    // 参考单位
	private Double cnt1;		    // 参考毛用量
	private Integer pr;         // 接收存储过程；
	private String curStatus;
	private String status;      // 状态，暂无用
	private Explan explan;    	//生成计划
	private String memo;//备注
	private String ex;//判断门店加工品/总部加工品 wjf 报表导出判断
	private String ex1;//判断门店加工品/总部加工品 wjf 报表导出判断
	private double cntMx;
	private String kc;//库存
	
	public String getKc() {
		return kc;
	}
	public void setKc(String kc) {
		this.kc = kc;
	}
	public String getItemDes() {
		return itemDes;
	}
	public void setItemDes(String itemDes) {
		this.itemDes = itemDes;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	private List<Product> ProductList;
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public Double getExrate() {
		return exrate;
	}
	public void setExrate(Double exrate) {
		this.exrate = exrate;
	}
	public Double getExcnt() {
		return excnt;
	}
	public void setExcnt(Double excnt) {
		this.excnt = excnt;
	}
	public Double getCnt2() {
		return cnt2;
	}
	public void setCnt2(Double cnt2) {
		this.cnt2 = cnt2;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getSale() {
		return sale;
	}
	public void setSale(Double sale) {
		this.sale = sale;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public String getCurStatus() {
		return curStatus;
	}
	public void setCurStatus(String curStatus) {
		this.curStatus = curStatus;
	}
	public Explan getExplan() {
		return explan;
	}
	public void setExplan(Explan explan) {
		this.explan = explan;
	}
	public List<Product> getProductList() {
		return ProductList;
	}
	public void setProductList(List<Product> productList) {
		ProductList = productList;
	}
	public String getEx() {
		return ex;
	}
	public void setEx(String ex) {
		this.ex = ex;
	}
	public String getEx1() {
		return ex1;
	}
	public void setEx1(String ex1) {
		this.ex1 = ex1;
	}
	public double getCntMx() {
		return cntMx;
	}
	public void setCntMx(double cntMx) {
		this.cntMx = cntMx;
	}
	
}
