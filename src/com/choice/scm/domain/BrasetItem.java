package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 分店结算-支付明细
 * Created by mc on 14-12-31.
 */
public class BrasetItem {
    private String acct;    //帐套
    private Integer id;      //
    private Date dat;     //时间
    private String deliver; //仓库
    private double amtacct; //
    private double amtpay;  //支付金额
    private double amtadj;  //
    private String memo;    //备注
    private String madeby;  //操作员
    private String firm;    //门店
    private Integer chkno;   //单据号
    private Integer chktag;  //
    private double t_amtpay;//

    private String invalue;//入库单号
    private String outvalue;//出库单号
    private List<String> invalueList;//入库单号
    private List<String> outvalueList;//出库单号

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDat() {
        return dat;
    }

    public void setDat(Date dat) {
        this.dat = dat;
    }

    public String getDeliver() {
        return deliver;
    }

    public void setDeliver(String deliver) {
        this.deliver = deliver;
    }

    public double getAmtacct() {
        return amtacct;
    }

    public void setAmtacct(double amtacct) {
        this.amtacct = amtacct;
    }

    public double getAmtadj() {
        return amtadj;
    }

    public void setAmtadj(double amtadj) {
        this.amtadj = amtadj;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getMadeby() {
        return madeby;
    }

    public void setMadeby(String madeby) {
        this.madeby = madeby;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public Integer getChkno() {
        return chkno;
    }

    public void setChkno(Integer chkno) {
        this.chkno = chkno;
    }

    public Integer getChktag() {
        return chktag;
    }

    public void setChktag(Integer chktag) {
        this.chktag = chktag;
    }

    public String getInvalue() {
        return invalue;
    }

    public void setInvalue(String invalue) {
        this.invalue = invalue;
    }

    public String getOutvalue() {
        return outvalue;
    }

    public void setOutvalue(String outvalue) {
        this.outvalue = outvalue;
    }

    public List<String> getInvalueList() {
        return invalueList;
    }

    public void setInvalueList(List<String> invalueList) {
        this.invalueList = invalueList;
    }

    public List<String> getOutvalueList() {
        return outvalueList;
    }

    public void setOutvalueList(List<String> outvalueList) {
        this.outvalueList = outvalueList;
    }

    public double getT_amtpay() {
        return t_amtpay;
    }

    public void setT_amtpay(double t_amtpay) {
        this.t_amtpay = t_amtpay;
    }

    public double getAmtpay() {
        return amtpay;
    }

    public void setAmtpay(double amtpay) {
        this.amtpay = amtpay;
    }
}
