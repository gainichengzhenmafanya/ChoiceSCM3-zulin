package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import com.choice.framework.util.DateFormat;

/**
 * 入库单从表
 * @author lehui
 *
 */
public class Chkind {
	private String acct;//帐套
	private String yearr;//年
	private int    id;//
	private int    chkinno;//单号
	private Supply supply;//物资编码
	private double  amount;//标准数量
	private double  price;//价格
	private int    chkstono;//申购单号
	private String memo;//
	private int    spno;//
	private String inout;//入库标志
	private double  pound;//
	private int    sp_id;//spbatch 的主键 冲消用
	private String indept;//分店编码
	private double  totalamt;//总价
	private double  amount1;//参考数量
	private int    bakno;//
	private double  pricenew;//
	private Date   datchange;//
	private String empchange;//
	private String pr;//  存储过程接收参数
	private String des;//分店名称
	private double  extim;//加工工时
	private BigDecimal pricesale;
	
	private  Date dued;  //   生产日期
	private  String pcno;  //  批次管理
	private String deliver;//供应商到货验收 多档口用
	private int chkno;//验货时保存单号用  直配保存chkstono 配送保存supplyacct的chkno
	
	//*******采购系统用
	private Integer batchid;
	private String firm;//分店仓位
	private Integer qstate;//不合格状态
	private String noreason;//不合格原因
	
	private Double tax;
	private String taxdes;
	
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public Integer getBatchid() {
		return batchid;
	}
	public void setBatchid(Integer batchid) {
		this.batchid = batchid;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Integer getQstate() {
		return qstate;
	}
	public void setQstate(Integer qstate) {
		this.qstate = qstate;
	}
	public String getNoreason() {
		return noreason;
	}
	public void setNoreason(String noreason) {
		this.noreason = noreason;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public String getPcno() {
		return pcno;
	}
	public void setPcno(String pcno) {
		this.pcno = pcno;
	}
	
	public BigDecimal getPricesale() {
		return pricesale;
	}
	public void setPricesale(BigDecimal pricesale) {
		this.pricesale = pricesale;
	}
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getChkinno() {
		return chkinno;
	}
	public void setChkinno(int chkinno) {
		this.chkinno = chkinno;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getDued() {
		return dued;
	}
	public void setDued(Date dued) {
		this.dued = dued;
	}
	public int getChkstono() {
		return chkstono;
	}
	public void setChkstono(int chkstono) {
		this.chkstono = chkstono;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public int getSpno() {
		return spno;
	}
	public void setSpno(int spno) {
		this.spno = spno;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public double getPound() {
		return pound;
	}
	public void setPound(double pound) {
		this.pound = pound;
	}
	public int getSp_id() {
		return sp_id;
	}
	public void setSp_id(int sp_id) {
		this.sp_id = sp_id;
	}
	public String getIndept() {
		return indept;
	}
	public void setIndept(String indept) {
		this.indept = indept;
	}
	public double getTotalamt() {
		return totalamt;
	}
	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}
	public double getAmount1() {
		return amount1;
	}
	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}
	public int getBakno() {
		return bakno;
	}
	public void setBakno(int bakno) {
		this.bakno = bakno;
	}
	public double getPricenew() {
		return pricenew;
	}
	public void setPricenew(double pricenew) {
		this.pricenew = pricenew;
	}
	public Date getDatchange() {
		return datchange;
	}
	public void setDatchange(Date datchange) {
		this.datchange = datchange;
	}
	public String getEmpchange() {
		return empchange;
	}
	public void setEmpchange(String empchange) {
		this.empchange = empchange;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public double getExtim() {
		return extim;
	}
	public void setExtim(double extim) {
		this.extim = extim;
	}
	public int getChkno() {
		return chkno;
	}
	public void setChkno(int chkno) {
		this.chkno = chkno;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Chkind.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Supply){
						result.append(((Supply)cur).getSp_code());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
}
