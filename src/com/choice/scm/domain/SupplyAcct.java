package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

public class SupplyAcct {
	private List<SupplyAcct> SupplyAcctList;
	private String chkstoNo;
	private String acct;
	private String yearr;
	private String monthh;
	private int id;
	private String sp_code;
	private int chkno;
	private String firm;
	private String storecode;
	private String storedes;
	private Date dat;
	private String datstr;
	private String des;
	private double cntin;
	private double pricein;
	private double amtin;
	private double cntout;
	private double cntout1;
	private double pricesale;// 无税售价
	private double pricesaletax;// 含税售价
	private double priceout;// 无税进价
	private double priceouttax;// 含税进价
	private double amtout;
	private double cntbal;
	private double pricebal;
	private double amtbal;
	private int chktag;
	private String vouno;
	private String positn;
	private String positnNm;
	private int spno;
	private String unit;
	private String typ;
	private String typdes;
	private String sp_name;
	private String delivercode;
	private String deliverdes;
	private String firmcode;
	private String firmdes;
	private String area;
	private double priceinout;
	private String accby;
	private String madeby;
	private double cntinout;
	private double amtinout;
	private String spdesc;
	private String positndes;
	private String spdes;
	private String empfirm;
	private String empfirmName;// 验收人名字
	private String ynin;
	private double cntfirm;
	private double cntfirm1;
	private double cntuin;
	private double cntuout;
	private String unit1;
	private String chktyp;
	private String chktypdes;

	public String getChktypdes() {
		return chktypdes;
	}

	public void setChktypdes(String chktypdes) {
		this.chktypdes = chktypdes;
	}

	private String grp;
	private String grpdes;
	private String grptyp;
	private String grptypdes;
	private int chkinid;
	private double pricechk;
	private double cntfirmks;
	private double cntlogistks;
	private int chkks;
	private String chkyh;
	private String typothdes;
	private String typoth;
	private Date bdat;
	private Date edat;
	private String bdate;
	private String edate;
	private int folio;
	private int bill;
	private String inout;
	private String type;
	private String typea;
	private String sp_desc;
	private String positn1;
	private Supply supply;
	private double amount;
	private double amt;
	private double spCost;
	private double feiCost;
	private String checkMis;
	private String extim;
	private double outpricetax;
	private double amtouttax;
	private double inpricetax;
	private double amtintax;
	private Double tax;
	private Double taxamt;
	private String ynkc;// 是否使用库存系统
	private String sql;// 门店日盘点状态行转列拼写
	private String iszs;// 是否赠送
	private String deal;// 是否处理过，调度中心报货到计划用
	private Double yl;
	private Date maded; // 报货单填制日期 报表需要 2014.11.7 wjf
	private Date bmaded;// 报货日期开始日期 报表需要 2014.11.7 wjf
	private Date emaded;// 报货日期结束日期 报表需要 2014.11.7 wjf
	private String iscy;// 是否有差异 热辣一号加入
	private String accountId;// 加入账号，报表加权限用 2011.12.22wjf
	private String cwqx;// 仓位权限，为1时才使用仓位权限
	private String gysqx;// 供应商权限 为1时使用供应商权限
	private Date dued;// 生产日期
	private String pcno;// 批次号
	private int showtyp;// 显示形式
	private String dept;// 部門
	private List<String> firmList;// 门店编码数组
	private String firmtyp;// 仓位类型 验货差异处理用 wjf
	private String isDept;// 是否档口报货来的 wjf2015.3.9
	private String attribute; // 物资属性
	private int chkinno;// 验货保存生成的入库单号

	// 自定义报表用 wjf
	private String vname;
	private String reportPath;
	private Date week;

	private double priceintax;

	// 出库报表打印用 jinshuai
	private String priceprint;
	private String amtprint;

	private String isTax;// 是否含税

	private String bysale;// 按售价

	private String acceptPositn;// 领用仓位


	private String acceptPositnName;// 领用仓位

	private String startDate;
	private String endDate;
	
	
	//配送验货要报货数量和调整量
	private String unit3;
	private double amount1;
	private double amount1sto;
	
	public String getUnit3() {
		return unit3;
	}

	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}

	public double getAmount1() {
		return amount1;
	}

	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}

	public double getAmount1sto() {
		return amount1sto;
	}

	public void setAmount1sto(double amount1sto) {
		this.amount1sto = amount1sto;
	}

	public String getAcceptPositnName() {
		return acceptPositnName;
	}
	
	public void setAcceptPositnName(String acceptPositnName) {
		this.acceptPositnName = acceptPositnName;
	}
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAcceptPositn() {
		return acceptPositn;
	}

	public void setAcceptPositn(String acceptPositn) {
		this.acceptPositn = acceptPositn;
	}

	public String getBysale() {
		return bysale;
	}

	public void setBysale(String bysale) {
		this.bysale = bysale;
	}

	public String getIsTax() {
		return isTax;
	}

	public void setIsTax(String isTax) {
		this.isTax = isTax;
	}

	public String getPriceprint() {
		return priceprint;
	}

	public void setPriceprint(String priceprint) {
		this.priceprint = priceprint;
	}

	public String getAmtprint() {
		return amtprint;
	}

	public void setAmtprint(String amtprint) {
		this.amtprint = amtprint;
	}

	public double getPriceintax() {
		return priceintax;
	}

	public void setPriceintax(double priceintax) {
		this.priceintax = priceintax;
	}

	public Date getWeek() {
		return week;
	}

	public void setWeek(Date week) {
		this.week = week;
	}

	public String getVname() {
		return vname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public String getReportPath() {
		return reportPath;
	}

	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getEmpfirmName() {
		return empfirmName;
	}

	public void setEmpfirmName(String empfirmName) {
		this.empfirmName = empfirmName;
	}

	public String getIsDept() {
		return isDept;
	}

	public void setIsDept(String isDept) {
		this.isDept = isDept;
	}

	public String getFirmtyp() {
		return firmtyp;
	}

	public void setFirmtyp(String firmtyp) {
		this.firmtyp = firmtyp;
	}

	public Date getDued() {
		return dued;
	}

	public void setDued(Date dued) {
		this.dued = dued;
	}

	public String getPcno() {
		return pcno;
	}

	public void setPcno(String pcno) {
		this.pcno = pcno;
	}

	public String getGysqx() {
		return gysqx;
	}

	public void setGysqx(String gysqx) {
		this.gysqx = gysqx;
	}

	public String getCwqx() {
		return cwqx;
	}

	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getStorecode() {
		return storecode;
	}

	public void setStorecode(String storecode) {
		this.storecode = storecode;
	}

	public String getStoredes() {
		return storedes;
	}

	public void setStoredes(String storedes) {
		this.storedes = storedes;
	}

	public String getIscy() {
		return iscy;
	}

	public void setIscy(String iscy) {
		this.iscy = iscy;
	}

	public String getBdate() {
		return bdate;
	}

	public void setBdate(String bdate) {
		this.bdate = bdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public String getChkstoNo() {
		return chkstoNo;
	}

	public void setChkstoNo(String chkstoNo) {
		this.chkstoNo = chkstoNo;
	}

	public Double getYl() {
		return yl;
	}

	public void setYl(Double yl) {
		this.yl = yl;
	}

	public String getDeal() {
		return deal;
	}

	public void setDeal(String deal) {
		this.deal = deal;
	}

	public String getYnkc() {
		return ynkc;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public void setYnkc(String ynkc) {
		this.ynkc = ynkc;
	}

	public double getPricesaletax() {
		return pricesaletax;
	}

	public void setPricesaletax(double pricesaletax) {
		this.pricesaletax = pricesaletax;
	}

	public double getPriceouttax() {
		return priceouttax;
	}

	public void setPriceouttax(double priceouttax) {
		this.priceouttax = priceouttax;
	}

	public String getExtim() {
		return this.extim;
	}

	public double getAmt() {
		return this.amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public double getSpCost() {
		return this.spCost;
	}

	public void setSpCost(double spCost) {
		this.spCost = spCost;
	}

	public double getFeiCost() {
		return this.feiCost;
	}

	public void setFeiCost(double feiCost) {
		this.feiCost = feiCost;
	}

	public String getMonthh() {
		return this.monthh;
	}

	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}

	public void setExtim(String extim) {
		this.extim = extim;
	}

	public String getTypea() {
		return this.typea;
	}

	public void setTypea(String typea) {
		this.typea = typea;
	}

	public String getPositnNm() {
		return this.positnNm;
	}

	public void setPositnNm(String positnNm) {
		this.positnNm = positnNm;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public List<SupplyAcct> getSupplyAcctList() {
		return this.SupplyAcctList;
	}

	public void setSupplyAcctList(List<SupplyAcct> supplyAcctList) {
		this.SupplyAcctList = supplyAcctList;
	}

	public Supply getSupply() {
		return this.supply;
	}

	public void setSupply(Supply supply) {
		this.supply = supply;
	}

	public String getAcct() {
		return this.acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return this.yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSp_code() {
		return this.sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public int getChkno() {
		return this.chkno;
	}

	public void setChkno(int chkno) {
		this.chkno = chkno;
	}

	public String getFirm() {
		return this.firm;
	}

	public void setFirm(String firm) {
		this.firm = firm;
	}

	public Date getDat() {
		return this.dat;
	}

	public void setDat(Date dat) {
		this.dat = dat;
	}

	public String getDes() {
		return this.des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public double getCntin() {
		return this.cntin;
	}

	public void setCntin(double cntin) {
		this.cntin = cntin;
	}

	public double getPricein() {
		return this.pricein;
	}

	public void setPricein(double pricein) {
		this.pricein = pricein;
	}

	public double getAmtin() {
		return this.amtin;
	}

	public void setAmtin(double amtin) {
		this.amtin = amtin;
	}

	public double getCntout() {
		return this.cntout;
	}

	public void setCntout(double cntout) {
		this.cntout = cntout;
	}

	public double getCntout1() {
		return this.cntout1;
	}

	public void setCntout1(double cntout1) {
		this.cntout1 = cntout1;
	}

	public double getPriceout() {
		return this.priceout;
	}

	public void setPriceout(double priceout) {
		this.priceout = priceout;
	}

	public double getAmtout() {
		return this.amtout;
	}

	public void setAmtout(double amtout) {
		this.amtout = amtout;
	}

	public double getCntbal() {
		return this.cntbal;
	}

	public void setCntbal(double cntbal) {
		this.cntbal = cntbal;
	}

	public double getPricebal() {
		return this.pricebal;
	}

	public void setPricebal(double pricebal) {
		this.pricebal = pricebal;
	}

	public double getAmtbal() {
		return this.amtbal;
	}

	public void setAmtbal(double amtbal) {
		this.amtbal = amtbal;
	}

	public int getChktag() {
		return this.chktag;
	}

	public void setChktag(int chktag) {
		this.chktag = chktag;
	}

	public String getVouno() {
		return this.vouno;
	}

	public void setVouno(String vouno) {
		this.vouno = vouno;
	}

	public String getPositn() {
		return this.positn;
	}

	public void setPositn(String positn) {
		this.positn = positn;
	}

	public int getSpno() {
		return this.spno;
	}

	public void setSpno(int spno) {
		this.spno = spno;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTyp() {
		return this.typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTypdes() {
		return this.typdes;
	}

	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}

	public String getGrptyp() {
		return this.grptyp;
	}

	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}

	public String getSp_name() {
		return this.sp_name;
	}

	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}

	public String getDelivercode() {
		return this.delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public String getDeliverdes() {
		return this.deliverdes;
	}

	public void setDeliverdes(String deliverdes) {
		this.deliverdes = deliverdes;
	}

	public String getFirmcode() {
		return this.firmcode;
	}

	public void setFirmcode(String firmcode) {
		this.firmcode = firmcode;
	}

	public String getFirmdes() {
		return this.firmdes;
	}

	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public double getPriceinout() {
		return this.priceinout;
	}

	public void setPriceinout(double priceinout) {
		this.priceinout = priceinout;
	}

	public String getAccby() {
		return this.accby;
	}

	public void setAccby(String accby) {
		this.accby = accby;
	}

	public String getMadeby() {
		return this.madeby;
	}

	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}

	public double getCntinout() {
		return this.cntinout;
	}

	public void setCntinout(double cntinout) {
		this.cntinout = cntinout;
	}

	public double getAmtinout() {
		return this.amtinout;
	}

	public void setAmtinout(double amtinout) {
		this.amtinout = amtinout;
	}

	public String getSpdesc() {
		return this.spdesc;
	}

	public void setSpdesc(String spdesc) {
		this.spdesc = spdesc;
	}

	public String getPositndes() {
		return this.positndes;
	}

	public void setPositndes(String positndes) {
		this.positndes = positndes;
	}

	public String getSpdes() {
		return this.spdes;
	}

	public void setSpdes(String spdes) {
		this.spdes = spdes;
	}

	public String getEmpfirm() {
		return this.empfirm;
	}

	public void setEmpfirm(String empfirm) {
		this.empfirm = empfirm;
	}

	public String getYnin() {
		return this.ynin;
	}

	public void setYnin(String ynin) {
		this.ynin = ynin;
	}

	public double getCntfirm() {
		return this.cntfirm;
	}

	public void setCntfirm(double cntfirm) {
		this.cntfirm = cntfirm;
	}

	public double getCntfirm1() {
		return this.cntfirm1;
	}

	public void setCntfirm1(double cntfirm1) {
		this.cntfirm1 = cntfirm1;
	}

	public double getCntuin() {
		return this.cntuin;
	}

	public void setCntuin(double cntuin) {
		this.cntuin = cntuin;
	}

	public double getCntuout() {
		return this.cntuout;
	}

	public void setCntuout(double cntuout) {
		this.cntuout = cntuout;
	}

	public String getUnit1() {
		return this.unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	public String getChktyp() {
		return this.chktyp;
	}

	public void setChktyp(String chktyp) {
		this.chktyp = chktyp;
	}

	public double getPricesale() {
		return this.pricesale;
	}

	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}

	public String getGrp() {
		return this.grp;
	}

	public void setGrp(String grp) {
		this.grp = grp;
	}

	public String getGrpdes() {
		return this.grpdes;
	}

	public void setGrpdes(String grpdes) {
		this.grpdes = grpdes;
	}

	public String getGrptypdes() {
		return this.grptypdes;
	}

	public void setGrptypdes(String grptypdes) {
		this.grptypdes = grptypdes;
	}

	public int getChkinid() {
		return this.chkinid;
	}

	public void setChkinid(int chkinid) {
		this.chkinid = chkinid;
	}

	public double getPricechk() {
		return this.pricechk;
	}

	public void setPricechk(double pricechk) {
		this.pricechk = pricechk;
	}

	public double getCntfirmks() {
		return this.cntfirmks;
	}

	public void setCntfirmks(double cntfirmks) {
		this.cntfirmks = cntfirmks;
	}

	public double getCntlogistks() {
		return this.cntlogistks;
	}

	public void setCntlogistks(double cntlogistks) {
		this.cntlogistks = cntlogistks;
	}

	public int getChkks() {
		return this.chkks;
	}

	public void setChkks(int chkks) {
		this.chkks = chkks;
	}

	public String getChkyh() {
		return this.chkyh;
	}

	public void setChkyh(String chkyh) {
		this.chkyh = chkyh;
	}

	public String getTypothdes() {
		return this.typothdes;
	}

	public void setTypothdes(String typothdes) {
		this.typothdes = typothdes;
	}

	public String getTypoth() {
		return this.typoth;
	}

	public void setTypoth(String typoth) {
		this.typoth = typoth;
	}

	public Date getBdat() {
		return this.bdat;
	}

	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}

	public Date getEdat() {
		return this.edat;
	}

	public void setEdat(Date edat) {
		this.edat = edat;
	}

	public int getFolio() {
		return this.folio;
	}

	public void setFolio(int folio) {
		this.folio = folio;
	}

	public int getBill() {
		return this.bill;
	}

	public void setBill(int bill) {
		this.bill = bill;
	}

	public String getInout() {
		return this.inout;
	}

	public void setInout(String inout) {
		this.inout = inout;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSp_desc() {
		return this.sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public String getPositn1() {
		return this.positn1;
	}

	public void setPositn1(String positn1) {
		this.positn1 = positn1;
	}

	public String getCheckMis() {
		return this.checkMis;
	}

	public void setCheckMis(String checkMis) {
		this.checkMis = checkMis;
	}

	public double getOutpricetax() {
		return this.outpricetax;
	}

	public void setOutpricetax(double outpricetax) {
		this.outpricetax = outpricetax;
	}

	public double getAmtouttax() {
		return this.amtouttax;
	}

	public void setAmtouttax(double amtouttax) {
		this.amtouttax = amtouttax;
	}

	public Double getTax() {
		return this.tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Double getTaxamt() {
		return this.taxamt;
	}

	public void setTaxamt(Double taxamt) {
		this.taxamt = taxamt;
	}

	public double getInpricetax() {
		return this.inpricetax;
	}

	public void setInpricetax(double inpricetax) {
		this.inpricetax = inpricetax;
	}

	public double getAmtintax() {
		return this.amtintax;
	}

	public void setAmtintax(double amtintax) {
		this.amtintax = amtintax;
	}

	public String getIszs() {
		return iszs;
	}

	public void setIszs(String iszs) {
		this.iszs = iszs;
	}

	public Date getMaded() {
		return maded;
	}

	public void setMaded(Date maded) {
		this.maded = maded;
	}

	public Date getBmaded() {
		return bmaded;
	}

	public void setBmaded(Date bmaded) {
		this.bmaded = bmaded;
	}

	public Date getEmaded() {
		return emaded;
	}

	public void setEmaded(Date emaded) {
		this.emaded = emaded;
	}

	public int getShowtyp() {
		return showtyp;
	}

	public void setShowtyp(int showtyp) {
		this.showtyp = showtyp;
	}

	public List<String> getFirmList() {
		return firmList;
	}

	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public int getChkinno() {
		return chkinno;
	}

	public void setChkinno(int chkinno) {
		this.chkinno = chkinno;
	}
	
	public String getDatstr() {
		return datstr;
	}

	public void setDatstr(String datstr) {
		this.datstr = datstr;
	}
}