package com.choice.scm.domain;

import java.util.Date;
/**
 * 周次维护
 * @author css
 *
 */
public class WeekSet {

	private String acct;//帐套
	private String yearr;//年
	private String weekk;//周次
	private String weekno;//流水号
	private Date datf;//开始日期
	private Date datt;//结束日期
	private String des;//周次描述
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getWeekk() {
		return weekk;
	}
	public void setWeekk(String weekk) {
		this.weekk = weekk;
	}
	public String getWeekno() {
		return weekno;
	}
	public void setWeekno(String weekno) {
		this.weekno = weekno;
	}
	public Date getDatf() {
		return datf;
	}
	public void setDatf(Date datf) {
		this.datf = datf;
	}
	public Date getDatt() {
		return datt;
	}
	public void setDatt(Date datt) {
		this.datt = datt;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
}
