package com.choice.scm.domain;

import java.util.Date;

/**
 * 帐套
 * @author css
 *
 */
public class CntUse {
	private String  acct;       // 帐套
	private String  positn;       // 门店
	private Date  bdat;       // 起始日期
	private Date  edat;       // 终止日期
	private Integer pr;         // 接收存储过程；
	
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	
}
