package com.choice.scm.domain;
  
public class Pubgrp {
	private String  pubgrp;
	private String code;
	private  String pgrpdes;
	public String getPubgrp() {
		return pubgrp;
	}
	public void setPubgrp(String pubgrp) {
		this.pubgrp = pubgrp;
	}
	public String getPgrpdes() {
		return pgrpdes;
	}
	public void setPgrpdes(String pgrpdes) {
		this.pgrpdes = pgrpdes;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
