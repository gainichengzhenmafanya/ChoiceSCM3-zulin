package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.choice.framework.util.DateFormat;

/**
 * 报货单--主表
 * @author csb
 *
 */
public class Chkstom {

	private List<Chkstod> chkstodList;
	private String acct; //帐套号
	private String yearr; //年度
	private Integer chkstoNo; //申购单主键，流水号
	private String	chkstoNo_c    ;//	申购拆单单号
	private Date maded; //填制日期
	private String madet; //时间
	private Date checd; //审核日期
	private Date chect; //审核时间
	private String madeby; //填制人
	private String checby; //审核人
	private String vouno; //凭证号 ，规则算出的 一般为年度日期-001
	private String dept; //申购的部门编号
	private String firm; //申购分店
	private Positn positn; //仓位，向哪个仓位申购。用处不大，目前默认都是主仓位
	private List<String> listPositn;
	private double totalAmt; //合计金额 明细数据的合计金额
	private String status; //状态
	private String build;
	private String chectim; //审核时间
	private Integer pr;         //用来接收存储过程的输出标识
	private Integer chkCount;   //接收查询出来的数量
	private Date bMaded;//上传的最初时间
	private Date eMaded;//上传的最后时间
	private String bMad;//上传的最初时间
	private String eMad;//上传的最后时间
	private int countChecked;  //当天多少条已经审核的
	private int countNoChecked;//当天多少条未审核的
	//五芳斋相关功能用
	private String yntg;		//是否团购
	private String typ_eas;		//报货分类
	private String id;		//报货分类
	private String ynjd;		//是否传入金蝶库
	private String yn_x;//  是否有虚拟物料
	//档口报货单用
	private String	  bak1       	;//	存放对应生成的总部申购单的明细的ID
	private Integer	  bak2       	;//	存放对应生成的总部申购单的单号
	
	private String madebyName;//制单人名字 
	private String checbyName;//审核人名字
	
	private Date hoped;//存放报货单的默认到货日期 门店boh用wjf
	
	private Integer createTyp;//报货单来源  门店boh用 wjf  默认1填制来的
	private String manifsttyp;//报货类别 门店boh用wjf 
	private String typ;//报货类型  正常报货9970 补单报货9971 报货退货9972 紧急报货9973
	
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Integer getCreateTyp() {
		return createTyp;
	}
	public void setCreateTyp(Integer createTyp) {
		this.createTyp = createTyp;
	}
	public String getManifsttyp() {
		return manifsttyp;
	}
	public void setManifsttyp(String manifsttyp) {
		this.manifsttyp = manifsttyp;
	}
	public Date getHoped() {
		return hoped;
	}
	public void setHoped(Date hoped) {
		this.hoped = hoped;
	}
	public String getMadebyName() {
		return madebyName;
	}
	public void setMadebyName(String madebyName) {
		this.madebyName = madebyName;
	}
	public String getChecbyName() {
		return checbyName;
	}
	public void setChecbyName(String checbyName) {
		this.checbyName = checbyName;
	}
	public String getYn_x() {
		return yn_x;
	}
	public void setYn_x(String yn_x) {
		this.yn_x = yn_x;
	}
	public Chkstom() {
		super();
	}
	public Chkstom(Integer chkstoNo) {
		super();
		this.chkstoNo = chkstoNo;
	}

	public String getYnjd() {
		return ynjd;
	}
	public void setYnjd(String ynjd) {
		this.ynjd = ynjd;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTyp_eas() {
		return typ_eas;
	}
	public void setTyp_eas(String typ_eas) {
		this.typ_eas = typ_eas;
	}
	public String getYntg() {
		return yntg;
	}
	public void setYntg(String yntg) {
		this.yntg = yntg;
	}
	public String getChkstoNo_c() {
		return chkstoNo_c;
	}
	public void setChkstoNo_c(String chkstoNo_c) {
		this.chkstoNo_c = chkstoNo_c;
	}
	public List<String> getListPositn() {
		return listPositn;
	}
	public void setListPositn(List<String> listPositn) {
		this.listPositn = listPositn;
	}
	public String getbMad() {
		return bMad;
	}
	public void setbMad(String bMad) {
		this.bMad = bMad;
	}
	public String geteMad() {
		return eMad;
	}
	public void seteMad(String eMad) {
		this.eMad = eMad;
	}
	public List<Chkstod> getChkstodList() {
		return chkstodList;
	}
	public void setChkstodList(List<Chkstod> chkstodList) {
		this.chkstodList = chkstodList;
	}
	public Date getbMaded() {
		return bMaded;
	}
	public void setbMaded(Date bMaded) {
		this.bMaded = bMaded;
	}
	public Date geteMaded() {
		return eMaded;
	}
	public void seteMaded(Date eMaded) {
		this.eMaded = eMaded;
	}
	public Integer getChkCount() {
		return chkCount;
	}
	public void setChkCount(Integer chkCount) {
		this.chkCount = chkCount;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkstoNo() {
		return chkstoNo;
	}
	public void setChkstoNo(Integer chkstoNo) {
		this.chkstoNo = chkstoNo;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public Date getChect() {
		return chect;
	}
	public void setChect(Date chect) {
		this.chect = chect;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public Integer getBak2() {
		return bak2;
	}
	public void setBak2(Integer bak2) {
		this.bak2 = bak2;
	}
	public String getChectim() {
		return chectim;
	}
	public void setChectim(String chectim) {
		this.chectim = chectim;
	}
	public int getCountChecked() {
		return countChecked;
	}
	public void setCountChecked(int countChecked) {
		this.countChecked = countChecked;
	}
	public int getCountNoChecked() {
		return countNoChecked;
	}
	public void setCountNoChecked(int countNoChecked) {
		this.countNoChecked = countNoChecked;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Chkstom.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Positn){
						result.append(((Positn)cur).getCode());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
}
