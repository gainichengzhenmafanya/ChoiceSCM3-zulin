package com.choice.scm.domain;

import java.util.List;

//盘点模板
public class Chkdemo {

	private String acct;//帐套
	private Positn positn;//仓位
	private int id;//编号
	private String title;//标题
	private String typ;//物资类别
	private Supply supply;//物资编码
	private List<Chkdemo> chkdemoList;   //批量删除用
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public List<Chkdemo> getChkdemoList() {
		return chkdemoList;
	}
	public void setChkdemoList(List<Chkdemo> chkdemoList) {
		this.chkdemoList = chkdemoList;
	}
}
