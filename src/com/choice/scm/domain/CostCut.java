package com.choice.scm.domain;

import java.util.List;

/**
 * 核减数据
 * @author css
 *
 */
public class CostCut {
	
	private String acct;
	private String dat;
	private Integer rec;
	private String firmid;
	private String firmdes;
	private String dept; 
	private String deptdes; 
	private String item;
	private String pdes;
	private String pitcode;
	private String punit;
	private double price;
	private String pgrp;
	private double cnt;
	private double disc;
	private double svc;
	private double refund;
	private double tax	;
	private double amt;
	private double pickdisc;
	
	private String bdate;
	private String edate;
	private List<String> codeList;
	
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public List<String> getCodeList() {
		return codeList;
	}
	public void setCodeList(List<String> codeList) {
		this.codeList = codeList;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	
	public String getDat() {
		return dat;
	}
	public void setDat(String dat) {
		this.dat = dat;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public String getFirmid() {
		return firmid;
	}
	public void setFirmid(String firmid) {
		this.firmid = firmid;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getPdes() {
		return pdes;
	}
	public void setPdes(String pdes) {
		this.pdes = pdes;
	}
	public String getPitcode() {
		return pitcode;
	}
	public void setPitcode(String pitcode) {
		this.pitcode = pitcode;
	}
	public String getPunit() {
		return punit;
	}
	public void setPunit(String punit) {
		this.punit = punit;
	}
	public String getPgrp() {
		return pgrp;
	}
	public void setPgrp(String pgrp) {
		this.pgrp = pgrp;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getDisc() {
		return disc;
	}
	public void setDisc(double disc) {
		this.disc = disc;
	}
	public double getSvc() {
		return svc;
	}
	public void setSvc(double svc) {
		this.svc = svc;
	}
	public double getRefund() {
		return refund;
	}
	public void setRefund(double refund) {
		this.refund = refund;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getPickdisc() {
		return pickdisc;
	}
	public void setPickdisc(double pickdisc) {
		this.pickdisc = pickdisc;
	}
	
}
