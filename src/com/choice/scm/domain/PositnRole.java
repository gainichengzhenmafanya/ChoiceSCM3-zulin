package com.choice.scm.domain;

public class PositnRole {

	private String id;//主键
	private String roleId;//角色Id   换成账号关联（此处暂时用这个字段代替accountId）
	private String positnId;//仓位Id
	
	public PositnRole() {
		super();
	}

	public PositnRole(String id, String roleId, String positnId) {
		super();
		this.id = id;
		this.roleId = roleId;
		this.positnId = positnId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getPositnId() {
		return positnId;
	}
	public void setPositnId(String positnId) {
		this.positnId = positnId;
	}
	
	
	
}
