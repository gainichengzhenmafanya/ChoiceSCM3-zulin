package com.choice.scm.domain;

/**
 * 物资多单位
 * @author css
 */

public class SupplyUnit {
	
	private String id;  //id
	private String sp_code;  //物资编码
	private String unit;	 //单位名称
	private double unitper;   //单位转换率
	private Integer sequence; //物资规格 wj 2015年1月8日 17:39:44
	private Integer orderNo;//单位排序
	private String ynDisunit;//是否配送单位
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	private double dismincnt;//配送单位最小申购量
	
	private String sp_codes;//多个物资条件
	private Integer ynDelete;//是否可删除
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getUnitper() {
		return unitper;
	}
	public void setUnitper(double unitper) {
		this.unitper = unitper;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getYnDisunit() {
		return ynDisunit;
	}
	public void setYnDisunit(String ynDisunit) {
		this.ynDisunit = ynDisunit;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public double getDismincnt() {
		return dismincnt;
	}
	public void setDismincnt(double dismincnt) {
		this.dismincnt = dismincnt;
	}
	public String getSp_codes() {
		return sp_codes;
	}
	public void setSp_codes(String sp_codes) {
		this.sp_codes = sp_codes;
	}
	public Integer getYnDelete() {
		return ynDelete;
	}
	public void setYnDelete(Integer ynDelete) {
		this.ynDelete = ynDelete;
	}
	
}
