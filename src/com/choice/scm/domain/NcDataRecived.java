package com.choice.scm.domain;

public class NcDataRecived {

	/**
	 * 
	 */
	private String unitcode;//仓位编码
	private String unitname;//仓位名
	private String forinvname;//物资类别等级
	private String invcode;// 物资编码
	private String invname;// 物资名称
	private String invclasscodeyi;// 大类编码
	private String invclassnameyi;// 大类名称
	private String invclasscodeer;// 中类编码
	private String invclassnameer;// 中类名称
	private String invclasscodesan ;// 小类编码
	private String invclassnamesan;//  小类名称
	private String shortname;//  单位编码
	private String measname;//   单位名称
	
	private String invspec; //  物资规格
	private String invtype; //  物资型号
	private String invpinpai; // 物资品牌
	private String sealflag; // 物资封存
	
	
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
	public String getForinvname() {
		return forinvname;
	}
	public void setForinvname(String forinvname) {
		this.forinvname = forinvname;
	}
	public String getInvcode() {
		return invcode;
	}
	public void setInvcode(String invcode) {
		this.invcode = invcode;
	}
	public String getInvname() {
		return invname;
	}
	public void setInvname(String invname) {
		this.invname = invname;
	}
	public String getInvclasscodeyi() {
		return invclasscodeyi;
	}
	public void setInvclasscodeyi(String invclasscodeyi) {
		this.invclasscodeyi = invclasscodeyi;
	}
	public String getInvclassnameyi() {
		return invclassnameyi;
	}
	public void setInvclassnameyi(String invclassnameyi) {
		this.invclassnameyi = invclassnameyi;
	}
	public String getInvclasscodeer() {
		return invclasscodeer;
	}
	public void setInvclasscodeer(String invclasscodeer) {
		this.invclasscodeer = invclasscodeer;
	}
	public String getInvclassnameer() {
		return invclassnameer;
	}
	public void setInvclassnameer(String invclassnameer) {
		this.invclassnameer = invclassnameer;
	}
	public String getInvclasscodesan() {
		return invclasscodesan;
	}
	public void setInvclasscodesan(String invclasscodesan) {
		this.invclasscodesan = invclasscodesan;
	}
	public String getInvclassnamesan() {
		return invclassnamesan;
	}
	public void setInvclassnamesan(String invclassnamesan) {
		this.invclassnamesan = invclassnamesan;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	public String getMeasname() {
		return measname;
	}
	public void setMeasname(String measname) {
		this.measname = measname;
	}
	public String getInvspec() {
		return invspec;
	}
	public void setInvspec(String invspec) {
		this.invspec = invspec;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getInvpinpai() {
		return invpinpai;
	}
	public void setInvpinpai(String invpinpai) {
		this.invpinpai = invpinpai;
	}
	public String getSealflag() {
		return sealflag;
	}
	public void setSealflag(String sealflag) {
		this.sealflag = sealflag;
	}
	
	
}
