package com.choice.scm.domain;

import java.util.Date;
/**
 * 供应商
 * @author csb
 *
 */
public class Deliver {

	private String id;//五芳斋财务对应用
	private String code;//供应商编码
	private String des;//供应商名称
	private String init;//缩写
	private String person;//负责人
	private String person1;//联系人
	private String addr;//地址
	private String tel;//传真
	private String tel1;//电话
	private String credit;//是否开发票
	private String mail;//邮箱
	private double amtc;//记账金额
	private double amtd;//已结金额
	private String memo;//备注
	private String locked;//是否已用
	private String inout;//是否直拨
	private int prn;
	private String firm;
	private String des1;//规格
	private String typ;//供应商类型
	private String typCode;//供应商类型编码
	private Date bdat;
	private String taxcode;//税号
	private String yntax;//是否一般纳税人
	private String bank;//银行名称
	private String bankcode;//银行账号
	private String www;//网址
	private String tel2;//电话
	private String tax;//费率
	private String positn;//默认仓位
	private String gysqx;//供应商权限，无筛选0，按照账号供应商权限1
	private String accountId;//供应商权限为1时候的筛选用
	private Integer enablestate;//启用状态
	
	private Integer warnDays;//物资到期提醒天数 wangjie 2014年11月21日 14:47:37
	
	private String isReal;//是否真正供应商  2015.2.26wjf  用来查询真正供应商
	private String sp_code;//根据物资选择供应商用***用于采购系统
	private Integer iaccttype;//账期类型 0 即时,1 一月,2 二月,3 季度,4 半年,5 一年
	private String jmudelivercode;//商城供应商编码
	private String jmudelivername;//商城供应商名称
	private String vhstore;//商城店铺ID
	
	private Date expirydate;//资质到期日
	private Integer expiryday;//还有多少天到期
	private String orderBy;//排序
	
	private String alias;//别名
	
	private String isRealAndEx;//实际供应商和加工间对应供应商
	private String isRealAndExAndPYPK;//实际供应商和加工间对应供应商和盘盈盘亏库
	private String isRealAndPYPK;//实际供应商和盘盈盘亏库
	
	public String getIsRealAndPYPK() {
		return isRealAndPYPK;
	}
	public void setIsRealAndPYPK(String isRealAndPYPK) {
		this.isRealAndPYPK = isRealAndPYPK;
	}
	public String getIsRealAndEx() {
		return isRealAndEx;
	}
	public void setIsRealAndEx(String isRealAndEx) {
		this.isRealAndEx = isRealAndEx;
	}
	public String getIsRealAndExAndPYPK() {
		return isRealAndExAndPYPK;
	}
	public void setIsRealAndExAndPYPK(String isRealAndExAndPYPK) {
		this.isRealAndExAndPYPK = isRealAndExAndPYPK;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public Date getExpirydate() {
		return expirydate;
	}
	public void setExpirydate(Date expirydate) {
		this.expirydate = expirydate;
	}
	public Integer getExpiryday() {
		return expiryday;
	}
	public void setExpiryday(Integer expiryday) {
		this.expiryday = expiryday;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getVhstore() {
		return vhstore;
	}
	public void setVhstore(String vhstore) {
		this.vhstore = vhstore;
	}
	public String getJmudelivercode() {
		return jmudelivercode;
	}
	public void setJmudelivercode(String jmudelivercode) {
		this.jmudelivercode = jmudelivercode;
	}
	public String getJmudelivername() {
		return jmudelivername;
	}
	public void setJmudelivername(String jmudelivername) {
		this.jmudelivername = jmudelivername;
	}
	public Integer getIaccttype() {
		return iaccttype;
	}
	public void setIaccttype(Integer iaccttype) {
		this.iaccttype = iaccttype;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getIsReal() {
		return isReal;
	}
	public void setIsReal(String isReal) {
		this.isReal = isReal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGysqx() {
		return gysqx;
	}
	public void setGysqx(String gysqx) {
		this.gysqx = gysqx;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getTypCode() {
		return typCode;
	}
	public void setTypCode(String typCode) {
		this.typCode = typCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	public String getPerson() {
		return person;
	}
	public void setPerson(String person) {
		this.person = person;
	}
	public String getPerson1() {
		return person1;
	}
	public void setPerson1(String person1) {
		this.person1 = person1;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getTel1() {
		return tel1;
	}
	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public double getAmtc() {
		return amtc;
	}
	public void setAmtc(double amtc) {
		this.amtc = amtc;
	}
	public double getAmtd() {
		return amtd;
	}
	public void setAmtd(double amtd) {
		this.amtd = amtd;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getLocked() {
		return locked;
	}
	public void setLocked(String locked) {
		this.locked = locked;
	}
	public int getPrn() {
		return prn;
	}
	public void setPrn(int prn) {
		this.prn = prn;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDes1() {
		return des1;
	}
	public void setDes1(String des1) {
		this.des1 = des1;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public String getTaxcode() {
		return taxcode;
	}
	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}
	public String getYntax() {
		return yntax;
	}
	public void setYntax(String yntax) {
		this.yntax = yntax;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getWww() {
		return www;
	}
	public void setWww(String www) {
		this.www = www;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public Integer getWarnDays() {
		return warnDays;
	}
	public void setWarnDays(Integer warnDays) {
		this.warnDays = warnDays;
	}
	
}
