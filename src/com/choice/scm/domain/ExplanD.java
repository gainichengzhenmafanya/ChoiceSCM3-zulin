package com.choice.scm.domain;

import java.util.Date;
import java.util.List;


/**
 * 加工单从表
 * @author css
 *
 */
public class ExplanD {
	private String acct;	//帐套
	private String yearr;	//年
	private int id;		//
	private String ids;		//多id，查询用
	private int    explanno;//单号
	private Supply supply;	//物资编码
	private double  amount;	//标准数量
	private double  amountin;//登记入库数量
	private double  amount1in;//登记入库参考数量
	private String ynrk;		//是否入库
	private String etim;		//入库时间
	private String memo;	//备注
	private Date	maded	  ;//日期   修改时改这个
	private Date	datEnd;		//结束日期
	private double extim;//加工工时
	private List<ExplanD> explanDList;	//备注
	private String positn;//仓位编码
	
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getEtim() {
		return etim;
	}
	public void setEtim(String etim) {
		this.etim = etim;
	}
	public String getYnrk() {
		return ynrk;
	}
	public void setYnrk(String ynrk) {
		this.ynrk = ynrk;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public List<ExplanD> getExplanDList() {
		return explanDList;
	}
	public void setExplanDList(List<ExplanD> explanDList) {
		this.explanDList = explanDList;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public Date getDatEnd() {
		return datEnd;
	}
	public void setDatEnd(Date datEnd) {
		this.datEnd = datEnd;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public int getExplanno() {
		return explanno;
	}
	public void setExplanno(int explanno) {
		this.explanno = explanno;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAmountin() {
		return amountin;
	}
	public void setAmountin(double amountin) {
		this.amountin = amountin;
	}
	public double getAmount1in() {
		return amount1in;
	}
	public void setAmount1in(double amount1in) {
		this.amount1in = amount1in;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getExtim() {
		return extim;
	}
	public void setExtim(double extim) {
		this.extim = extim;
	}
	
}
