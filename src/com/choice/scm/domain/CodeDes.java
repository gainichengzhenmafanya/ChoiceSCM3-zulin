package com.choice.scm.domain;

public class CodeDes {

	public static int S_SP_UNIT=0;//供货单位
	public static int S_DEL_TYPE=1;//供货商类型
	public static int S_ZX_PAY=2;//杂项费用
	public static int S_AREA=3;//区域
	public static int S_SYQ=4;//事业群
	public static int S_PS_AREA=5;//配送片区
	public static int S_JG_MADER=6;//加工制作人
	public static int S_DRIVER=7;//司机
	public static int S_GH_NOT_TYPE=8;//供货不合格类型
	public static int S_COST_TYP=9;//成本卡类型
	public static int S_IN_OUT=14;//申购方向
	public static int S_FRIMTYP = 10;//门店类型 wjf
	public static int S_POSITN_TYP=12;//区域
	public static int S_ATTRIBUTE=15;//物资属性
	public static int S_FUJIAXIANG_RESON=20;//附加项原因
	private String code; //编码
	private String des;  //名称
	private String typ;  //类型 
	private String locked;//是否使用
	private String codetyp;//报货方式
	private Integer days;  //几日可用
	private Integer beatsequence;//拍序列
	//private String parent_id;//父节点
	private Integer safetyvalue;//安全值 wj 15.01.12
	private String locale;//语言类型
	
	public Integer getBeatsequence() {
		return beatsequence;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
	public void setBeatsequence(Integer beatsequence) {
		this.beatsequence = beatsequence;
	}
	public String getCodetyp() {
		return codetyp;
	}
	public void setCodetyp(String codetyp) {
		this.codetyp = codetyp;
	}
	public String getCode() {
		return code;
	}
	public String getLocked() {
		return locked;
	}
	public void setLocked(String locked) {
		this.locked = locked;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	/*public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}*/
	public Integer getSafetyvalue() {
		return safetyvalue;
	}
	public void setSafetyvalue(Integer safetyvalue) {
		this.safetyvalue = safetyvalue;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
}
