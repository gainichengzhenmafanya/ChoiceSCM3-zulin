package com.choice.scm.domain;

/**
* 创建日期:
* @author ice 
*/
public class HolidayBoh  {
	private String pk_holidaydefine;
	private String vyear;
	private String dholidaydate;
	private String vholidayname;
	private Integer enablestate=2;
	private int vholidaygrade;
	private String vmemo;
	private String creator;
	private String creationtime;
	private String modifier;
	private String modifiedtime;
	private String ts;
	
	public String getPk_holidaydefine() {
		return pk_holidaydefine;
	}
	public void setPk_holidaydefine(String pk_holidaydefine) {
		this.pk_holidaydefine = pk_holidaydefine;
	}
	public String getVyear() {
		return vyear;
	}
	public void setVyear(String vyear) {
		this.vyear = vyear;
	}
	public String getDholidaydate() {
		return dholidaydate;
	}
	public void setDholidaydate(String dholidaydate) {
		this.dholidaydate = dholidaydate;
	}
	public String getVholidayname() {
		return vholidayname;
	}
	public void setVholidayname(String vholidayname) {
		this.vholidayname = vholidayname;
	}
	
	public int getVholidaygrade() {
		return vholidaygrade;
	}
	public void setVholidaygrade(int vholidaygrade) {
		this.vholidaygrade = vholidaygrade;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifiedtime() {
		return modifiedtime;
	}
	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
}