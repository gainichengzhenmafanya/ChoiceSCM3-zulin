package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.choice.framework.util.DateFormat;

/**
 * 请购单--副表结构
 * @author 王吉峰
 *
 */
public class Praybilld {
	private List<Praybilld> praybillList;
	private Praybillm praybillm;
	private Positn    position;//仓位
	private Supply	  supply; 		 //物资
	private String	  acct        	;//	帐套
	private String	  yearr       	;//	年度
	private Integer	  id          	;//	流水号
	private Integer	  praybillmNo   	;//	申购单号
	private double	  price       	;//	单价
	private double	  amount      	;//	数量
	private String	  hoped      	;//	希望到货日期
	private String	  status     	;//	状态暂无用
	private String	  memo       	;//	备注
	private Deliver   deliver    	;//	供应商填制后
	private double	  amountin    	;//	实际到货数量
	private Date	  ind        	;//	实际到货日期
	private double	  pricein     	;//	入库金额
	private String	  chkin       	;//	是否已入库
	private Date	  dued       	;//	有效日期
	private String	  chkout      	;//	是否已出库
	private String	  chkinout		;//	方向：直发、入库、直配、出库
	private String	  chkinout2    	;//	是否已直拨  已无用
	private String	  chksend     	;//	是否已上传  暂无用
	private String	  inout       	;//	方向：直发、入库、直配、出库
	private String	  memo1      	;//	备注1
	private double	  totalAmt    	;//	金额
	private double	  amount1     	;//	第二单位数量
	private double	  amount1in   	;//	第二单位入库数量 也就是验货数量
	private double     pricesale;
	private String	  chksta      	;//	状态
	private String	  empnam      	;//	员工
	private Date      amoutdate		;//验货日期
	private double	  amountbla   	;//	申购门店的库存余额
	private String	  positn     	;//	制定到哪个仓库出库
	private String	  chk1emp   	;//	审核1  暂不考虑
	private String    chk1tim;
	private String    chk1;
	private String    chk2emp;
	private String    chk2tim;
	private Integer   prncnt;
	private double     amountsto;
	private double     amount1sto;
	private double     pricesto;
	private String    empfirm;
	private String    chkyh;
	private int    psorder;  //配送顺序。报货分拨拆分的时候用
	private String    ynpsorder;//是否按照配送顺序
	private Integer   pr            ;//接收存储过程的输出参数
	//新加
	private String    iszs          ;//是否赠送；
	
	public Praybillm getPraybillm() {
		return praybillm;
	}
	public void setPraybillm(Praybillm praybillm) {
		this.praybillm = praybillm;
	}
	public String getChkinout() {
		return chkinout;
	}
	public void setChkinout(String chkinout) {
		this.chkinout = chkinout;
	}
	public double getPricesale() {
		return pricesale;
	}
	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}
	public int getPsorder() {
		return psorder;
	}
	public void setPsorder(int psorder) {
		this.psorder = psorder;
	}
	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}
	public Deliver getDeliver() {
		return deliver;
	}
	public Positn getPosition() {
		return position;
	}
	public void setPosition(Positn position) {
		this.position = position;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getHoped() {
		return hoped;
	}
	public void setHoped(String hoped) {
		this.hoped = hoped;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public double getAmountin() {
		return amountin;
	}
	public void setAmountin(double amountin) {
		this.amountin = amountin;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public double getPricein() {
		return pricein;
	}
	public void setPricein(double pricein) {
		this.pricein = pricein;
	}
	public String getChkin() {
		return chkin;
	}
	public void setChkin(String chkin) {
		this.chkin = chkin;
	}
	public Date getDued() {
		return dued;
	}
	public void setDued(Date dued) {
		this.dued = dued;
	}
	public String getChkout() {
		return chkout;
	}
	public void setChkout(String chkout) {
		this.chkout = chkout;
	}
	public String getChkinout2() {
		return chkinout2;
	}
	public void setChkinout2(String chkinout2) {
		this.chkinout2 = chkinout2;
	}
	public String getChksend() {
		return chksend;
	}
	public void setChksend(String chksend) {
		this.chksend = chksend;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public double getAmount1() {
		return amount1;
	}
	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}
	public double getAmount1in() {
		return amount1in;
	}
	public void setAmount1in(double amount1in) {
		this.amount1in = amount1in;
	}
	public String getChksta() {
		return chksta;
	}
	public void setChksta(String chksta) {
		this.chksta = chksta;
	}
	public String getEmpnam() {
		return empnam;
	}
	public void setEmpnam(String empnam) {
		this.empnam = empnam;
	}
	public double getAmountbla() {
		return amountbla;
	}
	public void setAmountbla(double amountbla) {
		this.amountbla = amountbla;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getChk1emp() {
		return chk1emp;
	}
	public void setChk1emp(String chk1emp) {
		this.chk1emp = chk1emp;
	}
	public String getChk1tim() {
		return chk1tim;
	}
	public void setChk1tim(String chk1tim) {
		this.chk1tim = chk1tim;
	}
	public String getChk1() {
		return chk1;
	}
	public void setChk1(String chk1) {
		this.chk1 = chk1;
	}
	public String getChk2emp() {
		return chk2emp;
	}
	public void setChk2emp(String chk2emp) {
		this.chk2emp = chk2emp;
	}
	public String getChk2tim() {
		return chk2tim;
	}
	public void setChk2tim(String chk2tim) {
		this.chk2tim = chk2tim;
	}
	public Integer getPrncnt() {
		return prncnt;
	}
	public void setPrncnt(Integer prncnt) {
		this.prncnt = prncnt;
	}
	public double getAmountsto() {
		return amountsto;
	}
	public void setAmountsto(double amountsto) {
		this.amountsto = amountsto;
	}
	public double getAmount1sto() {
		return amount1sto;
	}
	public void setAmount1sto(double amount1sto) {
		this.amount1sto = amount1sto;
	}
	public double getPricesto() {
		return pricesto;
	}
	public void setPricesto(double pricesto) {
		this.pricesto = pricesto;
	}
	public String getEmpfirm() {
		return empfirm;
	}
	public void setEmpfirm(String empfirm) {
		this.empfirm = empfirm;
	}
	public String getChkyh() {
		return chkyh;
	}
	public void setChkyh(String chkyh) {
		this.chkyh = chkyh;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	
	public List<Praybilld> getPraybillList() {
		return praybillList;
	}
	public void setPraybillList(List<Praybilld> praybillList) {
		this.praybillList = praybillList;
	}
	public Integer getPraybillmNo() {
		return praybillmNo;
	}
	public void setPraybillmNo(Integer praybillmNo) {
		this.praybillmNo = praybillmNo;
	}
	public Date getAmoutdate() {
		return amoutdate;
	}
	public void setAmoutdate(Date amoutdate) {
		this.amoutdate = amoutdate;
	}
	public String getYnpsorder() {
		return ynpsorder;
	}
	public void setYnpsorder(String ynpsorder) {
		this.ynpsorder = ynpsorder;
	}
	public String getIszs() {
		return iszs;
	}
	public void setIszs(String iszs) {
		this.iszs = iszs;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Praybilld.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Supply){
						result.append(((Supply)cur).getSp_code());
					}else if(cur instanceof Positn){
						result.append(((Positn)cur).getCode());
					}else if(cur instanceof Deliver){
						result.append(((Deliver)cur).getCode());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
}
