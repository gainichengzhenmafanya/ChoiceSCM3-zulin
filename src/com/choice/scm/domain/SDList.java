package com.choice.scm.domain;

import java.util.List;
/**
 * 价格模版list
 * @author yp
 */

public class SDList {

	List<SppriceDemo> demos ;

	public List<SppriceDemo> getDemos() {
		return demos;
	}

	public void setDemos(List<SppriceDemo> demos) {
		this.demos = demos;
	}
	
}
