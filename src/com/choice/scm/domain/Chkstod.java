package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.choice.framework.util.DateFormat;

/**
 * 报货单--副表结构
 * @author csb
 *
 */
public class Chkstod {
	private List<Chkstod> chkstodList;
	private Positn    position;//仓位
	private Supply	  supply;//供应商	
	private String	  acct        	;//	帐套
	private String	  yearr       	;//	年度
	private Integer chkstod_x_id	;//chkstod_x表的id
	private Integer	  id          	;//	流水号
	private Integer	  chkstoNo    	;//	申购单号
	private String    chkstoNos		;
	private String	  chkstoNo_c    ;//	申购拆单单号
	private double	  price       	;//	单价
	private Double	  amount      	;//	数量
	private String	  hoped      	;//	希望到货日期
	private String	  status     	;//	状态暂无用
	private String	  memo       	;//	备注
	private Deliver   deliver    	;//	供应商填制后ADD_CHKSTOD过程生成
	private double	  amountin    	;//	入库数量
	private Date	  ind        	;//	到货日期
	private double	  pricein     	;//	入库金额
	private String	  chkin       	;//	是否已入库
	private Date	  dued       	;//	生产日期
	private String	  chkout      	;//	是否已出库
	private String	  chkinout		;//	方向：直发、入库、直配、出库
	private String	  chkinout2    	;//	是否已直拨  已无用
	private String	  chksend     	;//	是否已上传  暂无用
	private String	  inout       	;//	方向：直发、入库、直配、出库
	private double	  pound       	;//	暂无用、权金城用作固定计算数量
	private Integer	  sp_id       	;//	暂无用
	private String	  sta         	;//	状态：是否有效
	private String	  memo1      	;//	备注1
	private double	  totalAmt    	;//	金额
	private double	  amount1     	;//	第二单位数量
	private double	  amount1in   	;//	第二单位入库数量 也就是验货数量
	private double     pricesale;
	private String	  chksta      	;//	状态
	private String	  empnam      	;//	员工
	private double	  amountbla   	;//	申购门店的库存余额
	private String	  positn     	;//	制定到哪个仓库出库
	private String	  firm     	;//	申购单位
	private String	  chk1emp   	;//	审核1  暂不考虑
	private String    chk1tim;
	private String    chk1;
	private String    chk2emp;
	private String    chk2tim;
	private Integer   prncnt;
	private double     amountsto;
	private double     amount1sto;
	private double     pricesto;
	private String    empfirm;
	private String    chkyh;
	private int    psorder;  //配送顺序。报货分拨拆分的时候用
	private Integer   pr            ;//接收存储过程的输出参数
	private String    iszs          ;//是否赠送；
	//档口报货单用
	private String	  bak1       	;//	暂无用
	private Integer	  bak2       	;//	暂无用
	private String puprorderemp;//采购订单制单人
	private String puprordertim;//采购订单制单时间
	private String ispuprorder;//是否已经生成采购订单
	private double cntmin;//最低库存   安全库存报货用 wjf
	private double cntmax;//最高库存   安全库存报货用 wjf
	private double cntnow;//当前库存   安全库存报货用 wjf
	private double cntway;//在途数量   安全库存报货用 wjf
	private double cntuse;//需求量   安全库存报货用 wjf
	
	private double stomin;//最小申购量
	private double stocnt;//申购倍数
	
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	private double dismincnt;//配送单位最小申购量
	private double amountdis;//配送单位数量
	
	public String getPuprorderemp() {
		return puprorderemp;
	}
	public void setPuprorderemp(String puprorderemp) {
		this.puprorderemp = puprorderemp;
	}
	public String getPuprordertim() {
		return puprordertim;
	}
	public void setPuprordertim(String puprordertim) {
		this.puprordertim = puprordertim;
	}
	public String getIspuprorder() {
		return ispuprorder;
	}
	public void setIspuprorder(String ispuprorder) {
		this.ispuprorder = ispuprorder;
	}
	public double getCntmin() {
		return cntmin;
	}
	public void setCntmin(double cntmin) {
		this.cntmin = cntmin;
	}
	public double getCntmax() {
		return cntmax;
	}
	public void setCntmax(double cntmax) {
		this.cntmax = cntmax;
	}
	public double getCntnow() {
		return cntnow;
	}
	public void setCntnow(double cntnow) {
		this.cntnow = cntnow;
	}
	public double getCntway() {
		return cntway;
	}
	public void setCntway(double cntway) {
		this.cntway = cntway;
	}
	public double getCntuse() {
		return cntuse;
	}
	public void setCntuse(double cntuse) {
		this.cntuse = cntuse;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getChkstoNos() {
		return chkstoNos;
	}
	public void setChkstoNos(String chkstoNos) {
		this.chkstoNos = chkstoNos;
	}
	public Integer getChkstod_x_id() {
		return chkstod_x_id;
	}
	public void setChkstod_x_id(Integer chkstod_x_id) {
		this.chkstod_x_id = chkstod_x_id;
	}
	public String getChkinout() {
		return chkinout;
	}
	public void setChkinout(String chkinout) {
		this.chkinout = chkinout;
	}
	public double getPricesale() {
		return pricesale;
	}
	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}
	public int getPsorder() {
		return psorder;
	}
	public void setPsorder(int psorder) {
		this.psorder = psorder;
	}
	public List<Chkstod> getChkstodList() {
		return chkstodList;
	}
	public void setChkstodList(List<Chkstod> chkstodList) {
		this.chkstodList = chkstodList;
	}
	public String getChkstoNo_c() {
		return chkstoNo_c;
	}
	public void setChkstoNo_c(String chkstoNo_c) {
		this.chkstoNo_c = chkstoNo_c;
	}
	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}
	public Deliver getDeliver() {
		return deliver;
	}
	public Positn getPosition() {
		return position;
	}
	public void setPosition(Positn position) {
		this.position = position;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getChkstoNo() {
		return chkstoNo;
	}
	public void setChkstoNo(Integer chkstoNo) {
		this.chkstoNo = chkstoNo;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getHoped() {
		return hoped;
	}
	public void setHoped(String hoped) {
		this.hoped = hoped;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public Integer getBak2() {
		return bak2;
	}
	public void setBak2(Integer bak2) {
		this.bak2 = bak2;
	}
	public double getAmountin() {
		return amountin;
	}
	public void setAmountin(double amountin) {
		this.amountin = amountin;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public double getPricein() {
		return pricein;
	}
	public void setPricein(double pricein) {
		this.pricein = pricein;
	}
	public String getChkin() {
		return chkin;
	}
	public void setChkin(String chkin) {
		this.chkin = chkin;
	}
	public Date getDued() {
		return dued;
	}
	public void setDued(Date dued) {
		this.dued = dued;
	}
	public String getChkout() {
		return chkout;
	}
	public void setChkout(String chkout) {
		this.chkout = chkout;
	}
	public String getChkinout2() {
		return chkinout2;
	}
	public void setChkinout2(String chkinout2) {
		this.chkinout2 = chkinout2;
	}
	public String getChksend() {
		return chksend;
	}
	public void setChksend(String chksend) {
		this.chksend = chksend;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public double getPound() {
		return pound;
	}
	public void setPound(double pound) {
		this.pound = pound;
	}
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public double getAmount1() {
		return amount1;
	}
	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}
	public double getAmount1in() {
		return amount1in;
	}
	public void setAmount1in(double amount1in) {
		this.amount1in = amount1in;
	}
	public String getChksta() {
		return chksta;
	}
	public void setChksta(String chksta) {
		this.chksta = chksta;
	}
	public String getEmpnam() {
		return empnam;
	}
	public void setEmpnam(String empnam) {
		this.empnam = empnam;
	}
	public double getAmountbla() {
		return amountbla;
	}
	public void setAmountbla(double amountbla) {
		this.amountbla = amountbla;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getChk1emp() {
		return chk1emp;
	}
	public void setChk1emp(String chk1emp) {
		this.chk1emp = chk1emp;
	}
	public String getChk1tim() {
		return chk1tim;
	}
	public void setChk1tim(String chk1tim) {
		this.chk1tim = chk1tim;
	}
	public String getChk1() {
		return chk1;
	}
	public void setChk1(String chk1) {
		this.chk1 = chk1;
	}
	public String getChk2emp() {
		return chk2emp;
	}
	public void setChk2emp(String chk2emp) {
		this.chk2emp = chk2emp;
	}
	public String getChk2tim() {
		return chk2tim;
	}
	public void setChk2tim(String chk2tim) {
		this.chk2tim = chk2tim;
	}
	public Integer getPrncnt() {
		return prncnt;
	}
	public void setPrncnt(Integer prncnt) {
		this.prncnt = prncnt;
	}
	public double getAmountsto() {
		return amountsto;
	}
	public void setAmountsto(double amountsto) {
		this.amountsto = amountsto;
	}
	public double getAmount1sto() {
		return amount1sto;
	}
	public void setAmount1sto(double amount1sto) {
		this.amount1sto = amount1sto;
	}
	public double getPricesto() {
		return pricesto;
	}
	public void setPricesto(double pricesto) {
		this.pricesto = pricesto;
	}
	public String getEmpfirm() {
		return empfirm;
	}
	public void setEmpfirm(String empfirm) {
		this.empfirm = empfirm;
	}
	public String getChkyh() {
		return chkyh;
	}
	public void setChkyh(String chkyh) {
		this.chkyh = chkyh;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public String getIszs() {
		return iszs;
	}
	public void setIszs(String iszs) {
		this.iszs = iszs;
	}
	
	public double getStomin() {
		return stomin;
	}
	public void setStomin(double stomin) {
		this.stomin = stomin;
	}
	public double getStocnt() {
		return stocnt;
	}
	public void setStocnt(double stocnt) {
		this.stocnt = stocnt;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public double getDismincnt() {
		return dismincnt;
	}
	public void setDismincnt(double dismincnt) {
		this.dismincnt = dismincnt;
	}
	public double getAmountdis() {
		return amountdis;
	}
	public void setAmountdis(double amountdis) {
		this.amountdis = amountdis;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Chkstod.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Supply){
						result.append(((Supply)cur).getSp_code());
					}else if(cur instanceof Positn){
						result.append(((Positn)cur).getCode());
					}else if(cur instanceof Deliver){
						result.append(((Deliver)cur).getCode());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
}
