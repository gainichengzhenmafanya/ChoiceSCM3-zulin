package com.choice.scm.domain;
/**
 * 报销单--副表结构
 * @author csb
 *
 */
public class Chkpayd {

	private String acct;//帐套                           
	private String yearr;//年份                           
	private Integer id;//主键                          
	private String memo;//备注                        
	private Integer chkno;//单号                           
	private Typ typ;//类别                        
	private String typDes;//类别名称                        
	private Deliver deliver;//供应商                        
	private String deliverDes;//供应商名称                        
	private double typAmt;//金额
	private double deliverAmt;//入库金额
	private double deliverPay;//审核可支付
	private String price;//报销物品的价格
	private String quantity;//报销物品数量
	private String uses;//用途
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quality) {
		this.quantity = quality;
	}
	public String getUses() {
		return uses;
	}
	public void setUses(String uses) {
		this.uses = uses;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getChkno() {
		return chkno;
	}
	public void setChkno(Integer chkno) {
		this.chkno = chkno;
	}
	public Typ getTyp() {
		return typ;
	}
	public void setTyp(Typ typ) {
		this.typ = typ;
	}
	public String getTypDes() {
		return typDes;
	}
	public void setTypDes(String typDes) {
		this.typDes = typDes;
	}
	public Deliver getDeliver() {
		return deliver;
	}
	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public double getTypAmt() {
		return typAmt;
	}
	public void setTypAmt(double typAmt) {
		this.typAmt = typAmt;
	}
	public double getDeliverAmt() {
		return deliverAmt;
	}
	public void setDeliverAmt(double deliverAmt) {
		this.deliverAmt = deliverAmt;
	}
	public double getDeliverPay() {
		return deliverPay;
	}
	public void setDeliverPay(double deliverPay) {
		this.deliverPay = deliverPay;
	}

}
