package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 领料单(单表)
 * @author css
 *
 */
public class ExplanSpcode {

	private Integer no;		//领料单号
	private String sp_code;	//物资编码
	private double cnt;	   	//数量1
	private String positn; 	//使用仓位（半成品加工间）
	private Date dat;		//领料时间
	private String madeby;	//领料人
	private String memo;   	//备注
	private String chk;   	//审核
	private String checkby;	//审核人
	private String positndes;//使用仓位名称
	private Supply supply;//物资
	private List<ExplanSpcode> explanSpcodeList;   //领料详情
	
	public String getCheckby() {
		return checkby;
	}
	public void setCheckby(String checkby) {
		this.checkby = checkby;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<ExplanSpcode> getExplanSpcodeList() {
		return explanSpcodeList;
	}
	public void setExplanSpcodeList(List<ExplanSpcode> explanSpcodeList) {
		this.explanSpcodeList = explanSpcodeList;
	}
	public String getPositndes() {
		return positndes;
	}
	public void setPositndes(String positndes) {
		this.positndes = positndes;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
}
