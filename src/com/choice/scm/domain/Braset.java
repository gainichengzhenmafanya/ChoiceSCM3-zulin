package com.choice.scm.domain;

import java.util.List;

/**
 * 分店结算
 * Created by mc on 14-12-30.
 */
public class Braset {
    private String firm;//分店编码
    private String firm_name;//分店名称
    private String positn;//仓位
    private String positn_name;//仓位名称
    private String deliver;//供应商编码
    private String deliver_name;//供应商名称
    private String typ;//单据类型
    private String bill;//单据付款状态
    private String folio;//结算状态
    private String inout;//入库状态 0 全部 1 仅出库 2 仅直发
    private String partPay;//部分支付
    private String bdat;//开始时间
    private String edat;//结束时间
    private Integer chkinno;//单据号
    private String vouno;//凭证号
    private double amt;//总金额
    private double pay;//支付金额
    private double amtye;//未付金额
    private double t_amt;//总 总金额
    private double t_pay;//总 支付金额
    private double t_Amtye;//总 未付金额
    private String accountId;
    private String billTyp;//单据类型标识 1 出库 其他为其他单据

    private String invalue;//入库单号
    private String outvalue;//出库单号
    private List<String> positnList;
    private List<String> firmList;
    private List<String> deliverList;
    private String positns;
    private String firms;
    private String delivers;
    
    
    
    public String getPositns() {
		return positns;
	}

	public void setPositns(String positns) {
		this.positns = positns;
	}

	public String getFirms() {
		return firms;
	}

	public void setFirms(String firms) {
		this.firms = firms;
	}

	public String getDelivers() {
		return delivers;
	}

	public void setDelivers(String delivers) {
		this.delivers = delivers;
	}

	private String locale;

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getFirm_name() {
        return firm_name;
    }

    public void setFirm_name(String firm_name) {
        this.firm_name = firm_name;
    }

    public String getPositn() {
        return positn;
    }

    public void setPositn(String positn) {
        this.positn = positn;
    }

    public String getPositn_name() {
        return positn_name;
    }

    public void setPositn_name(String positn_name) {
        this.positn_name = positn_name;
    }

    public String getDeliver() {
        return deliver;
    }

    public void setDeliver(String deliver) {
        this.deliver = deliver;
    }

    public String getDeliver_name() {
        return deliver_name;
    }

    public void setDeliver_name(String deliver_name) {
        this.deliver_name = deliver_name;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getInout() {
        return inout;
    }

    public void setInout(String inout) {
        this.inout = inout;
    }

    public String getPartPay() {
        return partPay;
    }

    public void setPartPay(String partPay) {
        this.partPay = partPay;
    }

    public String getBdat() {
        return bdat;
    }

    public void setBdat(String bdat) {
        this.bdat = bdat;
    }

    public String getEdat() {
        return edat;
    }

    public void setEdat(String edat) {
        this.edat = edat;
    }

    public Integer getChkinno() {
        return chkinno;
    }

    public void setChkinno(Integer chkinno) {
        this.chkinno = chkinno;
    }

    public String getVouno() {
        return vouno;
    }

    public void setVouno(String vouno) {
        this.vouno = vouno;
    }

    public List<String> getPositnList() {
        return positnList;
    }

    public void setPositnList(List<String> positnList) {
        this.positnList = positnList;
    }

    public List<String> getFirmList() {
        return firmList;
    }

    public void setFirmList(List<String> firmList) {
        this.firmList = firmList;
    }

    public List<String> getDeliverList() {
        return deliverList;
    }

    public void setDeliverList(List<String> deliverList) {
        this.deliverList = deliverList;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBillTyp() {
        return billTyp;
    }

    public void setBillTyp(String billTyp) {
        this.billTyp = billTyp;
    }

    public String getInvalue() {
        return invalue;
    }

    public void setInvalue(String invalue) {
        this.invalue = invalue;
    }

    public String getOutvalue() {
        return outvalue;
    }

    public void setOutvalue(String outvalue) {
        this.outvalue = outvalue;
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    public double getAmtye() {
        return amtye;
    }

    public void setAmtye(double amtye) {
        this.amtye = amtye;
    }

    public double getT_amt() {
        return t_amt;
    }

    public void setT_amt(double t_amt) {
        this.t_amt = t_amt;
    }

    public double getT_pay() {
        return t_pay;
    }

    public void setT_pay(double t_pay) {
        this.t_pay = t_pay;
    }

    public double getT_Amtye() {
        return t_Amtye;
    }

    public void setT_Amtye(double t_Amtye) {
        this.t_Amtye = t_Amtye;
    }

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
}
