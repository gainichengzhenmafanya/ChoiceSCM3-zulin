package com.choice.scm.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 * 物资价格模板
 * @author yp
 */

public class SppriceDemo {

	private String acct;	//帐套号
	private String firm;	//分店
	private String deliver;	//供应商
	private String sp_code;	//物资编码
	private Date bdat;		//开始日期
	private Date edat;		//结束日期
	private BigDecimal price;	//单价
	private String memo;	//备注
	private BigDecimal priceold;	//上期单价
	
	private String sp_name;	//物资名称
	private String sp_desc;	//规格
	private String sp_mark;	//品牌
	private String sp_addr;	//产地
	private String unit;	//单位
	private String deliverDes;//供应商名称
	private String firmDes;
	private String positnCode;//复制到门店code
	private String madeby;//操作人
	private Integer pr;         //用来接收存储过程的输出标识
	private String type;		//用于区分报价售价，报价为0 售价为1
	
	private String czdate;//操作日期，洞庭项目要求按照操作日期排序
	
	public String getCzdate() {
		return czdate;
	}
	public void setCzdate(String czdate) {
		this.czdate = czdate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	private List<SppriceDemo> suppriceDemoList; 
	private List<Positn> positnList;
	
	public SppriceDemo(){
		this.price = BigDecimal.valueOf(0);
		this.priceold = BigDecimal.valueOf(0);
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPriceold() {
		return priceold;
	}
	public void setPriceold(BigDecimal priceold) {
		this.priceold = priceold;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getSp_mark() {
		return sp_mark;
	}
	public void setSp_mark(String sp_mark) {
		this.sp_mark = sp_mark;
	}
	public String getSp_addr() {
		return sp_addr;
	}
	public void setSp_addr(String sp_addr) {
		this.sp_addr = sp_addr;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getFirmDes() {
		return firmDes;
	}
	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}
	public Spprice toSpprice(){
		Spprice cur = new Spprice();
		cur.setAcct(this.acct);
		cur.setArea(this.firm);
		cur.setBdat(this.bdat);
		cur.setEdat(this.edat);
		cur.setDeliver(this.deliver);
		cur.setSp_code(this.sp_code);
		cur.setPrice(this.price);
		cur.setMemo(this.memo);
		cur.setPriceold(this.priceold);
		return cur;
	}
	public List<SppriceDemo> getSuppriceDemoList() {
		return suppriceDemoList;
	}
	public void setSuppriceDemoList(List<SppriceDemo> suppriceDemoList) {
		this.suppriceDemoList = suppriceDemoList;
	}
	public List<Positn> getPositnList() {
		return positnList;
	}
	public void setPositnList(List<Positn> positnList) {
		this.positnList = positnList;
	}
	
}
