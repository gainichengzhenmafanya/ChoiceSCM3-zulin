package com.choice.scm.domain;

/**
 * 菜谱方案
 * @author css
 *
 */
public class ItemPrgm {
	private String  id;//ID
	private String des;//名称
	private String memo;//备注
	private String  sta;//状态
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}	
}
