package com.choice.scm.domain;

import java.util.List;

/**
 * 线路
 *
 */
public class LinesFirm {
	private String acct;
	private String code;//线路编码
	private String des;//线路名称
	private String des1;//线路
	private double mileage;//里程
	private double staoil;//标准油耗
	private String memo;//备注
	private List<AcctLineFirm> acctLineFirmList;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getDes1() {
		return des1;
	}
	public void setDes1(String des1) {
		this.des1 = des1;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public double getMileage() {
		return mileage;
	}
	public void setMileage(double mileage) {
		this.mileage = mileage;
	}
	public double getStaoil() {
		return staoil;
	}
	public void setStaoil(double staoil) {
		this.staoil = staoil;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<AcctLineFirm> getAcctLineFirmList() {
		return acctLineFirmList;
	}
	public void setAcctLineFirmList(List<AcctLineFirm> acctLineFirmList) {
		this.acctLineFirmList = acctLineFirmList;
	}
	 
}
