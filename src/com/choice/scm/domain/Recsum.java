package com.choice.scm.domain;

import java.util.List;

/**
 * 应收款汇总
 * Created by mc on 14-12-27.
 */
public class Recsum {
    private String code;//分店编码
    private String name;//分店名称
    private String firm;
    private String firm_name;
    private String positn;//仓位
    private String positn_name;//仓位
    private String isCheck;//是否审核
    private String priUnsett;//上期未结
    private String currentStock;//本期进货
    private String currentTett;//本期已结
    private String totalUnsett;//合计未结
    private String t_priUnsett;//上期未结 合计
    private String t_currentStock;//本期进货 合计
    private String t_currentTett;//本期已结 合计
    private String t_totalUnsett;//合计未结 合计
    private String bdat;
    private String edat;
    private String accountId;

    public List<String> positnList;
    public List<String> firmList;

    private String  positns;
    private String  firms;
    
    
    public String getPositns() {
		return positns;
	}

	public void setPositns(String positns) {
		this.positns = positns;
	}

	public String getFirms() {
		return firms;
	}

	public void setFirms(String firms) {
		this.firms = firms;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getFirm_name() {
        return firm_name;
    }

    public void setFirm_name(String firm_name) {
        this.firm_name = firm_name;
    }

    public String getPositn_name() {
        return positn_name;
    }

    public void setPositn_name(String positn_name) {
        this.positn_name = positn_name;
    }

    public String getPositn() {
        return positn;
    }

    public void setPositn(String positn) {
        this.positn = positn;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    public String getPriUnsett() {
        return priUnsett;
    }

    public void setPriUnsett(String priUnsett) {
        this.priUnsett = priUnsett;
    }

    public String getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(String currentStock) {
        this.currentStock = currentStock;
    }

    public String getCurrentTett() {
        return currentTett;
    }

    public void setCurrentTett(String currentTett) {
        this.currentTett = currentTett;
    }

    public String getTotalUnsett() {
        return totalUnsett;
    }

    public void setTotalUnsett(String totalUnsett) {
        this.totalUnsett = totalUnsett;
    }

    public String getT_priUnsett() {
        return t_priUnsett;
    }

    public void setT_priUnsett(String t_priUnsett) {
        this.t_priUnsett = t_priUnsett;
    }

    public String getT_currentStock() {
        return t_currentStock;
    }

    public void setT_currentStock(String t_currentStock) {
        this.t_currentStock = t_currentStock;
    }

    public String getT_currentTett() {
        return t_currentTett;
    }

    public void setT_currentTett(String t_currentTett) {
        this.t_currentTett = t_currentTett;
    }

    public String getBdat() {
        return bdat;
    }

    public void setBdat(String bdat) {
        this.bdat = bdat;
    }

    public String getEdat() {
        return edat;
    }

    public void setEdat(String edat) {
        this.edat = edat;
    }

    public List<String> getPositnList() {
        return positnList;
    }

    public void setPositnList(List<String> positnList) {
        this.positnList = positnList;
    }

    public List<String> getFirmList() {
        return firmList;
    }

    public void setFirmList(List<String> firmList) {
        this.firmList = firmList;
    }

    public String getT_totalUnsett() {
        return t_totalUnsett;
    }

    public void setT_totalUnsett(String t_totalUnsett) {
        this.t_totalUnsett = t_totalUnsett;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
