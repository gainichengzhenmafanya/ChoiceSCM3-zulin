package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 预估菜品销售计划
 * @author css
 *
 */
public class PosItemPlan {   

	private List<PosItemPlan> PosItemPlanList;   //修改用
	private List<String> firmList;   //分店号list
	private int sft;//班
	private double cntyg;//预估
	private double cntact;//实际
	private double cntcy;//差异
	private String tim;//分店号
	private String firmNm;//分店名
	private String firm;//分店号
	private Date dat;//日期
	private Date startdate;//起始日期
	private Date enddate;//结束日期
	private String item;//菜品流水号
	private String itcode;//菜品编码
	private String itdes;//菜品名称
	private String itunit;//菜品单位
	private double cnt1;//一班调整
	private double cnt2;//二班调整
	private double cnt3;//三班调整
	private double cnt4;//四班调整
	
	private double cnt1old;//一班预估
	private double cnt2old;//二班预估
	private double cnt3old;//三班预估
	private double cnt4old;//四班预估
	
	private double cnt1act;//一班实际
	private double cnt2act;//二班实际
	private double cnt3act;//三班实际
	private double cnt4act;//四班实际
	
	private double lv1;// 一班预估差异率
	private double lv2;// 二班预估差异率
	private double lv3;// 三班预估差异率
	private double lv4;// 四班预估差异率
	
	private double lv1old;// 一班调整差异率
	private double lv2old;// 二班调整差异率
	private double lv3old;// 三班调整差异率
	private double lv4old;// 四班调整差异率
	
	private double total;// 合计调整
	private double totalold;// 合计预估
	private double totalact;// 合计实际
	private double totallv;// 合计预估差异率
	private double totallvold;// 合计预估差异率
	
	private String dept;//档口编码
	private String deptdes;//档口名称
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
	
	public String getFirmNm() {
		return firmNm;
	}
	public void setFirmNm(String firmNm) {
		this.firmNm = firmNm;
	}
	public List<String> getFirmList() {
		return firmList;
	}
	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}
	public double getCntcy() {
		return cntcy;
	}
	public void setCntcy(double cntcy) {
		this.cntcy = cntcy;
	}
	public double getCntyg() {
		return cntyg;
	}
	public void setCntyg(double cntyg) {
		this.cntyg = cntyg;
	}
	public double getCntact() {
		return cntact;
	}
	public void setCntact(double cntact) {
		this.cntact = cntact;
	}
	public String getTim() {
		return tim;
	}
	public void setTim(String tim) {
		this.tim = tim;
	}
	public int getSft() {
		return sft;
	}
	public void setSft(int sft) {
		this.sft = sft;
	}
	public double getCnt1act() {
		return cnt1act;
	}
	public void setCnt1act(double cnt1act) {
		this.cnt1act = cnt1act;
	}
	public double getCnt2act() {
		return cnt2act;
	}
	public void setCnt2act(double cnt2act) {
		this.cnt2act = cnt2act;
	}
	public double getCnt3act() {
		return cnt3act;
	}
	public void setCnt3act(double cnt3act) {
		this.cnt3act = cnt3act;
	}
	public double getCnt4act() {
		return cnt4act;
	}
	public void setCnt4act(double cnt4act) {
		this.cnt4act = cnt4act;
	}
	public double getLv1() {
		return lv1;
	}
	public void setLv1(double lv1) {
		this.lv1 = lv1;
	}
	public double getLv2() {
		return lv2;
	}
	public void setLv2(double lv2) {
		this.lv2 = lv2;
	}
	public double getLv3() {
		return lv3;
	}
	public void setLv3(double lv3) {
		this.lv3 = lv3;
	}
	public double getLv4() {
		return lv4;
	}
	public void setLv4(double lv4) {
		this.lv4 = lv4;
	}
	public double getLv1old() {
		return lv1old;
	}
	public void setLv1old(double lv1old) {
		this.lv1old = lv1old;
	}
	public double getLv2old() {
		return lv2old;
	}
	public void setLv2old(double lv2old) {
		this.lv2old = lv2old;
	}
	public double getLv3old() {
		return lv3old;
	}
	public void setLv3old(double lv3old) {
		this.lv3old = lv3old;
	}
	public double getLv4old() {
		return lv4old;
	}
	public void setLv4old(double lv4old) {
		this.lv4old = lv4old;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getTotalold() {
		return totalold;
	}
	public void setTotalold(double totalold) {
		this.totalold = totalold;
	}
	public double getTotalact() {
		return totalact;
	}
	public void setTotalact(double totalact) {
		this.totalact = totalact;
	}
	public double getTotallv() {
		return totallv;
	}
	public void setTotallv(double totallv) {
		this.totallv = totallv;
	}
	public double getTotallvold() {
		return totallvold;
	}
	public void setTotallvold(double totallvold) {
		this.totallvold = totallvold;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public List<PosItemPlan> getPosItemPlanListList() {
		return PosItemPlanList;
	}
	public void setPosItemPlanList(List<PosItemPlan> posItemPlanListList) {
		PosItemPlanList = posItemPlanListList;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public double getCnt3() {
		return cnt3;
	}
	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}
	public double getCnt4() {
		return cnt4;
	}
	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}
	public double getCnt1old() {
		return cnt1old;
	}
	public void setCnt1old(double cnt1old) {
		this.cnt1old = cnt1old;
	}
	public double getCnt2old() {
		return cnt2old;
	}
	public void setCnt2old(double cnt2old) {
		this.cnt2old = cnt2old;
	}
	public double getCnt3old() {
		return cnt3old;
	}
	public void setCnt3old(double cnt3old) {
		this.cnt3old = cnt3old;
	}
	public double getCnt4old() {
		return cnt4old;
	}
	public void setCnt4old(double cnt4old) {
		this.cnt4old = cnt4old;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getItdes() {
		return itdes;
	}
	public void setItdes(String itdes) {
		this.itdes = itdes;
	}
	public String getItunit() {
		return itunit;
	}
	public void setItunit(String itunit) {
		this.itunit = itunit;
	}
	public List<PosItemPlan> getPosItemPlanList() {
		return PosItemPlanList;
	}

}
