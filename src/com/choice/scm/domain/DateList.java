package com.choice.scm.domain;

import java.util.Date;

/**
 * 会计月
 * @author css
 *
 */
public class DateList {
	private Date bdat1;
	private Date edat1;
	private Date bdat2;
	private Date edat2;
	private Date bdat3;
	private Date edat3;
	private Date bdat4;
	private Date edat4;
	private Date bdat5;
	private Date edat5;
	private Date bdat6;
	private Date edat6;
	private Date bdat7;
	private Date edat7;
	private Date bdat8;
	private Date edat8;
	private Date bdat9;
	private Date edat9;
	private Date bdat10;
	private Date edat10;
	private Date bdat11;
	private Date edat11;
	private Date bdat12;
	private Date edat12;
	public Date getBdat1() {
		return bdat1;
	}
	public void setBdat1(Date bdat1) {
		this.bdat1 = bdat1;
	}
	public Date getEdat1() {
		return edat1;
	}
	public void setEdat1(Date edat1) {
		this.edat1 = edat1;
	}
	public Date getBdat2() {
		return bdat2;
	}
	public void setBdat2(Date bdat2) {
		this.bdat2 = bdat2;
	}
	public Date getEdat2() {
		return edat2;
	}
	public void setEdat2(Date edat2) {
		this.edat2 = edat2;
	}
	public Date getBdat3() {
		return bdat3;
	}
	public void setBdat3(Date bdat3) {
		this.bdat3 = bdat3;
	}
	public Date getEdat3() {
		return edat3;
	}
	public void setEdat3(Date edat3) {
		this.edat3 = edat3;
	}
	public Date getBdat4() {
		return bdat4;
	}
	public void setBdat4(Date bdat4) {
		this.bdat4 = bdat4;
	}
	public Date getEdat4() {
		return edat4;
	}
	public void setEdat4(Date edat4) {
		this.edat4 = edat4;
	}
	public Date getBdat5() {
		return bdat5;
	}
	public void setBdat5(Date bdat5) {
		this.bdat5 = bdat5;
	}
	public Date getEdat5() {
		return edat5;
	}
	public void setEdat5(Date edat5) {
		this.edat5 = edat5;
	}
	public Date getBdat6() {
		return bdat6;
	}
	public void setBdat6(Date bdat6) {
		this.bdat6 = bdat6;
	}
	public Date getEdat6() {
		return edat6;
	}
	public void setEdat6(Date edat6) {
		this.edat6 = edat6;
	}
	public Date getBdat7() {
		return bdat7;
	}
	public void setBdat7(Date bdat7) {
		this.bdat7 = bdat7;
	}
	public Date getEdat7() {
		return edat7;
	}
	public void setEdat7(Date edat7) {
		this.edat7 = edat7;
	}
	public Date getBdat8() {
		return bdat8;
	}
	public void setBdat8(Date bdat8) {
		this.bdat8 = bdat8;
	}
	public Date getEdat8() {
		return edat8;
	}
	public void setEdat8(Date edat8) {
		this.edat8 = edat8;
	}
	public Date getBdat9() {
		return bdat9;
	}
	public void setBdat9(Date bdat9) {
		this.bdat9 = bdat9;
	}
	public Date getEdat9() {
		return edat9;
	}
	public void setEdat9(Date edat9) {
		this.edat9 = edat9;
	}
	public Date getBdat10() {
		return bdat10;
	}
	public void setBdat10(Date bdat10) {
		this.bdat10 = bdat10;
	}
	public Date getEdat10() {
		return edat10;
	}
	public void setEdat10(Date edat10) {
		this.edat10 = edat10;
	}
	public Date getBdat11() {
		return bdat11;
	}
	public void setBdat11(Date bdat11) {
		this.bdat11 = bdat11;
	}
	public Date getEdat11() {
		return edat11;
	}
	public void setEdat11(Date edat11) {
		this.edat11 = edat11;
	}
	public Date getBdat12() {
		return bdat12;
	}
	public void setBdat12(Date bdat12) {
		this.bdat12 = bdat12;
	}
	public Date getEdat12() {
		return edat12;
	}
	public void setEdat12(Date edat12) {
		this.edat12 = edat12;
	}
	
}
