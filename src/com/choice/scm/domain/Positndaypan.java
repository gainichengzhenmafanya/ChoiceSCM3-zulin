package com.choice.scm.domain;

import java.util.Date;

/***
 * 分店日盘表
 * @author wjf
 *
 */
public class Positndaypan {
	
	private String positn;
	private String sp_code;
	private Date dat;
	private Double cnt;
	private Double cnt1;
	private String ynpd;//是否盘点
	private String dept;//档口
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public String getYnpd() {
		return ynpd;
	}
	public void setYnpd(String ynpd) {
		this.ynpd = ynpd;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
}
