package com.choice.scm.domain;

import java.util.List;

/**
 * 应付款汇总
 * Created by mc on 14-12-28.
 */
public class Accpaysum {
    private String code;//门店编码
    private String des;//门店名称
    private String deliverCode;//门店
    private String deliverDes;//门店名称
    private String positn;//仓位
    private String positn_name;//仓位名称
    private String beginTotalamt;   //期初 未审核 总金额
    private String beginPay;        //期初 未审核 已支付
    private String beginAmt;        //期初 未审核 未支付
    private String beginAmtpay;     //期初 未审核 未开发票
    private String thisTotalamt;    //本期 已审核 总金额
    private String thisPay;         //本期 已审核 已支付
    private String thisTotalamtUnaud;   //本期 未审核 总金额
    private String thisPayUnaud;        //本期 未审核 已支付
    private String thisInvpay;      //本期已开发票
    private String totalAmt;      //合计未付款
    private String totalInvAmt;      //总开发票仍未结


    private String t_beginTotalamt;   //期初 未审核 总金额
    private String t_beginPay;        //期初 未审核 已支付
    private String t_beginAmt;      //总合计 期初 未审核 未支付
    private String t_beginAmtpay;   //总合计 期初 未审核 未开发票
    private String t_thisTotalamt;  //总合计 本期 已审核 总金额
    private String t_thisPay;       //总合计 本期 已审核 已支付
    private String t_thisTotalamtUnaud;   //本期 未审核 总金额
    private String t_thisPayUnaud;        //本期 未审核 已支付
    private String t_thisInvpay;    //总合计 本期已开发票
    private String t_totalAmt;      //总合计 合计未付款
    private String t_totalInvAmt;   //总合计 总开发票仍未结

    private String bdat;
    private String edat;
    private String isDat;//是否日期
    private String isCheck;//是否只审核
    private String accountId;//用户ID

    private List<String> deliverList;
    private List<String> positnList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDeliverCode() {
        return deliverCode;
    }

    public void setDeliverCode(String deliverCode) {
        this.deliverCode = deliverCode;
    }

    public String getDeliverDes() {
        return deliverDes;
    }

    public void setDeliverDes(String deliverDes) {
        this.deliverDes = deliverDes;
    }

    public String getPositn() {
        return positn;
    }

    public void setPositn(String positn) {
        this.positn = positn;
    }

    public String getPositn_name() {
        return positn_name;
    }

    public void setPositn_name(String positn_name) {
        this.positn_name = positn_name;
    }

    public String getBeginTotalamt() {
        return beginTotalamt;
    }

    public void setBeginTotalamt(String beginTotalamt) {
        this.beginTotalamt = beginTotalamt;
    }

    public String getBeginPay() {
        return beginPay;
    }

    public void setBeginPay(String beginPay) {
        this.beginPay = beginPay;
    }

    public String getBeginAmt() {
        return beginAmt;
    }

    public void setBeginAmt(String beginAmt) {
        this.beginAmt = beginAmt;
    }

    public String getBeginAmtpay() {
        return beginAmtpay;
    }

    public void setBeginAmtpay(String beginAmtpay) {
        this.beginAmtpay = beginAmtpay;
    }

    public String getThisTotalamt() {
        return thisTotalamt;
    }

    public void setThisTotalamt(String thisTotalamt) {
        this.thisTotalamt = thisTotalamt;
    }

    public String getThisPay() {
        return thisPay;
    }

    public void setThisPay(String thisPay) {
        this.thisPay = thisPay;
    }

    public String getThisTotalamtUnaud() {
        return thisTotalamtUnaud;
    }

    public void setThisTotalamtUnaud(String thisTotalamtUnaud) {
        this.thisTotalamtUnaud = thisTotalamtUnaud;
    }

    public String getThisPayUnaud() {
        return thisPayUnaud;
    }

    public void setThisPayUnaud(String thisPayUnaud) {
        this.thisPayUnaud = thisPayUnaud;
    }

    public String getThisInvpay() {
        return thisInvpay;
    }

    public void setThisInvpay(String thisInvpay) {
        this.thisInvpay = thisInvpay;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getTotalInvAmt() {
        return totalInvAmt;
    }

    public void setTotalInvAmt(String totalInvAmt) {
        this.totalInvAmt = totalInvAmt;
    }

    public String getT_beginAmt() {
        return t_beginAmt;
    }

    public void setT_beginAmt(String t_beginAmt) {
        this.t_beginAmt = t_beginAmt;
    }

    public String getT_beginAmtpay() {
        return t_beginAmtpay;
    }

    public void setT_beginAmtpay(String t_beginAmtpay) {
        this.t_beginAmtpay = t_beginAmtpay;
    }

    public String getT_thisTotalamt() {
        return t_thisTotalamt;
    }

    public void setT_thisTotalamt(String t_thisTotalamt) {
        this.t_thisTotalamt = t_thisTotalamt;
    }

    public String getT_thisPay() {
        return t_thisPay;
    }

    public void setT_thisPay(String t_thisPay) {
        this.t_thisPay = t_thisPay;
    }

    public String getT_thisInvpay() {
        return t_thisInvpay;
    }

    public void setT_thisInvpay(String t_thisInvpay) {
        this.t_thisInvpay = t_thisInvpay;
    }

    public String getT_totalAmt() {
        return t_totalAmt;
    }

    public void setT_totalAmt(String t_totalAmt) {
        this.t_totalAmt = t_totalAmt;
    }

    public String getT_totalInvAmt() {
        return t_totalInvAmt;
    }

    public void setT_totalInvAmt(String t_totalInvAmt) {
        this.t_totalInvAmt = t_totalInvAmt;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public List<String> getDeliverList() {
        return deliverList;
    }

    public void setDeliverList(List<String> deliverList) {
        this.deliverList = deliverList;
    }

    public List<String> getPositnList() {
        return positnList;
    }

    public void setPositnList(List<String> positnList) {
        this.positnList = positnList;
    }

    public String getIsDat() {
        return isDat;
    }

    public void setIsDat(String isDat) {
        this.isDat = isDat;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    public String getBdat() {
        return bdat;
    }

    public void setBdat(String bdat) {
        this.bdat = bdat;
    }

    public String getEdat() {
        return edat;
    }

    public void setEdat(String edat) {
        this.edat = edat;
    }

    public String getT_beginTotalamt() {
        return t_beginTotalamt;
    }

    public void setT_beginTotalamt(String t_beginTotalamt) {
        this.t_beginTotalamt = t_beginTotalamt;
    }

    public String getT_beginPay() {
        return t_beginPay;
    }

    public void setT_beginPay(String t_beginPay) {
        this.t_beginPay = t_beginPay;
    }

    public String getT_thisTotalamtUnaud() {
        return t_thisTotalamtUnaud;
    }

    public void setT_thisTotalamtUnaud(String t_thisTotalamtUnaud) {
        this.t_thisTotalamtUnaud = t_thisTotalamtUnaud;
    }

    public String getT_thisPayUnaud() {
        return t_thisPayUnaud;
    }

    public void setT_thisPayUnaud(String t_thisPayUnaud) {
        this.t_thisPayUnaud = t_thisPayUnaud;
    }
}
