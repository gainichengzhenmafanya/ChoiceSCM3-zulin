package com.choice.scm.domain;

import java.util.Date;

/**
 * 报货单单据模板子表
 * @author 孙胜彬
 */
public class ChkstoDemod {
	private String acct;//帐套号
	private String yearr;//年度
	private Integer id;//主键
	private Integer chkstodemono;//主表主键
	private String sp_code;//物资编码
	private Supply supply;//物资
	private Double price;//单价
	private Double amount;//数量
	private String memo;//备注
	private Date ind;//到货日期
	private Double pricesale;
	private String chkstodemonos;
	
	//wangjie 2014年10月23日 10:35:22
	private double reference_amount;//参考单位数量
	
	private Double totleMoney;//合计金额----用于模板子表的查询
	public double getReference_amount() {
		return reference_amount;
	}
	public void setReference_amount(double reference_amount) {
		this.reference_amount = reference_amount;
	}
	public Double getTotleMoney() {
		return totleMoney;
	}
	public void setTotleMoney(Double totleMoney) {
		this.totleMoney = totleMoney;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkstodemono() {
		return chkstodemono;
	}
	public void setChkstodemono(Integer chkstodemono) {
		this.chkstodemono = chkstodemono;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public Double getPricesale() {
		return pricesale;
	}
	public void setPricesale(Double pricesale) {
		this.pricesale = pricesale;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getChkstodemonos() {
		return chkstodemonos;
	}
	public void setChkstodemonos(String chkstodemonos) {
		this.chkstodemonos = chkstodemonos;
	}
	
}
