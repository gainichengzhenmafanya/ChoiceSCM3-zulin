package com.choice.scm.domain;

/**
 * 帐套信息
 * @author lq
 *
 */
public class Acct {

	private String code;//编码
	private String des;//帐套名
	private String usernam;//用户
	private String pass;//
	private String serial;//id
	private String warn;//是否使用
	private String ynkc;//是否使用库存系统
	private double amount;
	private double viewamount;
	private String plantyp;
	private String yncgqr;//是否需要采购确认
	private String yncgsh;//是否需要采购审核
	//关联采购系统
	/**
	 * 登录商城用户名
	 */
	private String jmuusername;
	/**
	 * 登录商城密码
	 */
	private String jmupassword;
	
	
	public String getJmuusername() {
		return jmuusername;
	}
	public void setJmuusername(String jmuusername) {
		this.jmuusername = jmuusername;
	}
	public String getJmupassword() {
		return jmupassword;
	}
	public void setJmupassword(String jmupassword) {
		this.jmupassword = jmupassword;
	}
	public String getYnkc() {
		return ynkc;
	}
	public void setYnkc(String ynkc) {
		this.ynkc = ynkc;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getUsernam() {
		return usernam;
	}
	public void setUsernam(String usernam) {
		this.usernam = usernam;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getWarn() {
		return warn;
	}
	public void setWarn(String warn) {
		this.warn = warn;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getViewamount() {
		return viewamount;
	}
	public void setViewamount(double viewamount) {
		this.viewamount = viewamount;
	}
	public String getPlantyp() {
		return plantyp;
	}
	public void setPlantyp(String plantyp) {
		this.plantyp = plantyp;
	}
	public String getYncgqr() {
		return yncgqr;
	}
	public void setYncgqr(String yncgqr) {
		this.yncgqr = yncgqr;
	}
	public String getYncgsh() {
		return yncgsh;
	}
	public void setYncgsh(String yncgsh) {
		this.yncgsh = yncgsh;
	}
	
}
