package com.choice.scm.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 出库单主单
 * @author yp
 */

@Component()
@Scope(value="prototype")
public class Chkoutm {

	private String	acct;
	private String  yearr;
	private Integer  chkoutno;
	private String  chkoutnos;
	private Date  maded;
	private String  madet;
	private Date  checd;
	private Date  chect;
	private double  totalamt;
	private Positn  firm;//领用仓位
	private Positn  positn;//出库仓位
	private String  typ;
	private String  madeby;
	private String  checby;
	private String  accby;
	private String  status;
	private String  vouno;
	private String  memo;
	private String  chk1sta;
	private String  chk1;
	private Date  chk1dat;
	private String  chk1memo;
	private String  chk1nam;
	private double  pay;
	private BigDecimal  folio;
	private BigDecimal  bill;
	private List<Chkoutd> chkoutd;
	private int pr;
	private String madebyName;//制单人名字  2014.12.11wjf
	private String checbyName;//审核人名字 2014.12.11wjf
	private List<String> listPositn;
	public List<String> getListPositn() {
		return listPositn;
	}
	public void setListPositn(List<String> listPositn) {
		this.listPositn = listPositn;
	}
	public String getMadebyName() {
		return madebyName;
	}
	public void setMadebyName(String madebyName) {
		this.madebyName = madebyName;
	}
	public String getChecbyName() {
		return checbyName;
	}
	public void setChecbyName(String checbyName) {
		this.checbyName = checbyName;
	}

	public String getChkoutnos() {
		return chkoutnos;
	}

	public void setChkoutnos(String chkoutnos) {
		this.chkoutnos = chkoutnos;
	}

	public int getPr() {
		return pr;
	}

	public void setPr(int pr) {
		this.pr = pr;
	}

	public Chkoutm(){
		this.chk1 = "N";
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public Integer getChkoutno() {
		return chkoutno;
	}

	public void setChkoutno(Integer chkoutno) {
		this.chkoutno = chkoutno;
	}

	public Date getMaded() {
		return maded;
	}

	public void setMaded(Date maded) {
		this.maded = maded;
	}

	public String getMadet() {
		return madet;
	}

	public void setMadet(String madet) {
		this.madet = madet;
	}

	public Date getChecd() {
		return checd;
	}

	public void setChecd(Date checd) {
		this.checd = checd;
	}

	public Date getChect() {
		return chect;
	}

	public void setChect(Date chect) {
		this.chect = chect;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public Positn getFirm() {
		return firm;
	}

	public void setFirm(Positn firm) {
		this.firm = firm;
	}

	public Positn getPositn() {
		return positn;
	}

	public void setPositn(Positn positn) {
		this.positn = positn;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getMadeby() {
		return madeby;
	}

	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}

	public String getChecby() {
		return checby;
	}

	public void setChecby(String checby) {
		this.checby = checby;
	}

	public String getAccby() {
		return accby;
	}

	public void setAccby(String accby) {
		this.accby = accby;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVouno() {
		return vouno;
	}

	public void setVouno(String vouno) {
		this.vouno = vouno;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getChk1sta() {
		return chk1sta;
	}

	public void setChk1sta(String chk1sta) {
		this.chk1sta = chk1sta;
	}

	public String getChk1() {
		return chk1;
	}

	public void setChk1(String chk1) {
		this.chk1 = chk1;
	}

	public Date getChk1dat() {
		return chk1dat;
	}

	public void setChk1dat(Date chk1dat) {
		this.chk1dat = chk1dat;
	}

	public String getChk1memo() {
		return chk1memo;
	}

	public void setChk1memo(String chk1memo) {
		this.chk1memo = chk1memo;
	}

	public String getChk1nam() {
		return chk1nam;
	}

	public void setChk1nam(String chk1nam) {
		this.chk1nam = chk1nam;
	}

	public double getPay() {
		return pay;
	}

	public void setPay(double pay) {
		this.pay = pay;
	}

	public BigDecimal getFolio() {
		return folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	public BigDecimal getBill() {
		return bill;
	}

	public void setBill(BigDecimal bill) {
		this.bill = bill;
	}

	public List<Chkoutd> getChkoutd() {
		return chkoutd;
	}

	public void setChkoutd(List<Chkoutd> chkoutd) {
		this.chkoutd = chkoutd;
	}
}
