package com.choice.scm.domain;


/**
 * 报货单--从表1
 * @author css
 *
 */
public class OfferCargoWhEntry {

	private String id; 				//id
	private String parentID; 		//主表id
	private String materialID; 		//物料id
	private String shippingWHID; 	//发货仓位
	private String customerID; 		//客户
	private double amount; 			//发货数量
	private double accSendAmount;	//累计发货数量
	private double unSendAmount;		//未发送数量
	private String offerCargoEntryID;//报货分录
	private Integer isTeilen;		//是否拆分
	private Integer Seq;			//单据分录序列号

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParentID() {
		return parentID;
	}
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
	public String getMaterialID() {
		return materialID;
	}
	public void setMaterialID(String materialID) {
		this.materialID = materialID;
	}
	public String getShippingWHID() {
		return shippingWHID;
	}
	public void setShippingWHID(String shippingWHID) {
		this.shippingWHID = shippingWHID;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getAccSendAmount() {
		return accSendAmount;
	}
	public void setAccSendAmount(double accSendAmount) {
		this.accSendAmount = accSendAmount;
	}
	public double getUnSendAmount() {
		return unSendAmount;
	}
	public void setUnSendAmount(double unSendAmount) {
		this.unSendAmount = unSendAmount;
	}
	public String getOfferCargoEntryID() {
		return offerCargoEntryID;
	}
	public void setOfferCargoEntryID(String offerCargoEntryID) {
		this.offerCargoEntryID = offerCargoEntryID;
	}
	public Integer getIsTeilen() {
		return isTeilen;
	}
	public void setIsTeilen(Integer isTeilen) {
		this.isTeilen = isTeilen;
	}
	public Integer getSeq() {
		return Seq;
	}
	public void setSeq(Integer seq) {
		Seq = seq;
	}
}
