package com.choice.scm.domain;

import java.util.List;

/**
 * 售价单接收数据用
 * @author csb
 *
 */
public class SppriceSaleList {

	private List<SppriceSale> sppriceSaleList;

	public List<SppriceSale> getSppriceSaleList() {
		return sppriceSaleList;
	}

	public void setSppriceSaleList(List<SppriceSale> sppriceSaleList) {
		this.sppriceSaleList = sppriceSaleList;
	}
	
}
