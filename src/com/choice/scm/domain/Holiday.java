package com.choice.scm.domain;

import java.util.Date;

public class Holiday {

	private String year;		//年
	private String typ;		//节日类型
	private Date dat;			//日期
	private String sdat;			//日期
	private String memo;
	private String grade;		//假期等级
	private String orderBy;
	private String orderDes;
	private String load;
	private String dholidaydate;
	private String vholidayname;
	private String vyear;
	
	
	public Holiday() {
		super();
	}

	public Holiday(String sdat) {
		super();
		this.sdat = sdat;
	}

	public String getSdat() {
		return sdat;
	}
	public void setSdat(String sdat) {
		this.sdat = sdat;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getOrderDes() {
		return orderDes;
	}
	public void setOrderDes(String orderDes) {
		this.orderDes = orderDes;
	}
	public String getLoad() {
		return load;
	}
	public void setLoad(String load) {
		this.load = load;
	}

	public String getDholidaydate() {
		return dholidaydate;
	}

	public void setDholidaydate(String dholidaydate) {
		this.dholidaydate = dholidaydate;
	}

	public String getVholidayname() {
		return vholidayname;
	}

	public void setVholidayname(String vholidayname) {
		this.vholidayname = vholidayname;
	}

	public String getVyear() {
		return vyear;
	}

	public void setVyear(String vyear) {
		this.vyear = vyear;
	}
	
	
}
