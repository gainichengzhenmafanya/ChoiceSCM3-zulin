package com.choice.scm.domain;

import java.util.Date;

/***
 * 盘点主表
 * @author wjf
 *
 */
public class PositnPanm {
	
	private String panno;
	private String acct;	
	private String year;	
	private String monthh;
	private String positn;
	private Date dat;
	private String tim;
	private String accby;
	private String memo;
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonthh() {
		return monthh;
	}
	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getTim() {
		return tim;
	}
	public void setTim(String tim) {
		this.tim = tim;
	}
	public String getAccby() {
		return accby;
	}
	public void setAccby(String accby) {
		this.accby = accby;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
