package com.choice.scm.domain;

import java.util.Date;

public class FirmIntransitd {
	
	//在途细表
	private String firm;
	//private Integer intransitmid;
	private Integer intransitdid;
	private String sp_code;
	private Double itcnt;
	private Double checkcnt;
	private Double amount;
	//private Integer state;
	private String chkindid;
	private Date chkinddate;
	private String chkindcode;
	//在途主表
	private Integer intransitmid;
	private String vouno;
	private String storeid;
	private Date sgdate;
	private String scode;
	private String ecode;
	private Date inputdate;
	private Date checkdate;
	private String checkcode;
	private String memo;
	private Integer state;
	private Date workdate;
	
	//物资
	private String spname;
	private String unit;
	private String grptypdes;
	private String grptyp;
	private String grpdes;
	private String grp;
	private String typdes;
	private String sp_type;
	
	private String bdate;
	private String edate;
	private String firmDes;
	
	public FirmIntransitd(){
		super();
	}
	
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Integer getIntransitdid() {
		return intransitdid;
	}
	public void setIntransitdid(Integer intransitdid) {
		this.intransitdid = intransitdid;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getItcnt() {
		return itcnt;
	}
	public void setItcnt(Double itcnt) {
		this.itcnt = itcnt;
	}
	public Double getCheckcnt() {
		return checkcnt;
	}
	public void setCheckcnt(Double checkcnt) {
		this.checkcnt = checkcnt;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getChkindid() {
		return chkindid;
	}
	public void setChkindid(String chkindid) {
		this.chkindid = chkindid;
	}
	public Date getChkinddate() {
		return chkinddate;
	}
	public void setChkinddate(Date chkinddate) {
		this.chkinddate = chkinddate;
	}
	public String getChkindcode() {
		return chkindcode;
	}
	public void setChkindcode(String chkindcode) {
		this.chkindcode = chkindcode;
	}
	public Integer getIntransitmid() {
		return intransitmid;
	}
	public void setIntransitmid(Integer intransitmid) {
		this.intransitmid = intransitmid;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getStoreid() {
		return storeid;
	}
	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}
	public Date getSgdate() {
		return sgdate;
	}
	public void setSgdate(Date sgdate) {
		this.sgdate = sgdate;
	}
	public String getScode() {
		return scode;
	}
	public void setScode(String scode) {
		this.scode = scode;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public Date getInputdate() {
		return inputdate;
	}
	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}
	public Date getCheckdate() {
		return checkdate;
	}
	public void setCheckdate(Date checkdate) {
		this.checkdate = checkdate;
	}
	public String getCheckcode() {
		return checkcode;
	}
	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getWorkdate() {
		return workdate;
	}
	public void setWorkdate(Date workdate) {
		this.workdate = workdate;
	}
	public String getSpname() {
		return spname;
	}
	public void setSpname(String spname) {
		this.spname = spname;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getGrptypdes() {
		return grptypdes;
	}
	public void setGrptypdes(String grptypdes) {
		this.grptypdes = grptypdes;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrpdes() {
		return grpdes;
	}
	public void setGrpdes(String grpdes) {
		this.grpdes = grpdes;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getTypdes() {
		return typdes;
	}
	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}
	public String getSp_type() {
		return sp_type;
	}
	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getFirmDes() {
		return firmDes;
	}
	public void setFirmDes(String firmDes) {
		this.firmDes = firmDes;
	}
	
}
