package com.choice.scm.domain;
/**
 * 分店供应商
 * @author lq
 *
 */
public class FirmDeliver {

	private String acct;//帐套
	private String firm;//分店
	private String deliver;//供应商
	private int inout;//0是直发 1是直配
	private String firmdes;//分店名称
	
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public int getInout() {
		return inout;
	}
	public void setInout(int inout) {
		this.inout = inout;
	}
	
	
}
