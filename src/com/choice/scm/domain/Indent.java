package com.choice.scm.domain;

public class Indent {

	private String positn1;
	private String positn2;
	private String positn3;
	private double amount1;
	private double amount2;
	private double amount3;
	private String memo1;
	private String memo2;
	private String memo3;
	
	public String getPositn1() {
		return positn1;
	}
	public void setPositn1(String positn1) {
		this.positn1 = positn1;
	}
	public String getPositn2() {
		return positn2;
	}
	public void setPositn2(String positn2) {
		this.positn2 = positn2;
	}
	public String getPositn3() {
		return positn3;
	}
	public void setPositn3(String positn3) {
		this.positn3 = positn3;
	}
	public double getAmount1() {
		return amount1;
	}
	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}
	public double getAmount2() {
		return amount2;
	}
	public void setAmount2(double amount2) {
		this.amount2 = amount2;
	}
	public double getAmount3() {
		return amount3;
	}
	public void setAmount3(double amount3) {
		this.amount3 = amount3;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public String getMemo2() {
		return memo2;
	}
	public void setMemo2(String memo2) {
		this.memo2 = memo2;
	}
	public String getMemo3() {
		return memo3;
	}
	public void setMemo3(String memo3) {
		this.memo3 = memo3;
	}
	
}
