package com.choice.scm.domain;

import java.util.Date;

/***
 * 盘点子表
 * @author wjf
 *
 */
public class PositnPand {

	private String panno;
	private String sp_code;
	private Double cntbla;
	private Double cntubla;
	private Double amtblan;
	private Double cntact;
	private Double cntuact;
	private Double cntdif;
	private Double cntudif;
	
	private String acct;	
	private String year;	
	private String monthh;
	private String positn;
	private Date dat;
	private String tim;
	private String accby;
	private String memo;
	private Date bdat;
	private Date edat;
	private String grptyp;
	private String grp;
	private String typ;
	private String accountId;
	private String cwqx;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCwqx() {
		return cwqx;
	}
	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getGrp() {
		return grp;
	}
	public void setGrp(String grp) {
		this.grp = grp;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getCntbla() {
		return cntbla;
	}
	public void setCntbla(Double cntbla) {
		this.cntbla = cntbla;
	}
	public Double getCntubla() {
		return cntubla;
	}
	public void setCntubla(Double cntubla) {
		this.cntubla = cntubla;
	}
	public Double getAmtblan() {
		return amtblan;
	}
	public void setAmtblan(Double amtblan) {
		this.amtblan = amtblan;
	}
	public Double getCntact() {
		return cntact;
	}
	public void setCntact(Double cntact) {
		this.cntact = cntact;
	}
	public Double getCntuact() {
		return cntuact;
	}
	public void setCntuact(Double cntuact) {
		this.cntuact = cntuact;
	}
	public Double getCntdif() {
		return cntdif;
	}
	public void setCntdif(Double cntdif) {
		this.cntdif = cntdif;
	}
	public Double getCntudif() {
		return cntudif;
	}
	public void setCntudif(Double cntudif) {
		this.cntudif = cntudif;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonthh() {
		return monthh;
	}
	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getTim() {
		return tim;
	}
	public void setTim(String tim) {
		this.tim = tim;
	}
	public String getAccby() {
		return accby;
	}
	public void setAccby(String accby) {
		this.accby = accby;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
