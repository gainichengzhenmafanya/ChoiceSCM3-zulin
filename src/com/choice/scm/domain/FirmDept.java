package com.choice.scm.domain;

/**
 * 分店部门
 * @author csb
 *
 */
public class FirmDept {

	private String firm;//分店
	private String code;//编码
	private String des;//名称
	private String typ;//类别
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
}
