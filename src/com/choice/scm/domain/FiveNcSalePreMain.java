package com.choice.scm.domain;

import java.util.List;

/**
 * 销售订单 主表
 * @author bella
 *
 */
public class FiveNcSalePreMain {

	private String Keykey;//主实体key值
	private String vreceiptcode;//单据号
	private String pk_corp;//公司编码
	private String ccustomerid;//客商编码
	private String cdeptid;//部门编码
	private String dabatedate;//失效日期
	private String dbilldate;//订货日期
	private String dmakedate;//制单日期
	
	private List<FiveNcSalePreSub> listFiveNcSalePreSub;//销售订单从表
	
	public String getKeykey() {
		return Keykey;
	}
	public void setKeykey(String keykey) {
		Keykey = keykey;
	}
	public String getVreceiptcode() {
		return vreceiptcode;
	}
	public void setVreceiptcode(String vreceiptcode) {
		this.vreceiptcode = vreceiptcode;
	}
	public String getPk_corp() {
		return pk_corp;
	}
	public void setPk_corp(String pk_corp) {
		this.pk_corp = pk_corp;
	}
	public String getCcustomerid() {
		return ccustomerid;
	}
	public void setCcustomerid(String ccustomerid) {
		this.ccustomerid = ccustomerid;
	}
	public String getCdeptid() {
		return cdeptid;
	}
	public void setCdeptid(String cdeptid) {
		this.cdeptid = cdeptid;
	}
	public String getDabatedate() {
		return dabatedate;
	}
	public void setDabatedate(String dabatedate) {
		this.dabatedate = dabatedate;
	}
	public String getDmakedate() {
		return dmakedate;
	}
	public void setDmakedate(String dmakedate) {
		this.dmakedate = dmakedate;
	}
	public List<FiveNcSalePreSub> getListFiveNcSalePreSub() {
		return listFiveNcSalePreSub;
	}
	public void setListFiveNcSalePreSub(List<FiveNcSalePreSub> listFiveNcSalePreSub) {
		this.listFiveNcSalePreSub = listFiveNcSalePreSub;
	}
	public String getDbilldate() {
		return dbilldate;
	}
	public void setDbilldate(String dbilldate) {
		this.dbilldate = dbilldate;
	}
	
	
}
