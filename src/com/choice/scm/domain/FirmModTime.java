package com.choice.scm.domain;

public class FirmModTime {
	
	//成本卡方案
	private String firm;
	private String id;
	private String modCode;
	private Integer bmonth;
	private Integer emonth;
	
	public String getModCode() {
		return modCode;
	}
	public void setModCode(String modCode) {
		this.modCode = modCode;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getBmonth() {
		return bmonth;
	}
	public void setBmonth(Integer bmonth) {
		this.bmonth = bmonth;
	}
	public Integer getEmonth() {
		return emonth;
	}
	public void setEmonth(Integer emonth) {
		this.emonth = emonth;
	}	
}
