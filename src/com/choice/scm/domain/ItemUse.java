package com.choice.scm.domain;

import java.util.List;

/**
 * 菜品点击率
 * @author css
 *
 */
public class ItemUse {   

	private List<ItemUse> ItemUseList; //  修改用
	private List<String> firmList;   //分店号list
	private String firm;//分店号
	private String item;//菜品流水号
	private double cnt1;//工作日一班调整
	private double cnt2;//工作日二班调整
	private double cnt3;//工作日三班调整
	private double cnt4;//工作日四班调整
	private double cnt1old;//工作日一班计算
	private double cnt2old;//工作日二班计算
	private double cnt3old;//工作日三班计算
	private double cnt4old;//工作日四班计算
	private String itcode;//菜品编码
	private String itdes;//菜品名称
	private String itunit;//菜品单位
	private double hcnt1;//节假日一班调整
	private double hcnt2;//节假日二班调整
	private double hcnt3;//节假日三班调整
	private double hcnt4;//节假日四班调整
	private double hcnt1old;//节假日一班计算
	private double hcnt2old;//节假日二班计算
	private double hcnt3old;//节假日三班计算
	private double hcnt4old;//节假日四班计算
	
	public List<String> getFirmList() {
		return firmList;
	}
	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}
	public List<ItemUse> getItemUseList() {
		return ItemUseList;
	}
	public void setItemUseList(List<ItemUse> itemUseList) {
		ItemUseList = itemUseList;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public double getCnt2() {
		return cnt2;
	}
	public void setCnt2(double cnt2) {
		this.cnt2 = cnt2;
	}
	public double getCnt3() {
		return cnt3;
	}
	public void setCnt3(double cnt3) {
		this.cnt3 = cnt3;
	}
	public double getCnt4() {
		return cnt4;
	}
	public void setCnt4(double cnt4) {
		this.cnt4 = cnt4;
	}
	public double getCnt1old() {
		return cnt1old;
	}
	public void setCnt1old(double cnt1old) {
		this.cnt1old = cnt1old;
	}
	public double getCnt2old() {
		return cnt2old;
	}
	public void setCnt2old(double cnt2old) {
		this.cnt2old = cnt2old;
	}
	public double getCnt3old() {
		return cnt3old;
	}
	public void setCnt3old(double cnt3old) {
		this.cnt3old = cnt3old;
	}
	public double getCnt4old() {
		return cnt4old;
	}
	public void setCnt4old(double cnt4old) {
		this.cnt4old = cnt4old;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getItdes() {
		return itdes;
	}
	public void setItdes(String itdes) {
		this.itdes = itdes;
	}
	public String getItunit() {
		return itunit;
	}
	public void setItunit(String itunit) {
		this.itunit = itunit;
	}
	public double getHcnt1() {
		return hcnt1;
	}
	public void setHcnt1(double hcnt1) {
		this.hcnt1 = hcnt1;
	}
	public double getHcnt2() {
		return hcnt2;
	}
	public void setHcnt2(double hcnt2) {
		this.hcnt2 = hcnt2;
	}
	public double getHcnt3() {
		return hcnt3;
	}
	public void setHcnt3(double hcnt3) {
		this.hcnt3 = hcnt3;
	}
	public double getHcnt4() {
		return hcnt4;
	}
	public void setHcnt4(double hcnt4) {
		this.hcnt4 = hcnt4;
	}
	public double getHcnt1old() {
		return hcnt1old;
	}
	public void setHcnt1old(double hcnt1old) {
		this.hcnt1old = hcnt1old;
	}
	public double getHcnt2old() {
		return hcnt2old;
	}
	public void setHcnt2old(double hcnt2old) {
		this.hcnt2old = hcnt2old;
	}
	public double getHcnt3old() {
		return hcnt3old;
	}
	public void setHcnt3old(double hcnt3old) {
		this.hcnt3old = hcnt3old;
	}
	public double getHcnt4old() {
		return hcnt4old;
	}
	public void setHcnt4old(double hcnt4old) {
		this.hcnt4old = hcnt4old;
	}
	
}
