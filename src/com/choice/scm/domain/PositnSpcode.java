package com.choice.scm.domain;

import java.util.List;


public class PositnSpcode {
	private String acct;     //帐套
	private String positn;	//仓位编码
	private String sp_code;	//物资编码
	private String sp_name;	//物资名称
	private String sp_init; //缩写
	private String sp_desc;	//物资规格
	private String unit;	//单位
	private double sp_max1;	//物资最大库存
	private double sp_min1;	//物资最低库存
	private double sp_price; //物资价格
	private double yhrate1;	//物资验货比率最低
	private double yhrate2;	//物资验货比率最高
	private String ynpd;	//是否可盘点
	private String ynsto;	//是否可申购
	private Double datsto;  //采购周期
	private String cntuse="0";  //日均用量
	private String sp_code_x;//虚拟物料编码 wjf
	private Integer sp_code_count;//放数量用的 wjf
	private String sp_codes;//连接的实际物资编码，名称wjf
	private List<PositnSpcode> positnSpcodeList;     
	private String positnCode;//要复制的门店的编码 wjf
	private Supply supply;//加物资属性 门店boh用wjf
	
	private double stomin;
	private double stocnt;
	private String disunit;//配送单位
	private double disunitper;//配送单位转换率
	private double dismincnt;//配送单位最小申购量
	private String ts;//最后修改时间
	
	private List<SupplyUnit> suList;//物资单位
	
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public String getPositnCode() {
		return positnCode;
	}
	public void setPositnCode(String positnCode) {
		this.positnCode = positnCode;
	}
	public double getYhrate1() {
		return yhrate1;
	}
	public void setYhrate1(double yhrate1) {
		this.yhrate1 = yhrate1;
	}
	public double getYhrate2() {
		return yhrate2;
	}
	public void setYhrate2(double yhrate2) {
		this.yhrate2 = yhrate2;
	}
	public String getYnpd() {
		return ynpd;
	}
	public void setYnpd(String ynpd) {
		this.ynpd = ynpd;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public List<PositnSpcode> getPositnSpcodeList() {
		return positnSpcodeList;
	}
	public void setPositnSpcodeList(List<PositnSpcode> positnSpcodeList) {
		this.positnSpcodeList = positnSpcodeList;
	}
	public String getCntuse() {
		return cntuse;
	}
	public void setCntuse(String cntuse) {
		this.cntuse = cntuse;
	}
	public Double getDatsto() {
		return datsto;
	}
	public void setDatsto(Double datsto) {
		this.datsto = datsto;
	}
	public String getSp_init() {
		return sp_init;
	}
	public void setSp_init(String sp_init) {
		this.sp_init = sp_init;
	}
	public String getYnsto() {
		return ynsto;
	}
	public void setYnsto(String ynsto) {
		this.ynsto = ynsto;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public double getSp_max1() {
		return sp_max1;
	}
	public void setSp_max1(double sp_max1) {
		this.sp_max1 = sp_max1;
	}
	public double getSp_min1() {
		return sp_min1;
	}
	public void setSp_min1(double sp_min1) {
		this.sp_min1 = sp_min1;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public String getSp_code_x() {
		return sp_code_x;
	}
	public void setSp_code_x(String sp_code_x) {
		this.sp_code_x = sp_code_x;
	}
	public Integer getSp_code_count() {
		return sp_code_count;
	}
	public void setSp_code_count(Integer sp_code_count) {
		this.sp_code_count = sp_code_count;
	}
	public String getSp_codes() {
		return sp_codes;
	}
	public void setSp_codes(String sp_codes) {
		this.sp_codes = sp_codes;
	}
	public double getStomin() {
		return stomin;
	}
	public void setStomin(double stomin) {
		this.stomin = stomin;
	}
	public double getStocnt() {
		return stocnt;
	}
	public void setStocnt(double stocnt) {
		this.stocnt = stocnt;
	}
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public double getDismincnt() {
		return dismincnt;
	}
	public void setDismincnt(double dismincnt) {
		this.dismincnt = dismincnt;
	}
	public List<SupplyUnit> getSuList() {
		return suList;
	}
	public void setSuList(List<SupplyUnit> suList) {
		this.suList = suList;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
}
