package com.choice.scm.domain;

import java.util.List;

public class PositnSupply {

	private String acct;//帐套
	private String yearr;//年份
	private String positn;//仓位编码
	private String sp_code;//仓位编码
	private String sp_name;//仓位名称
	private String sp_desc;//仓位规格
	private String sp_type;//物资类型
	private double unitper;//单价
	private String unit;//标准单位
	private String unit1;//参考单位
	private double inc0;//标准数量
	private double ina0;//标准金额
	private double incu0;//参考数量
	private double sp_price;//单价
	private String  qcrec;  //排序
	
	private List<PositnSupply> positnSupplyList;//list 期初
	
	public List<PositnSupply> getPositnSupplyList() {
		return positnSupplyList;
	}

	public void setPositnSupplyList(List<PositnSupply> positnSupplyList) {
		this.positnSupplyList = positnSupplyList;
	}

	public double getSp_price() {
		return sp_price;
	}

	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}

	public PositnSupply() {
		super();
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public String getPositn() {
		return positn;
	}

	public void setPositn(String positn) {
		this.positn = positn;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public String getSp_name() {
		return sp_name;
	}

	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}

	public String getSp_desc() {
		return sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public String getSp_type() {
		return sp_type;
	}

	public void setSp_type(String sp_type) {
		this.sp_type = sp_type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit1() {
		return unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	public double getUnitper() {
		return unitper;
	}

	public void setUnitper(double unitper) {
		this.unitper = unitper;
	}

	public double getInc0() {
		return inc0;
	}

	public void setInc0(double inc0) {
		this.inc0 = inc0;
	}

	public double getIna0() {
		return ina0;
	}

	public void setIna0(double ina0) {
		this.ina0 = ina0;
	}

	public double getIncu0() {
		return incu0;
	}

	public void setIncu0(double incu0) {
		this.incu0 = incu0;
	}

	public String getQcrec() {
		return qcrec;
	}

	public void setQcrec(String qcrec) {
		this.qcrec = qcrec;
	}
	
}
