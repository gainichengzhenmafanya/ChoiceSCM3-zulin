package com.choice.scm.domain;

import java.util.Date;

/**
 * 盘点模板子表
 * @author 孙胜彬
 */
public class InventoryDemod {
	private String acct;//帐套号
	private String yearr;//年度
	private Integer id;//主键
	private Integer chkstodemono;//主表主键
	private String sp_code;//物资编码
	private Supply supply;//物资
	private Double price;//单价
	private Double amount;//数量
	private String memo;//备注
	private Date ind;//到货日期
	private Double pricesale;
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkstodemono() {
		return chkstodemono;
	}
	public void setChkstodemono(Integer chkstodemono) {
		this.chkstodemono = chkstodemono;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getInd() {
		return ind;
	}
	public void setInd(Date ind) {
		this.ind = ind;
	}
	public Double getPricesale() {
		return pricesale;
	}
	public void setPricesale(Double pricesale) {
		this.pricesale = pricesale;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	
}
