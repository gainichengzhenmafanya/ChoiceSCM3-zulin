package com.choice.scm.domain;

import java.util.Date;

public class Spbatch {

	private String acct;//帐套
	private int id;//id
	private Date ind;//日期
	private int chkno;//单号
	private String positn;//仓位编码
	private String positndes;//仓位名称
	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private double price;//物资价格
	private double amount;//数量
	private String status;//状态
	private int stono;//
	private double amount1;//数量1
	private String deliver;//盘盈默认供应商
	private String deliverDes;//供应商名称
	private double pricesale;//报价
	private Date bdat;//日期
	private Date edat;//日期
	private double amt;//金额
	private String unit;//单位
	private String unit1;//单位1
	private String unitper;//单位转换率
	private String sp_desc;//规格
	private String tax;//税率
	private String cnt;//库存量
	
	private Date dued;   //生产日期
	private Date losed;//失效日期
	private String  pcno; //批次
	private Double sp_per1;//有效期报表用2015.1.3wjf
	private String accountId;//加入账号，报表加权限用 2011.12.22wjf
	private String cwqx;//仓位权限，为1时才使用仓位权限
	private String gysqx;//供应商权限 为1时使用供应商权限
	private String grp;//中类
	private String grptyp;//大类
	private String typ;//小类
	private Date bdued;//报表查询用
	private Date edued;//报表用
	
	public String getDeliverDes() {
		return deliverDes;
	}

	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}

	public Date getBdued() {
		return bdued;
	}

	public void setBdued(Date bdued) {
		this.bdued = bdued;
	}

	public Date getEdued() {
		return edued;
	}

	public void setEdued(Date edued) {
		this.edued = edued;
	}

	public Date getLosed() {
		return losed;
	}

	public void setLosed(Date losed) {
		this.losed = losed;
	}

	public String getGrp() {
		return grp;
	}

	public void setGrp(String grp) {
		this.grp = grp;
	}

	public String getGrptyp() {
		return grptyp;
	}

	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCwqx() {
		return cwqx;
	}

	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}

	public String getGysqx() {
		return gysqx;
	}

	public void setGysqx(String gysqx) {
		this.gysqx = gysqx;
	}

	public Double getSp_per1() {
		return sp_per1;
	}

	public void setSp_per1(Double sp_per1) {
		this.sp_per1 = sp_per1;
	}

	public Date getDued() {
		return dued;
	}

	public void setDued(Date dued) {
		this.dued = dued;
	}

	public String getPcno() {
		return pcno;
	}

	public void setPcno(String pcno) {
		this.pcno = pcno;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public String getSp_desc() {
		return sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getPositndes() {
		return positndes;
	}

	public String getSp_name() {
		return sp_name;
	}

	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}

	public void setPositndes(String positndes) {
		this.positndes = positndes;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit1() {
		return unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	public String getUnitper() {
		return unitper;
	}

	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}

	public Date getBdat() {
		return bdat;
	}

	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}

	public Date getEdat() {
		return edat;
	}

	public void setEdat(Date edat) {
		this.edat = edat;
	}

	public Spbatch() {
		super();
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getInd() {
		return ind;
	}

	public void setInd(Date ind) {
		this.ind = ind;
	}

	public int getChkno() {
		return chkno;
	}

	public void setChkno(int chkno) {
		this.chkno = chkno;
	}

	public String getPositn() {
		return positn;
	}

	public void setPositn(String positn) {
		this.positn = positn;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStono() {
		return stono;
	}

	public void setStono(int stono) {
		this.stono = stono;
	}

	public double getAmount1() {
		return amount1;
	}

	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}

	public String getDeliver() {
		return deliver;
	}

	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}

	public double getPricesale() {
		return pricesale;
	}

	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}
}
