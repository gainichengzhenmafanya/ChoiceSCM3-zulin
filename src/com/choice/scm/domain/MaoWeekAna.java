package com.choice.scm.domain;
/**
 * 毛利周分析
 * @author lq
 *
 */
public class MaoWeekAna {

	private String firm;//分店
	private String weekno;//周次
	private String weekdes;//起始日期
	private double amt	;//销售金额
	private double cost;//实际成本
	private double maorate;//毛利率
	private double theorycost;//理论成本
	private double theorymaorate;//理论毛利率
	private double costdiff;//成本差异
	private double costdiffrate;//差异率
	private double bperiod;//期初
	private double in;//本期入
	private double balances;//本期结存
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getWeekno() {
		return weekno;
	}
	public void setWeekno(String weekno) {
		this.weekno = weekno;
	}
	public String getWeekdes() {
		return weekdes;
	}
	public void setWeekdes(String weekdes) {
		this.weekdes = weekdes;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getMaorate() {
		return maorate;
	}
	public void setMaorate(double maorate) {
		this.maorate = maorate;
	}
	public double getTheorycost() {
		return theorycost;
	}
	public void setTheorycost(double theorycost) {
		this.theorycost = theorycost;
	}
	public double getTheorymaorate() {
		return theorymaorate;
	}
	public void setTheorymaorate(double theorymaorate) {
		this.theorymaorate = theorymaorate;
	}
	public double getCostdiff() {
		return costdiff;
	}
	public void setCostdiff(double costdiff) {
		this.costdiff = costdiff;
	}
	public double getCostdiffrate() {
		return costdiffrate;
	}
	public void setCostdiffrate(double costdiffrate) {
		this.costdiffrate = costdiffrate;
	}
	public double getBperiod() {
		return bperiod;
	}
	public void setBperiod(double bperiod) {
		this.bperiod = bperiod;
	}
	public double getIn() {
		return in;
	}
	public void setIn(double in) {
		this.in = in;
	}
	public double getBalances() {
		return balances;
	}
	public void setBalances(double balances) {
		this.balances = balances;
	}
	
}
