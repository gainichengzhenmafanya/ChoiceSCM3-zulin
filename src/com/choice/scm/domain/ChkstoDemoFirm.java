package com.choice.scm.domain;

/**
 * 报货单单据模板适用分店
 * @author 孙胜彬
 */
public class ChkstoDemoFirm {
	private Integer chkstodemono;//报货单主键
	private String firm;//分店
	private String chksto;
	public Integer getChkstodemono() {
		return chkstodemono;
	}
	public void setChkstodemono(Integer chkstodemono) {
		this.chkstodemono = chkstodemono;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getChksto() {
		return chksto;
	}
	public void setChksto(String chksto) {
		this.chksto = chksto;
	}
	
}
