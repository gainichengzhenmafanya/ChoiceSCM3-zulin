package com.choice.scm.domain;


/**
 * scm档口和餐饮部门对应关系
 * xlh  2016.01.27
 */

public class PositnFirmScm {

	private String acct;	//帐套编码
	private String positn;	//仓位分店编号  
	private String firm;	//对应餐饮的分店编号
	private String dept;	//对应餐饮的部门编号
	private String firmdes;	//对应餐饮的分店名称
	private String deptdes;	//对应餐饮的分店下部门名称
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getFirmdes() {
		return firmdes;
	}
	public void setFirmdes(String firmdes) {
		this.firmdes = firmdes;
	}
	public String getDeptdes() {
		return deptdes;
	}
	public void setDeptdes(String deptdes) {
		this.deptdes = deptdes;
	}
}
