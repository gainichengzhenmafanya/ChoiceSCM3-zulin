package com.choice.scm.domain;

import java.util.List;

/**
 * 申购单模板
 * @author csb
 *
 */
public class Chkstodemo {

	private String acct;//帐套
	private Positn positn;//仓位
	private Supply supply;//物资编码
	private double cnt;//数量
	private double cnt1;//数量1
	private Integer rec;//顺序
	private String memo;//备注
	private String title;//标题
	private String typ;//物资类别
	private String unitper;//转换率
	private String firm;
	private List<Chkstodemo> chkstodemoList;
	
	public List<Chkstodemo> getChkstodemoList() {
		return chkstodemoList;
	}
	public void setChkstodemoList(List<Chkstodemo> chkstodemoList) {
		this.chkstodemoList = chkstodemoList;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public double getCnt1() {
		return cnt1;
	}
	public void setCnt1(double cnt1) {
		this.cnt1 = cnt1;
	}
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getUnitper() {
		return unitper;
	}
	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	
}
