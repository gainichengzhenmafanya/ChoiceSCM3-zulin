package com.choice.scm.domain;


/**
 * 配送班表 主
 *
 */
public class ScheduleStore {
	private Integer scheduleID;		//配送班id
	private String sCode;			//门店id	
	private String grptyp;			//报货类别
	
	public String getGrptyp() {
		return grptyp;
	}
	public void setGrptyp(String grptyp) {
		this.grptyp = grptyp;
	}
	public String getsCode() {
		return sCode;
	}
	public void setsCode(String sCode) {
		this.sCode = sCode;
	}
	public Integer getScheduleID() {
		return scheduleID;
	}
	public void setScheduleID(Integer scheduleID) {
		this.scheduleID = scheduleID;
	}
	public ScheduleStore() {
		super();
	}

	public ScheduleStore(Integer scheduleID, String sCode) {
		super();
		this.scheduleID = scheduleID;
		this.sCode = sCode;
	}
}
