package com.choice.scm.domain;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import com.choice.framework.util.DateFormat;

/**
 * 入库单主表
 * @author lehui
 *
 */
public class Chkinm {
	private String	acct	  ;//帐套
	private String	yearr	  ;//
	private Integer	    chkinno	  ;//入库单号
	private Date	maded	  ;//日期   修改时改这个
	private String	madet	  ;//日期
	private Date	checd	  ;//审核日期
	private Date	chect	  ;//审核时间
	private String	madeby     ;//操作人员
	private String	checby     ;//审核人员
	private double	totalamt  ;//总金额
	private double  noTaxTotalamt;//税前金额
	private String	vouno	  ;//凭证号
	private String	status	  ;//
	private Deliver	deliver	  ;//供应商
	private Positn	positn	  ;//仓位
	private List<String> listPositn;
	private String	firm	  ;//分店
	private Integer	    folio	  ;//
	private String	stoby      ;//
	private String	accby      ;//
	private double	totalpound;//
	private String	inout	  ;//
	private String	indept	  ;//
	private Integer	    bakno	  ;//
	private String	chk1sta	  ;//
	private String	chk2sta	  ;//
	private String	chk1	  ;//
	private String	chk2	  ;//
	private Date	chk1dat	  ;//
	private Date	chk2dat	  ;//
	private String	chk1memo  ;//
	private String	chk2memo  ;//
	private Date	madedto	  ;//
	private String	chk2bak	  ;//
	private String	chk1nam	  ;//
	private String	chk2nam	  ;//
	private String	typ	      ;//单据类型
	private String	memo	  ;//
	private Integer	    prncnt	  ;//
	private String	chk3	  ;//
	private Date	chk3dat	  ;//
	private String	chk3memo  ;//
	private String	chk3nam   ;//
	private String	chk3sta	  ;//
	private Integer	    bill	  ;//
	private double   pay	  ;//
	private double	pay1	  ;//
	private Integer	    folio1	  ;//
	private Integer	    bill1	  ;//
	private Integer pr;// 接收存储过程；
//	private List<Chkind> chkinmList;//主表
	private List<Chkind> chkindList;//从表
	private String orderBy;//排序
	private String orderDes;
	private int month;
	
	private String madebyName;//制单人名字  2014.12.11wjf
	private String checbyName;//审核人名字 2014.12.11wjf
	private int count;//单据条数，css20151217
	
	//**************采购系统用到字段***************
	private Integer istate;//入库是否已经结算   1：未结算   2  结算
	private Integer pount;//暂时没用   默认值是0
	
	
	public double getNoTaxTotalamt() {
		return noTaxTotalamt;
	}
	public void setNoTaxTotalamt(double noTaxTotalamt) {
		this.noTaxTotalamt = noTaxTotalamt;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public Integer getPount() {
		return pount;
	}
	public void setPount(Integer pount) {
		this.pount = pount;
	}
	
	
	public String getMadebyName() {
		return madebyName;
	}
	public void setMadebyName(String madebyName) {
		this.madebyName = madebyName;
	}
	public String getChecbyName() {
		return checbyName;
	}
	public void setChecbyName(String checbyName) {
		this.checbyName = checbyName;
	}
	//	public List<Chkind> getChkinmList() {
//		return chkinmList;
//	}
//	public void setChkinmList(List<Chkind> chkinmList) {
//		this.chkinmList = chkinmList;
//	}
	public List<String> getListPositn() {
		return listPositn;
	}
	public void setListPositn(List<String> listPositn) {
		this.listPositn = listPositn;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkinno() {
		return chkinno;
	}
	public void setChkinno(Integer chkinno) {
		this.chkinno = chkinno;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public Date getChect() {
		return chect;
	}
	public void setChect(Date chect) {
		this.chect = chect;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public double getTotalamt() {
		return totalamt;
	}
	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Deliver getDeliver() {
		return deliver;
	}
	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Integer getFolio() {
		return folio;
	}
	public void setFolio(Integer folio) {
		this.folio = folio;
	}
	public String getStoby() {
		return stoby;
	}
	public void setStoby(String stoby) {
		this.stoby = stoby;
	}
	public String getAccby() {
		return accby;
	}
	public void setAccby(String accby) {
		this.accby = accby;
	}
	public double getTotalpound() {
		return totalpound;
	}
	public void setTotalpound(double totalpound) {
		this.totalpound = totalpound;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	public String getIndept() {
		return indept;
	}
	public void setIndept(String indept) {
		this.indept = indept;
	}
	public Integer getBakno() {
		return bakno;
	}
	public void setBakno(Integer bakno) {
		this.bakno = bakno;
	}
	public String getChk1sta() {
		return chk1sta;
	}
	public void setChk1sta(String chk1sta) {
		this.chk1sta = chk1sta;
	}
	public String getChk2sta() {
		return chk2sta;
	}
	public void setChk2sta(String chk2sta) {
		this.chk2sta = chk2sta;
	}
	public String getChk1() {
		return chk1;
	}
	public void setChk1(String chk1) {
		this.chk1 = chk1;
	}
	public String getChk2() {
		return chk2;
	}
	public void setChk2(String chk2) {
		this.chk2 = chk2;
	}
	public Date getChk1dat() {
		return chk1dat;
	}
	public void setChk1dat(Date chk1dat) {
		this.chk1dat = chk1dat;
	}
	public Date getChk2dat() {
		return chk2dat;
	}
	public void setChk2dat(Date chk2dat) {
		this.chk2dat = chk2dat;
	}
	public String getChk1memo() {
		return chk1memo;
	}
	public void setChk1memo(String chk1memo) {
		this.chk1memo = chk1memo;
	}
	public String getChk2memo() {
		return chk2memo;
	}
	public void setChk2memo(String chk2memo) {
		this.chk2memo = chk2memo;
	}
	public Date getMadedto() {
		return madedto;
	}
	public void setMadedto(Date madedto) {
		this.madedto = madedto;
	}
	public String getChk2bak() {
		return chk2bak;
	}
	public void setChk2bak(String chk2bak) {
		this.chk2bak = chk2bak;
	}
	public String getChk1nam() {
		return chk1nam;
	}
	public void setChk1nam(String chk1nam) {
		this.chk1nam = chk1nam;
	}
	public String getChk2nam() {
		return chk2nam;
	}
	public void setChk2nam(String chk2nam) {
		this.chk2nam = chk2nam;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Integer getPrncnt() {
		return prncnt;
	}
	public void setPrncnt(Integer prncnt) {
		this.prncnt = prncnt;
	}
	public String getChk3() {
		return chk3;
	}
	public void setChk3(String chk3) {
		this.chk3 = chk3;
	}
	public Date getChk3dat() {
		return chk3dat;
	}
	public void setChk3dat(Date chk3dat) {
		this.chk3dat = chk3dat;
	}
	public String getChk3memo() {
		return chk3memo;
	}
	public void setChk3memo(String chk3memo) {
		this.chk3memo = chk3memo;
	}
	public String getChk3nam() {
		return chk3nam;
	}
	public void setChk3nam(String chk3nam) {
		this.chk3nam = chk3nam;
	}
	public String getChk3sta() {
		return chk3sta;
	}
	public void setChk3sta(String chk3sta) {
		this.chk3sta = chk3sta;
	}
	public Integer getBill() {
		return bill;
	}
	public void setBill(Integer bill) {
		this.bill = bill;
	}
	public double getPay() {
		return pay;
	}
	public void setPay(double pay) {
		this.pay = pay;
	}
	public double getPay1() {
		return pay1;
	}
	public void setPay1(double pay1) {
		this.pay1 = pay1;
	}
	public Integer getFolio1() {
		return folio1;
	}
	public void setFolio1(Integer folio1) {
		this.folio1 = folio1;
	}
	public Integer getBill1() {
		return bill1;
	}
	public void setBill1(Integer bill1) {
		this.bill1 = bill1;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public List<Chkind> getChkindList() {
		return chkindList;
	}
	public void setChkindList(List<Chkind> chkindList) {
		this.chkindList = chkindList;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getOrderDes() {
		return orderDes;
	}
	public void setOrderDes(String orderDes) {
		this.orderDes = orderDes;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		Field[] fields = Chkinm.class.getDeclaredFields();
		try{
			for(Field f : fields){
				f.setAccessible(true);
				Object cur = f.get(this);
				if(null != cur && !(cur instanceof java.util.List<?>)){
					result.append(f.getName()).append(":");
					if(cur instanceof Date){
						result.append(DateFormat.getStringByDate((Date)cur, "yyyy-MM-dd"));
					}else if(cur instanceof Supply){
						result.append(((Supply)cur).getSp_code());
					}else if(cur instanceof Positn){
						result.append(((Positn)cur).getCode());
					}else if(cur instanceof Deliver){
						result.append(((Deliver)cur).getCode());
					}else{
						result.append(cur);
					}
					result.append(",");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
	
	
	
}
