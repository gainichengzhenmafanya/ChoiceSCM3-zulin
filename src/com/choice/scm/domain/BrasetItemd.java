package com.choice.scm.domain;

/**
 * 分店支付明细-子表（Foliochkm）
 * Created by mc on 14-12-31.
 */
public class BrasetItemd {
    private Integer folio;
    private Integer chkno;
    private double pay;
    private String inout;
    private double pay1;

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public Integer getChkno() {
        return chkno;
    }

    public void setChkno(Integer chkno) {
        this.chkno = chkno;
    }

    public double getPay() {
        return pay;
    }

    public void setPay(double pay) {
        this.pay = pay;
    }

    public String getInout() {
        return inout;
    }

    public void setInout(String inout) {
        this.inout = inout;
    }

    public double getPay1() {
        return pay1;
    }

    public void setPay1(double pay1) {
        this.pay1 = pay1;
    }
}
