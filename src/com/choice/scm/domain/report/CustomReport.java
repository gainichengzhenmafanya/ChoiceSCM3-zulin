package com.choice.scm.domain.report;

import java.util.List;

public class CustomReport {

	private String pk_customReport;
	private String vname;
	private String filePath;
	private String parentid;
	private String enablestate;
	private String storeFlag;
	private String startDateFlag;
	private String endDateFlag;
	private String actmFlag; //活动标识
	private String paymodeFlag;//支付方式条件
	private String typ;//小类
	private String sp_code;//物资编码
	private String week;//周次
	
	private List<String> flags;//启用的条件
	
	public CustomReport() {
		filePath="";
		enablestate="1";
		parentid="";
		vname="";
		storeFlag="";
		startDateFlag="";
		endDateFlag="";
	}
	
	public String getStoreFlag() {
		return storeFlag;
	}

	public void setStoreFlag(String storeFlag) {
		this.storeFlag = storeFlag;
	}

	public String getStartDateFlag() {
		return startDateFlag;
	}

	public void setStartDateFlag(String startDateFlag) {
		this.startDateFlag = startDateFlag;
	}

	public String getEndDateFlag() {
		return endDateFlag;
	}

	public void setEndDateFlag(String endDateFlag) {
		this.endDateFlag = endDateFlag;
	}

	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getPk_customReport() {
		return pk_customReport;
	}
	public void setPk_customReport(String pk_customReport) {
		this.pk_customReport = pk_customReport;
	}
	
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}

	public String getActmFlag() {
		return actmFlag;
	}

	public void setActmFlag(String actmFlag) {
		this.actmFlag = actmFlag;
	}

	public String getPaymodeFlag() {
		return paymodeFlag;
	}

	public void setPaymodeFlag(String paymodeFlag) {
		this.paymodeFlag = paymodeFlag;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public List<String> getFlags() {
		return flags;
	}

	public void setFlags(List<String> flags) {
		this.flags = flags;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}
	
	
}
