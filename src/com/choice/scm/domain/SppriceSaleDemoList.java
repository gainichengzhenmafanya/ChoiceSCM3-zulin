package com.choice.scm.domain;

import java.util.List;
/**
 * 售价单模板接收数据用
 * @author csb
 *
 */
public class SppriceSaleDemoList {

	private List<SppriceSaleDemo> sppriceSaleDemoList;

	public List<SppriceSaleDemo> getSppriceSaleDemoList() {
		return sppriceSaleDemoList;
	}

	public void setSppriceSaleDemoList(List<SppriceSaleDemo> sppriceSaleDemoList) {
		this.sppriceSaleDemoList = sppriceSaleDemoList;
	}
	
}
