package com.choice.scm.domain;

import java.math.BigDecimal;
/**
 * 出库单详单
 * @author yp
 */
public class Chkoutd {

	private Integer chkoutno;
	private String	 acct;
	private String   yearr;
	private BigDecimal   id;
	private String   chkoutm;
	private Supply   sp_code;
	private double   price;
	private double   amount;
	private BigDecimal   batchno;//spbatch 的主键  冲消用
	private String   memo;
	private BigDecimal   stoid;
	private BigDecimal   sp_id;
	private BigDecimal   chkstono;
	private double   totalamt;
	private Deliver   deliver;
	private BigDecimal   prn;
	private double   amount1;
	private double   pricesale;
	private double priceamt;
	
	public Integer getChkoutno() {
		return chkoutno;
	}

	public void setChkoutno(Integer chkoutno) {
		this.chkoutno = chkoutno;
	}

	public Chkoutd(){
		stoid = BigDecimal.valueOf(0);
		sp_id = BigDecimal.valueOf(0);
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getYearr() {
		return yearr;
	}

	public void setYearr(String yearr) {
		this.yearr = yearr;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getChkoutm() {
		return chkoutm;
	}

	public void setChkoutm(String chkoutm) {
		this.chkoutm = chkoutm;
	}

	public Supply getSp_code() {
		return sp_code;
	}

	public void setSp_code(Supply sp_code) {
		this.sp_code = sp_code;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public BigDecimal getBatchno() {
		return batchno;
	}

	public void setBatchno(BigDecimal batchno) {
		this.batchno = batchno;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public BigDecimal getStoid() {
		return stoid;
	}

	public void setStoid(BigDecimal stoid) {
		this.stoid = stoid;
	}

	public BigDecimal getSp_id() {
		return sp_id;
	}

	public void setSp_id(BigDecimal sp_id) {
		this.sp_id = sp_id;
	}

	public BigDecimal getChkstono() {
		return chkstono;
	}

	public void setChkstono(BigDecimal chkstono) {
		this.chkstono = chkstono;
	}

	public double getTotalamt() {
		return totalamt;
	}

	public void setTotalamt(double totalamt) {
		this.totalamt = totalamt;
	}

	public Deliver getDeliver() {
		return deliver;
	}

	public void setDeliver(Deliver deliver) {
		this.deliver = deliver;
	}

	public BigDecimal getPrn() {
		return prn;
	}

	public void setPrn(BigDecimal prn) {
		this.prn = prn;
	}

	public double getAmount1() {
		return amount1;
	}

	public void setAmount1(double amount1) {
		this.amount1 = amount1;
	}

	public double getPricesale() {
		return pricesale;
	}

	public void setPricesale(double pricesale) {
		this.pricesale = pricesale;
	}

	public double getPriceamt() {
		return priceamt;
	}

	public void setPriceamt(double priceamt) {
		this.priceamt = priceamt;
	}
	
}
