package com.choice.scm.domain;

import java.util.Date;

/**
 * 售价单模板
 * @author csb
 *
 */
public class SppriceSaleDemo {

	private String 	acct		;//帐套
	private String 	firm		;//分店
	private Supply 	supply		;//物资
	private Date 	bdat		;//开始日期
	private Date 	edat		;//结束日期
	private Double 	price		;//价格
	private String 	memo		;//备注
	private Double 	priceold	;//上次价格
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Double getPriceold() {
		return priceold;
	}
	public void setPriceold(Double priceold) {
		this.priceold = priceold;
	}
	
}
