package com.choice.scm.domain;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * 报表bean
 * @author yp
 *
 * @param <T>
 */
@Component
@Scope(value="prototype")
public class ReportObject<T> {

	private int total;
	private List<T> rows;
	private List<Map<String,Object>> footer;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public List<Map<String, Object>> getFooter() {
		return footer;
	}
	public void setFooter(List<Map<String,Object>> footer) {
		this.footer = footer;
	}
	
}
