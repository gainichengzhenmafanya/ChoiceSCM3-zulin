package com.choice.scm.domain;

import java.util.Date;
import java.util.List;
/**
 * 物资编码
 * @author lehui
 *
 */
public class Supply {
		private List<Supply> SupplyList;     
		private String acct;     //帐套
		private String sp_code;  //编号
		private String sp_name;  //名称
		private String sp_init;  //缩写
		private Double sp_price= 0.0;  //标准价
		private Double sp_tax= 0.0;   //税金
		private String sp_desc;  //规格
		private String sp_methord="先进先出";  //计价方式
		private String sp_type;   //类别
		private String sp_position;  //默认仓位
		private Double sp_per1= 0.0;  //周转（有效）期
		private Double sp_per2= 0.0;  //参考有效期
		private Double sp_max1= 0.0;  //最大库存
		private Double sp_max2= 0.0;  //暂时不用
		private Double sp_min1= 0.0;  //最小库存
		private Double pricesale= 0.0;  //售价
		private String locked="N";  //是否已用
		private String sp_cost="N";  //是否精品
		private String deliver;  //默认供应商
		private String sta="N";  // 状态 ，是否禁用
		private Double unitper0= 1.0;  //第一单位的单位转换率设置
		private Double unitper= 1.0;  //第二单位比第一单位的转换率
		private Double unitper2= 1.0;  //第三单位比第一单位的转换率
		private Double unitper3= 1.0;  //第四单位比第一单位的转换率
		private Double unitper4= 1.0;  //第五单位比第一单位的转换率
		private String unit;   //第一单位，标准单位
		private String unit1;  //第二单位，参考单位
		private String unit2;  //第三单位，成本单位
		private String unit3;  //第四单位，采购单位
		private String unit4;  //第五单位，库存单位
		private String ynunit="N";  //是否第二单位必须输入
		private String yngr="N";  //是否是周转物资
		private String memo;  //备注
		private Integer taxId;   //税率
		private Double tax= 0.0;   //税率
		private String taxdes;  //税率名称
		private String typoth;  //辅助类别
		private String typothdes; //辅助类别名称
		private String typdes;  //类别名称
		private String grp;  //二级类别
		private String grpdes;  //二级类别名称
		private String grptyp;  //一级类别
		private String grptypdes;  //一级类别名称
		private String positndes;  //默认仓位名称
		private Double cnt= 0.0;  //当前剩余数
		private Double cntu= 0.0; //当前第二单位剩余数
		private Double amt= 0.0;  //当前剩余金额
		private String deliverdes; //默认供应商名称
		private String sp_mark;  //品牌
		private String sp_addr;  //产地
		private String stomemo;  //申购标准
		private Double priceold= 0.0;  //上期价格
		private String quamemo;  //质量标准
		private String ex;   //是否门店加工品
		private String chk="0";  //咱不用 保留字段
		private String abc="A";  //暂不用
		
		private Double datsto;  //采购周期
		private String cntuse="0";  //日均用量
		private Double cntminsto= 0.0; //运送时间
		private String ex1;  //是否总部加工品
		private String positnex;  //加工车间
		private double extim;//加工工时
		private String sp_code1;  //条形码
		private String positnexdes; //加工间仓位
		private String ynex="N";      //是否多级加工
		private Double accprate= 0.0;  //验货比率上限
		private Double accpratemin = 0.0;//门店验货比率下限
		private Double stomax= 0.0;  //月最大申购量
		private String stochk1="N";  //是否特殊审核
		private Date datlast; //最后申购日期
		private double stomax1; //单次最大申购量
		private String positn1; //货架
		private String inout="N";  //是否直拨
		private String yndx="N";    //是否代销
		private String yndaypan; // 是否日盘点
		private String ynweekpan; //是否周盘点
		private String ynth; 		//是否可退货
		private String mincnt="0"; //最小申购量
		private String exemp;  //加工人
		private String exempname;  //加工人名称
		private String orderBy;//排序
		private String orderDes;
		private String typ_eas;//报货分类类别
		private String wzqx;//物资权限，无筛选0，按照分店物资属性1，按照账号物资权限2
		private String accountId;//物资权限为2时候的筛选用
		private String wzzhqx;//物资账号权限再细分：0具体到物资  1具体到小类  wjf
		private String upper;//报价涨幅上限
		private String lower;//物资报价下限
		private String psbb;//是否根据配送班表筛选物资
		private String receiveDate;//使用配送班表时候获取到货时间
		private String barCode;//备用
		private String  yhrate;//入库验货比
		private String chrate;//出库验货比
		private String  vcode;//标准产品编码
		private String vname;//标准产品名称
				
		private String is_supply_x;  // 是否存在虚拟物料
		private String sp_code_x;  //虚拟编号
		private String sp_name_x;  //虚拟名称
		private String sp_init_x;  //虚拟缩写
		private String unit_x;  //虚拟单位
		private double unitRate_x;  //虚拟单位对标准单位的转换率
		
		private Integer stotyp;   //报货方式：0不限制，1千元用量 2千次，3菜品点击率，4安全库存，5历史耗用
		private double ratioA;//今天预估系数
		private double ratioB;//明天预估系数
		private double ratioC;//后天预估系数
		private double ratioD;//大后天预估系数
		private double ratioE;//
		private double ratioF;//
		private double ratioG;//
		
		private String attribute;//物料属性2014.10.5css
		
		private Double last_price;//最后进价 2014.12.20wjf
		private String ynbatch;//是否批次管理 2014.12.29 wj
		private String ynCheck="N";//是否总部审核
		
		private double stomin;
		private double stocnt;
		
		private String disunit;//配送单位
		private double disunitper;//配送单位转换率
		private double dismincnt;//配送单位最小申购量
		
		private int pageSize;//用于mysql传一页多少条记录jinshuai
		
		private String exkc;//加工间是否有库存
		private Date dateCondition;//用于传参
		private String positnCondition;//用于传参
		
		public String getPositnCondition() {
			return positnCondition;
		}

		public void setPositnCondition(String positnCondition) {
			this.positnCondition = positnCondition;
		}

		public Date getDateCondition() {
			return dateCondition;
		}

		public void setDateCondition(Date dateCondition) {
			this.dateCondition = dateCondition;
		}

		//九毛九标准原料
		private String sp_code_s;
		private String sp_name_s;
		private String unit_s;
		private double unitRate_s;
		
		public String getExkc() {
			return exkc;
		}

		public void setExkc(String exkc) {
			this.exkc = exkc;
		}

		public int getPageSize() {
			return pageSize;
		}

		public void setPageSize(int pageSize) {
			this.pageSize = pageSize;
		}

		public Double getLast_price() {
			return last_price;
		}

		public void setLast_price(Double last_price) {
			this.last_price = last_price;
		}

		public String getAttribute() {
			return attribute;
		}

		public void setAttribute(String attribute) {
			this.attribute = attribute;
		}

		public Double getUnitper0() {
			return unitper0;
		}

		public void setUnitper0(Double unitper0) {
			this.unitper0 = unitper0;
		}

		public Double getUnitper3() {
			return unitper3;
		}

		public void setUnitper3(Double unitper3) {
			this.unitper3 = unitper3;
		}

		public Double getUnitper4() {
			return unitper4;
		}

		public void setUnitper4(Double unitper4) {
			this.unitper4 = unitper4;
		}

		public String getUnit3() {
			return unit3;
		}

		public void setUnit3(String unit3) {
			this.unit3 = unit3;
		}

		public String getUnit4() {
			return unit4;
		}

		public void setUnit4(String unit4) {
			this.unit4 = unit4;
		}

		public Integer getStotyp() {
			return stotyp;
		}

		public void setStotyp(Integer stotyp) {
			this.stotyp = stotyp;
		}

		public double getRatioF() {
			return ratioF;
		}

		public void setRatioF(double ratioF) {
			this.ratioF = ratioF;
		}

		public double getRatioG() {
			return ratioG;
		}

		public void setRatioG(double ratioG) {
			this.ratioG = ratioG;
		}
		public double getRatioA() {
			return ratioA;
		}

		public void setRatioA(double ratioA) {
			this.ratioA = ratioA;
		}

		public double getRatioB() {
			return ratioB;
		}

		public void setRatioB(double ratioB) {
			this.ratioB = ratioB;
		}

		public double getRatioC() {
			return ratioC;
		}

		public void setRatioC(double ratioC) {
			this.ratioC = ratioC;
		}

		public double getRatioD() {
			return ratioD;
		}

		public void setRatioD(double ratioD) {
			this.ratioD = ratioD;
		}

		public double getRatioE() {
			return ratioE;
		}

		public void setRatioE(double ratioE) {
			this.ratioE = ratioE;
		}
		
		public String getIs_supply_x() {
			return is_supply_x;
		}

		public void setIs_supply_x(String is_supply_x) {
			this.is_supply_x = is_supply_x;
		}

		public String getSp_code_x() {
			return sp_code_x;
		}

		public void setSp_code_x(String sp_code_x) {
			this.sp_code_x = sp_code_x;
		}

		public String getSp_name_x() {
			return sp_name_x;
		}

		public void setSp_name_x(String sp_name_x) {
			this.sp_name_x = sp_name_x;
		}

		public String getSp_init_x() {
			return sp_init_x;
		}

		public void setSp_init_x(String sp_init_x) {
			this.sp_init_x = sp_init_x;
		}

		

		public String getUnit_x() {
			return unit_x;
		}

		public void setUnit_x(String unit_x) {
			this.unit_x = unit_x;
		}

		public double getUnitRate_x() {
			return unitRate_x;
		}

		public void setUnitRate_x(double unitRate_x) {
			this.unitRate_x = unitRate_x;
		}

		public String getVcode() {
			return vcode;
		}

		public void setVcode(String vcode) {
			this.vcode = vcode;
		}

		public String getVname() {
			return vname;
		}

		public void setVname(String vname) {
			this.vname = vname;
		}

		public String getBarCode() {
			return barCode;
		}

		public void setBarCode(String barCode) {
			this.barCode = barCode;
		}

		public String getReceiveDate() {
			return receiveDate;
		}

		public void setReceiveDate(String receiveDate) {
			this.receiveDate = receiveDate;
		}

		public String getPsbb() {
			return psbb;
		}

		public void setPsbb(String psbb) {
			this.psbb = psbb;
		}
				
		public String getYhrate() {
			return yhrate;
		}

		public void setYhrate(String yhrate) {
			this.yhrate = yhrate;
		}

		public String getChrate() {
			return chrate;
		}

		public void setChrate(String chrate) {
			this.chrate = chrate;
		}
		public String getUpper() {
			return upper;
		}

		public void setUpper(String upper) {
			this.upper = upper;
		}

		public String getLower() {
			return lower;
		}

		public void setLower(String lower) {
			this.lower = lower;
		}
	
		public String getYnth() {
			return ynth;
		}

		public void setYnth(String ynth) {
			this.ynth = ynth;
		}

		public Supply() {
			super();
		}
		
		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		public String getWzqx() {
			return wzqx;
		}

		public void setWzqx(String wzqx) {
			this.wzqx = wzqx;
		}

		public Supply(String sp_code) {
			super();
			this.sp_code = sp_code;
		}

		public List<Supply> getSupplyList() {
			return SupplyList;
		}
		public void setSupplyList(List<Supply> supplyList) {
			SupplyList = supplyList;
		}
		public String getAcct() {
			return acct;
		}
		public void setAcct(String acct) {
			this.acct = acct;
		}
		public String getSp_code() {
			return sp_code;
		}
		public void setSp_code(String sp_code) {
			this.sp_code = sp_code;
		}
		public String getSp_name() {
			return sp_name;
		}
		public void setSp_name(String sp_name) {
			this.sp_name = sp_name;
		}
		public String getSp_init() {
			return sp_init;
		}
		public void setSp_init(String sp_init) {
			this.sp_init = sp_init;
		}
		public Double getSp_price() {
			return sp_price;
		}
		public void setSp_price(Double sp_price) {
			this.sp_price = sp_price;
		}
		public Double getSp_tax() {
			return sp_tax;
		}
		public void setSp_tax(Double sp_tax) {
			this.sp_tax = sp_tax;
		}
		public String getSp_desc() {
			return sp_desc;
		}
		public void setSp_desc(String sp_desc) {
			this.sp_desc = sp_desc;
		}
		public String getSp_methord() {
			return sp_methord;
		}
		public void setSp_methord(String sp_methord) {
			this.sp_methord = sp_methord;
		}
		public String getSp_type() {
			return sp_type;
		}
		public void setSp_type(String sp_type) {
			this.sp_type = sp_type;
		}
		public String getSp_position() {
			return sp_position;
		}
		public void setSp_position(String sp_position) {
			this.sp_position = sp_position;
		}
		public Double getSp_per1() {
			return sp_per1;
		}
		public void setSp_per1(Double sp_per1) {
			this.sp_per1 = sp_per1;
		}
		public Double getSp_per2() {
			return sp_per2;
		}
		public void setSp_per2(Double sp_per2) {
			this.sp_per2 = sp_per2;
		}
		public Double getSp_max1() {
			return sp_max1;
		}
		public void setSp_max1(Double sp_max1) {
			this.sp_max1 = sp_max1;
		}
		public Double getSp_max2() {
			return sp_max2;
		}
		public void setSp_max2(Double sp_max2) {
			this.sp_max2 = sp_max2;
		}
		public Double getSp_min1() {
			return sp_min1;
		}
		public void setSp_min1(Double sp_min1) {
			this.sp_min1 = sp_min1;
		}
		public Double getPricesale() {
			return pricesale;
		}
		public void setPricesale(Double pricesale) {
			this.pricesale = pricesale;
		}
		public String getLocked() {
			return locked;
		}
		public void setLocked(String locked) {
			this.locked = locked;
		}
		public String getSp_cost() {
			return sp_cost;
		}
		public void setSp_cost(String sp_cost) {
			this.sp_cost = sp_cost;
		}
		public String getDeliver() {
			return deliver;
		}
		public void setDeliver(String deliver) {
			this.deliver = deliver;
		}
		public String getSta() {
			return sta;
		}
		public void setSta(String sta) {
			this.sta = sta;
		}
		public Double getUnitper() {
			return unitper;
		}
		public void setUnitper(Double unitper) {
			this.unitper = unitper;
		}
		public String getYnunit() {
			return ynunit;
		}
		public void setYnunit(String ynunit) {
			this.ynunit = ynunit;
		}
		public String getYngr() {
			return yngr;
		}
		public void setYngr(String yngr) {
			this.yngr = yngr;
		}
		public Double getUnitper2() {
			return unitper2;
		}
		public void setUnitper2(Double unitper2) {
			this.unitper2 = unitper2;
		}
		public String getMemo() {
			return memo;
		}
		public void setMemo(String memo) {
			this.memo = memo;
		}
		
		public Integer getTaxId() {
			return taxId;
		}

		public void setTaxId(Integer taxId) {
			this.taxId = taxId;
		}

		public Double getTax() {
			return tax;
		}
		public void setTax(Double tax) {
			this.tax = tax;
		}
		public String getTaxdes() {
			return taxdes;
		}
		public void setTaxdes(String taxdes) {
			this.taxdes = taxdes;
		}
		public String getTypoth() {
			return typoth;
		}
		public void setTypoth(String typoth) {
			this.typoth = typoth;
		}
		public String getTypothdes() {
			return typothdes;
		}
		public void setTypothdes(String typothdes) {
			this.typothdes = typothdes;
		}
		public String getUnit() {
			return unit;
		}
		public void setUnit(String unit) {
			this.unit = unit;
		}
		public String getUnit1() {
			return unit1;
		}
		public void setUnit1(String unit1) {
			this.unit1 = unit1;
		}
		public String getUnit2() {
			return unit2;
		}
		public void setUnit2(String unit2) {
			this.unit2 = unit2;
		}
		public String getTypdes() {
			return typdes;
		}
		public void setTypdes(String typdes) {
			this.typdes = typdes;
		}
		public String getGrp() {
			return grp;
		}
		public void setGrp(String grp) {
			this.grp = grp;
		}
		public String getGrpdes() {
			return grpdes;
		}
		public void setGrpdes(String grpdes) {
			this.grpdes = grpdes;
		}
		public String getGrptyp() {
			return grptyp;
		}
		public void setGrptyp(String grptyp) {
			this.grptyp = grptyp;
		}
		public String getGrptypdes() {
			return grptypdes;
		}
		public void setGrptypdes(String grptypdes) {
			this.grptypdes = grptypdes;
		}
		public String getPositndes() {
			return positndes;
		}
		public void setPositndes(String positndes) {
			this.positndes = positndes;
		}
		public Double getCnt() {
			return cnt;
		}
		public void setCnt(Double cnt) {
			this.cnt = cnt;
		}
		public Double getCntu() {
			return cntu;
		}
		public void setCntu(Double cntu) {
			this.cntu = cntu;
		}
		public Double getAmt() {
			return amt;
		}
		public void setAmt(Double amt) {
			this.amt = amt;
		}
		public String getDeliverdes() {
			return deliverdes;
		}
		public void setDeliverdes(String deliverdes) {
			this.deliverdes = deliverdes;
		}
		public String getSp_mark() {
			return sp_mark;
		}
		public void setSp_mark(String sp_mark) {
			this.sp_mark = sp_mark;
		}
		public String getSp_addr() {
			return sp_addr;
		}
		public void setSp_addr(String sp_addr) {
			this.sp_addr = sp_addr;
		}
		public String getStomemo() {
			return stomemo;
		}
		public void setStomemo(String stomemo) {
			this.stomemo = stomemo;
		}
		public Double getPriceold() {
			return priceold;
		}
		public void setPriceold(Double priceold) {
			this.priceold = priceold;
		}
		public String getQuamemo() {
			return quamemo;
		}
		public void setQuamemo(String quamemo) {
			this.quamemo = quamemo;
		}
		public String getEx() {
			return ex;
		}
		public void setEx(String ex) {
			this.ex = ex;
		}
		public String getChk() {
			return chk;
		}
		public void setChk(String chk) {
			this.chk = chk;
		}
		public String getAbc() {
			return abc;
		}
		public void setAbc(String abc) {
			this.abc = abc;
		}
		public Double getDatsto() {
			return datsto;
		}
		public void setDatsto(Double datsto) {
			this.datsto = datsto;
		}
		public String getCntuse() {
			return cntuse;
		}
		public void setCntuse(String cntuse) {
			this.cntuse = cntuse;
		}
		public Double getCntminsto() {
			return cntminsto;
		}
		public void setCntminsto(Double cntminsto) {
			this.cntminsto = cntminsto;
		}
		public String getEx1() {
			return ex1;
		}
		public void setEx1(String ex1) {
			this.ex1 = ex1;
		}
		public String getPositnex() {
			return positnex;
		}
		public void setPositnex(String positnex) {
			this.positnex = positnex;
		}
		public String getSp_code1() {
			return sp_code1;
		}
		public void setSp_code1(String sp_code1) {
			this.sp_code1 = sp_code1;
		}
		public String getPositnexdes() {
			return positnexdes;
		}
		public void setPositnexdes(String positnexdes) {
			this.positnexdes = positnexdes;
		}
		public String getYnex() {
			return ynex;
		}
		public void setYnex(String ynex) {
			this.ynex = ynex;
		}
		public Double getAccprate() {
			return accprate;
		}
		public void setAccprate(Double accprate) {
			this.accprate = accprate;
		}
		public Double getAccpratemin() {
			return accpratemin;
		}

		public void setAccpratemin(Double accpratemin) {
			this.accpratemin = accpratemin;
		}

		public Double getStomax() {
			return stomax;
		}
		public void setStomax(Double stomax) {
			this.stomax = stomax;
		}
		public String getStochk1() {
			return stochk1;
		}
		public void setStochk1(String stochk1) {
			this.stochk1 = stochk1;
		}
		public Date getDatlast() {
			return datlast;
		}
		public void setDatlast(Date datlast) {
			this.datlast = datlast;
		}
		public double getStomax1() {
			return stomax1;
		}
		public void setStomax1(double stomax1) {
			this.stomax1 = stomax1;
		}
		public String getPositn1() {
			return positn1;
		}
		public void setPositn1(String positn1) {
			this.positn1 = positn1;
		}
		public String getInout() {
			return inout;
		}
		public void setInout(String inout) {
			this.inout = inout;
		}
		public String getYndx() {
			return yndx;
		}
		public void setYndx(String yndx) {
			this.yndx = yndx;
		}
		public String getYndaypan() {
			return yndaypan;
		}
		public void setYndaypan(String yndaypan) {
			this.yndaypan = yndaypan;
		}
		public String getYnweekpan() {
			return ynweekpan;
		}
		public void setYnweekpan(String ynweekpan) {
			this.ynweekpan = ynweekpan;
		}
		public String getMincnt() {
			return mincnt;
		}
		public void setMincnt(String mincnt) {
			this.mincnt = mincnt;
		}
		public String getExemp() {
			return exemp;
		}
		public void setExemp(String exemp) {
			this.exemp = exemp;
		}
		public String getExempname() {
			return exempname;
		}
		public void setExempname(String exempname) {
			this.exempname = exempname;
		}
		public String getOrderBy() {
			return orderBy;
		}
		public void setOrderBy(String orderBy) {
			this.orderBy = orderBy;
		}
		public String getOrderDes() {
			return orderDes;
		}
		public void setOrderDes(String orderDes) {
			this.orderDes = orderDes;
		}

		public String getTyp_eas() {
			return typ_eas;
		}

		public void setTyp_eas(String typ_eas) {
			this.typ_eas = typ_eas;
		}

		public double getExtim() {
			return extim;
		}

		public void setExtim(double extim) {
			this.extim = extim;
		}

		public String getWzzhqx() {
			return wzzhqx;
		}

		public void setWzzhqx(String wzzhqx) {
			this.wzzhqx = wzzhqx;
		}

		public String getYnbatch() {
			return ynbatch;
		}

		public void setYnbatch(String ynbatch) {
			this.ynbatch = ynbatch;
		}

		public String getYnCheck() {
			return ynCheck;
		}

		public void setYnCheck(String ynCheck) {
			this.ynCheck = ynCheck;
		}

		public double getStomin() {
			return stomin;
		}

		public void setStomin(double stomin) {
			this.stomin = stomin;
		}

		public double getStocnt() {
			return stocnt;
		}

		public void setStocnt(double stocnt) {
			this.stocnt = stocnt;
		}

		public String getDisunit() {
			return disunit;
		}

		public void setDisunit(String disunit) {
			this.disunit = disunit;
		}

		public double getDisunitper() {
			return disunitper;
		}

		public void setDisunitper(double disunitper) {
			this.disunitper = disunitper;
		}

		public double getDismincnt() {
			return dismincnt;
		}

		public void setDismincnt(double dismincnt) {
			this.dismincnt = dismincnt;
		}

		public String getSp_code_s() {
			return sp_code_s;
		}

		public void setSp_code_s(String sp_code_s) {
			this.sp_code_s = sp_code_s;
		}

		public String getSp_name_s() {
			return sp_name_s;
		}

		public void setSp_name_s(String sp_name_s) {
			this.sp_name_s = sp_name_s;
		}

		public String getUnit_s() {
			return unit_s;
		}

		public void setUnit_s(String unit_s) {
			this.unit_s = unit_s;
		}

		public double getUnitRate_s() {
			return unitRate_s;
		}

		public void setUnitRate_s(double unitRate_s) {
			this.unitRate_s = unitRate_s;
		}
		
}
