package com.choice.scm.domain;

import java.util.List;

/**
 * 菜品bom
 * @author  css
 *
 */
public class Costbom {
	private List<Costbom> costbomList;  
	private String  acct;//帐套
	private String  unit_sprice;//菜品销售单位
	private String pprice;//菜品价格
	private String item;//餐饮菜品编码
	private Supply supply;//物资
	private Double  cnt;//标准数量
	private String cnts;//用来显示用 防止科学计数法  wjf
	private Double  exrate;//取料率
	private Double  excnt;//成本卡单位净数量
	private String excnts;//用来显示用 防止科学计数法  wjf
	private String status;//状态，暂无用
	private String  unit;//餐饮菜品单位
	private Double  cnt1;//参考单位数量
	private String cnt1s;//用来显示用 防止科学计数法  wjf
	private Double  cnt2;//成本单位数量
	private String cnt2s;//用来显示用 防止科学计数法  wjf
	private String sp_exdes;//对应净料的名称（暂不设）
	private String itcode;//菜品编码
	private String mods;//成本卡类型
	private String curStatus;
	private String orderBy;//排序
	private String orderDes;
	private Integer pr;// 接收存储过程；
	
	private String sp_code_x;//虚拟物料编码
	private String unit_x;//虚拟物料单位
//	private Double cnt3;   //虚拟物料用量
//	private Double excnt1;//虚拟物料净用量
//	private Double exrate1; //虚拟物料的出成率(取料率)
	private String is_supply_x;  // 是否存在虚拟物料
	private String sp_code; //实际物料编码
	private String sp_code_n; //替代物料编码      -- 这个是存  被替代者    
	private String yn_n;//是否有可替代
//	private double cnt4;//实际物料用量
	private String mod;//保存时用
	
	private String ids;//批量删除菜品的时候用

    private String modsName;//成本卡类型 名称
	
	private String iscost;//是否核减
	
	public String getIscost() {
		return iscost;
	}
	public void setIscost(String iscost) {
		this.iscost = iscost;
	}
	public void setPprice(String pprice) {
		this.pprice = pprice;
	}
	public String getYn_n() {
		return yn_n;
	}
	public void setYn_n(String yn_n) {
		this.yn_n = yn_n;
	}
	public String getSp_code_n() {
		return sp_code_n;
	}
	public void setSp_code_n(String sp_code_n) {
		this.sp_code_n = sp_code_n;
	}
	public String getPprice() {
		return pprice;
	}
	public void setPrice(String pprice) {
		this.pprice = pprice;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getMod() {
		return mod;
	}
	public void setMod(String mod) {
		this.mod = mod;
	}
	public String getUnit_x() {
		return unit_x;
	}
	public List<Costbom> getCostbomList() {
		return costbomList;
	}
	public void setCostbomList(List<Costbom> costbomList) {
		this.costbomList = costbomList;
	}
	public void setUnit_x(String unit_x) {
		this.unit_x = unit_x;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getUnit_sprice() {
		return unit_sprice;
	}
	public void setUnit_sprice(String unit_sprice) {
		this.unit_sprice = unit_sprice;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Supply getSupply() {
		return supply;
	}
	public void setSupply(Supply supply) {
		this.supply = supply;
	}
	public Double getCnt() {
		return cnt;
	}
	public void setCnt(Double cnt) {
		this.cnt = cnt;
	}
	public Double getExrate() {
		return exrate;
	}
	public void setExrate(Double exrate) {
		this.exrate = exrate;
	}
	public Double getExcnt() {
		return excnt;
	}
	public void setExcnt(Double excnt) {
		this.excnt = excnt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getCnt1() {
		return cnt1;
	}
	public void setCnt1(Double cnt1) {
		this.cnt1 = cnt1;
	}
	public Double getCnt2() {
		return cnt2;
	}
	public void setCnt2(Double cnt2) {
		this.cnt2 = cnt2;
	}
	public String getSp_exdes() {
		return sp_exdes;
	}
	public void setSp_exdes(String sp_exdes) {
		this.sp_exdes = sp_exdes;
	}
	public String getItcode() {
		return itcode;
	}
	public void setItcode(String itcode) {
		this.itcode = itcode;
	}
	public String getMods() {
		return mods;
	}
	public void setMods(String mods) {
		this.mods = mods;
	}
	public String getCurStatus() {
		return curStatus;
	}
	public void setCurStatus(String curStatus) {
		this.curStatus = curStatus;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getOrderDes() {
		return orderDes;
	}
	public void setOrderDes(String orderDes) {
		this.orderDes = orderDes;
	}
	public Integer getPr() {
		return pr;
	}
	public void setPr(Integer pr) {
		this.pr = pr;
	}
	public String getSp_code_x() {
		return sp_code_x;
	}
	public void setSp_code_x(String sp_code_x) {
		this.sp_code_x = sp_code_x;
	}
	public String getIs_supply_x() {
		return is_supply_x;
	}
	public void setIs_supply_x(String is_supply_x) {
		this.is_supply_x = is_supply_x;
	}
	public String getExcnts() {
		return excnts;
	}
	public void setExcnts(String excnts) {
		this.excnts = excnts;
	}
	public String getCnts() {
		return cnts;
	}
	public void setCnts(String cnts) {
		this.cnts = cnts;
	}
	public String getCnt1s() {
		return cnt1s;
	}
	public void setCnt1s(String cnt1s) {
		this.cnt1s = cnt1s;
	}
	public String getCnt2s() {
		return cnt2s;
	}
	public void setCnt2s(String cnt2s) {
		this.cnt2s = cnt2s;
	}

    public String getModsName() {
        return modsName;
    }

    public void setModsName(String modsName) {
        this.modsName = modsName;
    }
}
