package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 配送班表 主
 *
 */
public class Schedule {
	private Integer scheduleID;		//配送班id
	private String scheduleName;	//配送班名称
	private Date beginDate;			//有效开始时间
	private Date endDate;			//有效结束时间
	private String comments;		//说明
	private String status;			//状态
	private String sCode;			//门店
	private String accountId;//仓位权限为1时候的筛选用
	private List<ScheduleStore> acctLineFirmList;//门店
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getsCode() {
		return sCode;
	}
	public void setsCode(String sCode) {
		this.sCode = sCode;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ScheduleStore> getAcctLineFirmList() {
		return acctLineFirmList;
	}
	public void setAcctLineFirmList(List<ScheduleStore> acctLineFirmList) {
		this.acctLineFirmList = acctLineFirmList;
	}
	public Integer getScheduleID() {
		return scheduleID;
	}
	public void setScheduleID(Integer scheduleID) {
		this.scheduleID = scheduleID;
	}
	public String getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
}
