package com.choice.scm.domain;

import java.util.Date;
/**
 * 分店部门物资帐套
 * @author csb
 *
 */
public class FirmDeptSupplyAcct {
	private String	 acct		;//帐套		
	private String	 yearr		;//年份			
	private String	 firm		;//分店		
	private Integer	 rec		;//序号			
	private String	 sp_code	;//物资编码			
	private Integer	 chkno		;//单号			
	private Date	 dat		;//日期			
	private String	 dept		;//部门		
	private String	 des		;//名称			
	private Double	 cntin		;//入库数量
	private Double	 pricein	;//入库价格
	private Double	 amtin		;//入库金额
	private Double	 cntout		;//出库数量
	private Double	 priceout   ;//出库价格
	private Double	 amtout		;//出库金额
	private Integer	 chktag		;//		
	private String	 vouno		;//凭证号			
	private String	 deliver	;//供应商			
	private Double	 cntuin		;//
	private Double	 cntuout	;//
	private String	 positn		;//仓位
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public Integer getRec() {
		return rec;
	}
	public void setRec(Integer rec) {
		this.rec = rec;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public Integer getChkno() {
		return chkno;
	}
	public void setChkno(Integer chkno) {
		this.chkno = chkno;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public Double getCntin() {
		return cntin;
	}
	public void setCntin(Double cntin) {
		this.cntin = cntin;
	}
	public Double getPricein() {
		return pricein;
	}
	public void setPricein(Double pricein) {
		this.pricein = pricein;
	}
	public Double getAmtin() {
		return amtin;
	}
	public void setAmtin(Double amtin) {
		this.amtin = amtin;
	}
	public Double getCntout() {
		return cntout;
	}
	public void setCntout(Double cntout) {
		this.cntout = cntout;
	}
	public Double getPriceout() {
		return priceout;
	}
	public void setPriceout(Double priceout) {
		this.priceout = priceout;
	}
	public Double getAmtout() {
		return amtout;
	}
	public void setAmtout(Double amtout) {
		this.amtout = amtout;
	}
	public Integer getChktag() {
		return chktag;
	}
	public void setChktag(Integer chktag) {
		this.chktag = chktag;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getDeliver() {
		return deliver;
	}
	public void setDeliver(String deliver) {
		this.deliver = deliver;
	}
	public Double getCntuin() {
		return cntuin;
	}
	public void setCntuin(Double cntuin) {
		this.cntuin = cntuin;
	}
	public Double getCntuout() {
		return cntuout;
	}
	public void setCntuout(Double cntuout) {
		this.cntuout = cntuout;
	}
	public String getPositn() {
		return positn;
	}
	public void setPositn(String positn) {
		this.positn = positn;
	}
	
}
