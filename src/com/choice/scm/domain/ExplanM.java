package com.choice.scm.domain;

import java.util.Date;
import java.util.List;

/**
 * 加工单主表
 * @author css
 *
 */
public class ExplanM {
	private String	acct	  ;//帐套
	private String	yearr	  ;//年度
	private Integer	explanno  ;//入库单号
	private Integer	chkoutno=0  ;//原料领料单号
	private Date	maded	  ;//日期   修改时改这个
	private String	madet	  ;//日期
	private Date	checd	  ;//领料日期
	private String	chect	  ;//领料时间
	private Date	indate    ;//登记入库日期
	private String	intime	  ;//登记入库时间
	private String	madeby    ;//操作人员
	private String	checby    ;//领料人员
	private String	inby      ;//登记入库人员
	
	private String	typ       ;//加工单类型：0填写，1报货，2安全库存
	private String	status	  ;//单据状态，加工计划0，已领料1，已入库2
	private Positn	positn	  ;//加工仓位
	private Date	hoped	  ;//期待到货日期
	private List<ExplanD> explanDList;//从表
	private String orderBy;//排序
	private String orderDes;
	private Date	dat;//日期
	private Date	datEnd;//结束日期
	private String ynll;	//是否领料
	
	public String getYnll() {
		return ynll;
	}
	public void setYnll(String ynll) {
		this.ynll = ynll;
	}
	public Date getDat() {
		return dat;
	}
	public void setDat(Date dat) {
		this.dat = dat;
	}
	public Date getDatEnd() {
		return datEnd;
	}
	public void setDatEnd(Date datEnd) {
		this.datEnd = datEnd;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getExplanno() {
		return explanno;
	}
	public void setExplanno(Integer explanno) {
		this.explanno = explanno;
	}
	public Integer getChkoutno() {
		return chkoutno;
	}
	public void setChkoutno(Integer chkoutno) {
		this.chkoutno = chkoutno;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public String getChect() {
		return chect;
	}
	public void setChect(String chect) {
		this.chect = chect;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	public String getIntime() {
		return intime;
	}
	public void setIntime(String intime) {
		this.intime = intime;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getInby() {
		return inby;
	}
	public void setInby(String inby) {
		this.inby = inby;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public Date getHoped() {
		return hoped;
	}
	public void setHoped(Date hoped) {
		this.hoped = hoped;
	}
	public List<ExplanD> getExplanDList() {
		return explanDList;
	}
	public void setExplanDList(List<ExplanD> explanDList) {
		this.explanDList = explanDList;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getOrderDes() {
		return orderDes;
	}
	public void setOrderDes(String orderDes) {
		this.orderDes = orderDes;
	}	
}
