package com.choice.scm.domain;

import java.util.Date;
import java.util.List;
/**
 * 报销单--主表结构
 * @author csb
 *
 */
public class Chkpaym {
	
	private List<Chkpayd> chkpayd;//报销单子表
	private String acct;//帐套                           
	private String yearr;//年份                          
	private Integer chkno;//单号                           
	private Date maded;//制单日期                        
	private String madet;//制单时间                        
	private Date checd;//审核日期                        
	private String chect;//审核时间                        
	private String madeby;//制单人                        
	private String checby;//审核人                        
	private String vouno;//凭证号                        
	private String status;//状态                        
	private Positn positn;//仓位                        
	private Date bdat;//开始日期                        
	private Date edat;//结束日期                        
	private Typ typ;//类别                        
	private String memo;//备注
	public List<Chkpayd> getChkpayd() {
		return chkpayd;
	}
	public void setChkpayd(List<Chkpayd> chkpayd) {
		this.chkpayd = chkpayd;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public Integer getChkno() {
		return chkno;
	}
	public void setChkno(Integer chkno) {
		this.chkno = chkno;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getMadet() {
		return madet;
	}
	public void setMadet(String madet) {
		this.madet = madet;
	}
	public Date getChecd() {
		return checd;
	}
	public void setChecd(Date checd) {
		this.checd = checd;
	}
	public String getChect() {
		return chect;
	}
	public void setChect(String chect) {
		this.chect = chect;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getChecby() {
		return checby;
	}
	public void setChecby(String checby) {
		this.checby = checby;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Positn getPositn() {
		return positn;
	}
	public void setPositn(Positn positn) {
		this.positn = positn;
	}
	public Date getBdat() {
		return bdat;
	}
	public void setBdat(Date bdat) {
		this.bdat = bdat;
	}
	public Date getEdat() {
		return edat;
	}
	public void setEdat(Date edat) {
		this.edat = edat;
	}
	public Typ getTyp() {
		return typ;
	}
	public void setTyp(Typ typ) {
		this.typ = typ;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
			
}
