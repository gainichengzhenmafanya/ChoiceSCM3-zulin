package com.choice.scm.domain;

import java.math.BigDecimal;
import java.util.List;

public class FirmCostAvg {
	
	private String acct;//标示
	private String yearr;//年份
	private String firm;//分店
	private CodeDes fei;//类别
	private BigDecimal amt;//金额
	private String monthh;//月份
	private String item;//半成品编码
	
	private BigDecimal allAmt;//总金额
	private List<FirmCostAvg> firmCostAvgList;
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getYearr() {
		return yearr;
	}
	public void setYearr(String yearr) {
		this.yearr = yearr;
	}
	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public CodeDes getFei() {
		return fei;
	}
	public void setFei(CodeDes fei) {
		this.fei = fei;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getMonthh() {
		return monthh;
	}
	public void setMonthh(String monthh) {
		this.monthh = monthh;
	}
	public BigDecimal getAllAmt() {
		return allAmt;
	}
	public void setAllAmt(BigDecimal allAmt) {
		this.allAmt = allAmt;
	}
	public List<FirmCostAvg> getFirmCostAvgList() {
		return firmCostAvgList;
	}
	public void setFirmCostAvgList(List<FirmCostAvg> firmCostAvgList) {
		this.firmCostAvgList = firmCostAvgList;
	}
	
}
