package com.choice.scm.domain;

public class AccountSupplyTyp {
	private String id;
	private String accountId;
	private String typcode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getTypcode() {
		return typcode;
	}
	public void setTypcode(String typcode) {
		this.typcode = typcode;
	}

}
