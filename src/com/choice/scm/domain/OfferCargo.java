package com.choice.scm.domain;

import java.util.Date;

/**
 * 报货单--主表
 * @author csb
 *
 */
public class OfferCargo {

//	private Integer biztype; 		//业务类型，总部直销报货=1,POS报货（内部单位）=2, 
									//加盟商报货内部单位报货=3,直营门店报货 POS报货（经销商）=4,   
	private Date deliverDate; 		//送货交货日期
//	private Integer isCreate; 		//是否生成物流配送单
//	private Integer	billStatus;		//单据状态，未生成=0,已生成=1
//	private Date auditTime; 		//审核时间
//	private String saleOrgUnit; 	//销售单位
	private String applyOrgUnitID; 	//要货单位
	private String deliveryUnitID; 	//发货单位
	private String customerID; 		//客户
//	private Integer isAutoCreate; 	//自动创建，0
	private Integer isInvaild; 		//是否作废，正常=1,已作废=0
	private String materialGroupID; //物料分类
//	private String whAuditorID; 	//仓库审核人
//	private Positn whAuditTime; 	//仓库审核时间
	private Integer closeStatus; 	//关闭状态已关闭=0,未关闭=1
	private String number; 			//单据编号
	private Date bizDate;			//业务日期
//	private String auditorID;		//审核人
	private String id;				//ID

	public Date getBizDate() {
		return bizDate;
	}
	public void setBizDate(Date bizDate) {
		this.bizDate = bizDate;
	}
	public Date getDeliverDate() {
		return deliverDate;
	}
	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}
	public String getApplyOrgUnitID() {
		return applyOrgUnitID;
	}
	public void setApplyOrgUnitID(String applyOrgUnitID) {
		this.applyOrgUnitID = applyOrgUnitID;
	}
	public String getDeliveryUnitID() {
		return deliveryUnitID;
	}
	public void setDeliveryUnitID(String deliveryUnitID) {
		this.deliveryUnitID = deliveryUnitID;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public Integer getIsInvaild() {
		return isInvaild;
	}
	public void setIsInvaild(Integer isInvaild) {
		this.isInvaild = isInvaild;
	}
	public String getMaterialGroupID() {
		return materialGroupID;
	}
	public void setMaterialGroupID(String materialGroupID) {
		this.materialGroupID = materialGroupID;
	}
	public Integer getCloseStatus() {
		return closeStatus;
	}
	public void setCloseStatus(Integer closeStatus) {
		this.closeStatus = closeStatus;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
