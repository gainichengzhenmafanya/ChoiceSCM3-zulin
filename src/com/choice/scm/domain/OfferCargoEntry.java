package com.choice.scm.domain;


/**
 * 报货单--从表2
 * @author css
 *
 */
public class OfferCargoEntry {

	private String id; 				//id
	private String parentID; 		//主表id
	private String materialID; 		//物料id
	private String model;			//规格
	private Integer isGift;			//是否赠品
	private String baseMeasureUnitID;//基本计量单位
	private double baseAmount;		//基本计量单位数量
	private String measureUnitID;	//计量单位
	private double amount;			//数量
	private double unitPrice;		//单价
	private double money;			//金额
	private String description;		//描述
	private double auditAmount;		//审核数量
	private double sendAmount;		//发送数量
	private double unSendAmount;		//未发送数量
//	private String impNumber;		//导入批次
	private double referencePrice;	//参考售价
	private Integer Seq;			//单据分录序列号
	
//	public String getImpNumber() {
//		return impNumber;
//	}
//	public void setImpNumber(String impNumber) {
//		this.impNumber = impNumber;
//	}
	public double getReferencePrice() {
		return referencePrice;
	}
	public void setReferencePrice(double referencePrice) {
		this.referencePrice = referencePrice;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParentID() {
		return parentID;
	}
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
	public String getMaterialID() {
		return materialID;
	}
	public void setMaterialID(String materialID) {
		this.materialID = materialID;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Integer getIsGift() {
		return isGift;
	}
	public void setIsGift(Integer isGift) {
		this.isGift = isGift;
	}
	public String getBaseMeasureUnitID() {
		return baseMeasureUnitID;
	}
	public void setBaseMeasureUnitID(String baseMeasureUnitID) {
		this.baseMeasureUnitID = baseMeasureUnitID;
	}
	public double getBaseAmount() {
		return baseAmount;
	}
	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	public String getMeasureUnitID() {
		return measureUnitID;
	}
	public void setMeasureUnitID(String measureUnitID) {
		this.measureUnitID = measureUnitID;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAuditAmount() {
		return auditAmount;
	}
	public void setAuditAmount(double auditAmount) {
		this.auditAmount = auditAmount;
	}
	public double getSendAmount() {
		return sendAmount;
	}
	public void setSendAmount(double sendAmount) {
		this.sendAmount = sendAmount;
	}
	public double getUnSendAmount() {
		return unSendAmount;
	}
	public void setUnSendAmount(double unSendAmount) {
		this.unSendAmount = unSendAmount;
	}
	public Integer getSeq() {
		return Seq;
	}
	public void setSeq(Integer seq) {
		Seq = seq;
	}

}
