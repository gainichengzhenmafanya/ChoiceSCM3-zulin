package com.choice.scm.domain;


/**
 * 到货单子表
 * @author wjf
 *
 */
public class Arrivald {
	
	private String	pk_group	;//	varchar(32)	企业ID
	private String	pk_arrivald	;//	varchar(32)	到货单子表主键
	private String	pk_arrival	;//	varchar(32)	到货单主表主键
	private String	pk_material	;//	varchar(32)	物资主键
	private String	vcode	;//	varchar(50)	物资编码
	private String	vname	;//	varchar(50)	物资名称
	private String  sp_desc;//物资规格
	private String  unit1;//参考单位
	private String  unitper;//标准单位和参考单位的转换率
	private String	pk_materialjmu	;//	varchar(32)	商城物资主键
	private String	vcodejmu	;//	varchar(50)	商城物资编码
	private String	vnamejmu	;//	varchar(50)	商城物资名称
	private double	npurnum	;//	double	采购数量
	private int	bisgift	;//	int	是否赠品
	private double	narrnum	;//	double	到货数量
	private String	pk_unit	;//	varchar(32)	单位
	private double	nprice	;//	double	单价
	private double	nmoney	;//	double	金额
	private String	vmemo	;//	varchar(200)	摘要
	private String	vbdef1	;//	varchar(50)	自定义项1
	private String	vbdef2	;//	varchar(50)	自定义项2
	private String	vbdef3	;//	varchar(50)	自定义项3
	private String	vbdef4	;//	varchar(50)	自定义项4
	private String	vbdef5	;//	varchar(50)	自定义项5
	private int	dr	;//	int	删除标志
	private String	ts	;//	varchar(19)	时间戳
	private double ninsnum;//验货数量
	
	private String pk_supplier;//供应商主键	pk_supplier	varchar(32)
	private String deliverDes;//供应商名称
	private String darrbilldate;//单据日期	darrbilldate	varchar(10)
	private String pk_org;//采购组织	pk_org	varchar(32)
	private String positnDes;//
	
	private String disunit;
	private double disunitper;
	
	public String getDisunit() {
		return disunit;
	}
	public void setDisunit(String disunit) {
		this.disunit = disunit;
	}
	public double getDisunitper() {
		return disunitper;
	}
	public void setDisunitper(double disunitper) {
		this.disunitper = disunitper;
	}
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_arrivald() {
		return pk_arrivald;
	}
	public void setPk_arrivald(String pk_arrivald) {
		this.pk_arrivald = pk_arrivald;
	}
	public String getPk_arrival() {
		return pk_arrival;
	}
	public void setPk_arrival(String pk_arrival) {
		this.pk_arrival = pk_arrival;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getUnitper() {
		return unitper;
	}
	public void setUnitper(String unitper) {
		this.unitper = unitper;
	}
	public String getPk_materialjmu() {
		return pk_materialjmu;
	}
	public void setPk_materialjmu(String pk_materialjmu) {
		this.pk_materialjmu = pk_materialjmu;
	}
	public String getVcodejmu() {
		return vcodejmu;
	}
	public void setVcodejmu(String vcodejmu) {
		this.vcodejmu = vcodejmu;
	}
	public String getVnamejmu() {
		return vnamejmu;
	}
	public void setVnamejmu(String vnamejmu) {
		this.vnamejmu = vnamejmu;
	}
	public double getNpurnum() {
		return npurnum;
	}
	public void setNpurnum(double npurnum) {
		this.npurnum = npurnum;
	}
	public int getBisgift() {
		return bisgift;
	}
	public void setBisgift(int bisgift) {
		this.bisgift = bisgift;
	}
	public double getNarrnum() {
		return narrnum;
	}
	public void setNarrnum(double narrnum) {
		this.narrnum = narrnum;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	public double getNprice() {
		return nprice;
	}
	public void setNprice(double nprice) {
		this.nprice = nprice;
	}
	public double getNmoney() {
		return nmoney;
	}
	public void setNmoney(double nmoney) {
		this.nmoney = nmoney;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVbdef1() {
		return vbdef1;
	}
	public void setVbdef1(String vbdef1) {
		this.vbdef1 = vbdef1;
	}
	public String getVbdef2() {
		return vbdef2;
	}
	public void setVbdef2(String vbdef2) {
		this.vbdef2 = vbdef2;
	}
	public String getVbdef3() {
		return vbdef3;
	}
	public void setVbdef3(String vbdef3) {
		this.vbdef3 = vbdef3;
	}
	public String getVbdef4() {
		return vbdef4;
	}
	public void setVbdef4(String vbdef4) {
		this.vbdef4 = vbdef4;
	}
	public String getVbdef5() {
		return vbdef5;
	}
	public void setVbdef5(String vbdef5) {
		this.vbdef5 = vbdef5;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public double getNinsnum() {
		return ninsnum;
	}
	public void setNinsnum(double ninsnum) {
		this.ninsnum = ninsnum;
	}
	public String getPk_supplier() {
		return pk_supplier;
	}
	public void setPk_supplier(String pk_supplier) {
		this.pk_supplier = pk_supplier;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getDarrbilldate() {
		return darrbilldate;
	}
	public void setDarrbilldate(String darrbilldate) {
		this.darrbilldate = darrbilldate;
	}
	public String getPk_org() {
		return pk_org;
	}
	public void setPk_org(String pk_org) {
		this.pk_org = pk_org;
	}
	public String getPositnDes() {
		return positnDes;
	}
	public void setPositnDes(String positnDes) {
		this.positnDes = positnDes;
	}
	
}
