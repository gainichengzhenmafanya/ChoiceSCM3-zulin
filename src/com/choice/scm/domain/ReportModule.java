package com.choice.scm.domain;

import java.util.List;

public class ReportModule {
	private String	id;
	private String	name;
	private String	url;
	private String	sta;
	private String	typ;
	private Integer	sequence;
	
	private String accountId;
	private String locale;
	private String accountModuleId;
	private List<ReportModule> rms;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getAccountModuleId() {
		return accountModuleId;
	}
	public void setAccountModuleId(String accountModuleId) {
		this.accountModuleId = accountModuleId;
	}
	public List<ReportModule> getRms() {
		return rms;
	}
	public void setRms(List<ReportModule> rms) {
		this.rms = rms;
	}

}
