package com.choice.scm.domain;

import java.util.Date;
import java.util.List;


/**
 * 到货单主表
 * @author wjf
 *
 */
public class Arrivalm {
	
	//字段
	private String pk_group;//企业ID	pk_group	varchar(32)
	private String pk_arrival;//到货单主键	pk_arrival	varchar(32)
	private String pk_arrivals;//批量主键   批量验收用
	private String varrbillno;//到货单单号	varrbillno	varchar(32)
	private String vpuprbillnojmu;//商城订单号	vpuprbillnojmu	varchar(32)
	private String pk_supplier;//供应商主键	pk_supplier	varchar(32)
	private String deliverDes;//供应商名称
	private String mail;//邮箱
	private double nmoney;//合计金额	nmoney	double
	private Integer istate;//单据状态	istate	int
	private String darrbilldate;//单据日期	darrbilldate	varchar(10)
	private String pk_org;//采购组织	pk_org	varchar(32)
	private String positnDes;//
	private String darrbilltime;//单据时间	darrbilltime	varchar(32)
	private String vmemo;//摘要	vmemo	varchar(200)
	private int dr;//	dr	int
	private String ts;//	ts	varchar(19)
	private String vdef1;//	自定义项1varchar(50)
	private String vdef2;//	自定义项2varchar(50)
	private String vdef3;//	自定义项3varchar(50)
	private String vdef4;//	自定义项4varchar(50)
	private String vdef5;//	自定义项5varchar(50)
	private String vbatchno;//报货单凭证号
	private int ichkinno;//入库单号
	private int chkstono;//报货单号
	//查询条件
	private String bdate;
	private String edate;
	
	//生产入库单用
	private List<Arrivald> arrivaldList;
	private String acct;//帐套
	private Date maded;//入库单日期
	private String madeby;//制单人
	private String pr;//页面上判断入库成功还是失败
	
	private String title;//邮件主题
	private String content;//邮件正文
	
	private String sp_code;
	private int selectType;//总部验收入查询方式 0按单据 1按物资 2按日期
	
	public String getPk_group() {
		return pk_group;
	}
	public void setPk_group(String pk_group) {
		this.pk_group = pk_group;
	}
	public String getPk_arrivals() {
		return pk_arrivals;
	}
	public void setPk_arrivals(String pk_arrivals) {
		this.pk_arrivals = pk_arrivals;
	}
	public String getPk_arrival() {
		return pk_arrival;
	}
	public void setPk_arrival(String pk_arrival) {
		this.pk_arrival = pk_arrival;
	}
	public String getVarrbillno() {
		return varrbillno;
	}
	public void setVarrbillno(String varrbillno) {
		this.varrbillno = varrbillno;
	}
	public String getVpuprbillnojmu() {
		return vpuprbillnojmu;
	}
	public void setVpuprbillnojmu(String vpuprbillnojmu) {
		this.vpuprbillnojmu = vpuprbillnojmu;
	}
	public String getPk_supplier() {
		return pk_supplier;
	}
	public void setPk_supplier(String pk_supplier) {
		this.pk_supplier = pk_supplier;
	}
	public String getDeliverDes() {
		return deliverDes;
	}
	public void setDeliverDes(String deliverDes) {
		this.deliverDes = deliverDes;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public double getNmoney() {
		return nmoney;
	}
	public void setNmoney(double nmoney) {
		this.nmoney = nmoney;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public String getDarrbilldate() {
		return darrbilldate;
	}
	public void setDarrbilldate(String darrbilldate) {
		this.darrbilldate = darrbilldate;
	}
	public String getPk_org() {
		return pk_org;
	}
	public void setPk_org(String pk_org) {
		this.pk_org = pk_org;
	}
	public String getPositnDes() {
		return positnDes;
	}
	public void setPositnDes(String positnDes) {
		this.positnDes = positnDes;
	}
	public String getDarrbilltime() {
		return darrbilltime;
	}
	public void setDarrbilltime(String darrbilltime) {
		this.darrbilltime = darrbilltime;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getVbatchno() {
		return vbatchno;
	}
	public void setVbatchno(String vbatchno) {
		this.vbatchno = vbatchno;
	}
	public int getIchkinno() {
		return ichkinno;
	}
	public void setIchkinno(int ichkinno) {
		this.ichkinno = ichkinno;
	}
	public int getChkstono() {
		return chkstono;
	}
	public void setChkstono(int chkstono) {
		this.chkstono = chkstono;
	}
	public List<Arrivald> getArrivaldList() {
		return arrivaldList;
	}
	public void setArrivaldList(List<Arrivald> arrivaldList) {
		this.arrivaldList = arrivaldList;
	}
	public Date getMaded() {
		return maded;
	}
	public void setMaded(Date maded) {
		this.maded = maded;
	}
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getMadeby() {
		return madeby;
	}
	public void setMadeby(String madeby) {
		this.madeby = madeby;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}	
	public int getSelectType() {
		return selectType;
	}
	public void setSelectType(int selectType) {
		this.selectType = selectType;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	
}
