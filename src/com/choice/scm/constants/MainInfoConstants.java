package com.choice.scm.constants;

public class MainInfoConstants {

	//页面跳转
	public static final String LIST_MAININFO="scm/costcut/listMainInfo";
	public static final String UPDATE_MAININFO="scm/costcut/updateMainInfo";

	public static final String MAIN="tableMain";
	public static final String MAIN_XWY="tableMain_xwy";
	public static final String IMAGEMAIN="orientation/myPage";
	public static final String CONFIG_MAIN="scm/message/tableMainManage";
	
	public static final String OVER_LIMIT="scm/message/supplyOverLimit";
	public static final String LESS_LIMIT="scm/message/supplyLessLimit";
	
	public static final String USED_TAG = "scm/costcut/listModelTable";
	
	public static final String USED_REPORT = "scm/report/listUsedReport";//我的常用报表
}
