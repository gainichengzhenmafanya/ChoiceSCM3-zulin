package com.choice.scm.constants;

public class Chkstom_xConstants {

	//页面跳转
	public static final String REPORT_PRINT_URL="report/chkstom/chkstomReport.jasper";
	public static final String TABLE_CHKSTOM="scm/chkstom_x/tableChkstom_x";
	public static final String MIS_TABLE_CHKSTOM="mis/chkstom_x/tableChkstom_x";//MIS
	public static final String TABLE_CHECKED_CHKSTOM="scm/chkstom_x/listCheckedChkstom_x";
	public static final String MIS_TABLE_CHECKED_CHKSTOM="mis/chkstom_x/listCheckedChkstom_x";//MIS
	public static final String SEARCH_CHKSTOM="scm/chkstom_x/searchChkstom_x";
	public static final String MIS_SEARCH_CHKSTOM="mis/chkstom_x/searchChkstom_x";//MIS
	public static final String SEARCH_UPLOAD="scm/chkstom_x/searchUpload_x";
	public static final String MIS_SEARCH_UPLOAD="mis/chkstom_x/searchUpload_x";//MIS
	public static final String SEARCH_CHECKED_CHKSTOM="scm/chkstom_x/tableCheckedChkstom_x";
	public static final String MIS_SEARCH_CHECKED_CHKSTOM="mis/chkstom_x/tableCheckedChkstom_x";//MIS
}
