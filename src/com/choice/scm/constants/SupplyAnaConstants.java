package com.choice.scm.constants;

public class SupplyAnaConstants {

	public static final String MIS_REPORT_LIST = "/mis/report/anaReportListWare";
	public static final String REPORT_LIST = "/scm/reportAnalysis/reportListWare";
	
	//价格预警和比重
	public static final String REPORT_SHOW_PRICEWARNING="scm/reportAnalysis/JgYujingBizhong";
	public static final String REPORT_SHOW_PRICEWARNING_MIS="/mis/reportAnalysis/JgYujingBizhong";
	public static final String REPORT_PRINT_URL_PRICEWARNING="/report/analysis/PriceWarning.jasper";//报表地址
	public static final String REPORT_EXP_URL_PRICEWARNING="/report/analysis/PriceWarning.jasper";
	
	//采购价格异动
	public static final String REPORT_SHOW_PURCHASEPRICECHANGE = "scm/reportAnalysis/CgJiageYidong";
	public static final String REPORT_SHOW_PURCHASEPRICECHANGE_MIS = "/mis/reportAnalysis/CgJiageYidong";
	public static final String REPORT_PRINT_URL_PURCHASEPRICECHANGE="/report/analysis/PurchasePriceChange.jasper";//报表地址
	public static final String REPORT_EXP_URL_PURCHASEPRICECHANGE="/report/analysis/PurchasePriceChange.jasper";
	
	//采购价格异动1 
	public static final String REPORT_SHOW_PURCHASEPRICECHANGE1="scm/reportAnalysis/CgJiageYidong1";
	public static final String REPORT_PRINT_URL_PURCHASEPRICECHANGE1="/report/analysis/PurchasePriceChange1.jasper";//报表地址
	public static final String REPORT_EXP_URL_PURCHASEPRICECHANGE1="/report/analysis/PurchasePriceChange1.jasper";
	
	//物资供应商分析
	public static final String REPORT_SHOW_SUPPLYDELIVER = "scm/reportAnalysis/WzGysFenxi";
		
	//进货价格分析
	public static final String REPORT_SHOW_FIRMSTOCKPRICE="scm/reportAnalysis/JhJiageFenxi";
	public static final String REPORT_PRINT_URL_FIRMSTOCKPRICE="/report/analysis/FirmStockPrice.jasper";//报表地址
	public static final String REPORT_EXP_URL_FIRMSTOCKPRICE="/report/analysis/FirmStockPrice.jasper";
	//进货价格比较
	public static final String REPORT_SHOW_FIRMSTOCKPRICE_MIS="mis/reportAnalysis/JhJiageBijiao";
	
	//库存周转率
	public static final String LIST_KCZZL="scm/reportAnalysis/KcZhouzhuanLv";//库存周转率
	public static final String LIST_KCZZL_MIS="mis/reportAnalysis/KcZhouzhuanLv";//库存周转率
	public static final String REPORT_PRINT_URL_KCZZL="/report/chkinm/kucunzhouzhuanlv.jasper";//库存周转率
	
	//库龄综合分析
	public static final String REPORT_SHOW_INVENTORYAGINGSUM="scm/reportAnalysis/KlZongheFenxi";
	public static final String REPORT_SHOW_INVENTORYAGINGSUM_MIS="mis/reportAnalysis/KlZongheFenxi";
	public static final String REPORT_PRINT_URL_INVENTORYAGINGSUM="/report/analysis/InventoryAgingSum.jasper";//报表地址
	public static final String REPORT_EXP_URL_INVENTORYAGINGSUM="/report/analysis/InventoryAgingSum.jasper";
	
	//物资库领明细
	public static final String LIST_SUPPLYKULINGDETAIL="scm/reportAnalysis/WzKulingMingxi";//物资库龄明细
	public static final String LIST_SUPPLYKULINGDETAIL_MIS="mis/reportAnalysis/WzKulingMingxi";//物资库龄明细
	public static final String REPORT_PRINT_URL_SUPPLYKULINGDETAIL="/report/chkinm/supplyKuLingDetail.jasper";//物资库龄明细报表地址
	
	//物资ABC分析
	public static final String REPORT_SHOW_SUPPLYABCSTATISTICS="scm/reportAnalysis/WzABCFenxi";
	public static final String REPORT_PRINT_URL_SUPPLYABCSTATISTICS="/report/analysis/supplyABCStatistics.jasper";//报表地址
	public static final String REPORT_EXP_URL_SUPPLYABCSTATISTICS="/report/analysis/supplyABCStatistics.jasper";
	
	//仓库周发货对比
	public static final String REPORT_SHOW_CANGKUZHOUFAHUODUIBI="scm/reportAnalysis/CkZhoufahuoDuibi";
	public static final String REPORT_PRINT_URL_CANGKUZHOUFAHUODUIBI="/report/analysis/CangKuZhouFaHuoDuiBi.jasper";//报表地址
	public static final String REPORT_EXP_URL_CANGKUZHOUFAHUODUIBI="/report/analysis/CangKuZhouFaHuoDuiBi.jasper";
	
	//仓库领用对比
	public static final String REPORT_SHOW_WAREHOUSEREQUISITIONEDCONTRAST="scm/reportAnalysis/CkLingyongDuibi";
	
	//入库成本分析
	public static final String REPORT_SHOW_RUKUCHENGBENFENXI="scm/reportAnalysis/RkChengbenFenxi";
	public static final String REPORT_SHOW_RUKUCHENGBENFENXI_MIS="mis/reportAnalysis/RkChengbenFenxi";
	public static final String REPORT_PRINT_URL_RECEIPTCOSTANALYSISSUM="/report/analysis/ReceiptCostAnalysisSum.jasper";//报表地址
	
	//资金占用情况
	public static final String REPORT_SHOW_ZIJINZHANYONGQINGKUANG="/scm/reportAnalysis/ZjZhanyongQingkuang";
	public static final String REPORT_PRINT_URL_ZIJINZHANYONGQINGKUANG="/report/Profit/ziJinZhanYongQingKuang.jasper";
	
	//周入库成本分析
	public static final String REPORT_SHOW_ZHOURUKUCHENGBENFENXI="scm/reportAnalysis/zhouRkChengbenFenxi";
	public static final String REPORT_PRINT_URL_ZHOURUKUCHENGBENFENXI="/report/analysis/ZhouRuKuChengBenFenXi.jasper";//报表地址
	//入库成本分析
	//public static final String REPORT_SHOW_ZHOURUKUCHENGBENFENXI_MIS="scm/reportAnalysis/zhouRuKuChengBenFenXi";
		
	//不合格进货统计BhgJinhuoTongji
	public static final String REPORT_SHOW_UNQUALIFIEDSTATISTICS="scm/reportAnalysis/BhgJinhuoTongji";
	public static final String REPORT_PRINT_URL_UNQUALIFIEDSTATISTICS="/report/analysis/UnqualifiedStatistics.jasper";//报表地址
	public static final String REPORT_EXP_URL_UNQUALIFIEDSTATISTICS="/report/analysis/UnqualifiedStatistics.jasper";
	
	//申购到货分析
	public static final String REPORT_SHOW_APPLYARRIVESTATISTICS="scm/reportAnalysis/SgDaohuoFenxi";
	public static final String REPORT_PRINT_URL_APPLYARRIVESTATISTICS="/report/analysis/applyArriveStatistics.jasper";//报表地址
	public static final String REPORT_EXP_URL_APPLYARRIVESTATISTICS="/report/analysis/applyArriveStatistics.jasper";
	
	//申购出库分析
	public static final String REPORT_SHOW_APPLYOUTSTATISTICS="scm/reportAnalysis/SgChukuFenxi";
	public static final String REPORT_PRINT_URL_APPLYOUTSTATISTICS="/report/analysis/applyOutStatistics.jasper";//报表地址
	public static final String REPORT_EXP_URL_APPLYOUTSTATISTICS="/report/analysis/applyOutStatistics.jasper";

	//物流配送分析
	public static final String REPORT_SHOW_LOGISTICSANALYSIS="scm/reportAnalysis/WlPeisongFenxi";
	public static final String REPORT_PRINT_URL_LOGISTICSANALYSIS="/report/analysis/LogisticsAnalysis.jasper";//报表地址
	public static final String REPORT_EXP_URL_LOGISTICSANALYSIS="/report/analysis/LogisticsAnalysis.jasper";
	
	//库存量预估使用
	public static final String REPORT_SHOW_INVENTORYESTIMATES="scm/reportAnalysis/KclYuguShiyong";
	public static final String REPORT_PRINT_URL_INVENTORYESTIMATES="/report/analysis/InventoryEstimates.jasper";//报表地址
	public static final String REPORT_EXP_URL_INVENTORYESTIMATES="/report/analysis/InventoryEstimates.jasper";
	
	//采购测算分析
	public static final String REPORT_PRINT_URL_CAIGOUCESUAN="/report/chkinm/caiGouCeSuan.jasper";//报表地址
	
	
}
