package com.choice.scm.constants;

public class PositnRoleConstants {

	public static final String LIST_POSITN_ROLE = "scm/positnRole/listPositnRole";
	public static final String LIST_POSITN_ACCOUNT = "scm/positnRole/listPositnAccount";
	
	
	public static final String LIST_Firm_ACCOUNT = "scm/positnRole/listFirmAccount";
	
}
