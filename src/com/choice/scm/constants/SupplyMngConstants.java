package com.choice.scm.constants;

public class SupplyMngConstants {

	public static final String LIST_SUPPLYMNG="scm/supplyMng/listSupplyMng"; //物资管理的物资树结构
	
	public static final String TABLE_SUPPLYMNG="scm/supplyMng/tableSupplyMng"; //物资管理的物资列表
	
	public static final String TABLE_SUPPLYMNG_X="scm/supplyMng/tableSupplyMng_x";//物资管理的物资列表(使用虚拟物料)
	
	public static final String SELECT_SUPPLYDELIVER="scm/supplyMng/selectSupplyDeliver"; //选择物资对应的供应商
	
	public static final String SELECT_SUPPLYPOSITN="scm/supplyMng/selectSupplyPositn"; //选择物资对应的仓位
	
	public static final String UPDATE_SUPPLY_X="scm/supplyMng/update_supply_x";//选择物资对应的虚拟物料
	
	public static final String UPDATE_SUPPLY="scm/supplyMng/update_supply";//选择物资对应的实际物料
	
	public static final String SELECT_SUPPLYPOSITN1="scm/supplyMng/selectSupplyPositn1"; // 选择物资对应的货架
	
	public static final String SET_ABC="scm/supplyMng/setABC"; // 选择物资对应的ABC类型
	
	public static final String TABLE_SECUSTOCK="scm/supplyMng/tableSecuStockSet"; // 安全库存设置
	public static final String TABLE_SECUSTOCK_FIRM="scm/supplyMng/tableSecuStockSetFirm"; // 门店安全库存设置
	public static final String UPDATE_CNT="scm/supplyMng/updateCnt"; // 按类别修改库存量页面
	
	public static final String MODIFY_ATTRIBUTE="scm/supplyMng/modifyAttribute"; // 选择物资属性
	//物资是否领料属性的页面
	public static final String SHOW_UPDATE_EXKC="scm/supplyMng/showUpdateExkc";
}
