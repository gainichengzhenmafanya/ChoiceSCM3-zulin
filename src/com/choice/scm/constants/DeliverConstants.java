package com.choice.scm.constants;

public class DeliverConstants {

	//页面跳转
	public static final String REPORT_PRINT_URL="report/deliver/deliver.jasper";//供应商信息打印
	public static final String TABLE_DELIVERS="scm/deliver/tableDeliver";
	
	public static final String TABLE_DELIVERS1="scm/deliver/listDeliverType";//供应商管理左边供应商类型
	
	public static final String SAVE_DELIVERS="scm/deliver/saveDeliver";
	public static final String COPY_DELIVERS="scm/deliver/copyDeliver";
	public static final String UPDATE_DELIVERS="scm/deliver/updateDeliver";
	public static final String SEARCH_DELIVERS="scm/deliver/searchDeliver";
	public static final String PRICE_DELIVERS = "scm/deliver/sppriceDeliver";
	public static final String SELECT_DELIVERS = "scm/deliver/selectDeliver";
	public static final String SEARCH_ALLDELIVERS = "scm/deliver/searchAllDeliver";
	public static final String SELECT_ONEDELIVERS = "scm/deliver/selectOneDeliver";
	public static final String ADD_DELIVERSBATCH="scm/deliver/addDeliverBatch";
	public static final String SELECT_DELIVERPOSITN = "scm/deliver/selectDeliverPositn";
	
	public static final String SELECT_DELIVERPOSITNS = "scm/deliver/selectDeliverPositns";//修改供应商对应仓位编码
	
	public static final String LIST_DELIVERDIS = "scm/deliver/listDeliverDis";
	
	public static final String SELECT_ONEDELIVERS2 = "scm/deliver/selectOneDeliver2";//选择供应商新页面，左右分栏
	public static final String TABLE_DELIVERS2="scm/deliver/tableDeliver2";//选择供应商右侧栏
	
	//wangjie 2014年11月20日 15:55:20  批量修改供应商物资到期提醒设置 页面
	public static final String SAVE_WARMDAYS = "scm/deliver/saveWarmDays";
	//供应物资范围
	public static final String ADD_SUPPLY_BATCH_FIRM_ADD = "scm/deliver/addSupplyBatchFirmByAdd";
	public static final String SELECT_NSUPPLY="scm/deliver/selectNSupply";
	public static final String SELECT_TABLENSUPPLY="scm/deliver/selectTableNSupply";
	public static final String  DIVISIONSUPPLY="scm/deliver/divisionSupply";
	public static final String  GROUPSUPPLY="scm/deliver/groupSupply";
	public static final String  CLASSSUPPLY="scm/deliver/classSupply";
	public static final String  SAVEDELIVERSUPPLY="scm/deliver/saveDeliverSupply";
	/**
	 * 关联商城供应商
	 */
	public static final String SELECTJMUDELIVER = "scm/deliver/selectjmuSupplier";
	/**
	 * 关联商城物资
	 */
	public static final String SELECTJMUSUPPLY = "scm/deliver/chooseMallMaterial";
		
}
