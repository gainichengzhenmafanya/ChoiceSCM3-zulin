package com.choice.scm.constants;



public class BanChengPinPriceConstants {
	/**
	 * 半成品 价格 跳转
	 */
	public final static String LIST_BANCHENGPIN="scm/BanChengPinPrice/listBanChengPinPrice";
	/**
	 * 中心费用信息
	 */
	public final static String LIST_FIRMCOSTAVG="scm/BanChengPinPrice/listfirmCostAvg";
	/**
	 * 中心费用信息
	 */
	public final static String LIST_FIRMCOSTAVGNEW="scm/BanChengPinPrice/listfirmCostAvgNew";
	/**
	 * 加工工时信息
	 */
	public final static String LIST_EXTIM="scm/BanChengPinPrice/listExTim";
	/**
	 * 加工工时信息
	 */
	public final static String LIST_EXTIMNEW="scm/BanChengPinPrice/listExTimNew";
	/**
	 * 半成品费用 跳转
	 */
	public final static String LIST_SPCODEEXPRICE="scm/BanChengPinPrice/listSpCodeExPrice";
	/**
	 * 半成品费用 跳转
	 */
	public final static String LIST_SPCODEEXPRICENEW="scm/BanChengPinPrice/listSpCodeExPriceNew";
}
