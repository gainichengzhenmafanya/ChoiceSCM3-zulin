package com.choice.scm.constants;

public class ChkstodemoConstants {

	//页面跳转
	public static final String TABLE_CHKSTODEMO="scm/chkstodemo/treeChkstodemo";
	public static final String LIST_CHKSTODEMO="scm/chkstodemo/listChkstodemo";
	public static final String ADD_CHKSTODEMO="scm/chkstodemo/addChkstodemo";
	public static final String SAVE_CHKSTODEMO="scm/chkstodemo/saveChkstodemo";
	public static final String MIS_TABLE_CHKSTODEMO="mis/chkstodemo/treeChkstodemo";
	public static final String MIS_LIST_CHKSTODEMO="mis/chkstodemo/listChkstodemo";
	public static final String MIS_ADD_CHKSTODEMO="mis/chkstodemo/addChkstodemo";
	public static final String MIS_SAVE_CHKSTODEMO="mis/chkstodemo/saveChkstodemo";
}
