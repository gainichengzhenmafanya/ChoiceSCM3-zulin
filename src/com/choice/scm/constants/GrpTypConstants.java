package com.choice.scm.constants;
/**
 * @author yp
 */
public class GrpTypConstants {
	
	public static final String LIST_GRPTYP="scm/grpTyp/listGrpTyp";
	
	public static final String TABLE_GRPTYP="scm/grpTyp/tableGrpTyp";
	
	public static final String SAVE_GRPTYP="scm/grpTyp/saveGrpTyp";
	
	public static final String UPDATE_GRPTYP="scm/grpTyp/updateGrpTyp";
	
	public static final String ACTION_DONE="scm/grpTyp/done";
	
	public static final String SELECT_SUPPLY="scm/grpTyp/selectGrpTyp";
	
	public static final String SELECT_TYP="scm/grpTyp/selectTyp";
	
	public static final String ERROR_DONE = "share/errorDone";

	public static final String SELECT_TYP_MORE = "scm/grpTyp/selectTypMore";

	public static final String TABLE_TYPOTH = "scm/grpTyp/tableTypoth";//参考类别设置 2014.11.19wjf
	
	public static final String SAVE_TYPOTH = "scm/grpTyp/saveTypoth";

	public static final String UPDATE_TYPOTH = "scm/grpTyp/updateTypoth";
}
