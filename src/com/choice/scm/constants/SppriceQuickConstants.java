package com.choice.scm.constants;

public  class SppriceQuickConstants {
	public static final String LIST_SPPRICEQUICK="scm/sppriceQuick/listSppriceQuick";
	public static final String SEARCH_ALLPOSITN = "scm/sppriceQuick/searchAllPositn";
	public static final String LIST_SPPRICEBYDELIVERID = "scm/sppriceQuick/sppriceByDeliverId";
	/****************以下为 价格模板 批量添加********************************************/
	public static final String LIST_SPPRICEDEMO_QUICK="scm/sppriceDemo/listSppriceDemoQuick";
	public static final String ADD_SUPPLY_BATCH_QUICK="scm/sppriceDemo/addSupplyBatchQuick";
}
