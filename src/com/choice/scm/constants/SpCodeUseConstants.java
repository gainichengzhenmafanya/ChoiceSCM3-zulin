package com.choice.scm.constants;

public class SpCodeUseConstants {

	//页面跳转(分店用)
	public static final String LIST_SPCODEUSE="mis/spCodeUse/listSpCodeUse";//菜品点击率表
	
	public static final String REPORT_FORECAST="report/forecast/itemUseReport.jasper";//菜品点击率表
	
	public static final String LIST_STORECARGO="scm/storeCargo/listStoreCargo";//门店预估报货前三个界面
	public static final String TABLE_STORECARGO="scm/storeCargo/tableStoreCargo";//门店预估报货生成报货界面
	public static final String LIST_STORECARGO_B="scm/storeCargo/tableStoreCargo_B";//预估报货(按照菜品销售计划和菜品bom)
	public static final String LIST_STORECARGO_MIS="scm/storeCargo/tableStoreCargo_MIS";//木屋项目预估报货（按照千元用量和门店营业额预估）
	public static final String LIST_STORECARGO_ZB="scm/storeCargo/tableStoreCargo_ZB";//木屋项目总部预估报货（按照千元用量和门店营业额预估）
	public static final String LIST_STORECARGO_JGJ="scm/storeCargo/tableStoreCargo_JGJ";//木屋项目总部预估报货（按照千元用量和门店营业额预估）
	//public static final String LIST_STORECARGO_B="scm/storeCargo/tableStoreCargo_B";//木屋项目预估报货
	public static final String LIST_STORECARGOZB_B="scm/storeCargo/tableStoreCargoZb_B";//木屋项目预估报货
	public static final String TABLE_STORECARGO_CK="scm/storeCargo/tableStoreCargoCk";//门店根据出库物资预估报货生成报货界面
	public static final String UPDATE_STORECARGO="scm/storeCargo/updateStoreCargo";//生成报货单
	
}
