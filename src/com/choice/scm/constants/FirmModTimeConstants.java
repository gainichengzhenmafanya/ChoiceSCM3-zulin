package com.choice.scm.constants;

public class FirmModTimeConstants {

	//页面跳转
	public static final String LIST_FIRMMODTIME="scm/firmModTime/listFirmModTime";
	public static final String TABLE_FIRMMODTIME="scm/firmModTime/tableFirmModTime";
	
	public static final String ADD_FIRMMODTIME="scm/firmModTime/addFirmModTime";
	public static final String UPDATE_FIRMMODTIME="scm/firmModTime/updateFirmModTime";
}
