package com.choice.scm.constants;

public  class FirmSupplyConstants {
	public static final String LIST_FIRM="scm/firmSupply/listFirm";
	public static final String TABLE_SUPPLY="scm/firmSupply/tableFirmSupply";
	public static final String ADD_SUPPLY_BATCH_FIRM="scm/firmSupply/addSupplyBatchFirm";            //清除 重选方法
	public static final String ADD_SUPPLY_BATCH_FIRM_ADD="scm/firmSupply/addSupplyBatchFirmByAdd";   //新增的方法
	public static final String SELECT_POSITN="scm/firmSupply/selectOnePositn";
	//复制分店--可多选
	public static final String SELECT_POSITN_F="scm/firmSupply/selectOnePositn_f";
	//复制分店--可多选
	public static final String SELECT_POSITN_D="scm/firmSupply/selectOnePositn_d";
	
	public static final String SELECT_NSUPPLY="scm/firmSupply/selectNSupply";
	public static final String SELECT_TABLENSUPPLY="scm/firmSupply/selectTableNSupply";
	
	public static final String UPDATE = "scm/firmSupply/updatePositnSpcode";
	
	public static final String  DIVISIONSUPPLY="scm/firmSupply/divisionSupply";
	public static final String  GROUPSUPPLY="scm/firmSupply/groupSupply";
	public static final String  CLASSSUPPLY="scm/firmSupply/classSupply";
	//物料盘存表   
	public static final String LIST_FIRMSUPPLY="scm/report/listFirmSupply";
}
