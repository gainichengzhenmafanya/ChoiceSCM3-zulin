package com.choice.scm.constants;

public class PraybillmConstants {

	//页面跳转
	public static final String TABLE_PRAYBILLM="scm/praybillm/tablePraybillm";//新增，编辑请购单页面
	public static final String SEARCH_PRAYBILLM="scm/praybillm/searchPraybillm";//查找请购单页面
	public static final String REPORT_PRINT_URL="report/praybillm/praybillmReport.jasper";
	public static final String TABLE_CHECKED_PRAYBILLM="scm/praybillm/listCheckedPraybillm";
	public static final String SEARCH_CHECKED_PRAYBILLM="scm/praybillm/tableCheckedPraybillm";
	
	public static final String MIS_TABLE_CHKSTOM="mis/chkstom/tableChkstom";//MIS
	
	public static final String MIS_TABLE_CHECKED_CHKSTOM="mis/chkstom/listCheckedChkstom";//MIS
	
	public static final String MIS_SEARCH_CHKSTOM="mis/chkstom/searchChkstom";//MIS
	public static final String SEARCH_UPLOAD="scm/chkstom/searchUpload";
	public static final String MIS_SEARCH_UPLOAD="mis/chkstom/searchUpload";//MIS
	
	public static final String MIS_SEARCH_CHECKED_CHKSTOM="mis/chkstom/tableCheckedChkstom";//MIS
	
	
	public static final String TABLE_CHKSTOM_WFZ="scm/chkstom/WFZtableChkstom";//五芳斋特有
}
