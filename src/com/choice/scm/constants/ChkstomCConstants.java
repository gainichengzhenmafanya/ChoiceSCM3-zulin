package com.choice.scm.constants;

public class ChkstomCConstants {
	
	//页面跳转
	public static final String TABLE_CHKSTOM="scm/chkstom/tableChkstom";
	public static final String TABLE_CHECKED_CHKSTOM="scm/chkstom/listCheckedChkstom_c";
	public static final String SEARCH_CHKSTOM="scm/chkstom/searchChkstom";
	public static final String SEARCH_UPLOAD="scm/chkstom/searchUpload";
	public static final String SEARCH_CHECKED_CHKSTOM="scm/chkstom/tableCheckedChkstom_c";
}
