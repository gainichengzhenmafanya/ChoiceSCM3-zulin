package com.choice.scm.constants;

public class ExplanMConstants {

	//页面跳转--查询所有未审核页面
	public static final String LIST_EXPLANM="scm/explan/listExplanM";
	//添加，修改  加工单页面  主要的
	public static final String UPDATE_EXPLAN="scm/explan/tableExplan";
	//打印
//	public static final String PRINT_CHKINM="scm/explan/printChkinm"; 
	//报表地址   wangjie 2014年11月4日 09:58:15
	public static final String REPORT_URL="/report/explan/ExplanReport.jasper";
//	public static final String REPORT_URL="/report/explan/chkinmdetailquery.jasper";
	
	//加工单生成领料界面
	public static final String SAVE_CHKOUTM="scm/explan/saveChkoutm";
	//安全库存到加工计划
	public static final String SAFECNT_PLAN="scm/explan/safeCntToPlan1";
	//报货到加工计划
	public static final String PLAN_CHKSTOM="scm/explan/planExplan1";
	//报货到加工 竖直显示分店申购信息
	public static final String PLAN_CHKSTOM2="scm/explan/planExplan2";
	//报货到加工打印模板
	public static final String ORDER_TO_WORK_PRINT_MODULE="report/explan/ExplanReportM2.jasper";
	//打印PDF
	public static final String ORDER_TO_WORK_PRINTPDF_STR="printPdf";
	//加工单领料--上方页面
	public static final String LIST_EXPLANM_JG="scm/explan/listExplanMJg";
	//加工单领料--下方页面
	public static final String TABLE_EXPLANM_JG="scm/explan/tableExplanMJg";
	//添加，修改  领料单页面  主要的
	public static final String UPDATE_EXPLANLL="scm/explan/tableExplanLL";
	//添加，修改  领料单页面  主要的
	public static final String TABLE_CHKOUT="scm/explan/tableChkout";
	//领料单调制提交界面--查询领料单上界面
	public static final String LIST_EXPLANSPCODE="scm/explan/listExplanSpcode";
	//领料单调制提交界面--查询领料单下界面
	public static final String TABLE_EXPLANSPCODE="scm/explan/tableExplanSpcode";
	//领料单调制提交界面--查询领料单上界面
	public static final String LIST_REGIST="scm/explan/listRegist";
	//领料单查询界面--查询领料单上界面
	public static final String LIST_EXPLANSPCODELL="scm/explan/listExplanSpcodeLL";
	//产品入库单填制页面
	public static final String UPDATE_CHKINM = "scm/explan/updateChkinm";
	//领料单调制提交界面--查询领料单下界面
	public static final String TABLE_EXPLANSPCODELL="scm/explan/tableExplanSpcodeLL";
	
	public static final String TABLE_LISTEXPLAND="scm/explan/listExpland";
	//查询已审核加工单
	public static final String TABLE_CHECKED_EXPLANM="scm//explan/listCheckedExplanM";
	//已审核加工单单据详情
	public static final String SEARCH_CHECKED_EXPLANM="scm//explan/tableCheckedExplanM";	
}
