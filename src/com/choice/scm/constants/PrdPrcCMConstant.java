package com.choice.scm.constants;

public final class PrdPrcCMConstant {
	
	private PrdPrcCMConstant(){}
	
	public static final String ACCT = "1";  //默认帐套值
	public static final String REPORT_LIST="scm/prdPrcCM/reportList";
	//列选择：
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";

	//分店部门物资进出表明细
	public static final String BASICINFO_REPORT_COST_VARIANCE = "583,584,585,586,588,589,591,592,593,594,595,596,597,598,600,601,603,10060,10061,10062,10063,10064,10065,10066,10067,10068,10069";//默认选择列
	public static final String TABLE_NAME_COST_VARIANCE = "CostVariance";//选择表名
	public static final String BASICINFO_REPORT_COST_VARIANCE_FROZEN = "";//左侧固定列
	
	//分店部门物资进出表明细choice3 西贝用
	public static final String BASICINFO_REPORT_COSTVARIANCECHOICE3 = "581,582,583,584,585,586,587,588,589,590,598,599,600,602,604,606,608,610,612,614,616";//默认选择列
	public static final String REPORT_NAME_COSTVARIANCECHOICE3 = "MISCostVarianceChoice3";//选择表名
	public static final String BASICINFO_REPORT_COSTVARIANCECHOICE3_FROZEN = "";//左侧固定列
	public static final String LIST_COST_VARIANCECHOICE3 = "scm/prdPrcCM/listCostVarianceChoice3";//分店部门物资进出明细(成本差异)
	public static final String REPORT_PRINT_URL_COST_VARIANCECHOICE3 = "report/prdPrcCM/CostVarianceChoice3.jasper";//报表地址
	public static final String REPORT_EXP_URL_COST_VARIANCECHOICE3 = "report/prdPrcCM/CostVarianceChoice3.jasper";

	//核减明细
	public static final String BASICINFO_REPORT_HEJIAN = "602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618";//默认选择列
	public static final String TABLE_NAME_HEJIAN = "Hejian";//选择表名
	public static final String BASICINFO_REPORT_HEJIAN_FROZEN = "";//左侧固定列

	//毛利查询
	public static final String BASICINFO_REPORT_MAOLI = "691,692,693,694,695,696,697,698,699";
	public static final String TABLE_NAME_MAOLI = "maoLi";//选择表名
	public static final String BASICINFO_REPORT_MAOLI_FROZEN = "";//左侧固定列

	//毛利率周分析
	public static final String SHOW_MAOWEEKANA = "scm/prdPrcCM/maoWeekAna";//跳转页面
	public static final String SHOW_GRPWEEK = "scm/prdPrcCM/grpWeek";//跳转页面
	public static final String REPORT_MAOWEEKANA = "570,571,572,573,574,575,576,577,578,579,580,581,582";//默认选择列
	public static final String TABLE_NAME_MAOWEEKANA = "MaoliWeekAna";//选择表名
	public static final String REPORT_MAOWEEKANA_FROZEN = "";
	public static final String REPORT_SHOW_MAOWEEKANA = "scm/report/supplyDetailsInfo";
	public static final String REPORT_PRINT_URL_MAOWEEKANA = "report/profit/profitWeek.jasper";//报表地址
	public static final String REPORT_EXP_URL_MAOWEEKANA = "report/profit/profitWeek.jasper";

	//应产率分析
	public static final String SHOULDYIEDLANA = "scm/prdPrcCM/shouldYieldAna";//跳转页面
	public static final String REPORT_SHOULDYIEDLANA = "617,618,619,620,621,622,623,624,625,626,627,628,629";//默认选择列
	public static final String TABLE_NAME_SHOULDYIEDLANA = "YingChanLvAna";//选择表名
	public static final String REPORT_SHOULDYIEDLANA_FROZEN = "";
	public static final String REPORT_PRINT_URL_SHOULDYIELDANA="/report/prdPrcCM/shouldyield.jasper";//报表地址
	
	//加工间利润分析
	public static final String JGJLIRUNANA = "scm/prdPrcCM/jiaGongJianLiRunAna";//跳转页面
	public static final String REPORT_JGJLIRUNANA = "674,675,676,677,678,679,680,681,682";//默认选择列
	public static final String TABLE_NAME_JGJLIRUNANA = "jiaGongJianLiRunAna";//选择表名
	public static final String REPORT_JGJLIRUNANA_FROZEN = "";
	public static final String REPORT_PRINT_URL_JGJLIRUN="/report/profit/jgjlirun.jasper";//报表地址
	public static final String REPORT_EXP_URL_JGJLIRUN="/report/profit/jgjlirun.jasper";
	
	//产品理论成本
	public static final String JGJPRODUCTCOST = "scm/prdPrcCM/jiaGongJianTheoryCost";
	public static final String REPORT_JGJPRODUCTCOST = "830,831,832,833,834,835,836,837,838,839";//产品理论成本
	public static final String TABLE_NAME_JGJPRODUCTCOST = "JGJTheoryCost";//产品理论成本
	public static final String REPORT_PRINT_URL_EXTHEORYCOST = "report/profit/extheorycost.jasper";//打印文件
	
	//生产耗用分析
	public static final String BASICINFO_REPORT_PROCOSTANA = "630,631,632,633,635,636,638,639,641,642,644,645,646,647,648,649,650";//默认选择列
	public static final String TABLE_NAME_PROCOSTANA = "ProCostAna";//选择表名
	public static final String BASICINFO_REPORT_PROCOSTANAE_FROZEN = "630,631,632,633";//左侧固定列

	//页面跳转+报表打印
	//分店部门物资进出明细(成本差异)
	public static final String LIST_COST_VARIANCE = "scm/prdPrcCM/listCostVariance";//分店部门物资进出明细(成本差异)
	public static final String REPORT_PRINT_URL_COST_VARIANCE = "report/prdPrcCM/CostVariance.jasper";//报表地址
	public static final String REPORT_EXP_URL_COST_VARIANCE = "report/prdPrcCM/CostVariance.jasper";
	//菜品利润
	public static final String LIST_CPLR = "scm/prdPrcCM/listCplr";//菜品利润        主     
	public static final String LIST_POSITN_CPLR = "scm/prdPrcCM/listPositnCplr";//菜品利润      左侧
	public static final String TABLE_POSITN_CPLR = "scm/prdPrcCM/tablePositnCplr";//菜品利润      右侧
	public static final String TABLE_NAME_CPLR = "caiPinLiRun";//选择表名
	public static final String REPORT_CPLR = "651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673";//默认选择列
	//核减明细
	public static final String LIST_HEJIAN = "scm/prdPrcCM/hejianDetail";//分店部门物资进出明细(成本差异)--核减详细
	public static final String REPORT_PRINT_URL_HEJIAN = "report/prdPrcCM/heJian.jasper";//报表地址
	public static final String REPORT_EXP_URL_HEJIAN = "report/prdPrcCM/heJian.jasper";

	//分店耗用对比
	public static final String LIST_FIRMUSECOMPARE = "scm/prdPrcCM/firmUseCompare";//分店耗用对比
	public static final String REPORT_PRINT_URL_FIRMUSECOMPARE = "";//报表地址
	public static final String REPORT_EXP_URL_FIRMUSECOMPARE = "";

	//生产耗用分析
	public static final String LIST_PROCOSTANA = "scm/prdPrcCM/listProCostAna";//生产耗用分析
	public static final String REPORT_PRINT_URL_PROCOSTANA = "report/prdPrcCM/ProCostAna.jasper";//报表地址
	public static final String REPORT_EXP_URL_PROCOSTANA = "report/prdPrcCM/ProCostAna.jasper";
	
	//类别成本报表
	public static final String BASICINFO_REPORT_LBCBBB = "1,2,3,4,5,6,7";//默认选择列
	public static final String TABLE_NAME_LBCBBB = "LeiBieChengBen";//选择表名
	public static final String BASICINFO_REPORT_LBCBBB_FROZEN = "";//左侧固定列
	public static final String LIST_LBCBBB = "scm/prdPrcCM/leiBieChengBen";//类别成本报表

}