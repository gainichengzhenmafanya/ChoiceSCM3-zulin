package com.choice.scm.constants;

public class LinesFirmConstants {

	//页面跳转
	public static final String LIST_LINEFIRM="scm/LinesFirm/listLinesFirm";
	//右边窗口跳转
	public static final String LIST_ACCTLINEFIRM="scm/LinesFirm/listAcctLinesFirm";
	//修改到点时间跳转
	public static final String LIST_UDATETIM="scm/LinesFirm/updateTim";
	//添加配送线路
	public static final String ADD_LINEFIRM="scm/LinesFirm/addLinesFirm";
	//修改配送线路
	public static final String UPDATE_LINEFIRM="scm/LinesFirm/updateLinesFirm";

}
