package com.choice.scm.constants;

public class AnnouncementConstants {

	//页面跳转
	public static final String LIST_ANNOUNCE="scm/message/listAnnouncement";
	public static final String UPDATE_ANNOUNCE="scm/message/updateAnnouncement";
	public static final String ADD_ANNOUNCE="scm/message/addAnnouncement";

	public static final String SEE_ANNOUNCE="scm/message/showAnnouncement";
}
