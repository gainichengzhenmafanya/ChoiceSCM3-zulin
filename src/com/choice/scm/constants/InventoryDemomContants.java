package com.choice.scm.constants;
/**
 * 盘点模板页面
 * @author 孙胜彬
 */
public class InventoryDemomContants {
	//盘点模板子页面
	public static final String LIST_LISTINVENTORYDEMOD="scm/inventoryDemom/listInventoryDemom";
	//盘点模板主页面
	public static final String LIST_LISTINVENTORYDEMOM="scm/inventoryDemom/treeInventoryDemom";
	//盘点模板适用分店
	public static final String LIST_LISTINVENTORYDEMOFIRM="scm/inventoryDemom/listFirmInventoryDemom";
	//新增盘点模板页面
	public static final String ADD_LISTINVENTORYDEMOM="scm/inventoryDemom/addInventoryDemom";
	//修改盘点模板页面
	public static final String UPDATE_LISTINVENTORYDEMOM="scm/inventoryDemom/updateInventoryDemom";
	
}
