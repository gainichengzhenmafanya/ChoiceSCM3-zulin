package com.choice.scm.constants;

public final class TeleStringConstant {
	private TeleStringConstant(){}
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	//表维护
	//分店设置列选择
	public static final String BASICINFO_REPORT_FIRM = "277,278,279,280,281,282,283,284,285,286,287,288";  //默认选择列
	public static final String REPORT_NAME_FIRM = "firm";  //选择表名
	public static final String REPORT_NAME_CN_FIRM = "营业分店设置表";  //中文名
	public static final String REPORT_QUERY_METHOD_FIRM = "listFirm";
	//分店设置指标率定义
	public static final String REPORT_NAME_CN_MTHRAT = "营业分店指标率设置表";  //中文名
	public static final String REPORT_QUERY_METHOD_MTHRAT = "queryMthrat";
	//附加项定义
	public static final String BASICINFO_REPORT_REDEFINE = "427,428,429,430,431,432,433,434,435,436";  //默认选择列
	public static final String REPORT_NAME_REDEFINE = "TeleRedefine";  //选择表名
	public static final String REPORT_NAME_CN_REDEFINE = "附加项定义";  //中文名
	public static final String REPORT_QUERY_METHOD_REDEFINE = "listRedefine";
	//节假日设置列选择
	public static final String BASICINFO_REPORT_HOLIDAY = "913,914,915,916";  //默认选择列
	public static final String REPORT_NAME_HOLIDAY = "holiday";  //选择表名
	public static final String REPORT_NAME_CN_HOLIDAY = "节假日设置表";  //中文名
	public static final String REPORT_QUERY_METHOD_HOLIDAY = "listHoliday";
	//菜谱方案
	public static final String BASICINFO_REPORT_ITEMPRGM = "690,691,692,693,694,695,696,697";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGM = "TeleItemprgm";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGM = "菜谱方案管理";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGM = "listItemprgm";
	//菜谱方案-菜谱明细
//	public static final String BASICINFO_REPORT_ITEMPRGD = "698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,994";  //默认选择列,增加了厨师列，在向阳渔港库中
	public static final String BASICINFO_REPORT_ITEMPRGD = "698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGD = "TeleItemprgd";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGD = "菜谱方案明细-菜谱明细";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGD = "listItemprgd";
	//菜谱方案-大类
	public static final String BASICINFO_REPORT_ITEMPRGGRPTYP = "743,744,745,746";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGGRPTYP = "TeleItemprgGrptyp";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGGRPTYP = "菜谱方案明细-大类";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGGRPTYP = "listItemprgGrptyp";
	//菜谱方案-小类
	public static final String BASICINFO_REPORT_ITEMPRGGRP = "747,748,749,750";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGGRP = "TeleItemprgGrp";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGGRP = "菜谱方案明细-小类";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGGRP = "listItemprgGrp";
	//菜谱方案-套餐类别
	public static final String BASICINFO_REPORT_ITEMPRGPKGGRP = "751,752,753";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGPKGGRP = "TeleItemprgPkggrp";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGPKGGRP = "菜谱方案明细-套餐类别";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGPKGGRP = "listItemprgPkggrp";
	//菜谱方案-套餐
	public static final String BASICINFO_REPORT_ITEMPRGPACKAGE = "754,755,756,757,758,759,760,761,762,763,764,765,766,767";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGPACKAGE = "TeleItemprgPackage";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGPACKAGE = "菜谱方案明细-套餐";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGPACKAGE = "listItemprgPackage";
	//菜谱方案-适用分店
	public static final String BASICINFO_REPORT_ITEMPRGFIRM = "768,769,770";  //默认选择列
	public static final String REPORT_NAME_ITEMPRGFIRM = "TeleItemprgFirm";  //选择表名
	public static final String REPORT_NAME_CN_ITEMPRGFIRM = "菜谱方案明细-适用分店";  //中文名
	public static final String REPORT_QUERY_METHOD_ITEMPRGFIRM = "listItemprgFirm";
	//打印方案
	public static final String BASICINFO_REPORT_PRNITEM = "794,795,796,797,798,799,800,801,802,803,804,805,806";  //默认选择列
	public static final String REPORT_NAME_PRNITEM = "TeleFdItemPrn";  //选择表名
	public static final String REPORT_NAME_CN_PRNITEM = "分店菜谱打印机设置";  //中文名
	public static final String REPORT_QUERY_METHOD_PRNITEM = "listPrnItem";
	//打印机定义
	public static final String BASICINFO_REPORT_DEPTPRN = "808,809,810,811,812,813,814,815";  //默认选择列
	public static final String REPORT_NAME_DEPTPRN = "TeleDeptprn";  //选择表名
	public static final String REPORT_NAME_CN_DEPTPRN = "打印机定义";  //中文名
	public static final String REPORT_QUERY_METHOD_DEPTPRN = "listDeptprn";
	//支付方案
	public static final String BASICINFO_REPORT_PRGPAY = "680,681,682";  //默认选择列
	public static final String REPORT_NAME_PRGPAY = "TelePrgPay";  //选择表名
	public static final String REPORT_NAME_CN_PRGPAY = "支付方案设置";  //中文名
	public static final String REPORT_QUERY_METHOD_PRGPAY = "listPrgpay";
	//折扣方案
	public static final String BASICINFO_REPORT_DISCPRGM = "545,546,547,548,549,551,552,553";  //默认选择列
	public static final String REPORT_NAME_DISCPRGM = "TeleDiscprgm";  //选择表名
	public static final String REPORT_NAME_CN_DISCPRGM = "折扣方案管理";  //中文名
	public static final String REPORT_QUERY_METHOD_DISCPRGM = "listDiscprgm";
	//折扣方案-折扣定义
	public static final String BASICINFO_REPORT_DISCPRGD = "569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592";  //默认选择列
	public static final String REPORT_NAME_DISCPRGD = "TeleDiscprgd";  //选择表名
	public static final String REPORT_NAME_CN_DISCPRGD = "折扣方案设置";  //中文名
	public static final String REPORT_QUERY_METHOD_DISCPRGD = "listDiscprgd";
	
	/****************************************************************************************************************/
	//报表
	//map
	public static final String IREPORT_HTML = "share/ireport/mapSource/html";  //html页面
	public static final String IREPORT_EXCEL = "share/ireport/mapSource/excel";  //excel页面
	public static final String IREPORT_PDF = "share/ireport/mapSource/pdf";  //pdf页面
	public static final String IREPORT_WORD = "share/ireport/mapSource/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/mapSource/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/mapSource/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/mapSource/printWord";  //直接打印word页面
	//bean
	public static final String IREPORT_HTML_BEAN = "share/ireport/html";  //html页面
	public static final String IREPORT_EXCEL_BEAN = "share/ireport/excel";  //excel页面
	public static final String IREPORT_PDF_BEAN = "share/ireport/pdf";  //pdf页面
	public static final String IREPORT_WORD_BEAN = "share/ireport/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL_BEAN = "share/ireport/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF_BEAN = "share/ireport/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD_BEAN = "share/ireport/printWord";  //直接打印word页面
	//集团销售分析按店销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYFIRM = "1,4,5,6,7,8,9,10,11,12";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYFIRM = "GroupSalesSum";  //选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYFIRM = "按店销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYFIRM_FROZEN = "1,4";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYFIRM = "findGroupSalesSum";
	//集团销售分析按地区销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYAREA = "2,4,5,6,7,8,9,10,11,12";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYAREA = "GroupSalesSum";  //选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYAREA = "按地区销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYAREA_FROZEN = "2,4";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYAREA = "findGroupSalesSum";
	//集团销售分析按模式销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYMOD = "3,4,5,6,7,8,9,10,11,12";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYMOD = "GroupSalesSum";  //选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYMOD = "按模式销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYMOD_FROZEN = "3,4";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYMOD = "findGroupSalesSum";
	//单店营业分析_列表显示
	public static final String BASICINFO_REPORT_DDYYFX_BYINFO = "885,886,887,888,889,1032,1033,890,891,892,893,895,896,897,898,929,1030";  //默认选择列
	public static final String REPORT_NAME_DDYYFX_BYINFO = "DanDianYingYeFenXiInfo";  //选择表名
	public static final String REPORT_NAME_CN_DDYYFX_BYINFO = "单店营业分析_列表显示";  //选择表名
	public static final String BASICINFO_REPORT_DDYYFX_BYINFO_FROZEN = "885";
	public static final String REPORT_QUERY_METHOD_DDYYFX_BYINFO="queryDdyyfxByInfo";
	//单店营业分析_汇总显示
	public static final String BASICINFO_REPORT_DDYYFX_BYALL = "899,900,901,902,903,904,905,906,907,909,910,911,912,930,1031,1034,1035";  //默认选择列
	public static final String REPORT_NAME_DDYYFX_BYALL = "DanDianYingYeFenXiAll";  //选择表名
	public static final String REPORT_NAME_CN_DDYYFX_BYALL = "单店营业分析_汇总显示";  //选择表名
	public static final String BASICINFO_REPORT_DDYYFX_BYALL_FROZEN = "899";
	public static final String REPORT_QUERY_METHOD_DDYYFX_BYALL="queryDdyyfxByAll";
	//单店账单分析_表格显示
	//向阳渔港
	public static final String BASICINFO_REPORT_DDZDFX_BYTABLE_XYYG = "13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,954,955,956,957,958,959,960,961,962,963,964";  //默认选择列
	public static final String BASICINFO_REPORT_DDZDFX_BYTABLE = "13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42";  //默认选择列  西贝、豪享来
	public static final String REPORT_NAME_DDZDFX_BYTABLE = "DanDianZhangDanFenXi";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYTABLE = "单店账单分析_表格显示";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYTABLE_FROZEN = "13,14";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYTABLE="queryDdzdfx";
	//小尾羊
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYTABLE_XWY="queryDdzdfx_xwy";
	//单店账单分析_汇总报表
	public static final String BASICINFO_REPORT_DDZDFX_BYALL = "75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90";  //默认选择列
	public static final String REPORT_NAME_DDZDFX_BYALL = "DdzdfxByHuiZong";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYALL = "单店账单分析_汇总报表";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYALL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYALL = "queryDdzdfxByHuiZong";
	//单店账单分析_账单明细
	public static final String BASICINFO_REPORT_DDZDFX_BYINFO = "91,92,93,94,95,96,97,98,99,100,931";  //默认选择列
	public static final String REPORT_NAME_DDZDFX_BYINFO = "DdzdfxZdmx";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYINFO = "单店账单分析_账单明细";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYINFO_FROZEN = "91,92";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYINFO = "findZhangDanMingXi";
	//向阳渔港单店账单分析-账单明细修改版向阳渔港start----------------------------------------------------------------------------
	public static final String BASICINFO_REPORT_DDZDFX_BYINFOALL = "91,92,93,94,95,96,97,98,99,100,931";  //默认选择列
	public static final String REPORT_NAME_DDZDFX_BYINFOALL = "DdzdfxZdmx";  //选择表名
	public static final String REPORT_NAME_CN_DDZDFX_BYINFOALL = "单店账单分析_账单明细";  //选择表名
	public static final String BASICINFO_REPORT_DDZDFX_BYINFOALL_FROZEN = "91,92";
	public static final String REPORT_QUERY_METHOD_DDZDFX_BYINFOALL = "findZhangDanMingXiAll";
	//向阳渔港单店账单分析-账单明细修改版向阳渔港end----------------------------------------------------------------------------
	//单店收银统计_收银方式统计
	public static final String BASICINFO_REPORT_DDSYFSTJ = "125,126,127";  //默认选择列
	public static final String REPORT_NAME_DDSYFSTJ = "DanDianShouYinTongJi";  //选择表名
	public static final String REPORT_NAME_CN_DDSYFSTJ = "单店收银统计_收银方式统计";  //选择表名
	public static final String BASICINFO_REPORT_DDSYFSTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDSYFSTJ = "queryDdsyfstj";
	//单店收银统计_结算方式明细
	public static final String BASICINFO_REPORT_DDSYJSINFO = "128,129,130,131";  //默认选择列
	public static final String REPORT_NAME_DDSYJSINFO = "DanDianShouYinTongJiMx";  //选择表名
	public static final String REPORT_NAME_CN_DDSYJSINFO = "单店收银统计_结算方式明细";  //选择表名
	public static final String BASICINFO_REPORT_DDSYJSINFO_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDSYJSINFO = "queryDdsyjsinfo";
	//单店销售统计_菜类销售统计
	public static final String BASICINFO_REPORT_DDXSTJBYCLTJ = "173,174,175,176,177,178,179,180,181,182";  //默认选择列
	public static final String REPORT_NAME_DDXSTJBYCLTJ = "DdxstjByCltj";  //选择表名
	public static final String REPORT_NAME_CN_DDXSTJBYCLTJ = "单店销售统计_菜类销售统计";  //选择表名
	public static final String BASICINFO_REPORT_DDXSTJBYCLTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDXSTJBYCLTJ = "queryDdxstjbycltj";
	//单店销售统计_菜品销售统计
	public static final String BASICINFO_REPORT_DDXSTJBYCPTJ = "183,184,185,186,187,188,189,190,191,192";  //默认选择列
	public static final String REPORT_NAME_DDXSTJBYCPTJ = "DdxstjByCptj";  //选择表名
	public static final String REPORT_NAME_CN_DDXSTJBYCPTJ = "查询单店销售统计_菜品销售统计";  //选择表名
	public static final String BASICINFO_REPORT_DDXSTJBYCPTJ_FROZEN = "183,184";
	public static final String REPORT_QUERY_METHOD_DDXSTJBYCPTJ = "queryDdxstjbycptj";
	//单店销售统计_菜品销售明细
	public static final String BASICINFO_REPORT_DDXSTJBYCPMX = "193,194,195,196,197,198,199,200,201,202,203";  //默认选择列
	public static final String REPORT_NAME_DDXSTJBYCPMX = "DdxstjByCpmx";  //选择表名
	public static final String REPORT_NAME_CN_DDXSTJBYCPMX = "单店销售统计_菜品销售明细";  //选择表名
	public static final String BASICINFO_REPORT_DDXSTJBYCPMX_FROZEN = "193,194,195";
	public static final String REPORT_QUERY_METHOD_DDXSTJBYCPMX = "queryDdxstjByCpmx";
	//单店销售统计_菜品销售明细来源--向阳渔港start---------------------------------------------------
	public static final String BASICINFO_REPORT_DDXSTJBYCPMXLY = "975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992";  //默认选择列
	public static final String REPORT_NAME_DDXSTJBYCPMXLY = "DdxstjByCpmxLy";  //选择表名
	public static final String REPORT_NAME_CN_DDXSTJBYCPMXLY = "单店销售统计_菜品销售明细来源";  //选择表名
	public static final String BASICINFO_REPORT_DDXSTJBYCPMXLY_FROZEN = "975,976";
	public static final String REPORT_QUERY_METHOD_DDXSTJBYCPMXLY = "queryDdxstjByCpmxly";
	//单店销售统计_菜品销售明细来源--向阳渔港end--------------------------------------
	//单店营业时间分析
	public static final String BASICINFO_REPORT_DDYYSJFX = "214,215,216,217,218,219,220,221";  //默认选择列
	public static final String REPORT_NAME_DDYYSJFX = "Ddyysjfx";  //选择表名
	public static final String REPORT_NAME_CN_DDYYSJFX = "单店营业时间分析";  //选择表名
	public static final String BASICINFO_REPORT_DDYYSJFX_FROZEN = "214";
	public static final String REPORT_QUERY_METHOD_DDYYSJFX = "queryDdyysjfx";
	//单店预收款汇总 
	public static final String BASICINFO_REPORT_DDYSKHZ = "222,223,224,225,226,227,228,229,230,231";  //默认选择列
	public static final String REPORT_NAME_DDYSKHZ = "Ddyskhz";  //选择表名
	public static final String REPORT_NAME_CN_DDYSKHZ = "单店预收款汇总";  //选择表名
	public static final String BASICINFO_REPORT_DDYSKHZ_FROZEN = "222,223";
	public static final String REPORT_QUERY_METHOD_DDYSKHZ = "queryDdyskhz";
	//单店反结算分析
	public static final String BASICINFO_REPORT_DDFJSFX = "248,249,250,251,252,253,254,255,256,257";  //默认选择列
	public static final String REPORT_NAME_DDFJSFX = "Ddfjsfx";  //选择表名
	public static final String REPORT_NAME_CN_DDFJSFX = "单店反结算分析";  //选择表名
	public static final String BASICINFO_REPORT_DDFJSFX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDFJSFX = "queryDdfjsfx";
	//单店反结算分析明细
	public static final String BASICINFO_REPORT_DDFJSFXBYMX = "258,259,260,261,262";  //默认选择列
	public static final String REPORT_NAME_DDFJSFXBYMX = "DdfjsfxBymx";  //选择表名
	public static final String REPORT_NAME_CN_DDFJSFXBYMX = "单店反结算分析明细";  //选择表名
	public static final String BASICINFO_REPORT_DDFJSFXBYMX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DDFJSFXBYMX = "findDdfjsmx";
	//员工业绩查询_点菜统计--向阳渔港start---------------------------------------------------------------------
	public static final String BASICINFO_REPORT_YGYJCXDCTJ = "939,940,941,942,943";  //默认选择列
	public static final String REPORT_NAME_YGYJCXDCTJ = "YgyjcxDctj";  //选择表名
	public static final String REPORT_NAME_CN_YGYJCXDCTJ = "员工业绩查询点菜统计";  //选择表名
	public static final String BASICINFO_REPORT_YGYJCXDCTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_YGYJCXDCTJ = "queryYgyjcxDctj";
	//	员工业绩查询_菜品统计
	public static final String BASICINFO_REPORT_YGYJCXCPTJ = "965,966,967,968,969";  //默认选择列
	public static final String REPORT_NAME_YGYJCXCPTJ = "YgyjcxCptj";  //选择表名
	public static final String REPORT_NAME_CN_YGYJCXCPTJ = "员工业绩查询菜品统计";  //选择表名
	public static final String BASICINFO_REPORT_YGYJCXCPTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_YGYJCXCPTJ = "queryYgyjcxCptj";
	//	员工业绩查询_菜类统计
	public static final String BASICINFO_REPORT_YGYJCXCLTJ = "970,971,972,973,974";  //默认选择列
	public static final String REPORT_NAME_YGYJCXCLTJ = "YgyjcxCltj";  //选择表名
	public static final String REPORT_NAME_CN_YGYJCXCLTJ = "员工业绩查询菜类统计";  //选择表名
	public static final String BASICINFO_REPORT_YGYJCXCLTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_YGYJCXCLTJ = "queryYgyjcxCltj";
	//员工业绩查询_点菜统计--向阳渔港end---------------------------------------------------------------------

	//质量分析-集团营业损失分析
	public static final String BASICINFO_REPORT_GROUPLOSSSUM = "43,44,45,46,47,48,49";  //默认选择列
	public static final String REPORT_NAME_GROUPLOSSSUM = "GroupLossSum";  //选择表名
	public static final String REPORT_NAME_CN_GROUPLOSSSUM = "营业损失分析明细表";  //选择表名
	public static final String BASICINFO_REPORT_GROUPLOSSSUM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_GROUPLOSSSUM = "findGroupLossSum";

	public static final String BASICINFO_REPORT_LOSS_SUM_BYDAY = "43,44,45,46,47,48,49";  //默认选择列
	public static final String REPORT_NAME_LOSS_SUM_BYDAY = "GroupLossSum";  //选择表名
	public static final String REPORT_NAME_CN_LOSS_SUM_BYDAY = "营业损失分析表每日明细";  //选择表名
	public static final String BASICINFO_REPORT_LOSS_SUM_BYDAY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_LOSS_SUM_BYDAY = "findGroupLossSum";
	public static final String BASICINFO_REPORT_LOSS_SUM_TTL = "43,45,46,47,48,49";  //默认选择列
	public static final String REPORT_NAME_LOSS_SUM_TTL = "GroupLossSum";  //选择表名
	public static final String REPORT_NAME_CN_LOSS_SUM_TTL = "营业损失分析表按店总计";  //选择表名
	public static final String BASICINFO_REPORT_LOSS_SUM_TTL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_LOSS_SUM_TTL = "findGroupLossSum";
	//集团消退菜分析_单店退菜明细--start----------------------------------------========================
	//集团消退菜分析
	public static final String REPORT_NAME_GROUPCRDISHES_SUM = "GroupCRDishesSum";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHES_SUM = "集团消退菜分析表";  //选择表名
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHES_SUM = "findGroupCRDishes";

//	集团消退菜分析_单店退菜明细-向阳渔港
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM = "932,933,934,935,936,937,938";  //默认选择列
	public static final String REPORT_NAME_GROUPCRDISHES_FIRM = "groupcrdishes";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHES_FIRM = "集团消退菜分析_单店退菜明细";  //选择表名
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHES_FIRM = "findGroupCRDishesByFirm";
//	集团消退菜分析_单店退菜明细-小尾羊
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM_XWY = "951,952,953,954,955,956,957";  //默认选择列
	public static final String REPORT_NAME_GROUPCRDISHES_FIRM_XWY = "groupcrdishes";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHES_FIRM_XWY = "集团消退菜分析_单店退菜明细";  //选择表名
	public static final String BASICINFO_REPORT_GROUPCRDISHES_FIRM_XWY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHES_FIRM_XWY = "findGroupCRDishesByFirm";
	
	//集团消退菜分析_单店退菜明细--end----------------------------------------========================

	//废弃菜品统计-西贝
	public static final String BASICINFO_REPORT_FQCPTJ = "951,952,953,954,955,956,957,958";  //默认选择列
	public static final String REPORT_NAME_FQCPTJ = "MdcpfqtzFqcptj";  //选择表名
	public static final String REPORT_NAME_CN_FQCPTJ="废弃菜品统计";  //选择表名
	public static final String BASICINFO_REPORT_FQCPTJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_FQCPTJ= "queryFqcptj";
	//废弃菜品明细-西贝
	public static final String BASICINFO_REPORT_FQCPMX = "959,960,961,962,963,964,965,966,967";  //默认选择列
	public static final String REPORT_NAME_FQCPMX = "MdcpfqtzFqcpmx";  //选择表名
	public static final String REPORT_NAME_CN_FQCPMX="废弃菜品明细";  //选择表名
	public static final String BASICINFO_REPORT_FQCPMX_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_FQCPMX= "queryFqcpmx";

	//集团消退菜分析_西贝退菜明细
	public static final String BASICINFO_REPORT_GROUPCRDISHESXB_FIRM = "968,969,970,971";  //默认选择列
	public static final String REPORT_NAME_GROUPCRDISHESXB_FIRM = "Xiaotuicaipintongji";  //选择表名
	public static final String REPORT_NAME_CN_GROUPCRDISHESXB_FIRM = "消退菜品统计";  //选择表名
	public static final String BASICINFO_REPORT_GROUPCRDISHESXB_FIRM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_GROUPCRDISHESXB_FIRM = "findGroupCRDishesByFirm";
	
	
	public static final String BASICINFO_REPORT_DISCOUNT_BILL = "879,101,102,103,104,105,106,107,108,109,110,111";  //默认选择列
	public static final String REPORT_NAME_DISCOUNT_BILL = "ShopBillDiscount";  //选择表名
	public static final String REPORT_NAME_CN_DISCOUNT_BILL = "账单折扣表";  //选择表名
	public static final String BASICINFO_REPORT_DISCOUNT_BILL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DISCOUNT_BILL = "findShopBillDiscount";
	
	public static final String BASICINFO_REPORT_DISCOUNT_DISH = "880,112,113,114,115,116,117,118,119,120,121";  //默认选择列
	public static final String REPORT_NAME_DISCOUNT_DISH = "ShopDishDiscount";  //选择表名
	public static final String REPORT_NAME_CN_DISCOUNT_DISH = "菜品折扣表";  //选择表名
	public static final String BASICINFO_REPORT_DISCOUNT_DISH_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DISCOUNT_DISH = "findShopDishDiscount";
	
	public static final String BASICINFO_REPORT_DISCOUNT_TOTAL = "124,881,122,123,948";  //默认选择列
	public static final String REPORT_NAME_DISCOUNT_TOTAL = "ShopTotalDiscount";  //选择表名
	public static final String REPORT_NAME_CN_DISCOUNT_TOTAL = "账单折扣汇总表";  //选择表名
	public static final String BASICINFO_REPORT_DISCOUNT_TOTAL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DISCOUNT_TOTAL = "findShopTotalDiscount";
	
	public static final String BASICINFO_REPORT_DISCOUNT_DISHTOTAL = "882,883,884";  //默认选择列
	public static final String REPORT_NAME_DISCOUNT_DISHTOTAL = "ShopTotalDiscountDish";  //选择表名
	public static final String REPORT_NAME_CN_DISCOUNT_DISHTOTAL = "菜品折扣汇总表";  //选择表名
	public static final String BASICINFO_REPORT_DISCOUNT_DISHTOTAL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DISCOUNT_DISHTOTAL = "findShopDishDiscountTotal";

	
	//集团销售分析按菜销售汇总

	public static final String BASICINFO_REPORT_SALES_SUM_BYFOOD = "61,62,63,64,65,66,67,68,69,70,71,72,73,74";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYFOOD = "GroupSalesSumByFood";  //选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYFOOD = "按菜销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYFOOD_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYFOOD = "findGroupSalesSum";
	//集团销售分析按类销售汇总
	public static final String BASICINFO_REPORT_SALES_SUM_BYCATEGORY = "50,51,52,53,54,55,56,57,58,59,60";  //默认选择列
	public static final String REPORT_NAME_SALES_SUM_BYCATEGORY = "GroupSalesSumByCate";  //选择表名
	public static final String REPORT_NAME_CN_SALES_SUM_BYCATEGORY = "按类销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_SUM_BYCATEGORY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALES_SUM_BYCATEGORY = "findGroupSalesSum";
	//集团销售分析按店销售明细
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYFIRM = "132,133,134,135,136,137,138,139,140,141,142,143";  //默认选择列
	public static final String REPORT_NAME_SALES_DETAIL_BYFIRM = "GroupSalesDetailByFirm";  //选择表名
	public static final String REPORT_NAME_CN_SALES_DETAIL_BYFIRM = "按店销售明细表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYFIRM_FROZEN = "132,133,134";
	public static final String REPORT_QUERY_METHOD_SALES_DETAIL_BYFIRM = "findGroupSalesDetail";
	//集团销售分析按菜销售明细
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYFOOD = "144,145,146,147,148,149,150,151,152,153,154,155,156,157,158";  //默认选择列
	public static final String REPORT_NAME_SALES_DETAIL_BYFOOD = "GroupSalesDetailByFood";  //选择表名
	public static final String REPORT_NAME_CN_SALES_DETAIL_BYFOOD = "按菜销售明细表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYFOOD_FROZEN = "144,145,146";
	public static final String REPORT_QUERY_METHOD_SALES_DETAIL_BYFOOD = "findGroupSalesDetail";
	//集团销售分析按天销售明细
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYDAY = "159,160,161,162,163,164,165,166,167,168,169,170";  //默认选择列
	public static final String REPORT_NAME_SALES_DETAIL_BYDAY = "GroupSalesDetailByDay";  //选择表名
	public static final String REPORT_NAME_CN_SALES_DETAIL_BYDAY = "按天销售明细表";  //选择表名
	public static final String BASICINFO_REPORT_SALES_DETAIL_BYDAY_FROZEN = "159,160,161";
	public static final String REPORT_QUERY_METHOD_SALES_DETAIL_BYDAY = "findGroupSalesDetail";
	//集团销售分析按日期销售汇总
	public static final String BASICINFO_REPORT_GROUPSALES_SUM_BYDATE = "204,205,206,207,208,209,210,211,212,213";  //默认选择列
	public static final String REPORT_NAME_GROUPSALES_SUM_BYDATE = "GroupSalesDetailByDate";  //选择表名
	public static final String REPORT_NAME_CN_GROUPSALES_SUM_BYDATE = "按时间段销售汇总表";  //选择表名
	public static final String BASICINFO_REPORT_GROUPSALES_SUM_BYDATE_FROZEN = "204";
	public static final String REPORT_QUERY_METHOD_GROUPSALES_SUM_BYDATE = "findGroupSalesSumByDate";
	//集团菜品销售分析集团汇总
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUJITUAN = "232,233,234,235,236";  //默认选择列
	public static final String REPORT_NAME_CAIPINXIAOSHOUJITUAN = "CaiPinXiaoShouJiTuan";  //选择表名
	public static final String REPORT_NAME_CN_CAIPINXIAOSHOUJITUAN = "集团汇总报表";  //选择表名
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUJITUAN_FROZEN = "232,233";
	public static final String REPORT_QUERY_METHOD_CAIPINXIAOSHOUJITUAN = "findJiTuanCaiPinXiaoShou";
	//集团菜品销售分析分店汇总
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUFENDIAN = "237,238,239,240,241";  //默认选择列
	public static final String REPORT_NAME_CAIPINXIAOSHOUFENDIAN = "CaiPinXiaoShouFenDian";  //选择表名
	public static final String REPORT_NAME_CN_CAIPINXIAOSHOUFENDIAN = "分店汇总报表";  //选择表名
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUFENDIAN_FROZEN = "237,238";
	public static final String REPORT_QUERY_METHOD_CAIPINXIAOSHOUFENDIAN = "findJiTuanCaiPinXiaoShou";
	//集团菜品销售分析分店明细
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUFENDIANMINGXI = "242,243,244,245,246,247";  //默认选择列
	public static final String REPORT_NAME_CAIPINXIAOSHOUFENDIANMINGXI = "CaiPinXiaoShouFenDianMingXi";  //选择表名
	public static final String REPORT_NAME_CN_CAIPINXIAOSHOUFENDIANMINGXI = "分店明细报表";  //选择表名
	public static final String BASICINFO_REPORT_CAIPINXIAOSHOUFENDIANMINGXI_FROZEN = "242,243,244";
	public static final String REPORT_QUERY_METHOD_CAIPINXIAOSHOUFENDIANMINGXI = "findJiTuanCaiPinXiaoShou";
	//集团销售对比分析
	public static final String BASICINFO_REPORT_JITUANXIAOSHOUDUIBI = "263,264,265,266,267,268,269,270,271,272,273,274,275,276";  //默认选择列
	public static final String REPORT_NAME_JITUANXIAOSHOUDUIBI = "JiTuanXiaoShouDuiBi";  //选择表名
	public static final String REPORT_NAME_CN_JITUANXIAOSHOUDUIBI = "集团销售对比分析";  //选择表名
	public static final String BASICINFO_REPORT_JITUANXIAOSHOUDUIBI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JITUANXIAOSHOUDUIBI = "findJiTuanXiaoShouDuiBi";
	//集团优免项比重分析
	public static final String REPORT_NAME_CN_JITUANYOUMIANXIANGBIZHONG = "集团优免项比重分析";  //选择表名
	public static final String BASICINFO_REPORT_JITUANYOUMIANXIANGBIZHONG_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JITUANYOUMIANXIANGBIZHONG = "findYouMianXiangBiZhong";
	//集团营业显示分析
	public static final String REPORT_NAME_CN_GROUPBUSINESSSHOW = "集团营业显示分析";  //选择表名
	public static final String REPORT_QUERY_METHOD_GROUPBUSINESSSHOW = "findGroupBusinessShow";
	//集团收入分析（按店汇总）
	public static final String BASICINFO_REPORT_GROUPINCOME_BYFRIM = "289,290,291,292,293,294,295,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312";  //默认选择列
	public static final String BASICINFO_REPORT_GROUPINCOME_BYFRIM_HXL = "289,290,291,292,293,294,295,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,1035";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYFRIM = "groupIncome_ByFrim";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYFRIM = "集团收入分析";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYFRIM_FROZEN = "289";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYFRIM = "findGroupIncome";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYFRIM_XWY = "findGroupIncomexwy";
	//集团收入分析（按段汇总）
//	public static final String BASICINFO_REPORT_GROUPINCOME_BYDATE = "319,320,321,322,323,324,325,326,327,328,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345";  //默认选择列
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDATE = "319,321,323,324,325,326,327,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYDATE = "groupIncome_ByDate";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYDATE = "集团收入分析-按段汇总";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDATE_FROZEN = "319";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDATE = "findGroupIncome";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDATE_XWY = "findGroupIncomexwy";
	//集团收入分析（按日明细）
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDAY = "352,353,354,355,356,357,358,359,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYDAY = "groupIncome_ByDay";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYDAY = "集团收入分析-按日明细";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDAY_FROZEN = "352";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDAY = "findGroupIncome";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDAY_XWY = "findGroupIncomexwy";
	//集团收入分析（同期对比）
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDIFF = "383,384,385,386,387,388,389,390,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407";  //默认选择列
	public static final String REPORT_NAME_GROUPINCOME_BYDIFF = "groupIncome_ByDiff";  //选择表名
	public static final String REPORT_NAME_CN_GROUPINCOME_BYDIFF = "集团收入分析-同期对比";  //选择表名
	public static final String BASICINFO_REPORT_GROUPINCOME_BYDIFF_FROZEN = "383";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDIFF = "findGroupIncome";
	public static final String REPORT_QUERY_METHOD_GROUPINCOME_BYDIFF_XWY = "findGroupIncomexwy";
	
	//集团消费额度区间分析
	//xyyg、西贝
	public static final String BASICINFO_REPORT_EDUQUJIAN = "630,631,632,633,634,635,636,637,638,639,640,641";  //默认选择列
	public static final String REPORT_NAME_EDUQUJIAN = "EDuQuJian";  //选择表名
	public static final String REPORT_NAME_CN_EDUQUJIAN = "集团消费额度区间分析";  //选择表名
	public static final String BASICINFO_REPORT_EDUQUJIAN_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_EDUQUJIAN = "findEDuQuJian";
	//hxl
	public static final String BASICINFO_REPORT_EDUQUJIANHXL = "932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947";  //默认选择列
	public static final String REPORT_NAME_EDUQUJIANHXL = "EDuQuJianhxl";  //默认选择列
	public static final String REPORT_NAME_CN_EDUQUJIANHXL = "集团消费额度区间分析";  //选择表名
	public static final String BASICINFO_REPORT_EDUQUJIANHXL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_EDUQUJIANHXL = "findEDuQuJian";
	//hns
	public static final String BASICINFO_REPORT_EDUQUJIANHNS = "951,952";  //默认选择列
	public static final String REPORT_NAME_EDUQUJIANHNS = "EDuQuJianhns";  //默认选择列
	public static final String REPORT_NAME_CN_EDUQUJIANHNS = "集团消费额度区间分析";  //选择表名
	public static final String BASICINFO_REPORT_EDUQUJIANHNS_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_EDUQUJIANHNS = "findEDuQuJian_hns";
	//GZJJ
	public static final String BASICINFO_REPORT_EDUQUJIANGZJJ = "1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060";  //默认选择列
	public static final String REPORT_NAME_EDUQUJIANGZJJ = "EDuQuJiangzjj";  //默认选择列
	public static final String REPORT_NAME_CN_EDUQUJIANGZJJ = "集团消费额度区间分析";  //选择表名
	public static final String BASICINFO_REPORT_EDUQUJIANGZJJ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_EDUQUJIANGZJJ = "findEDuQuJian";
	
	//集团菜品出错率分析
	public static final String BASICINFO_REPORT_JITUANDIANCAICHUCUOLV = "862,863,864,865,866,867,868";  //默认选择列
	public static final String REPORT_NAME_JITUANDIANCAICHUCUOLV = "CaiPinChuCuoLv";  //选择表名
	public static final String REPORT_NAME_CN_JITUANDIANCAICHUCUOLV = "集团菜品出错率分析";  //选择表名
	public static final String BASICINFO_REPORT_JITUANDIANCAICHUCUOLV_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_JITUANDIANCAICHUCUOLV = "findCaiPinChuCuoLv";
	
	//集团菜品上台率分析
	public static final String BASICINFO_REPORT_CAIPINSHANGTAILV = "917,918,920,921,922,923,924,926,927,928";  //默认选择列
	public static final String REPORT_NAME_CAIPINSHANGTAILV = "CaiPinShangTaiLv";  //选择表名
	public static final String REPORT_NAME_CN_CAIPINSHANGTAILV = "集团菜品上台率分析";  //选择表名
	public static final String BASICINFO_REPORT_CAIPINSHANGTAILV_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_CAIPINSHANGTAILV = "findCaiPinShangTaiLv";
	
	//集团营业分析
	public static final String REPORT_NAME_BUSINESSANALYSIS = "";  //选择表名
	public static final String REPORT_NAME_CN_BUSINESSANALYSIS = "集团营业分析";  //选择表名
	public static final String  REPORT_QUERY_METHOD_BUSINESSANALYSIS = "findBusinessAnalysis";
	
	public static final String REPORT_NAME_BUSINESSANALYSIS_XWY = "";  //选择表名
	public static final String REPORT_NAME_CN_BUSINESSANALYSIS_XWY = "集团营业分析";  //选择表名
	public static final String  REPORT_QUERY_METHOD_BUSINESSANALYSIS_XWY = "findBusinessAnalysisXwy";
//	
	//销售分析
	//营业日报综合分析
	public static final String REPORT_NAME_CN_BUSINESSDAYREPORT = "营业日报综合分析";  //选择表名
	public static final String REPORT_QUERY_METHOD_BUSINESSDAYREPORT = "findDayreport";
	//销售分析-分店销售综合分析
	public static final String BASICINFO_REPORT_BRANCHSALES = "839,840,841,842,843,844,845,846,847,848,849,850";  //默认选择列
	public static final String REPORT_NAME_BRANCHSALES = "branchSales";  //选择表名
	public static final String REPORT_NAME_CN_BRANCHSALES = "分店销售综合分析";  //选择表名
	public static final String BASICINFO_REPORT_BRANCHSALES_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_BRANCHSALES = "findBranchSales";
	//销售分析-部门销售综合分析
	public static final String BASICINFO_REPORT_DEPARTMENTSALES = "851,852,853,854,855,856,857,858,859,860,861";  //默认选择列
	public static final String REPORT_NAME_DEPARTMENTSALES = "departmentSales";  //选择表名
	public static final String REPORT_NAME_CN_DEPARTMENTSALES = "部门销售综合分析";  //选择表名
	public static final String BASICINFO_REPORT_DEPARTMENTSALES_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DEPARTMENTSALES = "findDepartmentSales";
	//销售分析-台位人数综合分析
	public static final String BASICINFO_REPORT_TABLECUSTOMERNO_SUM = "415,416,417,418,419,420,421,422,423,424,425,426";  //默认选择列
	public static final String REPORT_NAME_TABLECUSTOMERNO_SUM = "TableCustomerNo";  //选择表名
	public static final String REPORT_NAME_CN_TABLECUSTOMERNO_SUM = "台位人数综合分析";  //选择表名
	public static final String BASICINFO_REPORT_TABLECUSTOMERNO_SUM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_TABLECUSTOMERNO_SUM = "findTableCustomerNo";
	//销售分析-销售比重综合分析
	public static final String REPORT_NAME_CN_SALESPRO_DALEI = "销售比重大类分析";  //大类
	public static final String REPORT_QUERY_METHOD_SALESPRO_DALEI  = "queryXsbzzhfx";
	public static final String REPORT_NAME_CN_SALESPRO_XIAOLEI = "销售比重小类分析";  //小类
	public static final String REPORT_QUERY_METHOD_SALESPRO_XIAOLEI  = "queryXsbzzhfx";
	public static final String REPORT_NAME_CN_SALESPRO_BUMEN = "销售比重部门分析";  //部门
	public static final String REPORT_QUERY_METHOD_SALESPRO_BUMEN  = "queryXsbzzhfx";
	public static final String REPORT_NAME_CN_SALESPRO_FUZHULB = "销售比重辅助类别分析";  //辅助类别
	public static final String REPORT_QUERY_METHOD_SALESPRO_FUZHULB  = "queryXsbzzhfx";
	public static final String REPORT_NAME_CN_SALESPRO_ERJILB = "销售比重辅助类别分析";  //二级类别
	public static final String REPORT_QUERY_METHOD_SALESPRO_ERJILB  = "queryXsbzzhfx";
	//销售分析-菜价差异综合分析
	public static final String BASICINFO_REPORT_PRICEDIFFERENCES_SUM = "554,555,556,557,558,559,560";  //默认选择列
	public static final String REPORT_NAME_PRICEDIFFERENCES_SUM = "PriceDifferences";  //选择表名
	public static final String REPORT_NAME_CN_PRICEDIFFERENCES_SUM = "菜价差异综合分析";  //选择表名
	public static final String BASICINFO_REPORT_PRICEDIFFERENCES_SUM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PRICEDIFFERENCES_SUM  = "queryCjcyzhfx";

	//销售分析——套餐销售统计--销售统计
	public static final String BASICINFO_REPORT_SALETONGJI = "648,650,651,652,653,655";  //默认选择列
	public static final String REPORT_NAME_SALETONGJI = "saleTongJi";  //选择表名
	public static final String REPORT_NAME_CN_SALETONGJI = "销售统计表";  //选择表名
	public static final String BASICINFO_REPORT_SALETONGJI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_SALETONGJI = "queryXstjb";

	//销售分析——套餐销售统计--明细统计
	public static final String BASICINFO_REPORT_DETAILTONGJI = "643,644,645,646";  //默认选择列
	public static final String REPORT_NAME_DETAILTONGJI = "detailTongJi";  //选择表名
	public static final String REPORT_NAME_CN_DETAILTONGJI = "明细统计表";  //选择表名
	public static final String BASICINFO_REPORT_DETAILTONGJI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DETAILTONGJI = "queryMxtjb";
	
	

	
	//销售分析——菜品上台率综合分析
	public static final String BASICINFO_REPORT_COSTORDERRATE_SUM = "671,672,673,674,675,676,677";  //默认选择列
	public static final String REPORT_NAME_COSTORDERRATE_SUM = "costOrderRate";  //选择表名
	public static final String REPORT_NAME_CN_COSTORDERRATE_SUM = "菜品上台率综合分析";  //选择表名
	public static final String BASICINFO_REPORT_COSTORDERRATE_SUM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_COSTORDERRATE_SUM = "queryCostOrderRateSumAna";
	
	//销售分析——菜品时段销售综合分析
	public static final String BASICINFO_REPORT_COSTTIMECNT_SUM = "678,679";  //默认选择列
	public static final String REPORT_NAME_COSTTIMECNT_SUM = "costTimeCnt";  //选择表名
	public static final String REPORT_NAME_CN_COSTTIMECNT_SUM = "菜品时段销售综合分析";  //选择表名
	public static final String BASICINFO_REPORT_COSTTIMECNT_SUM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_COSTTIMECNT_SUM = "queryCostTimeCntSumAna";
	
	//销售分析——类别菜品综合统计--按区域类别汇总分析
	public static final String BASICINFO_REPORT_QUYULEIBIEHUIZONGFENXI = "663,664,665,666,667,668,669,670";  //默认选择列
	public static final String REPORT_NAME_QUYULEIBIEHUIZONGFENXI = "quYuLeiBieHuiZongFenXi";  //选择表名
	public static final String REPORT_NAME_CN_QUYULEIBIEHUIZONGFENXI = "按区域类别汇总分析";  //选择表名
	public static final String BASICINFO_REPORT_QUYULEIBIEHUIZONGFENXI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_QUYULEIBIEHUIZONGFENXI = "queryQylbfx";
	
	//销售分析——类别菜品综合统计--按类别汇总分析
	public static final String BASICINFO_REPORT_LEIBIEHUIZONGFENXI = "656,657,658,659,660,661,662";  //默认选择列
	public static final String REPORT_NAME_LEIBIEHUIZONGFENXI = "leiBieHuiZongFenXi";  //选择表名
	public static final String REPORT_NAME_CN_LEIBIEHUIZONGFENXI = "按类别汇总分析";  //选择表名
	public static final String BASICINFO_REPORT_LEIBIEHUIZONGFENXI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_LEIBIEHUIZONGFENXI = "queryLbcpzhfx";
	
	//销售分析——菜价差异综合分析
	public static final String BASICINFO_REPORT_CAIJIACHAYIZONGHEFENXI = "683,684,685,686,687,688,689";  //默认选择列
	public static final String REPORT_NAME_CAIJIACHAYIZONGHEFENXI = "caiJiaChaYiZongHeFenXi";  //选择表名
	public static final String REPORT_NAME_CN_CAIJIACHAYIZONGHEFENXI = "菜价差异综合分析";  //选择表名
	public static final String BASICINFO_REPORT_CAIJIACHAYIZONGHEFENXI_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_CAIJIACHAYIZONGHEFENXI = "queryCjcyzhfx";
	
	//销售分析——经营对比综合分析汇总
	public static final String BASICINFO_REPORT_OPERATINGCONTRAST = "";  //默认选择列
	public static final String REPORT_NAME_OPERATINGCONTRAST = "";  //选择表名
	public static final String REPORT_NAME_CN_OPERATINGCONTRAST = "经营对比综合分析汇总";  //选择表名
	public static final String BASICINFO_REPORT_OPERATINGCONTRAST_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_OPERATINGCONTRAST = "findOperatingContrastHz";
	//销售分析——经营对比综合分析明细
	public static final String BASICINFO_REPORT_OPERATINGCONTRASTBYFIRM = "";  //默认选择列
	public static final String REPORT_NAME_OPERATINGCONTRASTBYFIRM = "";  //选择表名
	public static final String REPORT_NAME_CN_OPERATINGCONTRASTBYFIRM = "经营对比综合分析明细";  //选择表名
	public static final String BASICINFO_REPORT_OPERATINGCONTRASTBYFIRM_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_OPERATINGCONTRASTBYFIRM = "findOperatingContrast";
	
	//销售分析——营业客流分析--明细
	public static final String BASICINFO_REPORT_OPERATPASSENGER = "614,616,617,618,619,620,621,622,623,624,625,626,627,628,629";  //默认选择列
	public static final String REPORT_NAME_OPERATPASSENGER = "OperatPassenger";  //选择表名
	public static final String REPORT_NAME_CN_OPERATPASSENGER = "营业客流分析--明细";  //中文名
	public static final String BASICINFO_REPORT_OPERATPASSENGER_FROZEN = "614,616,617";
	public static final String REPORT_QUERY_METHOD_OPERATPASSENGER = "queryYyklzhfxMx";
	//销售分析——营业客流分析--汇总
	public static final String BASICINFO_REPORT_OPERATPASSENGERHZ_HXL = "869,871,872,873,874,875,876,877,949,950";  //默认选择列   豪享来
	public static final String BASICINFO_REPORT_OPERATPASSENGERHZ = "869,871,872,873,874,876,877,950";  //默认选择列  西贝、向阳渔港
	public static final String REPORT_NAME_OPERATPASSENGERHZ = "OperatPassengerHZ";  //选择表名
	public static final String REPORT_NAME_CN_OPERATPASSENGERHZ = "营业客流分析--汇总";  //选择表名
	public static final String BASICINFO_REPORT_OPERATPASSENGERHZ_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_OPERATPASSENGERHZ = "queryYyklzhfxHz";
	//月度客流综合分析
	public static final String REPORT_NAME_CN_YDKLFX = "月度客流综合分析";
	public static final String REPORT_QUERY_METHOD_YDKLFX = "queryYdklfx";
	//菜品沽清综合分析
	public static final String BASICINFO_REPORT_GUQINGTTL_FROZEN = "";
	public static final String BASICINFO_REPORT_GUQINGSUM_FROZEN = "";
	public static final String REPORT_NAME_CN_GUQINGTTL = "菜品沽清明细表";  
	public static final String REPORT_NAME_CN_GUQINGSUM = "菜品沽清汇总表";  
	public static final String REPORT_QUERY_METHOD_GUQINGTTL  = "queryCaiPinGuQing";
	public static final String REPORT_QUERY_METHOD_GUQINGSUM  = "queryCaiPinGuQingSum";
	//菜品沽清
	public static final String BASICINFO_REPORT_GUQING_FROZEN = "";
	public static final String REPORT_NAME_GUQING = "";  
	public static final String REPORT_NAME_CN_GUQING = "菜品沽清表";   
	public static final String REPORT_QUERY_METHOD_GUQING = "queryCPGQ";
	
	/*分店经理报表start*/
	//营业收入报告一
	public static final String BASICINFO_REPORT_REVENUEREPORT = "993,994,995,996";  //默认选择列
	public static final String REPORT_NAME_REVENUEREPORT = "revenueReport";  //选择表名
	public static final String REPORT_NAME_CN_REVENUEREPORT = "营业收入报告";  //选择表名
	public static final String BASICINFO_REPORT_REVENUEREPORT_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_REVENUEREPORT = "findRevenueReport";

	//套餐业绩统计
	public static final String BASICINFO_REPORT_PACKAGESALESSTATISTICS = "997,998,999,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009";  //默认选择列
	public static final String REPORT_NAME_PACKAGESALESSTATISTICS = "packageSalesStatistics";  //选择表名
	public static final String REPORT_NAME_CN_PACKAGESALESSTATISTICS = "套餐业绩查询";  //选择表名
	public static final String BASICINFO_REPORT_PACKAGESALESSTATISTICS_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PACKAGESALESSTATISTICS = "findPackageSalesStatistics";
	
	//项目销售统计
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICS = "1010,1011,1012,1013,1014,1015,1016,1017";  //默认选择列
	public static final String REPORT_NAME_PROJECTSALESSTATISTICS = "projectSalesStatistics";  //选择表名
	public static final String REPORT_NAME_CN_PROJECTSALESSTATISTICS = "项目销售统计";  //选择表名
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICS_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PROJECTSALESSTATISTICS = "findProjectSalesStatistics";
	
	//项目销售统计 --明细
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICSDETAIL = "1019,1020,1021,1022,1023,1026,1027,1028";  //默认选择列
	public static final String REPORT_NAME_PROJECTSALESSTATISTICSDETAIL = "packageSalesStatisticsDetail";  //选择表名
	public static final String REPORT_NAME_CN_PROJECTSALESSTATISTICSDETAIL = "明细统计表";  //选择表名
	public static final String BASICINFO_REPORT_PROJECTSALESSTATISTICSDETAIL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PROJECTSALESSTATISTICSDETAIL = "findProjectSalesStatistics";
	
	/*分店经理报表ent*/
/**
 * begin
 * @author Xain
 *
 */	
	//折扣定义
	public static final String BASICINFO_REPORT_DISC = "816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838";  //默认选择列
	public static final String REPORT_NAME_DISC = "disc";  //选择表名
	public static final String REPORT_NAME_CN_DISC = "折扣定义";  //选择表名
	public static final String BASICINFO_REPORT_DISC_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_DISC = "";	
	
/**
 * end
 * @author Xain
 *
 */	
	
	/*
	 * 区域班次营业报表
	 * @author ygb  广州酒家使用
	 */
	public static final String BASICINFO_REPORT_AREAANDSFTBUS = "1032,1033,1034,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044";  //默认选择列
	public static final String REPORT_NAME_AREAANDSFTBUS = "areaandsftbus";  //选择表名
	public static final String REPORT_NAME_CN_AREAANDSFTBUS = "区域班次营业报表";  //选择表名
	public static final String BASICINFO_REPORT_AREAANDSFTBUS_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_AREAANDSFTBUS = "findBussinessByArea";
	/*
	 * 点菜类型统计报表
	 * @author ygb  广州酒家使用
	 */
	public static final String REPORT_NAME_CN_FOODTYP = "点菜类型统计报表";  //选择表名
	public static final String BASICINFO_REPORT_FOODTYP_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_FOODTYP = "findFoodTyp";
}