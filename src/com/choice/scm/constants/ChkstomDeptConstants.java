package com.choice.scm.constants;

public class ChkstomDeptConstants {

	//页面跳转
	public static final String REPORT_PRINT_URL="report/chkstom/chkstomReport.jasper";
	public static final String TABLE_CHKSTOM="mis/chkstomDept/tableChkstomDept";
	public static final String LIST_CHKSTOM="mis/chkstomDept/listChkstom";
	public static final String TABLE_DETAIL="mis/chkstomDept/tableDetail";
	public static final String TABLE_DETAIL_2="mis/chkstomDept/tableChkstom";
	public static final String SEARCH_CHKSTOM="mis/chkstomDept/searchChkstom";
//	public static final String SEARCH_CHECKED_CHKSTOM="mis/chkstomDept/tableCheckedChkstom";
	public static final String TABLE_CHKSTOM_NEW="mis/chkstomDept/tableChkstomDept_new";//档口报货单新页面wjf
	public static final String TABLE_DETAIL_NEW="mis/chkstomDept/tableDetail_new";
	public static final String TABLE_DETAIL_2_NEW="mis/chkstomDept/tableChkstom_new";
}
