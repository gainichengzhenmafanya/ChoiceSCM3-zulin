package com.choice.scm.constants;

public class TeShuCaoZuoConstants {

	//页面跳转
	public static final String LIST_FENDIAN_YUEJIE="scm/teShuCaoZuo/fenDianYueJie";//分店月结
	public static final String LIST_NIAN_JIE_ZHUAN="scm/teShuCaoZuo/nianJieZhuan";//年结转
	public static final String LIST_YUEMO_JIEZHANG="scm/teShuCaoZuo/yueMoJieZhang";//月末结账
	public static final String LIST_CLEAR_DATA="scm/teShuCaoZuo/clearData";//清空数据
	public static final String LIST_CANGKU_QICHU="scm/teShuCaoZuo/cangkuInit";//仓库期初
	public static final String MAIN_CHECK = "scm/teShuCaoZuo/mainCheck";//基础设置查错
	public static final String CHECK_DETAIL = "scm/teShuCaoZuo/checkDetail";
	public static final String IMPORT_INIT="scm/teShuCaoZuo/importInit";//期初导入
	public static final String IMPORT_RESULT="scm/teShuCaoZuo/importResult";//导入结果
	//分店mis
	public static final String LIST_CANGKU_QICHU_MIS="mis/teShuCaoZuo/cangkuInit";//仓库期初
}
