package com.choice.scm.constants.ana;

public class AnaConstants {

	//产品理论成本分析   
	public static final String EX_THEORY_COST_ANA="scm/exAna/exTheoryCostAna";//页面跳转
	public static final String EX_THORY_COST_ANA_TABLE="exTheoryCostAna";          //表名
	public static final String EX_THORY_COST_ANA_COLUMN="840,841,842,843,844,845,846,847,848,849"; //列
	public static final String REPORT_PRINT_URL_EXTHEORYCOST = "report/profit/extheorycost.jasper";//打印文件
	//分店毛利对比
	public static final String LIST_FIRMPROFITCMP = "scm/ana/firmProfitCmp";
	//分店档口毛利
	public static final String LIST_FIRMDANGKOUPROFIT = "scm/ana/firmDangKouProfit";
	//月综合毛利分析
	public static final String LIST_MONTHZHMAOLIANA = "scm/ana/monthMaoLiAna";
	public static final String MONTHZHMAOLIANA_TABLE="firmMaoLiAna";          //表名
	public static final String MONTHZHMAOLIANA_COLUMN="867,868,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896"; //列
	public static final String FROZEN_MONTHZHMAOLIANA = "867,868";//固定列
	public static final String REPORT_PRINT_URL_MONTHZHMAOLIANA = "report/profit/firmMonthMaoLi.jasper";//打印文件
	//周综合毛利分析
	public static final String LIST_WEEKZHMAOLIANA = "scm/ana/weekMaoLiAna";
	public static final String WEEKZHMAOLIANA_TABLE="firmMaoLiAna";          //表名
	public static final String WEEKZHMAOLIANA_COLUMN="867,869,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896"; //列
	public static final String FROZEN_WEEKZHMAOLIANA = "867,869,870";//固定列
	public static final String REPORT_PRINT_URL_WEEKZHMAOLIANA = "report/profit/firmWeekMaoLi.jasper";//打印文件
	//应产率分析
	public static final String LIST_PTIVITYANA = "scm/ana/ptivityAna";
	public static final String PTIVITYANA_TABLEJ="misptivity";          //表名
	public static final String PTIVITYANA_COLUMN="950,951,952,953,954,955,956,957,958,959,960,961"; //列
	public static final String FROZEN_PTIVITYANA = "950,951,952";//固定列
	public static final String REPORT_PRINT_URL_PTIVITYANA = "report/profit/ptivity.jasper";//打印文件
	//物资万元用量分析
	public static final String REPORT_SHOW_WZWANYUANYONGLIANGFENXI = "scm/ana/wzWanyuanyongliangFenxi";
	public static final String REPORT_NAME_WZWANYUANYONGLIANGFENXI = "wzWanyuanyongliangFenxi";          //表名
	public static final String BASICINFO_REPORT_WZWANYUANYONGLIANGFENXI = "946,948,950,952,954,956,958,960,962,964,966,968,970,971,972"; //列
	public static final String BASICINFO_REPORT_WZWANYUANYONGLIANGFENXI_FROZEN = "946,948,950,952,954,956";//固定列
	//物资用量分析
	public static final String REPORT_SHOW_WZYONGLIANGFENCHAFENXI = "scm/ana/wzYongliangfenchaFenxi";
	public static final String REPORT_NAME_WZYONGLIANGFENCHAFENXI = "wzYongliangfenchaFenxi";          //表名
	public static final String BASICINFO_REPORT_WZYONGLIANGFENCHAFENXI = "946,948,950,952,954,956,958,960,962,964,966,968,969,970,972,974,975,976,978,980"; //列
	public static final String BASICINFO_REPORT_WZYONGLIANGFENCHAFENXI_FROZEN = "946,948,950,952,954,956";//固定列
	
	//单菜毛利率
	public static final String REPORT_NAME_DANCAIMAOLILV = "dancaiMaolilvScm";//选择表名
	public static final String BASICINFO_REPORT_DANCAIMAOLILV = "772,774,776,778,780,782,784,786,788,790,792,794,795,796,797,798,799,800,801,802";//默认选择列
	public static final String BASICINFO_REPORT_DANCAIMAOLILV_FROZEN = "";//左侧固定列
	public static final String LIST_DANCAIMAOLILV="scm/ana/dancaiMaolilv";
	
	//半成品成本分析
	public static final String REPORT_NAME_BCPCHENGBENFENXI = "bcpChengbenFenxi";//选择表名
	public static final String BASICINFO_REPORT_BCPCHENGBENFENXI = "772,774,776,778,780,782,784,786,788,790,792,794,795,796,797,798,799,800,801,802";//默认选择列
	public static final String BASICINFO_REPORT_BCPCHENGBENFENXI_FROZEN = "";//左侧固定列
	public static final String LIST_BCPCHENGBENFENXI="scm/ana/bcpChengbenFenxi";
	
	//门店成本对比，九毛九用 wjf
	public static final String REPORT_SHOW_MDCHENGBENDUIBI = "scm/ana/mdChengbenDuibi";
}
