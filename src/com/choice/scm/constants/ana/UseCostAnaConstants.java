package com.choice.scm.constants.ana;
/**
 * 耗用成本分析
 * @author csb
 *
 */
public class UseCostAnaConstants {
	
	public static final String ACCT = "1";  //默认帐套值
	
	//列选择：
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	//耗用成本分析--合计
	public static final String BASICINFO_REPORT_USECOSTANA = "921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946";  //默认选择列
	public static final String REPORT_NAME_USECOSTANA = "useCostAna";  //选择表名
	
	//耗用成本分析--明细
	public static final String BASICINFO_REPORT_USECOSTANA_DETAIL = "962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987";  //默认选择列
	public static final String REPORT_NAME_USECOSTANA_DETAIL = "useCostAnaDtl";  //选择表名

	//页面跳转
	public static final String LIST_USECOSTANA = "scm/ana/useCostAna";//耗用成本分析
	public static final String LIST_USECOSTANA_TUBIAO = "scm/ana/useCostAnaTuBiao";//耗用成本分析
	public static final String REPORT_PRINT_URL_USECOSTANA = "report/ana/useCostAnaDtl.jasper";//报表地址;

}
