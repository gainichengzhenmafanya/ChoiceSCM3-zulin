package com.choice.scm.constants.ana;
/**
 * 菜品实际毛利
 * @author csb
 *
 */
public class CaiPinShiJiMaoLiConstants {
	
	public static final String ACCT = "1";  //默认帐套值
	
	//列选择：
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	//分店菜品利润表
	public static final String BASICINFO_REPORT_CAIPIN_SHIJI_MAOLI = "850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,866";  //默认选择列
	public static final String REPORT_NAME_CAIPIN_SHIJI_MAOLI = "caiPinShiJiMaoLi";  //选择表名

	//页面跳转
	public static final String LIST_CAIPIN_SHIJI_MAOLI = "scm/ana/caiPinShiJiMaoLi";//菜品实际毛利
	public static final String REPORT_PRINT_URL_CAIPIN_SHIJI_MAOLI = "report/ana/caiPinShiJiMaoLi.jasper";//报表地址
}
