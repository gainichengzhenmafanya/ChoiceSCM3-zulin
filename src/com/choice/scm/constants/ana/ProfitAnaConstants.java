package com.choice.scm.constants.ana;

public class ProfitAnaConstants {

	//页面跳转
	//毛利日趋势
	public static final String LIST_PROFITDAY = "scm/ana/profitDay";//毛利日趋势
	public static final String REPORT_PRINT_URL_PROFITDAY = "report/profit/profitDay.jasper";//报表地址
	public static final String REPORT_EXP_URL_PROFITDAY = "";
	public static final String PROFIT_DAY_TABLE="profitDay";          //表名
	public static final String PROFIT_DAY_COLUMN="898,899,900,900,901,902,903,904,905,906,907,908"; //列.
	public static final String BASICINFO_REPORT_PROFIT_DAY_FROZEN = "";//左侧固定列
	
	//毛利月趋势
	public static final String LIST_PROFITMONTH = "scm/ana/profitMonth";//毛利月趋势
	public static final String REPORT_PRINT_URL_PROFITMONTH = "report/profit/profitMonth.jasper";//报表地址
	public static final String REPORT_EXP_URL_PROFITMONTH = "";
	public static final String PROFIT_MONTH_TABLE="profitMonth";          //表名
	public static final String PROFIT_MONTH_COLUMN="909,910,911,912,913,914,915,916,917,918,919"; //列
	
	//分店日毛利分析
	public static final String LIST_FIRM_DAYPROFIT = "scm/ana/firmDayProfitAna";//毛利月趋势
	public static final String REPORT_PRINT_URL_FIRM_DAYPROFIT = "";//报表地址
	public static final String REPORT_EXP_URL_FIRM_DAYPROFIT = "";
	public static final String FIRM_DAYPROFIT_TABLE="profitMonth";          //表名
	//public static final String FIRM_DAYPROFIT_COLUMN="906,907,908,908,910,911,912,913,914,915,916,917"; //列
}
