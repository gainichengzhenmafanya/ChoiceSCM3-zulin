package com.choice.scm.constants;

public class SupplyAcctConstants {
	public static final String MIS_REPORT_LIST = "/mis/report/reportListWare";
	public static final String WARE_REPORT_LIST = "/scm/report/reportListWare";
	public static final String FIRM_REPORT_LIST = "/scm/reportFirm/reportListWare";

	// 入库明细查询
	public static final String REPORT_SHOW_CHKINDETAILS = "scm/report/rkMingxiChaxun";
	public static final String REPORT_SHOW_CHKINDETAILS_FIRM = "scm/reportFirm/rkMingxiChaxun";
	public static final String REPORT_SHOW_CHKINDETAILS_MIS = "/mis/report/rkMingxiChaxun";// 分店用
	public static final String REPORT_PRINT_URL_CHKINDETAILS = "/report/chkinm/chkinmdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILS = "/report/chkinm/chkinmdetailquery.jasper";
	public static final String REPORT_SHOW_YXQ = "scm/report/yxqChaxun"; // 有效期查询
																			// 2015.1.3wjf
	public static final String REPORT_PRINT_URL_YXQ = "/report/yxqChaxun/yxqChaxun.jasper";// 有效期查询打印报表地址
																							// wjf
	public static final String REPORT_EXP_URL_YXQ = "/report/yxqChaxun/yxqChaxun.jasper";

	// 入库明细汇总
	public static final String REPORT_SHOW_CHKINDETAILSUM = "scm/report/rkMingxiHuizong";
	public static final String REPORT_SHOW_CHKINDETAILSUM_FIRM = "scm/reportFirm/rkMingxiHuizong";
	public static final String REPORT_SHOW_CHKINDETAILSUM_MIS = "/mis/report/rkMingxiHuizong";// 分店用
	public static final String REPORT_PRINT_URL_CHKINDETAILSUM = "/report/chkinm/chkinmdetailsum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILSUM = "/report/chkinm/chkinmdetailsum.jasper";

	// 入库汇总查询
	public static final String REPORT_SHOW_CHKINSUMQUERY = "scm/report/rkHuizongChaxun";
	public static final String REPORT_SHOW_CHKINSUMQUERY_FIRM = "scm/reportFirm/rkHuizongChaxun";
	public static final String REPORT_SHOW_CHKINSUMQUERY_MIS = "/mis/report/rkHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKINSUMQUERY = "/report/chkinm/chkinmsummaryquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINSUMQUERY = "/report/chkinm/chkinmsummaryquery.jasper";

	// 入库类别汇总
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY = "scm/report/rkLeibieHuizong";
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY_MIS = "/mis/report/rkLeibieHuizong";
	public static final String REPORT_SHOW_CHKINMCATEGORYSUMMARY_FIRM = "/scm/reportFirm/rkLeibieHuizong";
	public static final String REPORT_PRINT_URL_CHKINMCATEGORYSUMMARY = "/report/chkinm/chkinmcategorysummary.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMCATEGORYSUMMARY = "/report/chkinm/chkinmcategorysummary.jasper";

	// 入库综合查询
	public static final String REPORT_SHOW_CHKINMSYNQUERY = "scm/report/rkZongheChaxun";
	public static final String REPORT_SHOW_CHKINMSYNQUERY_MIS = "/mis/report/rkZongheChaxun";
	public static final String REPORT_SHOW_CHKINMSYNQUERY_FIRM = "scm/reportFirm/rkZongheChaxun";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_CATL = "/report/chkinm/chkinmsynquery_catl.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_CATL = "/report/chkinm/chkinmsynquery_catl.jasper";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_SUM = "/report/chkinm/chkinmsynquery_sum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_SUM = "/report/chkinm/chkinmsynquery_sum.jasper";
	public static final String REPORT_PRINT_URL_CHKINMSYNQUERY_DETAIL = "/report/chkinm/chkinmsynquery_detail.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKINMSYNQUERY_DETAIL = "/report/chkinm/chkinmsynquery_detail.jasper";

	// 出库明细查询
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY = "scm/report/ckMingxiChaxun";
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY_FIRM = "scm/reportFirm/ckMingxiChaxun";
	public static final String REPORT_SHOW_CHKOUTDETAILQUERY_MIS = "/mis/report/ckMingxiChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTDETAILQUERY = "/report/chkout/chkoutdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDETAILQUERY = "/report/chkout/chkoutdetailquery.jasper";

	// 出库明细汇总
	public static final String REPORT_SHOW_CHKOUTDETAILSUM = "scm/report/ckMingxiHuizong";
	public static final String REPORT_SHOW_CHKOUTDETAILSUM_FIRM = "scm/reportFirm/ckMingxiHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTDETAILSUM = "/report/chkout/chkoutdetailsum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDETAILSUM = "/report/chkout/chkoutdetailsum.jasper";

	// 出库汇总查询
	public static final String REPORT_SHOW_CHKOUTSUMQUERY = "scm/report/ckHuizongChaxun";
	public static final String REPORT_SHOW_CHKOUTSUMQUERY_FIRM = "scm/reportFirm/ckHuizongChaxun";
	public static final String REPORT_SHOW_CHKOUTSUMQUERY_MIS = "/mis/report/ckHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTSUMQUERY = "/report/chkout/chkoutsumquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTSUMQUERY = "/report/chkout/chkoutsumquery.jasper";

	// 出库盈利查询
	public static final String REPORT_SHOW_CHKOUTPROFITQUERY = "scm/report/ckYingliChaxun";
	public static final String REPORT_PRINT_URL_CHKOUTPROFITQUERY = "/report/chkout/ChkoutProfitQuery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTPROFITQUERY = "/report/chkout/ChkoutProfitQuery.jasper";

	// 出库日期汇总
	public static final String REPORT_SHOW_CHKOUTDATESUM = "scm/report/ckRiqiHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM = "/report/chkout/chkoutdatesum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM = "/report/chkout/chkoutdatesum.jasper";

	// 出库日期汇总1
	public static final String REPORT_SHOW_CHKOUTDATESUM1 = "scm/report/ckRiqiHuizong1";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM1 = "/report/chkout/chkoutdatesum1.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM1 = "/report/chkout/chkoutdatesum1.jasper";

	// 出库日期汇总2
	public static final String REPORT_SHOW_CHKOUTDATESUM2 = "scm/report/ckRiqiHuizong2";
	public static final String REPORT_PRINT_URL_CHKOUTDATESUM2 = "/report/chkout/chkoutdatesum2.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTDATESUM2 = "/report/chkout/chkoutdatesum2.jasper";

	// 出库按月汇总
	public static final String REPORT_SHOW_CHKOUTMONTHSUM = "scm/report/ckMonthHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTMONTHSUM = "/report/chkout/chkoutmonthsum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTMONTHSUM = "/report/chkout/chkoutmonthsum.jasper";

	// 出库类别汇总
	public static final String REPORT_SHOW_CHKOUTCATEGORYSUM = "scm/report/ckLeibieHuizong";
	// 出库类别汇总-分店用
	public static final String REPORT_SHOW_CHKOUTMSYNQUERY_MIS = "mis/report/ckLeibieHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTCATEGORYSUM = "/report/chkout/ChkoutCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTCATEGORYSUM = "/report/chkout/ChkoutCategorySum.jasper";

	// 类别分店汇总
	public static final String REPORT_SHOW_CATEGORYPOSITNSUM = "scm/report/leibieFendianHuizong";
	public static final String REPORT_PRINT_URL_CATEGORYPOSITNSUM = "/report/chkout/CategoryPositnSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYPOSITNSUM = "/report/chkout/CategoryPositnSum.jasper";

	// 分店类别汇总
	public static final String REPORT_SHOW_POSITNCATEGORYSUM = "scm/report/fendianLeibieHuizong";
	public static final String REPORT_PRINT_URL_POSITNCATEGORYSUM = "/report/chkout/PositnCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_POSITNCATEGORYSUM = "/report/chkout/PositnCategorySum.jasper";

	// 出库单据汇总
	public static final String REPORT_SHOW_CHKOUTORDERSUM = "scm/report/ckDanjuHuizong";
	public static final String REPORT_PRINT_URL_CHKOUTORDERSUM = "/report/chkout/ChkoutOrderSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKOUTORDERSUM = "/report/chkout/ChkoutOrderSum.jasper";

	// 配送验货查询
	public static final String REPORT_SHOW_DELIVERYEXAMINEQUERY = "scm/report/PsYanhuoChaxun";
	public static final String REPORT_PRINT_URL_DELIVERYEXAMINEQUERY = "/report/chkout/DeliveryExamineQuery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERYEXAMINEQUERY = "/report/chkout/DeliveryExamineQuery.jasper";

	// 直配验货查询
	public static final String REPORT_SHOW_DIREINSPECTION = "scm/report/ZpYanhuoChaxun";
	public static final String REPORT_PRINT_URL_DIREINSPECTIONQUERY = "/report/chkstom/direInspectionQuery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DIREINSPECTIONQUERY = "/report/chkstom/direInspectionQuery.jasper";

	// 调拨明细查询
	public static final String REPORT_SHOW_CHKALLOTDETAILQUERY = "scm/report/dbMingxiChaxun";
	public static final String REPORT_SHOW_CHKALLOTDETAILQUERY_FIRM = "scm/reportFirm/dbMingxiChaxun";
	public static final String REPORT_PRINT_URL_CHKALLOTDETAILQUERY = "/report/allot/allotdetailquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKALLOTDETAILQUERY = "/report/allot/allotdetailquery.jasper";

	// 调拨汇总查询
	public static final String REPORT_SHOW_CHKALLOTDSUMQUERY = "scm/report/dbHuizongChaxun";
	public static final String REPORT_SHOW_CHKALLOTDSUMQUERY_FIRM = "scm/reportFirm/dbHuizongChaxun";
	public static final String REPORT_PRINT_URL_CHKALLOTDSUMQUERY = "/report/allot/allotsumquery.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHKALLOTDSUMQUERY = "/report/allot/allotsumquery.jasper";

	// 供应商进货汇总
	public static final String REPORT_SHOW_DELIVERSTOCKSUM = "scm/report/GysJinhuoHuizong";
	public static final String REPORT_SHOW_DELIVERSTOCKSUM_MIS = "/mis/report/GysJinhuoHuizong";
	public static final String REPORT_PRINT_URL_DELIVERSTOCKSUM = "/report/deliver/DeliverStockSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERSTOCKSUM = "/report/deliver/DeliverStockSum.jasper";

	// 类别供应商汇总
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM = "scm/report/leibieGysHuizong";
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM_MIS = "/mis/report/leibieGysHuizong";
	public static final String REPORT_PRINT_URL_CATEGORYDELIVERSUM = "/report/deliver/CategoryDeliverSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYDELIVERSUM = "/report/deliver/CategoryDeliverSum.jasper";

	// 类别供应商汇总1
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM1 = "scm/report/leibieGysHuizong1";
	public static final String REPORT_SHOW_CATEGORYDELIVERSUM1_MIS = "/mis/report/leibieGysHuizong1";
	public static final String REPORT_PRINT_URL_CATEGORYDELIVERSUM1 = "/report/deliver/CategoryDeliverSum1.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CATEGORYDELIVERSUM1 = "/report/deliver/CategoryDeliverSum1.jasper";

	// 供应商分店汇总
	public static final String REPORT_SHOW_DELIVERPOSITNSUM = "scm/report/gysFendianHuizong";
	public static final String REPORT_PRINT_URL_DELIVERPOSITNSUM = "/report/deliver/DeliverPositnSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERPOSITNSUM = "/report/deliver/DeliverPositnSum.jasper";

	// 供应商类别汇总
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM = "scm/report/gysLeibieHuizong";
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM_MIS = "/mis/report/gysLeibieHuizong";
	public static final String REPORT_PRINT_URL_DELIVERCATEGORYSUM = "/report/deliver/DeliverCategorySum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERCATEGORYSUM = "/report/deliver/DeliverCategorySum.jasper";

	// 领用供应商类别汇总
	public static final String REPORT_SHOW_LYDELIVERCATEGORYSUM = "scm/report/lyGysLeibieHuizong";

	// 供应商类别汇总2
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM2 = "scm/report/gysLeibieHuizong2";
	public static final String REPORT_PRINT_URL_DELIVERCATEGORYSUM2 = "/report/deliver/DeliverCategorySum2.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERCATEGORYSUM2 = "/report/deliver/DeliverCategorySum2.jasper";

	// 供应商类别汇总3
	public static final String REPORT_SHOW_DELIVERCATEGORYSUM3 = "scm/report/gysLeibieHuizong3";

	// 供应商汇总列表
	public static final String REPORT_SHOW_DELIVERSUM = "scm/report/gysHuizongLiebiao";
	public static final String REPORT_SHOW_DELIVERSUM_MIS = "/mis/report/gysHuizongLiebiao";
	public static final String REPORT_PRINT_URL_DELIVERSUM = "/report/deliver/deliverSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERSUM = "/report/deliver/deliverSum.jasper";

	// 进货单据汇总
	public static final String REPORT_SHOW_STOCKBILLSUM = "scm/report/jhDanjuHuizong";
	public static final String REPORT_SHOW_STOCKBILLSUM_MIS = "/mis/report/jhDanjuHuizong";
	public static final String REPORT_PRINT_URL_STOCKBILLSUM = "/report/stockbill/StockBillSum.jasper";// 报表地址
	public static final String REPORT_EXP_URL_STOCKBILLSUM = "/report/stockbill/StockBillSum.jasper";

	// 供应商付款情况
	public static final String REPORT_SHOW_DELIVERPAYMENT = "scm/report/gysFukuanQingkuang";
	public static final String REPORT_SHOW_DELIVERPAYMENT_MIS = "/mis/report/gysFukuanQingkuang";
	public static final String REPORT_PRINT_URL_DELIVERPAYMENT = "/report/deliver/DeliverPayment.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DELIVERPAYMENT = "/report/deliver/DeliverPayment.jasper";

	// 物资明细账
	public static final String REPORT_SHOW_SUPPLYDETAILSINFO = "scm/report/wzMingxiZhang";
	public static final String REPORT_SHOW_SUPPLYDETAILSINFO_MIS = "/mis/report/wzMingxiZhang";
	public static final String REPORT_PRINT_URL_SUPPLYDETAILSINFO = "report/supply/SupplyDetailsInfo.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYDETAILSINFO = "report/supply/SupplyDetailsInfo.jasper";

	// 物资余额查询
	public static final String REPORT_SHOW_SUPPLYBALANCE = "scm/report/wzYueChaxun";
	public static final String REPORT_SHOW_SUPPLYBALANCE_MIS = "/mis/report/wzYueChaxun";
	public static final String REPORT_PRINT_URL_SUPPLYBALANCE = "report/supplyBalance/SupplyBalance.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYBALANCE = "report/supplyBalance/SupplyBalance.jasper";

	// 物资明细进出表
	public static final String REPORT_SHOW_SUPPLYINOUTINFO = "scm/report/wzMingxiJinchu";
	public static final String REPORT_SHOW_SUPPLYINOUTINFO_FIRM = "scm/reportFirm/wzMingxiJinchu";
	public static final String REPORT_SHOW_SUPPLYINOUTINFO_MIS = "/mis/report/wzMingxiJinchu";
	public static final String REPORT_PRINT_URL_SUPPLYINOUTINFO = "report/supplyInOutInfo/SupplyInOutInfo.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYINOUTINFO = "report/supplyInOutInfo/SupplyInOutInfo.jasper";

	// 物资类别进出表
	public static final String REPORT_SHOW_SUPPLYTYPINOUT = "scm/report/wzLeibieJinchubiao";
	public static final String REPORT_SHOW_SUPPLYTYPINOUT_FIRM = "scm/reportFirm/wzLeibieJinchubiao";
	public static final String REPORT_SHOW_SUPPLYTYPINOUT_MIS = "/mis/report/wzLeibieJinchubiao";
	public static final String REPORT_PRINT_URL_SUPPLYTYPINOUT = "report/supply/SupplyTypInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYTYPINOUT = "report/supply/SupplyTypInOut.jasper";

	// 物资综合进出表
	public static final String REPORT_SHOW_SUPPLYSUMINOUT = "scm/report/wzZongheJinchubiao";
	public static final String REPORT_SHOW_SUPPLYSUMINOUT_FIRM = "scm/reportFirm/wzZongheJinchubiao";
	public static final String REPORT_SHOW_SUPPLYSUMINOUT_MIS = "/mis/report/wzZongheJinchubiao";
	public static final String REPORT_PRINT_URL_SUPPLYSUMINOUT = "report/supply/SupplySumInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SUPPLYSUMINOUT = "report/supply/SupplySumInOut.jasper";

	// 销售订单报表
	public static final String REPORT_SHOW_XIAOSHOUDINGDAN = "scm/report/xiaoShouDingDan";

	// 物资仓库进出表
	public static final String REPORT_SHOW_GOODSSTOREINOUT = "scm/report/wzCangkuJinchubiao";
	public static final String REPORT_SHOW_GOODSSTOREINOUT_FIRM = "scm/reportFirm/wzCangkuJinchubiao";
	public static final String REPORT_SHOW_GOODSSTOREINOUT_MIS = "/mis/report/wzCangkuJinchubiao";
	public static final String REPORT_PRINT_URL_GOODSSTOREINOUT = "/report/supply/GoodsStoreInout.jasper";// 报表地址
	public static final String REPORT_EXP_URL_GOODSSTOREINOUT = "/report/supply/GoodsStoreInout.jasper";

	// 仓库类别进出表
	public static final String REPORT_SHOW_STOCKCATEGORYINOUT = "scm/report/cangkuLeibieJinchu";
	public static final String REPORT_PRINT_URL_STOCKCATEGORYINOUT = "/report/stockbill/StockCategoryInOut.jasper";// 报表地址
	public static final String REPORT_EXP_URL_STOCKCATEGORYINOUT = "/report/stockbill/StockCategoryInOut.jasper";

	// 分店毛利查询
	public static final String LIST_GROSS_PROFIT = "scm/prdPrcCM/grossProfit";
	public static final String REPORT_PRINT_URL_GROSS_PROFIT = "report/prdPrcCM/grossProfit.jasper";// 报表地址
	public static final String REPORT_EXP_URL_GROSS_PROFIT = "report/prdPrcCM/grossProfit.jasper";

	// 门店盘点状态查询查询
	public static final String LIST_MDRIPANDIANZHUANGTAIBIAO_PROFIT = "scm/report/mdRiPanDianZhuangTaiBiao"; // 门店盘点状态表
	public static final String REPORT_PRINT_URL_MDRIPANDIANZHUANGTAIBIAO_PROFIT = "scm/report/mdRiPanDianZhuangTaiBiao";// 报表地址

	// 存货盘点
	public static final String REPORT_SHOW_CUNHUOPANDIAN_FIRM = "scm/reportFirm/cunhuoPandian";
	public static final String REPORT_PRINT_URL_CUNHUOPANDIAN = "/report/cunhuoPandian/cunhuoPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CUNHUOPANDIAN = "/report/cunhuoPandian/cunhuoPandian.jasper";

	// 月末盘点
	public static final String REPORT_SHOW_YUEMOPANDIAN_FIRM = "scm/reportFirm/yuemoPandian";
	public static final String REPORT_SHOW_YUEMOPANDIAN_FIRM2 = "scm/reportFirm/yuemoPandian2";
	public static final String REPORT_PRINT_URL_YUEMOPANDIAN = "/report/yuemoPandian/yuemoPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_YUEMOPANDIAN = "/report/yuemoPandian/yuemoPandian.jasper";

	// 差异管理
	public static final String REPORT_SHOW_CHAYIGUANLI_FIRM = "scm/reportFirm/chayiGuanli";
	public static final String REPORT_PRINT_URL_CHAYIGUANLI = "/report/chayiGuanli/chayiGuanli.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CHAYIGUANLI = "/report/chayiGuanli/chayiGuanli.jasper";

	// 每日差异对照
	public static final String REPORT_SHOW_MEIRICHAYIDUIZHAO_FIRM = "scm/reportFirm/meiriChayiDuizhao";
	public static final String REPORT_PRINT_URL_MEIRICHAYIDUIZHAO = "/report/meiriChayiDuizhao/meiriChayiDuizhao.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MEIRICHAYIDUIZHAO = "/report/meiriChayiDuizhao/meiriChayiDuizhao.jasper";

	// 历史盘点
	public static final String REPORT_SHOW_LISHIPANDIAN_FIRM = "scm/reportFirm/lsPandianFirm";
	public static final String REPORT_PRINT_URL_LISHIPANDIAN_FIRM = "/report/lsPandianFirm/lsPandianFirm.jasper";// 报表地址
	public static final String REPORT_EXP_URL_LISHIPANDIAN_FIRM = "/report/lsPandianFirm/lsPandianFirm.jasper";

	// 调拨汇总
	public static final String REPORT_SHOW_DIAOBOHUIZONG_FIRM = "scm/reportFirm/dbHuizongFirm";
	public static final String REPORT_PRINT_URL_DIAOBOHUIZONG_FIRM = "/report/dbHuizongFirm/dbHuizongFirm.jasper";// 报表地址
	public static final String REPORT_EXP_URL_DIAOBOHUIZONG_FIRM = "/report/dbHuizongFirm/dbHuizongFirm.jasper";

	// 分店盈利情况表
	public static final String REPORT_SHOW_FDYINGLIQINGKUANG = "scm/report/fdYingliQingkuang";
	public static final String REPORT_PRINT_URL_FDYINGLIQINGKUANG = "/report/fdYingliQingkuang/fdYingliQingkuang.jasper";// 报表地址
	public static final String REPORT_EXP_URL_FDYINGLIQINGKUANG = "/report/fdYingliQingkuang/fdYingliQingkuang.jasper";

	// 物资成本汇总表
	// public static final String REPORT_SHOW_WZCHENGBENHUIZONG =
	// "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_WZCHENGBENHUIZONG_FIRM = "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_PRINT_URL_WZCHENGBENHUIZONG = "report/wzChengbenHuizong/wzChengbenHuizong.jasper";// 报表地址
	public static final String REPORT_EXP_URL_WZCHENGBENHUIZONG = "report/wzChengbenHuizong/wzChengbenHuizong.jasper";

	// 物资成本明细表
	// public static final String REPORT_SHOW_WZCHENGBENHUIZONG =
	// "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_WZCHENGBENMINGXI_FIRM = "scm/reportFirm/wzChengbenMingxi";
	public static final String REPORT_PRINT_URL_WZCHENGBENMINGXI = "report/wzChengbenHuizong/wzChengbenMingxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_WZCHENGBENMINGXI = "report/wzChengbenHuizong/wzChengbenMingxi.jasper";

	// 月成本综合分析
	// public static final String REPORT_SHOW_WZCHENGBENHUIZONG =
	// "scm/reportFirm/wzChengbenHuizong";
	public static final String REPORT_SHOW_YUECHENGBENZONGHEFENXI_FIRM = "scm/reportFirm/yueChengbenZongheFenxi";
	public static final String REPORT_PRINT_URL_YUECHENGBENZONGHEFENXI = "report/yueChengbenZongheFenxi/yueChengbenZongheFenxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_YUECHENGBENZONGHEFENXI = "report/yueChengbenZongheFenxi/yueChengbenZongheFenxi.jasper";

	// 各店毛利率
	public static final String REPORT_SHOW_GDMAOLILV = "scm/prdPrcCM/gdMaolilv";
	public static final String REPORT_PRINT_URL_GDMAOLILV = "/scm/prdPrcCM/gdMaolilv.jasper";// 报表地址

	// 单菜毛利横向对比
	public static final String REPORT_SHOW_DCMLHENGXIANGDUIBI = "scm/prdPrcCM/dcMaolilv";
	public static final String REPORT_PRINT_URL_DCMLHENGXIANGDUIBI = "/scm/prdPrcCM/dcMaolilv.jasper";// 报表地址

	// 原料应产率
	public static final String REPORT_SHOW_YLYINGCHANLV = "scm/prdPrcCM/ylYingchanlv";
	public static final String REPORT_PRINT_URL_YLYINGCHANLV = "/scm/prdPrcCM/ylYingchanlv.jasper";// 报表地址

	// 存货汇总
	public static final String REPORT_SHOW_CUNHUOHUIZONG_FIRM = "scm/reportFirm/cunhuoHuizong";
	public static final String REPORT_PRINT_URL_CUNHUOHUIZONG = "/report/cunhuoHuizong/cunhuoHuizong.jasper";// 报表地址
	public static final String REPORT_EXP_URL_CUNHUOHUIZONG = "/report/cunhuoHuizong/cunhuoHuizong.jasper";

	// 第二单位查询
	public static final String REPORT_SHOW_SECONDUNIT_FIRM = "scm/reportFirm/secondunit";
	public static final String REPORT_SHOW_SECONDUNITCY_FIRM = "scm/reportFirm/secondunit_cy";
	public static final String REPORT_PRINT_URL_SECONDUNIT_SUPPLY = "/report/secondunit/secondunit_supply.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_SUPPLY = "/report/secondunit/secondunit_supply.jasper";
	public static final String REPORT_PRINT_URL_SECONDUNIT_DATE = "/report/secondunit/secondunit_date.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_DATE = "/report/secondunit/secondunit_date.jasper";
	public static final String REPORT_PRINT_URL_SECONDUNIT_CHAYI = "/report/secondunit/secondunit_chayi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_SECONDUNIT_CHAYI = "/report/secondunit/secondunit_chayi.jasper";

	// 历史盘点查询 2014.12.9wjf
	public static final String REPORT_SHOW_LISHIPANDIAN = "scm/report/lsPandian";
	public static final String REPORT_PRINT_URL_LISHIPANDIAN = "report/lsPandian/lsPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_LISHIPANDIAN = "report/lsPandian/lsPandian.jasper";

	// 门店盘点核减状态查询 2015.10.17wjf
	public static final String REPORT_SHOW_MDPDHJZHUANGTAICHAXUN = "scm/report/mdPdhjState"; // 门店盘点状态表
	public static final String REPORT_PRINT_URL_MDPDHJZHUANGTAICHAXUN = "report/mdPandian/mdPdhjState.jasper";// 报表地址

	// 门店盘点查询 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN = "scm/report/mdPandian";
	public static final String REPORT_PRINT_URL_MDPANDIAN = "report/mdPandian/mdPandian.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN = "report/mdPandian/mdPandian.jasper";

	// 门店盘点查询 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN_DETAIL = "scm/report/mdPandianDetail";
	public static final String REPORT_PRINT_URL_MDPANDIAN_DETAIL = "report/mdPandian/mdPandianDetail.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN_DETAIL = "report/mdPandian/mdPandianDetail.jasper";

	// 门店盘点查询 2015.9.1wjf
	public static final String REPORT_SHOW_MDPANDIAN_MINGXI = "scm/report/mdPandianMingxi";
	public static final String REPORT_PRINT_URL_MDPANDIAN_MINGXI = "report/mdPandian/mdPandianMingxi.jasper";// 报表地址
	public static final String REPORT_EXP_URL_MDPANDIAN_MINGXI = "report/mdPandian/mdPandianMingxi.jasper";

	// 门店盘点分析表 耗用分析表 2015.10.20 wjf
	public static final String REPORT_SHOW_MDPANDIANFENXI = "scm/report/mdPandianFenxi";
	public static final String REPORT_SHOW_MDHAOYONGFENXI = "scm/report/mdHaoyongFenxi";

	// 物资分店汇总2015.8.10wjf
	public static final String REPORT_SHOW_WZFENDIANHUIZONG = "scm/report/wzFendianHuizong";

	// 自定义报表2015.10.12 wjf
	public static final String CUSTOMREPORTTREE = "/scm/report/customReport/customReportTree"; // 报表树
	public static final String ADD_FIRSTMENU = "/scm/report/customReport/addFirstMenu"; // 添加一级树节点
	public static final String ADD_CUSTOMREPORT = "/scm/report/customReport/addCustomReport"; // 添加报表树节点
	public static final String UPDATE_FIRSTMENU = "/scm/report/customReport/updateFirstMenu"; // 修改一级树节点
	public static final String UPDATE_CUSTOMREPORT = "/scm/report/customReport/updateCustomReport"; // 修改报表树节点

	// 门店验货查询20160321 js
	public static final String MDYANHUO_REPORT = "scm/report/mdYanhuoChaxun";
	// 仓库类别进出报表
	public static final String CANGKU_LEIBIE_INOUTB = "scm/report/cangkuLeibieJinchuSecond";
}
