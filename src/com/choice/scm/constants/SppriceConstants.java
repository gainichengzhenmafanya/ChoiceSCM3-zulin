package com.choice.scm.constants;
/**
 * @author yp
 */
public class SppriceConstants {

	public static final String LIST_SPPRICE = "scm/spprice/listSpprice";
	public static final String TABLE_SPPRICE = "scm/spprice/tableSpprice";
	public static final String SEARCH_SPPRICE = "scm/spprice/searchSpprice";
	public static final String TABLE_RESULT = "scm/spprice/tableResult";
	public static final String EDAT_SPPRICE = "scm/spprice/edatSpprice";
	public static final String MANAGE_SPPRICE = "scm/spprice/SppriceManage";
	public static final String SPPRICE_VIEW = "scm/spprice/viewSpprice";
	public static final String SPPRICE_IMPORT_VIEW = "scm/spprice/importSpprice";
	public static final String SPPRICE_IMPORT_RESULT = "scm/spprice/sppriceImportResult";
}
