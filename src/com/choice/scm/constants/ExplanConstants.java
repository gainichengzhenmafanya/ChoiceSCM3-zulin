package com.choice.scm.constants;

public class ExplanConstants {

	//页面跳转
	public static final String LIST_EXPLAN="scm/explan/listExplan";
	public static final String TABLE_EXPLAN="scm/explan/tableExplan";
	public static final String SELECT_GRP="scm/explan/selectGrp";
	public static final String SELECT_POSITN="scm/explan/selectPositn";
	public static final String SAVE_CHKSTOM="scm/explan/saveChkstom";
	public static final String PLAN_CHKSTOM="scm/explan/planExplan";
	public static final String SAFECNT_PLAN="scm/explan/safeCntToPlan";
	public static final String SELECT_CODEDES="scm/explan/selectCodeDes";	
	
	public static final String PLAN="计划";

}
