package com.choice.scm.constants;

public class DisConstants {

	//报货分拨
	public static final String LIST_DIS="scm/dis/listDis";//报货分拨
	public static final String CAIGOU_TOTAL_DELIVERDISTRIBUTION="scm/dis/caiGouTotalDeliverDis";//配送查询，采购汇总
	public static final String CAIGOU_TOTAL_DISTRIBUTION="scm/dis/caiGouTotalDis";//报货分拨，采购汇总
	public static final String PEISONG_TOTAL_DISTRIBUTION="scm/dis/peiSongTotalDis";//报货分拨，配送汇总
	public static final String DEPT_DETAIL_DISTRIBUTION="scm/dis/deptDetail";//报货分拨，部门明细
	public static final String DEPT_DETAIL_DISTRIBUTION2="scm/dis/deptDetail2";//报货分拨，部门明细
	public static final String YANHUO_ZHIFA_DISTRIBUTION="scm/dis/yanHuoZhiFa";//报货分拨，验货直发
	public static final String REPORT_INDENT_URL="report/dis/indent.jasper";//分拨单
	public static final String REPORT_INDENT_URL_NEW2="report/dis/indentNew2.jasper";//分拨单1
	public static final String REPORT_INSPECTION_URL="report/dis/inspection.jasper";//验货单
	public static final String REPORT_DELIVERY_URL="report/dis/delivery.jasper";//配送单	
	public static final String REPORT_RECEIPT_URL="report/dis/Receipt.jasper";//验收单
	public static final String REPORT_TOTAL_DELIVER_URL="report/dis/disDeliverTotal.jasper";//报货汇总
	public static final String REPORT_TOTAL_URL="report/dis/disTotal.jasper";//报货汇总
	public static final String REPORT_DEPTDETAIL2_URL="report/dis/disDeptDetail.jasper";//部门明细2
	public static final String REPORT_EXCEL_URL="report/dis/distributionExcel.jasper";//Excel
	//报货验收
	public static final String LIST_DIS_CHECK="scm/dis/listDisCheck";//报货验收
	public static final String TABLE_CHECKIN="scm/dis/tableCheckin";//验收入库
	public static final String REPORT_YSRK_URL="/report/dis/ysrkChkstomReport.jasper";//验收入库打印
	public static final String TABLE_CHECKOUT="scm/dis/tableCheckout";//验收出库
	public static final String REPORT_YSCK_URL="/report/dis/ysckChkstomReport.jasper";//验收出库打印
	//分店MIS
	public static final String MIS_TABLE_CHECK="mis/dis/tableCheck";//供应商到货验收
	public static final String MIS_TABLE_CHECKIN="mis/dis/tableCheckin";//验收入库
	public static final String MIS_TABLE_CHECKIN_NEW = "mis/dis/tableCheckin_new";//新版mis验收入库 wjf
	public static final String SAVE_CHKINM_DEPT_ZP="mis/dis/saveChkinmDept_zp";//供应商到货验收多档口
	public static final String SAVE_CHKINM_GROUP_ZP="mis/dis/saveChkinmGroup_zp";//供应商到货验收多档口
	public static final String LIST_DISPATCH="mis/dispatch/listDispatch";//配送中心到货验收
	public static final String REPORT_DISPATCH_URL="report/dis/disReport.jasper";//配送中心到货验收表
	public static final String SAVE_CHKINM_DEPT="mis/dispatch/saveChkinmDept";//配送中心到货验收
	public static final String SAVE_CHKINM_GROUP="mis/dispatch/saveChkinmGroup";//配送中心到货验收 分 多档口
	//调拨验货
	public static final String LIST_DISPATCH_DB="mis/dispatch/listDispatchDb";//调拨验收
	public static final String SAVE_CHKINM_GROUP_DB="mis/dispatch/saveChkinmGroupDb";//调拨验收 多档口
	
	public static final String LIST_DIS_NEW="scm/dis/listDis_new";//新版报货分拨页面 wjf
	public static final String REPORT_EXCEL_URL_NEW="report/dis/distributionExcel_new.jasper";//Excel wjf
	public static final String REPORT_TOTAL_URL_NEW="report/dis/disTotal_new.jasper";//报货汇总wjf
	public static final String REPORT_INDENT_URL_NEW="report/dis/indent_new.jasper";//分拨单wjf
	
	public static final String TABLE_CHECKIN_NEW="scm/dis/tableCheckin_new";//新版验收入库wjf
	public static final String TABLE_CHECKOUT_NEW="scm/dis/tableCheckout_new";//新版验收出库wjf
	public static final String YANHUO_ZHIFA_DISTRIBUTION_NEW="scm/dis/yanHuoZhiFa_new";//新版验货直发wjf
}
