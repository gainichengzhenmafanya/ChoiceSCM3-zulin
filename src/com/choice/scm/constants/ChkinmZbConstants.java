package com.choice.scm.constants;

public class ChkinmZbConstants {

	//页面跳转
	public static final String LIST_CHKINM="mis/chkinm/listChkinmZb";//查询所有未审核页面
	public static final String LIST_CHECKED_CHKINM="mis/chkinm/listCheckedChkinmZb";//查询所有已审核页面
//	public static final String TABLE_CHKINM="scm/chkinm/tableChkinm";
//	public static final String SAVE_CHKINM="mis/chkinm/saveChkinm";
//	public static final String SEARCH_CHECKED_CHKINM="scm/chkinm/updateCheckedChkinm";
	public static final String SEARCH_CHECKED_CHKINMZB="mis/chkinm/updateCheckedChkinmZb";
//	public static final String PRINT_CHKINM="mis/chkinm/printChkinm"; //打印
	public static final String UPDATE_CHKINMZB="mis/chkinm/updateChkinmzb";//直拨单添加页面
//	public static final String TABLE_CHKINM_CX="scm/chkinm/tableChkinmCx";//添加冲消物资界面
	public static final String REPORT_URL_ZB="/report/chkinm/chkinmzbMisReport.jasper";//报表地址
	public static final String TABLE_CHKINMZF_CX = "mis/chkinm/tableChkinmzfCx";
	
	public static final String in="in"; 
	public static final String conk="inout"; 
	public static final String explanIn="9905"; 
	public static final String inVententoryIn="9901";

}
