package com.choice.scm.constants;

public class InventoryConstants {

	//页面跳转
	public static final String LIST_INVENTORY="scm/inventory/listInventory";
	public static final String LIST_INVENTORY_MIS="mis/inventory/listInventory";
	public static final String LIST_INVENTORY_DEPT="mis/inventory/listInventory_dept";
	public static final String ADD_INVENTORY="scm/inventory/addInventory";
	public static final String UPDATE_INVENTORY="scm/inventory/updateInventory";
	public static final String LIST_NO_INVENTORY_POSITN="scm/inventory/ListNoInventoryPostin";
	public static final String ADD_INVENTORY_DEMO="scm/inventory/addInvendemo"; // 盘点时候调用盘点模板
	
	
	//存货盘点
	public static final String LIST_STOCK="scm/inventory/listStock";
	public static final String STOCK_TABLE="CunHuoPanDianBiao";          //表名
	public static final String STOCK_COLUMN="997,998,999,1000,1001,1002,1003,1004,1005"; //列
	public static final String FROZEN_STOCK = "";//固定列
	public static final String REPORT_PRINT_URL_STOCK = "/report/scm/inventory/stock.jasper";//打印文件
	
	
	//月盘点差异报告
	public static final String REPORT_SHOW_MONTHPd="scm/inventory/monthPd";
	public static final String BASICINFO_REPORT_MONTHPd = "802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828";//默认选择列
	public static final String TABLE_NAME_MONTHPd = "monthPd";//选择表名
	public static final String BASICINFO_REPORT_MONTHPd_FROZEN = "802,803,804,805,806";//固定列
	public static final String REPORT_PRINT_URL_MONTHPD="/report/scm/inventory/monthPd.jasper";//报表地址
	
	public static final String REPORT_PRINT_URL_INVENTORY = "/report/scm/inventory/inventory.jasper";//空白盘点表 2014.12.26wjf
	public static final String REPORT_PRINT_URL_MIS_INVENTORY = "/report/scm/inventory/misInventory.jasper";//mis空白盘点表 2014.12.26wjf

	//加工间盘点
	public static final String LIST_EXINVENTORY = "scm/explan/listExInventory";
}
