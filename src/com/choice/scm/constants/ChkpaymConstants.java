package com.choice.scm.constants;

public class ChkpaymConstants {

	//页面跳转
	public static final String ADD_CHKPAYM="scm/chkpaym/addChkpaym";
	public static final String REPORT_PRINT_URL="report/chkpaym/chkpaymReport.jasper";
	public static final String TABLE_CHKPAYM="scm/chkpaym/tableChkpaym";
	public static final String SEARCH_CHKPAYM="scm/chkpaym/searchChkpaym";
}
