package com.choice.scm.constants;

public class CostbomConstants {

	//页面跳转
	public static final String LIST_COSTBOM="scm/costbom/listCostbom";
	public static final String TABLE_COSTBOM="scm/costbom/tableCostbom";
	public static final String TABLE_COSTBOM_X="scm/costbom/tableCostbom_x";
	public static final String TABLE_COSTBOM_GZJJ_N="scm/costbom/table_costbom_gzjj_n";
	public static final String TABLE_COSTBOM_GZJJ_X="scm/costbom/table_costbom_gzjj_x";
	public static final String SELECT_SUPPLY="scm/costbom/selectSupply";
	public static final String SELECT_SUPPLY_SAFE="scm/explan/selectSupply";
	public static final String SELECT_TABLE_SUPPLY="scm/costbom/selectTableSupply";
	public static final String SELECT_SUPPLY_X="scm/costbom/selectSupply_x";
	public static final String SELECT_TABLE_SUPPLY_X="scm/costbom/selectTableSupply_x";
	public static final String SELECT_S_X_S="scm/costbom/selectSupply_sxs";
	
	public static final String LIST_COSTBOM_GZJJ="scm/costbom/listCostbom_gzjj";
	public static final String TABLE_COSTBOM_GZJJ="scm/costbom/tableCostbom_gzjj";
	//菜品理论利润率
	public static final String LIST_CAI_PING_LI_LUNLILUNLV="scm/costbom/listCaiPingLiLunLiLunLv";
	public static final String REPORT="report/costbom/costbomReport.jasper";//菜品bom报表
	
	//菜品bom右侧下方，虚拟物料用
	public static final String DOWN_COSTBOM="scm/costbom/downCostbom";
	//替换物料
	public static final String DOWN_COSTBOM_N="scm/costbom/downCostbom_n";
	
	//复制成本卡
	public static final String COSTBOMTYP = "scm/costbom/costbomTyp";
	
	//复制成本卡
	public static final String COSTBOMTYP_X = "scm/costbom/costbomTyp_x";
	
	//刷新
	public static final String DONE = "share/done";
	

	public static final String SELECT_SUPPLY_MODX="scm/chkstodemo_x/selectSupply_x";
	public static final String SELECT_TABLE_SUPPLY_MODX="scm/chkstodemo_x/selectTableSupply_x";

}
