package com.choice.scm.constants;
/**
 * 报货单单据模板
 * @author 孙胜彬
 */
public class ChkstoDemomConstants {
	//报货单单据模板主页面
	public static final String LIST_CHKSTODEMOM="scm/chkstoDemom/treeChkstoDemom";
	//报货单单据模板子页面
	public static final String LIST_CHKSTODEMOD="scm/chkstoDemom/listChkstoDemod";
	//修改报货单单据模板页面
	public static final String UPD_CHKSTODEMOD="scm/chkstoDemom/updateChkstoDemod";
	//新增报货单单据模板页面
	public static final String ADD_ChkstoDemod="scm/chkstoDemom/addChkstoDemod";
	//新增报货单单据模板页面
	public static final String LIST_listChkstoDemoFirm="scm/chkstoDemom/listFirmChkstoDemom";
	// 报货时候调用报货模板
	public static final String ADD_CHKSTO_DEMO="scm/chkstom/addChkstodemo"; 
	
	// 报货时候调用报货模板(新报货流程用2014.12.27wjf)
	public static final String ADD_CHKSTO_DEMO_NEW="scm/chkstom/addChkstodemo_new"; 
	
	//******************虚拟物料*******************
	//报货单单据模板主页面
	public static final String LIST_CHKSTODEMOM_X="scm/chkstodemo_x/treeChkstoDemom";
	//报货单单据模板子页面
	public static final String LIST_CHKSTODEMOD_X="scm/chkstodemo_x/listChkstoDemod";
	//修改报货单单据模板页面
	public static final String UPD_CHKSTODEMOD_X="scm/chkstodemo_x/updateChkstoDemod";
	//新增报货单单据模板页面
	public static final String ADD_CHKSTODEMOD_X="scm/chkstodemo_x/addChkstoDemod";
	//新增报货单单据模板页面
	public static final String LIST_LISTCHKSTODEMOFIRM_X="scm/chkstodemo_x/listFirmChkstoDemom";
	// 报货时候调用报货模板
	public static final String ADD_CHKSTO_DEMO_X="scm/chkstodemo_x/addChkstodemo"; 
	
}
