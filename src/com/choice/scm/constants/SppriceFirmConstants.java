package com.choice.scm.constants;

public  class SppriceFirmConstants {
	public static final String LIST_FIRM="scm/sppriceFirm/listFirm";
	public static final String TABLE_SUPPLY="scm/sppriceFirm/tableFirmSupply";
	public static final String SELECT_POSITN="scm/sppriceFirm/selectOnePositn";
	public static final String TO_SPPRICE = "scm/sppriceFirm/listSpprice";
	public static final String TO_SPPRICESALE = "scm/sppriceFirm/listSppriceSale";
	
}
