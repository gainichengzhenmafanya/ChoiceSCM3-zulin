package com.choice.scm.constants;

public class FirmDeptConstants {

	//页面跳转
	public static final String LIST_FIRMDEPT="scm/firmDept/listFirmDept";
	
	//部门类型
	public static final String dept_1 = "1楼吧台";	
	public static final String dept_2 = "2楼吧台";	
	public static final String dept_3 = "吧台";	
	public static final String dept_4 = "办公室";	
	public static final String dept_5 = "仓库";	
	public static final String dept_6 = "后厨部";	
	public static final String dept_7 = "凉菜";	
	public static final String dept_8 = "盘亏";	
	public static final String dept_9 = "前厅";	
	public static final String dept_10 = "热菜";	
	public static final String dept_11 = "试菜";	
	public static final String dept_12 = "水煮鱼";	
	public static final String dept_13 = "宿舍";	
	public static final String dept_14 = "小吃";	
	public static final String dept_15 = "员工餐";	
	public static final String dept_16 = "自制饮料";	
}
