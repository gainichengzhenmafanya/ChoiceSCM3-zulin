package com.choice.scm.constants;

public class TemplateConstants {

	public static final String LIST_SUPPLY="scm/supply/listSupply";//物资列表左侧页面
	
	public static final String CHOOSE_SUPPLY="scm/supply/chooseSupply";
	
	public static final String CHOOSE_SUPPLY_SPPRICE="scm/supply/chooseSupplySpprice";
	
	public static final String TABLE_SUPPLY="scm/supply/tableSupply";//物资列表右侧主页面
	
	public static final String CHOOSE_TABLE_SUPPLY="scm/supply/chooseTableSupply";
	
	public static final String CHOOSE_TABLE_SUPPLY_SPPRICE="scm/supply/chooseTableSupplySpprice";
	
	public static final String SAVE_SUPPLY="scm/supply/saveSupply";//保存页面
	public static final String SAVE_SUPPLY_NEW="scm/supply/saveSupply_new";//保存页面
	public static final String IMPORT_TEMPLATE="scm/template/importTemplate";//导入页面
	
	public static final String IMPORT_RESULT="scm/supply/importResult";//导入结果
	
	public static final String SELECT_SUPPLY="scm/supply/selectGrpTyp";//新增修改中 选择 类别页面
	
	public static final String UPDATE_SUPPLY="scm/supply/updateSupply";//修改物资页面
	public static final String UPDATE_SUPPLY_NEW="scm/supply/updateSupply_new";//修改物资页面
	public static final String COPY_SUPPLY="scm/supply/copySupply";//复制物资页面
	public static final String COPY_SUPPLY_NEW="scm/supply/copySupply_new";//复制物资页面
	public static final String REPORT_SUPPLY="scm/supply/reportSupply";//物资报表页面
	
	public static final String REPORT_URL="/report/supply/supplyReport.jasper";//报表地址
	
	public static final String ADD_SUPPLYBATCH="scm/supply/addSupplyBatch";
	
	public static final String SELECT_NSUPPLY="scm/supply/selectNSupply";
	
	public static final String SELECT_SUPPLY_X="scm/supply/select_supply_x";
	
	public static final String SELECT_SUPPLY_XBOM="scm/supply/select_supply_xbom";//菜品BOM新增专用页面
	
	public static final String TABLESUPPLYBYSP_CODE_X="scm/supply/tableSupplyBySp_code_x";//虚拟物料对应的实际物料信息
	
	public static final String TABLESUPPLYLISTBYSP_CODE_X="scm/supply/tableSupplyListBySp_code_x";//显示所有的虚拟物料
	
	public static final String TABLESUPPLYLISTBYSP_CODE="scm/supply/tableSupplyListBySp_code";//显示所有的虚拟物料
	
	public static final String SELECT_TABLENSUPPLY="scm/supply/selectTableNSupply";
	
	public static final String  DIVISIONSUPPLY="system/accountSupply/divisionSupply";
	
	public static final String  GROUPSUPPLY="system/accountSupply/groupSupply";
	
	public static final String  CLASSSUPPLY="system/accountSupply/classSupply";

	public static final String SEARCH_ALL_PUBITEM = "scm/supply/searchAllPubitem";

	public static final String UPLOAD_SUPPLY_IMG = "scm/supply/uploadSupplyImg";

	public static final String SEARCHSUPPLY_X="scm/supply/searchSupply_x";

	public static final String UPDATESUPPLY_X="scm/supply/updateSupply_x";//修改虚拟物料
	
	public static final String DOWN_COSTBOM="scm/supply/downSupply_x";

	public static final String JOINPOSITN = "scm/supply/joinPositn";//加入分店 wjf

	public static final String JOINACCOUNT = "scm/supply/joinAccount";//适配用户 wjf
	
	public static final String TABLE_SUPPLYUNIT="scm/supply/tableSupplyUnit";//物资列表右侧主页面
	public static final String SAVE_SUPPLYUNIT="scm/supply/saveSupplyUnit";//保存页面
	public static final String UPDATE_SUPPLYUNIT="scm/supply/updateSupplyUnit";//修改物资页面

}
