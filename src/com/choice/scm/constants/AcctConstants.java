package com.choice.scm.constants;

public class AcctConstants {

	//页面跳转
	public static final String LIST_ACCT="scm/acct/listAcct";
	public static final String ADD_ACCT="scm/acct/addAcct";
	public static final String UPDATE_ACCT="scm/acct/updateAcct";
	
	//不能操作之前的数据
	public static final String CAN_NOT_CHECK_BEFORE_DATA="com.choice.framework.exception.CRUDException: 不能操作当前会计期之前的数据！";
	public static final String NOT_NIAN_JIE_ZHUAN = "com.choice.framework.exception.CRUDException: com.choice.framework.exception.CRUDException: 系统出现严重问题----没有年结转！！请先停止使用，联系辰森研发人员 ";
}
