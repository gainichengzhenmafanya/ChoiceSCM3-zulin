package com.choice.scm.constants;



public final class ScmStringConstant {
	private ScmStringConstant(){}
	
	//
	public static final String ACCT = "1";  //默认帐套值
	
	//列选择：
	//列选择跳转的页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	public static final String BASICINFO_TABLE_DEFAULT_COLUMNS = "02,03,04,07,30,33,38,17,20,05,16,48";  //默认选择列
	public static final String TABLE_NAME_SUPPLY = "supply";  //选择表名
	//入库明细查询报表
	public static final String BASICINFO_REPORT_CHKIN = "72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,10000,10001,10025,10026";  //默认选择列
	public static final String MIS_BASICINFO_REPORT_CHKIN = "88,73,74,75,76,77,78,79,80,81,82,83,84,85,86";  //分店默认选择列
	public static final String FIRM_BASICINFO_REPORT_CHKIN = "88,74,10025,76,77,78,79,80,81,82,83,84";  //总部分店默认选择列
	public static final String DELIVER_BASICINFO_REPORT_CHKIN = "10000,10001,10025,10026,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90";  //分店默认选择列
	public static final String REPORT_NAME_CHKIN = "chkinDetailQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKIN_FROZEN = "72,73";  
	public static final String MIS_BASICINFO_REPORT_CHKIN_FROZEN = "73,74";  
	public static final String FIRM_BASICINFO_REPORT_CHKIN_FROZEN = "73,74";  
	public static final Integer[] EXCEL_NAME_CHKIN = {80,81,82,84};  //要导出的数字列
	//入库明细汇总报表
	public static final String BASICINFO_REPORT_CHKINCDS = "89,10030,91,92,93,94,95,96,97,98,99";  //默认选择列
	public static final String DELIVER_BASICINFO_REPORT_CHKINCDS = "89,91,92,93,94,96";  //默认选择列
	public static final String REPORT_NAME_CHKINCDS = "chkinDetailSum";  //选择表名
	public static final String BASICINFO_REPORT_CHKINCDS_FROZEN = "89,90";  
	//入库汇总查询
	public static final String BASICINFO_REPORT_CHKINCSQ = "99,100,101,102,103,104,105,106,107";  //默认选择列
	public static final String REPORT_NAME_CHKINCSQ = "ChkinSumQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKINCSQ_FROZEN = "";  
	//小类信息PUBGRP
	public static final String PUBGRP_TABLE_DEFAULT_COLUMNS = "561,562,563,564,565,566,567";  //默认选择列
	public static final String TABLE_NAME_PUBGRP = "pubgrp";  //选择表名
	//菜谱信息PUBITEM
	public static final String PUBITEM_TABLE_DEFAULT_COLUMNS = "593,594,595,807,598,596,599,597,605,603,612,604,606,607,608,609,610,611,613";  //默认选择列
	public static final String TABLE_NAME_PUBITEM = "pubitem";  //选择表名
	//套餐提成信息ITEMCOMM
	public static final String ITEMCOMM_TABLE_DEFAULT_COLUMNS = "771,772,773,774,775,776,777,779,778,780,781";  //默认选择列
	public static final String TABLE_NAME_ITEMCOMM = "itemcomm";  //选择表名
	
	//入库综合查询
	public static final String BASICINFO_REPORT_CHKINMSYNQUERY_DETAIL = "148,149,150,151,152,153,154,155,156,157,158";  //默认选择列
	public static final String REPORT_NAME_CHKINMSYNQUERY_DETAIL = "ChkinmSynQuery_Detail";  //选择表名
	
	public static final String BASICINFO_REPORT_CHKINMSYNQUERY_SUM = "159,160,161,162,163,164,165";  //默认选择列
	public static final String REPORT_NAME_CHKINMSYNQUERY_SUM = "ChkinmSynQuery_Sum";  //选择表名
	
	public static final String BASICINFO_REPORT_CHKINMSYNQUERY_CATL = "159,160,161,162,163,164,165";  //默认选择列
	public static final String REPORT_NAME_CHKINMSYNQUERY_CATL = "ChkinmSynQuery_Catl";  //选择表名
	//出库明细查询
	public static final String BASICINFO_REPORT_CHKOUTDETAILQUERY = "106,107,10020,10021,108,109,110,111,112,113,114,115,116,117,118,119,120,1100";  //默认选择列
	public static final String MIS_REPORT_CHKOUTDETAILQUERY = "106,109,10020,10021,111,112,113,114,115,116,117,118";  //默认选择列
//	public static final String FIRM_REPORT_CHKOUTDETAILQUERY = "106,109,110,111,112,113,114,115,116,117,118";  //默认选择列,1043,1044,1045,1046,1047
	public static final String FIRM_REPORT_CHKOUTDETAILQUERY = "106,109,10020,10021,111,112,113,114,115,116,117,118";  //默认选择列,1043,1044,1045,1046,1047
	public static final String REPORT_NAME_CHKOUTDETAILQUERY = "ChkoutDetailQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTDETAILQUERY_FROZEN = "";
	//出库明细汇总
	public static final String BASICINFO_REPORT_CHKOUTSYNQUERY = "121,122,123,124,125,126,127,128";  //默认选择列
	public static final String REPORT_NAME_CHKOUTSYNQUERY = "ChkoutDetailSum";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTSYNQUERY_FROZEN = "";
	//出库汇总查询
	public static final String BASICINFO_REPORT_CHKOUTSUMQUERY = "129,130,131,132,133,134,135,136,137,138,139,140,141";  //默认选择列
	public static final String REPORT_NAME_CN_CHKOUTSUMQUERY = "出库明细汇总报表";  //选择表名
	public static final String REPORT_NAME_CHKOUTSUMQUERY = "ChkoutSumQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTSUMQUERY_FROZEN = "";
	//出库日期汇总
	public static final String BASICINFO_REPORT_CHKOUTDATESUM = "";  //默认选择列
	public static final String REPORT_NAME_CHKOUTDATESUM = "ChkoutDateSum";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTDATESUM_FROZEN = "";
	//出库日期汇总1
	public static final String BASICINFO_REPORT_CHKOUTDATESUM1 = "142,143,144";  //默认选择列
	public static final String REPORT_NAME_CHKOUTDATESUM1 = "ChkoutDateSum1";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTDATESUM1_FROZEN = "";
	//出库日期汇总2
	public static final String BASICINFO_REPORT_CHKOUTDATESUM2 = "145,146,147";  //默认选择列
	public static final String REPORT_NAME_CHKOUTDATESUM2 = "ChkoutDateSum2";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTDATESUM2_FROZEN = "";
	
	// 出库按月汇总
	public static final String BASICINFO_REPORT_CHKOUTMonthSUM = "144,145"; // 默认选择列
	public static final String REPORT_NAME_CHKOUTMonthSUM = "ChkoutMonthSum"; // 选择表名
	public static final String BASICINFO_REPORT_CHKOUTMonthSUM_FROZEN = "144,145";
		
		
	//调拨明细查询
	public static final String BASICINFO_REPORT_CHKALLOTDETAILQUERY = "172,173,174,175,176,177,178,179,180";  //默认选择列
	public static final String REPORT_NAME_CHKALLOTDETAILQUERY = "ChkAllotDetailQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKALLOTDETAILQUERY_FROZEN = "";
	//调拨汇总查询
	public static final String BASICINFO_REPORT_CHKALLOTSUMQUERY = "181,182,183";  //默认选择列
	public static final String REPORT_NAME_CHKALLOTSUMQUERY = "ChkAllotSumQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKALLOTSUMQUERY_FROZEN = "";
	//供应商进货汇总
	public static final String BASICINFO_REPORT_DELIVERSTOCKSUM = "184,185,186,187,188,189,190";  //默认选择列
	public static final String REPORT_NAME_DELIVERSTOCKSUM = "DeliverStockSum";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERSTOCKSUM_FROZEN = "";
	//供应商类别汇总
	public static final String BASICINFO_REPORT_DELIVERCATEGORYSUM = "191,192,193,194,195";  //默认选择列
	public static final String REPORT_NAME_DELIVERCATEGORYSUM = "DeliverCategorySum";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERCATEGORYSUM_FROZEN = "";
	//出库盈利查询
	public static final String BASICINFO_REPORT_CHKOUTPROFITQUERY = "196,197,198,199,200,201,202,203,204,205";  //默认选择列
	public static final String REPORT_NAME_CHKOUTPROFITQUERY = "ChkoutProfitQuery";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTPROFITQUERY_FROZEN = "";
	//类别分店汇总
	public static final String BASICINFO_REPORT_CATEGORYPOSITNSUM = "206,207,208,209,210";  //默认选择列
	public static final String REPORT_NAME_CATEGORYPOSITNSUM = "CategoryPositnSum";  //选择表名
	public static final String BASICINFO_REPORT_CATEGORYPOSITNSUM_FROZEN = "";
	//分店类别汇总
	public static final String BASICINFO_REPORT_POSITNCATEGORYSUM = "211,212,213,214,215";  //默认选择列
	public static final String REPORT_NAME_POSITNCATEGORYSUM = "PositnCategorySum";  //选择表名
	public static final String BASICINFO_REPORT_POSITNCATEGORYSUM_FROZEN = "";
	//出库单据汇总
	public static final String BASICINFO_REPORT_CHKOUTORDERSUM = "216,217,218,219,220,221,222,223,224";  //默认选择列
	public static final String REPORT_NAME_CHKOUTORDERSUM = "ChkoutOrderSum";  //选择表名
	public static final String BASICINFO_REPORT_CHKOUTORDERSUM_FROZEN = "";
	//配送验货查询
	public static final String BASICINFO_REPORT_DELIVERYEXAMINEQUERY = "225,226,227,228,229,230,231,232,233,234,235,236,237,238";  //默认选择列
	public static final String REPORT_NAME_DELIVERYEXAMINEQUERY = "DeliveryExamineQuery";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERYEXAMINEQUERY_FROZEN = "";
	//直配验货查询
	public static final String BASICINFO_REPORT_DIREINSPECTION = "4000,4001,4002,4003,4004,4005,4006,4007,4008,4009,4010,4011,4012,4013,4014,4015,4016,4017,4018,4019,4020";  //默认选择列
	public static final String REPORT_NAME_DIREINSPECTION = "DireInspectionQuery";  //选择表名
	public static final String BASICINFO_REPORT_DIREINSPECTION_FROZEN = "";
	//类别供应商汇总
	public static final String BASICINFO_REPORT_CATEGORYDELIVERSUM = "239,240,241,242,243,244";  //默认选择列 jinshuai 20160421 增加含税金额
	public static final String REPORT_NAME_CATEGORYDELIVERSUM = "CategoryDeliverSum";  //选择表名
	public static final String BASICINFO_REPORT_CATEGORYDELIVERSUM_FROZEN = "";
	//类别供应商汇总1
	public static final String BASICINFO_REPORT_CATEGORYDELIVERSUM1 = "244,245,246,247,248,249,250,251";  //默认选择列	jinshuai 20160421 增加含税金额
	public static final String REPORT_NAME_CATEGORYDELIVERSUM1 = "CategoryDeliverSum1";  //选择表名
	public static final String BASICINFO_REPORT_CATEGORYDELIVERSUM1_FROZEN = "";
	//供应商分店汇总
	public static final String BASICINFO_REPORT_DELIVERPOSITNSUM = "251,252,253,254,255";  //默认选择列
	public static final String REPORT_NAME_DELIVERPOSITNSUM = "DeliverPositnSum";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERPOSITNSUM_FROZEN = "";
	//供应商汇总
	public static final String BASICINFO_REPORT_DELIVERSUM = "256,257,258,259";  //默认选择列
	public static final String REPORT_NAME_DELIVERSUM = "DeliverSum";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERSUM_FROZEN = "";
	//供应商付款情况
	public static final String BASICINFO_REPORT_DELIVERPAYMEN = "273,274,275,276,277,278,279,280,281";  //默认选择列
	public static final String REPORT_NAME_DELIVERPAYMENT = "DeliverPayment";  //选择表名
	public static final String BASICINFO_REPORT_DELIVERPAYMENT_FROZEN = "";
	//进货单据汇总
	public static final String BASICINFO_REPORT_STOCKBILLSUM = "260,261,262,263,264,265,266,267,268,269,270,271,272";//默认选择列
	public static final String REPORT_NAME_STOCKBILLSUM = "StockBillSum";//选择表名
	public static final String BASICINFO_REPORT_STOCKBILLSUM_FROZEN = "";
	
	//物资仓库进出表
	public static final String BASICINFO_REPORT_GOODSSTOREINOUT = "282,283,287,289,293,301";//默认选择列
	public static final String REPORT_NAME_GOODSSTOREINOUT = "GoodsStoreInout";//选择表名
	public static final String BASICINFO_REPORT_GOODSSTOREINOUT_FROZEN = "282,283";

	//物资余额查询 SupplyBalance
	public static final String BASICINFO_REPORT_SUPPLYBALANCE = "301,302,303,304,305,306,307,308,309,310,311,312,313,314,315";//默认选择列
	public static final String REPORT_NAME_SUPPLYBALANCE = "SupplyBalance";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYBALANCE_FROZEN = "301,302,303,304,305";//固定列
	//物资明细进出表
	public static final String BASICINFO_REPORT_SUPPLYINOUTINFO = "322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337";//默认选择列
	public static final String BASICINFO_REPORT_SUPPLYINOUTINFO_FIRM = "322,323,324,325,329,330,331,332,333,334";//默认选择列
	public static final String REPORT_NAME_SUPPLYINOUTINFO = "SupplyInOutInfo";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYINOUTINFO_FROZEN = "322,323,324,325";//固定列
	//仓库类别进出表
	public static final String BASICINFO_REPORT_STOCKCATEGORYINOUT = "";//默认选择列
	public static final String REPORT_NAME_STOCKCATEGORYINOUT = "CategoryInOut";//选择表名
	public static final String BASICINFO_REPORT_STOCKCATEGORYINOUT_FROZEN = "";
	//物资综合进出表
	public static final String BASICINFO_REPORT_SUPPLYSUMINOUT = "338,339,340,341,342,343,344";//默认选择列
	public static final String REPORT_NAME_SUPPLYSUMINOUT = "SupplySumInOut";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYSUMINOUT_FROZEN = "";
	//销售订单报表
	public static final String REPORT_NAME_XIAOSHOUDINGDAN="XiaoShouDingDan";
	public static final String BASICINFO_REPORT_XIAOSHOUDINGDAN="10066,10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084,10085,10086,10087,10088,10089,10090";
	public static final String BASICINFO_REPORT_XIAOSHOUDINGDAN_FROZEN="";
	//物资类别进出表
	public static final String BASICINFO_REPORT_SUPPLYTYPINOUT = "346,347,348,349,350,351";//默认选择列
	public static final String REPORT_NAME_SUPPLYTYPINOUT = "SupplyTypInOut";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYTYPINOUT_FROZEN = "";
	//物资明细账报表
	public static final String BASICINFO_REPORT_SUPPLYDETAILSINFO = "366,367,368,369,370,371,372,373,374,375,376,377,378";//默认选择列
	public static final String REPORT_NAME_SUPPLYDETAILSINFO = "SupplyDetailsInfo";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYDETAILSINFO_FROZEN = "";
	//价格预警和比重
	public static final String BASICINFO_REPORT_PRICEWARNING = "389,390,391,392,393,394,395,396,397,398,399,400";//默认选择列
	public static final String REPORT_NAME_PRICEWARNING = "PriceWarning";//选择表名
	public static final String BASICINFO_REPORT_PRICEWARNING_FROZEN = "";
	//不合格进货统计
	public static final String BASICINFO_REPORT_UNQUALIFIEDSTATISTICS = "434,435,436,437,438,439,440";//默认选择列
	public static final String REPORT_NAME_UNQUALIFIEDSTATISTICS = "UnqualifiedStatistics";//选择表名
	public static final String BASICINFO_REPORT_UNQUALIFIEDSTATISTICS_FROZEN = "";
	//申购到货分析
	public static final String BASICINFO_REPORT_APPLYARRIVESTATISTICS = "506,507,508,509,510,511,512,513";//默认选择列
	public static final String REPORT_NAME_APPLYARRIVESTATISTICS = "ApplyArriveStatistics";//选择表名
	public static final String BASICINFO_REPORT_APPLYARRIVESTATISTICS_FROZEN = "";
	//申购出库分析
	public static final String BASICINFO_REPORT_APPLYOUTSTATISTICS = "514,515,516,517,518,519,520,521";//默认选择列
	public static final String REPORT_NAME_APPLYOUTSTATISTICS = "ApplyOutStatistics";//选择表名
	public static final String BASICINFO_REPORT_APPLYOUTSTATISTICS_FROZEN = "";
	//物资库领明细
	public static final String BASICINFO_REPORT_SUPPLYKULINGDETAIL = "710,711,712,713,714,715,716,717,718,719";  //默认选择列
	public static final String REPORT_NAME_SUPPLYKULINGDETAIL = "supplyKuLingDetail";  //选择表名
	//物资ABC分析
	public static final String BASICINFO_REPORT_SUPPLYABCSTATISTICS_SUM = "528,529,530,531,532";//默认选择列
	public static final String BASICINFO_REPORT_SUPPLYABCSTATISTICS_DETAIL = "533,534,535,536,537,538,539,540,541";
	public static final String REPORT_NAME_SUPPLYABCSTATISTICS_SUM = "SupplyABCStatisticsSum";//选择表名
	public static final String REPORT_NAME_SUPPLYABCSTATISTICS_DETAIL = "SupplyABCStatistics";//选择表名
	public static final String BASICINFO_REPORT_SUPPLYABCSTATISTICSSUM_FROZEN = "";
	
	//库存周转率
	public static final String BASICINFO_REPORT_KCZZL = "762,763,764,765,766,767,768";  //默认选择列
	public static final String REPORT_NAME_KCZZL = "kuCunZhouZhuanLv";  //选择表名
	
	//库存周转率详情
	public static final String BASICINFO_REPORT_KCZZL_DETAIL = "769,770,771,772,773,774,775";  //默认选择列
	public static final String REPORT_NAME_KCZZL_DETAIL = "kuCunZhouZhuanLv_detail";  //选择表名
	
	//库龄综合分析
	public static final String BASICINFO_REPORT_INVENTORYAGING_SUM = "408,409,410,411,412,413,414,415,416,417,418,419,420";//默认选择列
	public static final String BASICINFO_REPORT_INVENTORYAGING_DETAIL = "421,422,423,424,425,426,427,428,429,430,431,432,433";
	public static final String REPORT_NAME_INVENTORYAGING_SUM = "InventoryAging_Sum";//选择表名
	public static final String REPORT_NAME_INVENTORYAGING_DETAIL = "InventoryAging_Detail";//选择表名
	public static final String BASICINFO_REPORT_INVENTORYAGINGSUM_FROZEN = "";
	
	//采购价格异动分析
	public static final String BASICINFO_REPORT_PURCHASEPRICECHANGE = "471,472,473,474,475,476,477,478,479,480,481,483";//默认选择列
	public static final String REPORT_NAME_PURCHASEPRICECHANGE = "PurchasePriceChange";//选择表名
	public static final String BASICINFO_REPORT_PURCHASEPRICECHANGE_FROZEN = "";
	//入库成本分析
	public static final String BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_SUM = "441,442,443,444,445,446,447,448,449,450,451,452,453,454";//默认选择列
	public static final String REPORT_NAME_RECEIPT_COST_ANALYSIS_SUM = "ReceiptCostAnalysis_sum";//选择表名
	public static final String REPORT_NAME_RECEIPT_COST_ANALYSIS_DETAIL = "ReceiptCostAnalysis_Detail";//选择表名
	public static final String BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_DETAIL = "455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470";//默认选择列
	public static final String BASICINFO_REPORT_RECEIPT_COST_ANALYSIS_FROZEN = "";
	//资金占用情况
	public static final String BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG = "990,991,992,993,994,995,996";
	public static final String REPORT_NAME_ZIJINZHANYONGQINGKUANG = "ZiJinZhanYongQingKuang";//选择表名
	public static final String BASICINFO_REPORT_ZIJINZHANYONGQINGKUANG_FROZEN = "";//固定列
	//周入库成本分析
	public static final String BASICINFO_REPORT_ZHOU_RU_KU_CHENG_BEN_FEN_XI = "522,523,524,525,526,527";//默认选择列
	public static final String REPORT_NAME_ZHOU_RU_KU_CHENG_BEN_FEN_XI = "ZhouRuKuChengBenFenXi";//选择表名
	public static final String BASICINFO_REPORT_ZHOU_RU_KU_CHENG_BEN_FEN_XI_FROZEN = "";//固定列
	//物流配送分析
	public static final String BASICINFO_REPORT_LOGISTICSANALYSIS = "484,485,486,487,488,489,490,491,492,493";//默认选择列
	public static final String REPORT_NAME_LOGISTICSANALYSIS = "LogisticsAnalysis";//选择表名
	public static final String BASICINFO_REPORT_LOGISTICSANALYSIS_FROZEN = "";
	//库存量预估使用
	public static final String BASICINFO_REPORT_INVENTORYESTIMATES = "494,495,496,497,498,499,500,501,502,503,504,505";//默认选择列
	public static final String REPORT_NAME_INVENTORYESTIMATES = "InventoryEstimates";//选择表名
	public static final String BASICINFO_REPORT_INVENTORYESTIMATES_FROZEN = "";
	//分店进货价格分析
//	public static final String BASICINFO_REPORT_FIRMSTOCKPRICE = "546,547,548,549,550,551,552,553,554";//默认选择列
	public static final String BASICINFO_REPORT_FIRMSTOCKPRICE = "546,547,548,549,551,554,553";//默认选择列   先隐藏分店均价，比例
	public static final String REPORT_NAME_FIRMSTOCKPRICE = "FirmStockPrice";//选择表名
	public static final String BASICINFO_REPORT_FIRMSTOCKPRICE_FROZEN = "";
	//采购价格异动分析1
	public static final String BASICINFO_REPORT_PURCHASEPRICECHANGE1 = "555,556,558,559,560,561,562,563,564,565,567,568";//默认选择列
	public static final String REPORT_NAME_PURCHASEPRICECHANGE1 = "PurchasePriceChange1";//选择表名
	public static final String BASICINFO_REPORT_PURCHASEPRICECHANGE1_FROZEN = "";
	//仓库周发货对比
	public static final String BASICINFO_REPORT_CANGKUZHOUFAHUODUIBI = "542,543,544,545";//默认选择列
	public static final String REPORT_NAME_CANGKUZHOUFAHUODUIBI = "CangKuZhouFaHuoDuiBi";//选择表名
	public static final String BASICINFO_REPORT_CANGKUZHOUFAHUODUIBI_FROZEN = "";
	//采购测算分析
	public static final String LIST_CAIGOUCESUAN = "scm/reportAnalysis/CgCesuan";
	public static final String CAIGOUCESUAN_TABLE="CaiGouCeSuan";          //表名
	public static final String CAIGOUCESUAN_COLUMN="1006,1007,1008,1009,1010,1011,1012,1013,1014"; //列
	public static final String FROZEN_CAIGOUCESUAN = "";//固定列
	
	//报表
	public static final String IREPORT_HTML = "share/ireport/html";  //html页面
	public static final String IREPORT_EXCEL = "share/ireport/excel";  //excel页面
	public static final String IREPORT_PDF = "share/ireport/pdf";  //pdf页面
	public static final String IREPORT_WORD = "share/ireport/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/printWord";  //直接打印word页面
	//打印插件下载
	public static final String REPORT_FILE_NAME = "install_lodop.exe"; 
	public static final String REPORT_FILE_ADDRESS = "printPlugin";
	
	//序列名称
	public static final String GEN_EXNO="GEN_EXNO.Nextval";

	//节假日设置列选择
	public static final String BASICINFO_REPORT_HOLIDAY = "1043,1044,1045,1046";  //默认选择列
	public static final String REPORT_NAME_HOLIDAY = "holiday";  //选择表名
	public static final String REPORT_NAME_CN_HOLIDAY = "节假日设置表";  //中文名
	public static final String REPORT_QUERY_METHOD_HOLIDAY = "listHoliday";
	
	//存货盘点表
	public static final String BASICINFO_REPORT_CUNHUOPANDIAN_FIRM = "1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1064,1065,1066,1067";//默认选择列
	public static final String REPORT_NAME_CUNHUOPANDIAN = "cunhuoPandian";//选择表名
	public static final String BASICINFO_REPORT_CUNHUOPANDIAN_FROZEN = "1052,1053,1054,1055";//固定列

	//月末盘点表
	public static final String BASICINFO_REPORT_YUEMOPANDIAN_FIRM = "1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083";//默认选择列
	public static final String REPORT_NAME_YUEMOPANDIAN = "yuemoPandian";//选择表名
	public static final String REPORT_NAME_YUEMOPANDIAN2 = "yuemoPandian2";//选择表名
	public static final String BASICINFO_REPORT_YUEMOPANDIAN_FROZEN = "1068,1069,1070,1071";//固定列
	public static final String BASICINFO_REPORT_YUEMOPANDIAN_FIRM2 = "3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031,3032,3033";//默认选择列
	public static final String BASICINFO_REPORT_YUEMOPANDIAN_FROZEN2 = "1070,1071";//固定列
	
	//差异管理
	public static final String BASICINFO_REPORT_CHAYIGUANLI_FIRM = "1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105";//默认选择列
	public static final String REPORT_NAME_CHAYIGUANLI = "chayiGuanli";//选择表名
	public static final String BASICINFO_REPORT_CHAYIGUANLI_FROZEN = "1084,1085,1086,1087,1088,1089";//固定列
	
	//历史盘点
	public static final String BASICINFO_REPORT_LISHIPANDIAN_FIRM = "3020,3021,3022,3023,3024,3025,3026,3027,3028,3029,3030,3031";//默认选择列
	public static final String REPORT_NAME_LISHIPANDIAN_FIRM = "lishiPandianFirm";//选择表名
	public static final String BASICINFO_REPORT_LISHIPANDIAN_FROZEN_FIRM = "3020,3021,3022,3023";//固定列
	
	//调拨汇总
	public static final String BASICINFO_REPORT_DIAOBOHUIZONG_FIRM = "3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3047,3048,3049,3050,3051";//默认选择列
	public static final String REPORT_NAME_DIAOBOHUIZONG_FIRM = "dbHuizong";//选择表名
	public static final String BASICINFO_REPORT_DIAOBOHUIZONG_FROZEN_FIRM = "3035";//固定列
		
	//每日差异对照
//	public static final String BASICINFO_REPORT_MEIRICHAYIDUIZHAO_FIRM = "1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094";//默认选择列
//	public static final String REPORT_NAME_MEIRICHAYIDUIZHAO = "meiriChayiDuizhao";//选择表名
//	public static final String BASICINFO_REPORT_MEIRICHAYIDUIZHAO_FROZEN = "1076,1077,1078,1079,1080,1081";//固定列

	//分店盈利情况表
	public static final String BASICINFO_REPORT_FDYINGLIQINGKUANG = "1076,1077,1078,1079,1080,1081,1082";  //默认选择列
	public static final String MIS_REPORT_FDYINGLIQINGKUANG = "1076,1077,1078,1079,1080,1081,1082";  //默认选择列
	public static final String FIRM_REPORT_FDYINGLIQINGKUANG = "1076,1077,1078,1079,1080,1081,1082";  //默认选择列
	public static final String REPORT_NAME_FDYINGLIQINGKUANG = "fdYingliQingkuang";  //选择表名
	public static final String BASICINFO_REPORT_FDYINGLIQINGKUANG_FROZEN = "";
	
	//物资成本汇总表
	public static final String BASICINFO_REPORT_WZCHENGBENHUIZONG = "1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120";//默认选择列
	public static final String REPORT_NAME_WZCHENGBENHUIZONG = "wzChengbenHuizong";//选择表名
	public static final String BASICINFO_REPORT_WZCHENGBENHUIZONG_FROZEN = "1106,1107";
	
	//物资成本明细表
	public static final String BASICINFO_REPORT_WZCHENGBENMINGXI = "1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135";//默认选择列
	public static final String REPORT_NAME_WZCHENGBENMINGXI = "wzChengbenMingxi";//选择表名
	public static final String BASICINFO_REPORT_WZCHENGBENMINGXI_FROZEN = "1121,1122,1123,1124";
	
	//月成本综合分析
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXI = "10052,10053,1181,1182,1183,1184,1185,1186,1187,1188,1189,1190,1191,1192,1193,1194,1195,1196,1197,1198,1199,1200,1201,1202";//默认选择列
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_Y = "10040,10041,1177,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051,10052,10053,10054,10055,10056";//ynkc＝Y 默认选择列
	public static final String REPORT_NAME_YUECHENGBENZONGHEFENXI = "yueChengbenZongheFenxi";//选择表名
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_FROZEN = "10055,10056,1177";
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_FROZEN_Y = "10055,10056,1177";
	
	//月成本综合分析
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXICHOICE3 = "1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1187,1188,1189,1190,1191,1192,1193,1194,1195,1202,1203,1204,1205,1206,1207,1208,1209,1210,1211";//默认选择列
	public static final String REPORT_NAME_YUECHENGBENZONGHEFENXICHOICE3 = "yueChengbenZongheFenxiChoice3";//选择表名
	public static final String BASICINFO_REPORT_YUECHENGBENZONGHEFENXI_FROZENCHOICE3 = "1175,1176,1177";


    /*10040,yueChengbenZongheFenxi,turnover
    10041,yueChengbenZongheFenxi,netsales
    10042,yueChengbenZongheFenxi,bprofits
    10043,yueChengbenZongheFenxi,aprofits
    10044,yueChengbenZongheFenxi,matcost
    10045,yueChengbenZongheFenxi,flacost
    10046,yueChengbenZongheFenxi,mercost
    10047,yueChengbenZongheFenxi,totalcost
    10048,yueChengbenZongheFenxi,water
    10049,yueChengbenZongheFenxi,elect
    10050,yueChengbenZongheFenxi,natural*/
	
	//存货汇总表
	public static final String BASICINFO_REPORT_CUNHUOHUIZONG_FIRM = "1136,1137,1138,1139,1140,1141,1142,1143,1144,1145,1146,1147,1148,1149";//默认选择列
	public static final String REPORT_NAME_CUNHUOHUIZONG = "cunhuoHuizong";//选择表名
	public static final String BASICINFO_REPORT_CUNHUOHUIZONG_FROZEN = "1134,1135";//固定列
	
	//第二单位查询
	public static final String BASICINFO_REPORT_SECONDUNIT_SUPPLY = "1150,1151,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167";  //默认选择列
	public static final String REPORT_NAME_SECONDUNIT_SUPPLY = "secondUnit_supply";  //选择表名
	
	public static final String BASICINFO_REPORT_SECONDUNIT_DATE = "1168,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183";  //默认选择列
	public static final String REPORT_NAME_SECONDUNIT_DATE = "secondUnit_date";  //选择表名
	
	//历史盘点查询2014.12.10 wjf
	public static final String BASICINFO_REPORT_LISHIPANDIAN = "3000,3001,3002,3003,3004,3005,3006,3007,3008,3009,3010,3011,3012,3013,3014,3017";//默认选择列
	public static final String BASICINFO_REPORT_LISHIPANDIAN_FROZEN = "";
	public static final String REPORT_NAME_LISHIPANDIAN = "lishiPandian";//选择表名
	
	//门店盘点查询2015.9.1 wjf
	public static final String BASICINFO_REPORT_MDPANDIAN = "5050,5052,5054,5056,5058,5060,5062,5064,5066,5068,5070";//默认选择列
	public static final String BASICINFO_REPORT_MDPANDIAN_FROZEN = "";
	public static final String REPORT_NAME_MDPANDIAN = "mdPandian";//选择表名
	
	//门店盘点查询明细2015.9.1 wjf
	public static final String BASICINFO_REPORT_MDPANDIAN_DETAIL = "5060,5061,5062,5072,5074,5076,5078,5079,5080,5081,5082,5084,5086,5088,5090,5092,5094,5096,5098,5099,5100,5101,5102,5103,5104,5105,5106,5108,5109,5110,5112";//默认选择列
	public static final String BASICINFO_REPORT_MDPANDIAN_DETAIL_FROZEN = "5072,5074,5076";
	public static final String REPORT_NAME_MDPANDIAN_DETAIL = "mdPandianDetail";//选择表名
	
	//门店盘点查询2015.9.1 wjf
	public static final String BASICINFO_REPORT_MDPANDIAN_MINGXI = "5040,5041,5042,5050,5052,5054,5056,5058,5060,5062,5064,5066,5068,5070,5072,5074,5076,5078,5079,5080,5081,5082,5084,5086,5088,5090,5092,5094,5096,5098,5099,5100,5101,5102,5103,5104,5105,5106,5108,5109,5110,5112";//默认选择列
	public static final String BASICINFO_REPORT_MDPANDIAN_MINGXI_FROZEN = "5050,5052,5054,5056,5058";
	public static final String REPORT_NAME_MDPANDIAN_MINGXI = "mdPandianMingxi";//选择表名
	
	//批量维护物资参考类别页面
	public static final String TOBATCHTYPOTH = "scm/supply/toBatchEditTypoth";
	
	//有效期查询报表2015.1.3wjf
	public static final String BASICINFO_REPORT_YXQ = "10002,10003,10004,10006,10007,10008,10009,10010,10013,10014,10015,10016,10017,10018";  //默认选择列
	public static final String REPORT_NAME_YXQ = "YxqQuery";  //选择表名
	public static final String BASICINFO_REPORT_YXQ_FROZEN = "";  
	
	//物资分店汇总2015.8.10wjf
	public static final String BASICINFO_REPORT_WZFENDIANHUIZONG = "";  //默认选择列
	public static final String REPORT_NAME_WZFENDIANHUIZONG = "ChkoutDateSum";  //选择表名
	public static final String BASICINFO_REPORT_WZFENDIANHUIZONG_FROZEN = "";

}