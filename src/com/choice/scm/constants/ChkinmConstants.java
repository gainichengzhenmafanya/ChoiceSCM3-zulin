package com.choice.scm.constants;

public class ChkinmConstants {

	//页面跳转
	public static final String LIST_CHKINM="scm/chkinm/listChkinm";//查询所有未审核页面
	public static final String MIS_LIST_CHKINM="mis/chkinm/listChkinm";//查询所有未审核页面 MIS
	public static final String LIST_CHECKED_CHKINM="scm/chkinm/listCheckedChkinm";//查询所有已审核页面
	public static final String MIS_LIST_CHECKED_CHKINM="mis/chkinm/listCheckedChkinm";//查询所有已审核页面MIS
	public static final String TABLE_CHKINM="scm/chkinm/tableChkinm";
	public static final String SAVE_CHKINM="scm/chkinm/saveChkinm";
	public static final String UPDATE_CHKINM="scm/chkinm/updateChkinm";//添加，修改  入库单页面  主要的
	public static final String MIS_TABLE_CHKINM="mis/chkinm/tableChkinm";    //mis
	public static final String MIS_SAVE_CHKINM="mis/chkinm/saveChkinm";      //mis
	public static final String MIS_UPDATE_CHKINM="mis/chkinm/updateChkinm";//添加，修改  入库单页面  主要的MIS
	public static final String SEARCH_CHECKED_CHKINM="scm/chkinm/updateCheckedChkinm";
	public static final String SEARCH_CHECKED_CHKINMZB="scm/chkinm/updateCheckedChkinmzb";
	public static final String MIS_SEARCH_CHECKED_CHKINMZB="mis/chkinm/updateCheckedChkinmzb";//mis
	public static final String MIS_SEARCH_CHECKED_CHKINM="mis/chkinm/updateCheckedChkinm";//mis
	public static final String PRINT_CHKINM="scm/chkinm/printChkinm"; //打印
	public static final String UPDATE_CHKINMZB="scm/chkinm/updateChkinmzb";//直拨单添加页面
	public static final String MIS_UPDATE_CHKINMZB="mis/chkinm/updateChkinmzb";//直拨单添加页面
	public static final String TABLE_CHKINM_CX="scm/chkinm/tableChkinmCx";//添加冲消物资界面
	public static final String MIS_TABLE_CHKINM_CX="mis/chkinm/tableChkinmCx";//添加冲消物资界面
	
	public static final String DELIVERSA_LIST ="scm/chkinm/listDeliverSa";//供应商结算主界面
	public static final String FOLIO_LIST ="scm/chkinm/listFolios";//付款记录
	public static final String BILL_LIST ="scm/chkinm/listBills";//发票记录
	public static final String PAY_MONEY ="scm/chkinm/payMoney";//添加付款页面
	public static final String ADD_BILL ="scm/chkinm/addBill";//添加支票页面
	public static final String ADD_DELIVER_MONEY ="scm/chkinm/addDeliverMoney";//添加期初金额页面
	
	public static final String REPORT_URL="/report/chkinm/chkinmReport.jasper";//报表地址
	public static final String REPORT_URL_ZB="/report/chkinm/chkinmzbReport.jasper";//报表地址
	
	public static final String pur="9928";//采购入库
	public static final String in="in"; 
	public static final String conk="inout"; 
	//*********采购入库标识******
	public static final String explanIn="9905"; 
	public static final String inVententoryIn="9901";//盘盈入库
	
	public static final String IMPORT_CHKINM = "scm/chkinm/importChkinm"; //导入入库单 wjf
	public static final String MIS_IMPORT_CHKINM = "mis/chkinm/importChkinm"; //导入入库单 wjf mis
	public static final String IMPORT_RESULT = "scm/chkinm/importResult";
	public static final String TABLE_CHKINMZF_CX = "scm/chkinm/tableChkinmzfCx";
	public static final String SYNCHRONCDATA = "scm/chkinm/synchroNCData";//NC同步信息页面

}
