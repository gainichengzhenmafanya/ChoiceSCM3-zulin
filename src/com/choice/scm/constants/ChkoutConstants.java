package com.choice.scm.constants;
/**
 * @author yp
 */
public class ChkoutConstants {

	public static final String MANAGE_CHKOUT = "scm/chkout/listChkout";
	public static final String MIS_MANAGE_CHKOUT = "mis/chkout/listChkout";//MIS
	public static final String CHECKED_CHKOUT = "scm/chkout/listCheckedChkout";
	public static final String MIS_CHECKED_CHKOUT = "mis/chkout/listCheckedChkout";//MIS
	public static final String ADD_CHKOUT = "scm/chkout/saveChkout";
	public static final String MIS_ADD_CHKOUT = "mis/chkout/saveChkout";//MIS
	public static final String UPDATE_CHKOUT = "scm/chkout/saveChkout";
	public static final String MIS_UPDATE_CHKOUT = "mis/chkout/saveChkout";//MIS
	public static final String PRINT_CHKOUT = "scm/chkout/printChkout";
	public static final String SEARCH_CHECKED_CHKOUT = "scm/chkout/searchCheckedChkout";
	public static final String TABLE_CHKOUT_CX="scm/chkout/tableChkoutCx";//添加冲消物资界面
	public static final String TABLE_CHKOUT_PC="scm/chkout/tableChkoutPc";//添加查询批次出库界面
	
	public static final String REPORT_PRINT_URL="/report/chkout/Chkout_print.jasper";//报表地址
	public static final String REPORT_EXP_URL="/report/chkout/Chkout_exp.jasper";
	
	
	public static final String inVententoryOut="9910"; //盘亏出库
	public static final String inVententoryEndOut="盘亏后出库";
	public static final String IMPORT_CHKOUT = "scm/chkout/importChkout"; //导入出库单 wjf
	public static final String IMPORT_RESULT = "scm/chkout/importResult";
	
	public static final String LIST_DIFF = "scm/chkout/listDiff";//总部配送和门店验收差异 wjf
}
