package com.choice.scm.constants;
/**
 * @author yp
 */
public class PositnConstants {

	public static final String LIST_POSITN_TYPE = "scm/positn/listPositnType";
	public static final String LIST_POSITN_TYPE_FORSE = "scm/positn/listPositnTypeForse";//通过选择框选择单一仓库使用(左侧)
	public static final String LIST_POSITN_FORSE="scm/positn/listPositnForse";//通过选择框选择单一仓库使用（右侧）
	public static final String LIST_POSITN = "scm/positn/listPositn";
	public static final String CHOOSE_POSITN = "scm/positn/choosePositn";
	public static final String ADD_POSITN = "scm/positn/addPositn";
	public static final String UPDATE_POSITN = "scm/positn/updatePositn";
	public static final String COPY_POSITN = "scm/positn/copyPositn";
	public static final String TABLE_ACCOUNT = "scm/positn/tablePositn";
	public static final String SELECT_POSITN = "scm/positn/selectPositn";
	public static final String ADD_POSITNBATCH="scm/positn/addPositnBatch";
	public static final String SELECT_ONEPOSITN="scm/positn/selectOnePositn";//选择单条仓位
	public static final String SELECT_FIRM="scm/positn/selectFirm";//选择单条仓位
	public static final String SEARCH_ALLPOSITN = "scm/positn/searchAllPositn";
	public static final String SELECT_ONE_TO_ONEPOSITN="scm/positn/selectOneTOnePositn";//同一类别仓位单选
	public static final String SELECT_ONE_TO_MANYPOSITN="scm/positn/selectOneTManyPositn";//同一类别仓位多选

	public static final String LIST_POSITN_TYPE_FORSE_FIRM = "scm/positn/listPositnTypeForseFirm";//通过选择框选择单一仓库使用(左侧)
	public static final String LIST_POSITN_FORSE_FIRM="scm/positn/listPositnForseFirm";//通过选择框选择单一仓库使用（右侧）
	
	//仓位类型
	public static final String type_1 = "1201";
	public static final String type_2 = "1202";
	public static final String type_3 = "1204";
	public static final String type_4 = "1205";
	public static final String type_5 = "1203";
	public static final String type_6 = "加盟店";
	public static final String type_7 = "合作店";
	public static final String type_8 = "其他店";
	
	public static final String RELATION_DEPT = "scm/positn/relation_dept";
	
	/**
	 * 新选择仓位公用页面
	 */
	public static final String SELECT_POSITN_SUPER = "scm/positn/positnsuper/selectPositnSuper";
	public static final String SELECT_ONEPOSITN_SUPER="scm/positn/positnsuper/selectOnePositnSuper";//选择单条仓位
	public static final String SELECT_ONE_TO_ONEPOSITN_SUPER="scm/positn/positnsuper/selectOneTOnePositnSuper";//同一类别仓位单选
	public static final String SELECT_ONE_TO_MANYPOSITN_SUPER="scm/positn/positnsuper/selectOneTManyPositnSuper";//同一类别仓位多选
	
	public static final String CHOOSE_DEPARTMENTLIST = "scm/positn/positnsuper/chooseDepartment"; 
	
	public static final String SETDELIVER = "scm/positn/setDeliver";//同步虚拟供应商页面
	
}
