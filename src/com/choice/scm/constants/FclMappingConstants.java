package com.choice.scm.constants;

/**
 * 描述：财务科目映射设置
 * 日期：2014年1月12日 上午11:08:37
 * 作者：毛明武mmw
 * 文件：FclMappingConstants.java
 */
public class FclMappingConstants {
	//view常量
	public static final String LIST_FCLMAPPING = "/scm/fclMapping/listFclmapping";
	public static final String EDIT_FCLMAPPING = "/scm/fclMapping/editFclmapping";
//	public static final String TOCHOOSELIST_FCLMAPPING = "/scm/fclMapping/chooseFclMapping";
	public static final String TABLE_DELIVERS="scm/fclMapping/tableDelivers";//选择供应商
	public static final String TABLE_POSITNS="scm/fclMapping/tablePositns";//选择仓位
	public static final String TABLE_CUSTOMER="scm/fclMapping/tableCustomer";//选择客户
	public static final String TABLE_WAREHOUSE="scm/fclMapping/tableWareHouse";//选择仓库
}
