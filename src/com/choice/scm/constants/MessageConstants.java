package com.choice.scm.constants;

public class MessageConstants {

	//页面跳转
	public static final String LIST_EMAIL="scm/email/listEmail";
	public static final String ADD_EMAIL="scm/email/addEmail";

	public static final String SEE_EMAIL="scm/email/showEmail";
	public static final String ACCOUNT="scm/email/listAccount";
}
