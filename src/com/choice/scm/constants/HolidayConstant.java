package com.choice.scm.constants;

public class HolidayConstant {

	public static final String LIST_HOLIDAY="scm/holiday/listHoliday";
	public static final String ADD_HOLIDAY="scm/holiday/addHoliday";
	public static final String REPORT_URL_HOLIDAY = "report/holiday/listHoliday.jasper";
}
