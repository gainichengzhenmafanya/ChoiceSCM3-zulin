package com.choice.scm.constants;

public class ChkstomConstants {

	//页面跳转
	public static final String REPORT_PRINT_URL="report/chkstom/chkstomReport.jasper";
	public static final String TABLE_CHKSTOM="scm/chkstom/tableChkstom";
	public static final String MIS_TABLE_CHKSTOM="mis/chkstom/tableChkstom";//MIS
	public static final String TABLE_CHECKED_CHKSTOM="scm/chkstom/listCheckedChkstom";
	public static final String MIS_TABLE_CHECKED_CHKSTOM="mis/chkstom/listCheckedChkstom";//MIS
	public static final String SEARCH_CHKSTOM="scm/chkstom/searchChkstom";
	public static final String MIS_SEARCH_CHKSTOM="mis/chkstom/searchChkstom";//MIS
	public static final String SEARCH_UPLOAD="scm/chkstom/searchUpload";
	public static final String MIS_SEARCH_UPLOAD="mis/chkstom/searchUpload";//MIS
	public static final String SEARCH_CHECKED_CHKSTOM="scm/chkstom/tableCheckedChkstom";
	public static final String MIS_SEARCH_CHECKED_CHKSTOM="mis/chkstom/tableCheckedChkstom";//MIS
	
	
	
	public static final String TABLE_CHKSTOM_WFZ="scm/chkstom/WFZtableChkstom";//五芳斋特有
	public static final String IMPORT_CHKSTOM = "scm/chkstom/importChkstom";//报货单导入wjf
	public static final String IMPORT_RESULT = "scm/chkstom/importResult";//报货单导入wjf
	
	public static final String TABLE_CHKSTOM_NEW="scm/chkstom/tableChkstom_new";//新报货流程 2014.12.27wjf
	public static final String SEARCH_CHECKED_CHKSTOM_NEW="scm/chkstom/tableCheckedChkstom_new";//查询已审核报货单新wjf
	public static final String MIS_TABLE_CHKSTOM_NEW="mis/chkstom/tableChkstom_new";//MIS新报货流程wjf
	
	public static final String TABLE_SAFE_CHKSTOM="scm/chkstom/tableSafeChkstom";//安全库存报货
	public static final String TABLE_SAFE_CHKSTOM_MIS="mis/chkstom/tableSafeChkstomMis";//安全库存报货mis
	
	public static final String LIST_CHKSTOM_STATE= "scm/chkstom/listChkstomState";//九毛九报货单状态查询
}
