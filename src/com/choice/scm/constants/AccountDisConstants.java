package com.choice.scm.constants;

/***
 * 报货分拨特殊权限
 * @author 王吉峰
 *
 */
public class AccountDisConstants {

	//页面跳转
	public static final String OTHER_PERMISSION = "scm/accountDis/otherPermission";

}
