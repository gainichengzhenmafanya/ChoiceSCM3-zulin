package com.choice.scm.constants;

public class DeliverPlatConstants {

	//页面跳转
	public static final String LIST_CHKSTOM="scm/deliverPlat/listChkstom";
	
	public static final String LIST_CHKSTOMSUM="scm/deliverPlat/listChkstomSum";
	
	public static final String LIST_INSPECTION="scm/deliverPlat/listInspection";
	
	public static final String LIST_INSPECTIONSUM="scm/deliverPlat/listInspectionSum";
	
	public static final String LIST_INSPECTIONNORMAL="scm/deliverPlat/listInspectionNormal";
	
	public static final String LIST_INSPECTIONNORMALSUM="scm/deliverPlat/listInspectionNormalSum";
	
	public static final String REPORT_INDENT_URL="report/scm/deliverPlat/indent.jasper";//分拨单
	public static final String REPORT_INSPECTION_URL="report/scm/deliverPlat/inspection.jasper";//验货单
	public static final String REPORT_DELIVERY_URL="report/scm/deliverPlat/delivery.jasper";//配送单	

	public static final String LIST_CHKINMBYDELIVER = "scm/deliverPlat/listChkinmByDeliver";
	
	public static final String REPORT_SHOW_CHKINDETAILS="scm/deliverPlat/chkinDetailS";
	public static final String REPORT_SHOW_CHKINDETAILSUM="scm/deliverPlat/chkinDetailSum";
	
	public static final String REPORT_PRINT_URL_CHKINDETAILSUM="/report/scm/deliverPlat/chkinmdetailsum.jasper";//报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILSUM="/report/scm/deliverPlat/chkinmdetailsum.jasper";
	
	public static final String REPORT_PRINT_URL_CHKINDETAILS="/report/scm/deliverPlat/chkinmdetailquery.jasper";//报表地址
	public static final String REPORT_EXP_URL_CHKINDETAILS="/report/scm/deliverPlat/chkinmdetailquery.jasper";
	
	//九毛九单据验货
	public static final String LIST_ARRIVALM = "scm/deliverPlat/listArrivalmDeliver";//直配
	public static final String LIST_ARRIVALM_SUM = "scm/deliverPlat/listArrivalmDeliverSum";//直配查询汇总
	public static final String LIST_ARRIVALD = "scm/deliverPlat/listArrivaldDeliver";
		
}
