package com.choice.scm.constants;

public class SppriceSaleDemoConstants {

	//页面跳转
	public static final String TABLE_SPPRICE_SALE_DEMO="scm/sppriceSaleDemo/treeSppriceSaleDemo";
	public static final String LIST_SPPRICE_SALE_DEMO="scm/sppriceSaleDemo/listSppriceSaleDemo";
	public static final String SPPRICE_SALE_CHECK="scm/sppriceSaleDemo/sppriceSaleCheck";
	public static final String LIST_SPPRICESALE="scm/sppriceSaleDemo/listSppriceSale";
	public static final String ADD_SPPRICESALEDEMO="scm/sppriceSaleDemo/addSppriceSaleDemo";
	public static final String UPDATE_SPPRICESALEDEMO="scm/sppriceSaleDemo/updateSppriceSaleDemo";
	public static final String IMPORT_SPPRICESALE = "scm/sppriceSaleDemo/importSppriceSale";//导入界面 wjf
	public static final String IMPORT_RESULT = "scm/sppriceSaleDemo/importResult";
}
