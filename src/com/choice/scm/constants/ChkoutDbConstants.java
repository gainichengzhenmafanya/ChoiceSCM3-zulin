package com.choice.scm.constants;
/**
 * @author yp
 */
public class ChkoutDbConstants {

	public static final String MANAGE_CHKOUT = "mis/chkout/listChkoutDb";
	public static final String CHECKED_CHKOUT = "mis/chkout/listCheckedChkoutDb";
	public static final String ADD_CHKOUT = "mis/chkout/saveChkoutDb";
	public static final String UPDATE_CHKOUT = "mis/chkout/saveChkoutDb";
	public static final String MIS_UPDATE_CHKOUT = "mis/chkout/saveChkout";//MIS
	public static final String PRINT_CHKOUT = "scm/chkout/printChkout";
	public static final String SEARCH_CHECKED_CHKOUT = "mis/chkout/searchCheckedChkoutDb";
	public static final String TABLE_CHKOUT_CX="scm/chkout/tableChkoutCx";//添加冲消物资界面
	
	public static final String REPORT_PRINT_URL="/report/chkout/chkout_print.jasper";//报表地址
	public static final String REPORT_EXP_URL="/report/chkout/chkout_exp.jasper";
	
	
	public static final String inVententoryOut="9910"; 
	public static final String inVententoryEndOut="盘亏后出库";
	public static final String IMPORT_CHKOUT = "scm/chkout/importChkout"; //导入出库单 wjf
	public static final String IMPORT_RESULT = "scm/chkout/importResult";
}
