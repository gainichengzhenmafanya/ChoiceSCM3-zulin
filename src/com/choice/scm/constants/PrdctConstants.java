package com.choice.scm.constants;

public class PrdctConstants {

	//页面跳转
	public static final String LIST_PRDCTBOM="scm/prdctbom/listPrdctbom";
	public static final String TABLE_PRDCTBOM="scm/prdctbom/tablePrdctbom";
	public static final String ADD_PRDCTBOM="scm/prdctbom/savePrdctbom";
	public static final String UPD_PRDCTBOM="scm/prdctbom/updtPrdctbom";
	public static final String LIST_PRDCTBOM_FIRM="scm/prdctbom/listPrdctbomFirm";
	public static final String REPORT="report/prdctbom/prdctbomReport.jasper";//产品bom报表
	
	public static final String SELECT= "scm/prdctbom/selectBomSupply";//物资存在的产品以及物资用量页面
	//复制成本卡
	public static final String PRDCTTYP = "scm/prdctbom/prdctTyp";
}
