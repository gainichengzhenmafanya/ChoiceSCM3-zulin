package com.choice.scm.constants;

public class CodeDesConstants {

	//页面跳转
	public static final String LIST_CODEDES="scm/codeDes/listCodeDes";
	public static final String TABLE_CODEDES="scm/codeDes/tableCodeDes";
	public static final String SAVE_CODEDES="scm/codeDes/saveCodeDes";
	public static final String UPDATE_CODEDES="scm/codeDes/updateCodeDes";
	public static final String SELECTCLASSIFICATION="scm/supplyMng/selectClassification";
	
	//税率
	public static final String SHOW_TAX_LIST="scm/codeDes/tableCodeDes_tax";
	public static final String SHOW_TAX_ADD="scm/codeDes/saveTax";
	public static final String SHOW_TAX_UPDATE="scm/codeDes/updateTax";
	
	//报货类别添加报货方式
	//查询界面
	public static final String TABLE_CODEDES_T="scm/codeDes/tableCodeDes_typ";
	//新增页面
	public static final String SAVE_CODEDES_T="scm/codeDes/saveCodeDes_typ";
	//修改页面
	public static final String UPDATE_CODEDES_T="scm/codeDes/updateCodeDes_typ";
	//查询界面--仓位类型
	public static final String TABLE_CODEDES_S="scm/codeDes/tableCodeDes_posint";
}
