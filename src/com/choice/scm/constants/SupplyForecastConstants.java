package com.choice.scm.constants;

public class SupplyForecastConstants {

	public static final String LIST_SUPPLYMNG="scm/supplyForecast/listSupplyForecast"; //物资管理的物资树结构
	public static final String TABLE_SUPPLYMNG="scm/supplyForecast/tableSupplyForecast"; //物资管理的物资列表
	public static final String SET_ABC="scm/supplyForecast/setABC"; // 选择物资对应的ABC类型
}
