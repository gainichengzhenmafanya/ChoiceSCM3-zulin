package com.choice.scm.constants;

/**
 * 应收应付
 * Created by mc on 14-12-27.
 */
public class AccruedConstants {
    /**
     * 应付款汇总 首页
     */
    public static final String ACCPAYSUM_LIST="scm/accrued/accpaysum/accpaysumList";
    /**
     * 应收款汇总 首页
     */
    public static final String RECSUM_LIST="scm/accrued/recsum/recsumList";
    /**
     * 分店结算 首页
     */
    public static final String BRASET_LIST="scm/accrued/braset/brasetList";
    /**
     * 分店结算 支付明细
     */
    public static final String BRASET_ITEM_LIST="scm/accrued/braset/brasetItem";
    /**
     * 付款界面
     */
    public static final String PAY_MONEY="scm/accrued/braset/payMoney";
    /**
     * 期初界面
     */
    public static final String ADD_MONEY="scm/accrued/braset/addMoney";
}
