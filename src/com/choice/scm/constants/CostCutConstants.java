package com.choice.scm.constants;

public class CostCutConstants {

	//页面跳转
	public static final String LIST_COSTCUT="scm/costcut/listCostCut";
	public static final String TABLE_COSTCUT="scm/costcut/tableCostCut";
	public static final String LIST_SEARCH="scm/costcut/searchChkout";
	public static final String UPD_PRDCTBOM="scm/prdctbom/updtPrdctbom";
	//分店
	public static final String MIS_LIST_COSTCUT="mis/costMng/listCostCut";
	public static final String MIS_LIST_COSTCUT2="mis/costMng/listCostCut2";
	public static final String MIS_TABLE_COSTCUT="mis/costMng/tableCostCut";
	public static final String MIS_TABLE_COSTCUT2="mis/costMng/tableCostCut2";
	public static final String MIS_LIST_SEARCH="mis/costMng/searchChkout";
	//加工间理论成本核减
	public static final String LIST_EXCOSTCUT="scm/costcut/listExCostCut";
}
