package com.choice.scm.constants;

public class SupplyDefaultDeliverConstants {

	public static final String LIST_SUPPLYDEFAULTDELIVER="scm/defaultPositn/listSupplyDefaultDeliver"; //物资管理的物资树结构
	
	
	public static final String TABLE_SUPPLYDEFAULTDELIVER="scm/defaultPositn/tableSupplyDefaultDeliver"; //物资管理的物资列表
	
	public static final String TABLE_SUPPLYWORKSHOP="scm/defaultPositn/tableSupplyWorkShop"; //出品加工间
	
	public static final String SELECT_SUPPLYPOSITN = "scm/defaultPositn/selectSupplyPositn";
}
