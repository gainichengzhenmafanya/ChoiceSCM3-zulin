package com.choice.scm.constants;

public class HolidayBohConstant {
	//加载主页面
	public static final String LIST_HOLIDAY = "/scm/holidayBoh/listHoliday";
	//加载新增页面
	public static final String SAVE_HOLIDAY="/scm/holidayBoh/saveHoliday";
	//加载批量新增页面
	public static final String GENERATE_HOLIDAY="/scm/holidayBoh/generateHoliday";
	//加载修改页面
	public static final String UPDATE_HOLIDAY="/scm/holidayBoh/updateHoliday";
}
