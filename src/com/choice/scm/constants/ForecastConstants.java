package com.choice.scm.constants;

public class ForecastConstants {

	//页面跳转(分店用)
	public static final String MIS_LIST_COSTCTR="mis/forecast/listCostCTR";//菜品点击率表
	public static final String MIS_LIST_FORECAST="mis/forecast/listForeCast";//营业预估
	public static final String MIS_LIST_PLAN="mis/forecast/listPlan";//菜品销售计划
	public static final String MIS_LIST_SALECMP="mis/forecast/listSaleCmp";//营业预估实际对比
	public static final String MIS_LIST_COSTCMP="mis/forecast/listCostCmp";//菜品预估实际对比
	public static final String MIS_LIST_CMP="mis/forecast/listCmp";//菜品预估沽清对比
	
	public static final String REPORT_FORECAST="report/forecast/itemUseReport.jasper";//菜品点击率表
	public static final String REPORT_CAST="report/forecast/posSalePlanReport.jasper";//营业预估
	public static final String REPORT_PLAN="report/forecast/posItemPlanReport.jasper";//菜品销售计划
	public static final String REPORT_SALECMP="report/forecast/saleCmpReport.jasper";//营业预估实际对比
	public static final String REPORT_COSTCMP="report/forecast/costCmpReport.jasper";//菜品预估实际对比
	public static final String REPORT_CMP="report/forecast/cmpReport.jasper";//菜品预估沽清对比
	
	//页面跳转(总店用)
	public static final String LIST_COSTCTR="scm/forecast/listCostCTR";//菜品点击率表
	public static final String LIST_FORECAST="scm/forecast/listForeCast";//营业预估
	public static final String LIST_PLAN="scm/forecast/listPlan";//菜品销售计划
	public static final String LIST_SALECMP="scm/forecast/listSaleCmp";//营业预估实际对比
	public static final String LIST_COSTCMP="scm/forecast/listCostCmp";//菜品预估实际对比
	public static final String LIST_CMP="scm/forecast/listCmp";//菜品预估沽清对比
}
