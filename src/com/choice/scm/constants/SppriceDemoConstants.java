package com.choice.scm.constants;

public class SppriceDemoConstants {

	public static final String LIST_SPPRICEDEMO = "scm/sppriceDemo/listSppriceDemo";
	public static final String ADD_SPPRICEDEMO = "scm/sppriceDemo/addSppriceDemo";
	public static final String ADD_BATCHDEMO = "scm/sppriceDemo/addBatchDemo";
	public static final String MANAGE_SPPRICEDEMO = "scm/sppriceDemo/manageSppriceDemo";
	public static final String UPDATE_PRICE = "scm/sppriceDemo/updatePrice";
	public static final String UPDATE_DATE = "scm/sppriceDemo/updateDate";
}
