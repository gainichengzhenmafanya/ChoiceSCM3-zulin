package com.choice.scm.constants;

public class WapFirmMisConstants {
	public static final String WAPFIRMMIS_LOGIN="scm/wapFirmMis/login";
	public static final String LIST_CHKSTODEMO="scm/wapFirmMis/listChkstodemo";
	public static final String LIST_CHKSTODEMO_TODAY="scm/wapFirmMis/listChkstodOfToday";
	public static final String SET_FIRM = "scm/wapFirmMis/setFirm";
}
