package com.choice.scm.constants;

public class ScheduleConstants {

	//页面跳转
	public static final String LIST_SCHEDULE="scm/schedule/listSchedule";
	
	//添加配送班表
	public static final String ADD_SCHEDULE="scm/schedule/addSchedule";
	//修改配送班表
	public static final String UPDATE_SCHEDULE="scm/schedule/updateSchedule";
	public static final String LIST_POSITN_SCHEDULE = "scm/schedule/listPositnSchedule";
	public static final String LIST_SCHEDULE_POSITN = "scm/schedule/listSchedulePositn";
	
	//右边窗口跳转
	public static final String LIST_ACCTSCHEDULE="scm/schedule/tableSchedule";
	//添加配送班表明细
	public static final String ADD_DETAIL="scm/schedule/addScheduleD";
	//修改明细
	public static final String UPDATE_DETAIL="scm/schedule/updateScheduleD";

	public static final String LIST_CALENDAR="scm/schedule/calendar";
	//添加配送班表明细
	public static final String ADD_DETAIL_C="scm/schedule/addScheduleD_c";
	//修改明细
	public static final String UPDATE_DETAIL_C="scm/schedule/updateScheduleD_c";
	//按报货类别复制明细
	public static final String COPY_DETAIL_C="scm/schedule/copyScheduleD_c";
	//配送班表 复制页面
	public static final String COPY_DETAIL = "scm/schedule/copyScheduleD";
	public static final String SHOW_FIRM_SCHEDULE = "scm/schedule/showFirmSchedule";
	
}
