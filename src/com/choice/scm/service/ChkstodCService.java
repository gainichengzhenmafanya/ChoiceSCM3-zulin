package com.choice.scm.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.persistence.ChkstodCMapper;

@Service
public class ChkstodCService {

	@Autowired
	private ChkstodCMapper chkstodCMapper;

	private final transient Log log = LogFactory.getLog(ChkstodService.class);
		
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod) throws CRUDException
	{
		try {
			return chkstodCMapper.findByChkstoNo(chkstod);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}	
}