package com.choice.scm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.shiro.ScmLoginToken;
import com.choice.framework.shiro.handler.Handler;
import com.choice.framework.shiro.tools.SpringUtils;
import com.choice.framework.shiro.tools.UserSpace;

/**
 * usedfor：加载配置参数回调函数
 * Created by javahao on 2017/8/8.
 * auth：JavaHao
 */
@Service("loginSuccessHandler")
public class LoadParamsHandler implements Handler {
    private static final Logger LOG = LoggerFactory.getLogger(LoadParamsHandler.class);
    @Override
    public boolean execute() {
        // 增加用户登录加载参数（系统、企业、用户）先redis后DB
        try {
            ScmLoginToken scmLoginToken = UserSpace.getLoginToken();
            LOG.debug("登录成功加载用户数据, 企业编码[{}], 用户名[{}]", scmLoginToken.getPkGroup(), scmLoginToken.getUsername());
            SpringUtils.getBean(SysParamService.class).loginCacheInitialized(scmLoginToken.getPkGroup(), scmLoginToken.getUsername());
        } catch (CRUDException e) {
            LOG.error(e.getMessage(), e);
        }
        return true;
    }
}
