package com.choice.scm.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.InventoryDemoFirm;
import com.choice.scm.domain.InventoryDemod;
import com.choice.scm.domain.InventoryDemom;
import com.choice.scm.persistence.InventoryDemomMapper;
import com.choice.scm.persistence.MainInfoMapper;

/**
 * 盘点模板
 * @author 孙胜彬
 */
@Service
public class InventoryDemomService {

	@Autowired
	private InventoryDemomMapper inventoryDemomMapper;
	@Autowired
	private PageManager<InventoryDemod> pageManager;
	@Autowired
	private PageManager<InventoryDemom> pageManager1;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	private static Logger log = Logger.getLogger(ChkstoDemomService.class);
	
	/**
	 * 盘点模板子表查询
	 * @param inventoryDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<InventoryDemod> listInventoryDemod(InventoryDemod inventoryDemod, Page page) throws CRUDException {
		try{
			return pageManager.selectPage(inventoryDemod, page, InventoryDemomMapper.class.getName()+".listInventoryDemod");
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	/**
	 * 盘点模板子表查询
	 * @param inventoryDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<InventoryDemod> listInventoryDemod(InventoryDemod inventoryDemod) throws CRUDException {
		try{
			return inventoryDemomMapper.listInventoryDemod(inventoryDemod);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	/**
	 * 盘点模板主表查询
	 * @param inventoryDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<InventoryDemom> listInventoryDemom(InventoryDemom inventoryDemom, Page page) throws CRUDException {
		try{
			return pageManager1.selectPage(inventoryDemom, page, InventoryDemomMapper.class.getName()+".listInventoryDemom");
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	/**
	 * 根据分店查询使用的所有模板
	 * @param inventoryDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<InventoryDemom> listInvenDemomByFirm(InventoryDemom inventoryDemom, Page page) throws CRUDException {
		try{
			return pageManager1.selectPage(inventoryDemom, page, InventoryDemomMapper.class.getName()+".listInvenDemomByFirm");
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	/**
	 * 盘点模板适用分店查询
	 * @param inventoryDemoFirm
	 * @param page
	 * @return
	 */
	public List<InventoryDemoFirm> listInventoryDemoFirm(InventoryDemoFirm inventoryDemoFirm){
		try{
			return inventoryDemomMapper.listInventoryDemoFirm(inventoryDemoFirm);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}
	
	/**
	 * 新增报货单模板子表
	 * @param inventoryDemod
	 */
	public void saveInventoryDemod(InventoryDemod inventoryDemod) throws CRUDException{
		try{
			inventoryDemomMapper.saveInventoryDemod(inventoryDemod);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 新增报货单模板主表
	 * @param inventoryDemom
	 */
	public void saveInventoryDemom(InventoryDemom inventoryDemom) throws CRUDException{
		try{
			inventoryDemomMapper.saveInventoryDemom(inventoryDemom);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 新增报货单模板适用分店
	 * @param inventoryDemoFirm
	 */
	public void saveInventoryDemoFirm(InventoryDemoFirm inventoryDemoFirm) throws CRUDException{
		try{
			deleteInventoryDemoFirm(inventoryDemoFirm.getChksto());
			List<String> isList=Arrays.asList(inventoryDemoFirm.getChksto().split(","));
			List<String> idList=Arrays.asList(inventoryDemoFirm.getFirm().split(","));
			if(isList.size() !=0){ 
				for(int i=0;i<isList.size();i++){
					inventoryDemoFirm.setChkstodemono(Integer.parseInt(isList.get(i)));
					for(int j=0;j<idList.size();j++){
						inventoryDemoFirm.setFirm(idList.get(j));
						inventoryDemomMapper.saveInventoryDemoFirm(inventoryDemoFirm);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 修改报货单模板子表
	 * @param inventoryDemod
	 */
	public void updateInventoryDemod(InventoryDemod inventoryDemod) throws CRUDException{
		try{
			inventoryDemomMapper.updateInventoryDemod(inventoryDemod);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 修改报货单模板主表
	 * @param inventoryDemom
	 */
	public void updateInventoryDemom(InventoryDemom inventoryDemom) throws CRUDException {
		try{
			inventoryDemomMapper.updateInventoryDemom(inventoryDemom);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 删除盘点模板子表
	 * @param inventoryDemod
	 */
	public void deleteInventoryDemod(String ids) throws CRUDException{
		try{
			List<String> idList=Arrays.asList(ids.split(","));
			inventoryDemomMapper.deleteInventoryDemod(idList);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 删除盘点模板主表
	 * @param inventoryDemom
	 */
	public void deleteInventoryDemom(String ids) throws CRUDException{
		try{
			List<String> idList=Arrays.asList(ids.split(","));
			inventoryDemomMapper.deleteInventoryDemom(idList);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	/**
	 * 删除盘点模板适用分店
	 * @param inventoryDemom
	 */
	public void deleteInventoryDemoFirm(String ids) throws CRUDException{
		try{
			List<String> idList=Arrays.asList(ids.split(","));
			inventoryDemomMapper.deleteInventoryDemoFirm(idList);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
	}
	
	/**
	 * 新增盘点模板
	 */
	public String saveInventoryDemomm(InventoryDemom inventoryDemom,HttpSession session) throws CRUDException{
		String acct = session.getAttribute("ChoiceAcct").toString();
		String accountName = session.getAttribute("accountName").toString();
		String yearr = mainInfoMapper.findYearrList().get(0);
		Integer chkstodemono = inventoryDemomMapper.findMaxNo();
		try{
			if(inventoryDemom !=null){
				inventoryDemom.setAcct(acct);
			    inventoryDemom.setYearr(yearr);
			    inventoryDemom.setChkstodemono(chkstodemono);
			    inventoryDemom.setMaded(DateFormat.formatDate(new Date(), "yyyyMMdd"));
			    inventoryDemom.setMadet(DateFormat.getStringByDate(new Date(), "HH:mm:ss"));
			    inventoryDemom.setMadeby(accountName);
			    inventoryDemomMapper.saveInventoryDemom(inventoryDemom);
				for(int i=0;i<inventoryDemom.getInventoryDemod().size();i++){
					InventoryDemod demod=inventoryDemom.getInventoryDemod().get(i);
					demod.setAcct(acct);
					demod.setYearr(yearr);
					demod.setChkstodemono(chkstodemono);
					inventoryDemomMapper.saveInventoryDemod(demod);
				}
				return "1";
			}else{
				return "-1";
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return "-1";
	}
	
	/**
	 * 修改盘点模板
	 */
	public int updInventoryDemom(InventoryDemom inventoryDemom) throws CRUDException{
		try{
			if(inventoryDemom !=null){
				inventoryDemomMapper.updateInventoryDemom(inventoryDemom);
				List<String> idList = new ArrayList<String>();
				idList.add(inventoryDemom.getChkstodemono()+"");
				inventoryDemomMapper.deleteInventoryDemod(idList);
				for(int i=0;i<inventoryDemom.getInventoryDemod().size();i++){
					InventoryDemod demod=inventoryDemom.getInventoryDemod().get(i);
					demod.setAcct(inventoryDemom.getAcct());
					demod.setYearr(inventoryDemom.getYearr());
					demod.setChkstodemono(inventoryDemom.getChkstodemono());
					inventoryDemomMapper.saveInventoryDemod(demod);
				}
				return 1;
			}else{
				return -1;
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 查找所有的盘点模板标题
	 * @param inventoryDemom
	 * @return
	 * @throws CRUDException
	 */
	public List<InventoryDemom> findAllTitle(InventoryDemom inventoryDemom) throws CRUDException
	{
		try {
			return inventoryDemomMapper.findAllTitle(inventoryDemom);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
