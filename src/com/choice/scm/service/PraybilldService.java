package com.choice.scm.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Praybilld;
import com.choice.scm.persistence.PraybilldMapper;

@Service
public class PraybilldService {

	@Autowired
	private PraybilldMapper praybilldMapper;

	private final transient Log log = LogFactory.getLog(PraybilldService.class);
	
	
	/**
	 * 根据单号查询
	 * @param praybilld
	 * @return
	 * @throws CRUDException
	 */
	public List<Praybilld> findByPraybillmNo(Praybilld praybilld) throws CRUDException
	{
		try {
			return praybilldMapper.findByPraybillmNo(praybilld);
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的请购单
	 * @return
	 * @throws CRUDException
	 */
	public List<Praybilld> findAllPraybilld(Praybilld praybilld) throws CRUDException {
		try {
			return praybilldMapper.findAllPraybilld(praybilld);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}