package com.choice.scm.service;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnRole;
import com.choice.scm.persistence.PositnRoleMapper;

@Service
public class PositnRoleService {

	private static Logger log = Logger.getLogger(PositnRoleService.class);
	
	@Autowired
	private PositnRoleMapper positnRoleMapper;
	
	/**
	 * 根据角色 查询  仓位
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public List<PositnRole> findRolePositnList(String roleId) throws CRUDException{
		try{
			return positnRoleMapper.findRolePositnList(roleId);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  保存  角色下的  所有仓位   权限
	 * @param roleId
	 * @param listPositnId
	 * @throws CRUDException
	 */
	public void savePositnRole(String roleId,List<String> listPositnId) throws CRUDException{
		try{
			positnRoleMapper.deleteRolePositnByRoleId(roleId);//先删除该角色下的所有仓位权限
			//再逐条添加该角色的仓位权限
			 for (int i = 0; i < listPositnId.size(); i++) {
				 PositnRole positnRole=new PositnRole(CodeHelper.createUUID(),roleId,listPositnId.get(i));
				 positnRoleMapper.saveRolePositn(positnRole);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询权限下            所有的分店和仓位信息
	 */
	public List<Positn> findAllPositn(String acct, String accountId) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("accountId", accountId);
			map.put("acct", acct);
//			return positnPositnMapper.findPositn(map);
			return positnRoleMapper.findPositnByAccount(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存  改账号  的所属分店    可能多个账号
	 * @param accountIds
	 * @param poositnCode
	 * @throws CRUDException
	 */
	public void saveFrim(String accountIds,String poositnCode) throws CRUDException{
		try{ 
			HashMap< String,Object> map =new HashMap<String, Object>();
			map.put("accountIds", CodeHelper.replaceCode(accountIds));
			map.put("poositnCode", poositnCode);
			positnRoleMapper.saveFrim(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
