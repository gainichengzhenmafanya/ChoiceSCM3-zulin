package com.choice.scm.service.mis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.persistence.mis.MisChkoutdMapper;
import com.choice.scm.persistence.mis.MisChkoutmMapper;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ReadChkoutExcel;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.FileWorked;

@Service
public class MisChkoutService {

	@Autowired
	private MisChkoutdMapper misChkoutdMapper;
	@Autowired
	private MisChkoutmMapper misChkoutmMapper;
	@Autowired
	private PageManager<Chkoutm> pageManager;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CodeDesMapper codeDesMapper;
	private final transient Log log = LogFactory.getLog(MisChkoutService.class);
	
	/**
	 * 分店MIS验收查询
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkoutd> findChkoutFromMis(Map<String,Object> map) throws CRUDException{
		try{
			Supply supply=(Supply) map.get("supply");
			if(null!=supply.getGrp() && !"".equals(supply.getGrp())){
				List<String> grpList = Arrays.asList(supply.getGrp().split(","));
				map.put("grpList", grpList);
			}
			log.warn("分店验收查询：\n"+ 
					"grp:" + map.get("grpList")+
					",deliver:" + map.get("deliver")+
					",sp_code:" + map.get("sp_code")+
					",sp_init:" + map.get("sp_init")
					);
			return misChkoutdMapper.findChkoutFromMis(map);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}

	/**
	 * 模糊查询 出库单
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkoutm> findChkoutm(Chkoutm chkoutm,Date bdat,Date edat,Page page,String locale,String db) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			if(null == bdat){
				bdat = new Date();
			}
			if(null == edat){
				edat = new Date();
			}
			map.put("bdat", bdat);
			map.put("edat", edat);
			map.put("locale", locale);
			map.put("chkoutm", chkoutm);
			map.put("db", db);
			log.warn("出库单模糊查询：\n"+
					"bdat:"+DateFormat.getStringByDate(bdat, "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(edat, "yyyy-MM-dd")+
					",chkoutm.checkby:"+chkoutm.getChecby()+
					",chkoutm.firm:"+chkoutm.getChecby()+
					",chkoutm.positn:"+chkoutm.getChecby()+
					",chkoutm.chkoutno:"+chkoutm.getChecby()+
					",chkoutm.vouno:"+chkoutm.getChecby()
					);
			return pageManager.selectPage(map, page, MisChkoutmMapper.class.getName()+".findChkoutm");
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据id获取出库单信息
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public Chkoutm findChkoutmById(Chkoutm chkoutm) throws CRUDException{
		try{
			log.warn("获取出库单："+chkoutm.getChkoutno());
			return misChkoutmMapper.findChkoutmById(chkoutm);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String saveChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			result.put("chkoutno", String.valueOf(chkoutm.getChkoutno()));
//			call add_chkoutm(#{acct},#{yearr},#{chkoutno},#{vouno},#{maded},#{madet},#{positn.code},
//			#{firm.code},#{madeby},#{totalamt},#{typ},#{memo},#{pr,jdbcType=INTEGER,mode=OUT})
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("acct", chkoutm.getAcct());
			map.put("yearr", mainInfoMapper.findYearrList().get(0)+"");
			map.put("chkoutno", chkoutm.getChkoutno());
			map.put("vouno", calChkNum.getNext(CalChkNum.CHKOUT, chkoutm.getMaded()));
			map.put("maded", chkoutm.getMaded());
			map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			map.put("positn", chkoutm.getPositn().getCode());
			map.put("firm", chkoutm.getFirm().getCode());
			map.put("madeby", chkoutm.getMadeby());
			//修改出库单没有总价  wjf
			double totalAmt = 0;
			if(chkoutm.getChkoutd().size() != 0){
				for(Chkoutd chkoutd : chkoutm.getChkoutd()){
					double amount=chkoutd.getAmount();
					double price=chkoutd.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkoutm.setTotalamt(totalAmt);
			map.put("totalamt", chkoutm.getTotalamt());
			map.put("typ", chkoutm.getTyp());
			map.put("memo", chkoutm.getMemo());
			map.put("pr", 0);
			log.warn("新增出库单(主单):\n"+
					"yearr:" + map.get("yearr")+
					",chkoutno:" + map.get("chkoutno")+
					",vouno:" + map.get("vouno")+
					",maded:" + map.get("maded")+
					",positn:" + map.get("positn")+
					",firm:" + map.get("firm")+
					",madeby:" + map.get("madeby")+
					",totalamt:" + map.get("totalamt")+
					",typ:" + map.get("typ")+
					",memo:" + map.get("memo")
					);
			misChkoutmMapper.saveChkoutm(map);
			result.put("pr", map.get("pr").toString());
			if(! (Integer.parseInt(map.get("pr").toString()) == 1)){
				result.put("pr", "-1");
				throw new Exception("出库单"+chkoutm.getVouno()+"添加失败");
			}

			log.warn("新增出库单(详单):");
			if(chkoutm.getChkoutd() != null)
				for(Chkoutd chkoutd : chkoutm.getChkoutd()){
	//			call add_chkoutd(#{chkoutno},#{sp_code.sp_code},#{amount},#{price},#{memo},#{stoid},
	//				#{batchid},#{deliver.code},#{amount1},#{pricesale},#{pr,mode=OUT,jdbcType=INTEGER})
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutm.getChkoutno());///
					m.put("sp_code", chkoutd.getSp_code().getSp_code());
					m.put("amount", chkoutd.getAmount());
					m.put("price", chkoutd.getPrice());
					m.put("memo", chkoutd.getMemo());
					m.put("stoid", chkoutd.getStoid());
					m.put("batchid", chkoutd.getBatchno());
					if(null != chkoutd.getDeliver())
						m.put("deliver", chkoutd.getDeliver().getCode());
					m.put("amount1", chkoutd.getAmount1());
					m.put("pricesale", chkoutd.getPricesale());
					m.put("chkstono", chkoutd.getChkstono());//wjf
					m.put("pr", 0);
					log.warn("sp_code:" + m.get("sp_code")+
							",amount:" + m.get("amount")+
							",price:" + m.get("price")+
							",memo:" + m.get("memo")+
							",stoid:" + m.get("stoid")+
							",batchid:" + m.get("batchid")+
							",deliver:" + m.get("deliver")+
							",amount1:" + m.get("amount1")+
							",pricesale:" + m.get("pricesale")
							);
					misChkoutdMapper.saveChkoutd(m);
					
					//int aasdf=10/0;

					result.put("pr", map.get("pr").toString());
					switch(Integer.parseInt(m.get("pr").toString())){
					case -1:
						result.put("pr", "-1");
						throw new Exception("出库单"+chkoutm.getVouno()+"添加失败"+chkoutd.getSp_code().getSp_code());
					case -2:
						result.put("pr", "-2");
						throw new Exception("出库单"+chkoutm.getVouno()+"添加失败"+chkoutd.getSp_code().getSp_code());
					}
				}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			//检测要保存的出库单据是否被审核    //  去掉凭证号
			Chkoutm  chkoutmchke=new Chkoutm();
			chkoutmchke.setChkoutno(chkoutm.getChkoutno());
			Chkoutm chkoutChec = misChkoutmMapper.findChkoutmById(chkoutmchke);
			if(null != chkoutChec.getChecby() && !"".equals(chkoutChec.getChecby())){
				result.put("pr","-2");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}
			result.put("chkoutno", chkoutm.getChkoutno().toString());
			Map<String,Object> chkout = new HashMap<String,Object>();
			chkout.put("chkoutno", chkoutm.getChkoutno().toString());
			chkout.put("vouno", chkoutm.getVouno());
			chkout.put("maded", chkoutm.getMaded());
			chkout.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			chkout.put("positn", chkoutm.getPositn().getCode());
			chkout.put("madeby", chkoutm.getMadeby());
			chkout.put("firm", chkoutm.getFirm().getCode());
			chkout.put("totalamt", chkoutm.getTotalamt());
			chkout.put("typ", chkoutm.getTyp());
			chkout.put("memo", chkoutm.getMemo());
			chkout.put("pr", 0);
			log.warn("更新出库单(主单):\n"+
					"chkoutno:" + chkout.get("chkoutno")+
					",vouno:" + chkout.get("vouno")+
					",maded:" + chkout.get("maded")+
					",madet:" + chkout.get("madet")+
					",positn:" + chkout.get("positn")+
					",madeby:" + chkout.get("madeby")+
					",firm:" + chkout.get("firm")+
					",totalamt:" + chkout.get("totalamt")+
					",typ:" + chkout.get("typ")+
					",memo:" + chkout.get("memo")
					);
			misChkoutmMapper.updateChkoutm(chkout);
			result.put("pr", chkout.get("pr").toString());
			if(!(Integer.parseInt(chkout.get("pr").toString()) == 1)) throw new Exception("出库单修改失败");
			log.warn("更新出库单(详单):");
			for(Chkoutd chkoutd : chkoutm.getChkoutd()){
				Map<String,Object> m = new HashMap<String,Object>();
				m.put("chkoutno", chkoutm.getChkoutno());
				m.put("sp_code", chkoutd.getSp_code().getSp_code());
				m.put("amount", chkoutd.getAmount());
				m.put("price", chkoutd.getPrice());
				m.put("memo", chkoutd.getMemo());
				m.put("stoid", chkoutd.getStoid());
				m.put("batchid", chkoutd.getBatchno());
				m.put("deliver", chkoutd.getDeliver());
				m.put("amount1", chkoutd.getAmount1());
				m.put("pricesale", chkoutd.getPricesale());
				m.put("pr", 0);
				log.warn("sp_code:" + m.get("sp_code")+
						",amount:" + m.get("amount")+
						",price:" + m.get("price")+
						",memo:" + m.get("memo")+
						",stoid:" + m.get("stoid")+
						",batchid:" + m.get("batchid")+
						",deliver:" + m.get("deliver")+
						",amount1:" + m.get("amount1")+
						",pricesale:" + m.get("pricesale")
						);
				misChkoutdMapper.saveChkoutd(m);
				switch(Integer.parseInt(m.get("pr").toString())){
				case -1:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单"+chkoutm.getVouno()+"更新失败"+chkoutd.getSp_code().getSp_code());
				case -2:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单"+chkoutm.getVouno()+"已审核，更新失败"+chkoutd.getSp_code().getSp_code());
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String deleteChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			Map<String,Object> chkout = new HashMap<String,Object>();
			String[] no = chkoutm.getVouno().split(",");
			log.warn("删除出库单:\n"+chkoutm.getVouno());
			for(String n : no){
				chkout.put("chkoutno",n);
				chkout.put("pr", 0);
				misChkoutmMapper.deleteChkoutm(chkout);
				if(!(Integer.parseInt(chkout.get("pr").toString()) == 1)) throw new Exception("单据已审核，不能删除！");
				result.put("pr", chkout.get("pr").toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateByAudit(Chkoutm chkoutm, String checby) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			//使用vouno接收chkoutno参数
			if (null==chkoutm.getChkoutnos()) {
				chkoutm = misChkoutmMapper.findChkoutmById(chkoutm);
				chkoutm.setChecby(checby);
				map.put("chkoutno", chkoutm.getChkoutno());
				map.put("month",  acctService.getOnlyAccountMonth(chkoutm.getMaded())+"");
				map.put("emp", checby);
				map.put("sta", "Y");
				map.put("chk1memo", chkoutm.getChk1memo());//审核备注  wjf
				if(chkoutm.getFirm().getCode().equals(CalChkNum.profitDeliver)){//如果使用仓位是盘亏，并且出库仓位是分店类型或者部门 ，则sta=P,勿动！2014.12.20wjf
					//门店是先进先出，还是加权平均   勿动！2015.07.16  xlh
					String   sta=ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"firmOutWay");
					if("Y".equals(sta)){					
						map.put("sta", sta);//先进先出
					}else{
						map.put("sta", "P");
					}
				}
				map.put("pr", 0);
				map.put("errdes", "");
				log.warn("\tchkoutno:"+chkoutm.getChkoutno()+"\n"+
						"month:"+map.get("month")+
						",emp:"+map.get("emp")+
						",sta:"+map.get("sta")
						);
				misChkoutmMapper.AuditChkout(map);
				misChkoutmMapper.updateChkoutAmount(chkoutm);
				result.put("pr", map.get("pr").toString());
				result.put("msg", map.get("errdes") != null ? map.get("errdes").toString() : "");
				switch(Integer.parseInt(map.get("pr").toString())){
					case 0:break;
					case 1:break;
					case -1:break;
					case -3:break;
					case -4:break;
					case -5:break;
					default:
						throw new CRUDException(map.get("errdes").toString());
				}
			}else{
				String[] no = chkoutm.getChkoutnos().split(",");
				log.warn("审核出库单:\n"+chkoutm.getVouno());
				for(String n : no){
					Chkoutm chkoutm1=new Chkoutm();//原先好大的bug    循环里居然用  一个对象  到底  xlh  2014.11.21
					chkoutm1.setChkoutno(Integer.valueOf(n));
					chkoutm1 = misChkoutmMapper.findChkoutmById(chkoutm1);
					map.put("chkoutno", n);
					map.put("month",  acctService.getOnlyAccountMonth(chkoutm1.getMaded())+"");
					map.put("emp", checby);
					map.put("sta", "Y");
					
					if(chkoutm1.getFirm().getCode().equals(CalChkNum.profitDeliver)){//如果使用仓位是盘亏，并且出库仓位是分店类型 或者部门，则sta=P,勿动！2014.12.20wjf
						//门店是先进先出，还是加权平均   勿动！2015.07.16  xlh
						String   sta=ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"firmOutWay");
						if("Y".equals(sta)){					
							map.put("sta", sta);//先进先出
						}else{
							map.put("sta", "P");
						}
					}
					map.put("pr", 0);
					map.put("errdes", "");
					log.warn("\tchkoutno:"+n+"\n"+
							"month:"+map.get("month")+
							",emp:"+map.get("emp")+
							",sta:"+map.get("sta")
							);
					misChkoutmMapper.AuditChkout(map);
					misChkoutmMapper.updateChkoutAmount(chkoutm1);
					String pr =map.get("pr") != null ? map.get("pr").toString() : "";
					String errdes =map.get("errdes") != null ? map.get("errdes").toString() : "";
					result.put("pr", pr);
					result.put("msg", errdes);
					
					System.out.println(pr+":::::::::::"+errdes);
					switch(Integer.parseInt(map.get("pr").toString())){
					case 0:break;
					case 1:break;
					case -1:break;
					case -3:break;
					case -4:break;
					case -5:break;
					default:
						throw new CRUDException(map.get("errdes").toString());
					}
				}
			}
			result.put("checby", checby);//审核人
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取下一单号
	 * @return
	 * @throws CRUDException
	 */
	public BigDecimal findNextNo() throws CRUDException{
		try{
			return BigDecimal.valueOf(misChkoutmMapper.findNextNo());
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active, Chkoutm chkoutm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			chkoutm.setVouno("");
			Chkoutm listChecked =misChkoutmMapper.findChkoutmById(chkoutm);
			if(null == listChecked){
				showM = "单号："+chkoutm.getChkoutno()+"的单据已被删除，不能进行"+action+"操作。"; 
			}else if(null != listChecked.getChecby() && !"".equals(listChecked.getChecby())){
				showM = "凭证号："+listChecked.getVouno()+"的单据已被审核，不能进行"+action+"操作。";
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取入库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> findChkoutByCx(Spbatch spbatch) throws CRUDException {
		try {
			
			Positn positn1=new  Positn();
			positn1.setCode(spbatch.getPositn());
			Positn positn= positnMapper.findPositnByCode(positn1);
			if("1203".equals(positn.getPcode())){// 分店   冲销   spbatch_x
				return misChkoutmMapper.findChkoutByCx_X(spbatch);
			}else{// 其它   冲销   spbatch
				return misChkoutmMapper.findChkoutByCx(spbatch);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String fileName = "出库单.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="
					+ fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String realFileName = "chkout.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId) throws CRUDException {
		boolean checkResult = ReadChkoutExcel.check(accountId,path, supplyMapper,positnMapper,deliverMapper,codeDesMapper);
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkoutm chkoutm = ReadChkoutExcel.chkoutm;
			chkoutm.setAcct(accountId);
			List<Chkoutd> chkoutdList = ReadChkoutExcel.chkoutdList;
			chkoutm.setChkoutd(chkoutdList);
			return chkoutm;
		} else {
			errorList = ReadChkoutExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}

}