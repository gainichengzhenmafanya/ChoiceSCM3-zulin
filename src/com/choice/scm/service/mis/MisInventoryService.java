package com.choice.scm.service.mis;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.ChkoutConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.Positndaypan;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.ChkoutdMapper;
import com.choice.scm.persistence.ChkoutmMapper;
import com.choice.scm.persistence.mis.MisInventoryMapper;
import com.choice.scm.service.AcctService;
import com.choice.scm.util.CalChkNum;

@Service
public class MisInventoryService {
	@Autowired
	private MisInventoryMapper misInventoryMapper;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkoutmMapper chkoutmMapper;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private ChkoutdMapper chkoutdMapper;
	private final transient Log log = LogFactory.getLog(MisInventoryService.class);	
	/**
	 * 查询盘点信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findAllInventory(Inventory inventory) throws CRUDException{
		try{
			return misInventoryMapper.findAllInventory(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 保存盘点
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	public void updateInventory(Inventory inventory,String acct) throws CRUDException{
		try{
			for (int i = 0; i < inventory.getInventoryList().size(); i++) {
				Inventory in = inventory.getInventoryList().get(i);
				//1.先删除 positndaypan 表  再插入positndaypan
				Positndaypan pd = new Positndaypan();
				pd.setPositn(inventory.getPositn().getCode());
				pd.setSp_code(in.getSupply().getSp_code());
				pd.setDat(inventory.getDate());
				misInventoryMapper.deletePositndaypan(pd);
				pd.setCnt(in.getCnttrival());
				pd.setCnt1(in.getCntutrival());
				pd.setYnpd("N");
				pd.setDept(inventory.getPositn().getCode());
				misInventoryMapper.insertPositndaypan(pd);
				//2.更新positnsupply 表的余额   感觉没必要  wjf
//				Inventory ivt=new Inventory();
//				ivt.setAcct(acct);
//				ivt.setYearr(inventory.getYearr());
//				ivt.setSupply(new Supply(inventory.getInventoryList().get(i).getSupply().getSp_code()));
//				ivt.setPositn(inventory.getPositn());
//				ivt.setCnttrival(inventory.getInventoryList().get(i).getCnttrival());
//				ivt.setCntutrival(inventory.getInventoryList().get(i).getCntutrival());
//				misInventoryMapper.updateInventory(ivt);
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 结束盘点(档口盘点 采用倒挤式盘点 只生成盘亏出库)门店没有档口的情况下，也采用这种方式
	 * @throws CRUDException
	 */
	public String endInventory(String accountName,Inventory inven,List<Inventory> listInventory) throws CRUDException{
		try{
			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
			String result="";
			for (int i = 0; i < listInventory.size(); i++) {
				Inventory inventory=listInventory.get(i);
				//如果盘点数-结存数 <0.01 或者 结存数-盘点数>0.01 的才生成盘亏出库
				if((inventory.getCnttrival()-inventory.getCntbla() < 0.01 || inventory.getCnttrival()-inventory.getCntbla() > 0.01) &&
						inventory.getCnttrival()-inventory.getCntbla() != 0){
					listInventoryChkOut.add(inventory);
				}
			}
			Deliver deliver=new Deliver();
			deliver.setCode(CalChkNum.profitDeliver);
			Date date = inven.getDate();
			//生成出库单
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno=calChkNum.getNext(CalChkNum.CHKOUT,date);
				int chkoutno=chkoutmMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", deliver.getCode());
				map.put("madeby", accountName);
				map.put("typ", ChkoutConstants.inVententoryOut);
				map.put("memo", ChkoutConstants.inVententoryEndOut);
				chkoutmMapper.saveChkoutm(map);
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSupply().getSp_code());
					m.put("amount", invt.getCntbla()-invt.getCnttrival());
					double price = 0.0;
					if(invt.getCntbla()>0.1){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
						SupplyAcct sa = new SupplyAcct();
						sa.setAcct(inven.getAcct());
						sa.setPositn(inven.getPositn().getCode());
						sa.setSp_code(invt.getSupply().getSp_code());
						Double lastPrice = misInventoryMapper.findLastPriceBySupplyAcct(sa);
						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSupply().getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					m.put("price", price);
					m.put("memo", "盘亏出库");
					m.put("stoid", "");
					m.put("batchid", "");
					m.put("deliver", deliver.getCode());
					m.put("amount1", invt.getCntubla()-invt.getCntutrival());
					m.put("pricesale", invt.getSupply().getPricesale());
					m.put("chkstono", "");//wjf
					m.put("pr", 0);
					chkoutdMapper.saveChkoutd(m);
				}
				result +="出库单已生成,单号"+chkoutno+" ";
				//审核
				map.put("month", acctService.getOnlyAccountMonth(date));
				map.put("emp", accountName);
				map.put("sta", "P");
				chkoutmMapper.AuditChkout(map);
			}
			return  result;
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店盘点  采用档口的情况下（同总部仓位盘点 生成盘亏出库 盘盈入库）
	 * @param accountName
	 * @param inven
	 * @param listInventory
	 * @return
	 * @throws CRUDException
	 */
	public String endDeptInventory(String accountName,Inventory inven,List<Inventory> listInventory) throws CRUDException{
		try{
			ArrayList<Inventory> listInventoryChkIn =new ArrayList<Inventory>();
			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
			String result="";
			for (int i = 0; i < listInventory.size(); i++) {
				Inventory inventory=listInventory.get(i);
				if((inventory.getCnttrival()-inventory.getCntbla()>0.01)){//盘盈入库
					listInventoryChkIn.add(inventory);
				}else if((inventory.getCnttrival()-inventory.getCntbla()<-0.01)){//盘亏出库
					listInventoryChkOut.add(inventory);
				}
			}
			Deliver deliver=new Deliver();
			deliver.setCode(CalChkNum.profitDeliver);
			//拿到当前盘点哪一天
			Date date = inven.getDate();
			//生成入库单
			if(listInventoryChkIn.size()>0){
				//生成入库单主表
				Chkinm chkinm=new Chkinm();
				String vouno=calChkNum.getNext(CalChkNum.CHKIN,date);
				int chkinno=chkinmMapper.getMaxChkinno();
			  	chkinm.setAcct(inven.getAcct());
			  	chkinm.setMadeby(accountName);
			  	chkinm.setYearr(inven.getYearr());
			  	chkinm.setChkinno(chkinno);
			  	chkinm.setVouno(vouno);
			  	chkinm.setDeliver(deliver);
			  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));//如库、单据日期为页面选择日期
				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss "));
				chkinm.setInout(ChkinmConstants.in);
				chkinm.setTyp(ChkinmConstants.inVententoryIn);//盘盈入库
				chkinm.setPositn(listInventory.get(0).getPositn());   //仓位
				chkinmMapper.saveChkinm(chkinm);
				//生成入库单从表
				for (int i = 0; i < listInventoryChkIn.size(); i++) {
					Inventory invt=listInventoryChkIn.get(i);
					Chkind chkind=new Chkind();
					chkind.setChkinno(chkinm.getChkinno());
					chkind.setSupply(invt.getSupply());
					chkind.setAmount(invt.getCnttrival()-invt.getCntbla());  //数量
					double price = 0.0;
					if(invt.getCntbla()>0.1){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
						SupplyAcct sa = new SupplyAcct();
						sa.setAcct(inven.getAcct());
						sa.setPositn(inven.getPositn().getCode());
						sa.setSp_code(invt.getSupply().getSp_code());
						Double lastPrice = misInventoryMapper.findLastPriceBySupplyAcct(sa);
						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSupply().getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					chkind.setPrice(price);
					chkind.setMemo("盘盈入库");
//					chkind.setDued(date);
					chkind.setAmount1(invt.getCntutrival() - invt.getCntubla());
					chkind.setInout(ChkinmConstants.in);   //加入入库标志
					chkindMapper.saveChkind(chkind);
				}
				result +="入库单已生成,单号"+chkinno+" ";
				chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
				chkinm.setChecby(accountName);
				chkinm.setStatus("in");
				chkinm.setChk1memo("盘盈入库审核通过");
				chkinmMapper.checkChkinm(chkinm);//审核入库单
				Integer pr1=chkinm.getPr();
				 System.out.println(pr1);
			}
			//生成出库单
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno=calChkNum.getNext(CalChkNum.CHKOUT,date);
				int chkoutno=chkoutmMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", deliver.getCode());
				map.put("madeby", accountName);
				map.put("typ", ChkoutConstants.inVententoryOut);
				map.put("memo", ChkoutConstants.inVententoryEndOut);
				chkoutmMapper.saveChkoutm(map);
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSupply().getSp_code());
					m.put("amount", invt.getCntbla()-invt.getCnttrival());
					double price = 0.0;
					if(invt.getCntbla()>0.1){//根据流程图来的 如果结存数量>0.1 则单价=结存金额/结存数量
						price = invt.getAmtbla()/invt.getCntbla();
					}else{
						//改为取物资在本仓位的最后进价，需要查supplyacct，没有则取标准价
						SupplyAcct sa = new SupplyAcct();
						sa.setAcct(inven.getAcct());
						sa.setPositn(inven.getPositn().getCode());
						sa.setSp_code(invt.getSupply().getSp_code());
						Double lastPrice = misInventoryMapper.findLastPriceBySupplyAcct(sa);
						price = (lastPrice != null && lastPrice != 0) ? lastPrice : invt.getSupply().getSp_price() ;//有最后进价取最后进价没有取标准价
					}
					m.put("price", price);
					m.put("memo", "盘亏出库");
					m.put("stoid", "");
					m.put("batchid", "");
					m.put("deliver", deliver.getCode());
					m.put("amount1", invt.getCntubla()-invt.getCntutrival());
					m.put("pricesale", invt.getSupply().getPricesale());
					m.put("chkstono", "");//wjf
					m.put("pr", 0);
					chkoutdMapper.saveChkoutd(m);
				}
				result +="出库单已生成,单号"+chkoutno+" ";
				//审核
				map.put("month", acctService.getOnlyAccountMonth(date));
				map.put("emp", accountName);
				map.put("sta", "Y");
				map.put("pr", "");
				chkoutmMapper.AuditChkout(map);
				 int pr= (Integer) map.get("pr");
				 String errdes= (String) map.get("errdes"); 
				 System.out.println(pr+":"+errdes);
				 if(-4==pr){
					 result =pr+":【"+errdes+"】";
				 }
			}
			return  result;
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/***
	 * mis 盘点
	 * @author wjf
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory) {
		return misInventoryMapper.findAllInventoryPage(inventory);
	}

	/***
	 * mis 盘点总数
	 * @param inventory
	 * @return
	 */
	public int findAllInventoryCount(Inventory inventory) {
		return misInventoryMapper.findAllInventoryCount(inventory);
	}

	/***
	 * 保存positndaypan
	 * @param listInventory
	 */
	public void savePositndaypan(Inventory inventory,List<Inventory> listInventory) {
		
		//1.先全部删除  一个店一天的
		if(listInventory.size() != 0){
			Positndaypan pd = new Positndaypan();
			pd.setPositn(listInventory.get(0).getPositn().getCode());
			pd.setDat(inventory.getDate());
			misInventoryMapper.deletePositndaypan(pd);
		}
		for(Inventory in:listInventory){
			//1.先删除 positndaypan 表  再插入positndaypan
			Positndaypan pd = new Positndaypan();
			pd.setPositn(inventory.getPositn().getCode());
			pd.setSp_code(in.getSupply().getSp_code());
			pd.setDat(inventory.getDate());
			pd.setCnt(in.getCnttrival());
			pd.setCnt1(in.getCntutrival());
			pd.setYnpd("Y");//已盘点
			pd.setDept(inventory.getPositn().getCode());
			misInventoryMapper.insertPositndaypan(pd);
		}
		
	}
}