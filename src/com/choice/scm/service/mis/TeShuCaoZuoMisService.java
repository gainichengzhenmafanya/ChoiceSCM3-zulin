package com.choice.scm.service.mis;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Holiday;
import com.choice.scm.domain.Main;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.mis.TeShuCaoZuoMisMapper;
import com.choice.scm.util.CalChkNum;

@Service
public class TeShuCaoZuoMisService {

	@Autowired
	private TeShuCaoZuoMisMapper teShuCaoZuoMisMapper;
	@Autowired
	private CalChkNum calChkNum;
	
	/**
	 * 检查系统是否正在盘点
	 * @param accountId
	 * @return
	 */
	public Map<String,Object> findChktag(Map<String,Object> map){
		return teShuCaoZuoMisMapper.findChktag(map);
	}
	
	/**
	 * 查找月末结转的月份
	 * @return
	 */
	public Main findMain(){
		return teShuCaoZuoMisMapper.findMain();
	}
	
	/**
	 * 结转
	 * @return
	 */
	public String jiezhuan(String acct, Main main){
		main.setAcct(acct);
		main.setMonthh("1");
		main.setChktag("N");
		teShuCaoZuoMisMapper.insertMain(main);
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("pacct", acct);//帐套
		map.put("psta", "C");//参数:
		Integer year = Integer.parseInt(main.getYearr()) - 1;//这里应该传当年   2015.1.2wjf
		map.put("pyearr", year);
		return teShuCaoZuoMisMapper.closeYear(map);
	}
	
	/**
	 * 查找当前会计年
	 * @return
	 */
	public Main findMax(){
		return teShuCaoZuoMisMapper.findMax();
	}
	
	/**
	 * 月末结转
	 * @param monthh
	 */
	public void updateMonthh(Map<String,Object> map){
		teShuCaoZuoMisMapper.updateMonthh(map);
	}
	
	/**
	 * 清空数据
	 */
	public String clearData(String acct,String type,String yearr){
		Map<String,Object> map=new HashMap<String, Object>();
		if(null!=type && "Y".equals(type)){
			map.put("pacct", acct);//帐套
			map.put("psta", "Y");//参数，是否连编码一起清空，Y是连编码一起清空；空只清空操作数据，但保留编码
			map.put("pyearr", yearr);
			teShuCaoZuoMisMapper.clearData(map);
		}else{
			map.put("pacct", acct);//帐套
			map.put("psta", "");//参数，是否连编码一起清空，Y是连编码一起清空；空只清空操作数据，但保留编码
			map.put("pyearr", yearr);
			teShuCaoZuoMisMapper.clearData(map);
		}
		return map.get("pr").toString();
	}
	
	/**
	 * 获取仓位是否已经初始
	 * @param monthh
	 */
	public Positn getQC(Positn positn){
		return teShuCaoZuoMisMapper.getQC(positn);
	}
	
	/**
	 * 仓库期初
	 * @param monthh
	 */
	public List<PositnSupply> getpositnSupplyList(PositnSupply positnSupply){
		return teShuCaoZuoMisMapper.getpositnSupplyList(positnSupply);
	}
	/***
	 * 仓库期初导出excel
	 */
	public boolean exportCangkuInit(ServletOutputStream os, List<PositnSupply> list,String positn) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(positn+"仓库期初表", 0);//加上要期初的仓位名称 wjf
			sheet.addCell(new Label(0, 0,positn+"期初表",titleStyle));
			sheet.mergeCells(0, 0, 13, 0);
            sheet.addCell(new Label(0, 1, "物资编码"));   
            sheet.addCell(new Label(1, 1, "物资名称")); 
			sheet.addCell(new Label(2, 1, "规格"));  
			sheet.addCell(new Label(3, 1, "单位|标准单位"));
			sheet.addCell(new Label(4, 1, "数量|标准单位"));
			sheet.addCell(new Label(5, 1, "金额|标准单位"));
			sheet.addCell(new Label(6, 1, "单位|参考单位"));
			sheet.addCell(new Label(7, 1, "数量|参考单位"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new Label(3, index, list.get(j).getUnit()));
            	sheet.addCell(new Label(4, index, ""+list.get(j).getInc0()));
            	sheet.addCell(new Label(5, index, ""+list.get(j).getIna0()));
            	sheet.addCell(new Label(6, index, list.get(j).getUnit1()));
            	sheet.addCell(new Label(7, index, ""+list.get(j).getIncu0()));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 保存期初
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	public void updateCkInit(PositnSupply positnSupply,String acct,String yearr) throws CRUDException{
		try{
			teShuCaoZuoMisMapper.deleteCkInit(positnSupply);
			for (int i = 0; i < positnSupply.getPositnSupplyList().size(); i++) {
				PositnSupply positnsp=new PositnSupply();
				positnsp.setQcrec(calChkNum.getMaxsequences("POSITNSUPPLY")+"");//取得序列最大值的公共方法);//加入排序
				positnsp.setAcct(acct);
				positnsp.setYearr(yearr);
				positnsp.setPositn(positnSupply.getPositn());
				positnsp.setSp_code(positnSupply.getPositnSupplyList().get(i).getSp_code());
				positnsp.setInc0(positnSupply.getPositnSupplyList().get(i).getInc0());
				positnsp.setIna0(positnSupply.getPositnSupplyList().get(i).getIna0());
				positnsp.setIncu0(positnSupply.getPositnSupplyList().get(i).getIncu0());
				teShuCaoZuoMisMapper.saveCkInit(positnsp);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 仓库期初-确认初始
	 * @param positnSupply
	 * @param acct
	 * @throws CRUDException
	 */
	public void initation(List<PositnSupply> positnSupplyList, String positn, String acct) throws CRUDException
	{
		//String result="1";
		try {
			for (int i = 0; i < positnSupplyList.size(); i++) {
				/*InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");  
				Properties properties = new Properties();
				properties.load(inputStream);*/
				Spbatch spbatch = new Spbatch();
				spbatch.setAcct(acct);
				spbatch.setId(teShuCaoZuoMisMapper.getMaxId().getId());
				spbatch.setInd(DateFormat.getDateByString(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd"));
				spbatch.setChkno(0);
				spbatch.setSp_code(positnSupplyList.get(i).getSp_code());
				if (positnSupplyList.get(i).getInc0()==0) {
					spbatch.setPrice(0);
					spbatch.setPricesale(0);
				}else {
					spbatch.setPrice(positnSupplyList.get(i).getIna0()/positnSupplyList.get(i).getInc0());
					spbatch.setPricesale(positnSupplyList.get(i).getIna0()/positnSupplyList.get(i).getInc0());
				}
				spbatch.setAmount(positnSupplyList.get(i).getInc0());
				spbatch.setPositn(positn);
				spbatch.setStatus("期初");
				spbatch.setStono(0);
				spbatch.setAmount1(positnSupplyList.get(i).getIncu0());
				spbatch.setDeliver("1999");
				teShuCaoZuoMisMapper.insertSpbatch(spbatch);//插入spbatch表，确认初始
				//更新物资表中的 物资余额记录  
				Supply  supply=new Supply();
				supply.setCnt(positnSupplyList.get(i).getInc0());
				supply.setCntu(positnSupplyList.get(i).getIncu0());
				supply.setAmt(positnSupplyList.get(i).getIna0());
				supply.setSp_code(positnSupplyList.get(i).getSp_code());
				supply.setAcct(acct);
				teShuCaoZuoMisMapper.updateSupplyCnt(supply);
			}
			PositnSupply positnsp=new PositnSupply();
			positnsp.setAcct(acct);
			positnsp.setPositn(positn);
			teShuCaoZuoMisMapper.updatePositn(positnsp);//更新仓位状态为已初始
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 执行sql文件方法
	 * @param request 
	 * @return
	 */
	public String doSql(HttpServletRequest request) throws Exception{
		//1.拿到sql文件路径
		String dirpath = request.getSession().getServletContext().getRealPath("sql/scm")+"\\";
		File dir = new File(dirpath);
		//2.遍历此文件夹下的所有后缀是sql的文件，放到files中
		File[] files = dir.listFiles(new FilenameFilter(){
			@Override
			public boolean accept(File dir, String name) {
				boolean b = false;
				if(name.contains(".sql")){
					b = true;
				}else{
					b = false;
				}
				return b;
			}
		});
		if(files == null || files.length == 0){//没有发现sql文件
			return "-2";
		}
		//3.遍历files
		for(File file : files){
			//4.执行sql文件
			if(!runner(file))
				return "-1";
		}
		return "0";
	}
	
	/***
	 * 执行sql文件
	 * @param file
	 * @return
	 */
	private boolean runner(File file){
		try {
		   Connection conn = getConnection();
		   ScriptRunner runner = new ScriptRunner(conn);
		   runner.setErrorLogWriter(null);
		   runner.setLogWriter(null);
		   runner.runScript(new FileReader(file));
		   return true;
		} catch (Exception e) {
		   e.printStackTrace();
		   return false;
		}
	}
	/***
	 * 获得连接的方法
	 */
	private Connection getConnection() throws Exception{
	   Properties props = Resources.getResourceAsProperties("jdbc.properties");
	   String url = props.getProperty("jdbc.url");
	   String driver = props.getProperty("jdbc.driver");
	   String username = props.getProperty("jdbc.username");
	   String password = props.getProperty("jdbc.password");
	   Class.forName(driver);
	   return (Connection) DriverManager.getConnection(url, username, password);
	}
	
	/**
	 * 1.检查物资的默认仓位
	 */
	public List<Supply> findSpPosition(){
		return teShuCaoZuoMisMapper.findSpPosition();
	}
	
	/**
	 * 2.检查物资的默认供应商
	 */
	public List<Supply> findDeliver(){
		return teShuCaoZuoMisMapper.findDeliver();
	}
	
	/**
	 * 3.检查设置了半成品但是没有设置加工仓位
	 */
	public List<Supply> findPositnEx1(){
		return teShuCaoZuoMisMapper.findPositnEx1();
	}
	
	/**
	 * 4.检查设置了半成品但是没有设置BOM
	 */
	public List<Supply> findEx1Bom(){
		return teShuCaoZuoMisMapper.findPositnEx1();
	}
	
	/**
	 * 5.检查半成品BOM用的原材料已删除
	 */
	public List<Spcodeexm> findEx1BomSpCode(){
		return teShuCaoZuoMisMapper.findEx1BomSpCode();
	}
	
	/**
	 * 6.检查菜品BOM用的原材料已删除
	 */
	public List<Costbom> findItemBomSpCode(){
		return teShuCaoZuoMisMapper.findItemBomSpCode();
	}
	
	/**
	 * 7.检查主直拨库
	 */
	public List<Positn> findMainPositn(){
		return teShuCaoZuoMisMapper.findMainPositn();
	}
	
	/**
	 * 8.检查有仓位没有设定简称，将影响分拨功能的使用
	 */
	public List<Positn> findPositnDes1(){
		return teShuCaoZuoMisMapper.findPositnDes1();
	}
	
	/**
	 * 9.检查有主仓库或加工间没有在供应商中定义
	 */
	public List<Positn> findDeliverPositn(){
		return teShuCaoZuoMisMapper.findDeliverPositn();
	}
	
	/**
	 * 10.检查没有定义节假日
	 */
	public List<Holiday> findHoliday(){
		return teShuCaoZuoMisMapper.findHoliday();
	}
	
	/**
	 * 11.检查分店物资属性 同虚拟物料的不同物资
	 */
	public List<Supply> findPositnSpcode(){
		return teShuCaoZuoMisMapper.findPositnSpcode();
	}
}
