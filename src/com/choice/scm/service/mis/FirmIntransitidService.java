package com.choice.scm.service.mis;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.FirmIntransitd;
import com.choice.scm.persistence.reportMis.FirmIntransitdMapper;

@Service
public class FirmIntransitidService {

	private final transient Log log = LogFactory.getLog(FirmIntransitidService.class);
	@Autowired
	private FirmIntransitdMapper firmIntransitdMapper ;
	@Autowired
	private PageManager<FirmIntransitd> pageManager;
	
	/**
	 * 查询在途清单
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmIntransitd> findFirmIntransitd(FirmIntransitd firmIntransitd, Page page) throws CRUDException{
		try{
//			if(null!=firmIntransitd.getSp_type()&&!"".equals(firmIntransitd.getSp_type())){
//				String str = firmIntransitd.getSp_type();
//				firmIntransitd.setGrptyp(str.substring(0, 2));
//				firmIntransitd.setGrp(str.substring(3, 5));
//				firmIntransitd.setSp_type(str.substring(6, 8));
//			}
			
			return pageManager.selectPage(firmIntransitd, page, FirmIntransitdMapper.class.getName()+".queryFirmIntransitd");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<FirmIntransitd> findFirmIntransitd(FirmIntransitd firmIntransitd) throws CRUDException{
		try{
//			if(null!=firmIntransitd.getSp_type()&&!"".equals(firmIntransitd.getSp_type())){
//				String str = firmIntransitd.getSp_type();
//			}
			
			return firmIntransitdMapper.queryFirmIntransitd(firmIntransitd);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出Excel 
	public boolean exportExcel(OutputStream os,List<FirmIntransitd> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"在途清单"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "单据号"));   
			sheet.addCell(new Label(1, 2, "物资编码"));  
			sheet.addCell(new Label(2, 2, "物资名称"));   
			sheet.addCell(new Label(3, 2, "物资单位"));  
			sheet.addCell(new Label(4, 2, "物资一级类别"));   
			sheet.addCell(new Label(5, 2, "物资二级类别"));  
			sheet.addCell(new Label(6, 2, "物资三级类别"));   
			sheet.addCell(new Label(7, 2, "在途数量"));  
			sheet.addCell(new Label(8, 2, "审核数量"));   
			sheet.addCell(new Label(9, 2, "入库数量"));  
			sheet.addCell(new Label(10, 2, "入库单号"));   
			sheet.addCell(new Label(11, 2, "单据状态"));  
			sheet.addCell(new Label(12, 2, "备注"));   
            sheet.mergeCells(0, 0, list.size()-1, 0);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getVouno()));
            	sheet.addCell(new Label(1, i+3, list.get(i).getSp_code()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getSpname()));
            	sheet.addCell(new Label(3, i+3, list.get(i).getUnit()));
            	sheet.addCell(new Label(4, i+3, list.get(i).getGrptypdes()));
	    		sheet.addCell(new Label(5, i+3, list.get(i).getGrpdes()));
	    		sheet.addCell(new Label(6, i+3, list.get(i).getTypdes()));
	    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getItcnt())));
	    		sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getCheckcnt())));
	    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getAmount())));
	    		sheet.addCell(new Label(10, i+3, list.get(i).getChkindid()));
	    		sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getState())));
	    		sheet.addCell(new Label(12, i+3, list.get(i).getMemo()));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}	
}
