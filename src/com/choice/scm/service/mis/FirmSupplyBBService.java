package com.choice.scm.service.mis;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.FirmSupply;
import com.choice.scm.persistence.reportMis.FirmSupplyBBMapper;

@Service
public class FirmSupplyBBService {

	private final transient Log log = LogFactory.getLog(FirmSupplyBBService.class);
	@Autowired
	private PageManager<FirmSupply> pageManager;
	private final static int MAXNUM = 30000;
	/**
	 * 查询物料盘存表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmSupply> findFirmSupply(FirmSupply firmSupply, Page page) throws CRUDException{
		try{
//			if(null!=firmSupply.getSp_type()&&!"".equals(firmSupply.getSp_type())){
//				String str = firmSupply.getSp_type();
//				firmSupply.setGrptyp(str.substring(0, 2));
//				firmSupply.setGrp(str.substring(3, 5));
//				firmSupply.setSp_type(str.substring(6, 8));
//			}
			
			return pageManager.selectPage(firmSupply, page, FirmSupplyBBMapper.class.getName()+".queryFirmSupply");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *物料盘存表---Excel 
	 * 
	 * 
	 */
	public boolean exportExcel(OutputStream os,List<FirmSupply> list) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			//List<String> deptList = findTableHead(firmCode);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"物料盘存"));
				sheet.addCell(new Label(0, 2, "物资编码")); 
	            sheet.addCell(new Label(1, 2, "物资名称"));   
	            sheet.addCell(new Label(2, 2, "一级分类")); 
				sheet.addCell(new Label(3, 2, "二级分类"));  
				sheet.addCell(new Label(4, 2, "三级分类"));	
				sheet.addCell(new Label(5, 2, "标准单位"));
				sheet.addCell(new Label(6, 2, "规格"));
				sheet.addCell(new Label(7, 2, "标准数量"));
				sheet.addCell(new Label(8, 2, "参考数量"));
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	sheet.addCell(new Label(0, index+3, list.get(j).getSp_code().toString()));
	            	sheet.addCell(new Label(1, index+3, list.get(j).getSpname().toString()));
	            	sheet.addCell(new Label(2, index+3, list.get(j).getGrptypdes().toString()));//一级分类
	            	sheet.addCell(new Label(3, index+3, list.get(j).getGrpdes().toString()));//二级分类
	            	sheet.addCell(new Label(4, index+3, list.get(j).getTypdes().toString()));//三级分类
	            	sheet.addCell(new Label(5, index+3, list.get(j).getUnit().toString()));
	            	sheet.addCell(new Label(6, index+3, list.get(j).getSp_desc().toString()));
	            	sheet.addCell(new Label(7, index+3, list.get(j).getCnt().toString()));
	            	sheet.addCell(new Label(8, index+3, list.get(j).getCnt1().toString()));
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}
