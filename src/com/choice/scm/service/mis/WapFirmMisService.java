package com.choice.scm.service.mis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.MD5;
import com.choice.orientationSys.constants.StringConstant;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.ChkstodMapper;
import com.choice.scm.persistence.ChkstodemoMapper;
import com.choice.scm.persistence.ChkstomMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.service.ChkstodemoService;

@Service
public class WapFirmMisService {
	
	@Autowired
	private ChkstodemoMapper chkstodemoMapper;
	@Autowired
	private ChkstomMapper chkstomMapper;
	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(ChkstodemoService.class);
	
	/**
	 * 用户登录验证
	 */
	public String validateLogin(HttpSession session,Account account) throws CRUDException{
		String info = StringConstant.TRUE;
		Account accountResult;
		
		//判断用户名是否为空
		if(account.getName() == null || account.getName().equals(""))
			return LoginConstants.MESSAGE_NAME_ISNULL;
		
		//判断用户密码是否为空
		else if(account.getPassword() == null || account.getPassword().equals(""))
			return LoginConstants.MESSAGE_PASSWORD_ISNULL;
		
		try{
			//判断用户名是否存在
			accountResult = accountMapper.findAccountByName(account.getName());
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_NAME_NOT_EXIST;
			
			//判断密码是否正确
			String password = account.getPassword();	//存放输入时的原始密码
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validatePassword(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_PASSWORD_ERROR;
			//账号状态未被审核
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validateStatus(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_STATUS_ERROR;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		if(info==StringConstant.TRUE){
			Account accounts=accountMapper.findAccountById(accountResult.getId());
			if(accounts.getPositn()!=null){
				session.setAttribute("firmName", accounts.getPositn().getCode());
			}
			session.setAttribute("accountName", accountResult.getName());
			session.setAttribute("UsersCode", accounts.getUser().getCode());
			session.setAttribute("accountId", accountResult.getId());
			session.setAttribute("ChoiceAcct", "1");
		}
		return info;
	}
	/**
	 * 应用程序 用户登录验证
	 */
	public HashMap<String,String> AppvalidateLogin(Account account) throws CRUDException{
		HashMap<String,String> info=new HashMap<String, String>();
		info.put("info", "T");
		Account accountResult;
		
		//判断用户名是否为空
		if(account.getName() == null || account.getName().equals("")){
			info.put("info",LoginConstants.MESSAGE_NAME_ISNULL);
			return info;
		}
		
		//判断用户密码是否为空
		else if(account.getPassword() == null || account.getPassword().equals("")){
			info.put("info", LoginConstants.MESSAGE_PASSWORD_ISNULL);
			return info;
		}
		
		try{
			//判断用户名是否存在
			accountResult = accountMapper.findAccountByName(account.getName());
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals(""))){
				info.put("info", LoginConstants.MESSAGE_NAME_NOT_EXIST);
				return info;
			}
			
			//判断密码是否正确
			String password = account.getPassword();	//存放输入时的原始密码
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validatePassword(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals(""))){
				info.put("info", LoginConstants.MESSAGE_PASSWORD_ERROR);
				return info;
			}
			//账号状态未被审核
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validateStatus(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals(""))){
				info.put("info", LoginConstants.MESSAGE_STATUS_ERROR);
				return info;
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		if(info.get("info")==StringConstant.TRUE){
			Account accounts=accountMapper.findAccountById(accountResult.getId());
			if(accounts.getPositn()!=null){
				info.put("firmName", accounts.getPositn().getCode());
			}
			info.put("accountName", accounts.getName());
			info.put("accountId", accountResult.getId());
			info.put("ChoiceAcct", "1");
		}
		return info;
	}
	
	/**
	 * 查询信息
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstodemo> findChkstodemo(Chkstodemo chkstodemo) throws CRUDException
	{
		try {
			return chkstodemoMapper.findChkstodemo(chkstodemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询信息
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findChkstomToday(Chkstom chkstom) throws CRUDException
	{
		try {
			HashMap<Object,Object> map=new HashMap<Object,Object>();
			map.put("chkstom", chkstom);
			return chkstomMapper.findByKey(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 添加 报货查询信息
	 * @return
	 * @throws CRUDException
	 */
	public String saveChkstodemo(Chkstom chkstom) throws CRUDException
	{
		String result="success";
		try {
			String hoped=DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", 1, 2),"yyyy-MM-dd");
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					double amount=chkstod.getAmount();
					double price=chkstod.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			//添加报货单日志
			log.warn("添加报货单(主单)：\n" + chkstom);
			chkstomMapper.saveNewChkstom(chkstom);
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					chkstod.setHoped(hoped);
					//保存报货单日志
					log.warn(chkstod);
					chkstodMapper.saveNewChkstod(chkstod);
					if(chkstod.getPr()>=1){
						result="success";
					}
				}
			}
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有分店
	 * @return
	 */
	public List<Positn> AppfindPositn() throws CRUDException{
		try {
			return positnMapper.findAllPositn();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
