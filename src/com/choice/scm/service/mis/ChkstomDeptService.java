package com.choice.scm.service.mis;


import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkstomMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SppriceMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.persistence.mis.ChkstodDeptMapper;
import com.choice.scm.persistence.mis.ChkstomDeptMapper;

@Service

public class ChkstomDeptService {

	@Autowired
	private ChkstodDeptMapper chkstodDeptMapper;
	@Autowired
	private ChkstomDeptMapper chkstomMapper;
	@Autowired
	private ChkstomMapper chkstoMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private SppriceMapper sppriceMapper;
	@Autowired
	private PageManager<Chkstom> pageManager;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;

	private final transient Log log = LogFactory.getLog(ChkstomDeptService.class);
	
	/**
	 * 查询单据审核状态
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findCheckSta(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 添加物资前验证
	 */
	public String checkSaveNewChk(Chkstom chkstom) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		boolean flag=true;
		try {
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					//查询报货单据里的物资是否设置了默认供应商
					Spprice spprice=new Spprice();//报价里的默认供应商
					spprice.setSp_code(chkstod.getSupply().getSp_code());
					spprice.setAcct(chkstom.getAcct());
					spprice.setArea(chkstom.getFirm());
					spprice.setBdat(chkstom.getMaded());
					spprice.setEdat(chkstom.getMaded());
					spprice.setSta("Y");
					Supply supply=new Supply();//物资表里的默认供应商
					supply.setAcct(chkstom.getAcct());
					supply.setSp_code(chkstod.getSupply().getSp_code());
					List<Spprice> list1=sppriceMapper.findDeliverBySpprice(spprice);
					Supply supply1=supplyMapper.findDeliverByCode(supply);
					if(list1.size()==0 && (null==supply1 || "".equals(supply1))){//如果物资没有设置供应商，返回
						result.put("sp_name", chkstod.getSupply().getSp_name());
						result.put("pr", "1");
						flag=false;
					}
				}
				if(flag==true){
					result.put("pr", "2");
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加或修改报货单
	 * @param chkstom
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Chkstom chkstom, String sta) throws CRUDException
	{
		String result="1";
		try {
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstoMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					double amount=chkstod.getAmount();
					double price=chkstod.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				//添加报货单日志
				log.warn("添加部门报货单(主单)：\n" + chkstom);
				chkstomMapper.saveNewChkstom(chkstom);
			}else{
				HashMap<Object,Object>  map = new HashMap<Object,Object> ();
				Chkstom chkstomN = new Chkstom();
				chkstomN.setChkstoNo(chkstom.getChkstoNo());
				map.put("chkstom", chkstomN);
				chkstom.setVouno(chkstomMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改部门报货单(详单)：\n" + chkstom);
				chkstomMapper.updateChkstom(chkstom);
			}
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存部门报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					chkstodDeptMapper.saveNewChkstod_new(chkstod);
				
					if(chkstod.getPr()>=1){
						result="1";
					}
				}
			}
			Positn positn1 = new Positn();
			positn1.setOldcode(chkstom.getDept());
			positn1.setLocked("Y");
			positnMapper.updatePositn(positn1);
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstomMap, page, ChkstomDeptMapper.class.getName()+".findByKey");			
			//return chkstomMapper.findByKey(chkstomMap);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public List<Chkstom> findByKey(HashMap<Object,Object> chkstomMap) throws CRUDException
	{
		try {
			return chkstomMapper.findByKey(chkstomMap);			
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询当前最大单号
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxChkstono() throws CRUDException
	{
		try {
			return chkstomMapper.getMaxChkstono();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除
	 */
	public String deleteChkstom(Chkstom chkstom,String chkstoNoIds) throws CRUDException
	{
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//删除报货单日志
			log.warn("删除报货单:\n" + chkstom + "\n" + chkstoNoIds);
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c=new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				chkstomMapper.deleteChk(c);
				result.put("pr", c.getPr().toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}	

}
