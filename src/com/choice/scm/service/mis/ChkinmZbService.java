package com.choice.scm.service.mis;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.mis.ChkinmZbMapper;
import com.choice.scm.service.AcctService;

@Service

public class ChkinmZbService {
	
	@Autowired
	private ChkinmZbMapper chkinmZbMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	@Autowired
	private AcctService acctService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(ChkinmZbService.class);

	
	/**
	 * 查询入库单号序列最大值
	 */
	public Integer getMaxChkinno() throws CRUDException {
		return chkinmZbMapper.getMaxChkinno();
	}
		
	/**
	 * 查询所有的入库单
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinm(String checkOrNot,Chkinm chkinm,Page page,Date madedEnd,String locale) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			chkinm.setInout(ChkinmConstants.conk);
			map.put("chkinm", chkinm);
			map.put("locale", locale);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			map.put("typ", chkinm.getTyp());
			log.warn("直拨单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+
					",checkOrNot:"+map.get("checkOrNot")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMapper.class.getName()+".findAllChkinmMisByinput");

			
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 查询所有的入库单   模糊查询           暂时没用到
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinmByinput(Chkinm chkinm,Page page,Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("chkinm", chkinm);
			map.put("madedEnd", madedEnd);
			log.warn("入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmZbMapper.class.getName()+".findAllChkinmByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveChkinm(Chkinm chkinm,String inout) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			log.warn("添加入库单(主单)：\n"+chkinm);
	 		chkinmZbMapper.saveChkinm(chkinm);
	 		log.warn("添加入库单(详单)：");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setDued(DateFormat.getDateByString("1899-12-30", "yyyy-MM-dd"));
				chkind.setInout(ChkinmConstants.conk);   //加入直发标志
				log.warn(chkind);
	 			chkindMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据id查询 入库单
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public Chkinm findChkinmByid(Chkinm chkinm) throws CRUDException {
		try {
			if(null==chkinm.getChkinno()){
				return null;
			}else{
				log.warn("获取入库单：" + chkinm.getChkinno());
				return chkinmZbMapper.findChkinmByid(chkinm);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int updateChkinm(Chkinm chkinm) throws CRUDException {
		try {
			//验证该入库单是否已被审核
			Chkinm chkinmChec = findChkinmByid(chkinm);
			if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
				return -1;
			}
			//删除该条记录

			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			
			log.warn("更新入库单(主单):\n"+chkinm);
	 		chkinmZbMapper.updateChkinm(chkinm);//修改入库单
	 		//修改入库单从表---思路   先删除  再插入  
	 		
	 		//List<String> ids = Arrays.asList((chkinm.getChkinno()+"").split(","));//删除  在存储过程有
	 		//chkindMapper.deleteChkind(ids);
	 		
	 		//再添加入数据
	 		log.warn("更新入库单(详单):");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				log.warn(chkind);
	 			chkindMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}		
	
	/**
	 * 删除入库单
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteChkinm(List<String> listId) throws CRUDException {
		try {
			log.warn("删除入库单:\n"+listId);
			//验证要删除的入库单是否已被审核：n--未审核，y--已审核
			String sta = "n";
			for(String id : listId){
				Chkinm chkinm = new Chkinm();
				chkinm.setChkinno(Integer.parseInt(id));
				Chkinm chkinmChec = findChkinmByid(chkinm);
				if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
					sta = "y";
				}
			}
			if("y".equals(sta)){
				return;
			}else{
				chkinmZbMapper.deleteChkinm(listId);
				//删除从表
				chkindMapper.deleteChkindByChkinno(listId);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取直发冲消数据
	 * @author 2014.11.4 wjf
	 * @param spbatch
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch) throws CRUDException {
		try {
//			Positn positn1=new  Positn();
//			positn1.setCode(spbatch.getPositn());
//			Positn positn= positnMapper.findPositnByCode(positn1);
//			if("分店".equals(positn.getTyp())){// 分店   冲销   spbatch_x
//				return chkinmZbMapper.addChkinmzfByCx_x(spbatch);
//			}else{// 其它   冲销   spbatch
			return chkinmZbMapper.addChkinmzfByCx(spbatch);
//			}
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核入库单   单条或批量审核
	 * @param listId
	 * @throws CRUDException
	 */
	public String checkChkinm(Chkinm chkinm,String chkinnoids) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			int pr=1;
			
			log.warn("审核入库单：\n");
//			Date date=new Date();
//			chkinm.setYearr(date.getMonth()+1+"");//  此字段当成月份使用
			if(null==chkinnoids){
				log.warn(chkinm);
				chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月 
				chkinmZbMapper.checkChkinm(chkinm);//单条审核入库单
				 pr=chkinm.getPr();
			}else{
				log.warn(chkinnoids);
				List<String> ids = Arrays.asList(chkinnoids.split(","));
				for (int i = 0; i < ids.size(); i++) {
					chkinm.setChkinno(Integer.parseInt(ids.get(i)));
					Chkinm chkinm_ = chkinmZbMapper.findChkinmByid(chkinm);
					chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm_.getMaded()));//会计月 
					chkinmZbMapper.checkChkinm(chkinm);//循环批量 审核入库单
					 pr=chkinm.getPr();
				}
			}
			result.put("pr", pr+"");	
			result.put("checby", chkinm.getChecby());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询是否必须入到默认仓位
	 * @param chkinm
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public String chkPositn(SpCodeMod spCodeMod) throws CRUDException {
		String msg = "OK";
		try {
			if ("".equals(spCodeMod.getSp_code())) {
				msg = "入库单据不能为空！";
				return msg;
			}
			spCodeMod.setSp_code(CodeHelper.replaceCode(spCodeMod.getSp_code()));
			Supply supply = new Supply();
			supply.setSp_code(CodeHelper.replaceCode(spCodeMod.getSp_code()));
			if ("0".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "INOUT_IS_NOT"))) {
				int aaa = chkinmZbMapper.getPositn(spCodeMod).size();
				int bbb = chkinmZbMapper.getPositn1(supply).size();
				if (aaa==0 && bbb==0) {
					msg = "物资未设置默认仓位!";
					return msg;
				}
				boolean first = true;
				List<SpCodeMod> spCodeModList = chkinmZbMapper.chkPositn(spCodeMod);
				for (int i=0; i<spCodeModList.size(); i++) {
					if (first) {
						msg = spCodeModList.get(i).getSp_code();
					}else {
						msg = msg + "," + spCodeModList.get(i).getSp_code();
					}
					first=false;
				}
				if (!"OK".equals(msg)) {
					msg = msg + "的默认仓位不是所选入库仓位!";
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		finally{
			return  msg;
		}
	}
	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active,Chkinm chkinm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("chkinm", chkinm);
			List<Chkinm> listChecked = pageManager.selectPage(map,new Page(),ChkinmMapper.class.getName()+".findAllChkinmByinput");
			switch(listChecked.size()){
				case 0 : 
					showM = "凭证号："+chkinm.getVouno()+"的单据已被删除，不能进行"+action+"操作。"; 
					break;
				case 1 :  
					if(null != listChecked.get(0).getChecby() && !"".equals(listChecked.get(0).getChecby())){
						showM = "凭证号："+chkinm.getVouno()+"的单据已被审核，不能进行"+action+"操作。"; 
					}
					break;
				default :
					showM = "凭证号："+chkinm.getVouno()+"的单据数据记录异常，请核对。";
					break;
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取入库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch) throws CRUDException {
		try {
			return chkinmZbMapper.addChkinmByCx(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
}
