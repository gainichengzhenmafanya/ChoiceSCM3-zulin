package com.choice.scm.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ExplanConstants;
import com.choice.scm.domain.Explan;
import com.choice.scm.domain.ExplanD;
import com.choice.scm.domain.ExplanM;
import com.choice.scm.domain.ExplanSpcode;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.SpCodeExPrice;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.BanChengPinPriceMapper;
import com.choice.scm.persistence.CostCutMapper;
import com.choice.scm.persistence.ExplanMMapper;
import com.choice.scm.persistence.ExplanMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.PrdctbomMapper;

@Service

public class ExplanMService {
	
	@Autowired
	private ExplanMMapper explanMMapper;
	@Autowired
	private ExplanMapper explanMapper;
	@Autowired
	private PageManager<ExplanM> pageManager;
	@Autowired
	private PageManager<ExplanD> pageManager3;
	@Autowired
	private PageManager<ExplanSpcode> pageManager2;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PrdctbomMapper prdctMapper;
	@Autowired
	private BanChengPinPriceMapper banChengPinPriceMapper;
	@Autowired
	private PageManager<Map<String,Object>> pageManager1;
	@Autowired
	private AcctService acctService;
	@Autowired
	private CostCutMapper costcutMapper;
	private final transient Log log = LogFactory.getLog(ExplanMService.class);

	
	/**
	 * 查询加工单号序列最大值
	 */
	public Integer getMaxExplanno() throws CRUDException {
		return explanMMapper.getMaxExplanNo();
	}
		
	/**
	 * 查询领料单号序列最大值
	 */
	public Integer getMaxExplanLLNo() throws CRUDException {
		return explanMMapper.getMaxExplanLLNo();
	}
	
	/**
	 * 查询所有的加工单
	 * @param explanM
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanM> findAllExplan(ExplanM explanM, Page page, Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			explanM.setStatus("0");
			map.put("explanM", explanM);
			map.put("madedEnd", madedEnd);
			return pageManager.selectPage(map,page,ExplanMMapper.class.getName()+".findAllExplanMByinput");

			
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的加工单   模糊查询       2014.10.21wjf
	 * @param explanM
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanM> findAllExplanMByinput(ExplanM explanM, Page page, Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("explanM", explanM);
			map.put("madedEnd", madedEnd);
//			return explanMMapper.findAllExplanMByinput(map);
			return pageManager.selectPage(map,page,ExplanMMapper.class.getName()+".findAllExplanMByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货单单据模板子表
	 * @param explanD
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanD> findAllExplanD(ExplanD explanD) throws CRUDException {
		try {
			return explanMMapper.findAllExplanD(explanD);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存加工单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveExplanM(ExplanM explanM) throws CRUDException {
		try {
			int result=-1;
			Date date=new Date();
			explanM.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			explanM.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			explanM.setStatus("0");
			explanM.setTyp("0");
			explanM.setYnll("N");
	 		explanMMapper.saveExplanM(explanM);
			for (int i = 0; i < explanM.getExplanDList().size(); i++) {
				ExplanD explanD=explanM.getExplanDList().get(i);
				explanD.setId(explanMMapper.getMaxExplanDNo());
				explanD.setExplanno(explanM.getExplanno());
				explanD.setAcct(explanM.getAcct());
				explanD.setYnrk("N");
				explanD.setYearr(explanM.getYearr());
				explanMMapper.saveExplanD(explanD);
			}
			result=1;
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据id查询 加工单
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public ExplanM findExplanMByid(ExplanM explanM) throws CRUDException {
		try {
			if(null==explanM.getExplanno()){
				return null;
			}else{
				return explanMMapper.findExplanMByid(explanM);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改加工单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int updateExplanM(ExplanM explanM) throws CRUDException {
		try {
			//验证该加工单是否已被审核
			ExplanM explanMChec = findExplanMByid(explanM);
			if(null != explanMChec.getChecby() && !"".equals(explanMChec.getChecby())){
				return 0;
			}
			//删除该条记录
			List<String> listId = new ArrayList<String>();
			listId.add(explanM.getExplanno()+"");
			explanMMapper.deleteExplanD(listId);
			explanM.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			explanM.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			explanM.setYnll("N");
	 		explanMMapper.updateExplanM(explanM);//修改加工单
	 		
	 		//再添加入数据
			for (int i = 0; i < explanM.getExplanDList().size(); i++) {
				ExplanD explanD=explanM.getExplanDList().get(i);
				explanD.setAcct(explanM.getAcct());
				explanD.setYearr(explanM.getYearr());
				explanD.setExplanno(explanM.getExplanno());
				explanD.setExplanno(explanM.getExplanno());
				explanMMapper.saveExplanD(explanD);
			}
			return 1;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改加工单主表(加入计划)
	 * @param explanM
	 * @throws CRUDException
	 */
	public int updateExplanMM(ExplanM explanM, String accountName) throws CRUDException {
		try {
			if ("1".equals(explanM.getStatus())){
				Date date=new Date();
				explanM.setChecd(date);
				explanM.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				explanM.setChecby(accountName);
				explanMMapper.updateExplanM(explanM);//修改加工单
			}else{
				explanMMapper.updateExplanMForUncheck(explanM);//修改加工单
			}
			
			return 1;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除加工单
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteExplanM(List<String> listId) throws CRUDException {
		try {
			//验证要删除的加工单是否已加入计划
			String sta = "n";
			for(String id : listId){
				ExplanM explanM = new ExplanM();
				explanM.setExplanno(Integer.parseInt(id));
				ExplanM explanMChec = findExplanMByid(explanM);
				if("1".equals(explanMChec.getStatus())){
					sta = "y";
				}
			}
			if("y".equals(sta)){
				return;
			}else{
				//删除主表
				explanMMapper.deleteExplanM(listId);
				//删除从表
				explanMMapper.deleteExplanD(listId);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 转为领料单并向总部要货   
	 * @param product_
	 * @param madeby
	 * @throws CRUDException
	 */
	public String saveChkoutm(ExplanSpcode explanSpcode, String acct, String madeby, String idList) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			result.put("pr", "-1");
			Integer no = explanMMapper.getMaxExplanLLNo();
			if (explanSpcode!=null && explanSpcode.getExplanSpcodeList().size()>0) {
				for (int i=0;i<explanSpcode.getExplanSpcodeList().size();i++) {
					ExplanSpcode explanSpcode_ = explanSpcode.getExplanSpcodeList().get(i);
					explanSpcode_.setNo(no);
					explanSpcode_.setMadeby(madeby);
					explanSpcode_.setDat(new Date());
					//添加领料单
					explanMMapper.saveExplanMLL(explanSpcode_);
				}
			}
			
			//更新加工单的领料单号和已领料状态
			if (idList!=null){
				Map<String,String> mapResult = new HashMap<String,String>();
				mapResult.put("listId", CodeHelper.replaceCode(idList));
				mapResult.put("chkoutno", no+"");
				explanMMapper.updateExplanMForYnll(mapResult);
			}
			
			//调用存储过程，对半成品，添加加工单。对默认仓位是1000的填写报货单，其余的填写出库单
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("no", no);
			map.put("acct", acct);
			map.put("year", mainInfoMapper.findYearrList().get(0)+"");
			map.put("madeby", madeby);
			map.put("positn", explanSpcode.getPositn());
			map.put("pr", 0);
			map.put("pmessage", "");
			explanMMapper.explanSpcode_new(map);
			result.put("pr", map.get("pr").toString());
			if((Integer.parseInt(map.get("pr").toString()) == -2)){
				result.put("pr", "-2");
				result.put("msg", "半成品生成加工单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -3)){
				result.put("pr", "-3");
				result.put("msg", "生成报货单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -4)){
				result.put("pr", "-4");
				result.put("msg", "生成出库单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == 1)){
				result.put("pr", "1");
				result.put("msg", "成功生成领料单,"+map.get("pmessage").toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存领料单    
	 * @param explanSpcode
	 * @throws CRUDException
	 */
	public String saveChkout(ExplanSpcode explanSpcode, String acct,String madeby, String idList) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			result.put("pr", "1");
			Integer no = explanMMapper.getMaxExplanLLNo();
			if (explanSpcode!=null && explanSpcode.getExplanSpcodeList().size()>0) {
				for (int i=0;i<explanSpcode.getExplanSpcodeList().size();i++) {
					ExplanSpcode explanSpcode_ = explanSpcode.getExplanSpcodeList().get(i);
					explanSpcode_.setNo(no);
					explanSpcode_.setMadeby(madeby);
					explanSpcode_.setDat(new Date());
					//添加领料单
					explanMMapper.saveExplanMLL(explanSpcode_);
				}
			}
			//更新加工单的领料单号和已领料状态
			if (idList!=null){
				Map<String,String> mapResult = new HashMap<String,String>();
				mapResult.put("listId", CodeHelper.replaceCode(idList));
				mapResult.put("chkoutno", no+"");
				explanMMapper.updateExplanMForYnll(mapResult);
			}
			//保存领料单 直接生成对应的出库单或者报货单
			//调用存储过程，对半成品，添加加工单。对默认仓位是1000的填写报货单，其余的填写出库单
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("no", no);
			map.put("acct", acct);
			map.put("year", mainInfoMapper.findYearrList().get(0)+"");
			map.put("madeby", madeby);
			map.put("positn", explanSpcode.getPositn());
			map.put("pr", 0);
			map.put("pmessage", "");
			explanMMapper.explanSpcode_new(map);
			result.put("pr", map.get("pr").toString());
			if((Integer.parseInt(map.get("pr").toString()) == -2)){
				result.put("pr", "-2");
				result.put("msg", "半成品生成加工单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -3)){
				result.put("pr", "-3");
				result.put("msg", "生成报货单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -4)){
				result.put("pr", "-4");
				result.put("msg", "生成出库单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == 1)){
				result.put("pr", "1");
				result.put("msg", "成功生成领料单,"+map.get("pmessage").toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核领料单    
	 * @param explan
	 * @throws CRUDException
	 */
	public String auditChkout(ExplanSpcode explanSpcode, String acct, String madeby, String idList) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			result.put("pr", "-1");
			//调用存储过程，对半成品，添加加工单。对默认仓位是1000的填写报货单，其余的填写出库单
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("no", idList);
			map.put("acct", acct);
			map.put("year", mainInfoMapper.findYearrList().get(0)+"");
			map.put("madeby", madeby);
			map.put("positn", explanSpcode.getPositn());
			map.put("pr", 0);
			explanMMapper.explanSpcode(map);
			result.put("pr", map.get("pr").toString());
			if((Integer.parseInt(map.get("pr").toString()) == -2)){
				result.put("pr", "-2");
				result.put("msg", "半成品生成加工单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -3)){
				result.put("pr", "-3");
				result.put("msg", "生成报货单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == -4)){
				result.put("pr", "-4");
				result.put("msg", "生成出库单失败");
			}else if((Integer.parseInt(map.get("pr").toString()) == 1)){
				result.put("pr", "1");
				result.put("msg", "领料单拆分成功");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存来自安全库存的    
	 * @param explan
	 * @throws CRUDException
	 */
	public String saveExplanBySafeCnt(ExplanM explanM,String acct) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String, Object>();
			Date date=new Date();
			explanM.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			explanM.setAcct(acct);
			explanM.setMaded(DateFormat.formatDate(date,"yyyy-MM-dd"));
			explanM.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			explanM.setStatus("0");
			explanM.setYnll("N");
			explanM.setHoped(DateFormat.formatDate(date,"yyyy-MM-dd"));
	 		explanMMapper.saveExplanM(explanM);
			for (int i = 0; i < explanM.getExplanDList().size(); i++) {
				ExplanD explanD=explanM.getExplanDList().get(i);
				explanD.setId(explanMMapper.getMaxExplanDNo());
				explanD.setExplanno(explanM.getExplanno());
				explanD.setAcct(explanM.getAcct());
				explanD.setYnrk("N");
				explanD.setYearr(explanM.getYearr());
				explanMMapper.saveExplanD(explanD);
			}
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存来自计划的    
	 * @param explan
	 * @throws CRUDException
	 */
	public String saveExplanByPlan(ExplanM explanM,String acct) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String, Object>();
			Date date=new Date();
			explanM.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			explanM.setAcct(acct);
			explanM.setMaded(DateFormat.formatDate(date,"yyyy-MM-dd"));
			explanM.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			explanM.setStatus("0");
			explanM.setYnll("N");
			explanM.setHoped(DateFormat.formatDate(date,"yyyy-MM-dd"));
			Positn positn = new Positn();
			positn.setCode(explanM.getExplanDList().get(0).getSupply().getPositnex());
			explanM.setPositn(positn);
	 		explanMMapper.saveExplanM(explanM);
	 		String ids = "'";
			boolean flag = true;
			for (int i = 0; i < explanM.getExplanDList().size(); i++) {
				if (!flag) {
					ids += ",'";
				}
				flag = false;
				ExplanD explanD=explanM.getExplanDList().get(i);
				explanD.setId(explanMMapper.getMaxExplanDNo());
				explanD.setExplanno(explanM.getExplanno());
				explanD.setAcct(explanM.getAcct());
				explanD.setYearr(explanM.getYearr());
				explanD.setYnrk("N");
				ids += explanD.getSupply().getSp_code();
				ids += "'";
				explanMMapper.saveExplanD(explanD);
			}
			//更新申购单从表的bak1字段为已处理状态
			Map<String,Object> chkstodMap= new HashMap<String,Object>();
			chkstodMap.put("bak1", "1");
			chkstodMap.put("positnex", explanM.getExplanDList().get(0).getSupply().getPositnex());//加工间
			chkstodMap.put("bdat", explanM.getDat());
			chkstodMap.put("edat", explanM.getDatEnd());
			chkstodMap.put("ids", ids);
			explanMapper.saveExplanByPlan(chkstodMap);
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增
	 * @param explan
	 * @throws CRUDException
	 */
	public void saveExplan(Explan explan) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> itemids = Arrays.asList(explan.getSupply().getSp_code().split(",")); 
			Product product=new Product();
			product.setAcct(explan.getAcct());
			map.put("ynex", "ynex");//是否有可分解产品
			map.put("product", product);
			map.put("itemids", itemids);
			explan.setCntkc(explanMapper.getCntkc(explan.getSupply()));//设置库存量
			explan.setCntplan1(explanMapper.getCntplan1(explan.getSupply()));//设置已加工量
			List<Product> productList = prdctMapper.findmaterialBylist(map);
			if(productList.size()>0){
				explan.setYnex("Y");//可分解
			}else{
				explan.setYnex("N");//不可分解
			}	
		
			explan.setYnsto("N");
			explan.setYnin("N");
			explan.setYnout("N");
			explan.setSta(ExplanConstants.PLAN);//计划
			explan.setDat(new Date());
			explanMapper.saveExplan(explan);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/************* 安全库存到计划    ************/
	public List<Map<String,Object>> safeCntToPlan(Map<String,Object> map,Page page)throws CRUDException {
		try {
			SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
			if(null!=supplyAcct && !"".equals(supplyAcct)){
//				if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
//					supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
//				}
				if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
					supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
				}
				map.put("supplyAcct", supplyAcct);
			}
			String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			map.put("year", year);
			//判断是不是走配送片区默认默认仓位的
			int count = explanMapper.findSpcodemodCount();
			if(count == 0){//没有说明只走物资表  默认仓位和默认加工间
				return pageManager1.selectPage(map,page,ExplanMMapper.class.getName()+".safeCntToPlan1");
			}else{
				return pageManager1.selectPage(map,page,ExplanMMapper.class.getName()+".safeCntToPlan2");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
//	查询表头
	public List<Positn> findTableHead(Map<String,Object> condition){
		List<Positn> positnList = positnMapper.findPositnByTypCode(condition);
		return positnList;
	}
	
	/**
	 * 查询所有的报货单
	 */
	public List<ExplanSpcode> findExplanSpcodeM(ExplanSpcode explanSpcode, Page page, Date madedStart, Date madedEnd) throws CRUDException {
		try {
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("explanSpcode", explanSpcode);
			map.put("madedStart", madedStart);
			map.put("madedEnd", madedEnd);
			//return pageManager2.selectPage(map,page,ExplanMMapper.class.getName()+".findExplanSpcodeM");
			return explanMMapper.findExplanSpcodeM(map);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货单单据模板子表
	 * @param explanD
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanSpcode> findExplanSpcodeD(ExplanSpcode explanSpcode, Page page) throws CRUDException {
		try {
			return explanMMapper.findExplanSpcodeD(explanSpcode);
//			return pageManager2.selectPage(explanSpcode,page,ExplanMMapper.class.getName()+".findExplanSpcodeD");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 登记入库界面
	 * @param explanD
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanD> findExplanDForDj(ExplanD explanD, Page page) throws CRUDException {
		try {
			//判断是不是走配送片区默认仓位
			int count = explanMapper.findSpcodemodCount();
			if(count == 0){//没有说明只走物资表  默认仓位和默认加工间
				return pageManager3.selectPage(explanD, page, ExplanMMapper.class.getName()+".findExplanDForDj");
			}else{
				return pageManager3.selectPage(explanD, page, ExplanMMapper.class.getName()+".findExplanDForDj2");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  保存登记   修改数量 
	 * @param explan
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateCntact(ExplanD explanD,String acct) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String, Object>();
//			String day = DateFormat.getStringByDate(explanD.getMaded(), "yyyy-MM");
//			String yearr = day.split("-")[0];
//			String monthh = Integer.valueOf(day.split("-")[1])>=10?day.split("-")[1]:(day.split("-")[1].length()==2?day.split("-")[1].substring(1,2):day.split("-")[1]);
			String yearr = mainInfoMapper.findYearrList().get(0);//会计年wjf
			String monthh = acctService.getOnlyAccountMonth(new Date())+"";
			for (int i = 0; i < explanD.getExplanDList().size(); i++) {//批量修改
				ExplanD explan_=new ExplanD();
				explan_.setAcct(acct);
				explan_.setId(explanD.getExplanDList().get(i).getId());
				explan_.setAmountin(explanD.getExplanDList().get(i).getAmountin());
				explan_.setAmount1in(explanD.getExplanDList().get(i).getAmount1in());
				explan_.setExtim(explanD.getExplanDList().get(i).getExtim());
				explanMMapper.updateCntact(explan_);
				//保存登记时记录生成工时
				Supply su = new Supply();
				su.setSp_code(explanD.getExplanDList().get(i).getSupply().getSp_code());
				su.setPositnex(explanD.getExplanDList().get(i).getSupply().getPositnex());
				SpCodeExPrice spCodeExPrice = new SpCodeExPrice();
				spCodeExPrice.setAcct(acct);
				spCodeExPrice.setYearr(yearr);
				spCodeExPrice.setMonthh(monthh);
				spCodeExPrice.setSupply(su);
				SpCodeExPrice sp  = banChengPinPriceMapper.selectSpCodeExPrice(spCodeExPrice);
				if(sp!=null ){
					spCodeExPrice.setExtimSum(explanD.getExplanDList().get(i).getExtim());//wjf
					banChengPinPriceMapper.updateSpCodeExPrice(spCodeExPrice);
				}else{
					spCodeExPrice.setExtim(explanD.getExplanDList().get(i).getExtim());
					banChengPinPriceMapper.saveSpCodeExPrice(spCodeExPrice);
				}
			}
			result.put("updateNum", explanD.getExplanDList().size());
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的报货单主表
	 * @param explanM
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanSpcode> findExplanSpcodeBHM(ExplanSpcode explanSpcode, Page page, Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("explanSpcode", explanSpcode);
			map.put("madedEnd", madedEnd);
			return pageManager2.selectPage(map,page,ExplanMMapper.class.getName()+".findExplanSpcodeBHM");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询加工单明细
	 * @param explanno
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanD> toSelectDetailed(int explanno) throws CRUDException {
		try {
			ExplanD expland = new ExplanD();
			expland.setExplanno(explanno);
			return explanMMapper.toSelectDetailed(expland);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 报货到加工单导出excel  2014.10.21 wjf
	 * @param os
	 * @param fendianList
	 * @param list
	 * @return
	 */
	public boolean exportExplanM(ServletOutputStream os, List<Positn> fendianList,List<Map<String,Object>> list) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("报货到加工单导出表", 0);
			sheet.addCell(new Label(0, 0,"报货到加工单导出表",titleStyle));
			sheet.mergeCells(0, 0, 8+fendianList.size(), 0);
            sheet.addCell(new Label(0, 1, "物资编码"));   
            sheet.addCell(new Label(1, 1, "物资名称")); 
			sheet.addCell(new Label(2, 1, "规格"));  
			sheet.addCell(new Label(3, 1, "标准单位"));
			sheet.addCell(new Label(4, 1, "生产车间"));
			sheet.addCell(new Label(5, 1, "合计需求"));
			sheet.addCell(new Label(6, 1, "当前库存"));
			sheet.addCell(new Label(7, 1, "正加工量"));
			sheet.addCell(new Label(8, 1, "需加工量"));
			//遍历fendianList
			for(int i = 0;i < fendianList.size();i++){
				sheet.addCell(new Label(i+9,1,fendianList.get(i).getDes()));
			}
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).get("SP_CODE").toString()));
            	sheet.addCell(new Label(1, index, list.get(j).get("SP_NAME").toString()));
            	sheet.addCell(new Label(2, index, list.get(j).get("SP_DESC") == null ? "" : list.get(j).get("SP_DESC").toString()));
            	sheet.addCell(new Label(3, index, list.get(j).get("UNIT") == null ? "" : list.get(j).get("UNIT").toString()));
            	sheet.addCell(new Label(4, index, list.get(j).get("POSITNEXDES") == null ? "" : list.get(j).get("POSITNEXDES").toString()));
            	sheet.addCell(new Label(5, index, list.get(j).get("AMOUNT") == null ? "" : list.get(j).get("AMOUNT").toString()));
            	sheet.addCell(new Label(6, index, list.get(j).get("CNT") == null ? "" : list.get(j).get("CNT").toString()));
            	sheet.addCell(new Label(7, index, list.get(j).get("AMOUNT_NOW") == null ? "" : list.get(j).get("AMOUNT_NOW").toString()));
            	sheet.addCell(new Label(8, index, list.get(j).get("AMOUNT_XU") == null ? "" : list.get(j).get("AMOUNT_XU").toString()));
            	for(int i = 0;i < fendianList.size();i++){
            		String code = "F_"+fendianList.get(i).getCode();
            		if(list.get(j).get(code) != null){//如果这个门店得到的值不为空，则放到这个门店下
            			sheet.addCell(new Label(i+9,index,list.get(j).get(code).toString()));
            		}else{
            			continue;
            		}
    			}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 报货到加工单导出excel 20160415 jinshuai
	 * @param os
	 * @param fendianList
	 * @param list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean exportExplanM2(ServletOutputStream os, List<Positn> fendianList,List<Map<String,Object>> list) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableFont contentFont1 = new WritableFont(WritableFont.TIMES, 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle1 = new WritableCellFormat(contentFont1);// 文本内容格式
		WritableFont contentFont2 = new WritableFont(WritableFont.TIMES, 11, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle2 = new WritableCellFormat(contentFont2);// 文本内容格式
		WritableFont contentFont3 = new WritableFont(WritableFont.TIMES, 11, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle3 = new WritableCellFormat(contentFont3);// 文本内容格式
		
		try {
			// 表头文字居中
			titleStyle.setAlignment(Alignment.LEFT);
			titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			titleStyle.setBackground(Colour.GREY_25_PERCENT);

			// 设置边框
			titleStyle.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			titleStyle.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);
			
			workBook = Workbook.createWorkbook(os);
			
			// 设置边框
			contentStyle2.setBorder(jxl.format.Border.TOP, BorderLineStyle.THIN);
			contentStyle2.setBorder(jxl.format.Border.BOTTOM, BorderLineStyle.THIN);
			contentStyle2.setBorder(jxl.format.Border.RIGHT, BorderLineStyle.THIN);
			contentStyle2.setBorder(jxl.format.Border.LEFT, BorderLineStyle.THIN);
			
			//无数据
			if(list==null||list.size()==0){
				return false;
			}
			
			//无数据
			if(fendianList==null||fendianList.size()==0){
				return false;
			}
			
			//保存区分的加工间，及加工间下面的物资信息
			Map<String,Object> exfenzs = new HashMap<String,Object>();
			
			//把物资list集合根据物资分组
			//wzfenzs key为物资编码 value为对应物资对分店的数据
			Map<String,Map<String,Object>> wzfenzs = null;
			for(int i=0,len=list.size();i<len;i++){
				Map<String,Object> map = list.get(i);
				//先判断加工品属于哪个加工间
				String positnexdes = String.valueOf(map.get("POSITNEXDES"));
				String spCode = String.valueOf(map.get("SP_CODE"));
				if(exfenzs.containsKey(positnexdes)){
					//已经有保存的在此加工间的物资了
					wzfenzs = (Map<String,Map<String,Object>>)exfenzs.get(positnexdes);
					wzfenzs.put(spCode, map);
				}else{
					//没有这个加工间的物资
					wzfenzs = new HashMap<String,Map<String,Object>>();
					exfenzs.put(positnexdes, wzfenzs);
					wzfenzs.put(spCode, map);
				}
			}
			
			//开始行
			int beginLine = 0;
			//开始列
			int beginCols = 0;
			//循环输出
			Iterator<String> iter = exfenzs.keySet().iterator();
			int sheetNum = 0;
			Map<String,Map<String,Object>> wzfenzs2 = null;
			//循环加工间
			while(iter.hasNext()){
				String ex = iter.next();
				//循环创建sheet
				WritableSheet sheet = workBook.createSheet(ex==null?"其它加工间":ex.trim(), sheetNum++);
				wzfenzs2 = (Map<String,Map<String,Object>>)exfenzs.get(ex);
				
				//sheet写加工间
				sheet.mergeCells(beginCols, 0, beginCols+4, 1);
				sheet.addCell(new Label(beginCols,0,"加工间:"+ex,titleStyle));
				
				//循环物资
				Iterator<String> iter2 = wzfenzs2.keySet().iterator();
				int line = beginLine+2;
				while(iter2.hasNext()){
					//得到物资编码
					String spCode = iter2.next();
					//循环当前编码物资下面分店申购信息
					Map<String,Object> spCodeMap = wzfenzs2.get(spCode);
					//物资信息
					sheet.mergeCells(beginCols, line, beginCols+5, line+1);
					//物资名称
					String spName = String.valueOf(spCodeMap.get("SP_NAME"));
					//仓库库存量
					String spCnt = String.valueOf(spCodeMap.get("AMOUNT_NOW"));
					//需要加工量
					String amountXu = String.valueOf(spCodeMap.get("AMOUNT_XU"));
					String str = "物资编码："+spCode+"      "+spName+"      仓库库存量："+spCnt+"      车间需生产量："+amountXu;
					sheet.addCell(new Label(beginCols,line,str,contentStyle1));
					line = line + 2;
					//设置列宽
					sheet.setColumnView(0, 10);
					sheet.setColumnView(1, 20);
					sheet.setColumnView(2, 30);
					sheet.setColumnView(3, 20);
					sheet.setColumnView(4, 20);
					
	            	sheet.addCell(new Label(beginCols, line, "序号",contentStyle2));
	            	sheet.addCell(new Label(beginCols+1, line, "门店编码",contentStyle2));
	            	sheet.addCell(new Label(beginCols+2, line, "门店名称",contentStyle2));
	            	sheet.addCell(new Label(beginCols+3, line, "订货量",contentStyle2));
	            	sheet.addCell(new Label(beginCols+4, line, "单位",contentStyle2));
	            	
	            	line ++;
	            	//序列号
	            	int xlh = 1;
	            	Iterator<String> iter3 = spCodeMap.keySet().iterator();
					//循环写入excel中
					while(iter3.hasNext()){
						String thisKey = iter3.next();
						Positn ptn = null;
						String firmCode = "";
						String firmDes = "";
						String firmDhl = "";
						for(int x=0,xlen=fendianList.size();x<xlen;x++){
							ptn = fendianList.get(x);
							String dianCode = "F_"+ptn.getCode();
							if(thisKey.equals((dianCode))){
								firmDhl = String.valueOf(spCodeMap.get(thisKey));
								firmDes = ptn.getDes();
								firmCode = ptn.getCode();
				            	sheet.addCell(new Label(beginCols, line, String.valueOf(xlh++),contentStyle2));
				            	sheet.addCell(new Label(beginCols+1, line, firmCode,contentStyle2));
				            	sheet.addCell(new Label(beginCols+2, line, firmDes,contentStyle2));
				            	sheet.addCell(new Label(beginCols+3, line, firmDhl,contentStyle2));
				            	sheet.addCell(new Label(beginCols+4, line, String.valueOf(spCodeMap.get("UNIT")),contentStyle2));
							
				            	//下一行
				            	line ++;
							}
						}
					}
					
					//添加合计行
					sheet.addCell(new Label(beginCols+2, line,"合计：",contentStyle3));
					sheet.addCell(new Label(beginCols+3, line,String.valueOf(spCodeMap.get("AMOUNT")),contentStyle3));
					
					line ++;
					line ++;
				}
			}
			
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 原材料需求单据 导出excel   wangjie
	 * @param os
	 * @param productList
	 * @param list
	 */
	public boolean exportSupplyRequst(ServletOutputStream os, List<Product> productList,List<Map<String,Object>> list,String productmemo,String productcnt){
		
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("原材料需求单据", 0);
			sheet.addCell(new Label(0, 0,"原材料需求单据",titleStyle));
			sheet.mergeCells(0, 0, 8, 0);
            sheet.addCell(new Label(0, 1, "物资编码"));   
            sheet.addCell(new Label(1, 1, "物资名称")); 
			sheet.addCell(new Label(2, 1, "规格"));  
			sheet.addCell(new Label(3, 1, "单位"));
			sheet.addCell(new Label(4, 1, "计算量"));
			sheet.addCell(new Label(5, 1, "调整量"));
			sheet.addCell(new Label(6, 1, "成本单位"));
			sheet.addCell(new Label(7, 1, "成本数量"));
			sheet.addCell(new Label(8, 1, "备注"));
			
			for(int i = 0;i < productList.size(); i++){
				//原材料调整量
				Double adjustNums = productList.get(i).getCnt() - Double.parseDouble(productList.get(i).getKc());
			    Double limitNums = adjustNums < 0 ? 0 : adjustNums;
			    
				sheet.addCell(new Label(0, (i+2), productList.get(i).getSupply().getSp_code()));   
	            sheet.addCell(new Label(1, (i+2), productList.get(i).getSupply().getSp_name())); 
				sheet.addCell(new Label(2, (i+2), productList.get(i).getSupply().getSp_desc()));  
				sheet.addCell(new Label(3, (i+2), productList.get(i).getSupply().getUnit()));
				sheet.addCell(new Label(4, (i+2), String.format("%.2f",productList.get(i).getCnt())+""));
				sheet.addCell(new Label(5, (i+2), String.format("%.2f",limitNums)+""));
				sheet.addCell(new Label(6, (i+2), productList.get(i).getSupply().getUnit2()+""));
				sheet.addCell(new Label(7, (i+2), String.format("%.2f",productList.get(i).getCnt2())+""));
				sheet.addCell(new Label(8, (i+2), productList.get(i).getMemo()));
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @return
	 * @throws CRUDException
	 */
	public List<ExplanM> findByKey(HashMap<String,Object> explanMMap, Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(explanMMap, page, ExplanMMapper.class.getName()+".findByKey");			
			//return chkstomMapper.findByKey(chkstomMap);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public ExplanM findByExplanno(ExplanM explanM) throws CRUDException
	{
		try {
			return explanMMapper.findByExplanno(explanM);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	

	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的    
	 * 2014.10.20  wjf修改
	 */
	public int checkYnUnChk(ExplanM explanM) throws CRUDException
	{
		try {
			if ("Y".equals(explanMMapper.checkYnLL(explanM))){
				return -1;
			}
			return explanMMapper.checkYnUnChk(explanM);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
