package com.choice.scm.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ana.ExThCostAna;
import com.choice.scm.persistence.ExAnaMapper;

@Service
public class ExAnaService {

	@Autowired
	private ExAnaMapper exAnaMapper;
	@Autowired
	private PageManager<ExThCostAna> pageManager;
	/**
	 * 产品理论成本分析
	 */
	public List<ExThCostAna> exTheoryCostAna(ExThCostAna exThCostAna,Page page){
		exThCostAna.setPositnex(CodeHelper.replaceCode(exThCostAna.getPositnex()));
		return pageManager.selectPage(exThCostAna,page,ExAnaMapper.class.getName()+".exTheoryCostAna");
	}
	public List<ExThCostAna> exTheoryCostAna(ExThCostAna exThCostAna){
		exThCostAna.setPositnex(CodeHelper.replaceCode(exThCostAna.getPositnex()));
 		return exAnaMapper.exTheoryCostAna(exThCostAna);
	}
	/**
	 * 产品理论成本分析  合计
	 */
	public ExThCostAna exTheoryCostAna_total(ExThCostAna exThCostAna){
//		exThCostAna.setPositnex(CodeHelper.replaceCode(exThCostAna.getPositnex()));
		return exAnaMapper.exTheoryCostAna_total(exThCostAna);
	}

}