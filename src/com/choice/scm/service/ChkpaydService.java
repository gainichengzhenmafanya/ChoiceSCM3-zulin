package com.choice.scm.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Chkpayd;
import com.choice.scm.domain.Chkpaym;
import com.choice.scm.persistence.ChkpaydMapper;

@Service
public class ChkpaydService {

	@Autowired
	private ChkpaydMapper chkpaydMapper;
	private final transient Log log = LogFactory.getLog(ChkpaydService.class);

	
	/**
	 * 获取当前序号
	 * @return
	 * @throws CRUDException
	 */
	public int getCurId() throws CRUDException
	{
		try {
			return chkpaydMapper.getCurId();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据单号查询
	 * @param chkpayd
	 * @throws CRUDException
	 */
	public List<Chkpayd> findByChkno(Chkpayd chkpayd) throws CRUDException
	{
		try {
			return chkpaydMapper.findByChkno(chkpayd);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询所有的报销单
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkpayd> findAllChkpayd(Chkpaym chkpaym) throws CRUDException
	{
		try {
			return chkpaydMapper.findAllChkpayd(chkpaym);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 打印报销单
	 * @param chkpaym
	 * @return
	 * @throws CRUDException
	 */
	public Map<String,Object> printChkpay(Chkpaym chkpaym) throws CRUDException
	{
		try {
			return chkpaydMapper.printChkpay(chkpaym);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/**
	 * 保存修改
	 * @param chkpayd
	 * @throws CRUDException
	 */
	public void saveByUpdate(Chkpayd chkpayd) throws CRUDException
	{
		try {
			chkpaydMapper.saveByUpdate(chkpayd);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
