package com.choice.scm.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.util.ForResourceFiles;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.wsyd.util.DateFormat;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadChkoutExcel {
	
	// Excel表头信息
	static String[] excelColumns = { 
		"物资编码"                              
		,"物资名称"                              
		,"出库数量"                              
		,"备注"                              
	};
	// 类中信息
	static String[] supplyColumns = { 
		"sp_code"           
		,"sp_name"           
	    ,"amount.Double"
		,"memo"               
	};
	
	public static List<String> errorList;
	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Chkoutd> chkoutdList = new ArrayList<Chkoutd>();
	
	public static Chkoutm chkoutm;
	
	private static Chkoutd chkoutd;
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String accountId, String path, 
			SupplyMapper supplyMapper, PositnMapper positnMapper,DeliverMapper deliverMapper,CodeDesMapper codeDesMapper) {
		Boolean checkResult = true;
		Workbook book = null;
		chkoutm = new Chkoutm();
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				//0.判断单据类型
				Cell cell0 = sheet.getCell(1, 0);
				if(isEmpty(cell0.getContents())){
					errorList.add("单据类型不能为空");
					checkResult = false;
				}else if(!checkDJtype(cell0.getContents())){
					errorList.add("单据类型不存在");
					checkResult = false;
				}else{
					chkoutm.setTyp(cell0.getContents());
					//0.1  如果是填的报废单  还要读取报废原因和报废人
					if("9915".equals(cell0.getContents())){
						Cell cell01 = sheet.getCell(3, 3);//报废原因
						Cell cell02 = sheet.getCell(5, 3);//报废人
						if(!isEmpty(cell01.getContents())){
							if(checkBFYY(cell01.getContents(),codeDesMapper)){
								chkoutm.setMemo(cell01.getContents()+"-");
								if(!isEmpty(cell02.getContents())){
									chkoutm.setMemo(chkoutm.getMemo()+cell02.getContents());
								}
							}
						}
					}else{
						//其他的得到摘要
						Cell cell4 = sheet.getCell(1,3);
						if(!isEmpty(cell4.getContents())){
							chkoutm.setMemo(cell4.getContents());
						}
					}
				}
				//1.判断日期格式对不对
				Cell cell1 = sheet.getCell(3, 0);
				if(isEmpty(cell1.getContents())){
					errorList.add("填制日期不能为空");
					checkResult = false;
				}else if(!checkDate(cell1.getContents())){
					errorList.add("填制日期格式不对");
					checkResult = false;
				}else{
					chkoutm.setMaded(DateFormat.getDateByString(cell1.getContents(), "yyyy-MM-dd"));
				}
				//2.判断仓位有没有
				Cell cell2 = sheet.getCell(1,1);
				if(isEmpty(cell2.getContents())){
					errorList.add("仓位不能为空");
					checkResult = false;
				}else{
					Positn positn = checkPositn(accountId,cell2.getContents(),positnMapper);
					if(positn == null){
						errorList.add("仓位不正确");
						checkResult = false;
					}else{
						chkoutm.setPositn(positn);
					}
				}
				//3.判断供应商
				Cell cell3 = sheet.getCell(1,2);
				if(isEmpty(cell3.getContents())){
					errorList.add("使用仓位不能为空");
					checkResult = false;
				}else{
					Positn positn = checkPositn(accountId,cell3.getContents(),positnMapper);
					if(positn == null){
						errorList.add("使用仓位不正确");
						checkResult = false;
					}else{
						chkoutm.setFirm(positn);
					}
				}
				
				//5.判断物资
				if (!checkSupply(accountId,rowNum, colNum, sheet,supplyMapper)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	/***
	 * 校验报废原因
	 * @param contents
	 * @return
	 */
	private static boolean checkBFYY(String dj,CodeDesMapper codeDesMapper) {
		CodeDes codDes = new CodeDes();
		codDes.setDes(dj);
		codDes.setTyp("13");
		List<CodeDes> codess = codeDesMapper.findCodeDes(codDes);
		if(codess.size() == 0){
			return false;
		}else{
			return true;
		}
	}

	/***
	 * 校验单据类型 wjf
	 * 9908	正常出库
		9909	调拨出库
		9910	盘亏出库
		9911	赠品出库
		9912	代销出库
		9913	出库退库
		9914	出库冲消
		9915	出库报废
	 * @return
	 */
	private static boolean checkDJtype(String dj){
		if(dj.equals("9908") || dj.equals("9909") ||dj.equals("9910") ||dj.equals("9911") 
				||dj.equals("9912") ||dj.equals("9913") ||dj.equals("9914") ||dj.equals("9915")){
			return true;
		}
		return false;
	}
	
	
	/***
	 * 判断日期对不对 wjf
	 */
	private static boolean checkDate(String date){
		java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
		try{
			format.parse(date);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/***
	 * 判断是否有此仓位
	 * @param positncode
	 * @param positnMapper
	 * @return
	 */
	private static Positn checkPositn(String accountId,String positncode,PositnMapper positnMapper){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("cwqx", ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
		map.put("accountId", accountId);
		map.put("searchInfo", positncode);
		List<Positn> positns = positnMapper.findPositnBy(map);
		if(positns.size() == 0){
			return null;
		}else{
			return positns.get(0);
		}
	}

	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * 
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(String accountId,int rowNum, int colNum, Sheet sheet,SupplyMapper supplyMapper)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		chkoutdList.clear();
		for (int i = 5; i < rowNum; i++) { // 行循环
			chkoutd = new Chkoutd();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if (col == 0 || col == 2) { // 判断必填字段是否为空
					if (isEmpty(cell.getContents())) {
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "为空");
						bool = false;
						continue;
					}
				}
				//验证
				if (!("").equals(cell.getContents())) {
					bool = validateColumns(cell.getContents(),i,col,supplyMapper);
				}
				if(bool){
					if (!("").equals(cell.getContents())) {// 如果不为""时将物资信息保存到实体类中
						if(col == 2){//放数量
							chkoutd.setAmount(Double.parseDouble(cell.getContents()));
						}else if(col == 3){//放备注
							chkoutd.setMemo(cell.getContents());
						}
					}
				}
				booleanList.add(bool);
			}
			if(bool){
				//1.参考数量需要算出来。。
				if(chkoutd.getSp_code() != null && chkoutm.getFirm() != null && chkoutm.getMaded() != null){
					chkoutd.setAmount1(chkoutd.getAmount()*chkoutd.getSp_code().getUnitper());
					//2.取报价，没有报价取标准价
					Spprice sprice = new Spprice();
					sprice.setAcct(accountId);
					sprice.setSp_code(chkoutd.getSp_code().getSp_code());
					sprice.setArea(chkoutm.getFirm().getCode());
					sprice.setMadet(DateFormat.getStringByDate(chkoutm.getMaded(), "yyyy-MM-dd"));
					sprice = supplyMapper.findBprice(sprice);
					if (sprice == null){
						chkoutd.setPrice(chkoutd.getSp_code().getSp_price());//没有就取标准价
					}else{
						chkoutd.setPrice(sprice.getPrice().doubleValue());
					}
					//1.取售价放到price里，取报价放到pricesale里
					SppriceSale spriceSale = new SppriceSale();
					spriceSale.setSupply(chkoutd.getSp_code());
					spriceSale.setAcct(accountId);
					spriceSale.setArea(chkoutm.getFirm());
					spriceSale.setMadet(chkoutm.getMadet());
					spriceSale = supplyMapper.findSprice(spriceSale);
					if (spriceSale == null){
						chkoutd.setPricesale(chkoutd.getSp_code().getSp_price());//没有就取标准价
						chkoutd.setTotalamt(chkoutd.getAmount()*chkoutd.getPricesale());
					}else{
						chkoutd.setPricesale(spriceSale.getPrice().doubleValue());
						chkoutd.setTotalamt(chkoutd.getAmount()*chkoutd.getPricesale());
					}
					
				}
				chkoutdList.add(chkoutd);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	private static boolean validateColumns(String cell, int i,int col,SupplyMapper supplyMapper) {
		boolean bool=true;
		switch (col) {
		case 0:
			//验证编码
			String regCode = "^[\\d\\w][\\d\\w-]*";;
			if(cell.matches(regCode)){
				Supply supply = checkSupply(cell,supplyMapper);
				if(supply == null){
					bool = false;
					errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "不存在");
				}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
					chkoutd.setSp_code(supply);
					bool = true;
				}
			}else{
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				bool = false;
			}
			break;
		case 2:
			//验证报货数量
			try{
				Double.parseDouble(cell);
				bool = true;
				break;
			}catch(Exception e){
				bool = false;
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		default:
			break;
		}
		return bool;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @param supplyMapper
	 * @return
	 */
	private static Supply checkSupply(String code,SupplyMapper supplyMapper){
		Supply supply = new Supply();
		supply.setSp_code(code);
		Supply supply1 = supplyMapper.findSupplyById(supply);
		return supply1;
	}
	
	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

}
