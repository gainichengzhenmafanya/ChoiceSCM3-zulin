package com.choice.scm.service;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ChartsXml;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.CreateChart;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Condition;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.ReportModule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.persistence.MainInfoMapper;

@Service
public class MainInfoService {

	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private PageManager<Supply> pageManager;
	
	private final transient Log log = LogFactory.getLog(MainInfoService.class);
	
	/**
	 * 查询 帐套信息
	 * @param mainInfo
	 * @return
	 * @throws CRUDException
	 */
	public MainInfo findMainInfo(int yearr) throws CRUDException{
		try{
			//System.out.println("调用这个方法获取时间了没????????????"+mainInfoMapper.findMainInfo(yearr));
			return mainInfoMapper.findMainInfo(yearr);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据年份查询 帐套信息
	 * @param mainInfo
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public Object findMainInfoByYear(int yearr) throws CRUDException{
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			MainInfo mainInfo = mainInfoMapper.findMainInfo(yearr);
			resultMap.put("acct", mainInfo.getAcct());
			resultMap.put("yearr", mainInfo.getYearr());
			resultMap.put("monthh", mainInfo.getMonthh());
			resultMap.put("chktag", mainInfo.getChktag());
			
			resultMap.put("bdat1", DateFormat.getStringByDate(mainInfo.getBdat1(), "yyyy-MM-dd"));
			resultMap.put("edat1", DateFormat.getStringByDate(mainInfo.getEdat1(), "yyyy-MM-dd"));
			resultMap.put("bdat2", DateFormat.getStringByDate(mainInfo.getBdat2(), "yyyy-MM-dd"));
			resultMap.put("edat2", DateFormat.getStringByDate(mainInfo.getEdat2(), "yyyy-MM-dd"));  
			resultMap.put("bdat3", DateFormat.getStringByDate(mainInfo.getBdat3(), "yyyy-MM-dd"));
			resultMap.put("edat3", DateFormat.getStringByDate(mainInfo.getEdat3(), "yyyy-MM-dd"));
			resultMap.put("bdat4", DateFormat.getStringByDate(mainInfo.getBdat4(), "yyyy-MM-dd"));
			resultMap.put("edat4", DateFormat.getStringByDate(mainInfo.getEdat4(), "yyyy-MM-dd"));
			resultMap.put("bdat5", DateFormat.getStringByDate(mainInfo.getBdat5(), "yyyy-MM-dd"));
			resultMap.put("edat5", DateFormat.getStringByDate(mainInfo.getEdat5(), "yyyy-MM-dd"));
			resultMap.put("bdat6", DateFormat.getStringByDate(mainInfo.getBdat6(), "yyyy-MM-dd"));
			resultMap.put("edat6", DateFormat.getStringByDate(mainInfo.getEdat6(), "yyyy-MM-dd"));
			resultMap.put("bdat7", DateFormat.getStringByDate(mainInfo.getBdat7(), "yyyy-MM-dd"));
			resultMap.put("edat7", DateFormat.getStringByDate(mainInfo.getEdat7(), "yyyy-MM-dd"));
			resultMap.put("bdat8", DateFormat.getStringByDate(mainInfo.getBdat8(), "yyyy-MM-dd"));
			resultMap.put("edat8", DateFormat.getStringByDate(mainInfo.getEdat8(), "yyyy-MM-dd"));
			resultMap.put("bdat9", DateFormat.getStringByDate(mainInfo.getBdat9(), "yyyy-MM-dd"));
			resultMap.put("edat9", DateFormat.getStringByDate(mainInfo.getEdat9(), "yyyy-MM-dd"));
			resultMap.put("bdat10", DateFormat.getStringByDate(mainInfo.getBdat10(), "yyyy-MM-dd"));
			resultMap.put("edat10", DateFormat.getStringByDate(mainInfo.getEdat10(), "yyyy-MM-dd"));
			resultMap.put("bdat11", DateFormat.getStringByDate(mainInfo.getBdat11(), "yyyy-MM-dd"));
			resultMap.put("edat11", DateFormat.getStringByDate(mainInfo.getEdat11(), "yyyy-MM-dd"));
			resultMap.put("bdat12", DateFormat.getStringByDate(mainInfo.getBdat12(), "yyyy-MM-dd"));
			resultMap.put("edat12", DateFormat.getStringByDate(mainInfo.getEdat12(), "yyyy-MM-dd"));

		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}finally{
			JSONObject rs = JSONObject.fromObject(resultMap);
			return rs.toString();
		}
		
	}
	
	/**
	 * 查询所有帐套年份
	 */
	public List<String> findYearrList() throws CRUDException{
		try{
			return mainInfoMapper.findYearrList();
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改帐套信息
	 */
	public void updateMainInfo(MainInfo mainInfo) throws CRUDException{
		try{
			mainInfoMapper.updateMainInfo(mainInfo);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
//------------------------------------------------------------------------------------	
	/**
	 * 查询前10条物资信息超限量
	 */
	public List<Supply> findSupplyOver(Map<String,Object> map) throws CRUDException{
		try{
			return mainInfoMapper.findSupplyOver(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 图表查询前10条物资信息超限量
	 */
	public ChartsXml findXmlForSupplyOver(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			List<Supply> list = mainInfoMapper.findSupplyOver(map);
			
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Supply supply = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", supply.getSp_name());
				dataMap.put("value", supply.getTax());
				dataMap.put("hoverText", supply.getSp_name()+" 当前库存:"+supply.getCnt()+" 最大限量:"+supply.getSp_max1()+" 超限量:"+(supply.getCnt()-supply.getSp_max1()));
				dataList.add(dataMap);
			}
			
			return CreateChart.createHistogram(response, "库存超上限TOP10", "", "超上限比率(%)", dataList, "12");
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询前10条物资信息库存不足
	 */
	public List<Supply> findSupplyLow(Map<String,Object> map) throws CRUDException{
		try{
			return mainInfoMapper.findSupplyLow(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 图表查询前10条物资信息不足限量
	 */
	public ChartsXml findXmlForSupplyLow(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			List<Supply> list = mainInfoMapper.findSupplyLow(map);
			
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Supply supply = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", supply.getSp_name());
				dataMap.put("value", supply.getTax());
				dataMap.put("hoverText", supply.getSp_name()+" 当前库存:"+supply.getCnt()+" 最低限量:"+supply.getSp_min1()+" 不足限量:"+(supply.getSp_min1()-supply.getCnt()));
				dataList.add(dataMap);
			}
			
			return CreateChart.createHistogram(response, "库存不足下限TOP10", "", "不足下限比率(%)", dataList, "12");
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询全部物资信息超限量
	 */
	public List<Supply> findAllSupplyOver(Supply supply,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(supply,page, MainInfoMapper.class.getName()+".findAllSupplyOver");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询全部物资信息库存不足
	 */
	public List<Supply> findAllSupplyLow(Supply supply,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(supply,page, MainInfoMapper.class.getName()+".findAllSupplyLow");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询入库单单据汇总信息
	 */
	public List<Map<String,Object>> findStackBillSum(Map<String,Object> condition) throws CRUDException{
		try{
			return mainInfoMapper.findStackBillSum(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询出库单单据汇总信息
	 */
	public List<Map<String,Object>> findOutBillSum(Map<String,Object> condition) throws CRUDException{
		try{
			return mainInfoMapper.findOutBillSum(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询未审核信息
	 */
	public List<Map<String,Object>> findUnChecked(Map<String,Object> map) throws CRUDException{
		try{
			return mainInfoMapper.findUnChecked(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询当月出库金额Top10
	 * @return
	 */
	public ChartsXml findXmlForOutTop10(HttpServletResponse response) throws Exception{
		Date bdat = DateFormat.formatDate(DateFormat.getFirstDayOfCurrMonth(), "yyyy-MM-dd");
		Date edat = DateFormat.formatDate(new Date(), "yyyy-MM-dd");
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("bdat", bdat);
		paramMap.put("edat", edat);
		List<Map<String,Object>> list =  mainInfoMapper.findOutTop10(paramMap);
		
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		Map<String,Object> dataMap;
		for(Map<String,Object> map:list){
			dataMap = new HashMap<String, Object>();
			dataMap.put("name", ""+map.get("name"));
			dataMap.put("value", ""+map.get("amtout"));
			dataMap.put("hoverText", map.get("cntout")+" "+map.get("unit")+","+map.get("amtout"));
			dataList.add(dataMap);
		}
		
		return CreateChart.createArea2D(response, "本月出库物资TOP 10", "", "", dataList,"15");
		/*//---------------------------------------------------------------
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("name", "一月");
		map.put("value", 15);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "二月");
		map.put("value", 25);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "三月");
		map.put("value", 35);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "四月");
		map.put("value", 55);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "五月");
		map.put("value", 65);
		dataList.add(map);
		map.put("name", "6月");
		map.put("value", 95);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "7月");
		map.put("value", 125);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "8月");
		map.put("value", 135);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "9月");
		map.put("value", 155);
		dataList.add(map);
		map = new HashMap<String, Object>();
		map.put("name", "10月");
		map.put("value", 185);
		map.put("hoverText", 185);
		dataList.add(map);
		
		return CreateChart.createArea2D(response, "测试", "月", "值", dataList, null);*/
	}
	
	/**
	 * 图表查询进货单据汇总
	 */
	public ChartsXml findXmlForPurchaseInSum(HttpServletResponse response,Map<String,Object> condition) throws CRUDException{
		try{
			List<Map<String,Object>> list = mainInfoMapper.findStackBillSum(condition);
			
			List<String> xNameList = new ArrayList<String>();
			List<String> typeList = new ArrayList<String>();
			List<Object[]> dataList = new ArrayList<Object[]>();
			Object[] inObj = new Object[list.size()];
			Object[] directObj = new Object[list.size()];
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				xNameList.add(""+map.get("DELIVER"));
				inObj[i] = map.get("INAMT");
				directObj[i] = map.get("DAMT");
			}
			typeList.add("入库");
			typeList.add("直发");
			
			dataList.add(inObj);
			dataList.add(directObj);
			return CreateChart.createScrollColumn2D(response, "进货单据汇总", xNameList, typeList, "", dataList, null);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 图表查询出库单据汇总
	 */
	public ChartsXml findXmlForOutBillSum(HttpServletResponse response,Map<String,Object> condition) throws CRUDException{
		try{
			List<Map<String,Object>> list = mainInfoMapper.findOutBillSum(condition);
			
			List<String> xNameList = new ArrayList<String>();
			List<String> typeList = new ArrayList<String>();
			List<Object[]> dataList = new ArrayList<Object[]>();
			Object[] Obj = new Object[list.size()];
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				xNameList.add(""+map.get("POSITNDES"));
				Obj[i] = map.get("AMT");
			}
			typeList.add("仓位金额");
			
			dataList.add(Obj);
			return CreateChart.createScrollColumn2D(response, "出库单据汇总", xNameList, typeList, "", dataList, null);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
//------------------------------------------------------------------------------------------------------	
	/**
	 * 用户保存工作台信息
	 */
	public void saveWorkBench(WorkBench workbench) throws CRUDException{
		try{
			mainInfoMapper.saveWorkBench(workbench);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据角色查询工作台信息
	 * @param id
	 * @return
	 */
	public String findRoleWorkBench(String id) throws CRUDException{
		try{
			return mainInfoMapper.findRoleWorkBench(id);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据用户查询工作台信息
	 */
	public WorkBench findWorkBench(String accountId) throws CRUDException{
		try{
			return mainInfoMapper.findWorkBench(accountId);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据用户角色修改工作台信息
	 * @param workbench
	 */
	public void updateWorkBench(WorkBench workbench) throws CRUDException{
		try{
			mainInfoMapper.updateWorkBench(workbench);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存常用操作模块id
	 * @param workBench
	 * @return
	 * @throws CRUDException
	 */
	public String saveUsedModel(WorkBench workBench) throws CRUDException{
		try{
			String result ="yes";
			mainInfoMapper.saveUsedModel(workBench);
			return result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	//-------------------------------------------------------临时功能，待定--------------------------------------------------------------------------------
	/**
	 * 门店营业额
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> firmAmtList(Condition condition) throws CRUDException{
		try{
			return mainInfoMapper.firmAmtList(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 事业群总营业额
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findModAmt(HttpServletResponse response, Condition condition) throws CRUDException{
		try{
			List<Map<String, Object>>  list = mainInfoMapper.modAmtList(condition);
			
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", map.get("MODNAM"));
				dataMap.put("value",  map.get("AMT"));
				dataList.add(dataMap);
			}
			return CreateChart.createDoughnut2D(response, "事业群总营业额", dataList, "11");
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 本月营业额走势图
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findModLineAmt(HttpServletResponse response, Condition condition) throws CRUDException{
		try{
			List<Map<String, Object>>  list = mainInfoMapper.modAmtLineList(condition);
			
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name",map.get("DAT"));
				dataMap.put("value",  map.get("AMT"));
				dataMap.put("hoverText",  map.get("DAT")+","+map.get("AMT")+"元");
				dataList.add(dataMap);
			}
			return CreateChart.createLine(response, "本月营业额走势图", "", "营业额", dataList, "11");
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 本月营业额走势图 多折线
	 * @param response
	 * @param condition
	 * @param conBefore
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findModMultiLineAmt(HttpServletResponse response, Condition condition,Condition conBefore) throws CRUDException{
		try{
			List<Map<String, Object>>  listBefore = mainInfoMapper.modAmtLineListBefore(conBefore);
			List<Map<String, Object>>  list = mainInfoMapper.modAmtLineList(condition);

			List<Map<String,Object>> dataListBefore = new ArrayList<Map<String,Object>>();
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", "第"+(i+1)+"周");
				dataMap.put("hoverText", DateFormat.getYear(condition.getBdat())+"年"+DateFormat.getMonth(condition.getBdat())+"月第"+(i+1)+"周"+","+map.get("AMT"));
				dataMap.put("value",  map.get("AMT"));
				dataList.add(dataMap);
			}
			for(int i=0;i<listBefore.size();i++){
				Map<String,Object> map = listBefore.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", "第"+(i+1)+"周");
				dataMap.put("hoverText", DateFormat.getYear(conBefore.getBdat())+"年"+DateFormat.getMonth(conBefore.getBdat())+"月第"+(i+1)+"周"+","+map.get("AMT"));
				dataMap.put("value",  map.get("AMT"));
				dataListBefore.add(dataMap);
			}
			
//			return CreateChart.createMultiLine(response, "本月营业额走势图", "", "营业额", dataList, "11");
			Map<String,String> attribute = new HashMap<String, String>();
			attribute.put("caption", "营业额走势图");
//			attribute.put("formatNumber", "2");
			attribute.put("decimalPrecision", "2");
			attribute.put("showValues", "0");
			attribute.put("formatNumberScale", "3");
			ChartsXml xml = ChartsXml.builtChartsXml(attribute, response);
			attribute = new HashMap<String,String>();
			attribute.put("font", "Arial");
			attribute.put("fontSize", "11");
			attribute.put("fontColor", "000000");
			attribute.put("captionPadding","0");
			attribute.put("chartTopMargin","0");
			attribute.put("chartBottomMargin","0");
			attribute.put("showvalues","0");
			attribute.put("formatNumberScale","0");
			attribute.put("slantLabels","1");
			xml.addElement("categories", attribute);
			for(int i = 1;i<=5;i++){
				attribute = new HashMap<String,String>();
				attribute.put("name", "第"+i+"周");
				xml.addElement("categories", "category", attribute);
			}
			Random ran = new Random(666);
			attribute.put("seriesname", "1");
			attribute.put("color", String.format("%06x", ran.nextInt(0xffffff)));
			xml.addElement("dataset", attribute);
			for(Map<String,Object> map:dataListBefore){
		    	Map<String,String> attr = new HashMap<String,String>();
		    	try{
		    		attr.put("name", map.get("name").toString());
		    	}catch(Exception e){
		    		System.out.println("名称为空！");
		    	}
		    	try{
		    		attr.put("value", map.get("value").toString());
		    	}catch(Exception e){
		    		System.out.println("值为空！");
		    	}
				try{
					attr.put("hoverText", map.get("hoverText").toString());
				}catch(Exception e){
					System.out.println("图标提示为空！");
				}
					
//				attr.put("color", String.format("%06x", ran.nextInt(0xffffff)));
				xml.addElement("dataset","set", attr);
		    }
			attribute = new HashMap<String,String>();
			attribute.put("seriesname", "2");
			attribute.put("color", String.format("%06x", ran.nextInt(0xffffff)));
			xml.addElement("dataset", attribute);
			for(Map<String,Object> map:dataList){
		    	Map<String,String> attr = new HashMap<String,String>();
		    	try{
		    		attr.put("name", map.get("name").toString());
		    	}catch(Exception e){
		    		System.out.println("名称为空！");
		    	}
		    	try{
		    		attr.put("value", map.get("value").toString());
		    	}catch(Exception e){
		    		System.out.println("值为空！");
		    	}
				try{
					attr.put("hoverText", map.get("hoverText").toString());
				}catch(Exception e){
					System.out.println("图标提示为空！");
				}
					
//				attr.put("color", String.format("%06x", ran.nextInt(0xffffff)));
				xml.addElement("dataset","set", attr);
		    }
			return  xml;
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 菜品类别排行（柱状）
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findCaiPinLeiBiePaiHang(HttpServletResponse response, Condition condition) throws CRUDException{
		try{
			List<Map<String, Object>>  list = mainInfoMapper.queryDdxstjbycltj(condition);
			List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				Map<String,Object> dataMap = new HashMap<String, Object>();
				dataMap.put("name", map.get("DES"));
				dataMap.put("value",  map.get("AMTT"));
				dataMap.put("hoverText",  map.get("DES")+","+map.get("AMTT")+"元");
				dataList.add(dataMap);
			}
			
			return CreateChart.createHistogram(response, "菜品类别排行", "", "营业额", dataList, "11");
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 昨日未上传分店
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> getUnUploadFirm(Condition condition) throws CRUDException{
		try{
			return mainInfoMapper.getUnUploadFirm(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询所有的会计年列表
	 * @author wjf
	 * @param acct
	 * @return
	 */
	public List<MainInfo> findAllYears(String acct) {
		return mainInfoMapper.findAllYears(acct);
	}
	
	//---------------------------总部BOH快餐--------------------------------
		/**
		 * 描述：门店营业额
		 * @param response
		 * @param map
		 * @return
		 * @throws CRUDException
		 * author:spt
		 * 日期：2014-5-7
		 */
		public List<Map<String, Object>> findStoreYye() throws CRUDException{
			try{
				String dateString = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", -1, 1),"yyyy-MM-dd");
				 List<Map<String, Object>> list= mainInfoMapper.findStoreYye(dateString);
				return list;
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		/**
		 * 描述：类别销售额对比
		 * @param response
		 * @return
		 * @throws CRUDException
		 * author:spt
		 * 日期：2014-5-7
		 */
		public ChartsXml findLbtjList(HttpServletResponse response) throws CRUDException{
			try{
				String dateString = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", -1, 1),"yyyy-MM-dd");
				List<Map<String, Object>> list= mainInfoMapper.findLbtjList(dateString);
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				for(int i=0;i<list.size();i++){
					Map<String,Object> mapObj= list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", mapObj.get("NAME"));
					dataMap.put("value", mapObj.get("NYMONEY"));
					dataList.add(dataMap);
				}
				
				return CreateChart.createHistogram(response, "类别销售对比", "", "金额", dataList, "14");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		/**
		 * 描述：市场销售额对比
		 * @param response
		 * @return
		 * @throws CRUDException
		 * author:spt
		 * 日期：2014-5-7
		 */
		public ChartsXml findSczbList(HttpServletResponse response) throws CRUDException{
			try{
				String dateString = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", -1, 1),"yyyy-MM-dd");
				List<Map<String, Object>> list= mainInfoMapper.findSczbList(dateString);
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				for(int i=0;i<list.size();i++){
					Map<String,Object> mapObj= list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", mapObj.get("VNAME"));
					dataMap.put("value", mapObj.get("TOTALMONEY"));
					dataList.add(dataMap);
				}
				return CreateChart.createDoughnut2D(response, "市场营业额占比", dataList, "11");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		
		/**
		 *TODO:快餐  我的桌面  4个仪表盘
		 *@return
		 *@throws CRUDException
		 *@author:spt
		 *2015-1-22 上午10:55:29
		 */
		public List<Map<String,Object>> getYiBiaoPanKcBoh() throws CRUDException{
			try{
				SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
				Calendar calendar = Calendar.getInstance();
				String pk_store=null;
				String bdat=sft.format(DateFormat.getFirstDayOfCurrMonth());
				String edat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				calendar.setTime(DateFormat.getFirstDayOfCurrMonth()); 
				calendar.add(Calendar.MONTH, -1);    //减一个月
				String sbdat=sft.format(calendar.getTime());
				calendar.setTime(DateFormat.getDateBefore(new Date(),"day",-1,1)); 
				calendar.add(Calendar.MONTH, -1);    //减一个月	
				String sedat=sft.format(calendar.getTime());
				
				return mainInfoMapper.getYiBiaoPanKcBoh(pk_store, bdat, edat, sbdat, sedat);
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		 
		/**
		 *TODO:本月营业额走势图 快餐
		 *@param response
		 *@return
		 *@throws CRUDException
		 *@author:spt
		 *2015-1-22 下午1:39:03
		 */
		public ChartsXml findTotalMoneyQushiByDays(HttpServletResponse response) throws CRUDException{
			try{
				Condition condition = new Condition();
				Calendar calendar = Calendar.getInstance();
				Integer dateNum=0;
				dateNum = calendar.get(Calendar.DATE); 
				String sqlstr="";
				String tempstr="";
				for(int i=1;i<=dateNum;i++){
					tempstr=""+i;
					if(i<10) tempstr="0"+i;
					if(i<dateNum){
						sqlstr += "SELECT '"+tempstr+"' AS DWORKDATE UNION ";
					}
					if(i==dateNum){
						sqlstr += "SELECT '"+tempstr+"' AS DWORKDATE ";
					}
				}
				condition.setSqlStr1(sqlstr);
				List<Map<String, Object>>  list = mainInfoMapper.findTotalMoneyQushiByDays(condition,dateNum);
				
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
//				String[] month={"一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"};
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", map.get("DWORKDATE"));
					dataMap.put("value",  map.get("TOTALMONEY"));
					dataList.add(dataMap);
				}
				return CreateChart.createHistogram(response, "本月营业额走势图", "", "营业额", dataList, "11");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		/**
		 *TODO:菜品类别占比
		 *@param response
		 *@return
		 *@throws CRUDException
		 *@author:spt
		 *2015-1-22 下午3:26:12
		 */
		public ChartsXml findCplbzbList(HttpServletResponse response) throws CRUDException{
			try{
				String dateString = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", -1, 1),"yyyy-MM-dd");
				List<Map<String, Object>> list= mainInfoMapper.findCplbzbList(dateString);
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				for(int i=0;i<list.size();i++){
					Map<String,Object> mapObj= list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", mapObj.get("NAME"));
					dataMap.put("value", mapObj.get("NMONEY"));
					dataList.add(dataMap);
				}
				return CreateChart.createDoughnut2D(response, "菜品类别占比", dataList, "11");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		
		//---------------------------总部BOH中餐--------------------------------
		/**
		 *四个仪表盘
		 */
		public List<Map<String,Object>> getYiBiaoPan() throws CRUDException{
			try{
				SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
				Calendar calendar = Calendar.getInstance();
				String pk_store=null;
				String bdat=sft.format(DateFormat.getFirstDayOfCurrMonth());
				String edat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				calendar.setTime(DateFormat.getFirstDayOfCurrMonth()); 
				calendar.add(Calendar.MONTH, -1);    //减一个月
				String sbdat=sft.format(calendar.getTime());
				calendar.setTime(DateFormat.getDateBefore(new Date(),"day",-1,1)); 
				calendar.add(Calendar.MONTH, -1);    //减一个月	
				String sedat=sft.format(calendar.getTime());
				
				return mainInfoMapper.getYiBiaoPanKcBoh(pk_store, bdat, edat, sbdat, sedat);
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}

		/**
		 * 门店营业额    多折现
		 * @param response
		 * @param condition
		 * @param conBefore
		 * @return
		 * @throws CRUDException
		 */
		public ChartsXml findModMultiAmtList(HttpServletResponse response) throws CRUDException{
			try{
				SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
				String pk_store=null;
				String bdat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				String edat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				
				List<Map<String, Object>>  list =mainInfoMapper.firmAmtList(pk_store, bdat, edat);

				List<Map<String,Object>> dataListBefore = new ArrayList<Map<String,Object>>();
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", map.get("VNAME"));
					dataMap.put("hoverText",  ""+map.get("VNAME")+map.get("AMT"));
					dataMap.put("value",  map.get("AMT"));
					dataList.add(dataMap);
				}
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name",  map.get("VNAME"));
					dataMap.put("hoverText",  ""+map.get("VNAME")+map.get("CNT"));
					dataMap.put("value",  map.get("CNT"));
					dataListBefore.add(dataMap);
				}
				return CreateChart.createHistogram(response, "各门店昨日营业额对比分析图", "", "营业额", dataList, "11");
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		
		/**
		 * 集团月营业额分析柱状图表
		 * @param response
		 * @param map
		 * @return
		 * @throws CRUDException
		 */
		public ChartsXml jituanyingyeeyuefenxi(HttpServletResponse response) throws CRUDException{
			try{
				List<Map<String, Object>>  list = mainInfoMapper.jituanyingyeeyuefenxi();
				
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				String[] month={"一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"};
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", month[i]);
					dataMap.put("value",  map.get("MONTH1"));
					dataList.add(dataMap);
				}
				return CreateChart.createHistogram(response, "集团营业额按月对比分析图", "", "营业额", dataList, "11");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
		
		/**
		 * 菜品类别排行（柱状）
		 * @param response
		 * @param map
		 * @return
		 * @throws CRUDException
		 */
		public ChartsXml findCaiPinLeiBiePaiHang(HttpServletResponse response) throws CRUDException{
			try{
				SimpleDateFormat sft = new SimpleDateFormat("yyyy-MM-dd");
				String pk_store=null;
				String bdat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				String edat=sft.format(DateFormat.getDateBefore(new Date(),"day",-1,1));
				
				List<Map<String, Object>>  list = mainInfoMapper.queryDdxstjbycltj(pk_store, bdat, edat);
				List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					Map<String,Object> dataMap = new HashMap<String, Object>();
					dataMap.put("name", map.get("DES"));
					dataMap.put("value",  map.get("AMT"));
					dataMap.put("hoverText",  map.get("DES")+","+map.get("AMT")+"元");
					dataList.add(dataMap);
				}
				
				return CreateChart.createHistogram(response, "菜品类别排行", "", "营业额", dataList, "11");
				
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}

		/***
		 * 根据门店和日期查询配送班表
		 * @param scheduleD
		 * @return
		 */
		public List<ScheduleD> findSchedulesByPositn(ScheduleD scheduleD) {
			return mainInfoMapper.findSchedulesByPositn(scheduleD);
		}

		/***
		 * 查询直配需要验货的数量 根据供应商分组显示
		 * @param dis
		 * @return
		 */
		public List<Dis> findDireCountGroupbyDeliver(Dis dis) {
			return mainInfoMapper.findDireCountGroupbyDeliver(dis);
		}
		
		/***
		 * 查询统配需要验货的总数量
		 * @param dis
		 * @return
		 */
		public List<SupplyAcct> findOutCount(Dis dis) {
			return mainInfoMapper.findOutCount(dis);
		}
		
		/***
		 * 查询调拨需要验货的总数 根据门店来查
		 * @param dis
		 * @return
		 */
		public List<SupplyAcct> findDbCountGroupByPositn(Dis dis) {
			return mainInfoMapper.findDbCountGroupByPositn(dis);
		}

		/***
		 * 查询配置的报表
		 * @param rm
		 * @return
		 */
		public List<ReportModule> findMyReportModule(ReportModule rm) {
			return mainInfoMapper.findMyReportModule(rm);
		}

		/***
		 * 查询所有报表
		 * @param rm
		 * @return
		 */
		public List<ReportModule> findAllReportModule(ReportModule rm) {
			return mainInfoMapper.findAllReportModule(rm);
		}

		/***
		 * 保存我的常用报表
		 * @param rm
		 * @return
		 * @throws CRUDException 
		 */
		public void saveUsedReport(ReportModule rm) throws CRUDException {
			try{
				mainInfoMapper.deleteUsedReport(rm);
				for(ReportModule r : rm.getRms()){
					r.setId(CodeHelper.createUUID());
					r.setAccountId(rm.getAccountId());
					mainInfoMapper.saveUsedReport(r);
				}
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
		}
}

