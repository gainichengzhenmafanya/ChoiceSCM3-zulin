package com.choice.scm.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.ItemPrgm;
import com.choice.scm.domain.Pubitem;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.CostbomMapper;

@Service

public class CostbomService {
	
	@Autowired
	private CostbomMapper costbomMapper;
	@Autowired
	private PageManager<Pubitem> pageManager;
	private final transient Log log = LogFactory.getLog(CostbomService.class);

		

	/**
	 * 查询所有菜品类别
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<Costbom> findAllPubgrp() throws CRUDException {
		try {
			return costbomMapper.findAllPubgrp();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品类别
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<Costbom> findAllPubgrp_boh() throws CRUDException {
		try {
			return costbomMapper.findAllPubgrp_boh();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public List<Costbom> findAllPubgrp_pos() throws CRUDException {
		try {
			return costbomMapper.findAllPubgrp_pos();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询菜谱方案 
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPrgm> findItemPrgm() throws CRUDException {
		try {
			return costbomMapper.findItemPrgm();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询菜谱方案 
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemPrgm> findItemPrgm_boh() throws CRUDException {
		try {
			return costbomMapper.findItemPrgm_boh();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public List<ItemPrgm> findItemPrgm_pos() throws CRUDException {
		try {
			return costbomMapper.findItemPrgm_pos();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询成本卡类型
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findMod() throws CRUDException {
		try {
			return costbomMapper.findMod();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem(Pubitem pubitem) throws CRUDException {
		try {
			
			return costbomMapper.findAllPubitem(pubitem);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem_boh(Pubitem pubitem) throws CRUDException {
		try {
			if(null != pubitem && null != pubitem.getItcode()){
				pubitem.setItcode(pubitem.getItcode().toUpperCase());
			}
			return costbomMapper.findAllPubitem_boh(pubitem);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem_pos(Pubitem pubitem) throws CRUDException {
		try {
			
			return costbomMapper.findAllPubitem_pos(pubitem);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem_pos(Pubitem pubitem, Page page) throws CRUDException {
		try {
			return pageManager.selectPage(pubitem, page, CostbomMapper.class.getName()+".findAllPubitem_pos");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem    分页
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem(Pubitem pubitem,Page page) throws CRUDException {
		try {
			return pageManager.selectPage(pubitem, page, CostbomMapper.class.getName()+".findAllPubitem");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有菜品
	 * @param Pubitem    分页
	 * @return
	 * @throws CRUDException
	 */
	public List<Pubitem> findAllPubitem_boh(Pubitem pubitem,Page page) throws CRUDException {
		try {
			return pageManager.selectPage(pubitem, page, CostbomMapper.class.getName()+".findAllPubitem_boh");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询传入     某道菜的     成本   根据item  acct
	 * @param Pubitem    
	 */
	public Pubitem findPubitemCostByItem(Pubitem pubitem) throws CRUDException {
		try {
			return costbomMapper.findPubitemCostByItem(pubitem);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的成本卡   模糊查询 根据左侧   条件
	 * @param costbom
	 * @return
	 * @throws CRUDException
	 */
	public List<Costbom> findAllCostbombomByLeftId(Costbom costbom) throws CRUDException {
		try {
			
			return costbomMapper.findAllCostbombomByLeftId(costbom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的成本卡   模糊查询 根据左侧   条件(虚拟物料)
	 * @param costbom
	 * @return
	 * @throws CRUDException
	 */
	public List<Costbom> findAllCostbombomByLeftId_x(Costbom costbom) throws CRUDException {
		try {
			
			return costbomMapper.findAllCostbombomByLeftId_x(costbom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询已做成本卡Id   list
	 * @param costbom
	 * @return
	 */
	public List<Costbom> findAllCostdtlmIdList() throws CRUDException {
		try {
			return costbomMapper.findAllCostdtlmIdList();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询已做成本卡Id   list 虚拟
	 * @param costbom
	 * @return
	 */
	public List<Costbom> findAllCostdtlmIdList_x() throws CRUDException {
		try {
			return costbomMapper.findAllCostdtlmIdList();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 增  删  改 方法
	 * @param costbom
	 * @throws CRUDException
	 */
	public int saveOrUpdateOrDelCostbom(Costbom costbom) throws CRUDException {
		try {
			if(null == costbom.getStatus() || "".equals(costbom.getStatus())){
				costbom.setStatus("N");
			}else{
				costbom.setStatus("Y");
			}
			costbomMapper.saveOrUpdateOrDelCostbom(costbom);
			return  costbom.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 增  删  改 方法   替换物料  
	 * @param costbom
	 * @throws CRUDException
	 */
	public int saveOrUpdateOrDelCostbom_n(Costbom costbom) throws CRUDException {
		try {
			costbom.setStatus("主料");
			costbomMapper.saveOrUpdateOrDelCostbom_n(costbom);
			return  costbom.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 增  删  改 方法(虚拟物料)
	 * @param costbom
	 * @throws CRUDException
	 */
	public int saveOrUpdateOrDelCostbom_x(Costbom costbom,String cntt) throws CRUDException {
		try {
			costbom.setStatus("主料");
			if("A".equals(costbom.getCurStatus())||"E".equals(costbom.getCurStatus())){
				costbom.setUnit( java.net.URLDecoder.decode(costbom.getUnit(),"UTF-8"));
				if("A".equals(costbom.getCurStatus())){
					costbomMapper.saveCostbom(costbom);
				}else if("E".equals(costbom.getCurStatus())){
					costbomMapper.updateCostbom(costbom);
				}
				String sp_code = costbom.getSp_code();//实际物料
				//String cnt4Str = costbom.getCnt4().toString();//实际物料用量
				String cnt = cntt.substring(0,cntt.lastIndexOf(","));
				sp_code = sp_code.substring(0,sp_code.lastIndexOf(","));
				String[] spcodeArr = sp_code.split(",");
				String[] cntArr = cnt.split(",");
				for(int i=0;i<spcodeArr.length;i++){
					costbom.setSp_code(spcodeArr[i]);
					costbom.setCnt(Double.parseDouble(cntArr[i]));
//					costbomMapper.saveCostdtlm(costbom);
					costbomMapper.saveOrUpdateOrDelCostbom_x(costbom);
				}
			}else if("D".equals(costbom.getCurStatus())){
				
				costbomMapper.saveOrUpdateOrDelCostbom_x(costbom);
			}
			
			//costbomMapper.saveOrUpdateOrDelCostbom_x(costbom);
			return  costbom.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询右侧下方(虚拟物料)
	 * @param map
	 * @return
	 */
	public List<Costbom> getDownCostbom(Costbom costbom){
		return costbomMapper.getDownCostbom(costbom);
	}
	/**
	 * 查询右侧下方(替代物料)
	 * @param map
	 * @return
	 */
	public List<Costbom> getDownCostbom_n(Costbom costbom){
		return costbomMapper.getDownCostbom_n(costbom);
	}
	
	/**
	 * 根据id查询 成本卡
	 * @param costbom
	 * @return
	 * @throws CRUDException
	 */
	public Costbom findCostbomByid(Costbom costbom) throws CRUDException {
		try {
			return costbomMapper.findCostbomByid(costbom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改成本卡
	 * @param costbom
	 * @throws CRUDException
	 */
	public int updateCostbom(Costbom costbom) throws CRUDException {
		try {
			return  0;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 删除成本卡
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteCostbom(List<String> listId) throws CRUDException {
		try {
		//	costbomMapper.deleteCostbom(listId);
			//删除从表
			} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 导出/gzjj
	 */
	public boolean exportExcel(OutputStream os, List<Pubitem> list) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = totalCount;
//			if(totalCount>65535){
//				sheetNum = totalCount/MAXNUM+1;
//			}
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++) {
                WritableSheet sheet = workBook.createSheet(list.get(i).getItdes(), i);
                addSheet(sheet,0,list.get(i),1);
            }
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

    /**
     * 拼接Excel
     * @param sheet
     * @param index
     * @param pubitem
     * @param unitIndex
     * @throws WriteException
     */
    private void addSheet(WritableSheet sheet,int index,Pubitem pubitem,int unitIndex) throws WriteException {
        //遍历list填充表格内容
        Costbom costbom = new Costbom();
        costbom.setItcode(pubitem.getPubitem());
        switch(unitIndex){
            case 1:
                if(pubitem.getUnit()==null){
                    return;
                }
                sheet.addCell(new Label(0, index, "菜品：" + pubitem.getItcode() + "     " + pubitem.getItdes() + "     销售单位：" + pubitem.getUnit() + "     售价：" + pubitem.getPrice()));
                costbom.setUnit_sprice(pubitem.getUnit());
                break;
            case 2:
                if(pubitem.getUnit2()==null){
                    return;
                }
                sheet.addCell(new Label(0, index, "菜品：" + pubitem.getItcode() + "     " + pubitem.getItdes() + "     销售单位：" + pubitem.getUnit2() + "     售价：" + pubitem.getPrice2()));
                costbom.setUnit_sprice(pubitem.getUnit2());
                break;
            case 3:
                if(pubitem.getUnit3()==null){
                    return;
                }
                sheet.addCell(new Label(0, index, "菜品：" + pubitem.getItcode() + "     " + pubitem.getItdes() + "     销售单位：" + pubitem.getUnit3() + "     售价：" + pubitem.getPrice3()));
                costbom.setUnit_sprice(pubitem.getUnit3());
                break;
        }
        Map<String,List<Costbom>> map=list2Map(costbomMapper.findAllCostbombomByLeftId(costbom));
        index+=1;
        sheet.addCell(new Label(0, index, "编码"));
        sheet.addCell(new Label(1, index, "名称"));
        sheet.addCell(new Label(2, index, "规格"));
        sheet.addCell(new Label(3, index, "取料率"));
        sheet.addCell(new Label(4, index, "成本单位"));
        sheet.addCell(new Label(5, index, "净用量"));
        sheet.addCell(new Label(6, index, "毛用量"));
        sheet.addCell(new Label(7, index, "标准单位"));
        sheet.addCell(new Label(8, index, "毛用量"));
        sheet.addCell(new Label(9, index, "单价"));
        sheet.addCell(new Label(10, index, "金额"));
        sheet.addCell(new Label(11, index, "参考单位"));
        sheet.addCell(new Label(12, index, "毛用量"));
        if(map!=null) {
            for(Map.Entry<String,List<Costbom>> costbomEntry:map.entrySet()) {
                List<Costbom> costbomList=costbomEntry.getValue();
                index += 1;
                sheet.addCell(new Label(0, index, "成本卡类型："+costbomEntry.getKey()+","+costbomList.get(0).getModsName()));
                for (int j = 0; j < costbomList.size(); j++) {
                    if (j == costbomList.size()) {
                        break;
                    }
                    index += 1;
                    if(costbomList.get(j).getSupply()==null){
                        costbomList.get(j).setSupply(new Supply());
                    }
                    sheet.addCell(new Label(0, index, costbomList.get(j).getSupply().getSp_code()));
                    sheet.addCell(new Label(1, index, costbomList.get(j).getSupply().getSp_name()));
                    sheet.addCell(new Label(2, index, costbomList.get(j).getSupply().getSp_desc()));
                    sheet.addCell(new Label(3, index, costbomList.get(j).getExrate()==null?"":costbomList.get(j).getExrate().toString()));
                    sheet.addCell(new Label(4, index, costbomList.get(j).getSupply().getUnit2()));
                    sheet.addCell(new Label(5, index, costbomList.get(j).getExcnt()==null?"":costbomList.get(j).getExcnt().toString()));
                    sheet.addCell(new Label(6, index, costbomList.get(j).getCnt2()==null?"":costbomList.get(j).getCnt2().toString()));
                    sheet.addCell(new Label(7, index, costbomList.get(j).getSupply().getUnit()));
                    sheet.addCell(new Label(8, index, costbomList.get(j).getCnt()==null?"":costbomList.get(j).getCnt().toString()));
                    sheet.addCell(new Label(9, index, costbomList.get(j).getSupply().getSp_price().toString()));
                    sheet.addCell(new Label(10, index, costbomList.get(j).getSupply().getAmt().toString()));
                    sheet.addCell(new Label(11, index, costbomList.get(j).getSupply().getUnit1()));
                    sheet.addCell(new Label(12, index, costbomList.get(j).getCnt1()==null?"":costbomList.get(j).getCnt1().toString()));
                }
                index+=1;
            }
            if (unitIndex + 1 < 4) {
                addSheet(sheet, index + 5, pubitem, unitIndex + 1);
            }
        }
    }

    /**
     * 根据成本卡类型分类
     * @param list
     * @return
     */
    private Map<String,List<Costbom>> list2Map(List<Costbom> list){
        if(list==null){
            return null;
        }
        Map<String,List<Costbom>> listMap=new HashMap<String, List<Costbom>>();
        for(int i=0;i<list.size();i++){
            List<Costbom> costboms=listMap.get(list.get(i).getMods());
            if(costboms==null){
                List<Costbom> costbomList=new ArrayList<Costbom>();
                costbomList.add(list.get(i));
                listMap.put(list.get(i).getMods(),costbomList);
            }else{
                costboms.add(list.get(i));
            }
        }
        return listMap;
    }
	/**
	 * 查询菜品bom已做成本卡菜品数据
	 * @return
	 */
	public List<Costbom> findCostBomList() {
		return costbomMapper.findCostBomList();
	}
	
	/**
	 * 批量删除菜品----虚拟物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbom_x(Map<String,String> map) throws CRUDException{
		try {
			List<String> mm = new ArrayList<String>();
			String[] str = map.get("ids").split(",");
			for (int i = 0; i < str.length; i++) {
				mm.add(str[i]);
			}
			Map<String,Object> m = new HashMap<String,Object>();
			m.put("ids", mm);
			m.put("acct", map.get("acct"));
			m.put("ite", map.get("item"));
			costbomMapper.deleteCostbom_x_d(m);
			costbomMapper.deleteCostbom_x(m);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量删除菜品----实际物料
	 * @param costbom
	 * @throws CRUDException
	 */
	public void deleteCostbom(Map<String,String> map) throws CRUDException{
		try {
			List<String> mm = new ArrayList<String>();
			String[] str = map.get("ids").split(",");
			for (int i = 0; i < str.length; i++) {
				mm.add(str[i]);
			}
			Map<String,Object> m = new HashMap<String,Object>();
			m.put("ids", mm);
			m.put("acct", map.get("acct"));
			m.put("ite", map.get("item"));
			m.put("mods", map.get("mods"));
			costbomMapper.deleteCostbom(m);
			costbomMapper.deleteCostbomd(m);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/*
	 * 获取所有的成本卡类型
	 */
	public List<CodeDes> selectCodedes() throws CRUDException{
		try {
			return costbomMapper.selectCodedes();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 复制单个成本卡----虚拟物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public void copyCostbom(String item,String mods,String ids,String acct) throws CRUDException{
		try {
			Costbom costbom1 = new Costbom();
			costbom1.setItem(item);
			costbom1.setMods(mods);
			List<Costbom> listCostbom = costbomMapper.selectCostbomByItem(costbom1);//获取当前选中类别下所有的bom
			String[] str = ids.split(",");
			for (int i = 0; i < str.length; i++) {
				for (int j = 0; j < listCostbom.size(); j++) {
					Costbom costbom2 = listCostbom.get(j);
					costbom2.setAcct(acct);
					List<Costbom> list = costbomMapper.selectCostbomByItem_d(costbom2);//查询bom子表
					costbom2.setMods(str[i]);
					costbomMapper.deleteCostbomByItem(costbom2);
					costbomMapper.saveCostbom(costbom2);//主表
					for (int k = 0; k < list.size(); k++) {
						Costbom costbom3 = list.get(k);
						costbom3.setAcct(acct);
						costbom3.setCurStatus("A");
						costbom3.setMods(str[i]);
						costbomMapper.deleteCostbomByItem_d(costbom3);
						costbomMapper.saveOrUpdateOrDelCostbom_x(costbom3);//子表
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 类别复制成本卡----虚拟物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public void copyCostbomByTyp(String mods,String ids,String acct) throws CRUDException{
		try {
			Costbom costbom1 = new Costbom();
			costbom1.setMods(mods);
			List<Costbom> listCostbom = costbomMapper.selectCostbomByMods(costbom1);//获取当前选中类别下所有的bom
			String[] str = ids.split(",");
			for (int i = 0; i < str.length; i++) {
				for (int j = 0; j < listCostbom.size(); j++) {
					Costbom costbom2 = listCostbom.get(j);
					costbom2.setAcct(acct);
					List<Costbom> list = costbomMapper.selectCostbomByItem_d(costbom2);//查询bom子表
					costbom2.setMods(str[i]);
					costbomMapper.deleteCostbomByItem(costbom2);
					costbomMapper.saveCostbom(costbom2);//主表
					for (int k = 0; k < list.size(); k++) {
						Costbom costbom3 = list.get(k);
						costbom3.setAcct(acct);
						costbom3.setCurStatus("A");
						costbom3.setMods(str[i]);
						costbomMapper.deleteCostbomByItem_d(costbom3);
						costbomMapper.saveOrUpdateOrDelCostbom_x(costbom3);//子表
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 复制单个成本卡----实际物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public void copyCostBom(String item,String mods,String ids,String acct) throws CRUDException{
		try {
			Costbom costbom1 = new Costbom();
			costbom1.setItem(item);
			costbom1.setMod(mods);
			List<Costbom> listCostbom = costbomMapper.selectCostbomByItemId(costbom1);//获取当前选中类别下所有的bom
			String[] str = ids.split(",");
			for (int j = 0; j < listCostbom.size(); j++) {
				Costbom costbom2 = listCostbom.get(j);
				costbom2.setAcct(acct);
				List<Costbom> cb = costbomMapper.selectCostbomByItemId_d(costbom2);//查询bom子表
				for (int i = 0; i < str.length; i++) {
					costbom2.setMod(str[i]);
					costbomMapper.deleteCostbomByItemId(costbom2);
					costbomMapper.saveCostdtlm(costbom2);//主表
					//多单位用循环 css
					for (int k=0; k<cb.size(); k++) {
						cb.get(k).setAcct(acct);
						cb.get(k).setCurStatus("E");
						cb.get(k).setMod(str[i]);
						costbomMapper.deleteCostbomByItemId_d(cb.get(k));
						costbomMapper.saveCostbombomByItem(cb.get(k));//子表
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 类别复制成本卡----实际物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public void copyCostBomByTyp(String mods,String ids,String acct) throws CRUDException{
		try {
			String[] str = ids.split(",");
			
			//add by jinshuai at 20160406
			//先删除目标成本卡类型的全部成本卡
			Costbom dc;
			for (int i = 0; i < str.length; i++) {
				String tmod = str[i];
				if(tmod!=null&&tmod.equals(mods)){
					//不能复制到相同的类型下面
					continue;
				}
				dc = new Costbom();
				dc.setMod(tmod);
				//删主表
				costbomMapper.deleteCostbomByItemId(dc);
				//删子表
				costbomMapper.deleteCostbomByItemId_d(dc);
				
				dc.setIds(mods);
				costbomMapper.copyCostdtlm(dc);
				costbomMapper.copyCostdtl(dc);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新物资编码的单位
	 * @return
	 */
	public String updateUnit(String acct) throws CRUDException {
		try {
			Costbom costbom =new Costbom();
			costbom.setAcct(acct);
            costbomMapper.updateUnit(costbom);
            if(costbom.getPr()==1){
            	return "1";
            }else{
            	return "-1";
            }
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据菜品编码获取bom信息
	 */
	public List<Map<String, Object>> getCostDtlByVcode(String mod, String iscost, String vcode, String unit) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mod", mod);
		map.put("iscost", iscost);
		map.put("vcode", vcode);
		map.put("unit", unit);
		
		return costbomMapper.getCostDtlForChkPos(map);
	}
}
