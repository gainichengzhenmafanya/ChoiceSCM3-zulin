package com.choice.scm.service;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkstodMapper;
import com.choice.scm.persistence.ChkstomMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SppriceMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service

public class ChkstomService {

	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private ChkstomMapper chkstomMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private SppriceMapper sppriceMapper;
	@Autowired
	private PageManager<Chkstom> pageManager;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;

	private final transient Log log = LogFactory.getLog(ChkstomService.class);

	/**
	 * 查看上传
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findUpload(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findUpload(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**查看未上传
	 * 
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findNoUpload(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findNoUpload(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**查看未上传数目
	 * 
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public int findNoUploadCount(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findNoUploadCount(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询单据审核状态
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findCheckSta(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 *  该分店今天     已审核多少条， 未审核多少条
	 * @throws CRUDException
	 */
	public Chkstom findCountByFirmId(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.findCountByFirmId(chkstom);

		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加物资前验证
	 */
	public String checkSaveNewChk(Chkstom chkstom) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		boolean flag=true;
		try {
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					//查询报货单据里的物资是否设置了默认供应商
					Spprice spprice=new Spprice();//报价里的默认供应商
					spprice.setSp_code(chkstod.getSupply().getSp_code());
					spprice.setAcct(chkstom.getAcct());
					spprice.setArea(chkstom.getFirm());
					spprice.setBdat(chkstom.getMaded());
					spprice.setEdat(chkstom.getMaded());
					spprice.setSta("已审核");
					Supply supply=new Supply();//物资表里的默认供应商
					supply.setAcct(chkstom.getAcct());
					supply.setSp_code(chkstod.getSupply().getSp_code());
					List<Spprice> list1=sppriceMapper.findDeliverBySpprice(spprice);
					Supply supply1=supplyMapper.findDeliverByCode(supply);
					if(list1.size()==0 && (null==supply1 || "".equals(supply1))){//如果物资没有设置供应商，返回
						result.put("sp_name", chkstod.getSupply().getSp_name());
						result.put("pr", "1");
						flag=false;
					}
				}
				if(flag==true){
					result.put("pr", "2");
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检查是否单据内物资有已经采购确认或者采购审核的    
	 * 2014.10.20  wjf修改
	 */
	public int checkYnUnChk(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomMapper.checkYnUnChk(chkstom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加物资前验证
	 */
	public double checkSppriceSale(String firmid) throws CRUDException
	{
	 
		try {
			return chkstomMapper.checkSppriceSale(firmid);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加或修改报货单
	 * @param chkstom
	 * @param sta
	 * @return 1成功 其它错误信息
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Chkstom chkstom, String sta) throws CRUDException{
		String result="";
		try {
//			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");  
//			Properties properties = new Properties();
//			properties.load(inputStream);
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			//判断是否有主直拨库，或者是否是多个主直拨库
			String ckcode = "";
			try{
				ckcode = chkstomMapper.findMainPositnByFirm(chkstom.getFirm());
			}catch(Exception e){
				//异常：多个主直拨库
				if(e.getMessage().indexOf("TooManyResultsException")!=-1){
					throw new Exception("异常：配送片区设置了多个主直拨库！！！");
				}
			}
			//异常：没有主直拨库
			if(ckcode==null){
				throw new Exception("异常：配送片区未设置主直拨库！！！");
			}
			//继续往下走
			positn.setCode(ckcode);
			chkstom.setPositn(positn);
//			if(chkstom.getChkstodList() != null){
//				for(Chkstod chkstod : chkstom.getChkstodList()){
//					double amount=chkstod.getAmount();
//					double price=chkstod.getPrice();
//					totalAmt+=amount*price;
//				}
//			}
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			//保存主单
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				//添加报货单日志
				log.warn("添加报货单(主单)：\n" + chkstom);
				chkstomMapper.saveNewChkstom(chkstom);
				result=chkstom.getPr().toString();
				//异常：添加报货单(主单)异常
				if("-1".equals(result)){
					throw new Exception("异常：添加报货单(主单)异常！！！");
				}
			}else{
				HashMap<Object,Object>  map = new HashMap<Object,Object> ();
				Chkstom chkstomN = new Chkstom();
				chkstomN.setChkstoNo(chkstom.getChkstoNo());
				map.put("chkstom", chkstomN);
				chkstom.setVouno(chkstomMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改报货单(主单)：\n" + chkstom);
				chkstomMapper.updateChkstom(chkstom);
				result=chkstom.getPr().toString();
				//异常：修改报货单(主单)异常
				if("-1".equals(result)){
					throw new Exception("异常：修改报货单(主单)异常！！！");
				}
			}
			//保存详单
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					chkstodMapper.saveNewChkstod_new(chkstod);
					if(chkstod.getPr()==-2){
						//异常：保存报货单(详单)异常，物资配送片区默认仓位或物资默认仓位或配送片区主直拨库未设置供应商！！！
						throw new Exception("异常：保存报货单(详单)异常，物资："+chkstod.getSupply().getSp_code()+"，"+chkstod.getSupply().getSp_name()+"默认仓位未设置对应供应商！！！");
					}
				}
			}
			//更新主表的totalamt
			chkstomMapper.updateChkstomTotalamt(chkstom);
			/***这里需要判断一下 ，如果是档口报货来的   需要重新更新档口报货信息wjf***/
			//1.判断是否档口报货来的
			Integer id = chkstom.getChkstoNo();
			List<Chkstom> list = chkstomMapper.findChkstonoDept(id);
			if(list.size() > 0){
				//2.是档口报货来的，得到对应的档口报货单号
				StringBuffer idList = new StringBuffer();
				for(Chkstom c : list){
					idList.append(c.getChkstoNo()+",");
				}
				idList.append("0");
				Chkstod chkstod = new Chkstod();
				chkstod.setChkstoNo(chkstom.getChkstoNo());
				chkstod.setChkstoNos(idList.toString());
				chkstod.setBak2(chkstom.getChkstoNo());
				
				chkstomMapper.updateChkstomDept(chkstod);
			}
			
			return "1";
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e.getMessage());
		}
	}
	
	/**
	 * 添加或修改报货单(来自档口报货)
	 * @param chkstom
	 * @throws CRUDException
	 */
	public String saveOrUpdateChkDept(Chkstom chkstom, String idList) throws CRUDException
	{
		String result="1";
		try {
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					double amount=chkstod.getAmount();
					double price=chkstod.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			//添加报货单日志
			log.warn("添加报货单(主单)：\n" + chkstom);
			chkstomMapper.saveNewChkstom(chkstom);
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					chkstodMapper.saveNewChkstod_new(chkstod);
					if(chkstod.getPr()>=1){
						result="1";
					}
				}
			}
			Chkstod chkstod = new Chkstod();
			chkstod.setChkstoNo(chkstom.getChkstoNo());
			chkstod.setChkstoNos(idList);
			chkstod.setBak2(chkstom.getChkstoNo());
			chkstomMapper.updateChkstomDept(chkstod);
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 添加或修改报货单
	 * @param chkstom
	 * @throws CRUDException
	 */
	public String saveOrUpdateChkMis(Chkstom chkstom, String sta) throws CRUDException
	{
		String result="1";
		try {
//			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");  
//			Properties properties = new Properties();
//			properties.load(inputStream);
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					double amount=chkstod.getAmount();
					double price=chkstod.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				//添加报货单日志
				log.warn("添加报货单(主单)：\n" + chkstom);
				chkstomMapper.saveNewChkstom(chkstom);//不要走老存储过程  wjf
			}else{
				HashMap<Object,Object>  map = new HashMap<Object,Object> ();
				Chkstom chkstomN = new Chkstom();
				chkstomN.setChkstoNo(chkstom.getChkstoNo());
				map.put("chkstom", chkstomN);
				chkstom.setVouno(chkstomMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改报货单(详单)：\n" + chkstom);
				chkstomMapper.updateChkstom(chkstom);
			}
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					chkstodMapper.saveNewChkstod(chkstod);
					if(chkstod.getPr()>=1){
						result="1";
					}
				}
			}
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstomMap, page, ChkstomMapper.class.getName()+".findByKey");			
			//return chkstomMapper.findByKey(chkstomMap);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询当前最大单号
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxChkstono() throws CRUDException
	{
		try {
			return chkstomMapper.getMaxChkstono();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除
	 */
	public String deleteChkstom(Chkstom chkstom,String chkstoNoIds) throws CRUDException
	{
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//删除报货单日志
			log.warn("删除报货单:\n" + chkstom + "\n" + chkstoNoIds);
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c=new Chkstom();
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				chkstomMapper.deleteChk(c);
				chkstomMapper.deleteChkM_c(c);
				chkstomMapper.deleteChkD_c(c);
				result.put("pr", c.getPr().toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 审核
	 */
	public String checkChk(Chkstom chkstom, String chkstoNoIds) throws CRUDException
	{
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		int chkstono = 0;
		try {
			//审核报货单日志
			log.warn("审核报货单:\n" + chkstom + "\n" + chkstoNoIds);
			if(null!=chkstoNoIds && !"".equals(chkstoNoIds)){
				ids=Arrays.asList(chkstoNoIds.split(","));
			}else{
				ids=Arrays.asList(chkstom.getChkstoNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Chkstom c = new Chkstom();
				chkstono = Integer.parseInt(ids.get(i));
				c.setChkstoNo(Integer.parseInt(ids.get(i)));
				c.setChecby(chkstom.getChecby());
				chkstomMapper.checkChkstom(c);//更新一级审核2015.5.6wjf
				chkstomMapper.checkChk_new(c);
				result.put("pr", c.getPr().toString());	
				if(!"1".equals(c.getPr().toString())){//审核成功
					JSONObject rs = JSONObject.fromObject(result);
					return rs.toString();
				}
				result.put("checby", c.getChecby());
				//更新主表的totalamt
				chkstomMapper.updateChkstomTotalamt(c);
			}
			JSONObject rs = JSONObject.fromObject(result);
			chkstom.setChkstoNo(chkstono);
			chkstomMapper.updatePositn(chkstom);//更改仓做引用状态
			return rs.toString();
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 反审核
	 */
	public String uncheckChk(Chkstom chkstom, String chkstoNoIds) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		try {
			chkstomMapper.updateChkstomByUnchk(chkstom);//反审核报货单主表 
			chkstomMapper.updateChkstodByUnchk(chkstom);//反审核报货单从表
			result.put("status", "1");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 调用报货单拆单存储过程
	 * @param chkstom
	 */
	public void saveNewChkstom_c(Chkstom chkstom) throws CRUDException{
		try {
			chkstomMapper.saveNewChkstom_c(chkstom);
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 调用扣款 存储过程
	 * @param chkstom
	 */
	public void JOININGCHKSTOM(Chkstom chkstom) throws CRUDException{
		try {
			chkstomMapper.JOININGCHKSTOM(chkstom);
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		PublicExportExcel.downloadTemplate(response, request, "报货单.xls"); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId) throws CRUDException {
		boolean checkResult = ReadChkstomExcel.check(path, supplyMapper,positnMapper);
//		Map<String, String> map = new HashMap<String, String>();
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkstom chkstom1 = ReadChkstomExcel.chkstom;
			chkstom1.setAcct(accountId);
			List<Chkstod> chkstodList = ReadChkstomExcel.chkstodList;
			chkstom1.setChkstodList(chkstodList);
			return chkstom1;
		} else {
//			map = ReadChkstomExcel.map;
			errorList = ReadChkstomExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
	
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	
	/***
	 * 安全库存报货查询
	 * @param chkstom
	 * @return
	 */
	public List<Chkstod> findSafeChkstodList(Chkstom chkstom) {
		//当期会计年
		String yearr = mainInfoMapper.findYearrList().get(0);
		chkstom.setYearr(yearr);
		Positn p = new Positn();
		p.setCode(chkstom.getFirm());
		chkstom.setBak1(positnMapper.findPositnByCode(p).getPcode());//借用一下 用来存仓位类型
		return chkstomMapper.findSafeChkstodList(chkstom);
	}

	/***
	 * 分店报货单状态查询
	 * @param chkstom
	 * @return
	 */
	public List<Map<String,Object>> listChkstomState(Chkstom chkstom) {
		return chkstomMapper.listChkstomState(chkstom);
	}
}
