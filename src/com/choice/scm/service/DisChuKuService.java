package com.choice.scm.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Dis;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.DisChuKuMapper;
@Service
public class DisChuKuService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private DisChuKuMapper disChuKuMapper;
	@Autowired
	private PageManager<Dis> pageManager;
	private final transient Log log = LogFactory.getLog(DisChuKuService.class);
	
	/**
	 * 验收出库
	 */
	public List<Dis> findAllDis(Dis dis,String ind1,Page page) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if("inout".equals(inout)){dis.setChkin("Y");}
			if("out".equals(inout)){dis.setChkin("N");}
//			dis.setChkin("Y");
			dis.setChkout("N");
//			dis.setChk1("Y");
//			dis.setSta("Y");
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null == page)//如果page为null  则查询所有 2014.10.24 wjf
				list = disChuKuMapper.findAllDis(dis);
			else
				list=pageManager.selectPage(dis, page, DisChuKuMapper.class.getName()+".findAllDis"); 			
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨/报货验收，打印用，不带分页
	 */
	public List<Dis> findAllDisForReport(Dis dis,String ind1) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if("inout".equals(inout)){dis.setChkin("Y");}
			if("out".equals(inout)){dis.setChkin("N");}
//			dis.setChkin("Y");
			dis.setChkout("N");
//			dis.setChk1("Y");
//			dis.setSta("Y");
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			list=disChuKuMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量修改       验收入库     验收出库    采购确认  采购审核   等成功后的标志字段
	 */
	public void updateByIds(Dis dis,String accountName) throws CRUDException {
		try {
			if(null!=dis.getId() && !"".equals(dis.getId())){
				dis.setId(CodeHelper.replaceCode(dis.getId()));
			}
			if(null!=dis.getChk1() && !"".equals(dis.getChk1())){
				dis.setChk1emp(accountName);//采购确认人
				dis.setChk1tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));//采购确认时间
			}
			if(null!=dis.getSta() && !"".equals(dis.getSta())){
				dis.setChk2emp(accountName);//采购审核人
				dis.setChk2tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));//采购审核时间
			}
			disChuKuMapper.updateByIds(dis);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新发货数量
	 * 返回结果0成功
	 * 1失败
	 * @param idAndNum
	 * @return
	 */
	public String updateChkoutNum(List<Dis> list){
		String result = "0";
		if(list==null){
			return result;
		}
		try{
			for(int i=0,len=list.size();i<len;i++){
				Dis dis = list.get(i);
				disChuKuMapper.updateChkoutNum(dis);
			}
		}catch(Exception e){
			result = "1";
		}
		return result;
	}
}
