package com.choice.scm.service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.Account;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.AcctLineFirm;
import com.choice.scm.domain.LinesFirm;
import com.choice.scm.persistence.LinesFirmMapper;

@Service
public class LinesFirmService {

	@Autowired
	private LinesFirmMapper linesFirmMapper;
	@Autowired
	private AccountMapper accountMapper;
	
	/**
	 * 查询配送线路 list
	 */
	public List<LinesFirm> findLinesFirm(LinesFirm linesFirm) throws CRUDException{
		try{
 
			return linesFirmMapper.findLinesFirm(linesFirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 根据配送ID查询该配送线下的所有分店
	 */
	public List<AcctLineFirm> findFirmByLinesFirmId(AcctLineFirm acctlinefirm) throws CRUDException{
		try{
			return linesFirmMapper.findFirmByLinesFirmId(acctlinefirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 根据分店id 查询对应路线
	 */
//	public AcctLineFirm findFirmByLinesFirmIdOne(String	firmCode,String typ) throws CRUDException{
//		try{
//			if(typ.equals("accountId")){
//				Account account=accountMapper.findAccountById(firmCode);
//				if(account.getPositn()!=null){
//					return linesFirmMapper.findFirmByLinesFirmIdOne(account.getPositn().getCode()).get(0);
//				}
//			}
//			return linesFirmMapper.findFirmByLinesFirmIdOne(firmCode).get(0);
//		}catch(Exception e){
//			e.printStackTrace();
//			throw new CRUDException(e);
//		}
//		
//	}
	public AcctLineFirm findFirmByLinesFirmIdOne(String firmCode, String typ)throws CRUDException
		   {
			List<AcctLineFirm> acctList=null;
		    try
		    {
		    	if (typ.equals("accountId")) {
		    		Account account = this.accountMapper.findAccountById(firmCode);
		    		if (account.getPositn() != null) {
		    			acctList=this.linesFirmMapper.findFirmByLinesFirmIdOne(account.getPositn().getCode());
		    		}
		    	}
				acctList=this.linesFirmMapper.findFirmByLinesFirmIdOne(firmCode);
				if(acctList==null||acctList.size()<=0){
					return null;
				}
				return acctList.get(0);
		   } catch (Exception e) {
		       e.printStackTrace();
	      throw new CRUDException(e);
	   }
		  }

	/**
	 * 根据acct与code查询配送线路
	 */
	public List<LinesFirm> findLineFirmById(LinesFirm linesFirm) throws CRUDException{
		try{
			return linesFirmMapper.findLinesFirm(linesFirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 添加配送路线
	 */
	public void saveLiensFirm(HttpSession session,LinesFirm linesFirm) throws CRUDException{
		try{
			linesFirm.setAcct(session.getAttribute("ChoiceAcct").toString());
			linesFirmMapper.saveLiensFirm(linesFirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 刪除 左边 配送路线
	 */
	public void deleteLiensFirm(LinesFirm linesFirm) throws CRUDException{
		try{
			
			linesFirmMapper.deleteAllAcctLiensFirm(linesFirm);
			linesFirmMapper.deleteLiensFirm(linesFirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改 左边 配送路线
	 */
	public void updateLineFirm(HttpSession session,LinesFirm linesFirm) throws CRUDException{
		try{
			linesFirm.setAcct(session.getAttribute("ChoiceAcct").toString());
			linesFirmMapper.updateLineFirm(linesFirm);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除分店 右边
	 */
	public void deleteAcctLinesFirm(AcctLineFirm acctlinefirm) throws CRUDException{
		try{
			List<String> idList = Arrays.asList(acctlinefirm.getFirm().getCode().split(","));
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", idList);
			params.put("acctlinefirm", acctlinefirm);
			linesFirmMapper.deleteAcctLiensFirm(params);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 添加分店 右边
	 */
	public String saveAcctLineFirm(AcctLineFirm acctlinefirm) throws CRUDException{
		try{
			List<String> idList = Arrays.asList(acctlinefirm.getFirm().getCode().split(","));
			String msg = "";
			for (int i = 0; i < idList.size(); i++) {
				acctlinefirm.getFirm().setCode(CodeHelper.replaceCode(idList.get(i)));
				//判断该分店是否有配送线路
				//分店编码有字母的问题
				List<AcctLineFirm> acctLineFirms = linesFirmMapper.findFirmByLinesFirmId(acctlinefirm);
				if(acctLineFirms!=null && acctLineFirms.size()>0){
					msg +="编码为：【"+idList.get(i)+"】的配送路线信息已经存在！\n";
				}else{
					acctlinefirm.getFirm().setCode(idList.get(i));
					linesFirmMapper.saveAcctLineFirm(acctlinefirm);
				}
			}
//			return "ok";
			return msg;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改到店时间
	 */
	public String updateTimto(LinesFirm linesFirm) throws CRUDException{
		try{
			for (AcctLineFirm acctlinefirm : linesFirm.getAcctLineFirmList()) {
				linesFirmMapper.updateTimto(acctlinefirm);
			}
			return "success";
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

}