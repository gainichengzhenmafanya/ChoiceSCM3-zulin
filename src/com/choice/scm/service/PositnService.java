package com.choice.scm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.domain.system.Role;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountRoleMapper;
import com.choice.framework.persistence.system.RoleMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ScmStringConstant;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnFirmScm;
import com.choice.scm.domain.PositnRole;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.PositnRoleMapper;

@Service
public class PositnService {

	private static Logger log = Logger.getLogger(PositnService.class);
	
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Positn> pageManager;
	@Autowired
	private PositnRoleMapper positnRoleMapper;
	@Autowired
	private RoleMapper roleMapper ;
	@Autowired
	private AccountRoleMapper accountRoleMapper ;
	/**
	 * 模糊查询分店和仓位信息
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositn(Positn positn ,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".findPositn");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询仓位
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositn(String searchInfo ,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			return pageManager.selectPage(map, page, PositnMapper.class.getName()+".findPositnBy");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询仓位----用于分店物资属性，过滤掉主直拨库和基地仓库
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositnBySupply(String searchInfo ,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			return pageManager.selectPage(map, page, PositnMapper.class.getName()+".findPositnBySupply");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询仓位----用于分店物资属性，过滤掉主直拨库和基地仓库  只查询分店，不分页  wjf
	 * @param positn 
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositnBySupply1(Positn positn) throws CRUDException{
		try{
			return positnMapper.findPositnBySupply1(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询仓位====用于分店供应商
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositnDel(String searchInfo ,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			return pageManager.selectPage(map, page, PositnMapper.class.getName()+".findPositnByDel");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	public List<Positn> findPositnDelNoPage(String searchInfo) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
//			return pageManager.selectPage(map, page, PositnMapper.class.getName()+".findPositnByDel");
			return positnMapper.findPositnByDel(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 模糊查询仓位
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositn(String searchInfo ,Page page, String cwqx, String accountId) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			map.put("cwqx", cwqx);
			map.put("accountId", accountId);
			return pageManager.selectPage(map, page, PositnMapper.class.getName()+".findPositnBy");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询仓位
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findPositn(String cwqx, String accountId) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("cwqx", cwqx);
			map.put("accountId", accountId);
			return positnMapper.findPositnBy(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 添加仓位类型查询条件模糊查询仓位
	 * @param cwqx
	 * @param accountId
	 * @param type 类型 
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositn(String cwqx, String accountId,String type) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("cwqx", cwqx);
			map.put("accountId", accountId);
			map.put("type", type);
			return positnMapper.findPositnBy(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的分店和仓位信息
	 */
	public List<Positn> findAllPositn(Positn positn) throws Exception{
		try{
			return positnMapper.findPositn(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 快速报价模块 分店仓位查询 wangjie 2014年12月3日 09:16:02
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	public List<Positn> findPositnForQuickSpprice(Positn positn,Page page) throws Exception{
		try{
//			return positnMapper.findPositnForQuickSpprice(positn);
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".findPositnForQuickSpprice");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的加工间
	 */
	public List<Positn> findPositnex(Positn positn) throws Exception{
		try{
			return positnMapper.findPositnex(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的分店和仓位信息----用于配送线路门店选择
	 */
	public List<Positn> searchAllPositnPs(Positn positn) throws Exception{
		try{
			return positnMapper.searchAllPositnPs(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的分店和仓位信息---- 用于供应商对应仓位编码，不可重复
	 */
	public List<Positn> findAllPositns(Positn positn) throws Exception{
		try{
			return positnMapper.findPositns(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的分店和仓位信息不分页
	 */
	public List<Positn> findAllPositn() throws Exception{
		try{
			return positnMapper.findAllPositn();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的分店和仓位的类型
	 */
	public List<Positn> findAllPositnTyp() throws Exception{
		try{
			return positnMapper.findAllPositnTyp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询所有的分店和仓位信息，带分页
	 */
	public List<Positn> findAllPositnByPage(Positn positn,Page page) throws Exception{
		try{
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".findPositn");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 根据acct和code查询分店和仓位信息
	 * @param Positn
	 */
	public Positn findPositnByCode(Positn positn) throws CRUDException{
		try{
			return positnMapper.findPositnByCode(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新分店和仓位信息====用于更新到物资表中的分店仓位
	 * @param positn
	 * @throws CRUDException 
	 */
	public void updateSupply(Positn positn) throws CRUDException{
		try{
			positnMapper.updateSupply(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新分店和仓位信息
	 * @param positn
	 * @throws CRUDException 
	 */
	public void updatePositn(Positn positn) throws CRUDException{
		try{
			positnMapper.updatePositn(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除分店和仓位信息
	 * @param listPositn deleteByIds
	 * @throws CRUDException
	 */
	public void deleteByIds(String code) throws CRUDException{
		try{
			String codes = CodeHelper.replaceCode(code);
			positnMapper.deleteByIds(codes);
			//删除对应关系
			Positn p = new Positn();
			p.setCode(codes);
			positnMapper.deletePositnFirms(p);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除分店和仓位信息
	 * @param listPositn 
	 * @throws CRUDException
	 */
	public void deletePositn(Positn positn) throws CRUDException{
		try{
			if(positn!=null && positn.getCode()!=""){
				positn.setCode(CodeHelper.replaceCode(positn.getCode()));
			}
			positnMapper.deletePositn(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 新建分店和仓位信息
	 * @param positn
	 * @throws CRUDException
	 */
	public void savePositn(Positn positn) throws CRUDException{
		try{
			if(null == positn.getQc()){
				positn.setQc("N");
			}
			if(null == positn.getInout2()){
				positn.setInout2("入库");
			}
			if(null == positn.getPd()){
				positn.setPd("N");
			}
			if(null == positn.getLocked()){
				positn.setLocked("N");
			}
			if(null == positn.getAcct())
				positn.setAcct(ScmStringConstant.ACCT);
			
			//add wangjie 
			if(!positn.getTyp().equals("1207")){
				positn.setDeptattr("");
			}else{
				positn.setDeptattr(positn.getDeptattr().split(",")[1]);
			}
			
			String accountId = positn.getAccountId();
			positn.setAccountId(null);
			positnMapper.savePositn(positn);
			//添加操作人员对该仓位的权限
			PositnRole positnRole=new PositnRole(CodeHelper.createUUID(),accountId,positn.getCode());
			positnRoleMapper.saveRolePositn(positnRole);
			//添加系统管理员角色账号对该仓位权限
			//添加系统管理员角色账号对改物资的权限
			Role role = new Role();
			role.setName("系统管理员");
			role.setDeleteFlag("T");
			List<Role> listRole = roleMapper.findAllRole(role);
			if(listRole!=null && listRole.size()>0){//存在该角色
				String roleId ="";
				for(Role ro : listRole){
					if("系统管理员".equals(ro.getName())){
						roleId = ro.getId();
						break;
					}
				}
				if(roleId!=null && !"".equals(roleId)){
					//查询该角色下账号信息
					AccountRole accountRole = new AccountRole();
					accountRole.setRoleId(roleId);
					List<AccountRole> listAccountRole = accountRoleMapper.findAccountRole(accountRole);
					if(listAccountRole!=null && listAccountRole.size()>0){
						PositnRole admin=new PositnRole(null,null,positn.getCode());
						for(AccountRole aRole:listAccountRole){
							admin.setRoleId(aRole.getAccountId());
							admin.setId(CodeHelper.createUUID());
							positnRoleMapper.saveRolePositn(admin);
						}
					}
				}
			} 
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有出库仓位(主直拨库和基地仓库)
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnOut() throws CRUDException{
		try{
			Positn positn = new Positn();
			positn.setTyp("1201");
			List<Positn> result = positnMapper.findPositn(positn);
			positn.setTyp("1202");
			if(!result.addAll(positnMapper.findPositn(positn)))throw new Exception("findPositnIn failed!!!");
			return result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有出库仓位(主直拨库和基地仓库，加工间)
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnOut1() throws CRUDException{
		try{
			Positn positn = new Positn();
			positn.setTyp("1201");
			List<Positn> result = positnMapper.findPositn(positn);
			positn.setTyp("1202");
			List<Positn> list1 = positnMapper.findPositn(positn);
			if(list1!=null&&list1.size()>0){
				result.addAll(list1);
			}
			positn.setTyp("1204");
			list1 = positnMapper.findPositn(positn);
			if(list1!=null&&list1.size()>0){
				result.addAll(list1);
			}		
			return result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有分店
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnOut2() throws CRUDException{
		try{
			Positn positn = new Positn();
			positn.setTyp("1203");
			List<Positn> result = positnMapper.findPositn(positn);
			return result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有申领仓位
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnIn() throws CRUDException{
		try{
			Positn positn = new Positn();
			positn.setTyp("1201");
			List<Positn> result = positnMapper.findPositn(positn);
			positn.setTyp("1202");
			result.addAll(positnMapper.findPositn(positn));
			positn.setTyp("1204");
			result.addAll(positnMapper.findPositn(positn));
			positn.setTyp("1203");
			result.addAll(positnMapper.findPositn(positn));
			positn.setTyp("1207");
			result.addAll(positnMapper.findPositn(positn));
			positn.setTyp("1206");
			result.addAll(positnMapper.findPositn(positn));
			return result;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * ajax查询分店前N条符合条件的记录
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnN(Positn positn) throws CRUDException {
		try{
			return positnMapper.findPositnN(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找货架
	 * @return
	 * @author ZGL
	 */
	public List<Map<String,String>> findPositn1() {
		return positnMapper.findPositn1();
	}
	/**
	 * 查询仓位（出库单/入库单填制）
	 * @return
	 */
	public List<Positn>findPositnCangwei(){
		return positnMapper.findPositnCangwei();
		
	}
	/**
	 * 查询使用仓位（出库单/入库单填制）
	 * @return
	 */
	public List<Positn>findPositnUse(){
		return positnMapper.findPositnUse();
		
	}
	/**
	 * 
	* @Title: findCodedesType 
	* @Description: TODO(查询仓库分类) 
	* @Author：LI Shuai
	* @date：2014-1-22下午3:35:08
	* @return  List<CodeDes>
	* @throws
	 */
	public List<CodeDes>findCodedesType(Positn positn){
		
		return positnMapper.findCodedesType(positn);
		
	}
	
	/**
	 * 
	* @Title: findCodedesType 
	* @Description: TODO(查询仓库分类) ====用于预估系统模块
	* @Author：LI Shuai
	* @date：2014-1-22下午3:35:08
	* @return  List<CodeDes>
	* @throws
	 */
	public List<CodeDes>findCodedesTypeFirm(Positn positn){
		
		return positnMapper.findCodedesTypeFirm(positn);
		
	}
	
	/**
	 * 根据acct、code（多个）、类型 查询仓位信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<Positn> findPositnByTypCode(Map<String,Object> map){
		return positnMapper.findPositnByTypCode(map);
	}

	/**
	 * 根据区域编码查询分店仓位
	 * @param positn
	 * @return
	 */
	public List<Positn> findPositnByArea(Positn positn) {
		return positnMapper.findPositnByArea(positn);
	}
	/**
	 * 查询分店
	 */
	public List<Positn> findPositnMonit() {
		return positnMapper.findPositnMonit();
	}
	
	/**
	 * 根据门店编码获取所有部门
	 * @param positn 
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<Positn> findDeptByPositn(Positn positn) throws CRUDException{
		try{
			return positnMapper.findDeptByPositn(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 添加仓位类型查询条件模糊查询仓位 只为出库单使用仓位用  wjf
	 * @param cwqx
	 * @param accountId
	 * @param type 类型 
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findChkoutPositnIn(String cwqx, String accountId) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("cwqx", cwqx);
			map.put("accountId", accountId);
			return positnMapper.findChkoutPositnIn(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店仓位  根据分店编码或者分店名称 查询所有分店 wangjie 2014年12月24日 14:46:44
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> selectFirmByCodeOrDes(Positn positn) throws CRUDException{
		try{
			return positnMapper.selectFirmByCodeOrDes(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有基地仓库和主直拨库   新增物资编码时 
	 * @param positn
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnForAddSupply(Positn positn) throws CRUDException{
		try{
			return positnMapper.findPositnForAddSupply(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/** 
	 * 严禁修改     严禁修改     严禁修改     严禁修改      严禁修改   严禁修改   严禁修改
	 * 查询仓位信息公共 。。。。。。。。。。。。。。。。。。。。。。。。。2015.1.8后 所有方法都可以走 2015.1.8  xlh    严禁修改
	 *  控制typ值就行     比如   查询 基地  仓位和加工间     typ= “typ in （基地仓位，加工间）”
	 */
	public List<Positn> findPositnSuper(Positn positn ,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".findPositnSuper");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/** 
	 * 严禁修改     严禁修改     严禁修改     严禁修改      严禁修改   严禁修改   严禁修改
	 * 查询仓位信息公共 。。。。。。。。。。。。。。。。。。。。。。。。。2015.1.8后 所有方法都可以走 2015.1.8  xlh    严禁修改
	 *  控制typ值就行     比如   查询 基地  仓位和加工间     typ= “typ in （基地仓位，加工间）”  不分页
	 */
	public List<Positn> findPositnSuperNOPage(Positn positn) throws CRUDException{
		try{
			return   positnMapper.findPositnSuper(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取boh门店部门列表
	 * @return
	 * @throws Exception
	 */
	public List<Positn> getBOHDeptList(Page page,Positn positn) throws Exception{
		try{
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".getBOHDeptList");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 获取scm档口和boh部门关系，返回单条
	 * @return
	 * @throws Exception
	 */
	public PositnFirmScm FindPositnFirmScmBy(PositnFirmScm positnFirmScm) throws Exception{
		try{
			return positnMapper.FindPositnFirmScmBy(positnFirmScm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 处理添加绑定
	 * @param positn
	 * @throws Exception
	 */
	public void doAddConnectDept(Positn positn,Page page) throws Exception{
		String[] firm = positn.getCode().split(",");//获取选择的档口
		
		try{
			for(String firmcode:firm){
				//先删除绑定
				Positn positn2 = new Positn();
				positn2.setCode(firmcode);
				positn2.setFirm(positn.getFirm());
				positn2.setPcode(positn.getPcode());
				positn2.setAcct(positn.getAcct());
//				List<Positn> list = pageManager.selectPage(positn2, page, PositnMapper.class.getName()+".getBOHDeptList");
//				if(list != null && list.size() > 0){
//					positn2.setDes(list.get(0).getDes());
//					positn2.setPname(list.get(0).getPname());
//				}
				//先删除之前的
//				if(positn.getDes()==null || "".equals(positn.getDes())){//bug 必须删除之前的 xlh  2016.01.27
					positnMapper.deletePositnFirm(positn2);
//				}else{
					//在新增
					positnMapper.insertPositnFirm(positn2);
//				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询仓位新方法
	 * @param positn
	 * @return
	 * @throws Exception
	 */
	public List<Positn> selectPositnNew(Positn positn) throws Exception{
		try{
			String[] code = positn.getCode().split(",");
			List<String> list = new ArrayList<String>();
			for(String positn_code:code){
//				list.add(CodeHelper.replaceCode(positn_code));
				list.add(positn_code);
			}
			positn.setListCode(list.size()==0?null:list);
			return positnMapper.selectPositnNew(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	
	/**
	 * 查询组织结构类型为门店的数据列表
	 * @param department
	 * @param type 
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> queryFirm(Positn positn, Page page) throws CRUDException{
		try{
			return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".findPositnSuper");
//			if(positn.getAcct()=="" || "".equals(positn.getAcct())){
//				return null;
//			}
//			if(page == null ){//如果分页参数类为空，不走分页方法
//				return positnMapper.queryFirm(positn);
//			}else{
//				return pageManager.selectPage(positn, page, PositnMapper.class.getName()+".queryFirm");
////				return pageManager.selectPage(department, page,DepartmentMapper.class.getName()+".queryFirm");
//			}
////			return departmentMapper.queryFirm(department);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询所有未设置虚拟供应商的朱直拨库，基地仓库，加工间，分店，其他
	 * @return
	 */
	public List<Positn> findPositnNoSetDeliver() {
		return positnMapper.findPositnNoSetDeliver();
	}

	/***
	 * 查询boh的成本卡类型
	 * @param code
	 * @return
	 */
	public String getAirdittypeFromBoh(String code) {
		return positnMapper.getAirdittypeFromBoh(code);
	}
}
