package com.choice.scm.service;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FclMapping;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.FclMappingMapper;

/**
 * 描述：财务科目映射设置
 * 日期：2014年1月12日 上午11:24:31
 * 作者：毛明武mmw
 * 文件：FclMappingService.java
 */
@Service
public class FclMappingService{
	private static Logger log = Logger.getLogger(FclMappingService.class);
	@Autowired
	private FclMappingMapper fclMappingMapper;
	@Autowired
	private PageManager<FclMapping> pageManager;
	@Autowired
	private PageManager<Deliver> pageManager1;
	@Autowired
	private PageManager<Positn> pageManager2;
	
	/**
	 * 描述：查询所有数据并且分页
	 * 日期：2014年1月12日 上午11:25:26
	 * 作者：毛明武mmw
	 * 文件：FclMappingService.java
	 * @param FclMapping
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<FclMapping> findAllFclMapping(FclMapping FclMapping, Page page)  throws CRUDException {
		try{
			return pageManager.selectPage(FclMapping, page,FclMappingMapper.class.getName()+".findAllFclMapping");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：根据id查询数据
	 * 日期：2014年1月12日 上午11:25:44
	 * 作者：毛明武mmw
	 * 文件：FclMappingService.java
	 * @param pk_FclMapping
	 * @return
	 * @throws CRUDException
	 */
	public FclMapping findFclMappingById(String pk_FclMapping) throws CRUDException{
		try{
			return fclMappingMapper.findFclMappingById(pk_FclMapping);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 方法描述：保存数据
	 * 日期：2013-10-10 上午10:24:06
	 * 作者：mmw
	 * @param FclMapping
	 * @return
	 */
	public Object editFclMapping(FclMapping FclMapping,String edittype)throws CRUDException {
		try{
			if(null !=edittype && !"".equals(edittype)){ 
				if(edittype.equals("update")){
					fclMappingMapper.updateFclMapping(FclMapping);
				}else if(edittype.equals("save")){
					FclMapping.setPk_fclmapping(CodeHelper.createUUID().substring(0, 20).toUpperCase());
					fclMappingMapper.saveFclMapping(FclMapping);
				}
			}
			return "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 描述：根据id删除数据
	 * 日期：2014年1月12日 上午11:26:28
	 * 作者：毛明武mmw
	 * 文件：FclMappingService.java
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteFclMappingById(List<String> listId) throws CRUDException{
		try{
			fclMappingMapper.deleteFclMappingById(listId);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述:参照界面选择
	 * 作者:mmw
	 * 日期:2013-11-14 上午10:10:50
	 * @param pk_id
	 * @return
	 */
	public List<FclMapping> findAllFclMappingbyId(String pk_id)throws CRUDException {
		try{
			List<String> listId=Arrays.asList(pk_id.split(","));
			List<FclMapping> list=fclMappingMapper.findAllFclMappingbyId(listId);
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDelivers(Deliver deliver, Page page) throws CRUDException
	{
		try {
			return pageManager1.selectPage(deliver, page, FclMappingMapper.class.getName()+".findDeliver");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询仓位信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositns(Deliver deliver, Page page) throws CRUDException
	{
		try {
			return pageManager2.selectPage(deliver, page, FclMappingMapper.class.getName()+".findPositns");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findCustomer(Deliver deliver, Page page) throws CRUDException
	{
		try {
			return pageManager1.selectPage(deliver, page, FclMappingMapper.class.getName()+".findCustomer");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
