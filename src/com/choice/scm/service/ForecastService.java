package com.choice.scm.service;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ItemUse;
import com.choice.scm.domain.PosItemPlan;
import com.choice.scm.domain.PosSalePlan;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.ForecastMapper;

@Service
public class ForecastService {
	
	@Autowired
	private ForecastMapper forecastMapper;
	@Autowired
	private PageManager<ItemUse> pageManager;
	@Autowired
	private PageManager<PosSalePlan> pageManager1;
	@Autowired
	private PageManager<PosItemPlan> pageManager2;

	private final transient Log log = LogFactory.getLog(ForecastService.class);

	private String driver = "";
	private String url = "";
	private String uname = "";
	private String password = "";
	
	/**
	 * 销售数据库连接获取（决策，总部boh，老总部快餐）
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");  
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver_boh");
		url = properties.getProperty("jdbc.url_boh");
		uname = properties.getProperty("jdbc.username_boh");
		password = properties.getProperty("jdbc.password_boh");
	}
	
	/**
	 * 物流库连接获取
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn_scm() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties");  
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver");
		url = properties.getProperty("jdbc.url");
		uname = properties.getProperty("jdbc.username");
		password = properties.getProperty("jdbc.password");
	}
	
	/********************************************************菜品点击率start*************************************************/
	/**
	 * 查找所有的菜品点击率
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemUse> findAllItemUse(ItemUse itemUse, Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(itemUse, page, ForecastMapper.class.getName()+".findAllItemUse");	
			//return forecastMapper.findAllItemUse();			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的菜品点击率
	 * @return
	 * @throws CRUDException
	 */
	public List<ItemUse> findAllItemUse(ItemUse itemUse) throws CRUDException
	{
		try {
			return forecastMapper.findAllItemUse(itemUse);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAll(ItemUse itemUse) throws CRUDException{
		try{
			forecastMapper.deleteAll(itemUse);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算点击率
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalculate(String firmId, String acct, Date bdate, Date edate) throws CRUDException
	{
		try {
			ItemUse itemUse = new ItemUse();
			itemUse.setFirm(firmId);
			deleteAll(itemUse);
			String typ = forecastMapper.getPlanTyp(acct);
			// 获取点击率表达方式（单数，营业额，人数）
			if ("".equals(typ)) {
				typ="人数";
			}
			
			// 获取各班次（人数，营业额，单数）的总数
			//数组值依次代表，工作日一班，工作日二班，工作日三班，工作日四班，节假日一班，节假日二班，节假日三班，节假日四班
			double[] a =  new double[]{0,0,0,0,0,0,0,0};
			a = getAmt(firmId, typ, bdate, edate);
			
		    //计算菜品点击次数并添加到数据库（itemuse）
			insertItemUse(firmId, bdate, edate, a);				
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 获取各班次某道菜的数量
	 * @return
	 * @throws CRUDException
	 */
	public void insertItemUse(String firmId, Date bdate, Date edate, double[] a) throws CRUDException {
		try {
			getJdbcConn();
			Connection conn=null;  
			String sql = "";
			SimpleDateFormat formater = new SimpleDateFormat();  
			String tele_boh = "choice7";
			if("tele".equals(tele_boh)){
				sql = "select TO_DATE(A.DAT,'YYYY-MM-DD') as dat,C.SFT,A.ITEM,A.UNIT,B.pITCODE as itcode,B.pDES as des,SUM(A.CNT) AS CNT ";
				sql += " from FD_ORDR A,pubITEM B,FD_FOLIO C  where  A.FIRMID2=C.FIRMID2 AND C.Firmid2 = '";
				sql += firmId;
				sql += "' and (A.Item=B.pubItem) and (A.Code=C.Serial) AND A.VOID<>'Y' and (A.REFUND=0)";
				sql += " And TO_DATE(A.DAT,'YYYY-MM-DD') >= to_Date('";
				formater.applyPattern("yyyy-MM-dd");  
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And TO_DATE(A.DAT,'YYYY-MM-DD') <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
				sql += "Group by TO_DATE(A.DAT,'YYYY-MM-DD'),C.SFT,A.ITEM,A.UNIT,B.pITCODE,B.pDES order by itcode,TO_DATE(A.DAT,'YYYY-MM-DD'),C.SFT,A.ITEM";
			}else if ("pos".equals(tele_boh)){
				sql = "select a.dat,a.SFT,A.ITEM,A.UNIT,a.itcode,a.des,SUM(A.CNT) AS CNT ";
				sql += " from view_itemsale_dat A  where  A.FIRMID = '";
				sql += firmId;
				sql += " And A.DAT >= to_Date('";
				formater.applyPattern("yyyy-MM-dd");  
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And A.DAT <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
				sql += "Group by A.DAT,A.SFT,A.ITEM,A.UNIT,A.ITCODE,A.DES order by A.DAT,A.SFT,A.ITEM";
			}else if ("choice7".equals(tele_boh)){
				sql = "SELECT STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d') AS DAT,B.ICHANGETABLE AS SFT, A.VPCODE AS ITEM,A.VUNIT AS UNIT,A.VPCODE AS ITCODE,A.VPNAME AS DES,SUM(A.NCOUNT) AS CNT ";
				sql += " from CBOH_ORDR_3CH A,cboh_folio_3ch B  WHERE a.vbcode=b.vbcode AND b.vscode = '";
				sql += firmId;
				sql += "' And STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d') >= STR_TO_DATE('";
				formater.applyPattern("yyyy-MM-dd");  
				sql += formater.format(bdate);
				sql += "','%Y-%m-%d') And STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d') <= STR_TO_DATE('";
				sql += formater.format(edate);
				sql += "','%Y-%m-%d')";
				sql += "Group by STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d') ,B.ICHANGETABLE,A.VPCODE,A.VUNIT, A.VPCODE , A.VPNAME  ORDER by A.VPCODE,STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d'), B.ICHANGETABLE";

			}
			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			PreparedStatement ptmt=conn.prepareStatement(sql);
			ResultSet rs=ptmt.executeQuery(); 
			String item = "";
			boolean first = true;
			boolean empty = true;
			double[] b =  new double[]{0,0,0,0,0,0,0,0};
			ItemUse itemUse = new ItemUse();
			while(rs.next()) {
				String temp = rs.getString("item");
				empty = false;
				if (temp==null){
					return;
				}
				if(!temp.equals(item)) {
					item = temp;
					if (!first) {
						// 计算这道菜的点击率
						if (a[0]!=0) {// 总额不等于0
							itemUse.setCnt1((1000*b[0])/a[0]);
							itemUse.setCnt1old(1000*b[0]/a[0]);
						}else {
							itemUse.setCnt1(0);
							itemUse.setCnt1old(0);
						}
						if (a[1]!=0) {
							itemUse.setCnt2(1000*b[1]/a[1]);
							itemUse.setCnt2old(1000*b[1]/a[1]);
						}else {
							itemUse.setCnt2(0);
							itemUse.setCnt2old(0);
						}
						if (a[2]!=0) {
							itemUse.setCnt3(1000*b[2]/a[2]);
							itemUse.setCnt3old(1000*b[2]/a[2]);
						}else {
							itemUse.setCnt3(0);
							itemUse.setCnt3old(0);
						}
						if (a[3]!=0) {
							itemUse.setCnt4(1000*b[3]/a[3]);
							itemUse.setCnt4old(1000*b[3]/a[3]);
						}else {
							itemUse.setCnt4(0);
							itemUse.setCnt4old(0);
						}
						if (a[4]!=0) {
							itemUse.setHcnt1(1000*b[4]/a[4]);
							itemUse.setHcnt1old(1000*b[4]/a[4]);
						}else {
							itemUse.setHcnt1(0);
							itemUse.setHcnt1old(0);
						}
						if (a[5]!=0) {
							itemUse.setHcnt2(1000*b[5]/a[5]);
							itemUse.setHcnt2old(1000*b[5]/a[5]);
						}else {
							itemUse.setHcnt2(0);
							itemUse.setHcnt2old(0);
						}
						if (a[6]!=0) {
							itemUse.setHcnt3(1000*b[6]/a[6]);
							itemUse.setHcnt3old(1000*b[6]/a[6]);
						}else {
							itemUse.setHcnt3(0);
							itemUse.setHcnt3old(0);
						}
						if (a[7]!=0) {
							itemUse.setHcnt4(1000*b[7]/a[7]);
							itemUse.setHcnt4old(1000*b[7]/a[7]);
						}else {
							itemUse.setHcnt4(0);
							itemUse.setHcnt4old(0);
						}
						//把这道菜的点击率统计信息添加到表
						forecastMapper.insertItemUse(itemUse);
					}else {
						first=false;
					}
					itemUse = new ItemUse();
					itemUse.setFirm(firmId);
					itemUse.setItcode(rs.getString("itcode"));
					itemUse.setItdes(rs.getString("des"));
					itemUse.setItem(rs.getString("item"));
					itemUse.setItunit(rs.getString("unit"));
					for(int i=0;i<8;i++) {
						b[i]=0;
					}
				}
				int sft  = Integer.parseInt(rs.getString("sft")); //班次
				double cnt = Double.parseDouble(rs.getString("cnt"));   //某道菜点击次数
				if (isHoliday(formater.parse(rs.getString("dat")))) {//判断如果是假期
					b[(3+sft)] += cnt;
				}else {
					b[sft-1] += cnt;
				}
			}
			//查询出来的数据为空的时候不进行最后一道菜的点击率计算
			if (!empty) {
				//计算最后一道菜的点击率
				if (a[0]!=0) {//总额不等于0
					itemUse.setCnt1((1000*b[0])/a[0]);
					itemUse.setCnt1old(1000*b[0]/a[0]);
				}else {
					itemUse.setCnt1(0);
					itemUse.setCnt1old(0);
				}
				if (a[1]!=0) {
					itemUse.setCnt2(1000*b[1]/a[1]);
					itemUse.setCnt2old(1000*b[1]/a[1]);
				}else {
					itemUse.setCnt2(0);
					itemUse.setCnt2old(0);
				}
				if (a[2]!=0) {
					itemUse.setCnt3(1000*b[2]/a[2]);
					itemUse.setCnt3old(1000*b[2]/a[2]);
				}else {
					itemUse.setCnt3(0);
					itemUse.setCnt3old(0);
				}
				if (a[3]!=0) {
					itemUse.setCnt4(1000*b[3]/a[3]);
					itemUse.setCnt4old(1000*b[3]/a[3]);
				}else {
					itemUse.setCnt4(0);
					itemUse.setCnt4old(0);
				}
				if (a[4]!=0) {
					itemUse.setHcnt1(1000*b[4]/a[4]);
					itemUse.setHcnt1old(1000*b[4]/a[4]);
				}else {
					itemUse.setHcnt1(0);
					itemUse.setHcnt1old(0);
				}
				if (a[5]!=0) {
					itemUse.setHcnt2(1000*b[5]/a[5]);
					itemUse.setHcnt2old(1000*b[5]/a[5]);
				}else {
					itemUse.setHcnt2(0);
					itemUse.setHcnt2old(0);
				}
				if (a[6]!=0) {
					itemUse.setHcnt3(1000*b[6]/a[6]);
					itemUse.setHcnt3old(1000*b[6]/a[6]);
				}else {
					itemUse.setHcnt3(0);
					itemUse.setHcnt3old(0);
				}
				if (a[7]!=0) {
					itemUse.setHcnt4(1000*b[7]/a[7]);
					itemUse.setHcnt4old(1000*b[7]/a[7]);
				}else {
					itemUse.setHcnt4(0);
					itemUse.setHcnt4old(0);
				}
				//把这道菜的点击率统计信息添加到表
				forecastMapper.insertItemUse(itemUse);
			}
			conn.close(); 
		}catch(Exception e){  
			log.error(e);
			throw new CRUDException(e);  
		} 
	}
	
	/**
	 * 获取各班次（人数，营业额，单数）的总数
	 * @return
	 * @throws CRUDException
	 */
	public double[] getAmt(String firmId, String typ, Date bdate, Date edate) throws CRUDException {
		try {
			getJdbcConn();
//			firmId="10101001";//测试用
			Connection conn=null;  
			double[] a;
			a =  new double[]{0,0,0,0,0,0,0,0};
			SimpleDateFormat formater = new SimpleDateFormat();  
			formater.applyPattern("yyyy-MM-dd");  
			String sql ="";
			String tele_boh = "choice7";
			if("tele".equals(tele_boh)){
				sql = "select TO_DATE(DAT,'YYYY-MM-DD') as dat,to_char(TO_DATE(DAT,'YYYY-MM-DD'),'D') as weekk,sft, ";
			    if (typ=="人数") {
			    	sql += " pax ";
			    }else if (typ=="单数") {
			    	sql += " tbls as pax";
			    } else {
			    	sql += " amt as pax";
			    }
			    sql += " from statisrest where sft in (1,2,3,4) And Firmid2 = '";
			    sql += firmId;
			    
			    sql += "' And TO_DATE(DAT,'YYYY-MM-DD') >= to_Date('";
			   
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And TO_DATE(DAT,'YYYY-MM-DD') <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
			}else if ("pos".equals(tele_boh)){
				sql = "select DAT,to_char(DAT,''D'') as weekk,sft, ";
				if (typ=="人数") {
			    	sql += " 0 as pax ";
			    }else if (typ=="单数") {
			    	sql += " tbls as pax";
			    } else {
			    	sql += " amt as pax";
			    }
				sql += " from view_sale_dat 1=1 and sft in (1,2,3,4) And Firmid= '";
			    sql += firmId;
			    
			    sql += "' And DAT >= to_Date('";
			   
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And DAT <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
			}else if("choice7".equals(tele_boh)){
				sql = "select STR_TO_DATE(dworkdate,'%Y-%m-%d') as dat,DAYOFWEEK(STR_TO_DATE(dworkdate,'%Y-%m-%d')) as weekk,ichangetable as sft, ";
			    if (typ=="人数") {
			    	sql += " ipeolenum as pax ";
			    }else if (typ=="单数") {
			    	sql += " count(vbcode) as pax";
			    } else {
			    	sql += " sum(nymoney) as pax";
			    }
			    sql += " from CBOH_FOLIO_3CH where ichangetable in (1,2,3,4) And vscode = '";
			    sql += firmId;
			    
			    sql += "' And STR_TO_DATE(dworkdate,'%Y-%m-%d') >= STR_TO_DATE('";
			   
				sql += formater.format(bdate);
				sql += "', '%Y-%m-%d') And STR_TO_DATE(dworkdate,'%Y-%m-%d') <= STR_TO_DATE('";
				sql += formater.format(edate);
				sql += "', '%Y-%m-%d') GROUP BY STR_TO_DATE(dworkdate,'%Y-%m-%d') ,DAYOFWEEK(STR_TO_DATE(dworkdate,'%Y-%m-%d')),ichangetable";

			}
			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			PreparedStatement ptmt=conn.prepareStatement(sql);
			ResultSet rs=ptmt.executeQuery(); 
			while(rs.next()) {  
				int sft  = Integer.parseInt(rs.getString("sft")); //班次
				double pax = Double.valueOf(rs.getString("pax"));   //营业额（人数，单数）
				if (isHoliday(formater.parse(rs.getString("dat")))) {//判断如果是假期
					a[(3+sft)] += pax;
				}else {
					a[sft-1] += pax;
				}
				
			}
			conn.close(); 
			return a;
		}catch(Exception e){  
			log.error(e);
			throw new CRUDException(e);  
		} 
	}
	
	/**
	 * 判断是否节假日
	 * @return true 是节假日
	 * @throws CRUDException
	 */
	public boolean isHoliday(Date date) throws CRUDException { 
		Connection conn=null;  
		PreparedStatement ptmt=null;
		ResultSet rs=null;
		try {
			getJdbcConn_scm();  
			SimpleDateFormat formater = new SimpleDateFormat(); 
			String sql ="";
			sql="select * from holiday Where DAT =  to_date('"; 
			formater.applyPattern("yyyy/MM/dd");  
			sql+=formater.format(date);
			sql += "', 'YYYY/MM/DD')";
			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			ptmt=conn.prepareStatement(sql);
			rs=ptmt.executeQuery();
			if (!rs.next()) {
				return false;
			}
			return true;
		}catch(Exception e){  
			log.error(e);
			throw new CRUDException(e);  
		}finally{
			try {
				if(null!=conn){
					conn.close();
				}
				if(null!=ptmt){
					ptmt.close();
				}
				if(null!=rs){
					rs.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @throws CRUDException
	 */
	public void update(ItemUse itemUse) throws CRUDException {
		try {
			for (int i = 0; i < itemUse.getItemUseList().size(); i++) {//批量修改
				ItemUse itemUse_=new ItemUse();
				itemUse_.setItem(itemUse.getItemUseList().get(i).getItem());
				itemUse_.setCnt1(itemUse.getItemUseList().get(i).getCnt1());
				itemUse_.setCnt2(itemUse.getItemUseList().get(i).getCnt2());
				itemUse_.setCnt3(itemUse.getItemUseList().get(i).getCnt3());
				itemUse_.setCnt4(itemUse.getItemUseList().get(i).getCnt4());
				
				itemUse_.setHcnt1(itemUse.getItemUseList().get(i).getHcnt1());
				itemUse_.setHcnt2(itemUse.getItemUseList().get(i).getHcnt2());
				itemUse_.setHcnt3(itemUse.getItemUseList().get(i).getHcnt3());
				itemUse_.setHcnt4(itemUse.getItemUseList().get(i).getHcnt4());
				forecastMapper.update(itemUse_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出菜品点击率Excel 
	public boolean exportExcel(OutputStream os,List<ItemUse> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"菜品点击率"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "菜品编码"));   
			sheet.addCell(new Label(1, 2, "菜品名称"));  
			sheet.addCell(new Label(2, 2, "单位"));   
			sheet.addCell(new Label(3, 2, "工作日|一班|计算"));  
			sheet.addCell(new Label(4, 2, "工作日|一班|调整"));   
			sheet.addCell(new Label(5, 2, "工作日|二班|计算"));  
			sheet.addCell(new Label(6, 2, "工作日|二班|调整"));   
			sheet.addCell(new Label(7, 2, "工作日|三班|计算"));  
			sheet.addCell(new Label(8, 2, "工作日|三班|调整"));
			sheet.addCell(new Label(9, 2, "工作日|四班|计算"));  
			sheet.addCell(new Label(10, 2, "工作日|四班|调整")); 
			sheet.addCell(new Label(11, 2, "节假日|一班|计算"));  
			sheet.addCell(new Label(12, 2, "节假日|一班|调整"));   
			sheet.addCell(new Label(13, 2, "节假日|二班|计算"));  
			sheet.addCell(new Label(14, 2, "节假日|二班|调整"));   
			sheet.addCell(new Label(15, 2, "节假日|三班|计算"));  
			sheet.addCell(new Label(16, 2, "节假日|三班|调整"));   
			sheet.addCell(new Label(17, 2, "节假日|四班|计算"));
			sheet.addCell(new Label(18, 2, "节假日|四班|调整"));
            sheet.mergeCells(0, 0, 18, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getItcode()));
            	sheet.addCell(new Label(1, i+3, list.get(i).getItdes()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getItunit()));
            	sheet.addCell(new Label(3, i+3, String.valueOf(list.get(i).getCnt1old())));
            	sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getCnt1())));
	    		sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getCnt2old())));
	    		sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getCnt2())));
	    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getCnt3old())));
	    		sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getCnt3())));
	    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getCnt4old())));
	    		sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getCnt4())));
	    		sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getHcnt1old())));
	    		sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getHcnt1())));
	    		sheet.addCell(new Label(13, i+3, String.valueOf(list.get(i).getHcnt2old())));
	    		sheet.addCell(new Label(14, i+3, String.valueOf(list.get(i).getHcnt2())));
	    		sheet.addCell(new Label(15, i+3, String.valueOf(list.get(i).getHcnt3old())));
	    		sheet.addCell(new Label(16, i+3, String.valueOf(list.get(i).getHcnt3())));
	    		sheet.addCell(new Label(17, i+3, String.valueOf(list.get(i).getHcnt4old())));
	    		sheet.addCell(new Label(18, i+3, String.valueOf(list.get(i).getHcnt4())));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/********************************************************菜品点击率end*************************************************/
	
	/********************************************************营业预估start*************************************************/
	
	/**
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosSalePlan> findPosSalePlan(PosSalePlan posSalePlan, Page page) throws CRUDException
	{
		try {
			return pageManager1.selectPage(posSalePlan, page, ForecastMapper.class.getName()+".findPosSalePlan");	
			//return forecastMapper.findPosSalePlan(posSalePlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找营业预估
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosSalePlan> findPosSalePlan(PosSalePlan posSalePlan) throws CRUDException
	{
		try {
			return forecastMapper.findPosSalePlan(posSalePlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 总部重新计算营业预估
	 * @param ls
	 * @param string
	 * @param bdate
	 * @param edate
	 * @param startdate
	 * @param enddate
	 * @param mis
	 * @throws CRUDException 
	 */
	public void saveCalPosSalePlanByArea(List<Positn> list, String acct,
			Date bdate, Date edate, Date startdate, Date enddate, String mis) throws CRUDException {
		for(Positn positn: list){
			log.info("----saveCalPosSalePlan----"+positn.getFirm()+"****"+positn.getCode());
			saveCalPosSalePlan(positn.getFirm(), positn.getCode(), acct, bdate, edate, startdate, enddate, mis);
		}
	}
	/**
	 * 重新计算营业预估
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPosSalePlan(String firmId, String firmCode, String acct, Date bdate, Date edate, Date startdate, Date enddate,String ynZb) throws CRUDException
	{
		try {
			String typ = forecastMapper.getPlanTyp(acct);
			// 获取点击率表达方式（单数，营业额，人数）
			if ("".equals(typ)) {
				typ="人数";
			}
			
			// 获取各班各工作日的天数
			//数组值依次代表，{一班，二班，三班，四班}{星期一，星期二，星期三，星期四，星期五，星期六，星期天}
			double[][] a =  new double[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
			double[][] b =  new double[][]{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
			String[] c =  new String[1000]; 
			String[] d =  new String[1000]; 
			String[] e =  new String[1000]; 
			SimpleDateFormat formater = new SimpleDateFormat();  
			formater.applyPattern("yyyy-MM-dd"); 
			getJdbcConn();
			Connection conn=null;   
			String sql = "";
			String tele_boh = "choice7";
			if("tele".equals(tele_boh)){
				sql = "select TO_DATE(DAT,'YYYY-MM-DD') as dat,to_char(TO_DATE(DAT,'YYYY-MM-DD'),'D') as weekk,sft, ";
			    if (typ=="人数") {
			    	sql += " pax ";
			    }else if (typ=="单数") {
			    	sql += " tbls as pax";
			    } else {
			    	sql += " amt as pax";
			    }
			    sql += " from statisrest where sft in (1,2,3,4) And Firmid = '";
			    sql += firmId;
			    sql += "' And TO_DATE(DAT,'YYYY-MM-DD') >= to_Date('";
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And TO_DATE(DAT,'YYYY-MM-DD') <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
			}else if("pos".equals(tele_boh)){
				sql = "select dat,to_char(DAT,'D')  as week,sft,";
			    if (typ=="人数") {
			    	sql += " pax ";
			    }else if (typ=="单数") {
			    	sql += " tbls as pax";
			    } else {
			    	sql += " amt as pax";
			    }
			    sql += " from view_sale_dat where 1=1 and sft in (1,2,3,4) And Firmid = '";
			    sql += firmId;
			    sql += "' And dat >= to_Date('";
				formater.applyPattern("yyyy-MM-dd");  
				sql += formater.format(bdate);
				sql += "', 'YYYY-MM-DD') And dat <= to_Date('";
				sql += formater.format(edate);
				sql += "', 'YYYY-MM-DD')";
			}else if("choice7".equals(tele_boh)){
				sql = "select STR_TO_DATE(a.dworkdate,'%Y-%m-%d') as dat,DAYOFWEEK(STR_TO_DATE(a.dworkdate,'%Y-%m-%d')) as weekk,A.ichangetable as sft,dp.vcode as dept,";
			    if (typ=="人数") {
			    	sql += " A.ipeolenum as pax ";
			    }else if (typ=="单数") {
			    	sql += " count(C.vbcode) as pax";
			    } else {
			    	sql += " sum(C.nymoney) as pax";
			    }
			    sql += ",b.specialevents from CBOH_ORDR_3CH C LEFT JOIN CBOH_FOLIO_3CH  A ON A.VBCODE =C.VBCODE";
			    sql += " LEFT JOIN cboh_pubitem_3ch d on c.vpcode = d.vcode   LEFT JOIN cboh_printprogt_3ch pt on a.pk_store = pt.pk_store";
			    sql += " LEFT JOIN cboh_printprog_3ch pr on pt.pk_printprogt = pr.pk_printprogt and pr.pk_pubitem = d.pk_pubitem";
			    sql += " LEFT JOIN cboh_storedept_3ch dp on pr.pk_storedept = dp.vcode and a.pk_store=dp.pk_store";
			    sql += " LEFT JOIN CBOH_cashoperate_3CH b  ON a.dworkdate = b.dworkdate where a.ichangetable in (1,2,3,4) And a.vscode = '";
			    sql += firmId;
			    sql += "' And STR_TO_DATE(a.dworkdate,'%Y-%m-%d') >= STR_TO_DATE('";
				sql += formater.format(bdate);
				sql += "', '%Y-%m-%d') And STR_TO_DATE(a.dworkdate,'%Y-%m-%d') <= STR_TO_DATE('";
				sql += formater.format(edate);
				sql += "', '%Y-%m-%d') GROUP BY STR_TO_DATE(a.dworkdate,'%Y-%m-%d'),DAYOFWEEK(STR_TO_DATE(a.dworkdate,'%Y-%m-%d')),A.ichangetable,B.SPECIALEVENTS,dp.vcode  ORDER  BY  DAT ,SFT";
			}

			Class.forName(driver);  
			conn=DriverManager.getConnection(url, uname, password);
			PreparedStatement ptmt=conn.prepareStatement(sql);
			ResultSet rs=ptmt.executeQuery(); 
			int i=0;
			while(rs.next()) {
				int num =i++;
				int week = Integer.parseInt(rs.getString("weekk")); //周几
				int sft  = Integer.parseInt(rs.getString("sft")); //班次
				c[num]  = rs.getString("specialevents"); //特殊事件
				d[num]  = rs.getString("dept"); //档口
//				e[i++]  = rs.getString("VCCLEASSNAME"); //档口名称
				double pax = Double.valueOf(rs.getString("pax"));   //营业额（人数，单数）
				a[sft-1][week-1]++;
				b[sft-1][week-1]+=pax;
			}
			conn.close(); 
			
			Calendar bcal = Calendar.getInstance();
			Calendar ecal = Calendar.getInstance();
			bcal.setTime(startdate);
			ecal.setTime(enddate);
			ecal.add(Calendar.DATE, 1);     // 加一天
			// 按日期循环
			int j =0;
			while (!bcal.equals(ecal)) {
			int w = bcal.get(Calendar.DAY_OF_WEEK);//值为1~7分别代表星期日，星期一，星期二，星期三，星期四，星期五，星期六
			 
			 PosSalePlan posSalePlan = new PosSalePlan();
			 posSalePlan.setDat(DateFormat.formatDate(bcal.getTime(), "yyyy-MM-dd"));
			 posSalePlan.setFirm(firmCode);
			 posSalePlan.setYnZb(ynZb==null?"Y":"");//空的话为总部 非空为门店
			 posSalePlan.setMemo(c[j++]);//特殊事件
			 posSalePlan.setDept(d[j++]);
			 posSalePlan.setDeptdes(e[j++]);
			 forecastMapper.deletePosSalePlan(posSalePlan); // 删除数据库中某日数据
			 if (a[0][w-1]!=0) {
				 posSalePlan.setPax1(b[0][w-1]/a[0][w-1]);
				 posSalePlan.setPax1old(b[0][w-1]/a[0][w-1]); 
			 }else {
				 posSalePlan.setPax1(0);
				 posSalePlan.setPax1old(0);
			 }
			 if (a[1][w-1]!=0) {
				 posSalePlan.setPax2(b[1][w-1]/a[1][w-1]);
				 posSalePlan.setPax2old(b[1][w-1]/a[1][w-1]); 
			 }else {
				 posSalePlan.setPax2(0);
				 posSalePlan.setPax2old(0);
			 }
			 if (a[2][w-1]!=0) {
				 posSalePlan.setPax3(b[2][w-1]/a[2][w-1]);
				 posSalePlan.setPax3old(b[2][w-1]/a[2][w-1]);
			 }else {
				 posSalePlan.setPax3(0);
				 posSalePlan.setPax3old(0);
			 }
			 if (a[3][w-1]!=0) {
				 posSalePlan.setPax4(b[3][w-1]/a[3][w-1]);
				 posSalePlan.setPax4old(b[3][w-1]/a[3][w-1]);
			 }else {
				 posSalePlan.setPax4(0);
				 posSalePlan.setPax4old(0);
			 }
			 forecastMapper.insertPosSalePlan(posSalePlan); // 添加数据库中某日数据
			 bcal.add(Calendar.DATE, 1);     // 加一天
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 保存修改调整量(营业预估)
	 * @param posSalePlan
	 * @throws CRUDException
	 */
	public void updateSalePlan(PosSalePlan posSalePlan) throws CRUDException {
		try {
			for (int i = 0; i < posSalePlan.getPosSalePlanList().size(); i++) {//批量修改
				PosSalePlan posSalePlan_=new PosSalePlan();
				posSalePlan_.setFirm(posSalePlan.getPosSalePlanList().get(i).getFirm());
				posSalePlan_.setDat(posSalePlan.getPosSalePlanList().get(i).getDat());
				posSalePlan_.setPax1(posSalePlan.getPosSalePlanList().get(i).getPax1());
				posSalePlan_.setPax2(posSalePlan.getPosSalePlanList().get(i).getPax2());
				posSalePlan_.setPax3(posSalePlan.getPosSalePlanList().get(i).getPax3());
				posSalePlan_.setPax4(posSalePlan.getPosSalePlanList().get(i).getPax4());
				
				forecastMapper.updateSalePlan(posSalePlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出营业预估Excel 
	public boolean exportExcelcast(OutputStream os,List<PosSalePlan> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"营业预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "分店"));      
            sheet.addCell(new Label(1, 2, "日期"));      
            sheet.addCell(new Label(2, 2, "星期"));      
            sheet.addCell(new Label(3, 2, "假日"));      
            sheet.addCell(new Label(4, 2, "特殊事件"));  
            sheet.addCell(new Label(5, 2, "一班|预估")); 
            sheet.addCell(new Label(6, 2, "一班|调整")); 
            sheet.addCell(new Label(7, 2, "二班|预估")); 
            sheet.addCell(new Label(8, 2, "二班|调整")); 
            sheet.addCell(new Label(9, 2, "三班|预估")); 
            sheet.addCell(new Label(10, 2, "三班|调整"));
            sheet.addCell(new Label(11, 2, "四班|预估"));
            sheet.addCell(new Label(12, 2, "四班|调整"));
            sheet.addCell(new Label(13, 2, "合计|预估"));
            sheet.addCell(new Label(14, 2, "合计|调整"));

            sheet.mergeCells(0, 0, 14, 1);
            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd"); 
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm())); 
            	sheet.addCell(new Label(1, i+3, String.valueOf(sdf.format(list.get(i).getDat()))));                  
            	sheet.addCell(new Label(2, i+3, list.get(i).getWeek()));                   
            	sheet.addCell(new Label(3, i+3, ""));                                      
            	sheet.addCell(new Label(4, i+3, ""));                                      
            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getPax1old())));
            	sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getPax1())));   
            	sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getPax2old())));    
            	sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getPax2())));       
            	sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getPax3old())));    
            	sheet.addCell(new Label(10,i+3, String.valueOf(list.get(i).getPax3())));       
            	sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getPax4old())));   
            	sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getPax4())));      
            	sheet.addCell(new Label(13, i+3, String.valueOf(list.get(i).getTotalold())));  
            	sheet.addCell(new Label(14, i+3, String.valueOf(list.get(i).getTotal())));     
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/********************************************************营业预估end*************************************************/
	
	/****************************************************预估菜品销售计划start*************************************************/
	
	/**
	 * 查找预估菜品销售计划
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosItemPlan> findPosItemPlan(PosItemPlan posItemPlan, Page page) throws CRUDException
	{
		try {			
			return pageManager2.selectPage(posItemPlan, page, ForecastMapper.class.getName()+".findPosItemPlan");			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找预估菜品销售计划
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosItemPlan> findPosItemPlan(PosItemPlan posItemPlan) throws CRUDException
	{
		try {
			return forecastMapper.findPosItemPlan(posItemPlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找菜品销售计划天数
	 * @param posItemPlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosItemPlan> findPosItemPlanForDays(PosItemPlan posItemPlan) throws CRUDException
	{
		try {
			return forecastMapper.findPosItemPlanForDays(posItemPlan);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算菜品销售计划
	 * @return
	 * @throws CRUDException
	 */
	public void saveCalPlan(String firmId, Date bdate, double a[],String  dept) throws CRUDException
	{
		try {
			ItemUse itemUse = new ItemUse();
			itemUse.setFirm(firmId);
			List<ItemUse> itemUseList = forecastMapper.findAllItemUse(itemUse);	
			// 根据每道菜的点击率计算此道菜的销售计划
			for (int i=0;i<itemUseList.size();i++) {
				PosItemPlan posItemPlan = new PosItemPlan();
				Calendar bcal = Calendar.getInstance();
				bcal.setTime(bdate);
				// 按日期循环
				int w = bcal.get(Calendar.DAY_OF_WEEK);//值为1~7分别代表星期日，星期一，星期二，星期三，星期四，星期五，星期六
				if (w==1||w==7) {//节假日
					posItemPlan.setCnt1(Math.round(itemUseList.get(i).getHcnt1()*a[0]/1000));
					posItemPlan.setCnt2(Math.round(itemUseList.get(i).getHcnt2()*a[1]/1000));
					posItemPlan.setCnt3(Math.round(itemUseList.get(i).getHcnt3()*a[2]/1000));
					posItemPlan.setCnt4(Math.round(itemUseList.get(i).getHcnt4()*a[3]/1000));
					posItemPlan.setCnt1old(Math.round(itemUseList.get(i).getHcnt1old()*a[0]/1000));
					posItemPlan.setCnt2old(Math.round(itemUseList.get(i).getHcnt2old()*a[1]/1000));
					posItemPlan.setCnt3old(Math.round(itemUseList.get(i).getHcnt3old()*a[2]/1000));
					posItemPlan.setCnt4old(Math.round(itemUseList.get(i).getHcnt4old()*a[3]/1000));
				}else {//工作日
					posItemPlan.setCnt1(Math.round(itemUseList.get(i).getCnt1()*a[0]/1000));
					posItemPlan.setCnt2(Math.round(itemUseList.get(i).getCnt2()*a[1]/1000));
					posItemPlan.setCnt3(Math.round(itemUseList.get(i).getCnt3()*a[2]/1000));
					posItemPlan.setCnt4(Math.round(itemUseList.get(i).getCnt4()*a[3]/1000));
					posItemPlan.setCnt1old(Math.round(itemUseList.get(i).getCnt1old()*a[0]/1000));
					posItemPlan.setCnt2old(Math.round(itemUseList.get(i).getCnt2old()*a[1]/1000));
					posItemPlan.setCnt3old(Math.round(itemUseList.get(i).getCnt3old()*a[2]/1000));
					posItemPlan.setCnt4old(Math.round(itemUseList.get(i).getCnt4old()*a[3]/1000));
				}

				posItemPlan.setDat(bdate);
				posItemPlan.setItcode(itemUseList.get(i).getItcode());
				posItemPlan.setFirm(firmId);
				posItemPlan.setDept(dept);
				posItemPlan.setItdes(itemUseList.get(i).getItdes());
				posItemPlan.setItunit(itemUseList.get(i).getItunit());
				posItemPlan.setItem(itemUseList.get(i).getItem());
				
				forecastMapper.insertPosItemPlan(posItemPlan);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posSalePlan
	 * @throws CRUDException
	 */
	public void updateItemPlan(PosItemPlan posItemPlan) throws CRUDException {
		try {
			for (int i = 0; i < posItemPlan.getPosItemPlanList().size(); i++) {//批量修改
				forecastMapper.updateItemPlan(posItemPlan.getPosItemPlanList().get(i));
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改调整量(预估菜品销售计划)
	 * @param posSalePlan
	 * @throws CRUDException
	 */
	public void updateItemPlan(PosItemPlan posItemPlan, String firm) throws CRUDException {
		try {
			for (int i = 0; i < posItemPlan.getPosItemPlanList().size(); i++) {//批量修改
				PosItemPlan posItemPlan_=new PosItemPlan();
				posItemPlan_.setFirm(firm);
				posItemPlan_.setItem(posItemPlan.getPosItemPlanList().get(i).getItem());
				posItemPlan_.setDat(posItemPlan.getPosItemPlanList().get(i).getDat());
				posItemPlan_.setCnt1(posItemPlan.getPosItemPlanList().get(i).getCnt1());
				posItemPlan_.setCnt2(posItemPlan.getPosItemPlanList().get(i).getCnt2());
				posItemPlan_.setCnt3(posItemPlan.getPosItemPlanList().get(i).getCnt3());
				posItemPlan_.setCnt4(posItemPlan.getPosItemPlanList().get(i).getCnt4());
				
				forecastMapper.updateItemPlan(posItemPlan_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有预估菜品销售计划
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAllPlan(PosItemPlan posItemPlan) throws CRUDException{
		try{
			forecastMapper.deleteAllPlan(posItemPlan);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	//导出菜品销售计划Excel 
	public boolean exportExcelPlan(OutputStream os,List<PosItemPlan> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"分店菜品销售预估"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "分店"));      
            sheet.addCell(new Label(1, 2, "编码"));      
            sheet.addCell(new Label(2, 2, "名称"));      
            sheet.addCell(new Label(3, 2, "单位"));      
            sheet.addCell(new Label(4, 2, "一班|计算")); 
            sheet.addCell(new Label(5, 2, "一班|改后")); 
            sheet.addCell(new Label(6, 2, "二班|计算")); 
            sheet.addCell(new Label(7, 2, "二班|改后")); 
            sheet.addCell(new Label(8, 2, "三班|计算")); 
            sheet.addCell(new Label(9, 2, "三班|改后")); 
            sheet.addCell(new Label(10,2, "四班|计算")); 
            sheet.addCell(new Label(11, 2, "四班|改后"));
            sheet.mergeCells(0, 0, 11, 1);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm()));                     
            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));                     
            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));                      
            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));                     
            	sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getCnt1old())));    
            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getCnt1())));       
            	sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getCnt2old())));    
            	sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getCnt2())));       
            	sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getCnt3old())));    
            	sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getCnt3())));       
            	sheet.addCell(new Label(10,i+3, String.valueOf(list.get(i).getCnt4old())));    
            	sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getCnt4())));      
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/********************************************************预估菜品销售计划end*************************************************/

	/****************************************************营业预估实际对比start*************************************************/

	/**
	 * 查找营业预估实际对比
	 * @param posSalePlan
	 * @return
	 * @throws CRUDException
	 */
	public List<PosSalePlan> findPosSalePlan1(PosSalePlan posSalePlan, String acct) throws CRUDException
	{
		try {
			String typ = forecastMapper.getPlanTyp(acct);
			// 获取点击率表达方式（单数，营业额，人数）
			if ("".equals(typ)) {
				typ="人数";
			}
			
			// 获取各班次（人数，营业额，单数）的总数
			// 数组值依次代表，工作日一班，工作日二班，工作日三班，工作日四班，节假日一班，节假日二班，节假日三班，节假日四班
			double[] a =  new double[]{0,0,0,0,0,0,0,0};
			posSalePlan.setFirm(CodeHelper.replaceCode(posSalePlan.getFirm()));
			List<PosSalePlan> posSalePlanList = forecastMapper.findPosSalePlan1(posSalePlan);
			// 设置销售实际量，预估差异率和调整差异率
			for (int i=0;i<posSalePlanList.size();i++) {
				a = getAmt(posSalePlanList.get(i).getFirm(), typ, posSalePlanList.get(i).getDat(), posSalePlanList.get(i).getDat());
				posSalePlanList.get(i).setPax1act(posSalePlanList.get(i).getPax()+a[0]+a[4]);
				posSalePlanList.get(i).setPax2act(posSalePlanList.get(i).getPax()+a[1]+a[5]);
				posSalePlanList.get(i).setPax3act(posSalePlanList.get(i).getPax()+a[2]+a[6]);
				posSalePlanList.get(i).setPax4act(posSalePlanList.get(i).getPax()+a[3]+a[7]);
				if (posSalePlanList.get(i).getPax()+a[0]+a[4]!=0) {
					posSalePlanList.get(i).setLv1((posSalePlanList.get(i).getPax1()-posSalePlanList.get(i).getPax1act())*100/posSalePlanList.get(i).getPax1act());
					posSalePlanList.get(i).setLv1old((posSalePlanList.get(i).getPax1old()-posSalePlanList.get(i).getPax1act())*100/posSalePlanList.get(i).getPax1act());
				}else {
					posSalePlanList.get(i).setLv1(0);
				}
				if (posSalePlanList.get(i).getPax()+a[1]+a[5]!=0) {
					posSalePlanList.get(i).setLv2((posSalePlanList.get(i).getPax2()-posSalePlanList.get(i).getPax2act())*100/posSalePlanList.get(i).getPax2act());
					posSalePlanList.get(i).setLv2old((posSalePlanList.get(i).getPax2old()-posSalePlanList.get(i).getPax2act())*100/posSalePlanList.get(i).getPax2act());
				}else {
					posSalePlanList.get(i).setLv2(0);
				}
				if (posSalePlanList.get(i).getPax()+a[2]+a[6]!=0) {
					posSalePlanList.get(i).setLv3((posSalePlanList.get(i).getPax3()-posSalePlanList.get(i).getPax3act())*100/posSalePlanList.get(i).getPax3act());
					posSalePlanList.get(i).setLv3old((posSalePlanList.get(i).getPax3old()-posSalePlanList.get(i).getPax3act())*100/posSalePlanList.get(i).getPax3act());
				}else {
					posSalePlanList.get(i).setLv3(0);
				}
				if (posSalePlanList.get(i).getPax()+a[3]+a[7]!=0) {
					posSalePlanList.get(i).setLv4((posSalePlanList.get(i).getPax4()-posSalePlanList.get(i).getPax4act())*100/posSalePlanList.get(i).getPax4act());
					posSalePlanList.get(i).setLv4old((posSalePlanList.get(i).getPax4old()-posSalePlanList.get(i).getPax4act())*100/posSalePlanList.get(i).getPax4act());
				}else {
					posSalePlanList.get(i).setLv4(0);
				}
				
				posSalePlanList.get(i).setTotalact(posSalePlanList.get(i).getPax1act()+posSalePlanList.get(i).getPax2act()+posSalePlanList.get(i).getPax3act()+posSalePlanList.get(i).getPax4act());
				posSalePlanList.get(i).setTotallv(posSalePlanList.get(i).getLv1()+posSalePlanList.get(i).getLv2()+posSalePlanList.get(i).getLv3()+posSalePlanList.get(i).getLv4());
				posSalePlanList.get(i).setTotallvold(posSalePlanList.get(i).getLv1old()+posSalePlanList.get(i).getLv2old()+posSalePlanList.get(i).getLv3old()+posSalePlanList.get(i).getLv4old());
				
			}		
			return posSalePlanList;		
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
		//导出营业预估实际对比Excel 
		public boolean exportSaleCmp(OutputStream os,List<PosSalePlan> list) {   
			WritableWorkbook workBook = null;
			try {
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("Sheet1", 0);
	            sheet.addCell(new Label(0,0,"营业预估实际对比"));
	            //设置表格表头
	            sheet.addCell(new Label(0, 2, "分店"));            
	            sheet.addCell(new Label(1, 2, "日期"));            
	            sheet.addCell(new Label(2, 2, "星期"));            
	            sheet.addCell(new Label(3, 2, "假日"));            
	            sheet.addCell(new Label(4, 2, "特殊事件"));   
	            
	            sheet.addCell(new Label(5, 2, "一班|预估"));       
	            sheet.addCell(new Label(6, 2, "一班|调整"));       
	            sheet.addCell(new Label(7, 2, "一班|实际"));       
	            sheet.addCell(new Label(8, 2, "一班|预估差异率")); 
	            sheet.addCell(new Label(9, 2, "一班|调整差异率")); 
	            
	            sheet.addCell(new Label(10,2, "二班|预估"));       
	            sheet.addCell(new Label(11, 2, "二班|调整"));      
	            sheet.addCell(new Label(12, 2, "二班|实际"));      
	            sheet.addCell(new Label(13, 2, "二班|预估差异率"));
	            sheet.addCell(new Label(14, 2, "二班|调整差异率"));
	            
	            sheet.addCell(new Label(15, 2, "三班|预估"));      
	            sheet.addCell(new Label(16, 2, "三班|调整"));      
	            sheet.addCell(new Label(17, 2, "三班|实际"));      
	            sheet.addCell(new Label(18, 2, "三班|预估差异率"));
	            sheet.addCell(new Label(19, 2, "三班|调整差异率"));
	            
	            sheet.addCell(new Label(20, 2, "四班|预估"));      
	            sheet.addCell(new Label(21, 2, "四班|调整"));      
	            sheet.addCell(new Label(22, 2, "四班|实际"));      
	            sheet.addCell(new Label(23, 2, "四班|预估差异率"));
	            sheet.addCell(new Label(24, 2, "四班|调整差异率"));
	            
	            sheet.addCell(new Label(25, 2, "合计|预估"));      
	            sheet.addCell(new Label(26, 2, "合计|调整"));      
	            sheet.addCell(new Label(27, 2, "合计|实际"));      
	            sheet.addCell(new Label(28, 2, "合计|预估差异率"));
	            sheet.addCell(new Label(29, 2, "合计|调整差异率"));
				
	            sheet.mergeCells(0, 0, 29, 1);
	            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd"); 
	            //遍历list填充表格内容
	            for(int i=0;i<list.size();i++) {
	            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm()));                     
	            	sheet.addCell(new Label(1, i+3, String.valueOf(sdf.format(list.get(i).getDat()))));        
	            	sheet.addCell(new Label(2, i+3, list.get(i).getWeek()));                       
	            	sheet.addCell(new Label(3, i+3, ""));                                          
	            	sheet.addCell(new Label(4, i+3, ""));     
	            	
	            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getPax1old())));    
	            	sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getPax1())));       
	            	sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getPax1act())));    
	            	sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getLv1old())));     
	            	sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getLv1())));    
	            	
	            	sheet.addCell(new Label(10,i+3, String.valueOf(list.get(i).getPax2old())));    
	            	sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getPax2())));      
	            	sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getPax2act())));   
	            	sheet.addCell(new Label(13, i+3, String.valueOf(list.get(i).getLv2old())));    
	            	sheet.addCell(new Label(14, i+3, String.valueOf(list.get(i).getLv2())));    
	            	
	            	sheet.addCell(new Label(15, i+3, String.valueOf(list.get(i).getPax3old())));   
	            	sheet.addCell(new Label(16, i+3, String.valueOf(list.get(i).getPax3())));      
	            	sheet.addCell(new Label(17, i+3, String.valueOf(list.get(i).getPax3act())));   
	            	sheet.addCell(new Label(18, i+3, String.valueOf(list.get(i).getLv3old())));    
	            	sheet.addCell(new Label(19, i+3, String.valueOf(list.get(i).getLv3())));     
	            	
	            	sheet.addCell(new Label(20, i+3, String.valueOf(list.get(i).getPax4old())));   
	            	sheet.addCell(new Label(21, i+3, String.valueOf(list.get(i).getPax4())));      
	            	sheet.addCell(new Label(22, i+3, String.valueOf(list.get(i).getPax4act())));   
	            	sheet.addCell(new Label(23, i+3, String.valueOf(list.get(i).getLv4old())));    
	            	sheet.addCell(new Label(24, i+3, String.valueOf(list.get(i).getLv4())));     
	            	
	            	sheet.addCell(new Label(25, i+3, String.valueOf(list.get(i).getTotalold())));  
	            	sheet.addCell(new Label(26, i+3, String.valueOf(list.get(i).getTotal())));     
	            	sheet.addCell(new Label(27, i+3, String.valueOf(list.get(i).getTotalact())));  
	            	sheet.addCell(new Label(28, i+3, String.valueOf(list.get(i).getTotallv())));   
	            	sheet.addCell(new Label(29, i+3, String.valueOf(list.get(i).getTotallvold())));
		    		}
	            workBook.write();
	            os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		
		/********************************************************营业预估实际对比end*************************************************/
		

		/****************************************************菜品预估实际对比start*************************************************/
			
		/**
		 * 查找一段时间内的预估菜品销售计划
		 * @param posItemPlan
		 * @return
		 * @throws CRUDException
		 */
		public List<PosItemPlan> findPosItemPlan1(PosItemPlan posItemPlan) throws CRUDException
		{
			try {
				return forecastMapper.findPosItemPlan1(posItemPlan);			
			} catch (Exception e) {
				log.error(e);
				throw new CRUDException(e);
			}
		}
		
		/**
		 * 获取各班次某道菜的数量
		 * @return
		 * @throws CRUDException
		 */
		public List<PosItemPlan> getList(String firmId, Date bdate, Date edate, PosItemPlan posItemPlan) throws CRUDException { 
			try {
				List<PosItemPlan> posItemPlanList = forecastMapper.findPosItemPlan1(posItemPlan);
				getJdbcConn();
				SimpleDateFormat formater = new SimpleDateFormat();  
				Connection conn=null;  
				String sql = "";
				String tele_boh = "choice7";
				if("tele".equals(tele_boh)){
					sql = "select A.FIRMID AS FIRM,C.SFT,A.ITEM,A.UNIT,B.PITCODE AS ITCODE,B.PDES AS DES,SUM(A.CNT) AS CNT ";
					sql += "  from FD_ORDR A,PUBITEM B,FD_FOLIO C  where  A.FIRMID=C.FIRMID AND C.Firmid = '";
					sql += firmId;
					sql += "' and (A.Item=B.pubItem) and (A.Code=C.Serial) AND A.VOID<>'Y' and (A.REFUND=0)";
					sql += " And TO_DATE(A.DAT,'YYYY-MM-DD') >= to_Date('";
					formater.applyPattern("yyyy-MM-dd");  
					sql += formater.format(bdate);
					sql += "', 'YYYY-MM-DD') And TO_DATE(A.DAT,'YYYY-MM-DD') <= to_Date('";
					sql += formater.format(edate);
					sql += "', 'YYYY-MM-DD')";
					sql += "Group by  A.FIRMID,C.SFT,A.ITEM,A.UNIT,B.pITCODE,B.pDES order by  A.FIRMID,itcode,C.SFT,A.ITEM";
				}else if("pos".equals(tele_boh)){
					sql = "select A.FIRMID AS FIRM,a.SFT,A.ITEM,A.UNIT,a.itcode,a.des,SUM(A.CNT) AS CNT ";
					sql += "  from view_itemsale_dat A where  a.Firmid= '";
					sql += firmId;
					sql += " And A.DAT >= to_Date('";
					formater.applyPattern("yyyy-MM-dd");  
					sql += formater.format(bdate);
					sql += "', 'YYYY-MM-DD') And A.DAT <= to_Date('";
					sql += formater.format(edate);
					sql += "', 'YYYY-MM-DD')";
					sql += " Group by A.DAT,A.SFT,A.ITEM,A.UNIT,A.ITCODE,A.DES order by A.DAT,A.SFT,A.ITEM";
				}else if ("choice7".equals(tele_boh)){
					sql = "SELECT a.vscode as firm,B.ICHANGETABLE AS SFT, A.VPCODE AS ITEM,A.NUNITCUR AS UNIT,A.VPCODE AS ITCODE,A.VPNAME AS DES,SUM(A.NCOUNT) AS CNT ";
					sql += " from CBOH_ORDR_3CH A,cboh_folio_3ch B  WHERE a.vbcode=b.vbcode AND b.vscode = '";
					sql += firmId;
					sql += "' And TO_DATE(B.DWORKDATE,'YYYY-MM-DD') >= to_Date('";
					formater.applyPattern("yyyy-MM-dd");  
					sql += formater.format(bdate);
					sql += "', 'YYYY-MM-DD') And TO_DATE(B.DWORKDATE,'YYYY-MM-DD') <= to_Date('";
					sql += formater.format(edate);
					sql += "', 'YYYY-MM-DD')";
					sql += "Group by a.vscode, B.ICHANGETABLE,A.VPCODE,A.nUNITCUR, A.VPCODE , A.VPNAME  ORDER by A.VPCODE, B.ICHANGETABLE,A.VPCODE";
				}

				Class.forName(driver);
				conn=DriverManager.getConnection(url, uname, password);
				PreparedStatement ptmt=conn.prepareStatement(sql);
				ResultSet rs=ptmt.executeQuery(); 
				List<PosItemPlan> posItemPlanList_ = new ArrayList<PosItemPlan>();//新表
				DecimalFormat df = new DecimalFormat("#.00");
				if (posItemPlanList.size()>0) {//菜品预估表里有值
					int i = 0; // i代表预估表里的第i条数据					
					int j = -1; // j代表新表里的第i条数据
					int itcode = Integer.parseInt(posItemPlanList.get(i).getItcode());//预估表菜品code
					int code = 0;
					while(rs.next()) {//循环销售数据
						boolean first = true;
						int temp = Integer.parseInt(rs.getString("itcode"));
						int sft  = Integer.parseInt(rs.getString("sft")); //班次;
						double cnt = Double.parseDouble(rs.getString("cnt"));   //某道菜点击次数
						String firm  = rs.getString("firm"); //分店;
						while (first) {//true代表销售数据尚未录入计算
							if (code == 0 && temp >= itcode) { //第一个菜品，有预估、没有销售数据的情况
								j++;
								posItemPlanList_.add(posItemPlanList.get(i));
								posItemPlanList_.get(j).setCnt1act(0);
								posItemPlanList_.get(j).setLv1(0);
								posItemPlanList_.get(j).setLv1old(0);
								posItemPlanList_.get(j).setCnt2act(0);
								posItemPlanList_.get(j).setLv2(0);
								posItemPlanList_.get(j).setLv2old(0);
								posItemPlanList_.get(j).setCnt3act(0);
								posItemPlanList_.get(j).setLv3(0);
								posItemPlanList_.get(j).setLv3old(0);
								posItemPlanList_.get(j).setCnt4act(0);
								posItemPlanList_.get(j).setLv4(0);
								posItemPlanList_.get(j).setLv4old(0);
								code = Integer.parseInt(posItemPlanList_.get(j).getItcode());
							}else if (code == 0 && temp < itcode) { //第一个菜品，没有预估、有销售数据的情况
								j++;
								
								posItemPlan = new PosItemPlan();
								posItemPlan.setFirm(firm);
								posItemPlan.setItcode(rs.getString("itcode"));
								posItemPlan.setItdes(rs.getString("des"));
								posItemPlan.setItunit(rs.getString("unit"));
								posItemPlan.setCnt1(0);
								posItemPlan.setCnt1old(0);
								posItemPlan.setCnt1act(0);
								posItemPlan.setLv1(0);
								posItemPlan.setLv1old(0);
								posItemPlan.setCnt2(0);
								posItemPlan.setCnt2old(0);
								posItemPlan.setCnt2act(0);
								posItemPlan.setLv2(0);
								posItemPlan.setLv2old(0);
								posItemPlan.setCnt3(0);
								posItemPlan.setCnt3old(0);
								posItemPlan.setCnt3act(0);
								posItemPlan.setLv3(0);
								posItemPlan.setLv3old(0);
								posItemPlan.setCnt4(0);
								posItemPlan.setCnt4old(0);
								posItemPlan.setCnt4act(0);
								posItemPlan.setLv4(0);
								posItemPlan.setLv4old(0);
								first = false;
								if (sft==1) {
									posItemPlan.setCnt1act(cnt);
									posItemPlan.setLv1((posItemPlan.getCnt1()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act());
									posItemPlan.setLv1old((posItemPlan.getCnt1old()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act());
								}else if (sft==2) {
									posItemPlan.setCnt2act(cnt);
									posItemPlan.setLv2((posItemPlan.getCnt2()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act());
									posItemPlan.setLv2old((posItemPlan.getCnt2old()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act());
								}else if (sft==3) {
									posItemPlan.setCnt3act(cnt);
									posItemPlan.setLv3((posItemPlan.getCnt3()-posItemPlan.getCnt3act())*100/posItemPlan.getCnt3act());
									posItemPlan.setLv3old((posItemPlan.getCnt3old()-posItemPlan.getCnt3act())*100/posItemPlan.getCnt3act());
								}else if (sft==4) {
									posItemPlan.setCnt4act(cnt);
									posItemPlan.setLv4((posItemPlan.getCnt4()-posItemPlan.getCnt4act())*100/posItemPlan.getCnt4act());
									posItemPlan.setLv4old((posItemPlan.getCnt4old()-posItemPlan.getCnt4act())*100/posItemPlan.getCnt4act());
								}
								posItemPlanList_.add(posItemPlan);
								code = Integer.parseInt(posItemPlanList_.get(j).getItcode());
							}else if(temp==code) {//销售数据的菜品code和新表中菜品code相等
								first = false; //代表该菜品有销售数据，销售数据录入计算
								if (sft==1) {
									posItemPlanList_.get(j).setCnt1act(cnt);
									posItemPlanList_.get(j).setLv1((posItemPlanList_.get(j).getCnt1()-posItemPlanList_.get(j).getCnt1act())*100/posItemPlanList_.get(j).getCnt1act());
									posItemPlanList_.get(j).setLv1old((posItemPlanList_.get(j).getCnt1old()-posItemPlanList_.get(j).getCnt1act())*100/posItemPlanList_.get(j).getCnt1act());
								}else if (sft==2) {
									posItemPlanList_.get(j).setCnt2act(cnt);
									posItemPlanList_.get(j).setLv2((posItemPlanList_.get(j).getCnt2()-posItemPlanList_.get(j).getCnt2act())*100/posItemPlanList_.get(j).getCnt2act());
									posItemPlanList_.get(j).setLv2old((posItemPlanList_.get(j).getCnt2old()-posItemPlanList_.get(j).getCnt2act())*100/posItemPlanList_.get(j).getCnt2act());
								}else if (sft==3) {
									posItemPlanList_.get(j).setCnt3act(cnt);
									posItemPlanList_.get(j).setLv3((posItemPlanList_.get(j).getCnt3()-posItemPlanList_.get(j).getCnt3act())*100/posItemPlanList_.get(j).getCnt3act());
									posItemPlanList_.get(j).setLv3old((posItemPlanList_.get(j).getCnt3old()-posItemPlanList_.get(j).getCnt3act())*100/posItemPlanList_.get(j).getCnt3act());
								}else if (sft==4) {
									posItemPlanList_.get(j).setCnt4act(cnt);
									posItemPlanList_.get(j).setLv4((posItemPlanList_.get(j).getCnt4()-posItemPlanList_.get(j).getCnt4act())*100/posItemPlanList_.get(j).getCnt4act());
									posItemPlanList_.get(j).setLv4old((posItemPlanList_.get(j).getCnt4old()-posItemPlanList_.get(j).getCnt4act())*100/posItemPlanList_.get(j).getCnt4act());
								}
							}else { //下一道菜
								//计算合计
 								posItemPlanList_.get(j).setTotal(Double.parseDouble(df.format((posItemPlanList_.get(j).getCnt1()+posItemPlanList_.get(j).getCnt2()+posItemPlanList_.get(j).getCnt3()+posItemPlanList_.get(j).getCnt4()))));
								posItemPlanList_.get(j).setTotalact(Double.parseDouble(df.format((posItemPlanList_.get(j).getCnt1act()+posItemPlanList_.get(j).getCnt2act()+posItemPlanList_.get(j).getCnt3act()+posItemPlanList_.get(j).getCnt4act()))));
								posItemPlanList_.get(j).setTotallv(Double.parseDouble(df.format((posItemPlanList_.get(j).getLv1()+posItemPlanList_.get(j).getLv2()+posItemPlanList_.get(j).getLv3()+posItemPlanList_.get(j).getLv4()))));
								posItemPlanList_.get(j).setTotallvold(Double.parseDouble(df.format((posItemPlanList_.get(j).getLv1old()+posItemPlanList_.get(j).getLv2old()+posItemPlanList_.get(j).getLv3old()+posItemPlanList_.get(j).getLv4old()))));
								posItemPlanList_.get(j).setTotalold(Double.parseDouble(df.format((posItemPlanList_.get(j).getCnt1old()+posItemPlanList_.get(j).getCnt2old()+posItemPlanList_.get(j).getCnt3old()+posItemPlanList_.get(j).getCnt4old()))));

								/*i++;*/
								if (i < posItemPlanList.size()-1) {
									// 编辑下一条预估数据，初值设为零
									itcode = Integer.parseInt(posItemPlanList.get(i+1).getItcode());
									
									if (temp < itcode) {//销售数据加入新表，预估值设为0
										
										posItemPlan = new PosItemPlan();
										posItemPlan.setFirm(firm);
										posItemPlan.setItcode(rs.getString("itcode"));
										posItemPlan.setItdes(rs.getString("des"));
										posItemPlan.setItunit(rs.getString("unit"));
										posItemPlan.setCnt1(0);
										posItemPlan.setCnt1old(0);
										posItemPlan.setCnt1act(0);
										posItemPlan.setLv1(0);
										posItemPlan.setLv1old(0);
										posItemPlan.setCnt2(0);
										posItemPlan.setCnt2old(0);
										posItemPlan.setCnt2act(0);
										posItemPlan.setLv2(0);
										posItemPlan.setLv2old(0);
										posItemPlan.setCnt3(0);
										posItemPlan.setCnt3old(0);
										posItemPlan.setCnt3act(0);
										posItemPlan.setLv3(0);
										posItemPlan.setLv3old(0);
										posItemPlan.setCnt4(0);
										posItemPlan.setCnt4old(0);
										posItemPlan.setCnt4act(0);
										posItemPlan.setLv4(0);
										first=false;
										if (sft==1) {
											posItemPlan.setCnt1act(cnt);
											posItemPlan.setLv1(Double.parseDouble(df.format(((posItemPlan.getCnt1()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act())+"")));
											posItemPlan.setLv1old(Double.parseDouble(df.format(((posItemPlan.getCnt1old()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act())+"")));
										}else if (sft==2) {
											posItemPlan.setCnt2act(cnt);
											posItemPlan.setLv2(Double.parseDouble(df.format(((posItemPlan.getCnt2()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act())+"")));
											posItemPlan.setLv2old(Double.parseDouble(df.format(((posItemPlan.getCnt2old()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act())+"")));
										}else if (sft==3) {
											posItemPlan.setCnt3act(cnt);
											posItemPlan.setLv3(Double.parseDouble(df.format(((posItemPlan.getCnt3()-posItemPlan.getCnt3act())*100/posItemPlan.getCnt3act())+"")));
											posItemPlan.setLv3old(Double.parseDouble(df.format(((posItemPlan.getCnt3old()-posItemPlan.getCnt3act())*100/posItemPlan.getCnt3act())+"")));
										}else if (sft==4) {
											posItemPlan.setCnt4act(cnt);
											posItemPlan.setLv4(Double.parseDouble(df.format(((posItemPlan.getCnt4()-posItemPlan.getCnt4act())*100/posItemPlan.getCnt4act())+"")));
											posItemPlan.setLv4old(Double.parseDouble(df.format(((posItemPlan.getCnt4old()-posItemPlan.getCnt4act())*100/posItemPlan.getCnt4act())+"")));
										}
										posItemPlanList_.add(posItemPlan);
										j++;
										code = Integer.parseInt(posItemPlanList_.get(j).getItcode());
									}else if (temp >= itcode) {//把预估表中的数据加入到新表
										i++;
										posItemPlanList_.add(posItemPlanList.get(i));
										j++;
										code = itcode;
										// 新表设初值0
										posItemPlanList_.get(j).setCnt1act(0);
										posItemPlanList_.get(j).setLv1(0);
										posItemPlanList_.get(j).setLv1old(0);
										posItemPlanList_.get(j).setCnt2act(0);
										posItemPlanList_.get(j).setLv2(0);
										posItemPlanList_.get(j).setLv2old(0);
										posItemPlanList_.get(j).setCnt3act(0);
										posItemPlanList_.get(j).setLv3(0);
										posItemPlanList_.get(j).setLv3old(0);
										posItemPlanList_.get(j).setCnt4act(0);
										posItemPlanList_.get(j).setLv4(0);
										posItemPlanList_.get(j).setLv4old(0);
									}
								}else if (i>=posItemPlanList.size()-1) {
									//这是实际销售菜品在预估数据表中不存在的情况，并且此时销售菜品编号大于预估表中最大菜品编号。
									
									j++;
									posItemPlan = new PosItemPlan();
									posItemPlan.setFirm(firm);
									posItemPlan.setItcode(rs.getString("itcode"));
									posItemPlan.setItdes(rs.getString("des"));
									posItemPlan.setItunit(rs.getString("unit"));
									posItemPlan.setCnt1(0);
									posItemPlan.setCnt1old(0);
									posItemPlan.setCnt1act(0);
									posItemPlan.setLv1(0);
									posItemPlan.setLv1old(0);
									posItemPlan.setCnt2(0);
									posItemPlan.setCnt2old(0);
									posItemPlan.setCnt2act(0);
									posItemPlan.setLv2(0);
									posItemPlan.setLv2old(0);
									posItemPlan.setCnt3(0);
									posItemPlan.setCnt3old(0);
									posItemPlan.setCnt3act(0);
									posItemPlan.setLv3(0);
									posItemPlan.setLv3old(0);
									posItemPlan.setCnt4(0);
									posItemPlan.setCnt4old(0);
									posItemPlan.setCnt4act(0);
									posItemPlan.setLv4(0);
									posItemPlan.setLv4old(0);
									
									first=false;
									if (sft==1) {
										posItemPlan.setCnt1act(cnt);
										posItemPlan.setLv1((posItemPlan.getCnt1()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act());
										posItemPlan.setLv1old((posItemPlan.getCnt1old()-posItemPlan.getCnt1act())*100/posItemPlan.getCnt1act());
									}else if (sft==2) {
										posItemPlan.setCnt2act(cnt);
										posItemPlan.setLv2((posItemPlan.getCnt2()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act());
										posItemPlan.setLv2old((posItemPlan.getCnt2old()-posItemPlan.getCnt2act())*100/posItemPlan.getCnt2act());
									}else if (sft==3) {
										posItemPlan.setCnt3act(cnt);
										posItemPlan.setLv3((posItemPlan.getCnt3()-posItemPlanList.get(i).getCnt3act())*100/posItemPlan.getCnt3act());
										posItemPlan.setLv3old((posItemPlan.getCnt3old()-posItemPlan.getCnt3act())*100/posItemPlan.getCnt3act());
									}else if (sft==4) {
										posItemPlan.setCnt4act(cnt);
										posItemPlan.setLv4((posItemPlan.getCnt4()-posItemPlanList.get(i).getCnt4act())*100/posItemPlan.getCnt4act());
										posItemPlan.setLv4old((posItemPlan.getCnt4old()-posItemPlan.getCnt4act())*100/posItemPlan.getCnt4act());
									}
									posItemPlanList_.add(posItemPlan);	
									code = Integer.parseInt(posItemPlanList_.get(j).getItcode());
								}
							}
						}
					}
					// 无销售数据，有预估数据时
					while (i < posItemPlanList.size()-1) {
						// 编辑下一条预估数据，初值设为零
						posItemPlanList_.add(posItemPlanList.get(i));
						// 新表设初值0
						j++;
						posItemPlanList_.get(j).setCnt1act(0);
						posItemPlanList_.get(j).setLv1(0);
						posItemPlanList_.get(j).setLv1old(0);
						posItemPlanList_.get(j).setCnt2act(0);
						posItemPlanList_.get(j).setLv2(0);
						posItemPlanList_.get(j).setLv2old(0);
						posItemPlanList_.get(j).setCnt3act(0);
						posItemPlanList_.get(j).setLv3(0);
						posItemPlanList_.get(j).setLv3old(0);
						posItemPlanList_.get(j).setCnt4act(0);
						posItemPlanList_.get(j).setLv4(0);
						posItemPlanList_.get(j).setLv4old(0);
						//计算合计
						posItemPlanList_.get(j).setTotal(Double.parseDouble(df.format(posItemPlanList_.get(j).getCnt1()+posItemPlanList_.get(j).getCnt2()+posItemPlanList_.get(j).getCnt3()+posItemPlanList_.get(j).getCnt4())));
						posItemPlanList_.get(j).setTotalact(0);
						posItemPlanList_.get(j).setTotallv(0);
						posItemPlanList_.get(j).setTotallvold(0);
						posItemPlanList_.get(j).setTotalold(Double.parseDouble(df.format(posItemPlanList_.get(j).getCnt1old()+posItemPlanList_.get(j).getCnt2old()+posItemPlanList_.get(j).getCnt3old()+posItemPlanList_.get(j).getCnt4old())));
						
						i++;
					}
				}
				conn.close(); 
				return posItemPlanList_;
			}catch(Exception e){  
				log.error(e);
				throw new CRUDException(e);  
			} 
		}
		
		//导出营业预估实际对比Excel 
		public boolean exportCostCmp(OutputStream os,List<PosItemPlan> list) {   
			WritableWorkbook workBook = null;
			try {
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("Sheet1", 0);
	            sheet.addCell(new Label(0,0,"菜品预估实际对比"));
	            //设置表格表头
	            sheet.addCell(new Label(0, 2, "分店"));                   
	            sheet.addCell(new Label(1, 2, "编码"));                  
	            sheet.addCell(new Label(2, 2, "名称"));                
	            sheet.addCell(new Label(3, 2, "单位"));
	            
	            sheet.addCell(new Label(4, 2, "一班|预估"));       
	            sheet.addCell(new Label(5, 2, "一班|调整"));       
	            sheet.addCell(new Label(6, 2, "一班|实际"));       
	            sheet.addCell(new Label(7, 2, "一班|预估差异率")); 
	            sheet.addCell(new Label(8, 2, "一班|调整差异率"));  
	            
	            sheet.addCell(new Label(9, 2, "二班|预估"));       
	            sheet.addCell(new Label(10,2, "二班|调整"));       
	            sheet.addCell(new Label(11, 2, "二班|实际"));      
	            sheet.addCell(new Label(12, 2, "二班|预估差异率"));
	            sheet.addCell(new Label(13, 2, "二班|调整差异率"));  
	            
	            sheet.addCell(new Label(14, 2, "三班|预估"));      
	            sheet.addCell(new Label(15, 2, "三班|调整"));      
	            sheet.addCell(new Label(16, 2, "三班|实际"));                                                  
	            sheet.addCell(new Label(17, 2, "三班|预估差异率"));                                                       
	            sheet.addCell(new Label(18, 2, "三班|调整差异率"));   
	            
	            sheet.addCell(new Label(19, 2, "四班|预估"));                                                       
	            sheet.addCell(new Label(20, 2, "四班|调整"));      
	            sheet.addCell(new Label(21, 2, "四班|实际"));                                                               
	            sheet.addCell(new Label(22, 2, "四班|预估差异率"));
	            sheet.addCell(new Label(23, 2, "四班|调整差异率"));  
	            
	            sheet.addCell(new Label(24, 2, "合计|预估"));      
	            sheet.addCell(new Label(25, 2, "合计|调整"));      
	            sheet.addCell(new Label(26, 2, "合计|实际"));      
	            sheet.addCell(new Label(27, 2, "合计|预估差异率"));
	            sheet.addCell(new Label(28, 2, "合计|调整差异率"));
				
	            sheet.mergeCells(0, 0, 28, 1);
	            //遍历list填充表格内容
	            for(int i=0;i<list.size();i++) {
	            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm()));
	            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));
	            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));
	            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));
	            	
	            	sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getCnt1old())));
	            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getCnt1())));
	            	sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getCnt1act())));
	            	sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getLv1old())));
	            	sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getLv1())));
	            	
	            	sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getCnt2old())));
	            	sheet.addCell(new Label(10,i+3, String.valueOf(list.get(i).getCnt2())));
	            	sheet.addCell(new Label(11, i+3, String.valueOf(list.get(i).getCnt2act())));
	            	sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getLv2old())));
	            	sheet.addCell(new Label(13, i+3, String.valueOf(list.get(i).getLv2())));
	            	
	            	sheet.addCell(new Label(14, i+3, String.valueOf(list.get(i).getCnt3old())));
	            	sheet.addCell(new Label(15, i+3, String.valueOf(list.get(i).getCnt3())));
	            	sheet.addCell(new Label(16, i+3, String.valueOf(list.get(i).getCnt3act())));
	            	sheet.addCell(new Label(17, i+3, String.valueOf(list.get(i).getLv3old())));
	            	sheet.addCell(new Label(18, i+3, String.valueOf(list.get(i).getLv3())));
	            	
	            	sheet.addCell(new Label(19, i+3, String.valueOf(list.get(i).getCnt4old())));
	            	sheet.addCell(new Label(20, i+3, String.valueOf(list.get(i).getCnt4())));
	            	sheet.addCell(new Label(21, i+3, String.valueOf(list.get(i).getCnt4act())));
	            	sheet.addCell(new Label(22, i+3, String.valueOf(list.get(i).getLv4old())));
	            	sheet.addCell(new Label(23, i+3, String.valueOf(list.get(i).getLv4())));
	            	
	            	sheet.addCell(new Label(24, i+3, String.valueOf(list.get(i).getTotalold())));
	            	sheet.addCell(new Label(25, i+3, String.valueOf(list.get(i).getTotal())));
	            	sheet.addCell(new Label(26, i+3, String.valueOf(list.get(i).getTotalact())));
	            	sheet.addCell(new Label(27, i+3, String.valueOf(list.get(i).getTotallv())));
	            	sheet.addCell(new Label(28, i+3, String.valueOf(list.get(i).getTotallvold())));
		    		}
	            workBook.write();
	            os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		
		/********************************************************菜品预估实际对比end*************************************************/
		

		/****************************************************菜品预估沽清对比start*************************************************/

		/**
		 * 获取某班次各道菜的信息
		 * @return
		 * @throws CRUDException
		 */
		public List<PosItemPlan> getCmpList(PosItemPlan posItemPlan) throws CRUDException { 
			try {
				String firmId = posItemPlan.getFirm();
				Date bdate = posItemPlan.getDat();
				List<PosItemPlan> posItemPlanList = forecastMapper.findPosItemPlan2(posItemPlan);
				getJdbcConn();
				Connection conn=null;  
				
				String sql = "";
				String tele_boh = "choice7";
				if("pos".equals(tele_boh)){
					sql = "select A.FIRMID AS FIRM,a.SFT,A.ITEM,A.UNIT,a.itcode,a.des,SUM(A.CNT) AS CNTact,max(b.tjtime) as tim ";
					sql += " from view_itemsale_dat A  , kpartitmes b where a.firmid=b.scode and a.Firmid = '";
					sql += firmId;
					sql += "' AND a.SFT = '";
					sql += posItemPlan.getSft();
//					sql += "' and (A.Item=B.pubItem) and (A.Code=C.Serial) AND A.VOID<>'Y' and (A.REFUND=0)";
					sql += "' And A.DAT = '";
					sql += new SimpleDateFormat("yyyy-MM-dd").format(bdate);
					sql += "' Group by A.DAT,A.SFT,A.ITEM,A.UNIT,A.ITCODE,A.DES order by A.DAT,A.SFT,A.ITEM";
				}else if("tele".equals(tele_boh)){
					sql = "select A.FIRMID AS FIRM,F.FIRMDES AS FIRMNM,A.ITEM,A.UNIT,B.PITCODE AS ITCODE,B.PDES AS DES,SUM(A.CNT) AS CNTact,Max(A.TIM) AS TIM ";
					sql += "from FD_ORDR A,PUBITEM B,FD_FOLIO C ,FIRM F where  A.FIRMID=F.FIRMID AND A.FIRMID=C.FIRMID AND C.Firmid2 = '";
					sql += firmId;
					sql += "' AND C.SFT = '";
					sql += posItemPlan.getSft();
					sql += "' and (A.Item=B.pubItem) and (A.Code=C.Serial) AND A.VOID<>'Y' and (A.REFUND=0)";
					sql += " And A.DAT = '";
					sql += new SimpleDateFormat("yyyy-MM-dd").format(bdate);
					sql += "' Group by A.FIRMID,F.FIRMDES,A.ITEM,A.UNIT,B.PITCODE,B.PDES order by A.FIRMID,A.ITEM";
				}else if ("choice7".equals(tele_boh)){
					sql = "SELECT a.vscode as firm,c.vname as firmnm, A.VPCODE AS ITEM,A.NUNITCUR AS UNIT,A.VPCODE AS ITCODE,A.VPNAME AS DES,SUM(A.NCOUNT) AS CNTact,max(b.dbrtime) as tim ";
					sql += " from CBOH_ORDR_3CH A,cboh_folio_3ch B,cboh_store_3ch c  WHERE c.vcode=a.vbcode and a.vbcode=b.vbcode AND b.vscode = '";
					sql += firmId;
					sql += "' And STR_TO_DATE(B.DWORKDATE,'%Y-%m-%d') = STR_TO_DATE('";
					sql += new SimpleDateFormat("yyyy-MM-dd").format(bdate);
					sql += "','%Y-%m-%d') GROUP BY A.VSCODE, C.VNAME,B.ICHANGETABLE, A.VPCODE, A.NUNITCUR, A.VPCODE, A.VPNAME ORDER BY A.VPCODE, C.VNAME, B.ICHANGETABLE";
				}
				Class.forName(driver);
				conn=DriverManager.getConnection(url, uname, password);
				PreparedStatement ptmt=conn.prepareStatement(sql);
				ResultSet rs=ptmt.executeQuery(); 
				List<PosItemPlan> posItemPlanList_ = new ArrayList<PosItemPlan>();//新表
				if (posItemPlanList.size()>0) {//菜品预估表里有值
					int i = 0; // i代表预估表里的第i条数据					
					int j = -1; // j代表新表里的第i条数据
					int itcode = Integer.parseInt(posItemPlanList.get(i).getItcode());//预估表菜品code
					while(rs.next()) {//循环销售数据
						boolean first = true;
						int temp = Integer.parseInt(rs.getString("itcode"));
						double cntact = Double.valueOf(rs.getString("cntact"));   //某道菜点击次数
						String tim = rs.getString("tim");   //最后点单时间
						String firm  = rs.getString("firm"); //分店;
						while (first) {//true代表销售数据尚未录入计算
							if(temp==itcode) {//销售数据的菜品code和新表中菜品code相等
								first = false; //代表该菜品有销售数据，销售数据录入计算
								posItemPlanList_.add(posItemPlanList.get(i));
								j++;
								i++;
								posItemPlanList_.get(j).setCntact(cntact);
								posItemPlanList_.get(j).setTim(tim);
								if (i<posItemPlanList.size()) {
									itcode = Integer.parseInt(posItemPlanList.get(i).getItcode());
								}
								else {
									itcode=99999;
								}
								
							}else if(temp > itcode) { //代表该菜品无销售数据，销售数据录入0
								posItemPlanList_.add(posItemPlanList.get(i));
								j++;
								i++;
								posItemPlanList_.get(j).setCntact(0);
								posItemPlanList_.get(j).setTim("");
								posItemPlanList_.get(j).setCntcy(0-posItemPlanList_.get(j).getCntyg());
								if (i<posItemPlanList.size()) {
									itcode = Integer.parseInt(posItemPlanList.get(i).getItcode());
								}
								else {
									itcode=99999;
								}
							}else if (temp < itcode) { //代表该菜品无预估数据，预估数据录入0
								first = false; //代表该菜品有销售数据，销售数据录入计算
								posItemPlan = new PosItemPlan();
								posItemPlan.setFirmNm(rs.getString("firmnm"));
								posItemPlan.setFirm(firm);
								posItemPlan.setItcode(rs.getString("itcode"));
								posItemPlan.setItdes(rs.getString("des"));
								posItemPlan.setItunit(rs.getString("unit"));
								posItemPlan.setCntact(cntact);
								posItemPlan.setCntyg(0);
								posItemPlan.setTim(tim);
								posItemPlan.setCntcy(cntact);
								posItemPlanList_.add(posItemPlan);
								j++;
							}
						}
					}
					// 无销售数据，有预估数据时
					while (i < posItemPlanList.size()-1) {
						// 编辑下一条预估数据，初值设为零
						posItemPlanList_.add(posItemPlanList.get(i));
						// 新表设初值0
						j++;
						posItemPlanList_.get(j).setCntact(0);
						posItemPlanList_.get(j).setCntcy(0-posItemPlanList_.get(j).getCntyg());
						i++;
					}
				}
				conn.close(); 
				return posItemPlanList_;
			}catch(Exception e){  
				log.error(e);
				throw new CRUDException(e);  
			} 
		}
		
		//导出菜品预估沽清对比Excel 
		public boolean exportExcelcmp(OutputStream os,List<PosItemPlan> list) {   
			WritableWorkbook workBook = null;
			try {
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("Sheet1", 0);
	            sheet.addCell(new Label(0,0,"菜品预估沽清对比"));
	            //设置表格表头
	            sheet.addCell(new Label(0, 2, "分店")); 
	            sheet.addCell(new Label(1, 2, "菜品编码"));   
				sheet.addCell(new Label(2, 2, "菜品名称"));  
				sheet.addCell(new Label(3, 2, "单位"));   
				sheet.addCell(new Label(4, 2, "最后点单时间"));  
				sheet.addCell(new Label(5, 2, "总销量"));   
				sheet.addCell(new Label(6, 2, "计划量"));  
				sheet.addCell(new Label(7, 2, "差异量"));   
	            sheet.mergeCells(0, 0, 7, 1);
	            //遍历list填充表格内容
	            for(int i=0;i<list.size();i++) {
	            	sheet.addCell(new Label(0, i+3, list.get(i).getFirmNm()));
	            	sheet.addCell(new Label(1, i+3, list.get(i).getItcode()));
	            	sheet.addCell(new Label(2, i+3, list.get(i).getItdes()));
	            	sheet.addCell(new Label(3, i+3, list.get(i).getItunit()));
	            	sheet.addCell(new Label(4, i+3, String.valueOf(list.get(i).getTim())));
	            	sheet.addCell(new Label(5, i+3, String.valueOf(list.get(i).getCntact())));
		    		sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getCntyg())));
		    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getCntcy())));
		    		}
	            workBook.write();
	            os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		/********************************************************菜品预估沽清对比end*************************************************/
		
}