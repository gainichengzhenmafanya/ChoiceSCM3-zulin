package com.choice.scm.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.scm.domain.FirmCostAvg;
import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.SpCodeExPrice;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.BanChengPinPriceMapper;
import com.choice.scm.persistence.TeShuCaoZuoMapper;

@Service
public class BanChengPinPriceService {
	
	@Autowired
	private BanChengPinPriceMapper banChengPinPriceMapper;
	private final transient Log log = LogFactory.getLog(BanChengPinPriceService.class);

	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private PrdctbomService prdctbomService;
	@Autowired
	private TeShuCaoZuoMapper teShuCaoZuoMapper;
	/**
	 * 查询  中心费用
	 * @param firmCostAvg
	 * @return
	 */
	public List<FirmCostAvg> findFirmCostAvg(FirmCostAvg firmCostAvg){
		List<FirmCostAvg> list=banChengPinPriceMapper.findFirmCostAvg(firmCostAvg);
		if(list.size()>0){
			return list;
		}
		return banChengPinPriceMapper.findCodeDes();
	}
	
	/**
	 * 保存 中心费用
	 * @param session
	 * @param firmCostAvgs
	 * @return
	 */
	public String savefirmCpstAcg(HttpSession session,FirmCostAvg firmCostAvgs) {
		try {
			String year=DateFormat.getStringByDate(new Date(), "yyyy");
			banChengPinPriceMapper.delectFirmCostAvg(year, firmCostAvgs.getMonthh());
			for(FirmCostAvg firmCostAvg:firmCostAvgs.getFirmCostAvgList()){
				firmCostAvg.setAcct(session.getAttribute("ChoiceAcct").toString());
				firmCostAvg.setYearr(year);
				banChengPinPriceMapper.saveFirmCostAvg(firmCostAvg);
			}
			return "success";
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			return "error";
		}
	}
	
	/**
	 * 查询  加工工时 --及 主记录 
	 */
	public List<SpCodeExPrice> findSpCodeExPrice(SpCodeExPrice spCodeExPrice) throws CRUDException{
		try{
			String bdat=DateFormat.getStringByDate(new Date(), "yyyy")+"-"+spCodeExPrice.getMonthh()+"-"+"1";//拼接某月的第一天
			Date edats=DateFormat.getEndDayOfCurrMonth(DateFormat.getDateByString(bdat, "yyyy-MM-d"));//某月的最后一天
			spCodeExPrice.setBdat(DateFormat.getDateByString(bdat,"yyyy-MM-dd"));
			spCodeExPrice.setEdat(edats);
			return banChengPinPriceMapper.findSpCodeExPrice(spCodeExPrice);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存 加工工时 
	 * @param session
	 * @param supplyAcct
	 * @return
	 */
	public String saveExTim(String acct, List<SpCodeExPrice> SpCodeExPriceList) {
		try {
			String year=DateFormat.getStringByDate(new Date(), "yyyy");
			
//			String bdat=DateFormat.getStringByDate(new Date(), "yyyy")+"-"+SpCodeExPriceList.get(0).getMonthh()+"-"+"1";//拼接某月的第一天
//			Date edats=DateFormat.getEndDayOfCurrMonth(DateFormat.getDateByString(bdat, "yyyy-MM-d"));//某月的最后一天
			for(SpCodeExPrice spCodeExPrice:SpCodeExPriceList){
				spCodeExPrice.setAcct(acct);
				spCodeExPrice.setYearr(year);
				if (banChengPinPriceMapper.selectSpCodeExPrice(spCodeExPrice) != null) {
					banChengPinPriceMapper.updateSpCodeExPrice(spCodeExPrice);
				} else {
					banChengPinPriceMapper.saveSpCodeExPrice(spCodeExPrice);
				}
//				spCodeExPrice.setBdat(DateFormat.getDateByString(bdat,"yyyy-MM-dd"));
//				spCodeExPrice.setEdat(edats);
			}
			return "success";
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			return "error";
		}
	}
	
	/**
	 * 查询  费用
	 * @param firmCostAvg
	 * @return
	 */
	public List<FirmCostAvg> findSpexfeicost(FirmCostAvg firmCostAvg)throws CRUDException{
		try{
			return banChengPinPriceMapper.findSpexfeicost(firmCostAvg);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重计算半成品成本价格
	 * @param firmCostAvg
	 * @return
	 */
	public String updatecalculate(String acct, String month, String extimSum)throws CRUDException{
		String result ="";
		try{
			String  year=teShuCaoZuoMapper.findMain().getYearr();
			FirmCostAvg firmCostAvg = new FirmCostAvg();
			firmCostAvg.setAcct(acct);
			firmCostAvg.setYearr(year);
			firmCostAvg.setMonthh(month);
			List<FirmCostAvg> firmCostAvgList=banChengPinPriceMapper.findFirmCostAvg(firmCostAvg);//获取中心费用列表（m条费用）
			
			SpCodeExPrice spCodeExPrice = new SpCodeExPrice();
			spCodeExPrice.setAcct(acct);
			spCodeExPrice.setYearr(year);
			spCodeExPrice.setMonthh(month);
			String month_ =month.length()<2?"0"+month:month;
			String bdat=DateFormat.getStringByDate(new Date(), "yyyy")+"-"+month_+"-"+"01";//拼接某月的第一天
			Date edats=DateFormat.getEndDayOfCurrMonth(DateFormat.getDateByString(bdat, "yyyy-MM-dd"));//某月的最后一天
			spCodeExPrice.setBdat(DateFormat.getDateByString(bdat,"yyyy-MM-dd"));
			spCodeExPrice.setEdat(edats);
			List<SpCodeExPrice> spCodeExPriceList = banChengPinPriceMapper.findSpCodeExPrice(spCodeExPrice);//获取半成品费用列表
			
			Inventory inventory = new Inventory();
			inventory.setAcct(acct);
			inventory.setYearr(year);
			inventory.setMonth(month);
//			List<Inventory> listInventory = inventoryService.findAllInventoryByJGJ(inventory);//获取当月所有原材料用量
			List<Inventory> listInventory = inventoryService.findInventoryByJGJ(inventory);//获取加工间原材料实际耗用
			if(listInventory==null ||listInventory.size()==0){
				result= "2";//未做盘点
				return result;
			}
			//获取原材料价格--先进先出原则
			
			//查询本月生产半成品理论全部材料用量
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("bdat", bdat);
			map.put("edats", new SimpleDateFormat("yyyy-MM-dd").format(edats));
			map.put("acct", acct);
			System.out.println("-----"+map);
			List<Spcodeexm> listSpcodeexm = banChengPinPriceMapper.findSpcodeexmByMonth(map);//原材料根据bom获得的总用量
//			计算价格
			List<SupplyAcct> listPrice = banChengPinPriceMapper.findPriceBySupplyAcct(map);
			//根据每种
			for (int i=0;i<spCodeExPriceList.size();i++) {//对每项半成品进行循环
				String sp_code = spCodeExPriceList.get(i).getSupply().getSp_code();
				String positn = spCodeExPriceList.get(i).getSupply().getPositnex();
				Double amtSum = Double.valueOf(0);
				if (spCodeExPriceList.get(i).getAmount()!=0) {//半成品产品不为0时
					for (int j=0;j<firmCostAvgList.size();j++) {//往SpFeiCost表中添加或者更新数据，每个半成品每种费用对应一条数据
						firmCostAvgList.get(j).setItem(sp_code);
						firmCostAvgList.get(j).setFirm(positn);
						banChengPinPriceMapper.deleteSpexFeiCost(firmCostAvgList.get(j));//删除之前计算的结果
						BigDecimal amt = firmCostAvgList.get(j).getAmt();
						Double amtTmp = firmCostAvgList.get(j).getAmt().doubleValue()*spCodeExPriceList.get(i).getExtim()/Double.parseDouble(extimSum);//杂项费用
						firmCostAvgList.get(j).setAmt(new BigDecimal(amtTmp));
						banChengPinPriceMapper.insertSpexFeiCost(firmCostAvgList.get(j));//添加新计算结果
						firmCostAvgList.get(j).setAmt(amt);
						amtSum += amtTmp;
					}
					//获取bom
					Spcodeexm spcodeexm = new Spcodeexm();
					spcodeexm.setItem(spCodeExPriceList.get(i).getSupply().getSp_code());
					spcodeexm.setAcct(acct);
					List<Product> listBom = prdctbomService.findmaterial(spcodeexm);//该产品bom
					Double amt = 0.0;
					if(listBom!=null && listBom.size()>0){
						//删除半成品原料使用明细
						Map<String,Object> mapMx= new HashMap<String,Object>();
						mapMx.put("acct", acct);
						mapMx.put("yearr", year);
						mapMx.put("monthh", month);
						mapMx.put("item", spCodeExPriceList.get(i).getSupply().getSp_code());
						prdctbomService.deleteSpCodeExPriceCnt(mapMx);
						for(Product p:listBom){
							String spcode = p.getSupply().getSp_code();
							double cnt = spCodeExPriceList.get(i).getAmount();//半成品产量
							double price = this.getPriceBySp_code(spcode,listPrice);//本次入库材料平均价
							     //取得supplyacct 出库数据
								Map<String,Object> mapHY= new HashMap<String,Object>();
								mapHY.put("acct", acct);
								mapHY.put("yearr", year);
//								mapHY.put("monthh", month);
								mapHY.put("positn", positn);
								mapHY.put("sp_code", spcode);
								mapHY.put("bdat", bdat);
								mapHY.put("edats", new SimpleDateFormat("yyyy-MM-dd").format(edats));
								
								double cntsum=banChengPinPriceMapper.findsupplyCnt(mapHY);//根据加工间的出库 算
							
							
//							double cntsum = this.getCntSum(spcode,listInventory);//某原材料实际用量
							double cnts =this.getSumCntBySp_code(spcode,listSpcodeexm);//原材料根据BOM的总用量
							double cntSp =cnt * p.getCnt();//半成品该材料的bom用量
							//实际用量
							double yl = (cntSp/cnts)*cntsum;
							mapMx.put("sp_code", spcode);
							mapMx.put("cnt", yl);
							prdctbomService.saveSpCodeExPriceCnt(mapMx);
//							double priceAmt = (price*cntsum)*(cnt*p.getCnt()/cnts);//cnt*p.getCnt()/cnts  bom得出的比例
							double priceAmt = yl*price;//得出的比例
							System.out.println("半成品："+spcode+" - "+cnt+" 单价："+price+" 该原料总耗材："+cnts+" 原料成本" +priceAmt);
							amt +=priceAmt;
							
						}
					}
					
					spCodeExPriceList.get(i).setFeicost(amtSum);//费用成本
					amtSum =amtSum+amt ;
					spCodeExPriceList.get(i).setAmt(amtSum);//总费用
					spCodeExPriceList.get(i).setSpcost(amt);//原料成本
					spCodeExPriceList.get(i).setPrice(amtSum/spCodeExPriceList.get(i).getAmount());
					spCodeExPriceList.get(i).setAcct(acct);
					
					banChengPinPriceMapper.updateSpCodeExPrice(spCodeExPriceList.get(i));//更新SpCodeExPrice表中的价格和相关费用
				}
				
			//回填入库单    

				spCodeExPrice.setYearr(year);
				spCodeExPrice.setMonthh(month);
				spCodeExPrice.setAcct(acct);
				spCodeExPrice.setBdat(DateFormat.getDateByString(bdat,"yyyy-MM-dd"));
				spCodeExPrice.setEdat(edats);
				
				List<SpCodeExPrice> Listposint =banChengPinPriceMapper.findSpCodeExPricePositn(spCodeExPrice);
				for (int j = 0; j < Listposint.size(); j++) {//循环更新这个加工间的半成品价格
					spCodeExPrice.setPositnCode(Listposint.get(j).getPositnCode());
					banChengPinPriceMapper.updateEXPriceByPosint(spCodeExPrice);
				}
				
			}
			result = "0";//计算成功
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			result = "1";//计算失败
			throw new CRUDException(e);
		}finally{
			return result;
		}
		
	}
	private double getCntSum(String spcode, List<Inventory> listInventory) throws CRUDException{
		try {
			double cnt = 0.0;
			for(Inventory inv:listInventory){
				if(spcode.equals(inv.getSupply().getSp_code())){
					cnt = inv.getCntbla()-inv.getCnttrival();
				}
			}
			return cnt;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	private double getSumCntBySp_code(String spcode,List<Spcodeexm> listSpcodeexm) throws CRUDException{
		try {
			double cnt = 0.0;
			for(Spcodeexm sp:listSpcodeexm){
				if(spcode.equals(sp.getSp_code())){
					cnt = sp.getCnt();
				}
			}
			return cnt;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 查找原材料单价
	 * @param spcode
	 * @param listPrice
	 * @return
	 */
	private double getPriceBySp_code(String spcode, List<SupplyAcct> listPrice)  throws CRUDException{
		try {
			double price = 0.0;
			for(SupplyAcct su:listPrice){
				if(spcode.equals(su.getSp_code())){
					price = su.getPricesale();
				}
			}
			return price;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
