package com.choice.scm.service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.TeShuCaoZuoMapper;

@Service
public class AcctService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private TeShuCaoZuoMapper teShuCaoZuoMapper;
	
	private final transient static Log log = LogFactory.getLog(AcctService.class);
	
	/**
	 * 模糊查询 帐套信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Acct> findAcct(Acct acct) throws CRUDException{
		try{
//			return pageManager.selectPage(acct, page, AcctMapper.class.getName()+".findAcct");
			return acctMapper.findAcct(acct);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 增加帐套信息
	 */
	public void saveAcct(Acct acct) throws CRUDException{
		try{
			acctMapper.saveAcct(acct);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据ID查询帐套信息
	 */
	public Acct findAcctById(String code) throws CRUDException{
		try{
			return acctMapper.findAcctById(code);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询门店是否使用库存程序
	 */
	public Acct findYnkcFromAcct(String code) throws CRUDException{
		try{
			return acctMapper.findYnkcFromAcct(code);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据ID查询帐套是否存在
	 */
	@SuppressWarnings("finally")
	public String checkAcctById(String code) throws CRUDException{
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			Acct acct = acctMapper.findAcctById(code);
			
			resultMap.put("acct", acct);
			resultMap.put("test", "abc");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}finally{
			JSONObject rs = JSONObject.fromObject(resultMap);
			return rs.toString();
		}

	}
	
	/**
	 * 将当前默认改为非默认
	 */
	public void updateAcctWarn() throws CRUDException{
		try{
			acctMapper.updateAcctWarn();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改帐套信息
	 */
	public void updateAcct(Acct acct) throws CRUDException{
		try{
			acctMapper.updateAcct(acct);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 得到当前帐套以及会计月信息
	 */
	public Map<String,Object> findAcctMainInfo() throws CRUDException{
		Map<String,Object> resultMap = new HashMap<String, Object>();
		int month = 0;
		try{
//			Calendar calendar = Calendar.getInstance();
			//int year = calendar.get(Calendar.YEAR);
			int year = Integer.valueOf(teShuCaoZuoMapper.findMain().getYearr());
			MainInfo mainInfo = mainInfoMapper.findMainInfo(year);
			if(null == mainInfo){
				resultMap.put("code", -1);
				resultMap.put("serial", -1);
				resultMap.put("acct", -1);
				resultMap.put("des", "无");
				//month = getAccountMonth(null,null,new Date());
				//month = Integer.valueOf(mainInfo.getMonthh());
			}else{
				resultMap = acctMapper.findAcctMainInfo(year);
				if(resultMap==null ){
					resultMap = new HashMap<String, Object>();
				}
				//month = getAccountMonth(null,resultMap,new Date());
				month = Integer.valueOf(mainInfo.getMonthh());
			}
			
			resultMap.put("month", month);
			resultMap.put("year", year);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return resultMap;
	}
	
	/**
	 * 得到会计月信息
	 */
	public int getOnlyAccountMonth(Date maded) throws CRUDException{
		int month=0;
		try{
			maded = DateFormat.formatDate(maded, "yyyy-MM-dd");
			
//			Calendar calendar = Calendar.getInstance();
			int year = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			MainInfo mainInfo= mainInfoMapper.findMainInfo(year);
			month = getAccountMonth(mainInfo, null, maded);
			if(month==0){
				System.out.println("****系统出现严重问题！！！！！！----没有年结转----！！！！！！！！！！！！请先停止使用，联系辰森研发人员 ****");
				throw new  CRUDException("系统出现严重问题----没有年结转！！请先停止使用，联系辰森研发人员 ");
				
//				month = calendar.get(Calendar.MONTH)+1;
			}else{//如果查到的日期小于当前会计期，则不能审核
				Map<String,Object> resultMap = findAcctMainInfo();
				int currMonth = (Integer)resultMap.get("month");
				if(month < currMonth){
					throw new  CRUDException("不能操作当前会计期之前的数据！");
				}
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return month;
	}
	
	/***
	 * 得到会计月信息可以操作之前数据的  核减用20160421wjf
	 * @param maded
	 * @return
	 * @throws CRUDException
	 */
	public int getOnlyAccountMonth2(Date maded) throws CRUDException{
		int month=0;
		try{
			maded = DateFormat.formatDate(maded, "yyyy-MM-dd");
			
			int year = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			MainInfo mainInfo= mainInfoMapper.findMainInfo(year);
			month = getAccountMonth(mainInfo, null, maded);
			if(month==0){
				System.out.println("****系统出现严重问题！！！！！！----没有年结转----！！！！！！！！！！！！请先停止使用，联系辰森研发人员 ****");
				throw new  CRUDException("系统出现严重问题----没有年结转！！请先停止使用，联系辰森研发人员 ");
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return month;
	}
	
	/**
	 * 得到当前时间的 开始时间结束时间       开始时间用   bdat1   结束时间用 edat1   接收
	 */
	public MainInfo getmainInfo(Date maded) throws CRUDException{
		int month=0;
		try{
			maded = DateFormat.formatDate(maded, "yyyy-MM-dd");
			int year = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			MainInfo mainInfo= mainInfoMapper.findMainInfo(year);
			month = Integer.parseInt(mainInfo.getMonthh());
			if(month==0){
				System.out.println("****系统出现严重问题！！！！！！----没有年结转----！！！！！！！！！！！！请先停止使用，联系辰森研发人员 ****");
				throw new  CRUDException("系统出现严重问题----没有年结转！！请先停止使用，联系辰森研发人员 ");
				
//				month = calendar.get(Calendar.MONTH)+1;
			}
			MainInfo  mainInfodat=new MainInfo();
			Date bdate=new Date();
			Date edate=new Date();
			switch(month){
			case 1  :  
				  bdate=mainInfo.getBdat1();
				  edate=mainInfo.getEdat1();
				  break;
			case 2  :  
				bdate=mainInfo.getBdat2();
				edate=mainInfo.getEdat2();
				  break;
			case 3  :  
				bdate=mainInfo.getBdat3();
				edate=mainInfo.getEdat3();
				 break;
			case 4  :  
				bdate=mainInfo.getBdat4();
				edate=mainInfo.getEdat4();
				 break;
			case 5  :  
				bdate=mainInfo.getBdat5();
				edate=mainInfo.getEdat5();
				break;
			case 6  :  
				bdate=mainInfo.getBdat6();
				edate=mainInfo.getEdat6();
				break;
			case 7  :  
				bdate=mainInfo.getBdat7();
				edate=mainInfo.getEdat7();
				break;
			case 8  :  
				bdate=mainInfo.getBdat8();
				edate=mainInfo.getEdat8();
				break;
			case 9  :  
				bdate=mainInfo.getBdat9();
				edate=mainInfo.getEdat9();
				break;
			case 10  :  
				bdate=mainInfo.getBdat10();
				edate=mainInfo.getEdat10();
				break;
			case 11  :  
				bdate=mainInfo.getBdat11();
				edate=mainInfo.getEdat11();
				break;
			case 12  :  
				bdate=mainInfo.getBdat12();
				edate=mainInfo.getEdat12();		  
				break;
			}
			mainInfodat.setBdat1(bdate);//当前会计月的开始天
			mainInfodat.setEdat1(edate);//当前会计月的最后天
			mainInfodat.setMonthh(String.valueOf(month));
			return mainInfodat;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 得到会计月
	 * @param mainInfo 所有会计月的信息
	 * @param preMap 所有会计月的信息
	 * @param nowDate 日期
	 * @return 该日期属于哪个会计月
	 */
	
	public int getAccountMonth(MainInfo mainInfo,Map<String,Object> preMap,Date nowDate)throws CRUDException{
		int month = 0;
		if(null!=mainInfo){
			if(nowDate.getTime()>=mainInfo.getBdat1().getTime() && nowDate.getTime()<=mainInfo.getEdat1().getTime()){
				month=1;
			}else if(nowDate.getTime()>=mainInfo.getBdat2().getTime() && nowDate.getTime()<=mainInfo.getEdat2().getTime()){
				month=2;
			}else if(nowDate.getTime()>=mainInfo.getBdat3().getTime() && nowDate.getTime()<=mainInfo.getEdat3().getTime()){
				month=3;
			}else if(nowDate.getTime()>=mainInfo.getBdat4().getTime() && nowDate.getTime()<=mainInfo.getEdat4().getTime()){
				month=4;
			}else if(nowDate.getTime()>=mainInfo.getBdat5().getTime() && nowDate.getTime()<=mainInfo.getEdat5().getTime()){
				month=5;
			}else if(nowDate.getTime()>=mainInfo.getBdat6().getTime() && nowDate.getTime()<=mainInfo.getEdat6().getTime()){
				month=6;
			}else if(nowDate.getTime()>=mainInfo.getBdat7().getTime() && nowDate.getTime()<=mainInfo.getEdat7().getTime()){
				month=7;
			}else if(nowDate.getTime()>=mainInfo.getBdat8().getTime() && nowDate.getTime()<=mainInfo.getEdat8().getTime()){
				month=8;
			}else if(nowDate.getTime()>=mainInfo.getBdat9().getTime() && nowDate.getTime()<=mainInfo.getEdat9().getTime()){
				month=9;
			}else if(nowDate.getTime()>=mainInfo.getBdat10().getTime() && nowDate.getTime()<=mainInfo.getEdat10().getTime()){
				month=10;
			}else if(nowDate.getTime()>=mainInfo.getBdat11().getTime() && nowDate.getTime()<=mainInfo.getEdat11().getTime()){
				month=11;
			}else if(nowDate.getTime()>=mainInfo.getBdat12().getTime() && nowDate.getTime()<=mainInfo.getEdat12().getTime()){
				month=12;
			}
		}else if(null!=preMap){
			if(nowDate.getTime()>=getDateByObject(preMap.get("bdat1")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat1")).getTime()){
				month=1;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat2")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat2")).getTime()){
				month=2;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat3")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat3")).getTime()){
				month=3;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat4")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat4")).getTime()){
				month=4;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat5")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat5")).getTime()){
				month=5;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat6")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat6")).getTime()){
				month=6;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat7")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat7")).getTime()){
				month=7;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat8")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat8")).getTime()){
				month=8;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat9")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat9")).getTime()){
				month=9;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat10")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat10")).getTime()){
				month=10;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat11")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat11")).getTime()){
				month=11;
			}else if(nowDate.getTime()>=getDateByObject(preMap.get("bdat12")).getTime() && nowDate.getTime()<=getDateByObject(preMap.get("edat12")).getTime()){
				month=12;
			}
		}
		if(month==0){
			try{
				System.out.println("****系统出现严重问题！！！！！！----没有年结转----！！！！！！！！！！！！请先停止使用，联系辰森研发人员 ****");//xlh2015.1.2
				System.out.println(nowDate);
				throw new  CRUDException("系统出现严重问题----没有年结转！！请先停止使用，联系辰森研发人员 ");
			}catch(Exception e){
				log.error(e);
				throw new CRUDException(e);
			}
//			Calendar calendar = Calendar.getInstance();
//			month = calendar.get(Calendar.MONTH)+1;
		}
		return month;
	}
	
	/**
	 * 将Object类型转换成Date
	 * @param obj
	 * @return
	 */
	public Date getDateByObject(Object obj){
		Date resultDate = new Date();
		try{
			resultDate = (Date)obj;
		}catch(Exception e){
			log.equals(e);
		}
		return resultDate;
	}
	
	/**
	 * 生成月次
	 * @param acct
	 * @throws Exception
	 */
	public int checkAndUpdateMonth(String acct) throws Exception{  
    	String yearr = mainInfoMapper.findYearrList().get(0)+"";
    	int year = Integer.parseInt(yearr);
    	
    	if(mainInfoMapper.findMonthSet(Integer.parseInt(yearr))==0){//当前会计年 不存在 则添加
    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    		Map<String,Object> monthSet = new HashMap<String, Object>();
    		for (int i=1;i<13;i++){
    			monthSet.put("acct", acct);
    			monthSet.put("yearr", yearr);
    			monthSet.put("des", "");
    			monthSet.put("monthh", i);
    			if(i==1||i==3||i==5||i==7||i==8||i==10||i==12){
    				if(i<10){
    					monthSet.put("datf", dateFormat.parse(yearr+"-0"+i+"-01"));
    					monthSet.put("datt", dateFormat.parse(yearr+"-0"+i+"-31"));
    				}else{
    					monthSet.put("datf", dateFormat.parse(yearr+"-"+i+"-01"));
    					monthSet.put("datt", dateFormat.parse(yearr+"-"+i+"-31"));
    				}
    			}else if(i==2){
    				//判断是否为闰年
    		    	if((year%4 == 0)&&((year%100 != 0)|(year%400 == 0))){//闰年
    		    		monthSet.put("datf", dateFormat.parse(yearr+"-02"+"-01"));
            			monthSet.put("datt", dateFormat.parse(yearr+"-02"+"-29"));
    		    	}else{
    		    		monthSet.put("datf", dateFormat.parse(yearr+"-02"+"-01"));
            			monthSet.put("datt", dateFormat.parse(yearr+"-02"+"-28"));
    		    	}
    			}else{
    				if(i<10){
    					monthSet.put("datf", dateFormat.parse(yearr+"-0"+i+"-01"));
    					monthSet.put("datt", dateFormat.parse(yearr+"-0"+i+"-30"));
    				}else{
    					monthSet.put("datf", dateFormat.parse(yearr+"-"+i+"-01"));
    					monthSet.put("datt", dateFormat.parse(yearr+"-"+i+"-30"));
    				}
    			}
    			mainInfoMapper.insertMonthSet(monthSet);
    		}
    		return 1;
    	}else{
    		return 0;
    	}
    } 

	/***
	 * 根据日期查询所在会计年
	 * @param date
	 * @return
	 */
	public String findYearrByDate(Date date) throws CRUDException{
		try{
			return acctMapper.findYearrByDate(date);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}