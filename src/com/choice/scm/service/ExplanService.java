package com.choice.scm.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ExplanConstants;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Explan;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.ChkstodMapper;
import com.choice.scm.persistence.ChkstomMapper;
import com.choice.scm.persistence.ExplanMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.PrdctbomMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.service.report.WzYueChaxunService;
import com.choice.scm.util.CalChkNum;

@Service

public class ExplanService {
	
	@Autowired
	private ExplanMapper explanMapper;
	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private ChkstomMapper chkstomMapper;
	@Autowired
	private PrdctbomMapper prdctMapper;
	@Autowired
	private PageManager<Explan> pageManager;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Map<String,Object>> pageManager1;

	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private CalChkNum calChkNum;
	private final transient Log log = LogFactory.getLog(ExplanService.class);

	/**
	 * 查询所有调度
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<Explan> findAllExplan(Explan explan,String curStatus,String ids, Page page) throws CRUDException {
		try {
		//	HashMap<String, Object> map=new HashMap<String, Object>();
			if(null!=curStatus && !"".equals(curStatus) && !"".equals(curStatus) ){
				if("1".equals(curStatus)){//未报货
					explan.setYnsto("N");
				}else if("2".equals(curStatus)){//未分解
					explan.setYnex("N");
				}else if("3".equals(curStatus)){//完成但未入库
					explan.setYnex("Y");
					explan.setYnin("N");
				}else if("4".equals(curStatus)){//已入库
					explan.setYnin("Y");
				}
			}
			explan.setIds(ids);
			return pageManager.selectPage(explan, page, ExplanMapper.class.getName()+".findAllExplan");
			//return explanMapper.findAllExplan(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有调度条数
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public int findAllExplanCount(Explan explan,String curStatus) throws CRUDException {
		try {
		//	HashMap<String, Object> map=new HashMap<String, Object>();
			if(null!=curStatus && !"".equals(curStatus) && !"".equals(curStatus) ){
				if("1".equals(curStatus)){//未报货
					explan.setYnsto("N");
				}else if("2".equals(curStatus)){//未分解
					explan.setYnex("N");
				}else if("3".equals(curStatus)){//完成但未入库
					explan.setYnex("Y");
					explan.setYnin("N");
				}else if("4".equals(curStatus)){//已入库
					explan.setYnin("Y");
				}
			}
			return explanMapper.findAllExplanCount(explan);
			//return explanMapper.findAllExplan(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有调度分页
	 * @param Pubgrp
	 * @return
	 * @throws CRUDException
	 */
	public List<Explan> findAllExplanPage(Explan explan,String curStatus) throws CRUDException {
		try {
		//	HashMap<String, Object> map=new HashMap<String, Object>();
			if(null!=curStatus && !"".equals(curStatus) && !"".equals(curStatus) ){
				if("1".equals(curStatus)){//未报货
					explan.setYnsto("N");
				}else if("2".equals(curStatus)){//未分解
					explan.setYnex("N");
				}else if("3".equals(curStatus)){//完成但未入库
					explan.setYnex("Y");
					explan.setYnin("N");
				}else if("4".equals(curStatus)){//已入库
					explan.setYnin("Y");
				}
			}
			
			//mysql 分页用
			int start = explan.getStartNum();
			int end = explan.getEndNum();
			explan.setPageSize(end-start);
			
			return explanMapper.findAllExplanPage(explan);
			//return explanMapper.findAllExplan(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 新增
	 * @param explan
	 * @throws CRUDException
	 */
	public void saveExplan(Explan explan) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> itemids = Arrays.asList(explan.getSupply().getSp_code().split(",")); 
			Product product=new Product();
			product.setAcct(explan.getAcct());
			map.put("ynex", "ynex");//是否有可分解产品
			map.put("product", product);
			map.put("itemids", itemids);
			//库存设置的不对 ，应该查positnsupply 在页面上已经去查询了。。wjf
			double cntkc = 0.0;
			//1.先得到物资的默认仓位
			Supply supply = supplyMapper.findSupplyById(explan.getSupply());
			//查询库存量--物资余额表数据
			SupplyAcct sa = new SupplyAcct();
			sa.setPositn(supply.getSp_position());
			sa.setSp_code(supply.getSp_code());
			sa.setAcct(explan.getAcct());
			cntkc = wzYueChaxunService.findSupplyBalanceOnlyEndNum(sa);
			explan.setCntkc(cntkc);//设置库存量
			explan.setCntplan1(explanMapper.getCntplan1(explan.getSupply()));//设置已加工量
			List<Product> productList=prdctMapper.findmaterialBylist(map);
//			Spcodeexm spcodeexm = new Spcodeexm();
//			spcodeexm.setItem(explan.getSupply().getSp_code());
//			spcodeexm.setAcct(explan.getAcct());
//			List<Product> productList=prdctMapper.findmaterial(spcodeexm); 
			if(productList.size()>0){
				explan.setYnex("Y");//可分解
			}else{
				explan.setYnex("N");//不可分解
			}	
			//explan.setCnt(0);
			explan.setYnsto("N");
			explan.setYnin("N");
			explan.setYnout("N");
			explan.setSta(ExplanConstants.PLAN);//计划
			explan.setDat(new Date());
			explanMapper.saveExplan(explan);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  保存计划   修改需加工量  
	 * @param explan
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateCnt(Explan explan,String acct) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String, Object>();
			for (int i = 0; i < explan.getExplanList().size(); i++) {//批量修改
				Explan explan_=new Explan();
				explan_.setAcct(acct);
				explan_.setId(explan.getExplanList().get(i).getId());
				explan_.setCnt(explan.getExplanList().get(i).getCnt());
				explanMapper.updateCnt(explan_);
			}
			
			result.put("updateNum", explan.getExplanList().size());
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  保存登记   修改数量   数量2
	 * @param explan
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateCntact(Explan explan,String acct) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String, Object>();
			for (int i = 0; i < explan.getExplanList().size(); i++) {//批量修改
				Explan explan_=new Explan();
				explan_.setAcct(acct);
				explan_.setId(explan.getExplanList().get(i).getId());
				explan_.setCntact(explan.getExplanList().get(i).getCntact());
				explan_.setCntact1(explan.getExplanList().get(i).getCntact1());
				explan_.setEtim(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				explanMapper.updateCntact(explan_);
			}
			result.put("updateNum", explan.getExplanList().size());
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 转为申购单   
	 * @param product_
	 * @param madeby
	 * @throws CRUDException
	 */
	public String saveChkstom(Product product_,String firm,String madeby,String idList) throws CRUDException {
		try {
			String result="NO";
/*
		//	Chkstom chkstom=new Chkstom();
			int chkstono=chkstomMapper.getMaxChkstono();
			Date date=new Date();
			//Product product=product_.getProductList().get(0);
			Map<String,Object> chkstomMap = new HashMap<String,Object>();
			chkstomMap.put("acct", product_.getAcct());
			chkstomMap.put("yearr", DateFormat.getStringByDate(date,"yyyy"));
			chkstomMap.put("chkstoNo", chkstono);
		//	chkstomMap.put("vouno", chkstom.getVouno());//暂时没用到
			chkstomMap.put("maded",  date);
			chkstomMap.put("madet", DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
	//      chkstomMap.put("positn", "1022");
			chkstomMap.put("firm",firm);//半成品加工间    
			chkstomMap.put("madeby", madeby);
	//		chkstomMap.put("totalAmt", chkstom.getTotalAmt());
*/			
			Chkstom chkstom=new Chkstom();
			chkstom.setAcct(product_.getAcct());
			Date date=new Date();
			chkstom.setYearr(DateFormat.getStringByDate(date,"yyyy"));
			int chkstono=chkstomMapper.getMaxChkstono();
			chkstom.setVouno(calChkNum.getNextBytable("CHKSTOM","SG"+chkstom.getFirm()+"-",date));
			chkstom.setChkstoNo(chkstono);
			chkstom.setMaded(date);
			chkstom.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(chkstomMapper.findMainPositnByFirm(firm));
			chkstom.setPositn(positn);
			chkstom.setFirm(firm);
			chkstom.setMadeby(madeby);
			//修改加工间报货的时候没有总价问题，根据报货单填制提交来改的  总价=标准价*数量  wjf
			double totalAmt = 0;
			if(product_.getProductList().size() != 0){
				for(Product product:product_.getProductList()){
					Supply supply = product.getSupply();
					double amount=product.getCnt();
					double price=supply.getSp_price();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			chkstomMapper.saveNewChkstom(chkstom);
							
				for (int i = 0; i < product_.getProductList().size(); i++) { 
					Product product_1=product_.getProductList().get(i);
/*					
					Map<String,Object> chkstodMap= new HashMap<String,Object>();
					chkstodMap.put("chkstoNo", chkstono);
					chkstodMap.put("sp_code",product_1.getSupply().getSp_code());
					chkstodMap.put("amount", product_1.getCnt());
					chkstodMap.put("amount1", product_1.getCnt1());
					chkstodMap.put("price", "");
					chkstodMap.put("memo", product_1.getMemo());
					chkstodMap.put("hoped", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));		
*/
					Chkstod chkstod=new Chkstod();
					chkstod.setChkstoNo(chkstono);
					chkstod.setSupply(product_1.getSupply());
					chkstod.setAmount(product_1.getCnt());
					chkstod.setAmount1(product_1.getCnt1());
					chkstod.setMemo(product_1.getMemo());
					chkstod.setHoped(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
					chkstodMapper.saveNewChkstod_new(chkstod);			
				}
			//修改为已经报货	
				HashMap<String, Object> map=new HashMap<String, Object>();
				List<String> listId=Arrays.asList(idList.split(","));
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				map.put("listId", listId);
				map.put("ynsto", "Y");//已经申购
				map.put("sta", "开始");//已经申购
				map.put("btim", sdf.format(new Date()));
				explanMapper.updateExplan(map);
				result="OK";
				return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分解产品
	 * @throws CRUDException
	 */
	public void updateResolveExplan(String ids,String sp_codeIds,String acct) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> itemids = Arrays.asList(sp_codeIds.split(",")); 
			Product product_=new Product();
			product_.setAcct(acct);
			map.put("ynex", "ynex");//是否有可分解产品
			map.put("product", product_);
			map.put("itemids", itemids);
			List<Product> productList=prdctMapper.findmaterialBylist(map);
			for (int i = 0; i < productList.size(); i++) {
				Product  product=productList.get(i);
				Explan explan=new Explan();
				explan.setSupply(product.getSupply());
				explan.setExno(calChkNum.getMaxsequences("EXNO"));//取得序列最大值的公共方法
				explan.setCnt(product.getCnt()*product.getExplan().getCnt());//计算    需加工量*　数量２
				explan.setCntplan(product.getCnt()*product.getExplan().getCnt());//需求量    需加工量*　数量２
				explan.setAcct(acct);
				saveExplan(explan);
			}
			//修改为不可分解了
			HashMap<String, Object> map_=new HashMap<String, Object>();
			List<String> listId=Arrays.asList(ids.split(","));
			map_.put("listId", listId);
			map_.put("ynex", "N");//不可分解了,
			explanMapper.updateExplan(map_);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除调度
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteExplan(List<String> listId) throws CRUDException {
		try {
			explanMapper.deleteExplan(listId);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 增  删  改 方法
	 * @param explan
	 * @throws CRUDException
	 */
	public void saveOrUpdateOrDelExplan(Explan explan) throws CRUDException {
		try {
			 explanMapper.saveOrUpdateOrDelExplan(explan);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据id查询 调度
	 * @param explan
	 * @return
	 * @throws CRUDException
	 */
	public Explan findExplanByid(Explan explan) throws CRUDException {
		try {
			return explanMapper.findExplanByid(explan);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改调度
	 * @param explan
	 * @throws CRUDException
	 */
	public int updateExplan(Explan explan) throws CRUDException {
		try {
			return  0;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
		/**
	 * 根据加工间编码和物资编码获取供应商信息
	 * @param Supply
	 * @return
	 * @throws CRUDException
	 */
	public Deliver findDeliverByPositn(Deliver deliver) throws CRUDException {
		try {
			return explanMapper.findDeliverByPositn(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/*************    计划---计划    ************/
	public List<Map<String,Object>> findPlanFirm(Map<String,Object> map,Page page)throws CRUDException {
		try {
			SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
					supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
				}
				if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
					supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
				}
				map.put("supplyAcct", supplyAcct);
			}
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();// group by 用
			if(null!=map.get("codes") && !"".equals(map.get("codes"))){
				String firm = map.get("codes").toString();
				String[] firmArray = firm.split(",");
				for(String f : firmArray){
//					sqlStr.append(" sum(decode(cm.firm, '"+f+"', c.amount,null)) as f_"+f+",");
					sqlStr.append(" sum(case when cm.firm = '"+f+"' then cd.amountin else null end) as f_"+f+",");//适用sqlserver 2014.10.24 wjf
					sqlStr2.append("f_"+f+",");
				}
				map.put("codes", CodeHelper.replaceCode(firm));
			}else{
				List<Positn> firmList = findTableHead(map);
				for(Positn p : firmList){
//					sqlStr.append(" sum(decode(cm.firm, '"+p.getCode()+"', c.amount,null)) as f_"+p.getCode()+",");
					sqlStr.append(" sum(case when cm.firm = '"+p.getCode()+"' then cd.amountin else null end) as f_"+p.getCode()+",");
					sqlStr2.append("f_"+p.getCode()+",");
				}
			}
			map.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
			map.put("sqlStr2", sqlStr2.substring(0, sqlStr2.lastIndexOf(",")));
			
			String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			map.put("year", year);
			if(page != null)//2014.10.21 wjf
				return pageManager1.selectPage(map,page,ExplanMapper.class.getName()+".findPlanFirm");	
			else
				return explanMapper.findPlanFirm(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/*************    计划---计划    ************/
	/***
	 * 报货到加工间
	 * @author wjf
	 * @param map
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findPlanFirm_new(Map<String,Object> map,Page page)throws CRUDException {
		try {
		SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			map.put("supplyAcct", supplyAcct);
		}
		StringBuffer sqlStr = new StringBuffer();
		StringBuffer sqlStr2 = new StringBuffer();// group by 用
		if(null!=map.get("codes") && !"".equals(map.get("codes"))){
			String firm = map.get("codes").toString();
			String[] firmArray = firm.split(",");
			for(String f : firmArray){
//				sqlStr.append(" sum(decode(cm.firm, '"+f+"', c.amount,null)) as f_"+f+",");
				sqlStr.append(" sum(case when cm.firm = '"+f+"' then cd.amountin else null end) as F_"+f+",");//适用sqlserver 2014.10.24 wjf
				sqlStr2.append("F_"+f+",");
			}
			map.put("codes", CodeHelper.replaceCode(firm));
		}else{
			List<Positn> firmList = findTableHead(map);
			for(Positn p : firmList){
//				sqlStr.append(" sum(decode(cm.firm, '"+p.getCode()+"', c.amount,null)) as f_"+p.getCode()+",");
				sqlStr.append(" sum(case when cm.firm = '"+p.getCode()+"' then cd.amountin else null end) as F_"+p.getCode()+",");
				sqlStr2.append("F_"+p.getCode()+",");
			}
		}
		map.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
		map.put("sqlStr2", sqlStr2.substring(0, sqlStr2.lastIndexOf(",")));
		
		String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
		map.put("year", year);
		//判断是不是走配送片区默认仓位
		int count = explanMapper.findSpcodemodCount();
		if(count == 0){//没有说明只走物资表  默认仓位和默认加工间
			if(page != null)//2014.10.21 wjf
				return pageManager1.selectPage(map,page,ExplanMapper.class.getName()+".findPlanFirm1");	
			else
				return explanMapper.findPlanFirm1(map);
		}else{
			if(page != null)//2014.10.21 wjf
				return pageManager1.selectPage(map,page,ExplanMapper.class.getName()+".findPlanFirm2");	
			else
				return explanMapper.findPlanFirm2(map);
		}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<Positn> findTableHead(Map<String,Object> condition){
		List<Positn> positnList = positnMapper.findPositnByTypCode(condition);
		return positnList;
	}
	/************* 安全库存到计划    ************/
	public List<Map<String,Object>> safeCntToPlan(Map<String,Object> map,Page page)throws CRUDException {
		try {
			SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
					supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
				}
				map.put("supplyAcct", supplyAcct);
			}
			String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			map.put("year", year);
			return pageManager1.selectPage(map,page,ExplanMapper.class.getName()+".safeCntToPlan");	 
	} catch (Exception e) {
		log.error(e);
		throw new CRUDException(e);
	}
	}
	/**
	 * 保存来自计划的    
	 * @param explan
	 * @throws CRUDException
	 */
	public void saveExplanByPlan(Explan explan,String acct) throws CRUDException {
		try {
			String ids = "'";
			boolean flag = true;
			for (int i = 0; i < explan.getExplanList().size(); i++) {
				if (!flag) {
					ids += ",'";
				}
				flag = false;
				Explan exp=explan.getExplanList().get(i);
				exp.setAcct(acct);
				exp.setExno(explan.getExno()+i);
//				exp.setCntkc(explanMapper.getCntkc(exp.getSupply()));//设置库存量
//				exp.setCntplan1(explanMapper.getCntplan1(exp.getSupply()));//设置已加工量
				ids += exp.getSupply().getSp_code();
				ids += "'";
				saveExplan(exp);
			} 
			Map<String,Object> chkstodMap= new HashMap<String,Object>();
			chkstodMap.put("bak1", "1");
			chkstodMap.put("bdat", explan.getDat());
			chkstodMap.put("edat", explan.getDatEnd());
			chkstodMap.put("positnex", explan.getExplanList().get(0).getSupply().getPositnex());//加工间
			chkstodMap.put("ids", ids);
			explanMapper.saveExplanByPlan(chkstodMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存来自安全库存的    
	 * @param explan
	 * @throws CRUDException
	 */
	public void saveExplanBySafeCnt(Explan explan,String acct) throws CRUDException {
		try {
			for (int i = 0; i < explan.getExplanList().size(); i++) {
				Explan exp=explan.getExplanList().get(i);
				exp.setAcct(acct);
				exp.setExno(explan.getExno()+i);
				exp.setCntkc(explanMapper.getCntkc(exp.getSupply()));//设置库存量
				exp.setCntplan1(explanMapper.getCntplan1(exp.getSupply()));//设置已计划量
				saveExplan(exp);
			} 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
