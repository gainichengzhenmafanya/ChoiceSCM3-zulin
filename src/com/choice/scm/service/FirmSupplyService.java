package com.choice.scm.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.OracleUtil;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.persistence.FirmSupplyMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;

@Service
public class FirmSupplyService {
	
	private static Logger log = Logger.getLogger(AccountService.class);
	@Autowired
	private FirmSupplyMapper firmSupplyMapper; 
	@Autowired
	private PageManager<PositnSpcode> pageManager;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	
	/**
	 * 根据分店查询物资
	 * @param positnSpcode
	 * @return
	 */
	public List<PositnSpcode> findFirmSupply(PositnSpcode positnSpcode ,Page page){
		positnSpcode.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
		List<PositnSpcode> psList = pageManager.selectPage(positnSpcode, page, FirmSupplyMapper.class.getName()+".findFirmSupply");
		return getSuList(psList);
	}
	public List<PositnSpcode> findFirmSupply(PositnSpcode positnSpcode){
		positnSpcode.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
		List<PositnSpcode> psList = firmSupplyMapper.findFirmSupply(positnSpcode);
		return getSuList(psList);
	}
	
	/**
	 * 加上配送单位
	 * @param psList
	 * @return
	 */
	private List<PositnSpcode> getSuList(List<PositnSpcode> psList){
		//处理一下，加上配送单位
		if(psList.size() > 0){
			StringBuffer sb = new StringBuffer();
			for(PositnSpcode ps : psList){
				sb.append(ps.getSp_code()+",");
			}
			SupplyUnit su = new SupplyUnit();
			su.setSp_codes(OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(sb.substring(0, sb.length()-1))));
			su.setYnDisunit("Y");
			List<SupplyUnit> suList = supplyMapper.findAllSupplyUnit(su);
			for(PositnSpcode ps : psList){
				List<SupplyUnit> psSuList = new ArrayList<SupplyUnit>();
				for(SupplyUnit s : suList){
					if(ps.getSp_code().equals(s.getSp_code())){
						psSuList.add(s);
					}
				}
				ps.setSuList(psSuList);
			}
		}
		return psList;
	}
	
	/**
	 * 更新分店物资
	 * @param positn
	 * @throws CRUDException
	 */
	public void updateFirmSupply(PositnSpcode positnSpcode) throws CRUDException{
		try{
			String[] positn = positnSpcode.getPositn().split(",");
			positnSpcode.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
			for(String posi:positn){//遍历分店
				positnSpcode.setPositn(posi);
				firmSupplyMapper.updateFirmSupply(positnSpcode);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 只添加物资
	 * @param positnSpcode
	 * @return
	 */
	public String saveByAddOnly(PositnSpcode positnSpcode)throws CRUDException{
		String info = StringConstant.FALSE;
		StringBuffer info1=new StringBuffer();
		try {
			//查询已经加入的物资
			PositnSpcode positnSpcode2 =new PositnSpcode();
			//获取选择的分店
			String[] positn = positnSpcode.getPositn().split(",");
			for(String posid:positn){
				positnSpcode2.setPositn(posid);
				positnSpcode2.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
				List<PositnSpcode> listPostinSpcod = firmSupplyMapper.findFirmSupply(positnSpcode2);
				if(listPostinSpcod.size()!=0){
					for (int i = 0; i < listPostinSpcod.size(); i++) {
						info1.append(listPostinSpcod.get(i).getSp_code()+",");
					}
					return "【"+info1+"】已经添加在分店【"+posid+"】，请勿重复添加！";
				}
			}
			PositnSpcode positnSpcode1 = new PositnSpcode();
			positnSpcode1.setPositn(positnSpcode.getPositn());

			String ids[] = positnSpcode.getSp_code().split(",");
			String[] positns = positnSpcode.getPositn().split(",");
			for(String posi:positns){
				for (int i=0; i<ids.length;i++) {
					if (!"".equals(ids[i])) {
						positnSpcode.setSp_code(ids[i]);
						Supply supply = new Supply();
						supply.setSp_code(ids[i]);
						supply = supplyMapper.findSupplyById(supply);
						positnSpcode.setPositn(posi);
						positnSpcode.setSp_desc(supply.getSp_desc());
						positnSpcode.setSp_max1(supply.getSp_max1());
						positnSpcode.setSp_min1(supply.getSp_min1());
						positnSpcode.setYnsto("Y");
						positnSpcode.setYnpd("Y");//默认盘点 wjf
						
						/**配送单位默认采购单位**/
						positnSpcode.setDisunit(supply.getUnit3());
						positnSpcode.setDisunitper(supply.getUnitper3());
						positnSpcode.setDismincnt(Double.parseDouble(supply.getMincnt()));
						
						//更改分店引用
						Positn positn1 = new Positn();
						positn1.setLocked("Y");
						positn1.setOldcode(posi);
//						positn1.setAcct("");
						positnMapper.updatePositn(positn1);
						firmSupplyMapper.saveSupply(positnSpcode);
					}
				}
			}
			info = StringConstant.TRUE;
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
		return info;
	}	
	/**
	 * 删除并添加物资
	 * @param positnSpcode
	 * @return
	 */
	public String saveSupply(PositnSpcode positnSpcode)throws CRUDException{
		String info = StringConstant.FALSE;
		try {
			//查询已经加入的物资
			PositnSpcode positnSpcode2 =new PositnSpcode();
			positnSpcode2.setPositn(positnSpcode.getPositn());
			positnSpcode2.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
			//遍历左侧选择的分店
			String[] positnStr = positnSpcode.getPositn().split(",");
			String ids[] = positnSpcode.getSp_code().split(",");
			for(String positn:positnStr){
				PositnSpcode positnSpcode1 = new PositnSpcode();
				positnSpcode1.setPositn(positn);
				//先清空该分店下的所有物资
				firmSupplyMapper.deleteFirmSupply(positnSpcode1);
				for (int i=0; i<ids.length;i++) {
					positnSpcode.setSp_code(ids[i]);
					Supply supply = new Supply();
					supply.setSp_code(ids[i]);
					supply = supplyMapper.findSupplyById(supply);
					positnSpcode.setSp_desc(supply.getSp_desc());
					positnSpcode.setSp_max1(supply.getSp_max1());
					positnSpcode.setSp_min1(supply.getSp_min1());
					positnSpcode.setYnsto("Y");
					positnSpcode.setPositn(positn);
					
					/**配送单位默认采购单位**/
					positnSpcode.setDisunit(supply.getUnit3());
					positnSpcode.setDisunitper(supply.getUnitper3());
					positnSpcode.setDismincnt(Double.parseDouble(supply.getMincnt()));
					
					firmSupplyMapper.saveSupply(positnSpcode);
				}
			}
			
			info = StringConstant.TRUE;
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
		return info;
	}
	
	
//	/**
//	 * 分店复制
//	 * @param positnSpcode
//	 * @return
//	 */
//	public void saveByCopy(String positnCode, PositnSpcode positnSpcode)throws CRUDException{
//		try {
//			PositnSpcode positnSpcode1 = new PositnSpcode();
//			positnSpcode1.setPositn(positnCode);
//			List<PositnSpcode> positnSpcodeList = firmSupplyMapper.findFirmSupply(positnSpcode1);
//			firmSupplyMapper.deleteFirmSupply(positnSpcode);
//				for (int i=0; i<positnSpcodeList.size();i++) {
//				positnSpcode.setSp_code(positnSpcodeList.get(i).getSp_code());
//				Supply supply = new Supply();
//				supply.setSp_code(positnSpcodeList.get(i).getSp_code());
//				supply = supplyMapper.findSupplyById(supply);
//				positnSpcode.setSp_desc(supply.getSp_desc());
//				positnSpcode.setSp_max1(supply.getSp_max1());
//				positnSpcode.setSp_min1(supply.getSp_min1());
//				positnSpcode.setYnsto("Y");
//				firmSupplyMapper.saveSupply(positnSpcode);
//			}
//		} catch (Exception e) {
//			log.error(e);
//			e.printStackTrace();
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 物资复制
	 */
	public String saveByCopySupply(String positnCode, PositnSpcode positnSpcode,String code,String ids,String acct){
		try {
			String[] strSupplyCode = ids.split(",");//要复制的物资的编码
			String[] strPositnCode = code.split(",");//要复制的门店的编码
			//List<Supply> listSupply = firmSupplyMapper.findAllFirmSupply(CodeHelper.replaceCode(ids));
			for (int i = 0; i < strPositnCode.length; i++) {//循环门店
				String positn = strPositnCode[i];
				PositnSpcode positnSpcode2 = new PositnSpcode();
				positnSpcode2.setPositn(positn);//复制到的门店编码
				positnSpcode2.setPositnCode(positnCode);//要复制的门店编码
				for (int j = 0; j < strSupplyCode.length; j++) {
					positnSpcode2.setSp_code(strSupplyCode[j]);
					firmSupplyMapper.deleteFirmSupplyByIds(positnSpcode2);//删除当前要复制门店下已存在的物资
					firmSupplyMapper.savePositnSpcodeByCopy(positnSpcode2);
				}                                                                                                                                                        
			}
			return StringConstant.TRUE;
		} catch (Exception e) {
			e.printStackTrace();
			return StringConstant.FALSE;
		}
	}
	
	/**
	 * 分店复制
	 * @param positnSpcode
	 * @return
	 */
	public void saveByCopy(String positnCode, PositnSpcode positnSpcode,String code,String acct)throws CRUDException{
		try {
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("acct", acct);
			map.put("ffirm", positnCode);
			map.put("tfirm", code);
			map.put("emp", null);
			firmSupplyMapper.saveSupplyByCopy(map);
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店复制--洞庭专用 wangjie 2014年12月9日 13:26:43
	 * @param positnSpcode
	 * @return
	 */
	public void saveByCopyDT(String positnCode, PositnSpcode positnSpcode,String code,String acct)throws CRUDException{
		try {
			
			String[] strList = code.split(",");
			if(positnSpcode.getPositnCode().indexOf(",")>-1){
				positnSpcode.setPositnCode(positnSpcode.getPositnCode().split(",")[1]);
			}
			for(String vfirm:strList){
				firmSupplyMapper.deleteFirmSupplyForDT(vfirm);
				positnSpcode.setPositn(vfirm);
				firmSupplyMapper.insertFirmSupplyForDT(positnSpcode);
			}
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除系统编码信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteFirmSupply(PositnSpcode positnSpcode) throws CRUDException
	{
		try{
			positnSpcode.setSp_code(CodeHelper.replaceCode(positnSpcode.getSp_code()));
			String[] positnStr = positnSpcode.getPositn().split(",");
			//批量删除 多个分店的多种物资
			for(String positn:positnStr){
				positnSpcode.setPositn(positn);
				firmSupplyMapper.deleteFirmSupply(positnSpcode);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店安全库存设置
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<PositnSpcode> findPositnSpCode(Supply  supply,String level,String code,Page page) throws CRUDException {
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
			return pageManager.selectPage(supply, page, FirmSupplyMapper.class.getName()+".findPositnSpCode");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<PositnSpcode> findCangKuSpCode(Supply  supply,Page page) throws CRUDException {
		try { 
			return pageManager.selectPage(supply, page, FirmSupplyMapper.class.getName()+".findCangKuSpCode");
//			return firmSupplyMapper.findCangKuSpCode(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	} 
	
	/**
	 * 根据大中小类添加账号物资权限
	 * @param accountId 账号
	 * @param classType 类别大0中1小2
	 * @param classCode 类别编码
	 */
	public void saveFirmSupply(String positnCode, List<Supply> list, String acct) throws Exception{
		PositnSpcode positnSpcode = new PositnSpcode();
		positnSpcode.setPositn(positnCode);
//		firmSupplyMapper.deleteFirmSupply(positnSpcode);  //  写这行代码的太坑爹了，
		for(Supply supply:list){
			PositnSpcode positnSpcode1 = new PositnSpcode();
			positnSpcode1.setAcct(acct);
			positnSpcode1.setPositn(positnCode);
			positnSpcode1.setSp_code(supply.getSp_code());
			positnSpcode1.setSp_name(supply.getSp_name());
			positnSpcode1.setSp_desc(supply.getSp_desc());
			positnSpcode1.setSp_max1(supply.getSp_max1());
			positnSpcode1.setSp_min1(supply.getSp_min1());
			positnSpcode1.setYnsto("Y");
			firmSupplyMapper.saveSupply(positnSpcode1);
		}
	}

	/**
	 * 分店物资属性添加物资
	 */
	public void saveSupplyByClass(List<Supply> listSupply, String positnCode) throws CRUDException {
		PositnSpcode positnSpcode = new PositnSpcode();
		positnSpcode.setPositn(positnCode);
		//删除全部
//		firmSupplyMapper.deleteFirmSupply(positnSpcode);  写这行代码的人 真坑  
		//加判断
		if(listSupply==null||listSupply.size()==0){
			return;
		}
		// 新增
		for (Supply s:listSupply) {
				positnSpcode.setSp_code(s.getSp_code());				
				//根据分店及物资联合   判断是否存在 存在就不增加了，   2015.6.11  xlh  
				if(null==firmSupplyMapper.findFirmSupplyByFirmAndSpcode(positnSpcode)){					
					Supply supply = new Supply();
					supply.setSp_code(s.getSp_code());
					supply = supplyMapper.findSupplyById(supply);
					positnSpcode.setSp_desc(supply.getSp_desc());
					positnSpcode.setSp_max1(supply.getSp_max1());
					positnSpcode.setSp_min1(supply.getSp_min1());
					positnSpcode.setYnsto("Y");
					firmSupplyMapper.saveSupply(positnSpcode);
				}
		}
	}
	
	/**
	 * 原材料需求单据 导出excel   wangjie
	 * @param os
	 * @param productList
	 * @param list
	 */
	public boolean exportFirmSupply(ServletOutputStream os, Map<String, List<PositnSpcode>> map,List<Map<String,Object>> list){
		
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			
			//循环多家分店 生成多个sheet页
			Iterator<?> it = map.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry<?,?> entry = (Map.Entry<?,?>) it.next();
				String firmname = (String) entry.getKey();
				List<PositnSpcode> positnList = map.get(firmname);
				
				WritableSheet sheet = workBook.createSheet(firmname+"物资列表", 0);
				sheet.addCell(new Label(0, 0,firmname+"物资列表",titleStyle));
				sheet.mergeCells(0, 0, 11, 0);
	            sheet.addCell(new Label(0, 1, "物资编码"));   
	            sheet.addCell(new Label(1, 1, "物资名称")); 
				sheet.addCell(new Label(2, 1, "规格"));  
				sheet.addCell(new Label(3, 1, "单位"));
				sheet.addCell(new Label(4, 1, "可申购"));
				sheet.addCell(new Label(5, 1, "可盘点"));
				sheet.addCell(new Label(6, 1, "最低验货比"));
				sheet.addCell(new Label(7, 1, "最高验货比"));
				sheet.addCell(new Label(8, 1, "最高库存"));
				sheet.addCell(new Label(9, 1, "最低库存"));
				sheet.addCell(new Label(10, 1, "日均耗用"));
				sheet.addCell(new Label(11, 1, "采购周期"));
				
				//原材料调整量
//				String 
				
				for(int i = 0;i < positnList.size(); i++){
					sheet.addCell(new Label(0, (i+2), positnList.get(i).getSp_code()));   
		            sheet.addCell(new Label(1, (i+2), positnList.get(i).getSp_name())); 
					sheet.addCell(new Label(2, (i+2), positnList.get(i).getSp_desc()));  
					sheet.addCell(new Label(3, (i+2), positnList.get(i).getUnit()));
					sheet.addCell(new Label(4, (i+2), positnList.get(i).getYnsto()==null?"":positnList.get(i).getYnsto()+""));
					sheet.addCell(new Label(5, (i+2), positnList.get(i).getYnpd()==null?"":positnList.get(i).getYnpd()+""));
					sheet.addCell(new Label(6, (i+2), positnList.get(i).getSp_min1()+""));
					sheet.addCell(new Label(7, (i+2), positnList.get(i).getSp_max1()+""));
					sheet.addCell(new Label(8, (i+2), positnList.get(i).getYhrate1()+""));
					sheet.addCell(new Label(9, (i+2), positnList.get(i).getYhrate2()+""));
					sheet.addCell(new Label(10, (i+2), positnList.get(i).getCntuse()));
					sheet.addCell(new Label(11, (i+2), positnList.get(i).getDatsto()==null?"":positnList.get(i).getDatsto()+""));
				}
				
			}
			
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 保存配送单位
	 * @param positnSpcode
	 * @return
	 */
	public int updateDisunit(PositnSpcode positnSpcode) throws CRUDException {
		try { 
			for(PositnSpcode ps : positnSpcode.getPositnSpcodeList()){
				firmSupplyMapper.updateDisunit(ps);
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 批量复制配送单位
	 * @param positnSpcode
	 * @return
	 * @throws CRUDException
	 */
	public int updateDisunitBatch(PositnSpcode positnSpcode) throws CRUDException {
		try {
			positnSpcode.setPositn(CodeHelper.replaceCode(positnSpcode.getPositn()));
			String[] spcodeList = positnSpcode.getSp_code().split(",");
			for(String spcode : spcodeList){
				PositnSpcode ps = new PositnSpcode();
				ps.setPositn(positnSpcode.getPositn());
				ps.setPositnCode(positnSpcode.getPositnCode());
				ps.setSp_code(spcode);
				firmSupplyMapper.updateDisunitBatch(ps);
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 根据门店和物资查询分店物资属性
	 * @param positnSpcode
	 * @return
	 */
	public PositnSpcode findFirmSupplyByFirmAndSpcode(PositnSpcode positnSpcode) {
		return firmSupplyMapper.findFirmSupplyByFirmAndSpcode(positnSpcode);
	}
}
