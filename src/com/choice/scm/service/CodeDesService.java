package com.choice.scm.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Tax;
import com.choice.scm.persistence.CodeDesMapper;

@Service
public class CodeDesService {

	@Autowired
	private CodeDesMapper codeDesMapper;
	@Autowired
	private PageManager<CodeDes> pageManager;
	@Autowired
	private PageManager<Tax> pageManager2;
	private final transient Log log = LogFactory.getLog(CodeDes.class);
	
	/**
	 * 查询所有的系统编码信息
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findCodeDes(CodeDes codeDes,Page page) throws CRUDException
	{
		try {
			String code = codeDes.getCode();
			if(code!=null&&!code.startsWith("'")){
				code = CodeHelper.replaceCode(code);
				codeDes.setCode(code);
			}
			return pageManager.selectPage(codeDes, page, CodeDesMapper.class.getName()+".findCodeDes");		
			//return codeDesMapper.findAllCodeDes(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货类别
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findCodeDes_typ(CodeDes codeDes,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(codeDes, page, CodeDesMapper.class.getName()+".findCodeDes_typ");		
			//return codeDesMapper.findAllCodeDes(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据code查询信息
	 */
	public CodeDes findCodeDesByCode(CodeDes codeDes) throws CRUDException
	{
		try {
			return codeDesMapper.findCodeDesByCode(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据code查询信息--报货类别
	 */
	public CodeDes findCodeDesByCode_typ(CodeDes codeDes) throws CRUDException
	{
		try {
			return codeDesMapper.findCodeDesByCode_typ(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的系统编码信息
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findCodeDes(CodeDes codeDes) throws CRUDException
	{
		try {
			List<CodeDes> list = new ArrayList<CodeDes>();
			if(codeDes.getCode() != null && !"".equals(codeDes.getCode())){
				for(String code :codeDes.getCode().split(",")){
					CodeDes codeDes2 = new CodeDes();
					codeDes2.setTyp(codeDes.getTyp());
					codeDes2.setCode(code);
					
					list.addAll(codeDesMapper.findCodeDes(codeDes2));
				}
			}else{
				list = codeDesMapper.findCodeDes(codeDes);
			}
			
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 按类型系统编码信息
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findCodeDes(String typ) throws CRUDException
	{
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(typ);
		try {
			return codeDesMapper.findCodeDes(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改排序列===用于保存的时候
	 * @param codeDes
	 * @throws CRUDException
	 * @author wangchao
	 */
	public void updateCodedesPX(CodeDes codeDes) throws CRUDException{
		try {
			if (codeDesMapper.getBeatsequence(codeDes) > 0) {//判断排序列是否存在
				codeDesMapper.updateCodedesPX(codeDes);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改排序列====用于修改的时候
	 * @param codeDes
	 * @throws CRUDException
	 * @author wangchao
	 */
	public void updateCodedesPx(CodeDes codeDes) throws CRUDException{
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			//根据code查询codedes
			CodeDes cd = codeDesMapper.selectCodedesByCode(codeDes);
			map.put("oldBeatsequence", cd.getBeatsequence());
			map.put("codeDes", codeDes);
			if (cd.getBeatsequence() != codeDes.getBeatsequence()) {
				if (cd.getBeatsequence() > codeDes.getBeatsequence()) {//排序列从大的改成小的
					codeDesMapper.updateCodedesPx1(map);
				} else if (cd.getBeatsequence() < codeDes.getBeatsequence()) {//排序列从小的改成大的
					codeDesMapper.updateCodedesPx2(map);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存系统编码信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public String saveCodeDes(CodeDes codeDes,Page page) throws CRUDException
	{
		CodeDes param1=new CodeDes();
		//update by js at 20160322
		//如果以单引号开头，则不进行转换
		String codetest = codeDes.getCode();
		codetest = codetest.startsWith("'")?codetest:CodeHelper.replaceCode(codetest);
		param1.setCode(codetest);
		param1.setTyp(codeDes.getTyp());
		if (codeDes.getTyp().equals("11")) {//报货类别--需要添加报货方式
			param1.setCodetyp(codeDes.getCodetyp());
		}
		CodeDes param2=new CodeDes();
		param2.setDes(codeDes.getDes());
		param2.setTyp(codeDes.getTyp());
		if (codeDes.getTyp().equals("11")) {//报货类别--需要添加报货方式
			param2.setCodetyp(codeDes.getCodetyp());
		}
		String str="";
		try {
			if(findCodeDes(param1,page).size()!=0 && findCodeDes(param2,page).size()==0){
				str="codeError";
			}else if(findCodeDes(param2,page).size()!=0 && findCodeDes(param1,page).size()==0){
				str="desError";
			}else if(findCodeDes(param2,page).size()!=0 && findCodeDes(param1,page).size()!=0){
				str="allError";
			}else{
				if (codeDes.getTyp().equals("11")) {//报货类别--需要添加报货方式
					codeDesMapper.saveCodeDes_typ(codeDes);
				} else {
					codeDesMapper.saveCodeDes(codeDes);
				}
				str="succ";
			}
			return str;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询税率 jinshuai 20160428
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<Tax> findTaxRateList(Tax tax,Page page) throws CRUDException
	{
		try {
			return pageManager2.selectPage(tax, page, CodeDesMapper.class.getName()+".findTaxRateList");		
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增保存税率信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public String saveTaxMes(Tax tax) throws CRUDException{
		try{
			//判断是否有相同税率，或者税率名称的税率
			Tax tax1 = new Tax();
			tax1.setTax(tax.getTax());
			Page page = new Page();
			List<Tax> list1 = findTaxRateList(tax1,page);
			Tax tax2 = new Tax();
			tax2.setTaxdes(tax.getTaxdes());
			List<Tax> list2 = findTaxRateList(tax2,page);
			if(list1!=null&&list1.size()>0){
				return "taxError";
			}
			if(list2!=null&&list2.size()>0){
				return "desError";
			}
			//保存税率信息
			codeDesMapper.saveTaxMes(tax);
		}catch(CRUDException e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		return "succ";
	}
	
	/**
	 * 修改税率信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public String updateTaxMes(Tax tax) throws CRUDException{
		try{
			//判断是否有相同税率，或者税率名称的税率
			Tax tax1 = new Tax();
			tax1.setTax(tax.getTax());
			Page page = new Page();
			//查询是否有相同的税率
			List<Tax> list1 = findTaxRateList(tax1,page);
			Tax tax2 = new Tax();
			tax2.setTaxdes(tax.getTaxdes());
			//查询是否有相同名称的税率
			List<Tax> list2 = findTaxRateList(tax2,page);
			if(list1!=null&&list1.size()>0){
				for(int i=0,len=list1.size();i<len;i++){
					tax2 = list1.get(i);
					if(tax2!=null&&tax!=null&&tax2.getId()!=tax.getId()){
						return "taxError";
					}
				}
			}
			if(list2!=null&&list2.size()>0){
				for(int i=0,len=list2.size();i<len;i++){
					tax2 = list2.get(i);
					if(tax2!=null&&tax!=null&&tax2.getId()!=tax.getId()){
						return "desError";
					}
				}
			}
			//更新税率信息
			codeDesMapper.updateTaxMes(tax);
		}catch(CRUDException e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		return "succ";
	}
	
	/**
	 * 删除税率信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteTaxMes(String ids) throws CRUDException
	{
		//删除税率信息
		codeDesMapper.deleteTaxMes(ids);
	}
	
	/**
	 * 删除系统编码信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteCodeDes(String codes, String types) throws CRUDException
	{
		try{
			CodeDes codeDes=new CodeDes();
			//update by js at 20160322
			//如果以单引号开头，则不进行转换
			codes = codes.startsWith("'")?codes:CodeHelper.replaceCode(codes);
			codeDes.setCode(codes);
			codeDes.setTyp(types);
			codeDesMapper.deleteCodeDes(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有要删除的编码
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> getAllCodeDes(CodeDes codeDes) throws CRUDException{
		try {
			//update by js at 20160322
			//如果以单引号开头，则不进行转换
			String codetest = codeDes.getCode();
			codetest = codetest.startsWith("'")?codetest:CodeHelper.replaceCode(codetest);
			codeDes.setCode(codetest);
			return codeDesMapper.getAllCodeDes(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除之后重新排序
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void updateCodedespx(CodeDes codeDes) throws CRUDException{
		try {
			codeDesMapper.updateCodedespx(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新系统编码信息
	 * @param codeDesMap
	 * @return
	 * @throws CRUDException
	 */
	public String updateCodeDes(HashMap<String, Object> codeDesMap) throws CRUDException{
		CodeDes codeDes = (CodeDes) codeDesMap.get("codeDes");
		CodeDes param2=new CodeDes();
		param2.setDes(codeDes.getDes());
		param2.setTyp(codeDes.getTyp());
		if (codeDes.getTyp().equals("11")) {//报货类别--需要添加报货方式
			param2.setCodetyp(codeDes.getCodetyp());
		}
		//返回值
		String str = "succ";
		Page page = new Page();
		try {
			//判断是否有其他编码是相同的名称
			List<CodeDes> list = findCodeDes(param2,page);
			if(list.size()>0){
				for(int i=0,len=list.size();i<len;i++){
					CodeDes cd = list.get(i);
					if(cd!=null&&cd.getCode()!=null&&codeDes.getCode()!=null&&!cd.getCode().equals(codeDes.getCode())){
						str="desError";
						break;
					}
				}
			}
			if(!"desError".equals(str)){
				codeDesMapper.updateCodeDes(codeDesMap);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return str;
	}
	/**
	 * 验证输入的名称在所选的类别下是否有已存在的记录
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public int getDes(CodeDes codeDes)throws CRUDException{
		try {
			int ru = codeDesMapper.findSameDesByTyp(codeDes);
			return ru>0?1:0;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 验证输入的名称在所选的类别下是否有已存在的记录--修改
	 * @param codeDes
	 * @return
	 * @throws CRUDException
	 */
	public int getDesu(CodeDes codeDes)throws CRUDException{
		try {
			int ru = codeDesMapper.findSameDesByTypu(codeDes);
			return ru>0?1:0;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询是否被引用
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public int deleteyh(String codes, String type) throws CRUDException{
		try {
			CodeDes codeDes=new CodeDes();
			codeDes.setCode(CodeHelper.replaceCode(codes));
			codeDes.setTyp(type);
			if (type != null && "1".equals(type)) {
				return codeDesMapper.deleteYHDeliver(codeDes);
			} else if (type != null && "3".equals(type)) {
				return codeDesMapper.deleteYHArea(codeDes);
			} else if (type != null && "4".equals(type)) {
				return codeDesMapper.deleteYHMod2(codeDes);
			} else if (type != null && "5".equals(type)) {
				return codeDesMapper.deleteYHDistributionArea(codeDes);
			} else if (type != null && "9".equals(type)) {
				return codeDesMapper.deleteYHCardType(codeDes);
			} else if (type != null && "10".equals(type)) {
				return codeDesMapper.deleteYHFirmTyp(codeDes);
			} else if (type != null && "11".equals(type)) {
				return codeDesMapper.deleteYHDailyGoodsTyp(codeDes);
			} else if (type != null && "0".equals(type)) {
				return codeDesMapper.deleteYHDailyNuit(codeDes);
			} else if (type != null && "13".equals(type)) {
				return codeDesMapper.deleteYHDailyScrap(codeDes);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return 0;
	}
	
	/**
	 * 获取最大的排序列的值
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	public String getMaxBeatsequence(CodeDes codeDes) throws Exception{
		try {
			return codeDesMapper.getMaxBeatsequence(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取单据类型
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	public List<CodeDes> findDocumentType(CodeDes codeDes) throws Exception{
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("locale", codeDes.getLocale());
			map.put("listType", Arrays.asList(codeDes.getCodetyp().split(",")));
			return codeDesMapper.findDocumentType(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据单据类型编码 查询单据类型 名称
	 * @param codeDes
	 * @return
	 * @throws Exception
	 */
	public CodeDes findDocumentByCode(CodeDes codeDes)  throws Exception{
		try {
			return codeDesMapper.findDocumentByCode(codeDes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
