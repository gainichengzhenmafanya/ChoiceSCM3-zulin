package com.choice.scm.service;


import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.persistence.ChkstomCMapper;

@Service

public class ChkstomCService {

	@Autowired
	private ChkstomCMapper chkstomCMapper;
	@Autowired
	private PageManager<Chkstom> pageManager;

	private final transient Log log = LogFactory.getLog(ChkstomService.class);

	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public Chkstom findByChkstoNo(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomCMapper.findByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 按单号查询
	 * @param chkstom
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findListByChkstoNo(Chkstom chkstom) throws CRUDException
	{
		try {
			return chkstomCMapper.findListByChkstoNo(chkstom);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 关键字查找报货单
	 * @param chkstomMap
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstom> findByKey(HashMap<String,Object> chkstomMap,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstomMap, page, ChkstomCMapper.class.getName()+".findByKey");			
			//return chkstomMapper.findByKey(chkstomMap);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更改上传状态
	 * @param chkstom
	 */
	public void updateChkstomC(Chkstom chkstom) throws CRUDException{
		try {
			chkstomCMapper.updateChkstomC(chkstom);
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}
}
