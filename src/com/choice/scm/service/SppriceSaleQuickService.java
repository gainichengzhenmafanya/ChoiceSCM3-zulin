package com.choice.scm.service;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.SppriceQuickMapper;
import com.choice.scm.persistence.SppriceSaleQuickMapper;

@Service
public class SppriceSaleQuickService {
	@Autowired
	private SppriceSaleQuickMapper sppriceSaleQuickMapper;
	@Autowired
	private SppriceQuickMapper sppriceQuickMapper;
	/**
	 * 快速 批量插入售价单 并审核
	 * @param session
	 * @param sppriceSales
	 * @return
	 */
	public String saveSppriceSaleQuick(HttpSession session,SppriceSale sppriceSales){
		StringBuffer result = new StringBuffer();
		try {
			for (SppriceSale sppriceSale : sppriceSales.getSppriceSaleList()) {
				for (Positn positn : sppriceSale.getPositnList()) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("deliver", sppriceSale.getDeliver().getCode());
					map.put("area", positn.getCode());
					map.put("sp_code",sppriceSale.getSupply().getSp_code());
					map.put("price", sppriceSale.getPrice());
					map.put("bdat", sppriceSale.getBdat());
					map.put("edat", sppriceSale.getEdat());
					if(findSppriceByDeliverPositnSupplyPriceDate(map) != 0){
						result.append(sppriceSale.getSupply().getSp_code()+"物资在"+positn.getCode()+"门店中当前价格，当前时间已添加售价；已过滤。\n");
						continue;
					}
					sppriceSale.setAcct(session.getAttribute("ChoiceAcct").toString());
					sppriceSale.setMadet(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss").toString());
					sppriceSale.setSta("Y");
					sppriceSale.setChecby(session.getAttribute("accountName").toString());
					sppriceSale.setEmp(session.getAttribute("accountName").toString());
					sppriceSale.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
					sppriceSale.setArea(positn);
					sppriceSaleQuickMapper.saveSppriceSale(sppriceSale);
				}
			}
			
			return "执行完成！\n"+result.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	/***
	 * 根据供货商得到所有的物资售价
	 * @param sppriceSale
	 * @param page
	 * @return
	 */
	public List<SppriceSale> findSppriceSale(SppriceSale sppriceSale, Page page) {
//		return sppriceSalePageManager.selectPage(sppriceSale, page, SppriceSaleQuickMapper.class.getName()+".findSppriceSale");
		return sppriceSaleQuickMapper.findSppriceSale(sppriceSale);
	}

	/**
	 * 查询 物资编码信息
	 */
	public List<Supply> findSupplyById(Supply supply) throws CRUDException {
		try {
			List<String> idList = Arrays.asList(supply.getSp_code().split(","));
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", idList);
			params.put("acct", supply.getAcct());
			return sppriceQuickMapper.findSupplyById(params);
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据条件：供应商，仓位，时间，物资，价格条件查询售价，校验 wjf
	 * @param map
	 * @return
	 */
	public Integer findSppriceByDeliverPositnSupplyPriceDate(Map map){
		return sppriceSaleQuickMapper.findSppriceSaleByDeliverPositnSupplyPriceDate(map);
	}
}
