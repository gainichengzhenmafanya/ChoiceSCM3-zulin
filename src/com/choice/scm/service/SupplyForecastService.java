package com.choice.scm.service;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.OracleUtil;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.SupplyForecastMapper;

@Service
public class SupplyForecastService {

	@Autowired
	private SupplyForecastMapper supplyForecastMapper;
	
	private final transient Log log = LogFactory.getLog(SupplyForecastService.class);		
			
	/**
	 * 修改物资的货架信息
	 * @param id
	 * @param positn1
	 * @throws CRUDException
	 */
	public void saveABCByUpd(String id, Supply supply) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("supply", supply);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyForecastMapper.saveABCByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
